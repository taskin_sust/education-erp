﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Qmaster.Print.Helper
{
    public static class WebHelper
    {
        private const int RequestTimeout = 60 * 1000;

        public static string CommonErrorMessage
        {
            get { return "Something went wrong"; }
        }

        public static string GetRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.KeepAlive = false;
            request.Timeout = RequestTimeout;

            WebResponse response = request.GetResponse();
            using (Stream receiveStream = response.GetResponseStream())
            {
                if (receiveStream != null)
                {
                    StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    var responseString = readStream.ReadToEnd();
                    return responseString;
                }
            }
            response.Close();
            return string.Empty;
        }

        public static string PostRequest(string url, string data)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.KeepAlive = false;
            request.Timeout = RequestTimeout;

            byte[] byteArray = Encoding.UTF8.GetBytes(data);
            //request.ContentType = "application/x-www-form-urlencoded";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            request.ContentLength = byteArray.Length;

            using (Stream dataStream1 = request.GetRequestStream())
            {
                dataStream1.Write(byteArray, 0, byteArray.Length);
                dataStream1.Close();
            }

            WebResponse response = request.GetResponse();

            using (Stream receiveStream = response.GetResponseStream())
            {
                if (receiveStream != null)
                {
                    StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    var responseString = readStream.ReadToEnd();
                    return responseString;
                }
            }
            response.Close();
            return string.Empty;
        }

    }

    public static class APIInformation
    {
        public static string UrlLink()
        {
            return "http://localhost/";
        }
    }
}
