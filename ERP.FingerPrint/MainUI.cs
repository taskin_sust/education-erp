﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using UdvashERP.FingerPrint.FormUI;
using System.Xml.Serialization;
using UdvashERP.FingerPrint.BusinessModel;
using System.IO;

namespace UdvashERP.FingerPrint
{
    public partial class MainUI : BaseForm
    {
        #region Propertise & Object Initialization

        public MainUI()
        {
            InitializeComponent();
        }

        private void MainUI_Load(object sender, EventArgs e)
        {
            smiLogin_ItemClick(null, null);
        }

        #endregion

        #region Operational Menu Function

        private void smiLogin_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            UserLogin dlg = new UserLogin();
            dlg.ShowDialog();

            //
        }

        private void siDeviceCommunication_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeviceSettings dlg = new DeviceSettings();
            BringTabbedMdiTop(dlg, false, e.Item.Name, 1);
        }

        private void siScanFinger_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ScanFinger dlg = new ScanFinger();
            BringTabbedMdiTop(dlg, false, e.Item.Name, 1);
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        public Form BringTabbedMdiTop(BaseForm dlg, bool isDialog, string menuName, int imageIndex)
        {
            return BringTabbedMdiTopx(dlg, isDialog, menuName, imageIndex, true);
        }

        public Form BringTabbedMdiTopx(BaseForm dlg, bool isDialog, string menuName, int imageIndex, bool isNeedToCheckAuthorization)
        {
            try
            {
                #region menu authorization checking

                bool isAuthorized = false;
                //if (isNeedToCheckAuthorization == false)
                //    isAuthorized = true;
                //else if (string.IsNullOrEmpty(menuName) == false)
                //{
                //    isAuthorized = authorizedMenuList.Contains(menuName);
                //}

                //testOnly
                isAuthorized = true;

                #endregion

                #region  Checking dialog or TabbedMDI

                if (isAuthorized)
                {
                    if (isDialog)
                    {
                        dlg.ShowDialog();
                        return dlg;
                    }
                    else
                    {
                        //finding child is opened
                        Form returnForm;
                        bool isFindChild = false;
                        int nChild = this.MdiChildren.Length, i = 0;
                        for (i = 0; i < nChild; i++)
                        {
                            //searching window text
                            if (this.MdiChildren[i].Text == dlg.Text)
                            {
                                isFindChild = true;
                                break;
                            }
                        }

                        if (isFindChild)
                        {
                            //if tab found just set active
                            dlg.Dispose();
                            this.MdiChildren[i].Activate();
                            returnForm = this.MdiChildren[i];
                        }
                        else
                        {
                            //tab not found so create one
                            dlg.MdiParent = this;
                            dlg.Show();
                            // xtMdiManager.SelectedPage.ImageIndex = imageIndex;
                            returnForm = dlg;
                        }
                        return returnForm;
                    }
                }
                else
                {
                    XtraMessageBox.Show(this, "You are not authorized to perform this action.", "Access denied...",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return null;
                }

                #endregion
            }
            catch (Exception ex)
            {

                XtraMessageBox.Show(this, "Application error.", "Application error",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return null;
            }
        }


        #endregion

        #region Helper Function

        #endregion

    }
}