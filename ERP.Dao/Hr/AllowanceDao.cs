﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using NHibernate.Loader.Criteria;
using NHibernate.Persister.Entity;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IAllowanceDao : IBaseDao<Allowance, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        Allowance GetAllowance(string pin, DateTime date);

        #endregion

        #region List Loading Function

        IList<DailyAllowanceDto> LoadDailyAllowance(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdIdList, long? campusId, long? departmentId, int start, int length, string orderBy, string orderDir, bool isHistory);

        #endregion

        #region Others Function

        int DailyAllowanceRowCount(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdIdList, long? campusId, long? departmentId, bool isHistory);
        bool CheckHasAllounceOnADate(long teamMemberId, DateTime date);

        #endregion

        #region Helper Function
        #endregion

    }
    public class AllowanceDao : BaseDao<Allowance, long>, IAllowanceDao
    {

        #region Propertise & Object Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public Allowance GetAllowance(string pin, DateTime date)
        {
            var criteria = Session.CreateCriteria<Allowance>();
            criteria.CreateAlias("TeamMember", "hm");
            criteria.Add(Restrictions.Eq("hm.Pin", Convert.ToInt32(pin)));
            criteria.Add(Restrictions.Eq("DailyAllowanceDate", date));
            criteria.SetMaxResults(1);
            criteria.UniqueResult<Allowance>();
            //var query = GetGeneratedSql(criteria);
            return criteria.UniqueResult<Allowance>();
        }
        /*public String GetGeneratedSql(ICriteria criteria)
        {
            var criteriaImpl = (CriteriaImpl)criteria;
            var sessionImpl = (SessionImpl)criteriaImpl.Session;
            var factory = (SessionFactoryImpl)sessionImpl.SessionFactory;
            var implementors = factory.GetImplementors(criteriaImpl.EntityOrClassName);
            var loader = new CriteriaLoader((IOuterJoinLoadable)factory.GetEntityPersister(implementors[0]), factory, criteriaImpl, implementors[0], sessionImpl.EnabledFilters);

            return loader.SqlString.ToString();
        }*/
        #endregion

        #region List Loading Function
        
        public IList<DailyAllowanceDto> LoadDailyAllowance(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdIdList, long? campusId, long? departmentId, int start, int length, string orderBy, string orderDir, bool isHistory)
        {
            var filterPagination = "";
            if (length > 0)
            {
                filterPagination = " where pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            string sqlQuery = GetDailyAllowanceQuery(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdIdList, campusId, departmentId, isHistory);
            string query = @"DECLARE @rowsperpage INT 
		                    DECLARE @start INT 
		                    SET @start = " + (start + 1) + @" 
		                    SET @rowsperpage = " + length + @" 
                Select * From (SELECT row_number() OVER (ORDER BY A.Id asc) AS RowNum, A.Id,A.Pin,A.TeamMemberId,A.Name,A.Department,A.Designation,A.Organization,A.Branch,A.Campus,A.DailyAllowanceDate,A.TimeFrom,A.TimeTo,A.IsApproved,A.ModifyBy, A.LastModificationDate
	                            FROM(
		                            " + sqlQuery + @"
	                            ) A ) as pagination 
                            " + filterPagination;

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<DailyAllowanceDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<DailyAllowanceDto>().ToList();
        }
        
        #endregion

        #region Others Function

        public int DailyAllowanceRowCount(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdIdList, long? campusId, long? departmentId, bool isHistory)
        {
            string sqlQuery = GetDailyAllowanceQuery(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdIdList, campusId, departmentId, isHistory);
            string query = "Select Count(*) as totalRow From (" + sqlQuery + ") AS A";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(5000);
            return iQuery.UniqueResult<int>();
        }

        public bool CheckHasAllounceOnADate(long teamMemberId, DateTime date)
        {
            int allowanceCount = Session.QueryOver<Allowance>().Where(x => x.DailyAllowanceDate.Value.Date == date.Date && x.IsApproved == true && x.Status == Allowance.EntityStatus.Active && x.TeamMember.Id == teamMemberId).RowCount();

            if (allowanceCount > 0)
                return true;
            return false;
        }

        #endregion

        #region Helper Function

        private string GetDailyAllowanceQuery(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdIdList, long? campusId, long? departmentId, bool isHistory)
        {
            string query = "";
            string pinFiltering = "";
            string filtering = "";
            string mentorTeamMemberFiltering = "";
            string attendanceFilterQuery = " AttendanceDate >= '" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "' AND AttendanceDate <= '" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "'  ";
            string dailyAllowanceDateFilterQuery = " and hra.DailyAllowanceDate >= '" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "' AND hra.DailyAllowanceDate <= '" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "'  ";

            if (!string.IsNullOrEmpty(pinList))
            {
                pinFiltering = " and [Pin] IN (" + pinList + ")";
            }
            if (mentorTeamMemberIdList.Any())
            {
                mentorTeamMemberFiltering += " and Id IN (" + String.Join(", ", mentorTeamMemberIdList) + ")";
            }
            if (organizationIdList != null && organizationIdList.Any())
            {
                filtering += " and o.Id IN  (" + String.Join(", ", organizationIdList) + ")";
            }
            if (branchIdIdList != null && branchIdIdList.Any())
            {
                filtering += " and br.Id IN (" + String.Join(", ", branchIdIdList) + ")";
            }
            if (campusId != null && campusId > 0)
            {
                filtering += " and c.Id = " + campusId;
            }
            if (departmentId != null && departmentId > 0)
            {
                filtering += " and dp.Id = " + departmentId;
            }

            if (isHistory)
            {
                query = @"Select hras.*
	                            ,ISNULL(hra.IsApproved,0) AS IsApproved
	                            ,hrm.Name
	                            ,hrm.Pin
	                            ,c.Name AS Campus
	                            ,br.Name AS Branch
	                            ,o.ShortName AS Organization
	                            ,dp.Name AS Department
	                            ,dg.Name AS Designation
                                ,hra.ModifyBy
                                , hra.ModificationDate as LastModificationDate
                            from [dbo].[HR_Allowance] AS hra	 
                            INNER Join (
	                            Select Id,TeamMemberId AS TeamMemberId,AttendanceDate AS DailyAllowanceDate, [InTime] as TimeFrom,[OutTime] as TimeTo 
	                            from [dbo].[HR_AttendanceSummary]
	                            Where " + attendanceFilterQuery + @" AND [TeamMemberId] IN (
		                            SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1  " + mentorTeamMemberFiltering + pinFiltering + @" 
	                            ) AND [Status] = " + AttendanceSummary.EntityStatus.Active
                          + @") AS hras ON hra.TeamMemberId = hras.TeamMemberId and hra.DailyAllowanceDate = hras.DailyAllowanceDate " + dailyAllowanceDateFilterQuery + @"         
                            Left Join [dbo].[HR_TeamMember] AS hrm ON hrm.Id = hras.TeamMemberId AND hrm.[Status] = " +
                          TeamMember.EntityStatus.Active + @"

                            CROSS APPLY  (
	                            Select * from (
		                            Select 
		                            emh.CampusId as CampusId
		                            , emh.DesignationId as DesignationId
		                            , emh.DepartmentId as DepartmentId
		                            , emh.TeamMemberId as TeamMemberId
		                            , emh.EffectiveDate As EffectiveDate
		                            , emh.EmploymentStatus as EmploymentStatus
		                            , Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		                            from HR_EmploymentHistory as emh
		                            where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		                            and  emh.EffectiveDate <=  hras.DailyAllowanceDate 
		                            and emh.TeamMemberId = hras.TeamMemberId
	                            ) as employmentHistory 
	                            where 1=1
	                            and employmentHistory.r = 1
                            ) as currentPosition

                            Left Join [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status =  " +
                          Campus.EntityStatus.Active + @"
                            Left Join [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status =  " +
                          Branch.EntityStatus.Active + @"
                            Left Join [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  " +
                          Organization.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  " +
                          Department.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  " +
                          Designation.EntityStatus.Active + " Where 1=1 AND hra.Status = " + Allowance.EntityStatus.Active + filtering;
            }
            else
            {
                query = @"Select hras.*
	                            ,ISNULL(hra.IsApproved,0) AS IsApproved
	                            ,hrm.Name
	                            ,hrm.Pin
	                            ,c.Name AS Campus
	                            ,br.Name AS Branch
	                            ,o.ShortName AS Organization
	                            ,dp.Name AS Department
	                            ,dg.Name AS Designation
                                ,hra.ModifyBy
                                , hra.ModificationDate as LastModificationDate
                            from (
	                            Select Id,TeamMemberId AS TeamMemberId,AttendanceDate AS DailyAllowanceDate, [InTime] as TimeFrom,[OutTime] as TimeTo 
	                            from [dbo].[HR_AttendanceSummary]
	                            Where " + attendanceFilterQuery + @" AND [TeamMemberId] IN (
		                            SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1  " + mentorTeamMemberFiltering + pinFiltering + @" 
	                            ) AND [Status] = " + AttendanceSummary.EntityStatus.Active
                           + @") AS hras
                            Left Join [dbo].[HR_Allowance] AS hra ON hra.TeamMemberId = hras.TeamMemberId AND hra.Status = " + Allowance.EntityStatus.Active + dailyAllowanceDateFilterQuery + @"
                            Left Join [dbo].[HR_TeamMember] AS hrm ON hrm.Id = hras.TeamMemberId AND hrm.[Status] = " +
                           TeamMember.EntityStatus.Active + @"

                            CROSS APPLY  (
	                            Select * from (
		                            Select 
		                            emh.CampusId as CampusId
		                            , emh.DesignationId as DesignationId
		                            , emh.DepartmentId as DepartmentId
		                            , emh.TeamMemberId as TeamMemberId
		                            , emh.EffectiveDate As EffectiveDate
		                            , emh.EmploymentStatus as EmploymentStatus
		                            , Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		                            from HR_EmploymentHistory as emh
		                            where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		                            and  emh.EffectiveDate <=  hras.DailyAllowanceDate 
		                            and emh.TeamMemberId = hras.TeamMemberId
	                            ) as employmentHistory 
	                            where 1=1
	                            and employmentHistory.r = 1
                            ) as currentPosition

                            Left Join [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status =  " +
                           Campus.EntityStatus.Active + @"
                            Left Join [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status =  " +
                           Branch.EntityStatus.Active + @"
                            Left Join [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  " +
                           Organization.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  " +
                           Department.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  " +
                           Designation.EntityStatus.Active + " Where 1=1 " + filtering;
            }

            return query;
        }
//        private string GetDailyAllowanceQuery(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdIdList, long? campusId, long? departmentId, bool isHistory)
//        {
//            string query = "";
//            string pinFiltering = "";
//            string filtering = "";
//            string mentorTeamMemberFiltering = "";
//            string attendanceFilterQuery = " AttendanceDate >= '" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "' AND AttendanceDate <= '" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "'  ";
//            string dailyAllowanceDateFilterQuery = " and hra.DailyAllowanceDate >= '" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "' AND hra.DailyAllowanceDate <= '" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "'  ";

//            if (!string.IsNullOrEmpty(pinList))
//            {
//                pinFiltering = " and [Pin] IN (" + pinList + ")";
//            }
//            if (mentorTeamMemberIdList.Any())
//            {
//                mentorTeamMemberFiltering += " and Id IN (" + String.Join(", ", mentorTeamMemberIdList) + ")";
//            }
//            if (organizationIdList != null && organizationIdList.Any())
//            {
//                filtering += " and o.Id IN  (" + String.Join(", ", organizationIdList) + ")";
//            }
//            if (branchIdIdList != null && branchIdIdList.Any())
//            {
//                filtering += " and br.Id IN (" + String.Join(", ", branchIdIdList) + ")";
//            }
//            if (campusId != null && campusId > 0)
//            {
//                filtering += " and c.Id = " + campusId;
//            }
//            if (departmentId != null && departmentId > 0)
//            {
//                filtering += " and dp.Id = " + departmentId;
//            }

//            if (isHistory)
//            {
//                query = @"Select hras.*
//	                            ,ISNULL(hra.IsApproved,0) AS IsApproved
//	                            ,hrm.Name
//	                            ,hrm.Pin
//	                            ,c.Name AS Campus
//	                            ,br.Name AS Branch
//	                            ,o.ShortName AS Organization
//	                            ,dp.Name AS Department
//	                            ,dg.Name AS Designation
//                            from [dbo].[HR_Allowance] AS hra	 
//                            INNER Join (
//	                            Select Id,TeamMemberId AS TeamMemberId,AttendanceDate AS DailyAllowanceDate, [InTime] as TimeFrom,[OutTime] as TimeTo 
//	                            from [dbo].[HR_AttendanceSummary]
//	                            Where " + attendanceFilterQuery + @" AND [TeamMemberId] IN (
//		                            SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1  " + mentorTeamMemberFiltering + pinFiltering + @" 
//	                            ) AND [Status] = " + AttendanceSummary.EntityStatus.Active
//                          + @") AS hras ON hra.TeamMemberId = hras.TeamMemberId and hra.DailyAllowanceDate = hras.DailyAllowanceDate " + dailyAllowanceDateFilterQuery + @"         
//                            Left Join [dbo].[HR_TeamMember] AS hrm ON hrm.Id = hras.TeamMemberId AND hrm.[Status] = " +
//                          TeamMember.EntityStatus.Active + @"
//
//                            Left Join (
//	                            Select * from (
//		                            Select [CampusId]
//			                            ,[DesignationId]
//			                            ,[DepartmentId]
//			                            ,[TeamMemberId]
//			                            ,[EffectiveDate]
//			                            ,[EmploymentStatus]
//			                            ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC) AS R  
//			                            from [dbo].[HR_EmploymentHistory] 
//			                            Where [Status] = " + EmploymentHistory.EntityStatus.Active +
//                          @" and TeamMemberId IN (SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1 " + pinFiltering +
//                          @") and EffectiveDate<='" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + @"'
//	                            ) AS A Where A.R = 1 
//                            ) AS currentPosition ON currentPosition.TeamMemberId  = hras.TeamMemberId
//
//                            Left Join [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status =  " +
//                          Campus.EntityStatus.Active + @"
//                            Left Join [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status =  " +
//                          Branch.EntityStatus.Active + @"
//                            Left Join [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  " +
//                          Organization.EntityStatus.Active + @"
//                            Left Join [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  " +
//                          Department.EntityStatus.Active + @"
//                            Left Join [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  " +
//                          Designation.EntityStatus.Active + " Where 1=1 " + filtering;
//            }
//            else
//            {
//                query = @"Select hras.*
//	                            ,ISNULL(hra.IsApproved,0) AS IsApproved
//	                            ,hrm.Name
//	                            ,hrm.Pin
//	                            ,c.Name AS Campus
//	                            ,br.Name AS Branch
//	                            ,o.ShortName AS Organization
//	                            ,dp.Name AS Department
//	                            ,dg.Name AS Designation
//                            from (
//	                            Select Id,TeamMemberId AS TeamMemberId,AttendanceDate AS DailyAllowanceDate, [InTime] as TimeFrom,[OutTime] as TimeTo 
//	                            from [dbo].[HR_AttendanceSummary]
//	                            Where " + attendanceFilterQuery + @" AND [TeamMemberId] IN (
//		                            SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1  " + mentorTeamMemberFiltering + pinFiltering + @" 
//	                            ) AND [Status] = " + AttendanceSummary.EntityStatus.Active
//                           + @") AS hras
//                            Left Join [dbo].[HR_Allowance] AS hra ON hra.TeamMemberId = hras.TeamMemberId " + dailyAllowanceDateFilterQuery + @"
//                            Left Join [dbo].[HR_TeamMember] AS hrm ON hrm.Id = hras.TeamMemberId AND hrm.[Status] = " +
//                           TeamMember.EntityStatus.Active + @"
//                            Left Join (
//	                            Select * from (
//		                            Select [CampusId]
//			                            ,[DesignationId]
//			                            ,[DepartmentId]
//			                            ,[TeamMemberId]
//			                            ,[EffectiveDate]
//			                            ,[EmploymentStatus]
//			                            ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC) AS R  
//			                            from [dbo].[HR_EmploymentHistory] 
//			                            Where [Status] = " + EmploymentHistory.EntityStatus.Active +
//                           @" and TeamMemberId IN (SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1 " + pinFiltering +
//                           @") and EffectiveDate<='" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + @"'
//	                            ) AS A Where A.R = 1 
//                            ) AS currentPosition ON currentPosition.TeamMemberId  = hras.TeamMemberId
//                            Left Join [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status =  " +
//                           Campus.EntityStatus.Active + @"
//                            Left Join [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status =  " +
//                           Branch.EntityStatus.Active + @"
//                            Left Join [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  " +
//                           Organization.EntityStatus.Active + @"
//                            Left Join [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  " +
//                           Department.EntityStatus.Active + @"
//                            Left Join [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  " +
//                           Designation.EntityStatus.Active + " Where 1=1 " + filtering;
//            }

//            return query;
//        }
        
        #endregion

    }

}
