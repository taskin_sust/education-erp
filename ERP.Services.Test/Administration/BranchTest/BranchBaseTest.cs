﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq.Functions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.BranchTest
{
    public class BranchBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal readonly BranchService _branchService;
        internal OrganizationService _organizationService;
        internal StudentExamService _studentExamService;
        internal ProgramBranchSessionService _programBranchSessionService;
        internal List<long> IdList = new List<long>();
        private  ISession _session;
        internal BranchFactory _branchFactory;
        internal ProgramFactory _programFactory;
        internal SessionFactory _SessionFactory;
        internal TestBaseService<ProgramBranchSession> _TestBaseService;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        //internal readonly string _code;
        internal const long OrgId = 1;
        internal DateTime _startTime = DateTime.Now.AddDays(1).AddHours(1);
        internal DateTime _endTime = DateTime.Now.AddDays(1).AddHours(10);
        #endregion

        public BranchBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _branchFactory = new BranchFactory(null, _session);
            _programFactory = new ProgramFactory(null,_session);
            _SessionFactory = new SessionFactory(null,_session);
            _branchService = new BranchService(_session);
            _organizationService=new OrganizationService(_session);
            _studentExamService = new StudentExamService(Session);
            _programBranchSessionService = new ProgramBranchSessionService(Session);
           // _code = _branchFactory._code;
            _TestBaseService = new TestBaseService<ProgramBranchSession>(Session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            //delete on load branch test
            if (_programFactory.Object != null && _SessionFactory.Object != null && _branchFactory.ObjectList != null && _branchFactory.ObjectList.Count >0 )
            {
                _TestBaseService.DeleteProgramBranchSession(_programFactory.Object.Id, _SessionFactory.Object.Id, _branchFactory.ObjectList.Select(x => x.Id).ToArray());
                _TestBaseService.DeleteProgramStudentExamSession(_programFactory.Object.Id, _SessionFactory.Object.Id);
            }
            _programFactory.Cleanup();
            _branchFactory.Cleanup();
            _SessionFactory.Cleanup();
            base.Dispose();
        }
    }   
}
