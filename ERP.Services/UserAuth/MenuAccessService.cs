﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.UserAuth
{
    public interface IMenuAccessService : IBaseService
    {
        #region Operational Function
        void Save(MenuAccess erpMenuAccess);
        void Update(long id, MenuAccess erpMenuAccess);
        void Delete(MenuAccess menuAccessObj);
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        IList<MenuAccess> LoadActiveByMenuId(long menu); 
        #endregion
        
        #region Others Function
        bool CheckedMenuAccess(Menu menu, AreaControllers controller, Actions action);
        #endregion
    }    
    public class MenuAccessService : BaseService, IMenuAccessService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UserAuthService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IMenuAccessDao _menuAccessDao;
        private readonly IMenuDao _menuDao;
        //private readonly IAreaControllersDao _areaControllersDao;
        private readonly IActionsDao _actionsDao;
        public MenuAccessService(ISession session)
        {
            Session = session;
            _menuAccessDao = new MenuAccessDao() { Session = Session };
            _menuDao = new MenuDao() { Session = Session };
            _actionsDao = new ActionsDao(){Session = Session};
        }
        #endregion

        #region Operational Function
        public void Save(MenuAccess erpMenuAccess)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    _menuAccessDao.Save(erpMenuAccess);
                    transaction.Commit();
                }
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        public void Update(long id, MenuAccess menuAccessObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    var tempMenuAccess = _menuAccessDao.LoadById(id);
                    tempMenuAccess.Id = id;
                    tempMenuAccess.Menu = menuAccessObj.Menu;
                    tempMenuAccess.HasReferrer = menuAccessObj.HasReferrer;
                    tempMenuAccess.Actions = menuAccessObj.Actions;
                    _menuAccessDao.Update(tempMenuAccess);
                    transaction.Commit();
                }
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        public void Delete(MenuAccess menuAccess)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    
                    var menuAccessObj = _menuAccessDao.LoadById(menuAccess.Id);
                    menuAccessObj.Actions.MenuAccess.Remove(menuAccessObj);
                    menuAccessObj.Menu.MenuAccess.Remove(menuAccessObj);
                    _menuAccessDao.Delete(menuAccessObj);
                   
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function
      
        #endregion

        #region List Loading Function
        public IList<MenuAccess> LoadActiveByMenuId(long menu)
        {
            try
            {
                return _menuAccessDao.LoadActiveByMenuId(menu);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        #endregion

        #region Others Function
        public bool CheckedMenuAccess(Menu menu, AreaControllers controller, Actions action)
        {
            try
            {
                return _menuAccessDao.CheckedMenuAccess(menu, controller, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        #endregion        
    }
}
