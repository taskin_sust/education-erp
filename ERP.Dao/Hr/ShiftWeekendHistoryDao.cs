﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IShiftWeekendHistoryDao : IBaseDao<ShiftWeekendHistory, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        ShiftWeekendHistory GetShiftWeekendHistory(long id);
        ShiftWeekendHistory GetLastShiftWeekendHistory(TeamMember teamMember, DateTime punchDate, long id = 0);
        ShiftWeekendHistory GetNextShiftWeekendHistory(TeamMember member, DateTime effectiveDate, long id = 0);

        #endregion

        #region List Loading Function

        IList<ShiftWeekendHistory> LoadShiftWeekendHistory(long teamMemberId, DateTime? searchingDate);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
    public class ShiftWeekendHistoryDao : BaseDao<ShiftWeekendHistory, long>, IShiftWeekendHistoryDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public ShiftWeekendHistory GetShiftWeekendHistory(long id)
        {
            return Session.QueryOver<ShiftWeekendHistory>().Where(x => x.Id == id).Take(1).SingleOrDefault<ShiftWeekendHistory>();
        }

        public ShiftWeekendHistory GetLastShiftWeekendHistory(TeamMember teamMember, DateTime punchDate, long id = 0)
        {
            return
                Session.QueryOver<ShiftWeekendHistory>()
                    .Where(x => x.TeamMember == teamMember && x.EffectiveDate <= punchDate && x.Id != id)
                    .OrderBy(x => x.EffectiveDate)
                    .Desc()
                    .Take(1)
                    .SingleOrDefault<ShiftWeekendHistory>();
        }

        public ShiftWeekendHistory GetNextShiftWeekendHistory(TeamMember teamMember, DateTime effectiveDate, long id = 0)
        {
            return
                Session.QueryOver<ShiftWeekendHistory>()
                    .Where(x => x.TeamMember == teamMember && x.EffectiveDate > effectiveDate && x.Id != id)
                    .OrderBy(x => x.EffectiveDate)
                    .Asc()
                    .Take(1)
                    .SingleOrDefault<ShiftWeekendHistory>();
        }

        #endregion

        #region List Loading Function
        
        public IList<ShiftWeekendHistory> LoadShiftWeekendHistory(long teamMemberId, DateTime? searchingDate)
        {
            var criteria = Session.CreateCriteria<ShiftWeekendHistory>().Add(Restrictions.Eq("Status", ShiftWeekendHistory.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "tm").Add(Restrictions.Not(Restrictions.Eq("tm.Status", TeamMember.EntityStatus.Delete)));
            criteria.CreateAlias("Shift", "sift").Add(Restrictions.Not(Restrictions.Eq("sift.Status", Shift.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("tm.Id", teamMemberId));
            if (searchingDate != null)
                criteria.Add(Restrictions.Le("EffectiveDate", searchingDate.Value.Date));
            criteria.AddOrder(Order.Desc("EffectiveDate"));
            return criteria.List<ShiftWeekendHistory>();
        }

        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion

       
    }

}
