using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class CalendarMap : BaseClassMap<Calendar,long> {
        
        public CalendarMap() {
			Table("HR_Calendar");
			LazyLoad();
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
        }
    }
}
