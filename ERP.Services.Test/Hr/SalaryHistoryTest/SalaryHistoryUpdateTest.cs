﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.SalaryHistoryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "SalaryHistory")]
    public class SalaryHistoryUpdateTest : SalaryHistoryBaseTest
    {
        #region Basic Test

        [Fact]
        public void SalaryHistory_Save_And_Update_Should_Pass_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            //_salaryHistoryFactory.Object.EffectiveDate = DateTime.Now.AddDays(5);
            _salaryHistoryFactory.Object.SalaryPurpose = SalaryPurpose.AdministrativePurpose;
            _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object);
            Assert.NotNull(_salaryHistoryService.GetSalaryHistory(_salaryHistoryFactory.Object.Id));
        }


        [Fact]
        public void SalaryHistory_Save_And_Update_Salary_Orgranization_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
               // .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.SalaryPurpose = SalaryPurpose.AdministrativePurpose;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));
        }

        [Fact]
        public void SalaryHistory_Save_And_Update_Salary_Department_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.SalaryDepartment = null;
            _salaryHistoryFactory.Object.SalaryPurpose = SalaryPurpose.AdministrativePurpose;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));
        }

        [Fact]
        public void SalaryHistory_Save_And_Update_Salary_Designation_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            //var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                //.WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.SalaryPurpose = SalaryPurpose.AdministrativePurpose;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));
        }

        [Fact]
        public void SalaryHistory_Save_And_Update_Salary_Campaus_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.SalaryCampus = null;
            _salaryHistoryFactory.Object.SalaryPurpose = SalaryPurpose.AdministrativePurpose;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));
        }

        [Fact]
        public void SalaryHistory_Save_And_Update_Salary_Purpose_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.SalaryPurpose = new SalaryPurpose();
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));
        }

        [Fact]
        public void SalaryHistory_Save_And_Update_Salary_EffectiveDate_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.EffectiveDate = null;
            _salaryHistoryFactory.Object.SalaryPurpose = SalaryPurpose.AdministrativePurpose;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));
        }

        
        #endregion

    }
}
