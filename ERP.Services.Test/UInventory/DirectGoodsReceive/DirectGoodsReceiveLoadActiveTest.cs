﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.DirectGoodsReceive
{

    public interface IDirectGoodsReceiveLoadActiveTest
    {
        void Should_Return_List_of_GoodsReceive_By_WorkOrderId_Succesfully();
        void Should_Return_Invalid_WorkorderId();
        void Should_Return_List_Of_ItemOpeningBalanceDetailsViewModel_WithAuth_Successfully();
        void Should_Return_List_Of_ItemOpeningBalanceDetailsViewModel_WithoutAuth_Successfully();
        void Should_Return_List_Of_GoodsReceive_Successfully();
        void Should_Return_Invalid_User_Menu();
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReceive")]
    public class DirectGoodsReceiveLoadActiveTest : DirectGoodsReceiveBaseTest, IDirectGoodsReceiveLoadActiveTest
    {

        [Fact]
        public void Should_Return_List_of_GoodsReceive_By_WorkOrderId_Succesfully()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(_branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();

            // receive against workorder number
            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            IList<GoodsReceive> list = _goodsReceiveService.LoadGoodsReceiveByWorkOrder(_directWorkOrderFactory.Object.Id);
            Assert.True(list.Count > 0);
        }

        public void Should_Return_Invalid_WorkorderId()
        {
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.LoadGoodsReceiveByWorkOrder(-1));
        }

        public void Should_Return_List_Of_ItemOpeningBalanceDetailsViewModel_WithAuth_Successfully()
        {
            throw new NotImplementedException();
        }

        public void Should_Return_List_Of_ItemOpeningBalanceDetailsViewModel_WithoutAuth_Successfully()
        {
            throw new NotImplementedException();
        }

        [Fact]
        public void Should_Return_List_Of_GoodsReceive_Successfully()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(_organizationFactory.Object, _commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), _branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(_branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();
            //receive
            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            List<OrderedGoodsRecViewModel> list = _goodsReceiveService.LoadGoodsReceived(0, 0, int.MaxValue, userMenus, _organizationFactory.Object.Id, _branchFactory.Object.Id,
                _itemFactory.Object.Name, _goodsReceiveFactory.Object.GoodsReceivedDate.ToString(), _directWorkOrderFactory.Object.WorkOrderNo, _directWorkOrderFactory.Object.WorkOrderDate.ToString(), "");
            Assert.True(list.Count > 0);
        }

        [Fact]
        public void Should_Return_Invalid_User_Menu()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();
            
            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(_branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();

            //receive
            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.LoadGoodsReceived(0, 0, int.MaxValue, null, _organizationFactory.Object.Id, _branchFactory.Object.Id, "", DateTime.Now.ToString(), "", "", ""));
        }
    }
}
