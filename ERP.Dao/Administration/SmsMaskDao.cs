﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{

    public interface ISmsMaskDao : IBaseDao<SmsMask, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        //IList<SmsMask> LoadSmsMask(int start, int length, string orderBy, string orderDir, string organization, string name, string status);
        IList<SmsMask> LoadSmsMask(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, string name, string status);
        IList<SmsMask> LoadSmsMask(List<long> organizationIdList, int? status);
        #endregion

        #region Others Function
        bool CheckDuplicateFields(string name, long organizationId, long id);
        //int GetSmsMaskCount(string orderBy, string orderDir, string organization, string name, string status);
        int GetSmsMaskCount(string orderBy, string orderDir, List<long> organizationIdList, string name, string status);
        #endregion

    }

    public class SmsMaskDao : BaseDao<SmsMask, long>, ISmsMaskDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function


        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        //public IList<SmsMask> LoadSmsMask(int start, int length, string orderBy, string orderDir, string organization, string name, string status)
        public IList<SmsMask> LoadSmsMask(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, string name, string status)
        {
            //ICriteria criteria = GetSmsMaskCriteria(orderBy, orderDir, organization, name, status);
            ICriteria criteria = GetSmsMaskCriteria(orderBy, orderDir, organizationIdList, name, status);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<SmsMask>();
        }

        public IList<SmsMask> LoadSmsMask(List<long> organizationIdList, int? status )
        {
            var criteria = Session.CreateCriteria<SmsMask>();
            if(status != null)
                criteria.Add(Restrictions.Eq("Status", status));
            if (organizationIdList != null && !organizationIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("Organization.Id", organizationIdList));
            }
            var dataList = criteria.List<SmsMask>().ToList().OrderBy(x => x.Rank).ToList();
            return dataList;
        }
        #endregion

        #region Others Function
        public bool CheckDuplicateFields(string name, long organizationId, long id)
        {
            ICriteria criteria = Session.CreateCriteria<SmsMask>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", SmsMask.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("Name", name));
            //criteria.Add(Expression.Sql("Name =? COLLATE Latin1_General_CS_AS", name, NHibernateUtil.String));
            criteria.Add(Restrictions.Eq("Organization.Id", organizationId));
            if (id > 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            criteria.SetProjection(Projections.RowCount());
            var smsMaskCount = Convert.ToInt32(criteria.UniqueResult());//criteria.List<SmsMask>();
            if (smsMaskCount > 0)
            {
                return true;
            }
            return false;
        }
        //public int GetSmsMaskCount(string orderBy, string orderDir, string organization, string name, string status)
        public int GetSmsMaskCount(string orderBy, string orderDir, List<long> organizationIdList, string name, string status)
        {
            //ICriteria criteria = GetSmsMaskCriteria(orderDir, orderDir, organization, name, status, true);
            ICriteria criteria = GetSmsMaskCriteria(orderDir, orderDir, organizationIdList, name, status, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function
        //private ICriteria GetSmsMaskCriteria(string orderBy, string orderDir, string organization, string name, string status, bool countQuery = false)
        private ICriteria GetSmsMaskCriteria(string orderBy, string orderDir, List<long> organizationIdList, string name, string status, bool countQuery = false)
        {
            var criteria = Session.CreateCriteria<SmsMask>().Add(Restrictions.Not(Restrictions.Eq("Status", SmsMask.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Eq("organization.Status",Organization.EntityStatus.Active));
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name.Trim(), MatchMode.Anywhere));
            }
            //if (!String.IsNullOrEmpty(organization))
            //{
            //    criteria.Add(Restrictions.Eq("organization.Id",Convert.ToInt64(organization)));
            //}

            if (organizationIdList != null)
            {
                criteria.Add(Restrictions.In("organization.Id", organizationIdList));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status.Trim())));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }
        #endregion
    }

}
