﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherSubjectPriorityService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<TeacherSubjectPriority> LoadSubjectPriorityByTeacherId(long id);
        #endregion

        #region Others Function

        #endregion
    }
    public class TeacherSubjectPriorityService : BaseService, ITeacherSubjectPriorityService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ITeacherSubjectPriorityDao _teacherSubjectPriorityDao;

        public TeacherSubjectPriorityService(ISession session)
        {
            Session = session;
            _teacherSubjectPriorityDao = new TeacherSubjectPriorityDao() { Session = session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<TeacherSubjectPriority> LoadSubjectPriorityByTeacherId(long id)
        {
            try
            {
                return _teacherSubjectPriorityDao._teacherSubjectPriorityService(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function

        #endregion
    }
}
