﻿using System;
using System.Configuration;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.Dao.UserAuth;

namespace UdvashERP.Test.SessionFactory
{
    public static class NHibernateSessionFactory
    {
        private static ISessionFactory _sessionFactory;
        public static ISessionFactory GetSessionFactory()
        {
            try
            {
                if (_sessionFactory == null)
                {
                    //_sessionFactory = Fluently.Configure().

                    //   Database(MsSqlConfiguration.MsSql2008.ConnectionString(ConfigurationManager
                    //    //.ConnectionStrings["UdvashErpConnectionString"].ConnectionString))
                    //       .ConnectionStrings["UdvashErpConnectionString"].ConnectionString))
                    //   .Mappings(m => m.FluentMappings.AddFromAssemblyOf<StudentDao>())
                    //    //.ExposeConfiguration(BuildSchema)
                    //   .BuildSessionFactory();
                    ////NOTE: Before calling this method ensure that ASP.Net membership related tables are created. 
                    ////CreateUserProfileForeginKey(_sessionFactory.OpenSession());
                    _sessionFactory = Fluently.Configure()
                                .Database(
                                    MsSqlConfiguration.MsSql2008.ConnectionString(
                                        x => x
                                            .Server("192.168.0.7")
                                            .Database("udvash_erp_v5_3")
                                            .Username("coder")
                                            .Password("i_am_coder")
                                        )
                                )
                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<AspNetUserDao>())
                                .BuildSessionFactory();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return _sessionFactory;
        }

        //private static void BuildSchema(Configuration obj)
        //{
        //    var se = new SchemaExport(obj);
        //    se.Execute(true, true, false);
        //    //var connectionString = "";
        //}

        public static ISession OpenSession()
        {
            return GetSessionFactory().OpenSession();
        }
    }
}