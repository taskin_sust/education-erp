﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherClassEntryDetailsService : IBaseService
    {
        #region Operational Function
        bool Update(TeacherClassEntryDetails teacherClassEntryDetails);
        #endregion

        #region Single Instances Loading Function
        TeacherClassEntryDetails GetTeacherClassEntryDetails(long id);
        #endregion

        #region List Loading Function
        IList<TeacherClassEntryDto> LoadTeacherClassEntryDetails(int start, int length, string orderBy, string orderDir, long organization, long program, long session, long branch, long campus, int paymentStatus, string teacherName, string heldDate, string trackingId, string tpin);
        #endregion

        #region Others Function
        bool Delete(long teacherClassEntryDetailsId);
        long TeacherClassEntryDetailsRowCount(long organization, long program, long session, long branch, long campus, int paymentStatus, string teacherName, string heldDate, string trackingId, string tpin);
        #endregion
    }

    public class TeacherClassEntryDetailsService: BaseService, ITeacherClassEntryDetailsService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeacherService"); 
        #endregion

        #region Propertise & Object Initialization
            private readonly ITeacherClassTypeDao _teacherClassTypeDao;
            private readonly ITeacherClassEntryDao _teacherClassEntryDao;
            private readonly ITeacherClassEntryDetailsDao _teacherClassEntryDetailsDao;
            public TeacherClassEntryDetailsService(ISession session)
            {
                Session = session;
                _teacherClassTypeDao = new TeacherClassTypeDao() { Session = session };
                _teacherClassEntryDao = new TeacherClassEntryDao(){Session = session};
                _teacherClassEntryDetailsDao = new TeacherClassEntryDetailsDao(){Session = session};
            }
        #endregion

        #region Operational Function
           
            public bool Delete(long teacherClassEntryDetailsId)
            {
                ITransaction transaction = null;
                try
                {
                    var tempObj = _teacherClassEntryDetailsDao.LoadById(teacherClassEntryDetailsId);
                    if (tempObj.PaymentStatus == 1)
                        throw new DependencyException("You can't delete this class entry, payment is already paid.");

                    using (transaction = Session.BeginTransaction())
                    {
                        tempObj.Status = TeacherClassEntryDetails.EntityStatus.Delete;
                        _teacherClassEntryDetailsDao.Update(tempObj);
                        transaction.Commit();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    if (transaction != null && transaction.IsActive)
                        transaction.Rollback();
                    _logger.Error(ex);
                    throw;
                }
            }

            public bool Update(TeacherClassEntryDetails teacherClassEntryDetails)
            {
                ITransaction transaction = null;
                try{
                    if (teacherClassEntryDetails.PaymentStatus == 1)
                        throw new DependencyException("You can't update this class entry, payment is already paid.");

                    using (transaction = Session.BeginTransaction())
                    {
                        _teacherClassEntryDetailsDao.Update(teacherClassEntryDetails);
                        transaction.Commit();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    if (transaction != null && transaction.IsActive)
                        transaction.Rollback();
                    _logger.Error(ex);
                    throw;
                }
            }

        #endregion

        #region Single Instances Loading Function
            public TeacherClassEntryDetails GetTeacherClassEntryDetails(long id)
            {
                try
                {
                    return _teacherClassEntryDetailsDao.LoadById(id);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    throw;
                }
            }

        #endregion

        #region List Loading Function

            public IList<TeacherClassEntryDto> LoadTeacherClassEntryDetails(int start, int length, string orderBy, string orderDir, long organization, long program, long session, long branch, long campus, int paymentStatus, string teacherName, string heldDate, string trackingId, string tpin)
        {
            try
            {
                return _teacherClassEntryDetailsDao.LoadTeacherClassEntryDetails(start, length, orderBy, orderDir, organization, program, session, branch, campus, paymentStatus, teacherName, heldDate, trackingId, tpin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        

        #endregion

        #region Others Function
        public long TeacherClassEntryDetailsRowCount(long organization, long program, long session, long branch, long campus, int paymentStatus, string teacherName, string heldDate, string trackingId, string tpin) 
        {
            try
            {
                return _teacherClassEntryDetailsDao.TeacherClassEntryDetailsRowCount(organization, program, session, branch, campus, paymentStatus, teacherName, heldDate, trackingId, tpin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        
        #endregion   
    }
}
