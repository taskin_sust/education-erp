using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class ChildrenInfoMap : BaseClassMap<ChildrenInfo,long> {
        
        public ChildrenInfoMap() {
			Table("HR_ChildrenInfo");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Name).Column("Name");
			Map(x => x.Dob).Column("Dob");
			Map(x => x.Gender).Column("Gender");
			Map(x => x.IsStudying).Column("IsStudying");
			Map(x => x.Remarks).Column("Remarks");


            //HasMany(x => x.ChildrenInfoLog).KeyColumn("ChildrenInfoId");
        }
    }
}
