﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.MediaDb;

namespace UdvashERP.Dao.Mapping.MediaMapping
{
    public class StudentMediaImageMap : BaseClassMap<StudentMediaImage, long>
    {
        public StudentMediaImageMap()
        {
            Table("StudentMediaImage");
            LazyLoad();
            Map(x => x.ImageGuid);
            Map(x => x.Images).Column("Images").CustomSqlType("VARBINARY (MAX) FILESTREAM").Length(2147483647).LazyLoad();
            Map(x => x.ImageRef);
        }
    }
}
