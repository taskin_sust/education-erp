﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Exam
{
    public interface IExamsStudentMarksDao : IBaseDao<ExamsStudentMarks, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        ExamsStudentMarks LoadExamsStudentMarksByExamsDetailsStudentProgram(long examsDetailsId, long studentProgramId);
        ExamsStudentMarks LoadByExamDetailId(long p);
        IndividualMeritListDto GetIndividualReport(long programId, long sessionId, long examId, long studentProgramId, long branchId);

        #endregion

        #region List Loading Function
        IList<ExamsStudentMarks> LoadIndividualExamMarks(string studentProgramRoll, long? examId = null);
        IList<ExamsStudentMarks> GetExamMarksByExamId(long examId);
        IList<ExamsStudentMarks> LoadExamsStudentMarksByExamStudentProgram(long examId, long studentProgramId, int examType);
        IList<ExamsStudentMarks> GetExamMarksByProgramId(long examId, long studentProgramId, int examType);

        IList<ExamsStudentMarks> GetExamResultList(long[] selectedExam);

        IList<ExamsStudentMarks> GetExamMarksByProgramSession(long programId, long sessionId);
        IList<ExamsStudentMarks> GetExamResultList(long[] selectedExam, string studentProgramRoll);

        IList<ExamsStudentMarks> ClearMarksStudentList(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, string gender, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long examId, long examType);
        IList<IndividualMeritListNewDto> LoadIndividualMeritListReport(StudentProgram studentProgram);
        IList<ExamMeritListDto> GetExamResultList(List<long> programIdList, List<long> branchIdList, int start, int length, long[] selectedExam,
            string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds,
            long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn, int totalRow = -1);

        IList<ExamStudentMarksDto> GetExamTotalResultList(List<long> branchIdList, List<long> programIdList, long programId, long sessionId, long selectedExamType, long examId);

        IList<ExamStudentClearMarksDto> ClearExamMarksOfStudents
            (List<UserMenu> userMenu, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn);
     
        
        List<ExamsStudentMarksUploadRawData> LoadExamsSudentMarksUploadData(int draw, int start, int length, string orderBy, string orderDir, string markdUploadLogText);

        #endregion

        #region Others Function
        
        int GetExamResultListCount(List<long> programIdList, List<long> branchIdList, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocksList, string prn = "");
        int GetExamResultListCountForClearMarks(List<long> programIdList, List<long> branchIdList, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocksList, string prn = "");
        
        bool isAlreadyExistMarks(long examId, long studentProgramId, int examType);
        int CountClearMarksStudent(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, string gender, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long examId, long examType);

        int GetMobileCount(List<long> programIdList, List<long> branchIdList, long[] selectedExam,
            string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId,
            long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType,
            string selectedMeritBy, List<Batch> batchList, int[] smsReciever, List<ServiceBlock> serviceBlocks);
        
        long GetFinalMeritPosition(long programId, long sessionId, long studentProgramId, IList<long> registeredCourseIds);
        bool DeleteClearMarks(string ids);
        int CountStudentForMeritListSendSms
            (List<long> programIdList, List<long> branchIdList, long organizationId, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds,
            string[] batchDays, string[] batchTimes, long[] batchIds, long examId, int examType, int gender);

        int CountExamsSudentMarksUploadData(string markdUploadLogText);
        bool ExamHasAlreayMarks(long examId, int setId);

        #endregion

    }
    public class ExamsStudentMarksDao : BaseDao<ExamsStudentMarks, long>, IExamsStudentMarksDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        public ExamsStudentMarks LoadExamsStudentMarksByExamsDetailsStudentProgram(long examsDetailsId, long studentProgramId)
        {
            return
                Session.QueryOver<ExamsStudentMarks>()
                    .Where(x => x.ExamsDetails.Id == examsDetailsId && x.StudentProgram.Id == studentProgramId)
                    .SingleOrDefault<ExamsStudentMarks>();
        }

        public ExamsStudentMarks LoadByExamDetailId(long examDetailId)
        {
            return
                Session.QueryOver<ExamsStudentMarks>()
                    .Where(x => x.ExamsDetails.Id == examDetailId).Take(1)
                    .SingleOrDefault<ExamsStudentMarks>();
        }
        #endregion

        #region List Loading Function
        public IList<ExamsStudentMarks> LoadIndividualExamMarks(string studentProgramRoll, long? examId = null)
        {
            //ExamsStudentMarks examDetailsExamMarks = null;
            //StudentProgram sp = null;
            //var query = Session.Query()<ExamsStudentMarks>(() => examDetailsExamMarks)
            //    .JoinAlias(() => examDetailsExamMarks.StudentProgram, () => sp)
            //    .Where(() => sp.PrnNo == studentProgramRoll)
            //    .List<ExamsStudentMarks>();

            //return query;
            var query =
                Session.Query<ExamsStudentMarks>()
                    .Where(x => x.StudentProgram.PrnNo == studentProgramRoll);
            if (examId != null)
            {
                query = query.Where(x => x.ExamsDetails.Exams.Id == examId);
            }
            return query.ToList();
        }

        public IList<ExamsStudentMarks> GetExamMarksByExamId(long examId)
        {
            ExamsDetails examExamDetailsAlias = null;
            ExamsStudentMarks examDetailsExamMarks = null;
            Exams examsAlias = null;
            var query = Session.QueryOver<ExamsStudentMarks>(() => examDetailsExamMarks)
                .JoinAlias(() => examDetailsExamMarks.ExamsDetails, () => examExamDetailsAlias)
                .JoinAlias(() => examExamDetailsAlias.Exams, () => examsAlias)
                .Where(() => examsAlias.Id == examId)
                .List<ExamsStudentMarks>();

            return query;
        }

        public IList<ExamsStudentMarks> LoadExamsStudentMarksByExamStudentProgram(long examId, long studentProgramId, int examType)
        {
            var criteria = Session.CreateCriteria<ExamsStudentMarks>();
            criteria.CreateAlias("ExamsDetails", "ed");
            criteria.Add(Restrictions.Eq("StudentProgram.Id", studentProgramId));
            criteria.Add(Restrictions.Eq("ed.Exams.Id", examId));
            criteria.Add(Restrictions.Eq("ed.ExamType", examType));
            var list = criteria.List<ExamsStudentMarks>();
            return list;
        }


        public IList<ExamsStudentMarks> GetExamMarksByProgramId(long examId, long studentProgramId, int examType)
        {
            var criteria = Session.CreateCriteria<ExamsStudentMarks>();
            criteria.Add(Restrictions.Eq("Status", ExamsStudentMarks.EntityStatus.Active));
            criteria.CreateAlias("ExamsDetails", "ed").Add(Restrictions.Eq("ed.Status", ExamsDetails.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("ed.Exams.Id", examId));
            criteria.Add(Restrictions.Eq("ed.ExamType", examType));
            criteria.Add(Restrictions.Eq("StudentProgram.Id", studentProgramId));
            IList<ExamsStudentMarks> examStudentMarks = criteria.List<ExamsStudentMarks>();
            return examStudentMarks;
        }
        public IList<ExamsStudentMarks> GetExamResultList(long[] selectedExam)
        {
            var criteria = Session.CreateCriteria<ExamsStudentMarks>();
            criteria.CreateAlias("ExamsDetails", "ed").Add(Restrictions.Eq("ed.Status", ExamsDetails.EntityStatus.Active));
            if (selectedExam.Length == 1 && selectedExam[0] == 0) { }
            else
            {
                criteria.Add(Restrictions.In("ed.Exams.Id", selectedExam));
            }
            return criteria.List<ExamsStudentMarks>();
        }


        public IList<ExamsStudentMarks> GetExamMarksByProgramSession(long programId, long sessionId)
        {
            var criteria = Session.CreateCriteria<ExamsStudentMarks>();
            criteria.Add(Restrictions.Eq("Status", ExamsStudentMarks.EntityStatus.Active));
            criteria.CreateAlias("ExamsDetails", "ed");
            criteria.CreateAlias("ed.Exams", "e");
            criteria.CreateAlias("e.Program", "p");
            criteria.CreateAlias("e.Session", "s");
            criteria.Add(Restrictions.Eq("p.Id", programId));
            criteria.Add(Restrictions.Eq("s.Id", sessionId));
            return criteria.List<ExamsStudentMarks>();
            // return Session.QueryOver<ExamsStudentMarks>().Where(x => x.ExamsDetails.Exams.Program.Id == programId && x.ExamsDetails.Exams.Batch.Session.Id==sessionId).List<List<ExamsStudentMarks>>();

        }

        public IList<ExamsStudentMarks> GetExamResultList(long[] selectedExam, string studentProgramRoll)
        {
            var criteria = Session.CreateCriteria<ExamsStudentMarks>();
            criteria.CreateAlias("ExamsDetails", "ed").Add(Restrictions.Eq("ed.Status", ExamsDetails.EntityStatus.Active));
            criteria.CreateAlias("StudentProgram", "stdProgram").Add(Restrictions.Eq("stdProgram.Status", StudentProgram.EntityStatus.Active));
            if (selectedExam.Length == 1 && selectedExam[0] == 0) { }
            else
            {
                criteria.Add(Restrictions.In("ed.Exams.Id", selectedExam));
            }
            criteria.Add(Restrictions.Eq("stdProgram.PrnNo", studentProgramRoll));
            return criteria.List<ExamsStudentMarks>();
        }

        public IList<ExamsStudentMarks> ClearMarksStudentList(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, string gender, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long examId, long examType)
        {
            var criteria = GetAuthorizedStudentsExamMarksQuery(programIdList, branchIdList, programId, sessionId, branchIds, campusIds,
                batchDays, batchTimes, batchIds, courseIds, Convert.ToInt32(gender), examId, Convert.ToInt32(examType));

           // criteria.SetProjection(Projections.ProjectionList().Add(Projections.Id()));
            //criteria.SetProjection(Projections.Property("Id"));
            //criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            //criteria.List<ExamsStudentMarks>();
            criteria.SetTimeout(5000);
            IList<ExamsStudentMarks> sxm = criteria.List<ExamsStudentMarks>();
            return sxm;
        }

        public IList<IndividualMeritListNewDto> LoadIndividualMeritListReport(StudentProgram studentProgram)
        {
            #region OldQuery
//            string query = @"
//                            DECLARE @FMP INT;
//                            --DECLARE @TT FLOAT;
//                            Select distinct a.CourseId 
//                            INTO #StudentCourseDetails 
//                            From (
//	                            select sp.id, sc.SubjectId, sc.courseId
//	                            from studentProgram as sp
//	                            inner join studentCourseDetails as scd on sp.id = scd.studentProgramId
//	                            inner join courseSubject as sc on scd.courseSubjectId = sc.id
//	                            where scd.status!=-404 AND sp.PrnNo='" + studentProgram.PrnNo + @"'  
//                            ) as a
//
//                            Select a.courseId 
//                            INTO #StudentCourseList
//                            from (
//	                                Select * from #StudentCourseDetails 
//	                                union all 
//	                                Select cc.CompCourseId AS courseId from [dbo].[ComplementaryCourse] as cc		
//	                                Where cc.CourseId IN ( Select * from #StudentCourseDetails )
//		                            union all 
//	                                Select cc.CourseId AS courseId from [dbo].[ComplementaryCourse] as cc		
//	                                Where cc.CompCourseId IN ( Select * from #StudentCourseDetails )	
//                                ) as a            	
//
//                            --TEMP COMPLEMENTARY TABLE
//                                             SELECT DISTINCT * 
//				                             INTO #StudentCourseDetailsComplementary 
//					                             FROM (
//					                            --  main course and subject
//						                            select sp.id, cs.SubjectId, cs.courseId
//						                            from studentProgram as sp
//						                            inner join studentCourseDetails as scd on sp.id = scd.studentProgramId
//						                            inner join courseSubject as cs on scd.courseSubjectId = cs.id
//						                            where scd.status!=-404  
//						                            AND cs.CourseId IN (Select courseId from  #StudentCourseList) 
//					                            --	AND sp.Id IN (44926)
//                        
//					                            union all
//						                            -- main course combine subject
//						                            select DISTINCT sp.id, NULL AS SubjectId, cs.courseId
//						                            from studentProgram as sp
//						                            inner join studentCourseDetails as scd on sp.id = scd.studentProgramId
//						                            inner join courseSubject as cs on scd.courseSubjectId = cs.id
//						                            where scd.status!=-404 
//						                            AND cs.CourseId IN (Select courseId from  #StudentCourseList)  
//						                            --AND sp.Id IN (44926)
//					                            union all
//						                            -- Complementary Courses and subject
//						                            select b.id,a.SubjectId, a.CompCourseId as courseId from (
//						                            Select cc.CourseId, cc.CompCourseId,cs.SubjectId from [dbo].[ComplementaryCourse] as cc
//						                            inner join [dbo].[CourseSubject] AS cs ON cs.CourseId = cc.CompCourseId WHERE 1=1  
//						                            AND cc.CourseId IN (Select courseId from  #StudentCourseList) 
//						                            ) as a
//						                            inner join
//						                            (select distinct sp.id, cs.courseId
//						                            from studentProgram as sp
//						                            inner join studentCourseDetails as scd on sp.id = scd.studentProgramId
//						                            inner join courseSubject as cs on scd.courseSubjectId = cs.id
//						                            where scd.status!=-404  
//						                            AND cs.CourseId IN (Select courseId from  #StudentCourseList) 
//						                            --AND sp.Id IN (44926)
//						                            ) as b on a.CourseId = b.courseId 
//					                            union all 
//						                            -- Complementary Courses cobmine subject
//						                            select distinct b.id, null AS SubjectId, a.CompCourseId as courseId from (
//						                            Select cc.CourseId, cc.CompCourseId,cs.SubjectId from [dbo].[ComplementaryCourse] as cc
//						                            inner join [dbo].[CourseSubject] AS cs ON cs.CourseId = cc.CompCourseId WHERE 1=1  
//						                            AND cc.CourseId IN (Select courseId from  #StudentCourseList) 
//
//						                            ) as a
//						                            inner join
//						                            (select distinct sp.id, cs.courseId
//						                            from studentProgram as sp
//						                            inner join studentCourseDetails as scd on sp.id = scd.studentProgramId
//						                            inner join courseSubject as cs on scd.courseSubjectId = cs.id
//						                            where scd.status!=-404 
//						                            AND cs.CourseId IN (Select courseId from  #StudentCourseList) 
//						                            --AND sp.Id IN (44926)
//						                            ) as b on a.CourseId = b.courseId 
//					                            ) as a
//
//                            Select Distinct e.*
//                            INTO #PossibleExams 
//                            FROM [dbo].[ExamsDetails] AS ed 
//                            INNER JOIN [dbo].[Exams] AS e ON e.Id = ed.ExamId
//                            INNER JOIN #StudentCourseDetailsComplementary AS scd ON ((ed.SubjectId = scd.SubjectId) OR (ISNULL(ed.SubjectId, scd.SubjectId) IS NULL)) 
//	                            AND e.CourseId = scd.courseId 
//	                            AND scd.Id = " + studentProgram.Id + @"
//                            WHERE e.status = 1 AND ed.status = 1 
//                             AND e.ProgramId = "+studentProgram.Program.Id+@" AND e.SessionId = "+studentProgram.Batch.Session.Id+@"  
//                             AND e.CourseId IN (Select courseId from  #StudentCourseList)
//                             SELECT a.StudentProgramId, a.PrnNo, a.BranchId
//		                                                    , SUM(FullMarks) as FullMarks
//								                            , SUM(MCQ) AS MCQ, SUM(Written) AS Written, SUM(ObtainMark) AS Total
//								                            ,((SUM(PercentMarks)/COUNT(examDetailsId))) as PercentMarks 
//								                            ,RANK() OVER(PARTITION BY a.BranchId,a.examId ORDER BY SUM(ObtainMark) DESC) AS BMP 
//								                            ,RANK() OVER(PARTITION BY a.examId ORDER BY SUM(ObtainMark) DESC) AS CMP								
//								                            ,a.examId
//								                            , COUNT(examDetailsId) AS totalAttendExamId
//                                INTO #AllStudentMarks
//	                                                    FROM (
//                                                    SELECT esm.StudentProgramId, sp.PrnNo, b.BranchId
//			                                            , esm.ObtainMark AS MCQ, CAST(NULL AS Float) AS Written, esm.ObtainMark
//                                                        ,ed.Id as examDetailsId
//							                            ,e.Id as examId
//							                            ,(ISNULL(ed.TotalQuestion,0) * ISNULL(ed.MarksPerQuestion,0) ) as FullMarks 
//							                            ,((esm.ObtainMark/(ISNULL(ed.TotalQuestion,0) * ISNULL(ed.MarksPerQuestion,0))) * 100) as PercentMarks
//		                                            FROM [dbo].[ExamsStudentMarks] AS esm
//		                                            INNER JOIN [dbo].[ExamsDetails] AS ed ON ed.Id = esm.ExamsDetailsId AND ed.status = 1
//			                                        INNER JOIN [dbo].[Exams] AS e ON e.Id = ed.ExamId AND e.status = 1
//			                                        INNER JOIN [dbo].[StudentProgram] AS sp ON esm.StudentProgramId = sp.Id AND sp.status = 1
//                                                --    INNER JOIN [dbo].[Student] AS s ON s.Id = sp.StudentId AND s.Status = 1
//			                                        INNER JOIN [dbo].[Batch] AS b ON b.Id = sp.BatchId AND b.status = 1
//                                                    INNER JOIN #StudentCourseDetailsComplementary AS scd ON scd.Id = sp.Id
//			                                        WHERE esm.status = 1
//				                                        AND ed.ExamType = 1 AND ((ed.SubjectId = scd.SubjectId) OR (ISNULL(ed.SubjectId, scd.SubjectId) IS NULL)) AND e.CourseId = scd.courseId
//			                                             AND e.ProgramId = "+studentProgram.Program.Id+@" AND e.SessionId = "+studentProgram.Batch.Session.Id+@"  AND e.CourseId IN (Select courseId from  #StudentCourseList) 
//							                            -- AND sp.Id = 44926
//							                            -- AND e.Id IN (1309,1310)
//                                                    UNION ALL 
//		                                            SELECT esm.StudentProgramId, sp.PrnNo, b.BranchId
//			                                            , CAST(NULL AS Float) AS MCQ, esm.ObtainMark AS Written, esm.ObtainMark
//                                                        ,ed.Id as examDetailsId
//							                            ,e.Id as examId
//							                            ,ISNULL(ed.WrittenFullMark,0) as FullMarks
//							                            ,((esm.ObtainMark/ISNULL(ed.WrittenFullMark,0))*100) as PercentMarks
//		                                            FROM [dbo].[ExamsStudentMarks] AS esm
//		                                            INNER JOIN [dbo].[ExamsDetails] AS ed ON ed.Id = esm.ExamsDetailsId AND ed.status = 1
//			                                        INNER JOIN [dbo].[Exams] AS e ON e.Id = ed.ExamId AND e.status = 1
//			                                        INNER JOIN [dbo].[StudentProgram] AS sp ON esm.StudentProgramId = sp.Id AND sp.status = 1
//                                                  --  INNER JOIN [dbo].[Student] AS s ON s.Id = sp.StudentId AND s.Status = 1
//			                                        INNER JOIN [dbo].[Batch] AS b ON b.Id = sp.BatchId AND b.status = 1
//                                                    INNER JOIN #StudentCourseDetailsComplementary AS scd ON scd.Id = sp.Id
//			                                        WHERE esm.status = 1
//				                                        AND ed.ExamType = 2 AND ((ed.SubjectId = scd.SubjectId) OR (ISNULL(ed.SubjectId, scd.SubjectId) IS NULL)) AND e.CourseId = scd.courseId
//			                                             AND e.ProgramId = "+studentProgram.Program.Id+@" AND e.SessionId = "+studentProgram.Batch.Session.Id+@"  AND e.CourseId IN (Select courseId from  #StudentCourseList)  
//							                             -- AND sp.Id = 44926
//							                            -- AND e.Id IN (1309,1310)
//	                                                    ) AS a
//	                                                    GROUP BY a.StudentProgramId, a.examId, a.BranchId, a.PrnNo
//
//                            --Where A.PrnNo = '60153000001' 
//
//                            --Select A.* from #AllStudentMarks AS A
//                            Select @FMP = FMP
//                            --,@TT = TT 
//                            from (
//	                            Select StudentProgramId,FMP,TT from (
//		                            Select StudentProgramId, RANK() OVER(PARTITION BY 1 ORDER BY SUM(Total) DESC) AS FMP,SUM(Total) as TT from #AllStudentMarks Group By StudentProgramId
//	                            ) AS A Where A.StudentProgramId = "+studentProgram.Id+ @"
//                            ) AS A
//                            Select A.Id,A.Name, B.HighestMarks, (A.McqFullMarks+A.WrittenFullMarks) AS FullMarks,A.CourseId as CourseId,A.McqFullMarks,A.WrittenFullMarks
//                            , CASE WHEN A.IsMcq = 0 THEN 'N/A' ELSE ISNULL(CONVERT(varchar,round(C.MCQ,2,1)),'A') END AS [MCQMarks]
//                            , CASE WHEN A.IsWritten = 0 THEN 'N/A' ELSE ISNULL(CONVERT(varchar,round(C.Written,2,1)),'A') END AS [WrittenMarks] 
//                            ,C.Total AS Total
//                            ,C.BMP
//                            ,C.CMP
//                            , @FMP AS FMP
//                             --,@TT AS TT
//                            --,D.FMP
//                            from #PossibleExams AS A
//                            Left JOIN(SELECT examId, MAX(Total) AS HighestMarks From #AllStudentMarks Group By examId) AS B ON A.Id = B.examId
//                            Left Join (Select * from #AllStudentMarks Where StudentProgramId = " + studentProgram.Id+@") AS C ON C.examId = A.Id
//                            Where B.HighestMarks IS NOT NULL
//                            --Left JOIN (
//                            --	Select StudentProgramId,FMP from (
//                            --		Select StudentProgramId,RANK() OVER(PARTITION BY 1 ORDER BY SUM(Total) DESC) AS FMP	from #AllStudentMarks 
//                            --	) AS A Where A.StudentProgramId = "+studentProgram.Id+@"
//                            --) AS D ON C.StudentProgramId = D.StudentProgramId
//
//                            IF EXISTS(SELECT * FROM #PossibleExams) DROP TABLE #PossibleExams;
//                            IF EXISTS(SELECT * FROM #AllStudentMarks) DROP TABLE #AllStudentMarks;
//                            IF EXISTS(SELECT * FROM #StudentCourseDetailsComplementary) DROP TABLE #StudentCourseDetailsComplementary;
//                            IF EXISTS(SELECT * FROM #StudentCourseDetails) DROP TABLE #StudentCourseDetails;
//                            IF EXISTS(SELECT * FROM #StudentCourseList) DROP TABLE #StudentCourseList;
//            
            //                ";
            #endregion

            string query = @"
                            DECLARE @programId BIGINT;
                            DECLARE @sessionId BIGINT;
                            DECLARE @studentProgramId BIGINT;
                            DECLARE @fmp BIGINT;

                            SET @fmp = 0;

                            SELECT @programId = sp.ProgramId, @sessionId = b.SessionId, @studentProgramId = sp.Id FROM StudentProgram sp 
                            INNER JOIN Batch b ON b.Id = sp.BatchId
                            WHERE sp.PrnNo='" + studentProgram.PrnNo + @"';

                            SELECT @fmp = FMP
                            FROM (	
	                            SELECT esm.StudentProgramId	
		                            , RANK() OVER(PARTITION BY 1 ORDER BY SUM(esm.ObtainMark) DESC) FMP
	                            FROM ExamsStudentMarks esm
	                            LEFT JOIN ExamsDetails ed on ed.Id = esm.ExamsDetailsId
	                            LEFT JOIN Exams e On ed.ExamId = e.Id
	                            LEFT JOIN StudentProgram sp ON sp.Id = esm.StudentProgramId
	                            WHERE e.ProgramId = @programId AND e.SessionId = @sessionId AND esm.IsNonPayable = 0 
		                            AND e.Status = 1 AND ed.Status = 1 AND esm.Status = 1 AND sp.Status = 1
		                            AND e.CourseId In (
			                            SELECT DISTINCT * FROM (
				                            SELECT scd.CourseId FROM CourseSubject cs
				                            INNER JOIN StudentCourseDetails scd ON cs.Id = scd.CourseSubjectId
				                            WHERE scd.StudentProgramId = @studentProgramId
				                            UNION ALL
				                            SELECT CompCourseId FROM ComplementaryCourse cc 
				                            WHERE cc.CourseId IN (
					                            SELECT scd.CourseId FROM CourseSubject cs
					                            INNER JOIN StudentCourseDetails scd ON cs.Id = scd.CourseSubjectId
					                            where scd.StudentProgramId = @studentProgramId
				                            )
			                            ) as a
		                            )
	                            GROUP BY esm.StudentProgramId	
                            ) AS a 
                            WHERE a.StudentProgramId = @studentProgramId

                            SELECT DISTINCT	e.Id, e.CourseId, ltrim(rtrim(e.Name)) Name --, e.ShortName, e.Code
	                            , e.McqFullMarks+e.WrittenFullMarks FullMarks
	                            , HM.HighestMarks
	                            , e.McqFullMarks
	                            , e.WrittenFullMarks
	                            , CASE 
		                            WHEN e.McqFullMarks = 0 THEN 'N/A' 
		                            WHEN sm.MCQ is null THEN 'A' 
		                            ELSE CONVERT(VARCHAR,ROUND(sm.MCQ,2,1))
	                            END MCQMarks
	                            , CASE 
		                            WHEN e.WrittenFullMarks = 0 THEN 'N/A' 
		                            WHEN sm.Written is null THEN 'A' 
		                            ELSE CONVERT(VARCHAR,ROUND(sm.Written,2,1))
	                            END WrittenMarks
	                            , sm.Total Total
	                            , sm.BMP BMP
	                            , sm.CMP CMP
	                            , @fmp FMP
                            FROM Exams as e 
                            INNER JOIN ExamsDetails ed ON e.Id = ed.ExamId
                            LEFT JOIN (
	                            SELECT DISTINCT a.ExamId, a.Total HighestMarks FROM (
		                            SELECT esm.StudentProgramId, ed.ExamId
			                            , sum(esm.ObtainMark) Total
			                            , RANK() OVER(PARTITION BY ed.ExamId ORDER BY SUM(esm.ObtainMark) DESC) CMP
		                            FROM ExamsStudentMarks esm
		                            LEFT JOIN ExamsDetails ed on ed.Id = esm.ExamsDetailsId
			                        LEFT JOIN Exams e On ed.ExamId = e.Id
		                            WHERE e.ProgramId = @programId AND e.SessionId = @sessionId AND e.Status = 1 
                                        AND esm.Status!=-404 AND ed.Status!=-404 AND esm.IsNonPayable = 0
		                            GROUP BY esm.StudentProgramId, ed.ExamId
	                            ) AS a
	                            WHERE a.CMP = 1
                            ) AS HM ON hm.ExamId = e.Id
                            LEFT JOIN (
	                            SELECT * 
	                            FROM (
		                            SELECT a.StudentProgramId, MAX(b.BranchId) BranchId, a.ExamId
			                            , SUM(MCQ) MCQ, SUM(Written) Written, SUM(Total) Total
			                            , RANK() OVER(PARTITION BY ExamId ORDER BY SUM(Total) DESC) CMP
			                            , RANK() OVER(PARTITION BY ExamId, MAX(b.BranchId) ORDER BY SUM(Total) DESC) BMP
		                            FROM (
			                            SELECT esm.StudentProgramId, esm.ExamsDetailsId, ed.ExamType, ed.ExamId
				                            , sum(case when ed.ExamType = 1 then esm.ObtainMark else null end) MCQ
				                            , sum(case when ed.ExamType = 2 then esm.ObtainMark else null end) Written 
				                            , sum(esm.ObtainMark) Total	
			                            FROM ExamsStudentMarks esm
			                            LEFT JOIN ExamsDetails ed on ed.Id = esm.ExamsDetailsId
			                            LEFT JOIN Exams e On ed.ExamId = e.Id
			                            LEFT JOIN StudentProgram sp ON sp.Id = esm.StudentProgramId
			                            WHERE e.ProgramId = @programId AND e.SessionId = @sessionId AND esm.IsNonPayable = 0 
				                            AND e.Status = 1 AND ed.Status = 1 AND esm.Status = 1 AND sp.Status = 1 
			                            GROUP BY esm.StudentProgramId, esm.ExamsDetailsId, ed.ExamType, ed.ExamId
		                            ) As a
		                            LEFT JOIN StudentProgram sp ON sp.Id = a.StudentProgramId
		                            LEFT JOIN Batch b On b.Id = sp.BatchId
		                            GROUP BY a.StudentProgramId, a.ExamId
	                            ) AS a 
	                            WHERE a.StudentProgramId = @studentProgramId
                            ) AS SM ON sm.ExamId = e.Id
                            WHERE e.Status != -404
	                            AND ed.Status != -404
	                            AND HM.HighestMarks>0
                                --Check to remove Extra religion and non taken subject
	                            AND (
		                            e.CourseId in (
			                            SELECT scd.CourseId FROM CourseSubject cs
			                            INNER JOIN StudentCourseDetails scd ON cs.Id = scd.CourseSubjectId
			                            where scd.StudentProgramId = @studentProgramId
		                            ) 
		                            AND (ed.SubjectId is null 
			                            OR ed.SubjectId in (
				                            SELECT SubjectId FROM StudentCourseDetails scd
				                            INNER JOIN CourseSubject cs ON cs.Id = scd.CourseSubjectId
				                            WHERE scd.Status=1 AND scd.StudentProgramId = @studentProgramId
			                            )
		                            )
		                            OR
		                            e.CourseId in (
			                            SELECT CompCourseId FROM ComplementaryCourse cc 
			                            WHERE cc.CourseId IN (
				                            SELECT scd.CourseId FROM CourseSubject cs
				                            INNER JOIN StudentCourseDetails scd ON cs.Id = scd.CourseSubjectId
				                            where scd.StudentProgramId = @studentProgramId
			                            )
		                            )
	                            )
                            ORDER BY CourseId, Name


            ";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<IndividualMeritListNewDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<IndividualMeritListNewDto>().ToList();
        }

        public IList<ExamStudentMarksDto> GetExamTotalResultList(List<long> branchIdList, List<long> programIdList, long programId, long sessionId,
            long selectedExamType, long examId)
        {
            ICriteria criteria = Session.CreateCriteria<ExamsStudentMarks>().Add(Restrictions.Not(Restrictions.Eq("Status", ExamsStudentMarks.EntityStatus.Delete)));
            criteria.CreateAlias("ExamsDetails", "ed").Add(Restrictions.Eq("ed.Status", ExamsDetails.EntityStatus.Active));
            criteria.CreateAlias("StudentProgram", "sp").Add(Restrictions.Eq("sp.Status", StudentProgram.EntityStatus.Active));
            criteria.CreateAlias("sp.Batch", "b").Add(Restrictions.Eq("b.Status", Batch.EntityStatus.Active));
            criteria.CreateAlias("b.Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active)).Add(Restrictions.Eq("p.Id", programId));
            criteria.CreateAlias("b.Session", "s").Add(Restrictions.Eq("s.Status", Program.EntityStatus.Active)).Add(Restrictions.Eq("s.Id", sessionId));
            criteria.Add(Restrictions.Eq("ed.Exams.Id", examId));
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("b.Program.Id", programIdList));
            }
            if (selectedExamType != -1)
            {
                criteria.Add(Restrictions.Eq("ed.ExamType", Convert.ToInt32(selectedExamType)));
            }
            criteria.SetProjection(Projections.ProjectionList()
                .Add(Projections.Property("Id"), "Id")
                .Add(Projections.Property("StudentProgram"), "StudentProgram")
                .Add(Projections.Property("ExamsDetails"), "ExamsDetails")
                .Add(Projections.Property("ObtainMark"), "ObtainMark")
                );
            criteria.SetResultTransformer(Transformers.AliasToBean(typeof(ExamStudentMarksDto)));
            return criteria.List<ExamStudentMarksDto>().ToList();
            //return criteria.List<ExamsStudentMarks>().ToList();
        }

        public IList<ExamStudentClearMarksDto> ClearExamMarksOfStudents(List<UserMenu> userMenu, long[] selectedExam, string selectedGenerateBy, string selectedGender,
            string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds,
            string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocksList,
            string prn)
        {
            #region Filter
            string filterGender = (selectedGender == "1" || selectedGender == "2") ? " AND s.Gender = " + selectedGender + " " : " ";
            string filterProgramSession = " AND sp.ProgramId = " + programId.ToString() + " AND b.SessionId = " + sessionId.ToString() + " ";
            //string filterProgramSessionExam = " AND e.ProgramId = " + programId.ToString() + " AND e.SessionId = " + sessionId.ToString() + " ";
            string filterExamtype = "";
            
            #region selectedExamType

            if (selectedExamType == "1")
            {
                filterExamtype = " AND ed.ExamType = 1 ";
            }
            else if (selectedExamType == "2")
            {
                filterExamtype = " AND ed.ExamType = 2 ";
            }
            #endregion

            #region selectedExamQuery
            string selectedExamQuery = "";
            if (selectedExam[0] != 0)
            {
                foreach (var e in selectedExam)
                {
                    if (selectedExamQuery == "")
                        selectedExamQuery += e.ToString();
                    else
                        selectedExamQuery += ", " + e.ToString();
                }
                if (selectedExamQuery != "")
                {
                    selectedExamQuery = " e.Id IN (" + selectedExamQuery + ") ";
                }
            }
            
            #endregion
            
            #region filterBranch
            string filterBranch = "";
            if (branchIds[0] != 0)
            {
                foreach (var e in branchIds)
                {
                    if (filterBranch == "")
                    {
                        filterBranch += e.ToString();
                    }
                    else
                    {
                        filterBranch += ", " + e.ToString();
                    }
                }
                if (filterBranch != "")
                {
                    filterBranch = " AND b.BranchId IN (" + filterBranch + ") ";
                }
            }
            #endregion

            #region filterCampus
            string filterCampus = "";
            if (campusIds[0] != 0)
            {
                foreach (var e in campusIds)
                {
                    if (filterCampus == "")
                    {
                        filterCampus += e.ToString();
                    }
                    else
                    {
                        filterCampus += ", " + e.ToString();
                    }
                }
                if (filterCampus != "")
                {
                    filterCampus = " AND b.CampusId IN (" + filterCampus + ") ";
                }
            }
            #endregion

            #region filterBatch
            string filterBatch = "";
            foreach (var b in batchList)
            {
                if (filterBatch == "")
                {
                    filterBatch += b.Id.ToString();
                }
                else
                {
                    filterBatch += ", " + b.Id.ToString();
                }
            }
            if (filterBatch != "") { filterBatch = " AND b.Id IN (" + filterBatch + ") "; }
            else { filterBatch = " AND b.Id IN (0) "; }
            #endregion
            
            #endregion

            #region query
            string query = @" Select a.Id as Id from (
                              SELECT esm.StudentProgramId as studentProgramId,esm.Id as Id
                                    FROM [dbo].[ExamsStudentMarks] AS esm
                                    INNER JOIN [dbo].[ExamsDetails] AS ed ON ed.Id = esm.ExamsDetailsId AND ed.status = 1
                                    INNER JOIN [dbo].[Exams] AS e ON e.Id = ed.ExamId AND e.status = 1                            
                                WHERE  " + selectedExamQuery + filterExamtype + @" 
                            ) as a
                            inner Join (
                             SELECT sp.Id as studentProgramId
                                    FROM [dbo].[StudentProgram] AS sp 
                                    INNER JOIN [dbo].[Student] AS s ON s.Id = sp.StudentId
                                    INNER JOIN [dbo].[Batch] AS b ON b.Id = sp.BatchId AND b.Status = 1
                                    INNER JOIN [dbo].[Branch] as br ON br.Id = b.BranchId AND br.Status = 1
		                            INNER JOIN [dbo].[Campus] AS c ON c.Id = b.CampusId AND c.Status = 1                         
                                WHERE 1=1 " + filterProgramSession + filterBranch + filterBatch + filterCampus + filterGender + @"
                            ) as b ON a.studentProgramId = b.studentProgramId WHERE b.studentProgramId>0";

//            string query = @"
//                            SELECT esm.Id as Id
//                                FROM [dbo].[ExamsStudentMarks] AS esm
//                                INNER JOIN [dbo].[StudentProgram] AS sp ON esm.StudentProgramId = sp.Id
//                                INNER JOIN [dbo].[Student] AS s ON s.Id = sp.StudentId
//                                INNER JOIN [dbo].[Batch] AS b ON b.Id = sp.BatchId AND b.Status = 1
//                                INNER JOIN [dbo].[Branch] as br ON br.Id = b.BranchId AND br.Status = 1
//								INNER JOIN [dbo].[Campus] AS c ON c.Id = b.CampusId AND c.Status = 1 
//                                INNER JOIN [dbo].[ExamsDetails] AS ed ON ed.Id = esm.ExamsDetailsId AND ed.status = 1
//                                INNER JOIN [dbo].[Exams] AS e ON e.Id = ed.ExamId AND e.status = 1                            
//                            WHERE " + selectedExamQuery + filterExamtype + filterProgramSession + filterBranch +filterBatch+filterCampus+ filterGender+"";

            
            #endregion

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(5000);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ExamStudentClearMarksDto>());
            var result = iQuery.List<ExamStudentClearMarksDto>().ToList();
            return result;
        }

        public List<ExamsStudentMarksUploadRawData> LoadExamsSudentMarksUploadData(int draw, int start, int length, string orderBy, string orderDir,
            string markdUploadLogText)
        {
            ICriteria criteria = Session.CreateCriteria<ExamsStudentMarksUploadRawData>();
            if (!string.IsNullOrEmpty(markdUploadLogText))
            {
                var or = Restrictions.Disjunction();
                or.Add(Restrictions.Like("Roll", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("RegNo", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Board", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("BarCode", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("QrCode", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Gender", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Version", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Image", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Answer", markdUploadLogText, MatchMode.Anywhere))
                    //.Add(Restrictions.Like("ObtainMark", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Message", markdUploadLogText, MatchMode.Anywhere));
                criteria.Add(or);
                
            }
            if (!string.IsNullOrEmpty(orderBy))
            {
                if (orderDir == "ASC")
                {
                    criteria.AddOrder(Order.Asc(orderBy));
                }
                else
                {
                    criteria.AddOrder(Order.Desc(orderBy));
                }
            }
            
            return criteria.SetFirstResult(start).SetMaxResults(length).List<ExamsStudentMarksUploadRawData>().ToList();
        }

        #endregion

        #region Others Function
        
        public int GetExamResultListCount(List<long> programIdList, List<long> branchIdList, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocksList,string prn="")
        {
            /*
             * ServiceBlockes
             * *********COMMON*********
             * Program*
             * Session*
             * CourseIds
             * *********RESULT*********
             * ExamIds
             * ExamTypeId*
             * *********STUDENT*********
             * BranchIds
             * CampusIds
             * BatchIds
             * GenderId*
             * Prn*
             * 
             */
            #region Block Student Service Filter

            string blockServiceFilter = "";
            if (serviceBlocksList.Any())
            {
                foreach (var sb in serviceBlocksList)
                {
                    if (sb.ConditionType == (int)ConditionType.PaymentDue)
                    {
                        blockServiceFilter += " AND DueAmount = 0 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.ImageDue)
                    {
                        blockServiceFilter += " AND IsImage = 1 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.PoliticalReference)
                    {
                        blockServiceFilter += " AND IsPolitical = 0 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.Jsc)
                    {
                        blockServiceFilter += " AND IsJsc = 1 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.Ssc)
                    {
                        blockServiceFilter += " AND IsSsc = 1 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.Hsc)
                    {
                        blockServiceFilter += " AND IsHsc = 1 ";
                        break;
                    }
                }
            }
            #endregion

            #region filterCourseIds
            string filterCourseIds = "";
            if (courseIds[0] != 0)
            {
                foreach (var e in courseIds)
                {
                    if (filterCourseIds == "")
                    {
                        filterCourseIds += e.ToString();
                    }
                    else
                    {
                        filterCourseIds += ", " + e.ToString();
                    }
                }
                filterCourseIds = " AND CourseId IN (" + filterCourseIds + ") ";
            }
            #endregion
            #region filterExamIds
            string filterExamIds = "";
            string examNames = " 'All' ";
            if (selectedExam[0] != 0)
            {
                foreach (var e in selectedExam)
                {
                    if (filterExamIds == "")
                        filterExamIds += e.ToString();
                    else
                        filterExamIds += ", " + e.ToString();
                }
                if (filterExamIds != "")
                {
                    filterExamIds = " AND Id IN (" + filterExamIds + ") ";
                    examNames = " STUFF((SELECT ', ' + CAST(ShortName AS VARCHAR(255)) [text()] FROM [dbo].[Exams] AS e WHERE 1=1 " + filterExamIds + @" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') ";
                }
            }
            #endregion
            string filterExamType = selectedExamType == "-1" ? " " : " AND ed.ExamType = " + selectedExamType;
            #region filterBranchIds
            string filterBranchIds = "";
            if (branchIds[0] != 0)
            {
                foreach (var e in branchIds)
                {
                    if (filterBranchIds == "")
                    {
                        filterBranchIds += e.ToString();
                    }
                    else
                    {
                        filterBranchIds += ", " + e.ToString();
                    }
                }
                if (filterBranchIds != "")
                {
                    filterBranchIds = " AND BranchId IN (" + filterBranchIds + ") ";
                }
            }
            #endregion
            #region filterCampusIds
            string filterCampusIds = "";
            if (campusIds[0] != 0)
            {
                foreach (var e in campusIds)
                {
                    if (filterCampusIds == "")
                    {
                        filterCampusIds += e.ToString();
                    }
                    else
                    {
                        filterCampusIds += ", " + e.ToString();
                    }
                }
                if (filterCampusIds != "")
                {
                    filterCampusIds = " AND CampusId IN (" + filterCampusIds + ") ";
                }
            }
            #endregion
            #region filterBatchIds
            string filterBatchIds = "";
            if (batchList.Count > 0)
            {
                foreach (var b in batchList)
                {
                    if (filterBatchIds == "")
                    {
                        filterBatchIds += b.Id.ToString();
                    }
                    else
                    {
                        filterBatchIds += ", " + b.Id.ToString();
                    }
                }
                if (filterBatchIds != "")
                {
                    filterBatchIds = " AND BatchId IN (" + filterBatchIds + ") ";
                }
            }
            #endregion
            string filterGender = (selectedGender == "1" || selectedGender == "2") ? " AND Gender = " + selectedGender + " " : " ";
            #region filterProgramRoll
            string filterPrn = "";
            if (!String.IsNullOrEmpty(prn))
            {
                filterPrn = " AND PrnNo = '" + prn + "' ";
            }
            #endregion            
            #region filterAttendanceType
            string filterAttendanceType = selectedAttendanceType == "2" ? " LEFT " : " INNER ";
            #endregion


            #region MainQuery
            string query = @"
                DECLARE @ProgramId BIGINT;
                DECLARE @SessionId BIGINT;
                DECLARE @DeletedStatus INT;
                DECLARE @ActiveStatus INT;

                DECLARE @TotalParticipant INT;
                DECLARE @FullMarks FLOAT;
                DECLARE @AverageMarks FLOAT;
                DECLARE @MCQFullMarks FLOAT;
                DECLARE @WrittenFullMarks FLOAT;
                DECLARE @HighestMarks FLOAT;

                SET @ProgramId = " + programId.ToString() + @";
                SET @SessionId = " + sessionId.ToString() + @";
                SET @DeletedStatus = -404;
                SET @ActiveStatus = 1;

                SELECT  
	                StudentProgramId 
	                , " + examNames + @" ExamName
	                , MAX(MCQMarks) MCQMarks
	                , MAX(WrittenMarks) WrittenMarks
	                , MAX(TotalMarks) TotalMarks
	                , MAX(TotalMarks) HighestMarks
	                , MAX(MCQFullMarks) MCQFullMarks
	                , MAX(WrittenFullMarks) WrittenFullMarks
	                , MAX(FullMarks) FullMarks
	                , MAX(PercentMarks) PercentMarks
	                , MAX(BMP) BMP
	                , MAX(CMP) CMP
	                , MAX(BMP_P) BMP_P
	                , MAX(CMP_P) CMP_P
	                , MAX(TotalParticipant) TotalParticipant
                INTO #TempResultSummery
                FROM (
	                SELECT a.StudentProgramId 
		                , ROUND(SUM(MCQ),2,1) MCQMarks
		                , ROUND(SUM(Written),2,1) WrittenMarks
		                --, ROUND(( SUM(Total) / ( SUM(MCQFull) + SUM(WrittenFull) ) ) * 100,2,1) PercentMarks
		                , ROUND(( SUM(PercentMark) / Count(*) ) ,2,1) PercentMarks
		                , ROUND(SUM(Total),2,1) TotalMarks
		                , ROUND(SUM(MCQFull),2,1) MCQFullMarks
		                , ROUND(SUM(WrittenFull),2,1) WrittenFullMarks
		                , ROUND(( SUM(MCQFull) + SUM(WrittenFull) ),2,1) FullMarks
		                , RANK() OVER(PARTITION BY MAX(b.BranchId) ORDER BY SUM(Total) DESC) BMP
		                , RANK() OVER(PARTITION BY 1 ORDER BY SUM(Total) DESC) CMP
		                , RANK() OVER(PARTITION BY MAX(b.BranchId) ORDER BY ( SUM(PercentMark) / Count(*) ) DESC) BMP_P
		                , RANK() OVER(PARTITION BY 1 ORDER BY ( SUM(PercentMark) / Count(*) ) DESC) CMP_P	
		                , Count(*) OVER() TotalParticipant
	                FROM (
		                SELECT esm.StudentProgramId, ed.ExamId
			                , MAX(e.ShortName) ExamName
			                , SUM(CASE WHEN ed.ExamType = 1 THEN esm.ObtainMark ELSE null END) MCQ
			                , SUM(CASE WHEN ed.ExamType = 2 THEN esm.ObtainMark ELSE null END) Written 
			                , SUM(CASE WHEN ed.ExamType = 1 THEN ISNULL(ed.TotalQuestion,0) * ISNULL(ed.MarksPerQuestion,0) ELSE 0 END) MCQFull
			                , SUM(CASE WHEN ed.ExamType = 2 THEN ISNULL(ed.WrittenFullMark,0) ELSE 0 END) WrittenFull 
			                , SUM(CASE WHEN ed.ExamType = 1 
								THEN (esm.ObtainMark / (ISNULL(ed.TotalQuestion,0) * ISNULL(ed.MarksPerQuestion,0)) ) * 100 
								ELSE (esm.ObtainMark / (ISNULL(ed.WrittenFullMark,0)) ) * 100  
							END) AS PercentMark
			                , SUM(esm.ObtainMark) Total	
		                FROM (
			                SELECT * FROM Exams WHERE Status = @ActiveStatus AND ProgramId = @ProgramId AND SessionId = @SessionId 
				                " + filterCourseIds + filterExamIds + @" 
		                ) AS e
		                INNER JOIN ExamsDetails AS ed ON ed.ExamId = e.Id
		                INNER JOIN ExamsStudentMarks AS esm ON esm.ExamsDetailsId = ed.Id
		                WHERE ed.Status = @ActiveStatus AND esm.Status = @ActiveStatus AND esm.IsNONPayable = 0 
			                " + filterExamType + @" 
		                GROUP BY esm.StudentProgramId, esm.ExamsDetailsId, ed.ExamType, ed.ExamId		
	                ) As a
	                INNER JOIN StudentProgram AS sp ON sp.Id = a.StudentProgramId AND sp.Status = @ActiveStatus
	                INNER JOIN Batch AS b ON b.Id = sp.BatchId AND b.Status = @ActiveStatus
	                GROUP BY a.StudentProgramId
                ) AS a
                GROUP BY a.StudentProgramId;

                SELECT @TotalParticipant = MAX(TotalParticipant)
	                , @FullMarks = MAX(FullMarks)
	                , @AverageMarks = ROUND(SUM(TotalMarks)/MAX(TotalParticipant),2,1)
	                , @MCQFullMarks = MAX(MCQFullMarks)
	                , @WrittenFullMarks = MAX(WrittenFullMarks)
	                , @HighestMarks = MAX(TotalMarks)
                FROM #TempResultSummery

                SELECT  
	                a.PrnNo, a.RegistrationNo, a.NickName, a.FullName, i.Name Institute, a.FatherName, a.Mobile, a.GuardiansMobile1, a.GuardiansMobile2
	                , a.Gender, a.IsJsc, a.IsSsc, a.IsHsc, a.DueAmount, a.IsImage, a.IsPolitical
	                , p.ShortName ProgramShortName, sess.Name SessionName, br.Name BranchName, c.Name CampusName, a.BatchName, a.BatchId
		            , a.SessionId, a.BranchId, a.CampusId
	                , a.TotalStudent
	                , result.*
                INTO #TempFullMeritList
                FROM (	
	                SELECT *, COUNT(*) OVER() TotalStudent
	                FROM (
	                    SELECT DISTINCT sp.Id StudentProgramId, sp.PrnNo, sp.ProgramId, sp.InstituteId, sp.DueAmount, sp.IsImage, sp.IsPolitical
		                    , s.RegistrationNo, s.NickName, s.FullName, s.FatherName, s.Mobile, s.GuardiansMobile1, s.GuardiansMobile2, s.Gender, s.IsJsc, s.IsSsc, s.IsHsc
		                    , b.SessionId, b.BranchId, b.CampusId, b.Name BatchName, b.Id BatchId
		
	                    FROM (
		                    SELECT Id, PrnNo, DueAmount, StudentId, BatchId, ProgramId, InstituteId, IsImage, IsPolitical FROM StudentProgram
		                    WHERE Status = @ActiveStatus AND ProgramId = @ProgramId /*AND DueAmount = 1 AND IsImage = 1 AND IsPolitical = 1 */
	                    ) AS sp
	                    INNER JOIN (
		                    SELECT Id, RegistrationNo, NickName, FullName, FatherName, Mobile, GuardiansMobile1, GuardiansMobile2, Gender, IsJsc, IsSsc, IsHsc
		                    FROM Student 
		                    WHERE Status = @ActiveStatus /*AND Gender = 1 AND IsJsc = 1 AND IsSsc = 1 AND IsHsc = 1*/
	                    ) AS s ON s.Id = sp.StudentId
	                    INNER JOIN (
		                    SELECT Id, SessionId, BranchId, CampusId, Name  FROM Batch WHERE Status = @ActiveStatus AND ProgramId = @ProgramId AND SessionId = @SessionId 
	                    ) AS b ON b.Id = sp.BatchId
	                    INNER JOIN (
		                    SELECT distinct cs.CourseId, scd.StudentProgramId
		                    FROM StudentCourseDetails AS scd
		                    INNER JOIN CourseSubject AS cs ON cs.Id = scd.CourseSubjectId
		                    WHERE scd.Status = @ActiveStatus AND cs.Status = @ActiveStatus " + filterCourseIds + @"  
	                    ) AS scd ON scd.StudentProgramId = sp.Id
                    ) AS a
                ) AS a
                LEFT JOIN Institute AS I ON i.Id = a.InstituteId
                INNER JOIN Program AS p ON p.Id = a.ProgramId
                INNER JOIN [Session] AS sess ON sess.Id = a.SessionId
                INNER JOIN Branch AS br ON br.Id = a.BranchId
                INNER JOIN Campus AS c ON c.Id = a.CampusId
                " + filterAttendanceType + @" JOIN #TempResultSummery AS result ON result.StudentProgramId = a.StudentProgramId;

                SELECT COUNT(*) AS C
                FROM (
                    SELECT * 
                    FROM #TempFullMeritList
                    WHERE 1=1 
                        " + blockServiceFilter + @" /* AND DueAmount = 0 AND IsImage = 1 AND IsPolitical = 0 AND IsJsc = 1 AND IsSsc = 1 AND IsHsc = 1 */ 
                        " + filterGender + @" /* AND Gender = 1 */ 
                        " + filterBranchIds + @" /* AND BranchId IN () */ 
                        " + filterCampusIds + @" /* AND CampusId IN ()*/
	                    " + filterBatchIds + @" /* AND BatchId IN () */ 
	                    " + filterPrn + @" /* Filter By PrnNumber*/
                    ) AS a 

                IF EXISTS(SELECT 1 FROM #TempFullMeritList) DROP TABLE #TempFullMeritList;
                IF EXISTS(SELECT 1 FROM #TempResultSummery) DROP TABLE #TempResultSummery;
            ";
            #endregion

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(5000);
            return iQuery.UniqueResult<int>();
        }

        public int GetExamResultListCountForClearMarks(List<long> programIdList, List<long> branchIdList, long[] selectedExam,
            string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId,
            long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType,
            string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocksList, string prn = "")
        {
            #region Filter
            string filterGender = (selectedGender == "1" || selectedGender == "2") ? " AND s.Gender = " + selectedGender + " " : " ";
            string filterProgramSession = " AND sp.ProgramId = " + programId.ToString() + " AND b.SessionId = " + sessionId.ToString() + " ";
            //string filterProgramSessionExam = " AND e.ProgramId = " + programId.ToString() + " AND e.SessionId = " + sessionId.ToString() + " ";
            string filterExamtype = "";

            #region selectedExamType

            if (selectedExamType == "1")
            {
                filterExamtype = " AND ed.ExamType = 1 ";
            }
            else if (selectedExamType == "2")
            {
                filterExamtype = " AND ed.ExamType = 2 ";
            }
            #endregion

            #region selectedExamQuery
            string selectedExamQuery = "";
            if (selectedExam[0] != 0)
            {
                foreach (var e in selectedExam)
                {
                    if (selectedExamQuery == "")
                        selectedExamQuery += e.ToString();
                    else
                        selectedExamQuery += ", " + e.ToString();
                }
                if (selectedExamQuery != "")
                {
                    selectedExamQuery = " e.Id IN (" + selectedExamQuery + ") ";
                }
            }

            #endregion

            #region filterBranch
            string filterBranch = "";
            if (branchIds[0] != 0)
            {
                foreach (var e in branchIds)
                {
                    if (filterBranch == "")
                    {
                        filterBranch += e.ToString();
                    }
                    else
                    {
                        filterBranch += ", " + e.ToString();
                    }
                }
                if (filterBranch != "")
                {
                    filterBranch = " AND b.BranchId IN (" + filterBranch + ") ";
                }
            }
            #endregion

            #region filterCampus
            string filterCampus = "";
            if (campusIds[0] != 0)
            {
                foreach (var e in campusIds)
                {
                    if (filterCampus == "")
                    {
                        filterCampus += e.ToString();
                    }
                    else
                    {
                        filterCampus += ", " + e.ToString();
                    }
                }
                if (filterCampus != "")
                {
                    filterCampus = " AND b.CampusId IN (" + filterCampus + ") ";
                }
            }
            #endregion

            #region filterBatch
            string filterBatch = "";
            foreach (var b in batchList)
            {
                if (filterBatch == "")
                {
                    filterBatch += b.Id.ToString();
                }
                else
                {
                    filterBatch += ", " + b.Id.ToString();
                }
            }
            if (filterBatch != "") { filterBatch = " AND b.Id IN (" + filterBatch + ") "; }
            else { filterBatch = " AND b.Id IN (0) "; }
            #endregion

            #endregion

            #region query

            string query = @" Select Count(*) from (
                              SELECT esm.StudentProgramId as studentProgramId
                                    FROM [dbo].[ExamsStudentMarks] AS esm
                                    INNER JOIN [dbo].[ExamsDetails] AS ed ON ed.Id = esm.ExamsDetailsId AND ed.status = 1
                                    INNER JOIN [dbo].[Exams] AS e ON e.Id = ed.ExamId AND e.status = 1                            
                                WHERE  " + selectedExamQuery + filterExamtype + @" 
                            ) as a
                            left Join (
                             SELECT sp.Id as studentProgramId
                                    FROM [dbo].[StudentProgram] AS sp 
                                    INNER JOIN [dbo].[Student] AS s ON s.Id = sp.StudentId
                                    INNER JOIN [dbo].[Batch] AS b ON b.Id = sp.BatchId AND b.Status = 1
                                    INNER JOIN [dbo].[Branch] as br ON br.Id = b.BranchId AND br.Status = 1
		                            INNER JOIN [dbo].[Campus] AS c ON c.Id = b.CampusId AND c.Status = 1                         
                                WHERE 1=1 "+filterProgramSession+ filterBranch + filterBatch + filterCampus + filterGender +@"
                            ) as b ON a.studentProgramId = b.studentProgramId WHERE b.studentProgramId>0";

//            string query = @"
//                            SELECT count(*)
//                                FROM [dbo].[ExamsStudentMarks] AS esm
//                                INNER JOIN [dbo].[StudentProgram] AS sp ON esm.StudentProgramId = sp.Id
//                                INNER JOIN [dbo].[Student] AS s ON s.Id = sp.StudentId
//                                INNER JOIN [dbo].[Batch] AS b ON b.Id = sp.BatchId AND b.Status = 1
//                                INNER JOIN [dbo].[Branch] as br ON br.Id = b.BranchId AND br.Status = 1
//								INNER JOIN [dbo].[Campus] AS c ON c.Id = b.CampusId AND c.Status = 1 
//                                INNER JOIN [dbo].[ExamsDetails] AS ed ON ed.Id = esm.ExamsDetailsId AND ed.status = 1
//                                INNER JOIN [dbo].[Exams] AS e ON e.Id = ed.ExamId AND e.status = 1                            
//                            WHERE " + selectedExamQuery + filterExamtype + filterProgramSession + filterBranch + filterBatch + filterCampus + filterGender + "";


            #endregion
            
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(5000);
            return iQuery.UniqueResult<int>();
        }

        public int GetMobileCount(List<long> programIdList, List<long> branchIdList, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, int[] smsReciever, List<ServiceBlock> serviceBlocks)
        {

            #region Block Student Service Filter

            string blockServiceFilter = "";
            if (serviceBlocks.Any())
            {
                foreach (var sb in serviceBlocks)
                {
                    if (sb.ConditionType == (int)ConditionType.PaymentDue)
                    {
                        blockServiceFilter += " AND sp.DueAmount = 0 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.ImageDue)
                    {
                        blockServiceFilter += " AND sp.IsImage = 1 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.PoliticalReference)
                    {
                        blockServiceFilter += " AND sp.IsPolitical = 0 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.Jsc)
                    {
                        blockServiceFilter += " AND s.IsJsc = 1 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.Ssc)
                    {
                        blockServiceFilter += " AND s.IsSsc = 1 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.Hsc)
                    {
                        blockServiceFilter += " AND s.IsHsc = 1 ";
                        break;
                    }
                }
            }
            #endregion

            #region Filter
            string filterGender = (selectedGender == "1" || selectedGender == "2") ? " AND s.Gender = " + selectedGender + " " : " ";
            string filterProgramSession = " AND sp.ProgramId = " + programId.ToString() + " AND b.SessionId = " + sessionId.ToString() + " ";
            string filterProgramSessionExam = " AND e.ProgramId = " + programId.ToString() + " AND e.SessionId = " + sessionId.ToString() + " ";
            #region selectedExamQuery
            string selectedExamQuery2 = "";
            string selectedExamQuery = "";
            if (selectedExam[0] != 0)
            {
                foreach (var e in selectedExam)
                {
                    if (selectedExamQuery == "")
                        selectedExamQuery += e.ToString();
                    else
                        selectedExamQuery += ", " + e.ToString();
                }
                if (selectedExamQuery != "")
                {
                    selectedExamQuery = " AND e.Id IN (" + selectedExamQuery + ") ";
                }
            }
            if (selectedAttendanceType == "1")
            {
                selectedExamQuery2 = @" AND sp.Id IN (
                                        SELECT DISTINCT esm.StudentProgramId
                                        FROM [dbo].[ExamsStudentMarks] AS esm
                                        LEFT JOIN [dbo].[ExamsDetails] AS ed ON ed.Id = esm.ExamsDetailsId AND ed.status = 1
                                        LEFT JOIN [dbo].[Exams] AS e ON e.Id = ed.ExamId AND e.status = 1
                                        WHERE esm.status = 1
                                        " + selectedExamQuery + ")";
            }
            else if (selectedAttendanceType == "2")
            {
                selectedExamQuery2 = @" AND sp.Id NOT IN (
                                        SELECT DISTINCT esm.StudentProgramId
                                        FROM [dbo].[ExamsStudentMarks] AS esm
                                        LEFT JOIN [dbo].[ExamsDetails] AS ed ON ed.Id = esm.ExamsDetailsId AND ed.status = 1
                                        LEFT JOIN [dbo].[Exams] AS e ON e.Id = ed.ExamId AND e.status = 1
                                        WHERE esm.status = 1
                                        " + selectedExamQuery + ")";
            }
            #endregion
            #region filterCourse
            string filterCourse = "";
            string filterCourseExam = "";
            string filterCourseSCD1 = "";
            string filterCourseSCD2 = "";
            string filterCourseSCD3 = "";

            #region old code
            //            if (courseIds[0] != 0)
            //            {
            //                foreach (var e in courseIds)
            //                {
            //                    if (filterCourse == "")
            //                    {
            //                        filterCourse += e.ToString();
            //                    }
            //                    else
            //                    {
            //                        filterCourse += ", " + e.ToString();
            //                    }
            //                }
            //                if (filterCourse != "")
            //                {
            //                    filterCourseExam = " AND e.CourseId IN (" + filterCourse + ") ";
            //                    filterCourse = " AND cs.CourseId IN (" + filterCourse + ") ";
            //                    //CTE Implementation
            //                    filterCourseSCD1 = @"--TEMP TABLE
            //                                        SELECT * 
            //				                        INTO #StudentCourseDetails
            //				                        FROM (
            //					                        SELECT DISTINCT scd.StudentProgramId, cs.CourseId 
            //		                                    FROM [dbo].[StudentCourseDetails] AS scd 
            //		                                    LEFT JOIN [dbo].[CourseSubject] AS cs ON cs.Id = scd.CourseSubjectId
            //		                                    WHERE scd.status = 1 AND cs.status = 1 " + filterCourse + @"
            //				                        ) as a;";
            //                    filterCourseSCD2 = " INNER JOIN #StudentCourseDetails AS scd ON scd.StudentProgramId = sp.Id ";
            //                    filterCourseSCD3 = @" --TEMP TABLE
            //				                        IF EXISTS(SELECT * FROM #StudentCourseDetails) DROP TABLE #StudentCourseDetails; ";
            //                }
                        //            }
#endregion
            #endregion
            #region filterBranch
            string filterBranch = "";
            if (branchIds[0] != 0)
            {
                foreach (var e in branchIds)
                {
                    if (filterBranch == "")
                    {
                        filterBranch += e.ToString();
                    }
                    else
                    {
                        filterBranch += ", " + e.ToString();
                    }
                }
                if (filterBranch != "")
                {
                    filterBranch = " AND b.BranchId IN (" + filterBranch + ") ";
                }
            }
            #endregion
            #region filterCampus
            string filterCampus = "";
            if (campusIds[0] != 0)
            {
                foreach (var e in campusIds)
                {
                    if (filterCampus == "")
                    {
                        filterCampus += e.ToString();
                    }
                    else
                    {
                        filterCampus += ", " + e.ToString();
                    }
                }
                if (filterCampus != "")
                {
                    filterCampus = " AND b.CampusId IN (" + filterCampus + ") ";
                }
            }
            #endregion
            #region filterBatch
            string filterBatch = "";
            foreach (var b in batchList)
            {
                if (filterBatch == "")
                {
                    filterBatch += b.Id.ToString();
                }
                else
                {
                    filterBatch += ", " + b.Id.ToString();
                }
            }
            if (filterBatch != "") { filterBatch = " AND b.Id IN (" + filterBatch + ") "; }
            else { filterBatch = " AND b.Id IN (0) "; }
            #endregion

            #endregion

            #region query

            string query = filterCourseSCD1 + @"                
               
                
                    with MobileNoCte as ( SELECT   s.id studentId,s.Mobile, s.GuardiansMobile1, s.GuardiansMobile2
	                 
                    FROM [dbo].[StudentProgram] AS sp
	                " + filterCourseSCD2 + @"
                    LEFT JOIN [dbo].[Student] AS s ON s.Id = sp.StudentId AND s.Status = 1
	                LEFT JOIN [dbo].[Batch] AS b ON b.Id = sp.BatchId AND b.Status = 1
	                LEFT JOIN [dbo].[Institute] AS i ON i.Id = sp.InstituteId AND i.Status = 1
	                LEFT JOIN [dbo].[Branch] AS br ON br.Id = b.BranchId AND br.Status = 1
	                LEFT JOIN [dbo].[Campus] AS c ON c.Id = b.CampusId AND c.Status = 1                      
	                WHERE sp.status = 1 
                    " + filterProgramSession + filterGender + filterBranch + filterCampus + filterBatch + blockServiceFilter +
                           selectedExamQuery2 + @" 
                
                ";//+ filterCourseSCD3;
            #endregion

            query += " )";
            string outerQuery = "";
            string innerQuery = "";
            if (smsReciever.Contains(1))
            {
                innerQuery += "Mobile,";
            }
            if (smsReciever.Contains(2))
            {
                innerQuery += "GuardiansMobile1,";
            }
            if (smsReciever.Contains(3))
            {
                innerQuery += "GuardiansMobile2";
            }
            innerQuery = innerQuery.Trim(',', ' ');
            outerQuery += @"SELECT SUM(Mobilecount)
	                                FROM (
	                                SELECT COUNT(DISTINCT MobileNos) Mobilecount
	                                FROM MobileNoCte
	                                UNPIVOT(MobileNos FOR Receiver IN ( ";
            outerQuery += innerQuery;
            outerQuery += @")) unpiv
	                            GROUP BY studentId
	                            ) x";
            outerQuery += filterCourseSCD3;
            query += outerQuery;
            IQuery iQuery = Session.CreateSQLQuery(query);
            //iQuery.SetResultTransformer(Transformers.AliasToBean<MobileCountDto>());
            iQuery.SetTimeout(5000);
            var count= iQuery.UniqueResult<int>();
            return count;
        }
        public IList<ExamMeritListDto> GetExamResultList(List<long> programIdList, List<long> branchIdList, int start, int length, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn = "", int totalRow = -1) 
        {
            string query = GenerateStudentResultQuery(programIdList, branchIdList, start, length, selectedExam, selectedGenerateBy, selectedGender, selectedOrderBy, programId, sessionId, courseIds, branchIds, campusIds, selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, serviceBlocks, prn, totalRow);

            if (length > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY ";
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ExamMeritListDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<ExamMeritListDto>().ToList();

        }

        public bool isAlreadyExistMarks(long examId, long studentProgramId, int examType)
        {
            var criteria = Session.CreateCriteria<ExamsStudentMarks>();
            criteria.Add(Restrictions.Eq("Status", ExamsStudentMarks.EntityStatus.Active));
            criteria.CreateAlias("ExamsDetails", "ed").Add(Restrictions.Eq("ed.Status", ExamsDetails.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("ed.Exams.Id", examId));
            criteria.Add(Restrictions.Eq("ed.ExamType", examType));
            criteria.Add(Restrictions.Eq("StudentProgram.Id", studentProgramId));
            IList<ExamsStudentMarks> examStudentMarks = criteria.List<ExamsStudentMarks>();
            if (examStudentMarks.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ExamHasAlreayMarks(long examId, int setId)
        {
            var criteria = Session.CreateCriteria<ExamsStudentMarks>();
            criteria.Add(Restrictions.Eq("Status", ExamsStudentMarks.EntityStatus.Active));
            criteria.CreateAlias("ExamsDetails", "ed").Add(Restrictions.Eq("ed.Status", ExamsDetails.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("ed.Exams.Id", examId));
            criteria.Add(Restrictions.Eq("SetCode", setId));
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            criteria.SetProjection(Projections.CountDistinct("Id"));
            int rowCount = Convert.ToInt32(criteria.UniqueResult());
            if (rowCount > 0)
                return true;

            return false;
        }

        public int CountClearMarksStudent(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, string gender, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long examId, long examType)
        {
            var criteria = GetAuthorizedStudentsExamMarksQuery(programIdList, branchIdList, programId, sessionId, branchIds, campusIds,
              batchDays, batchTimes, batchIds, courseIds, Convert.ToInt32(gender), examId, Convert.ToInt32(examType));
            criteria.SetProjection(Projections.CountDistinct("StudentProgram"));
            var x = (int)criteria.UniqueResult();
            return x;
        }

        public bool DeleteClearMarks(string ids)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        var deleteQuery = "delete from ExamsStudentMarks where id in("+ids+")";
                        Session.CreateSQLQuery(deleteQuery).SetTimeout(5000)
                        .UniqueResult();
                        trans.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                       // _logger.Error(ex);
                        trans.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public int CountStudentForMeritListSendSms(List<long> programIdList, List<long> branchIdList, long organizationId, long programId, long sessionId, long[] courseIds,
            long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long examId, int examType, int gender)
        {
            var criteria = GetAuthorizedStudentsMeritListSendSmsQuery(programIdList, branchIdList, programId, sessionId, branchIds, campusIds,
                batchDays, batchTimes, batchIds, courseIds, Convert.ToInt32(gender), examId, Convert.ToInt32(examType));
            criteria.SetProjection(Projections.CountDistinct("StudentProgram"));
            criteria.SetTimeout(10000);
            var x = (int)criteria.UniqueResult();
            return x;
        }
        
        public IndividualMeritListDto GetIndividualReport(long programId, long sessionId, long examId,
            long studentProgramId, long branchId)
        {
            var sql = @"
                            DECLARE @studentProgramId BIGINT;
                            DECLARE @selectedExamId BIGINT;

					        SET @selectedExamId = " + examId.ToString() + @";
					        SET @studentProgramId = " + studentProgramId.ToString() + @";


                            SELECT TOP 1 e.Id AS ExamId --, @studentProgramId spId, e.CourseId, ltrim(rtrim(e.Name)) Name --, e.ShortName, e.Code
						   , ROUND(ISNULL(sm.MCQ,0),2,1) AS McqMarks
						   , ROUND(ISNULL(sm.Written,0),2,1)  AS WrittenMarks
	                            , ISNULL(sm.Total,0) AS TotalMarks
	                            , e.McqFullMarks+e.WrittenFullMarks AS FullMarks
	                            , HM.HighestMarks AS HighestMark
	                            --, e.McqFullMarks
	                            --, e.WrittenFullMarks
	                            , sm.CMP AS Cmp
	                            , sm.BMP AS Bmp
	                            , CAST(CASE 
		                            WHEN e.McqFullMarks > 0 THEN 1 
		                            ELSE 0
	                            END AS bit) AS IsMcq
	                            , CAST(CASE 
		                            WHEN e.WrittenFullMarks > 0 THEN 1 
		                            ELSE 0
	                            END AS bit) AS IsWritten
                            FROM Exams as e 
                            INNER JOIN ExamsDetails ed ON e.Id = ed.ExamId
                            LEFT JOIN (
	                            SELECT DISTINCT a.ExamId, a.Total HighestMarks FROM (
		                            SELECT esm.StudentProgramId, ed.ExamId
			                            , sum(esm.ObtainMark) Total
			                            , RANK() OVER(PARTITION BY ed.ExamId ORDER BY SUM(esm.ObtainMark) DESC) CMP
		                            FROM ExamsStudentMarks esm
		                            LEFT JOIN ExamsDetails ed on ed.Id = esm.ExamsDetailsId
		                            WHERE esm.Status!=-404 AND ed.Status!=-404 AND esm.IsNonPayable = 0 AND ed.ExamId = @selectedExamId 
		                            GROUP BY esm.StudentProgramId, ed.ExamId
	                            ) AS a
	                            WHERE a.CMP = 1
                            ) AS HM ON hm.ExamId = e.Id
                            LEFT JOIN (
	                            SELECT * 
	                            FROM (
		                            SELECT a.StudentProgramId, MAX(b.BranchId) BranchId, a.ExamId
			                            , SUM(MCQ) MCQ, SUM(Written) Written, SUM(Total) Total
			                            , RANK() OVER(PARTITION BY ExamId ORDER BY SUM(Total) DESC) CMP
			                            , RANK() OVER(PARTITION BY ExamId, MAX(b.BranchId) ORDER BY SUM(Total) DESC) BMP
		                            FROM (
			                            SELECT esm.StudentProgramId, esm.ExamsDetailsId, ed.ExamType, ed.ExamId
				                            , sum(case when ed.ExamType = 1 then esm.ObtainMark else null end) MCQ
				                            , sum(case when ed.ExamType = 2 then esm.ObtainMark else null end) Written 
				                            , sum(esm.ObtainMark) Total	
			                            FROM ExamsStudentMarks esm
			                            LEFT JOIN ExamsDetails ed on ed.Id = esm.ExamsDetailsId
			                            LEFT JOIN Exams e On ed.ExamId = e.Id
			                            LEFT JOIN StudentProgram sp ON sp.Id = esm.StudentProgramId
			                            WHERE 1=1 --e.ProgramId = @programId AND e.SessionId = @sessionId 
									   AND esm.IsNonPayable = 0 
				                            AND e.Status = 1 AND ed.Status = 1 AND esm.Status = 1 AND sp.Status = 1 
									   AND e.Id = @selectedExamId 
			                            GROUP BY esm.StudentProgramId, esm.ExamsDetailsId, ed.ExamType, ed.ExamId
		                            ) As a
		                            LEFT JOIN StudentProgram sp ON sp.Id = a.StudentProgramId
		                            LEFT JOIN Batch b On b.Id = sp.BatchId
		                            GROUP BY a.StudentProgramId, a.ExamId
	                            ) AS a 
	                            WHERE a.StudentProgramId = @studentProgramId
                            ) AS SM ON sm.ExamId = e.Id
                            WHERE e.Id = @selectedExamId 
						        AND e.Status != -404
	                            AND ed.Status != -404 
                                AND sm.CMP > 0";
            #region OldQuery
            //sql+=";with sml as ( "+
            //     "select "+
            //     "x.spId,x.eId," +
            //     " sum((case" +
            //     " when x.eType = 1 and x.obtainMark is not null" +
            //     " then x.obtainMark" +
            //    // " else 0" +
            //     " end)) as mcqMarks," +
            //     " sum((case" +
            //     " when x.eType = 2 and x.obtainMark is not null" +
            //     " then x.obtainMark " +
            //     //" else 0 " +
            //     " end)) as writtenMarks," +
            //     " sum(x.obtainMark) as totalMarks, " +
            //     //" sum(x.McqFullMarks)+sum(writtenFullMarks) fullMarks," +
            //     " isMcq," +
            //     " isWritten " +
            //     " from " +
            //     " (select " +
            //     " e.Id eId,ed.Id edId,esm.Id esmId,esm.StudentProgramId spId," +
            //     " esm.ObtainMark obtainMark," +
            //     //"ed.TotalQuestion*ed.MarksPerQuestion McqFullMarks," +
            //    // " ed.WrittenFullMark writtenFullMarks," +
            //     "ed.ExamType eType,e.IsMcq isMcq,e.IsWritten isWritten" +
            //     " from " +
            //     " ExamsStudentMarks esm   " +
            //     " right join ExamsDetails ed on ed.Id=esm.ExamsDetailsId" +
            //     " inner join Exams e on e.Id=ed.ExamId" +
            //     " where esm.StudentProgramId= "+studentProgramId+" and e.Id= "+examId+" )x " +
            //     " group by x.eId,x.spId,isMcq,isWritten)" +
            //     " ," +
            //     " totalResult as" +
            //     " (select sml.*,y.highestMark from sml inner join" +
            //     " (" +
            //     " select x.Id eId,max(COALESCE(x.om,0)) highestMark " +
            //     "from( " +
            //     "select e.Id, " +
            //     "sum(COALESCE(esm.ObtainMark,0)) as om " +
            //     "from  " +
            //     "ExamsStudentMarks esm    " +
            //     "inner join ExamsDetails ed on ed.Id=esm.ExamsDetailsId " +
            //     "inner join Exams e on e.Id=ed.ExamId " +
            //     "where   e.Id= "+examId+
            //     " group by e.Id,esm.StudentProgramId " +
            //     ")x group by x.Id " +
            //     ") y on sml.eId=y.eId " +
            //     ") " +
            //     " , cmpList as (select " +
            //     "e.Id eId,rank() over (order by sum(esm.ObtainMark) desc) as cmp,esm.StudentProgramId spId " +
            //     "from " +
            //     "ExamsStudentMarks esm   " +
            //     "inner join ExamsDetails ed on ed.Id=esm.ExamsDetailsId " +
            //     "inner join Exams e on e.Id=ed.ExamId " +
            //     "where   e.Id= "+examId+ " " +
            //     "group by e.Id,esm.StudentProgramId " +
            //     ") " +
            //     ",cmpResult as (select eId,cmp from cmpList where spId= "+studentProgramId+" ) " +
            //     ", bmpList as ( " +
            //     "select " +
            //     "e.Id eId,rank() over (order by sum(esm.ObtainMark) desc) as bmp,esm.StudentProgramId spId " +
            //     "from " +
            //     "ExamsStudentMarks esm   " +
            //     "inner join ExamsDetails ed on ed.Id=esm.ExamsDetailsId " +
            //     "inner join Exams e on e.Id=ed.ExamId " +
            //     "inner join StudentProgram sp on esm.StudentProgramId=sp.Id " +
            //     "inner join Batch b on sp.BatchId=b.Id " +
            //     "inner join Branch br on b.BranchId=br.Id " +
            //     "where   br.Id= "+branchId+" and e.Id= "+examId+" " +
            //     "group by e.Id,esm.StudentProgramId " +
            //     ") " +
            //     ",bmpResult as (select eId,bmp from bmpList where spId= "+studentProgramId+" ) " +
            //     //", fmpList as ( " +
            //     //"select " +
            //     //"dense_rank() over (order by sum(esm.ObtainMark) desc) as fmp,esm.StudentProgramId spId " +
            //     //"from " +
            //     //"ExamsStudentMarks esm   " +
            //     //"inner join ExamsDetails ed on ed.Id=esm.ExamsDetailsId " +
            //     //"inner join Exams e on e.Id=ed.ExamId " +
            //     //"where e.ProgramId= "+programId+" and e.SessionId= "+sessionId+" " +
            //     //" group by esm.StudentProgramId " +
            //     //" ) " +
            //     //" ,fmpResult as " +
            //     //" (select spId,fmp from fmpList where spId= "+studentProgramId+" ) " +
            //     " select " +
            //     " cmpresult.eId ExamId," +
            //     " McqMarks ,WrittenMarks,TotalMarks," +
            //     "FullMarks," +
            //     "HighestMark,Cmp,Bmp" +
            //     //",Fmp" +
            //     ",IsMcq,IsWritten " +
            //     " from totalResult inner join cmpresult " +
            //     " on totalResult.eid=cmpresult.eid " +
            //     " inner join bmpresult " +
            //     " on totalResult.eid=bmpresult.eid " +
            //     " inner join " +
            //     " (select  e.Id examId," +
            //     //" sum(ed.TotalQuestion*ed.MarksPerQuestion)+sum(ed.WrittenFullMark) FullMarks" +
            //     "sum(e.WrittenFullMarks)+sum(e.MCQFullMarks) FullMarks "+
            //     " from  Exams e  where e.Id= "+examId+" group by e.Id)x " +
            //     " on x.examId=totalResult.eid" 
            //     //" inner join fmpResult " +
            //     //" on totalResult.spId=fmpresult.spId"
            //     ;
            #endregion

            IQuery iQuery = Session.CreateSQLQuery(sql);
            iQuery.SetResultTransformer(Transformers.AliasToBean<IndividualMeritListDto>());
            var result = iQuery.UniqueResult<IndividualMeritListDto>();
            return result;
        }

        public long GetFinalMeritPosition(long programId, long sessionId, long studentProgramId,IList<long>registeredCourseIds)
        {
           var sql=" ;with fmpList as (" +
                   " select " +
                   " rank() over (order by sum(esm.ObtainMark) desc) as fmp,esm.StudentProgramId spId " +
                   " from " +
                   " ExamsStudentMarks esm   " +
                   " inner join ExamsDetails ed on ed.Id=esm.ExamsDetailsId " +
                   " inner join Exams e on e.Id=ed.ExamId " +
                   " where e.ProgramId= "+programId+" and e.SessionId="+sessionId +
                   " and e.courseId in(" + string.Join(", ", registeredCourseIds) + ")" +
                   " group by esm.StudentProgramId" +
                   " )" +
                   " select fmp from fmpList where spId ="+studentProgramId
                                        ;
           IQuery iQuery = Session.CreateSQLQuery(sql);
           //iQuery.SetResultTransformer(Transformers.AliasToBean<IndividualMeritListDto>());
           var result = iQuery.UniqueResult<long>();
           return result;
        }

        public int CountExamsSudentMarksUploadData(string markdUploadLogText)
        {
            ICriteria criteria = Session.CreateCriteria<ExamsStudentMarksUploadRawData>();
            if (!string.IsNullOrEmpty(markdUploadLogText))
            {
                var or = Restrictions.Disjunction();
                or.Add(Restrictions.Like("Roll", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("RegNo", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Board", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("BarCode", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("QrCode", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Gender", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Version", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Image", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Answer", markdUploadLogText, MatchMode.Anywhere))
                    //.Add(Restrictions.Like("ObtainMark", markdUploadLogText, MatchMode.Anywhere))
                    .Add(Restrictions.Like("Message", markdUploadLogText, MatchMode.Anywhere));
                criteria.Add(or);
            }
            return criteria.SetProjection(Projections.RowCount()).UniqueResult<int>(); ;
        }


        #endregion

        #region Helper Function

        private string GenerateStudentResultQuery(List<long> programIdList, List<long> branchIdList, int start, int length, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn = "", int TotalRow = -1)
        {
            /*
             * ServiceBlockes
             * *********COMMON*********
             * Program*
             * Session*
             * CourseIds
             * *********RESULT*********
             * ExamIds
             * ExamTypeId*
             * *********STUDENT*********
             * BranchIds
             * CampusIds
             * BatchIds
             * GenderId*
             * Prn*
             * 
             * OrderBy
             * MeritListBy(Total or Percent)[selectedMeritBy]
             * MeritListType(GenerateBy Total or Subject Wise)[selectedGenerateBy]
             * AttendanceType[selectedAttendanceType]
             * Pagination
             */
            #region Block Student Service Filter OK

            string blockServiceFilter = "";
            if (serviceBlocks.Any())
            {
                foreach (var sb in serviceBlocks)
                {
                    if (sb.ConditionType == (int)ConditionType.PaymentDue)
                    {
                        blockServiceFilter += " AND sp.DueAmount = 0 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.ImageDue)
                    {
                        blockServiceFilter += " AND sp.IsImage = 1 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.PoliticalReference)
                    {
                        blockServiceFilter += " AND sp.IsPolitical = 0 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.Jsc)
                    {
                        blockServiceFilter += " AND s.IsJsc = 1 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.Ssc)
                    {
                        blockServiceFilter += " AND s.IsSsc = 1 ";
                        break;
                    }
                    else if (sb.ConditionType == (int)ConditionType.Hsc)
                    {
                        blockServiceFilter += " AND s.IsHsc = 1 ";
                        break;
                    }
                }
            }
            #endregion

            #region filterCourseIds OK
            string filterCourseIds = "";
            if (courseIds[0] != 0)
            {
                foreach (var e in courseIds)
                {
                    if (filterCourseIds == "")
                    {
                        filterCourseIds += e.ToString();
                    }
                    else
                    {
                        filterCourseIds += ", " + e.ToString();
                    }
                }
                filterCourseIds = " AND CourseId IN (" + filterCourseIds + ") ";
            }
            #endregion
            #region filterExamIds OK
            string filterExamIds = "";
            string examNames = " 'All' ";
            if (selectedExam[0] != 0)
            {
                foreach (var e in selectedExam)
                {
                    if (filterExamIds == "")
                        filterExamIds += e.ToString();
                    else
                        filterExamIds += ", " + e.ToString();
                }
                if (filterExamIds != "")
                {
                    filterExamIds = " AND Id IN (" + filterExamIds + ") ";
                    examNames = " STUFF((SELECT ', ' + CAST(ShortName AS VARCHAR(255)) [text()] FROM [dbo].[Exams] AS e WHERE 1=1 " + filterExamIds + @" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') ";
                }
            }
            #endregion
            string filterExamType = selectedExamType == "-1" ? " " : " AND ed.ExamType = " + selectedExamType; //OK
            #region filterBranchIds OK
            string filterBranchIds = "";
            if (branchIds[0] != 0)
            {
                foreach (var e in branchIds)
                {
                    if (filterBranchIds == "")
                    {
                        filterBranchIds += e.ToString();
                    }
                    else
                    {
                        filterBranchIds += ", " + e.ToString();
                    }
                }
                if (filterBranchIds != "")
                {
                    filterBranchIds = " AND b.BranchId IN (" + filterBranchIds + ") ";
                }
            }
            #endregion
            #region filterCampusIds OK
            string filterCampusIds = "";
            if (campusIds[0] != 0)
            {
                foreach (var e in campusIds)
                {
                    if (filterCampusIds == "")
                    {
                        filterCampusIds += e.ToString();
                    }
                    else
                    {
                        filterCampusIds += ", " + e.ToString();
                    }
                }
                if (filterCampusIds != "")
                {
                    filterCampusIds = " AND b.CampusId IN (" + filterCampusIds + ") ";
                }
            }
            #endregion
            #region filterBatchIds OK
            string filterBatchIds = "";
            if (batchList.Count > 0)
            {
                foreach (var b in batchList)
                {
                    if (filterBatchIds == "")
                    {
                        filterBatchIds += b.Id.ToString();
                    }
                    else
                    {
                        filterBatchIds += ", " + b.Id.ToString();
                    }
                }
                if (filterBatchIds != "")
                {
                    filterBatchIds = " AND sp.BatchId IN (" + filterBatchIds + ") ";
                }
            }
            #endregion
            string filterGender = (selectedGender == "1" || selectedGender == "2") ? " AND s.Gender = " + selectedGender + " " : " "; //OK
            #region filterProgramRoll OK
            string filterPrn = "";
            if (!String.IsNullOrEmpty(prn))
            {
                filterPrn = " AND a.PrnNo = '" + prn + "' ";
            }
            #endregion
            #region filterMeritListType OK
            string filterMeritListType = "";
            if (selectedGenerateBy == "2")
            {
                filterMeritListType = @"
                    , CASE WHEN (MAX(a.MCQFullMarks) OVER()) <=0 THEN 'N/A' WHEN MCQMarks IS NULL THEN 'A' ELSE 
                    STUFF(
	                    (
		                    SELECT ', ' + MCQ
		                    FROM (
			                    SELECT esm.StudentProgramId
			                        , CASE WHEN SUM(CASE WHEN ed.ExamType = 1 THEN esm.ObtainMark ELSE null END) IS NOT NULL 
					                    THEN ISNULL(MAX(sub.ShortName),'Combined') +': '+CONVERT(VARCHAR,SUM(CASE WHEN ed.ExamType = 1 THEN esm.ObtainMark ELSE null END)) END MCQ
		                        FROM (
			                        SELECT * FROM Exams WHERE Status = @ActiveStatus AND ProgramId = @ProgramId AND SessionId = @SessionId 
				                    " + filterCourseIds + filterExamIds + @" 
		                        ) AS e
		                        INNER JOIN ExamsDetails AS ed ON ed.ExamId = e.Id
		                        INNER JOIN ExamsStudentMarks AS esm ON esm.ExamsDetailsId = ed.Id
                                LEFT JOIN [Subject] AS sub ON Sub.Id = ed.SubjectId
		                        WHERE ed.Status = @ActiveStatus AND esm.Status = @ActiveStatus AND esm.IsNONPayable = 0 
			                        " + filterExamType + @" 			                
		                        GROUP BY esm.StudentProgramId, ed.ExamType, sub.Id
			                    --ORDER BY esm.StudentProgramId	
		                    ) as b
		                    WHERE b.StudentProgramId = a.StudentProgramId
		                    FOR XML PATH (''))
	                    , 1, 1, '')  
                    END AS MCQMarks 
	                , CASE WHEN (MAX(a.WrittenFullMarks) OVER()) <=0 THEN 'N/A' WHEN WrittenMarks IS NULL THEN 'A' ELSE 
                    STUFF(
	                    (
		                    SELECT ', ' + Written
		                    FROM (
			                    SELECT esm.StudentProgramId
			                        , CASE WHEN SUM(CASE WHEN ed.ExamType = 2 THEN esm.ObtainMark ELSE null END) IS NOT NULL 
					                    THEN ISNULL(MAX(sub.ShortName),'Combined') +': '+CONVERT(VARCHAR,SUM(CASE WHEN ed.ExamType = 2 THEN esm.ObtainMark ELSE null END)) END Written 
		                        FROM (
			                        SELECT * FROM Exams WHERE Status = @ActiveStatus AND ProgramId = @ProgramId AND SessionId = @SessionId 
				                    " + filterCourseIds + filterExamIds + @" 				                
		                        ) AS e
		                        INNER JOIN ExamsDetails AS ed ON ed.ExamId = e.Id
		                        INNER JOIN ExamsStudentMarks AS esm ON esm.ExamsDetailsId = ed.Id
                                LEFT JOIN [Subject] AS sub ON Sub.Id = ed.SubjectId
		                        WHERE ed.Status = @ActiveStatus AND esm.Status = @ActiveStatus AND esm.IsNONPayable = 0 
			                        " + filterExamType + @" 					                
		                        GROUP BY esm.StudentProgramId, ed.ExamType, sub.Id
			                    --ORDER BY esm.StudentProgramId	
		                    ) as b
		                    WHERE b.StudentProgramId = a.StudentProgramId
		                    FOR XML PATH (''))
	                    , 1, 1, '')  
                    END AS WrittenMarks 
                ";
            }
            else
            {
                filterMeritListType = @"
                    , CASE WHEN (MAX(a.MCQFullMarks) OVER()) <=0 THEN 'N/A' WHEN MCQMarks IS NULL THEN 'A' ELSE CONVERT(VARCHAR,MCQMarks) END MCQMarks
	                , CASE WHEN (MAX(a.WrittenFullMarks) OVER()) <=0 THEN 'N/A' WHEN WrittenMarks IS NULL THEN 'A' ELSE CONVERT(VARCHAR,WrittenMarks) END WrittenMarks
                ";
            }
            #endregion

            #region filterAttendanceType CANCLED
            string filterAttendanceType = selectedAttendanceType == "2" ? " LEFT " : " INNER ";
            #endregion

            #region OrderMeritListByTypePagination OK
            string orderBy = "";
            string MeritListBy = "";
            string paginationBy = "";
            if (selectedMeritBy == "2")
            {
                orderBy = selectedOrderBy == "1" ? " ORDER BY CONVERT(int,CMP) ASC " : " ORDER BY PrnNo ASC "; /*ORDER BY ISNULL(CMP,999999) ASC*/
                MeritListBy = " ISNULL(CONVERT(varchar,BMP_P),'-') AS BMP, ISNULL(CONVERT(varchar,CMP_P),'-') AS CMP ";
                paginationBy = selectedOrderBy == "1" ? " RANK() OVER(PARTITION BY 1 ORDER BY isnull(CMP_P,999999),PrnNo ASC) AS Id " : " RANK() OVER(PARTITION BY 1 ORDER BY PrnNo ASC) AS Id ";
            }
            else
            {
                orderBy = selectedOrderBy == "1" ? " ORDER BY CONVERT(int,CMP) ASC " : " ORDER BY PrnNo ASC "; /*ORDER BY ISNULL(CMP,999999) ASC*/
                MeritListBy = " ISNULL(CONVERT(varchar,BMP),'-') AS BMP, ISNULL(CONVERT(varchar,CMP),'-') AS CMP ";
                paginationBy = selectedOrderBy == "1" ? " RANK() OVER(PARTITION BY 1 ORDER BY isnull(CMP,999999),PrnNo ASC) AS Id " : " RANK() OVER(PARTITION BY 1 ORDER BY PrnNo ASC) AS Id ";
            }
            string paginationSelect = length > 0 ? "AND Id Between " + (start + 1).ToString() + " AND " + (start + length).ToString() + " " : " ";
            #endregion

            #region MainQuery
            string query = @"
                    DECLARE @ProgramId BIGINT;
                    DECLARE @SessionId BIGINT;
                    DECLARE @DeletedStatus INT;
                    DECLARE @ActiveStatus INT;

                    SET @ProgramId = " + programId.ToString() + @";
                    SET @SessionId = " + sessionId.ToString() + @";
                    SET @DeletedStatus = -404;
                    SET @ActiveStatus = 1;

                    SELECT * 
                    FROM (
	                    SELECT 
		                    " + paginationBy + @" /* Pagination By */ 
		                    , sp.PrnNo
		                    , s.RegistrationNo, s.NickName, s.FullName, i.Name AS Institute, s.FatherName, s.Mobile, s.GuardiansMobile1, s.GuardiansMobile2
		                    , p.ShortName AS ProgramShortName, sessn.Name AS SessionName, br.Name AS BranchName, c.Name AS CampusName, b.Name AS BatchName
		                    , a.ExamName
		                    " + filterMeritListType + @"/* Filter Merit List Type */
		                    , ISNULL(CONVERT(varchar,a.TotalMarks),'A') AS TotalMarks
		                    , CONVERT(varchar,MAX(a.TotalMarks) OVER()) AS HighestMarks
		                    , CONVERT(varchar,MAX(a.MCQFullMarks) OVER()) AS MCQFullMarks
		                    , CONVERT(varchar,MAX(a.WrittenFullMarks) OVER()) AS WrittenFullMarks
		                    , CONVERT(varchar,MAX(a.FullMarks) OVER()) AS FullMarks
		                    , CONVERT(varchar,ROUND(SUM(a.TotalMarks) OVER()/MAX(a.TotalParticipant) OVER(),2,1)) AS AverageMarks
		                    , ISNULL(CONVERT(varchar,a.PercentMarks),'A') AS PercentMarks
                            , " + MeritListBy + @" 
		                    /* //Merit List By |v| 
		                    , ISNULL(CONVERT(varchar,BMP),'-') AS BMP, ISNULL(CONVERT(varchar,CMP),'-') AS CMP
		                    , ISNULL(CONVERT(varchar,BMP_P),'-') AS BMP_P, ISNULL(CONVERT(varchar,CMP_P),'-') AS CMP_P
                            */
		                    , CONVERT(varchar,MAX(a.TotalParticipant) OVER()) AS TotalParticipant
		                    , CONVERT(varchar,0) TotalStudent 
		                    , COUNT(*) OVER() TotalRow 
		                    --, a.* 
		                    --, sp.*
		                    --, s.*
	                    FROM (
		                    SELECT  
			                    StudentProgramId 
			                    , " + examNames + @" ExamName
			                    , ISNULL(MAX(MCQMarks),0) MCQMarks
			                    , ISNULL(MAX(WrittenMarks),0) WrittenMarks
			                    , MAX(TotalMarks) TotalMarks
			                    , MAX(TotalMarks) HighestMarks
			                    , MAX(MCQFullMarks) MCQFullMarks
			                    , MAX(WrittenFullMarks) WrittenFullMarks
			                    , MAX(FullMarks) FullMarks
			                    , MAX(PercentMarks) PercentMarks
			                    , MAX(BMP) BMP
			                    , MAX(CMP) CMP
			                    , MAX(BMP_P) BMP_P
			                    , MAX(CMP_P) CMP_P
			                    , MAX(TotalParticipant) TotalParticipant
		                    FROM (
			                    SELECT a.StudentProgramId 
				                    , ROUND(SUM(MCQ),2,1) MCQMarks
				                    , ROUND(SUM(Written),2,1) WrittenMarks
				                    --, ROUND(( SUM(Total) / ( SUM(MCQFull) + SUM(WrittenFull) ) ) * 100,2,1) PercentMarks
				                    , ROUND(( SUM(PercentMark) / Count(*) ) ,2,1) PercentMarks
				                    , ROUND(SUM(Total),2,1) TotalMarks
				                    , ROUND(SUM(MCQFull),2,1) MCQFullMarks
				                    , ROUND(SUM(WrittenFull),2,1) WrittenFullMarks
				                    , ROUND(( SUM(MCQFull) + SUM(WrittenFull) ),2,1) FullMarks
				                    , RANK() OVER(PARTITION BY MAX(b.BranchId) ORDER BY SUM(Total) DESC) BMP
				                    , RANK() OVER(PARTITION BY 1 ORDER BY SUM(Total) DESC) CMP
				                    , RANK() OVER(PARTITION BY MAX(b.BranchId) ORDER BY ( SUM(PercentMark) / Count(*) ) DESC) BMP_P
				                    , RANK() OVER(PARTITION BY 1 ORDER BY ( SUM(PercentMark) / Count(*) ) DESC) CMP_P	
				                    , Count(*) OVER() TotalParticipant
			                    FROM (
				                    SELECT esm.StudentProgramId, ed.ExamId
					                    , MAX(e.ShortName) ExamName
					                    , SUM(CASE WHEN ed.ExamType = 1 THEN esm.ObtainMark ELSE null END) MCQ
					                    , SUM(CASE WHEN ed.ExamType = 2 THEN esm.ObtainMark ELSE null END) Written 
					                    , SUM(CASE WHEN ed.ExamType = 1 THEN ISNULL(ed.TotalQuestion,0) * ISNULL(ed.MarksPerQuestion,0) ELSE 0 END) MCQFull
					                    , SUM(CASE WHEN ed.ExamType = 2 THEN ISNULL(ed.WrittenFullMark,0) ELSE 0 END) WrittenFull 
					                    , SUM(CASE WHEN ed.ExamType = 1 
						                    THEN (esm.ObtainMark / (ISNULL(ed.TotalQuestion,0) * ISNULL(ed.MarksPerQuestion,0)) ) * 100 
						                    ELSE (esm.ObtainMark / (ISNULL(ed.WrittenFullMark,0)) ) * 100  
					                    END) AS PercentMark
					                    , SUM(esm.ObtainMark) Total	
				                    FROM (
					                    SELECT * FROM Exams WHERE Status = @ActiveStatus AND ProgramId = @ProgramId AND SessionId = @SessionId 
									        " + filterCourseIds + filterExamIds + @"  /* Filter Course Ids */ /* Filter Exam Ids */
				                    ) AS e
				                    INNER JOIN ExamsDetails AS ed ON ed.ExamId = e.Id
				                    INNER JOIN ExamsStudentMarks AS esm ON esm.ExamsDetailsId = ed.Id
				                    WHERE ed.Status = @ActiveStatus AND esm.Status = @ActiveStatus AND esm.IsNONPayable = 0 
								        " + filterExamType + @"            /* Filter Exam Type */
				                    GROUP BY esm.StudentProgramId, esm.ExamsDetailsId, ed.ExamType, ed.ExamId		
			                    ) As a
			                    INNER JOIN StudentProgram AS sp ON sp.Id = a.StudentProgramId AND sp.Status = @ActiveStatus
			                    INNER JOIN Batch AS b ON b.Id = sp.BatchId AND b.Status = @ActiveStatus
			                    GROUP BY a.StudentProgramId
		                    ) AS a
		                    GROUP BY a.StudentProgramId
	                    ) AS a
	                    INNER JOIN StudentProgram AS sp ON sp.id = a.StudentProgramId
	                    INNER JOIN Student AS s ON s.Id = sp.StudentId
	                    LEFT JOIN Institute AS i ON i.Id = sp.InstituteId
	                    INNER JOIN Program AS P ON p.Id = sp.ProgramId 
	                    INNER JOIN Batch AS b ON b.Id = sp.BatchId
	                    INNER JOIN [Session] AS sessn ON sessn.Id = b.SessionId
	                    INNER JOIN Campus AS c ON c.Id = b.CampusId
	                    INNER JOIN Branch AS br ON br.Id = b.BranchId
	                    WHERE 1=1
                            /*replace filter by pron */
		                    " + blockServiceFilter + @" /* AND DueAmount = 0 AND IsImage = 1 AND IsPolitical = 0 AND IsJsc = 1 AND IsSsc = 1 AND IsHsc = 1 */ 
                            " + filterGender + @" /* AND Gender = 1 */ 
                            " + filterBranchIds + @" /* AND BranchId IN () */ 
                            " + filterCampusIds + @" /* AND CampusId IN ()*/
	                        " + filterBatchIds + @" /* AND BatchId IN () */ 
                    ) AS a
                    WHERE 1=1 
		                " + filterPrn + @" /* Filter By PrnNumber*/
	                    -- " + paginationSelect + @" /* AND Id Between 1 AND 10   */ 
	                " + orderBy + @" /* ORDER BY CMP ASC*/
            ";

            #region OLD QUERY
            string oldQuery = @"/*--------------------------------------------------------------------------------------------*/            
/*
                DECLARE @ProgramId BIGINT;
                DECLARE @SessionId BIGINT;
                DECLARE @DeletedStatus INT;
                DECLARE @ActiveStatus INT;

                DECLARE @TotalParticipant INT;
                DECLARE @FullMarks FLOAT;
                DECLARE @AverageMarks FLOAT;
                DECLARE @MCQFullMarks FLOAT;
                DECLARE @WrittenFullMarks FLOAT;
                DECLARE @HighestMarks FLOAT;

                SET @ProgramId = " + programId.ToString() + @";
                SET @SessionId = " + sessionId.ToString() + @";
                SET @DeletedStatus = -404;
                SET @ActiveStatus = 1;

                SELECT  
	                StudentProgramId 
	                , " + examNames + @" ExamName
	                , MAX(MCQMarks) MCQMarks
	                , MAX(WrittenMarks) WrittenMarks
	                , MAX(TotalMarks) TotalMarks
	                , MAX(TotalMarks) HighestMarks
	                , MAX(MCQFullMarks) MCQFullMarks
	                , MAX(WrittenFullMarks) WrittenFullMarks
	                , MAX(FullMarks) FullMarks
	                , MAX(PercentMarks) PercentMarks
	                , MAX(BMP) BMP
	                , MAX(CMP) CMP
	                , MAX(BMP_P) BMP_P
	                , MAX(CMP_P) CMP_P
	                , MAX(TotalParticipant) TotalParticipant
                INTO #TempResultSummery
                FROM (
	                SELECT a.StudentProgramId 
		                , ROUND(SUM(MCQ),2,1) MCQMarks
		                , ROUND(SUM(Written),2,1) WrittenMarks
		                --, ROUND(( SUM(Total) / ( SUM(MCQFull) + SUM(WrittenFull) ) ) * 100,2,1) PercentMarks
		                , ROUND(( SUM(PercentMark) / Count(*) ) ,2,1) PercentMarks
		                , ROUND(SUM(Total),2,1) TotalMarks
		                , ROUND(SUM(MCQFull),2,1) MCQFullMarks
		                , ROUND(SUM(WrittenFull),2,1) WrittenFullMarks
		                , ROUND(( SUM(MCQFull) + SUM(WrittenFull) ),2,1) FullMarks
		                , RANK() OVER(PARTITION BY MAX(b.BranchId) ORDER BY SUM(Total) DESC) BMP
		                , RANK() OVER(PARTITION BY 1 ORDER BY SUM(Total) DESC) CMP
		                , RANK() OVER(PARTITION BY MAX(b.BranchId) ORDER BY ( SUM(PercentMark) / Count(*) ) DESC) BMP_P
		                , RANK() OVER(PARTITION BY 1 ORDER BY ( SUM(PercentMark) / Count(*) ) DESC) CMP_P	
		                , Count(*) OVER() TotalParticipant
	                FROM (
		                SELECT esm.StudentProgramId, ed.ExamId
			                , MAX(e.ShortName) ExamName
			                , SUM(CASE WHEN ed.ExamType = 1 THEN esm.ObtainMark ELSE null END) MCQ
			                , SUM(CASE WHEN ed.ExamType = 2 THEN esm.ObtainMark ELSE null END) Written 
			                , SUM(CASE WHEN ed.ExamType = 1 THEN ISNULL(ed.TotalQuestion,0) * ISNULL(ed.MarksPerQuestion,0) ELSE 0 END) MCQFull
			                , SUM(CASE WHEN ed.ExamType = 2 THEN ISNULL(ed.WrittenFullMark,0) ELSE 0 END) WrittenFull 
			                , SUM(CASE WHEN ed.ExamType = 1 
								THEN (esm.ObtainMark / (ISNULL(ed.TotalQuestion,0) * ISNULL(ed.MarksPerQuestion,0)) ) * 100 
								ELSE (esm.ObtainMark / (ISNULL(ed.WrittenFullMark,0)) ) * 100  
							END) AS PercentMark
			                , SUM(esm.ObtainMark) Total	
		                FROM (
			                SELECT * FROM Exams WHERE Status = @ActiveStatus AND ProgramId = @ProgramId AND SessionId = @SessionId 
				                " + filterCourseIds + filterExamIds + @" 
		                ) AS e
		                INNER JOIN ExamsDetails AS ed ON ed.ExamId = e.Id
		                INNER JOIN ExamsStudentMarks AS esm ON esm.ExamsDetailsId = ed.Id
		                WHERE ed.Status = @ActiveStatus AND esm.Status = @ActiveStatus AND esm.IsNONPayable = 0 
			                " + filterExamType + @" 
		                GROUP BY esm.StudentProgramId, esm.ExamsDetailsId, ed.ExamType, ed.ExamId		
	                ) As a
	                INNER JOIN StudentProgram AS sp ON sp.Id = a.StudentProgramId AND sp.Status = @ActiveStatus
	                INNER JOIN Batch AS b ON b.Id = sp.BatchId AND b.Status = @ActiveStatus
	                GROUP BY a.StudentProgramId
                ) AS a
                GROUP BY a.StudentProgramId;

                SELECT @TotalParticipant = MAX(TotalParticipant)
	                , @FullMarks = MAX(FullMarks)
	                , @AverageMarks = ROUND(SUM(TotalMarks)/MAX(TotalParticipant),2,1)
	                , @MCQFullMarks = MAX(MCQFullMarks)
	                , @WrittenFullMarks = MAX(WrittenFullMarks)
	                , @HighestMarks = MAX(TotalMarks)
                FROM #TempResultSummery

                SELECT  
	                a.PrnNo, a.RegistrationNo, a.NickName, a.FullName, i.Name Institute, a.FatherName, a.Mobile, a.GuardiansMobile1, a.GuardiansMobile2
	                , a.Gender, a.IsJsc, a.IsSsc, a.IsHsc, a.DueAmount, a.IsImage, a.IsPolitical
	                , p.ShortName ProgramShortName, sess.Name SessionName, br.Name BranchName, c.Name CampusName, a.BatchName, a.BatchId
		            , a.SessionId, a.BranchId, a.CampusId
	                , a.TotalStudent
	                , result.*
                INTO #TempFullMeritList
                FROM (	
	                SELECT *, COUNT(*) OVER() TotalStudent
	                FROM (
	                    SELECT DISTINCT sp.Id StudentProgramId, sp.PrnNo, sp.ProgramId, sp.InstituteId, sp.DueAmount, sp.IsImage, sp.IsPolitical
		                    , s.RegistrationNo, s.NickName, s.FullName, s.FatherName, s.Mobile, s.GuardiansMobile1, s.GuardiansMobile2, s.Gender, s.IsJsc, s.IsSsc, s.IsHsc
		                    , b.SessionId, b.BranchId, b.CampusId, b.Name BatchName, b.Id BatchId
		
	                    FROM (
		                    SELECT Id, PrnNo, DueAmount, StudentId, BatchId, ProgramId, InstituteId, IsImage, IsPolitical FROM StudentProgram
		                    WHERE Status = @ActiveStatus AND ProgramId = @ProgramId /*AND DueAmount = 1 AND IsImage = 1 AND IsPolitical = 1 */
	                    ) AS sp
	                    INNER JOIN (
		                    SELECT Id, RegistrationNo, NickName, FullName, FatherName, Mobile, GuardiansMobile1, GuardiansMobile2, Gender, IsJsc, IsSsc, IsHsc
		                    FROM Student 
		                    WHERE Status = @ActiveStatus /*AND Gender = 1 AND IsJsc = 1 AND IsSsc = 1 AND IsHsc = 1*/
	                    ) AS s ON s.Id = sp.StudentId
	                    INNER JOIN (
		                    SELECT Id, SessionId, BranchId, CampusId, Name  FROM Batch WHERE Status = @ActiveStatus AND ProgramId = @ProgramId AND SessionId = @SessionId 
	                    ) AS b ON b.Id = sp.BatchId
	                    INNER JOIN (
		                    SELECT distinct cs.CourseId, scd.StudentProgramId
		                    FROM StudentCourseDetails AS scd
		                    INNER JOIN CourseSubject AS cs ON cs.Id = scd.CourseSubjectId
		                    WHERE scd.Status = @ActiveStatus AND cs.Status = @ActiveStatus " + filterCourseIds + @"  
	                    ) AS scd ON scd.StudentProgramId = sp.Id
                    ) AS a
                ) AS a
                LEFT JOIN Institute AS I ON i.Id = a.InstituteId
                INNER JOIN Program AS p ON p.Id = a.ProgramId
                INNER JOIN [Session] AS sess ON sess.Id = a.SessionId
                INNER JOIN Branch AS br ON br.Id = a.BranchId
                INNER JOIN Campus AS c ON c.Id = a.CampusId
                " + filterAttendanceType + @" JOIN #TempResultSummery AS result ON result.StudentProgramId = a.StudentProgramId;

                SELECT Id, PrnNo, RegistrationNo, NickName, FullName, Institute, FatherName, Mobile, GuardiansMobile1, GuardiansMobile2
                    , ProgramShortName, SessionName, BranchName, CampusName, BatchName
	                , ExamName
	                " + filterMeritListType + @"
	                , ISNULL(CONVERT(varchar,TotalMarks),'A') AS TotalMarks
	                , CONVERT(varchar,@HighestMarks) HighestMarks
                    , CONVERT(varchar,@MCQFullMarks) MCQFullMarks
                    , CONVERT(varchar,@WrittenFullMarks) WrittenFullMarks
                    , CONVERT(varchar,@FullMarks) FullMarks
                    , CONVERT(varchar,@AverageMarks) AverageMarks
	                , ISNULL(CONVERT(varchar,PercentMarks),'A') AS PercentMarks
                    , " + MeritListBy + @" 
	                /*, ISNULL(CONVERT(varchar,BMP),'-') AS BMP, ISNULL(CONVERT(varchar,CMP),'-') AS CMP*/ 
	                /*, ISNULL(CONVERT(varchar,BMP_P),'-') AS BMP_P, ISNULL(CONVERT(varchar,CMP_P),'-') AS CMP_P */
                    , CONVERT(varchar,@TotalParticipant) TotalParticipant
                    , CONVERT(varchar,TotalStudent) TotalStudent 
                    , TotalRow
                FROM (
                    SELECT " + paginationBy + @", *, COUNT(*) OVER() TotalRow 
                    FROM #TempFullMeritList
                    WHERE 1=1 
                        " + blockServiceFilter + @" /* AND DueAmount = 0 AND IsImage = 1 AND IsPolitical = 0 AND IsJsc = 1 AND IsSsc = 1 AND IsHsc = 1 */ 
                        " + filterGender + @" /* AND Gender = 1 */ 
                        " + filterBranchIds + @" /* AND BranchId IN () */ 
                        " + filterCampusIds + @" /* AND CampusId IN ()*/
	                    " + filterBatchIds + @" /* AND BatchId IN () */ 
	                    " + filterPrn + @" /* Filter By PrnNumber*/
                    ) AS a 
                    WHERE 1=1 
	                " + paginationSelect + @" /* AND Id Between 1 AND 10   */ 
	                " + orderBy + @" /* ORDER BY CMP ASC*/

                IF EXISTS(SELECT 1 FROM #TempFullMeritList) DROP TABLE #TempFullMeritList;
                IF EXISTS(SELECT 1 FROM #TempResultSummery) DROP TABLE #TempResultSummery;
            */";
            #endregion
            #endregion

            return query;
        }

        private ICriteria GetAuthorizedStudentsMeritListSendSmsQuery(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int gender, long examId, int examType)
        {
            ICriteria criteria = Session.CreateCriteria<ExamsStudentMarks>().Add(Restrictions.Eq("Status", ExamsStudentMarks.EntityStatus.Active));
            criteria.CreateAlias("StudentProgram", "sp").Add(Restrictions.Eq("sp.Status", StudentProgram.EntityStatus.Active));
            criteria.CreateAlias("sp.Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("sp.Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            criteria.CreateAlias("sp.Batch", "b").Add(Restrictions.Eq("b.Status", Batch.EntityStatus.Active));
            criteria.CreateAlias("b.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("b.Campus", "ca").Add(Restrictions.Eq("ca.Status", Campus.EntityStatus.Active));

            criteria.CreateAlias("ExamsDetails", "ed").Add(Restrictions.Eq("ed.Status", ExamsDetails.EntityStatus.Active));
            criteria.CreateAlias("ed.Exams", "exm").Add(Restrictions.Eq("exm.Status", Exams.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("exm.Id", examId));
            if (examType != -1)
                criteria.Add(Restrictions.Eq("ed.ExamType", examType));

            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("br.Id", branchIdList));
            }
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("p.Id", programIdList));
            }
            criteria.Add(Restrictions.Eq("p.Id", programId));
            criteria.Add(Restrictions.Eq("se.Id", sessionId));

            if (!(Array.Exists(branchId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("br.Id", branchId));
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("ca.Id", campusId));
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {
                criteria.Add(Restrictions.In("b.Days", batchDays));
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }

            if (!(Array.Exists(batchId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Id", batchId));
            }

            if (!(Array.Exists(courseId, item => item == 0)))
                criteria.CreateCriteria("sp.StudentCourseDetails", "cd", JoinType.InnerJoin).CreateAlias("cd.CourseSubject", "cdcs").Add(Restrictions.Eq("cdcs.Status", CourseSubject.EntityStatus.Active))
                    .CreateAlias("cdcs.Course", "cdcsc").Add(Restrictions.Eq("cdcsc.Status", Course.EntityStatus.Active))
                    .Add(Restrictions.In("cdcsc.Id", courseId));

            if (gender != 4)
            {
                criteria.Add(Restrictions.Eq("st.Gender", gender));
            }
            //considerable
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            return criteria;
        }
        private ICriteria GetAuthorizedStudentsExamMarksQuery(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int gender, long examId, int examType)
        {
            ICriteria criteria = Session.CreateCriteria<ExamsStudentMarks>().Add(Restrictions.Eq("Status", ExamsStudentMarks.EntityStatus.Active));
            criteria.CreateAlias("StudentProgram", "sp").Add(Restrictions.Eq("sp.Status", StudentProgram.EntityStatus.Active));
            criteria.CreateAlias("sp.Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            //criteria.CreateAlias("sp.Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            criteria.CreateAlias("sp.Student", "st").Add(Restrictions.Not(Restrictions.Eq("st.Status", Student.EntityStatus.Delete)));
            criteria.CreateAlias("sp.Batch", "b").Add(Restrictions.Eq("b.Status", Batch.EntityStatus.Active));
            criteria.CreateAlias("b.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("b.Campus", "ca").Add(Restrictions.Eq("ca.Status", Campus.EntityStatus.Active));

            criteria.CreateAlias("ExamsDetails", "ed").Add(Restrictions.Eq("ed.Status", ExamsDetails.EntityStatus.Active));
            criteria.CreateAlias("ed.Exams", "exm").Add(Restrictions.Eq("exm.Status", Exams.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("exm.Id", examId));
            if (examType != -1)
                criteria.Add(Restrictions.Eq("ed.ExamType", examType));

            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("br.Id", branchIdList));
            }
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("p.Id", programIdList));
            }
            criteria.Add(Restrictions.Eq("p.Id", programId));
            criteria.Add(Restrictions.Eq("se.Id", sessionId));

            if (!(Array.Exists(branchId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("br.Id", branchId));
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("ca.Id", campusId));
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {
                criteria.Add(Restrictions.In("b.Days", batchDays));
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }

            if (!(Array.Exists(batchId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Id", batchId));
            }

            //DetachedCriteria courseDetachedCriteria = DetachedCriteria.For<StudentCourseDetail>().SetProjection(Projections.Distinct(Projections.Property("StudentProgram.Id")));
            //courseDetachedCriteria.CreateAlias("StudentProgram", "sp").CreateAlias("sp.Program", "Program").Add(Restrictions.Eq("Program.Id", programId));
            //courseDetachedCriteria.CreateAlias("CourseSubject", "cs").Add(Restrictions.Eq("cs.Status", CourseSubject.EntityStatus.Active));
            //courseDetachedCriteria.CreateAlias("cs.Course", "co").Add(Restrictions.Eq("co.Status", Course.EntityStatus.Active));//.Add(Restrictions.In("co.Id", courseId));

            //if (!(Array.Exists(courseId, item => item == 0)))
            //{
            //    courseDetachedCriteria.Add(Restrictions.In("co.Id", courseId));
            //}
            //criteria.Add(Subqueries.PropertyIn("sp.Id", courseDetachedCriteria));

            if (!(Array.Exists(courseId, item => item == 0)))
                criteria.CreateCriteria("sp.StudentCourseDetails", "cd", JoinType.InnerJoin).CreateAlias("cd.CourseSubject", "cdcs").Add(Restrictions.Eq("cdcs.Status", CourseSubject.EntityStatus.Active))
                    .CreateAlias("cdcs.Course", "cdcsc").Add(Restrictions.Eq("cdcsc.Status", Course.EntityStatus.Active))
                    .Add(Restrictions.In("cdcsc.Id", courseId));

            if (gender != 4)
            {
                criteria.Add(Restrictions.Eq("st.Gender", gender));
            }
            //considerable
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            return criteria;
        }
        
        #endregion
    }
}
