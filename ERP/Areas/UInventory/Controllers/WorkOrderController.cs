﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.UserAuth;


namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class WorkOrderController : Controller
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly ItemService _itemService;
        private readonly ItemGroupService _itemGroupService;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> authorizeMenu;
        private readonly IBranchService _branchService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private ISupplierPriceQuoteService _supplierPriceQuoteService;
        private IWorkOrderService _workOrderService;
        private IProgramBranchSessionService _programBranchSessionService;

        public WorkOrderController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _itemService = new ItemService(session);
            _organizationService = new OrganizationService(session);
            authorizeMenu = new List<UserMenu>();
            _itemGroupService = new ItemGroupService(session);
            _branchService = new BranchService(session);
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _supplierPriceQuoteService = new SupplierPriceQuoteService(session);
            _workOrderService = new WorkOrderService(session);
            _programBranchSessionService = new ProgramBranchSessionService(session);
        }

        #endregion

        #region Work Order Issue

        public ActionResult WorkOrderIssue(long supplierPriceQuotationId)
        {
            SupplierPriceQuote supplierPriceQuote;
            try
            {
                supplierPriceQuote = _supplierPriceQuoteService.GetSupplierPriceQuote(supplierPriceQuotationId);
                if (supplierPriceQuote == null)
                {
                    ViewBag.ErrorMessage = "Quotation can't empty!!";
                }
                else
                {
                    var totalOrderQuantity = supplierPriceQuote.Quotation.WorkOrderList.Where(x => x.Status == WorkOrder.EntityStatus.Active && x.WorkOrderStatus != WorkOrderStatus.Cancelled)
                        .Sum(x => x.OrderedQuantity);
                    ViewBag.TotalRemainingQuantity = (supplierPriceQuote.Quotation.QuotationQuantity - totalOrderQuantity) ?? 0;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View(supplierPriceQuote);
        }

        public ActionResult WorkOrderIssueToSupplier(long priceQuotationId, int orderQuantity, float negotiatedUnitCost)
        {
            try
            {
                var quotationStatus = (QuotationStatus)0;
                var supplierPriceQuote =
                    _supplierPriceQuoteService.GetSupplierPriceQuote(priceQuotationId);
                if (supplierPriceQuote == null)
                    return Json(new { IsSuccess = false, Message = "Price quotation can't empty!!" });

                var totalPreviosOrderQuantity = supplierPriceQuote.Quotation.WorkOrderList.Where(
                    x => x.Status == WorkOrder.EntityStatus.Active && x.WorkOrderStatus != WorkOrderStatus.Cancelled)
                    .Sum(x => x.OrderedQuantity);
                var totalOrderQuantity = orderQuantity + totalPreviosOrderQuantity;
                if (supplierPriceQuote.Quotation.QuotationQuantity < totalOrderQuantity || orderQuantity < 1)
                    return Json(new { IsSuccess = false, Message = "Please order valid quantity" });

                string workOrderNumber = "";

                if (supplierPriceQuote.Quotation.Branch != null &&
                    supplierPriceQuote.Quotation.Branch.Organization != null)
                {
                    var lastWorkOrderNo = _workOrderService.GetLastWorkOrderNo(supplierPriceQuote.Quotation.Branch.Id);
                    int workOrderLast6Digit = lastWorkOrderNo != ""
                        ? Convert.ToInt32(lastWorkOrderNo.Substring(lastWorkOrderNo.Length - 6, 6)) + 1
                        : 000001;

                    workOrderNumber = supplierPriceQuote.Quotation.Branch.Organization.ShortName.Substring(0, 3) + "" +
                                      supplierPriceQuote.Quotation.Branch.Code + "WO";

                    workOrderNumber = workOrderNumber + workOrderLast6Digit.ToString("000000");
                }
                else throw new NullObjectException("Organization or Branch can not empty");


                if (supplierPriceQuote.Quotation.QuotationQuantity == totalOrderQuantity)
                {
                    quotationStatus = QuotationStatus.Issued;
                }
                else if (supplierPriceQuote.Quotation.QuotationQuantity > totalOrderQuantity)
                {
                    quotationStatus = QuotationStatus.PertialIssued;
                }
                supplierPriceQuote.Quotation.QuotationStatus = quotationStatus;

                supplierPriceQuote.PriceQuoteStatus = (int)PriceQouteStatus.WoReceived;

                var workOrder = new WorkOrder()
                {
                    Supplier = supplierPriceQuote.Supplier,
                    Quotation = supplierPriceQuote.Quotation,
                    WorkOrderStatus = WorkOrderStatus.Issued,
                    DeliveryDate = supplierPriceQuote.Quotation.DeliveryDate,
                    OrderedQuantity = orderQuantity,
                    TotalCost = Convert.ToDecimal(orderQuantity * negotiatedUnitCost),
                    WorkOrderDate = DateTime.Now,
                    WorkOrderNo = workOrderNumber,
                    Branch = supplierPriceQuote.Quotation.Branch
                };

                var workOrderDetail = new WorkOrderDetails()
                {
                    Item = supplierPriceQuote.Quotation.Item,
                    Program = supplierPriceQuote.Quotation.Program,
                    Session = supplierPriceQuote.Quotation.Session,
                    WorkOrder = workOrder,
                    Purpose = supplierPriceQuote.Quotation.Purpose,
                    PurposeProgram = supplierPriceQuote.Quotation.PurposeProgram,
                    PurposeSession = supplierPriceQuote.Quotation.PurposeSession,
                    OrderQuantity = orderQuantity,
                    UnitCost = Convert.ToDecimal(negotiatedUnitCost),
                    TotalCost = Convert.ToDecimal(orderQuantity * negotiatedUnitCost),
                };

                IList<WorkOrderCriteria> workOrderCriteriaList = new List<WorkOrderCriteria>();
                foreach (var quotationCriteria in supplierPriceQuote.Quotation.QuotationCriteriaList)
                {
                    var workOrderCriteria = new WorkOrderCriteria()
                    {
                        WorkOrderDetail = workOrderDetail,
                        Criteria = quotationCriteria.Criteria,
                        CriteriaValue = quotationCriteria.CriteriaValue
                    };
                    workOrderCriteriaList.Add(workOrderCriteria);
                }

                workOrderDetail.WorkOrderCriteriaList = workOrderCriteriaList;
                workOrder.WorkOrderDetails.Add(workOrderDetail);
                _workOrderService.WorkOrderSave(workOrder);

                //var totalOrdrQuantity = supplierPriceQuote.Quotation.WorkOrderList.Where(x => x.Status == WorkOrder.EntityStatus.Active)
                //        .Sum(x => x.OrderedQuantity);
                //ViewBag.TotalRemainingQuantity = (supplierPriceQuote.Quotation.QuotationQuantity - totalOrdrQuantity) ?? 0;
                //ViewBag.SupplierPriceQuotation = supplierPriceQuote;
                //ViewBag.OrderQuantity = orderQuantity;
                //ViewBag.NegotiatedUnitCost = negotiatedUnitCost;
                //ViewBag.TotalCost = (orderQuantity*negotiatedUnitCost);

                ViewBag.WorkOrder = workOrder;

                ViewBag.SuccessMessage = "Work order issued Successfully !!";
                return PartialView("Partial/_printWorkOrder");
                //return Json(new { IsSuccess = true, Message = "Work order issued Successfully!!" });
            }
            catch (MessageException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (NullObjectException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, Message = WebHelper.SetExceptionMessage(ex) });
            }
        }

        #endregion

        #region print WOrkOrder

        public ActionResult PrintWorkOrderFromQuotationReview(long workOrderId)
        {
            WorkOrder obj = new WorkOrder();
            try
            {
                obj = _workOrderService.GetWorkOrder(workOrderId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View(obj);
        }

        #endregion

        #region Cancel Work Order

        public ActionResult CancelWorkOrderFromQuotationReview(long id)
        {
            try
            {
                bool isCancel = _workOrderService.IsCancel(id);
                if (isCancel)
                {
                    return Json(new { IsSuccess = true, returnMessage = "Work order cancelled." });
                }
                return Json(new Response(true, "Problem Occurred. Retry"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Report

        [HttpGet]
        public ActionResult WorkOrderReport()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var authorgList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            ViewBag.organizationList = authorgList;
            ViewBag.branchIdList = new SelectList(string.Empty, "Value", "Text");
            var itemList = _itemGroupService.LoadItemGroup();
            itemList.Insert(0, new ItemGroup() { Id = 0, Name = "All selected" });
            ViewBag.itemGroupList = new SelectList(itemList, "Id", "Name");
            ViewBag.itemList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.programSessionList = new SelectList(string.Empty, "Value", "Text");
            var purposeList = _commonHelper.LoadEmumToDictionary<Purpose>();
            purposeList = new Dictionary<int, string> { { 0, "All selected" }, { -1, "N/A" } }.Concat(purposeList).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.perposeList = new SelectList(purposeList, "Key", "Value");
            var workOrderTypeDictionary = _commonHelper.LoadEmumToDictionary<WorkOrderType>();
            workOrderTypeDictionary = new Dictionary<int, string> { { 0, "Select All" } }.Concat(workOrderTypeDictionary).ToDictionary(k => k.Key, v => v.Value);
            var workOrderStatusDictionary = _commonHelper.LoadEmumToDictionary<WorkOrderStatus>();
            workOrderStatusDictionary = (new Dictionary<int, string> { { 0, "Select All" } }).Concat(workOrderStatusDictionary).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.WorkOrderTypeList = new SelectList(workOrderTypeDictionary, "Key", "Value");
            ViewBag.WorkOrderStatusList = new SelectList(workOrderStatusDictionary, "Key", "Value");
            return View();
        }

        [HttpPost]
        public ActionResult WorkOrderReport(WorkOrderReportFormViewModel workOrderReport)
        {
            if (ModelState.IsValid)
            {
                var programSession = "";
                //if (workOrderReport.ProgramSession.Contains("0"))
                //{
                //    programSession = "All Program & Session";
                //}
                //var purpose = "";
                //if (workOrderReport.Purpose.Where(x => (int)x == SelectionType.SelelectAll).Any())
                //{
                //    purpose = "All Purposes";
                //}
                if (workOrderReport.ProgramSession == null)
                {
                    programSession = "All Program & Session";
                }
                else if (workOrderReport.ProgramSession.Contains("0"))
                {
                    programSession = "All Program & Session";
                }
                var purpose = "";
                if (workOrderReport.Purpose == null)
                {
                    purpose = "All Purposes";
                }
                else if (workOrderReport.Purpose.Where(x => (int)x == SelectionType.SelelectAll).Any())
                {
                    purpose = "All Purposes";
                }
                ViewBag.organizationName = _organizationService.LoadById(workOrderReport.OrganizationId).Name;
                ViewBag.branchName = workOrderReport.BranchId != null && !workOrderReport.BranchId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _branchService.LoadBranch(workOrderReport.BranchId).Select(x => x.Name).ToList()) : "All Udvash Branch";
                ViewBag.workOrderType = workOrderReport.WorkOrderType != SelectionType.SelelectAll ? _commonHelper.GetEmumIdToValue<WorkOrderType>(workOrderReport.WorkOrderType) : "All Type";
                ViewBag.itemGroup = workOrderReport.ItemGroupId != null && !workOrderReport.ItemGroupId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _itemGroupService.LoadItemGroup(workOrderReport.ItemGroupId.ToList()).Select(x => x.Name).ToList()) : "All Item Groups";
                ViewBag.item = workOrderReport.ItemId != null && !workOrderReport.ItemId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _itemService.LoadItemList(workOrderReport.ItemId).Select(x => x.Name).ToList()) : "All Items";
                ViewBag.workOrderStatus = workOrderReport.WorkOrderStatus != SelectionType.SelelectAll ? _commonHelper.GetEmumIdToValue<WorkOrderStatus>(workOrderReport.WorkOrderStatus) : "All Status";
                ViewBag.dateFrom = workOrderReport.DateFrom.ToString("yyyy-MM-dd");
                ViewBag.dateTo = workOrderReport.DateTo.ToString("yyyy-MM-dd");
                IList<long> programIdList = new List<long>();
                IList<long> sessionIdList = new List<long>();
                if (workOrderReport.ProgramSession != null)
                {
                    foreach (var programsession in workOrderReport.ProgramSession.Where(x => x != "0" && x != "-1"))
                    {
                        string[] programSessionArray = programsession.Split(new[] { "::" }, StringSplitOptions.None);
                        Program program = _programService.GetProgram(Convert.ToInt64(programSessionArray[0]));
                        Session session = _sessionService.LoadById(Convert.ToInt64(programSessionArray[1]));
                        programIdList.Add(program.Id);
                        sessionIdList.Add(session.Id);
                        if (!workOrderReport.ProgramSession.Contains("0"))
                        {
                            string programName = program.Name;
                            string sessionName = session.Name;
                            string programSessionName = programName + " & " + sessionName + ", ";
                            programSession += programSessionName;
                        }
                    }
                    if (workOrderReport.ProgramSession.Contains("-1"))
                    {
                        programIdList.Add(-1);
                        sessionIdList.Add(-1);
                        if (!workOrderReport.ProgramSession.Contains("0"))
                        {
                            programSession += " N/A";
                        }
                    }
                    programSession = programSession.Trim(',', ' ', ',');
                    ViewBag.ProgramIdList = programIdList;
                    ViewBag.SessionIdList = sessionIdList;
                }
                if (workOrderReport.Purpose != null)
                {
                    if (!workOrderReport.Purpose.Where(x => (int)x == SelectionType.SelelectAll).Any())
                    {
                        foreach (var purposeValue in workOrderReport.Purpose)
                        {
                            purpose += _commonHelper.GetEmumIdToValue<Purpose>((int)purposeValue) + ", ";
                        }
                        if (workOrderReport.Purpose.Where(x => (int)x == -1).Any())
                        {
                            purpose += " N/A";
                        }
                    }

                    purpose = purpose.Trim(' ', ',');
                }
                ViewBag.programSession = programSession;
                ViewBag.purpose = purpose;
                return PartialView("Partial/_WorkOrderReportPartialView", workOrderReport);
            }
            return View();
        }

        #region Render Data Table

        [HttpPost]
        public JsonResult WorkOrderReportAjaxRequest(int draw, int start, int length, long organizationId,
            List<long> branchIdList
            , int workOrderType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList
            , int workOrderstatus, string dateFrom, string dateTo)
        {
            var getData = new List<object>();
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime fromDate = Convert.ToDateTime(dateFrom);
                DateTime toDate = Convert.ToDateTime(dateTo);
                IList<WorkOrderReportDto> workOrderList =
                    _workOrderService.LoadWorkOrderReport(start, length, userMenu, organizationId, branchIdList,
                        workOrderType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, workOrderstatus,
                        fromDate, toDate).ToList();
                int recordsTotal = _workOrderService.GetWorkOrderReportCount(userMenu, organizationId, branchIdList,
                        workOrderType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, workOrderstatus,
                        fromDate, toDate);
                long recordsFiltered = recordsTotal;
                long i = start + 1;
                foreach (var wol in workOrderList)
                {
                    var str = new List<string>();
                    str.Add(i.ToString(CultureInfo.InvariantCulture));
                    str.Add(wol.Branch);
                    str.Add(wol.WorkOrderType);
                    str.Add(wol.ItemGroup);
                    str.Add(wol.ItemName);
                    str.Add(wol.ProgramSession);
                    str.Add(wol.WorkOrderDate.ToString("MMM dd, yyyy"));
                    str.Add(wol.WorkOrderRefNo);
                    str.Add(wol.WorkOrderStatus);
                    var workOrder = _workOrderService.GetWorkOrder(wol.WorkOrderId);
                    if (workOrder.Quotation == null)
                    {
                        str.Add("<a href='" + Url.Action("Details", "DirectWorkOrder") + "?Id=" + wol.WorkOrderId +
                                "' class='glyphicon glyphicon-th-list'></a>");
                    }
                    else
                    {
                        str.Add("<a href='" + Url.Action("Details", "Quotation") + "?Id=" + workOrder.Quotation.Id +
                                "' class='glyphicon glyphicon-th-list'></a>");
                    }
                    getData.Add(str);
                    i = i + 1;
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData
                });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                throw ex;
            }
        }

        #endregion

        #region Export

        public ActionResult ExportWorkOrderReport(string organizationString, string dateString, string branchString, string workOrderTypeString, string purposeString, string itemGroupString, string itemNameString, string programSessionString, string workOrderStatusString, long organizationId,
            List<long> branchIdList
            , int workOrderType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList
            , int workOrderstatus, string dateFrom, string dateTo)
        {
            var excelList = new List<List<object>>();
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime fromDate = Convert.ToDateTime(dateFrom);
                DateTime toDate = Convert.ToDateTime(dateTo);
                List<string> headerList = new List<string>();
                headerList.Add(_organizationService.LoadById(organizationId).Name);
                headerList.Add("Work Order Report");
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                columnList.Add("Branch");
                columnList.Add("Work Order Type");
                columnList.Add("Item Group");
                columnList.Add("Item Name");
                columnList.Add("Program & Session");
                columnList.Add("Work Order Date");
                columnList.Add("Work Order Ref. No");
                columnList.Add("Status");
                int recordsTotal = _workOrderService.GetWorkOrderReportCount(userMenu, organizationId, branchIdList,
                        workOrderType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, workOrderstatus,
                        fromDate, toDate);
                IList<WorkOrderReportDto> workOrderList =
                    _workOrderService.LoadWorkOrderReport(0, recordsTotal, userMenu, organizationId, branchIdList,
                        workOrderType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, workOrderstatus,
                        fromDate, toDate).ToList();
                foreach (var wol in workOrderList)
                {
                    var xlsRow = new List<object>();
                    xlsRow.Add(wol.Branch);
                    xlsRow.Add(wol.WorkOrderType);
                    xlsRow.Add(wol.ItemGroup);
                    xlsRow.Add(wol.ItemName);
                    xlsRow.Add(wol.ProgramSession);
                    xlsRow.Add(wol.WorkOrderDate.ToString("yyyy-MM-dd"));
                    xlsRow.Add(wol.WorkOrderRefNo);
                    xlsRow.Add(wol.WorkOrderStatus);
                    excelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, excelList, footerList, "Work_Order_Report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }
            catch (Exception ex)
            {
                //_logger.Error(ex.Message);
                return View("AutoClose");
            }
        }

        #endregion

        #endregion

        #region Ajax Helper

        public ActionResult LoadProgramAndSessionList(long organizationId, List<long> branchIdList, List<long> itemIdList)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //var programSessionList = _workOrderService.LoadProgramSessionList(authorizeMenu, organizationId, branchIdList, itemIdList);
                    var programSessionList =
                        _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList,
                            itemIdList).ToList();
                    var programSessionMultiSelectList = new MultiSelectList(programSessionList, "ProgramAndSessionId", "ProgramAndSessionName");
                    return Json(new { programSessionList = programSessionMultiSelectList, isSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        #endregion

    }
}