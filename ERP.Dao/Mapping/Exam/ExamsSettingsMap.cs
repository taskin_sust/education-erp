﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Exam;

namespace UdvashERP.Dao.Mapping.Exam
{
    public partial class ExamsSettingsMap : BaseClassMap<ExamsSettings, long>
    {
        public ExamsSettingsMap()
        {
            Table("ExamsSettings");
            LazyLoad();

            Map(x => x.Type).Column("Type");
            Map(x => x.KeyName).Column("KeyName");
            Map(x => x.Value).Column("Value");
            
        }
    }
}
