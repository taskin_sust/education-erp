﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using Constants = UdvashERP.BusinessRules.Constants;
using UdvashERP.BusinessModel.Dto;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class ProgramController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly IProgramService _programService;
        private readonly ICommonHelper _commonHelper;
        private readonly IBranchService _branchService;
        private readonly ISessionService _sessionService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly IUserService _userService;
        private readonly IOrganizationService _organizationService;
        private readonly IStudentExamService _studentExamService;
        private List<UserMenu> _userMenu;
        public ProgramController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _programService = new ProgramService(session);
                _commonHelper = new CommonHelper();
                _branchService = new BranchService(session);
                _sessionService = new SessionService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _userService = new UserService(session);
                _organizationService = new OrganizationService(session);
                _studentExamService = new StudentExamService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Index/Manage Page
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ManageProgram()
        {
            try
            {
                ViewBag.Organization = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
                //_userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //ViewBag.Name = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
                ViewBag.ProgramTypeList = _programService.GetProgramType();
                ViewBag.StatusText = _commonHelper.GetStatus();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View();
            }
        }

        #region Render Data Table
        public ActionResult ProgramList(int draw, int start, int length, string organization, string programName, string code, string shortName, string type, string status, string rank)
        {
            if (!Request.IsAjaxRequest()) return HttpNotFound();
            try
            {
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                #region OrderBy and Direction
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "organization.Name";
                            break;
                        case "1":
                            orderBy = "Name";
                            break;
                        case "2":
                            orderBy = "ShortName";
                            break;
                        case "3":
                            orderBy = "Code";
                            break;
                        case "4":
                            orderBy = "Type";
                            break;
                        case "5":
                            orderBy = "Rank";
                            break;
                        case "6":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                #endregion
                var program = new Program();
                int recordsTotal = _programService.GetProgramCount(orderBy, orderDir.ToUpper(), organization, programName, code, shortName, type, status, rank);
                long recordsFiltered = recordsTotal;
                List<Program> programList = _programService.LoadProgram(start, length, orderBy, orderDir.ToUpper(), organization, programName, code, shortName, type, status, rank).ToList();
                var maxMinRank = new int[2];
                maxMinRank[0] = _programService.GetMinimumRank(program);//1;
                maxMinRank[1] = _programService.GetMaximumRank(program);

                var data = new List<object>();
                foreach (var programObj in programList)
                {
                    var str = new List<string>();
                    str.Add(programObj.Organization != null ? programObj.Organization.ShortName : "");
                    str.Add(programObj.Name);
                    str.Add(!string.IsNullOrEmpty(programObj.ShortName) ? programObj.ShortName : "");
                    str.Add(programObj.Code);
                    if (programObj.Type == 1)
                    {
                        str.Add("Paid");
                    }
                    if (programObj.Type == 2)
                    {
                        str.Add("UnPaid");
                    }
                    if (programObj.Type == 3)
                    {
                        str.Add("CSR");
                    }
                    string rankbtn = programObj.Rank.ToString();
                    if (programObj.Rank > maxMinRank[0])
                    {
                        rankbtn += "<a href='#'  id='" + programObj.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    }
                    if (programObj.Rank < maxMinRank[1])
                    {
                        rankbtn += "<a href='#'  id='" + programObj.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    }
                    str.Add(rankbtn);
                    str.Add(_commonHelper.GetSatus(programObj.Status.ToString()));
                    str.Add("<a href='" + Url.Action("Details", "Program") + "?id=" + programObj.Id + "' data-id='" + programObj.Id + "' class='glyphicon glyphicon-th-list'></a>" +
                            "&nbsp;&nbsp; <a href='" + Url.Action("Edit", "Program") + "?id=" + programObj.Id + "' data-id='" + programObj.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; " +
                            "<a href='#' id='" + programObj.Id + "' data-name = '" + programObj.Name + "' data-id='" + programObj.Id + "' class='glyphicon glyphicon-trash'></a>");
                    data.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });

            }
            catch (Exception ex)
            {
                var data = new List<object>();
                _logger.Error(ex);
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });
            }
        }
        #endregion
        #endregion

        #region Operational Function

        #region Save Operation
        [HttpGet]
        public ActionResult AddProgram()
        {
            var organizations = _organizationService.LoadOrganization(Organization.EntityStatus.Active);
            ViewBag.organizationList = new SelectList(organizations.ToList(), "Id", "ShortName");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProgram(Program program)
        {
            try
            {
                var organizations = _organizationService.LoadOrganization(Organization.EntityStatus.Active);
                ViewBag.organizationList = new SelectList(organizations.ToList(), "Id", "ShortName");
                program.Organization = _organizationService.LoadById(program.Organization.OrganizationReference);
                _programService.ProgramSaveOrUpdate(program);

                ViewBag.SuccessMessage = "Program Add Successfully";
                return View();

                //ViewBag.ErrorMessage = "Program Add Failed";
                //return View();
            }
            catch (NullObjectException deex)
            {
                ViewBag.ErrorMessage = deex.Message;
                return View();
            }
            catch (InvalidDataException deex)
            {
                ViewBag.ErrorMessage = deex.Message;
                return View();
            }
            catch (DuplicateEntryException deex)
            {
                ViewBag.ErrorMessage = deex.Message;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View();
            }

        }
        #endregion

        #region Update Operation
        [HttpGet]
        public ActionResult Edit(long id)
        {
            try
            {
                long pId;
                bool isNum = long.TryParse(id.ToString(), out pId);
                if (!isNum)
                {
                    return HttpNotFound();
                }
                var p = _programService.GetProgram(id);
                var statusList = new SelectList(_commonHelper.GetStatus(), "Key", "Value");
                var programType = new SelectList(_programService.GetProgramType(), "Key", "Value");
                ViewBag.organizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(), "Id", "ShortName");
                var programStatus = (from KeyValuePair<string, int> statusKeyValuePair in statusList.Items
                                     select statusKeyValuePair.Value.ToString().Trim().Equals(p.Status.ToString()) ? new SelectListItem()
                                     {
                                         Text = statusKeyValuePair.Key,
                                         Value = statusKeyValuePair.Value.ToString(),
                                         Selected = true
                                     } : new SelectListItem()
                                     {
                                         Text = statusKeyValuePair.Key,
                                         Value = statusKeyValuePair.Value.ToString()
                                     }).ToList();

                var programTypeList = (from KeyValuePair<string, int> programTypeKeyValuePair in programType.Items
                                       select programTypeKeyValuePair.Value.ToString().Trim().Equals(p.Type.ToString()) ? new SelectListItem()
                                       {
                                           Text = programTypeKeyValuePair.Key,
                                           Value = programTypeKeyValuePair.Value.ToString(),
                                           Selected = true
                                       } : new SelectListItem()
                                       {
                                           Text = programTypeKeyValuePair.Key,
                                           Value = programTypeKeyValuePair.Value.ToString()
                                       }).ToList();
                ViewBag.Status = programStatus;
                //ViewBag.ProgramType = programTypeList;
                ViewBag.Type = programTypeList;
                ViewBag.SelectedOrganization = p.Organization != null
                    ? p.Organization.Id.ToString()
                    : "";
                return View(p);
            }
            catch (Exception ex)
            {
                var p = _programService.GetProgram(id);
                var statusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                var programType = new SelectList(_programService.GetProgramType(), "Value", "Key");
                /*View Error Message*/
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                ViewBag.Status = statusList;
                ViewBag.ProgramType = programType;
                _logger.Error(ex);
                return View(p);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        //public ActionResult Edit(Program program, int programType, int status)
        public ActionResult Edit(Program program)
        {
            try
            {
                //var oldProgram = _programService.GetProgram(program.Id);
                //var isSuccessfullyUpdated = _programService.UpdateProgram(oldProgram, program, programType, status);
                program.Organization = _organizationService.LoadById(program.Organization.OrganizationReference);
                _programService.ProgramSaveOrUpdate(program);

                //var programStatus = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                //var programTypeList = new SelectList(_programService.GetProgramType(), "Value", "Key");
                //ViewBag.Status = programStatus;
                //ViewBag.ProgramType = programTypeList;
                //if (isSuccessfullyUpdated)
                //{
                //    ViewBag.SuccessMessage = "Update Successfully";
                //    ViewBag.organizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(), "Id", "ShortName", program.Organization.Id.ToString());
                //    return View();
                //}
                ViewBag.SuccessMessage = "Program successfully updated.";
                ViewBag.organizationList =
                    new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(),
                        "Id", "ShortName", program.Organization.Id.ToString());
                return View();
            }
            catch (NullObjectException dex)
            {
                ViewBag.ErrorMessage = dex.Message;
                //var programStatus = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                //var programTypeList = new SelectList(_programService.GetProgramType(), "Value", "Key");
                //ViewBag.Status = programStatus;
                //ViewBag.ProgramType = programTypeList;
                //var oldProgram = _programService.GetProgram(program.Id);
                //ViewBag.organizationList =new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(),"Id", "ShortName", oldProgram.Organization.Id.ToString());
                //return View();
            }
            catch (InvalidDataException dex)
            {
                ViewBag.ErrorMessage = dex.Message;

                //var programStatus = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                //var programTypeList = new SelectList(_programService.GetProgramType(), "Value", "Key");
                //ViewBag.Status = programStatus;
                //ViewBag.ProgramType = programTypeList;
                //var oldProgram = _programService.GetProgram(program.Id);
                //ViewBag.organizationList =new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(),"Id", "ShortName", oldProgram.Organization.Id.ToString());
                //return View();
            }
            catch (DuplicateEntryException dex)
            {
                ViewBag.ErrorMessage = dex.Message;

                //var programStatus = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                //var programTypeList = new SelectList(_programService.GetProgramType(), "Value", "Key");
                //ViewBag.Status = programStatus;
                //ViewBag.ProgramType = programTypeList;
                //var oldProgram = _programService.GetProgram(program.Id);
                //ViewBag.organizationList =new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(),"Id", "ShortName", oldProgram.Organization.Id.ToString());
                //return View();
            }
            catch (DependencyException dex)
            {
                ViewBag.ErrorMessage = dex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Not Updated";
                _logger.Error(ex);
                var oldProgram = _programService.GetProgram(program.Id);
                ViewBag.organizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(), "Id", "ShortName", oldProgram.Organization.Id.ToString());
                return View();
            }
            finally
            {
                var programStatus = new SelectList(_commonHelper.GetStatus(), "Value", "Key", program.Status);
                var programTypeList = new SelectList(_programService.GetProgramType(), "Value", "Key", program.Type);
                ViewBag.Status = programStatus;
                ViewBag.Type = programTypeList;
                var oldProgram = _programService.GetProgram(program.Id);
                ViewBag.organizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(), "Id", "ShortName", oldProgram.Organization.Id.ToString());

            }
            return View();
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                var programObj = _programService.GetProgram(id);
                _programService.Delete(programObj);
                return Json(new Response(true, "Program Delete Successfully !"));
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "program Delete Fail !"));
            }
        }
        #endregion

        #region Details Operation
        [HttpGet]
        public ActionResult Details(long id)
        {
            try
            {
                long pId;
                bool isNum = long.TryParse(id.ToString(), out pId);
                if (!isNum)
                {
                    return HttpNotFound();
                }
                var program = _programService.GetProgram(id);
                if (program != null)
                {
                    program.CreateByText = _userService.GetUserNameByAspNetUserId(program.CreateBy);
                    program.ModifyByText = _userService.GetUserNameByAspNetUserId(program.ModifyBy);
                }
                return View(program);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Details View Fails because of " + ex.Message;
                _logger.Error(ex);
                return View();
            }

        }
        #endregion

        #region Rank Operation
        public ActionResult RankChange(int id, int current, string action)
        {
            #region OLD
            //if (!Request.IsAjaxRequest()) return HttpNotFound();
            //try
            //{
            //    Program programUpdateObj = _programService.GetProgram(Convert.ToInt64(id));
            //    if (programUpdateObj == null)
            //    {
            //        return HttpNotFound();
            //    }
            //    int newRank;
            //    if (action == "up")
            //        newRank = programUpdateObj.Rank - 1;
            //    else
            //        newRank = programUpdateObj.Rank + 1;

            //    var programGroupOldObj = _programService.GetProgramByRankNextOrPrevious(newRank, action);//_programService.LoadByRank(newRank);
            //    newRank = programGroupOldObj.Rank;

            //    programGroupOldObj.Rank = programUpdateObj.Rank;
            //    programUpdateObj.Rank = newRank;

            //    bool isSuccessfullyUpdateRank = _programService.UpdateRank(programGroupOldObj, programUpdateObj);
            //    if (isSuccessfullyUpdateRank)
            //    {
            //        return Json(new Response(true, "Successfully changed"));
            //    }
            //    return Json(new Response(false, "Something Wrong Occured!!"));
            //}
            //catch (Exception ex)
            //{
            //    _logger.Error(ex);
            //    return Json(new Response(false, "Something Wrong Occured!!"));
            //}
            #endregion

            if (!Request.IsAjaxRequest())
                return HttpNotFound();
            try
            {
                Program existingProgram = _programService.GetProgram(Convert.ToInt64(id));
                if (existingProgram == null)
                {
                    return HttpNotFound();
                }
                bool isSuccess = _programService.UpdateRank(existingProgram, action);
                if (isSuccess)
                    return Json(new Response(true, "Successfully changed"));
            }
            catch (NullObjectException ex)
            {
                return Json(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new Response(false, "Something Wrong Occured!!"));
        }
        #endregion
        #endregion

        #region Assign Program Operation
        [HttpGet]
        public ActionResult AssignProgram()
        {
            try
            {
                var organizationlist = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
                var sessionList = new SelectList(_sessionService.LoadSession(), "Id", "Name");
                var studentExamList = _studentExamService.LoadBoardExam(true);

                ViewBag.organization = organizationlist;
                ViewBag.sesion = sessionList;
                ViewBag.studentExamList = studentExamList;
                return View();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
        }

        [HttpPost]
        public ActionResult AssignProgramPost(long organizedId, long programId, long sessionId, long[] selectedBranchList, int?[] selectedAdmissionTypeList, int?[] selectedStudentExamList)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    List<long> nonDeletableBranchIds;
                    List<ResponseMessage> messageList = _programBranchSessionService.AssignProgram(organizedId,
                        programId, sessionId, selectedBranchList, selectedAdmissionTypeList, selectedStudentExamList,
                        out nonDeletableBranchIds);
                    return Json(new {messageList = messageList, nonDeletableBranchIds = nonDeletableBranchIds});
                }
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
           
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }
        #endregion

        #region Helper Function

        [HttpPost]
        public ActionResult LoadBranchList(long organizatonId, string viewName, long? sessionId = null, long? programId = null, int?[] admissionType = null)
        {
            ViewBag.BranchByOrganization = _branchService.LoadBranch(_commonHelper.ConvertIdToList(organizatonId)).ToList();
            if (sessionId != null && programId != null)
            {
                ViewBag.BranchByProgramSession = admissionType == null ? _branchService.LoadBranch(_commonHelper.ConvertIdToList(organizatonId), _commonHelper.ConvertIdToList(Convert.ToInt64(programId)), _commonHelper.ConvertIdToList(Convert.ToInt64(sessionId)), null, new List<int?>() { 0 }).ToList() : _branchService.LoadBranch(_commonHelper.ConvertIdToList(organizatonId), _commonHelper.ConvertIdToList(Convert.ToInt64(programId)), _commonHelper.ConvertIdToList(Convert.ToInt64(sessionId)), null, admissionType.ToList()).ToList();
            }

            return PartialView("Partial/" + viewName);
        }

        public JsonResult LoadProgramBoardSession(long programId, long sessionId)
        {
            try
            {
                var progamStudentExamSession = _programBranchSessionService.LoadProgramStudentExamSession(programId, sessionId);
                var progamStudentExamSessionSelectList = new SelectList(progamStudentExamSession.ToList(), "StudentExam.Id", "StudentExam.Id");
                return Json(new { progamStudentExamSessionSelectList = progamStudentExamSessionSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Board Info";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion
    }
}