﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_code;
using UdvashERP.Areas.Student.Models;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Students;
using UdvashERP.Services.Teachers;
using UdvashERP.App_Start;
using UdvashERP.Services.UserAuth;
using WebGrease.Activities;
using WebGrease.Css.Extensions;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class AttendanceController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly IProgramService _programService;
        private readonly IBranchService _branchService;
        private readonly IBatchService _batchService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ISubjectService _subjectService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IStudentClassAttendanceService _classAttendanceService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ICampusService _campusService;
        private readonly IExamsService _examService;
        private readonly IStudentExamAttendanceService _studentExamAttendanceService;
        private readonly IStudentClassAttendanceService _studentClassAttendanceService;
        private readonly IOrganizationService _organizationService;
        private readonly ILectureService _lectureService;
        private readonly ITeacherService _teacherService;
        private readonly IInformationService _informationService;
        private readonly IComplementaryCourseService _complementaryCourseService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserService _userService;
        private List<UserMenu> _userMenu;
        private readonly IStudentImagesMediaService _studentImagesMediaService;
        // GET: Student/Attendance
        public AttendanceController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();

                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _batchService = new BatchService(session);
                _sessionService = new SessionService(session);
                _classAttendanceService = new StudentClassAttendanceService(session);
                _courseService = new CourseService(session);
                _subjectService = new SubjectService(session);
                _studentProgramService = new StudentProgramService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _campusService = new CampusService(session);
                _examService = new ExamsService(session);
                _studentExamAttendanceService = new StudentExamAttendanceService(session);
                _studentClassAttendanceService = new StudentClassAttendanceService(session);
                _organizationService = new OrganizationService(session);
                _lectureService = new LectureService(session);
                _teacherService = new TeacherService(session);
                _informationService = new InformationService(session);
                _commonHelper = new CommonHelper();
                _studentImagesMediaService= new StudentImagesMediaService();
                _complementaryCourseService=new ComplementaryCourseService(session);
                _userService=new UserService(session);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;

                _logger.Error(ex);
            }

        }

        #endregion

        #region Attendance Report Selection
        [HttpGet]
        public ActionResult AttendanceReport()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                AttendanceSearchInfo attendanceSearchInfo = new AttendanceSearchInfo();

                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                ViewBag.Program = new SelectList(_programService.LoadAuthorizedProgram(userMenu), "Id", "Name");

                //Session
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");

                IList<Branch> branchList = new List<Branch>();
                ViewBag.SelectedBranch = new SelectList(branchList, "Id", "Name", SelectionType.SelelectAll);

                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedLecture = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedPaymentStatus = new SelectList(new List<SelectListItem>()
                                                               {
                                                                   new SelectListItem() {Value = SelectionType.SelelectAll.ToString(), Text = "All"},
                                                                   new SelectListItem() {Value = PaymentStatus.FullPaid.ToString(), Text = "Full Paid"},
                                                                   new SelectListItem() {Value = PaymentStatus.PaymentDue.ToString(), Text = "Payment Due"}
                                                                   
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());

                attendanceSearchInfo.AttendanceTypeList = new List<SelectListItem>()
                                                          {
                                                              //new SelectListItem() {Value = SelectionType.SelelectAll.ToString(), Text = "All"},
                                                              new SelectListItem() {Value = AttendanceType.ClassAttendance.ToString(), Text = "Class Attendance"}
                                                              //,
                                                              //new SelectListItem() {Value = AttendanceType.ExamAttendance.ToString(), Text = "Exam Attendance"}
                                                          };

                attendanceSearchInfo.AttendenseStatusList = new List<SelectListItem>()
                                                          {
                                                              //new SelectListItem() {Value = SelectionType.SelelectAll.ToString(), Text = "All"},
                                                              new SelectListItem() {Value = SelectionType.SelelectAll.ToString(), Text = "All"},
                                                              new SelectListItem() {Value = AttendanceStatus.Present.ToString(), Text = "Present"},
                                                              new SelectListItem() {Value =AttendanceStatus.Absent.ToString(), Text = "Absent"},
                                                              new SelectListItem() {Value =AttendanceStatus.NotEnrolled.ToString() , Text = "Not Enrolled"}
                                                          };

                return View(attendanceSearchInfo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                return null;
            }
        }
        public ActionResult ExamAttendanceReport()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                AttendanceSearchInfo attendanceSearchInfo = new AttendanceSearchInfo();

                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                ViewBag.Program = new SelectList(_programService.LoadAuthorizedProgram(userMenu), "Id", "Name");

                //Session
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");

                IList<Branch> branchList = new List<Branch>();
                ViewBag.SelectedBranch = new SelectList(branchList, "Id", "Name", SelectionType.SelelectAll);

                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
                ViewBag.SelectedPaymentStatus = new SelectList(new List<SelectListItem>()
                                                               {
                                                                   new SelectListItem() {Value = SelectionType.SelelectAll.ToString(), Text = "All"},
                                                                   new SelectListItem() {Value = PaymentStatus.FullPaid.ToString(), Text = "Full Paid"},
                                                                   new SelectListItem() {Value = PaymentStatus.PaymentDue.ToString(), Text = "Payment Due"}
                                                                   
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());

                attendanceSearchInfo.AttendanceTypeList = new List<SelectListItem>()
                                                          {
                                                              //new SelectListItem() {Value = SelectionType.SelelectAll.ToString(), Text = "All"},
                                                              new SelectListItem() {Value = AttendanceType.ClassAttendance.ToString(), Text = "Class Attendance"},
                                                              new SelectListItem() {Value = AttendanceType.ExamAttendance.ToString(), Text = "Exam Attendance"}
                                                          };

                attendanceSearchInfo.AttendenseStatusList = new List<SelectListItem>()
                                                          {
                                                              //new SelectListItem() {Value = SelectionType.SelelectAll.ToString(), Text = "All"},
                                                              new SelectListItem() {Value = SelectionType.SelelectAll.ToString(), Text = "All"},
                                                              new SelectListItem() {Value = AttendanceStatus.Present.ToString(), Text = "Present"},
                                                              new SelectListItem() {Value =AttendanceStatus.Absent.ToString(), Text = "Absent"},
                                                              new SelectListItem() {Value =AttendanceStatus.NotEnrolled.ToString() , Text = "Not Enrolled"}
                                                          };

                return View(attendanceSearchInfo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                return null;
            }
        }
        #endregion

        #region Class Attendance
        public ActionResult ClassAttendance()
        {
            try
            {
                ViewBag.lastIdEntered = "";
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.organizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult ClassAttendance(long programId,long lectureId, long teacherId, string heldDate,string classStart,string teacherLate, string[] studentRoll, string[] studentFeedback)
        {

            try
            {
                if (Request.IsAjaxRequest())
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var successRollsList = new List<string>();
                    List<ResponseMessage> messageLists = _classAttendanceService.Save(_userMenu,programId, lectureId,
                        teacherId,Convert.ToDateTime(heldDate),Convert.ToDateTime(classStart),teacherLate, studentRoll, studentFeedback, out successRollsList);
                    return Json(new { messageList = messageLists, successRolls = successRollsList });
                }
                else
                {
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));

            }

        }
        #endregion

        #region Class Attendance Reports

        [HttpPost]
        public ActionResult AttendanceReportList(int draw, int start, int length,
            AttendanceSearchInfo attendanceSerachInfo, DateTime startDate, DateTime endDate)
        {
           try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var batchIds = string.Join(",", attendanceSerachInfo.SelectedBatch);
                var lectureIds = string.Join(",", attendanceSerachInfo.SelectedLecture);
                var attendanceListResult = new List<StudentIndividualAttendanceReportViewModel>();
                int recordsTotal = _studentProgramService.GetStudentProgramCount( _userMenu,
                    attendanceSerachInfo.ProgramId, attendanceSerachInfo.SessionId,
                    attendanceSerachInfo.SelectedBranch
                    , attendanceSerachInfo.SelectedCampus, attendanceSerachInfo.SelectedBatchDays,
                    attendanceSerachInfo.SelectedBatchTime, attendanceSerachInfo.SelectedBatch,
                    attendanceSerachInfo.SelectedPaymentStatus, attendanceSerachInfo.AttendanceStatus, startDate,
                    endDate, batchIds, attendanceSerachInfo.SelectedCourse, lectureIds);
                int recordsFiltered = recordsTotal;
                var attendanceList =
                    _studentClassAttendanceService.LoadClassAttendanceReport(start, length, _userMenu, attendanceSerachInfo.ProgramId, attendanceSerachInfo.SessionId, attendanceSerachInfo.SelectedCourse,
                        startDate, endDate, batchIds, attendanceSerachInfo.SelectedPaymentStatus, attendanceSerachInfo.AttendanceStatus, lectureIds);
            var data = new List<object>();
            int sl = start + 1;
            foreach (var attendance in attendanceList)
            {
                var c =_studentProgramService.GetStudentProgram(attendance.StdProId);
                var str = new List<string>();
                str.Add(sl.ToString());

                        if (attendanceSerachInfo.InformationViewList != null)
                        {
                            foreach (string informationView in attendanceSerachInfo.InformationViewList)
                            {

                                switch (informationView)
                                {
                                    case "Program Roll":
                                        str.Add(c.PrnNo);
                                        break;
                                    case "Student's Name":
                                        if (c.Student != null && c.Student.FullName != null)
                                        {
                                            str.Add(c.Student.FullName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Nick Name":
                                        if (c.Student != null && c.Student.NickName != null)
                                        {
                                            str.Add(c.Student.NickName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Student)":
                                        if (c.Student != null && c.Student.Mobile != null)
                                        {
                                            str.Add(c.Student.Mobile);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Father)":
                                        if (c.Student != null && c.Student.GuardiansMobile1 != null)
                                        {
                                            str.Add(c.Student.GuardiansMobile1);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Mother)":
                                        if (c.Student != null && c.Student.GuardiansMobile2 != null)
                                        {
                                            str.Add(c.Student.GuardiansMobile2);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Father's Name":
                                        if (c.Student != null && c.Student.FatherName != null)
                                        {
                                            str.Add(c.Student.FatherName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Institute Name":
                                        if (c.Student.StudentPrograms[0].Institute != null)
                                        {
                                            str.Add(c.Student.StudentPrograms[0].Institute.Name);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Email":
                                        if (c.Student != null && c.Student.Email != null)
                                        {
                                            str.Add(c.Student.Email);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Branch":
                                        if (c.Batch != null && c.Batch.Branch != null)
                                        {
                                            str.Add(c.Batch.Branch.Name);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Batch":
                                        if (c.Batch != null && c.Batch.Name != null)
                                        {
                                            str.Add(c.Batch.Name);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Version of Study":
                                        if (c.VersionOfStudy != null)
                                        {
                                            str.Add(((VersionOfStudy)c.VersionOfStudy).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Gender":
                                        if (c.Student != null && c.Student.Gender != null)
                                        {
                                            str.Add(((Gender)c.Student.Gender).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Religion":
                                        if (c.Student != null && c.Student.Religion != null)
                                        {
                                            str.Add(((Religion)c.Student.Religion).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    default:
                                        break;
                                }
                            }
                        }
                        str.Add(attendance.LectureHeld.ToString());
                        str.Add(attendance.LectureAttend.ToString());
                        str.Add(attendance.RegisteredPresent.ToString());
                        str.Add(attendance.RegisteredAbsent.ToString());
                        str.Add(attendance.UnregisteredPresent.ToString());
                        str.Add("<a class='glyphicon glyphicon-search' target='_blank' href='IndividualReportView?programRoll=" + c.PrnNo + "&attendanceType=" + 1 + "&dateFrom=" + startDate + "&datetTo=" + endDate + "'></a>");
                        sl++;
                        data.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = new List<object>()
                    });
                }
               
           
           
        }

        [HttpGet]
        public ActionResult ExportAttendanceReport 
                (string programHeader, string sessionHeader, string branchHeader, string campusHeader,
                    string batchDayHeader, string batchHeader, string courseHeader
                    , string dateRangeHeader, string subjectHeader, long programId, long sessionId, long[] batchId,
                    string[] batchDays, string[] batchTime, long[] branchId,string batchIds,string lectureIds, long[] campusId, long[] courseId,
                    long[] paymentStatus
                    , long attendanceStatus, long attendanceType, string[] informationViewList, string dateFrom,
                    string dateTo, bool showError =
                false)
                {
                    try
                    {
                        var startDate = Convert.ToDateTime(dateFrom);
                        var endDate = Convert.ToDateTime(dateTo);
                        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                        var attendanceList = new List<StudentIndividualAttendanceReportViewModel>();
                        var attendanceListResult = new List<StudentIndividualAttendanceReportViewModel>();
                        var attListResult =
                            _studentClassAttendanceService.LoadClassAttendanceReport(0, 0, _userMenu, programId, sessionId, courseId,
                        startDate, endDate, batchIds, paymentStatus, attendanceStatus, lectureIds);
                        #region old code
                        //            var studentProgramIdList = new List<long>();
                        //if (studentProgramDtoList != null && studentProgramDtoList.Count > 0)
                        //{
                        //    studentProgramIdList = studentProgramDtoList.Select(x => x.Id).ToList();
                        //}

                        //IList<Course> courseList = new List<Course>();
                        //if (courseId.Contains(SelectionType.SelelectAll))
                        //{
                        //    courseList =
                        //        _courseService.LoadCourse(programId,
                        //            sessionId)
                        //            .Where(c => c.Status == StudentProgram.EntityStatus.Active)
                        //            .ToList();
                        //}
                        //else
                        //{
                        //    courseList = _courseService.LoadCourse(courseId);
                        //}
                        //if (studentProgramIdList.Count > 0)
                        //{
                        //    foreach (var studentProgramId in studentProgramIdList)
                        //    {
                        //        var studentProgram = _studentProgramService.GetStudentProgram(studentProgramId);
                        //        var individualAttendance = new StudentIndividualAttendanceReportViewModel();
                        //        var attendanceListByStudent =
                        //            studentProgram.StudentClassAttendanceDetails.Where(
                        //                x =>
                        //                    x.StudentClassAttendence.Lecture.LectureSettings.CourseSubject.Course.Id.In(
                        //                        courseList.Select(y => y.Id).ToArray()) &&
                        //                    x.StudentClassAttendence.HeldDate.Date >= startDate.Date &&
                        //                    x.StudentClassAttendence.HeldDate.Date <= endDate.AddDays(1).Date).ToList();
                        //        var registeredPresent = 0;
                        //        var registeredAbsent = 0;
                        //        var unregisteredPresent = 0;
                        //        var studentCourseDEtailsByDate = new List<StudentCourseDetail>();
                        //        if (attendanceListByStudent.Count > 0)
                        //        {
                        //            foreach (var attendance in attendanceListByStudent)
                        //            {
                        //                studentCourseDEtailsByDate =
                        //                    studentProgram.StudentCourseDetails.Where(
                        //                        x =>
                        //                            x.CourseSubject.CreationDate.Date <=
                        //                            attendance.StudentClassAttendence.HeldDate.Date)
                        //                        .ToList();
                        //                if (studentCourseDEtailsByDate.Count > 0)
                        //                {
                        //                    if (studentCourseDEtailsByDate.Select(x => x.CourseSubject.Id)
                        //                        .Contains(attendance.StudentClassAttendence.Lecture.LectureSettings.CourseSubject.Id))
                        //                    {
                        //                        registeredPresent++;
                        //                    }
                        //                    else
                        //                    {
                        //                        unregisteredPresent++;
                        //                    }
                        //                }
                        //            }
                        //        }

                        //        var lectureByCourseByDateByBatch =
                        //            studentProgram.Batch.CourseProgresses.Where(
                        //                x =>
                        //                    x.Lecture.LectureSettings.CourseSubject != null && x.Lecture.LectureSettings.CourseSubject.Course.Id.In(
                        //                        courseList.Select(y => y.Id).ToArray()) &&
                        //                    // CreationDate->held date
                        //                        x.HeldDate.Date >= startDate.Date &&
                        //                    // CreationDate->held date
                        //                        x.HeldDate.Date <= endDate.Date).Select(x => x.Lecture.Id)
                        //       .Distinct()
                        //       .ToArray().ToList();
                        //        lectureByCourseByDateByBatch.AddRange(studentProgram.Batch.CourseProgresses.Where(
                        //                 x =>
                        //                     x.Lecture.LectureSettings.CourseSubject == null &&
                        //                     // CreationDate->held date
                        //                         x.HeldDate.Date >= startDate.Date &&
                        //                     // CreationDate->held date
                        //                         x.HeldDate.Date <= endDate.Date).Select(x => x.Lecture.Id)
                        //        .Distinct()
                        //        .ToArray().ToList());
                        //        var registeredAbsentList = new List<string>();
                        //        foreach (var lectureInBatch in lectureByCourseByDateByBatch)
                        //        {
                        //            var studentPresence =
                        //                studentProgram.StudentClassAttendanceDetails.Where(
                        //                    x => x.StudentClassAttendence.HeldDate.Date >= startDate.Date && x.StudentClassAttendence.HeldDate.Date <= endDate.Date && x.StudentClassAttendence.Lecture.Id == lectureInBatch).ToList();
                        //            if (studentPresence.Count < 1)
                        //            {
                        //                var lecture = _lectureService.GetLecture(lectureInBatch);
                        //                if (lecture.LectureSettings.CourseSubject != null)
                        //                {
                        //                    var classDate = studentProgram.Batch.CourseProgresses.Where(
                        //                        x => x.Lecture.Id == lectureInBatch)
                        //                        //change CreationDate to heldDate
                        //                        .Select(x => x.HeldDate)
                        //                        .FirstOrDefault();
                        //                    if (studentProgram.StudentCourseDetails.Where(
                        //                        x =>
                        //                            x.CreationDate.Date <= classDate.Date &&
                        //                            x.Status == StudentCourseDetail.EntityStatus.Active)
                        //                        .Select(y => y.CourseSubject.Id)
                        //                        .ToArray()
                        //                        .Contains(lecture.LectureSettings.CourseSubject.Id))
                        //                    {
                        //                        registeredAbsent++;
                        //                    }
                        //                }
                        //                else
                        //                {
                        //                    registeredAbsent++;
                        //                }
                        //            }
                        //        }

                        //                individualAttendance.studentProgram = studentProgram;
                        //                individualAttendance.LectureHeld =
                        //                    //change CreationDate to heldDate
                        //                    studentProgram.Batch.CourseProgresses.Where(x => x.HeldDate.Date >= startDate.Date && x.HeldDate.Date <= endDate.Date).Select(y => y.Lecture).Distinct().ToArray().Length.ToString();
                        //                individualAttendance.LectureAttended = attendanceListByStudent.Count.ToString();
                        //                individualAttendance.RegisteredPresent = registeredPresent.ToString();
                        //                individualAttendance.RegisteredAbsent = registeredAbsent.ToString();

                        //                individualAttendance.UnregisteredPresent = unregisteredPresent.ToString();
                        //                attendanceList.Add(individualAttendance);
                        //            }
                        //        } 
                        #endregion
            var data = new List<object>();
            var headerList = new List<string>();
            headerList.Add(programHeader + " " + sessionHeader);
            headerList.Add("Attendance Report");
            headerList.Add("Branch : " + branchHeader);
            headerList.Add("Campus : " + campusHeader);
            headerList.Add("BatchDays : " + batchDayHeader);
            headerList.Add("Batch : " + batchHeader);
            // headerList.Add("Course Type : ");
            headerList.Add("Course : " + courseHeader);
            headerList.Add("Date Range : " + dateRangeHeader);
            headerList.Add("Subject : " + subjectHeader);
            headerList.Add("");
            var columnList = new List<string>();
            if (informationViewList != null)
            {
                if (informationViewList.Contains("Program Roll"))
                {
                    columnList.Add("Program Roll");
                }
                if (informationViewList.Contains("Student's Name"))
                {
                    columnList.Add("Full Name");
                }
                if (informationViewList.Contains("Nick Name"))
                {
                    columnList.Add("Nick Name");
                }
                if (informationViewList.Contains("Mobile Number (Student)"))
                {
                    columnList.Add("Mobile Number (Student)");
                }
                if (informationViewList.Contains("Mobile Number (Father)"))
                {
                    columnList.Add("Mobile Number (Father)");
                }
                if (informationViewList.Contains("Mobile Number (Mother)"))
                {
                    columnList.Add("Mobile Number (Mother)");
                }
                if (informationViewList.Contains("Father's Name"))
                {
                    columnList.Add("Father's Name");
                }
                if (informationViewList.Contains("Institute Name"))
                {
                    columnList.Add("Institute Name");
                }
                if (informationViewList.Contains("Email"))
                {
                    columnList.Add("Email");
                }
                if (informationViewList.Contains("Branch"))
                {
                    columnList.Add("Branch");
                }
                if (informationViewList.Contains("Batch"))
                {
                    columnList.Add("Batch");
                }
                if (informationViewList.Contains("Version of Study"))
                {
                    columnList.Add("Version of Study");
                }
                if (informationViewList.Contains("Gender"))
                {
                    columnList.Add("Gender");

                }
                if (informationViewList.Contains("Religion"))
                {
                    columnList.Add("Religion");
                }

            }
            columnList.Add("Lectures Held");
            columnList.Add("Lecture Attend");
            columnList.Add("Registered Present");
            columnList.Add("Registered Absent");
            columnList.Add("Unregistered Present");
            var footerList = new List<string>();
            footerList.Add("");
            //footerList.Add("Developed By OnnoRokom Software Ltd.");
            var studentExcelList = new List<List<object>>();
            foreach (var attendance in attListResult)
            {
                var xSes = _studentProgramService.GetStudentProgram(attendance.StdProId);
                    //attendance.studentProgram;
                var xlsRow = new List<object>();
                if (informationViewList != null)
                {
                    if (informationViewList.Contains("Program Roll"))
                    {
                        if (xSes.PrnNo != null)
                            xlsRow.Add(xSes.PrnNo);
                    }
                    if (informationViewList.Contains("Student's Name"))
                    {
                        xlsRow.Add(xSes.Student.FullName ?? "");
                    }
                    if (informationViewList.Contains("Nick Name"))
                    {
                        xlsRow.Add(xSes.Student.NickName ?? "");
                    }
                    if (informationViewList.Contains("Mobile Number (Student)"))
                    {
                        xlsRow.Add(xSes.Student.Mobile ?? "");
                    }
                    if (informationViewList.Contains("Mobile Number (Father)"))
                    {
                        xlsRow.Add(xSes.Student.GuardiansMobile1 ?? "");
                    }
                    if (informationViewList.Contains("Mobile Number (Mother)"))
                    {
                        xlsRow.Add(xSes.Student.GuardiansMobile2 ?? "");
                    }
                    if (informationViewList.Contains("Father's Name"))
                    {
                        xlsRow.Add(xSes.Student.FatherName ?? "");
                    }
                    if (informationViewList.Contains("Institute Name"))
                    {
                        if (xSes.Institute != null)
                            xlsRow.Add(xSes.Institute.Name ?? "");
                        else
                            xlsRow.Add("N/A");
                    }
                    if (informationViewList.Contains("Email"))
                    {
                        xlsRow.Add(xSes.Student.Email ?? "");
                    }
                    if (informationViewList.Contains("Branch"))
                    {
                        if (xSes.Batch != null)
                            xlsRow.Add(xSes.Batch.Branch.Name ?? "");
                    }
                    if (informationViewList.Contains("Batch"))
                    {
                        if (xSes.Batch != null)
                            xlsRow.Add(xSes.Batch.Name ?? "");
                    }
                    if (informationViewList.Contains("Version of Study"))
                    {
                        xlsRow.Add(xSes.VersionOfStudy != null ? ((VersionOfStudy)xSes.VersionOfStudy).ToString() : "--");
                    }
                    if (informationViewList.Contains("Gender"))
                    {
                        if (xSes.Student != null && xSes.Student.Gender != null)
                        {
                            xlsRow.Add(((Gender)xSes.Student.Gender).ToString());
                        }
                        else
                        {
                            xlsRow.Add("--");
                        }
                    }
                    if (informationViewList.Contains("Religion"))
                    {
                        if (xSes.Student != null && xSes.Student.Religion != null)
                        {
                            xlsRow.Add(((Religion)xSes.Student.Religion).ToString());
                        }
                        else
                        {
                            xlsRow.Add("--");
                        }
                    }
                }
                xlsRow.Add(attendance.LectureHeld);
                xlsRow.Add(attendance.LectureAttend);
                xlsRow.Add(attendance.RegisteredPresent);
                xlsRow.Add(attendance.RegisteredAbsent);
                xlsRow.Add(attendance.UnregisteredPresent);
                studentExcelList.Add(xlsRow);
            }
            ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "attendance-list-report__"
                + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

            return View("AutoClose");
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex.Message);
                        if (showError)
                            ViewBag.AutoCloseMsg = ex.ToString();
                        return View("AutoClose");
                    }
                   
                }
            

                #endregion

        #region Exam Attendance
            public
            ActionResult ExamAttendance()
        {
            try
            {
               ViewBag.lastIdEntered = "";
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.organizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult ExamAttendance(long programId,long examId, DateTime heldDate, string[] studentRoll, string[] studentFeedback)
        {
            
            try
            {
                if (Request.IsAjaxRequest())
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var successRollsList = new List<string>();
                    List<ResponseMessage> messageLists = _studentExamAttendanceService.Save(_userMenu, programId, examId, heldDate, studentRoll, studentFeedback, out successRollsList);
                    return Json(new { messageList = messageLists, successRolls = successRollsList });
                }
                else
                {
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));

            }
        }
        #endregion

        #region  Exam Attendance Reports
        [HttpPost]
        public ActionResult ExamAttendanceReportList(int draw, int start, int length,string batchIds, AttendanceSearchInfo attendanceSerachInfo, DateTime startDate, DateTime endDate)
        {
            try
            {
                #region prev code
                //_userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //var attendanceList = new List<StudentIndividualExamAttendanceReportViewModel>();
                //var attendanceListResult = new List<StudentIndividualExamAttendanceReportViewModel>();
                //int recordsTotal = 0;
                //long recordsFiltered = 0;
                //var studentProgramList =
                //        _studentProgramService.LoadStudentProgramByCriteriaForAttendance(start, length, _userMenu,
                //            attendanceSerachInfo.ProgramId, attendanceSerachInfo.SessionId,
                //            attendanceSerachInfo.SelectedBranch
                //            , attendanceSerachInfo.SelectedCampus, attendanceSerachInfo.SelectedBatchDays,
                //            attendanceSerachInfo.SelectedBatchTime, attendanceSerachInfo.SelectedBatch, attendanceSerachInfo.SelectedPaymentStatus, attendanceSerachInfo.AttendanceStatus);
                //IList<Course> courseList = new List<Course>();
                //if (attendanceSerachInfo.SelectedCourse.Contains(SelectionType.SelelectAll))
                //{
                //    courseList =
                //        _courseService.LoadCourse(attendanceSerachInfo.ProgramId,
                //            attendanceSerachInfo.SessionId)
                //            .Where(c => c.Status == StudentProgram.EntityStatus.Active)
                //            .ToList();
                //}
                //else
                //{
                //    courseList = _courseService.LoadCourse(attendanceSerachInfo.SelectedCourse);
                //}
                //if (studentProgramList.Count > 0)
                //{
                //    foreach (var studentProgram in studentProgramList)
                //    {
                //        var individualAttendance = new StudentIndividualExamAttendanceReportViewModel();
                //        var attendanceListByStudent =
                //            studentProgram.StudentExamAttendances.Where(
                //                x =>
                //                    x.Exams.Course.Id.In(
                //                        courseList.Select(y => y.Id).ToArray()) &&
                //                    x.HeldDate.Value.Date >= startDate.Date &&
                //                    x.HeldDate.Value.Date <= endDate.AddDays(1).Date).ToList();
                //        var registeredPresent = 0;
                //        var registeredAbsent = 0;
                //        var unregisteredPresent = 0;
                //        var studentCourseDEtailsByDate = new List<StudentCourseDetail>();
                //        if (attendanceListByStudent.Count > 0)
                //        {
                //            foreach (var attendance in attendanceListByStudent)
                //            {
                //                studentCourseDEtailsByDate =
                //                    studentProgram.StudentCourseDetails.Where(
                //                        x =>
                //                            x.CourseSubject.CreationDate.Date <= attendance.HeldDate.Value.Date)
                //                        .ToList();
                //                bool isRegistered = true;
                //                var courseId = attendance.Exams.Course.Id;
                //                var examSubjectIds = attendance.Exams.ExamDetails.Select(x => x.Subject.Id).ToList();
                //                var registeredSubjectIds =
                //                    studentCourseDEtailsByDate.Select(x => x.CourseSubject.Subject.Id).ToList();
                //                if (studentCourseDEtailsByDate.Select(x => x.CourseSubject.Course.Id).ToList().Contains(courseId))
                //                {
                //                    foreach (var examSubjectId in examSubjectIds)
                //                    {
                //                        if (!registeredSubjectIds.Contains(examSubjectId))
                //                        {
                //                            isRegistered = false;
                //                            break;
                //                        }
                //                    }
                //                }
                //                else
                //                {
                //                    isRegistered = false;
                //                }
                //                if (isRegistered)
                //                {
                //                    registeredPresent++;
                //                }
                //                else
                //                {
                //                    unregisteredPresent++;
                //                }
                //            }
                //        }
                //        var examByCourseByDateByBatch =
                //            studentProgram.Batch.StudentExamAttendances.Where(
                //                x =>
                //                    x.Exams.Course.Id.In(
                //                        courseList.Select(y => y.Id).ToArray()) &&
                //                    x.HeldDate.Value.Date >= startDate.Date &&
                //                    x.HeldDate.Value.Date <= endDate.AddDays(1).Date).Select(x => x.Exams.Id)
                //       .Distinct()
                //       .ToArray().ToList();
                //        var registeredAbsentList = new List<string>();
                //        foreach (var examInBatch in examByCourseByDateByBatch)
                //        {
                //            var studentPresence =
                //                studentProgram.StudentExamAttendances.Where(
                //                    x => x.HeldDate.Value.Date >= startDate.Date && x.HeldDate.Value.Date <= endDate.Date && x.Exams.Id == examInBatch).ToList();
                //            if (studentPresence.Count < 1)
                //            {
                //                var exam = _examService.LoadById(examInBatch);
                //                var examDate =
                //                    studentProgram.Batch.StudentExamAttendances.Where(
                //                        x => x.Exams.Id == examInBatch)
                //                        .Select(x => x.HeldDate)
                //                        .FirstOrDefault();

                //                bool isRegistered = true;
                //                var registeredCourseIds =
                //                    studentProgram.StudentCourseDetails.Where(
                //                        x => x.CourseSubject.CreationDate.Date <= examDate.Value.Date)
                //                        .Select(x => x.CourseSubject.Course.Id)
                //                        .ToList();
                //                var examcourseId = exam.Course.Id;
                //                var examSubjectIds = exam.ExamDetails.Select(x => x.Subject.Id).ToList();
                //                var registeredSubjectIds =
                //                    studentProgram.StudentCourseDetails.Where(x => x.CourseSubject.CreationDate.Date <= examDate.Value.Date).Select(x => x.CourseSubject.Subject.Id).ToList();
                //                if (registeredCourseIds.Contains(examcourseId))
                //                {
                //                    foreach (var examSubjectId in examSubjectIds)
                //                    {
                //                        if (!registeredSubjectIds.Contains(examSubjectId))
                //                        {
                //                            isRegistered = false;
                //                            break;
                //                        }
                //                    }
                //                }
                //                else
                //                {
                //                    isRegistered = false;
                //                }
                //                if (isRegistered)
                //                {
                //                    registeredAbsent++;
                //                }

                //            }
                //        }

                //        individualAttendance.studentProgram = studentProgram;
                //        individualAttendance.ExamHeld =
                //            studentProgram.Batch.StudentExamAttendances.Where(x => x.HeldDate.Value.Date >= startDate.Date && x.HeldDate.Value.Date <= endDate.Date).Select(y => y.Exams).Distinct().ToArray().Length.ToString();
                //        individualAttendance.ExamAttended = attendanceListByStudent.Count.ToString();
                //        individualAttendance.RegisteredPresent = registeredPresent.ToString();
                //        individualAttendance.RegisteredAbsent = registeredAbsent.ToString();
                //        individualAttendance.UnregisteredPresent = unregisteredPresent.ToString();
                //        attendanceList.Add(individualAttendance);
                //    }
                //}


                //if (attendanceSerachInfo.SelectedPaymentStatus.Contains(SelectionType.SelelectAll))
                //{
                //    attendanceListResult.AddRange(attendanceList);
                //}
                //else
                //{
                //    if (attendanceSerachInfo.SelectedPaymentStatus.Contains(PaymentStatus.FullPaid))
                //    {
                //        attendanceListResult.AddRange(
                //            attendanceList.Where(x => x.studentProgram.DueAmount == Convert.ToDecimal(0)));
                //    }
                //    if (attendanceSerachInfo.SelectedPaymentStatus.Contains(PaymentStatus.PaymentDue))
                //    {
                //        attendanceListResult.AddRange(
                //            attendanceList.Where(x => x.studentProgram.DueAmount > Convert.ToDecimal(0)));
                //    }
                //    if (attendanceSerachInfo.SelectedPaymentStatus.Contains(PaymentStatus.NotEnrolled))
                //    {
                //        attendanceListResult.AddRange(
                //            attendanceList.Where(x => Convert.ToInt32(x.UnregisteredPresent) > 0));
                //    }
                //}
                //attendanceListResult = attendanceListResult.DistinctBy(x => x.studentProgram.Id).ToList();
                //if (attendanceSerachInfo.AttendanceStatus == AttendanceStatus.Present)
                //{
                //    attendanceListResult =
                //        attendanceListResult.Where(
                //            x =>
                //                Convert.ToInt32(x.RegisteredPresent) > 0 ||
                //                Convert.ToInt32(x.UnregisteredPresent) > 0)
                //            .ToList();
                //}
                //if (attendanceSerachInfo.AttendanceStatus == AttendanceStatus.Absent)
                //{
                //    attendanceListResult =
                //        attendanceListResult.Where(
                //            x => Convert.ToInt32(x.RegisteredAbsent) > 0)
                //            .ToList();
                //}
                //if (!attendanceSerachInfo.SelectedPaymentStatus.Contains(3) && attendanceSerachInfo.AttendanceStatus == 0 && length != 0)
                //{
                //    recordsTotal = _studentProgramService.GetStudentProgramCountByCriteriaForAttendance(_userMenu, attendanceSerachInfo.ProgramId, attendanceSerachInfo.SessionId,
                //                      attendanceSerachInfo.SelectedBranch
                //                      , attendanceSerachInfo.SelectedCampus, attendanceSerachInfo.SelectedBatchDays,
                //                      attendanceSerachInfo.SelectedBatchTime, attendanceSerachInfo.SelectedBatch, attendanceSerachInfo.SelectedPaymentStatus, attendanceSerachInfo.AttendanceStatus);
                //    recordsFiltered = recordsTotal;
                //}
                //else
                //{
                //    recordsTotal = attendanceListResult.Count;
                //    recordsFiltered = recordsTotal;
                //    attendanceListResult = attendanceListResult.Skip(start).Take(length).ToList();
                //}

                //var data = new List<object>();
                //int sl = start + 1;
                //foreach (var attendance in attendanceListResult)
                //{
                //    var c = attendance.studentProgram;
                //    var str = new List<string>();
                //    str.Add(sl.ToString());

                //    if (attendanceSerachInfo.InformationViewList != null)
                //    {
                //        foreach (string informationView in attendanceSerachInfo.InformationViewList)
                //        {

                //            switch (informationView)
                //            {
                //                case "Program Roll":
                //                    str.Add(c.PrnNo);
                //                    break;
                //                case "Student's Name":
                //                    if (c.Student != null && c.Student.FullName != null)
                //                    {
                //                        str.Add(c.Student.FullName);
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Nick Name":
                //                    if (c.Student != null && c.Student.NickName != null)
                //                    {
                //                        str.Add(c.Student.NickName);
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Mobile Number (Student)":
                //                    if (c.Student != null && c.Student.Mobile != null)
                //                    {
                //                        str.Add(c.Student.Mobile);
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Mobile Number (Father)":
                //                    if (c.Student != null && c.Student.GuardiansMobile1 != null)
                //                    {
                //                        str.Add(c.Student.GuardiansMobile1);
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Mobile Number (Mother)":
                //                    if (c.Student != null && c.Student.GuardiansMobile2 != null)
                //                    {
                //                        str.Add(c.Student.GuardiansMobile2);
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Father's Name":
                //                    if (c.Student != null && c.Student.FatherName != null)
                //                    {
                //                        str.Add(c.Student.FatherName);
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Institute Name":
                //                    if (c.Student.StudentPrograms[0].Institute != null)
                //                    {
                //                        str.Add(c.Student.StudentPrograms[0].Institute.Name);
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Email":
                //                    if (c.Student != null && c.Student.Email != null)
                //                    {
                //                        str.Add(c.Student.Email);
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Branch":
                //                    if (c.Batch != null && c.Batch.Branch != null)
                //                    {
                //                        str.Add(c.Batch.Branch.Name);
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Batch":
                //                    if (c.Batch != null && c.Batch.Name != null)
                //                    {
                //                        str.Add(c.Batch.Name);
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Version of Study":
                //                    if (c.VersionOfStudy != null)
                //                    {
                //                        str.Add(((VersionOfStudy)c.VersionOfStudy).ToString());
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Gender":
                //                    if (c.Student != null && c.Student.Gender != null)
                //                    {
                //                        str.Add(((Gender)c.Student.Gender).ToString());
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                case "Religion":
                //                    if (c.Student != null && c.Student.Religion != null)
                //                    {
                //                        str.Add(((Religion)c.Student.Religion).ToString());
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        str.Add("");
                //                        break;
                //                    }
                //                default:
                //                    break;
                //            }
                //        }
                //    }
                //    str.Add(attendance.ExamHeld);
                //    str.Add(attendance.ExamAttended);
                //    str.Add(attendance.RegisteredPresent);
                //    str.Add(attendance.RegisteredAbsent);
                //    str.Add(attendance.UnregisteredPresent);
                //    str.Add("<a class='glyphicon glyphicon-search' target='_blank' href='IndividualReportView?programRoll=" + c.PrnNo + "&attendanceType=" + attendanceSerachInfo.AttendanceType + "&dateFrom=" + startDate + "&datetTo=" + endDate + "'></a>");
                //    sl++;
                //    data.Add(str);
                //}



                //return Json(new
                //{
                //    draw = draw,
                //    recordsTotal = recordsTotal,
                //    recordsFiltered = recordsFiltered,
                //    start = start,
                //    length = length,
                //    data = data
                //}); 
                #endregion

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //var attendanceList = new List<StudentIndividualAttendanceReportViewModel>();
                var attendanceListResult = new List<StudentIndividualAttendanceReportViewModel>();
                int recordsTotal = _studentExamAttendanceService.GetExamAttendanceCount(_userMenu,
                    attendanceSerachInfo.ProgramId, attendanceSerachInfo.SessionId,
                    attendanceSerachInfo.SelectedBranch
                    , attendanceSerachInfo.SelectedCampus, attendanceSerachInfo.SelectedBatchDays,
                    attendanceSerachInfo.SelectedBatchTime, attendanceSerachInfo.SelectedBatch,
                    attendanceSerachInfo.SelectedPaymentStatus, attendanceSerachInfo.AttendanceStatus, startDate,
                    endDate, batchIds, attendanceSerachInfo.SelectedCourse,attendanceSerachInfo.SelectedExam);
                int recordsFiltered = recordsTotal;
                var attendanceList =
                    _studentExamAttendanceService.LoadExamAttendanceReport(start, length, _userMenu, attendanceSerachInfo.ProgramId, attendanceSerachInfo.SessionId, attendanceSerachInfo.SelectedCourse,
                        startDate, endDate, batchIds, attendanceSerachInfo.SelectedPaymentStatus, attendanceSerachInfo.AttendanceStatus,attendanceSerachInfo.SelectedExam);
                var data = new List<object>();
                int sl = start + 1;
                foreach (var attendance in attendanceList)
                {
                    var c = _studentProgramService.GetStudentProgram(attendance.StdProId);
                    var str = new List<string>();
                    str.Add(sl.ToString());

                    if (attendanceSerachInfo.InformationViewList != null)
                    {
                        foreach (string informationView in attendanceSerachInfo.InformationViewList)
                        {

                            switch (informationView)
                            {
                                case "Program Roll":
                                    str.Add(c.PrnNo);
                                    break;
                                case "Student's Name":
                                    if (c.Student != null && c.Student.FullName != null)
                                    {
                                        str.Add(c.Student.FullName);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Nick Name":
                                    if (c.Student != null && c.Student.NickName != null)
                                    {
                                        str.Add(c.Student.NickName);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Mobile Number (Student)":
                                    if (c.Student != null && c.Student.Mobile != null)
                                    {
                                        str.Add(c.Student.Mobile);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Mobile Number (Father)":
                                    if (c.Student != null && c.Student.GuardiansMobile1 != null)
                                    {
                                        str.Add(c.Student.GuardiansMobile1);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Mobile Number (Mother)":
                                    if (c.Student != null && c.Student.GuardiansMobile2 != null)
                                    {
                                        str.Add(c.Student.GuardiansMobile2);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Father's Name":
                                    if (c.Student != null && c.Student.FatherName != null)
                                    {
                                        str.Add(c.Student.FatherName);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Institute Name":
                                    if (c.Student.StudentPrograms[0].Institute != null)
                                    {
                                        str.Add(c.Student.StudentPrograms[0].Institute.Name);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Email":
                                    if (c.Student != null && c.Student.Email != null)
                                    {
                                        str.Add(c.Student.Email);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Branch":
                                    if (c.Batch != null && c.Batch.Branch != null)
                                    {
                                        str.Add(c.Batch.Branch.Name);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Batch":
                                    if (c.Batch != null && c.Batch.Name != null)
                                    {
                                        str.Add(c.Batch.Name);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Version of Study":
                                    if (c.VersionOfStudy != null)
                                    {
                                        str.Add(((VersionOfStudy)c.VersionOfStudy).ToString());
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Gender":
                                    if (c.Student != null && c.Student.Gender != null)
                                    {
                                        str.Add(((Gender)c.Student.Gender).ToString());
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Religion":
                                    if (c.Student != null && c.Student.Religion != null)
                                    {
                                        str.Add(((Religion)c.Student.Religion).ToString());
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                default:
                                    break;
                            }
                        }
                    }
                    str.Add(attendance.LectureHeld.ToString());
                    str.Add(attendance.LectureAttend.ToString());
                    str.Add(attendance.RegisteredPresent.ToString());
                    str.Add(attendance.RegisteredAbsent.ToString());
                    str.Add(attendance.UnregisteredPresent.ToString());
                    str.Add("<a class='glyphicon glyphicon-search' target='_blank' href='IndividualReportView?programRoll=" + c.PrnNo + "&attendanceType=" + attendanceSerachInfo.AttendanceType + "&dateFrom=" + startDate + "&datetTo=" + endDate + "'></a>");
                    sl++;
                    data.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(false);
            }
        }

        public ActionResult ExportExamAttendanceReport(string programHeader, string sessionHeader, string branchHeader, string campusHeader,
                    string batchDayHeader, string batchHeader, string courseHeader
                    , string dateRangeHeader, string subjectHeader, long programId, long sessionId, long[] batchId,
                    string[] batchDays, string[] batchTime, long[] branchId, string batchIds, long[] campusId, long[] courseId,
                    long[] paymentStatus
                    , long attendanceStatus, long attendanceType, string[] informationViewList, string dateFrom,
                    string dateTo,long[]examIds, bool showError =
                false)
        {
            try
            {
                var startDate = Convert.ToDateTime(dateFrom);
                var endDate = Convert.ToDateTime(dateTo);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var attendanceList = new List<StudentIndividualAttendanceReportViewModel>();
                var attendanceListResult = new List<StudentIndividualAttendanceReportViewModel>();
                var attListResult =
                    _studentExamAttendanceService.LoadExamAttendanceReport(0, 0, _userMenu, programId, sessionId, courseId,
                startDate, endDate, batchIds, paymentStatus, attendanceStatus,examIds);
               
                var data = new List<object>();
                var headerList = new List<string>();
                headerList.Add(programHeader + " " + sessionHeader);
                headerList.Add("Exam Attendance Report");
                headerList.Add("Branch : " + branchHeader);
                headerList.Add("Campus : " + campusHeader);
                headerList.Add("BatchDays : " + batchDayHeader);
                headerList.Add("Batch : " + batchHeader);
                // headerList.Add("Course Type : ");
                headerList.Add("Course : " + courseHeader);
                headerList.Add("Date Range : " + dateRangeHeader);
                headerList.Add("Subject : " + subjectHeader);
                headerList.Add("");
                var columnList = new List<string>();
                if (informationViewList != null)
                {
                    if (informationViewList.Contains("Program Roll"))
                    {
                        columnList.Add("Program Roll");
                    }
                    if (informationViewList.Contains("Student's Name"))
                    {
                        columnList.Add("Full Name");
                    }
                    if (informationViewList.Contains("Nick Name"))
                    {
                        columnList.Add("Nick Name");
                    }
                    if (informationViewList.Contains("Mobile Number (Student)"))
                    {
                        columnList.Add("Mobile Number (Student)");
                    }
                    if (informationViewList.Contains("Mobile Number (Father)"))
                    {
                        columnList.Add("Mobile Number (Father)");
                    }
                    if (informationViewList.Contains("Mobile Number (Mother)"))
                    {
                        columnList.Add("Mobile Number (Mother)");
                    }
                    if (informationViewList.Contains("Father's Name"))
                    {
                        columnList.Add("Father's Name");
                    }
                    if (informationViewList.Contains("Institute Name"))
                    {
                        columnList.Add("Institute Name");
                    }
                    if (informationViewList.Contains("Email"))
                    {
                        columnList.Add("Email");
                    }
                    if (informationViewList.Contains("Branch"))
                    {
                        columnList.Add("Branch");
                    }
                    if (informationViewList.Contains("Batch"))
                    {
                        columnList.Add("Batch");
                    }
                    if (informationViewList.Contains("Version of Study"))
                    {
                        columnList.Add("Version of Study");
                    }
                    if (informationViewList.Contains("Gender"))
                    {
                        columnList.Add("Gender");

                    }
                    if (informationViewList.Contains("Religion"))
                    {
                        columnList.Add("Religion");
                    }

                }
                columnList.Add("Exam Held");
                columnList.Add("Exam Attend");
                columnList.Add("Registered Present");
                columnList.Add("Registered Absent");
                columnList.Add("Unregistered Present");
                var footerList = new List<string>();
                footerList.Add("");
                //footerList.Add("Developed By OnnoRokom Software Ltd.");
                var studentExcelList = new List<List<object>>();
                foreach (var attendance in attListResult)
                {
                    var xSes = _studentProgramService.GetStudentProgram(attendance.StdProId);
                    //attendance.studentProgram;
                    var xlsRow = new List<object>();
                    if (informationViewList != null)
                    {
                        if (informationViewList.Contains("Program Roll"))
                        {
                            if (xSes.PrnNo != null)
                                xlsRow.Add(xSes.PrnNo);
                        }
                        if (informationViewList.Contains("Student's Name"))
                        {
                            xlsRow.Add(xSes.Student.FullName ?? "");
                        }
                        if (informationViewList.Contains("Nick Name"))
                        {
                            xlsRow.Add(xSes.Student.NickName ?? "");
                        }
                        if (informationViewList.Contains("Mobile Number (Student)"))
                        {
                            xlsRow.Add(xSes.Student.Mobile ?? "");
                        }
                        if (informationViewList.Contains("Mobile Number (Father)"))
                        {
                            xlsRow.Add(xSes.Student.GuardiansMobile1 ?? "");
                        }
                        if (informationViewList.Contains("Mobile Number (Mother)"))
                        {
                            xlsRow.Add(xSes.Student.GuardiansMobile2 ?? "");
                        }
                        if (informationViewList.Contains("Father's Name"))
                        {
                            xlsRow.Add(xSes.Student.FatherName ?? "");
                        }
                        if (informationViewList.Contains("Institute Name"))
                        {
                            if (xSes.Institute != null)
                                xlsRow.Add(xSes.Institute.Name ?? "");
                            else
                                xlsRow.Add("N/A");
                        }
                        if (informationViewList.Contains("Email"))
                        {
                            xlsRow.Add(xSes.Student.Email ?? "");
                        }
                        if (informationViewList.Contains("Branch"))
                        {
                            if (xSes.Batch != null)
                                xlsRow.Add(xSes.Batch.Branch.Name ?? "");
                        }
                        if (informationViewList.Contains("Batch"))
                        {
                            if (xSes.Batch != null)
                                xlsRow.Add(xSes.Batch.Name ?? "");
                        }
                        if (informationViewList.Contains("Version of Study"))
                        {
                            xlsRow.Add(xSes.VersionOfStudy != null ? ((VersionOfStudy)xSes.VersionOfStudy).ToString() : "--");
                        }
                        if (informationViewList.Contains("Gender"))
                        {
                            if (xSes.Student != null && xSes.Student.Gender != null)
                            {
                                xlsRow.Add(((Gender)xSes.Student.Gender).ToString());
                            }
                            else
                            {
                                xlsRow.Add("--");
                            }
                        }
                        if (informationViewList.Contains("Religion"))
                        {
                            if (xSes.Student != null && xSes.Student.Religion != null)
                            {
                                xlsRow.Add(((Religion)xSes.Student.Religion).ToString());
                            }
                            else
                            {
                                xlsRow.Add("--");
                            }
                        }
                    }
                    xlsRow.Add(attendance.LectureHeld);
                    xlsRow.Add(attendance.LectureAttend);
                    xlsRow.Add(attendance.RegisteredPresent);
                    xlsRow.Add(attendance.RegisteredAbsent);
                    xlsRow.Add(attendance.UnregisteredPresent);
                    studentExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "exam_attendance-list-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (showError)
                    ViewBag.AutoCloseMsg = ex.ToString();
                return View("AutoClose");
            }
        }

       #endregion

        #region Individual Reports
        public ActionResult IndividualReport(string message = "", string roll = "")
        {
            try
            {
                if (!String.IsNullOrEmpty(message))
                {
                    ViewBag.ErrorMessage = message;
                    ViewBag.roll = roll;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpGet]
        public ActionResult IndividualReportView(string programRoll, int attendanceType, DateTime dateFrom, DateTime datetTo)
        {
            try
            {
                ViewBag.programRoll = programRoll;
                ViewBag.attendanceType = attendanceType;
                ViewBag.dateFrom = dateFrom;
                ViewBag.datetTo = datetTo;
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentProgram = _studentProgramService.GetStudentProgram(programRoll,true);
                if (studentProgram == null)
                {
                    return RedirectToAction("IndividualReport", new { message = "Student program roll not found", roll = programRoll });
                }
                var message = _studentProgramService.CheckAuthorizationForStudentProgram(_userMenu, studentProgram);
                if (message != "")
                    return RedirectToAction("IndividualReport", new { message = "You are not authorized to view this history", roll = programRoll });
                StudentMediaImage mediaStudentImages = new StudentMediaImage();
                if (studentProgram.StudentImage.Count > 0)
                {
                    mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(
                     studentProgram.StudentImage[0].Id);
                    studentProgram.MediaStudentImage = mediaStudentImages != null
                        ? mediaStudentImages
                        : new StudentMediaImage();
                }
                ViewBag.StudentProgram = studentProgram;
                if (attendanceType == AttendanceType.ClassAttendance)
                {
                    var individualReportResult = _studentClassAttendanceService.GetIndividualReport(programRoll, attendanceType, dateFrom, datetTo);
                    return View("IndividualReportView", individualReportResult.ToList());
                }
                else
                {
                    var individualReportResult =_studentExamAttendanceService.GetIndividualReport(programRoll, attendanceType, dateFrom, datetTo).DistinctBy(x=>x.ExamName).ToList();
                    return View("IndividualExamReportView", individualReportResult.ToList());
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("IndividualReport", new { message = WebHelper.CommonErrorMessage, roll = programRoll });
            }
        }

        [HttpGet]
        public ActionResult ExportIndividualReport(string programSessionHeader, string fullNameHeader, string prnHeader, string programRoll, int attendanceType, DateTime dateFrom, DateTime datetTo,bool showError=false)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentProgram = _studentProgramService.GetStudentProgram(programRoll);
                if (studentProgram == null)
                {
                    return HttpNotFound();
                }
                var message = _studentProgramService.CheckAuthorizationForStudentProgram(_userMenu, studentProgram);
                if (message != "")
                    return RedirectToAction("IndividualReport", new { message = "You are not authorized to view this history", roll = programRoll });
                var attendanceExcelList = new List<List<object>>();
                var columnList = new List<string>();
                var footerList = new List<string>();
                var headerList = new List<string>();
                headerList.Add(programSessionHeader);
                headerList.Add("Individual Attendance Report");
                headerList.Add(fullNameHeader);
                headerList.Add("PRN: " + prnHeader);
                if (attendanceType == AttendanceType.ClassAttendance)
                {
                    var individualReportResult = _studentClassAttendanceService.GetIndividualReport(programRoll, attendanceType, dateFrom, datetTo);
                    columnList.Add("Lecture Name");
                    columnList.Add("Subject Name");
                    columnList.Add("Held Date");
                    columnList.Add("Attending Date");
                    columnList.Add("Status");
                    footerList.Add("");

                    foreach (var individualReports in individualReportResult)
                    {
                        var xlsRow = new List<object>();
                        xlsRow.Add(individualReports.LectureName);
                        xlsRow.Add(individualReports.SubjectName);
                        xlsRow.Add(individualReports.HeldDate);
                        xlsRow.Add(individualReports.AttendingDate);
                        xlsRow.Add(individualReports.Status);
                        attendanceExcelList.Add(xlsRow);
                    }

                }
                else
                {

                    var individualReportResult = _studentExamAttendanceService.GetIndividualReport(programRoll, attendanceType, dateFrom, datetTo).DistinctBy(x=>x.ExamName);
                    columnList.Add("Exam Name");
                    columnList.Add("Subject Name");
                    columnList.Add("Held Date");
                    columnList.Add("Attending Date");
                    columnList.Add("Status");
                    footerList.Add("");

                    foreach (var individualReports in individualReportResult)
                    {
                        var xlsRow = new List<object>();
                        xlsRow.Add(individualReports.ExamName);
                        xlsRow.Add(individualReports.SubjectName);
                        xlsRow.Add(individualReports.HeldDate);
                        xlsRow.Add(individualReports.AttendingDate);
                        xlsRow.Add(individualReports.Status);
                        attendanceExcelList.Add(xlsRow);
                    }
                    
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, attendanceExcelList, footerList, "individual-attendance-list-report__"
                        + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (showError)
                    ViewBag.AutoCloseMsg = ex.ToString();
                
            }
            return View("AutoClose");
        }

        #endregion
       
        #region Generate Attendance Reports
        [HttpGet]
        public ActionResult GenerateAttendanceReport()
        {
            return RedirectToAction("AttendanceReport");
        }
        [HttpPost]
        public ActionResult GenerateAttendanceReport(AttendanceSearchInfo attendanceSerachInfo, string batchIds, string lectureIds)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var programName = _programService.GetProgram(attendanceSerachInfo.ProgramId).Name;
                var sessionName = _sessionService.LoadById(attendanceSerachInfo.SessionId).Name;
                ViewBag.sessionNamee = sessionName;
                ViewBag.programName = programName;
                ViewBag.brancheNames = "All";
                ViewBag.campusNames = "All";
                ViewBag.batchDay = "All";
                ViewBag.batcheNames = "All";
                ViewBag.courseNames = "All";
                ViewBag.batchIds = batchIds;
                ViewBag.lectureIds = lectureIds;
                if (!attendanceSerachInfo.SelectedBranch.Contains(0))
                {
                    //IList<Branch> branches = _branchService.LoadBranch(attendanceSerachInfo.SelectedBranch);
                    IList<Branch> branches = _branchService.LoadBranch(null, null, null, attendanceSerachInfo.SelectedBranch.ToList());
                    ViewBag.brancheNames = string.Join(", ", branches.Select(x => x.Name.Trim()).Distinct().ToList());
                }

                if (!attendanceSerachInfo.SelectedCampus.Contains(0))
                {
                    IList<Campus> campuses = _campusService.LoadCampus(null, null, null, null, attendanceSerachInfo.SelectedCampus.ToList());
                    ViewBag.campusNames = string.Join(", ", campuses.Select(x => x.Name.Trim()).Distinct().ToList());

                }
                if (attendanceSerachInfo.SelectedBatchDays != null && !attendanceSerachInfo.SelectedBatchDays.Contains("0"))
                {
                    string batchDaysString = "";
                    foreach (var bd in attendanceSerachInfo.SelectedBatchDays)
                    {
                        batchDaysString += bd + "; ";
                    }
                    batchDaysString = batchDaysString.Remove(batchDaysString.Length - 2);
                    ViewBag.batchDay = batchDaysString;
                }


                if (!attendanceSerachInfo.SelectedBatch.Contains(0))
                {
                    IList<Batch> batches = _batchService.LoadBatch(null, null, null, null, null,attendanceSerachInfo.SelectedBatch.ToList());
                    ViewBag.batcheNames = string.Join(", ", batches.Select(x => x.Name.Trim()).Distinct().ToList());

                }
                IList<Course> courseList = new List<Course>();
                if (attendanceSerachInfo.SelectedCourse.Contains(SelectionType.SelelectAll))
                {
                    courseList =
                        _courseService.LoadCourse(attendanceSerachInfo.ProgramId,
                            attendanceSerachInfo.SessionId)
                            .Where(c => c.Status == StudentProgram.EntityStatus.Active)
                            .ToList();
                }
                else
                {
                    courseList = _courseService.LoadCourse(attendanceSerachInfo.SelectedCourse);
                    ViewBag.courseNames = string.Join(", ", courseList.Select(x => x.Name.Trim()).Distinct().ToList());

                }
                var subjectList = new List<Subject>();
                foreach (var course in courseList)
                {
                    foreach (var courseSubject in course.CourseSubjects)
                    {
                        subjectList.Add(courseSubject.Subject);
                    }

                }
                string subjectNames = string.Join(", ", subjectList.Select(x => x.ShortName.Trim()).Distinct().ToList());
                ViewBag.subjectName = subjectNames;
                ViewBag.attendanceSerachInfo = attendanceSerachInfo;
                ViewBag.informationViewList = attendanceSerachInfo.InformationViewList;
                ViewBag.startDate = attendanceSerachInfo.StartDate;
                ViewBag.endDate = attendanceSerachInfo.EndDate;
                if (attendanceSerachInfo.SelectedLecture!=null && attendanceSerachInfo.SelectedLecture.Contains(0))
                {
                    attendanceSerachInfo.SelectedLecture = lectureIds.Split(',').ToList().Select(x => Convert.ToInt64(x)).ToArray();
                }
                if (attendanceSerachInfo.SelectedBatch.Contains(0))
                {
                    attendanceSerachInfo.SelectedBatch = batchIds.Split(',').ToList().Select(x => Convert.ToInt64(x)).ToArray();
                }
                if (attendanceSerachInfo.SelectedExam == null)
                {
                    return View("GenerateAttendanceReport");
                }
                else
                {
                    return View("GenerateExamAttendanceReport");

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("AttendanceReport");
            }
        }
        #endregion

        #region Clear Attendance
        #region Clear Class Attendance
        public ActionResult ClearClassAttendance()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                ViewBag.orgList = new SelectList(organizations, "Id", "ShortName");
                SelectList selectList = new SelectList(organizations, "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.orgList = selectList;
                ViewBag.SelectedLecture = new SelectList(new List<Subject>(), "Id", "Name");
                ViewBag.SelectedTeacher = new SelectList(new List<Subject>(), "Id", "Name");
                ViewBag.SelectedSubject = new SelectList(new List<Subject>(), "Id", "Name");
                ViewBag.SelectedUser = new SelectList(new List<UserProfile>(), "Id", "Name");
                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }

        [HttpPost]
        public ActionResult ClearClassAttendance(StudentClassSurveyReportViewModel studentClassSurveyReportViewModel, string lectureIds, string teacherIds,string userIds)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
            ViewBag.orgList = new SelectList(organizations, "Id", "ShortName","");
            SelectList selectList = new SelectList(organizations, "Id", "ShortName");
            ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
            ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.orgList = selectList;
            ViewBag.SelectedLecture = new SelectList(new List<Subject>(), "Id", "Name");
            ViewBag.SelectedTeacher = new SelectList(new List<Subject>(), "Id", "Name");
            ViewBag.SelectedSubject = new SelectList(new List<Subject>(), "Id", "Name");
            ViewBag.SelectedUser = new SelectList(new List<UserProfile>(), "Id", "Name");

            long[] lectureIdArray = null;
            long[] teacherIdArray = null;
            long[] userIdArray = null;
            if (studentClassSurveyReportViewModel.SelectedLecture.Contains(SelectionType.SelelectAll))
            {
                lectureIdArray = lectureIds.Split(',').ToList().Select(x => Convert.ToInt64(x)).ToArray();
            }
            else
            {

                lectureIdArray = studentClassSurveyReportViewModel.SelectedLecture;
            }
            if (studentClassSurveyReportViewModel.SelectedUser.Contains(SelectionType.SelelectAll))
            {
                userIdArray = userIds.Split(',').ToList().Select(x => Convert.ToInt64(x)).ToArray();
            }
            else
            {

                userIdArray = studentClassSurveyReportViewModel.SelectedUser;
            }
            if (studentClassSurveyReportViewModel.SelectedTeacher.Contains(SelectionType.SelelectAll))
            {
                teacherIdArray = teacherIds.Split(',').ToList().Select(x => Convert.ToInt64(x)).ToArray();
            }
            else
            {

                teacherIdArray = studentClassSurveyReportViewModel.SelectedTeacher;
            }
            bool isSuccess = _studentClassAttendanceService.ClearClassAttendace(string.Join(",", lectureIdArray), string.Join(",", teacherIdArray),string.Join(",", userIdArray), studentClassSurveyReportViewModel.FromTime, studentClassSurveyReportViewModel.ToTime);
            if (isSuccess)
                ViewBag.SuccessMessage = "Class attendance cleared successfully.";
            else
                ViewBag.ErrorMessage = "Class attendance clear fails.";
            ModelState.Clear();
            return View();
        }
        
        #endregion
       
        #region Clear Exam Attendance
        public ActionResult ClearExamAttendance()
        {

            ViewBag.SelectedUser = new SelectList(new List<UserProfile>(), "Id", "Name");
            ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
            ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedSubject = new SelectList(new List<Subject>(), "Id", "Name");
            ViewBag.orgList = new SelectList(new List<Organization>(), "Id", "ShortName");
            ViewBag.SelectedUser = new SelectList(new List<UserProfile>(), "Id", "Name");
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                ViewBag.orgList = new SelectList(organizations, "Id", "ShortName");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }

            return View();
        }
        [HttpPost]
        public ActionResult ClearExamAttendance(StudentExamSurveyReportViewModel examAttendanceobj, string userIds)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
             ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
            ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedSubject = new SelectList(new List<Subject>(), "Id", "Name");
            ViewBag.SelectedUser = new SelectList(new List<UserProfile>(), "Id", "Name");
            ViewBag.orgList = new SelectList(new List<Organization>(), "Id", "ShortName","");
            IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
            ViewBag.orgList = new SelectList(organizations, "Id", "ShortName");
            var courseList = new List<long>();
            string exmIdList = "";
            if (examAttendanceobj.SelectedCourse.Contains(0))
                courseList =
                    _courseService.LoadCourse(examAttendanceobj.OrganizationId, examAttendanceobj.ProgramId, examAttendanceobj.SessionId, examAttendanceobj.SelectedCourse)
                        .Select(x => x.Id)
                        .ToList();
            else
                courseList = examAttendanceobj.SelectedCourse.ToList();
            
            if (!examAttendanceobj.SelectedUser.Contains(0))
            {
                userIds = string.Join(",",examAttendanceobj.SelectedUser );
            }
            if (examAttendanceobj.SelectedExam.Contains(0))
            {
                IList<Exams> examList = _examService.LoadExams(examAttendanceobj.ProgramId, examAttendanceobj.SessionId,
                    courseList, examAttendanceobj.FromTime, examAttendanceobj.ToTime);
                exmIdList = string.Join(",", examList.Select(x=>x.Id).ToArray());
            }
            else
            {
                exmIdList = string.Join(",", examAttendanceobj.SelectedExam);
            }
            bool isSuccess = _studentExamAttendanceService.ClearExamAttendace(exmIdList, userIds, examAttendanceobj.FromTime, examAttendanceobj.ToTime);
            if (isSuccess)
                ViewBag.SuccessMessage = "Exam attendance cleared successfully.";
            else
                ViewBag.ErrorMessage = "Exam attendance clear fails.";
            ModelState.Clear();
            return View();
        } 
        #endregion
        #endregion

        #region Ajax Request Function
        
        [HttpPost]
        public ActionResult ClassAttendanceXmlUpload(HttpPostedFileBase xmlFile)
        {
            const string errorMessageForvalidXml = "Please upload a valid XML file.";
            try
            {
                
                if (xmlFile != null && xmlFile.ContentLength > 0 && xmlFile.ContentType.ToLower() == "text/xml")
                {
                   
                    XElement element = XElement.Load(new StreamReader(xmlFile.InputStream));
                    var classAttendanceList = (from student in
                                                   (from students in element.Elements("Students")
                                                    select students).Elements("Student")
                                               let roll=student.Attribute("Roll").Value
                                               let classQuality = student.Attribute("ClassQuality").Value
                                               
                                               select new
                                               {
                                                   Roll =roll ,
                                                   ClassQuality = classQuality,
                                                   isValidProgramRoll = _studentProgramService.GetStudentProgram(roll)==null
                                               })
                        .ToList();
                    if (classAttendanceList.Count > 0)
                    {
                        return Json(new {classAttendances = classAttendanceList, isSuccess = true});
                    }
                    else
                    {
                        return Json(new { errmsg = errorMessageForvalidXml, isSuccess = false });
                    }
                }
                else
                {
                    return Json(new { errmsg = errorMessageForvalidXml, isSuccess = false });
                }
            }
            catch (XmlException ex)
            {

                return Json(new { errmsg = errorMessageForvalidXml, isSuccess = false });
            }
            catch (Exception ex)
            {

                return Json(new { errmsg = WebHelper.CommonErrorMessage, isSuccess = false });
            }
        }
        [HttpPost]
        public ActionResult ExamAttendanceXmlUpload(HttpPostedFileBase xmlFile)
        {
            const string errorMessageForvalidXml = "Please upload a valid XML file.";
            try
            {

                if (xmlFile != null && xmlFile.ContentLength > 0 && xmlFile.ContentType.ToLower() == "text/xml")
                {

                    XElement element = XElement.Load(new StreamReader(xmlFile.InputStream));
                    var examAttendanceList = (from student in
                         (from students in element.Elements("Students")
                          select students).Elements("Student")
                     let roll = student.Attribute("Roll").Value
                     let examQuality = student.Attribute("QuestionQuality").Value

                     select new
                     {
                         Roll = roll,
                         QuestionQuality = examQuality,
                         isValidProgramRoll = _studentProgramService.GetStudentProgram(roll) == null
                     })
                        .ToList();
                    if (examAttendanceList.Count > 0)
                    {
                        return Json(new { examAttendances = examAttendanceList, isSuccess = true });
                    }
                    else
                    {
                        return Json(new { errmsg = errorMessageForvalidXml, isSuccess = false });
                    }
                }
                else
                {
                    return Json(new { errmsg = errorMessageForvalidXml, isSuccess = false });
                }
            }
            catch (XmlException ex)
            {

                return Json(new { errmsg = errorMessageForvalidXml, isSuccess = false });
            }
            catch (Exception ex)
            {

                return Json(new { errmsg = WebHelper.CommonErrorMessage, isSuccess = false });
            }
        }
        //[HttpPost]
        //public JsonResult AjaxRequestForSessions(long programId)
        //{
        //    if (Request.IsAjaxRequest())
        //    {
        //        try
        //        {
        //            _userMenu = (List<UserMenu>)ViewBag.UserMenu;

        //            var allAuthorizedSessions = _sessionService.LoadAuthorizedSession(_userMenu, null, _commonHelper.ConvertIdToList(programId)).ToList();
        //            SelectList sessionSelectList = new SelectList(allAuthorizedSessions, "Id", "Name");
        //            List<SelectListItem> sessionList = sessionSelectList.ToList();
        //            sessionList.Insert(0, new SelectListItem() { Value = "0", Text = "Select Session" });
        //            return Json(new { sessionList = sessionList, IsSuccess = true });
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.Error(ex);
        //            return Json(new Response(false, WebHelper.CommonErrorMessage));
        //        }
        //    }
        //    else
        //    {
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        //[HttpPost]
        //public JsonResult AjaxRequestForCourses(long programId, long sessionId)
        //{
        //    if (Request.IsAjaxRequest())
        //    {
        //        try
        //        {
        //            SelectList courseSelectList = new SelectList(_courseService.LoadCourse(programId, sessionId).Where(c => c.Status == StudentProgram.EntityStatus.Active).OrderBy(x => x.Rank).ToList(), "Id", "Name");
        //            List<SelectListItem> courseList = courseSelectList.ToList();

        //            courseList.Insert(0, new SelectListItem() { Value = "0", Text = "Select Course" });


        //            return Json(new { courseList = courseList, IsSuccess = true });

        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.Error(ex);
        //            return Json(new Response(false, WebHelper.CommonErrorMessage));
        //        }
        //    }
        //    else
        //    {
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        public JsonResult AjaxRequestForSubject(long courseId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var courseList = _courseService.GetCourse(courseId);

                    List<SelectListItem> subjectList = new List<SelectListItem>();
                    if (courseList != null)
                    {
                        var courseSubject = courseList.CourseSubjects.ToList();
                        List<Subject> subjects = new List<Subject>();
                        if (courseSubject != null && courseSubject.Count > 0)
                        {
                            foreach (var subject in courseSubject)
                            {
                                subjects.Add(subject.Subject);
                            }
                        }
                        SelectList subjectSelectList = new SelectList(subjects.Where(sub => sub.Status == StudentProgram.EntityStatus.Active).OrderBy(x => x.Rank).ToList(), "Id", "Name");
                        subjectList = subjectSelectList.ToList();

                    }

                    subjectList.Insert(0, new SelectListItem() { Value = "0", Text = "Select subject" });


                    return Json(new { subjectList = subjectList, IsSuccess = true });

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public JsonResult LoadTeacher(string heldDate)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var date = Convert.ToDateTime(heldDate);
                    var teacherList = _teacherService.LoadTeacher(_userMenu, date);
                    var teacherSelectList = new List<SelectListItem>();

                    teacherList.ForEach(x => teacherSelectList.Add(new SelectListItem() { Text = x.NickName + " (" + x.HscPassingYear.ToString().Substring(x.HscPassingYear.ToString().Length - 2) + ")", Value = x.Id.ToString() }));
                    return Json(new { returnTeacherList = teacherSelectList, IsSuccess = true });

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public JsonResult LoadProgram(string heldDate,long teacherId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var date = Convert.ToDateTime(heldDate);
                    var programList = _programService.LoadProgram(date,teacherId);
                    var programSelectList = new SelectList(programList, "Id", "Name");
                    return Json(new { returnProgramList = programSelectList, IsSuccess = true });

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public JsonResult LoadCourse(string heldDate, long teacherId,long programId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var date = Convert.ToDateTime(heldDate);
                    var courseList = _courseService.LoadCourse(date, teacherId,_commonHelper.ConvertIdToList(programId));
                    var courseSelectList = new SelectList(courseList, "Id", "Name");
                    return Json(new { returnCourseList = courseSelectList, IsSuccess = true });

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult LoadSubject(long courseId,long teacherId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var teacher = _teacherService.GetTeacher(teacherId);
                    var teacherSubjectPriorities = teacher.TeacherSubjectPriorities.Select(x => x.Subject).ToList();
                    var course = _courseService.GetCourse(courseId);
                    var subjectList = new List<SelectListItem>();
                    if (course != null)
                    {
                        var courseSubject = course.CourseSubjects.ToList();
                        var subjects = new List<Subject>();
                        if (courseSubject.Count > 0)
                        {
                            courseSubject.ForEach(x => subjects.Add(x.Subject));
                        }
                        subjects = subjects.Intersect(teacherSubjectPriorities).ToList();
                        var subjectSelectList = new SelectList(subjects.Where(sub => sub.Status == StudentProgram.EntityStatus.Active).OrderBy(x => x.Rank).ToList(), "Id", "Name");
                        subjectList = subjectSelectList.ToList();

                    }
                   return Json(new { returnSubjectList = subjectList, IsSuccess = true });

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        //load both lecture and teacher by subject
        [HttpPost]
        public ActionResult LoadLecture(long[] courseId, long[] subjectId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var lectureList = _lectureService.LoadLecture(courseId, subjectId).OrderBy(x=>x.LectureName).ToList();
                var lectureSelectList = new SelectList(lectureList, "Id", "LectureName").ToList();
                return Json(new { returnLectureList = lectureSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        //[HttpPost]
        //public ActionResult AjaxRequestForSession(long programId)
        //{
        //    try
        //    {
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var sessionList = _sessionService.LoadAuthorizedSession(_userMenu, null, _commonHelper.ConvertIdToList(programId));

        //        ViewBag.SessionList = _sessionService.LoadAuthorizedSession(_userMenu, null, _commonHelper.ConvertIdToList(programId)).ToList();
        //        //ViewBag.SessionList = _sessionService.LoadAuthorizedSession(userMenu, pIdLong);

        //        return View("Partial/_SessionList");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        _logger.Error(ex);
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}


        //[HttpPost]
        //public ActionResult AjaxRequestForBranch(long programId, long sessionId, long? organizationId = null)
        //{
        //    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //    try
        //    {
        //        //long pIdLong = 0;
        //        //if (programId != null)
        //        //    long.TryParse(programId.ToString(), out pIdLong);

        //        //long sIdLong = 0;
        //        //if (sessionId != null)
        //        //    long.TryParse(sessionId.ToString(), out sIdLong);

        //        List<SelectListItem> branchList = (new SelectList(_branchService.LoadAuthorizedBranch(userMenu,(organizationId==null)?null:_commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), _commonHelper.ConvertIdToList(programId),_commonHelper.ConvertIdToList(sessionId)), "Id", "Name")).ToList();
        //        if (branchList.Any())
        //            branchList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All BRANCH" });

        //        return Json(new { branchList = branchList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        // _logger.Error("Batch. AjaxRequestForBranch.", ex);
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}


        //[HttpPost]
        //public ActionResult AjaxRequestForCampus(long programId, long sessionId, long[] branchId)
        //{
        //    try
        //    {
        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;

        //        //long programIdLong = 0;
        //        //if (programId != null)
        //        //    long.TryParse(programId.ToString(), out programIdLong);

        //        //long sessionIdLong = 0;
        //        //if (sessionId != null)
        //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);

        //        IList<Campus> campusList = new List<Campus>();
        //        if (branchId == null || branchId.Contains(SelectionType.SelelectAll))
        //            campusList = _campusService.LoadAuthorizeCampus(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(),_commonHelper.ConvertIdToList(sessionId));
        //        else
        //            campusList = _campusService.LoadAuthorizeCampus(userMenu,null,null, branchId.ToList());

        //        List<SelectListItem> campusSelecteList = (new SelectList(campusList, "Id", "Name")).ToList();
        //        if (campusList.Any())
        //            campusSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All CAMPUS" });

        //        return Json(new { campusList = campusSelecteList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        //_logger.Error("Batch. AjaxRequestForCampus.", ex);
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        //[HttpPost]
        //public ActionResult AjaxRequestForBatchDay(long programId, long sessionId, long[] branchId, long[] campusId)
        //{
        //    try
        //    {
        //        #region old code
        //        //long programIdLong = 0;
        //        //if (programId != null)
        //        //    long.TryParse(programId.ToString(), out programIdLong);

        //        //long sessionIdLong = 0;
        //        //if (sessionId != null)
        //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);

        //        //IList<Batch> batchList = new List<Batch>();
        //        //if (branchId != null && !branchId.Contains(SelectionType.SelelectAll) && campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampusHabibXp(programIdLong, sessionIdLong, branchId, campusId);
        //        //else if (branchId != null && !branchId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchHabibXp(programIdLong, sessionIdLong, branchId);
        //        //else if (campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionCampusHabibXp(programIdLong, sessionIdLong, campusId);
        //        //else
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionHabibXp(programIdLong, sessionIdLong);

        //        ////Check by Batch Day
        //        //IList<string> batchDayList = (from x in batchList select x.Days).Distinct().ToList();
        //        //List<SelectListItem> batchSelecteList = (new SelectList(batchDayList)).ToList();

        //        //if (batchSelecteList.Any())
        //        //    batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All BATCH DAY" });

        //        #endregion

        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var batchList = _batchService.LoadAuthorizeBatchGroupByDays(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList());
        //        //Check by Batch Day
        //        //IList<string> batchDayList = (from x in batchList select x.Days).Distinct().ToList();
        //        List<SelectListItem> batchSelecteList = (new SelectList(batchList)).ToList();
        //        foreach (var selectListItem in batchSelecteList)
        //        {
        //            selectListItem.Value = selectListItem.Text;
        //        }
        //        if (batchSelecteList.Any())
        //            batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Day" });


        //        return Json(new { batchDays = batchSelecteList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        //_logger.Error("Batch. AjaxRequestForBatchDay.", ex);
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        //[HttpPost]
        //public ActionResult AjaxRequestForBatchTime(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays)
        //{
        //    try
        //    {
        //        #region old code
        //        //long programIdLong = 0;
        //        //if (programId != null)
        //        //    long.TryParse(programId.ToString(), out programIdLong);

        //        //long sessionIdLong = 0;
        //        //if (sessionId != null)
        //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);

        //        //IList<Batch> batchList = new List<Batch>();
        //        //if (branchId != null && !branchId.Contains(SelectionType.SelelectAll) && campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampusHabibXp(programIdLong, sessionIdLong, branchId, campusId);
        //        //else if (branchId != null && !branchId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchHabibXp(programIdLong, sessionIdLong, branchId);
        //        //else if (campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionCampusHabibXp(programIdLong, sessionIdLong, campusId);
        //        //else
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionHabibXp(programIdLong, sessionIdLong);

        //        ////Check Batch Day 
        //        //if (batchDays != null && !batchDays.Contains(SelectionType.SelelectAll.ToString()))
        //        //    batchList = (from a in batchList where batchDays.Any(ae => ae == a.Days) select a).ToList();


        //        //SelectList batchTimeSelectList = new SelectList(batchList, "BatchTime", "FormatedBatchTime");
        //        //List<SelectListItem> batchTimes = batchTimeSelectList.GroupBy(x => x.Value).Select(x => x.First()).ToList();


        //        //if (batchTimes.Any())
        //        //    batchTimes.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All BATCH TIME" });

        //        #endregion
        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var batchList = _batchService.LoadAuthorizeBatchGroupByTimes(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays);
        //        //batchList = batchList.DistinctBy(x => x.BatchTime).ToList();
        //        //SelectList batchTimeSelectList = new SelectList(batchList, "BatchTime", "FormatedBatchTime");
        //        var batchTimeSelectList = new SelectList(batchList);
        //        List<SelectListItem> batchTimes = batchTimeSelectList.ToList();
        //        if (batchTimes.Any())
        //            batchTimes.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Time" });
                

        //        return Json(new { batchTime = batchTimes, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        // _logger.Error("Batch. AjaxRequestForBatchTime.", ex);
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        //[HttpPost]
        //public ActionResult AjaxRequestForBatch(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, string[] batchTimeText)
        //{
        //    try
        //    {
        //        #region old code
        //        //long programIdLong = 0;
        //        //if (programId != null)
        //        //    long.TryParse(programId.ToString(), out programIdLong);


        //        //long sessionIdLong = 0;
        //        //if (sessionId != null)
        //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);


        //        //IList<Batch> batchList = new List<Batch>();
        //        //if (branchId != null && !branchId.Contains(SelectionType.SelelectAll) && campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampusHabibXp(programIdLong, sessionIdLong, branchId, campusId);
        //        //else if (branchId != null && !branchId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchHabibXp(programIdLong, sessionIdLong, branchId);
        //        //else if (campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionCampusHabibXp(programIdLong, sessionIdLong, campusId);
        //        //else
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionHabibXp(programIdLong, sessionIdLong);


        //        ////Check Batch Day
        //        //if (batchDays != null && !batchDays.Contains(SelectionType.SelelectAll.ToString()))
        //        //    batchList = (from a in batchList where batchDays.Any(ae => ae == a.Days) select a).ToList();


        //        ////Check Batch Time
        //        //if (batchTime != null && !batchTime.Contains(SelectionType.SelelectAll.ToString()))
        //        //    batchList = (from x in batchList where batchTimeText.Any(y => y == x.FormatedBatchTime) select x).ToList();


        //        //List<SelectListItem> batchSelecteList = (new SelectList(batchList, "Id", "Name")).ToList();
        //        //if (batchSelecteList.Any())
        //        //    batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All BATCH" });

        //        #endregion
        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var batchList = _batchService.LoadAuthorizeBatch(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime);
        //        List<SelectListItem> batchSelecteList = (new SelectList(batchList.ToList(), "Id", "Name")).ToList();
        //        if (batchSelecteList.Any())
        //            batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch" });
               

        //        return Json(new { batchName = batchSelecteList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        //_logger.Error("Batch. AjaxRequestForBatch.", ex);
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        public ActionResult LoadLectureForClearClassAttendance(List<long> organizationIdList, List<long> programIdList , List<long> sessionIdList , long[] courseIdList, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var courseList = new List<long>();
                if (courseIdList.Contains(0))
                    courseList =
                        _courseService.LoadCourse(organizationIdList[0], programIdList[0], sessionIdList[0], courseIdList)
                            .Select(x => x.Id)
                            .ToList();
                else
                    courseList = courseIdList.ToList();
                var lectureList = _lectureService.LoadLecture(_userMenu, organizationIdList, programIdList, sessionIdList, courseList.ToArray(), dateFrom, dateTo);
                var lectureSelectList = new SelectList(lectureList.DistinctBy(x => x.Id), "Id", "Name").ToList();
                return Json(new { lectureList = lectureSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public ActionResult LoadTeacherForClearClassAttendance(long[] lectureIdList, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                lectureIdList = lectureIdList.Where(x => x != 0).ToArray();
                var teacherList = _teacherService.LoadTeacher(lectureIdList, dateFrom, dateTo);
                var teacherSelectList = new SelectList(teacherList.DistinctBy(x => x.Id), "Id", "DisplayTeacherName").ToList();
                return Json(new { teacherList = teacherSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public ActionResult LoadLectureForClassAttendanceReport(List<long> organizationIdList, List<long> programIdList ,List<long> sessionIdList , List<long> branchIdList ,  List<long> campusIdList , string[] batchDaysList , string[] batchTimeList, long[] batchIdList,long[] courseIdList, DateTime dateFrom, DateTime dateTo )
        {
            try
            {
                
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                   
                if (batchIdList[0] == 0)
                {
                   var batchList  = _batchService.LoadAuthorizeBatch(_userMenu, organizationIdList, programIdList, branchIdList, sessionIdList, campusIdList, batchDaysList, batchTimeList, null, null);
                    batchIdList = batchList.Select(x => x.Id).ToArray();
                }

                if (courseIdList[0] == 0)
                {
                    courseIdList = _courseService.LoadCourse(organizationIdList[0], programIdList[0], sessionIdList[0], courseIdList).Select(x=>x.Id).ToArray();
                }

                var lectureList = _lectureService.LoadLecture(programIdList, sessionIdList, batchIdList, courseIdList, dateFrom, dateTo);
                var lectureSelectList = new SelectList(lectureList.DistinctBy(x => x.Id), "Id", "Name").ToList();
                return Json(new { lectureList = lectureSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public ActionResult GetClassAttendanceCount(long programId, long sessionId, long[] batchId,
                    string[] batchDays, string[] batchTime, long[] branchId,string batchIds,string lectureIds, long[] campusId, long[] courseId,
                    long[] paymentStatus
                    , long attendanceStatus, long attendanceType, DateTime dateFrom,
                    DateTime dateTo)
        {
            try
            {

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var count=_studentProgramService.GetStudentProgramCount(_userMenu,
                    programId, sessionId,
                    branchId
                    , campusId, batchDays,
                    batchTime, batchId,
                    paymentStatus, attendanceStatus, dateFrom,
                    dateTo, batchIds, courseId, lectureIds);
                return Json(new { rowCount = count, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public JsonResult GetStudentCount(long[] lectureIdList, long[] teacherIdList,long[] userIdList, DateTime dateFrom, DateTime dateTo)
        {
            //long organizationId,long programId,long sessionId,long[]branchIds,long[]campusIds,string[]batchDays,string []batchTimes,
            try
            {

                int recordsTotal = _studentClassAttendanceService.GetStudentCount(
                    lectureIdList, teacherIdList, dateFrom.Date, dateTo.Date,userIdList);
                if (recordsTotal > 0)
                {
                    return Json(new { rowCount = recordsTotal, IsSuccess = true });
                }
                return Json(new { rowCount = recordsTotal, IsSuccess = false });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw;
            }
        }
        public JsonResult GetClassAttendanceUsers(long[] organizationIds,long[] programIds,long[]sessionIds,long[] courseIds, long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var usersList= _userService.LoadUsers(_userMenu, organizationIds, programIds, sessionIds, courseIds, lectureIdList
                   , teacherIdList, dateFrom, dateTo);
                var userSelectList = new SelectList(usersList, "Id", "Email").ToList();
                return Json(new { userList = userSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                 return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
           
        }
        public JsonResult GetExamAttendanceUsers(long[] organizationIds, long[] programIds, long[] sessionIds, long[] courseIds, long[] examIdList,  DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var usersList = _userService.LoadUsers(_userMenu, organizationIds, programIds, sessionIds, courseIds, examIdList
                   , dateFrom, dateTo);
                var userSelectList = new SelectList(usersList, "Id", "Email").ToList();
                return Json(new { userList = userSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }
        
        public ActionResult LoadExamsForClearExamAttendance(long organizationId, long programId, long sessionId, long[] courseId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var courseList =new List<long>();
                if (courseId.Contains(0))
                    courseList =
                        _courseService.LoadCourse(organizationId, programId, sessionId, courseId)
                            .Select(x => x.Id)
                            .ToList();
                else
                    courseList = courseId.ToList();
                IList<Exams> examList = _examService.LoadExams(programId, sessionId, courseList, dateFrom, dateTo);
                List<SelectListItem> selectexamList = (new SelectList(examList, "Id", "Name")).ToList();
                if (examList.Any())
                {
                    selectexamList.Insert(0, new SelectListItem() { Value = "0", Text = "All Exam" });
                    return Json(new { examList = selectexamList, IsSuccess = true });
                }
                return Json(new { examList = new List<Exams>(), IsSuccess = true });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return Json(new { examList = new List<Exams>(), IsSuccess = false });

            }

        }
        public JsonResult GetStudentCountForExamAttendance(long organizationId, long programId, long sessionId, long[] courseId, DateTime dateFrom, DateTime dateTo, long[] examIds,long[] userIdList)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

               

                int recordsTotal = 0;

                recordsTotal = _studentExamAttendanceService.GetTotalStudentCount(_userMenu,organizationId, programId, sessionId, dateFrom, dateTo, examIds.ToList(), string.Join(",", userIdList));
                if (recordsTotal > 0)
                {
                    return Json(new { rowCount = recordsTotal, IsSuccess = true });
                }
                return Json(new { rowCount = recordsTotal, IsSuccess = false });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Others Function
        //[HttpPost]
        //public JsonResult GetCourseList(long programId, long sessionId)
        //{
        //    try
        //    {
        //        IList<Course> courseListDB = _courseService.LoadCourse(programId, sessionId).Where(c => c.Status == StudentProgram.EntityStatus.Active).ToList();
        //        if (courseListDB != null && courseListDB.Count > 0)
        //        {
        //            Course obj = new Course();
        //            obj.Id = SelectionType.SelelectAll;
        //            obj.Name = "ALL COURSE";
        //            courseListDB.Insert(0, obj);
        //        }

        //        var courseFullList = new SelectList(courseListDB.OrderBy(x => x.Rank).ToList(), "Id", "Name");
        //        ViewBag.ProgramId = courseFullList;
        //        return Json(new { courseList = courseFullList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        _logger.Error(ex);
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        //load only program by organization
      
        //public JsonResult GetProgramTeacherByOrganization(long organizatonId = 0)
        //{
        //    try
        //    {
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var programList = _programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(organizatonId));
        //        //var teacherList = _teacherService.LoadTeacher(organizatonId).DistinctBy(x => x.Id).ToList();
        //        var programSelectList = new SelectList(programList, "Id", "Name");
        //        //var teacherSelectList = new SelectList(teacherList, "Id", "NickName");
        //        return Json(new { programList = programSelectList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {

        //        _logger.Error(ex);
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        //[HttpPost]
        //public ActionResult GetSession(long programId)
        //{
        //    try
        //    {
        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        ViewBag.SessionList = _sessionService.LoadAuthorizedSession(userMenu,null, _commonHelper.ConvertIdToList(programId));
        //        return View("Partial/_SessionList");
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        //[HttpPost]
        //public JsonResult GetCourse(long programId, long sessionId)
        //{
        //    if (Request.IsAjaxRequest())
        //    {
        //        try
        //        {
        //            var courseSelectList = new SelectList(_courseService.LoadCourse(programId, sessionId), "Id", "Name");
        //            List<SelectListItem> courseList = courseSelectList.ToList();
        //            //if (courseList.Count > 0)
        //            //    courseList.Insert(0, new SelectListItem() { Value = "", Text = "Select Course" });
        //            return Json(new { courseList = courseList, IsSuccess = true });
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.Error(ex);
        //            return Json(new Response(false, WebHelper.CommonErrorMessage));
        //        }
        //    }
        //    else
        //    {
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        //[HttpPost]
        //public ActionResult GetBranch(long programId, long sessionId, long organizationId)
        //{
        //    try
        //    {
        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        List<SelectListItem> branchList = (new SelectList(_branchService.LoadAuthorizedBranch(userMenu,_commonHelper.ConvertIdToList( organizationId),_commonHelper.ConvertIdToList( programId),_commonHelper.ConvertIdToList( sessionId)).ToList(), "Id", "Name")).ToList();
        //        //if (branchList.Any())
        //        //    branchList.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });
        //        return Json(new { branchList = branchList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        //[HttpPost]
        //public ActionResult GetCampus(long programId, long sessionId, long[] branchId)
        //{
        //    try
        //    {
        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;

        //        var campusList = _campusService.LoadAuthorizeCampus(userMenu, null,_commonHelper.ConvertIdToList(programId),branchId.ToList(),_commonHelper.ConvertIdToList(sessionId));
        //        List<SelectListItem> campusSelecteList = (new SelectList(campusList.OrderBy(x => x.Rank).ToList(), "Id", "Name")).ToList();
        //        //if (campusList.Any())
        //        //    campusSelecteList.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });

        //        return Json(new { campusList = campusSelecteList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        //[HttpPost]
        //public ActionResult GetBatchDay(long programId, long sessionId, long[] branchId, long[] campusId)
        //{
        //    try
        //    {

        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var batchList = _batchService.LoadAuthorizeBatchGroupByDays(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList());
        //        List<SelectListItem> batchSelecteList = (new SelectList(batchList)).ToList();
        //        //if (batchSelecteList.Any())
        //        //    batchSelecteList.Insert(0, new SelectListItem() { Value = "", Text = "Select Batch Day" });

        //        return Json(new { batchDays = batchSelecteList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        //[HttpPost]
        //public ActionResult GetBatchTime(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays)
        //{
        //    try
        //    {

        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var batchList = _batchService.LoadAuthorizeBatchGroupByTimes(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays);
        //        var batchTimeSelectList = new SelectList(batchList);
        //        List<SelectListItem> batchTimes = batchTimeSelectList.ToList();
        //        //if (batchTimes.Any())
        //        //    batchTimes.Insert(0, new SelectListItem() { Value = "", Text = "Select Batch Time" });
        //        return Json(new { batchTime = batchTimes, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        //[HttpPost]
        //public ActionResult GetBatch(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, string[] batchTimeText)
        //{
        //    try
        //    {
        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var batchList = _batchService.LoadAuthorizeBatch(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime);
        //        List<SelectListItem> batchSelecteList = (new SelectList(batchList.ToList(), "Id", "Name")).ToList();
        //        //if (batchSelecteList.Any())
        //        //    batchSelecteList.Insert(0, new SelectListItem() { Value = "", Text = "Select Batch" });
        //        return Json(new { batchName = batchSelecteList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}
        
        [HttpPost]
        public ActionResult GetExam(long programId, long sessionId, long[] courseId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var examList = _examService.LoadExams(_userMenu, programId, sessionId, courseId);
                var examSelectList = new SelectList(examList, "Id", "Name").ToList();
                //lectureSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Lecture" });
                return Json(new { examList = examSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult CheckValidStudentProgramRoll(string studentProgramRoll)
       {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool isValidRoll = true;
                    var studentProgram = _studentProgramService.GetStudentProgram(studentProgramRoll);
                    if (studentProgram == null)
                    {
                        isValidRoll = false;
                    }
                    return Json(isValidRoll);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                // _logger.Error("Batch. AjaxRequestForBranch.", ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult AjaxRequestForExam(long programId, long sessionId, long courseId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var exams = _examService.GetExamByProgramSessionAndCourse(programId, sessionId, courseId);
                    var examList = new List<SelectListItem>();
                    if (exams != null)
                    {
                        var examSelectList = new SelectList(exams.OrderBy(x => x.Rank).ToList(), "Id", "Name");
                        examList = examSelectList.ToList();
                    }
                    examList.Insert(0, new SelectListItem() { Value = "0", Text = "Select Exam" });
                    return Json(new { examList = examList, IsSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    Console.WriteLine("{0}", ex.Message);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

    }

}

