﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Common;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.BusinessRules.Student;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentProgramService : IBaseService
    {
        #region Operational Function
        bool Save(StudentProgram studentProgramObj);
        bool Update(StudentProgram studentProgramObj);
        #endregion

        #region Single Instances Loading Function
        StudentProgram GetStudentProgram(long studentProgramId, bool isResetCache = false);
        StudentProgram GetStudentProgram(string prnNo, bool withInactive = false);
        StudentProgram GetStudentProgram(long programId, long sessionId, long studentId);
        StudentProgram GetStudentProgram(long programId, long sessionId, string board, long boardExam, string studentBoardRoll, string studentBoardRegNo); 
        #endregion

        #region List Loading Function
        IList<StudentProgram> LoadStudentProgram(string mobileOrPrnNo);
        IList<StudentProgram> LoadStudentProgram(string[] prnNo);
        IList<StudentProgram> LoadStudentProgram(long studentId);
        IList<StudentProgramDto> LoadStudentProgramListByCriteriaForPreviousStudentReport(int start, int length, List<UserMenu> usermenu, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo, int studentType);
        IList<dynamic> LoadStudentProgramListByCriteriaForPreviousStudentSummaryReport(int start, int length, List<UserMenu> _userMenu, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo, int studentType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userMenu"></param>
        /// <param name="programId"></param>
        /// <param name="sessionId"></param>
        /// <param name="branchId"></param>
        /// <param name="campusId"></param>
        /// <param name="batchDays"></param>
        /// <param name="batchTime"></param>
        /// <param name="batchId"></param>
        /// <param name="courseId"></param>
        /// <param name="version"></param>
        /// <param name="gender"></param>
        /// <param name="religion"></param>
        /// <param name="draw"></param>
        /// <param name="start"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        IList<StudentListDto> LoadAuthorizedStudents(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string orderBy, string orderDir, int start, int length, string startDate, string endDate, int selectedStatus,bool isDisplayAcademicInfo, string prnNo, string name, string mobile);
        IList<StudentListDto> LoadAuthorizedStudents(List<UserMenu> userMenu, long programId, long sessionId, string searchKey, string[] informationViewList, int start, int length, string orderBy, string orderDir, string prnNo, string name, string mobile);
        IList<StudentProgram> LoadStudentProgramByCriteriaForClassEvaluation(int start, int length, List<UserMenu> userMenu, long organizationId, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long lectureId, long teacherId, long[] batchIdList, DateTime dateFrom, DateTime dateTo);
       
        //Dictionary<long, int> LoadBranchProgramWiseStudentAdmssion(List<UserMenu> userMenu, Branch authorizeBranch, string dateFrom, string dateTo, bool lastSession);
        List<StudentAdmissionSummeryReport> LoadBranchProgramWiseStudentAdmssion(List<UserMenu> userMenu, List<long> authorizeBranchIdList, string dateFrom, string dateTo, bool lastSession);

        IList<long> LoadStudentProgramWithImage(List<UserMenu> userMenu, long[] programIds,
            long[] sessionIds, long[] branchIds, long[] campusIds, long[] batchNames, string[] batchDays,
            string[] batchTimes);

        IList<ProgramAttendedSummary> LoadProgramSummaryData(string stdRollOrStdProRoll); //Viwe model included according to masum

           
        #endregion

        #region Others Function
        int LoadStudentProgramCountForClassEvaluation(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long lectureId, long teacherId, long[] batchIdList, DateTime dateFrom, DateTime dateTo);
        int GetAdmittedStudentCountByBranchProgramSession(long branchId, long programId, long sessionId);      
        int GetStudentsCountForPreviousStudentReport(List<UserMenu> usermenu, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo, int studentType);      
        int GetAuthorizedStudentsCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus, string prnNo = null, string name = null, string mobile = null);       
        int GetAuthorizedStudentsCount(List<UserMenu> userMenu, long programId, long sessionId, string searchKey, string[] informationViewList, string prnNo, string name, string mobile);      
        int GetStudentProgramCount(List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds, long[] courseId,string lectureIds);
        int GetAuthorizedStudentProgramCount(List<UserMenu> _userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] course, int printingStatus, int? distributionStatus = null, List<ServiceBlock> serviceBlocks = null);


        List<CourseAPIDto> LoadCourses(long id);

        #endregion

        #region Helper Function

        string CheckAuthorizationForStudentProgram(List<UserMenu> userMenu, StudentProgram studentProgram);

        //IList<dynamic> LoadStudentImageByVariousCriteria(List<UserMenu> userMenu, int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, int? isMissing, string programRoll = "", string nickName = "", string mobileNumber = "", string purpose = "");
        IList<dynamic> LoadStudentImageByVariousCriteria(List<UserMenu> userMenu, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, string programRoll = "", string nickName = "", string mobileNumber = "", string purpose = "");

        //int CountStudentImageByVariousCriteria(List<UserMenu> userMenu, int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, int? isMissing, string programRoll = "", string nickName = "", string mobileNumber = "");
        int CountStudentImageByVariousCriteria(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, string programRoll = "", string nickName = "", string mobileNumber = "");

        //int CountStudentImageByVariousCriteria(List<UserMenu> userMenu, long programId, long sessionId, string statusVal, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId);


        #endregion
     
        #region clean
       // IList<StudentProgram> LoadStudentProgramByCriteriaForAttendance(int start, int length, List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus);
        //IList<StudentProgramDto> LoadStudentProgramByCriteriaForClassAttendance(int start, int length, List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, long[] courseId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds);
        //IList<long> LoadStudentProgramIds(List<long> courseIds);

        //int[] GetAdmittedPreviousStudentForParticularBranchByBranchProgramSession(long branchId, long programId, long sessionId, List<UserMenu> userMenu);
        //int GetAuthorizedStudentsCount(List<UserMenu> _userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] course, int printingStatus, int? distributionStatus = null);
       // int GetStudentProgramCount(List<long> programIds, List<long> sessionIds, List<long> subjectIds);
        //int GetStudentsCountForPreviousStudentSummaryReport(List<UserMenu> usermenu, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo, int studentType);
        #endregion



        
    }

    public class StudentProgramService : BaseService, IStudentProgramService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IInstituteDao _instituteDao;
        private readonly IStudentExamDao _studentExamDao;
        private readonly IStudentBoardDao _studentBoardDao;
        private readonly IStudentAdmissionSubjectDao _studentAdmissionSubjectDao;
        private readonly IProgramSettingDao _programSettingDao;
        private readonly IBranchService _branchService;
        private IDistrictDao _districtDao;
        private IBranchDao _branchDao;
        private readonly IBatchDao _batchDao;
        private CommonHelper _commonHelper;

        public StudentProgramService(ISession session)
        {
            Session = session;
            _studentProgramDao = new StudentProgramDao() { Session = session };
            _instituteDao = new InstituteDao() { Session = session };
            _studentExamDao = new StudentExamDao() { Session = session };
            _studentBoardDao = new StudentBoardDao() { Session = session };
            _studentAdmissionSubjectDao = new StudentUniversitySubjectDao() { Session = session };
            _districtDao = new DistrictDao() { Session = session };
            _programSettingDao = new ProgramSettingDao() { Session = session };
            _branchService = new BranchService(session);
            _branchDao = new BranchDao { Session = session };
            _batchDao = new BatchDao() { Session = session };
            _commonHelper = new CommonHelper();

        }
        #endregion

        #region Operational Function
        public bool Save(StudentProgram studentProgramObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    _studentProgramDao.Save(studentProgramObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool Update(StudentProgram studentProgramObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    _studentProgramDao.Update(studentProgramObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public StudentProgram GetStudentProgram(long studentProgramId, bool isResetCache = false)
        {
            try
            {
                if (isResetCache == true)
                {
                    //Session.Flush();
                    //Session.Clear();
                    Session.Evict(_studentProgramDao.LoadById(studentProgramId));
                    //Session.SessionFactory.Evict(typeof(StudentProgram), studentProgramId);
                }
                return _studentProgramDao.LoadById(studentProgramId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public StudentProgram GetStudentProgram(string prnNo, bool withInactive = false)
        {
            try
            {
                return _studentProgramDao.GetStudentProgram(prnNo, withInactive);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public StudentProgram GetStudentProgram(long programId, long sessionId, long studentId)
        {
            try
            {
                return _studentProgramDao.GetStudentProgram(programId, sessionId, studentId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public StudentProgram GetStudentProgram(long programId, long sessionId, string board, long boardExam, string studentBoardRoll,
            string studentBoardRegNo)
        {
            try
            {
                return _studentProgramDao.GetStudentProgram(programId, sessionId, board, boardExam, studentBoardRoll, studentBoardRegNo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public IList<StudentProgram> LoadStudentProgram(string mobileOrPrnNo)
        {
            try
            {
                return _studentProgramDao.LoadStudentProgram(mobileOrPrnNo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<StudentProgram> LoadStudentProgram(string[] prnNo)
        {
            try
            {
                return _studentProgramDao.LoadStudentProgram(prnNo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<StudentProgram> LoadStudentProgram(long studentId)
        {
            try
            {
                return _studentProgramDao.LoadStudentProgram(studentId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
               
        public IList<StudentProgramDto> LoadStudentProgramListByCriteriaForPreviousStudentReport(int start, int length, List<UserMenu> userMenu, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo,
            int studentType)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programIdTo));

                return _studentProgramDao.LoadStudentProgramListByCriteriaForPreviousStudentReport(start, length, programIdList, branchIdList, programIdFrom, sessionIdFrom, branchIdFrom, programIdTo, sessionIdTo, branchIdTo,
                     studentType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<dynamic> LoadStudentProgramListByCriteriaForPreviousStudentSummaryReport(int start, int length, List<UserMenu> userMenu,
            long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo,
            int studentType)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programIdTo));

                return _studentProgramDao.LoadStudentProgramListByCriteriaForPreviousStudentSummaryReport(start, length, programIdList, branchIdList, programIdFrom, sessionIdFrom, branchIdFrom, programIdTo, sessionIdTo, branchIdTo,
                     studentType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        

        /// <summary>
        /// refactored function
        /// </summary>
        /// <param name="userMenu"></param>
        /// <param name="programId"></param>
        /// <param name="sessionId"></param>
        /// <param name="branchId"></param>
        /// <param name="campusId"></param>
        /// <param name="batchDays"></param>
        /// <param name="batchTime"></param>
        /// <param name="batchId"></param>
        /// <param name="courseId"></param>
        /// <param name="version"></param>
        /// <param name="gender"></param>
        /// <param name="religion"></param>
        /// <param name="draw"></param>
        /// <param name="start"></param>
        /// <param name="length"></param>
        /// <returns></returns>

        public IList<StudentListDto> LoadAuthorizedStudents(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string orderBy, string orderDir, int start, int length, string startDate, string endDate, int selectedStatus, bool isDisplayAcademicInfo, string prnNo, string name, string mobile)
        {
            try
            {
                //List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu);//by menu
                //List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                //return _studentProgramDao.LoadAuthorizedStudents(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, orderBy, orderDir, start, length, startDate, endDate, selectedStatus, isDisplayAcademicInfo,prnNo, name, mobile);

                List<long> authoOrgaizationIdList = AuthHelper.LoadOrganizationIdList(userMenu);//by menu
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu, authoOrgaizationIdList, null, _commonHelper.ConvertIdToList(programId));//by menu
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrgaizationIdList, authorizedProgramLists, !branchId.ToList().Contains(SelectionType.SelelectAll) ? branchId.ToList() : null);
                return _studentProgramDao.LoadAuthorizedStudents(authorizedProgramLists, branchIdList, sessionId, branchId.ToList(), campusId.ToList(), batchDays.ToList(), batchTime.ToList(), batchId.ToList(), courseId.ToList(), version, gender, religion.ToList(), orderBy, orderDir, start, length, startDate, endDate, selectedStatus, isDisplayAcademicInfo, prnNo, name, mobile);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<StudentListDto> LoadAuthorizedStudents(List<UserMenu> userMenu, long programId, long sessionId, string searchKey, string[] informationViewList, int start, int length, string orderBy, string orderDir, string prnNo, string name, string mobile)
        {
            try
            {
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu);//by menu
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));

                List<int> religionIdList = new List<int>();
                List<int> genderIdList = new List<int>();
                List<int> versionOfStudyList = new List<int>();

                //search keyword 
                if (informationViewList.Any() && !String.IsNullOrEmpty(searchKey))
                {
                    foreach (string information in informationViewList)
                    {
                        switch (information)
                        {
                            case StudentListConstant.VersionOfStudy:
                                versionOfStudyList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<VersionOfStudy>(searchKey);
                                break;
                            case StudentListConstant.Gender:
                                genderIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Gender>(searchKey);
                                break;
                            case StudentListConstant.Religion:
                                religionIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Religion>(searchKey);
                                break;
                        }
                    }
                }

                return _studentProgramDao.LoadAuthorizedStudents(authorizedProgramLists, branchIdList, programId, sessionId, searchKey, informationViewList, start, length, orderBy, orderDir, prnNo, name, mobile, versionOfStudyList, genderIdList, religionIdList);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<StudentProgram> LoadStudentProgramByCriteriaForClassEvaluation(int start, int length, List<UserMenu> userMenu, long organizationId, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long lectureId, long teacherId, long[] batchIdList, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                var programIdList = _commonHelper.ConvertIdToList(programId);
                var sessionIdList = _commonHelper.ConvertIdToList(sessionId);
                if (batchIdList.Contains(0))
                {
                    List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(branchId.ToList()), CommonHelper.ConvertSelectedAllIdList(programIdList));
                    List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(programIdList), CommonHelper.ConvertSelectedAllIdList(branchId.ToList()));
                    var batchList = _batchDao.LoadAuthorizeBatch(authProgramIdList, authBranchIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList), CommonHelper.ConvertSelectedAllIdList(campusId.ToList()), batchDays, batchTime);
                    //calling private function
                    batchList = LoadAuthorizeBatch(batchList, batchDays, batchTime);
                    // batchIdList = batchList.Select(t => t != null ? t.Id : (long?)null).ToArray();
                    batchIdList = batchList.Select(x => x.Id).ToArray();
                }
                return _studentProgramDao.LoadStudentProgramByCriteriaForClassEvaluation(start, length, lectureId, teacherId, batchIdList, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public List<StudentAdmissionSummeryReport> LoadBranchProgramWiseStudentAdmssion(List<UserMenu> userMenu, List<long> authorizeBranchIdList, string dateFrom, string dateTo, bool lastSession)
        {
            try
            {
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList, null, authorizeBranchIdList);
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu, authOrganizationIdList, authBranchIdList);

                List<StudentAdmissionSummeryReport> branchProgramWiseStudentAdmssionList = _studentProgramDao.LoadBranchProgramWiseStudentAdmssion(authBranchIdList, authProgramIdList, dateFrom, dateTo, lastSession);

                return branchProgramWiseStudentAdmssionList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<long> LoadStudentProgramWithImage(List<UserMenu> userMenu, long[] programIds, long[] sessionIds, long[] branchIds, long[] campusIds, long[] batchNames, string[] batchDays, string[] batchTimes)
        {
            try
            {
                List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, null, programIds.ToList());
                IList<long> studentProgramIdList = _studentProgramDao.LoadStudentProgramWithImage(authorizedProgramIdList, authorizedBranchIdList, programIds, sessionIds, branchIds, campusIds, batchNames, batchDays, batchTimes);
                return studentProgramIdList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        public IList<ProgramAttendedSummary> LoadProgramSummaryData(string stdRollOrStdProRoll)
        {
            try
            {
                return _studentProgramDao.LoadProgramSummaryData(stdRollOrStdProRoll);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        

        #endregion

        #region Others Function
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userMenu"></param>
        /// <param name="programId"></param>
        /// <param name="sessionId"></param>
        /// <param name="branchId"></param>
        /// <param name="campusId"></param>
        /// <param name="batchDays"></param>
        /// <param name="batchTime"></param>
        /// <param name="batchId"></param>
        /// <param name="courseId"></param>
        /// <param name="version"></param>
        /// <param name="gender"></param>
        /// <param name="religion"></param>
        /// <param name="prnNo"></param>
        /// <param name="name"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public int GetAuthorizedStudentsCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus, string prnNo, string name
            , string mobile)
        {
            try
            {
                List<long> authoOrgaizationIdList = AuthHelper.LoadOrganizationIdList(userMenu);//by menu
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu, authoOrgaizationIdList, null, _commonHelper.ConvertIdToList(programId));//by menu
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrgaizationIdList, authorizedProgramLists, !branchId.ToList().Contains(SelectionType.SelelectAll) ? branchId.ToList() : null);
                //return _studentProgramDao.GetAuthorizedStudentsCount(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, prnNo, name, mobile);
                return _studentProgramDao.GetAuthorizedStudentsCount(authorizedProgramLists, branchIdList, sessionId, campusId.ToList(), batchDays.ToList(), batchTime.ToList(), batchId.ToList(), courseId.ToList(), version, gender, religion.ToList(), startDate, endDate, selectedStatus, prnNo, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userMenu"></param>
        /// <param name="programId"></param>
        /// <param name="sessionId"></param>
        /// <param name="searchKey"></param>
        /// <param name="informationViewList"></param>
        /// <param name="prnNo"></param>
        /// <param name="name"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public int GetAuthorizedStudentsCount(List<UserMenu> userMenu, long programId, long sessionId, string searchKey, string[] informationViewList, string prnNo, string name, string mobile)
        {
            try
            {
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu);//by menu
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));

                List<int> religionIdList = new List<int>();
                List<int> genderIdList = new List<int>();
                List<int> versionOfStudyList = new List<int>();

                //search keyword 
                if (informationViewList.Any() && !String.IsNullOrEmpty(searchKey))
                {
                    foreach (string information in informationViewList)
                    {
                        switch (information)
                        {
                            case StudentListConstant.VersionOfStudy:
                                versionOfStudyList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<VersionOfStudy>(searchKey);
                                break;
                            case StudentListConstant.Gender:
                                genderIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Gender>(searchKey);
                                break;
                            case StudentListConstant.Religion:
                                religionIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Religion>(searchKey);
                                break;
                        }
                    }
                }

                return _studentProgramDao.GetAuthorizedStudentsCount(authorizedProgramLists, branchIdList, programId, sessionId, searchKey, informationViewList, prnNo, name, mobile, versionOfStudyList, genderIdList, religionIdList);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

       


        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="length"></param>
        /// <param name="programIdFrom"></param>
        /// <param name="sessionIdFrom"></param>
        /// <param name="branchIdFrom"></param>
        /// <param name="programIdTo"></param>
        /// <param name="sessionIdTo"></param>
        /// <param name="branchIdTo"></param>
        /// <param name="studentType"></param>
        /// <returns></returns>
        public int GetStudentsCountForPreviousStudentReport(List<UserMenu> userMenu, long programIdFrom,
            long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo,
            int studentType)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programIdTo));

                return _studentProgramDao.GetStudentsCountForPreviousStudentReport(programIdList, branchIdList, programIdFrom, sessionIdFrom, branchIdFrom, programIdTo, sessionIdTo, branchIdTo,
                     studentType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetStudentProgramCountByCriteriaForAttendance(List<UserMenu> usermenu, long programId, long sessionId, long[] branchId,
             long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus,
             long attendanceStatus)
        {
            try
            {
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(usermenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programId));
                return _studentProgramDao.GetStudentProgramCountByCriteriaForAttendance(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId,
                    batchDays, batchTime, batchList, paymentStatus, attendanceStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public int LoadStudentProgramCountForClassEvaluation(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long lectureId, long teacherId, long[] batchIdList, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                var programIdList = _commonHelper.ConvertIdToList(programId);
                var sessionIdList = _commonHelper.ConvertIdToList(sessionId);
                if (batchIdList.Contains(0))
                {
                    List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(branchId.ToList()), CommonHelper.ConvertSelectedAllIdList(programIdList));
                    List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(programIdList), CommonHelper.ConvertSelectedAllIdList(branchId.ToList()));
                    var batchList = _batchDao.LoadAuthorizeBatch(authProgramIdList, authBranchIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList), CommonHelper.ConvertSelectedAllIdList(campusId.ToList()), batchDays, batchTime);
                    //calling private function
                    batchList = LoadAuthorizeBatch(batchList, batchDays, batchTime);
                    // batchIdList = batchList.Select(t => t != null ? t.Id : (long?)null).ToArray();
                    batchIdList = batchList.Select(x => x.Id).ToArray();
                }
                return _studentProgramDao.LoadStudentProgramCountForClassEvaluation(lectureId, teacherId, batchIdList, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public int GetAdmittedStudentCountByBranchProgramSession(long branchId, long programId, long sessionId)
        {
            try
            {
                return _studentProgramDao.GetAdmittedStudentCountByBranchProgramSession(branchId, programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        

        

        public int GetStudentProgramCount(List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds, long[] courseId, string lectureIds)
        {
            List<long> programIdList = AuthHelper.LoadProgramIdList(usermenu);
            List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programId));
            return _studentProgramDao.GetStudentProgramCount(programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchList, paymentStatus, attendanceStatus, startDate, endDate, batchIds, courseId,lectureIds);
        }
        public int GetAuthorizedStudentProgramCount(List<UserMenu> _userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] course, int printingStatus, int? distributionStatus = null, List<ServiceBlock> serviceBlocks = null)
        {
            try
            {
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(_userMenu);//by menu
                List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu, null, _commonHelper.ConvertIdToList(programId));
                return _studentProgramDao.GetAuthorizedStudentProgramCount(programId, sessionId, branchId, authorizedProgramLists, branchIdList, campusId, batchDays, batchTime, batchName, course, printingStatus, distributionStatus, serviceBlocks);
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        


        public List<CourseAPIDto> LoadCourses(long id)
        {
            try
            {
                return _studentProgramDao.LoadCourses(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Helper Function
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userMenu"></param>
        /// <param name="studentProgram"></param>
        /// <returns></returns>
        public string CheckAuthorizationForStudentProgram(List<UserMenu> userMenu, StudentProgram studentProgram)
        {
            bool allBranch = false;
            bool allProgram = false;
            List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu);
            List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
            if (branchIdList == null)
            {
                allBranch = true;
            }
            if (programIdList == null)
            {
                allProgram = true;
            }
            var message = "";

            if (studentProgram != null)
            {
                var userBranchId = studentProgram.Batch.Branch.Id;
                var userProgramId = studentProgram.Program.Id;
                if (!allBranch)
                {
                    bool isBranchSelect = branchIdList.Contains(userBranchId);
                    if (!isBranchSelect)
                        message = "You are not authorized to this branch";
                }
                if (!allProgram)
                {
                    bool isProgramSelect = programIdList.Contains(userProgramId);
                    if (!isProgramSelect)
                        message = "You are not authorized to this program";

                }
            }

            return message;
        }

        

        //public IList<dynamic> LoadStudentImageByVariousCriteria(List<UserMenu> userMenu, int draw, int start, int length, long programId, long sessionId,
        //    long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime,
        //    int? isMissing, string programRoll = "", string nickName = "", string mobileNumber = "", string purpose = "")
        //{
        //    List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenu);
        //    List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
        //    IList<dynamic> studentImageList = new List<dynamic>();
        //    if (purpose == "Excel")
        //    {
        //        studentImageList = _studentProgramDao.GenerateStudentImageList(authorizedProgramIdList, authorizedBranchIdList, 0, 0, Int32.MaxValue, programId, sessionId,
        //    branchId, campusId, batchName, statusVal, batchDays, batchTime,
        //    isMissing, programRoll, nickName, mobileNumber);
        //    }
        //    else
        //    {
        //        studentImageList = _studentProgramDao.GenerateStudentImageList(authorizedProgramIdList, authorizedBranchIdList, draw, start, length, programId, sessionId,
        //    branchId, campusId, batchName, statusVal, batchDays, batchTime,
        //    isMissing, programRoll, nickName, mobileNumber);
        //    }
        //    return studentImageList;
        //}
        public IList<dynamic> LoadStudentImageByVariousCriteria(List<UserMenu> userMenu, int start, int length, long programId, long sessionId,long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, string programRoll = "", string nickName = "", string mobileNumber = "", string purpose = "")
        {
            List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenu);
            List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
            IList<dynamic> studentImageList = new List<dynamic>();
            if (purpose == "Excel")
            {
                studentImageList = _studentProgramDao.GenerateStudentImageList(authorizedProgramIdList, authorizedBranchIdList, 0, Int32.MaxValue, programId, sessionId, branchId, campusId, batchName, statusVal, batchDays, batchTime, programRoll, nickName, mobileNumber);
            }
            else
            {
                studentImageList = _studentProgramDao.GenerateStudentImageList(authorizedProgramIdList, authorizedBranchIdList, start, length, programId, sessionId, branchId, campusId, batchName, statusVal, batchDays, batchTime, programRoll, nickName, mobileNumber);
            }
            return studentImageList;
        }

        //public int CountStudentImageByVariousCriteria(List<UserMenu> userMenu, int draw, int start, int length, long programId, long sessionId,
        //    long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime,
        //    int? isMissing, string programRoll = "", string nickName = "", string mobileNumber = "")
        //{
        //    List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenu);
        //    List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));

        //    int studentImageList = _studentProgramDao.CountStudentImageList(authorizedProgramIdList, authorizedBranchIdList, draw, start, length, programId, sessionId,
        //     branchId, campusId, batchName, statusVal, batchDays, batchTime,
        //     isMissing, programRoll, nickName, mobileNumber);
        //    return studentImageList;
        //}
        public int CountStudentImageByVariousCriteria(List<UserMenu> userMenu, long programId, long sessionId,long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, string programRoll = "", string nickName = "", string mobileNumber = "")
        {
            List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenu);
            List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));

            int studentImageList = _studentProgramDao.CountStudentImageList(authorizedProgramIdList, authorizedBranchIdList, programId, sessionId, branchId, campusId, batchName, statusVal, batchDays, batchTime, programRoll, nickName, mobileNumber);
            return studentImageList;
        }

        //public int CountStudentImageByVariousCriteria(List<UserMenu> userMenu, long programId, long sessionId, string statusVal, long[] branchId,
        //    long[] campusId, string[] batchDays, string[] batchTime, long[] batchId)
        //{
        //    List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenu);
        //    List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
        //    int count = _studentProgramDao.CountStudentImageList(authorizedProgramIdList, authorizedBranchIdList, 0, 1, 1, programId, sessionId,
        //     branchId, campusId, batchId, statusVal, batchDays, batchTime,
        //     1);

        //    return count;
        //}
        public int CountStudentImageByVariousCriteria(List<UserMenu> userMenu, long programId, long sessionId, string statusVal, long[] branchId,
            long[] campusId, string[] batchDays, string[] batchTime, long[] batchId)
        {
            List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenu);
            List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
            int count = _studentProgramDao.CountStudentImageList(authorizedProgramIdList, authorizedBranchIdList, programId, sessionId, branchId, campusId, batchId, statusVal, batchDays, batchTime);

            return count;
        }


        private IList<Batch> LoadAuthorizeBatch(IList<Batch> batchList, string[] batchDaysList, string[] batchTimeList = null)
        {
            try
            {
                if (batchDaysList != null)
                {
                    if (!batchDaysList.Contains(SelectionType.SelelectAll.ToString()))
                        batchList = (from x in batchList where batchDaysList.Any(y => y == x.Days) select x).ToList();
                }
                if (batchTimeList != null)
                {
                    if (!batchTimeList.Contains(SelectionType.SelelectAll.ToString()))
                        batchList = (from x in batchList where batchTimeList.Any(y => y == x.FormatedBatchTime) select x).ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchList;
        }

       
        #endregion

        #region clean
        //public IList<StudentProgram> LoadStudentProgramByCriteriaForAttendance(int start, int length, List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus)
        //{
        //    try
        //    {
        //        List<long> programIdList = AuthHelper.LoadProgramIdList(usermenu);
        //        List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programId));
        //        return _studentProgramDao.LoadStudentProgramByCriteriaForAttendance(start, length, programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchList, paymentStatus, attendanceStatus);

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}

        //public IList<StudentProgramDto> LoadStudentProgramByCriteriaForClassAttendance(int start, int length, List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, long[] courseId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds)
        //{
        //    try
        //    {
        //        List<long> programIdList = AuthHelper.LoadProgramIdList(usermenu);
        //        List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programId));
        //        return _studentProgramDao.LoadStudentProgramByCriteriaForClassAttendance(start, length, programIdList, branchIdList, programId, sessionId, branchId, campusId, courseId, batchDays, batchTime, batchList, paymentStatus, attendanceStatus, startDate, endDate);

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}
        //public IList<long> LoadStudentProgramIds(List<long> courseIds)
        //{
        //    return _studentProgramDao.LoadStudentProgramIds(courseIds);
        //}
        
        //public int[] GetAdmittedPreviousStudentForParticularBranchByBranchProgramSession(long branchId, long programId, long sessionId, List<UserMenu> userMenu)
        //{
        //    try
        //    {
        //        IList<ProgramSettings> programSettingses = _programSettingDao.LoadByNextProgramIdAndNextSessionId(programId, sessionId);
        //        var prevStudentPrograms = new List<string>();
        //        var presentStudentPrograms = new List<string>();
        //        var prevbranches = _branchService.LoadBranch();

        //        string[] studentProgramsinPartricularBranch = new string[] { };
        //        foreach (var programSettingse in programSettingses)
        //        {
        //            foreach (var branch in prevbranches)
        //            {
        //                if (branch.Id == branchId)
        //                {
        //                    string[] previoustudentPrograms =
        //                    _studentProgramDao.LoadStudentsByProgramAndSessionAndBranch(programSettingse.Program.Id, programSettingse.Session.Id, branch.Id).Select(x => x.Student.RegistrationNo).ToArray();
        //                    prevStudentPrograms.AddRange(previoustudentPrograms);

        //                    string[] presentstudentPrograms =
        //                    _studentProgramDao.LoadStudentsByProgramAndSession(programSettingse.NextProgram.Id, programSettingse.NextSession.Id).Select(x => x.Student.RegistrationNo).ToArray();
        //                    presentStudentPrograms.AddRange(presentstudentPrograms);

        //                    studentProgramsinPartricularBranch =
        //                               _studentProgramDao.LoadStudentsByProgramAndSessionAndBranch(programSettingse.NextProgram.Id, programSettingse.NextSession.Id, branch.Id).Select(x => x.Student.RegistrationNo).ToArray();

        //                    break;
        //                }
        //            }

        //        }
        //        int countExternalAchievement = 0;
        //        int countTotalAchievement = 0;
        //        int countinternalAchievement = 0;

        //        var countinternalAchievementArray = presentStudentPrograms.Intersect(prevStudentPrograms);
        //        countinternalAchievement = countinternalAchievementArray.Count();
        //        countExternalAchievement = studentProgramsinPartricularBranch.Intersect(prevStudentPrograms).Count();
        //        countExternalAchievement = studentProgramsinPartricularBranch.Count() - countExternalAchievement;
        //        countTotalAchievement = countinternalAchievement + countExternalAchievement;
        //        return new int[] { countinternalAchievement, countExternalAchievement, countTotalAchievement };
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }

        //}
        //public int GetAuthorizedStudentsCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId,
        //   string[] batchDays, string[] batchTime, long[] batchName, long[] courseIds, int printingStatus, int? distributionStatus = null)
        //{
        //    List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu);//by menu
        //    List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
        //    return _studentProgramDao.GetAuthorizedStudentsCount(programId, sessionId, branchId, authorizedProgramLists, branchIdList, campusId, batchDays, batchTime, batchName, courseIds, printingStatus, distributionStatus);
        //}
        //public int GetStudentProgramCount(List<long> programIds, List<long> sessionIds, List<long> subjectIds)
        //{
        //    return _studentProgramDao.GetStudentProgramCount(programIds, sessionIds, subjectIds);
        //}

        //public int GetStudentsCountForPreviousStudentSummaryReport(List<UserMenu> usermenu, long programIdFrom, long sessionIdFrom,
        //   long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo, int studentType)
        //{
        //    try
        //    {
        //        List<long> programIdList = AuthHelper.LoadProgramIdList(usermenu);
        //        List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programIdTo));

        //        return _studentProgramDao.GetStudentsCountForPreviousStudentSummaryReport(programIdList, branchIdList, programIdFrom, sessionIdFrom, branchIdFrom, programIdTo, sessionIdTo, branchIdTo,
        //             studentType);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}
        #endregion

    }
}