﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface IServiceBlockService : IBaseService
    {
        #region Operational Function
        void Delete(ServiceBlock sb);
        void Save(ServiceBlock sb);

        #endregion

        #region Single Instances Loading Function
        ServiceBlock GetServiceBlock(long programId, long sessionId, int serviceType, int contentType);
        #endregion

        #region List Loading Function
        IList<ServiceBlock> LoadContentTypeList(long? proId, long? sessId, int? serviceTypeId);
        
        #endregion

        #region Others Function
        bool CheckedSelectedContentType(long programId, long sessionId, int serviceType, int contentType);
        bool IsBlocked(long programId, long sessionId, int serviceType, StudentProgram studentProgram);

        string WhichBlocked(long programId, long sessionId, int serviceType, StudentProgram studentProgram);

        #endregion

        #region Helper Function

        #endregion

        #region Public API

        #endregion


    }

    public class ServiceBlockService:BaseService, IServiceBlockService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Propertise & Object Initialization

        private readonly IServiceBlockDao _serviceBlockDao;
        private readonly IServiceBlockLogDao _serviceBlockLogDao;
        private readonly ICommonHelper _commonHelper;
        public ServiceBlockService(ISession session)
        {
            Session = session;
            _serviceBlockDao = new ServiceBlockDao() { Session = session };
            _serviceBlockLogDao = new ServiceBlockLogDao(){Session = session};
            _commonHelper = new CommonHelper();

        }
        #endregion

        #region Operational Function
        public void Delete(ServiceBlock sb)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    var sbl = new ServiceBlockLog
                    {
                        ProgramId = sb.Program.Id,
                        SessionId = sb.Session.Id,
                        ServiceType = sb.ServiceType,
                        ConditionType = sb.ConditionType,
                        Status = ServiceBlockLog.EntityStatus.Delete
                    };
                    _serviceBlockLogDao.Save(sbl);

                    var obj = _serviceBlockDao.LoadById(sb.Id);
                    obj.Program.ServiceBlocks.Remove(obj);
                    obj.Session.ServiceBlocks.Remove(obj);
                    _serviceBlockDao.Delete(obj);
                    trans.Commit();
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw ex;
            }
        }

        public void Save(ServiceBlock sb)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    var sbl = new ServiceBlockLog
                    {
                        ProgramId = sb.Program.Id,
                        SessionId = sb.Session.Id,
                        ServiceType = sb.ServiceType,
                        ConditionType = sb.ConditionType,
                        Status = ServiceBlockLog.EntityStatus.Active
                    };
                    _serviceBlockLogDao.Save(sbl);
                    _serviceBlockDao.Save(sb);
                    trans.Commit();
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw ex;
            }
        }

        

        #endregion

        #region Single Instances Loading Function
        public ServiceBlock GetServiceBlock(long programId, long sessionId, int serviceType, int contentType)
        {
            try
            {
                return _serviceBlockDao.GetServiceBlock(programId, sessionId, serviceType, contentType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public IList<ServiceBlock> LoadContentTypeList(long? proId, long? sessId, int? serviceTypeId)
        {
            try
            {
                return _serviceBlockDao.LoadContentTypeList(proId, sessId, serviceTypeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Others Function
        public bool CheckedSelectedContentType(long programId, long sessionId, int serviceType, int contentType)
        {
            try
            {
                return _serviceBlockDao.CheckedSelectedContentType(programId, sessionId, serviceType, contentType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsBlocked(long programId, long sessionId, int serviceType,  StudentProgram studentProgram)
        {

            var serviceBlocks = _serviceBlockDao.LoadContentTypeList(programId, sessionId, serviceType);
            bool isBlock = false;
            if (serviceBlocks.Any())
            {
                foreach (var sb in serviceBlocks)
                {
                    if (sb.ConditionType == (int)ConditionType.PaymentDue)
                    {
                        if (studentProgram.DueAmount > 0)
                        {
                            isBlock = true;
                            break;
                        }
                    }
                    else if (sb.ConditionType == (int)ConditionType.ImageDue)
                    {
                        if (!studentProgram.IsImage)
                        {
                            isBlock = true;
                            break;
                        }
                    }
                    else if (sb.ConditionType == (int)ConditionType.PoliticalReference)
                    {
                        if (studentProgram.IsPolitical)
                        {
                            isBlock = true;
                            break;
                        }
                    }
                    else if (sb.ConditionType == (int) ConditionType.Jsc)
                    {
                        if (!studentProgram.Student.IsJsc)
                        {
                            isBlock = true;
                            break;
                        }
                    }
                    else if (sb.ConditionType == (int) ConditionType.Ssc)
                    {
                        if (!studentProgram.Student.IsSsc)
                        {
                            isBlock = true;
                            break;
                        }
                    }
                    else if (sb.ConditionType == (int)ConditionType.Hsc)
                    {
                        if (!studentProgram.Student.IsHsc)
                        {
                            isBlock = true;
                            break;
                        }
                    }
                }
            }
            return isBlock;
        }

        public string WhichBlocked(long programId, long sessionId, int serviceType, StudentProgram studentProgram)
        {
            var serviceBlocks = _serviceBlockDao.LoadContentTypeList(programId, sessionId, serviceType);
            string  contentType = "  ";
            if (serviceBlocks.Any())
            {
                foreach (var sb in serviceBlocks)
                {
                    if (sb.ConditionType == (int)ConditionType.PaymentDue)
                    {
                        if (studentProgram.DueAmount > 0)
                        {
                            contentType +=
                                _commonHelper.LoadEmumToDictionary<ConditionType>()
                                    .ToList()
                                    .Where(x => x.Key == (int) ConditionType.PaymentDue)
                                    .Select(x => x.Value)
                                    .First() + " , ";
                        }
                    }
                    else if (sb.ConditionType == (int)ConditionType.ImageDue)
                    {
                        if (!studentProgram.IsImage)
                        {
                            contentType +=
                                _commonHelper.LoadEmumToDictionary<ConditionType>()
                                    .ToList()
                                    .Where(x => x.Key == (int) ConditionType.ImageDue)
                                    .Select(x => x.Value)
                                    .First() + " , ";
                        }
                    }
                    else if (sb.ConditionType == (int)ConditionType.PoliticalReference)
                    {
                        if (studentProgram.IsPolitical)
                        {
                            contentType +=
                                _commonHelper.LoadEmumToDictionary<ConditionType>()
                                    .ToList()
                                    .Where(x => x.Key == (int) ConditionType.PoliticalReference)
                                    .Select(x => x.Value)
                                    .First() + " , ";
                        }
                    }

                    else if (sb.ConditionType == (int)ConditionType.Jsc)
                    {
                        if (!studentProgram.Student.IsJsc)
                        {
                            contentType +=
                                _commonHelper.LoadEmumToDictionary<ConditionType>()
                                    .ToList()
                                    .Where(x => x.Key == (int) ConditionType.Jsc)
                                    .Select(x => x.Value)
                                    .First() + " , ";
                        }
                    }
                    else if (sb.ConditionType == (int)ConditionType.Ssc)
                    {
                        if (!studentProgram.Student.IsSsc)
                        {
                            contentType +=
                                _commonHelper.LoadEmumToDictionary<ConditionType>()
                                    .ToList()
                                    .Where(x => x.Key == (int) ConditionType.Ssc)
                                    .Select(x => x.Value)
                                    .First() + " , ";
                        }
                    }
                    else if (sb.ConditionType == (int)ConditionType.Hsc)
                    {
                        if (!studentProgram.Student.IsHsc)
                        {
                            contentType +=
                                _commonHelper.LoadEmumToDictionary<ConditionType>()
                                    .ToList()
                                    .Where(x => x.Key == (int) ConditionType.Hsc)
                                    .Select(x => x.Value)
                                    .First() + " , ";
                        }
                    }
                }
            }
            var str = contentType.Remove(contentType.Length-2);
            return str;
        }

        #endregion

        #region Helper Function

        #endregion

        #region Public API

        #endregion
        
    }
}
