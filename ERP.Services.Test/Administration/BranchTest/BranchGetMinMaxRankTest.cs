﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.AdminisTration.BranchTest;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BranchTest
{
    interface IBranchGetMinMaxRankTest
    {
        void Should_Return_Successfully_Min_Rank();
        void Should_Return_Null_Exception_Min_Rank();
        void Should_Return_Successfully_Max_Rank();
        void Should_Return_Null_Exception_Max_Rank();
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Branch")]
    public class BranchGetMinMaxRankTest : BranchBaseTest, IBranchGetMinMaxRankTest
    {
        [Fact]
        public void Should_Return_Successfully_Min_Rank()
        {
            var branch = new Branch();
            var minRank = _branchService.GetMinimumRank(branch);
            Assert.NotEqual(minRank,0);
        }
        [Fact]
        public void Should_Return_Null_Exception_Min_Rank()
        {
            Assert.Throws<NullObjectException>(() => _branchService.GetMinimumRank(null));
        }
        [Fact]
        public void Should_Return_Successfully_Max_Rank()
        {
            var branch = new Branch();
            var minRank = _branchService.GetMaximumRank(branch);
            Assert.NotEqual(minRank, 0);
        }
        [Fact]
        public void Should_Return_Null_Exception_Max_Rank()
        {
            Assert.Throws<NullObjectException>(() => _branchService.GetMinimumRank(null));
        }
    }
}
