﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Recaptcha.Web;
using Recaptcha.Web.Mvc;
//using UdvashERP.PublicAdmission.Models;
using UdvashERP.PublicAdmission.PublicAdmissionService;

namespace UdvashERP.PublicAdmission.Controllers
{
   
    public class HomeController : Controller
    {
        PublicAdmissionService.PublicAdmissionClient client = new PublicAdmissionService.PublicAdmissionClient();
        string key = ConfigurationManager.AppSettings["apiKey"];
        string org = ConfigurationManager.AppSettings["orgBussinessId"]; 
        public ActionResult Index()
        {
            return RedirectToAction("Registration", "Home");
        }

        public ActionResult CourseCalculator()
        {
            try
            {
                IList<SelectListItem> programList = new SelectList(client.GetProgram(key, org), "Id", "Name").ToList();
                ViewBag.programList = programList;
            }
            catch (Exception ex)
            {
                
            }
            return View();
        }
        [HttpPost]
        public ActionResult AjaxRequestForCourseCalculator(StudentViewModel studentviewmodel) 
        {

            try
            {
                PaymentDetailsViewModel paymentDetails = client.GetCalculatedPayementByStudent(key, org, studentviewmodel);

                if (!(string.IsNullOrEmpty(paymentDetails.ErrorMessage)))
                {                   
                    return Json(new { returnList = paymentDetails.ErrorMessage, IsSuccess = false });
                }

                ViewBag.payemntData = paymentDetails;
                return PartialView("_PaymentInformationCourseCalculator");
               
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }           
        }


        public ActionResult Registration() 
        {
            //IList<CommonModel> paymentMethod = client.GetPayementMethod(key); 
            IList<SelectListItem> paymentMethodList, programList;
            try
            {
                 paymentMethodList = new SelectList(client.GetPayementMethod(key), "Id", "Name").ToList();
                 programList = new SelectList(client.GetProgram(key, org), "Id", "Name").ToList();                
            }
            catch (Exception ex)
            {
                 paymentMethodList = null;
                 programList = null;
            }
            ViewBag.programList = programList;
            ViewBag.PaymentMethods = paymentMethodList;
            return View();
        }

        [HttpPost]
        public ActionResult Registration(int paymentMethod, string[] transactionId)  
        {

            IList<SelectListItem> programList = new SelectList(client.GetProgram(key, org), "Id", "Name").ToList();
            ViewBag.programList = programList;
            //IList<SelectListItem> programList = new SelectList(client.GetProgram(key,org), "Id", "Name").ToList(); 
            //ViewBag.programList = programList;
            IList<SelectListItem> paymentMethodList = new SelectList(client.GetPayementMethod(key), "Id", "Name").ToList();
            ViewBag.PaymentMethods = paymentMethodList;
            if (paymentMethod != 2)
            {
                ViewBag.ErrorMessage = "Please Select Pament Method";
                return View();
            }


            transactionId = transactionId.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (!(transactionId.Any()))
            {
                ViewBag.ErrorMessage = "Please give one or more transaction Id";
                return View();
            }
            ViewBag.TransactionId = transactionId;
            ViewBag.PaymentMethod = paymentMethod;


            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();

            if (String.IsNullOrEmpty(recaptchaHelper.Response))
            {
                ViewBag.ErrorMessage = "Captcha answer cannot be empty";
                // ModelState.AddModelError("", "Captcha answer cannot be empty.");
                return View();
            }

            RecaptchaVerificationResult recaptchaResult = recaptchaHelper.VerifyRecaptchaResponse();
            if (recaptchaResult != RecaptchaVerificationResult.Success)
            {
                //ModelState.AddModelError("", "Incorrect captcha answer.");
                ViewBag.ErrorMessage = "Incorrect captcha answer";
                return View();
            }         


            try
            {
                //ViewBag.SuccessMessage = "Success";
                string commaseparatedTransactionId = "";
                foreach (var tr in transactionId)
                {
                    commaseparatedTransactionId += tr + ",";
                }
                commaseparatedTransactionId = commaseparatedTransactionId.Remove(commaseparatedTransactionId.Length - 1);
                ResponseModel transactionIdValidation = client.VarifyTransectionIdsByOrg(key, org, paymentMethod, commaseparatedTransactionId);
                
                if (transactionIdValidation == null)
                {
                    ViewBag.ErrorMessage = "Please give one or more transaction Id";
                    return View();
                }

                if (transactionIdValidation.Staus == false)
                {
                    ViewBag.ErrorMessage = transactionIdValidation.Message;
                    return View();
                }
                

                IList<SelectListItem> versionOfStudyList = new SelectList(client.GetVersionOfStudy(key), "Id", "Name").ToList();
                ViewBag.versionOfStudyList = versionOfStudyList;
                IList<SelectListItem> genderList = new SelectList(client.GetGender(key), "Id", "Name").ToList();
                ViewBag.genderList = genderList;
                IList<SelectListItem> religionList = new SelectList(client.GetReligion(key), "Id", "Name").ToList();
                ViewBag.religionList = religionList;

                //IList<SelectListItem> programList = new SelectList(client.GetProgram(key,org), "Id", "Name").ToList(); 
                //ViewBag.programList = programList;



                //IList<PaymentTransactionViewModel> ptvmList = new List<PaymentTransactionViewModel>();

                //foreach (var tr in transactionId)
                //{
                //    PaymentTransactionViewModel ptvm = new PaymentTransactionViewModel();
                //    ptvm.TransactionId = tr;
                //    ptvmList.Add(ptvm);
                //}


                StudentViewModel svm = new StudentViewModel();


                //svm.StudentPayment.PaymentMethod = paymentMethod;
                //svm.StudentPayment.PaymentTransactionViewModels = ptvmList;

                return View("StudentInformation", svm);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return View();
            }           
        }
        

        [HttpPost]
        public ActionResult AjaxRequestForSaveInformation(StudentViewModel studentviewmodel)
        {

            try
            {
                IList<SelectListItem> paymentMethodList = new SelectList(client.GetPayementMethod(key), "Id", "Name").ToList();
                ViewBag.PaymentMethods = paymentMethodList;
                PaymentDetailsViewModel paymentDetails = client.GetPayementByStudent(key, org, studentviewmodel);

                if (!(string.IsNullOrEmpty(paymentDetails.ErrorMessage)))
                {                   
                    return Json(new { returnList = paymentDetails.ErrorMessage, IsSuccess = false });
                }

                ViewBag.payemntData = paymentDetails;
                return PartialView("_PaymentInformation");
               
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }           
        }

        [HttpPost]
        public ActionResult AjaxRequestForSaveInformationFinal(StudentViewModel studentviewmodel)
        {

            try
            {
                //IList<SelectListItem> paymentMethodList = new SelectList(client.GetPayementMethod(key), "Id", "Name").ToList();
                //ViewBag.PaymentMethods = paymentMethodList;
                //PaymentDetailsViewModel paymentDetails = client.GetPayementByStudent(key, org, studentviewmodel);

                //if (string.IsNullOrEmpty(paymentDetails.ErrorMessage))
                //{                   
                //    return Json(new { returnList = paymentDetails.ErrorMessage, IsSuccess = false });
                //}

                //ViewBag.payemntData = paymentDetails;
                //return PartialView("_MoneyReceiptGeneration");
                //client.(key, org, studentviewmodel);
                StudentViewModel svm = client.SaveStudent(key,org,studentviewmodel);
                if (!(string.IsNullOrEmpty(svm.ErrorMessage))) 
                {
                    return Json(new { returnList = svm.ErrorMessage, IsSuccess = false });
                }
                string r = "Your Program Roll is: " + svm.PrnNo; 
                return Json(new { returnList = r, IsSuccess = true });
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }           
        }

        [HttpPost]
        public ActionResult AjaxRequestForSession(long programId) 
        {
            try
            {
                if (programId == 0)
                    return Json(new {returnList = "Invalid Program", IsSuccess = false});
                IList<SelectListItem> sessionList =
                    new SelectList(client.GetSessionByProgram(key, org, programId), "Id", "Name").ToList();
                return Json(new {returnList = sessionList, IsSuccess = true});
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }                         
        }

        [HttpPost]
        public ActionResult AjaxRequestForBranch(long programId, long sessionId) 
        {
            try
            {
                if (programId == 0)
                    return Json(new { returnList = "Invalid Program", IsSuccess = false });
                if (sessionId == 0)
                    return Json(new { returnList = "Invalid Session", IsSuccess = false });

                IList<SelectListItem> branchList = new SelectList(client.GetBranchByProgramSession(key, org, programId, sessionId), "Id", "Name").ToList();

                //var course = client.GetCourseByProgramSession(key, programId, sessionId);

                return Json(new { returnList = branchList, IsSuccess = true });                                  
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }          
        }

        [HttpPost]
        public ActionResult AjaxRequestForCourseInformationCalculator(long programId, long sessionId) 
        {
            try
            {
                if (programId == 0)
                    return Json(new { returnList = "Invalid Program", IsSuccess = false });
                if (sessionId == 0)
                    return Json(new { returnList = "Invalid Session", IsSuccess = false });

                var courses = client.GetCourseByProgramSession(key, org, programId, sessionId);
                if (courses == null)
                    return Json(new { IsSuccess = false });
                ViewBag.CourseData = courses;
                return PartialView("_CourseSubjectViewCalculator");
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }

        }

        [HttpPost]
        public ActionResult AjaxRequestForCourseInformation(long programId, long sessionId) 
        {
            try
            {
                if (programId == 0)
                    return Json(new { returnList = "Invalid Program", IsSuccess = false });
                if (sessionId == 0)
                    return Json(new { returnList = "Invalid Session", IsSuccess = false });

                var courses = client.GetCourseByProgramSession(key, org, programId, sessionId);
                if (courses == null)
                    return Json(new { IsSuccess = false });
                ViewBag.CourseData = courses;
                return PartialView("_CourseSubjectView");
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }
                      
        }

        [HttpPost]
        public ActionResult AjaxRequestForExamBoard(long programId, long sessionId) 
        {
            try
            {
                if (programId == 0)
                    return Json(new { returnList = "Invalid Program", IsSuccess = false });
                if (sessionId == 0)
                    return Json(new { returnList = "Invalid Session", IsSuccess = false });

                var ExamBoards = client.GetExamBoardByProgramSession(key, org, programId, sessionId);
                if (ExamBoards == null)
                    return Json(new { IsSuccess = false });


                ViewBag.TotalInfo = ExamBoards.Count();
                ViewBag.HasBoardInfo = "NO";

                if (ExamBoards.Count() > 0)
                {
                    ViewBag.HasBoardInfo = "YES";
                    //foreach (var item in list)
                    //{
                    //    var acaInfo = _studentAcademicInfoService.LoadByStudentId(studentId, item.StudentExam.Id);
                    //    if (acaInfo != null)
                    //    {
                    //        var bei = list.Where(x => x.StudentExam.Id == item.StudentExam.Id).FirstOrDefault();
                    //        if (bei != null)
                    //        {
                    //            bei.Year = acaInfo.Year;
                    //            bei.Board = acaInfo.StudentBoard.Id;
                    //            bei.Roll = acaInfo.BoradRoll;
                    //        }
                    //    }
                    //}
                }

                ViewBag.ProgramStudentExamSession = ExamBoards; 
                //ViewBag.BoardList = new SelectList(client.GetStudentBoard(key), "Id", "Name").ToList();
                ViewBag.BoardList = client.GetStudentBoard(key);


                ViewBag.ExamBoardData = ExamBoards;
                return PartialView("_ExamBoardView");
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }
                      
        }

        [HttpPost]
        public ActionResult AjaxRequestForCampus(long programId, long sessionId, long branchId)  
        {
            try
            {
                if (programId == 0)
                    return Json(new { returnList = "Invalid Program", IsSuccess = false });
                if (sessionId == 0)
                    return Json(new { returnList = "Invalid Session", IsSuccess = false });
                if (branchId == 0)
                    return Json(new { returnList = "Invalid Branch", IsSuccess = false });
                IList<SelectListItem> campusList = new SelectList(client.GetCampusByProgramSessionBranch(key, org, programId, sessionId, branchId), "Id", "Name").ToList();
                return Json(new { returnList = campusList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }
           
        }
        [HttpPost]       
        public ActionResult AjaxRequestForBatchDays(long programId, long sessionId, long branchId, long campusId, long versionOfStudy, long gender)  
        {
            try
            {
                if (versionOfStudy == 0)
                    return Json(new { returnList = "Invalid Version Of Study", IsSuccess = false });
                if (gender == 0)
                    return Json(new { returnList = "Invalid Gender", IsSuccess = false });
                if (programId == 0)
                    return Json(new { returnList = "Invalid Program", IsSuccess = false });
                if (sessionId == 0)
                    return Json(new { returnList = "Invalid Session", IsSuccess = false });
                if (branchId == 0)
                    return Json(new { returnList = "Invalid Branch", IsSuccess = false });
                if (campusId == 0)
                    return Json(new { returnList = "Invalid Campus", IsSuccess = false });

                IList<SelectListItem> campusList = new SelectList(client.GetBatchDayByProgramSessionBranchCampus(key, org, programId, sessionId, branchId, campusId,versionOfStudy,gender), "Id", "Name").ToList();
                return Json(new { returnList = campusList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }
            
        }

        [HttpPost]
        public ActionResult AjaxRequestForBatchTime(long programId, long sessionId, long branchId, long campusId, string batchDays, long versionOfStudy, long gender)  
        {
            try
            {
                if (versionOfStudy == 0)
                    return Json(new { returnList = "Invalid Version Of Study", IsSuccess = false });
                if (gender == 0)
                    return Json(new { returnList = "Invalid Gender", IsSuccess = false });
                if (programId == 0)
                    return Json(new { returnList = "Invalid Program", IsSuccess = false });
                if (sessionId == 0)
                    return Json(new { returnList = "Invalid Session", IsSuccess = false });
                if (branchId == 0)
                    return Json(new { returnList = "Invalid Branch", IsSuccess = false });
                if (campusId == 0)
                    return Json(new { returnList = "Invalid Campus", IsSuccess = false });
                if (string.IsNullOrEmpty(batchDays))
                    return Json(new { returnList = "Invalid Batch Days", IsSuccess = false });

                IList<SelectListItem> campusList = new SelectList(client.GetBatchTimeByProgramSessionBranchCampusBatchday(key, org, programId, sessionId, branchId, campusId, batchDays, versionOfStudy, gender), "Id", "Name").ToList();
                return Json(new { returnList = campusList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }
            
        }

        [HttpPost]
        public ActionResult AjaxRequestForBatch(long programId, long sessionId, long branchId, long campusId, string batchDays, string batchTime, long versionOfStudy, long gender)   
        {
            try
            {
                if (versionOfStudy == 0)
                    return Json(new { returnList = "Invalid Version Of Study", IsSuccess = false });
                if (gender == 0)
                    return Json(new { returnList = "Invalid Gender", IsSuccess = false });
                if (programId == 0)
                    return Json(new { returnList = "Invalid Program", IsSuccess = false });
                if (sessionId == 0)
                    return Json(new { returnList = "Invalid Session", IsSuccess = false });
                if (branchId == 0)
                    return Json(new { returnList = "Invalid Branch", IsSuccess = false });
                if (campusId == 0)
                    return Json(new { returnList = "Invalid Campus", IsSuccess = false });
                if (string.IsNullOrEmpty(batchDays))
                    return Json(new { returnList = "Invalid Batch Days", IsSuccess = false });
                if (string.IsNullOrEmpty(batchTime))
                    return Json(new { returnList = "Invalid Batch Time", IsSuccess = false });
                IList<SelectListItem> campusList = new SelectList(client.GetBatchByProgramSessionBranchCampusBatchdayBatchtime(key, org, programId, sessionId, branchId, campusId, batchDays, batchTime, versionOfStudy, gender), "Id", "Name").ToList();
                return Json(new { returnList = campusList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }
            
        }
    }
}
