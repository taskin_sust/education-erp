﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules.Teachers;

namespace UdvashERP.Dao.Teachers
{

    public interface ITeacherDao : IBaseDao<Teacher, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        Teacher GetTeacher(string mobileNumber);
        Teacher GetTeacherByTpin(int tpin, List<long> organizationIds = null);
        //Teacher GetAuthorizedTeacherByTpin(List<long> organizationIdList, int tpin);
        TeacherImages GetTeacherImageByTeacherId(long teacherId);

        #endregion

        #region List Loading Function

        IList<Teacher> LoadTeacher(long organizationId, List<long> subjectIdList = null);
        IList<Teacher> LoadTeacher(List<long> organizationIdList, List<long> teacherActivityIdList, List<long> subjectIdList, List<int> religionEmumIdList, List<int> versionOfPriorityEmumIdList, List<int> genderEmumIdList, string nickName = "", string instutute = "", string personalMobileNumber = "", string fatherMobileNumber = "", int tpin = 0, int? start = null, int? length = null, string orderBy = "NickName", string direction = "ASC");
        IList<Teacher> LoadTeacher(long[] lectureIdList, long[] batchIdList, DateTime dateFrom, DateTime dateTo, bool? clearClassAttendance = null);
        IList<Teacher> LoadTeacher(List<long> programIdList, List<long> branchIdList, DateTime date);
        IList<Teacher> LoadByIds(long[] teacherIds);
        IList<Teacher> LoadTeacher(long[] lectureIdList, DateTime dateFrom, DateTime dateTo);
        IList<Teacher> LoadTeacherAutoComplete(string query, long organizationId, long? activityTypeId);
        List<Teacher> LoadTeacherByKeyWordSearch(int start, int length, string orderBy, string direction, List<long> authorizedOrganizationList, string keyword, List<string> informationViewList, int tpin, string nickName, string mobileNumber1, string subjectPrority, List<int> religionIdList, List<int> genderIdList, List<int> bloodGroupIdList, List<int> versionPriorityIdList);

        #endregion

        #region Others Function

        int GetTeacherCount(List<long> organizationIdList, List<long> teacherActivityIdList, List<long> subjectIdList, List<int> religionEmumIdList, List<int> versionOfPriorityEmumIdList, List<int> genderEmumIdList, string nickName = "", string instutute = "", string personalMobileNumber = "", string fatherMobileNumber = "", int tpin = 0);
        bool IsDuplicateMobileNumber(string personalMobile, long id = 0);
        int GetNewTpin();
        int GetTeacherByKeyWordSearchCount(List<long> authorizedOrganizationList, string keyword, List<string> informationViewList, int tpin, string nickName, string mobileNumber1, string subjectPrority, List<int> religionIdList, List<int> genderIdList, List<int> bloodGroupIdList, List<int> versionPriorityIdList);

        #endregion

    }

    public class TeacherDao : BaseDao<Teacher, long>, ITeacherDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function


        #endregion

        #region Single Instances Loading Function

        public Teacher GetTeacher(string mobileNumber)
        {
            return Session.QueryOver<Teacher>().Where(x => x.PersonalMobile == mobileNumber.Trim() && x.Status == Teacher.EntityStatus.Active).SingleOrDefault<Teacher>();
        }
        public TeacherImages GetTeacherImageByTeacherId(long teacherId)
        {
            return Session.QueryOver<TeacherImages>()
                .Where(x => x.Status != Teacher.EntityStatus.Delete && x.Teacher.Id == teacherId)
                .OrderBy(x => x.Id)
                .Desc.Take(1)
                .SingleOrDefault();
        }

        //public Teacher GetTeacherByTpin(int tpin)
        //{

        //    ICriteria criteria = Session.CreateCriteria<Teacher>().Add(Restrictions.Not(Restrictions.Eq("Status", Teacher.EntityStatus.Delete)));
        //    criteria.CreateAlias("Organizations", "o").Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active));
        //    if (organizationIdList.Any())
        //    {
        //        criteria.Add(Restrictions.In("o.Id", organizationIdList));
        //    }
        //    criteria.Add(Restrictions.Eq("Tpin", tpin));
        //    criteria.SetResultTransformer(Transformers.DistinctRootEntity);
        //    Teacher teacher = criteria.UniqueResult<Teacher>();
        //    return teacher;
        //    //Teacher teacher = Session.QueryOver<Teacher>().Where(x => x.Status != Teacher.EntityStatus.Delete && x.Tpin == tpin).SingleOrDefault();
        //    return teacher;
        //}

        public Teacher GetTeacherByTpin(int tpin, List<long> organizationIdList = null)
        {
            ICriteria criteria = Session.CreateCriteria<Teacher>().Add(Restrictions.Not(Restrictions.Eq("Status", Teacher.EntityStatus.Delete)));
            criteria.CreateAlias("Organizations", "o").Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active));
            if (organizationIdList!=null && organizationIdList.Any())
            {
                criteria.Add(Restrictions.In("o.Id", organizationIdList));
            }
            criteria.Add(Restrictions.Eq("Tpin", tpin));
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            Teacher teacher  = criteria.UniqueResult<Teacher>();
            return teacher;
        }

        #endregion

        #region List Loading Function

        public IList<Teacher> LoadTeacher(long organizationId, List<long> subjectIdList = null)
        {
            var organization = Session.QueryOver<Organization>().Where(x => x.Id == organizationId && x.Status == Teacher.EntityStatus.Active).SingleOrDefault<Organization>();
            //return org.
            if (subjectIdList == null)
            {
                return organization.Teachers;
            }
            else
            {
                var teacherIdList = new List<long>();
                foreach (var subjectId in subjectIdList)
                {
                    var teacherId = Session.QueryOver<TeacherSubjectPriority>()
                        .Where(x => x.Subject.Id == subjectId)
                        .Select(x => x.Teacher.Id)
                        .List<long>();
                    teacherIdList.AddRange(teacherId);
                }
                return organization.Teachers.Where(x => x.Id.In(teacherIdList.ToArray())).Distinct().ToList();
            }
        }

        public IList<Teacher> LoadTeacher(long[] lectureIdList, long[] batchIdList, DateTime dateFrom, DateTime dateTo, bool? clearClassAttendance = null)
        {
            #region prev code
            //StudentClassAttendanceDetails scadAlias = null;
            //StudentClassAttendence scaAlias = null;
            //Lecture lAlias = null;
            //Teacher tAlias = null;
            //var classAttendanceDetails =
            //    Session.QueryOver<StudentClassAttendanceDetails>(() => scadAlias)
            //        .JoinAlias(() => scadAlias.StudentClassAttendence, () => scaAlias)
            //        .JoinAlias(() => scaAlias.Lecture, () => lAlias).JoinAlias(() => scaAlias.Teacher, () => tAlias);
            //if (clearClassAttendance==null)
            //{
            //    if ((!batchIdList.Contains(SelectionType.SelelectAll)))
            //    {
            //        var lectures = Session.QueryOver<CourseProgress>().Where(x => x.Batch.Id.IsIn(batchIdList.Distinct().ToArray()) && x.HeldDate >= dateFrom && x.HeldDate < dateTo.AddDays(1)).Select(x => x.Lecture.Id).List<long>();
            //        classAttendanceDetails
            //            .Where(x => lAlias.Id.IsIn(lectures.Distinct().ToArray()));
            //    }
            //}




            //if ((!lectureIdList.Contains(SelectionType.SelelectAll)))
            //{
            //    classAttendanceDetails
            //        .Where(x => lAlias.Id.IsIn(lectureIdList.Distinct().ToArray()));
            //}

            //classAttendanceDetails = classAttendanceDetails.Where(x => scaAlias.HeldDate >= dateFrom && scaAlias.HeldDate < dateTo.AddDays(1));
            //var classAttendanceList = classAttendanceDetails.Where(x => x.Status == StudentClassAttendence.EntityStatus.Active).Select(x => x.StudentClassAttendence.Id).List<long>().Distinct().ToArray();
            //var teacherIdList =
            //    Session.QueryOver<StudentClassAttendence>()
            //        .Where(x => x.Id.IsIn(classAttendanceList))
            //        .Select(x => x.Teacher.Id)
            //        .List<long>()
            //        .Distinct()
            //        .ToList();
            //var teacherList = Session.QueryOver<Teacher>()
            //    .Where(x => x.Id.IsIn(teacherIdList.Distinct().ToArray())).List<Teacher>().Distinct().ToList();
            //return teacherList.Where(x => x.Status == Teacher.EntityStatus.Active).ToList(); 
            #endregion

            var query = @"select distinct t.Id,t.FullName,t.NickName,t.HscPassingYear from StudentClassAttendence sca 
                        --inner join StudentClassAttendanceDetails scad on scad.StudentClassAttendanceId=sca.Id
                        inner join Lecture l on l.Id=sca.LectureId
                        inner join LectureSettings ls on l.LectureSettingsId=ls.Id
                        inner join teacher t on t.id=sca.teacherid
                        and sca.lectureid in({0}) 
                        and sca.HeldDate>='{1}' and sca.HeldDate<'{2}'";
            query = string.Format(query, string.Join(",", lectureIdList.Distinct().ToList()), dateFrom, dateTo.AddDays(1));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<Teacher>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<Teacher>().ToList();
            return list;
        }

        public IList<Teacher> LoadTeacher(List<long> organizationIdList, List<long> teacherActivityIdList, List<long> subjectIdList, List<int> religionEmumIdList, List<int> versionOfPriorityEmumIdList, List<int> genderEmumIdList, string nickName = "", string instutute = "", string personalMobileNumber = "", string fatherMobileNumber = "", int tpin = 0, int? start = null, int? length = null, string orderBy = "Tpin", string direction = "ASC")
        {
            ICriteria criteria = GetLoadTeacherInitialCriteriaQuery(organizationIdList, teacherActivityIdList, subjectIdList, religionEmumIdList, versionOfPriorityEmumIdList, genderEmumIdList, nickName, instutute, personalMobileNumber, fatherMobileNumber, tpin);

            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(direction == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }

            IList<Teacher> teacherlist = new List<Teacher>();
            if (start != null && length != null)
            {
                teacherlist = criteria.List<Teacher>().Skip(Convert.ToInt32(start)).Take(Convert.ToInt32(length)).ToList();
            }
            else
            {
                teacherlist = criteria.List<Teacher>();
            }


            return teacherlist;
        }

        public IList<Teacher> LoadTeacher(List<long> programIdList, List<long> branchIdList, DateTime date)
        {
            var classEntryDetails =
                Session.Query<TeacherClassEntryDetails>()
                    .Where(
                        x =>
                            x.Status == Teacher.EntityStatus.Active &&
                            x.TeacherClassEntry.Status == Teacher.EntityStatus.Active &&
                            x.TeacherClassEntry.HeldDate == date.Date
                            ).ToList();
            var classTypesArray = new[] { "Regular Class", "Solve Class", "Special Class", "Review Class" };
            //var teacherList = classEntryDetails.Where(x => ((x.TeacherClassEntry.Branch != null && x.TeacherClassEntry.Branch.Id.In(branchIdList.ToArray())) || x.TeacherClassEntry.Branch == null) && x.TeacherClassEntry.TeacherClassType.Name.In(classTypesArray) 
            //    && x.TeacherClassEntry.Program.Id.In(programIdList.ToArray())
            //    )
            //    .Select(x => x.Teacher);
            var tList = new List<Teacher>();
            foreach (var x in classEntryDetails)
            {
                if (x.TeacherClassEntry.Program != null && x.TeacherClassEntry.Session != null)
                {
                    if (x.TeacherClassEntry.Branch != null)
                    {
                        if (x.TeacherClassEntry.Branch.Id.In(branchIdList.ToArray()) &&
                            x.TeacherClassEntry.TeacherClassType.Name.In(classTypesArray)
                            && x.TeacherClassEntry.Program != null &&
                            x.TeacherClassEntry.Program.Id.In(programIdList.ToArray()))
                        {
                            tList.Add(x.Teacher);
                        }
                    }
                    else
                    {
                        if (
                             x.TeacherClassEntry.TeacherClassType.Name.In(classTypesArray)
                             && x.TeacherClassEntry.Program != null &&
                             x.TeacherClassEntry.Program.Id.In(programIdList.ToArray()))
                        {
                            tList.Add(x.Teacher);
                        }
                    }
                }
            }
            return tList.Distinct().ToList();
        }

        public IList<Teacher> LoadByIds(long[] teacherIds)
        {
            return Session.QueryOver<Teacher>().Where(x => x.Id.IsIn(teacherIds) && x.Status == Teacher.EntityStatus.Active).List<Teacher>();
        }

        public IList<Teacher> LoadTeacher(long[] lectureIdList, DateTime dateFrom, DateTime dateTo)
        {
            var query = @"select distinct t.Id,t.FullName,t.NickName,t.HscPassingYear from StudentClassAttendence sca 
                        --inner join StudentClassAttendanceDetails scad on scad.StudentClassAttendanceId=sca.Id
                        inner join Lecture l on l.Id=sca.LectureId
                        inner join LectureSettings ls on l.LectureSettingsId=ls.Id
                        inner join teacher t on t.id=sca.teacherid
                        and sca.lectureid in({0}) 
                        and  sca.HeldDate>='{1}' and  sca.HeldDate<'{2}'";
            query = string.Format(query, string.Join(",", lectureIdList), dateFrom, dateTo.AddDays(1));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<Teacher>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<Teacher>().ToList();
            return list;
        }

        public IList<Teacher> LoadTeacherAutoComplete(string query, long organizationId, long? activityTypeId)
        {
            var criteria = Session.CreateCriteria<Teacher>().Add(Restrictions.Eq("Status", Teacher.EntityStatus.Active));
            criteria.CreateAlias("Organizations", "o").Add(Restrictions.Eq("o.Id", organizationId));
            if (activityTypeId != null)
            {
                criteria.CreateAlias("TeacherActivityPriorities", "tap")
                    .Add(Restrictions.Eq("tap.Status", TeacherActivityPriority.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("tap.TeacherActivity.Id", activityTypeId));
            }
            if (!String.IsNullOrEmpty(query))
            {
                criteria.Add(Restrictions.Like("NickName", query, MatchMode.Anywhere));
            }
            criteria.AddOrder(Order.Asc("NickName"));
            criteria.SetFirstResult(0).SetMaxResults(10);
            return criteria.List<Teacher>();

        }

        public List<Teacher> LoadTeacherByKeyWordSearch(int start, int length, string orderBy, string direction, List<long> authorizedOrganizationList, string keyword, List<string> informationViewList, int tpin, string nickName, string mobileNumber1, string subjectPrority, List<int> religionIdList, List<int> genderIdList, List<int> bloodGroupIdList, List<int> versionPriorityIdList)
        {
            ICriteria criteria = GetTeacherByKeyWordSearchCriteriaQuery(authorizedOrganizationList, keyword, informationViewList, tpin, nickName, mobileNumber1, subjectPrority, religionIdList, genderIdList, bloodGroupIdList, versionPriorityIdList);

            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(direction == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }

            IList<Teacher> teacherlist = new List<Teacher>();
            if (start != null && length != null)
            {
                teacherlist = criteria.List<Teacher>().Skip(Convert.ToInt32(start)).Take(Convert.ToInt32(length)).ToList();
            }
            else
            {
                teacherlist = criteria.List<Teacher>();
            }
            return teacherlist.ToList();
        }

        #endregion

        #region Others Function

        public int GetTeacherCount(List<long> organizationIdList, List<long> teacherActivityIdList, List<long> subjectIdList, List<int> religionEmumIdList, List<int> versionOfPriorityEmumIdList, List<int> genderEmumIdList, string nickName = "", string instutute = "", string personalMobileNumber = "", string fatherMobileNumber = "", int tpin = 0)
        {
            ICriteria criteria = GetLoadTeacherInitialCriteriaQuery(organizationIdList, teacherActivityIdList, subjectIdList, religionEmumIdList, versionOfPriorityEmumIdList, genderEmumIdList, nickName, instutute, personalMobileNumber, fatherMobileNumber, tpin);

            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            criteria.SetProjection(Projections.CountDistinct("Id"));
            return Convert.ToInt32(criteria.UniqueResult());
        }
        public int GetTeacherByKeyWordSearchCount(List<long> authorizedOrganizationList, string keyword, List<string> informationViewList, int tpin, string nickName, string mobileNumber1, string subjectPrority, List<int> religionIdList, List<int> genderIdList, List<int> bloodGroupIdList, List<int> versionPriorityIdList)
        {
            ICriteria criteria = GetTeacherByKeyWordSearchCriteriaQuery(authorizedOrganizationList, keyword, informationViewList, tpin, nickName, mobileNumber1, subjectPrority, religionIdList, genderIdList, bloodGroupIdList, versionPriorityIdList);
 
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            criteria.SetProjection(Projections.CountDistinct("Id"));
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public bool IsDuplicateMobileNumber(string personalMobile, long id = 0)
        {
            var query = Session.QueryOver<Teacher>();
            query.Where(x => x.PersonalMobile == personalMobile && x.Id != id);
            if (query.RowCount() > 0) return true;
            return false;
        }

        public int GetNewTpin()
        {
            int tpin = Session.QueryOver<Teacher>()
                .Where(x => x.Status != Teacher.EntityStatus.Delete)
                .Select(Projections
                .ProjectionList()
                .Add(Projections.Max<Teacher>(x => x.Tpin)))
                .List<int>().First() + 1;
            return tpin;
        }

        #endregion

        #region Helper Function

        private ICriteria GetLoadTeacherInitialCriteriaQuery(List<long> organizationIdList, List<long> teacherActivityIdList, List<long> subjectIdList, List<int> religionEmumIdList, List<int> versionOfPriorityEmumIdList, List<int> genderEmumIdList, string nickName = "", string instutute = "", string personalMobileNumber = "", string fatherMobileNumber = "", int tpin = 0)
        {
            ICriteria criteria = Session.CreateCriteria<Teacher>().Add(Restrictions.Not(Restrictions.Eq("Status", Teacher.EntityStatus.Delete)));

            if (!String.IsNullOrEmpty(nickName)) { criteria.Add(Restrictions.Like("NickName", nickName, MatchMode.Anywhere)); }
            if (!String.IsNullOrEmpty(instutute)) { criteria.Add(Restrictions.Like("Institute", instutute, MatchMode.Anywhere)); }
            if (!String.IsNullOrEmpty(personalMobileNumber)) { criteria.Add(Restrictions.Like("PersonalMobile", personalMobileNumber, MatchMode.Anywhere)); }
            if (!String.IsNullOrEmpty(fatherMobileNumber)) { criteria.Add(Restrictions.Like("FathersMobile", fatherMobileNumber, MatchMode.Anywhere)); }
            if (tpin != 0)
            {
                criteria.Add(Restrictions.Eq("Tpin", tpin));
            }

            criteria.CreateAlias("Organizations", "o").Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active));
            criteria.CreateAlias("TeacherActivityPriorities", "tap").Add(Restrictions.Eq("tap.Status", TeacherActivityPriority.EntityStatus.Active));
            criteria.CreateAlias("TeacherSubjectPriorities", "tsp").Add(Restrictions.Eq("tsp.Status", TeacherSubjectPriority.EntityStatus.Active));
            criteria.CreateAlias("TeacherVersionOfStudyPriorities", "tvop").Add(Restrictions.Eq("tvop.Status", TeacherVersionOfStudyPriority.EntityStatus.Active));
            if (!organizationIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("o.Id", organizationIdList));
            }
            if (!teacherActivityIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("tap.TeacherActivity.Id", teacherActivityIdList));
            }
            if (!subjectIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("tsp.Subject.Id", subjectIdList));
            }
            if (!religionEmumIdList.Contains((int)SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("Religion", religionEmumIdList));
            }
            if (!versionOfPriorityEmumIdList.Contains((int)SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("tvop.VersionOfStudy", versionOfPriorityEmumIdList));
            }
            if (!genderEmumIdList.Contains((int)SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("Gender", genderEmumIdList));
            }

            return criteria;
        }


        private ICriteria GetTeacherByKeyWordSearchCriteriaQuery(List<long> authorizedOrganizationList, string keyword, List<string> informationViewList, int tpin, string nickName, string mobileNumber1, string subjectPrority, List<int> religionIdList, List<int> genderIdList, List<int> bloodGroupIdList, List<int> versionPriorityIdList)
        {
            ICriteria criteria = Session.CreateCriteria<Teacher>().Add(Restrictions.Not(Restrictions.Eq("Status", Teacher.EntityStatus.Delete)));

            if (!String.IsNullOrEmpty(nickName)) { criteria.Add(Restrictions.Like("NickName", nickName, MatchMode.Anywhere)); }
            if (!String.IsNullOrEmpty(mobileNumber1)) { criteria.Add(Restrictions.Like("PersonalMobile", mobileNumber1, MatchMode.Anywhere)); }
            if (tpin != 0)
            {
                criteria.Add(Restrictions.Eq("Tpin", tpin));
            }

            criteria.CreateAlias("Organizations", "o").Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active));
            criteria.CreateAlias("TeacherActivityPriorities", "tap").Add(Restrictions.Eq("tap.Status", TeacherActivityPriority.EntityStatus.Active));
            criteria.CreateAlias("TeacherSubjectPriorities", "tsp").Add(Restrictions.Eq("tsp.Status", TeacherSubjectPriority.EntityStatus.Active));
            criteria.CreateAlias("TeacherVersionOfStudyPriorities", "tvop").Add(Restrictions.Eq("tvop.Status", TeacherVersionOfStudyPriority.EntityStatus.Active));
            if (!authorizedOrganizationList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("o.Id", authorizedOrganizationList));
            }
            if (!String.IsNullOrEmpty(subjectPrority))
            {
                string[] subjectProrityArray = subjectPrority.Split(',');
                var disjunctionSubject = Restrictions.Disjunction(); // for OR statement 
                if (subjectProrityArray.Any())
                {
                    criteria.CreateAlias("tsp.Subject", "s").Add(Restrictions.Eq("s.Status", Subject.EntityStatus.Active));
                    foreach (string subjectName in subjectProrityArray)
                    {
                        disjunctionSubject.Add(Restrictions.Like("s.Name", subjectName, MatchMode.Anywhere));
                    }
                    criteria.Add(disjunctionSubject);
                }
            }

            if (!String.IsNullOrEmpty(keyword))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                if (informationViewList.Any())
                {
                    foreach (string information in informationViewList)
                    {
                        if (information == TeacherSearchConstants.Tpin)
                        {
                            disjunction.Add(Restrictions.Like(Projections.Cast(NHibernateUtil.String, Projections.Property<Teacher>(x => x.Tpin)), keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.NickName)
                        {
                            disjunction.Add(Restrictions.Like("NickName", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.MobileNumber1)
                        {
                            disjunction.Add(Restrictions.Like("PersonalMobile", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.Organization)
                        {
                            disjunction.Add(Restrictions.Like("o.ShortName", keyword, MatchMode.Anywhere));
                            disjunction.Add(Restrictions.Like("o.Name", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.FullName)
                        {
                            disjunction.Add(Restrictions.Like("FullName", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.FatherName)
                        {
                            disjunction.Add(Restrictions.Like("FatherName", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.Institute)
                        {
                            disjunction.Add(Restrictions.Like("Institute", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.MobileNumber2)
                        {
                            disjunction.Add(Restrictions.Like("AlternativeMobile", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.RoomMateMobileNumber)
                        {
                            disjunction.Add(Restrictions.Like("RoomMatesMobile", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.FatherMobileNumber)
                        {
                            disjunction.Add(Restrictions.Like("FathersMobile", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.MotherMobileNumber)
                        {
                            disjunction.Add(Restrictions.Like("MothersMobile", keyword, MatchMode.Anywhere));
                        }
                        //enum search keywords
                        if (religionIdList.Any())
                        {
                            disjunction.Add(Restrictions.In("Religion", religionIdList));
                        }
                        if (genderIdList.Any())
                        {
                            disjunction.Add(Restrictions.In("Gender", genderIdList));
                        }
                        if (bloodGroupIdList.Any())
                        {
                            disjunction.Add(Restrictions.In("BloodGroup", bloodGroupIdList));
                        }
                        if (versionPriorityIdList.Any())
                        {
                            disjunction.Add(Restrictions.In("tvop.VersionOfStudy", versionPriorityIdList));
                        }
                        //enum search keywords End

                        if (information == TeacherSearchConstants.DateOfBirth)
                        {
                            disjunction.Add(Restrictions.Like(Projections.Cast(NHibernateUtil.String, Projections.Property<Teacher>(x => x.DateOfBirth)), keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.Department)
                        {
                            disjunction.Add(Restrictions.Like("Department", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.HscPassingYear)
                        {

                            disjunction.Add(Restrictions.Like(Projections.Cast(NHibernateUtil.String, Projections.Property<Teacher>(x => x.HscPassingYear)), keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.Email)
                        {
                            disjunction.Add(Restrictions.Like("Email", keyword, MatchMode.Anywhere));
                        }
                        if (information == TeacherSearchConstants.ActivityPriority)
                        {
                            criteria.CreateAlias("tap.TeacherActivity", "ta").Add(Restrictions.Eq("ta.Status", TeacherActivity.EntityStatus.Active));
                            disjunction.Add(Restrictions.Like("ta.Name", keyword, MatchMode.Anywhere));
                        }
                    }
                    criteria.Add(disjunction);
                }
            }



            return criteria;
        }
        #endregion


    }
}
