﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class LeaveFactory : ObjectFactoryBase<Leave>
    {
        private OrganizationFactory _organizationFactory;
        private readonly ILeaveService _leaveService;
        public LeaveFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _leaveService = new LeaveService(_session);
            _organizationFactory = new OrganizationFactory(this, session);
        }


        #region Create

        public LeaveFactory WithNameAndIsPublic()
        {
            Object.Name = "Leave_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString();
            Object.IsPublic = false;

            return this;
        }

        public LeaveFactory CreateWith(long id, bool? isPublic, int? payType, int? repeatType, int? noOfDays
            , int? startFrom, bool? isCarryforward, int? maxCarryDays, bool? isTakingLimit, int? maxTakingLimit,
            int? applicationtype, int? applicationDays, bool? isEncash, int? minEncashReserveDays,
            DateTime? effectiveDate, DateTime? closingDate, bool? isMale, bool? isFemale, bool? isProbation, bool? isPermanent,
            bool? isPartTime, bool? isContractual, bool? isIntern, bool? isSingle, bool? isMarried, bool? isWidow, bool? isWidower,
            bool? isDevorced)
        {
            Object = new Leave()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = Leave.EntityStatus.Active,
                Name = "Leave_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString(),
                IsPublic = isPublic,
                PayType = payType,
                RepeatType = repeatType,
                NoOfDays = noOfDays,
                StartFrom = startFrom,
                IsCarryforward = isCarryforward,
                MaxCarryDays = maxCarryDays,
                IsTakingLimit = isTakingLimit,
                MaxTakingLimit = maxTakingLimit,
                Applicationtype = applicationtype,
                ApplicationDays = applicationDays,
                IsEncash = isEncash,
                MinEncashReserveDays = minEncashReserveDays,
                EffectiveDate = effectiveDate,
                ClosingDate = closingDate,
                IsMale = isMale,
                IsFemale = isFemale,
                IsProbation = isProbation,
                IsPermanent = isPermanent,
                IsPartTime = isPartTime,
                IsContractual = isContractual,
                IsIntern = isIntern,
                IsSingle = isSingle,
                IsMarried = isMarried,
                IsWidow = isWidow,
                IsWidower = isWidower,
                IsDevorced = isDevorced
            };
            //Object.Organization = _organizationFactory.Create().Persist().Object;
            SingleObjectList.Add(Object);

            return this;
        }

        public LeaveFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        public LeaveFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public LeaveFactory Create()
        {
            Object = new Leave()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = Leave.EntityStatus.Active,
                Name = "Leave_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString(),
                IsPublic = true,
                PayType = 1,
                RepeatType = 1,
                NoOfDays = 2,
                StartFrom = 1,
                IsCarryforward = true,
                MaxCarryDays = 10,
                IsTakingLimit = true,
                MaxTakingLimit = 5,
                Applicationtype = 1,
                ApplicationDays = 1,
                IsEncash = true,
                MinEncashReserveDays = 2,
                EffectiveDate = DateTime.Now.AddDays(-5),
                IsMale = true,
                IsFemale = true,
                IsProbation = true,
                IsPermanent = true,
                IsPartTime = true,
                IsContractual = true,
                IsIntern = true,
                IsSingle = true,
                IsMarried = true,
                IsWidow = true,
                IsWidower = true,
                IsDevorced = true
            };

            SingleObjectList.Add(Object);

            return this;
        }

        public LeaveFactory WithEffectiveDate(DateTime effectiveDate)
        {
            Object.EffectiveDate = effectiveDate;
            return this;
        }


        public LeaveFactory CreateMore(int num)
        {
            _organizationFactory.Create().Persist();
            for (int i = 0; i < num; i++)
            {
                var obj = new Leave()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Leave.EntityStatus.Active,
                    Name = "Leave_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString(),
                    IsPublic = true,
                    PayType = 1,
                    RepeatType = 1,
                    NoOfDays = 2,
                    StartFrom = 1,
                    IsCarryforward = true,
                    MaxCarryDays = 10,
                    IsTakingLimit = true,
                    MaxTakingLimit = 5,
                    Applicationtype = 1,
                    ApplicationDays = 1,
                    IsEncash = true,
                    MinEncashReserveDays = 2,
                    EffectiveDate = DateTime.Now,
                    IsMale = true,
                    IsFemale = true,
                    IsProbation = true,
                    IsPermanent = true,
                    IsPartTime = true,
                    IsContractual = true,
                    IsIntern = true,
                    IsSingle = true,
                    IsMarried = true,
                    IsWidow = true,
                    IsWidower = true,
                    IsDevorced = true,
                    Organization = _organizationFactory.Object
                };
                ObjectList.Add(obj);
            }
            return this;
        }

        public LeaveFactory CreateMore(int num,Organization organization)
        {
            for (int i = 0; i < num; i++)
            {
                var obj = new Leave()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Leave.EntityStatus.Active,
                    Name = "Leave_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString(),
                    IsPublic = true,
                    PayType = 1,
                    RepeatType = 1,
                    NoOfDays = 2,
                    StartFrom = 1,
                    IsCarryforward = true,
                    MaxCarryDays = 10,
                    IsTakingLimit = true,
                    MaxTakingLimit = 5,
                    Applicationtype = 1,
                    ApplicationDays = 1,
                    IsEncash = true,
                    MinEncashReserveDays = 2,
                    EffectiveDate = DateTime.Now,
                    IsMale = true,
                    IsFemale = true,
                    IsProbation = true,
                    IsPermanent = true,
                    IsPartTime = true,
                    IsContractual = true,
                    IsIntern = true,
                    IsSingle = true,
                    IsMarried = true,
                    IsWidow = true,
                    IsWidower = true,
                    IsDevorced = true,
                    Organization = organization
                };
                ObjectList.Add(obj);
            }
            return this;
        }
        public LeaveFactory Persist()
        {
            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    _leaveService.Save(x);
                }
            });            

            ObjectList.ForEach(item =>
            {
                if (item.Id == 0)
                {
                    _leaveService.Save(item);
                }
            });
            
            return this;
        }
        #endregion

        #region Others

        #endregion

        #region CleanUp

        public void Cleanup()
        {
            DeleteAll();
            _organizationFactory.Cleanup();
        }

        #endregion

    }
}
