﻿using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ReffererTest
{
    interface IReffererDeleteTest
    {
        #region Basic Test
        #endregion
    }
    [Trait("Area", "Administration")]
    [Trait("Service", "Refferer")]
    public class ReffererDeleteTest : ReffererBaseTest, IReffererDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            ReferrerFactory referrerFactory =
                    _referrerFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithCode(_code)
                    .Persist();
            Referrer reffererObj = _referenceService.LoadById(referrerFactory.Object.Id);
            var success = _referenceService.Delete(reffererObj);
            Assert.True(success);
            reffererObj = _referenceService.LoadById(referrerFactory.Object.Id);
            Assert.Null(reffererObj);
        }

        [Fact]
        public void Delete_Should_Throw_Dependency_Exception()
        {
            ReferrerFactory referrerFactory =
                    _referrerFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithCode(_code)
                    .Persist();
            Referrer reffererObj = _referenceService.LoadById(referrerFactory.Object.Id);
            reffererObj.StudentPayments=new List<StudentPayment>(){new StudentPayment()};
            Assert.Throws<DependencyException>(()=>_referenceService.Delete(reffererObj));
            
        }
        #endregion

    }
}
