using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{


    public partial class ExtradayAllowanceSettingMap : BaseClassMap<ExtradayAllowanceSetting,long>
    {

        public ExtradayAllowanceSettingMap()
        {
			Table("HR_ExtraDayAllowanceSetting");
			LazyLoad();
			References(x => x.Organization).Column("OrganizationId");
			Map(x => x.Name).Column("Name").Not.Nullable();
            Map(x => x.IsGazetted).Column("IsGazetted").Not.Nullable();
            Map(x => x.IsManagement).Column("IsManagement").Not.Nullable();
            Map(x => x.IsWeekend).Column("IsWeekend").Not.Nullable();
			Map(x => x.CalculationOn).Column("CalculationOn").Not.Nullable();
			Map(x => x.MultiplyingFactor).Column("MultiplyingFactor").Not.Nullable();
			Map(x => x.EffectiveDate).Column("EffectiveDate").Not.Nullable();
			Map(x => x.ClosingDate).Column("ClosingDate");
			Map(x => x.IsPermanent).Column("IsPermanent").Not.Nullable();
			Map(x => x.IsProbation).Column("IsProbation").Not.Nullable();
			Map(x => x.IsPartTime).Column("IsPartTime").Not.Nullable();
			Map(x => x.IsContractual).Column("IsContractual").Not.Nullable();
            Map(x => x.IsIntern).Column("IsIntern").Not.Nullable();
        }
    }
}
