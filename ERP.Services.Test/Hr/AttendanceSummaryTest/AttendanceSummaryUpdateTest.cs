﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSummaryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSummary")]
    public class AttendanceSummaryUpdateTest:AttendanceSummaryBaseTest
    {
        [Fact]
        public void Should_Return_PostAttendanceReset_Successfully()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            
            var tm = PersistTeamMember(joiningDate, dateTo, org);

            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var dept = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016,12,1), tm);

            DateTime holyDayDate = new DateTime(2016,12,2);

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            nightWorkFactory.CreateWith(new DateTime(2016,12, 7), (int)HolidayWorkApprovalStatus.Half)
                .WithTeamMember(tm).Persist();

            _overTimeFactory.CreateWith(new DateTime(2016, 12, 10), new DateTime(2000, 01, 01)).WithTeamMember(tm).Persist();
            
            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, menu);
            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016,12, 18), 
                new DateTime(2016,12,19), "test", 1).WithTeamMember(tm).Persist();

            _attendanceSummaryService.PostAttendanceReset(menu, startDate,endDate,commonHelper.ConvertIdToList(org.Id),commonHelper.ConvertIdToList(branch.Id),
                commonHelper.ConvertIdToList(campus.Id),commonHelper.ConvertIdToList(dept.Id),tm.Pin.ToString());

            var a = _attendanceSummaryService.LoadAttendanceSummeryReport(menu, org.Id.ToString(), dept.Id.ToString(),
                branch.Id.ToString(), null, "", startDate.ToString(), endDate.ToString(), "", 0, 100, "", "");
            Assert.NotEqual(0, a.Count());
        }

        [Fact]
        public void Should_Return_PostAttendanceReset_Exception_For_Usermenu()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var dept = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);

            DateTime holyDayDate = new DateTime(2016,12,2);

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));


            nightWorkFactory.CreateWith(new DateTime(2016,12, 7), (int)HolidayWorkApprovalStatus.Half)
                .WithTeamMember(tm).Persist();

            _overTimeFactory.CreateWith(new DateTime(2016,12,11),new DateTime(2000, 01, 01)).WithTeamMember(tm).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, menu);
            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016,12, 18), new DateTime(2016,12,19), "test", 1).WithTeamMember(tm).Persist();


            Assert.Throws<InvalidDataException>(
                () =>
                    _attendanceSummaryService.PostAttendanceReset(null, startDate, endDate,
                        commonHelper.ConvertIdToList(org.Id), commonHelper.ConvertIdToList(branch.Id),
                        commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(dept.Id),
                        tm.Pin.ToString()));
        }

    }
}
