using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface IQuotationDao : IBaseDao<Quotation, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        Quotation GetQuotationByItem(long id, long? itemId);

        #endregion

        #region List Loading Function

        IList<Quotation> LoadQuotation(int start, int length, string orderBy, string orderDir, List<long> authOrganizationIdList, List<long> authBranchIdList, string itemName,
            string deadLine, QuotationStatus? quotationStatus = null);
        IList<ManageQuotationDto> LoadSupplierManageQuotation(int start, int length, string supplierId, string organizationId, string branchId, string itemId, string status);
        List<QuotationReportDto> LoadQuotationReport(int start, int length, string orderBy, string orderDir, List<long> authOrganizationIdList, List<long> authBranchIdList,
            int[] purposeList, long[] itemGroupIdList, long[] itemIdList, string[] programSessionIds, int quotationStatus, string dateFrom, string dateTo);

        #endregion

        #region Others Function

        int LoadSupplierManageQuotationCount(string supplierId, string organizationId, string branchId, string itemId, string status);
        int LoadQuotationCount(string orderBy, string orderDir, List<long> authOrganizationIdList, List<long> authBranchIdList, string itemName, string deadLine, QuotationStatus? quotationStatus = null);
        bool IsDuplicateQuotationNo(string quotationNo, long id);
        int LoadQuotationReportCount(List<long> authOrganizationIdList, List<long> authBranchIdList,
            int[] purposeList, long[] itemGroupIdList, long[] itemIdList, string[] programSessionIds, int quotationStatus, string dateFrom, string dateTo);

        #endregion

    }

    public class QuotationDao : BaseDao<Quotation, long>, IQuotationDao
    {

        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public Quotation GetQuotationByItem(long id, long? itemId)
        {
            return
                Session.QueryOver<Quotation>()
                    .Where(x => x.Item.Id == itemId && x.Id == id && x.Status == Quotation.EntityStatus.Active)
                    .Take(1)
                    .SingleOrDefault<Quotation>();
        }

        #endregion

        #region List Loading Function

        public IList<Quotation> LoadQuotation(int start, int length, string orderBy, string orderDir, List<long> authOrganizationIdList, List<long> authBranchIdList,
            string itemName, string deadLine, QuotationStatus? quotationStatus = null)
        {
            ICriteria criteria = GetQuotationCriteria(orderBy, orderDir, authOrganizationIdList, authBranchIdList, itemName, deadLine, quotationStatus);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Quotation>();
        }

        public IList<ManageQuotationDto> LoadSupplierManageQuotation(int start, int length, string supplierId, string organizationId, string branchId, string itemId, string status)
        {
            string query = "DECLARE @rowsperpage INT DECLARE @start INT SET @start = " + (start + 1) + " SET @rowsperpage = " + length + " ";

            query += LoadSupplierQuotationManageQuery(supplierId, organizationId, branchId, itemId, status);

            if (length > 0)
            {
                query += " and a.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ManageQuotationDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<ManageQuotationDto>().ToList();
            return list;
        }

        public List<QuotationReportDto> LoadQuotationReport(int start, int length, string orderBy, string orderDir, List<long> authOrganizationIdList, List<long> authBranchIdList,
            int[] purposeList, long[] itemGroupIdList, long[] itemIdList, string[] programSessionIds, int quotationStatus, string dateFrom, string dateTo)
        {
            string query = "DECLARE @rowsperpage INT DECLARE @start INT SET @start = " + (start + 1) + " SET @rowsperpage = " + length + " ";

            query += @"Select * from(";

            query += GetReportQuoery(authOrganizationIdList, authBranchIdList, purposeList, itemGroupIdList, itemIdList, programSessionIds, quotationStatus,
                dateFrom, dateTo);
            query += ") as a where 1=1";
            if (length > 0)
            {
                query += " and a.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<QuotationReportDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<QuotationReportDto>().ToList();
            return list;
        }

        #endregion

        #region Others Function

        public int LoadSupplierManageQuotationCount(string supplierId, string organizationId, string branchId, string itemId, string status)
        {
            string query = LoadSupplierQuotationManageQuery(supplierId, organizationId, branchId, itemId, status, true);

            IQuery iQuery = Session.CreateSQLQuery(query);
            return (int)iQuery.UniqueResult();
        }

        public int LoadQuotationCount(string orderBy, string orderDir, List<long> authOrganizationIdList, List<long> authBranchIdList, string itemName, string deadLine,
            QuotationStatus? quotationStatus = null)
        {
            ICriteria criteria = GetQuotationCriteria(orderBy, orderDir, authOrganizationIdList, authBranchIdList, itemName, deadLine, quotationStatus, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public bool IsDuplicateQuotationNo(string quotationNo, long id)
        {
            ICriteria criteria = Session.CreateCriteria<Quotation>();
            if (id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (quotationNo != null)
            {
                criteria.Add(Restrictions.Eq("QuotationNo", quotationNo));
            }
            var quotationList = criteria.List<Quotation>();
            return quotationList != null && quotationList.Count > 0;
        }

        public int LoadQuotationReportCount(List<long> authOrganizationIdList, List<long> authBranchIdList, int[] purposeList,
            long[] itemGroupIdList, long[] itemIdList, string[] programSessionIds, int quotationStatus, string dateFrom,
            string dateTo)
        {
            string query = @"Select count(*) from(";

            query += GetReportQuoery(authOrganizationIdList, authBranchIdList, purposeList, itemGroupIdList, itemIdList, programSessionIds, quotationStatus,
                dateFrom, dateTo);

            query += ") as a";

            IQuery iQuery = Session.CreateSQLQuery(query);
            return (int)iQuery.UniqueResult();
        }

        #endregion

        #region Helper Function

        private ICriteria GetQuotationCriteria(string orderBy, string orderDir, List<long> authOrganizationIdList, List<long> authBranchIdList, string itemName, string deadLine,
            QuotationStatus? quotationStatus = null, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<Quotation>().Add(Restrictions.Not(Restrictions.Eq("Status", Quotation.EntityStatus.Delete)));
            criteria.CreateAlias("Branch", "branch").Add(Restrictions.Not(Restrictions.Eq("branch.Status", Branch.EntityStatus.Delete)));
            criteria.CreateAlias("branch.Organization", "org").Add(Restrictions.Not(Restrictions.Eq("org.Status", Organization.EntityStatus.Delete)));
            criteria.CreateAlias("Item", "item").Add(Restrictions.Not(Restrictions.Eq("item.Status", Item.EntityStatus.Delete)));

            if (authOrganizationIdList != null && authOrganizationIdList.Any())
            {
                criteria.Add(Restrictions.In("org.Id", authOrganizationIdList));
            }

            if (authBranchIdList != null && authBranchIdList.Any())
            {
                criteria.Add(Restrictions.In("branch.Id", authBranchIdList));
            }

            if (!String.IsNullOrEmpty(itemName))
            {
                criteria.Add(Restrictions.Like("item.Name", itemName, MatchMode.Anywhere));
            }

            if (!String.IsNullOrEmpty(deadLine))
            {
                //criteria.Add(Restrictions.Eq("PublishedDate", Convert.ToDateTime(deadLine)));
                string fromtDate = deadLine + " 00:00:00.000";
                string toDate = deadLine + " 23:59:59.000";
                criteria.Add(Restrictions.Ge("PublishedDate", Convert.ToDateTime(fromtDate.Trim()))).Add(Restrictions.Le("PublishedDate", Convert.ToDateTime(toDate.Trim())));
            }

            if (quotationStatus != null)
            {
                if (quotationStatus == QuotationStatus.Running)
                {
                    criteria.Add(Restrictions.Eq("QuotationStatus", QuotationStatus.Running));
                    criteria.Add(Restrictions.Gt("SubmissionDeadLine", DateTime.Now));
                }
                else if (quotationStatus == QuotationStatus.SubmissionClosed)
                {
                    criteria.Add(Restrictions.Eq("QuotationStatus", QuotationStatus.Running));
                    criteria.Add(Restrictions.Lt("SubmissionDeadLine", DateTime.Now));
                }
                else
                {
                    criteria.Add(Restrictions.Eq("QuotationStatus", quotationStatus));
                }
            }

            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
                else
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc("QuotationStatus") : Order.Desc("QuotationStatus"));
                }
            }

            return criteria;
        }

        private string LoadSupplierQuotationManageQuery(string supplierId, string organizationId, string branchId, string itemId, string status, bool countQuery = false)
        {
            string parameter = "";
            string parameter2 = "";
            string parameter3 = "";
            string sId = "";
            if (!String.IsNullOrEmpty(organizationId))
            {
                parameter += " and a.OrganizationId=" + Convert.ToInt64(organizationId) + "";
            }
            if (!String.IsNullOrEmpty(branchId))
            {
                parameter += " and a.BranchId=" + Convert.ToInt64(branchId) + "";
            }
            if (!String.IsNullOrEmpty(itemId))
            {
                parameter += " and a.ItemId=" + Convert.ToInt64(itemId) + "";
            }
            if (!String.IsNullOrEmpty(status))
            {
                if (Convert.ToInt32(status) == 0)
                {
                    parameter += " and a.Status=" + (int)QuotationStatus.Running + " and a.TableName='1Quotation'";
                }
                else
                {
                    parameter += " and a.Status=" + Convert.ToInt32(status) + "";
                }
            }
            if (!String.IsNullOrEmpty(supplierId))
            {
                sId = "and spq.SupplierId = " + Convert.ToInt64(supplierId);
                parameter3 = " and (spq.SupplierId!=" + Convert.ToInt64(supplierId) + " or spq.Id is null) ";
            }
            if (countQuery)
            {
                parameter2 = " count(*) ";
            }
            else parameter2 = " a.Id,a.PriceQuotationId,a.OrganizationId,a.BranchId,a.ItemId,a.Status,a.SubmissionDate,a.DeliveryDate,a.PublishedDate as PublishedDate,a.TableName ";

            string query = @"select " + parameter2 + " from (select  *,row_number() OVER (ORDER BY a.TableName) AS RowNum  from (select q.Id as Id,spq.Id as PriceQuotationId,b.OrganizationId as OrganizationId,q.BranchId as BranchId,q.ItemId,q.QuotationStatus as Status,q.SubmissionDeadLine as SubmissionDate," +
                        @"q.DeliveryDate as DeliveryDate,q.PublishedDate as PublishedDate,'1Quotation' as TableName
                        from UINV_Quotation q inner join Branch as b on q.BranchId = b.Id
                        left join UINV_SupplierPriceQuote spq on spq.QuotationId = q.Id                        
                        where 1=1 " + parameter3 + " and q.Status=" + Quotation.EntityStatus.Active + " " +
                           "and q.QuotationStatus=" + (int)QuotationStatus.Running + " and b.Status=" + Branch.EntityStatus.Active + " and q.SubmissionDeadLine>getDate() ";

            query += @"union All
                        select q.Id as Id,spq.Id as PriceQuotationId,b.OrganizationId as OrganizationId,q.BranchId as BranchId,q.ItemId,spq.PriceQuoteStatus as Status,q.SubmissionDeadLine as SubmissionDate,
                        q.DeliveryDate as DeliveryDate,q.PublishedDate as PublishedDate,'2PriceQuot' as TableName  from UINV_Quotation q 
                        inner join UINV_SupplierPriceQuote as spq on spq.QuotationId = q.Id
                        inner join Branch as b on q.BranchId = b.Id
                        where q.Status=" + Quotation.EntityStatus.Active + " " + sId + " and spq.Status = " + SupplierPriceQuote.EntityStatus.Active + " and b.Status=" + Branch.EntityStatus.Active + "";

            query += @" ) as a where 1=1 " + parameter + ") as a	where 1=1 ";

            return query;
        }

        private string GetReportQuoery(List<long> authOrganizationIdList, List<long> authBranchIdList, int[] purposeList, long[] itemGroupIdList, long[] itemIdList,
            string[] programSessionIds, int quotationStatus, string dateFrom, string dateTo)
        {
            string query = "";

            query = @"select 
                q.Id
                ,o.ShortName as OrgName
                ,b.Name as BranchName
                ,ig.name as ItemGroupName
                ,i.Name as ItemName
                ,Concat(p.Name,' ',s.Name) as ProgramSessionName
                ,q.PublishedDate as PublishedDate
                ,q.SubmissionDeadLine as SubmissionDeadLine
                ,q.QuotationNo as QuotationNo
                ,'QuotationStatus' =
                     CASE q.QuotationStatus ";
            query += @"    WHEN '" + (int)QuotationStatus.NotPublished + "' THEN 'Not Published'";
            query += @"    WHEN '" + (int)QuotationStatus.Running + "' THEN 'Running'";
            query += @"    WHEN '" + (int)QuotationStatus.SubmissionClosed + "' THEN 'Submission Closed'";
            query += @"    WHEN '" + (int)QuotationStatus.PertialIssued + "' THEN 'Pertial Issued'";
            query += @"    WHEN '" + (int)QuotationStatus.Issued + "' THEN 'Issued'	";
            query += @"    WHEN '" + (int)QuotationStatus.Cancelled + "' THEN 'Cancelled' ";
            query += @"ELSE 'Unknown'";
            query += @"                         
                      END
                 ,row_number() over(order by q.PublishedDate asc) as RowNum   

                 from [dbo].[UINV_Quotation] as q 
                INNER JOIN [dbo].[Branch] as b on b.Id = q.BranchId and b.Status = 1
                INNER JOIN [dbo].[Organization] as o on o.Id = b.OrganizationId and o.Status=1";
            
            query += @"INNER JOIN [dbo].[UINV_Item] as i on i.Id = q.ItemId and i.Status="+Item.EntityStatus.Active+"";
            query += @"INNER JOIN [dbo].[UINV_ItemGroup] as ig on ig.Id = i.ItemGroupId and ig.Status = "+ItemGroup.EntityStatus.Active+"";
            query += @"LEFT JOIN [dbo].[Program] as p on p.Id = q.ProgramId AND p.Status="+Program.EntityStatus.Active+"";
            query += @"LEFT JOIN [dbo].[Session] as s on s.Id = q.SessionId AND s.Status="+BusinessModel.Entity.Administration.Session.EntityStatus.Active+"";
           
            query += @" 
                where 1=1
                AND o.Id IN(" + string.Join(",", authOrganizationIdList) + ")";

            query += @" AND b.Id IN(" + string.Join(",", authBranchIdList) + ")";
            if (purposeList != null && !(Array.Exists(purposeList, item => item == SelectionType.SelelectAll)))
            {
                if (!(Array.Exists(purposeList, item => item == -1)))
                {
                    query += @" AND q.Purpose IN(" + string.Join(",", purposeList) + ") ";
                }
                else if ((Array.Exists(purposeList, item => item == -1)) && purposeList.Count() > 1)
                {
                    query += @" AND (q.Purpose IN(" + string.Join(",", purposeList.Where(x => x != -1)) + ") OR q.Purpose is NULL) ";
                }
                else
                {
                    query += @" AND q.Purpose is NULL ";
                }
            }

            if (itemGroupIdList != null && !(Array.Exists(itemGroupIdList, item => item == SelectionType.SelelectAll)))
            {
                query += @" AND ig.Id IN(" + string.Join(",", itemGroupIdList) + ") ";
            }

            if (itemIdList != null && !(Array.Exists(itemIdList, item => item == SelectionType.SelelectAll)))
            {
                query += @" AND i.Id IN(" + string.Join(",", itemIdList) + ") ";
            }

            if (programSessionIds != null && !programSessionIds.Contains("") && !(Array.Exists(programSessionIds, item => item == SelectionType.SelelectAll.ToString())))
            {
                string ps = "";
                if (!(Array.Exists(programSessionIds, item => item == "-1")))
                {

                    foreach (var programSessionId in programSessionIds)
                    {
                        var spltValue = programSessionId.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);

                        ps += "( q.ProgramId = " + Convert.ToInt64(spltValue[0]) + " AND q.SessionId = " + Convert.ToInt64(spltValue[1]) + " ) OR ";
                    }
                    ps = ps.Remove(ps.Length - 3);
                    query += " AND (" + ps + ")";
                }
                else if (Array.Exists(programSessionIds, item => item == "-1") && programSessionIds.Count() > 1)
                {
                    foreach (var programSessionId in programSessionIds.Where(x => x != "-1"))
                    {
                        var spltValue = programSessionId.Split(new string[] { "::" },
                            StringSplitOptions.RemoveEmptyEntries);

                        ps += "( q.ProgramId = " + Convert.ToInt64(spltValue[0]) + " AND q.SessionId = " +
                              Convert.ToInt64(spltValue[1]) + " ) OR ";
                    }
                    ps = ps.Remove(ps.Length - 3) + " OR ( q.ProgramId IS NULL AND q.SessionId IS NULL ) ";
                    query += " AND (" + ps + ")";
                }
                else
                {
                    query += " AND ( q.ProgramId IS NULL AND q.SessionId IS NULL )";
                }
            }

            if (quotationStatus != SelectionType.SelelectAll)
            {
                query += @" AND q.QuotationStatus = " + quotationStatus + "";
            }
            if (!String.IsNullOrEmpty(dateFrom))
            {
                string df = Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd") + " 00:00:00.000";
                query += @" AND q.SubmissionDeadLine>= '" + Convert.ToDateTime(df) + "'";
            }
            if (!String.IsNullOrEmpty(dateTo))
            {
                string toDate = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd") + " 23:59:59.000";
                query += @" AND q.SubmissionDeadLine<= '" + Convert.ToDateTime(toDate) + "'";
            }
            return query;
        }
        
        #endregion

    }
}
