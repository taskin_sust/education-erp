﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using FluentNHibernate.Testing.Values;

namespace UdvashERP.Areas.Student.Models
{

    [Serializable]
    public class StudentSif
    {
        public string StudentPrn { get; set; }
        public string StudentDob { get; set; }
        public int StudentBloodGroup { get; set; }
        public int StudentReligion { get; set; }
        public int StudentGender { get; set; }
        public int StudentVersionOfStudy { get; set; }
        public string StudentFatherMobile { get; set; }
        public string StudentMotherMobile { get; set; }
        public string StudentName { get; set; }
        public string StudentFatherName { get; set; }
    }

}