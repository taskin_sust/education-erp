﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UdvashERP.Api.Models
{
    public class AttendanceDeviceMemberData
    {
        public virtual long TeamMemberId { get; set; }
        public virtual long AttendanceDeviceId { get; set; }
        public virtual int EnrollNo { get; set; }
        public virtual DateTime LastUpdateDateTime { get; set; }
        public virtual int IsSynced { get; set; }
        public virtual DateTime LastSyncedDateTime { get; set; }
        public virtual string Name { get; set; }
        public virtual string FullName { get; set; }
        public virtual int Pin { get; set; }
        public virtual string CardNo { get; set; }
        public virtual int Status { get; set; }

        public virtual string Index0 { get; set; }
        public virtual string Index1 { get; set; }
        public virtual string Index2 { get; set; }
        public virtual string Index3 { get; set; }
        public virtual string Index4 { get; set; }
        public virtual string Index5 { get; set; }
        public virtual string Index6 { get; set; }
        public virtual string Index7 { get; set; }
        public virtual string Index8 { get; set; }
        public virtual string Index9 { get; set; }
    }
}