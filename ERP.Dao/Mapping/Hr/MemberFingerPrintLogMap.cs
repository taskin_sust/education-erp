﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{
    public class MemberFingerPrintLogMap : ClassMap<MemberFingerPrintLog>
    {
        public MemberFingerPrintLogMap()
        {
            Table("HR_MemberFingerPrintLog");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Index0).Column("Index0");
            Map(x => x.Index1).Column("Index1");
            Map(x => x.Index2).Column("Index2");
            Map(x => x.Index3).Column("Index3");
            Map(x => x.Index4).Column("Index4");
            Map(x => x.Index5).Column("Index5");
            Map(x => x.Index6).Column("Index6");
            Map(x => x.Index7).Column("Index7");
            Map(x => x.Index8).Column("Index8");
            Map(x => x.Index9).Column("Index9");
            Map(x => x.MemberFingerPrint).Column("MemberFingerPrintId");
        }

    }
}
