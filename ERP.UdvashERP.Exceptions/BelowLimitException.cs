﻿using System;

namespace UdvashERP.MessageExceptions
{
    public class BelowLimitException : Exception
    {
        public BelowLimitException() : base("Below Limit")
        {

        }

        public BelowLimitException(string message) : base(message)
        {

        }

    }
}
