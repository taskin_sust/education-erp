﻿using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface ICourseSubjectDao : IBaseDao<CourseSubject, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        CourseSubject LoadCourseSubjectByCourseIdAndSubjectId(long courseId, long subId);
        #endregion

        #region List Loading Function
        IList<CourseSubject> GetCourseSubjectListById(long id);
        IList<CourseSubject> LoadCourseSubjectByCourseId(long id);
        IList<CourseSubject> LoadCourseSubjectByCourseId(long programId,long sessionId,List<long> courseIdList);
        #endregion

        #region Others Function

        #endregion

        
    }
    public class CourseSubjectDao : BaseDao<CourseSubject, long>, ICourseSubjectDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public CourseSubject LoadCourseSubjectByCourseIdAndSubjectId(long courseId, long subId)
        {
            return
                Session.QueryOver<CourseSubject>()
                    .Where(
                        x =>
                            x.Course.Id == courseId && x.Subject.Id == subId &&
                            x.Status == CourseSubject.EntityStatus.Active)
                    .SingleOrDefault<CourseSubject>();
        }
        #endregion

        #region List Loading Function
        public IList<CourseSubject> GetCourseSubjectListById(long id)
        {
            return
                Session.QueryOver<CourseSubject>()
                    .Where(x => x.Id == id && x.Status == CourseSubject.EntityStatus.Active)
                    .List<CourseSubject>();
        }
        public IList<CourseSubject> LoadCourseSubjectByCourseId(long id)
        {
            return Session.QueryOver<CourseSubject>().Where(x => x.Course.Id == id && x.Status == CourseSubject.EntityStatus.Active).List<CourseSubject>();
        }

        public IList<CourseSubject> LoadCourseSubjectByCourseId(long programId, long sessionId, List<long> courseIdList)
        {

            var courseSubject =
                Session.Query<CourseSubject>()
                    .Where(
                        x => x.Course.Program.Id == programId &&
                            x.Course.RefSession.Id == sessionId && x.Status == CourseSubject.EntityStatus.Active).ToList();
            if (!courseIdList.Contains(SelectionType.SelelectAll))
            {
                courseSubject = courseSubject.Where(x => x.Course.Id.In(courseIdList.ToArray())).ToList();
            }
            Program programAlias = null;
            Session sessionAlias = null;
            Course courseAlias = null;
            LectureSettings lsAlias = null;
            var lectureSettings = Session.QueryOver<LectureSettings>(() => lsAlias).JoinAlias(() => lsAlias.Course, () => courseAlias)
                .JoinAlias(() => courseAlias.Program, () => programAlias)
                .JoinAlias(() => courseAlias.RefSession, () => sessionAlias)
                .Where(x => programAlias.Id == programId && sessionAlias.Id == sessionId && lsAlias.Status == LectureSettings.EntityStatus.Active);
            if (!courseIdList.Contains(SelectionType.SelelectAll))
            {
                lectureSettings = lectureSettings.Where(x => courseAlias.Id.IsIn(courseIdList));
            }
            var cs = lectureSettings.Select(x => x.CourseSubject).List<CourseSubject>();
            courseSubject.ToList().AddRange(cs);
            if (cs.Contains(null))
            {
                courseSubject.Add(null);
            }
            courseSubject = courseSubject.Distinct().ToList();
            return courseSubject;

        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}