﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.BusinessRules.Student
{
    public class StudentListConstant
    {
        public const string ProgramRoll = "Program Roll";
        public const string RegistrationNumber = "Registration Number";
        public const string StudentsName = "Student's Name";
        public const string StudentsFullName = "Full Name";
        public const string NickName = "Nick Name";
        public const string MobileNumberStudent = "Mobile Number (Student)";
        public const string MobileNumberFather = "Mobile Number (Father)";
        public const string MobileNumberMother = "Mobile Number (Mother)";
        public const string FathersName = "Father's Name";
        public const string InstituteName = "Institute Name";
        public const string Email = "Email";
        public const string Branch = "Branch";
        public const string Campus = "Campus";
        public const string Batch = "Batch Name";
        public const string BatchDays = "Batch Days";
        public const string BatchTime = "Batch Time";
        public const string VersionOfStudy = "Version of Study";
        public const string Gender = "Gender";
        public const string Religion = "Religion";
        public const string AcademicInfo = "Academic Info";
        public const string Advice = "Advice";
        public const string Complain = "Complain";
        public const string District = "District";
        public const string AcademicInfoYear = "Year";
        public const string AcademicInfoBoard = "Board";
        public const string AcademicInfoBoardRoll = "Board Roll";
        public const string AcademicInfoRegistrationNumber = "Reg. No.";

        public static List<string> GetStudentListInformationStringList()
        {
            return new List<String>
            {
                ProgramRoll
                , RegistrationNumber
                , StudentsName
                , NickName
                , MobileNumberStudent
                , MobileNumberFather
                , MobileNumberMother
                , FathersName
                , InstituteName
                , Email
                , Branch
                , Campus
                , BatchDays
                , BatchTime
                , Batch
                , VersionOfStudy
                , Gender
                , Religion
                , AcademicInfo
            };
        }

        public static List<string> GetStudentMakeSurveyInformationStringList()
        {
            return new List<String>
            {
                ProgramRoll
                , StudentsName
                , NickName
                , MobileNumberStudent
                , MobileNumberFather
                , MobileNumberMother
                , FathersName
                , InstituteName
                , Email
                , Branch
                , Campus
                , BatchDays
                , BatchTime
                , Batch
            };
        }

        public static List<string> GetStudentWiseSurveyReportInformationStringList()
        {
            return new List<String>
            {
                ProgramRoll
                , StudentsName
                , NickName
                , MobileNumberStudent
                , MobileNumberFather
                , MobileNumberMother
                , FathersName
                , InstituteName
                , Email
                , Branch
                , Campus
                , BatchDays
                , BatchTime
                , Batch
                , Advice
                , Complain
            };
        }

        public static List<string> GetStudentSearchStringList()
        {
            return new List<String>
            {
                ProgramRoll
                , RegistrationNumber
                , StudentsName
                , NickName
                , MobileNumberStudent
                , MobileNumberFather
                , MobileNumberMother
                , FathersName
                , InstituteName
                , Email
                , Branch
                , Batch
                , VersionOfStudy
                , Gender
                , Religion
                , District
            };
        }

        public static List<string> GetInfoSynchronizerInformationStringList()
        {
            return new List<String>
            {
                ProgramRoll
                , StudentsFullName
                , MobileNumberStudent
                , FathersName
                , InstituteName
                , NickName
                , MobileNumberFather
                , MobileNumberMother
                , AcademicInfoYear
                , AcademicInfoBoard
                , AcademicInfoBoardRoll
                , AcademicInfoRegistrationNumber
            };
        }
    }
}
