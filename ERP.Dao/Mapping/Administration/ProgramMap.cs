using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{


    public partial class ProgramMap : BaseClassMap<Program, long>
    {

        public ProgramMap()
        {
            Table("Program");
            LazyLoad();
            //Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Rank);
            Map(x => x.Name);
            Map(x => x.Code).Column("Code");
            Map(x => x.Type).Column("Type");
            Map(x => x.ShortName).Column("ShortName");
            References(x => x.Organization).Column("OrganizationId");
            HasMany(x => x.Batches).KeyColumn("ProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.Discounts).KeyColumn("ProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.Materials).KeyColumn("ProgramId").Cascade.AllDeleteOrphan().Inverse();


            //HasManyToMany(x => x.Subject).Cascade.AllDeleteOrphan().Table("SubjectProgram");

            HasMany(x => x.StudentPrograms).KeyColumn("ProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.UserMenus).KeyColumn("ProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.Courses).KeyColumn("ProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ProgramBranchSessions).KeyColumn("ProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ExamList).KeyColumn("ProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ServiceBlocks).KeyColumn("ProgramId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            HasMany(x => x.SmsStudentRegistrationSettingList).KeyColumn("ProgramId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
