﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.MediaDao;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Helper;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Services.MediaServices
{
    public interface ITeamMemberImagesMediaService
    {

        TeamMemberMediaImage GetTeamMemberImageMediaByRefId(long id);
        TeamMemberNomineeMediaImage GetMemberNomineeMediaImageByRefId(long id);

        void DeleteTeamMemberMediaImage(long refId);

        void Delete(TeamMember teamMember);

        TeamMemberMediaImage FileUpload(TeamMember teamMember, HttpPostedFileBase file, bool overwrite);
        bool FileDelete(TeamMember teamMember);

        TeamMemberNomineeMediaImage NomineeImageFileUpload(TeamMember teamMember, HttpPostedFileBase file, bool overwrite);
        bool FileNomineeDelete(TeamMember teamMember);
    }
    public class TeamMemberImagesMediaService : ITeamMemberImagesMediaService
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        private readonly ITeamMemberImageMediaDao _teamMemberImageMediaDao;
        private readonly ITeamMemberNomineeImageMediaDao _teamMemberNomineeImageMediaDao;
        private ISession session;
        public TeamMemberImagesMediaService(ISession sessionParam = null)
        {
            if (sessionParam == null)
            {

                var sessionMedia = NHibernateMediaSessionFactory.OpenSession();
                session = sessionMedia;   
            }
            else
            {
                session = sessionParam;
            }
            _teamMemberImageMediaDao = new TeamMemberImageMediaDao { Session = session };
            _teamMemberNomineeImageMediaDao = new TeamMemberNomineeImageMediaDao { Session = session };
        }

        //
        public TeamMemberMediaImage GetTeamMemberImageMediaByRefId(long id)
        {
            return _teamMemberImageMediaDao.GetTeamMemberImageMediaByRefId(id);
        }

        public TeamMemberNomineeMediaImage GetMemberNomineeMediaImageByRefId(long id)
        {
            return _teamMemberNomineeImageMediaDao.GetTeamMemberNomineeImageMediaByRefId(id);
        }

        public void DeleteTeamMemberMediaImage(long refId)
        {
            _teamMemberImageMediaDao.DeleteTeamMemberMediaImage(refId);
        }

        public void Delete(TeamMember teamMember)
        {
            var mediaTranacition = session.BeginTransaction();
            foreach (var imageObj in teamMember.Images)
            {
                _teamMemberImageMediaDao.DeleteTeamMemberMediaImage(imageObj.Id);
            }
            mediaTranacition.Commit();
        }
        public string CheckFileSettingsForTypeAndSize(string name, string contentType, int size)
        {
            try
            {
                if (name == null || contentType == null || size < 1)
                    return "This is not valid file. Check the file extension.";

                if (contentType.ToUpper() == "JPG" || contentType.ToUpper() == "JPEG" || contentType.ToUpper() == "IMAGE/JPEG")
                {
                    string message = "";
                    var imageSizeKB = TeamMemberImageSize.CalculateSize(Convert.ToDouble(size));

                    if (imageSizeKB < TeamMemberImageSize.MinimumSizeKB)
                        return "The image size is too small.";

                    if (imageSizeKB > TeamMemberImageSize.MaximumSizeKB)
                        return "The image size is too large. Max valid size is up to " + TeamMemberImageSize.MaximumSizeKB + "KB";

                    return message;
                }

                return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
            }
        }
        private byte[] ConvertFileInByteArray(Stream inputStream, int contentLength)
        {
            try
            {
                byte[] file = null;

                using (var binaryReader = new BinaryReader(inputStream))
                {
                    file = binaryReader.ReadBytes(contentLength);
                }

                return file;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        private string ComputeHash(byte[] file)
        {
            MD5 md5 = MD5.Create();

            byte[] hashAraay = md5.ComputeHash(file);

            var builder = new StringBuilder();

            foreach (byte b in hashAraay)
            {
                builder.AppendFormat("{0:x2}", b);
            }

            return builder.ToString();
        }

        
        public TeamMemberMediaImage FileUpload(TeamMember teamMember, HttpPostedFileBase file, bool overwrite)
        {
            try
            {
                if (file != null)
                {
                    string sizeCheckerMessage = CheckFileSettingsForTypeAndSize(file.FileName, file.ContentType, file.ContentLength);
                    if (sizeCheckerMessage != null && sizeCheckerMessage.Length > 1)
                        throw new InvalidDataException(sizeCheckerMessage);
                        //return sizeCheckerMessage;

                    var contentType = file.ContentType;
                    var imageFile = ConvertFileInByteArray(file.InputStream, file.ContentLength);
                    TeamMemberMediaImage teamMemberMediaImages = _teamMemberImageMediaDao.GetTeamMemberImageMediaByRefId(teamMember.Id);

                    //Check contentType for image
                    if (contentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                    {
                        using (var transicationMedia = session.BeginTransaction())
                        {
                            if (teamMemberMediaImages == null)
                                teamMemberMediaImages = new TeamMemberMediaImage();
                            teamMemberMediaImages.ImageGuid = Guid.NewGuid();
                            teamMemberMediaImages.Images = imageFile;
                            teamMemberMediaImages.Status = TeamMemberMediaImage.EntityStatus.Active;
                            teamMemberMediaImages.ImageRef = teamMember.Id;

                            //{
                            //  //  CreationDate = DateTime.Now,
                            //  //  ModificationDate = DateTime.Now,
                            // //   Status = StudentProgram.EntityStatus.Active,
                            ////    CreateBy = 1,//studentProgram.CreateBy,
                            ////    ModifyBy = 1,//studentProgram.ModifyBy,
                            //    ImageGuid = Guid.NewGuid(),
                            //    Images = imageFile,
                            //    ImageRef = teamMember.Id
                            //};
                            _teamMemberImageMediaDao.SaveOrUpdate(teamMemberMediaImages);
                            //_sessionMedia.Save(teamMemberMediaImages);
                            transicationMedia.Commit();
                            return teamMemberMediaImages;
                            // return "Success";
                        }
                    }
                    else
                    {
                        throw new InvalidDataException("This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension");
                    }
                    //return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
                }
                throw new InvalidDataException("File is missing");
                //return "File is missing";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
                //return "Failed to upload";
            }
        }

        public bool FileDelete(TeamMember teamMember)
        {
            ITransaction trans = null;
            try
            {

                using (trans = session.BeginTransaction())
                {
                    TeamMemberMediaImage teamMemberMediaImages = _teamMemberImageMediaDao.GetTeamMemberImageMediaByRefId(teamMember.Id);
                    if (teamMemberMediaImages != null)
                    {
                        _teamMemberImageMediaDao.Delete(teamMemberMediaImages);
                        trans.Commit();
                        return true;    
                    }
                    
                }

                return false;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool FileNomineeDelete(TeamMember teamMember)
        {
            ITransaction trans = null;
            try
            {

                using (trans = session.BeginTransaction())
                {
                    TeamMemberNomineeMediaImage teamMemberNomineeMediaImages = _teamMemberImageMediaDao.GetTeamMemberNomineeImageMediaByRefId(teamMember.Id);
                    if (teamMemberNomineeMediaImages != null)
                    {
                        _teamMemberNomineeImageMediaDao.Delete(teamMemberNomineeMediaImages);
                        trans.Commit();
                        return true;    
                    }
                    
                }

                return false;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public TeamMemberNomineeMediaImage NomineeImageFileUpload(TeamMember teamMember, HttpPostedFileBase file, bool overwrite)
        {
            try
            {
                if (file != null)
                {
                    string sizeCheckerMessage = CheckFileSettingsForTypeAndSize(file.FileName, file.ContentType, file.ContentLength);
                    if (sizeCheckerMessage != null && sizeCheckerMessage.Length > 1)
                        throw new InvalidDataException(sizeCheckerMessage);

                    var contentType = file.ContentType;
                    var imageFile = ConvertFileInByteArray(file.InputStream, file.ContentLength);
                    TeamMemberNomineeMediaImage teamMemberNomineeMediaImages = _teamMemberImageMediaDao.GetTeamMemberNomineeImageMediaByRefId(teamMember.Id);

                    //Check contentType for image
                    if (contentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                    {
                        using (var transicationMedia = session.BeginTransaction())
                        {
                            if (teamMemberNomineeMediaImages == null)
                                teamMemberNomineeMediaImages = new TeamMemberNomineeMediaImage();
                            teamMemberNomineeMediaImages.ImageGuid = Guid.NewGuid();
                            teamMemberNomineeMediaImages.Images = imageFile;
                            teamMemberNomineeMediaImages.Status = TeamMemberMediaImage.EntityStatus.Active;
                            teamMemberNomineeMediaImages.ImageRef = teamMember.Id;
                            _teamMemberNomineeImageMediaDao.SaveOrUpdate(teamMemberNomineeMediaImages);
                            transicationMedia.Commit();
                            return teamMemberNomineeMediaImages;
                        }
                    }
                    else
                    {
                        throw new InvalidDataException("This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension");
                    }
                    //return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
                }
                throw new InvalidDataException("File is missing");
                //return "File is missing";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
                //return "Failed to upload";
            }
        }
    }
}
