﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.NightWorkALlowanceSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "NightWorkAllowanceSettingService")]

    public class NightWorkAllowanceUpdateTest : NightWorkAllowanceSettingBaseTest
    {

        #region Basic Test

        [Fact]
        public void Should_Edit_Successfully()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            _nightworkAllowanceSettingFactory.Object.ClosingDate = DateTime.Now.AddDays(10);

            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            Assert.Equal(_nightWorkAllowanceSettingService.GetById(_nightworkAllowanceSettingFactory.Object.Id).ClosingDate.Value.Date, DateTime.Now.AddDays(10).Date);

        }

        #endregion

        #region Invalid Value Test

        [Fact]
        public void Should_Check_CalculationOn_For_LessThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 0;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_CalculationOn_For_GreaterThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 3;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_InHouseMultiplyingFactor_For_LessThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 0;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_InHouseMultiplyingFactor_For_GreaterThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 4;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }
        [Fact]
        public void Should_Check_OutHouseMultiplyingFactor_For_LessThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 0;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_OutHouseMultiplyingFactor_For_GreaterThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 4;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        #endregion

        #region Authorization Check

        [Fact]
        public void Should_Return_UnAuthorized_Menu_Message()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { organizationFactory.Create().Persist().Object }, null, null);
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30)).Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
                new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();

            _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());

            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = DateTime.Now.AddMonths(-1).Month,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_Invalid_Edit_Object()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            Assert.Throws<NullObjectException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, null));
        }

        [Fact]
        public void Should_Return_Invalid_Edit_Permission()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            Assert.Throws<AuthenticationException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(null, _nightworkAllowanceSettingFactory.Object));

        }

        [Fact]
        public void Should_Return_Invalid_Edit_DateTime()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);

            _nightworkAllowanceSettingFactory.Object.EffectiveDate = DateTime.Now.AddDays(5);
            _nightworkAllowanceSettingFactory.Object.ClosingDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_Edit_MultiplyingFactor()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            _nightworkAllowanceSettingFactory.Object.InHouseMultiplyingFactor = 0;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_IsPost_Condition_ForUpdate_And_Return_InvalidDataException_BecauseOf_Assigned_To_Members()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org)
                .WithDatetime(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(2016, 1, 1).Date,
                 new DateTime(2016, 1, 1, 9, 0, 0), new DateTime(2016, 1, 1, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();
            _nightWorkService.Save(new DateTime(2016, 1, 1).Date.ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            var sheetFormViewModel = new AllowanceSheetFormViewModel
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
            _nightworkAllowanceSettingFactory.Object.OutHouseMultiplyingFactor = 2;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menus, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_IsPost_Condition_ForUpdate_And_Return_InvalidDataException_BecauseOf_Inappropriate_ClosingDate()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(2016, 1, 1).Date, new DateTime(2016, 1, 1, 9, 0, 0), new DateTime(2016, 1, 1, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();

            _nightWorkService.Save(new DateTime(2016, 1, 1).Date.ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            var sheetFormViewModel = new AllowanceSheetFormViewModel
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
            _nightworkAllowanceSettingFactory.Object.ClosingDate = new DateTime(2016, 1, 30);
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menus, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_DuplicateEntry_For_Update_And_return_InvalidDataException_BecacuseOf_Already_Declared_One_For_EqualDate()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, null);
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2020, 12, 30));

            _nightworkAllowanceSettingFactory.Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(2016, 1, 1).Date, new DateTime(2016, 1, 1, 9, 0, 0), new DateTime(2016, 1, 1, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();

            _nightWorkService.Save(new DateTime(2016, 1, 1).Date.ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2016, 12, 30));
            _nightworkAllowanceSettingFactory.SingleObjectList[1].ClosingDate = new DateTime(2018, 01, 01);
            _nightworkAllowanceSettingFactory.Persist(menus);

            _nightworkAllowanceSettingFactory.SingleObjectList[1].Name = _nightworkAllowanceSettingFactory.SingleObjectList[0].Name;
            _nightworkAllowanceSettingFactory.SingleObjectList[1].ClosingDate = null;
            Assert.Throws<InvalidDataException>(() =>
                _nightWorkAllowanceSettingService.SaveOrUpdate(menus, _nightworkAllowanceSettingFactory.SingleObjectList[1]));

        }

        //TODO: Write test for changing valid property value.
        //Not sure but have written this kind of this test 

        //TODO: Write test for changing property value into invalid value.
        //Please check Invalid Value Test region.have already written this

        #endregion

    }
}
