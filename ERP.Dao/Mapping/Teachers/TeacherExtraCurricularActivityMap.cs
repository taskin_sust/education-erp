﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherExtraCurricularActivityMap : BaseClassMap<TeacherExtraCurricularActivity, long>
    {
        public TeacherExtraCurricularActivityMap()
        {
            Table("TeacherExtraCurricularActivity");
			LazyLoad();
            Map(x => x.Type);
            References(x => x.Teacher).Column("TeacherId");
            References(x => x.ExtraCurricularActivity).Column("ExtraCurricularActivityId");
        }
    }
}
