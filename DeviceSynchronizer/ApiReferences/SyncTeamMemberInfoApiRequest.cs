﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model.ApiModel;
using log4net;
using Newtonsoft.Json;

namespace DeviceSynchronizer.ApiReferences
{
    public class SyncTeamMemberInfoApiRequest : BaseApiRequest
    {
        #region Logger

        public static readonly ILog Log = LogManager.GetLogger("SyncTeamMemberInfoApiLogger");

        #endregion

        #region Properties & Object Initialization
        private const string attendanceSynchronizerController = "SyncTeamMemberInfoApi";
        private const string attendanceSynchronizerFuncName = "GetAttendanceSynchronizer";
        private const string currentDatetimeFuncName = "GetCurrentDateTime";
        private const string uploadAttendanceErrorPinsFuncName = "UploadAttendanceErrorPins";
        private HttpHelper _httpHelper;

        public SyncTeamMemberInfoApiRequest()
        {
            _httpHelper = new HttpHelper();
        }

        #endregion

        #region Operational Function

        public AttendanceSynchronizerModel GetAttendanceSynchronizer(List<string> hddSerialList)
        {
            try
            {
                var serializeList = JsonConvert.SerializeObject(hddSerialList);
                var dictionary = new Dictionary<string, string> 
                { 
                    { "hddSerialList", serializeList },
                    { "version", BusinessRules.AppVersion } 
                };
                string param = MakeParam(dictionary);
                string url = MakeUrl(attendanceSynchronizerController, attendanceSynchronizerFuncName, param);
                var jsonResponse = _httpHelper.HttpGetRequest(url);
                if (jsonResponse.IsSuccess)
                    return JsonConvert.DeserializeObject<AttendanceSynchronizerModel>(jsonResponse.Data.ToString());
                Log.Warn(jsonResponse.Data);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        //Use to make server request with sync key and get response as a Json object 
        //return server datetime 
        public DateTime? GetCurrentDateTime()
        {
            try
            {
                string param = MakeParam(null);
                string url = MakeUrl(attendanceSynchronizerController, currentDatetimeFuncName, param);
                var jsonResponse = _httpHelper.HttpGetRequest(url);
                if (jsonResponse.IsSuccess)
                {
                    var dt = DateTime.ParseExact(jsonResponse.Data, BusinessRules.DateTimeFormat, CultureInfo.InvariantCulture);
                    return dt;
                }
                Log.Warn(jsonResponse.Data);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        //Use to push attendance error pins which is failed to add in device
        //return true/false 
        public bool UploadAttendanceErrorPins(List<AttendanceErrorInfoModel> pinList)
        {
            try
            {
                var serialize = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(pinList);
                var dictionary = new Dictionary<string, string>
                {
                    {"attendanceErrorInfoList", serialize.ToString()},
                };

                string param = MakeParam(dictionary);
                string url = MakeUrl(attendanceSynchronizerController, uploadAttendanceErrorPinsFuncName, null);
                var jsonResponse = _httpHelper.HttpPostRequest(url, param);
                if (jsonResponse.IsSuccess) return true;
                Log.Warn(jsonResponse.Data);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return false;
        }

        #endregion
    }
}
