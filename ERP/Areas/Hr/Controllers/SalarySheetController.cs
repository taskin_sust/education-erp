﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Newtonsoft.Json;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class SalarySheetController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private List<UserMenu> _userMenu;
        private readonly CommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly ISalarySheetService _salarySheetService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IDepartmentService _departmentService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IDesignationService _designationService;
        private readonly IZoneSettingService _zoneSettingService;
        private readonly IEmployeeBenefitsFundSettingService _employeeBenefitsFundSettingService;
        private readonly ISalaryHistoryService _salaryHistoryService;
        private readonly IEmploymentHistoryService _employmentHistoryService;
        public SalarySheetController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _salarySheetService = new SalarySheetService(session);
            _zoneSettingService = new ZoneSettingService(session);
            _employeeBenefitsFundSettingService = new EmployeeBenefitsFundSettingService(session);
            _teamMemberService = new TeamMemberService(session);
            _branchService = new BranchService(session);
            _campusService = new CampusService(session);
            _departmentService = new DepartmentService(session);
            _designationService = new DesignationService(session);
            _salaryHistoryService = new SalaryHistoryService(session);
            _employmentHistoryService = new EmploymentHistoryService(session);
        }

        #endregion

        #region Salary Sheet

        public ActionResult Create()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new MultiSelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");
                ViewBag.DepartmentList = new MultiSelectList(new List<Department>(), "Id", "Name");

                var organizationTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<OrganizationType>(), "Key", "Value");
                ViewBag.OrganizationTypeList = organizationTypeList;
                ViewBag.SalarySheetMonthList = new SelectList(_commonHelper.LoadEmumToDictionary<MonthsOfYear>(), "Key", "Value");
                int initialYear = DateTime.Now.Date.Year - 1;
                int i;
                var yearDictionary = new Dictionary<int, string>();
                for (i = initialYear; i <= DateTime.Now.Date.Year; i++)
                {
                    yearDictionary.Add(i, i.ToString());
                }
                ViewBag.SalarySheetYearList = new SelectList(yearDictionary, "Key", "Value");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public PartialViewResult Create(SalarySheetFormViewModel viewModel)
        {
            var patients = new List<SalarySheetDto>();
            try
            {
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                
                if (ModelState.IsValid)
                {
                    ViewBag.Organization = _organizationService.LoadById(viewModel.OrganizationId) != null
                        ? _organizationService.LoadById(viewModel.OrganizationId).Name
                        : "";
                    ViewBag.Branch = viewModel.BranchId == 0
                        ? "All Branch"
                        : _branchService.GetBranch(viewModel.BranchId) == null
                            ? ""
                            : _branchService.GetBranch(viewModel.BranchId).Name;

                    ViewBag.Campus = viewModel.CampusId == 0
                        ? "All Campus"
                        : _campusService.GetCampus(viewModel.CampusId) == null
                            ? ""
                            : _campusService.GetCampus(viewModel.CampusId).Name;
                    ViewBag.Department = viewModel.DepartmentId == 0
                        ? "All Department"
                        : _departmentService.LoadById(viewModel.DepartmentId) == null
                            ? ""
                            : _departmentService.LoadById(viewModel.DepartmentId).Name;
                    ViewBag.Month = viewModel.SalaryMonth;
                    ViewBag.Year = viewModel.SalaryYear;

                    var currentMonth = DateTime.Now.Month;
                    var currentYear = DateTime.Now.Year;
                    if (viewModel.SalaryMonth >= currentMonth && currentYear == viewModel.SalaryYear)
                        ViewBag.ErrMsg = "you Can't generate current or future month salary sheet ";
                    else
                    {
                        int i = 0;
                        patients = _salarySheetService.LoadTeamMemberForSalarySheet(_userMenu, viewModel);
                        foreach (var patient in patients)
                        {
                            var employmentHistory =
                                _employmentHistoryService.GetTeamMemberFirstMonth(patient.TeamMemberId,
                                    viewModel.SalaryMonth, viewModel.SalaryYear);
                            if (employmentHistory != null)
                            {
                                var newObject = _salarySheetService.GetSalarySheetForFirstMonthTeamMember(_userMenu,viewModel, employmentHistory);
                                patients[i].TeamMemberId = newObject.TeamMemberId;
                                patients[i].SalaryHistoryId = newObject.SalaryHistoryId;
                                patients[i].SalaryOrganizationId = newObject.SalaryOrganizationId;
                                patients[i].SalaryBranchId = newObject.SalaryBranchId;
                                patients[i].SalaryCampusId = newObject.SalaryCampusId;
                                patients[i].SalaryDepartmentId = newObject.SalaryDepartmentId;
                                patients[i].SalaryDesignationId = newObject.SalaryDesignationId;
                                patients[i].OrganizationId = newObject.OrganizationId;
                                patients[i].BranchId = newObject.BranchId;
                                patients[i].CampusId = newObject.CampusId;
                                patients[i].DepartmentId = newObject.DepartmentId;
                                patients[i].DesignationId = newObject.DesignationId;
                                patients[i].Name = newObject.Name;
                                patients[i].Pin = newObject.Pin;
                                patients[i].Designation = newObject.Designation;
                                patients[i].BasicSalary = newObject.BasicSalary;
                                patients[i].HouseRent = newObject.HouseRent;
                                patients[i].Conveyance = newObject.Conveyance;
                                patients[i].Medical = newObject.Medical;
                                patients[i].GrossSalary = newObject.GrossSalary;
                                patients[i].Increament = newObject.Increament;
                                patients[i].Arrear = newObject.Arrear;
                                patients[i].ZoneAmount = newObject.ZoneAmount;
                                patients[i].FinalZoneAmount = newObject.FinalZoneAmount;
                                patients[i].AbsentAmount = newObject.FinalAbsentAmount;
                                patients[i].LeaveWithoutPayAmount = newObject.LeaveWithoutPayAmount;
                                patients[i].Loan = newObject.Loan;
                                patients[i].FinalLoanRefund = newObject.FinalLoanRefund;
                                patients[i].TdsAmount = newObject.TdsAmount;
                                patients[i].Ebf = newObject.Ebf;
                                patients[i].EbfForEmplyer = newObject.EbfForEmplyer;
                                patients[i].Bank = newObject.Bank;
                                patients[i].Cash = newObject.Cash;
                                patients[i].TotalAmount = newObject.TotalAmount;
                                patients[i].DeductionPerAbsent = newObject.DeductionPerAbsent;
                                patients[i].TotalAbsentDay = newObject.TotalAbsentDay;
                                patients[i].AbsentMultiplierFactor = newObject.AbsentMultiplierFactor;
                                patients[i].EbfSettingsId = newObject.EbfSettingsId;
                                patients[i].CalculationOn = newObject.CalculationOn;
                                patients[i].EmployeeContribution = newObject.EmployeeContribution;
                                patients[i].EmployerContribution = newObject.EmployerContribution;
                                patients[i].TotalSalaryWithoutDeduction = newObject.TotalSalaryWithoutDeduction;
                                patients[i].Remarks = newObject.Remarks;
                                patients[i].EmploymentStatus = newObject.EmploymentStatus;
                            }
                            i++;
                        }
                    }

                    ViewBag.salarySheetList = _salarySheetService.LoadSalarySheetList(_userMenu, viewModel);
                    ViewBag.ViewModel = viewModel;
                }
                ViewBag.IsSubmit = false;
            }
            catch (MessageException ex)
            {
                ViewBag.ErrMsg = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrMsg = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrMsg = WebHelper.SetExceptionMessage(ex);
            }
            return PartialView("Partial/_TeamMemberListForSalarySheet", patients);
        }

        [HttpPost]
        public PartialViewResult RegenerateSalarySheet(string salarySheetViewModel)
        {
            var patients = new List<SalarySheetDto>();
            try
            {
                if (salarySheetViewModel == "")
                    throw new NullObjectException("Invalid data.");

                dynamic viewModelJson = JsonConvert.DeserializeObject(salarySheetViewModel);

                var viewModel = new SalarySheetFormViewModel()
                {
                    OrganizationType = (OrganizationType)viewModelJson.OrganizationType,
                    BranchId = Convert.ToInt64(viewModelJson.BranchId),
                    CampusId = Convert.ToInt64(viewModelJson.CampusId),
                    DepartmentId = Convert.ToInt64(viewModelJson.DepartmentId),
                    OrganizationId = Convert.ToInt64(viewModelJson.OrganizationId),
                    SalaryMonth = Convert.ToInt32(viewModelJson.SalaryMonth),
                    SalaryYear = Convert.ToInt32(viewModelJson.SalaryYear),
                };
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                if (ModelState.IsValid)
                {
                    ViewBag.Organization = _organizationService.LoadById(viewModel.OrganizationId) != null
                        ? _organizationService.LoadById(viewModel.OrganizationId).Name
                        : "";
                    ViewBag.Branch = viewModel.BranchId == 0
                        ? "All Branch"
                        : _branchService.GetBranch(viewModel.BranchId) == null
                            ? ""
                            : _branchService.GetBranch(viewModel.BranchId).Name;

                    ViewBag.Campus = viewModel.CampusId == 0
                        ? "All Campus"
                        : _campusService.GetCampus(viewModel.CampusId) == null
                            ? ""
                            : _campusService.GetCampus(viewModel.CampusId).Name;

                    ViewBag.Department = viewModel.DepartmentId == 0
                        ? "All Department"
                        : _departmentService.LoadById(viewModel.DepartmentId) == null
                            ? ""
                            : _departmentService.LoadById(viewModel.DepartmentId).Name;
                    ViewBag.Month = viewModel.SalaryMonth;
                    ViewBag.Year = viewModel.SalaryYear;

                    var currentMonth = DateTime.Now.Month;
                    var currentYear = DateTime.Now.Year;
                    if (viewModel.SalaryMonth >= currentMonth && currentYear == viewModel.SalaryYear)
                        ViewBag.ErrMsg = "you Can't generate current or future month salary sheet ";
                    else
                    {
                        int i = 0;
                        patients = _salarySheetService.LoadTeamMemberForSalarySheet(_userMenu, viewModel);
                        foreach (var patient in patients)
                        {
                            var employmentHistory = _employmentHistoryService.GetTeamMemberFirstMonth(patient.TeamMemberId, viewModel.SalaryMonth, viewModel.SalaryYear);
                            if (employmentHistory != null)
                            {
                                var newObject = _salarySheetService.GetSalarySheetForFirstMonthTeamMember(_userMenu, viewModel, employmentHistory);
                                patients[i].TeamMemberId = newObject.TeamMemberId;
                                patients[i].SalaryHistoryId = newObject.SalaryHistoryId;
                                patients[i].SalaryOrganizationId = newObject.SalaryOrganizationId;
                                patients[i].SalaryBranchId = newObject.SalaryBranchId;
                                patients[i].SalaryCampusId = newObject.SalaryCampusId;
                                patients[i].SalaryDepartmentId = newObject.SalaryDepartmentId;
                                patients[i].SalaryDesignationId = newObject.SalaryDesignationId;
                                patients[i].OrganizationId = newObject.OrganizationId;
                                patients[i].BranchId = newObject.BranchId;
                                patients[i].CampusId = newObject.CampusId;
                                patients[i].DepartmentId = newObject.DepartmentId;
                                patients[i].DesignationId = newObject.DesignationId;
                                patients[i].Name = newObject.Name;
                                patients[i].Pin = newObject.Pin;
                                patients[i].Designation = newObject.Designation;
                                patients[i].BasicSalary = newObject.BasicSalary;
                                patients[i].HouseRent = newObject.HouseRent;
                                patients[i].Conveyance = newObject.Conveyance;
                                patients[i].Medical = newObject.Medical;
                                patients[i].GrossSalary = newObject.GrossSalary;
                                patients[i].Increament = newObject.Increament;
                                patients[i].Arrear = newObject.Arrear;
                                patients[i].ZoneAmount = newObject.ZoneAmount;
                                patients[i].FinalZoneAmount = newObject.FinalZoneAmount;
                                patients[i].AbsentAmount = newObject.FinalAbsentAmount;
                                patients[i].LeaveWithoutPayAmount = newObject.LeaveWithoutPayAmount;
                                patients[i].Loan = newObject.Loan;
                                patients[i].FinalLoanRefund = newObject.FinalLoanRefund;
                                patients[i].TdsAmount = newObject.TdsAmount;
                                patients[i].Ebf = newObject.Ebf;
                                patients[i].EbfForEmplyer = newObject.EbfForEmplyer;
                                patients[i].Bank = newObject.Bank;
                                patients[i].Cash = newObject.Cash;
                                patients[i].TotalAmount = newObject.TotalAmount;
                                patients[i].DeductionPerAbsent = newObject.DeductionPerAbsent;
                                patients[i].TotalAbsentDay = newObject.TotalAbsentDay;
                                patients[i].AbsentMultiplierFactor = newObject.AbsentMultiplierFactor;
                                patients[i].EbfSettingsId = newObject.EbfSettingsId;
                                patients[i].CalculationOn = newObject.CalculationOn;
                                patients[i].EmployeeContribution = newObject.EmployeeContribution;
                                patients[i].EmployerContribution = newObject.EmployerContribution;
                                patients[i].TotalSalaryWithoutDeduction = newObject.TotalSalaryWithoutDeduction;
                                patients[i].Remarks = newObject.Remarks;
                                patients[i].EmploymentStatus = newObject.EmploymentStatus;
                            }
                            i++;
                        }
                    }
                    ViewBag.salarySheetList = _salarySheetService.LoadSalarySheetList(_userMenu, viewModel);
                    ViewBag.ViewModel = viewModel;
                }
                ViewBag.IsSubmit = false;
            }

            catch (NullObjectException ex)
            {
                ViewBag.ErrMsg = ex.Message;
            }

            catch (InvalidDataException ex)
            {
                ViewBag.ErrMsg = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrMsg = WebHelper.SetExceptionMessage(ex);
            }
            return PartialView("Partial/_RegenerateTeamMemberListForSalarySheet", patients);
        }

        [HttpPost]
        public ActionResult SaveSalarySheet(string salarySheetViewModel, string buttonName, int month, int year)
        {
            string errorMessage = "";
            try
            {
                bool isSubmit = buttonName == "RegenerateSalarySheet" || buttonName == "FinaleSubmit";

                var lastDayOfMonth = DateTime.DaysInMonth(year, month);
                DateTime dateFrom = new DateTime(year, month, 1);
                DateTime dateTo = new DateTime(year, month, lastDayOfMonth);

                if (salarySheetViewModel == "")
                    throw new NullObjectException("Invalid data.");

                dynamic patientsList = JsonConvert.DeserializeObject(salarySheetViewModel);
                var objectList = new List<SalarySheet>();
                foreach (var patient in patientsList)
                {
                    var salaryst = new SalarySheet();
                    //var es = 2;
                    salaryst.TeamMember = _teamMemberService.LoadById(Convert.ToInt64(patient.TeamMemberId));
                    salaryst.SalaryHistory = _salaryHistoryService.GetSalaryHistory(Convert.ToInt64(patient.SalaryHistoryId));
                    salaryst.SalaryOrganization = patient.SalaryOrganizationId != null ? _organizationService.LoadById(Convert.ToInt64(patient.SalaryOrganizationId)) : null;
                    salaryst.SalaryBranch = patient.SalaryBranchId != null ? _branchService.GetBranch(Convert.ToInt64(patient.SalaryBranchId)) : null;
                    salaryst.SalaryCampus = patient.SalaryCampusId != null ? _campusService.GetCampus(Convert.ToInt64(patient.SalaryCampusId)) : null;
                    salaryst.SalaryDepartment = patient.SalaryDepartmentId != null ? _departmentService.LoadById(Convert.ToInt64(patient.SalaryDepartmentId)) : null;
                    salaryst.SalaryDesignation = patient.SalaryDesignationId != null ? _designationService.LoadById(Convert.ToInt64(patient.SalaryDesignationId)) : null;
                    salaryst.JobOrganization = patient.OrganizationId != null ? _organizationService.LoadById(Convert.ToInt64(patient.OrganizationId)) : null;
                    salaryst.JobBranch = patient.BranchId != null ? _branchService.GetBranch(Convert.ToInt64(patient.BranchId)) : null;
                    salaryst.JobCampus = patient.CampusId != null ? _campusService.GetCampus(Convert.ToInt64(patient.CampusId)) : null;
                    salaryst.JobDepartment = patient.DepartmentId != null ? _departmentService.LoadById(Convert.ToInt64(patient.DepartmentId)) : null;
                    salaryst.JobDesignation = patient.DesignationId != null ? _designationService.LoadById(Convert.ToInt64(patient.DesignationId)) : null;
                    salaryst.BasicSalaryAmount = patient.BasicSalary;
                    salaryst.HouseRentAmount = patient.HouseRent;
                    salaryst.ConveyanceAmount = patient.Conveyance;
                    salaryst.MedicalAmount = patient.Medical;
                    salaryst.GrossSalaryAmount = patient.GrossSalary;
                    salaryst.IncreamentAmount = patient.Increament;
                    salaryst.ArrearAmount = patient.Arrear;
                    salaryst.AutoZoneAmount = patient.ZoneAmount;
                    salaryst.FinalZoneAmount = patient.FinalZoneAmount;
                    salaryst.AutoAbsentAmount = patient.AbsentAmount;
                    salaryst.FinalAbsentAmount = patient.FinalAbsentAmount;
                    salaryst.LeaveWithoutPayAmount = patient.LeaveWithoutPayAmount;
                    salaryst.AutoLoanRefund = patient.Loan;
                    salaryst.FinalLoanRefund = patient.FinalLoanRefund;
                    salaryst.TdsAmount = patient.TdsAmount;
                    salaryst.BankAmount = patient.Bank;
                    salaryst.CashAmount = patient.Cash;
                    salaryst.TotalSalaryAmount = patient.TotalSalaryWithoutDeduction;
                    salaryst.TotalAmount = patient.TotalAmount;
                    salaryst.EbfAmount = patient.Ebf;
                    salaryst.EbfForEmplyer = patient.EbfForEmplyer;
                    salaryst.IsSubmit = isSubmit;
                    salaryst.StartDate = dateFrom;
                    salaryst.EndDate = dateTo;
                    salaryst.Year = year;
                    salaryst.Month = month;
                    salaryst.Remarks = patient.Remarks;
                    IList<TeamMemberZoneDto> tMemberZoneList = _zoneSettingService.TeamMemberZoneList(dateFrom, dateTo, Convert.ToInt64(patient.TeamMemberId),
                        Convert.ToInt32(patient.EmploymentStatus), (decimal)patient.TotalSalaryWithoutDeduction, (decimal)salaryst.JobOrganization.BasicSalary, 
                        (int)salaryst.JobOrganization.MonthlyWorkingDay,salaryst.JobOrganization.Id);

                    if (tMemberZoneList != null && tMemberZoneList.Count() > 0)
                    {
                        foreach (var teamMemberZoneDto in tMemberZoneList)
                        {
                            var dtils = new SalarySheetDetails()
                            {
                                SalarySheet = salaryst,
                                ZoneSetting =
                                    teamMemberZoneDto.ZoneSettingsId != null
                                        ? _zoneSettingService.LoadById(Convert.ToInt64(teamMemberZoneDto.ZoneSettingsId))
                                        : null,
                                ZoneCount =
                                    teamMemberZoneDto.ZoneCount != null
                                        ? Convert.ToInt32(teamMemberZoneDto.ZoneCount)
                                        : (int?) null,
                                ZoneUnitAmount =
                                    teamMemberZoneDto.ZoneUnitAmount != null
                                        ? Convert.ToDecimal(teamMemberZoneDto.ZoneUnitAmount)
                                        : (decimal?) null,
                                ZoneDeductionMultiplier =
                                    (decimal)
                                        (teamMemberZoneDto.ZoneDeductionMultiplier != null
                                            ? Convert.ToDecimal(teamMemberZoneDto.ZoneDeductionMultiplier)
                                            : (decimal?) null),
                                DeductionPerAbsent = Convert.ToDecimal(patient.DeductionPerAbsent),
                                TotalAbsentDay =
                                    patient.TotalAbsentDay != null ? Convert.ToInt32(patient.TotalAbsentDay) : null,
                                AbsentMultiplierFactor =
                                    patient.AbsentMultiplierFactor != null
                                        ? Convert.ToInt32(patient.AbsentMultiplierFactor)
                                        : null,
                            };
                            salaryst.SalarySheetDetailses.Add(dtils);
                        }
                    }
                    else
                    {
                        var dtils = new SalarySheetDetails()
                        {
                            SalarySheet = salaryst,
                            ZoneSetting = null,
                            ZoneCount = null,
                            ZoneUnitAmount = null,
                            ZoneDeductionMultiplier = null,
                            DeductionPerAbsent = Convert.ToDecimal(patient.DeductionPerAbsent),
                            TotalAbsentDay = patient.TotalAbsentDay != null ? Convert.ToInt32(patient.TotalAbsentDay) : null,
                            AbsentMultiplierFactor =
                                patient.AbsentMultiplierFactor != null
                                    ? Convert.ToInt32(patient.AbsentMultiplierFactor)
                                    : null,
                        };
                        salaryst.SalarySheetDetailses.Add(dtils);
                    }



                    var ebfHistory = new MemberEbfHistory()
                    {
                        SalarySheet = salaryst,
                        DateFrom = dateFrom,
                        DateTo = dateTo,
                        EmployeeBenefitsFundSetting = patient.EbfSettingsId != null ? _employeeBenefitsFundSettingService.GetEmployeeBenefitFundSetting(Convert.ToInt64(patient.EbfSettingsId)) : null,
                        CalculationOn = patient.CalculationOn != null ? Convert.ToDecimal(patient.CalculationOn) : null,
                        EmployeeContributionAmount = patient.EmployeeContribution != null ? Convert.ToDecimal(patient.EmployeeContribution) : null,
                        EmployoyerContributionAmount = patient.EmployerContribution != null ? Convert.ToDecimal(patient.EmployerContribution) : null
                    };

                    
                    salaryst.MemberEbfHistories.Add(ebfHistory);
                    objectList.Add(salaryst);
                }

                _salarySheetService.SaveOrUpdate(objectList);

                return Json(new { returnResult = "Successfully save", IsSuccess = true });
            }
            catch(InvalidDatePassedException ex)
            {
                errorMessage = ex.Message;
            }
            catch (MessageException ex)
            {
                errorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMessage = ex.Message;
            }
            catch (NullObjectException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.Errormessage = ex.Message;
                errorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { returnResult = errorMessage, IsSuccess = false });
        }

        public JsonResult ExportMemberListSalarySheet(string salarySheetViewModel, string totalBasicSalary, string totalHouseRent, string totalConveyance,
            string totalMedical, string totalGrossSalary, string totalIncreament, string totalArrear, string totalSalaryWithoutDeduction, string totalAbsent,
            string totalLoan, string totalTds, string totalEbf, string totalBank, string totalCash, string totalAmmount)
        {
            try
            {
                dynamic viewModelJson = JsonConvert.DeserializeObject(salarySheetViewModel);

                var viewModel = new SalarySheetFormViewModel()
                {
                    OrganizationType = (OrganizationType)viewModelJson.OrganizationType,
                    BranchId = Convert.ToInt64(viewModelJson.BranchId),
                    CampusId = Convert.ToInt64(viewModelJson.CampusId),
                    DepartmentId = Convert.ToInt64(viewModelJson.DepartmentId),
                    OrganizationId = Convert.ToInt64(viewModelJson.OrganizationId),
                    SalaryMonth = Convert.ToInt32(viewModelJson.SalaryMonth),
                    SalaryYear = Convert.ToInt32(viewModelJson.SalaryYear),
                };
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<SalarySheet> patients = _salarySheetService.LoadSalarySheetList(_userMenu, viewModel);
                var data = new List<List<object>>();
                var headerList = new List<string>();
                var columnList = new List<string>();
                var footerList = new List<string>();
                headerList.Add("Salary Sheet");
                footerList.Add("");

                columnList.Add("PIN");
                columnList.Add("Name");
                columnList.Add("Designation");
                columnList.Add("Basic Salary");
                columnList.Add("House Rent");
                columnList.Add("Conveyance");
                columnList.Add("Medical");
                columnList.Add("Gross Salary");
                columnList.Add("Increament");
                columnList.Add("Arrear");
                columnList.Add("Total Salary");
                columnList.Add("Zone");
                columnList.Add("Absent");
                columnList.Add("LWP");
                columnList.Add("Loan");
                columnList.Add("TDS");
                columnList.Add("EBF");
                columnList.Add("Bank");
                columnList.Add("Cash");
                columnList.Add("Total");
                columnList.Add("Remarks/Special Note");

                if (patients != null && patients.Any())
                {
                    foreach (var patient in patients)
                    {
                        var str = new List<object>();
                        str.Add(patient.TeamMember.Pin);
                        str.Add(patient.TeamMember.FullNameEng);
                        str.Add(patient.JobDesignation.Name);
                        str.Add(patient.BasicSalaryAmount);
                        str.Add(patient.HouseRentAmount);
                        str.Add(patient.ConveyanceAmount);
                        str.Add(patient.MedicalAmount);
                        str.Add(patient.GrossSalaryAmount);
                        str.Add(patient.IncreamentAmount);
                        str.Add(patient.ArrearAmount);
                        str.Add(patient.TotalSalaryAmount);
                        str.Add(patient.FinalZoneAmount);
                        str.Add(patient.FinalAbsentAmount);
                        str.Add(patient.LeaveWithoutPayAmount);
                        str.Add(patient.FinalLoanRefund);
                        str.Add(patient.TdsAmount);
                        str.Add(patient.EbfAmount);
                        str.Add(patient.BankAmount);
                        str.Add(patient.CashAmount);
                        str.Add(patient.TotalAmount);
                        str.Add(patient.Remarks);
                        data.Add(str);
                    }

                    var str2 = new List<object>();
                    str2.Add("");
                    str2.Add("");
                    str2.Add("Sub Total");
                    str2.Add(totalBasicSalary);
                    str2.Add(totalHouseRent);
                    str2.Add(totalConveyance);
                    str2.Add(totalMedical);
                    str2.Add(totalGrossSalary);
                    str2.Add(totalIncreament);
                    str2.Add(totalArrear);
                    str2.Add(totalSalaryWithoutDeduction);
                    str2.Add("");
                    str2.Add(totalAbsent);
                    str2.Add("");
                    str2.Add(totalLoan);
                    str2.Add(totalTds);
                    str2.Add(totalEbf);
                    str2.Add(totalBank);
                    str2.Add(totalCash);
                    str2.Add(totalAmmount);
                    data.Add(str2);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, data, footerList,
                    "salarySheet_" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return Json(HttpNotFound());
        }

        #endregion
    }
}