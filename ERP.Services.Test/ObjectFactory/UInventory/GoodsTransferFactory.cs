using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class GoodsTransferFactory : ObjectFactoryBase<GoodsTransfer>
    {
        #region Object Initialization

        private OrganizationFactory _organizationFactory;
        #endregion

        private readonly IGoodsTransferService _goodsTransferService;

        public GoodsTransferFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
        {
            _organizationFactory = new OrganizationFactory(this,_session);
            _goodsTransferService = new GoodsTransferService(session);

        }

        #region Create

        public GoodsTransferFactory Create()
        {
            Object = new GoodsTransfer()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Remarks = "Remarks Propertise",
                Status = GoodsTransfer.EntityStatus.Active
            };

            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsTransferFactory CreateWith(long id)
        {
            Object = null;
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsTransferFactory WithFromBranch(Branch branch)
        {
            Object.BranchFrom = branch;
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsTransferFactory WithToBranch(Branch branch)
        {
            Object.BranchTo = branch;
            SingleObjectList.Add(Object);
            return this;
        }


        public GoodsTransferFactory WithDetails(IList<GoodsTransferDetails> goodsTransferDetails)
        {
            Object.GoodsTransferDetails = goodsTransferDetails;
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsTransferFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _goodsTransferService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_goodsTransferService.SaveOrUpdate(x); });

            }
            return this;
        }

        public GoodsTransferFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new GoodsTransfer() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region Other


        #endregion

        #region CleanUp

        public GoodsTransferFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion

    }
}
