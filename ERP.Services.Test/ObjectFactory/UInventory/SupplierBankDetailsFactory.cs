using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class SupplierBankDetailsFactory : ObjectFactoryBase<SupplierBankDetails>
    {

        private readonly ISupplierBankDetailService _supplierBankDetailService;
        private readonly IBankBranchService _bankBranchService;
        private readonly BankFactory _bankFactory;
        private readonly BankBranchFactory _bankBranchFactory;
        public List<string> AssignAccountNoList = new List<string>();

        public SupplierBankDetailsFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _bankFactory = new BankFactory(this, session);
            _bankBranchFactory = new BankBranchFactory(this, session);
            _supplierBankDetailService = new SupplierBankDetailsService(session);
            _bankBranchService = new BankBranchService(session);

        }

        #region Create

        public SupplierBankDetailsFactory Create()
        {
            Object = new SupplierBankDetails()
            {
                Id = 0,
                AccountNo = GetUniqueAccountNo(),
                AccountTitle = "Personal",
                BusinessId = GetPcUserName(),
                CreateBy = 0,
                ModifyBy = 0,
                CreationDate = DateTime.Now,
                ModificationDate = DateTime.Now,
                Status = SupplierBankDetails.EntityStatus.Active,
                VersionNumber = 404,
                Rank = 1
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public SupplierBankDetailsFactory WithBankBranch()
        {
            _bankBranchFactory.Create().WithBank().Persist();
            Object.BankBranch = _bankBranchFactory.Object;
            return this;
        }
        public SupplierBankDetailsFactory WithBankBranch(BankBranch bankObj)
        {
            Object.BankBranch = bankObj;
            return this;
        }

        private string GetUniqueAccountNo()
        {
            int min = 1;
            int max = 999999;
            var list = _supplierBankDetailService.LoadAll();
            if (AssignAccountNoList == null)
                AssignAccountNoList = new List<string>();

            var codeList = list.Select(x => x.AccountNo).ToList<string>();
            if (AssignAccountNoList != null && AssignAccountNoList.Count > 0)
                codeList.AddRange(AssignAccountNoList);
            do
            {
                var r = new Random();
                var x = r.Next(min, max);
                string s = x.ToString("000000");
                if (codeList.Count > 0 && !codeList.Contains(s))
                {
                    AssignAccountNoList.Add(s);
                    return s;
                }
                if(codeList.Count <= 0)
                {
                    AssignAccountNoList.Add(s);
                    return s;
                }
                min++;
            } while (min <= max);
            return "Error Tin found";
        }

        public SupplierBankDetailsFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }
        
        public SupplierBankDetailsFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new SupplierBankDetails() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public SupplierBankDetailsFactory CleanUp()
        {

            DeleteAll();
            _bankBranchFactory.Cleanup();
            _bankFactory.Cleanup();
            return this;
        }

        #endregion

    }
}
