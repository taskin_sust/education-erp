﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface IMaterialTypeService : IBaseService
    {
        #region Operational Function
        void Save(MaterialType materialTypeObj);
        bool Update(long id, MaterialType materialTypeObj);
        bool Delete(long id);
        bool UpdateRank(MaterialType materialTypeOldObj, MaterialType materialTypeUpdateObj);
        #endregion

        #region Single Instances Loading Function
        MaterialType GetMaterialType(long materialTypeId);
        MaterialType GetMaterialTypeByRankNextOrPrevious(int rank, string action);
        #endregion

        #region List Loading Function
        IList<MaterialType> LoadMaterialType(long? organizationId = null);
        IList<MaterialType> LoadMaterialType(long sessionId, long programId);
        IList<MaterialType> LoadMaterialType(List<UserMenu> userMenu,int start, int length, string orderBy, string orderDir, string organization, string name, string shortName, string rank, string status);
        #endregion

        #region Others Function
        int GetMaximumRank(MaterialType obj);
        int GetMinimumRank(MaterialType obj);
        int GetMaterialTypeCount(List<UserMenu> userMenu,string orderBy, string orderDir, string organization, string name, string shortName, string rank, string status);
        #endregion
    }
    public class MaterialTypeService : BaseService, IMaterialTypeService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IMaterialTypeDao _materialTypeDao;
        private readonly IMaterialDao _materialDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly CommonHelper _commonHelper;
        public MaterialTypeService(ISession session)
        {
            Session = session;
            _materialTypeDao = new MaterialTypeDao { Session = Session };
            _materialDao = new MaterialDao { Session = Session };
            _organizationDao = new OrganizationDao { Session = Session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function
        private void CheckNullValue(MaterialType materialTypeObj)
        {
            if (materialTypeObj == null)
                throw new NullObjectException("Material type entity is null!");
            if (materialTypeObj.Name == null)
                throw new NullObjectException("Material type name is empty!");
            if (materialTypeObj.ShortName == null)
                throw new NullObjectException("Material type short name is empty!");
            if (materialTypeObj.Organization == null)
                throw new NullObjectException("Organization is empty!");
        }
        private void CheckBeforeSave(MaterialType materialTypeObj)
        {
            if (materialTypeObj == null)
            {
                throw new NullObjectException("Material Type can not be null");
            }
            var validationResult = ValidationHelper.ValidateEntity<MaterialType, MaterialTypeMetaData>(materialTypeObj);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }

            if (_materialTypeDao.HasDuplicateByName(materialTypeObj.Name, materialTypeObj.Organization.Id))
                throw new DuplicateEntryException("Duplicate material type found!");
            if (_materialTypeDao.HasDuplicateByShortName(materialTypeObj.ShortName, materialTypeObj.Organization.Id))
                throw new DuplicateEntryException("Duplicate short name found!");
        }
        public void Save(MaterialType materialTypeObj) 
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                   // CheckNullValue(materialTypeObj);
                    CheckBeforeSave(materialTypeObj);
                    materialTypeObj.Rank = _materialTypeDao.GetMaximumRank(materialTypeObj) + 1;
                    _materialTypeDao.Save(materialTypeObj);
                    trans.Commit();                   
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }
        private void CheckBeforeUpdate(MaterialType materialTypeObj)
        {
            if (materialTypeObj == null)
                throw new NullObjectException("Can't Update Null Object.");
            var validationResult = ValidationHelper.ValidateEntity<MaterialType, MaterialTypeMetaData>(materialTypeObj);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
            if (_materialTypeDao.HasDuplicateByName(materialTypeObj.Name, materialTypeObj.OrganizationId, materialTypeObj.Id))
                throw new DuplicateEntryException("Duplicate material type found");
            if (_materialTypeDao.HasDuplicateByShortName(materialTypeObj.ShortName, materialTypeObj.OrganizationId, materialTypeObj.Id))
                throw new DuplicateEntryException("Duplicate short name found");
        }
        public bool Update(long id, MaterialType materialTypeObj)
        {
            ITransaction trans = null;
            try
            {
                var tempMaterial = _materialTypeDao.LoadById(id);
                if (tempMaterial == null)
                    throw new NullObjectException("Material Type Not Found For Given Id.");
                CheckBeforeUpdate(materialTypeObj);
                using (trans = Session.BeginTransaction())
                {
                    tempMaterial.Name = materialTypeObj.Name;
                    tempMaterial.ShortName = materialTypeObj.ShortName;
                    if (tempMaterial.Organization != null && tempMaterial.Organization.Id != materialTypeObj.OrganizationId)
                    {
                        tempMaterial.Organization = materialTypeObj.Organization;
                    }
                    tempMaterial.Status = materialTypeObj.Status;
                    _materialTypeDao.Update(tempMaterial);
                    trans.Commit();
                    return true;
                }
            }
                
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }
        private void CheckBeforeDelete(MaterialType materialTypeObj)
        {
            //dependency check
            if (materialTypeObj.Materials.Count > 0)
                throw new DependencyException();
            
        }

        public bool Delete(long id)
        {
            ITransaction transaction = null;
            try
            {
                var erpMaterialType = _materialTypeDao.LoadById(id);
                CheckBeforeDelete(erpMaterialType);
                using (transaction = Session.BeginTransaction())
                {
                    var tempMaterial = _materialTypeDao.LoadById(id);
                    tempMaterial.Status = Material.EntityStatus.Delete;
                    _materialTypeDao.Update(tempMaterial);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool UpdateRank(MaterialType materialTypeOldObj, MaterialType materialTypeUpdateObj)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    _materialTypeDao.Update(materialTypeOldObj);
                    _materialTypeDao.Update(materialTypeUpdateObj);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public MaterialType GetMaterialType(long materialTypeId)
        {
            MaterialType materialType;
            try
            {
                materialType = _materialTypeDao.LoadById(materialTypeId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return materialType;
        }

        public MaterialType GetMaterialTypeByRankNextOrPrevious(int rank, string action)
        {
            MaterialType materialType;
            try
            {
                materialType = _materialTypeDao.LoadByRankDirection(rank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return materialType;
        }
        #endregion

        #region List Loading Function
        public IList<MaterialType> LoadMaterialType(long? organizationId = null)
        {
            IList<MaterialType> materialTypes;
            try
            {
                materialTypes = _materialTypeDao.LoadMaterialType(organizationId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return materialTypes;
        }
        public IList<MaterialType> LoadMaterialType(long sessionId, long programId)
        {
            IList<MaterialType> materialTypes;
            try
            {
                materialTypes = _materialTypeDao.LoadMaterialType(sessionId, programId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return materialTypes;
        }
        public IList<MaterialType> LoadMaterialType(List<UserMenu> userMenu ,int start, int length, string orderBy, string orderDir, string organization, string name, string shortName, string rank, string status)
        {
            IList<MaterialType> materialTypes;
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organization) ? (long?)null : Convert.ToInt32(organization);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                materialTypes = _materialTypeDao.LoadMaterialType(start, length, orderBy, orderDir, organizationIdList, name, shortName, rank, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return materialTypes;
        }
        #endregion

        #region Others Function
        public int GetMaximumRank(MaterialType obj)
        {
            int maximumRank;
            try
            {
                maximumRank = _materialTypeDao.GetMaximumRank(obj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return maximumRank;
        }
        public int GetMinimumRank(MaterialType obj)
        {
            int minimumRank;
            try
            {
                minimumRank= _materialTypeDao.GetMinimumRank(obj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return minimumRank;
        }
        public int GetMaterialTypeCount(List<UserMenu> userMenu,string orderBy, string orderDir, string organization, string name, string shortName, string rank, string status)
        {
            int materialTypeCount;
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organization) ? (long?)null : Convert.ToInt32(organization);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                materialTypeCount = _materialTypeDao.GetMaterialTypeCount(orderBy, orderDir, organizationIdList, name, shortName, rank, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return materialTypeCount;
        }

        #endregion
    }
}
