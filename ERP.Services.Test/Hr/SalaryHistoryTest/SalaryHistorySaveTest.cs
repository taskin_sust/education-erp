﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Services.Test.Hr.SalaryHistoryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "SalaryHistory")]
    public class SalaryHistorySaveTest : SalaryHistoryBaseTest
    {
        #region SaveOrUpdateSalaryHistory

        [Fact]
        public void InvalidDataExpection_SalaryHistory_Save_For_Null_Department()
        {
            var teamMember = PersistTeamMember(MentorId);
            //var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                //.WithDepartment(department)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));

        }

        [Fact]
        public void InvalidDataExpection_SalaryHistory_Save_For_Null_Organization()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                //.WithOrganization(org)
                .WithTeamMember(teamMember);
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));

        }

        [Fact]
        public void InvalidDataExpection_SalaryHistory_Save_For_Salary_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.Salary = null;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));

        }

        [Fact]
        public void InvalidDataExpection_SalaryHistory_Save_For_EffectiveDate_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.EffectiveDate = null;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));

        }

        [Fact]
        public void InvalidDataExpection_SalaryHistory_Save_For_BankAmount_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.BankAmount = null;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));

        }

        [Fact]
        public void InvalidDataExpection_SalaryHistory_Save_For_CashAmount_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.CashAmount = null;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));

        }

        [Fact]
        public void InvalidDataExpection_SalaryHistory_Save_For_Graterthen_Total_SalaryAmount()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.BankAmount = 350;
            _salaryHistoryFactory.Object.CashAmount = 300;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));

        }

        [Fact]
        public void InvalidDataExpection_SalaryHistory_Save_For_Invalid_Salary_Purpose()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.SalaryPurpose = new SalaryPurpose();
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));

        }

        #endregion

        #region SaveBatchSalaryHistory

        [Fact]
        public void Should_Return_InvalidDataExpection_SaveBatchSalaryHistory_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveBatchSalaryHistory(new List<SalaryHistory>() {_salaryHistoryFactory.Object});
            Assert.NotNull(_salaryHistoryService.GetSalaryHistory(_salaryHistoryFactory.Object.Id));
        }

        [Fact]
        public void Should_Return_InvalidDataExpection_SaveBatchSalaryHistory_Save_For_Null_Department()
        {
            var teamMember = PersistTeamMember(MentorId);
            //var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                //.WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveBatchSalaryHistory(new List<SalaryHistory>() { _salaryHistoryFactory.Object }));

        }

        [Fact]
        public void Should_Return_InvalidDataExpection_SaveBatchSalaryHistory_Save_For_Null_Designation()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            //var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                //.WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveBatchSalaryHistory(new List<SalaryHistory>() { _salaryHistoryFactory.Object }));

        }

        [Fact]
        public void Should_Return_InvalidDataExpection_SaveBatchSalaryHistory_Save_For_Null_Organization()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                //.WithOrganization(org)
                .WithTeamMember(teamMember);
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveBatchSalaryHistory(new List<SalaryHistory>() { _salaryHistoryFactory.Object }));

        }

        [Fact]
        public void Should_Return_InvalidDataExpection_SaveBatchSalaryHistory_Save_For_Salary_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.Salary = null;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveBatchSalaryHistory(new List<SalaryHistory>() { _salaryHistoryFactory.Object }));

        }

        [Fact]
        public void Should_Return_InvalidDataExpection_SaveBatchSalaryHistory_Save_For_EffectiveDate_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.EffectiveDate = null;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveBatchSalaryHistory(new List<SalaryHistory>() { _salaryHistoryFactory.Object }));

        }

        [Fact]
        public void Should_Return_InvalidDataExpection_SaveBatchSalaryHistory_Save_For_BankAmount_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.BankAmount = null;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveBatchSalaryHistory(new List<SalaryHistory>() { _salaryHistoryFactory.Object }));

        }

        [Fact]
        public void Should_Return_InvalidDataExpection_SaveBatchSalaryHistory_Save_For_CashAmount_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.CashAmount = null;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveBatchSalaryHistory(new List<SalaryHistory>() { _salaryHistoryFactory.Object }));

        }

        [Fact]
        public void Should_Return_InvalidDataExpection_SaveBatchSalaryHistory_Save_For_Graterthen_Total_SalaryAmount()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.BankAmount = 350;
            _salaryHistoryFactory.Object.CashAmount = 300;
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveBatchSalaryHistory(new List<SalaryHistory>() { _salaryHistoryFactory.Object }));

        }

        [Fact]
        public void Should_Return_InvalidDataExpection_SaveBatchSalaryHistory_Save_For_Invalid_Salary_Purpose()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryFactory.Object.SalaryPurpose = new SalaryPurpose();
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.SaveBatchSalaryHistory(new List<SalaryHistory>() { _salaryHistoryFactory.Object }));

        }
        
        #endregion
    }
}