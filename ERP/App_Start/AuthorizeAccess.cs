﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;
using log4net;
using Microsoft.AspNet.Identity;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Cache;

namespace UdvashERP.App_Start
{
    public class AuthorizeAccess : ActionFilterAttribute
    {
        private readonly ILog _logger = LogManager.GetLogger("AuthorizeAccess");


        public AuthorizeAccess()
        {
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                UserProfile userProfile = AuthorizationCache.GetCurrentUserProfile();
                AspNetUser aspNetUser = userProfile == null ? null : userProfile.AspNetUser;

                //for AllowAnonymous
                object[] attributes = filterContext.ActionDescriptor.GetCustomAttributes(typeof (AllowAnonymousAttribute), true);
                if (attributes.Any(a => a is AllowAnonymousAttribute))
                {
                    base.OnActionExecuting(filterContext);
                    if (aspNetUser != null)
                    {
                        //bool loadAll = (aspNetUser.UserName == ApplicationUsers.SuperAdmin);
                        //filterContext.Controller.ViewBag.AllMenu = AuthorizationCache.LoadMenuByUser(userProfile, loadAll);
                        //filterContext.Controller.ViewBag.AllMenu = AuthorizationCache.LoadMenuByUserProfile(userProfile);
                        var userMenuList = AuthorizationCache.LoadUserMenuByUserProfile(userProfile);
                        var menuList = FindUniqueMenu(userMenuList);
                        filterContext.Controller.ViewBag.AllMenu = menuList;
                    }
                    return;
                }

                //for AuthorizeRequired
                attributes = filterContext.ActionDescriptor.GetCustomAttributes(typeof (AuthorizeRequiredAttribute), true);
                if (attributes.Any(a => a is AuthorizeRequiredAttribute))
                {
                    base.OnActionExecuting(filterContext);
                    if (aspNetUser != null)
                    {
                        //bool loadAll = (aspNetUser.UserName == ApplicationUsers.SuperAdmin);
                        //filterContext.Controller.ViewBag.AllMenu = AuthorizationCache.LoadMenuByUser(userProfile, loadAll);
                        //filterContext.Controller.ViewBag.AllMenu = AuthorizationCache.LoadMenuByUserProfile(userProfile);
                        var userMenuList = AuthorizationCache.LoadUserMenuByUserProfile(userProfile);
                        var menuList = FindUniqueMenu(userMenuList);
                        filterContext.Controller.ViewBag.AllMenu = menuList;

                    }
                    else
                    {
                        RedirectToLogin(filterContext);
                    }
                    return;
                }

                //for everything else
                if (aspNetUser != null)
                {
                    var authInfo = AuthorizationCache.GetUserAuthInfo(aspNetUser);
                    if (aspNetUser.UserName == ApplicationUsers.SuperAdmin)
                    {
                        //var menuList = AuthorizationCache.LoadMenuByUser(userProfile, true);
                        //var menuList = AuthorizationCache.LoadMenuByUserProfile(userProfile);                     
                        var userMenuList = AuthorizationCache.LoadUserMenuByUserProfile(userProfile);
                        var menuList = FindUniqueMenu(userMenuList);
                        var authUserMenuList = FindAuthorizeUserMenu(filterContext, userProfile, userMenuList);
                        filterContext.Controller.ViewBag.AllMenu = menuList;
                        filterContext.Controller.ViewBag.UserMenu = authUserMenuList;
                    }
                    else if(authInfo.ForceLogoff)
                    {
                        RedirectToLogoff(filterContext);
                    }
                    else if (authInfo.PermissionDenied)
                    {
                        RedirectToPermissionDenied(filterContext);
                    }
                    else if (aspNetUser.UserName != ApplicationUsers.SuperAdmin)
                    {
                        if (userProfile.BusinessId == null)
                        {
                            //var menuList = AuthorizationCache.LoadMenuByUser(userProfile, false);
                            //var menuList = AuthorizationCache.LoadMenuByUserProfile(userProfile);  
                            //var userMenuList = FindAuthorizeUserMenu(filterContext, userProfile, menuList);
                            var userMenuList = AuthorizationCache.LoadUserMenuByUserProfile(userProfile);
                            var menuList = FindUniqueMenu(userMenuList);
                            var authUserMenuList = FindAuthorizeUserMenu(filterContext, userProfile, userMenuList);
                            if (authUserMenuList == null || authUserMenuList.Count < 1)
                            {
                                RedirectToPermissionDenied(filterContext);
                                return;
                            }
                            filterContext.Controller.ViewBag.AllMenu = menuList;
                            filterContext.Controller.ViewBag.UserMenu = authUserMenuList;
                        }
                        else
                        {
                            RedirectToManage(filterContext);
                        }

                    }
                    else
                    {
                        RedirectToPermissionDenied(filterContext);
                    }
                }
                else
                {
                    RedirectToLogin(filterContext);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                RedirectToPermissionDenied(filterContext);
            }

            base.OnActionExecuting(filterContext);
        }


        public static List<Menu> FindUniqueMenu(List<UserMenu> userMenuList)
        {
            //null protection
            if(userMenuList == null)
                userMenuList = new List<UserMenu>();

            //find unique
            var menuList = (from um in userMenuList select um.Menu).Distinct().ToList();

            //return
            return menuList;
        }

        private List<UserMenu> FindAuthorizeUserMenu(ActionExecutingContext filterContext, UserProfile userProfile, List<UserMenu> dbUserMenuList)
        {
            var authorizeUserMenu = new List<UserMenu>();

            #region For SuperAdmin

            if (userProfile.AspNetUser.UserName == ApplicationUsers.SuperAdmin)
            {
                foreach (var admUserMenu in dbUserMenuList)
                {
                    authorizeUserMenu.Add(new UserMenu(userProfile, admUserMenu.Menu));
                    break;
                }
                return authorizeUserMenu;
            }

            #endregion


            #region For Everyone-else

            // Find controller & action
            string controllerCurret = "", actionCurrent = "", controllerRef = "", actionRef = "";

            //current controller & action
            controllerCurret = (string) filterContext.RouteData.Values["controller"];
            actionCurrent = (string) filterContext.RouteData.Values["action"];
            long actionId = AuthorizationCache.GetActionId(actionCurrent, controllerCurret);

            //referrer controller & action
            Uri urlRef = filterContext.HttpContext.Request.UrlReferrer;
            if (urlRef != null && urlRef.Host == filterContext.HttpContext.Request.Url.Host)
            {
                //prepare
                var requestRef = new HttpRequest(null, urlRef.ToString(), null);
                var responseRef = new HttpResponse(new StringWriter());
                var httpContextRef = new HttpContext(requestRef, responseRef);

                // Extract the data    
                var routeDataRef = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContextRef));
                var values = routeDataRef.Values;
                controllerRef = (string) values["controller"] ?? "";
                actionRef = (string) values["action"] ?? "";
            }
            long actionRefId = AuthorizationCache.GetActionId(actionRef, controllerRef);
            
            ////Find authorize UserMenu
            //Get menuAccessList 
            var subQuery = from um in dbUserMenuList
                from ma in um.Menu.MenuAccess
                select ma;
            var menuAccessList = subQuery.ToList();

            //-------- 1.0--------
            //--------Check by default-actions from menuAccess------------
            //find authorizeMenuIds
            var queryMenuIdList = from ma in menuAccessList
                where
                    ma.Actions != null 
                    && ma.Actions.Id == actionId
                    && ma.HasReferrer == false
                select ma.Menu.Id;
            var menuIdList = queryMenuIdList.Distinct().ToList();

            //Get permittedUserMenu
            if (menuIdList.Any())
            {
                var queryUserMenuList = from um in dbUserMenuList
                    where menuIdList.Contains(um.Menu.Id)
                    select um;
                var userMenuList = queryUserMenuList.ToList();
                authorizeUserMenu.AddRange(userMenuList);
            }


            //-------- 2.0--------
            //--------Check by referance from menuAccess for AJAX------------
            if (authorizeUserMenu.Count < 1)
            {
                //get referrer menuIds
                var queryRefMenuIdList = from ma in menuAccessList
                                      where
                                          ma.Actions != null
                                          && ma.Actions.Id == actionRefId
                                          && ma.HasReferrer == false
                                      select ma.Menu.Id;
                var refMenuIdList = queryRefMenuIdList.ToList();

                //find authorizeMenuIds
                if (refMenuIdList.Any())
                {
                    var secQueryMenuIdList = from ma in menuAccessList
                                      where
                                          ma.Actions != null
                                          && ma.Actions.Id == actionId
                                          && ma.HasReferrer == true
                                          && refMenuIdList.Contains(ma.Menu.Id)
                                      select ma.Menu.Id;
                    var secMenuIdList = secQueryMenuIdList.ToList();

                    //Get permittedUserMenu
                    if (secMenuIdList.Any())
                    {
                        var queryUserMenuList = from um in dbUserMenuList
                                                where secMenuIdList.Contains(um.Menu.Id)
                                                select um;
                        var userMenuList = queryUserMenuList.ToList();
                        authorizeUserMenu.AddRange(userMenuList);
                    }                    
                }
            }
            
            #endregion

            return authorizeUserMenu;
        }

        private void RedirectToPermissionDenied(ActionExecutingContext filterContext)
        {
            var rvd = new RouteValueDictionary();
            rvd.Add("message", "Permission Denied. User appropiate login");
            filterContext.Result = new RedirectToRouteResult("PermissionDenied", rvd);
        }

        private void RedirectToManage(ActionExecutingContext filterContext)
        {
            var rvd = new RouteValueDictionary();
            rvd.Add("message", "Please change your password");
            filterContext.Result = new RedirectToRouteResult("Manage", rvd);
        }

        private void RedirectToLogin(ActionExecutingContext filterContext)
        {
            var rvd = new RouteValueDictionary();
            rvd.Add("message","Permission Denied. User appropiate login");
            filterContext.Result = new RedirectToRouteResult("Login", rvd);
        }

        private void RedirectToLogoff(ActionExecutingContext filterContext)
        {
            var rvd = new RouteValueDictionary();
            rvd.Add("message", "Please login again");
            filterContext.Result = new RedirectToRouteResult("LogOff", rvd);
        }
    }
}