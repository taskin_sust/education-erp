﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.SpecificationTemplateTest
{
    public interface ISpecificationTemplateSaveOrUpdateTest
    {
        #region basic test
        void Save_Should_Return_Null_Exception();
        void Should_Save_Successfully(); 
        #endregion

        #region business logic test
        void Should_Not_Save_Duplicate_Specification_Template();
        void Should_Not_Save_Duplicate_Specification_Criteria();
        void Should_Not_Save_Duplicate_Specification_Criteria_Option();
        void Should_Not_Save_Empty_Specification_Template_Name();
        void Should_Not_Save_Empty_Specification_Criteria_Name();
        void Should_Not_Save_Empty_Specification_Criteria_Option_Name();
        void Should_Not_Save_Without_Specification_Criteria();
        void Should_Not_Save_Without_Specification_Criteria_Option(); 
        #endregion
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "SpecificationCriteria")]
    public class SpecificationTemplateSaveOrUpdateTest : SpecificationTemplateBaseTest, ISpecificationTemplateSaveOrUpdateTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _specificationTemplateService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(Guid.NewGuid().ToString().Substring(0, 5)).Persist();
            var specificationT =
                _specificationTemplateService.GetSpecificationTemplate(_specificationTemplateFactory.Object.Id);
            Assert.NotNull(specificationT);
            Assert.True(_specificationTemplateFactory.Object.Id > 0);
            var criteriaList = _specificationTemplateFactory.Object.SpecificationCriteriaList;
            CleanUpSpecificationCriteria(criteriaList);
        }

        #endregion

        #region Business Logic Test
        [Fact]
        public void Should_Not_Save_Duplicate_Specification_Template()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name).Persist();
            var criteriaList = _specificationTemplateFactory.Object.SpecificationCriteriaList;
            var stFactory2 =
                _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name);
            Assert.Throws<DuplicateEntryException>(() => stFactory2.Persist());
            CleanUpSpecificationCriteria(criteriaList);
        }
        [Fact]
        public void Should_Not_Save_Duplicate_Specification_Criteria()
        {
            IList<SpecificationCriteria> specificationCriterias = new List<SpecificationCriteria>()
            {
                new SpecificationCriteria()
                {
                    Name = "A"
                },
                new SpecificationCriteria()
                {
                    Name = "A"
                }
            };
            _specificationTemplateFactory.Create().WithSpecificationCriteria(specificationCriterias).WithName(Guid.NewGuid().ToString().Substring(0, 5));
            Assert.Throws<DuplicateEntryException>(() => _specificationTemplateFactory.Persist());
        }
        [Fact]
        public void Should_Not_Save_Duplicate_Specification_Criteria_Option()
        {
            IList<SpecificationCriteria> specificationCriterias = new List<SpecificationCriteria>()
            {
                new SpecificationCriteria()
                {
                    Name = "A"
                }
            };
            IList<SpecificationCriteriaOption> specificationCriteriaOptions = new List<SpecificationCriteriaOption>()
            {
                new SpecificationCriteriaOption()
                {
                    Name = "B"
                },
                new SpecificationCriteriaOption()
                {
                    Name = "B"
                },
            };
            _specificationTemplateFactory.Create().WithSpecificationCriteria(specificationCriterias, specificationCriteriaOptions).WithName(Guid.NewGuid().ToString().Substring(0, 5));
            Assert.Throws<DuplicateEntryException>(() => _specificationTemplateFactory.Persist());
        }
        [Fact]
        public void Should_Not_Save_Empty_Specification_Template_Name()
        {
            _specificationTemplateFactory.Create().WithName("").WithSpecificationCriteria();
            Assert.Throws<InvalidDataException>(() => _specificationTemplateFactory.Persist());
        }
        [Fact]
        public void Should_Not_Save_Empty_Specification_Criteria_Name()
        {
            IList<SpecificationCriteria> specificationCriterias = new List<SpecificationCriteria>()
            {
                new SpecificationCriteria()
                {
                    Name = null
                }
            };
            IList<SpecificationCriteriaOption> specificationCriteriaOptions = new List<SpecificationCriteriaOption>()
            {
                new SpecificationCriteriaOption()
                {
                    Name = "B"
                },
                new SpecificationCriteriaOption()
                {
                    Name = "B"
                },
            };
            _specificationTemplateFactory.Create().WithSpecificationCriteria(specificationCriterias, specificationCriteriaOptions).WithName(Guid.NewGuid().ToString().Substring(0, 5));
            Assert.Throws<InvalidDataException>(() => _specificationTemplateFactory.Persist());
        }
        [Fact]
        public void Should_Not_Save_Empty_Specification_Criteria_Option_Name()
        {
            IList<SpecificationCriteria> specificationCriterias = new List<SpecificationCriteria>()
            {
                new SpecificationCriteria()
                {
                    Name = "A"
                }
            };
            IList<SpecificationCriteriaOption> specificationCriteriaOptions = new List<SpecificationCriteriaOption>()
            {
                new SpecificationCriteriaOption()
                {
                    Name = "B"
                },
                new SpecificationCriteriaOption()
                {
                    Name = null
                },
            };
            _specificationTemplateFactory.Create().WithSpecificationCriteria(specificationCriterias, specificationCriteriaOptions).WithName(Guid.NewGuid().ToString().Substring(0, 5));
            Assert.Throws<InvalidDataException>(() => _specificationTemplateFactory.Persist());
        }
        
        [Fact]
        public void Should_Not_Save_Without_Specification_Criteria()
        {
            _specificationTemplateFactory.Create().WithName(Guid.NewGuid().ToString().Substring(0, 5));
            Assert.Throws<InvalidDataException>(() => _specificationTemplateFactory.Persist());
        }
        
        [Fact]
        public void Should_Not_Save_Without_Specification_Criteria_Option()
        {
            IList<SpecificationCriteria> specificationCriterias = new List<SpecificationCriteria>()
            {
                new SpecificationCriteria()
                {
                    Controltype = FieldType.Dropdown,
                    Name = "A"
                }
            };
            
            _specificationTemplateFactory.Create().WithSpecificationCriteria(specificationCriterias).WithName(Guid.NewGuid().ToString().Substring(0, 5));
            Assert.Throws<InvalidDataException>(() => _specificationTemplateFactory.Persist());
        }
        #endregion
    }
}
