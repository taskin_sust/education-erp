﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public partial class SmsMaskMap : BaseClassMap<SmsMask, long>
    {
        public SmsMaskMap()
        {
            Table("SmsMask");
            LazyLoad();
            Map(x => x.Rank);
            Map(x => x.Name);
            References(x => x.Organization).Column("OrganizationId");
            HasMany(x => x.SmsStudentRegistrationSettingList).KeyColumn("SmsMaskId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
