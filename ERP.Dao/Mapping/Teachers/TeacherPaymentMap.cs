﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherPaymentMap : BaseClassMap<TeacherPayment, long>
    {
        public TeacherPaymentMap()
        {
            Table("TeacherPayment");
            LazyLoad();
            References(x => x.Teacher).Column("TeacherId");
            References(x => x.PaidFromBranch).Column("PaidFromBranchId");
            Map(x => x.PaidBy).Column("PaidBy");
            Map(x => x.PaymentDate).Column("PaymentDate");
            Map(x => x.VoucherNo).Column("VoucherNo");
            Map(x => x.TotalPaidAmount).Column("TotalPaidAmount");
            Map(x => x.TotalClassQuantity).Column("TotalClassQuantity");
            HasMany(x => x.TeacherPaymentDetails).KeyColumn("TeacherPaymentId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
