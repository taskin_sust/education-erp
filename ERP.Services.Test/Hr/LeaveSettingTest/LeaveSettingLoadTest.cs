﻿using System;
using System.Collections.Generic;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.LeaveSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "LeaveSettings")]
    public class LeaveSettingLoadTest : LeaveSettingBaseTest
    {

        #region GetById

        [Fact]
        public void Should_Return_GetById_Null()
        {
            long testId = -1;
            var reult = _leaveService.GetById(testId);
            Assert.Equal(reult, null);
        }

        [Fact]
        public void Should_Return_GetById_Object()
        {
            leaveFactory.Create().WithOrganization().Persist();
            var reult = _leaveService.GetById(leaveFactory.Object.Id);
            Assert.NotEqual(reult, null);
        }

        [Fact]
        public void Should_Return_GetById_List()
        {
            leaveFactory.CreateMore(5).Persist();
            IList<Leave> expectedLeaves = new List<Leave>();
            leaveFactory.ObjectList.ForEach(obj =>
            {
                var leave = _leaveService.GetById(obj.Id);
                expectedLeaves.Add(leave);
            });

            Assert.Equal(expectedLeaves, leaveFactory.ObjectList);
        }

        #endregion

        #region GetLeaveCount

        [Fact]
        public void Should_Return_GetLeaveCount_Successfully()
        {
            leaveFactory.CreateMore(10).Persist();
            var userMenu = BuildUserMenu(leaveFactory.ObjectList.Select(x => x.Organization).Distinct().ToList());
            Assert.Equal(10, _leaveService.GetLeaveCount(userMenu, leaveFactory.ObjectList.Select(x => x.Organization).FirstOrDefault().Id));
        }

        [Fact]
        public void Should_Return_GetLeaveCount_Invalid_Usermenu()
        {
            leaveFactory.CreateMore(10).Persist();
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveService.GetLeaveCount(null,
                        leaveFactory.ObjectList.Select(x => x.Organization).FirstOrDefault().Id));
        }

        [Fact]
        public void Should_Return_GetLeaveCount_Invalid_Organization()
        {
            leaveFactory.CreateMore(10).Persist();
            var userMenu = BuildUserMenu(leaveFactory.ObjectList.Select(x => x.Organization).Distinct().ToList());
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveService.GetLeaveCount(userMenu,
                        leaveFactory.ObjectList.Select(x => x.Organization).FirstOrDefault().Id + 1));
        }

        #endregion

        #region GetLeave

        [Fact]
        public void Should_Return_GetLeave_Invalid_Id()
        {
            var leaveObj = leaveFactory.Create().WithOrganization().Persist().Object;
            var dataObj = _leaveService.GetLeave(leaveObj.Id + 1);
            Assert.Null(dataObj);
        }

        [Fact]
        public void Should_Return_GetLeave_Object()
        {
            var leaveObj = leaveFactory.Create().WithOrganization().Persist().Object;
            var dataObj = _leaveService.GetLeave(leaveObj.Id);
            Assert.NotNull(dataObj);
        }

        #endregion

        #region LoadLeave

        [Fact]
        public void Should_Return_LoadLeave_All_ObjectList()
        {
            leaveFactory.Create().WithOrganization().Persist();
            var leaveList = _leaveService.LoadLeave(null);
            Assert.NotEmpty(leaveList);
        }

        [Fact]
        public void Should_Return_LoadLeave_List()
        {
            leaveFactory.CreateMore(10).Persist();
            var orgList = leaveFactory.ObjectList.Select(x => x.Organization.Id).ToList();
            var leaveList = _leaveService.LoadLeave(orgList);
            Assert.Equal(leaveFactory.ObjectList.Count, leaveList.Count);
        }

        [Fact]
        public void Should_Return_LoadLeave_Invalid_UserMenu()
        {
            leaveFactory.CreateMore(10).Persist();
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveService.LoadLeave(null,
                        leaveFactory.ObjectList.Select(x => x.Organization.Id).FirstOrDefault()));
        }

        [Fact]
        public void Should_Return_LoadLeave_Invalid_Organization()
        {
            leaveFactory.CreateMore(10).Persist();
            var userMenu = BuildUserMenu(leaveFactory.ObjectList.Select(x => x.Organization).ToList());
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveService.LoadLeave(userMenu,
                        leaveFactory.ObjectList.Select(x => x.Organization.Id).FirstOrDefault() + 1));
        }

        [Fact]
        public void Should_Return_LoadLeave_Successfully()
        {
            leaveFactory.CreateMore(10).Persist();
            var userMenu = BuildUserMenu(leaveFactory.ObjectList.Select(x => x.Organization).ToList());
            Assert.Equal(10, _leaveService.LoadLeave(userMenu,
                        leaveFactory.ObjectList.Select(x => x.Organization.Id).FirstOrDefault()).Count());
        }

        #endregion

        #region LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus

        [Fact]
        public void Should_Return_Invalid_Id()
        {
            const int employmentStatus = (int)MemberEmploymentStatus.Permanent;
            const int maritalInfoId = (int)MaritalType.Single;
            const int gender = (int)Gender.Male;
            var org = organizationFactory.Create().Persist().Object;
            leaveFactory.CreateMore(5, org).Persist();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(org.Id + 1, gender, employmentStatus, maritalInfoId);
            Assert.Equal(leaveList.Count, 0);
        }

        [Fact]
        public void Should_Paremeter_Check()
        {
            const int employmentStatus = (int)MemberEmploymentStatus.Permanent;
            const int maritalInfoId = (int)MaritalType.Single;
            // const int gender = (int)Gender.Male;
            var org = organizationFactory.Create().Persist().Object;
            leaveFactory.CreateMore(5, org).Persist();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(org.Id, null, employmentStatus, maritalInfoId);
            Assert.NotEmpty(leaveList);
        }

        [Fact]
        public void Should_Return_List()
        {
            const int employmentStatus = (int)MemberEmploymentStatus.Permanent;
            const int maritalInfoId = (int)MaritalType.Single;
            const int gender = (int)Gender.Male;
            var org = organizationFactory.Create().Persist().Object;
            leaveFactory.CreateMore(5, org).Persist();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(org.Id, gender, employmentStatus, maritalInfoId);
            Assert.NotEmpty(leaveList);
        }

        #endregion

    }

}
