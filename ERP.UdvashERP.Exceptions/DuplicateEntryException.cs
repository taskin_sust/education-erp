﻿using System;

namespace UdvashERP.MessageExceptions
{
    public class DuplicateEntryException : Exception
    {
        public DuplicateEntryException():base("Duplicate Found")
        {

        }

        public DuplicateEntryException(string message):base(message)
        {

        }
    }
}
