﻿$(document).on('click', '#submit', function (e) {

    $('.field-validation-error').remove();
    var programRoll = $('#ProgramRoll').val().trim();
    var isSuccess = true;

    if (programRoll == null || programRoll == "") {
        var validationMessage1 = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Program Roll is required</span></span>';
        $('#ProgramRoll').parent().append(validationMessage1);
        isSuccess = false;
    }
    else if (programRoll.length < 11 || programRoll.length > 11) {
        var validationMessage2 = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Program Roll will be exact 11 digit</span></span>';
        $('#ProgramRoll').parent().append(validationMessage2);
        isSuccess = false;
    }
    else {
        var reg = /^\d+$/;
        if (reg.test(programRoll) == false || programRoll <= 0) {
            var validationMessage3 = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Program Roll is not valid</span></span>';
            $('#ProgramRoll').parent().append(validationMessage3);
            isSuccess = false;
        }
    }

    if (isSuccess == true) { 
         return true;
    }
    else {
        e.preventDefault(); 
        return false;
    }
});
