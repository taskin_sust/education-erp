using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory {
    
    
    public class GoodsIssueDetailsMap : BaseClassMap<GoodsIssueDetails, long> {
        
        public GoodsIssueDetailsMap() {
			Table("UINV_GoodsIssueDetails");
			LazyLoad();
			References(x => x.GoodsIssue).Column("GoodsIssueId");
            References(x => x.PurposeProgramId).Column("PurposeProgramId");
            References(x => x.PurposeSessionId).Column("PurposeSessionId");
			References(x => x.Item).Column("ItemId");
			References(x => x.Program).Column("ProgramId");
			References(x => x.Session).Column("SessionId");
			Map(x => x.Purpose).Column("Purpose");
			Map(x => x.IssuedQuantity).Column("IssuedQuantity");
        }
    }
}
