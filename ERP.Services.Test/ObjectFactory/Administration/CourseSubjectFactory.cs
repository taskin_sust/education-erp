﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
   public class CourseSubjectFactory: ObjectFactoryBase<CourseSubject>
   {
       #region Object Initialization
       public CourseSubjectFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)       
       {
           
       }
       #endregion


       #region Create
       public CourseSubjectFactory CreateMore(Course course, List<Subject> subjects )  
       {
           CreateCourseSybjectList(course, subjects);
           return this;
       }

       private IList<CourseSubject> CreateCourseSybjectList(Course course, List<Subject> subjects) 
       {
           ListStartIndex = ObjectList.Count;
           var list = new List<CourseSubject>();
           foreach (var subject in subjects)
           {
               var courseSubject = new CourseSubject()
               {
                   Id = 0,
                   BusinessId = GetPcUserName(),                   
                   CreateBy = 1,
                   ModifyBy = 1,
                   Payment = 3500,
                   TutionFee = 2500,
                   GuideFee = 1000,
                   Course = course,
                   Subject = subject
               };
               list.Add(courseSubject);
           }
           ObjectList.AddRange(list);
           
           return list;
       }

       #endregion

       #region Cleanup
       public void Cleanup()
       {
           DeleteAll();
       }
       #endregion
   }
}
