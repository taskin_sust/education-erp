using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{


    public class GoodsReceiveDetailsMap : BaseClassMap<GoodsReceiveDetails, long>
    {

        public GoodsReceiveDetailsMap()
        {
            Table("UINV_GoodsReceiveDetails");
            LazyLoad();
            References(x => x.WorkOrderDetail).Column("WorkOrderDetailId");
            References(x => x.PurposeProgramId).Column("PurposeProgramId");
            References(x => x.PurposeSessionId).Column("PurposeSessionId");
            References(x => x.Item).Column("ItemId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.GoodsReceive).Column("GoodsRecieveId");
            Map(x => x.Purpose).Column("Purpose");
            Map(x => x.ReceivedQuantity).Column("ReceivedQuantity").Not.Nullable();
            Map(x => x.Rate).Column("Rate").Not.Nullable();
            Map(x => x.TotalCost).Column("TotalCost").Not.Nullable();
        }
    }
}
