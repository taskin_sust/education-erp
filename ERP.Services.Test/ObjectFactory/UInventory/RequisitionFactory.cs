using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FluentNHibernate.Testing.Values;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class RequisitionFactory : ObjectFactoryBase<Requisition>
    {
        public List<string> AssignedSlipNoList = new List<string>();
        private OrganizationFactory _organizationFactory;
        private BranchFactory _branchFactory;

        private readonly IRequisitionService _requisitionService;
        public IOrganizationService _organizationService;
        public RequisitionFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _organizationFactory = new OrganizationFactory(this, _session);
            _branchFactory = new BranchFactory(this, _session);
            
            _requisitionService = new RequisitionService(session);
            _organizationService = new OrganizationService(session);
            _organizationService = new OrganizationService(session);

        }

        #region Create

        public RequisitionFactory Create(Organization organization)
        {
            var branchFactory = _branchFactory.Create().WithOrganization(organization).Persist();
            Object = new Requisition
            {
                RequiredDate = DateTime.Today,
                VersionNumber = 404,
                CreationDate = DateTime.Now,
                ModificationDate = DateTime.Now,
                ModifyBy = 0,
                CreateBy = 0,
                RequisitionNo = GetRequisitionName(organization, branchFactory.Object),
                RequisionStatus = Requisition.EntityStatus.Pending,
                Remarks = GetPcUserName(),
                BusinessId = GetPcUserName(),
                Branch = branchFactory.Object
            };
            SingleObjectList.Add(Object);
            return this;
        }
        
        public RequisitionFactory CreateWithBranch(Organization organization, Branch branch)
        {
            Object = new Requisition
            {
                RequiredDate = DateTime.Today,
                VersionNumber = 404,
                CreationDate = DateTime.Now,
                ModificationDate = DateTime.Now,
                ModifyBy = 0,
                CreateBy = 0,
                RequisitionNo = GetRequisitionName(organization, branch),
                RequisionStatus = Requisition.EntityStatus.Pending,
                Remarks = GetPcUserName(),
                BusinessId = GetPcUserName(),
                Branch = branch
            };
            SingleObjectList.Add(Object);
            return this;
        }
        
        public RequisitionFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public RequisitionFactory Persist()
        {


            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                _requisitionService.SaveOrUpdate(SingleObjectList[0]);
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                foreach (var obj in ObjectList)
                {
                    _requisitionService.SaveOrUpdate(obj);
                }
            }
            return this;
        }

        public RequisitionFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new Requisition() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        public RequisitionFactory CreateMore(Organization organization, List<Branch> branchList, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Object = new Requisition
                {
                    RequiredDate = DateTime.Today,
                    VersionNumber = 404,
                    CreationDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    ModifyBy = 0,
                    CreateBy = 0,
                    RequisitionNo = GetRequisitionName(organization, branchList[i]),
                    RequisionStatus = Requisition.EntityStatus.Pending,
                    Remarks = GetPcUserName(),
                    BusinessId = GetPcUserName(),
                    Branch = branchList[i]
                };
                ObjectList.Add(Object);
            }
            return this;
        }

        #endregion

        #region other
        private string GetRequisitionName(Organization orgObj, Branch branchObj)
        {
            int min = 1;
            const int max = 999999;
            var organizationName = _organizationService.LoadById(orgObj.Id).ShortName.Substring(0, 3);
            var branchCode = branchObj.Code.ToString(CultureInfo.CurrentCulture);
            do
            {
                string name = "";
                var r = new Random();
                var x = r.Next(min, max);
                string newSl = x.ToString("000000");

                name = organizationName + branchCode + "RS" + newSl;
                if (!AssignedSlipNoList.Contains(name))
                {
                    AssignedSlipNoList.Add(name);
                    return name;
                }
                min++;

            } while (min <= max);
            return "Error RequisitionNo Genaration";
        }
        #endregion

        #region CleanUp

        public RequisitionFactory CleanUp()
        {
            DeleteAll();
            _branchFactory.Cleanup();
            _organizationFactory.Cleanup();
            return this;
        }

        #endregion

    }
}
