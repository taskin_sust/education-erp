﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.ItemTest
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "Item")]
    public class ItemLoadTest : ItemBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_LoadById_Null()
        {
            var itemFactory = _itemFactory.Create().WithItemGroup().WithOrganization().Persist();
            Item item = _itemService.LoadById(itemFactory.Object.Id + 1);
            Assert.Null(item);
        }
        
        [Fact]
        public void Should_Return_LoadById_NotNull()
        {
            var itemFactory = _itemFactory.Create().WithOrganization().WithItemGroup().Persist();
            Item item = _itemService.LoadById(itemFactory.Object.Id);
            Assert.NotNull(item);
            Assert.True(item.Id > 0);
        }

        [Fact]
        public void Should_Return_Load_item_Count()
        {
            const int numOfItem = 10;
            _itemFactory.CreateMore(numOfItem).Persist();
            var list = _itemService.LoadItemCount(_itemFactory.ObjectList.Select(x => x.Organization.Id).FirstOrDefault(), null, null, "", null);
            Assert.StrictEqual(list, numOfItem);
        }

        [Fact]
        public void should_Return_Load_Item_ParameterWize()
        {
            const int numOfItemGroup = 10;
            var itemFactory = _itemFactory.CreateMore(numOfItemGroup).Persist();
            var list = _itemService.LoadItemCount(null, null, null, itemFactory.ObjectList.Select(x => x.Name).FirstOrDefault(), null);
            Assert.StrictEqual(list, 1);
        }

        [Fact]
        public void Should_Return_Item_Empty_Value()
        {
            const int numOfItemGroup = 10;
            _itemFactory.CreateMore(numOfItemGroup).Persist();
            var list = _itemService.LoadItemCount(null, null, null, "TestCheck", null);
            Assert.StrictEqual(list, 0);
        }

        [Fact]
        public void should_Return_Load_All_Item_List()
        {
            const int numOfItem = 10;
            _itemFactory.CreateMore(numOfItem).Persist();
            var list = _itemService.LoadItem(0, 10, null, null, _itemFactory.ObjectList.Select(x => x.Organization.Id).FirstOrDefault(), null, null, null, null);
            Assert.StrictEqual(list.Count, numOfItem);
        }

        [Fact]
        public void Should_Return_Item_List()
        {
            const int numOfItem = 10;
            var itemFactory = _itemFactory.CreateMore(numOfItem).Persist();
            var list = _itemService.LoadItem(0, numOfItem, null, null, null, null, null, itemFactory.ObjectList.Select(x => x.Name).FirstOrDefault(), null);
            Assert.StrictEqual(list.Count, 1);
        }

        #endregion
    }
}
