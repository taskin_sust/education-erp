﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.StockReport
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "StockReport")]
    public class StockReportLoadTest : StockReportBaseTest
    {
        [Fact]
        public void Should_Return_Successfully()
        {
            const string orderBy = "ItemGroup";
            const string orderDir = "desc";
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate)
                        .WithItemGroup(_itemGroupFactory.Object)
                        .WithOrganization(_organizationFactory.Object)
                        .Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), 
                                        _commonHelper.ConvertIdToList(_programFactory.Object), 
                                        _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            //Opening
            _goodsReceiveDetailsFactory.Create()
                                        .WithPurpose()
                                        .WithItem(_itemFactory.Object)
                                        .WithProgram(_programFactory.Object)
                                        .WithSession(_sessionFactory.Object);
            _goodsReceiveFactory.Create().WithGoodsReceiveDetails(_goodsReceiveDetailsFactory.Object);
            var receiving = _goodsReceiveFactory.WithBranch(_branchFactory.Object).Persist(true);
            GoodsReceive goodsReceive = _goodsReceiveService.GetGoodsReceive(receiving.Object.Id);


            //Receive
            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create()
                                        .WithProgram(_programFactory.Object)
                                        .WithSession(_sessionFactory.Object)
                                        .WithItem(_itemFactory.Object)
                                        .WithPurpose()
                                        .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            var list = _stockReportService.StockSummaryReport(0, 10, orderBy, orderDir, usermenu, _organizationFactory.Object.Id,
                            _branchFactory.Object.Id, new int[] { (int)Purpose.ProgramPurpose }, programSessionList.ToArray(), new long[] { _itemGroupFactory.Object.Id},
                            new long[]{_itemFactory.Object.Id}, DateTime.Now.AddDays(2), DateTime.Now.AddMonths(2));

            Assert.True(list.Count > 0);
        }

        [Fact]
        public void Should_Return_Branch_Unauthorized()
        {
            const string orderBy = "ItemGroup";
            const string orderDir = "desc";
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate)
                        .WithItemGroup(_itemGroupFactory.Object)
                        .WithOrganization(_organizationFactory.Object)
                        .Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            /*var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object),
                                        _commonHelper.ConvertIdToList(_programFactory.Object),
                                        _commonHelper.ConvertIdToList(_branchFactory.Object));*/
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            //Opening
            _goodsReceiveDetailsFactory.Create()
                                        .WithPurpose()
                                        .WithItem(_itemFactory.Object)
                                        .WithProgram(_programFactory.Object)
                                        .WithSession(_sessionFactory.Object);
            _goodsReceiveFactory.Create().WithGoodsReceiveDetails(_goodsReceiveDetailsFactory.Object);
            var receiving = _goodsReceiveFactory.WithBranch(_branchFactory.Object).Persist(true);
            GoodsReceive goodsReceive = _goodsReceiveService.GetGoodsReceive(receiving.Object.Id);


            //Receive
            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create()
                                        .WithProgram(_programFactory.Object)
                                        .WithSession(_sessionFactory.Object)
                                        .WithItem(_itemFactory.Object)
                                        .WithPurpose()
                                        .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            var list = _stockReportService.StockSummaryReport(0, 10, orderBy, orderDir, new List<UserMenu>(), _organizationFactory.Object.Id,
                            _branchFactory.Object.Id, new int[] { (int)Purpose.ProgramPurpose }, programSessionList.ToArray(), new long[] { _itemGroupFactory.Object.Id },
                            new long[] { _itemFactory.Object.Id }, DateTime.Now.AddDays(2), DateTime.Now.AddMonths(2));

            Assert.Throws<InvalidDataException>(() => _stockReportService.StockSummaryReport(0, 10, orderBy, orderDir, new List<UserMenu>(), _organizationFactory.Object.Id,
                            _branchFactory.Object.Id, new int[] { (int)Purpose.ProgramPurpose }, programSessionList.ToArray(), new long[] { _itemGroupFactory.Object.Id },
                            new long[] { _itemFactory.Object.Id }, DateTime.Now.AddDays(2), DateTime.Now.AddMonths(2)));
        }


        [Fact]
        public void StockScheduleSummaryDetails_Return_Successfully()
        {
            const string orderBy = "ItemGroup";
            const string orderDir = "desc";
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate)
                        .WithItemGroup(_itemGroupFactory.Object)
                        .WithOrganization(_organizationFactory.Object)
                        .Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object),
                                        _commonHelper.ConvertIdToList(_programFactory.Object),
                                        _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            //Opening
            _goodsReceiveDetailsFactory.Create()
                                        .WithPurpose()
                                        .WithProperty(1000)
                                        .WithItem(_itemFactory.Object)
                                        .WithProgram(_programFactory.Object)
                                        .WithSession(_sessionFactory.Object);
            _goodsReceiveFactory.Create().WithGoodsReceiveDetails(_goodsReceiveDetailsFactory.Object);
            var receiving = _goodsReceiveFactory.WithBranch(_branchFactory.Object).Persist(true);
            GoodsReceive goodsReceive = _goodsReceiveService.GetGoodsReceive(receiving.Object.Id);


            //Receive
            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create()
                                        .WithProgram(_programFactory.Object)
                                        .WithSession(_sessionFactory.Object)
                                        .WithItem(_itemFactory.Object)
                                        .WithPurpose()
                                        .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            var list = _stockReportService.StockSummaryDetailsReport(0, 10, usermenu, _organizationFactory.Object.Id,
                                            _branchFactory.Object.Id, new int[] {(int) Purpose.ProgramPurpose}, programSessionList.ToArray(),
                                            new long[] {_itemGroupFactory.Object.Id}, new long[] {_itemFactory.Object.Id}, DateTime.Now.AddDays(2), DateTime.Now.AddDays(10));
            Assert.True(list.Count > 0);
        }
    }
}
