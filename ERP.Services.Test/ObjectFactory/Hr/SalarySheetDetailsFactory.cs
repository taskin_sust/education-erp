﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class SalarySheetDetailsFactory:ObjectFactoryBase<SalarySheetDetails>
    {
        public SalarySheetDetailsFactory(IObjectFactoryBase caller,ISession session):base(caller,session)
        {

        }

        public SalarySheetDetailsFactory Create()
        {
            Object = new SalarySheetDetails()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = MemberEbfHistory.EntityStatus.Active,
                IsBasic = true
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public SalarySheetDetailsFactory WithZone(ZoneSetting zoneSetting,int zoneCount,decimal zoneUnitAmount,decimal zoneDeductionMultiplier)
        {
            Object.ZoneSetting = zoneSetting;
            Object.ZoneCount = zoneCount;
            Object.ZoneUnitAmount = zoneUnitAmount;
            Object.ZoneDeductionMultiplier = zoneDeductionMultiplier;
            return this;
        }

        public SalarySheetDetailsFactory WithAbsentDay(decimal deductionPerAbsent, int totalAbsentDay, int absentMultiplierFactor)
        {
            Object.DeductionPerAbsent = deductionPerAbsent;
            Object.TotalAbsentDay = totalAbsentDay;
            Object.AbsentMultiplierFactor = absentMultiplierFactor;
            return this;
        }


        public SalarySheetDetailsFactory CreateWith(long id,bool isBasic)
        {
            Object = new SalarySheetDetails()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = MemberEbfHistory.EntityStatus.Active,
                IsBasic = isBasic
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public SalarySheetDetailsFactory WithSalarySheet(SalarySheet salarySheet)
        {
            Object.SalarySheet = salarySheet;
            return this;
        }

        public SalarySheetDetailsFactory CleanUp()
        {
            DeleteAll();
            return this;
        }
    }
}
