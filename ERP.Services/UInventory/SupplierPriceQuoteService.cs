using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Testing.Values;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface ISupplierPriceQuoteService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(SupplierPriceQuote supplierPriceQuote);

        #endregion

        #region Single Instances Loading Function

        SupplierPriceQuote GetSupplierPriceQuote(long id);

        #endregion

        #region List Loading Function
        #endregion

        #region Others Function

        bool CheckSupplierPriceQuotation(long supplierId, long quotationId);
        bool IsCancel(long id);

        #endregion

        #region Helper Function
        #endregion

    }
    public class SupplierPriceQuoteService : BaseService, ISupplierPriceQuoteService 
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");

        #endregion

        #region Propertise & Object Initialization


        private readonly ISupplierPriceQuoteDao _supplierPriceQuoteDao;
        public SupplierPriceQuoteService(ISession session)
        {
            Session = session;
            _supplierPriceQuoteDao=new SupplierPriceQuoteDao{Session = session};
        }

        #endregion

        #region Operational Functions

        public void SaveOrUpdate(SupplierPriceQuote supplierPriceQuote)
        {
            ITransaction transaction = null;
            try
            {
                if (supplierPriceQuote == null)
                    throw new NullObjectException("Price quotation can't be empty.");

                CustomModelValidationCheck.DataAnnotationCheck(supplierPriceQuote, new SupplierPriceQuote());

                if (supplierPriceQuote.Supplier == null)
                    throw new NullObjectException("Price quotation can't be empty.");
                if (supplierPriceQuote.Quotation == null)
                    throw new NullObjectException("Quotation can't be empty.");

                using (transaction = Session.BeginTransaction())
                {
                    _supplierPriceQuoteDao.SaveOrUpdate(supplierPriceQuote);
                    transaction.Commit();
                }
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (MessageException)
            {
                throw;
            }

            catch (EmptyFieldException)
            {
                throw;
            }

            catch (DuplicateEntryException)
            {
                throw;
            }

            catch (InvalidDataException)
            {
                throw;
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public SupplierPriceQuote GetSupplierPriceQuote(long id)
        {
            try
            {
                return _supplierPriceQuoteDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool CheckSupplierPriceQuotation(long supplierId, long quotationId)
        {
            var list = Session.QueryOver<SupplierPriceQuote>()
                .Where(x => x.Supplier.Id == supplierId && x.Quotation.Id == quotationId)
                .List<SupplierPriceQuote>()
                .ToList();
            return list.Count > 0;
        }

        public bool IsCancel(long id)
        {
            ITransaction trans = null;
            try
            {
                var obj = _supplierPriceQuoteDao.LoadById(id);
                if (obj == null)
                    throw new NullObjectException("Price quotation is not valid");

                using (trans = Session.BeginTransaction())
                {
                    obj.PriceQuoteStatus = (int)PriceQouteStatus.Cancelled;
                    _supplierPriceQuoteDao.Update(obj);
                    trans.Commit();
                    return true;
                }
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper function
        #endregion

    }
}
                                    
