﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Base;

namespace UdvashERP.Services.Base
{
    public interface ITestBaseService<TEntityT>
    {
        void Delete(TEntityT entity);
        void DeleteById(long id);

        void DeleteProgramSessionSubject(long programId, long sessionId, List<long> subjectIdList);
    }

    public class TestBaseService<TEntityT> : ITestBaseService<TEntityT> where TEntityT : class
    {
        IBaseDao<TEntityT, long> dao = null;
        private ISession _session;

        public TestBaseService(ISession session)
        {
            dao = new BaseDao<TEntityT, long>() { Session = session };
            _session = session;
        }

        public void Save(TEntityT entityT)
        {
            dao.Save(entityT);
            dao.Session.Flush();
        }

        public void Delete(TEntityT entity)
        {
            dao.Delete(entity);
            dao.Session.Flush();
        }

        public void DeleteById(long id)
        {
            dao.DeleteEntity(id);
            dao.Session.Flush();
        }

        public void DeleteByIdList(List<long> ids)
        {
            foreach (var id in ids)
            {
                dao.DeleteEntity(id);
            }
            dao.Session.Flush();
        }
        
        #region Hr Log
        public void DeleteShiftWeekendLogByShiftWeekendIdList(TeamMember teamMember)
        {
            var list = teamMember.ShiftWeekendHistory.Select(x => x.Id).ToList();
            var logHistoryIdList =

               dao.Session.QueryOver<ShiftWeekendHistoryLog>()
                // .Select(x => x.Id)
                   .Where(x => x.TeamMemberId == teamMember.Id || x.ShiftWeekendHistoryId.IsIn(list.ToArray()) || x.TeamMemberId == null)
                   .List<ShiftWeekendHistoryLog>();
            if (logHistoryIdList.Count > 0)
            {
                foreach (var swhl in logHistoryIdList)
                {
                    string a = "DELETE FROM [HR_ShiftWeekendHistoryLog] WHERE [Id] = " + swhl.Id + ";";
                    dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                }
                dao.Session.Flush();
                //string a = "DELETE FROM [HR_ShiftWeekendHistoryLog] WHERE [Id] IN (" + string.Join(",", logHistoryIdList) + "); ";
                //dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                //dao.Session.Flush();
            }
        }
        //public void DeleteEmploymentHistoryLogByEmploymentHistoryIdList(TeamMember teamMember)
        //{
        //    var list = teamMember.EmploymentHistory.Select(x => x.Id).ToList();
        //    var logHistoryIdList =
        //       dao.Session.QueryOver<EmploymentHistoryLog>()
        //        //.Select(x => x.Id)
        //           .Where(x => x.TeamMemberId == teamMember.Id || x.EmploymentHistoryId.IsIn(list.ToArray()) || x.TeamMemberId == null)
        //           .List<EmploymentHistoryLog>();

        //    if (logHistoryIdList.Count > 0)
        //    {
        //        foreach (var empl in logHistoryIdList)
        //        {
        //            string a = "DELETE FROM [HR_EmploymentHistoryLog] WHERE [Id] = " + empl.Id + ";";
        //            dao.Session.CreateSQLQuery(a).ExecuteUpdate();
        //        }
        //        //string a = "DELETE FROM [HR_EmploymentHistoryLog] WHERE [Id] IN (" + string.Join(",", logHistoryIdList) + "); ";
        //        //dao.Session.CreateSQLQuery(a).ExecuteUpdate();
        //        dao.Session.Flush();
        //    }
        //}
        public void DeleteMentorHistoryLogByMentorHistoryIdList(TeamMember teamMember)
        {
            //dao.Session.Delete(string.Format("from HR_MentorHistoryLog where MentorHistoryId in ({0})", string.Join(",", ids.ToArray())));
            var list = teamMember.MentorHistory.Select(x => x.Id).ToList();
            var logHistoryIdList =
               dao.Session.QueryOver<MentorHistoryLog>()
                //.Select(x => x.Id)
                   .Where(x => x.TeamMemberId == teamMember.Id || x.MentorHistoryId.IsIn(list.ToArray()) || x.TeamMemberId == null)
                   .List<MentorHistoryLog>();
            if (logHistoryIdList.Count > 0)
            {
                foreach (var mhl in logHistoryIdList)
                {
                    string a = "DELETE FROM [HR_MentorHistoryLog] WHERE [Id] = " + mhl.Id + ";";
                    dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                }
                dao.Session.Flush();
                //string a = "DELETE FROM [HR_MentorHistoryLog] WHERE [Id] IN (" + string.Join(",", logHistoryIdList) + "); ";
                //dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                //dao.Session.Flush();
            }
        }
        public void DeleteMemberOfficialDetailsLogByMemberOfficialDetailIdList(TeamMember teamMember)
        {
            var list = teamMember.MemberOfficialDetails.Select(x => x.Id).ToList();
            var logHistoryIdList =
               dao.Session.QueryOver<MemberOfficialDetailLog>()
                // .Select(x => x.Id)
                   .Where(x => x.MemberOfficialDetailId.IsIn(list.ToArray()) || x.MemberOfficialDetailId == null)
                   .List<MemberOfficialDetailLog>();
            if (logHistoryIdList.Count > 0)
            {
                foreach (var modl in logHistoryIdList)
                {
                    string a = "DELETE FROM [HR_MemberOfficialDetailLog] WHERE [Id] = " + modl.Id + ";";
                    dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                }
                dao.Session.Flush();
                //string a = "DELETE FROM [HR_MemberOfficialDetailLog] WHERE [Id] IN (" + string.Join(",", logHistoryIdList) + "); ";
                //dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                //dao.Session.Flush();
            }
        }

        public void DeleteChildrenInfoLogByChildrenInfo(TeamMember teamMember)
        {
            var list = teamMember.ChildrenInfoLog.Select(x => x.Id).ToList();

            var logHistoryIdList =
               dao.Session.QueryOver<ChildrenInfoLog>()
                //.Select(x => x.Id)
                   .Where(x => x.TeamMemberId == teamMember.Id || x.ChildrenInfoId.IsIn(list.ToArray()) || x.TeamMemberId == null)
                   .List<ChildrenInfoLog>();
            if (logHistoryIdList.Count > 0)
            {
                foreach (var cil in logHistoryIdList)
                {
                    string a = "DELETE FROM [HR_ChildrenInfoLog] WHERE [Id] = " + cil.Id + ";";
                    dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                }
                dao.Session.Flush();
                //string a = "DELETE FROM [HR_ChildrenInfoLog] WHERE [Id] IN (" + string.Join(",", logHistoryIdList) + "); ";
                //dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                //dao.Session.Flush();
            }
        }


        //public void DeleteTdsHistoryLogByTdsHisotry(TeamMember teamMember)
        //{
        //    var list = teamMember.TdsHistoryLog.Select(x => x.Id).ToList();

        //    var logHistoryIdList =
        //       dao.Session.QueryOver<TdsHistoryLog>()
        //           //.Select(x => x.Id)
        //           .Where(x => x.TeamMember.Id == teamMember.Id || x.TdsHistory.IsIn(list.ToArray()) || x.TeamMember == null)
        //           .List<TdsHistoryLog>();
        //    if (logHistoryIdList.Count > 0)
        //    {
        //        foreach (var tds in logHistoryIdList)
        //        {
        //            string a = "DELETE FROM [HR_TdsHistoryLog] WHERE [Id] = " + tds.Id + ";";
        //            dao.Session.CreateSQLQuery(a).ExecuteUpdate();
        //        }
        //        dao.Session.Flush();
        //        //string a = "DELETE FROM [HR_ChildrenInfoLog] WHERE [Id] IN (" + string.Join(",", logHistoryIdList) + "); ";
        //        //dao.Session.CreateSQLQuery(a).ExecuteUpdate();
        //        //dao.Session.Flush();
        //    }
        //}

        public void DeleteLeaveApplicationLogByLeaveApplicationIdList(List<LeaveApplication> leaveApplicationList)
        {
            if (leaveApplicationList != null && leaveApplicationList.Count > 0)
            {
                var list = leaveApplicationList.Select(x => x.Id).ToList();
                var logIdList = dao.Session.QueryOver<LeaveApplicationLog>().Where(x => x.LeaveApplicationId.IsIn(list.ToArray())).List<LeaveApplicationLog>();
                if (logIdList.Count > 0)
                {
                    foreach (var item in logIdList)
                    {
                        string a = "DELETE FROM [HR_LeaveApplicationLog] WHERE [Id] = " + item.Id + ";";
                        _session.CreateSQLQuery(a).ExecuteUpdate();
                    }
                    //dao.Session.Flush();
                }
            }
        }

        public void DeleteLeaveApplicationDetailsByLeaveApplicationIdList(List<LeaveApplication> leaveApplicationList)
        {
            if (leaveApplicationList != null && leaveApplicationList.Count > 0)
            {
                var list = leaveApplicationList.Select(x => x.Id).ToArray();

                
                IList<LeaveApplicationDetails> detailsIdList =
                    dao.Session.QueryOver<LeaveApplicationDetails>()
                        .WhereRestrictionOn(x => x.LeaveApplication.Id)
                        .IsIn(list)
                        .List<LeaveApplicationDetails>();
                if (detailsIdList.Count > 0)
                {
                    foreach (var item in detailsIdList)
                    {
                        string a = "DELETE FROM [HR_LeaveApplicationDetails] WHERE [Id] = " + item.Id + ";";
                        _session.CreateSQLQuery(a).ExecuteUpdate();
                    }
                   // dao.Session.Flush();
                }
            }
        }

        #endregion


        public void DeleteProgramBranchSession(long programId, long sessionId, long[] selectedBranchList)
        {
            if (selectedBranchList.Any())
            {
                foreach (var branchId in selectedBranchList)
                {
                    string query = "DELETE FROM [ProgramBranchSession] WHERE [ProgramId] =" + programId + " AND [SessionId] =" + sessionId + " AND [BranchId] = " + branchId + ";";
                    dao.Session.CreateSQLQuery(query).ExecuteUpdate();
                }
                dao.Session.Flush();
            }
        }

        public void DeleteProgramSessionSubject(long programId, long sessionId, List<long> subjectIdList)
        {
            string query = "DELETE FROM [ProgramSessionSubject] WHERE [ProgramId] =" + programId + " AND [SessionId] =" + sessionId + " AND [SubjectId] in( " + string.Join(", ", subjectIdList) + ");";
            dao.Session.CreateSQLQuery(query).ExecuteUpdate();
        }
        public void DeleteProgramStudentExamSession(long programId, long sessionId)
        {
            string query = "DELETE FROM [ProgramStudentExamSession] WHERE [ProgramId] =" + programId + " AND [SessionId] =" + sessionId + ";";
            dao.Session.CreateSQLQuery(query).ExecuteUpdate();

            dao.Session.Flush();

        }

        public void DeleteProgramSessionSubject(long programId, long sessionId)
        {

            string query = "DELETE FROM [ProgramSessionSubject] WHERE [ProgramId] =" + programId + " AND [SessionId] =" + sessionId + ";";
            dao.Session.CreateSQLQuery(query).ExecuteUpdate();

            dao.Session.Flush();

        }
        public void DeleteLecture(IList<Lecture> lectures)
        {
            string query = "DELETE FROM [Lecture] WHERE Id in (" + string.Join(", ", lectures.Select(x => x.Id).ToList()) + ")";
            dao.Session.CreateSQLQuery(query).ExecuteUpdate();
            dao.Session.Flush();
        }

        public void DeleteCampusRoom(List<long> campusRoomIdList)
        {
            string deleteQuery = "Delete From CampusRoom Where Id In (" + string.Join(", ", campusRoomIdList) + ")";
            _session.CreateSQLQuery(deleteQuery).ExecuteUpdate();
        }
        
        public void DeleteSpecificationCriteria(IList<SpecificationCriteria> specificationCriterias)
        {
            var specificationcriteriaIdString = string.Join(", ", specificationCriterias.Select(x => x.Id).ToList());
            var optionidlist = new List<long>();
            if (specificationCriterias != null)
                foreach (var specificationCriteria in specificationCriterias)
                {
                    if (specificationCriteria.SpecificationCriteriaOptions != null)
                        optionidlist.AddRange(
                            specificationCriteria.SpecificationCriteriaOptions.Select(
                                specificationCriteriaOption => specificationCriteriaOption.Id));
                }
            var specificationCriteriaOptionIdString = string.Join(", ", optionidlist);
            string deleteQuery = "";
            if (optionidlist.Count > 0)
            {
                deleteQuery += "Delete From UInv_specificationcriteriaoption Where Id In (" + specificationCriteriaOptionIdString + ");";
            }
            if (specificationCriterias != null && specificationCriterias.Count > 0)
            {
                deleteQuery += "Delete From UInv_specificationcriteriaoption Where SpecificationCriteriaId In (" + specificationcriteriaIdString + ");Delete From UINV_SpecificationCriteriaLog Where SpecificationCriteriaId In (" + specificationcriteriaIdString + ");Delete From UInv_specificationcriteria Where Id In (" + specificationcriteriaIdString + ");";
            }
            _session.CreateSQLQuery(deleteQuery).ExecuteUpdate();
        }
        public void DeleteGoodsReturnAndReceiveDetails(IList<GoodsReturn> goodsReturns)
        {
            var goodsReturnIdString = string.Join(", ", goodsReturns.Select(x => x.Id).ToList());
            var goodsReceive = goodsReturns.Select(x => x.GoodsReceive).ToList();
            string deleteQuery = "";
            if (goodsReturns != null && goodsReturns.Count > 0)
            {
                deleteQuery += "Delete From UInv_goodsreturndetails Where goodsreturnId In (" + goodsReturnIdString + ");";
            }
            if (goodsReceive.Count > 0)
            {
                var goodsReceiveIdString = string.Join(", ", goodsReceive.Select(x => x.Id).ToList());
                var branchIds = string.Join(", ", goodsReceive.Select(x => x.Branch.Id).ToList());
                //var itemIds = string.Join(", ", goodsReceive.Select(x => x.i.Id).ToList());
                var recDet = goodsReceive.Select(x => x.GoodsReceiveDetails).ToList();
                //var itemIds=recDet.Select
                var itemIdList = new List<long>();
                foreach (var rd in recDet)
                {
                    foreach (var r in rd)
                    {
                        itemIdList.Add(r.Item.Id);
                    }
                }
                deleteQuery += "Delete From UINV_GoodsReceiveDetails Where GoodsRecieveId In (" + goodsReceiveIdString + ");DELETE FROM [UINV_CurrentStockSummary] WHERE [BranchId] in( " + branchIds + ") and [ItemId] in(" + string.Join(", ", itemIdList) + ") ;";
            }
            _session.CreateSQLQuery(deleteQuery).ExecuteUpdate();
        }
    }
}
