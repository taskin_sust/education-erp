﻿
$(document).ready(function () {

    $(document).on("focus", ".form-date", function () {
        //var today = new Date();
        //var initialDate = today;
        //var startDate = today.getFullYear().toString() + "-" + (today.getMonth()) + "-" + "26";
        //var endDate = new Date(today.getFullYear(), (today.getMonth() + 1), 0);

        var today = new Date();
        var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();

        var pastMonth = new Date(today.setMonth(today.getMonth() - 1, 1));
        var startDate = pastMonth.getFullYear().toString() + "-" + (pastMonth.getMonth() + 1) + "-" + "01";
        var endDate = initialDate;

        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: initialDate,
            startDate: startDate,
            endDate: endDate,
            startView: 2,
            minView: 2,
            maxView: 4
        }).on('changeDate', function(ev) {
            if ($(".form-date").val() !== "") {
                $(".form-date").parent().find('span').remove();
                $(".form-date").parent().append('<span class="field-validation-valid text-danger" data-valmsg-for="FromTime" data-valmsg-replace="true"></span>');
            }
        });
    });

    $(document).on("input propertychange", "#PinList ", function () {
        var input = $(this),
            text = input.val().replace(/[^0-9 ]/g, "");
        input.val(text);
    });

    $(document).on("input propertychange", ".reason ", function () {
        console.log("ok");
        var input = $(this);
           if (input.val().length > 0) {
               input.removeClass('highlight');
           } else {
               input.addClass('highlight');
           }
    });

    $(document).on("click", "#Update", function (event) {
        console.log("ok");
        var message = "Are you sure you want to Approved All HolidayWork(s)?";
        bootbox.confirm(message, function (result) {
            if (result === true) {
                var isSuccess = true;
                //var date = $("#Date").val();
                var tableId = "holidayWorkApprovalDisplayList";
                var rowCount = $('#' + tableId + ' > tbody > tr').length;
              //  console.log(rowCount);
                if (rowCount > 0) {
                    var holidayWorkApprovalObjArray = [];
                    var holidayWorkApprovalStatusArray = [];
                    $('#' + tableId + ' > tbody > tr').each(function () {
                        var parentTr = $(this);
                        var holidayWorkId = parentTr.find('.holidayId').val();
                        var memberPin = parentTr.find('.memberPin').attr('data-pin');
                        var date = parentTr.find('.memberHolidayWorkDate').attr('data-workdate');
                        var approveType = 0;
                        if (parentTr.find('.halfHolidayApproval').prop('checked'))
                            approveType = 1;
                        else if (parentTr.find('.fullHolidayApproval').prop('checked'))
                            approveType = 2;
                        var reason = parentTr.find('.reason ').val();
                        if (validateApplicationRow(parentTr) === true) {
                            var holidayWorkApprovalObj = new HolidayWorkApprovalViewModel();
                            holidayWorkApprovalObj.HolidayWorkId = holidayWorkId;
                            holidayWorkApprovalObj.Pin = memberPin;
                            holidayWorkApprovalObj.ApprovalType = approveType;
                            holidayWorkApprovalObj.Reason = reason;
                            holidayWorkApprovalObj.HolidayWorkDate = date;
                            holidayWorkApprovalObjArray.push(holidayWorkApprovalObj);
                        } else {
                            holidayWorkApprovalStatusArray.push(1);
                        }
                    });
                    var holidayWorkApprovalViewModelList = holidayWorkApprovalObjArray;
                    if (holidayWorkApprovalStatusArray.length <= 0) {
                        $.ajax({
                            url: $("body").attr("data-project-root") + "Hr/HolidayWork/HolidayWorkApproval",
                            type: "POST",
                            data: { "holidayWorkApprovalViewModelList": holidayWorkApprovalViewModelList, isMentor: true },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data) {
                                $.unblockUI();
                                if (data.IsSuccess) {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "s"
                                    });
                                    $("#displayList").remove();
                                } else {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "e"
                                    });
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function () {
                                $.unblockUI();
                                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                            }
                        });
                    }
                }
            }
        }).css({
            'color': 'red',
            'font-size': '30px',
            'margin-top': (($(window).height() / 4))
        });

    });

});

function validateApplicationRow(parentTr) {
    var success = true;
    var memberPin = parentTr.find('.memberPin').attr('data-pin');
    if (memberPin.length <= 0 || memberPin === '') {
        success = false;
        parentTr.find('.memberPin').addClass('highlight');
    } else {
        parentTr.find('.memberPin').removeClass('highlight');
    }

    var approveType = 0;
    if (parentTr.find('.halfHolidayApproval').prop('checked'))
        approveType = 1;
    else if (parentTr.find('.fullHolidayApproval').prop('checked'))
        approveType = 2;

    var reason = parentTr.find('.reason ').val();
    if (reason.length <= 0 || reason === '') {
        parentTr.find('.reason ').addClass('highlight');
        success = false;
    } else {
        parentTr.find('.reason ').removeClass('highlight');
    }
    //if (approveType > 0) {
    //    if (reason.length <= 0 || reason ==='') {
    //        parentTr.find('.reason ').addClass('highlight');
    //        success = false;
    //    } else {
    //        parentTr.find('.reason ').removeClass('highlight');
    //    }
    //} 
    return success;
}


function HolidayWorkApprovalViewModel() {
    this.HolidayWorkId = 0;
    this.Pin = 0;
    this.HolidayWorkDate = "";
    this.Reason = "";
    this.ApprovalType = 0;
    this.HolidayWorkApprovalViewModel = function () { };
}