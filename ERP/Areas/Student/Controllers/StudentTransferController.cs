﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Web.Helpers;
using System.Web.Mvc;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using UdvashERP.App_code;
using UdvashERP.Areas.Student.Models;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.App_Start;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using Array = System.Array;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class StudentTransferController : Controller
    {
        #region Objects/Propertise/Dao/Services & Initialize

        private ILog logger = LogManager.GetLogger("StudentArea");
        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly IBatchService _batchService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IUserService _userService;
        private IList<ProgramBranchSession> _programBranchSessionList;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ICommonHelper _commonHelper;
        private readonly IStudentTransferService _studentTransferService;
        private readonly IOrganizationService _organizationService;
        private readonly IStudentImagesMediaService _studentImagesMediaService;
        private readonly IServiceBlockService _serviceBlockService;
        private readonly IStudentPaymentService _studentPaymentService;

        private List<UserMenu> _userMenu;
        public StudentTransferController()
        {

            var session = NHibernateSessionFactory.OpenSession();
            _sessionService = new SessionService(session);
            _programService = new ProgramService(session);
            _branchService = new BranchService(session);
            _campusService = new CampusService(session);
            _batchService = new BatchService(session);
            _studentProgramService = new StudentProgramService(session);
            _programBranchSessionList = new List<ProgramBranchSession>();
            _programBranchSessionService = new ProgramBranchSessionService(session);
            _userService = new UserService(session);
            _studentTransferService = new StudentTransferService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _studentImagesMediaService = new StudentImagesMediaService();
            _serviceBlockService = new ServiceBlockService(session);
            _studentPaymentService = new StudentPaymentService(session);
        }
        #endregion

        #region Student Transfer

        #region Get The Student Details By Prn
        public ActionResult GetDataByPrn(string prnumber)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    StudentProgram studentProgram = _studentProgramService.GetStudentProgram(prnumber);
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                    List<long> programIdList = AuthHelper.LoadProgramIdList(_userMenu);
                    
                    if (studentProgram != null)
                    {
                        var userBranchId = studentProgram.Batch.Branch.Id;
                        var userProgramId = studentProgram.Program.Id;
                        if (branchIdList != null)
                        {
                            bool isBranchSelect = branchIdList.Contains(userBranchId);
                            if (!isBranchSelect)
                                return Json(new Response(false, "You are not authorized to this branch"));
                        }
                        if (programIdList != null)
                        {
                            bool isProgramSelect = programIdList.Contains(userProgramId);
                            if (!isProgramSelect)
                                return Json(new Response(false, "You are not authorized to this program"));
                        }
                        var checkBlocked = _serviceBlockService.IsBlocked(studentProgram.Program.Id, studentProgram.Batch.Session.Id, (int)BlockService.StudentTransfer, studentProgram);
                        ViewBag.CheckBlocked = checkBlocked;

                        var payAbleAmount = _studentPaymentService.TotalpayAbleAmount(studentProgram.Id);
                        ViewBag.StudentPayableAmount = payAbleAmount;
                        var str = _serviceBlockService.WhichBlocked(studentProgram.Program.Id,
                        studentProgram.Batch.Session.Id, (int)BlockService.StudentTransfer, studentProgram);
                        ViewBag.BlockedMessage = str + " blocked for this student.So this student not enable to transfer";
                        ViewBag.CheckBlocked = checkBlocked;
                            string[] prnNo = new string[1] { prnumber };

                            ViewBag.StudentProgram = _studentProgramService.LoadStudentProgram(prnNo);
                            ViewBag.CheckOldBranch = studentProgram.Batch.Branch.Id;
                            ViewBag.CkeckOldCampus = studentProgram.Batch.Campus.Id;
                            
                            var branchList = _branchService.LoadBranch(null, _commonHelper.ConvertIdToList(studentProgram.Program.Id), _commonHelper.ConvertIdToList(studentProgram.Batch.Session.Id));
                        
                            var oldBranchList = new SelectList(branchList, "Id", "Name", studentProgram.Batch.Branch.Id);
                            ViewBag.OldBranch = oldBranchList;

                            var newBranchList = new SelectList(branchList, "Id", "name");
                            ViewBag.NewBranchList = newBranchList;

                            var campusList = _campusService.LoadCampus();

                            var oldCampuslist = new SelectList(campusList, "Id", "name", studentProgram.Batch.Campus.Id);
                            ViewBag.OldCampusList = oldCampuslist;

                            var batchList = _batchService.LoadBatch();

                            var oldBatchList = new SelectList(batchList, "Id", "Name", studentProgram.Batch.Id);
                            ViewBag.OldbatchList = oldBatchList;

                            var oldBatchDays = new SelectList(batchList, "Id", "Days", studentProgram.Batch.Id);
                            ViewBag.OldBatchDays = oldBatchDays;

                            var oldBatchTimes = new SelectList(batchList, "Id", "FormatedBatchTime",
                                studentProgram.Batch.Id);
                            ViewBag.OldBatchTimes = oldBatchTimes;

                            IList<Campus> _listCampus = new List<Campus>();
                            ViewBag.NewcampusList = new SelectList(_listCampus, "Value", "Text");

                            IList<Batch> _listBatch = new List<Batch>();
                            ViewBag.NewBatchList = new SelectList(_listBatch, "Value", "Text");


                            ViewBag.PreviousTransferInfo = _studentTransferService.GetPreviousTransferInfo(studentProgram.Id);
                        
                        
                        return PartialView("Partial/_StudentTransferData");
                    }
                    else
                    {
                        return Json(new Response(false, "Please correct Program roll"));
                    }

                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region Save Operation
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(StudentTranferViewModel studentTransfer)
        {
            StudentProgram studentProgram = _studentProgramService.GetStudentProgram(studentTransfer.PrnNo);
            if (studentProgram != null)
            {
                var whichBlocked = _serviceBlockService.WhichBlocked(studentProgram.Program.Id, studentProgram.Batch.Session.Id, (int)BlockService.StudentTransfer, studentProgram);
                var checkBlocked = _serviceBlockService.IsBlocked(studentProgram.Program.Id,
                    studentProgram.Batch.Session.Id, (int)BlockService.StudentTransfer, studentProgram);
                if (!checkBlocked)
                {
                    try
                    {
                        if (studentTransfer.batchName != 0 && studentTransfer.PrnNo != null)
                        {
                            Branch branch = _branchService.GetBranch(studentTransfer.BranchId);
                            Campus campus = _campusService.GetCampus(studentTransfer.CampusId);
                            if (campus != studentProgram.Batch.Campus)
                            {
                                StudentTransfer model = new StudentTransfer();
                                Batch batch = _batchService.GetBatch(studentTransfer.batchName);
                                var remainingCapacity = batch.Capacity - batch.StudentPrograms.Where(x => x.Batch == batch && x.Status == StudentProgram.EntityStatus.Active && x.Student.Status == BusinessModel.Entity.Students.Student.EntityStatus.Active).Select(y => y.Student).Count();
                                if (remainingCapacity > 0)
                                {
                                    model.FromBatch = studentProgram.Batch;
                                    model.ToBatch = batch;
                                    model.StudentProgram = studentProgram;
                                    model.TransferDate = DateTime.Now;
                                    model.DueAmount = studentProgram.DueAmount;
                                    if (studentTransfer.Payable != null)
                                        model.Payable = studentTransfer.Payable;
                                    else model.Receiveable = studentTransfer.Receiveable;
                                    model.DueClassExam = studentTransfer.DueClassExam;
                                    model.IsSettled = false;

                                    bool studentTransferSave = _studentTransferService.Save(model);
                                    if (studentTransferSave)
                                    {
                                        var sts = new StudentTransferSlip();
                                        //sts.StudentProgram = studentProgram;
                                        sts.PrnNo = studentTransfer.PrnNo;
                                        sts.FromBranch = model.FromBatch.Branch.Name;
                                        sts.FromCampus = model.FromBatch.Campus.Name;
                                        sts.FromBatchDays = model.FromBatch.Days;
                                        sts.FromBatchTime = model.FromBatch.FormatedBatchTime;
                                        sts.FromBatchName = model.FromBatch.Name;


                                        sts.Branch = branch.Name;
                                        sts.Campus = model.ToBatch.Campus.Name;
                                        sts.BatchDays = batch.Days;
                                        sts.BatchTime = batch.FormatedBatchTime;
                                        sts.BatchName = batch.Name;
                                        sts.StudentName = studentProgram.Student.NickName;
                                        sts.MobileNo = studentProgram.Student.Mobile;
                                        sts.ProgramName = studentProgram.Program.Name;
                                        sts.SessionName = model.ToBatch.Session.Name;

                                        sts.RollNo = studentProgram.Student.RegistrationNo;

                                        sts.CreateDate = String.Format("{0:dd/MM/yyyy}", model.TransferDate);

                                        TempData["StudentCourses"] = studentProgram.StudentCourseDetails;
                                        sts.CampusAddress = batch.Campus.Location;
                                        sts.PhoneNo = batch.Campus.ContactNumber;
                                        AspNetUser userProfile =
                                            _userService.LoadbyAspNetUserId(
                                                Convert.ToInt64(
                                                    System.Web.HttpContext.Current.User.Identity.GetUserId()));
                                        sts.AuthorityFullName = userProfile.FullName;

                                        bool flagForSms = SendSmsApi.GenarateInstantSmsAndNumber(5, model.StudentProgram.Program.Organization.Id,
                                            model.StudentProgram.Program.Id, model.StudentProgram.Batch.Session.Id,
                                            model.StudentProgram.Student.Id, model.StudentProgram.Id,
                                            model.FromBatch.Id);

                                        return RedirectToAction("TransferSlip", sts);
                                    }
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = "No available capacity for this batch.Please select another batch";
                                }
                            }
                            else
                            {
                                ViewBag.ErrorMessage = "Please select another campus";
                            }
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "Please select batch then transfer";
                        }
                    }
                    catch (NullObjectException ex)
                    {
                        ViewBag.ErrorMessage = ex;
                    }
                    catch (DuplicateEntryException ex)
                    {
                        ViewBag.ErrorMessage = ex;
                    }
                    catch (Exception e)
                    {
                        logger.Error(e);
                        ViewBag.ErrorMessage = e;
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = whichBlocked + " blocked for this service";
                }
                
            }
            else
            {
                ViewBag.ErrorMessage = "Invalid Program Roll";
            }



            return View();
        }

        #endregion
        #endregion

        #region Transfer Slip
        public ActionResult TransferSlip(StudentTransferSlip stsDto)
        {

            if (stsDto != null)
            {
                StudentProgram studentProgram = _studentProgramService.GetStudentProgram(stsDto.PrnNo);
                StudentTransferSlip studentSlip = new StudentTransferSlip();
                studentSlip = stsDto;

                StudentMediaImage mediaStudentImages = new StudentMediaImage();
                if (studentProgram != null && studentProgram.StudentImage.Count > 0)
                {
                    mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(
                     studentProgram.StudentImage[0].Id);
                    if (mediaStudentImages != null)
                    {
                        studentProgram.MediaStudentImage = mediaStudentImages;
                    }
                    else
                    {
                        studentProgram.MediaStudentImage = new StudentMediaImage();
                    }

                }
                studentSlip.StudentProgram = studentProgram;
            }


            //StudentTranferViewModel model = (StudentTranferViewModel)TempData["sts"];
            return View("TransferSlip", stsDto);
        }

        #region Individual Transfer Slip
        public ActionResult Details(long id)
        {
            var obj = _studentTransferService.LoadById(id);
            StudentTransferSlip slipObj = new StudentTransferSlip();

            if (obj != null)
            {
                StudentProgram studentProgram = obj.StudentProgram;
                BusinessModel.Entity.Students.Student student = studentProgram.Student;
                Batch toBatch = obj.ToBatch;
                Batch fromBatch = obj.FromBatch;


                TempData["StudentCourses"] = studentProgram.StudentCourseDetails;
                #region old code
                //var courses = new List<Object>();
                //slipObj.Courses = "";
                //ViewBag.StudentCourses = studentProgram.StudentCourseDetails;
                //if (studentProgram.StudentCourseDetails!=null)
                //{
                //    foreach (var c in studentProgram.StudentCourseDetails)
                //    {
                //        slipObj.Courses = c.CourseSubject.Course.Name + ",";
                //    }
                //}

                //if (slipObj.Courses != "")
                //    slipObj.Courses = slipObj.Courses.Remove(slipObj.Courses.Length - 1);
                #endregion

                StudentMediaImage mediaStudentImages = new StudentMediaImage();
                if (studentProgram.StudentImage.Count > 0)
                {
                    mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(
                     studentProgram.StudentImage[0].Id);
                    studentProgram.MediaStudentImage = mediaStudentImages != null
                        ? mediaStudentImages
                        : new StudentMediaImage();
                }
                // slipObj.StudentProgram = studentProgram;

                slipObj.StudentProgram = studentProgram;
                slipObj.ProgramName = studentProgram.Program.Name;
                slipObj.SessionName = toBatch.Session.Name;
                slipObj.PrnNo = studentProgram.PrnNo;
                slipObj.StudentName = student.NickName;
                slipObj.MobileNo = student.Mobile;
                slipObj.RollNo = student.RegistrationNo;

                slipObj.CreateDate = String.Format("{0:dd/MM/yyyy}", obj.CreationDate);

                slipObj.BatchDays = toBatch.Days;
                slipObj.BatchName = toBatch.Name;
                slipObj.BatchTime = toBatch.FormatedBatchTime;
                slipObj.Branch = toBatch.Branch.Name;
                slipObj.Campus = toBatch.Campus.Name;
                slipObj.CampusAddress = toBatch.Campus.Location;
                slipObj.PhoneNo = toBatch.Campus.ContactNumber;
                AspNetUser userProfile = _userService.LoadbyAspNetUserId(obj.CreateBy);
                slipObj.AuthorityFullName = userProfile.FullName;

                slipObj.FromBatchDays = fromBatch.Days;
                slipObj.FromBatchName = fromBatch.Name;
                slipObj.FromBatchTime = fromBatch.FormatedBatchTime;
                slipObj.FromBranch = fromBatch.Branch.Name;
                slipObj.FromCampus = fromBatch.Campus.Name;

            }
            return View("TransferSlip", slipObj);
        }
        #endregion
        
        #endregion

        #region Student Transfer Report
        [HttpGet]
        public ActionResult TransferReport()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            var programList = new List<Program> {new Program() {Id = 0, Name = "All"}};
            var sessionList = new List<Session> { new Session() { Id = 0, Name = "All" } };
            ViewBag.Program = new SelectList(programList, "Id", "Name");
            ViewBag.Session = new SelectList(sessionList, "Id", "Name");
            ViewBag.FromBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.FromCampus = new SelectList(new List<Campus>(), "Id", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult TransferReport(StudentInfoTransfer studentTransferDto)
        {
            ModelState.Remove("SelectedToBranch");
            ModelState.Remove("SelectedToCampus");
            if (ModelState.IsValid)
            {
                return PartialView("StudentTransferReportPartialView", studentTransferDto);
            }
            return View();
        }
        public JsonResult GetProgramByOrganization(List<long> organizationId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Program> programList = _programService.LoadAuthorizedProgram(_userMenu, organizationId);
                Program obj = new Program { Id = 0, Name = "-Select Program-" };
                programList.Insert(0, obj);

                var programSelectList = new SelectList(programList, "Id", "Name");
                return Json(programSelectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public ActionResult GetCampusByBranch(int? branchId)
        {
            if (Request.IsAjaxRequest())
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                try
                {
                    if (branchId != null)
                    {
                        SelectList campusSelectList = new SelectList(_campusService.LoadAuthorizeCampus(userMenu, null, null, _commonHelper.ConvertIdToList(Convert.ToInt64(branchId))), "Id", "Name");
                        List<SelectListItem> _list = campusSelectList.ToList();
                        _list.Insert(0, new SelectListItem() { Value = " ", Text = "Select Campus" });
                        ViewBag.CampusListCount = campusSelectList.Count();
                        ViewBag.CampusList = new SelectList(_list, "Value", "Text");
                    }
                    else
                    {
                        List<SelectListItem> _list = new List<SelectListItem>();
                        _list.Insert(0, new SelectListItem() { Value = " ", Text = "Select Campus" });
                        ViewBag.CampusList = new SelectList(_list, "Value", "Text");

                    }
                    return PartialView("Partial/_CampusList");
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        logger.Error("Campus Can't be loaded  due to " + ex.InnerException);
                    }
                    else
                    {
                        logger.Error("Campus Can't be loaded  due to " + ex.Message);
                    }
                    Console.WriteLine("{0}", ex.Message);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return HttpNotFound(); }
        }
        
        [HttpGet]
        public ActionResult ExportStudentTransferReport(long organizationId,long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, string prnNo, string name, string mobile,DateTime dateFrom,DateTime dateTo)
        {
            int start = 0;
            int length = 0;
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var branchIds = _branchService.LoadAuthorizedBranch(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId));
                if (!selectedFromBranch.Contains(SelectionType.SelelectAll))
                {
                    branchIds = branchIds.Where(b => b.Id.In(selectedFromBranch)).ToList();
                }
                var fromBranchIds = branchIds.Select(x => x.Id).ToArray();
                var campusIds = _campusService.LoadAuthorizeCampus(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), branchIds.Select(x => x.Id).ToList(), _commonHelper.ConvertIdToList(sessionId)).Select(x => x.Id).ToList();
                if (!selectedFromCampus.Contains(SelectionType.SelelectAll))
                {
                    campusIds = selectedFromCampus.ToList();
                }
                decimal currentPagePayable = 0;
                decimal currentPageReceivable = 0;
                decimal overallPayable = 0;
                decimal overallReceivable = 0;
                _studentTransferService.GetOvetAllPayableReceivable(programId, sessionId, fromBranchIds, campusIds.ToArray(),prnNo,name,mobile, dateFrom, dateTo, out overallPayable, out overallReceivable);
                IList<StudentTransferReportDto> studentTransferList = _studentTransferService.LoadStudentTransferReport(start, length, programId, sessionId, fromBranchIds, campusIds.ToArray(), prnNo, name, mobile, dateFrom, dateTo).ToList();
                //int recordsTotal = _studentTransferService.LoadStudentTransferReportCount(programId, sessionId, fromBranchIds, campusIds.ToArray(),prnNo,name,mobile, dateFrom, dateTo);
                //long recordsFiltered = recordsTotal;
                
                List<string> headerList = new List<string>();
                headerList.Add(ErpInfo.OrganizationNameFull);
                headerList.Add("Transfer List");

                List<string> footerList = new List<string>();
                footerList.Add("");

                List<string> columnList = new List<string>();
                columnList.Add("Program Roll");
                columnList.Add("Student's Name");
                columnList.Add("Previous Branch");
                columnList.Add("Previous Campus");
                columnList.Add("Current Branch");
                columnList.Add("Current Campus");
                columnList.Add("Receiveable");
                columnList.Add("Payable");
                columnList.Add("Due Amount");
                columnList.Add("Transfer Date");
                columnList.Add("Is Settled");
                var studentExcelList = new List<List<object>>();
                foreach (var xSes in studentTransferList)
                {
                    var xlsRow = new List<object>();
                    if (xSes.StudentRoll != null)
                        xlsRow.Add(xSes.StudentRoll);
                    xlsRow.Add(xSes.StudentName ?? "");
                    xlsRow.Add(xSes.PreviousBranch ?? "");
                    xlsRow.Add(xSes.PreviousCampus ?? "");
                    xlsRow.Add(xSes.CurrentBranch ?? "");
                    xlsRow.Add(xSes.CurrentCampus ?? "");
                    xlsRow.Add(xSes.Receivable ?? "");
                    xlsRow.Add(xSes.Payable ?? "");
                    xlsRow.Add(xSes.DueAmount ?? "");
                    var date = "";
                    if (!string.IsNullOrEmpty(xSes.TransferDate))
                        date = Convert.ToDateTime(xSes.TransferDate).ToString("dd-MM-yyyy");
                    xlsRow.Add(date);
                    xlsRow.Add(xSes.IsSettled);
                    studentExcelList.Add(xlsRow);
                }
                var overAllSummaryString = new List<object> { "", "", "", "", "",  "Overall summary", overallReceivable.ToString(), overallPayable.ToString(), "", "", "" };
                //studentExcelList.Add(currentPageSummaryString);
                studentExcelList.Add(overAllSummaryString);
                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "student-TranferList-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                //_logger.Error(ex.Message);
                return View("AutoClose");
            }
        }
        
        #region Count The Transfer Number
        
        public JsonResult TransferReportCount(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus )
        {
            if (programId > 0 && sessionId > 0)
            {
                try
                {
                    var studentTransferList = _studentTransferService.LoadStudentTransferByCriteriaCount(programId, sessionId,
                        selectedFromBranch, selectedFromCampus ,null,null);
                    var count = studentTransferList.Count;
                    ViewBag.Count = count;
                    return Json(count, JsonRequestBehavior.AllowGet);

                }
                catch (Exception)
                {
                    return Json(new Response(false, "Please select all field then calculate"));
                }
            }
            else
            {
                return Json(new Response(false, "Please select program and session"));
            }
        }
        #endregion
        
        #region Render Data Table

        [HttpPost]
        public JsonResult StudentTransferReportAjaxRequest(int draw, int start, int length, long organizationId, long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo)
        {
            NameValueCollection nvc = Request.Form;

            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var branchIds = _branchService.LoadAuthorizedBranch(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId));
                if (!selectedFromBranch.Contains(SelectionType.SelelectAll))
                {
                    branchIds = branchIds.Where(b => b.Id.In(selectedFromBranch)).ToList();
                }
                var fromBranchIds = branchIds.Select(x => x.Id).ToArray();
                var campusIds = _campusService.LoadAuthorizeCampus(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), branchIds.Select(x => x.Id).ToList(), _commonHelper.ConvertIdToList(sessionId)).Select(x=>x.Id).ToList();
                if (!selectedFromCampus.Contains(SelectionType.SelelectAll))
                {
                    campusIds = selectedFromCampus.ToList();
                }
                decimal currentPagePayable = 0;
                decimal currentPageReceivable = 0;
                decimal overallPayable = 0;
                decimal overallReceivable = 0;
                _studentTransferService.GetOvetAllPayableReceivable(programId, sessionId, fromBranchIds, campusIds.ToArray(),prnNo,name,mobile, dateFrom, dateTo,out overallPayable,out overallReceivable);
                //decimal overallPayable = _studentTransferService.GetTotalPayable(programId, sessionId,
                //            fromBranchIds, selectedFromCampus, prnNo, name, mobile, dateFrom, dateTo); 
                //decimal overallReceivable = _studentTransferService.GetTotalReceivable(programId, sessionId,
                //            fromBranchIds, selectedFromCampus, prnNo, name, mobile, dateFrom, dateTo);
            
                #region OrderBy and Direction

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "Code";
                            break;
                        case "2":
                            orderBy = "Status";
                            break;
                        case "3":
                            orderBy = "Rank";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                IList<StudentTransferReportDto> studentTransferList = _studentTransferService.LoadStudentTransferReport(start, length, programId, sessionId, fromBranchIds, campusIds.ToArray(), prnNo, name, mobile, dateFrom, dateTo).ToList();
                //var totalTransferList = _studentTransferService.LoadStudentTransferByCriteriaCount(programId, sessionId,
                //        fromBranchIds, selectedFromCampus, null, null, prnNo, name, mobile, dateFrom, dateTo);
                int recordsTotal = _studentTransferService.LoadStudentTransferReportCount(programId, sessionId, fromBranchIds, campusIds.ToArray(),prnNo,name,mobile,dateFrom, dateTo);
                long recordsFiltered = recordsTotal;
                long i = start + 1;
                foreach (var stl in studentTransferList)
                {
                    var str = new List<string>();
                    str.Add(i.ToString(CultureInfo.InvariantCulture));
                    str.Add(stl.StudentRoll);
                    str.Add(stl.StudentName);
                    str.Add(stl.PreviousBranch);
                    str.Add(stl.PreviousCampus);
                    str.Add(stl.CurrentBranch);
                    str.Add(stl.CurrentCampus);
                    str.Add(stl.Receivable);
                    str.Add(stl.Payable);
                    currentPagePayable += Convert.ToDecimal(stl.Payable);
                    currentPageReceivable += Convert.ToDecimal(stl.Receivable);
                    str.Add(stl.DueAmount);
                    var date = "";
                    if (!string.IsNullOrEmpty(stl.TransferDate))
                        date = Convert.ToDateTime(stl.TransferDate).ToString("dd-MM-yyyy");
                    str.Add(date);
                    str.Add(stl.IsSettled.ToString());
                    //str.Add(stl.re);
                    str.Add("<a href='" + Url.Action("Details", "StudentTransfer") + "?Id=" + stl.TransferId + "' data-id='" +
                    stl.TransferId + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp; ");
                    getData.Add(str);
                    i = i + 1;
                }
                var currentPageSummaryString = new List<string> {  "", "", "", "","","", "<strong>Current page summary</strong>", currentPageReceivable.ToString(), currentPagePayable.ToString(), "", "", "", "" };
                var overAllSummaryString = new List<string> {  "", "", "", "","","", "<strong>Overall summary</strong>", overallReceivable.ToString(), overallPayable.ToString(), "", "", "", "" };
                getData.Add(currentPageSummaryString);
                getData.Add(overAllSummaryString);
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData
                    //isSuccess = true
                });
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "Problem Occurred";
                throw ex;
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }

        
        #endregion
        
        #endregion
        
        #region Student Transfer List
        [HttpGet]
        public ActionResult TransferList()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.FromBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.ToBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.FromCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.ToCampus = new SelectList(new List<Campus>(), "Id", "Name");
            return View();
        }
       
        [HttpPost]
        public ActionResult TransferList(StudentInfoTransfer studentTransferDto)
        {
            ModelState.Remove("DateFrom");
            ModelState.Remove("DateTo");
            if (ModelState.IsValid)
            {
                return View("StudentTransferListReport", studentTransferDto);
            }
            else
            {
                return View();
            }

        }

        [HttpGet]
        public ActionResult ExportStudentTransferList(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string prnNo, string name, string mobile)
        {
            int start = 0;
            int length = 0;
            try
            {
                IList<StudentTransfer> studentTransferList = new List<StudentTransfer>();

                studentTransferList = _studentTransferService.LoadStudentTransferByCriteriaCount(programId, sessionId, selectedFromBranch, selectedFromCampus, selectedToBranch, selectedToCampus, prnNo, name, mobile);

                List<string> headerList = new List<string>();
                headerList.Add(ErpInfo.OrganizationNameFull);
                headerList.Add("Transfer List");

                List<string> footerList = new List<string>();
                footerList.Add("");

                List<string> columnList = new List<string>();
                columnList.Add("Program Roll");
                columnList.Add("Student's Name");
                columnList.Add("Mobile");
                columnList.Add("Previous Branch");
                columnList.Add("Previous Campus");
                //columnList.Add("Previous Campus");
                columnList.Add("Current Branch");
                //columnList.Add("Current Campus");
                //columnList.Add("Current Batch");
                columnList.Add("Receiveable");
                columnList.Add("Payable");
                columnList.Add("Due Amount");
                columnList.Add("Transfer Date");
                columnList.Add("Is Settled");

                var studentExcelList = new List<List<object>>();

                foreach (var xSes in studentTransferList)
                {
                    var xlsRow = new List<object>();
                    if (xSes.StudentProgram.PrnNo != null)
                        xlsRow.Add(xSes.StudentProgram.PrnNo);
                    xlsRow.Add(xSes.StudentProgram.Student.NickName ?? "");
                    xlsRow.Add(xSes.StudentProgram.Student.Mobile ?? "");
                    xlsRow.Add(xSes.FromBatch.Branch.Name ?? "");
                    xlsRow.Add(xSes.FromBatch.Campus.Name ?? "");
                    xlsRow.Add(xSes.ToBatch.Branch.Name ?? "");
                    //xlsRow.Add(xSes.ToBatch.Campus.Name ?? "");
                    //xlsRow.Add(xSes.ToBatch.Name ?? "");
                    xlsRow.Add(xSes.Receiveable.ToString() ?? "");
                    xlsRow.Add(xSes.Payable.ToString() ?? "");
                    xlsRow.Add(xSes.DueAmount.ToString() ?? "");
                    xlsRow.Add(xSes.TransferDate.ToString("yyyy-MM-dd") ?? "");
                    xlsRow.Add(xSes.IsSettled.ToString() ?? "");

                    studentExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "student-TranferList-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                //_logger.Error(ex.Message);
                return View("AutoClose");
            }
        }

        #region Count The Transfer Number

        public JsonResult TransferCount(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus)
        {
            if (programId > 0 && sessionId > 0)
            {
                try
                {
                    var studentTransferList = _studentTransferService.LoadStudentTransferByCriteriaCount(programId, sessionId,
                        selectedFromBranch, selectedFromCampus, selectedToBranch, selectedToCampus);
                    var count = studentTransferList.Count;
                    ViewBag.Count = count;
                    return Json(count, JsonRequestBehavior.AllowGet);

                }
                catch (Exception)
                {
                    return Json(new Response(false, "Please select all field then calculate"));
                }
            }
            else
            {
                return Json(new Response(false, "Please select program and session"));
            }
        }
        #endregion

        #region Render Data Table


        [HttpPost]
        public JsonResult StudentTransferListAjaxRequest(int draw, int start, int length, long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string prnNo, string name, string mobile)
        {
            NameValueCollection nvc = Request.Form;

            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "Code";
                            break;
                        case "2":
                            orderBy = "Status";
                            break;
                        case "3":
                            orderBy = "Rank";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                IList<StudentTransfer> studentTransferList = new List<StudentTransfer>();


                studentTransferList = _studentTransferService.LoadStudentTransferByCriteria(start, length, programId, sessionId, selectedFromBranch, selectedFromCampus,
                    selectedToBranch, selectedToCampus, prnNo, name, mobile).OrderBy(x => x.StudentProgram.PrnNo).ToList();

                var totalTransferList = _studentTransferService.LoadStudentTransferByCriteriaCount(programId, sessionId,
                        selectedFromBranch, selectedFromCampus, selectedToBranch, selectedToCampus, prnNo, name, mobile);
                int recordsTotal = totalTransferList.Count;
                long recordsFiltered = recordsTotal;


                long i = start + 1;
                foreach (var stl in studentTransferList)
                {
                    var str = new List<string>();
                    str.Add(i.ToString());
                    str.Add(stl.StudentProgram.PrnNo);
                    str.Add(stl.StudentProgram.Student.NickName);

                    str.Add(stl.StudentProgram.Student.Mobile);
                    str.Add(stl.FromBatch.Branch.Name);
                    str.Add(stl.ToBatch.Branch.Name);
                    str.Add(stl.ToBatch.Name);
                    str.Add(stl.Receiveable.ToString());
                    str.Add(stl.Payable.ToString());
                    str.Add(stl.DueAmount.ToString());

                    if (stl.TransferDate != null)
                        str.Add(stl.TransferDate.ToString("dd-MM-yyyy"));
                    else
                        str.Add("");
                    str.Add(stl.IsSettled.ToString());

                    str.Add("<a href='" + Url.Action("Details", "StudentTransfer") + "?Id=" + stl.Id + "' data-id='" +
                    stl.Id + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp; ");
                    getData.Add(str);
                    i = i + 1;
                }


                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData
                    //isSuccess = true
                });




            }
            catch (Exception)
            {

                ViewBag.ErrorMessage = "Problem Occurred";
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }



        #endregion

        #endregion
 
        #region Ajax Function

        public JsonResult LoadBatch(List<long> organizationIds, List<long> programIds, List<long> branchIds, List<long> sessionIds, List<long> campusIds, string[] batchDays, string[] batchTimes, List<int> selectedVersions = null, List<int> selectedGenders = null, bool isAuthorized = true)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = isAuthorized ? _batchService.LoadAuthorizeBatch(userMenu, organizationIds, programIds, branchIds, sessionIds, campusIds, batchDays, batchTimes, selectedVersions, selectedGenders).ToList() : _batchService.LoadBatch(organizationIds, programIds, sessionIds, branchIds, campusIds, null, batchDays, batchTimes, selectedGenders).ToList();

                batchList.ForEach(x =>
                {
                    x.RemainingCapacity = x.Capacity - (x.StudentPrograms.Where(y => y.Batch == x && y.Status == StudentProgram.EntityStatus.Active && y.Student.Status == BusinessModel.Entity.Students.Student.EntityStatus.Active).Select(y => y.Student)).Count();
                    x.Name += " (" + x.RemainingCapacity + ")";
                }
                );
                List<SelectListItem> batchSelecteList = (new SelectList(batchList.ToList(), "Id", "Name")).ToList();

                return Json(new { returnBatch = batchSelecteList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = "Program Occurred During Load Batch";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult RemainCapacityOfBatch(long batchId)
        {
            try
            {
                Batch batch = _batchService.GetBatch(batchId);
                var remainingCapacity = batch.Capacity - batch.StudentPrograms.Where(x => x.Batch == batch && x.Status == StudentProgram.EntityStatus.Active && x.Student.Status == BusinessModel.Entity.Students.Student.EntityStatus.Active).Select(y => y.Student).Count();
                return Json(new {remainingCapacity, IsSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public JsonResult GetBatchInfoByBranchCampus(long programId, long sessionId, long[] branchId, long[] campusId, int gender = 0, string[] batchDays = null, string[] batchTimes = null)
        {
            try
            {
                if (campusId != null)
                {
                    IList<Batch> batchList = _batchService.LoadBatch(null, _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId), branchId.ToList(), campusId.ToList(), null, batchDays, batchTimes, (gender != 0) ? _commonHelper.ConvertIdToList(gender) : null);

                    SelectList selectBatchDaysList = new SelectList(batchList.DistinctBy(x => x.Days), "Days", "Days");
                    List<SelectListItem> batchDay = selectBatchDaysList.ToList();

                    SelectList selectBatchTimeList = new SelectList(batchList.DistinctBy(x => x.BatchTime), "BatchTime", "FormatedBatchTime");
                    List<SelectListItem> batchTime = selectBatchTimeList.ToList();

                    SelectList selectBatchNameist = new SelectList(batchList.DistinctBy(x => x.Id), "Id", "Name");
                    List<SelectListItem> batchName = selectBatchNameist.ToList();

                    return
                        Json(
                            new
                            {
                                batchDays = batchDay,
                                batchTime = batchTime,
                                batchName = batchName,
                                JsonRequestBehavior.AllowGet
                            });
                }
                else
                {
                    return
                        Json(
                            new
                            {
                                batchDays = "",
                                batchTime = "",
                                batchName = "",
                                JsonRequestBehavior.AllowGet
                            });
                }

            }
            catch (Exception)
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        
        #endregion

        #region Transfer Settlement

        public ActionResult TransferSettlement()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name"); ;
            ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.FromBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.ToBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.FromCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.ToCampus = new SelectList(new List<Campus>(), "Id", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult TransferSettlement(StudentTransferSettlementViewModel studentTransferSettlement)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["organizationName"] = _organizationService.LoadById(studentTransferSettlement.OrganizationId).Name;
                    TempData["programSession"] = _programService.GetProgram(studentTransferSettlement.ProgramId).Name + " " +
                                                 _sessionService.LoadById(studentTransferSettlement.SessionId).Name;
                    TempData["fromBranch"] = _branchService.GetBranch(studentTransferSettlement.BranchFormId).Name;
                    TempData["toBranch"] = _branchService.GetBranch(studentTransferSettlement.BranchToId).Name;
                    TempData["fromCampus"] = _campusService.GetCampus(studentTransferSettlement.CampusFromId).Name;
                    TempData["toCampus"] = _campusService.GetCampus(studentTransferSettlement.CampusToId).Name;
                    return View("StudentTransferSettlementReport", studentTransferSettlement);
                }
                else
                {
                    return View();
                }

                
            }
            catch (Exception ex)
            {
                return View();
            }
            
        }
        [HttpPost]
        public JsonResult StudentTransferSettleReportAjaxRequest(int draw, int start, int length, long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus,
            string dateFrom, string dateTo, string prnNo, string name, string mobile)
        {
            NameValueCollection nvc = Request.Form;

            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "Code";
                            break;
                        case "2":
                            orderBy = "Status";
                            break;
                        case "3":
                            orderBy = "Rank";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                IList<StudentTransfer> studentTransferList = new List<StudentTransfer>();

                studentTransferList = _studentTransferService.LoadStudentTransferSettleByCriteria(start, length, programId, sessionId, selectedFromBranch, selectedFromCampus,
                    selectedToBranch, selectedToCampus, dateFrom, dateTo, prnNo, name, mobile).OrderBy(x => x.StudentProgram.PrnNo).ToList();

                var totalTransferList = _studentTransferService.LoadStudentTransferSettleByCriteriaCount(programId, sessionId,
                        selectedFromBranch, selectedFromCampus, selectedToBranch, selectedToCampus, dateFrom, dateTo, prnNo, name, mobile);

                int recordsTotal = totalTransferList.Count;
                long recordsFiltered = recordsTotal;


                long i = start + 1;
                foreach (var stl in studentTransferList)
                {
                    var str = new List<string>();
                    str.Add(i.ToString());
                    str.Add(stl.StudentProgram.PrnNo);
                    str.Add(stl.StudentProgram.Student.FullName);
                    str.Add(stl.StudentProgram.Student.NickName);
                    str.Add(stl.StudentProgram.Student.Mobile);
                    str.Add(stl.Payable.ToString());
                    str.Add(stl.Receiveable.ToString());
                    str.Add(stl.DueAmount.ToString());
                    str.Add(stl.TransferDate.ToString("yyyy-MM-dd"));

                    str.Add(stl.IsSettled ? "Settled" :
                        "<input type='checkbox' class='sCheckbox' payable='" + stl.Payable + "' receivable='" + stl.Receiveable + "' value='" + stl.Id + "'/>");
                    // 
                    //str.Add(stl.IsSettled ? "Settled" :
                    //    "<input type='checkbox' class='sCheckbox' name='check[]' payable='" + 100 + "' receivable='" + 100 + "' value='" + stl.Id + "'>");
                    getData.Add(str);
                    i = i + 1;
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData
                    //isSuccess = true
                });




            }
            catch (Exception)
            {

                ViewBag.ErrorMessage = "Problem Occurred";
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }
        [HttpPost]
        public ActionResult StudentTransferCompletelySettled(long[]transferIds)
        {
            try
            {
                if (transferIds != null)
                {
                    _studentTransferService.Update(transferIds);
                    return Json(new {IsSuccess = true});
                }
                else
                {
                    return Json(new { IsError = WebHelper.CommonErrorMessage });
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new { IsError = WebHelper.CommonErrorMessage });
            }
            
        }

        public ActionResult ExportStudentTransferSettlementList(long programId, long sessionId,
            long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus,string dateFrom,string dateTo, string prnNo, string name, string mobile)
        {
            int start = 0;
            int length = 0;
            try
            {
                IList<StudentTransfer> studentTransferList = new List<StudentTransfer>();

                studentTransferList = _studentTransferService.LoadStudentTransferSettleByCriteriaCount(programId, sessionId, selectedFromBranch, selectedFromCampus, selectedToBranch, selectedToCampus,dateFrom,dateTo, prnNo, name, mobile);

                var headerList = new List<string>
                {
                    ErpInfo.OrganizationNameFull,
                    _programService.GetProgram(programId).Name + " " + _sessionService.LoadById(sessionId).Name,
                    "Transfer Settlement List",
                    "From Branch :" + _branchService.GetBranch(selectedFromBranch.First()).Name,
                    "From Campus :" + _campusService.GetCampus(selectedFromCampus.First()).Name,
                    "To Branch :" + _branchService.GetBranch(selectedToBranch.First()).Name,
                    "To Campus :" + _campusService.GetCampus(selectedToCampus.First()).Name
                };
                var footerList = new List<string> {""};

                var columnList = new List<string>
                {
                    "Program Roll",
                    "Student's Full Name",
                    "Student's Nick Name",
                    "Mobile",
                    "Payable",
                    "Receiveable",
                    "Due Amount",
                    "Transfer Date",
                    "IsSettle"
                };

                var studentExcelList = new List<List<object>>();

                foreach (var xSes in studentTransferList)
                {
                    var xlsRow = new List<object>();
                    if (xSes.StudentProgram.PrnNo != null)
                        xlsRow.Add(xSes.StudentProgram.PrnNo);
                    xlsRow.Add(xSes.StudentProgram.Student.FullName ?? "");
                    xlsRow.Add(xSes.StudentProgram.Student.NickName ?? "");
                    xlsRow.Add(xSes.StudentProgram.Student.Mobile ?? "");
                    xlsRow.Add(xSes.Payable.ToString() ?? "");
                    xlsRow.Add(xSes.Receiveable.ToString() ?? "");
                    xlsRow.Add(xSes.DueAmount.ToString() ?? "");
                    xlsRow.Add(xSes.TransferDate.ToString("yyyy-MM-dd") ?? "");
                    xlsRow.Add(xSes.IsSettled);

                    studentExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "student-TranferSettlementList-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return View("AutoClose");
            }
        }

        public JsonResult TransferSettleCount(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, 
            long[] selectedToCampus, string dateFrom, string dateTo)
        {
            if (programId > 0 && sessionId > 0)
            {
                try
                {
                    var studentTransferList = _studentTransferService.LoadStudentTransferSettleByCriteriaCount(programId, sessionId,
                        selectedFromBranch, selectedFromCampus, selectedToBranch, selectedToCampus,dateFrom,dateTo);
                    var count = studentTransferList.Count;
                    ViewBag.Count = count;
                    return Json(count, JsonRequestBehavior.AllowGet);

                }
                catch (Exception)
                {
                    return Json(new Response(false, "Please select all field then calculate"));
                }
            }
            else
            {
                return Json(new Response(false, "Please select program and session"));
            }
        }

        #endregion

    }
}