﻿using System;
using System.Collections.Generic;
using System.Globalization;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class ProgramSettingFactory : ObjectFactoryBase<ProgramSettings>
    {
        #region Object Initialization

        protected readonly IProgramSettingService _programSettingService;

        public ProgramSettingFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _programSettingService = new ProgramSettingService(_session);
        }

        #endregion

        #region Create

        public ProgramSettingFactory WithNameAnd(string name)
        {
            Object.Name = name;
            return this;
        }
        public ProgramSettingFactory Create()
        {
            Object = new ProgramSettings()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 0,
                ModifyBy = 0,
                Status = Branch.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public ProgramSettingFactory WithProperties(Program program, Program nextProgram, Session session, Session nextSession)
        {
            Object.Program = program;
            Object.NextProgram = nextProgram;
            Object.Session = session;
            Object.NextSession = nextSession;
            return this;
        }
        public ProgramSettingFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _programSettingService.Save(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _programSettingService.Save(item);
                    }
                }
            }
            return this;
        }

        public ProgramSettingFactory CreateMore(int numberOfSessionSetting)
        {
            CreateProgramSettingList(numberOfSessionSetting);
            return this;
        }

        private List<ProgramSettings> CreateProgramSettingList(int numberOfSessionSetting )
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<ProgramSettings>();
            for (int i = 0; i < numberOfSessionSetting; i++)
            {
                var ss = new ProgramSettings()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Program.EntityStatus.Active
                };
                list.Add(ss);
            }
            ObjectList.AddRange(list);
            return list;
        }
        public ProgramSettingFactory CreateMore(int numberOfSessionSetting, List<Program> programList, List<Session> sessionlist
                                                    , List<Program> nextProgramList, List<Session> nextSessionlist)
        {
            CreateProgramSettingList(numberOfSessionSetting, programList, sessionlist, nextProgramList, nextSessionlist);
            return this;
        }

        private List<ProgramSettings> CreateProgramSettingList(int numberOfSessionSetting, List<Program> programList, List<Session> sessionlist
                                                                , List<Program> nextProgramList, List<Session> nextSessionlist)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<ProgramSettings>();
            for (int i = 0; i < numberOfSessionSetting; i++)
            {
                var ss = new ProgramSettings()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Program.EntityStatus.Active,
                    Program = programList[i],
                    NextProgram = nextProgramList[i],
                    Session = sessionlist[i],
                    NextSession = nextSessionlist[i]
                };
                list.Add(ss);
            }
            ObjectList.AddRange(list);
            return list;
        }

        #endregion

        #region Others

        #endregion

        #region Cleanup
        public void Cleanup()
        {
            DeleteAll();
        }

        #endregion

    }
}
