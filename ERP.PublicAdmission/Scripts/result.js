$(document).ready(function () {
    $("#resultCheck").submit(function () {
        return false;
    });
    $('body').keydown(function (e) {
        if (e.keyCode == 13) {
            //responseReturn();
            return false;
        }
    });
    //$(".loading").hide();
});
$(document).ready(function () {
    $(".loading").hide();
});

//$(document).on("input propertychange", "#ExamCode", function (event) {
//    if ($(this).val() == "") {
//        $(this).parent().append("<div class='errmsg' style='color:red;'>Exam code is required.</div>");
//    } else {
//        $(this).parent().find(".errmsg").remove();
//    }
//    $("#result").html("");
//    $("#ExamId").val("");
//    $("#ExamName").val("");
//    var courseId = $("#SelectedCourse").val();
//    var prnNo = $("#PrnNo").val();
//    var examCode = $("#ExamCode").val();
//    console.log("c: " + courseId + ", pn: " + prnNo + "ec: " + examCode);

//    if (prnNo != "" && prnNo.length == 11 && courseId != "" && examCode != "" && examCode.length == 3) {

//        $.ajax({
//            type: "post",
//            url: $("body").attr("data-project-root") + "Result/AjaxRequestForExamInformation",
//            cache: false,
//            async: true,
//            data: { examCode: examCode, courseId: courseId, prnNo: prnNo },
//            beforeSend: function () {
//                $("#ExamCode").parent().parent().find(".loading").show();
//                //$(".loading").show();
//                //$.blockUI({
//                //    timeout: 0,
//                //    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
//                //});
//            },
//            success: function (result) {
//                //  $.unblockUI();
//                $("#ExamCode").parent().parent().find(".loading").hide();
//                if (result.IsSuccess) {
//                    $(".customMessage ").html("");
//                    $("#ExamId").val(result.id);
//                    $("#ExamName").val(result.name);
//                }
//                else {
//                    var m = '<div class="alert alert-danger"><a data-dismiss="alert" class="close">X</a><strong>Error! </strong>' + result.returnList + '</div>';
//                    $(".customMessage ").html(m);
//                }
//            },
//            complete: function () {
//                console.log("Complete -----");
//                //  $.unblockUI();
//                $("#ExamCode").parent().parent().find(".loading").hide();
//            },
//            error: function (result) {
//                // $.unblockUI();
//                console.log("Error -----");
//                $("#ExamCode").parent().parent().find(".loading").hide();
//            }
//        });

//    }
//    //autoCompleteFunc();
//});

$(document).on("change", "#SelectedCourse", function (event) {
    

    $("#result").html("");
    $("#ExamId option").remove();
    $('#ExamId').append("<option value=''>--Select Exam--</option>");
    if ($(this).val() == "") {
        $(this).parent().append("<div class='errmsg' style='color:red;'>Course is required.</div>");
    } else {
        $(this).parent().find(".errmsg").remove();
    }
    var courseId = $("#SelectedCourse").val();
    var prnNo = $("#PrnNo").val();
    if (prnNo != "" && prnNo.length == 11 && courseId != "") {

        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Result/AjaxRequestForExamInformation",
            cache: false,
            async: true,
            data: { courseId: courseId, prnNo: prnNo },
            beforeSend: function () {
                $("#SelectedCourse").parent().parent().find(".loading").show();
            },
            success: function (result) {
                $("#SelectedCourse").parent().parent().find(".loading").hide();
                if (result.IsSuccess) {
                    $(".customMessage ").html("");
                    $.each(result.returnList, function (i, v) {
                        $('#ExamId').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    var m = '<div class="alert alert-danger"><a data-dismiss="alert" class="close">X</a><strong>Error! </strong>' + result.returnList + '</div>';
                    $(".customMessage ").html(m);
                }
            },
            complete: function () {
                console.log("Complete -----");
                $("#SelectedCourse").parent().parent().find(".loading").hide();
            },
            error: function (result) {
                console.log("Error -----");
                $("#SelectedCourse").parent().parent().find(".loading").hide();
            }
        });

    }


});

$(document).on("input propertychange", "#PrnNo ", function (event) {

    if ($(this).val() == "") {
        $(this).parent().append("<div class='errmsg' style='color:red;'>Program roll is required.</div>");
    } else {
        $(this).parent().find(".errmsg").remove();
    }
    $("#result").html("");
    $("#SelectedCourse option").remove();
    $('#SelectedCourse').append("<option value=''>--Select Course--</option>");
    $("#ExamId option").remove();
    $('#ExamId').append("<option value=''>--Select Exam--</option>");
    var prnNo = $(this).val();
    if (prnNo != "" && prnNo.length == 11) {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Result/AjaxRequestForCourse",
            cache: false,
            async: true,
            data: { "prnNo": prnNo },
            beforeSend: function () {
                $("#PrnNo").parent().parent().find(".loading").show();
                //$.blockUI({
                //    timeout: 0,
                //    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                //});
            },
            success: function (result) {
                // $.unblockUI();
                $("#PrnNo").parent().parent().find(".loading").hide();
                if (result.IsSuccess) {
                    $(".customMessage ").html("");
                    $.each(result.returnList, function (i, v) {
                        $('#SelectedCourse').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    
                    var m = '<div class="alert alert-danger"><a data-dismiss="alert" class="close">X</a><strong>Error! </strong>' + result.returnList + '</div>';
                    $(".customMessage ").html(m);
                }
            },
            complete: function () {
                console.log("Complete -----");
                // $.unblockUI();
                $("#PrnNo").parent().parent().find(".loading").hide();
            },
            error: function (result) {
                //  $.unblockUI();
                $("#PrnNo").parent().parent().find(".loading").hide();
                console.log("Error -----");
            }
        });
    }
});

$(document).on("click", "#displayResult", function (event) {
    var prnNo = $("#PrnNo").val();
    var courseId = $("#SelectedCourse").val();
    var examId = $("#ExamId").val();
    var success = true;
    $('input, select').each(
    function () {
        var input = $(this);
        var msg = "";
        if (input.val() == null || input.val() == "") {
            if (input.attr('id') == "PrnNo") {
                msg = "Program roll is required.";
                success = false;
            }
            if (input.attr('id') == "SelectedCourse") {
                msg = "Course is required.";
                success = false;
            }
            if (input.attr('id') == "ExamId") {
                msg = "Exam is required.";
                success = false;
            }
            input.parent().find(".errmsg").remove();
            input.parent().append("<div class='errmsg' style='color:red;'>" + msg + "</div>");

        }
    }
);
    $("#result").html("");
    if (prnNo != "" && courseId != "" && examId != "" && success) {
        $(".customMessage ").html("");
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Result/AjaxRequestForReport",
            cache: false,
            async: true,
            data: { "prnNo": prnNo, "courseId": courseId, "examId": examId },
            beforeSend: function () {
                $("#displayResult").parent().parent().find(".loading").show();
                //$.blockUI({
                //    timeout: 0,
                //    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                //});
            },
            success: function (result) {
                $("#displayResult").parent().parent().find(".loading").hide();
                // $.unblockUI();
                $(".customMessage ").html("");
                if (!result.hasOwnProperty("IsSuccess")) {
                    $("#result").html(result);
                }
                else {
                    var m = '<div class="alert alert-danger"><a data-dismiss="alert" class="close">X</a><strong>Error! </strong>' + result.returnList + '</div>';
                    $(".customMessage ").html(m);
                }
            },
            complete: function () {
                console.log("Complete -----");
                // $.unblockUI();
                $("#displayResult").parent().parent().find(".loading").hide();
            },
            error: function (result) {
                $("#displayResult").parent().parent().find(".loading").hide();
                // $.unblockUI();
                console.log("Error -----");
            }
        });
    }
    //else {
    //    var m = '<div class="alert alert-danger"><a data-dismiss="alert" class="close">X</a><strong>Error!</strong> Please fill all fields</div>';
    //    $(".customMessage ").html(m);
    //}
});

//$(document).on("input propertychange", ".tridclass ", function (event) {
//    var input = $(this),
//    text = input.val().replace(/[^0-9.]/g, "");
//    input.val(text);
//});