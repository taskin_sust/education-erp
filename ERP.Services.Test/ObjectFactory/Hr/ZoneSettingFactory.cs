﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;
using System.Collections;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class ZoneSettingFactory : ObjectFactoryBase<ZoneSetting>
    {        
        protected readonly IZoneSettingService _zoneSettingService;        
        private OrganizationFactory _organizationFactory;

        public ZoneSettingFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
        {   
            _zoneSettingService = new ZoneSettingService(_session);            
            _organizationFactory = new OrganizationFactory(this, _session);                      
        }

        public ZoneSettingFactory Create(int toleranceTime = 1, int toleranceDay = 0, decimal deductionMultiplier = 0)
        {
            Object = new ZoneSetting
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 10,
                ToleranceTime = toleranceTime,
                ToleranceDay = toleranceDay,
                DeductionMultiplier = deductionMultiplier,
                CreateBy = 1,
                ModifyBy = 1,
                Status = ZoneSetting.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public ZoneSettingFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }
        public ZoneSettingFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }
        internal ZoneSettingFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        internal ZoneSettingFactory WithToleranceTimeToleranceDayDeductionMultiplier(int toleranceTime, int toleranceDay, decimal deductionMultiplier)
        {
            Object.ToleranceTime = toleranceTime;
            Object.ToleranceDay = toleranceDay;
            Object.DeductionMultiplier = deductionMultiplier;
            return this;
        }
        public ZoneSettingFactory Persist()
        {
            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    _zoneSettingService.Save(x);
                }
            });            

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _zoneSettingService.Save(item);
                    }
                }
            }

            return this;
        }

        public ZoneSettingFactory CreateMore(int numberOfZone)
        {
            CreateZoneList(numberOfZone);
            return this;
        }

        private List<ZoneSetting> CreateZoneList(int numberOfZone)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<ZoneSetting>();
            var orgNamePrefix = Guid.NewGuid().ToString();
            _organizationFactory.Create()
                .WithNameAndRank(orgNamePrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"), 1)
                .Persist();

            for (int i = 0; i < numberOfZone; i++)
            {
                int toleranceTime = i;
                int toleranceDay = new Random().Next(0, 5);
                decimal deductionMultiplier = new Random().Next(0, (numberOfZone/2)/numberOfZone);
                var zoneSetting = new ZoneSetting
                {
                    Id = 0,
                    Name = Name + "_" + i,
                    Rank = i,
                    ToleranceTime = toleranceTime,
                    ToleranceDay = toleranceDay,
                    DeductionMultiplier = deductionMultiplier,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = ZoneSetting.EntityStatus.Active
                };
                zoneSetting.Organization = _organizationFactory.Object;
                list.Add(zoneSetting);
            }
            ObjectList.AddRange(list);
            return list;
        }

        public ZoneSettingFactory WithOrganization(string orgName, int rank, string shortName, string contact, decimal basicSalary, decimal houseRent, decimal medicalAllowance, decimal convenyance, int monthlyWorkingDay, decimal deductionPerAbsent)
        {
            _organizationFactory.CreateWith(orgName, rank, shortName, contact, basicSalary, houseRent, medicalAllowance, convenyance, monthlyWorkingDay, deductionPerAbsent);
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        public void Cleanup()
        {
            DeleteAll();
            _organizationFactory.Cleanup();
        }

        
    }
}
