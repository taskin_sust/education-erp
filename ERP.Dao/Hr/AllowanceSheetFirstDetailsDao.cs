﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Dao.Base;
using UdvashERP.MessageExceptions;
using NHibernate.Criterion;

namespace UdvashERP.Dao.Hr
{
    public interface IAllowanceSheetFirstDetailsDao : IBaseDao<BusinessModel.Entity.Hr.AllowanceSheetFirstDetails, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        IList<AllowanceSheetFirstDetails> GetAllowanceSheetFirstDetailByOvertimeAllowanceSheet(long overtimeAllowanceSheetId);

        #endregion

        #region List Loading Function

        IList<AllowanceSheetFirstDetails> LoadByDailySupportAllowanceId(long dailySupportAllowanceId);
        IList<AllowanceSheetFirstDetails> LoadByNightWorkAllowanceId(long nightworkAllowanceId);
        IList<TeamMemberAllowanceSheetFirstDetailsDto> LoadAllowanceSheetFirstDetailsDto(long teamMemberId, DateTime dateFrom, DateTime dateTo);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion


    }
    public class AllowanceSheetFirstDetailsDao : BaseDao<BusinessModel.Entity.Hr.AllowanceSheetFirstDetails, long>, IAllowanceSheetFirstDetailsDao
    {
        #region Properties & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public IList<AllowanceSheetFirstDetails> GetAllowanceSheetFirstDetailByOvertimeAllowanceSheet(long overtimeAllowanceSheetId)
        {
            ICriteria criteria = Session.CreateCriteria<AllowanceSheetFirstDetails>().Add(Restrictions.Eq("Status", AllowanceSheetFirstDetails.EntityStatus.Active));
            criteria.CreateAlias("OvertimeAllowanceSetting", "overtimeAllowanceSetting").Add(Restrictions.Eq("overtimeAllowanceSetting.Status", OvertimeAllowanceSetting.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("overtimeAllowanceSetting.Id", overtimeAllowanceSheetId));
            return criteria.List<AllowanceSheetFirstDetails>();
        }

        #endregion

        #region List Loading Function

        public IList<AllowanceSheetFirstDetails> LoadByDailySupportAllowanceId(long dailySupportAllowanceId)
        {
            return Session.QueryOver<AllowanceSheetFirstDetails>().Where(x => x.DailySupportAllowanceSetting.Id == dailySupportAllowanceId).List<AllowanceSheetFirstDetails>();
        }

        public IList<AllowanceSheetFirstDetails> LoadByNightWorkAllowanceId(long nightworkAllowanceId)
        {
            return Session.QueryOver<AllowanceSheetFirstDetails>().Where(x => x.NightWorkAllowanceSetting.Id == nightworkAllowanceId).List<AllowanceSheetFirstDetails>();
        }

        public IList<TeamMemberAllowanceSheetFirstDetailsDto> LoadAllowanceSheetFirstDetailsDto(long teamMemberId, DateTime dateFrom, DateTime dateTo)
        {
            string query = GetAllowanceSheetFirstDetailsDtoQueryString(teamMemberId, dateFrom, dateTo);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberAllowanceSheetFirstDetailsDto>());
            iQuery.SetTimeout(2700);
            List<TeamMemberAllowanceSheetFirstDetailsDto> list = iQuery.List<TeamMemberAllowanceSheetFirstDetailsDto>().ToList();
            return list;
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        private string GetAllowanceSheetFirstDetailsDtoQueryString(long teamMemberId, DateTime dateFrom, DateTime dateTo)
        {
            string query = "";
            string startDate = dateFrom.ToString("yyyy-MM-dd");
            string endDate = dateTo.ToString("yyyy-MM-dd");

            query = @"
DECLARE @StartDate DATE = '" + startDate + @"';
DECLARE @EndDate DATE = '" + endDate + @"';
DECLARE @teamMemberId int = " + teamMemberId + @";
WITH N1 (N) AS (SELECT 1 FROM (VALUES (1), (1), (1), (1), (1), (1), (1), (1), (1), (1)) n (N)),
N2 (N) AS (SELECT 1 FROM N1 AS N1 CROSS JOIN N1 AS N2),
N3 (N) AS (SELECT 1 FROM N2 AS N1 CROSS JOIN N2 AS N2)
select 
	MonthDate.Date as Date
	, atm.AttendanceDate
	, atm.InTime
	, atm.OutTime
	, atm.isGazetted as IsGazetted
	, atm.isManagement as IsManagement
	, atm.IsWeekend
	, emh.EffectiveDate as EmploymentHistoryEffectiveDate
	, emh.OrganizationId as JobOrganizationId
	, emh.DepartmentId as JobDepartmentId
	, emh.DesignationId as JobDesignationId
	, emh.CampusId as JobCampusId
	, emh.WorkingHour as JobWorkingHour
	, emh.MonthlyWorkingDay as JobMonthlyWorkingDay
	, emh.EmployeeStatus as EmployeeStatus
	, sh.SalaryId as SalaryId
	, sh.EffectiveDate as SalaryHistoryEffectiveDate
	, sh.SalaryOrganizationId as SalaryOrganizationId
	, sh.SalaryDepartmentId as SalaryDepartmentId
	, sh.SalaryDesignationId as SalaryDesignationId
	, sh.SalaryCampusId as SalaryCampusId
	, sh.Salary as Salary
	, sh.BankAmount as BankAmount
	, sh.CashAmount as CashAmount
	, sh.oneDayBasicSalary as OneDayBasicSalary
	, sh.oneDayGrossSalary as OneDayGrossSalary
	, sh.oneMinBasicSalary as OneMinBasicSalary
	, sh.oneMinGrossSalary as OneMinGrossSalary
	, dailySupportAllowanceSetting.Id as DailySupportAllowanceSettingId
	, extraDayAllowance.Id as ExtraDayAllowanceId
	, overTimeAllowance.Id as OverTimeAllowanceId
	, nightWorkAllowance.Id as NightWorkAllowanceId
	, ROUND(CASE when (isnull(dailySupportAllowanceSetting.id,0) > 0 and isnull(atm.id,0) > 0  and atm.InTime is not null and atm.OutTime is not null)  then dailySupportAllowanceSetting.Amount else 0 end,0) as 
	DailySupportAllowanceAmount
	, ROUND(CASE when (isnull(sh.SalaryId,0) > 0 and isnull(extraDayAllowance.id,0) > 0 )  
		then Case when extraDayAllowance.CalculationOn = " + (int)CalculationOnTypeEnum.Basic + @" then  (sh.oneDayBasicSalary* extraDayAllowance.HolidayWorkApprovalType*extraDayAllowance.MultiplyingFactor) else (sh.oneDayGrossSalary* extraDayAllowance.HolidayWorkApprovalType*extraDayAllowance.MultiplyingFactor) end 
	  else 0 end,0) as ExtradayAllowanceAmount
	  , ROUND(CASE when (isnull(sh.SalaryId,0) > 0 and isnull(overTimeAllowance.id,0) > 0 )  
		then Case when overTimeAllowance.CalculationOn = " + (int)CalculationOnTypeEnum.Basic + @" then  (sh.oneMinBasicSalary* overTimeAllowance.ApprovedOverTime*overTimeAllowance.MultiplyingFactor) else (sh.oneMinGrossSalary* overTimeAllowance.ApprovedOverTime*overTimeAllowance.MultiplyingFactor) end 
	  else 0 end,0) as OvertimeAllowanceAmount
	  , ROUND(CASE when (isnull(sh.SalaryId,0) > 0 and isnull(nightWorkAllowance.id,0) > 0 )  
		then Case when nightWorkAllowance.CalculationOn = " + (int)CalculationOnTypeEnum.Basic + @" then  
				case when nightWorkAllowance.InHouseWork = " + (int)HouseStatus.In + @" then (sh.oneDayBasicSalary* nightWorkAllowance.nightWorkApprovalType*nightWorkAllowance.InHouseMultiplyingFactor) 
					else (sh.oneDayBasicSalary* nightWorkAllowance.nightWorkApprovalType*nightWorkAllowance.OutHouseMultiplyingFactor)  
				end
		   	else case when nightWorkAllowance.InHouseWork = " + (int)HouseStatus.In + @" then (sh.oneDayGrossSalary* nightWorkAllowance.nightWorkApprovalType*nightWorkAllowance.InHouseMultiplyingFactor) 
				else (sh.oneDayGrossSalary* nightWorkAllowance.nightWorkApprovalType*nightWorkAllowance.OutHouseMultiplyingFactor)  
				end
			end
	  else 0 end,0) as NightWorkAllowanceAmount
from (
SELECT TOP (DATEDIFF(DAY, @StartDate, @EndDate) + 1)
		Date = DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DAY, ROW_NUMBER() OVER(ORDER BY N) - 1, @StartDate)))
FROM N3 ) as MonthDate
---- Attendance Summary Start 
/**
	Gazetted = 1,
    Management = 2,
    Weekend = 3
**/
left join (
select atm.*
 , case when (ISNULL(hds.Id,0) > 0 and ISNULL(hds.HolidayType,0) > 0 and hds.HolidayType = " + (int)HolidayTypeEnum.Gazetted + @") then 1 else 0 end as isGazetted 
 , case when (ISNULL(hds.Id,0) > 0 and ISNULL(hds.HolidayType,0) > 0 and hds.HolidayType = " + (int)HolidayTypeEnum.Management + @") then 1 else 0 end as isManagement
 , case when (ISNULL(HolidaySettingId,0) <= 0 and IsWeekend = 0 ) then 1 else 0 end as isWorkingDay
 from 
	HR_AttendanceSummary as atm 
	left join HR_HolidaySetting as hds on hds.Id = atm.HolidaySettingId and hds.Status = " + HolidaySetting.EntityStatus.Active + @"
	where atm.Status = " + AttendanceSummary.EntityStatus.Active + @"
) as atm on atm.TeamMemberId =  @teamMemberId and atm.AttendanceDate = MonthDate.Date
---- Attendance Summary End
---- Employment History Start  -----
Outer APPLY  (
	Select * from (
		Select 
		    Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		, emh.EmploymentStatus as EmployeeStatus
		, emh.EffectiveDate As EffectiveDate
		, emh.CampusId as CampusId
		, emh.DepartmentId as DepartmentId
		, emh.DesignationId as DesignationId
		, org.Id as OrganizationId
		, org.WorkingHour as WorkingHour
		, org.MonthlyWorkingDay as MonthlyWorkingDay
		, org.BasicSalary as BasicSalary
		from HR_EmploymentHistory as emh
		inner join HR_Department as dep on dep.Id = emh.DepartmentId
		inner join Organization as org on org.Id = dep.OrganizationId
		where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		and  emh.EffectiveDate <=  MonthDate.Date
		and emh.TeamMemberId = @teamMemberId
	) as employmentHistory 
	where 1=1
	and employmentHistory.r = 1
) as emh
---- Employment History End ----- 

---- Salary History Start 
Outer APPLY  (
	Select * from (
		Select 
		    Rank() Over(Partition by sh.TeamMemberId order by sh.EffectiveDate DESC, sh.Id DESC) AS r
		, sh.EffectiveDate As EffectiveDate
		, sh.SalaryCampusId as SalaryCampusId
		, sh.SalaryDepartmentId as SalaryDepartmentId
		, sh.SalaryDesignationId as SalaryDesignationId
		, sh.SalaryOrganizationId as SalaryOrganizationId
		, sh.Salary as Salary
		, sh.BankAmount as BankAmount
		, sh.CashAmount as CashAmount
		, sh.Id as SalaryId
		, case when (isnull(sh.Salary,0) > 0 and isnull(emh.MonthlyWorkingDay,0)  > 0 and isnull(emh.BasicSalary,0)  > 0) then ((sh.Salary * (emh.BasicSalary)) / 100) / emh.MonthlyWorkingDay Else 0 end as oneDayBasicSalary
		, case when (isnull(sh.Salary,0) > 0 and isnull(emh.MonthlyWorkingDay,0)  > 0 and isnull(emh.BasicSalary,0)  > 0 and isnull(emh.WorkingHour,0)  > 0) then ((sh.Salary * (emh.BasicSalary)) / 100) / (emh.MonthlyWorkingDay * (emh.WorkingHour * 60)) Else 0 end as oneMinBasicSalary
		, case when (isnull(sh.Salary,0) > 0  and isnull(emh.MonthlyWorkingDay,0)  > 0 ) then sh.Salary  / emh.MonthlyWorkingDay Else 0 end as oneDayGrossSalary
		, case when (isnull(sh.Salary,0) > 0 and isnull(emh.MonthlyWorkingDay,0)  > 0 and isnull(emh.WorkingHour,0)  > 0 ) then sh.Salary / (emh.MonthlyWorkingDay * (emh.WorkingHour * 60)) Else 0 end as oneMinGrossSalary
		from HR_SalaryHistory as sh
		where sh.Status = " + SalaryHistory.EntityStatus.Active + @"
		and  sh.EffectiveDate <=  MonthDate.Date
		and sh.TeamMemberId = @teamMemberId
	) as salaryHistory 
	where 1=1
	and salaryHistory.r = 1
) as sh
---- Salary History End
---- Daily Support Allowance Setting Start 
Outer APPLY  (
	select * from (
		select top 1 *
		, (
			select count(id) as c 
			from  HR_DailySupportAllowanceBlock 
			where Status =" + DailySupportAllowanceBlock.EntityStatus.Active + @"
			and IsBlocked = 1
			and TeamMemberId = @teamMemberId
			and BlockingDate = MonthDate.Date
		 ) as dSblock
		from  HR_DailySupportAllowanceSetting as dsas
		where dsas.Status = " + DailySupportAllowanceSetting.EntityStatus.Active + @"
		and dsas.DesignationId = emh.DesignationId
		and dsas.EffectiveDate <=  MonthDate.Date
		and (dsas.ClosingDate is null  or dsas.ClosingDate >= MonthDate.Date)
		order by dsas.EffectiveDate DESC
	) as a
	where a.dSblock  = 0
) as dailySupportAllowanceSetting
---- Daily Support Allowance Setting End
---- Extra day Allowance Calculation Start
/**
 Half = 1,
Full = 2
**/
Outer APPLY  (
	select * from (
		select top 1 *
		, (
			select case when ApprovalType = " + (int)HolidayWorkApprovalStatus.Half + @" then .5 when ApprovalType = " + (int)HolidayWorkApprovalStatus.Full + @" then 1 else 0 end   as HolidayWorkApprovalType
			from HR_HolidayWork
			where Status = " + HolidayWork.EntityStatus.Active + @"
			and TeamMemberId = @teamMemberId
			and HolidayWorkDate = MonthDate.Date
			and isnull(ApprovalType,0) > 0
		) as HolidayWorkApprovalType
		from HR_ExtraDayAllowanceSetting as edas
		where edas.Status = " + ExtradayAllowanceSetting.EntityStatus.Active + @"
		and edas.OrganizationId = emh.OrganizationId
		and (
			(edas.IsWeekend = 1 and atm.IsWeekend =1)
			or (edas.IsManagement = 1 and atm.isManagement =1)
			or (edas.IsGazetted = 1 and atm.isGazetted =1)
			) 
		and (
			(edas.IsProbation = 1 and emh.EmployeeStatus = " + (int)MemberEmploymentStatus.Probation + @")
			or (edas.IsPermanent = 1 and emh.EmployeeStatus = " + (int)MemberEmploymentStatus.Permanent + @")
			or (edas.IsPartTime = 1 and emh.EmployeeStatus =" + (int)MemberEmploymentStatus.PartTime + @")
			or (edas.IsContractual = 1 and emh.EmployeeStatus =" + (int)MemberEmploymentStatus.Contractual + @")
			or (edas.IsIntern = 1 and emh.EmployeeStatus =" + (int)MemberEmploymentStatus.Intern + @")
			) 
		and edas.EffectiveDate <=  MonthDate.Date
		and (edas.ClosingDate is null  or edas.ClosingDate >= MonthDate.Date)
		order by edas.EffectiveDate desc, edas.MultiplyingFactor desc
	) as a 
	where a.HolidayWorkApprovalType > 0
) as extraDayAllowance
---- Extra day Allowance Calculation END
---- Overtime Allowance Calculation Start
Outer APPLY  (
	select * from (
		select top 1 *
		, (
			select  ((DATEPART(hh,ApprovedTime) * 60) + DATEPART(mi,ApprovedTime))  as ApprovedOverTime
			from HR_Overtime
			where Status = " + Overtime.EntityStatus.Active + @"
			and TeamMemberId = @teamMemberId
			and OverTimeDate = MonthDate.Date
			and (DATEPART(hh,ApprovedTime) > 0 or DATEPART(mi,ApprovedTime) > 0)
		) as ApprovedOverTime
		from HR_OvertimeAllowanceSetting as otas
		where otas.Status = " + OvertimeAllowanceSetting.EntityStatus.Active + @"
		and otas.OrganizationId = emh.OrganizationId
		and (
			(otas.IsManagementWeekend = 1 and (atm.IsWeekend =1 or atm.isManagement =1))
			or (otas.IsGazzeted = 1 and atm.isGazetted =1)
			or (otas.IsWorkingDay = 1 and atm.isWorkingDay =1)
			) 
		and (
			(otas.IsProbation = 1 and emh.EmployeeStatus = " + (int)MemberEmploymentStatus.Probation + @")
			or (otas.IsPermanent = 1 and emh.EmployeeStatus = " + (int)MemberEmploymentStatus.Permanent + @")
			or (otas.IsPartTime = 1 and emh.EmployeeStatus =" + (int)MemberEmploymentStatus.PartTime + @")
			or (otas.IsContractual = 1 and emh.EmployeeStatus =" + (int)MemberEmploymentStatus.Contractual + @")
			or (otas.IsIntern = 1 and emh.EmployeeStatus =" + (int)MemberEmploymentStatus.Intern + @")
			) 
		and otas.EffectiveDate <=  MonthDate.Date
		and (otas.ClosingDate is null  or otas.ClosingDate >= MonthDate.Date)
		order by otas.EffectiveDate desc, otas.MultiplyingFactor desc
	) as a 
	where a.ApprovedOverTime > 0
) as overTimeAllowance
---- Overtime Allowance Calculation END
----  Night TransactionHelper.Work Allowance Calculation Start
Outer APPLY  (
	select * from (
		select top 1 *
		, (
			select case when ApprovalType = " + (int)HolidayWorkApprovalStatus.Half + @" then .5 when ApprovalType = " + (int)HolidayWorkApprovalStatus.Full + @" then 1 else 0 end   as nightWorkApprovalType
			from HR_NightWork
			where Status = " + NightWork.EntityStatus.Active + @"
			and TeamMemberId = @teamMemberId
			and NightWorkDate = MonthDate.Date
			and isnull(ApprovalType,0) > 0
		) as nightWorkApprovalType
		, (
			select InHouseWork
			from HR_NightWork
			where Status = " + NightWork.EntityStatus.Active + @"
			and TeamMemberId = @teamMemberId
			and NightWorkDate = MonthDate.Date
			and isnull(ApprovalType,0) > 0
		) as InHouseWork
		from HR_NightWorkAllowanceSetting as nwas
		where nwas.Status = " + NightWorkAllowanceSetting.EntityStatus.Active + @"
		and nwas.OrganizationId = emh.OrganizationId
		and (
			(nwas.IsProbation = 1 and emh.EmployeeStatus = " + (int)MemberEmploymentStatus.Probation + @")
			or (nwas.IsPermanent = 1 and emh.EmployeeStatus = " + (int)MemberEmploymentStatus.Permanent + @")
			or (nwas.IsPartTime = 1 and emh.EmployeeStatus =" + (int)MemberEmploymentStatus.PartTime + @")
			or (nwas.IsContractual = 1 and emh.EmployeeStatus =" + (int)MemberEmploymentStatus.Contractual + @")
			or (nwas.IsIntern = 1 and emh.EmployeeStatus =" + (int)MemberEmploymentStatus.Intern + @")
			) 
		and nwas.EffectiveDate <=  MonthDate.Date
		and (nwas.ClosingDate is null  or nwas.ClosingDate >= MonthDate.Date)
		order by nwas.EffectiveDate desc, nwas.InHouseMultiplyingFactor desc
	) as a 
	where a.nightWorkApprovalType > 0
) as nightWorkAllowance
----  Night Work Allowance Calculation END
where 1=1
order by MonthDate.Date desc";

            return query;
        }

        #endregion
    }
}
