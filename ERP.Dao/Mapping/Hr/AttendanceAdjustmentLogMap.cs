using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {


    public partial class AttendanceAdjustmentLogMap : ClassMap<AttendanceAdjustmentLog>
    {
        
        public AttendanceAdjustmentLogMap() {
			Table("HR_AttendanceAdjustmentLog");
			LazyLoad();
            References(x => x.AttendanceAdjustment).Cascade.All().Column("AttendanceAdjustmentId");
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
			Map(x => x.Date).Column("Date");
			Map(x => x.StartTime).Column("StartTime");
			Map(x => x.EndTime).Column("EndTime");
			Map(x => x.Reason).Column("Reason");
            Map(x => x.IsPost).Column("IsPost");
			Map(x => x.AdjustmentStatus).Column("AdjustmentStatus");
        }
    }
}
