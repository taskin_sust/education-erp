﻿using System;
using System.Collections.Generic;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.AdminisTration.ProgramTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ActionTest
{
    interface IActionDeleteTest
    {
        #region Basic Test
        
        #endregion
    }
    [Trait("Area", "Administration")]
    [Trait("Service", "Action")]
    public class ActionDeleteTest : ActionBaseTest, IActionDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            var actionFactory =
                _actionsFactory.Create().WithController().WithNameAndRank(_name, 1)
                    .Persist();
            var success=_actionsService.Delete(actionFactory.Object.Id, actionFactory.Object);
            var action = _actionsService.LoadById(actionFactory.Object.Id);
            Assert.True(success);
            Assert.Null(action);
        }

        
        [Fact]
        public void Action_Delete_Should_Not_Delete_Controller()
        {
            var actionFactory =
                _actionsFactory.Create().WithController().WithNameAndRank(_name, 1)
                    .Persist();
            var success = _actionsService.Delete(actionFactory.Object.Id, actionFactory.Object);
            AreaControllers areaControllers = _areaControllersService.LoadById(actionFactory.Object.AreaControllers.Id);
            Assert.True(success);
            Assert.NotNull(areaControllers);
        }
       
        #endregion

    }
}
