﻿namespace Synchronizer.Updater.Model
{
    public class JsonResponse
    {
        public bool IsSuccess { get; set; }
        public dynamic Data { get; set; }

        public JsonResponse()
        {
        }

        public JsonResponse(bool isSuccess, dynamic data)
        {
            IsSuccess = isSuccess;
            Data = data;
        }
    }
}
