﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MentorHisotyTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "MentorHistory")]
    public class MentorHistoryLoadTest : MentorHistoryBaseTest
    {
        [Fact]
        public void GetMentorHistory_Should_Return_Mentor_History_Entity_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember);
            _mentorHistoryService.Save(teamMember.MentorHistory[0].Mentor.Pin, _mentorHistoryFactory.Object);
            var entity = _mentorHistoryService.GetMentorHistory(_mentorHistoryFactory.Object.Id);
            Assert.NotNull(entity);
            Assert.Equal(entity.Id, _mentorHistoryFactory.Object.Id);
        }


        [Fact]
        public void GetTeamMemberMentorHistory_Should_Return_Mentor_History_Entity_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember);
            _mentorHistoryService.Save(teamMember.MentorHistory[0].Mentor.Pin, _mentorHistoryFactory.Object);
            var entity = _mentorHistoryService.GetTeamMemberMentorHistory(_mentorHistoryFactory.Object.EffectiveDate, teamMember.Id, teamMember.Pin);
            Assert.NotNull(entity);
            Assert.Equal(entity.Id, _mentorHistoryFactory.Object.Id);
        }

        [Fact]
        public void LoadMentorHistory_Should_Return_Mentor_History_List()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember);
            _mentorHistoryService.Save(teamMember.MentorHistory[0].Mentor.Pin, _mentorHistoryFactory.Object);
            var list = _mentorHistoryService.LoadMentorHistory(teamMember.Pin, _mentorHistoryFactory.Object.EffectiveDate);
            Assert.NotNull(list);
            Assert.True(list.Count > 0);
            Assert.Equal(list[0].Id, _mentorHistoryFactory.Object.Id-1);
        }
    }
}
