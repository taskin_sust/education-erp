using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;

namespace UdvashERP.Dao.UInventory
{
    //public class ExpandoObjectResultSetTransformers : IResultTransformer
    //{
    //    private string[] allAloases;
    //    public object TransformTuple(object[] tuple, string[] aliases)
    //    {
    //        var expando = new ExpandoObject();
    //        var dictionary = (IDictionary<string, object>)expando;
    //        for (int i = 0; i < tuple.Length; i++)
    //        {
    //            string alias = aliases[i];
    //            if (alias != null)
    //            {
    //                dictionary[alias] = tuple[i];
    //            }
    //        }
    //        return expando;
    //    }
    //    public IList TransformList(IList collection)
    //    {
    //        foreach (KeyValuePair<string, object> dictionary in collection)
    //        {
    //            // generate a class with it's property here 
    //            // set the value of those properties
    //        }
    //        return collection;
    //    }
    //}

    public interface IGoodsReturnDao : IBaseDao<GoodsReturn, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        List<GoodsReturnDto> LoadGoodsReturn(int start, int length, string orderBy, string toUpper, List<long> branchIdList, string item, DateTime? goodsReturnDateValue, int? returnStatus);

        IList<dynamic> LoadGoodsReturnReport(int start, int length, List<long> authBranchIdList, List<long> purposeList, List<long> itemGroupList, List<long> itemList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, DateTime dateFrom, DateTime dateTo, List<string> columnList, int reportType, List<string> programsession);

        IList<string> LoadGoodsReturnBranchNameList(List<long> authBranchIdList);

        IList<string> LoadGoodsReturnProgramSessionNameList(List<long> programIdList, List<long> sessionIdList);

        #endregion

        #region Others Function

        int GetGoodsReturnCount(List<long> branchIdList, string item, DateTime? goodsReturnDateValue, int? returnStatus);

        string GenerateSeqeuntialNo(long branchId);

        long GetGoodsReturnReportCount(List<long> authBranchIdList, List<long> purposeList, List<long> itemGroupList, List<long> itemList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, DateTime dateFrom, DateTime dateTo, List<string> columnList, int reportType, List<string> programsession);

        long GetTotalReturnAmountAgainstWorkorderNo(string workOrderNo);
        long GetTotalReturnAmountAgainstReceiveId(long receiveId);

        #endregion


    }

    public class GoodsReturnDao : BaseDao<GoodsReturn, long>, IGoodsReturnDao
    {
        #region Properties & Object & Initialization
        public static IResultTransformer ExpandoObject;
        private const int MaximumParameter = 2000;
        public GoodsReturnDao()
        {
            ExpandoObject = new ExpandObjectResultSetTransformers();
        }
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public List<GoodsReturnDto> LoadGoodsReturn(int start, int length, string orderBy, string toUpper, List<long> branchIdList, string item, DateTime? goodsReturnDateValue, int? returnStatus)
        {
            string query = GetCommonSqlQuery(branchIdList, item, goodsReturnDateValue, returnStatus);
            IQuery iQuery = Session.CreateSQLQuery(query);
            if (length > 0)
            {
                iQuery.SetFirstResult(start).SetMaxResults(length);
            }
            iQuery.SetResultTransformer(Transformers.AliasToBean<GoodsReturnDto>());
            var list = iQuery.List<GoodsReturnDto>().ToList();
            return list;
        }

        public IList<string> LoadGoodsReturnProgramSessionNameList(List<long> programIdList, List<long> sessionIdList)
        {
            string wherequery = "";
            if (programIdList != null && programIdList.Any())
            {
                wherequery += " and p.id in(" + string.Join(", ", programIdList) +
                ")";
            }
            if (sessionIdList != null && sessionIdList.Any())
            {
                wherequery += " and s.id in(" + string.Join(", ", sessionIdList) +
                ")";
            }
            string query = @"select p.ShortName+' '+s.Name from UINV_GoodsReturn gret
                            inner join UINV_GoodsReceive grec on gret.GoodsReceivedId=grec.Id
                            inner join UINV_GoodsReceiveDetails grecd on grec.Id=grecd.GoodsRecieveId
                            inner join Program p on grecd.ProgramId=p.Id
                            inner join Session s on grecd.SessionId=s.Id 
                            where gret.status=1 and grec.status=1 and grecd.status=1 and p.status=1 and s.status=1 "
                            + wherequery;
            IQuery iQuery = Session.CreateSQLQuery(query);
            var list = iQuery.List<string>().ToList();
            return list.Distinct().ToList();
        }

        public IList<dynamic> LoadGoodsReturnReport(int start, int length, List<long> authBranchIdList, List<long> purposeList, List<long> itemGroupList, List<long> itemList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, DateTime dateFrom, DateTime dateTo, List<string> columnList, int reportType, List<string> programsession)
        {
            var query = GetGoodsReturnReportCommonQuery(authBranchIdList, purposeList, itemGroupList, itemList, programIdList, sessionIdList, supplierIdList, dateFrom, dateTo, columnList, reportType, programsession);
            IQuery fQuery = Session.CreateSQLQuery(query).SetFirstResult(start).SetMaxResults(length);
            IList<dynamic> result = DynamicList(fQuery);
            return result;
        }

        #endregion

        #region Others Function

        public int GetGoodsReturnCount(List<long> branchIdList, string item, DateTime? goodsReturnDateValue, int? returnStatus)
        {
            string query = "Select count(*) from( " + GetCommonSqlQuery(branchIdList, item, goodsReturnDateValue, returnStatus) + " )x";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public long GetTotalReturnAmountAgainstWorkorderNo(string workOrderNo)
        {
            var queryStr = @"select sum(gretdet.[ReturnedQuantity]) [return quantity] from [UINV_GoodsReturn] gret inner join [UINV_GoodsReturnDetails] gretdet on gret.id = gretdet.[GoodsReturnId]
                            where gret.[GoodsReceivedId] in(select gr.id from [UINV_GoodsReceive] gr 
                                where gr.workorderid =(select worder.id from [UINV_WorkOrder] worder where worder.[WorkOrderNo]= " + "'" + workOrderNo + "'" + " ))";

            IQuery iQuery = Session.CreateSQLQuery(queryStr);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public long GetTotalReturnAmountAgainstReceiveId(long receiveId)
        {
            var queryStr = @"select sum(gretdet.[ReturnedQuantity]) [return quantity] from [UINV_GoodsReturn] gret inner join [UINV_GoodsReturnDetails] gretdet on gret.id = gretdet.[GoodsReturnId]
                            where gret.[GoodsReceivedId] =" + receiveId;
            IQuery iQuery = Session.CreateSQLQuery(queryStr);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function

        private string GetCommonSqlQuery(List<long> branchIdList, string item, DateTime? goodsReturnDateValue, int? returnStatus)
        {
            string whereQuery = "";
            if (branchIdList != null && branchIdList.Count > 0)
            {
                whereQuery += " and b.id in(" + string.Join(", ", branchIdList) + ")";
            }
            if (!string.IsNullOrEmpty(item))
            {
                whereQuery += " and i.Name Like '%" + item + "%'";
            }
            if (goodsReturnDateValue != null)
            {
                whereQuery += " and grd.CreationDate>='" + goodsReturnDateValue.Value.Date + "' and grd.CreationDate<'" + goodsReturnDateValue.Value.AddDays(1).Date + "'";
            }
            if (returnStatus != null)
            {
                whereQuery += "and grd.ReturnStatus=" + returnStatus;
            }
            string query =
                    @"select grd.Id 'GoodsReturnDetailsId',o.ShortName 'OrganizationShortName'
                    ,b.Name 'Branch',i.Name 'Item',grec.CreationDate 'GoodsReceiveDate',grec.GoodsReceivedNo 'GoodsReceiveNo'
                    ,grd.CreationDate 'GoodsReturnDate',gret.GoodsReturnNo 'GoodsReturnNo',
                     case when grd.ReturnStatus = "
                    + (int)GoodsReturnStatus.PartiallyReturned
                    + " then '" + GetEnumDescription.Description(GoodsReturnStatus.PartiallyReturned) + "'"
                    + @" when grd.ReturnStatus= " + (int)GoodsReturnStatus.Returned
                    + @" then '" + GetEnumDescription.Description(GoodsReturnStatus.Returned) + "'"
                    + @" end 'Status'
                    from UINV_GoodsReturn gret 
                    inner join UINV_GoodsReturnDetails grd on gret.Id=grd.GoodsReturnId
                    inner join UINV_GoodsReceive grec on grec.Id=gret.GoodsReceivedId
                    inner join Branch b on grec.BranchId=b.Id
                    inner join Organization o on o.Id=b.OrganizationId
                    inner join UINV_Item i on grd.ItemId=i.Id where 1=1";
            return query + whereQuery;
        }

        public string GenerateSeqeuntialNo(long branchId)
        {
            string goodsReturnNo = "";
            int count = Session.Query<GoodsReturn>().Count(x => x.GoodsReceive.Branch.Id == branchId);
            if (count == 0)
            {
                goodsReturnNo = "000001";
            }
            else
            {
                var goodsReturn = Session.Query<GoodsReturn>().Where(x => x.GoodsReceive.Branch.Id == branchId).OrderByDescending(x => x.Id).FirstOrDefault();
                if (goodsReturn != null)
                {
                    var lastGoodsReturnNo = Convert.ToInt32(goodsReturn.GoodsReturnNo.Substring(goodsReturn.GoodsReturnNo.Length - 6));
                    goodsReturnNo = (lastGoodsReturnNo + 1).ToString();
                }
            }

            return goodsReturnNo;
        }

        private static string GetGoodsReturnReportCommonQuery(List<long> authBranchIdList, List<long> purposeList,
            List<long> itemGroupList,
            List<long> itemList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList,
            DateTime dateFrom, DateTime dateTo, List<string> columnNameList, int reportType, List<string> programsessionList)
        {
            string query = "";
            string whereString = "";
            string supplierQuery = "";
            if (authBranchIdList != null && authBranchIdList.Any() &&
                !authBranchIdList.Contains(SelectionType.SelelectAll))
            {
                whereString += " and b.id in (" + string.Join(", ", authBranchIdList) + ")";
            }
            if (purposeList != null && purposeList.Any() && !purposeList.Contains(SelectionType.SelelectAll))
            {
                //whereString += " and grecd.purpose in (" + string.Join(", ", purposeList) + ")";
                if (purposeList.Count == 1 && purposeList.Contains(-1))
                {
                    whereString += " and grecd.purpose is null";
                }
                else if (purposeList.Count > 1 && purposeList.Contains(-1))
                {
                    whereString += " and (grecd.purpose in( " + string.Join(", ", purposeList) + ") or grecd.purpose is null )";
                }
                else
                {
                    whereString += " and (grecd.purpose in( " + string.Join(", ", purposeList) + "))";
                }
            }
            if (itemGroupList != null && itemGroupList.Any() && !itemGroupList.Contains(SelectionType.SelelectAll))
            {
                whereString += " and ig.id in (" + string.Join(", ", itemGroupList) + ")";
            }
            if (itemList != null && itemList.Any() && !itemList.Contains(SelectionType.SelelectAll))
            {
                whereString += " and i.id in (" + string.Join(", ", itemList) + ")";
            }

            if (reportType == 1)
            {
                if (programIdList != null && programIdList.Count > 0 && !programIdList.Contains(SelectionType.SelelectAll))
                {
                    if (programIdList.Count == 1 && programIdList.Contains(-1))
                    {
                        whereString += " and grecd.programId is null";
                    }
                    else if (programIdList.Count > 1 && programIdList.Contains(-1))
                    {
                        whereString += " and (grecd.programId in( " + string.Join(", ", programIdList) + ") or grecd.programId is null )";
                    }
                    else
                    {
                        whereString += " and (grecd.programId in( " + string.Join(", ", programIdList) + "))";
                    }
                    // whereString += " and grecd.programId in (" + string.Join(", ", programIdList) + ")";
                }
                if (sessionIdList != null && sessionIdList.Count > 0 && !sessionIdList.Contains(SelectionType.SelelectAll))
                {
                    if (sessionIdList.Count == 1 && sessionIdList.Contains(-1))
                    {
                        whereString += " and grecd.sessionId is null";
                    }
                    else if (sessionIdList.Count > 1 && sessionIdList.Contains(-1))
                    {
                        whereString += " and (grecd.sessionId in( " + string.Join(", ", sessionIdList) + ") or grecd.sessionId is null )";
                    }
                    else
                    {
                        whereString += " and (grecd.sessionId in( " + string.Join(", ", sessionIdList) + "))";
                    }
                    // whereString += " and grecd.sessionId in (" + string.Join(", ", sessionIdList) + ")";
                }

            }
            else if (reportType == 2)
            {
                var programsessions = new List<string>();
                programsessions.AddRange(programsessionList);
                programsessions.Remove("0");
                programsessions.Remove("-1");
                programsessions.Remove("");
                if (programsessions.Any())
                {
                    whereString += " and(";
                    foreach (var programsession in programsessions)
                    {
                        var programId = programsession.Split(new[] { "::" }, StringSplitOptions.None)[0];
                        var sessionId = programsession.Split(new[] { "::" }, StringSplitOptions.None)[1];
                        whereString += "(p.Id=" + programId + " and s.Id=" + sessionId + ") or ";
                    }
                    whereString = whereString.Trim(' ', 'o', 'r');
                    whereString += ")";
                }

            }

            whereString += " and gr.CreationDate>='" + dateFrom.Date + "'";
            whereString += " and gr.CreationDate<'" + dateTo.AddDays(1).Date + "'";
            if (supplierIdList != null && supplierIdList.Any() && !supplierIdList.Contains(SelectionType.SelelectAll))
            {
                supplierQuery += "  inner join UINV_SupplierItem si on si.itemid=i.id and si.supplierid in (" +
                                 string.Join(", ", supplierIdList) + ")";
            }
            string pivotString = "";
            if (columnNameList != null && columnNameList.Any())
            {
                foreach (var columnName in columnNameList)
                {
                    pivotString += "[" + columnName + "],";
                }
            }
            pivotString = pivotString.Trim(' ', ',');
            if (reportType == 1)
            {
                query = @"select piv.* from(select distinct ig.Name itemGroup
                                    ,i.Name item, isnull(Program.ShortName + session.Name,'N/A') as ProgramSession
                                    ,b.Name branch, isnull(gr.ReturnedQuantity,0) quantity
                                    from 
                                    UINV_GoodsReturn gr inner join UINV_GoodsReturnDetails grd on gr.Id =grd.GoodsReturnId
                                    inner join UINV_GoodsReceive grec on grec.Id=gr.GoodsReceivedId
                                    inner join UINV_GoodsReceiveDetails grecd on grec.Id=grecd.GoodsRecieveId
                                    left join program on grecd.ProgramId = Program.Id
                                    left join Session on grecd.SessionId = Session.Id
                                    left join UINV_Item i on grd.ItemId = i.Id
                                    left join UINV_ItemGroup ig on ig.Id=i.ItemGroupId
                                    right join Branch b on b.Id=grec.BranchId " + supplierQuery + whereString +
                                    @")src 
                                    PIVOT(SUM(quantity)
                                    FOR [branch] IN( " + pivotString + @" )) as piv
                                    where item is not null";
            }
            else if (reportType == 2)
            {
                query = @"select * from(select distinct ig.Name itemGroup,i.Name item
                                                ,isnull(grd.ReturnedQuantity,0) quantity,isnull(p.ShortName+' '+s.Name,'N/A') programsession
                                                from UINV_GoodsReturn gr inner join UINV_GoodsReturnDetails grd on gr.Id =grd.GoodsReturnId
                                                inner join UINV_GoodsReceive grec on grec.Id=gr.GoodsReceivedId
                                                inner join UINV_GoodsReceiveDetails grecd on grec.Id=grecd.GoodsRecieveId
                                                left join UINV_Item i on i.Id=grd.ItemId
                                                left join UINV_ItemGroup ig on i.itemgroupid=ig.id                                                 
                                                left join Branch b on b.Id=grec.BranchId                                                
                                                right join Program p on p.Id=grecd.ProgramId
                                                right join Session s on s.Id=grecd.SessionId
                                                
                            " + supplierQuery + whereString + ") src" +
                        @" pivot
                            (
                            sum(quantity)
                            for programsession in(" + pivotString + ",[N/A]" + ")" +
                        ")piv where item is not null";
            }
            return query;
        }


        public IList<string> LoadGoodsReturnBranchNameList(List<long> authBranchIdList)
        {
            ICriteria criteria = Session.CreateCriteria<GoodsReturn>();
            criteria.CreateAlias("GoodsReceive", "gr").Add(Restrictions.Eq("gr.Status", GoodsReceive.EntityStatus.Active));
            criteria.CreateAlias("gr.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            criteria.Add(Restrictions.In("br.Id", authBranchIdList));
            criteria.SetProjection(Projections.ProjectionList().Add(Projections.Property("br.Name")));
            IList<string> branchNames = criteria.List<string>();
            return branchNames.Distinct().ToList();
        }

        public long GetGoodsReturnReportCount(List<long> authBranchIdList, List<long> purposeList, List<long> itemGroupList, List<long> itemList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, DateTime dateFrom, DateTime dateTo, List<string> columnList, int reportType, List<string> programsession)
        {
            var query = "Select Count(*) from ( " + GetGoodsReturnReportCommonQuery(authBranchIdList, purposeList, itemGroupList, itemList, programIdList, sessionIdList, supplierIdList, dateFrom, dateTo, columnList, reportType, programsession) + " )x";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public IList<dynamic> DynamicList(IQuery query)
        {
            var result = query.SetResultTransformer(ExpandoObject)
                        .List<dynamic>();
            return result;
        }
        #endregion
    }
}
