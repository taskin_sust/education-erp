using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{


    public partial class MemberLoanApplicationMap : BaseClassMap<MemberLoanApplication,long>
    {

        public MemberLoanApplicationMap()
        {
			Table("HR_MemberLoanApplication");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.RequestedAmount).Column("RequestedAmount").Not.Nullable();
			Map(x => x.LoanStatus).Column("LoanStatus").Not.Nullable();
			Map(x => x.Remarks).Column("Remarks");
            HasMany(x => x.MemberLoan).KeyColumn("MemberLoanApplicationId").Cascade.AllDeleteOrphan().Inverse(); ;
            HasMany(x => x.MemberLoanCsr).KeyColumn("MemberLoanApplicationId").Cascade.AllDeleteOrphan().Inverse(); ;
            HasMany(x => x.MemberLoanZakat).KeyColumn("MemberLoanApplicationId").Cascade.AllDeleteOrphan().Inverse(); ;
        }
    }
}
