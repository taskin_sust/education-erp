﻿
$(document).ready(function () {


    //function validateReadOnlyTextField(controlId, errorMessage) {

    //    $(document).on("change", "#" + controlId, function (event) {

    //        var value = $("#" + controlId).val().trim();
    //        if (value.length == 0) {
    //            showErrorMessageBelowCtrl(controlId, "", false);
    //            showErrorMessageBelowCtrl(controlId, errorMessage, true);
    //            return false;
    //        } else {
    //            showErrorMessageBelowCtrl(controlId, "", false);
    //        }
    //    });
    //}

    function addOperation(e,button) {
        $('.field-validation-error').remove();
        var organizationId = $('#Organization').val();
        console.log(organizationId);
        var name = $('#Name').val();
        var startTime = $('#StartTime').val();
        var endTime = $('#EndTime').val();
        var isSuccess = true;
        var customError = false;
        if (organizationId == null || organizationId <= 0) {
            window.showErrorMessageBelowCtrl("Organization", "", false);
            window.showErrorMessageBelowCtrl("Organization", "Please Select Organization.", true);
            isSuccess = false;
        }
        if (startTime == null || startTime.length <= 0) {
            window.showErrorMessageBelowCtrl("StartTime", "", false);
            window.showErrorMessageBelowCtrl("StartTime", "Please select Start Time.", true);
            isSuccess = false;
        }
        if (endTime == null || endTime.length <= 0) {
            window.showErrorMessageBelowCtrl("EndTime", "", false);
            window.showErrorMessageBelowCtrl("EndTime", "Please select End Time.", true);
            isSuccess = false;
        }
        if (name == null || name.length <= 0) {
            window.showErrorMessageBelowCtrl("Name", "", false);
            window.showErrorMessageBelowCtrl("Name", "Please enter valid Shift Name.", true);
            isSuccess = false;
        }
        if (isSuccess == true) {
            var postData = $("#CreateShift").serializeArray();
            $.ajax({
                url: $("body").attr("data-project-root") + "Hr/ShiftSetting/Create",
                type: "POST",
                data: postData,
                //data: {
                //    Name: name, Organization: organizationId, StartTime: startTime, EndTime  : endTime
                //},
                success: function (data) {
                    if (data.IsSuccess) {
                        if (button == 1 && customError == false) {
                            window.location = $("body").attr("data-project-root") + "Hr/ShiftSetting/ManageShiftSetting?message=" + data.Message;
                        } else {
                                $('#Name').val("");
                                $('#StartTime').val("");
                                $('#EndTime').val("");
                                var organizationIdSelect = $('#Organization');
                                organizationIdSelect[0].selectedIndex = 0;
                            $.fn.customMessage({
                                displayMessage: data.Message,
                                displayMessageType: "s",
                                //displayTime: 5000
                            });
                        }

                    } else {
                        $.fn.customMessage({
                            //displayMessage: ERROR_MESS,
                            displayMessage: data.Message,
                            displayMessageType: "e",
                            //displayTime: 5000
                        });
                    }
                },
                error: function () {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
            e.preventDefault();
        } else {
            e.preventDefault();
        }
    }

    $(document).on('click', '#saveAndExit', function (e) {
        addOperation(e,1);
    });
    $(document).on('click', '#saveAndNew', function (e) {
        addOperation(e, 2);
    });

    $(document).on('click', '#update', function (e) {
        $('.field-validation-error').remove();
        var organizationId = $('#Organization').val();
        var id = $('#Id').val();
        console.log(id);
        var name = $('#Name').val();
        var startTime = $('#StartTime').val();
        var endTime = $('#EndTime').val();
        var status = $('#Status').val();
        var isSuccess = true;
        var customError = false;
        if (organizationId == null || organizationId <= 0) {
            window.showErrorMessageBelowCtrl("Organization", "", false);
            window.showErrorMessageBelowCtrl("Organization", "Please Select Organization.", true);
            isSuccess = false;
        }
        if (startTime == null || startTime.length <= 0) {
            window.showErrorMessageBelowCtrl("StartTime", "", false);
            window.showErrorMessageBelowCtrl("StartTime", "Please select Start Time.", true);
            isSuccess = false;
        }
        if (endTime == null || endTime.length <= 0) {
            window.showErrorMessageBelowCtrl("EndTime", "", false);
            window.showErrorMessageBelowCtrl("EndTime", "Please select End Time.", true);
            isSuccess = false;
        }
        if (name == null || name.length <= 0) {
            window.showErrorMessageBelowCtrl("Name", "", false);
            window.showErrorMessageBelowCtrl("Name", "Please enter valid Shift Name.", true);
            isSuccess = false;
        }
        if (isSuccess == true) {
            var postData = $("#UpdateShift").serializeArray();
            console.log(postData);
            $.ajax({
                url: $("body").attr("data-project-root") + "Hr/ShiftSetting/Edit",
                type: "POST",
                data: postData,
                //data: {
                //    id:id, name: name, organizationId: organizationId, startTime: startTime, endTime: endTime, status:status
                //},
                success: function (data) {
                    if (data.IsSuccess) {
                        //if (button == 1 && customError == false) {
                        //    window.location = $("body").attr("data-project-root") + "HR/ShiftSetting/ManageShiftSetting?message=" + data.Message;
                        //} else {
                            $.fn.customMessage({
                                displayMessage: data.Message,
                                displayMessageType: "s",
                                //displayTime: 5000
                            });
                        //}

                    } else {
                        $.fn.customMessage({
                            //displayMessage: ERROR_MESS,
                            displayMessage: data.Message,
                            displayMessageType: "e",
                            //displayTime: 5000
                        });
                    }
                },
                error: function () {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
            e.preventDefault();
        } else {
            e.preventDefault();
        }
    });

   
});