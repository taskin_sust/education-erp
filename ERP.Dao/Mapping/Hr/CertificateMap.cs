using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class CertificateMap : BaseClassMap<Certificate,long> {
        
        public CertificateMap() {
			Table("HR_Certificate");
			LazyLoad();
			References(x => x.AcademicInfo).Column("AcademicInfoId");
			References(x => x.TrainingInfo).Column("TrainingInfoId");
			References(x => x.JobExperience).Column("HRJobExperienceId");
			Map(x => x.Name).Column("Name");
			Map(x => x.CertificateType).Column("CertificateType");
			
        }
    }
}
