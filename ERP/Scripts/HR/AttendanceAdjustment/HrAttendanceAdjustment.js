﻿
$(document).ready(function () {

    showPendingForm();

    $(document).on("input propertychange", ".reasonForAdjustment", function (event) {
        $(this).removeClass("highlight");
    });

    $(document).on("focus", "#recentDate", function () {
        var today = new Date();
        var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();
        var startDate = (today.getFullYear() - 1).toString() + "-" + (today.getMonth() + 1) + "-" + "01";
        var endDate = initialDate;
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: initialDate,
            startDate: startDate,
            endDate: endDate,
            startView: 2,
            minView: 2,
            maxView: 4
        }).on('changeDate', function (ev) {
            $(this).removeClass("highlight");
        });
    });

    $(document).on("focus", "input.datepicker", function () {
        var today = new Date();
        var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();

        var pastMonth = new Date(today.setMonth(today.getMonth() - 1, 1));
        var startDate = pastMonth.getFullYear().toString() + "-" + (pastMonth.getMonth() + 1) + "-" + "01";

        var endDate = initialDate;
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: initialDate,
            startDate: startDate,
            endDate: endDate,
            startView: 2,
            minView: 2,
            maxView: 4
        }).on('changeDate', function (ev) {
            $(this).removeClass("highlight");
        });
    });
    
    $(document).on("focus", "input.historyDate", function () {
        var today = new Date();
        var startDate = (today.getFullYear() - 1).toString() + "-" + (1) + "-" + "01";
        var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: today,
            startDate: startDate,
            endDate: lastDay,
            startView: 2,
            minView: 2,
            maxView: 4
        });
    });

    $(document).on("focus", "input.timeFrom", function () {
        $(this).datetimepicker({
            format: 'HH:ii p',
            showMeridian: true,
            startView: 1,
            pickDate: false,
            minuteStep: 5,
            autoclose: 1,
            timeOnly: true,
        }).on('changeDate', function (ev) {
            $(this).removeClass("highlight");
        });
    });

    $(document).on("focus", "input.timeTo", function () {
        $(this).datetimepicker({
            format: 'HH:ii p',
            showMeridian: true,
            startView: 1,
            pickDate: false,
            minuteStep: 5,
            autoclose: 1,
            timeOnly: true,
        }).on('changeDate', function (ev) {
            $(this).removeClass("highlight");
        });
    });

    $(document).on("click", ".hrRecentHistoryTab, #showFormHistory", function () {
        var dateFrom = $("#dateFrom").val();
        var dateTo = $("#dateTo").val();
        if (dateFrom > dateTo) {

            $.fn.customMessage({
                displayMessage: "From Datetime must be less than or equal to To Datetime",
                displayMessageType: "error",
            });
        }
        else {
            dataTableRender();
        }
    });

    $(document).on("click", ".hrPendingListTab, #showForm", function () {
        showPendingForm();
    });

    $(document).on("blur", ".memberPinClass", function (event) {
        var _this = $(this);
        var memberPin = parseInt(_this.val().trim());
        var adjDate = _this.parent().prev('td').find('input.datepicker').val();
        if (memberPin !== "" && adjDate !== "" && adjDate.length > 0) {
            if (memberPin > 0) {
                $.ajax({
                    type: "POST",
                    //url: $("body").attr("data-project-root") + "HR/AttendanceAdjustment/AjaxRequestHrForMemberInformation",
                    url: $("body").attr("data-project-root") + "HR/CommonAjaxHr/GetTeamMemberInformation",
                    cache: false,
                    async: true,
                    data: { pin: memberPin, searchingDate: adjDate, isDepartment: true, isOrganization: true, isBranch: true, isCampus: true },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (result) {
                        $.unblockUI();
                        if (result.IsSuccess) {
                            _this.prev('input.employeeId').val(result.teamMemberId);
                            _this.parent().next().find('input.employeeName').val(result.teamMemberName);
                            _this.parent().parent().find('input.employeedept').val(result.departmentName);
                            _this.parent().parent().find('input.employeeOrg').val(result.organizationName);
                            _this.parent().parent().find('input.employeeBr').val(result.branchName);
                            _this.parent().parent().find('input.employeeCam').val(result.campusName);
                        }
                        else {
                            //_this.val("");
                            _this.prev('input.employeeId').val(0);
                            _this.parent().next().find('input.employeeName').val("");
                            _this.parent().parent().find('input.employeedept').val("");
                            _this.parent().parent().find('input.employeeOrg').val("");
                            _this.parent().parent().find('input.employeeBr').val("");
                            _this.parent().parent().find('input.employeeCam').val("");
                        }
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function () {
                        $.unblockUI();
                    }
                });
            }
        }
    });

    var itemCount = $(".totalCount").val();
    var output = "";
    $(document).on("click", ".dynamicRowGenerate", function (event) {
        var html = "";
        // dynamically create rows in the table
        html = "<tr id='tr" + itemCount + "' >" +
            "<td><input type='text' name='date" + itemCount + "' id='date_" + itemCount + "'  class='form-control text-box single-line datepicker' placeholder='Date' readonly='readonly'/></td>" +
            "<td><input type='hidden' id='employeeId" + itemCount + "' value='' class='employeeId'/><input type='text' name='pinName" + itemCount + "' id='pinName_" + itemCount + "' class='form-control text-box single-line autoComplete memberPinClass' value='' rowindex='" + itemCount + "'/></td>" +
            "<td><input type='text' name='employeeName" + itemCount + "' id='employeeName_" + itemCount + "'  value='' class='employeeName form-control text-box single-line' readonly='readonly'/></td>" +
            "<td><input type='text' name='employeeDept" + itemCount + "' id='employeeDept_" + itemCount + "'  value='' class='employeedept form-control text-box single-line' readonly='readonly'/></td>" +
            "<td><input type='text' name='employeeOrg" + itemCount + "' id='employeeOrg_" + itemCount + "'  value='' class='employeeOrg form-control text-box single-line' readonly='readonly'/></td>" +
            "<td><input type='text' name='employeeBr" + itemCount + "' id='employeeBr_" + itemCount + "'  value='' class='employeeBr form-control text-box single-line' readonly='readonly'/></td>" +
            "<td><input type='text' name='employeeCam" + itemCount + "' id='employeeCam_" + itemCount + "'  value='' class='employeeCam form-control text-box single-line' readonly='readonly'/></td>" +
            "<td><input type='text' name='reasonforad" + itemCount + "' id='reasonforad_" + itemCount + "' class='form-control text-box single-line reasonForAdjustment' placeholder='Reason for Adjustment'> </td>" +
            "<td><input type='text' name='timefrom" + itemCount + "' id='timefrom_" + itemCount + "' class='form-control text-box single-line timeFrom' placeholder='Time From' readonly='readonly'/> </td>" +
            "<td><input type='text' name='timeto" + itemCount + "' id='timeto_" + itemCount + "' class='form-control text-box single-line timeTo' placeholder='Time To' readonly='readonly'/></td>" +
            "<td><a type='link' href='#' class='glyphicon glyphicon-ok hrapproveTeamMemberAAd'></a>&nbsp;&nbsp;<a href='#' id='" + itemCount + "' class='glyphicon glyphicon-remove'></a></td>" +
            "</tr>";
        $(".hrattendanceAdjustmentTableNew_table").append(html);
        $("#hrAttendanceAdjustmentTableList").tableHeadFixer({ "left": 3, "z-index": 2 });
        $("#" + itemCount).click(function () {
            var buttonId = $(this).attr("id");
            $("#tr" + buttonId).remove();
            setAttendanceAdjustmentRowCount();
        });
        itemCount++;
        $(".displayData").animate({ scrollTop: $('.displayData').prop("scrollHeight") }, 1000);
        setAttendanceAdjustmentRowCount();
    });

    $(document).on('change', '#Organization', function (event) {
        $('#Branch').empty();
        $('#Branch').append("<option value=''>Select Branch </option>");
        $('#Campus').empty();
        $('#Campus').append("<option value=''>Select Campus</option>");
        $('#Department').empty();
        $('#Department').append("<option value=''>Select Department</option>");
        var organizationId = $('#Organization').val();

        if (organizationId != 0) {
            $.ajax({
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                data: { organizationIds: organizationId, isAuthorized: true, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnBranchList, function (i, v) {
                            $('#Branch').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.ajax({
                        type: "POST",
                        url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadDepartment",
                        data: { organizationIds: organizationId },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (response) {
                            $.unblockUI();
                            if (response.IsSuccess) {
                                $.each(response.returndepartmentList, function (i, v) {
                                    $('#Department').append($('<option>').text(v.Text).attr('value', v.Value));
                                });
                            } else {
                                $.fn.customMessage({
                                    displayMessage: response.Message,
                                    displayMessageType: "error",
                                });
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.fn.customMessage({
                                displayMessage: ajaxError,
                                displayMessageType: "error",
                                displayTime: 5000
                            });
                        }
                    });
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    });

    $(document).on('change', '#Branch', function (event) {
        $('#Campus').empty();
        $('#Campus').append("<option value=''>Select Campus</option>");
        var organizationId = $('#Organization').val();
        var branchId = $('#Branch').val();

        if (branchId != 0) {
            $.ajax({
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                data: { organizationIds: organizationId, programIds: null, branchIds: branchId, sessionIds: null, isAuthorized: true, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnCampusList, function (i, v) {
                            $('#Campus').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    });

    $(document).on('change', '#OrganizationRecent', function (event) {

        $('#BranchRecent').empty();
        $('#BranchRecent').append("<option value=''>Select Branch </option>");
        $('#CampusRecent').empty();
        $('#CampusRecent').append("<option value=''>Select Campus</option>");
        $('#DepartmentRecent').empty();
        $('#DepartmentRecent').append("<option value=''>Select Department</option>");

        var organizationRecentId = $('#OrganizationRecent').val();

        if (organizationRecentId != 0) {
            $.ajax({
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                data: { organizationIds: organizationRecentId, isAuthorized: true, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnBranchList, function (i, v) {
                            $('#BranchRecent').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.ajax({
                        type: "POST",
                        url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadDepartment",
                        data: { organizationIds: organizationRecentId },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (response) {
                            $.unblockUI();
                            if (response.IsSuccess) {
                                $.each(response.returndepartmentList, function (i, v) {
                                    $('#DepartmentRecent').append($('<option>').text(v.Text).attr('value', v.Value));
                                });
                            } else {
                                $.fn.customMessage({
                                    displayMessage: response.Message,
                                    displayMessageType: "error",
                                });
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.fn.customMessage({
                                displayMessage: ajaxError,
                                displayMessageType: "error",
                                displayTime: 5000
                            });
                        }
                    });
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    });

    $(document).on('change', '#BranchRecent', function (event) {
        $('#CampusRecent').empty();
        $('#CampusRecent').append("<option value=''>Select Campus</option>");
        var organizationRecentId = $('#OrganizationRecent').val();
        var branchRecentId = $('#BranchRecent').val();

        if (branchRecentId !== 0) {
            $.ajax({
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                data: { organizationIds: organizationRecentId, programIds: null, branchIds: branchRecentId, sessionIds: null, isAuthorized: true, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnCampusList, function (i, v) {
                            $('#CampusRecent').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    });

    $("#hrAttendanceAdjustmentTableList .hrattendanceAdjustmentTableNew_table").on(function (event) {
        // alert("123");
        if (event.target.type == 'link') {
            //alert("ok");
            var target = $(event.target);
            var row = target.parent().parent();
            var checkRow = checkIndividualRowValidity(row);
            if (checkRow > 0) {
                // notValidRow(row);
                //  alert("error");
            } else {

                bootbox.confirm("<h3>Are you sure you want to approve attendance?</h3>", function (result) {
                    if (result) {
                        var hrAttendanceAdjustmentAllRowArray = [];
                        var hrAttendanceAdjustmentRowViewModel = {};
                        hrAttendanceAdjustmentRowViewModel["Id"] = row.find(".tableId").val();
                        hrAttendanceAdjustmentRowViewModel["MemberId"] = row.find(".employeeId").val();
                        hrAttendanceAdjustmentRowViewModel["Date"] = row.find(".datepicker").val();
                        hrAttendanceAdjustmentRowViewModel["ReasonForAdjustment"] = row.find(".reasonForAdjustment").val();
                        hrAttendanceAdjustmentRowViewModel["TimeFrom"] = row.find(".timeFrom").val();
                        hrAttendanceAdjustmentRowViewModel["TimeTo"] = row.find(".timeTo").val();
                        hrAttendanceAdjustmentAllRowArray.push(hrAttendanceAdjustmentRowViewModel);
                        //var data = JSON.stringify({ "MentorAttendanceAdjustments": mentorAttendanceAdjustmentAllRowArray });
                        var data = JSON.stringify({ "hrAttendanceAdjustments": hrAttendanceAdjustmentAllRowArray });
                        $.ajax({
                            type: "post",
                            //url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/MentorApproveAllAttendanceAdjustment",
                            url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/HrApproveAllAttendanceAdjustment",
                            cache: false,
                            async: true,
                            dataType: "json",
                            contentType: "application/json",
                            data: data,
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (result2) {
                                $.unblockUI();
                                if (result.IsSuccess) {
                                    $.fn.customMessage({
                                        displayTime: 120000,
                                        displayMessage: result2.returnSuccess,
                                        displayMessageType: "success",
                                    });
                                    window.location.reload(true);
                                } else {
                                    $.fn.customMessage({
                                        displayMessage: result.Message,
                                        displayMessageType: "error",
                                    });
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function () {
                                $.unblockUI();
                            }
                        });
                    }
                }).css({ 'margin-top': (($(window).height() / 4)) });

            }
        }
    });

    //$("#hrsubmitForm").click(function () {
    $(document).on("click","#hrsubmitForm",function (){
        var checkValidRow;
        var e = 0;
        var hrAttendanceAdjustmentAllRowArray = [];
        bootbox.confirm("<h3>Are you sure you want to Approve all adjustment?</h3>", function (confirmResult) {
            if (confirmResult) {
                if ($("#hrAttendanceAdjustmentTableList tbody tr").length > 50) {
                    e = e + 1;
                    $.fn.customMessage({
                        displayMessage: "You can approve maximum 50 attendance adjustment at a time. Please filter again.",
                        displayMessageType: "error",
                    });
                }

                $("#hrAttendanceAdjustmentTableList tbody tr").each(function () {

                    checkValidRow = checkIndividualRowValidity($(this));
                    if (checkValidRow > 0) {
                        e = e + 1;
                    } else {
                        var hrAttendanceAdjustmentRowViewModel = {};
                        hrAttendanceAdjustmentRowViewModel["Id"] = $(this).find(".tableId").val();
                        hrAttendanceAdjustmentRowViewModel["MemberId"] = $(this).find(".employeeId").val();
                        hrAttendanceAdjustmentRowViewModel["Date"] = $(this).find(".datepicker").val();
                        hrAttendanceAdjustmentRowViewModel["ReasonForAdjustment"] = $(this).find(".reasonForAdjustment").val();
                        hrAttendanceAdjustmentRowViewModel["TimeFrom"] = $(this).find(".timeFrom").val();
                        hrAttendanceAdjustmentRowViewModel["TimeTo"] = $(this).find(".timeTo").val();

                        var checkObject = containsObject(hrAttendanceAdjustmentRowViewModel, hrAttendanceAdjustmentAllRowArray);
                        if (checkObject === true) {
                            e = e + 1;
                            notValidRow($(this));
                        } else {
                            hrAttendanceAdjustmentAllRowArray.push(hrAttendanceAdjustmentRowViewModel);
                        }
                    }
                });

                if (e == 0) {
                    var data = JSON.stringify({ "hrAttendanceAdjustments": hrAttendanceAdjustmentAllRowArray });

                    $.ajax({
                        type: "post",
                        url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/HrApproveAllAttendanceAdjustment",
                        cache: false,
                        async: true,
                        dataType: "json",
                        contentType: "application/json",
                        data: data,
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (result) {
                            $.unblockUI();
                            if (result.IsSuccess) {
                                $.fn.customMessage({
                                    displayTime: 120000,
                                    displayMessage: result.returnSuccess,
                                    displayMessageType: "success",
                                });
                                window.setTimeout(function () { location.reload(); }, 1000);
                            }
                            else {
                                $.fn.customMessage({
                                    displayMessage: result.Message,
                                    displayMessageType: "error",
                                });
                            }
                            setAttendanceAdjustmentRowCount();
                        },
                        complete: function () {

                            $.unblockUI();
                        },
                        error: function (result) {
                            $.unblockUI();
                            console.log("Failed -----");
                        }
                    });

                } else {

                }
            }
        }).css({ 'margin-top': (($(window).height() / 4)) });

    });
    
    $(document).on("click", ".hrapproveTeamMemberAAd", function () {
        var row = $(this).parent().parent();
        var checkRow = checkIndividualRowValidity(row);
        if (checkRow > 0) {
            notValidRow(row);
        } else {
            bootbox.confirm("<h3>Are you sure you want to approve attendance?</h3>", function (result) {
                if (result) {
                    var hrAttendanceAdjustmentAllRowArray = [];
                    var hrAttendanceAdjustmentRowViewModel = {};
                    var trId = row.attr("id");
                    hrAttendanceAdjustmentRowViewModel["Id"] = row.find(".tableId").val();
                    hrAttendanceAdjustmentRowViewModel["MemberId"] = row.find(".employeeId").val();
                    hrAttendanceAdjustmentRowViewModel["Date"] = row.find(".datepicker").val();
                    hrAttendanceAdjustmentRowViewModel["ReasonForAdjustment"] = row.find(".reasonForAdjustment").val();
                    hrAttendanceAdjustmentRowViewModel["TimeFrom"] = row.find(".timeFrom").val();
                    hrAttendanceAdjustmentRowViewModel["TimeTo"] = row.find(".timeTo").val();
                    hrAttendanceAdjustmentAllRowArray.push(hrAttendanceAdjustmentRowViewModel);
                    var data = JSON.stringify({ "hrAttendanceAdjustments": hrAttendanceAdjustmentAllRowArray });
                    $.ajax({
                        type: "post",
                        //url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/MentorApproveAllAttendanceAdjustment",
                        url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/HrApproveAllAttendanceAdjustment",
                        cache: false,
                        async: true,
                        dataType: "json",
                        contentType: "application/json",
                        data: data,
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (result2) {
                            if (result2.IsSuccess) {
                                $.fn.customMessage({
                                    displayTime: 120000,
                                    displayMessage: result2.returnSuccess,
                                    displayMessageType: "success",
                                });
                                $("#hrAttendanceAdjustmentTableList tbody.hrattendanceAdjustmentTableNew_table tr#" + trId).remove();
                                //window.location.reload(true);
                            } else {
                                $.fn.customMessage({
                                    displayMessage: result2.Message,
                                    displayMessageType: "error",
                                });
                            }
                            $.unblockUI();
                            setAttendanceAdjustmentRowCount();
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function () {
                            $.unblockUI();
                            console.log("Failed -----");
                        }
                    });
                }
            }).css({ 'margin-top': (($(window).height() / 4)) });

        }

    });

    $(document).on("click", ".hrrejectTeammemberAAd", function () {
        var id = $(this).parent().parent().find('input.tableId').val();
        var trId = $(this).parent().parent().attr("id");
        bootbox.confirm("<h3>Are you sure you want to reject attendance?</h3>", function (result) {
            if (result) {
                $.ajax({
                    type: "post",
                    //url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/MentorRejectedAttendanceAdjustment",
                    url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/RejectAttendanceAdjustmentByHr",
                    cache: false,
                    async: true,
                    //dataType: "json",
                    //contentType: "application/json",
                    data: { "id": id },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (result2) {
                        if (result2.IsSuccess) {
                            $.fn.customMessage({
                                displayTime: 120000,
                                displayMessage: result2.returnSuccess,
                                displayMessageType: "success",
                            });
                            //window.location.reload(true);
                            $("#hrAttendanceAdjustmentTableList tbody.hrattendanceAdjustmentTableNew_table tr#" + trId).remove();
                        } else {
                            $.fn.customMessage({
                                displayMessage: result2.Message,
                                displayMessageType: "error",
                            });
                        }
                        $.unblockUI();
                        setAttendanceAdjustmentRowCount();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function () {
                        $.unblockUI();
                    }
                });
            }
        }).css({ 'margin-top': (($(window).height() / 4)) });
    });
    
    //$(".submitRejectForm").click(function () {
    $(document).on("click",".submitRejectForm",function (){
        var date = $(".datepicker").val();
        var startTime = $(".timeFrom").val();
        var endTime = $(".timeTo").val();
        var tableId = $(".tableId").val();
        var memberId = $(".memberId").val();
        var hrAttendanceAdjustmentViewModel = {};
        hrAttendanceAdjustmentViewModel["Id"] = tableId;
        hrAttendanceAdjustmentViewModel["MemberId"] = memberId;
        hrAttendanceAdjustmentViewModel["Date"] = date;
        hrAttendanceAdjustmentViewModel["ReasonForAdjustment"] = "";
        hrAttendanceAdjustmentViewModel["TimeFrom"] = startTime;
        hrAttendanceAdjustmentViewModel["TimeTo"] = endTime;
        console.log(hrAttendanceAdjustmentViewModel);
        var data = JSON.stringify({ "hrAttendanceAdjustmentViewModel": hrAttendanceAdjustmentViewModel });
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/RejectAttendanceAdjustmentViewModelByHr",
            cache: false,
            async: true,
            dataType: "json",
            contentType: "application/json",
            data: data,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                if (result.IsSuccess) {
                    $.fn.customMessage({
                       // displayTime: 120000,
                        displayMessage: result.Message,
                        displayMessageType: "success",
                    });
                } else {
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "error",
                    });
                }
                //window.location.reload(true);
                $.unblockUI();

            },
            complete: function () {

                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    });

    //$(".submitApproveForm").click(function () {
    $(document).on("click",".submitApproveForm",function (){
        var date = $(".datepicker").val();
        var startTime = $(".timeFrom").val();
        var endTime = $(".timeTo").val();
        var tableId = $(".tableId").val();
        var memberId = $(".memberId").val();
        var hrAttendanceAdjustmentViewModel = {};
        hrAttendanceAdjustmentViewModel["Id"] = tableId;
        hrAttendanceAdjustmentViewModel["MemberId"] = memberId;
        hrAttendanceAdjustmentViewModel["Date"] = date;
        hrAttendanceAdjustmentViewModel["ReasonForAdjustment"] = "";
        hrAttendanceAdjustmentViewModel["TimeFrom"] = startTime;
        hrAttendanceAdjustmentViewModel["TimeTo"] = endTime;
        console.log(hrAttendanceAdjustmentViewModel);
        var data = JSON.stringify({ "hrAttendanceAdjustmentViewModel": hrAttendanceAdjustmentViewModel });
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/ApproveAttendanceAdjustmentByHr",
            cache: false,
            async: true,
            dataType: "json",
            contentType: "application/json",
            data: data,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                if (result.IsSuccess) {
                    $.fn.customMessage({
                        displayTime: 120000,
                        displayMessage: result.returnSuccess,
                        displayMessageType: "success",
                    });
                } else {
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "error",
                    });
                }
                //window.location.reload(true);
                $.unblockUI();

            },
            complete: function () {

                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    });

});

function dataTableRender() {
    var pin = $("#pinRecent").val();
    var dateFrom = $("#dateFrom").val();
    var dateTo = $("#dateTo").val();
    var org = $("#OrganizationRecent").val();
    var branch = $("#BranchRecent").val();
    var campus = $("#CampusRecent").val();
    var department = $("#DepartmentRecent").val();

    var displayNo = 100;
    if ($('#DisplayNoRecent').val() != "" && /^\d+$/.test($('#DisplayNoRecent').val())) {
        displayNo = parseInt($('#DisplayNoRecent').val());
    } else {
        $('#DisplayNoRecent').val(displayNo);
    }
    $("#DataGrid").dataTable({
        destroy: true,
        "processing": true,
        searching: false,
        serverSide: true,
        "scrollX": true,
        "scrollCollapse": true,
        "scrollY": "60vh",
        "bLengthChange": false,
        "iDisplayLength": displayNo,
        "aoColumnDefs": [{ "bSortable": false, "aTargets": ['_all'] }],
        order: [[0, "Asc"]],
        fixedColumns:   {
            leftColumns: 3
        },
        ajax: {
            url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/HrAttendanceAdjustmentRecentHistory",
            type: 'POST',
            data: function (d) {
                d.pin = pin;
                d.dateFrom = dateFrom;
                d.dateTo = dateTo;
                d.org = org;
                d.branch = branch;
                d.campus = campus;
                d.department = department;
            },
            beforeSend: function () {
                //$.blockUI({
                //    timeout: 0,
                //    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                //});
            },
            complete: function () {},
            error: function (response) {
                ShowErrorMessage("data load error. Please try again.");
            }
        }
    });
}

function checkIndividualRowValidity(row) {
    var error = 0;
    var date = row.find(".datepicker").val();
    var reasonForAdjustment = row.find(".reasonForAdjustment").val();
    var timeFrom = row.find(".timeFrom").val();
    var timeTo = row.find(".timeTo").val();
    if (date == "") {
        error = error + 1;
        row.find(".datepicker").addClass("highlight");
    } else {
        row.find(".datepicker").removeClass("highlight");
    }
    if (reasonForAdjustment == "") {
        error = error + 1;
        row.find(".reasonForAdjustment").addClass("highlight");
    } else {
        row.find(".reasonForAdjustment").removeClass("highlight");
    }
    if (timeFrom == "") {
        error = error + 1;
        row.find(".timeFrom").addClass("highlight");
    } else {
        row.find(".timeFrom").removeClass("highlight");
    }
    if (timeTo == "") {
        error = error + 1;
        row.find(".timeTo").addClass("highlight");
    } else {
        row.find(".timeTo").removeClass("highlight");
    }
    if (error == 0) {
        row.removeClass("danger");
        row.addClass("info");
    } else {
        row.addClass("danger");
        row.removeClass("info");
    }
    // alert(error);
    return error;
}

function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (JSON.stringify(list[i]) === JSON.stringify(obj)) {
            return true;
        }
    }
    return false;
}

function notValidRow(row) {
    try {
        row.find(".datepicker").addClass("highlight");
        row.find(".reasonForAdjustment").addClass("highlight");
        row.find(".timeFrom").addClass("highlight");
        row.find(".timeTo").addClass("highlight");
        row.find(".timeTo").addClass("highlight");
        return 1;
    } catch (e) {
        return 0;
    }

}

function setAttendanceAdjustmentRowCount() {
    var attendanceAdjustmentCount = $('#hrAttendanceAdjustmentTableList >tbody >tr').length;
    $("#adjustmentCount").html(attendanceAdjustmentCount);
}

function showPendingForm() {
    var pin = $("#pin").val();
    var org = $("#Organization").val();
    var branch = $("#Branch").val();
    var campus = $("#Campus").val();
    var department = $("#Department").val();
    var date = $("#date").val();
    $.ajax({
        type: "post",
        url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/DisplayAttendanceAdjustmentForHr",
        cache: false,
        async: true,
        data: { "pin": pin, "org[]": org, "branch[]": branch, "campus[]": campus, "department[]": department, "date": date },
        beforeSend: function () {
            $.blockUI({
                timeout: 0,
                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
            });
        },
        success: function (result) {
            $.unblockUI();
            $(".displayData").empty();
            $(".displayData").html(result);
            setAttendanceAdjustmentRowCount();
        },
        complete: function () {
            $.unblockUI();
            $("#hrAttendanceAdjustmentTableList").tableHeadFixer({ "left": 3, "z-index": 2 });
        },
        error: function (result) {
            $.unblockUI();
        }
    });
}