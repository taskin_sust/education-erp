﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Teachers;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Teachers.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Teachers")]
    public class TeacherReportController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("TeachersArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly ITeacherService _teacherService;
        private readonly IUserService _userService;
        private readonly ITeacherClassEntryService _teacherClassEntryService;
        private readonly IProgramService _programService;
        private readonly IBranchService _branchService;
        private readonly ISessionService _sessionService;
        private readonly ITeacherPaymentService _teacherPaymentService;
        private List<UserMenu> _userMenu;

        public TeacherReportController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _organizationService = new OrganizationService(session);
                _teacherService = new TeacherService(session);
                _userService = new UserService(session);
                _teacherClassEntryService = new TeacherClassEntryService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _branchService = new BranchService(session);
                _teacherPaymentService = new TeacherPaymentService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        #endregion

        #region Index/Manage

        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region Class Report

        public ActionResult ClassReport()
        {
            ViewBag.OrganizationList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.ReportTypeList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.ReportTypeGroupByList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.PaymentStatusList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-01");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.NumberOfData = 10;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                
                SelectList organizationList =  new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName");
                List<SelectListItem> oList = organizationList.ToList();
                oList.Insert(0, new SelectListItem() { Value = "0", Text = "All Organization" });
                ViewBag.OrganizationList = new MultiSelectList(oList, "Value", "Text");

                ViewBag.ReportTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<TeacherClassReportType>(), "Key", "Value");
                ViewBag.ReportTypeGroupByList = new SelectList(_commonHelper.LoadEmumToDictionary<TeacherClassReportTypeGroupBy>(), "Key", "Value");
                ViewBag.PaymentStatusList = new SelectList(_commonHelper.LoadEmumToDictionary<TeacherClassReportPaymentStatus>(), "Key", "Value");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult ClassReport(TeacherClassPaymentFormViewModel teacherClassPayment)
        {
            List<string> fieldsStringList = new List<string>();
            ViewBag.OrganizationFullName = "";
            ViewBag.ReportWiseType = "";
            ViewBag.ReportType = "";
            ViewBag.ProgramSession = "";
            ViewBag.DateFromTo = "";
            try
            {
                if (teacherClassPayment.OrganizationId > 0)
                {
                    string programName = (teacherClassPayment.ProgramId != SelectionType.SelelectAll) ? _programService.GetProgram(teacherClassPayment.ProgramId).Name : "All Program ";
                    string sessionName = (teacherClassPayment.SessionId != SelectionType.SelelectAll) ? _sessionService.LoadById(teacherClassPayment.SessionId).Name : "All Session ";
                    ViewBag.OrganizationFullName = (teacherClassPayment.OrganizationId != SelectionType.SelelectAll) ? _organizationService.LoadById(teacherClassPayment.OrganizationId).Name : "All Organization";
                    ViewBag.ReportWiseType = _commonHelper.GetEmumIdToValue<TeacherClassReportTypeGroupBy>((int)teacherClassPayment.ReportTypeGroupBy);
                    ViewBag.ReportType = _commonHelper.GetEmumIdToValue<TeacherClassReportType>((int)teacherClassPayment.ReportType);
                    ViewBag.ProgramSession = programName + " " + sessionName;
                    ViewBag.DateFromTo = teacherClassPayment.DateFrom.ToString("dd MMM, yyyy") + " to " + teacherClassPayment.DateTo.ToString("dd MMM, yyyy");
                }
                if (teacherClassPayment.ReportTypeGroupBy == TeacherClassReportTypeGroupBy.BranchWise)
                {
                    fieldsStringList = TeacherClassPaymentReportConstant.GetBranchWiseReport(teacherClassPayment.ReportType);
                }
                else if (teacherClassPayment.ReportTypeGroupBy == TeacherClassReportTypeGroupBy.ProgramWise)
                {
                    fieldsStringList = TeacherClassPaymentReportConstant.GetProgramWiseReport(teacherClassPayment.ReportType);
                }
                else if (teacherClassPayment.ReportTypeGroupBy == TeacherClassReportTypeGroupBy.TeacherWise)
                {
                    fieldsStringList = TeacherClassPaymentReportConstant.GetTeacherWiseReport(teacherClassPayment.ReportType);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            ViewBag.ReportFields = fieldsStringList;
            return PartialView("Partial/_ClassReport");
        }

        public ActionResult ClassReportDataTable(int draw, int start, int length, TeacherClassPaymentFormViewModel teacherClassPayment)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            int recordsTotal = 0;
            int recordsFiltered = 0;
            var data = new List<object>();
            List<string> fieldsStringList = new List<string>();
            try
            {
                if (teacherClassPayment.ReportTypeGroupBy == TeacherClassReportTypeGroupBy.BranchWise)
                {
                    fieldsStringList = TeacherClassPaymentReportConstant.GetBranchWiseReport(teacherClassPayment.ReportType);
                }
                else if (teacherClassPayment.ReportTypeGroupBy == TeacherClassReportTypeGroupBy.ProgramWise)
                {
                    fieldsStringList = TeacherClassPaymentReportConstant.GetProgramWiseReport(teacherClassPayment.ReportType);
                }
                else if (teacherClassPayment.ReportTypeGroupBy == TeacherClassReportTypeGroupBy.TeacherWise)
                {
                    fieldsStringList = TeacherClassPaymentReportConstant.GetTeacherWiseReport(teacherClassPayment.ReportType);
                }
                length = teacherClassPayment.NoOfData;
                #region datatable

                #region OrderBy and Direction

                //string pinList = (!String.IsNullOrEmpty(teacherClassPayment.TpinList)) ? teacherClassPayment.TpinList.Trim() : "";
                //Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                //pinList = regex.Replace(pinList, @",");
                //teacherClassPayment.TpinList = pinList;

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];

                    switch (fieldsStringList[Convert.ToInt32(orderBy)])
                    {
                        case TeacherClassPaymentReportConstant.Tpin:
                            orderBy = " t.Tpin ";
                            break;
                        case TeacherClassPaymentReportConstant.Date:
                            orderBy = " a.HeldDate ";
                            break;
                        case TeacherClassPaymentReportConstant.TeacherName:
                            orderBy = " a.Name ";
                            break;
                        case TeacherClassPaymentReportConstant.TeacherBatch:
                            orderBy = " t.HscPassingYear ";
                            break;
                        case TeacherClassPaymentReportConstant.ProgramSession:
                            orderBy = " p.Name " + orderDir + " , s.Name ";
                            break;
                        case TeacherClassPaymentReportConstant.Branch:
                            orderBy = " b.Name ";
                            break;
                        case TeacherClassPaymentReportConstant.Campus:
                            orderBy = " c.Name ";
                            break;
                        case TeacherClassPaymentReportConstant.TotalClass:
                            orderBy = " a.TotalClass ";
                            break;
                        case TeacherClassPaymentReportConstant.TotalAmount:
                            orderBy = " a.TotalAmount ";
                            break;
                        default:
                            orderBy = " t.Tpin ";
                            break;
                    }
                }

                #endregion

                int rowCount = _teacherClassEntryService.GetClassPaymentReportListCount(_userMenu, teacherClassPayment);
                List<TeacherClassPaymentReport> teacherClassPaymentList = _teacherClassEntryService.LoadClassPaymentReportList(_userMenu, start, orderBy, orderDir, teacherClassPayment);
                recordsTotal = rowCount;
                recordsFiltered = recordsTotal;
                int grandTotalClass = 0;
                decimal grandTotalAmount = 0;

                //IList<TeacherClassPaymentReport> teacherClassPaymentList = new List<TeacherClassPaymentReport>();

                int sl = start;
                foreach (var teacherClass in teacherClassPaymentList)
                {
                    sl++;
                    List<string> str = new List<string>();//new List<string> { sl.ToString() };
                    grandTotalClass += teacherClass.TotalClass ?? 0;
                    grandTotalAmount += teacherClass.TotalAmount ?? 0;
                    #region datatable

                    if (fieldsStringList != null && fieldsStringList.Any())
                    {
                        foreach (string field in fieldsStringList)
                        {
                            switch (field)
                            {
                                case TeacherClassPaymentReportConstant.Date:
                                    str.Add(teacherClass.Date);
                                    break;
                                case TeacherClassPaymentReportConstant.Tpin:
                                    str.Add(teacherClass.Tpin.ToString());
                                    break;
                                case TeacherClassPaymentReportConstant.TeacherName:
                                    str.Add(teacherClass.TeacherName);
                                    break;
                                case TeacherClassPaymentReportConstant.TeacherBatch:
                                    str.Add(teacherClass.TeacherBatch);
                                    break;
                                case TeacherClassPaymentReportConstant.Branch:
                                    str.Add(teacherClass.Branch);
                                    break;
                                case TeacherClassPaymentReportConstant.Campus:
                                    str.Add(teacherClass.Campus);
                                    break;
                                case TeacherClassPaymentReportConstant.ProgramSession:
                                    str.Add(teacherClass.ProgramSession);
                                    break;
                                case TeacherClassPaymentReportConstant.TotalClass:
                                    str.Add(teacherClass.TotalClass.ToString());
                                    break;
                                case TeacherClassPaymentReportConstant.TotalAmount:
                                    str.Add(teacherClass.TotalAmount.ToString());
                                    break;
                                default:
                                    break;
                            }

                        }

                    }

                    #endregion

                    data.Add(str);
                }
                if (fieldsStringList != null && fieldsStringList.Any() && rowCount > 0)
                {
                    List<string> str2 = new List<string>();
                    if (fieldsStringList.Count > 3)
                    {
                        for (int i = 0; i < fieldsStringList.Count - 3; i++)
                        {
                            str2.Add("");
                        }
                    }
                    str2.Add("Grand Total");
                    str2.Add(grandTotalClass.ToString());
                    str2.Add(grandTotalAmount.ToString());
                    data.Add(str2);
                }

                #endregion
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }

            return Json(new { draw = draw, recordsTotal = recordsTotal, recordsFiltered = recordsFiltered, start = start, length = length, data = data });
        }

        public ActionResult ExportClassReportDataTable(string teacherClassPaymentString)
        {
            var teacherClassPayment = new JavaScriptSerializer().Deserialize<TeacherClassPaymentFormViewModel>(teacherClassPaymentString);

            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            List<string> fieldsStringList = new List<string>();
            var classReportExcelList = new List<List<object>>();
            try
            {
                var headerList = new List<string>();
                if (teacherClassPayment.OrganizationId > 0)
                {
                    string programName = (teacherClassPayment.ProgramId != SelectionType.SelelectAll) ? _programService.GetProgram(teacherClassPayment.ProgramId).Name : "All Program ";
                    string sessionName = (teacherClassPayment.SessionId != SelectionType.SelelectAll) ? _sessionService.LoadById(teacherClassPayment.SessionId).Name : "All Session ";
                    string organizationFullName = (teacherClassPayment.OrganizationId != SelectionType.SelelectAll) ? _organizationService.LoadById(teacherClassPayment.OrganizationId).Name : "All Organization";
                    string reportWiseType = _commonHelper.GetEmumIdToValue<TeacherClassReportTypeGroupBy>((int)teacherClassPayment.ReportTypeGroupBy);
                    string reportType = _commonHelper.GetEmumIdToValue<TeacherClassReportType>((int)teacherClassPayment.ReportType);
                    string programSession = programName + " " + sessionName;
                    string dateFromTo = teacherClassPayment.DateFrom.ToString("dd MMM, yyyy") + " to " + teacherClassPayment.DateTo.ToString("dd MMM, yyyy");
                    headerList.Add(organizationFullName);
                    headerList.Add(reportWiseType);
                    headerList.Add(reportType);
                    headerList.Add(programSession);
                    headerList.Add(dateFromTo);
                    headerList.Add("");
                }
                if (teacherClassPayment.ReportTypeGroupBy == TeacherClassReportTypeGroupBy.BranchWise)
                {
                    fieldsStringList = TeacherClassPaymentReportConstant.GetBranchWiseReport(teacherClassPayment.ReportType);
                }
                else if (teacherClassPayment.ReportTypeGroupBy == TeacherClassReportTypeGroupBy.ProgramWise)
                {
                    fieldsStringList = TeacherClassPaymentReportConstant.GetProgramWiseReport(teacherClassPayment.ReportType);
                }
                else if (teacherClassPayment.ReportTypeGroupBy == TeacherClassReportTypeGroupBy.TeacherWise)
                {
                    fieldsStringList = TeacherClassPaymentReportConstant.GetTeacherWiseReport(teacherClassPayment.ReportType);
                }
                var columnList = new List<string>();
                if (fieldsStringList.Any())
                {
                    columnList.AddRange(fieldsStringList);
                }
                int start = 0;
                //int length = teacherClassPayment.NoOfData;
                #region datatable
                //string pinList = (!String.IsNullOrEmpty(teacherClassPayment.TpinList)) ? teacherClassPayment.TpinList.Trim() : "";
                //Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                //pinList = regex.Replace(pinList, @",");
                //teacherClassPayment.TpinList = pinList;
                int rowCount = _teacherClassEntryService.GetClassPaymentReportListCount(_userMenu, teacherClassPayment);
                teacherClassPayment.NoOfData = rowCount;
                string orderBy = " t.Tpin ";
                string orderDir = "ASC";
                List<TeacherClassPaymentReport> teacherClassPaymentList = _teacherClassEntryService.LoadClassPaymentReportList(_userMenu, start, orderBy, orderDir, teacherClassPayment);
                var footerList = new List<string>();
                footerList.Add("");
                int grandTotalClass = 0;
                decimal grandTotalAmount = 0;

                foreach (var teacherClass in teacherClassPaymentList)
                {
                    var xlsRow = new List<object>();
                    grandTotalClass += teacherClass.TotalClass ?? 0;
                    grandTotalAmount += teacherClass.TotalAmount ?? 0;

                    #region datatable

                    if (fieldsStringList != null && fieldsStringList.Any())
                    {
                        foreach (string field in fieldsStringList)
                        {
                            switch (field)
                            {
                                case TeacherClassPaymentReportConstant.Date:
                                    xlsRow.Add(teacherClass.Date);
                                    break;
                                case TeacherClassPaymentReportConstant.Tpin:
                                    xlsRow.Add(teacherClass.Tpin.ToString());
                                    break;
                                case TeacherClassPaymentReportConstant.TeacherName:
                                    xlsRow.Add(teacherClass.TeacherName);
                                    break;
                                case TeacherClassPaymentReportConstant.TeacherBatch:
                                    xlsRow.Add(teacherClass.TeacherBatch);
                                    break;
                                case TeacherClassPaymentReportConstant.Branch:
                                    xlsRow.Add(teacherClass.Branch);
                                    break;
                                case TeacherClassPaymentReportConstant.Campus:
                                    xlsRow.Add(teacherClass.Campus);
                                    break;
                                case TeacherClassPaymentReportConstant.ProgramSession:
                                    xlsRow.Add(teacherClass.ProgramSession);
                                    break;
                                case TeacherClassPaymentReportConstant.TotalClass:
                                    xlsRow.Add(teacherClass.TotalClass.ToString());
                                    break;
                                case TeacherClassPaymentReportConstant.TotalAmount:
                                    xlsRow.Add(teacherClass.TotalAmount.ToString());
                                    break;
                                default:
                                    break;
                            }

                        }

                    }
                    classReportExcelList.Add(xlsRow);

                    #endregion

                }
                if (fieldsStringList != null && fieldsStringList.Any() && teacherClassPaymentList.Any())
                {
                    var xlsRow2 = new List<object>();
                    if (fieldsStringList.Count > 3)
                    {
                        for (int i = 0; i < fieldsStringList.Count - 3; i++)
                        {
                            xlsRow2.Add("");
                        }
                    }
                    xlsRow2.Add("Grand Total");
                    xlsRow2.Add(grandTotalClass.ToString());
                    xlsRow2.Add(grandTotalAmount.ToString());
                    classReportExcelList.Add(xlsRow2);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, classReportExcelList, footerList, "teacher-class-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");

                #endregion
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }

        #endregion

        #region Seniorship Calculation Report

        public ActionResult SeniorshipCalculationReport()
        {
            ViewBag.OrganizationList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.TillDate = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.NumberOfData = 10;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult SeniorshipCalculationReport(TeacherSeniorshipCalculationReportFormViewModel teacherSeniorshipCalculation)
        {
            ViewBag.OrganizationFullName = "";
            ViewBag.TillDate = teacherSeniorshipCalculation.TillDate.ToString("dd MMM, yyyy");
            try
            {
                if (teacherSeniorshipCalculation.OrganizationId > 0)
                {
                    ViewBag.OrganizationFullName = _organizationService.LoadById(teacherSeniorshipCalculation.OrganizationId).Name;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return PartialView("Partial/_SeniorshipCalculationReport");
        }

        public ActionResult SeniorshipCalculationReportDataTable(int draw, int start, int length, TeacherSeniorshipCalculationReportFormViewModel teacherSeniorshipCalculation)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            int recordsTotal = 0;
            int recordsFiltered = 0;
            var data = new List<object>();
            try
            {
                length = teacherSeniorshipCalculation.NoOfData;

                #region datatable

                #region OrderBy and Direction

                string pinList = (!String.IsNullOrEmpty(teacherSeniorshipCalculation.TpinList)) ? teacherSeniorshipCalculation.TpinList.Trim() : "";
                Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                pinList = regex.Replace(pinList, @",");
                teacherSeniorshipCalculation.TpinList = pinList;
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                #region ordering

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];

                    switch (Convert.ToInt32(orderBy))
                    {
                        case 0:
                            orderBy = " t.TPin ";
                            break;
                         case 1:
                            orderBy = " TeacherName ";
                            break;
                        case 2:
                            orderBy = " TeacherBatch ";
                            break;
                        case 3:
                            orderBy = " TotalClass ";
                            break;
                        default:
                            orderBy = " TotalClass ";
                            break;
                    }
                }

                #endregion

                #endregion

                int rowCount = _teacherClassEntryService.GetSeniorshipCalculationReportListCount(_userMenu, teacherSeniorshipCalculation);
                List<TeacherSeniorshipCalculationReport> teacherSeniorshipCalculationReportList = _teacherClassEntryService.LoadSeniorshipCalculationReportList(_userMenu, start, orderBy, orderDir, teacherSeniorshipCalculation);
                recordsTotal = rowCount;
                recordsFiltered = recordsTotal;
                foreach (var teacherSeniorshipCalc in teacherSeniorshipCalculationReportList)
                {
                    List<string> str = new List<string>
                    {
                        teacherSeniorshipCalc.Tpin.ToString(),
                        teacherSeniorshipCalc.TeacherName,
                        teacherSeniorshipCalc.TeacherBatch,
                        teacherSeniorshipCalc.TotalClass.ToString(),
                        teacherSeniorshipCalc.Status
                    };

                    #region datatable


                    #endregion

                    data.Add(str);
                }

                #endregion
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }

            return Json(new { draw = draw, recordsTotal = recordsTotal, recordsFiltered = recordsFiltered, start = start, length = length, data = data });
        }

        public ActionResult ExportSeniorshipCalculationReportDataTable(string teacherSeniorshipCalculationString)
        {
            try
            {
                var teacherSeniorshipCalculation = new JavaScriptSerializer().Deserialize<TeacherSeniorshipCalculationReportFormViewModel>(teacherSeniorshipCalculationString);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var teacherSeniorshipCalculationExcelList = new List<List<object>>();
                var headerList = new List<string>();
                if (teacherSeniorshipCalculation.OrganizationId > 0)
                {
                    string organizationFullName = _organizationService.LoadById(teacherSeniorshipCalculation.OrganizationId).Name;
                    headerList.Add(organizationFullName);
                    headerList.Add("Teacher Senioship Calculation Report");
                    headerList.Add("Till Date: " + teacherSeniorshipCalculation.TillDate.ToString("dd MMM, yyyy"));
                    headerList.Add("");
                }
                var columnList = new List<string>();
                columnList.Add("TPIN");
                columnList.Add("Teacher Name");
                columnList.Add("Batch");
                columnList.Add("Total Class");
                columnList.Add("Status");
                int start = 0;
                string pinList = (!String.IsNullOrEmpty(teacherSeniorshipCalculation.TpinList)) ? teacherSeniorshipCalculation.TpinList.Trim() : "";
                Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                pinList = regex.Replace(pinList, @",");
                teacherSeniorshipCalculation.TpinList = pinList;
                int rowCount = _teacherClassEntryService.GetSeniorshipCalculationReportListCount(_userMenu, teacherSeniorshipCalculation);
                teacherSeniorshipCalculation.NoOfData = rowCount;
                string orderBy = " TotalClass ";
                string orderDir = " DESC";
                List<TeacherSeniorshipCalculationReport> teacherSeniorshipCalculationReportList = _teacherClassEntryService.LoadSeniorshipCalculationReportList(_userMenu, start, orderBy, orderDir, teacherSeniorshipCalculation);
                var footerList = new List<string>();
                footerList.Add("");
                foreach (var teacherSeniorshipCalc in teacherSeniorshipCalculationReportList)
                {
                    var xlsRow = new List<object>()
                    {
                        teacherSeniorshipCalc.Tpin.ToString(),
                        teacherSeniorshipCalc.TeacherName,
                        teacherSeniorshipCalc.TeacherBatch,
                        teacherSeniorshipCalc.TotalClass.ToString(),
                        teacherSeniorshipCalc.Status  
                    };
                    teacherSeniorshipCalculationExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, teacherSeniorshipCalculationExcelList, footerList, "teacher-seniorship-calculation-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"), false);
                return View("AutoClose");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }

        #endregion

        #region Teacher Payment Report

        public ActionResult PaymentReport()
        {
            ViewBag.OrganizationList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.ActivityTypeList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-01");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.Tpin = "";
            ViewBag.NumberOfData = 10;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                SelectList organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName");
                List<SelectListItem> oList = organizationList.ToList();
                oList.Insert(0, new SelectListItem() { Value = "0", Text = "All Organization" });
                ViewBag.OrganizationList = new MultiSelectList(oList, "Value", "Text");

                SelectList branchList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu).ToList(), "Id", "Name");
                List<SelectListItem> list = branchList.ToList();
                list.Insert(0, new SelectListItem() { Value = "0", Text = "All Payment Branch" });
                ViewBag.BranchList = new SelectList(list, "Value", "Text");

                ViewBag.ActivityTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<TeacherActivityEnum>(), "Key", "Value");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult PaymentReport(TeacherPaymentFormViewModel teacherPayment)
        {
            List<string> fieldsStringList = new List<string>();
            ViewBag.OrganizationFullName = "";
            ViewBag.Activity = "";
            ViewBag.BranchName = "";
            ViewBag.DateFromTo = "";
            try
            {
                if (teacherPayment.OrganizationId >= 0)
                {
                    ViewBag.BranchName = (teacherPayment.BranchId != SelectionType.SelelectAll) ? _branchService.GetBranch(teacherPayment.BranchId).Name : "All Branch ";
                    ViewBag.OrganizationFullName = (teacherPayment.OrganizationId != SelectionType.SelelectAll) ? _organizationService.LoadById(teacherPayment.OrganizationId).Name : "All Organization";
                    ViewBag.Activity = _commonHelper.GetEmumIdToValue<TeacherActivityEnum>((int)teacherPayment.ActivityId);
                    ViewBag.DateFromTo = teacherPayment.DateFrom.ToString("dd MMM, yyyy") + " to " + teacherPayment.DateTo.ToString("dd MMM, yyyy");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return PartialView("Partial/_PaymentReport");
        }

        public ActionResult PaymentReportDataTable(int draw, int start, int length, TeacherPaymentFormViewModel teacherPayment)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            int recordsTotal = 0;
            int recordsFiltered = 0;
            var data = new List<object>();
            List<string> fieldsStringList = new List<string>();
            try
            {
                length = teacherPayment.NoOfData;
                #region datatable

                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        //case "0":
                        //    orderBy = "Organization.Name";
                        //    break;
                        case "1":
                            orderBy = "PaidDate";
                            break;
                        case "2":
                            orderBy = "Tpin";
                            break;
                        case "3":
                            orderBy = "TeacherName";
                            break;
                        case "4":
                            orderBy = "PaidBranchName";
                            break;
                        //case "5":
                        //    orderBy = "Paid";
                        //    break;
                        case "6":
                            orderBy = "TotalAmount";
                            break;
                        case "7":
                            orderBy = "VoucherNo";
                            break;
                        default:
                            orderBy = "PaidDate";
                            break;
                    }
                }

                #endregion

                int rowCount = _teacherPaymentService.GetPaymentReportListCount(_userMenu, teacherPayment);
                List<TeacherPaymentReport> teacherPaymentList = _teacherPaymentService.LoadPaymentReportList(_userMenu, start, orderBy, orderDir, teacherPayment);
                recordsTotal = rowCount;
                recordsFiltered = recordsTotal;
                decimal grandTotalAmount = 0;

                int sl = start;
                foreach (var tPayment in teacherPaymentList)
                {

                    grandTotalAmount += tPayment.TotalAmount;
                    sl++;
                    List<string> str = new List<string> { sl.ToString() };

                    #region datatable

                    str.Add(tPayment.PaidDate.ToString("dd MMM, yyyy"));
                    str.Add(tPayment.Tpin.ToString());
                    str.Add(tPayment.TeacherName);
                    str.Add(tPayment.PaidBranchName);
                    str.Add(tPayment.PaidBy);
                    str.Add(tPayment.TotalAmount.ToString(CultureInfo.InvariantCulture));
                    str.Add("<a target='_blank' href='" + Url.Action("ViewTeacherPayment", "TeacherPayment") + "?voucherNo=" + tPayment.VoucherNo + "'  class=''>" + tPayment.VoucherNo + "</a>");
                    
                    #endregion

                    data.Add(str);
                }
                if (rowCount > 0)
                {
                    List<string> str2 = new List<string>();
                    for (int i = 0; i < 5; i++)
                    {
                        str2.Add("");
                    }
                    str2.Add("Grand Total");
                    str2.Add(grandTotalAmount.ToString());
                    str2.Add("");
                    data.Add(str2);
                }

                #endregion
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }

            return Json(new { draw = draw, recordsTotal = recordsTotal, recordsFiltered = recordsFiltered, start = start, length = length, data = data });
        }

        public ActionResult ExportPaymentReportDataTable(string teacherPaymentString)
        {
            var teacherPayment = new JavaScriptSerializer().Deserialize<TeacherPaymentFormViewModel>(teacherPaymentString);

            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var paymentReportExcelList = new List<List<object>>();
            try
            {
                var headerList = new List<string>();
                if (teacherPayment.OrganizationId >= 0)
                {
                    string branchName = (teacherPayment.BranchId != SelectionType.SelelectAll) ? _branchService.GetBranch(teacherPayment.BranchId).Name : "All Branch ";
                    string organizationFullName = (teacherPayment.OrganizationId != SelectionType.SelelectAll) ? _organizationService.LoadById(teacherPayment.OrganizationId).Name : "All Organization";
                    string activity = _commonHelper.GetEmumIdToValue<TeacherActivityEnum>((int)teacherPayment.ActivityId);
                    string dateFromTo = teacherPayment.DateFrom.ToString("dd MMM, yyyy") + " to " + teacherPayment.DateTo.ToString("dd MMM, yyyy");
                    headerList.Add(organizationFullName);
                    headerList.Add(branchName);
                    headerList.Add(activity);
                    headerList.Add(dateFromTo);
                    headerList.Add("");
                }
                var columnList = new List<string> {
                                    "Payment Date"
                                    , "Tpin"
                                    , "Teacher Name"
                                    , "Payment Branch"
                                    , "Paid By"
                                    , "Total Amount"
                                    , "Voucher No."
                                };
               
                int start = 0;

                #region datatable

                int rowCount = _teacherPaymentService.GetPaymentReportListCount(_userMenu, teacherPayment);
                teacherPayment.NoOfData = rowCount;
                string orderBy = " PaidDate ";
                string orderDir = "DESC";
                List<TeacherPaymentReport> teacherPaymentList = _teacherPaymentService.LoadPaymentReportList(_userMenu, start, orderBy, orderDir, teacherPayment);

                var footerList = new List<string>();
                footerList.Add("");
                decimal grandTotalAmount = 0;

                foreach (var tPayment in teacherPaymentList)
                {
                    var xlsRow = new List<object>();
                    grandTotalAmount += tPayment.TotalAmount;

                    #region datatable
                    xlsRow.Add(tPayment.PaidDate.ToString("dd MMM, yyyy"));
                    xlsRow.Add(tPayment.Tpin.ToString());
                    xlsRow.Add(tPayment.TeacherName);
                    xlsRow.Add(tPayment.PaidBranchName);
                    xlsRow.Add(tPayment.PaidBy);
                    xlsRow.Add(tPayment.TotalAmount.ToString(CultureInfo.InvariantCulture));
                    xlsRow.Add(tPayment.VoucherNo);

                    paymentReportExcelList.Add(xlsRow);

                    #endregion

                }
                if (teacherPaymentList.Any())
                {
                    var xlsRow2 = new List<object>();
                    for (int i = 0; i < 4; i++)
                    {
                        xlsRow2.Add("");
                    }
                    xlsRow2.Add("Grand Total");
                    xlsRow2.Add(grandTotalAmount.ToString());
                    xlsRow2.Add("");
                    paymentReportExcelList.Add(xlsRow2);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, paymentReportExcelList, footerList, "teacher-payment-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");

                #endregion
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }

        #endregion

        #region Details Operation
        #endregion

        #region Delete Operation
        #endregion

        #region Update Operation
        #endregion

    }
}