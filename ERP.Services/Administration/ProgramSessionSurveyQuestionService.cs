﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Administration
{
    public interface IProgramSessionSurveyQuestionService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<ProgramSessionSurveyQuestion> LoadProgramSessionSurveyQuestion(long programId, long sessionId, int toInt32, string[] selectedAnswer, long[] questionIds = null);
        IList<ProgramSessionSurveyQuestion> LoadProgramSessionSurveyQuestion(long sessionId, long programId, int surveyType);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class ProgramSessionSurveyQuestionService:BaseService,IProgramSessionSurveyQuestionService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IProgramSessionSurveyQuestionDao _programSessionSurveyQuestionDao;
        public ProgramSessionSurveyQuestionService(ISession session)
        {
            Session = session;
            
            _programSessionSurveyQuestionDao = new ProgramSessionSurveyQuestionDao() { Session = Session };
       
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<ProgramSessionSurveyQuestion> LoadProgramSessionSurveyQuestion(long sessionId, long programId, int surveyType)
        {
            try
            {
                return _programSessionSurveyQuestionDao.LoadSurveyQuestionByProgramSession(sessionId, programId, surveyType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<ProgramSessionSurveyQuestion> LoadProgramSessionSurveyQuestion(long programId, long sessionId, int surveyType, string[] selectedAnswer, long[] questionIds = null)
        {
            try
            {
                return _programSessionSurveyQuestionDao.LoadSurveyQuestionByProgramSessionAnswerIds(sessionId, programId, surveyType, selectedAnswer, questionIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
