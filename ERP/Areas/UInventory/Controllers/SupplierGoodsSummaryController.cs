﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentNHibernate.Testing.Values;
using log4net;
using NHibernate.Util;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("UInventory")]
    public class SupplierGoodsSummaryController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ISupplierService _supplierService;
        private readonly IOrganizationService _organizationService;
        private readonly IBranchService _branchService;
        private readonly ICommonHelper _commonHelper;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IItemService _itemService;
        private readonly IItemGroupService _itemGroupService;
        private readonly IGoodsReceiveService _goodsReceiveService;


        public SupplierGoodsSummaryController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _supplierService = new SupplierService(session);
                _organizationService = new OrganizationService(session);
                _branchService = new BranchService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _itemService = new ItemService(session);
                _itemGroupService = new ItemGroupService(session);
                _goodsReceiveService = new GoodsReceiveService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        public ActionResult Report()
        {
            try
            {
                InitialReportView();
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        [HttpPost]
        public ActionResult Report(SupplierGoodsSummaryReportForm model)
        {
            try
            {
                InitialReportHeaderDiv(model);
                return PartialView("_SupplierGoodsSummaryReport", model);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #region Ajax funtions

        [HttpPost]
        public ActionResult SupplierGoodsSummaryReport(int draw, int start, int length, long organizationId, long[] branchIds
                                                        , int[] purposeIds, long[] itemGroupIds, long[] itemIds, string[] programSession, long[] supplierIds
                                                        , DateTime dateFrom, DateTime dateTo)
        {
            int recordsFiltered = 0;
            int recordsTotal = 0;
            bool isSuccess = true;

            var getData = new List<object>();
            try
            {
                const string orderBy = "SupplierName";
                const string orderDir = "DESC";
                IList<SupplierGoodsSummaryReportDto> list = _goodsReceiveService.LoadSuppliersReceivedGoodsSummaryList(start, length, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu
                                                                                                                        , organizationId, branchIds, itemGroupIds, itemIds, supplierIds
                                                                                                                        , purposeIds, programSession, dateFrom, dateTo);
                recordsTotal = _goodsReceiveService.LoadSuppliersReceivedGoodsSummaryCount(start, length, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds
                                                                                            , itemGroupIds, itemIds, supplierIds, purposeIds, programSession, dateFrom, dateTo);
                recordsFiltered = recordsTotal;

                int sl = start + 1;
                foreach (var row in list)
                {

                    var str = new List<string>
                    {
                        sl.ToString(CultureInfo.CurrentCulture),
                        row.SupplierName,
                        row.ItemGroup,
                        row.ItemName,
                        row.ProgramSession,
                        row.Quantity.ToString(CultureInfo.InvariantCulture),
                        row.Rate.ToString(CultureInfo.InvariantCulture),
                        row.Total.ToString(CultureInfo.InvariantCulture),
                        row.GrandTotal.ToString(CultureInfo.InvariantCulture)
                    };
                    getData.Add(str);
                    sl++;
                }
            }
            catch (NullObjectException ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        //[HttpPost]
        public ActionResult SupplierGoodsSummaryExport(long organizationId, long[] branchIds, int[] purposeIds, long[] itemGroupIds, long[] itemIds
                                                        , string[] programSession, long[] supplierIds, DateTime dateFrom, DateTime dateTo
                                                        , string branchlbl, string purposelbl, string programSessionlbl, string supplierlbl, string itemlbl, string itemGrouplbl)
        {
            try
            {
                const string orderBy = "SupplierName";
                const string orderDir = "DESC";
                int recordsTotal = _goodsReceiveService.LoadSuppliersReceivedGoodsSummaryCount(0, 0, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu
                                                                                                , organizationId, branchIds, itemGroupIds, itemIds, supplierIds
                                                                                                , purposeIds, programSession, dateFrom, dateTo);
                if (recordsTotal < 1)
                {
                    return View("AutoClose");
                }
                IList<SupplierGoodsSummaryReportDto> list = _goodsReceiveService.LoadSuppliersReceivedGoodsSummaryList(0, recordsTotal, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu
                                                                                                                        , organizationId, branchIds, itemGroupIds, itemIds, supplierIds
                                                                                                                        , purposeIds, programSession, dateFrom, dateTo);
                var organization = _organizationService.LoadById(organizationId);
                var branchName = _branchService.LoadBranch(branchIds).Select(x => x.Name).ToList();
                var headerList = new List<string>()
                {
                    organization.Name,
                    string.Join(",", branchName),
                    "Supplier(s) Goods Summary Report",
                    "Purpose: " + purposelbl,
                    "Item Group(s): " + itemGrouplbl,
                    "Item(s): " + itemlbl,
                    "Program & Session: " + programSessionlbl,
                    "Supplier(s): " + supplierlbl, dateFrom.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture) 
                                    + " To " + dateTo.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture)
                };
                var footerList = new List<string>() { "" };

                var columnList = new List<string>()
                {
                    "Suppliers Name", "Item Group", "Item Name", "Program & Session", "Quantity", "Rate", "Total", "Grand Total"
                };
                var excelList = new List<List<object>>();
                foreach (var row in list)
                {
                    int index = 0;
                    var str = new List<object>
                    {
                        row.SupplierName,
                        row.ItemGroup,
                        row.ItemName,
                        row.ProgramSession,
                        row.Quantity.ToString(CultureInfo.InvariantCulture),
                        row.Rate.ToString(CultureInfo.InvariantCulture),
                        row.Total.ToString(CultureInfo.InvariantCulture),
                        row.GrandTotal.ToString(CultureInfo.InvariantCulture)
                    };
                    excelList.Add(str);
                }
                string fileName = "SupplierGoodsReport_" + DateTime.Now.ToString("yyyyMMdd-HHmmss");
                ExcelGenerator.GenerateExcel(headerList, columnList, excelList, footerList, fileName);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View("AutoClose");
        }

        #endregion

        #region Helper Function

        private void InitialReportView()
        {
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization((List<UserMenu>)ViewBag.UserMenu), "Id", "ShortName");
            ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
            var purposeList = (new Dictionary<int, string> { { 0, "Select All" }, { -1, "N/A" } }).Concat(_commonHelper.LoadEmumToDictionary<Purpose>()).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.PurposeList = new SelectList(purposeList.ToList().OrderBy(x => x.Key), "Key", "Value");
            ViewBag.ItemGroupList = new SelectList((new Dictionary<long, string> { { 0, "Select All" } })
                                                    .Concat(_itemGroupService.LoadItemGroup().ToDictionary(ig => ig.Id, ig => ig.Name))
                                                    .ToDictionary(k => k.Key, v => v.Value)
                                                  .OrderBy(x => x.Key).ToList(), "Key", "Value");
            ViewBag.ProgramSessionIdlist = new SelectList(string.Empty, "Value", "Text");
            ViewBag.Itemlist = new SelectList(string.Empty, "Value", "Text");
            ViewBag.Supplierlist = new SelectList((new Dictionary<long, string> { { 0, "Select All" } })
                                                    .Concat(_supplierService.LoadAll().ToDictionary(k => k.Id, v => v.Name)).ToDictionary(k => k.Key, v => v.Value)
                                                    , "Key", "Value");
        }


        private void InitialReportHeaderDiv(SupplierGoodsSummaryReportForm model)
        {
            #region Report Title

            const string reportTypeText = "Supplier(s) Goods Summary";

            var organization = _organizationService.LoadById(model.OrganizationId);
            var reportTitle = new string[3];
            reportTitle[0] = organization.Name;
            reportTitle[1] = reportTypeText;
            reportTitle[2] = model.DateFrom.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture) + " To " +
                             model.DateTo.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture);

            #endregion

            #region Program & Session List

            var programSession = "All";

            if (model.ProgramSession != null && model.ProgramSession[0] != "")
            {
                if (model.ProgramSession[0] == "0")
                {
                    programSession = "All";
                }
                else
                {
                    if (model.ProgramSession[0] == "-1")
                    {
                        programSession = "N/A,";
                    }
                    var psNamelist = GetProgramSessionNameList(model.ProgramSession);
                    programSession += string.Join(",", psNamelist);
                }
            }
            #endregion

            #region branch/item/item group

            var branchName = "All";
            if (model.BranchId != null)
            {
                branchName = model.BranchId[0] == 0 ? "All" : string.Join(", ",
                                                    _branchService.LoadAuthorizedBranchNameList(
                                                                    (List<UserMenu>)ViewBag.UserMenu
                                                                    , null, model.BranchId.ToList())
                                                                    .ToList());
            }

            var itemGroupString = "All";
            if (model.ItemGroupId != null)
            {
                itemGroupString = model.ItemGroupId[0] == 0 ? "All" : string.Join(", ", _itemGroupService.LoadItemGroup(model.ItemGroupId.ToList()).Select(x => x.Name).ToList());
            }

            var itemString = "All";
            if (model.ItemGroupId != null)
            {
                itemString = model.ItemGroupId[0] == 0 ? "All" : string.Join(", ", string.Join(", ", _itemService.LoadItemList(model.ItemId).Select(x => x.Name).ToList()));
            }
            #endregion

            #region Purpose

            var purpose = "All";
            if (model.PurposeId != null)
            {
                if (model.PurposeId[0] == 0)
                {
                    purpose = "All";
                }
                else
                {
                    purpose = model.PurposeId.Where(pId => pId > 0).Aggregate(purpose, (current, pId) => current + (GetEnumDescription.Description((Purpose)pId) + ","));
                    purpose = purpose.TrimEnd(',');
                    if (model.PurposeId.Contains(-1))
                    {
                        purpose = "N/A," + purpose;
                    }
                }
            }
            #endregion

            #region Suppliers

            var suppliers = "All";
            if (model.Supplier != null)
            {
                if (model.Supplier[0] <= 0)
                {
                    suppliers = "All";
                }
                else
                {
                    suppliers = string.Empty;
                    suppliers = model.Supplier.Aggregate(suppliers, (current, id) => current + (_supplierService.LoadById(id).Name + ", "));
                    suppliers = suppliers.Trim().TrimEnd(',');
                }
            }
            #endregion

            ViewBag.ReportTitle = reportTitle;
            ViewBag.ProgramSessionlbl = programSession;
            ViewBag.Purposelbl = purpose;
            ViewBag.Branchlbl = branchName;
            ViewBag.ItemGrouplbl = itemGroupString;
            ViewBag.Itemlbl = itemString;
            ViewBag.Supplierslbl = suppliers;
        }

        private IEnumerable<string> GetProgramSessionNameList(IEnumerable<string> programSession)
        {
            IList<string> programSessionNameList = new List<string>();

            foreach (var ps in programSession)
            {
                string[] psIds = ps.Split(new string[] {"::"}, StringSplitOptions.None);
                if (psIds.Length == 2)
                {
                    string programName = _programService.GetProgram(Convert.ToInt64(psIds[0])).ShortName;
                    string sessionName = _sessionService.LoadById(Convert.ToInt64(psIds[1])).Name;
                    if (!string.IsNullOrWhiteSpace(programName) && !string.IsNullOrWhiteSpace(sessionName))
                    {
                        programSessionNameList.Add(programName + " " + sessionName);
                    }
                }
            }
            return programSessionNameList;
        }

        #endregion
    }
}