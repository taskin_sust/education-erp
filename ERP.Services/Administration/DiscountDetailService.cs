﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Administration
{
    public interface IDiscountDetailService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<DiscountDetail> GetAllDiscountDetailByDiscountId(long disId);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }

    public class DiscountDetailService : BaseService, IDiscountDetailService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
           private readonly IDiscountDetailDao _discountDetailDao;
           public DiscountDetailService(ISession session)
        {
            Session = session;
         
            _discountDetailDao = new DiscountDetailDao() { Session = Session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<DiscountDetail> GetAllDiscountDetailByDiscountId(long disId)
        {
            IList<DiscountDetail> discountDetails;
            try
            {
                discountDetails = _discountDetailDao.GetByDiscount(disId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return discountDetails;
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
