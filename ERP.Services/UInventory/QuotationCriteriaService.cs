using log4net;
using NHibernate;
using UdvashERP.Services.Base;
namespace UdvashERP.Services.UInventory
{
    public interface IQuotationCriteriaService : IBaseService
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion
    }
    public class QuotationCriteriaService : BaseService, IQuotationCriteriaService 
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");

        #endregion

        #region Propertise & Object Initialization

        public QuotationCriteriaService(ISession session)
        {
            Session = session;
        }

        #endregion

        #region Operational Functions
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper function
        #endregion

    }
}
                                    
