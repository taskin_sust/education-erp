﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
namespace UdvashERP.Dao.Mapping.Administration
{
    public partial class OrganizationMap : BaseClassMap<Organization, long>
    {
        public OrganizationMap()
        {
            Table("Organization");
            LazyLoad();
            Map(x => x.Name);
            Map(x => x.Rank);
            Map(x => x.ShortName);
            Map(x => x.Website);
            Map(x => x.Email);
            Map(x => x.ContactNo);
            Map(x => x.LogoImageUrl);
            Map(x => x.ReportImageUrl);
            Map(x => x.IdFooterImageUrl);
            Map(x => x.SmsApiUserName);
            Map(x => x.SmsApiPassword);
            Map(x => x.HighPrioritySmsApiUserName);
            Map(x => x.HighPrioritySmsApiPassword);
            Map(x => x.BasicSalary).Column("BasicSalary");
            Map(x => x.HouseRent).Column("HouseRent");
            Map(x => x.MedicalAllowance).Column("MedicalAllowance");
            Map(x => x.Convenyance).Column("Convenyance");
            Map(x => x.MonthlyWorkingDay).Column("MonthlyWorkingDay");
            Map(x => x.DeductionPerAbsent).Column("DeductionPerAbsent");
            Map(x => x.WorkingHour).Column("WorkingHour");
            Map(x => x.AttendanceStartTime).Column("AttendanceStartTime");
            HasMany(x => x.Programs).KeyColumn("OrganizationId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.Branches).KeyColumn("OrganizationId").Cascade.AllDeleteOrphan().Inverse();

            //HasMany(x => x.Teachers).KeyColumn("OrganizationId").Cascade.AllDeleteOrphan();
            HasManyToMany(x => x.Teachers).Table("TeacherOrganization")
                .ParentKeyColumn("OrganizationId").ChildKeyColumn("TeacherId")
                .Cascade.SaveUpdate();
            //HasManyToMany(x =&gt; x.Movies)
            //.WithTableName("Cast")
            //.WithParentKeyColumn("ActorId")
            //.WithChildKeyColumn("MovieId")
            //.LazyLoad()
            //.Cascade.SaveUpdate();

            HasMany(x => x.ExtraCurricularActivities).KeyColumn("OrganizationId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SmsMasks).KeyColumn("OrganizationId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.YearlyHolidays).KeyColumn("OrganizationId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.MemberLeaveSummaries).KeyColumn("OrganizationId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.DailySupportAllowances).KeyColumn("OrganizationId").Cascade.AllDeleteOrphan().Inverse();

            HasMany(x => x.FestivalBonusSheetsSalary).KeyColumn("SalaryOrganizationId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.FestivalBonusSheetsJob).KeyColumn("JobOrganizationId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
