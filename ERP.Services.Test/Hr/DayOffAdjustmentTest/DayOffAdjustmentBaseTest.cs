﻿using System;
using System.Globalization;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using System.Collections.Generic;
using UdvashERP.Services.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.DayOffAdjustmentTest
{
    public class DayOffAdjustmentBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly IAttendanceAdjustmentService _attendanceAdjustmentService;
        internal readonly IDayOffAdjustmentService _dayOffAdjustmentService;
        internal readonly ILeaveApplicationService _leaveApplicationService;
        internal readonly IOrganizationService _organizationService;
        internal readonly DayOffAdjustmentFactory _dayOffAdjustmentFactory;
        internal readonly HolidayWorkFactory _holyHolidayWorkFactory;
        internal readonly HolidaySettingFactory _holidaySettingFactory;
        

        #endregion

        public DayOffAdjustmentBaseTest()
        {
            _attendanceAdjustmentService = new AttendanceAdjustmentService(Session);
            _dayOffAdjustmentService = new DayOffAdjustmentService(Session);
            _leaveApplicationService = new LeaveApplicationService(Session);
            _dayOffAdjustmentFactory = new DayOffAdjustmentFactory(null, Session);
            _holyHolidayWorkFactory = new HolidayWorkFactory(null,Session);
            _holidaySettingFactory = new HolidaySettingFactory(null,Session);
            _organizationService = new OrganizationService(Session);
        }
           
        public void Dispose()
        {
            attendanceAdjustmentFactory.LogCleanUp(attendanceAdjustmentFactory);
            attendanceAdjustmentFactory.CleanUp();
            _dayOffAdjustmentFactory.LogCleanUp(_dayOffAdjustmentFactory);
            _dayOffAdjustmentFactory.CleanUp();
            _holyHolidayWorkFactory.LogCleanUp(_holyHolidayWorkFactory);
            _holyHolidayWorkFactory.CleanUpFactory();
            _holidaySettingFactory.Cleanup();
            base.Dispose();
        }                
    }   
}
