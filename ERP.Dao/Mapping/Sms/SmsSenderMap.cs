﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Mapping.Sms
{
    public class SmsSenderMap : BaseClassMap<SmsSender, long>
    {
        public SmsSenderMap()
        {
            Table("SmsSender");
            LazyLoad();
            Map(x => x.Sender);
            Map(x => x.Time);
            Map(x => x.Text);
            Map(x => x.ResponseCode);

        }
    }
}
