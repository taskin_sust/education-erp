﻿using System;
using System.IO;
using System.Windows.Forms;
using log4net;
using log4net.Config;
using Synchronizer.Updater.BusinessRule;
using Synchronizer.Updater.Services;

namespace Synchronizer.Updater
{
    public partial class MainUi : Form
    {
        #region Propertise, Object, Service, Dao Initialization

        private static readonly ILog Log = LogManager.GetLogger("UpdaterLogger");
        private static readonly ILog OperationLog = LogManager.GetLogger("OperationLogger");

        private SynchronizerUpdater _synchronizerUpdater;

        public MainUi()
        {
            InitializeComponent();
            XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "log4net.config"));
        }

        #endregion

        #region Delegate & Function

        private delegate void DelegateUpdateViewMessage(string message);

        private delegate void DelegateClearMessage();

        private void OnClearMessage()
        {
            try
            {
                if (gvMessage.InvokeRequired)
                {
                    var clearMessageDelegate = new DelegateClearMessage(OnClearMessage);
                    gvMessage.BeginInvoke(clearMessageDelegate);
                }
                else
                {
                    gvMessage.Rows.Clear();
                    gvMessage.Refresh();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void OnViewMessageUpdater(string currentMessage)
        {
            try
            {
                if (gvMessage.InvokeRequired)
                {
                    var viewMessageDelegate = new DelegateUpdateViewMessage(OnViewMessageUpdater);
                    gvMessage.BeginInvoke(viewMessageDelegate, currentMessage);
                }
                else
                {
                    string[] rowData = { DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), currentMessage };
                    OperationLog.Info(currentMessage);
                    //var index = gvMessage.Rows.Count - 1;
                    //gvMessage.Rows.Insert(index, rowData);
                    gvMessage.Rows.Insert(0, rowData);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        #endregion

        #region Main UI
        private void SetupApplicationTitle()
        {
            Text = SoftwareInfo.ApplicationName + @" " + SoftwareInfo.ApplicationVersion + @" " +"";
        }

        private void MainUi_Load(object sender, EventArgs e)
        {
            try
            {
                btnStart.PerformClick();
                SetupApplicationTitle();
                ShowInTaskbar = true;
                notificationIcon.Visible = false;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void MinimizeMainUserInterface()
        {
            ShowInTaskbar = false;
            WindowState = FormWindowState.Minimized;
            notificationIcon.Visible = true;
            notificationIcon.BalloonTipTitle = SoftwareInfo.ApplicationName;
            notificationIcon.BalloonTipText = notificationIcon.Text = SoftwareInfo.ApplicationName + @" " + SoftwareInfo.ApplicationVersion + @" " + @" is running";
            notificationIcon.ShowBalloonTip(5000);
        }

        private void notificationIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowInTaskbar = true;
            notificationIcon.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        #endregion

        #region Operational Function

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                btnStart.Enabled = false;
                btnStop.Enabled = true;

                if (_synchronizerUpdater == null)
                {
                    _synchronizerUpdater = new SynchronizerUpdater(OnViewMessageUpdater);
                    _synchronizerUpdater.Start();
                }
                else
                {
                    OnClearMessage();
                    _synchronizerUpdater.Restart();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                btnStart.Enabled = true;
                btnStop.Enabled = false;

                if (_synchronizerUpdater != null)
                    _synchronizerUpdater.Stop();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            string msg = "Are you want to close \"" + SoftwareInfo.ApplicationName + "\" software?";
            if (MessageBox.Show(msg, @"Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Close();
            }
        }

        #endregion 
    }
}