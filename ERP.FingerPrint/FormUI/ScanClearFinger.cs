﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UdvashERP.FingerPrint.FormUI
{
    public partial class ScanClearFinger : BaseForm
    {
        public bool IsRescanClick { get; set; }
        public bool IsClear { get; set; }

        public ScanClearFinger()
        {
            InitializeComponent();
        }

        public ScanClearFinger(int machineNo)
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            IsClear = true;
            this.Close();
        }

        private void btnScanFinger_Click(object sender, EventArgs e)
        {
            IsRescanClick = true;
            this.Close();
        }
    }
}
