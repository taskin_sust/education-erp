﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.QuotationTest
{
    interface IQuotationSaveTest
    {
        #region Basic Test

        void Should_Return_Save_Successfully();
        void Should_Return_Null_Exception();
        void Should_Return_Invalid_Data_Exception(string deliveryDate, string submissionDate, string publishedDate);
        
        #endregion

        #region Business Logic Test

        void Should_Return_Duplicate_QuotationNo_Exception();
        void Should_Return_Null_For_Zero_Quantity();
        void Should_Return_Quotation_Criteria_Null_Exception();
        void Should_Return_Quotation_Item_Null_Exception();

        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "Quotation")]
    public class QuotationSaveTest : QuotationBaseTest, IQuotationSaveTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Save_Successfully()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate)
                .WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program,_quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            //TODO: Have to check service save method not factory method.
            Assert.True(_quotationFactory.Object.Id > 0);
        }

        [Fact]
        public void Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _quotationService.SaveOrUpdate(null));
        }
        

        [Theory]
        [InlineData("2016-05-31 00:00:00.000", "2016-05-31 00:00:00.000", "2016-05-31 00:00:00.000")]
        public void Should_Return_Invalid_Data_Exception(string deliveryDate, string submissionDate, string publishedDate)
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, Purpose.ProgramPurpose, QuotationStatus.Running, Convert.ToDateTime(deliveryDate), Convert.ToDateTime(submissionDate), Convert.ToDateTime(publishedDate)).WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program,_quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object);

            Assert.Throws<InvalidDataException>(() => _quotationService.SaveOrUpdate(_quotationFactory.Object));
        }
        
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_Null_For_Zero_Quantity()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 0, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddMonths(3), DateTime.Now.AddDays(2), DateTime.Now.AddDays(10))
                .WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program, _quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.SaveOrUpdate(_quotationFactory.Object));
        }

        [Fact]
        public void Should_Return_Quotation_Criteria_Null_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program, _quotationFactory.Object.Session)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object);
            Assert.Throws<NullObjectException>(() => _quotationService.SaveOrUpdate(_quotationFactory.Object));
        }

        [Fact]
        public void Should_Return_Quotation_Item_Null_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.Create()
                .WithBranch(_branchFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program, _quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object);
            Assert.Throws<MessageException>(() => _quotationService.SaveOrUpdate(_quotationFactory.Object));
        }

        [Fact]
        public void Should_Return_Duplicate_QuotationNo_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup()
                .WithOrganization(organizationFactory.Object)
                .WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create()
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .Persist();
            var quotationFactory1 = _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithPurposeprogramAndPurposeSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();
            var quotationNo = quotationFactory1.Object.QuotationNo;

            var quotationFactory2 = _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(quotationNo);
            Assert.Throws<DuplicateEntryException>(() => _quotationService.SaveOrUpdate(quotationFactory2.Object));
        }
        
        [Fact(DisplayName = "While Purpose is 8 and ProgramSession is null and branch is Corporate")]
        public void Combination_No_1()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, (Purpose)8, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(null, null)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program,_quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.SaveOrUpdate(_quotationFactory.Object));
        }

        [Fact(DisplayName = "WHile branch is individual and programSession is not null")]
        public void Combination_No_2()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program,_quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();
            Assert.True(_quotationFactory.Object.Id > 0);
        }

        [Fact(DisplayName = "WHile branch is corporate and programSession is not null")]
        public void Combination_No_3()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();
            Assert.True(_quotationFactory.Object.Id > 0);
        }

        [Fact(DisplayName = "WHile item is common item,Purpose is first 3 and purpose of program and session is null")]
        public void Combination_No_4()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(null,null)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.SaveOrUpdate(_quotationFactory.Object));
        }

        [Fact(DisplayName = "WHile item is common item,Purpose is first 3 and purpose of program and session is null")]
        public void Combination_No_5()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(null,null)
                .WithPurposeprogramAndPurposeSession(organizationFactory.Object)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();
            Assert.True(_quotationFactory.Object.Id > 0);
        }

        [Fact(DisplayName = "WHile item is common item,Purpose is first 3 and purpose of program and session is not null and programSession not null")]
        public void Combination_No_6()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program, _quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.SaveOrUpdate(_quotationFactory.Object));
        }

        [Fact(DisplayName = "WHile item is common item,Purpose is N/A and purpose of program and session is not null")]
        public void Combination_No_7()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, null, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program, _quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.SaveOrUpdate(_quotationFactory.Object));
        }

        [Fact(DisplayName = "WHile item is common item,Purpose is N/A and purpose of program and session is  null")]
        public void Combination_No_8()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(null,null)
                .WithPurposeprogramAndPurposeSession(organizationFactory.Object)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();
            Assert.True(_quotationFactory.Object.Id > 0);
        }


        [Fact(DisplayName = "WHile item is program item and purpose any and purpose of program and session null")]
        public void Combination_No_9()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.CommonItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(null,null)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.SaveOrUpdate(_quotationFactory.Object));
        }

        [Fact(DisplayName = "WHile item is program item and purpose any and purpose of program and session not null")]
        public void Combination_No_10()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program,_quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();
            Assert.True(_quotationFactory.Object.Id > 0);
        }

        [Fact(DisplayName = "WHile item is program item and purpose N/A and purpose of program and session not null")]
        public void Combination_No_11()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, null, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program,_quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.SaveOrUpdate(_quotationFactory.Object));
        }

        [Fact(DisplayName = "WHile item is program item and purpose any and purpose of program and session null")]
        public void Combination_No_12()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().IsCorporate(true).WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.CreateWith(0, 100, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(2), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(null, null)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();
            Assert.True(_quotationFactory.Object.Id > 0);
        }

        #endregion
    }
}