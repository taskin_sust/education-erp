﻿
$(document).ready(function () {


    function validateReadOnlyTextField(controlId, errorMessage) {

        $(document).on("change", "#" + controlId, function (event) {

            var value = $("#" + controlId).val().trim();
            if (value.length == 0) {
                window.showErrorMessageBelowCtrl(controlId, "", false);
                window.showErrorMessageBelowCtrl(controlId, errorMessage, true);
                return false;
            } else {
                window.showErrorMessageBelowCtrl(controlId, "", false);
            }
        });
    }

    $("#OrganizationId").change(function () {
        //remove all dependant dropdownlist option
        $('#ProgramId').empty();
        $('#ProgramId').append("<option value=''>Select program</option>");

        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select session</option>");

        $('#BranchId').empty();
        $('#BranchId').append("<option value=''>Select branch</option>");

        $('#CampusId').empty();
        $('#CampusId').append("<option value=''>Select campus</option>");
        var organizationId = $("#OrganizationId").val();

        //console.log("organization Id", organizationId);

        if (organizationId.length > 0) {
            $.ajax({
                //url: "/Administration/Batch/GetProgram",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                type: "POST",
                data: { organizationIds: organizationId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                //success: function (data) {
                //    $('#ProgramId').html(data);
                //    $("#SessionId").html("<option value=''>Select a Session</option>");
                //    $.unblockUI();
                //},

                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.returnProgramList, function (i, v) {
                            $('#ProgramId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "error",
                        });
                    }
                },

                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    //$.fn.customMessage({
                    //    displayMessage: AJAX_ERROR,
                    //    displayMessageType: "error",
                    //    displayTime: 5000
                    //});
                }
            });
        }


    });

    $("#ProgramId").change(function () {

        //remove all dependant dropdownlist option
        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select session</option>");

        $('#BranchId').empty();
        $('#BranchId').append("<option value=''>Select branch</option>");

        $('#CampusId').empty();
        $('#CampusId').append("<option value=''>Select campus</option>");

        var pId = $(this).val();
        var organizationId = $("#OrganizationId").val();
        console.log("Program Id", pId);

        $.ajax({
            //url: "/Administration/Batch/GetSession",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
            type: "POST",
            data: { organizationIds: organizationId, programIds: pId, isAuthorized: true },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                //$.unblockUI();
                //console.log("Success -----");
                //console.log(response);
                //$("#SessionId").html(response);
                if (response.IsSuccess) {
                    $.each(response.returnSessionList, function (i, v) {
                        $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "error",
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (response, status, xhr) {
                $.unblockUI();
                console.log("Failed -----");
                console.log(response);
            }
        });
    });

    $("#SessionId").change(function () {

        //remove all dependant dropdownlist option
        $('#BranchId').empty();
        $('#BranchId').append("<option value=''>Select branch</option>");

        $('#CampusId').empty();
        $('#CampusId').append("<option value=''>Select campus</option>");

        
        var organizationIds = $("#OrganizationId").val();
        var programIds = $('#ProgramId').val();
        var sessionIds = $('#SessionId').val();

       // console.log("Program Id", pId);

        //var sId = $(this).val();
        //console.log("Session Id", sId);

        $.ajax({
            //url: "/Administration/Batch/GetBranch",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
            type: "POST",
            data: { organizationIds: organizationIds, programIds: programIds, sessionIds: sessionIds, isAuthorized: true },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response, status, xhr) {
                $.unblockUI();
                //console.log("Success -----");
                //console.log(response);
                //$("#BranchId").html(response);

                if (response.IsSuccess) {
                    $.each(response.returnBranchList, function (i, v) {
                        $('#BranchId').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "error",
                    });
                }
            }, complete: function () {
                $.unblockUI();
            },
            error: function (response, status, xhr) {
                $.unblockUI();
                console.log("Failed -----");
                console.log(response);
            }
        });
    });

    $("#BranchId").change(function () {

        //remove all dependant dropdownlist option 
        $('#CampusId').empty();
        $('#CampusId').append("<option value=''>Select campus</option>");

        var organizationIds = $("#OrganizationId").val();
        var programIds = $('#ProgramId').val();
        var branchIds = $("#BranchId").val();
        var sessionIds = $('#SessionId').val();


        //console.log("Branch Id", bId);
         
        $.ajax({
            //url: "/Administration/Batch/GetCampus",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
            type: "POST",
            data: { organizationIds: organizationIds, programIds: programIds, branchIds: branchIds, sessionIds: sessionIds },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response, status, xhr) {
                //$.unblockUI();
                //console.log("Success -----");
                //console.log(response);
                //$("#CampusId").html(response);

                if (response.IsSuccess) {
                    $.each(response.returnCampusList, function (i, v) {
                        $('#CampusId').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "error",
                    });
                }
            }, complete: function () {
                $.unblockUI();
            },
            error: function (response, status, xhr) {
                $.unblockUI();
                console.log("Failed -----");
                console.log(response);
            }
        });
    });

    $(document).on('click', '#createBatch', function (e) {

        $('.field-validation-error').remove();
        var organizationId = $('#OrganizationId').val();
        var programId = $('#ProgramId').val();
        var sessionId = $('#SessionId').val();
        var branchId = $('#BranchId').val();
        var campusId = $('#CampusId').val();
        var name = $('#Name').val();
        var startTime = $('#StartTime').val();
        var endTime = $('#EndTime').val();
        var capacity = $('#Capacity').val();
        var versionOfStudyId = $('#VersionOfStudy').val();
        var genderId = $('#Gender').val();
        var isSuccess = true;

        if (organizationId == null || organizationId <= 0) {
            window.showErrorMessageBelowCtrl("OrganizationId", "", false);
            window.showErrorMessageBelowCtrl("OrganizationId", "Please Select Organization.", true);
            isSuccess = false;
        }

        if (programId == null || programId <= 0) {
            window.showErrorMessageBelowCtrl("ProgramId", "", false);
            window.showErrorMessageBelowCtrl("ProgramId", "Please Select Program.", true);
            isSuccess = false;
        }

        if (sessionId == null || sessionId <= 0) {
            window.showErrorMessageBelowCtrl("SessionId", "", false);
            window.showErrorMessageBelowCtrl("SessionId", "Please Select Session", true);
            isSuccess = false;
        }

        if (branchId == null || branchId <= 0) {
            window.showErrorMessageBelowCtrl("BranchId", "", false);
            window.showErrorMessageBelowCtrl("BranchId", "Please Select Branch.", true);
            isSuccess = false;
        }

        if (campusId == null || campusId <= 0) {
            window.showErrorMessageBelowCtrl("CampusId", "", false);
            window.showErrorMessageBelowCtrl("CampusId", "Please Select Campus.", true);
            isSuccess = false;
        }

        if (startTime == null || startTime.length <= 0) {
            window.showErrorMessageBelowCtrl("StartTime", "", false);
            window.showErrorMessageBelowCtrl("StartTime", "Please select Start Time.", true);

            isSuccess = false;
        }
        if (endTime == null || endTime.length <= 0) {
            window.showErrorMessageBelowCtrl("EndTime", "", false);
            window.showErrorMessageBelowCtrl("EndTime", "Please select End Time.", true);
            isSuccess = false;
        }

        if (name == null || name.length <= 0) {
            window.showErrorMessageBelowCtrl("Name", "", false);
            window.showErrorMessageBelowCtrl("Name", "Please enter valid Batch Name.", true);
            isSuccess = false;
        }
        if (capacity == null || capacity <= 0) {
            window.showErrorMessageBelowCtrl("Capacity", "", false);
            window.showErrorMessageBelowCtrl("Capacity", "Please enter valid Batch Capacity.", true);
            isSuccess = false;
        }
        if (versionOfStudyId == null || versionOfStudyId <= 0) {
            window.showErrorMessageBelowCtrl("VersionOfStudy", "", false);
            window.showErrorMessageBelowCtrl("VersionOfStudy", "Please select Version Of Study.", true);
            isSuccess = false;
        }
        if (genderId == null || genderId <= 0) {
            window.showErrorMessageBelowCtrl("Gender", "", false);
            window.showErrorMessageBelowCtrl("Gender", "Please select Gender.", true);
            isSuccess = false;
        }
        if (!($("#WeekDays input:checkbox:checked").length > 0)) {
            $.fn.customMessage({
                displayMessage: "Please select some days for Batch Days .",
                displayMessageType: "e"
            });
            isSuccess = false;
        }

        if (isSuccess == true) {
            return true;
        } else {
            e.preventDefault();
            return false;
        }

    });

    $(document).on('click', '#updateBatch', function (e) {

        $('.field-validation-error').remove();

        var organizationId = $('#OrganizationId').val();
        var programId = $('#ProgramId').val();
        var sessionId = $('#SessionId').val();
        var branchId = $('#BranchId').val();
        var campusId = $('#CampusId').val();
        var name = $('#Name').val();
        var startTime = $('#StartTime').val();
        var endTime = $('#EndTime').val();
        var capacity = $('#Capacity').val();
        var gender = $('#Gender').val();
        var versionOfStudy = $('#VersionOfStudy').val();

        var isSuccess = true;
        if (organizationId == null || organizationId <= 0) {
            window.showErrorMessageBelowCtrl("OrganizationId", "", false);
            window.showErrorMessageBelowCtrl("OrganizationId", "Please Select Organization.", true);
            isSuccess = false;
        }

        if (programId == null || programId <= 0) {
            window.showErrorMessageBelowCtrl("ProgramId", "", false);
            window.showErrorMessageBelowCtrl("ProgramId", "Please Select Program.", true);
            isSuccess = false;
        }

        if (sessionId == null || sessionId <= 0) {
            window.showErrorMessageBelowCtrl("SessionId", "", false);
            window.showErrorMessageBelowCtrl("SessionId", "Please Select Session", true);
            isSuccess = false;
        }

        if (branchId == null || branchId <= 0) {
            window.showErrorMessageBelowCtrl("BranchId", "", false);
            window.showErrorMessageBelowCtrl("BranchId", "Please Select Branch.", true);
            isSuccess = false;
        }

        if (campusId == null || campusId <= 0) {
            window.showErrorMessageBelowCtrl("CampusId", "", false);
            window.showErrorMessageBelowCtrl("CampusId", "Please Select Campus.", true);
            isSuccess = false;
        }

        if (startTime == null || startTime.length <= 0) {
            window.showErrorMessageBelowCtrl("StartTime", "", false);
            window.showErrorMessageBelowCtrl("StartTime", "Please select Start Time.", true);
            isSuccess = false;
        }
        if (endTime == null || endTime.length <= 0) {
            window.showErrorMessageBelowCtrl("EndTime", "", false);
            window.showErrorMessageBelowCtrl("EndTime", "Please select End Time.", true);
            isSuccess = false;
        }

        if (name == null || name.length <= 0) {
            window.showErrorMessageBelowCtrl("Name", "", false);
            window.showErrorMessageBelowCtrl("Name", "Please enter valid Batch Name.", true);
            isSuccess = false;
        }
        if (capacity == null || capacity <= 0) {
            window.showErrorMessageBelowCtrl("Capacity", "", false);
            window.showErrorMessageBelowCtrl("Capacity", "Please enter valid Batch Capacity.", true);
            isSuccess = false;
        }
        if (gender == null || gender <= 0) {
            window.showErrorMessageBelowCtrl("Gender", "", false);
            window.showErrorMessageBelowCtrl("Gender", "Please select Gender.", true);
            isSuccess = false;
        }

        if (versionOfStudy == null || versionOfStudy <= 0) {
            window.showErrorMessageBelowCtrl("VersionOfStudy", "", false);
            window.showErrorMessageBelowCtrl("VersionOfStudy", "Please select Version Of Study.", true);
            isSuccess = false;
        }


        if (!($("#WeekDays input:checkbox:checked").length > 0)) {
            $.fn.customMessage({
                displayMessage: "Please select some days for Batch Days .",
                displayMessageType: "e"
            });
            isSuccess = false;
        }

        if (isSuccess == true) {
            return true;
        } else {
            e.preventDefault();
            return false;
        }
    });
});