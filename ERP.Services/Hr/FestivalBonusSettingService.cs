using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Util;
using FluentNHibernate.Testing.Values;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.Hr
{
    public interface IFestivalBonusSettingService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(FestivalBonusSetting prFestivalBonus);
        bool IsDelete(long id);

        #endregion

        #region Single Instances Loading Function

        FestivalBonusSetting LoadById(long id);
        BusinessModel.Entity.Hr.FestivalBonusSetting GetFestivalBonusSetting(long id, long organizationId);

        #endregion

        #region List Loading Function

        IList<FestivalBonusSetting> LoadFestivalBonusList(string orderBy, string p, List<UserMenu> _userMenu, string organization, string employmentStatus, string religion, string calculationOn);
        IList<FestivalBonusSetting> LoadAuthorizedFestivalBonusSettings(List<UserMenu> userMenu, List<long> organizationIds,DateTime startDate);
        IList<FestivalBonusSetting> LoadFestivalBonusSettings(List<long> organizationIds,DateTime startDate);

        #endregion

        #region Others Function

        int LoadFestivalBonusListCount(List<UserMenu> _userMenu, string organization, string employmentStatus, string religion, string calculationOn);
        bool HasDuplicateByFbTitle(string name, long organizationId, long? id,DateTime effectiveDate,DateTime? closingDate);
        
        #endregion

        #region Helper Function
        #endregion
        
    }
    public class FestivalBonusSettingService : BaseService, IFestivalBonusSettingService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("PayrollService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IFestivalBonusSettingDao _prFestivalBonusDao;
        public FestivalBonusSettingService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _prFestivalBonusDao = new FestivalBonusSetttingDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public void SaveOrUpdate(FestivalBonusSetting prFestivalBonus)
        {
            ITransaction transaction = null;
            try
            {
                CheckbeforeSaveOrUpdate(prFestivalBonus);
                var obj = new FestivalBonusSetting();
                List<FestivalBonusSettingCalculation> clonedList = prFestivalBonus.PrFestivalBonusCalculation.CloneList().ToList();
                using (transaction = Session.BeginTransaction())
                {
                    if (prFestivalBonus.Id == 0)
                    {
                        obj = prFestivalBonus;
                        obj.Status = FestivalBonusSetting.EntityStatus.Active;
                        obj.PrFestivalBonusCalculation.Clear();
                        foreach (var festivalBonusSettingCalculation in clonedList)
                        {
                            var ob = new FestivalBonusSettingCalculation
                            {
                                PrFestivalBonusSetting = obj,
                                EmploymentStatus = festivalBonusSettingCalculation.EmploymentStatus,
                                CalculationOn = festivalBonusSettingCalculation.CalculationOn,
                                BonusPercentage = festivalBonusSettingCalculation.BonusPercentage,
                                Status = FestivalBonusSettingCalculation.EntityStatus.Active,
                                CreateBy = GetCurrentUserId()
                            };
                            obj.PrFestivalBonusCalculation.Add(ob);
                        }
                    }
                    else
                    {
                        obj = _prFestivalBonusDao.LoadById(prFestivalBonus.Id);
                        obj.Name = prFestivalBonus.Name;
                        obj.Organization = prFestivalBonus.Organization;
                        obj.IsIslam = prFestivalBonus.IsIslam;
                        obj.IsHinduism = prFestivalBonus.IsHinduism;
                        obj.IsChristianity = prFestivalBonus.IsChristianity;
                        obj.IsBuddhism = prFestivalBonus.IsBuddhism;
                        obj.IsOthers = prFestivalBonus.IsOthers;
                        obj.EffectiveDate = prFestivalBonus.EffectiveDate;
                        obj.ClosingDate = prFestivalBonus.ClosingDate;
                        obj.PrFestivalBonusCalculation.Clear();
                        foreach (var festivalBonusSettingCalculation in clonedList)
                        {
                            var ob = new FestivalBonusSettingCalculation
                            {
                                PrFestivalBonusSetting = obj,
                                EmploymentStatus = festivalBonusSettingCalculation.EmploymentStatus,
                                CalculationOn = festivalBonusSettingCalculation.CalculationOn,
                                BonusPercentage = festivalBonusSettingCalculation.BonusPercentage,
                                Status = FestivalBonusSettingCalculation.EntityStatus.Active,
                                CreateBy = obj.CreateBy,
                                ModifyBy = GetCurrentUserId()
                            };
                            obj.PrFestivalBonusCalculation.Add(ob);
                        }
                    }
                    _prFestivalBonusDao.SaveOrUpdate(obj);
                    transaction.Commit();
                }
            }
            catch (NullObjectException) { throw; }

            catch (MessageException) { throw; }

            catch (EmptyFieldException) { throw; }

            catch (DuplicateEntryException) { throw; }

            catch (InvalidDataException) { throw; }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        
        public bool IsDelete(long id)
        {
            ITransaction trans = null;
            try
            {
                var obj = _prFestivalBonusDao.LoadById(id);
                if (obj==null)
                    throw new InvalidDataException("Invalid Festival Bonus");

                if (obj.Id > 0)
                {
                    var isCheck =Session.QueryOver<FestivalBonusSheet>().Where(x => x.FestivalBonusSetting.Id == obj.Id && x.IsSubmit).List<FestivalBonusSheet>();
                    if (isCheck != null && isCheck.Any())
                        throw new MessageException("Festival bonus already assign in festival bonus sheet.So you can not delete");
                }
                using (trans = Session.BeginTransaction())
                {
                    obj.Status = FestivalBonusSetting.EntityStatus.Delete;
                    List<FestivalBonusSettingCalculation> clonedList = obj.PrFestivalBonusCalculation.CloneList().ToList();
                    obj.PrFestivalBonusCalculation.Clear();
                    foreach (var festivalBonusSettingCalculation in clonedList)
                    {
                        var ob = new FestivalBonusSettingCalculation
                        {
                            PrFestivalBonusSetting = obj,
                            EmploymentStatus = festivalBonusSettingCalculation.EmploymentStatus,
                            CalculationOn = festivalBonusSettingCalculation.CalculationOn,
                            BonusPercentage = festivalBonusSettingCalculation.BonusPercentage,
                            Status = FestivalBonusSettingCalculation.EntityStatus.Delete,
                            CreateBy = obj.CreateBy,
                            ModifyBy = GetCurrentUserId()
                        };
                        obj.PrFestivalBonusCalculation.Add(ob);
                    }
                    _prFestivalBonusDao.Update(obj);
                    trans.Commit();
                    return true;
                }
            }
            catch (MessageException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public FestivalBonusSetting LoadById(long id)
        {
            try
            {
                return _prFestivalBonusDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public FestivalBonusSetting GetFestivalBonusSetting(long id, long organizationId)
        {
            return
                Session.QueryOver<FestivalBonusSetting>()
                    .Where(x => x.Id == id && x.Organization.Id == organizationId)
                    .SingleOrDefault();
        }

        #endregion

        #region List Loading Function

        public IList<FestivalBonusSetting> LoadFestivalBonusList(string orderBy, string orderDir, List<UserMenu> _userMenu, string organization,
            string employmentStatus, string religion, string calculationOn)
        {
            try
            {
                if (_userMenu == null || !_userMenu.Any())
                    throw new InvalidDataException("Invalid User menu");

                var organizationIdList = !string.IsNullOrEmpty(organization) ? _commonHelper.ConvertIdToList(Convert.ToInt64(organization)) : null;
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(_userMenu, organizationIdList);

                if (authOrganizationIdList == null || !authOrganizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                return _prFestivalBonusDao.LoadFestivalBonusList(orderBy, orderDir, authOrganizationIdList, employmentStatus, religion, calculationOn);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<FestivalBonusSetting> LoadAuthorizedFestivalBonusSettings(List<UserMenu> userMenu, List<long> organizationIds,DateTime startDate)
        {
            try
            {
                if (userMenu == null || !userMenu.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> authorizedOrganizationIdList = AuthHelper.LoadOrganizationIdList(
                    userMenu,
                    (organizationIds != null && !organizationIds.Contains(SelectionType.SelelectAll) &&
                     !organizationIds.Contains(-1))
                        ? organizationIds
                        : null);

                if (authorizedOrganizationIdList == null || !authorizedOrganizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                return _prFestivalBonusDao.LoadFestivalBonusSettingList(authorizedOrganizationIdList, startDate);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<FestivalBonusSetting> LoadFestivalBonusSettings(List<long> organizationIds,DateTime startDate)
        {
            try
            {
               return _prFestivalBonusDao.LoadFestivalBonusSettingList(organizationIds,startDate);            
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function

        public int LoadFestivalBonusListCount(List<UserMenu> userMenus, string organization, string employmentStatus, string religion,
            string calculationOn)
        {
            try
            {
                if (userMenus == null || !userMenus.Any())
                    throw new InvalidDataException("Invalid User menu");

                var organizationIdList = !string.IsNullOrEmpty(organization) ? _commonHelper.ConvertIdToList(Convert.ToInt64(organization)) : null;
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenus, organizationIdList);

                if (authOrganizationIdList == null || !authOrganizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                return _prFestivalBonusDao.LoadFestivalBonusListCount(authOrganizationIdList, employmentStatus, religion, calculationOn);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool HasDuplicateByFbTitle(string name, long organizationId, long? id, DateTime effectiveDate, DateTime? closingDate)
        {
            try
            {
                return _prFestivalBonusDao.HasDuplicateByFbTitle(name, organizationId, id, effectiveDate, closingDate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function
        
        private void CheckbeforeSaveOrUpdate(FestivalBonusSetting prFestivalBonus)
        {
            if (prFestivalBonus == null)
                throw new NullObjectException("Festival Bonus Setting Empty");

            CustomModelValidationCheck.DataAnnotationCheck(prFestivalBonus, new FestivalBonusSetting());

            if (prFestivalBonus.Organization == null)
                throw new NullObjectException("Organization can not empty");

            if (prFestivalBonus.IsIslam == true || prFestivalBonus.IsHinduism == true ||
                prFestivalBonus.IsChristianity == true || prFestivalBonus.IsBuddhism == true ||
                prFestivalBonus.IsOthers == true) { }
            else
            {
                throw new InvalidDataException("At least a religion is required");
            }

            if (prFestivalBonus.PrFestivalBonusCalculation == null)
                throw new InvalidDataException("Festival bonus calculation fields must be required");


            foreach (var festivalBonusSettingCalculation in prFestivalBonus.PrFestivalBonusCalculation)
            {
                CustomModelValidationCheck.DataAnnotationCheck(festivalBonusSettingCalculation, new FestivalBonusSettingCalculation());
            }

            var count = prFestivalBonus.PrFestivalBonusCalculation.Count(x => x.BonusPercentage == null || x.CalculationOn == null);
            if (count > 0)
                throw new InvalidDataException("Festival bonus calculation fields must be required");
            if (prFestivalBonus.ClosingDate.HasValue)
            {
                DateTime effectiveDate = DateTime.ParseExact(prFestivalBonus.EffectiveDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                DateTime closingDate = DateTime.ParseExact(prFestivalBonus.ClosingDate.Value.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                if (closingDate <= effectiveDate)
                    throw new InvalidDataException("effective date can not greater than closing date");
            }

            bool checkDuplicateBonusTitle = _prFestivalBonusDao.HasDuplicateByFbTitle(prFestivalBonus.Name,prFestivalBonus.Organization.Id, prFestivalBonus.Id,prFestivalBonus.EffectiveDate,prFestivalBonus.ClosingDate);
            if (!checkDuplicateBonusTitle)
                throw new DuplicateEntryException("Sorry!! Festival Bonus duplicate found.");

            if (prFestivalBonus.Id > 0)
            {
                var isCheck =
                    Session.QueryOver<FestivalBonusSheet>()
                        .Where(x => x.FestivalBonusSetting.Id == prFestivalBonus.Id && x.IsSubmit)
                        .List<FestivalBonusSheet>();
                if(isCheck!=null && isCheck.Any())
                    throw new MessageException("Festival bonus already assign in festival bonus sheet.So you can not update");
            }
        }

        #endregion
    }
}

