﻿using System;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using System.Collections.Generic;
using UdvashERP.Services.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.DepartmentTest
{
    public class DepartmentBaseTest : TestBase,IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal DepartmentService _departmentService;
        internal OrganizationService _organizationService;
        internal List<long> IdList = new List<long>();

        private ISession _session;
        internal DepartmentFactory _departmentFactory;

        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal const long OrgId = 1;

        #endregion

        public DepartmentBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();            
            _departmentFactory = new DepartmentFactory(null,_session);
            _departmentService = new DepartmentService(_session);
            _organizationService = new OrganizationService(_session);
        }
               
        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _departmentFactory.Cleanup();
            base.Dispose();
        }                
    }   
}
