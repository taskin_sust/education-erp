﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.FestivalBonusSettingTest
{

    [Trait("Area", "Hr")]
    [Trait("Service", "FestivalBonusSetting")]
    public class FestivalBonusSettingLoadTest : FestivalBonusSettingBaseTest
    {
        #region Festival Bonus Setting LoadById

        [Fact]
        public void Should_Return_Null()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            Assert.Null(_festivalBonusSettingService.LoadById(_festivalBonusSettingFactory.Object.Id + 1));
        }

        [Fact]
        public void Should_Return_Not_Null()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            Assert.NotNull(_festivalBonusSettingService.LoadById(_festivalBonusSettingFactory.Object.Id));
        } 
        #endregion

        #region Festival Bouns Count Test

        [Fact]
        public void Should_Return_Successfully_LoadListCount()
        {
            organizationFactory.Create().Persist();
            _festivalBonusSettingFactory.CreateMore(10, organizationFactory.Object).Persist();

            var usermenu = BuildUserMenu(organizationFactory.Object, program: null, branch: null);

            var listCount = _festivalBonusSettingService.LoadFestivalBonusListCount(usermenu, organizationFactory.Object.Id.ToString(), "", "", "");
            Assert.Equal(10, listCount);
        }

        [Fact]
        public void Should_Return_InvalidException_Null_UserMenu()
        {
            organizationFactory.Create().Persist();
            _festivalBonusSettingFactory.CreateMore(10, organizationFactory.Object).Persist();

            Assert.Throws<InvalidDataException>(() => _festivalBonusSettingService.LoadFestivalBonusListCount(null, organizationFactory.Object.Id.ToString(), "", "", ""));
        }

        [Fact]
        public void Should_Return_InvalidException_Invalid_Organization()
        {
            organizationFactory.Create().Persist();
            _festivalBonusSettingFactory.CreateMore(10, organizationFactory.Object).Persist();
            var usermenu = BuildUserMenu(organizationFactory.Object, program: null, branch: null);
            Assert.Throws<InvalidDataException>(() => _festivalBonusSettingService.LoadFestivalBonusListCount(usermenu, organizationFactory.Object.Id.ToString()+1, "", "", ""));
        }

        #endregion

        #region Festival Bonus List Test

        [Fact]
        public void Should_Return_Successfully_LoadList()
        {
            organizationFactory.Create().Persist();
            _festivalBonusSettingFactory.CreateMore(10, organizationFactory.Object).Persist();

            var usermenu = BuildUserMenu(organizationFactory.Object, program: null, branch: null);

            var list = _festivalBonusSettingService.LoadFestivalBonusList("", "", usermenu,
                organizationFactory.Object.Id.ToString(), "", "", "");
            Assert.Equal(10, list.Count());
        }

        [Fact]
        public void Should_Return_Invalid_Exception_Null_UserMenu()
        {
            organizationFactory.Create().Persist();
            _festivalBonusSettingFactory.CreateMore(10, organizationFactory.Object).Persist();

            Assert.Throws<InvalidDataException>(
                    () => _festivalBonusSettingService.LoadFestivalBonusList("", "", null, organizationFactory.Object.Id.ToString(), "", "", ""));
        }

        [Fact]
        public void Should_Return_Invalid_Exception_Invalid_Organization()
        {
            organizationFactory.Create().Persist();
            _festivalBonusSettingFactory.CreateMore(10, organizationFactory.Object).Persist();

            var usermenu = BuildUserMenu(organizationFactory.Object, program: null, branch: null);

            Assert.Throws<InvalidDataException>(
                () => _festivalBonusSettingService.LoadFestivalBonusList("", "", usermenu,organizationFactory.Object.Id.ToString()+1, "", "", ""));
        }

        #endregion

        #region LoadAuthorizedFestivalBonusSettings

        [Fact]
        public void Should_Return_LoadAuthorizedFestivalBonusSettings_Successfully_LoadList()
        {
            organizationFactory.Create().Persist();
            _festivalBonusSettingFactory.CreateMore(10, organizationFactory.Object).Persist();

            var usermenu = BuildUserMenu(organizationFactory.Object, program: null, branch: null);

            var list = _festivalBonusSettingService.LoadAuthorizedFestivalBonusSettings(usermenu,commonHelper.ConvertIdToList(organizationFactory.Object.Id),DateTime.Now.AddMonths(5));
            Assert.Equal(10, list.Count());
        }


        [Fact]
        public void Should_Return_LoadAuthorizedFestivalBonusSettings_Invalid_Exception_Null_UserMenu()
        {
            organizationFactory.Create().Persist();
            _festivalBonusSettingFactory.CreateMore(10, organizationFactory.Object).Persist();

            Assert.Throws<InvalidDataException>(()=>_festivalBonusSettingService.LoadAuthorizedFestivalBonusSettings(null, commonHelper.ConvertIdToList(organizationFactory.Object.Id), DateTime.Now.AddMonths(5)));
        }

        [Fact]
        public void Should_Return_LoadAuthorizedFestivalBonusSettings_Invalid_Exception_Invalid_Organization()
        {
            organizationFactory.Create().Persist();
            _festivalBonusSettingFactory.CreateMore(10, organizationFactory.Object).Persist();

            var usermenu = BuildUserMenu(organizationFactory.Object, program: null, branch: null);

            Assert.Throws<InvalidDataException>(
                () => _festivalBonusSettingService.LoadAuthorizedFestivalBonusSettings(usermenu, commonHelper.ConvertIdToList(organizationFactory.Object.Id+1), DateTime.Now.AddMonths(5)));
        }

        #endregion
    }
}
