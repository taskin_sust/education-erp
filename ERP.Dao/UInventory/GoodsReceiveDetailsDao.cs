using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using FluentNHibernate.Conventions.Inspections;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Students;

namespace UdvashERP.Dao.UInventory
{

    //public class ExpandoObjectResultSetTransformer : IResultTransformer
    //{
    //    public object TransformTuple(object[] tuple, string[] aliases)
    //    {
    //        var expando = new ExpandoObject();
    //        var dictionary = (IDictionary<string, object>)expando;
    //        for (int i = 0; i < tuple.Length; i++)
    //        {
    //            string alias = aliases[i];
    //            if (alias != null)
    //            {
    //                dictionary[alias] = tuple[i];
    //            }
    //        }
    //        return expando;
    //    }
    //    public IList TransformList(IList collection)
    //    {
    //        return collection;
    //    }
    //}

    public interface IGoodsReceiveDetailDao : IBaseDao<GoodsReceiveDetails, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        IList<dynamic> LoadGoodReceive(int start, int length, List<long> branchIdList, List<int> purposeList,
            List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierId, int receiveType,
            int reportType, string dateFrom, string dateTo, string branchNameStr, List<string> programsession, bool isOnlyNa = false);
        IList<dynamic> LoadGoodReceiveProgramWise(int start, int length, List<long> authBranchIdList, List<int> purposeList,
            List<long> itemGroupIdList, List<long> itemIdList, List<long> authProgramList, List<long> sessionIdList, List<long> supplierIdList, int receiveType, string dateFrom,
            string dateTo, string programSessionNameStr, List<String> programsession, bool isOnlyNa = false);

        #endregion

        #region Others Function

        long GetPrevSumReceiveAmountByWorkOrderDetailId(long workOrderDetailid);
        long GetLastReceivedAmountByReceiveDetailsId(long id);
        long GetPrevSumReceiveAmountByGoodsReceiveDetailsId(long id);
        long GetPrevReceiveAmountByReceiveDetailsId(long id);
        long GetPrevReceivedAmountFromSelectedReceivedNo(string goodsRecNo);
        int CountGoodReceive(List<long> authBranchIdList, List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, List<long> supplierId, int receiveType, int reportType, string dateFrom, string dateTo, string branchNameList, List<string> programsession, bool isOnlyNa = false);

        int CountGoodReceiveProgramWise(List<long> authBranchIdList, List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList,
            int receiveType, int reportType, string dateFrom, string dateTo, string programSessionNameStr, List<string> programsession, bool isOnlyNa = false);

        #endregion

    }

    public class GoodsReceiveDetailsDao : BaseDao<GoodsReceiveDetails, long>, IGoodsReceiveDetailDao
    {
        #region Propertise & Object Initialization

        public static IResultTransformer ExpandoObject;

        public GoodsReceiveDetailsDao()
        {
            ExpandoObject = new ExpandoObjectResultSetTransformers();
        }

        #endregion


        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<dynamic> LoadGoodReceive(int start, int length, List<long> branchIdList, List<int> purposeList,
          List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdsList, int receiveType,
          int reportType, string dateFrom, string dateTo, string branchNameStr, List<string> programsession, bool isOnlyNa = false)
        {
            string[] brAry = branchNameStr.Split(',');

            string query = @" SELECT piv.* , ";
            int index = 0;
            foreach (var brName in brAry)
            {
                if (index == 0)
                    query += @" ((case when piv." + brName + " is null then 0 else piv." + brName + " end) + ";
                else
                    query += @" (case when piv." + brName + " is null then 0 else piv." + brName + " end) +";
                index++;
            }
            query = query.Trim();
            query = query.Remove(query.Length - 2);
            query += ") ";
            query += @" as Total  
                              FROM ( ";
            query += GenerateGoodReceiveBranchWiseQuery(branchIdList, purposeList, itemGroupIdList,
               itemIdList, programIdList, sessionIdList, supplierIdsList, receiveType,
               reportType, dateFrom, dateTo, branchNameStr, programsession, isOnlyNa);
            query += @") as piv order by piv.[Item Group] OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            IQuery queryI = Session.CreateSQLQuery(query);
            IList<dynamic> result = DynamicList(queryI);
            return result;
        }

        public IList<dynamic> LoadGoodReceiveProgramWise(int start, int length, List<long> branchIdList, List<int> purposeList, List<long> itemGroupIdList,
            List<long> itemIdList, List<long> authProgramList, List<long> sessionIdList, List<long> supplierIdsList, int receiveType, string dateFrom,
            string dateTo, string programSessionNameStr, List<String> programsessionList, bool isOnlyNa = false)
        {
            string[] prSessionAry = programSessionNameStr.Split(',');

            string query = @" SELECT piv.* , ";
            int index = 0;
            foreach (var psObj in prSessionAry)
            {
                if (index == 0)
                    query += @" ((case when piv." + psObj + " is null then 0 else piv." + psObj + " end) +";
                else
                    query += @" (case when piv." + psObj + " is null then 0 else piv." + psObj + " end) +";
                index++;
            }
            query = query.Trim().Remove(query.Length - 2);
            query += ") ";
            query += @" as Total  
                              FROM ( ";
            query += GenerateGoodReceiveProgramWiseQuery(branchIdList, purposeList, itemGroupIdList, itemIdList, supplierIdsList, receiveType, dateFrom, dateTo, programSessionNameStr, programsessionList, isOnlyNa);
            query += @") as piv order by piv.[Item Name] OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            IQuery queryI = Session.CreateSQLQuery(query);
            IList<dynamic> result = DynamicList(queryI);
            return result;
        }

        private string GenerateGoodReceiveProgramWiseQuery(List<long> branchIdList, List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> supplierIdsList, int receiveType, string dateFrom,
            string dateTo, string programSessionNameStr, List<string> programsessionList, bool isOnlyNa = false)
        {

            string where = "";
            if (programsessionList != null && programsessionList.Any() && !programsessionList.Contains(SelectionType.SelelectAll.ToString())
                )
            {
                where += " WHERE";
                if (programsessionList.Contains("-101") && isOnlyNa)
                {
                    programsessionList = programsessionList.Skip(1).Take(programsessionList.Count - 1).ToList();
                    where += " (PR.Id is null and SE.Id is null ) or ";
                }
                else if (programsessionList.Contains("-101"))
                {
                    programsessionList = programsessionList.Skip(1).Take(programsessionList.Count - 1).ToList();
                    where += " (PR.Id is null and SE.Id is null ) or ";
                }
                if (!isOnlyNa)
                {
                    foreach (var programsession in programsessionList)
                    {
                        var programId = programsession.Split(new string[] { "::" }, StringSplitOptions.None)[0];
                        var sessionId = programsession.Split(new string[] { "::" }, StringSplitOptions.None)[1];
                        where += "(PR.Id=" + programId + " and SE.Id=" + sessionId + ") or ";
                    }
                }
                where = where.Trim();
                where = where.Remove(where.Length - 2);
            }
            string query = @"
                             SELECT IG.Name as [Item Group] ,IT.Name AS [Item Name],
                              ISNULL(PR.ShortName + ' & ' + SE.Name, 'N/A') AS [Program & Session], GRD_S.ReceivedQuantity AS Quantity FROM [UINV_GoodsReceiveDetails] AS GRD_S
                            INNER JOIN [UINV_GoodsReceive] AS GR
                            ON GRD_S.GoodsRecieveId = GR.Id ";
            if (supplierIdsList.Count > 0 && !supplierIdsList.Contains(SelectionType.SelelectAll))
                query += @" AND GR.SupplierId IN (" + string.Join(", ", supplierIdsList) + ") ";

            if (!String.IsNullOrEmpty(dateFrom))
                query += @" AND CAST(GR.GoodsReceivedDate AS DATE) >= '" + Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd") + "' ";

            if (!String.IsNullOrEmpty(dateTo))
                query += @" AND CAST(GR.GoodsReceivedDate AS DATE) <= '" + Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd") + "' ";

            if (!purposeList.Contains(SelectionType.SelelectAll))
            {
                if (purposeList.Contains(-101))
                {
                    query += @" AND (GRD_S.Purpose IS NULL OR ";
                }
                if (query.Contains("OR"))
                {
                    query += @" GRD_S.Purpose IN (" + string.Join(", ", purposeList) + ") ) ";
                }
                else { query += @" AND GRD_S.Purpose IN (" + string.Join(", ", purposeList) + ") "; }
            }

            if (receiveType == (int)ReceiveType.Direct)
                query += @"AND GRD_S.WorkOrderDetailId IS NULL ";

            else if (receiveType == (int)ReceiveType.Ordered)
                query += @"AND GRD_S.WorkOrderDetailId IS NOT NULL ";

            if (itemIdList.Count > 0 && !itemIdList.Contains(SelectionType.SelelectAll))
                query += @" AND GRD_S.ItemId IN (" + string.Join(", ", itemIdList) + ") ";

            query += @" LEFT JOIN [dbo].[Program] AS PR
                              ON PR.Id = GRD_S.ProgramId
                             LEFT JOIN [dbo].[Session] AS SE
                              ON SE.Id = GRD_S.SessionId
                              INNER JOIN [dbo].[UINV_Item] AS IT
                              ON IT.Id = GRD_S.ItemId ";

            query += @" INNER JOIN [dbo].[UINV_ItemGroup] AS IG ON IG.Id = IT.ItemGroupId ";
            if (itemGroupIdList.Count > 0 && !itemGroupIdList.Contains(SelectionType.SelelectAll))
                query += @" AND IG.ID IN (" + string.Join(", ", itemGroupIdList) + ") ";

            query += @" INNER JOIN [Branch] AS BR
                              ON BR.Id = GR.BranchId ";
            if (branchIdList.Count > 0 && !branchIdList.Contains(SelectionType.SelelectAll))
                query += @" AND BR.Id IN (" + string.Join(", ", branchIdList) + ") ";
            query += where;
            query += @" ) AS FIN 
                                                PIVOT
                        	                          (
                        	                             SUM(FIN.Quantity)
                        	                             FOR  [Program & Session] IN( " + programSessionNameStr;
            query += @" ) ";
            return query;
        }

        private string GenerateGoodReceiveBranchWiseQuery(List<long> branchIdList, List<int> purposeList, List<long> itemGroupIdList,
            List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdsList, int receiveType, int reportType, string dateFrom, string dateTo, string branchNameStr,
            List<string> programsessionList, bool isOnlyNa = false)
        {
            string where = "";
            if (programsessionList != null && programsessionList.Any() && !programsessionList.Contains(SelectionType.SelelectAll.ToString()))
            {
                where += " WHERE";
                if (programsessionList.Contains("-101") && isOnlyNa)
                {
                    programsessionList = programsessionList.Skip(1).Take(programsessionList.Count - 1).ToList();
                    where += " (PR.Id is null and SE.Id is null ) or ";
                }
                else if (programsessionList.Contains("-101"))
                {
                    programsessionList = programsessionList.Skip(1).Take(programsessionList.Count - 1).ToList();
                    where += " (PR.Id is null and SE.Id is null ) or ";
                }
                if (!isOnlyNa)
                {
                    foreach (var programsession in programsessionList)
                    {
                        var programId = programsession.Split(new string[] { "::" }, StringSplitOptions.None)[0];
                        var sessionId = programsession.Split(new string[] { "::" }, StringSplitOptions.None)[1];
                        where += "(PR.Id=" + programId + " and SE.Id=" + sessionId + ") or ";
                    }
                }
                where = where.Trim();
                where = where.Remove(where.Length - 2);
            }

            string query = @" SELECT IG.Name as [Item Group] ,IT.Name as [Item Name], ISNULL(PR.ShortName + ' ' + SE.Name, 'N/A') AS [Program & Session] , BR.Name as [Branch Name], 
                                            GRD_S.ReceivedQuantity as Quantuty FROM [dbo].[UINV_GoodsReceive] AS GR 
            	                            INNER JOIN [dbo].[UINV_GoodsReceiveDetails] AS GRD_S ON GR.Id = GRD_S.GoodsRecieveId ";

            if (supplierIdsList.Count > 0 && !supplierIdsList.Contains(SelectionType.SelelectAll))
                query += @" AND GR.SupplierId IN (" + string.Join(", ", supplierIdsList) + ") ";

            if (!String.IsNullOrEmpty(dateFrom))
                query += @" AND CAST(GR.GoodsReceivedDate AS DATE) >= '" + Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd") + "' ";

            if (!String.IsNullOrEmpty(dateTo))
                query += @" AND CAST(GR.GoodsReceivedDate AS DATE) <= '" + Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd") + "' ";

            if (!purposeList.Contains(SelectionType.SelelectAll))
            {
                if (purposeList.Contains(-101))
                {
                    query += @" AND (GRD_S.Purpose IS NULL OR ";
                }
                if (query.Contains("OR"))
                {
                    query += @" GRD_S.Purpose IN (" + string.Join(", ", purposeList) + ") ) ";
                }
                else { query += @" AND GRD_S.Purpose IN (" + string.Join(", ", purposeList) + ") "; }
            }

            if (receiveType == (int)ReceiveType.Direct)
                query += @"AND GRD_S.WorkOrderDetailId IS NULL ";

            else if (receiveType == (int)ReceiveType.Ordered)
                query += @"AND GRD_S.WorkOrderDetailId IS NOT NULL ";

            if (itemIdList.Count > 0 && !itemIdList.Contains(SelectionType.SelelectAll))
                query += @" AND GRD_S.ItemId IN (" + string.Join(", ", itemIdList) + ") ";

            query += @" LEFT JOIN [dbo].[Program] AS PR ON PR.Id = GRD_S.ProgramId ";
            query += @" LEFT JOIN [dbo].[Session] AS SE ON SE.Id = GRD_S.SessionId ";

            query += @" INNER JOIN [dbo].[UINV_Item] AS IT ON IT.Id = GRD_S.ItemId ";
            query += @" INNER JOIN [dbo].[UINV_ItemGroup] AS IG ON IG.Id = IT.ItemGroupId ";
            if (itemGroupIdList.Count > 0 && !itemGroupIdList.Contains(SelectionType.SelelectAll))
                query += @" AND IG.ID IN (" + string.Join(", ", itemGroupIdList) + ") ";
            query += @" INNER JOIN [dbo].[Branch] AS BR ON BR.ID = GR.BranchId ";
            if (branchIdList.Count > 0 && !branchIdList.Contains(SelectionType.SelelectAll))
                query += @" AND BR.Id IN (" + string.Join(", ", branchIdList) + ") ";

            query += where;
            query += @") AS FINAL
                        	                             PIVOT
                        	                             (
                        	                             SUM(Quantuty)
                        	                             FOR [Branch Name] IN( " + branchNameStr;
            query += @" )  ";
            return query;
        }

        #endregion

        #region Others Function


        public long GetPrevSumReceiveAmountByWorkOrderDetailId(long workOrderDetailid)
        {
            var goodsReceiveDetails = Session.QueryOver<GoodsReceiveDetails>()
                .Where(x => x.WorkOrderDetail.Id == workOrderDetailid)
                .List<GoodsReceiveDetails>()
                .Sum(x => x.ReceivedQuantity);

            if (goodsReceiveDetails != null)
                return
                    goodsReceiveDetails;
            return 0;
        }

        public long GetLastReceivedAmountByReceiveDetailsId(long id)
        {
            var goodsReceiveDetails = Session.QueryOver<GoodsReceiveDetails>()
                .Where(x => x.Id == id)
                .SingleOrDefault();
            return goodsReceiveDetails != null ? goodsReceiveDetails.ReceivedQuantity : 0;
        }

        public long GetPrevSumReceiveAmountByGoodsReceiveDetailsId(long id)
        {
            var goodsReceiveDetails = Session.QueryOver<GoodsReceiveDetails>()
                .Where(x => x.Id == id)
               .SingleOrDefault<GoodsReceiveDetails>();

            if (goodsReceiveDetails != null)
                return goodsReceiveDetails.ReceivedQuantity;
            return 0;
        }

        public long GetPrevReceiveAmountByReceiveDetailsId(long id)
        {
            var goodsReceiveDetails = Session.QueryOver<GoodsReceiveDetails>()
               .Where(x => x.Id == id)
               .SingleOrDefault();
            return goodsReceiveDetails != null ? goodsReceiveDetails.ReceivedQuantity : 0;
        }

        public long GetPrevReceivedAmountFromSelectedReceivedNo(string goodsRecNo)
        {
            var query = @"SELECT SUM(ReceivedQuantity) FROM [UINV_GoodsReceiveDetails]
                            WHERE [WorkOrderDetailId] IN(
			                            SELECT WOD.Id AS [WOR_DID] FROM(
			                                SELECT WorkOrderId, GoodsReceivedNo FROM [UINV_GoodsReceive] WHERE [GoodsReceivedNo]= " + "'" + goodsRecNo + "'";
            query += @" ) AS F
			                                 INNER JOIN [UINV_WorkOrderDetails] AS WOD
				                                ON F.WorkOrderId = WOD.WorkOrderId)
				             AND GoodsRecieveId <(SELECT Id AS RecId FROM [UINV_GoodsReceive] WHERE [GoodsReceivedNo]=" + "'" + goodsRecNo + "'" + " )";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int CountGoodReceive(List<long> authBranchIdList, List<int> purposeList, List<long> itemGroupIdList,
            List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdsList, int receiveType, int reportType,
            string dateFrom, string dateTo, string branchNameList, List<string> programsession, bool isOnlyNa = false)
        {
            string query = "SELECT COUNT(piv.[Item Group]) from ( ";
            query += GenerateGoodReceiveBranchWiseQuery(authBranchIdList, purposeList, itemGroupIdList,
              itemIdList, programIdList, sessionIdList, supplierIdsList, receiveType,
              reportType, dateFrom, dateTo, branchNameList, programsession, isOnlyNa);
            query += @") as piv ";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int CountGoodReceiveProgramWise(List<long> authBranchIdList, List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, int receiveType, int reportType, string dateFrom,
            string dateTo, string programSessionNameStr, List<string> programsession, bool isOnlyNa = false)
        {
            string query = "SELECT COUNT(piv.[Item Name]) from ( ";
            query += GenerateGoodReceiveProgramWiseQuery(authBranchIdList, purposeList, itemGroupIdList,
              itemIdList, supplierIdList, receiveType, dateFrom, dateTo, programSessionNameStr, programsession, isOnlyNa);
            query += @") as piv ";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function
        public IList<dynamic> DynamicList(IQuery query)
        {
            var result = query.SetResultTransformer(ExpandoObject).List<dynamic>();
            return result;
        }

        #endregion


    }
}
