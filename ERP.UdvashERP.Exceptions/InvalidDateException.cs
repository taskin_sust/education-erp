﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.MessageExceptions
{
    public class InvalidDatePassedException : Exception
    {
        public InvalidDatePassedException()
            : base("Invalid Data")
        {

        }

        public InvalidDatePassedException(string message)
            : base(message)
        {

        }

    }

}
