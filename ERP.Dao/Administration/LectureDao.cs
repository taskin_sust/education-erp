﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Administration
{
    public interface ILectureDao : IBaseDao<Lecture, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<Lecture> LoadLectureByCourseSubject(long[] courseIdList, long[] subjectIdList, long?[] batchIdList = null, string dateFrom = null, string dateTo = null, bool? clearClassAttendance = null);
        IList<Lecture> LoadByIds(long[] lectureIds);
        IList<Lecture> LoadLecture(List<long> programIds, List<long> sessionIds, List<long> courseIds, List<long> subjectIds, List<Batch> batchList, bool returnDoneLectures);
        IList<Lecture> LoadLecture(long batchId, DateTime date);
        List<Lecture> LoadLecture(List<long> authProgramIdList, List<long> programIdList, List<long> sessionIdList, long[] courseIdList, DateTime dateFrom, DateTime dateTo);
        IList<Lecture> LoadLecture(List<long> programIdList, List<long> sessionIdList, long[] batchIdList, long[] courseIdList, DateTime dateFrom, DateTime dateTo);
        IList<Lecture> LoadLecture(long lecturSettingId);
        #endregion

        #region Others Function

        #endregion

    }

    public class LectureDao : BaseDao<Lecture, long>, ILectureDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<Lecture> LoadLectureByCourseSubject(long[] courseIdList, long[] subjectIdList, long?[] batchIdList = null, string dateFrom = null, string dateTo = null, bool? clearClassAttendance = null)
        {
            StudentClassAttendanceDetails scadAlias = null;
            StudentClassAttendence scaAlias = null;
            Lecture lAlias = null;
            DateTime df = new DateTime();
            DateTime dt = new DateTime();
            if (dateFrom != null && dateTo != null)
            {

                df = DateTime.ParseExact(dateFrom, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                dt = DateTime.ParseExact(dateTo, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                dt = dt.AddDays(1);
            }
            Lecture lecture = null;
            LectureSettings lectureSettings = null;
            CourseSubject cs = null;
            StudentClassAttendence sca = null;
            var classAttendance = Session.QueryOver<Lecture>().JoinAlias(r => r.LectureSettings, () => lectureSettings).JoinAlias(r => lectureSettings.CourseSubject, () => cs).Left.JoinAlias(r => r.StudentClassAttendence, () => sca);
            if (clearClassAttendance == null)
            {
                if (batchIdList != null && batchIdList.Length > 0)
                {
                    var lectures =
                                   Session.QueryOver<CourseProgress>()
                                       .Where(x => x.Batch.Id.IsIn(batchIdList) && x.HeldDate >= df && x.HeldDate < dt)
                                       .Select(x => x.Lecture.Id)
                                       .List<long>();
                    classAttendance.Where(x => x.Id.IsIn(lectures.Distinct().ToArray()));
                }
            }

            if (!courseIdList.Contains(SelectionType.SelelectAll))
            {
                classAttendance.Where(x => cs.Course.Id.IsIn(courseIdList));
            }
            if (!subjectIdList.Contains(SelectionType.SelelectAll))
            {
                classAttendance.Where(x => cs.Subject.Id.IsIn(subjectIdList));
            }

            if (dateFrom != null && dateTo != null && dateFrom != "" && dateTo != "")
            {
                //classAttendance = classAttendance.Where(x => sca.HeldDate >= dateFrom && sca.HeldDate <= dateTo.Value.AddDays(1));
                classAttendance = classAttendance.Where(x => sca.HeldDate >= df && sca.HeldDate < dt);
            }

            var lectureList = classAttendance.List<Lecture>().Distinct().ToList();
            return lectureList.Where(x => x.Status == 1).ToList();
        }

        public IList<Lecture> LoadByIds(long[] lectureIds)
        {

            return Session.QueryOver<Lecture>().Where(x => x.Id.IsIn(lectureIds) && x.Status == Lecture.EntityStatus.Active).List<Lecture>();
        }

        public IList<Lecture> LoadLecture(List<long> programIds, List<long> sessionIds, List<long> courseIds, List<long> subjectIds, List<Batch> batchList, bool returnDoneLectures)
        {
            Program programAlias = null;
            Session sessionAlias = null;
            Course courseAlias = null;
            LectureSettings lsAlias = null;
            CourseSubject csAlias = null;
            Subject subjectAlias = null;
            var lectureSettings = Session.QueryOver<LectureSettings>(() => lsAlias).JoinAlias(() => lsAlias.Course, () => courseAlias)
                .JoinAlias(() => courseAlias.Program, () => programAlias)
                .JoinAlias(() => courseAlias.RefSession, () => sessionAlias)
                .Where(x => programAlias.Id.IsIn(programIds) && sessionAlias.Id.IsIn(sessionIds) && x.Status == LectureSettings.EntityStatus.Active);
            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                lectureSettings = lectureSettings.Where(x => courseAlias.Id.IsIn(courseIds));
            }
            IList<LectureSettings> lectureSettingsWithCourseSubject = new List<LectureSettings>();
            IList<LectureSettings> lectureSettingsWithoutCourseSubject = new List<LectureSettings>();
            var lectureList = new List<Lecture>();
            var lectureListFinal = new List<Lecture>();
            var lectureSettingsList = lectureSettings.List<LectureSettings>();
            lectureSettingsWithCourseSubject = lectureSettingsList.Where(x => x.CourseSubject != null).ToList();
            lectureSettingsWithoutCourseSubject = lectureSettingsList.Where(x => x.CourseSubject == null).ToList();
            if (lectureSettingsWithCourseSubject.Count > 0)
            {
                foreach (var lectureSetting in lectureSettingsWithCourseSubject)
                {
                    lectureList.AddRange(lectureSetting.Lectures);
                }
            }
            if (lectureSettingsWithoutCourseSubject.Count > 0)
            {
                foreach (var lectureSetting in lectureSettingsWithoutCourseSubject)
                {
                    lectureList.AddRange(lectureSetting.Lectures);
                }
            }
            if (!subjectIds.Contains(SelectionType.SelelectAll))
            {
                if (subjectIds.Contains(-1))
                {
                    lectureListFinal.AddRange(lectureList.Where(x => x.LectureSettings.CourseSubject == null).ToList());
                }
                if (subjectIds.Any(i => i > 0))
                {
                    lectureListFinal.AddRange(lectureList.Where(x => x.LectureSettings.CourseSubject != null && x.LectureSettings.CourseSubject.Subject.Id.In(subjectIds.ToArray())).ToList());
                }
            }
            else
            {
                lectureListFinal = lectureList;
            }
            var lecturesDone = new List<Lecture>();
            foreach (var selectedBatch in batchList)
            {
                var donelectures =
                    Session.QueryOver<CourseProgress>()
                        .Where(x => x.Batch.Id == selectedBatch.Id && x.Status == CourseProgress.EntityStatus.Active)
                        .Select(x => x.Lecture)
                        .List<Lecture>();
                lecturesDone.AddRange(donelectures);
            }
            if (lecturesDone.Count > 0)
            {
                if (returnDoneLectures)
                {
                    lectureListFinal =
                        lectureListFinal.Where(x => x.Id.In(lecturesDone.Select(y => y.Id).Distinct().ToArray())).ToList();
                }
                else
                {
                    lectureListFinal =
                        lectureListFinal.Where(x => !x.Id.In(lecturesDone.Select(y => y.Id).Distinct().ToArray())).ToList();
                }
            }
            return lectureListFinal.Where(x => x.Status == Lecture.EntityStatus.Active).ToList();
        }

        public IList<Lecture> LoadLecture(long batchId, DateTime date)
        {
            return
                Session.Query<CourseProgress>()
                    .Where(x => x.CreationDate == date.Date && x.Batch.Id == batchId)
                    .Select(x => x.Lecture)
                    .ToList();
        }

        public List<Lecture> LoadLecture(List<long> authProgramIdList, List<long> programIdList,
            List<long> sessionIdList, long[] courseIdList, DateTime dateFrom, DateTime dateTo)
        {
            var query = @"select distinct l.Id,(ls.ClassNamePrefix+l.ClassNameSuffix) Name from StudentClassAttendence sca 
                        --inner join StudentClassAttendanceDetails scad on scad.StudentClassAttendanceId=sca.Id
                        inner join Lecture l on l.Id=sca.LectureId
                        inner join LectureSettings ls on l.LectureSettingsId=ls.Id
                        and ls.CourseId in({0}) 
                        and sca.HeldDate>='{1}' and sca.HeldDate<'{2}'";
            query = string.Format(query, string.Join(",", courseIdList), dateFrom, dateTo.AddDays(1));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<Lecture>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<Lecture>().ToList();
            return list;

        }

        public IList<Lecture> LoadLecture(List<long> programIdList, List<long> sessionIdList, long[] batchIdList,
            long[] courseIdList, DateTime dateFrom, DateTime dateTo)
        {
            var query = @"select distinct l.Id,(ls.ClassNamePrefix+l.ClassNameSuffix) Name  from CourseProgress cp 
                        inner join Lecture l on l.Id=cp.LectureId
                        inner join LectureSettings ls on l.LectureSettingsId=ls.Id
                        and cp.CourseId in({0})
						--and sca.lectureid in(select distinct LectureId from CourseProgress where HeldDate>='{1}' and HeldDate<'{2}' and BatchId in({3})
                        --and courseid in({0})
                        --) 
                        and cp.HeldDate>='{1}' and cp.HeldDate<'{2}' ";
            query = string.Format(query, string.Join(",", courseIdList), dateFrom, dateTo.AddDays(1), string.Join(",", batchIdList));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<Lecture>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<Lecture>().ToList();
            return list;
        }

        public IList<Lecture> LoadLecture(long lecturSettingId)
        {
            IList<Lecture> lectures =
                Session.QueryOver<Lecture>().Where(x => x.LectureSettings.Id == lecturSettingId).List<Lecture>();
            return lectures;
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
