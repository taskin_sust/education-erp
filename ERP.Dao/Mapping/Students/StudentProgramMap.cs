using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{


    public partial class StudentProgramMap : BaseClassMap<StudentProgram, long>
    {

        public StudentProgramMap()
        {
            Table("StudentProgram");
            LazyLoad();
            Map(x => x.PrnNo).Column("PrnNo");
            Map(x => x.DueAmount).ReadOnly();
            Map(x => x.VersionOfStudy).Column("VersionOfStudy");
            Map(x => x.IsSync);
            Map(x => x.IsImage);
            Map(x => x.IsPolitical);

            References(x => x.Student).Column("StudentId");
            References(x => x.Batch).Column("BatchId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Institute).Column("InstituteId");
            HasMany(x => x.StudentImage).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentClassAttendanceDetails).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentCourseDetails).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse().Where(ss => ss.Status != StudentCourseDetail.EntityStatus.Delete);
            //HasMany(x => x.StudentCourseDetails).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentIdCards).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentPayments).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentTransfers).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentExamAttendances).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentSurveyAnswers).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentExamAttendanceDetails).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SmsRegistrationReceiverLogList).KeyColumn("StudentProgramId").Cascade.AllDeleteOrphan().Inverse();
            
            HasManyToMany(x => x.Materials).Cascade.All()
                //.Inverse()
                .Table("StudentMaterials");
        }
    }
}
