﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.WorkOrderTest
{
    interface IWorkOrderSaveTest
    {
        #region Basic Test
        
        void Should_Return_WorkOrder_Save_Successfully();
        void Should_Return_WorkOrder_Message_Exception();
        
        #endregion

        #region Business Logic Test
        
        void Should_Return_WorkOrder_Null_Exception();
        void Should_Return_WorkOrder_WorkOrderDetails_Null_Exception();
        void Should_Return_WorkOrder_Remaining_Quantity_Exception();
        void Should_Return_WorkOrder_negative_Quantity_Exception();
        void Should_Return_WorkOrder_Quotation_Empty_Exception();
        
        #endregion

    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "WorkOrder")]
    public class WorkOrderSaveTest : WorkOrderBaseTest, IWorkOrderSaveTest
    {

        #region Basic Test
        
        [Fact]
        public void Should_Return_WorkOrder_Save_Successfully()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _quotationFactory.Object.QuotationStatus = QuotationStatus.PertialIssued;

            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object)
                .Persist();
            Assert.NotNull(_workOrderService.GetWorkOrder(_workOrderFactory.Object.Id));
            Assert.True(_workOrderService.GetWorkOrder(_workOrderFactory.Object.Id).Id > 0);
        }

        [Fact]
        public void Should_Return_WorkOrder_Message_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _workOrderFactory.Create()
                .WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object);
            Assert.Throws<MessageException>(() => _workOrderService.WorkOrderSave(_workOrderFactory.Object));
        }
        
        #endregion

        #region Business Logic Test

        

        [Fact]
        public void Should_Return_WorkOrder_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _workOrderService.WorkOrderSave(null));
        }

        [Fact]
        public void Should_Return_WorkOrder_WorkOrderDetails_Null_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object);
            Assert.Throws<NullObjectException>(() => _workOrderService.WorkOrderSave(_workOrderFactory.Object));
        }

        [Fact]
        public void Should_Return_WorkOrder_Remaining_Quantity_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 10, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now).WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();
            _quotationFactory.Object.QuotationStatus = QuotationStatus.PertialIssued;
            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object);
            Assert.Throws<InvalidDataException>(() => _workOrderService.WorkOrderSave(_workOrderFactory.Object));
        }

        [Fact]
        public void Should_Return_WorkOrder_negative_Quantity_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now).WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();
            _quotationFactory.Object.QuotationStatus = QuotationStatus.PertialIssued;
            _workOrderFactory.CreateWith(0, DateTime.Now.AddDays(10), -100, 1000, DateTime.Now.AddDays(5), WorkOrderStatus.Issued).WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object);

            Assert.Throws<NullObjectException>(() => _workOrderService.WorkOrderSave(_workOrderFactory.Object));
        }

        [Fact]
        public void Should_Return_WorkOrder_Quotation_Empty_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _quotationFactory.Object.QuotationStatus = QuotationStatus.PertialIssued;
            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object);

            Assert.Throws<NullObjectException>(() => _workOrderService.WorkOrderSave(_workOrderFactory.Object));
        }

        [Fact(DisplayName = "Work order quantity is greater than Quoted quantity")]
        public void Combination_1()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now).WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();
            _quotationFactory.Object.QuotationStatus = QuotationStatus.PertialIssued;
            _workOrderFactory.CreateWith(0, DateTime.Now.AddDays(2), 50000, 5000000, DateTime.Now.AddDays(1), WorkOrderStatus.Issued).WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object);
            Assert.Throws<InvalidDataException>(() => _workOrderFactory.Persist());
        }

        [Fact(DisplayName = "When work order Issued to supplier quotation status must be issued or partial issued")]
        public void Combination_2()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now).WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _workOrderFactory.CreateWith(0, DateTime.Now.AddDays(2), 50000, 5000000, DateTime.Now.AddDays(1), WorkOrderStatus.Issued)
                .WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object);
            Assert.Throws<InvalidDataException>(() => _workOrderFactory.Persist());
        }
        
        #endregion
    }
}
