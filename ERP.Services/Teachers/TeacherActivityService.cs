﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherActivityService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        TeacherActivity GetTeacherActivity(long id);
        #endregion

        #region List Loading Function
        IList<TeacherActivity> LoadTeacherActivity();

        #endregion

        #region Others Function

        #endregion
       
    }

    public class TeacherActivityService : BaseService, ITeacherActivityService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ITeacherActivityDao _teacherActivityDao;
        public TeacherActivityService(ISession session)
        {
            Session = session;
            _teacherActivityDao = new TeacherActivityDao() { Session = session };

        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public TeacherActivity GetTeacherActivity(long id)
        {
            try
            {
               return _teacherActivityDao.LoadById(id);
            }
            catch (Exception ex)
            {
               _logger.Error(ex); 
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public IList<TeacherActivity> LoadTeacherActivity()
        {
            IList<TeacherActivity> teacherActivities;
            try
            {
                teacherActivities=_teacherActivityDao.LoadAllOk();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return teacherActivities;
        }
        #endregion

        #region Others Function

        #endregion
        
    }
}
