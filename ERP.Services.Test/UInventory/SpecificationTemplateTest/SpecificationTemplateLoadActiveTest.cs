﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.SpecificationTemplateTest
{
    public interface ISpecificationTemplateLoadActiveTest
    {
        #region Basic Test
        void Load_Specification_Criteria_Should_Return_List(); 
        #endregion

        #region Business Logic Test
        void Load_Specification_Criteria_Should_Not_Return_List(); 
        #endregion
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "SpecificationCriteria")]
    public class SpecificationTemplateLoadActiveTest : SpecificationTemplateBaseTest, ISpecificationTemplateLoadActiveTest
    {
        #region Basic Test

        [Fact]
        public void Load_Specification_Criteria_Should_Return_List()
        {
            _specificationTemplateFactory.CreateMore(10).Persist();
            var stList = _specificationTemplateService.LoadSpecificationTemplateList(0, 10, "");
            Assert.True(stList.Count >= 10);
            var templateList =
                _specificationTemplateFactory.GetLastCreatedObjectList();
            var criteriaList = new List<SpecificationCriteria>();
            if (templateList != null)
                foreach (var specificationTemplate in templateList)
                {
                    if (specificationTemplate.SpecificationCriteriaList != null)
                        criteriaList.AddRange(specificationTemplate.SpecificationCriteriaList);
                }
            CleanUpSpecificationCriteria(criteriaList);
        }

        #endregion

        #region Business Logic Test
        
        [Fact]
        public void Load_Specification_Criteria_Should_Not_Return_List()
        {
            _specificationTemplateFactory.CreateMore(10).Persist();
            var stList = _specificationTemplateService.LoadSpecificationTemplateList(0, 10, "hello world");
            Assert.True(stList.Count == 0);
            var templateList =
                _specificationTemplateFactory.GetLastCreatedObjectList();
            var criteriaList = new List<SpecificationCriteria>();
            if (templateList != null)
                foreach (var specificationTemplate in templateList)
                {
                    if (specificationTemplate.SpecificationCriteriaList != null)
                        criteriaList.AddRange(specificationTemplate.SpecificationCriteriaList);
                }
            CleanUpSpecificationCriteria(criteriaList);
        } 

        [Fact]
        public void Load_Specification_Criteria_Should_Return_List_By_Name_Search()
        {
            _specificationTemplateFactory.CreateMore(10).Persist();
            var nameSearch = _specificationTemplateFactory.GetLastCreatedObjectList()[0].Name.Substring(0, 2);
            var stList = _specificationTemplateService.LoadSpecificationTemplateList(0, 10, nameSearch);
            Assert.True(stList.Count>=10);
            var templateList =
                _specificationTemplateFactory.GetLastCreatedObjectList();
            var criteriaList = new List<SpecificationCriteria>();
            if (templateList != null)
                foreach (var specificationTemplate in templateList)
                {
                    if (specificationTemplate.SpecificationCriteriaList != null)
                        criteriaList.AddRange(specificationTemplate.SpecificationCriteriaList);
                }
            CleanUpSpecificationCriteria(criteriaList);
        } 
        
        #endregion

    }
}
