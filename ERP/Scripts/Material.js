﻿/*Written By Taskin */

$(document).ready(function () {

    // Add button click handler
    $(document).on('click', '.addButton', function () {

        var $template = $('#optionTemplate'),
            $clone = $template
                .clone()
                .removeClass('hide')
                .removeAttr('id')
               .insertBefore($template),
            $option = $clone.find('[name="option[]"]');
        $clone.find("input").addClass('extraMaterialNameField');
        // Add new field
    });

    // Remove button click handler
    $(document).on('click', '.removeButton', function () {
        var $row = $(this).parents('.form-group'),
            $option = $row.find('[name="option[]"]');
        // Remove element containing the option
        $row.remove();

    });

    $(document).on('click', '#submitValue', function (bool) {

        $('.field-validation-error').remove();
        var materialName = {};
        var ii = 0;
        var programId = $('#ProgramList').val();
        var sessionId = $('#Session').val();
        var materialTypeId = $('#MaterialType').val();
        var isSuccess = true;

        var successMessage = '<div style="text-align: center;"class="alert alert-success"><a class="close" data-dismiss="alert">×</a><strong>Success!</strong> Material Add Successfully</div>';
        var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Failed</div>';

        $('#materialName .extraMaterialNameField').each(function (index, value) {

            if ($(this).val().length == 0) {
                $(this).css('border-color', 'red');
                var customValidationMssageForEmptyMaterialName = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a Material Name</span></span>';
                $(this).closest().find("span").append(customValidationMssageForEmptyMaterialName);
                isSuccess = false;
            } else {
                $(this).css('border-color', '#cccccc');
                materialName[index] = $(this).val().toString();
                isSuccess = true;
            }
        });
        if (programId.length <= 0) {
            var customValidationMssageForEmptyProgram = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a Program</span></span>';
            $('#ProgramList').parent().append(customValidationMssageForEmptyProgram);
            isSuccess = false;
        }
        if (sessionId.length <= 0) {
            var customValidationMssageForEmptySession = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a Session</span></span>';
            $('#Session').parent().append(customValidationMssageForEmptySession);
            isSuccess = false;
        }
        if (materialTypeId.length <= 0) {
            var customValidationMssageForEmptyMaterialType = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a MaterialType</span></span>';
            $('#MaterialType').parent().append(customValidationMssageForEmptyMaterialType);
            isSuccess = false;
        }

        if (isSuccess == true) {

            $.ajax({
                url: "/Administration/Material/AddMaterials",
                type: "POST",
                data: { materialNames: JSON.stringify(materialName), programId: programId, sessionId: sessionId, materialTypeId: materialTypeId },
                success: function (data) {

                    if ($.trim(data) === "Material Add Successfully") {

                        $('.customMessage').append(successMessage);
                    } else {

                        $('.customMessage').append('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Failed!! Possible Duplicate Material Name Found</div>');
                    }
                },
                error: function (data) {
                    $('.customMessage').append(errorMessage);
                }
            });
        } else {
            $('.customMessage').append(errorMessage);
        }

    });

});

