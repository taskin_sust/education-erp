﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceSynchronizer.Helper
{
    public class BusinessRules
    {
        public const string AppName = "Attendance Synchronizer";
        public const string AppVersion = "3.0";
        public const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        public static bool DevMode = false;
        public static int BackupNumber = 12;
    }

    public enum DeviceCommunicationType
    {
        //SerialPort = 1,
        Ethernet = 2,
        Usb = 3
    }

    public enum MessageType
    {
        Attention,
        Success,
        Error,
        Info,
        Important,        
    }

    public static class MemberStatus
    {
        public static int Active { get { return 1; } }
        public static int Inactive { get { return -1; } }
        public static int Delete { get { return -404; } }
        public static int Pending { get { return 2; } }
        public static int Missing { get { return 0; } }
    }

}
