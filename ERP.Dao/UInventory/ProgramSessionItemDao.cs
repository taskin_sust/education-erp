using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface IProgramSessionItemDao : IBaseDao<ProgramSessionItem, long>
    {

        #region Operational Function

        void IsDelete(List<long> idList);

        #endregion

        #region Single Instances Loading Function

        ProgramSessionItem GetProgramSessionItem(Item item = null, Program program = null, Session session = null);

        #endregion

        #region List Loading Function

        IList<ProgramSessionItem> LoadProgramSessionItemByItemId(long? itemId);
        IList<ProgramSessionItemDto> LoadProgramSessionItemDtoByItemId(long[] itemId);

        #endregion

        #region Others Function
        #endregion

    }

    public class ProgramSessionItemDao : BaseDao<ProgramSessionItem, long>, IProgramSessionItemDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function

        public void IsDelete(List<long> idList)
        {
            string deleteSql = "Delete FROM [UINV_ProgramSessionItem] WHERE  1=1 AND Id IN(" + string.Join(",", idList) + ");";
            ExecuteQuery(deleteSql);
        }



        #endregion

        #region Single Instances Loading Function

        public ProgramSessionItem GetProgramSessionItem(Item item, Program program, Session session)
        {
            return
                Session.QueryOver<ProgramSessionItem>()
                    .Where(x => x.Program == program)
                    .Where(x => x.Session == session)
                    .Where(x => x.Item == item).SingleOrDefault<ProgramSessionItem>();
        }

        #endregion

        #region List Loading Function

        public IList<ProgramSessionItem> LoadProgramSessionItemByItemId(long? itemId)
        {
            if (itemId == null)
                return
                    Session.QueryOver<ProgramSessionItem>()
                        .Where(x => x.Status == ProgramSessionItem.EntityStatus.Active)
                        .List<ProgramSessionItem>();
            return
                Session.QueryOver<ProgramSessionItem>()
                    .Where(x => x.Item.Id == itemId && x.Status == ProgramSessionItem.EntityStatus.Active)
                    .List<ProgramSessionItem>();
        }

        public IList<ProgramSessionItemDto> LoadProgramSessionItemDtoByItemId(long[] itemId)
        {
            string query = @"SELECT psi.ProgramId, psi.SessionId, psi.ItemId,
                            p.Name AS ProgramName,
                            s.Name AS SessionName 
                            FROM [UINV_ProgramSessionItem] psi 
	                            INNER JOIN Program p ON p.Id = psi.ProgramId
	                            INNER JOIN [Session] s ON s.Id = psi.SessionId
	                            WHERE 1=1 AND psi.ItemId IN (" + string.Join(",", itemId) + ") GROUP BY ProgramId, SessionId, psi.ItemId, p.Name, s.Name;";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ProgramSessionItemDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<ProgramSessionItemDto>().ToList();
            return list;
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion

    }
}
