﻿using System;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SubjectTest
{
    interface ISubjectRankTest
    {
        #region Basic Test

        [Fact]
        void Get_Maximum_Rank_Not_Null_Check();
        void Get_Minimum_Rank_Not_Null_Check();
        #endregion

        #region Business Logic Test
        
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Subject")]
    public class SubjectRankTest : SubjectBaseTest, ISubjectRankTest
    {
        #region Basic Test

        [Fact]
        public void Get_Maximum_Rank_Not_Null_Check()
        {
            SubjectFactory subjectFactory =
                    _subjectFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithProperties(_shortName)
                    .Persist();
            int maxRank = _subjectService.GetMaximumRank(subjectFactory.Object);
            Assert.True(maxRank>0);

        }

        [Fact]
        public void Get_Minimum_Rank_Not_Null_Check()
        {
            SubjectFactory subjectFactory =
                    _subjectFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithProperties(_shortName)
                    .Persist();
            int minRank = _subjectService.GetMinimumRank(subjectFactory.Object);
            Assert.True(minRank > 0);

        }
        #endregion
        
        #region Business Logic Test
        
        #endregion
    }
}
