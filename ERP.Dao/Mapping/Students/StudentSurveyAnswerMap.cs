﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students 
{
    public partial class StudentSurveyAnswerMap: BaseClassMap<StudentSurveyAnswer,long> 
    {
        public StudentSurveyAnswerMap()
        {
            Table("StudentSurveyAnswer");
            LazyLoad();
            Map(x => x.Advice).Column("Advice");
            Map(x => x.Complain).Column("Complain");
            References(x => x.StudentProgram).Column("StudentProgramId");
            References(x => x.SurveyQuestionAnswer).Column("SurveyQuestionAnswerId");
            References(x => x.SurveyQuestion).Column("SurveyQuestionId");
        }
    }
}
