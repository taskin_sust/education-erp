﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherClassEntryDetailsMap : BaseClassMap<TeacherClassEntryDetails, long>
    {
        public TeacherClassEntryDetailsMap()
        {
            Table("TeacherClassEntryDetails");
            LazyLoad();
            References(x => x.TeacherClassEntry).Column("TeacherClassEntryId");
            References(x => x.Teacher).Column("TeacherId");
            References(x => x.Branch).Column("PaidFromBranchId");
            Map(x => x.Description).Column("Description");
            Map(x => x.Duration).Column("Duration");
            Map(x => x.Quantity).Column("Quantity");
            Map(x => x.Amount).Column("Amount");
            Map(x => x.PaymentStatus).Column("PaymentStatus");
            Map(x => x.PaymentDate).Column("PaymentDate");
            Map(x => x.PaidBy).Column("PaidBy");
        }
    }
}
