﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class ProgramSettingsMap: BaseClassMap<ProgramSettings, long>
    {
        public ProgramSettingsMap()
        {
            Table("ProgramSettings");
            LazyLoad();
            
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.NextProgram).Column("NextProgramId");
            References(x => x.NextSession).Column("NextSessionId");
        }
    }
}
