﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentIdCardDao : IBaseDao<StudentIdCard, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        StudentIdCard getByStudentProgramId(long id);
        StudentIdCard GetByPrn(long programId, long sessionId, string prn);
        #endregion

        #region List Loading Function
        
        #endregion

        #region Others Function

        #endregion
    }

    public class StudentIdCardDao : BaseDao<StudentIdCard, long>, IStudentIdCardDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public StudentIdCard GetByPrn(long programId, long sessionId, string prn)
        {
            var criteria = Session.CreateCriteria<StudentIdCard>();
            criteria.Add(Restrictions.Eq("Status", Institute.EntityStatus.Active));
            criteria.CreateAlias("StudentProgram", "sp");
            criteria.Add(Restrictions.Eq("sp.Program.Id", programId));
            criteria.Add(Restrictions.Eq("sp.PrnNo", prn));
            criteria.CreateAlias("sp.Batch", "spb");
            criteria.Add(Restrictions.Eq("spb.Session.Id", sessionId));
            criteria.Add(Restrictions.Eq("Status", Institute.EntityStatus.Active));
            return criteria.UniqueResult<StudentIdCard>();
        }

        public StudentIdCard getByStudentProgramId(long id)
        {
            return
                    Session.CreateCriteria<StudentIdCard>()
                        .Add(Restrictions.Eq("StudentProgram.Id", id))
                        .Add(Restrictions.Eq("Status", Institute.EntityStatus.Active))
                        .UniqueResult<StudentIdCard>();
        }

        #endregion

        #region List Loading Function
        
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion  
    }
}