﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class ShiftSettingController : Controller
    {
       #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HRArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private readonly IShiftService _hrShiftService;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> _userMenu; 
        public ShiftSettingController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _hrShiftService = new ShiftService(nHsession);
            _organizationService = new OrganizationService(nHsession);
        }

        #endregion

        #region Index/Manage Page

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ManageShiftSetting(string message)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (!String.IsNullOrEmpty(message))
                {

                    ViewBag.SuccessMessage = message;
                }
                var organizationSelectList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.Organization = organizationSelectList;
            }
            catch (Exception ex)
            {
               _logger.Error(ex);
            }
            return View();
        }

        public JsonResult LoadShiftSetting(int draw, long? organizationId = null)
        {
            var getData = new List<object>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Organization.ShortName";
                            break;
                        case "1":
                            orderBy = "Name";
                            break;
                        case "2":
                            orderBy = "ShoftDuration";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion
                int recordsTotal = _hrShiftService.GetShiftRowCount(_userMenu,orderBy, orderDir.ToUpper(), organizationId);
                long recordsFiltered = recordsTotal;
                List<Shift> shiftList = _hrShiftService.LoadShift(_userMenu,orderBy, orderDir.ToUpper(), organizationId).ToList();
                var data = new List<object>();
                foreach (var shift in shiftList)
                {
                    var str = new List<string> ();
                    var startTimeString = (shift.StartTime != null) ? shift.StartTime.Value.ToString("HH:mm tt") :"N/A";
                    var endTimeString = (shift.EndTime != null) ? shift.EndTime.Value.ToString("HH:mm tt") :"N/A";
                    str.Add(shift.Organization.ShortName);
                    str.Add(shift.Name);
                    str.Add(startTimeString + " to " + endTimeString);
                    str.Add(LinkGenerator.GetGeneratedLink("Edit", "Delete", "ShiftSetting", shift.Id, shift.Name));
                    data.Add(str);
                }
                
                return Json(new
                {
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    data = data,
                    isSuccess = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                recordsTotal = 0,
                recordsFiltered = 0,
                data = getData,
                isSuccess = false
            });
        }

        #endregion

        #region Operational Function

        #region Save Operation
        public ActionResult Create()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var organizationSelectList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.organizationList = organizationSelectList;
            return View();
        }

        [HttpPost]
        public ActionResult Create(ShiftViewModel shiftViewModel)
        {
            var successState = false;
            var successMessage = "";
            var customError = true;
            try
            {
                CheckEmptyField(shiftViewModel);
                Shift shiftObj = shiftViewModel.FillShiftValue(new Shift());
                shiftObj.Organization = _organizationService.LoadById(Convert.ToInt64(shiftViewModel.Organization));
                bool isSuccess = _hrShiftService.Save(shiftObj);
                if (isSuccess)
                {
                    customError = false;
                    successState = true;
                    successMessage = "Shift Setting successfully saved";
                }
            }
            catch (EmptyFieldException ex)
            {
                successMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                successMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                successMessage = ex.Message;
            }
            return Json(new { customError = customError, IsSuccess = successState, Message = successMessage });
        }

        #endregion

        #region Update Operation

        public ActionResult Edit(long id)
        {
            try
            {
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                GetStatusText();
                var shiftSettingObj = _hrShiftService.LoadById(id);
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", shiftSettingObj.Organization.Id);
                ShiftViewModel shiftviewModelObject = new ShiftViewModel(shiftSettingObj);
                
                return View(shiftviewModelObject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                GetStatusText();
                return View();
            }
        }

        [HttpPost]
        public JsonResult Edit(ShiftViewModel shiftViewModel)
        {
            var successState = false;
            var message = "";
            var customError = true;
            try
            {
                CheckEmptyField(shiftViewModel);
                var shiftSettingObj = _hrShiftService.LoadById((long)shiftViewModel.Id);
                if(shiftSettingObj == null)
                    throw new EmptyFieldException("Invalid Shift Found!");
                
                shiftSettingObj = shiftViewModel.FillShiftValue(shiftSettingObj);
                shiftSettingObj.Organization = _organizationService.LoadById(Convert.ToInt64(shiftViewModel.Organization));
                bool isSuccess = _hrShiftService.Update(shiftSettingObj);
                if (isSuccess)
                {
                    successState = true;
                    customError = false;
                    message = "Shift Setting successfully updated.";
                }
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occurred.";
                _logger.Error(ex);
            }
            return Json(new { customError = customError, IsSuccess = successState, Message = message });
        }

        #endregion

        #region Delete Operation

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var successState = false;
            var message = "";
            var customError = true;
            try
            {
                bool isSuccess = _hrShiftService.Delete(id);
                if (isSuccess)
                {
                    successState = true;
                    message = "Shift Setting sucessfully deleted.";
                }
            }
            catch (DependencyException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occurred.";
                _logger.Error(ex);
            }
            return Json(new { customError = customError, IsSuccess = successState, Message = message });
        }

        #endregion

        #region Details Operation
        #endregion

        #endregion

        #region Helper Function

        private void GetStatusText()
        {
            ViewBag.StatusText = _commonHelper.GetStatus();
        }
        
        private void CheckEmptyField(ShiftViewModel shiftViewModel)
        {
            if (String.IsNullOrEmpty(shiftViewModel.Name))
                throw new EmptyFieldException("Name is empty");
            if (shiftViewModel.Organization <= 0)
                throw new EmptyFieldException("No organization is empty");
            else
            {
                var organization = _organizationService.LoadById(Convert.ToInt64(shiftViewModel.Organization));
                if (organization == null)
                    throw new EmptyFieldException("No organization found!");
            }
            if (String.IsNullOrEmpty(shiftViewModel.StartTime))
                throw new EmptyFieldException("Start Time is empty");

            if (String.IsNullOrEmpty(shiftViewModel.StartTime))
                throw new EmptyFieldException("End Time is empty");

        }

        #endregion
    }
}