using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Hr
{
    public interface IOvertimeAllowanceSettingDao : IBaseDao<OvertimeAllowanceSetting, long>
    {
        #region Single Instances Loading Function

        OvertimeAllowanceSetting GetOvertimeAllowanceSetting(long organizationId, int employmentStatus, bool isWorkingDay, bool isManagementWeekend, bool isGazzeted, DateTime date);

        #endregion

        #region List Loading Function

        IList<OvertimeAllowanceSetting> LoadOvertimeAllowanceSetting(List<long> authorizedOrganization, int start, int length, string orderBy, string orderDir, long organizationId, bool isProbation, bool isPermanent, bool isPartTime, bool isContractual, bool isIntern, bool isWorkingDay, bool isGazzeted, bool isManagementWeekend);

        #endregion

        #region Others Function
   
        int GetOvertimeAllowanceSettingCount(string orderBy, string orderDir, long organizationId, bool isProbation, bool isPermanent, bool isPartTime, bool isContractual, bool isIntern, bool isRegular, bool isGazetted, bool isManagementWeekend);
        
        #endregion

        #region Helper Function

        bool HasDuplicate(OvertimeAllowanceSetting overtimeAllowanceSetting);
        bool IsOvertimeAllowanceSettingNameExist(string name, long organizationId, long? id);

        #endregion
    }

    public class OvertimeAllowanceSettingDao : BaseDao<OvertimeAllowanceSetting, long>, IOvertimeAllowanceSettingDao
    {
        #region Properties & Object & Initialization

        public OvertimeAllowanceSettingDao()
        {
          
        }

        #endregion

        #region Single Instances Loading Function

        public bool IsOvertimeAllowanceSettingNameExist(string name, long organizationId, long? id)
        {
            ICriteria criteria = Session.CreateCriteria<OvertimeAllowanceSetting>().Add(Restrictions.Eq("Status", OvertimeAllowanceSetting.EntityStatus.Active));
            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active)).Add(Restrictions.Eq("organization.Id", organizationId));
            criteria.Add(Restrictions.Eq("Name", name));
            if (id > 0)
                criteria.Add(!Restrictions.Eq("Id", id));

            criteria.SetTimeout(3000);
            criteria.SetProjection(Projections.RowCount());
            bool returnValue = false;
            int count = Convert.ToInt32(criteria.UniqueResult());
            return returnValue = count > 0 ? true : false;
        }

        public OvertimeAllowanceSetting GetOvertimeAllowanceSetting(long organizationId, int employmentStatus, bool isWorkingDay, bool isManagementWeekend, bool isGazzeted, DateTime date)
        {
            ICriteria criteria = Session.CreateCriteria<OvertimeAllowanceSetting>().Add(Restrictions.Eq("Status", OvertimeAllowanceSetting.EntityStatus.Active));
            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active)).Add(Restrictions.Eq("organization.Id", organizationId));

            criteria.Add(Restrictions.Le("EffectiveDate", date.Date));
            var disjunction = Restrictions.Disjunction(); // for OR statement 
            disjunction.Add(Restrictions.IsNull("ClosingDate") || Restrictions.Ge("ClosingDate", date.Date));
            criteria.Add(disjunction);

            if (employmentStatus != (int)MemberEmploymentStatus.Permanent)
            {
                criteria.Add(Restrictions.Eq("IsPermanent", true));
            }
            else if (employmentStatus != (int)MemberEmploymentStatus.Probation)
            {
                criteria.Add(Restrictions.Eq("IsProbation", true));
            }
            else if (employmentStatus != (int)MemberEmploymentStatus.PartTime)
            {
                criteria.Add(Restrictions.Eq("IsPartTime", true));
            }
            else if (employmentStatus != (int)MemberEmploymentStatus.Contractual)
            {
                criteria.Add(Restrictions.Eq("IsContractual", true));
            }

            criteria.Add(Restrictions.Eq("IsWorkingDay", isWorkingDay));
            criteria.Add(Restrictions.Eq("IsManagementWeekend", isManagementWeekend));
            criteria.Add(Restrictions.Eq("IsGazzeted", isGazzeted));

            criteria.AddOrder(Order.Desc("EffectiveDate")).AddOrder(Order.Desc("MultiplyingFactor"));

            criteria.SetTimeout(3000);
            return criteria.SetMaxResults(1).UniqueResult<OvertimeAllowanceSetting>();
        }

        #endregion

        #region List Loading Function

        public IList<OvertimeAllowanceSetting> LoadOvertimeAllowanceSetting(List<long> authorizedOrganization, int start, int length, string orderBy, string orderDir, long organizationId, bool isProbation, bool isPermanent, bool isPartTime, bool isContractual, bool isIntern, bool isWorkingDay, bool isGazzeted, bool isManagementWeekend)
        {
            ICriteria criteria = GetOvertimeAllowanceSettingCriteria(orderBy, orderDir, organizationId, isProbation, isPermanent, isPartTime, isContractual, isIntern, isWorkingDay, isGazzeted, isManagementWeekend);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<OvertimeAllowanceSetting>();

        }


        #endregion

        #region Others Function

        public int GetOvertimeAllowanceSettingCount(string orderBy, string orderDir, long organizationId, bool isProbation, bool isPermanent, bool isPartTime, bool isContractual, bool isIntern, bool isRegular, bool isGazetted, bool isManagementWeekend)
        {
            int returnValue = 0;
            ICriteria criteria = GetOvertimeAllowanceSettingCriteria(orderBy, orderDir, organizationId, isProbation, isPermanent, isPartTime, isContractual, isIntern, isRegular, isGazetted, isManagementWeekend, true);
            criteria.SetProjection(Projections.RowCount());
            returnValue = Convert.ToInt32(criteria.UniqueResult());
            return returnValue;

        }

        public bool HasDuplicate(OvertimeAllowanceSetting overtimeAllowanceSetting)
        {
            ICriteria criteria = Session.CreateCriteria<OvertimeAllowanceSetting>().Add(Restrictions.Eq("Status", OvertimeAllowanceSetting.EntityStatus.Active));
            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active)).Add(Restrictions.Eq("organization.Id", overtimeAllowanceSetting.Organization.Id));
            criteria.Add(Restrictions.Le("EffectiveDate", overtimeAllowanceSetting.EffectiveDate));

            var disjunction = Restrictions.Disjunction(); // for OR statement 
            disjunction.Add(Restrictions.IsNull("ClosingDate") || Restrictions.Ge("ClosingDate", overtimeAllowanceSetting.EffectiveDate));
            criteria.Add(disjunction);
            var forEmployeeStatus = Restrictions.Disjunction();
            if (overtimeAllowanceSetting.IsProbation && overtimeAllowanceSetting.IsPermanent && overtimeAllowanceSetting.IsPartTime && overtimeAllowanceSetting.IsContractual && overtimeAllowanceSetting.IsIntern)
            {
                forEmployeeStatus.Add(Restrictions.Eq("IsPermanent", overtimeAllowanceSetting.IsPermanent)
                    || Restrictions.Eq("IsProbation", overtimeAllowanceSetting.IsProbation)
                    || Restrictions.Eq("IsPartTime", overtimeAllowanceSetting.IsPartTime)
                    || Restrictions.Eq("IsContractual", overtimeAllowanceSetting.IsContractual)
                    || Restrictions.Eq("IsIntern", overtimeAllowanceSetting.IsIntern)
                );
            }
            else
            {
                if (overtimeAllowanceSetting.IsPermanent)
                    forEmployeeStatus.Add(Restrictions.Eq("IsPermanent", overtimeAllowanceSetting.IsPermanent));
                if (overtimeAllowanceSetting.IsProbation)
                    forEmployeeStatus.Add(Restrictions.Eq("IsProbation", overtimeAllowanceSetting.IsProbation));
                if (overtimeAllowanceSetting.IsPartTime)
                    forEmployeeStatus.Add(Restrictions.Eq("IsPartTime", overtimeAllowanceSetting.IsPartTime));
                if (overtimeAllowanceSetting.IsContractual)
                    forEmployeeStatus.Add(Restrictions.Eq("IsContractual", overtimeAllowanceSetting.IsContractual));
                if (overtimeAllowanceSetting.IsIntern)
                    forEmployeeStatus.Add(Restrictions.Eq("IsIntern", overtimeAllowanceSetting.IsIntern));
            }

            if (forEmployeeStatus != null)
            {
                criteria.Add(forEmployeeStatus);
            }

            var forOvertimeStatus = Restrictions.Disjunction();
            if (overtimeAllowanceSetting.IsWorkingDay && overtimeAllowanceSetting.IsManagementWeekend && overtimeAllowanceSetting.IsGazzeted)
            {
                forOvertimeStatus.Add(Restrictions.Eq("IsWorkingDay", overtimeAllowanceSetting.IsWorkingDay)
                    || Restrictions.Eq("IsManagementWeekend", overtimeAllowanceSetting.IsManagementWeekend)
                    || Restrictions.Eq("IsGazzeted", overtimeAllowanceSetting.IsGazzeted));
            }
            else
            {
                if (overtimeAllowanceSetting.IsWorkingDay)
                    forOvertimeStatus.Add(Restrictions.Eq("IsWorkingDay", overtimeAllowanceSetting.IsWorkingDay));
                if (overtimeAllowanceSetting.IsManagementWeekend)
                    forOvertimeStatus.Add(Restrictions.Eq("IsManagementWeekend", overtimeAllowanceSetting.IsManagementWeekend));
                if (overtimeAllowanceSetting.IsGazzeted)
                    forOvertimeStatus.Add(Restrictions.Eq("IsGazzeted", overtimeAllowanceSetting.IsGazzeted));
            }

            if (forOvertimeStatus != null)
            {
                criteria.Add(forOvertimeStatus);
            }

            if (overtimeAllowanceSetting.Id > 0)
                criteria.Add(!Restrictions.Eq("Id", overtimeAllowanceSetting.Id));

            criteria.SetTimeout(3000);
            criteria.SetProjection(Projections.RowCount());
            bool returnValue = false;
            int count = Convert.ToInt32(criteria.UniqueResult());
            return returnValue = count > 0 ? true : false;
        }
        
        
        #endregion

        #region Helper Function

        private ICriteria GetOvertimeAllowanceSettingCriteria(string orderBy, string orderDir, long organizationId, bool isProbation, bool isPermanent, bool isPartTime, bool isContractual, bool isIntern, bool isWorkingDay, bool isGazzeted, bool isManagementWeekend, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<OvertimeAllowanceSetting>().Add(Restrictions.Not(Restrictions.Eq("Status", OvertimeAllowanceSetting.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Not(Restrictions.Eq("organization.Status", Organization.EntityStatus.Delete)));

            if (organizationId > 0)
            {
                criteria.Add(Restrictions.Eq("organization.Id", organizationId));
            }

            if (isProbation)
                criteria.Add(Restrictions.Eq("IsProbation", isProbation));
            if (isPermanent)
                criteria.Add(Restrictions.Eq("IsPermanent", isPermanent));
            if (isPartTime)
                criteria.Add(Restrictions.Eq("IsPartTime", isPartTime));
            if (isContractual)
                criteria.Add(Restrictions.Eq("IsContractual", isContractual));
            if (isIntern)
                criteria.Add(Restrictions.Eq("IsIntern", isIntern));
            if (isWorkingDay)
                criteria.Add(Restrictions.Eq("IsWorkingDay", isWorkingDay));
            if (isGazzeted)
                criteria.Add(Restrictions.Eq("IsGazzeted", isGazzeted));
            if (isManagementWeekend)
                criteria.Add(Restrictions.Eq("IsManagementWeekend", isManagementWeekend));

            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }

        #endregion
    }
}
