﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface IBatchService : IBaseService
    {
        #region Operational Function
        bool Save(Batch batch);
        bool Update(long id, Batch batch);
        bool Delete(long batchId, Batch batch);
        bool UpdateRank(Batch batchOldObj, Batch batchUpdatedObj);
        #endregion

        #region Single Instances Loading Function
        Batch GetBatch(long batchId);
        Batch GetBatchAuthorize(long batchId, List<UserMenu> userMenus);
        Batch GetBatchByRankNextOrPrevious(int rank, string action);
        #endregion

        #region List Loading Function
        IList<Batch> LoadBatch(List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> batchIdList = null, string[] batchDaysList = null, string[] batchTimeList = null, List<int> gender = null);
        IList<Batch> LoadBatch(List<UserMenu> userMenus, int start, int length, string orderBy, string direction, long? organizationId, long? programId, string sessionName, long? branchId, string campusName, string batchName, string batchStatus);
        IList<Batch> LoadBatch(long[] batchIds);
        IList<Batch> LoadAuthorizeBatch(List<UserMenu> userMenus, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null, List<long> campusIdList = null, string[] batchDaysList = null, string[] batchTimeList = null, List<int> versionOfStudyList = null, List<int> genderList = null);
        #endregion

        #region Others Function
        int GetBatchCount(List<UserMenu> userMenus, string orderBy, string direction, long? organizationId, long? programId, string sessionName, long? branchId, string campusName, string batchName, string batchStatus);
        int GetMaximumRank(Batch batch);
        int GetMinimumRank(Batch batch);
        #endregion

        #region Helper Function
        IList<string> LoadAuthorizeBatchGroupByDays(List<UserMenu> userMenus, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null, List<long> campusIdList = null, List<int> versionOfStudyList = null, List<int> genderList = null);
        IList<string> LoadBatchGroupByDays(List<long> organizationIds, List<long> programIds, List<long> branchIds, List<long> sessionIds, List<long> campusIds, List<int> selectedVersions, List<int> selectedGenders);
        IList<string> LoadAuthorizeBatchGroupByTimes(List<UserMenu> userMenus, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null, List<long> campusIdList = null, string[] batchDaysList = null, List<int> versionOfStudyList = null, List<int> genderList = null);
        IList<string> LoadBatchGroupByTimes(List<long> organizationIds, List<long> programIds, List<long> branchIds, List<long> sessionIds, List<long> campusIds, string[] batchDays, List<int> selectedVersions, List<int> selectedGenders);
        #endregion

        #region Public API
        IList<string> LoadPublicBatchDay(string orgBusinessId, long programId, long sessionId, long branchId, long campusId, long versionOfStudy = 0, long gender = 0);

        IList<string> LoadPublicBatchTime(string orgBusinessId, long programId, long sessionId, long branchId, long campusId, string[] batchDaysList, long versionOfStudy = 0, long gender = 0);

        IList<Batch> LoadPublicBatch(string orgBusinessId, long programId, long sessionId, long branchId, long campusId, string[] batchDaysList, string[] batchTimeList, long versionOfStudy = 0, long gender = 0);
        #endregion
    }
    public class BatchService : BaseService, IBatchService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IBatchDao _batchDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly CommonHelper _commonHelper;
        public BatchService(ISession session)
        {
            Session = session;
            _batchDao = new BatchDao() { Session = session };
            _studentProgramDao = new StudentProgramDao() { Session = session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public bool Save(Batch batchObj)
        {
            ITransaction trans = null;
            try
            {
                if (batchObj == null)
                {
                    throw new NullObjectException("batch can not be null");
                }
                DoBeforeAdd(batchObj);
                if (batchObj.StartTime != null && batchObj.EndTime != null)
                {
                    batchObj.StartTime = new DateTime(2000, 01, 01, batchObj.StartTime.Value.Hour, batchObj.StartTime.Value.Minute, batchObj.StartTime.Value.Second);
                    batchObj.EndTime = new DateTime(2000, 01, 01, batchObj.EndTime.Value.Hour, batchObj.EndTime.Value.Minute, batchObj.EndTime.Value.Second);
                }
                using (trans = Session.BeginTransaction())
                {
                    batchObj.Rank = _batchDao.GetMaximumRank(batchObj) + 1;
                    _batchDao.Save(batchObj);
                    trans.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool Update(long id, Batch batchObj)
        {
            ITransaction trans = null;
            try
            {
                if (batchObj == null)
                    throw new NullObjectException("batch can not be null");

                var dbBatch = _batchDao.LoadById(id);
                DoBeforeUdpate(batchObj, dbBatch);
                if (batchObj.StartTime != null && batchObj.EndTime != null)
                {
                    batchObj.StartTime = new DateTime(2000, 01, 01, batchObj.StartTime.Value.Hour, batchObj.StartTime.Value.Minute, batchObj.StartTime.Value.Second);
                    batchObj.EndTime = new DateTime(2000, 01, 01, batchObj.EndTime.Value.Hour, batchObj.EndTime.Value.Minute, batchObj.EndTime.Value.Second);
                }
                using (trans = Session.BeginTransaction())
                {
                    dbBatch.Program = batchObj.Program;
                    dbBatch.Session = batchObj.Session;
                    dbBatch.Branch = batchObj.Branch;
                    dbBatch.Campus = batchObj.Campus;
                    dbBatch.Days = batchObj.Days;
                    dbBatch.StartTime = batchObj.StartTime;
                    dbBatch.EndTime = batchObj.EndTime;
                    dbBatch.Name = batchObj.Name;
                    dbBatch.Capacity = batchObj.Capacity;
                    dbBatch.Status = batchObj.Status;
                    dbBatch.VersionOfStudy = batchObj.VersionOfStudy;
                    dbBatch.Gender = batchObj.Gender;
                    _batchDao.Update(dbBatch);
                    trans.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool Delete(long batchId, Batch batchObj)
        {
            ITransaction trans = null;
            try
            {
                DoBeforeDelete(batchObj);
                using (trans = Session.BeginTransaction())
                {
                    Batch dbBatch = _batchDao.LoadById(batchId);
                    dbBatch.Status = Batch.EntityStatus.Delete;
                    _batchDao.Update(dbBatch);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool UpdateRank(Batch batchOldObj, Batch batchUpdatedObj)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    try
                    {
                        _batchDao.Update(batchOldObj);
                        _batchDao.Update(batchUpdatedObj);
                        trans.Commit();
                        return true;
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public Batch GetBatch(long batchId)
        {
            Batch batch;
            try
            {
                batch = _batchDao.LoadById(batchId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batch;
        }
        public Batch GetBatchAuthorize(long batchId, List<UserMenu> userMenus)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenus);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenus);
                return _batchDao.GetBatchAuthorize(batchId, branchIdList, programIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public Batch GetBatchByRankNextOrPrevious(int rank, string action)
        {
            Batch batch;
            try
            {
                batch = _batchDao.LoadByRankDirection(rank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batch;
        }

        #endregion

        #region List Loading Function

        public IList<Batch> LoadAuthorizeBatch(List<UserMenu> userMenus, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null, List<long> campusIdList = null, string[] batchDaysList = null, string[] batchTimeList = null, List<int> versionOfStudyList = null, List<int> genderList = null)
        {
            IList<Batch> batchList;
            try
            {
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList), CommonHelper.ConvertSelectedAllIdList(programIdList));
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(programIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList));
                var versionOfStudyLists = GetVersionOfStudyLists(versionOfStudyList);
                var genderLists = GetGenderLists(genderList);
                batchList = _batchDao.LoadAuthorizeBatch(authProgramIdList, authBranchIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList), CommonHelper.ConvertSelectedAllIdList(campusIdList), batchDaysList, batchTimeList, versionOfStudyLists, genderLists);
                //calling private function
                batchList = LoadAuthorizeBatch(batchList, batchDaysList, batchTimeList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchList;
        }

        public IList<Batch> LoadBatch(List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> batchIdList = null, string[] batchDaysList = null, string[] batchTimeList = null, List<int> genderList = null)
        {
            IList<Batch> batches;
            try
            {
                var genderLists = GetGenderLists(genderList);
                batches = _batchDao.LoadBatch(organizationIdList, programIdList, sessionIdList, branchIdList, campusIdList, batchIdList, batchDaysList, batchTimeList, genderLists);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batches;
        }

        public IList<Batch> LoadBatch(List<UserMenu> userMenus, int start, int length, string orderBy, string direction, long? organizationId, long? programId, string sessionName, long? branchId, string campusName, string batchName, string batchStatus)
        {
            IList<Batch> batches;
            try
            {
                List<long> notNullOrganizationList = (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null;
                List<long> notNullProgramList = (programId != null) ? _commonHelper.ConvertIdToList((long)programId) : null;
                List<long> notNullBranchList = (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null;
                List<ProgramBranchPair> programBranchPairList = AuthHelper.LoadProgramBranchPairList(userMenus, notNullOrganizationList, notNullProgramList, notNullBranchList);
                batches = _batchDao.LoadBatch(programBranchPairList, start, length, orderBy, direction, organizationId, sessionName, campusName, batchName, batchStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batches;
        }

        public IList<Batch> LoadBatch(long[] batchIds)
        {
            try
            {
                return _batchDao.LoadBatch(batchIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        #endregion

        #region Others Function

        public int GetBatchCount(List<UserMenu> userMenus, string orderBy, string direction, long? organizationId, long? programId, string sessionName, long? branchId, string campusName, string batchName, string batchStatus)
        {
            int batchCount;
            try
            {
                List<long> notNullOrganizationList = (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null;
                List<long> notNullProgramList = (programId != null) ? _commonHelper.ConvertIdToList((long)programId) : null;
                List<long> notNullBranchList = (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null;
                List<ProgramBranchPair> programBranchPairList = AuthHelper.LoadProgramBranchPairList(userMenus, notNullOrganizationList, notNullProgramList, notNullBranchList);
                batchCount = _batchDao.GetBatchCount(programBranchPairList, orderBy, direction, organizationId, sessionName, campusName, batchName, batchStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchCount;
        }

        public int GetMaximumRank(Batch batch)
        {
            int maximumRank;
            try
            {
                maximumRank = _batchDao.GetMaximumRank(batch);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return maximumRank;
        }

        public int GetMinimumRank(Batch batch)
        {
            int minimumRank;
            try
            {
                minimumRank = _batchDao.GetMinimumRank(batch);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return minimumRank;
        }


        #endregion

        #region Helper function

        //Data annotation validation check  
       private void DoBeforeAdd(Batch batchObj)
        {
            CustomModelValidationCheck.DataAnnotationCheck(batchObj,new BatchMetaData());
            if (batchObj.StartTime >= batchObj.EndTime)
                throw new MessageException("Batch start time grater then or equal to end time!");

            //duplicate batch check   
            bool duplicateFound = _batchDao.CheckDuplicateBatch(batchObj, false);
            if (duplicateFound)
                throw new DuplicateEntryException("Duplicate batch found");
        }

        private void DoBeforeUdpate(Batch batchObj, Batch dbBatch)
        {
            CustomModelValidationCheck.DataAnnotationCheck(batchObj,new BatchMetaData());
            if (batchObj.StartTime >= batchObj.EndTime)
                throw new MessageException("Batch start time grater then or equal to end time!");
            bool duplicateFound = _batchDao.CheckDuplicateBatch(batchObj, true);
            var batchHasStudent = _studentProgramDao.GetBatchStudentCount(batchObj);
            if (batchObj.Status == Batch.EntityStatus.Inactive)
            {
                if (batchHasStudent > 0)
                    throw new DependencyException("This Batch Has " + batchHasStudent + " Student(s).");
            }
            if (duplicateFound)
                throw new DuplicateEntryException("Duplicate batch found");
            if (batchObj.Program != dbBatch.Program || batchObj.Session != dbBatch.Session ||
                batchObj.Branch != dbBatch.Branch)
            {
                if (batchHasStudent > 0)
                    throw new DependencyException("This Batch Has " + batchHasStudent + " Student(s).");
            }
        }

        private void DoBeforeDelete(Batch batchObj)
        {
            //campus assign check
            if (batchObj == null)
                throw new ValueNotAssignedException("Batch is not assigned");
            bool dependencyFound = _batchDao.HasDependency(batchObj);
            if (dependencyFound)
                throw new DependencyException("Dependency found");
        }

        private List<int> GetGenderLists(List<int> genderList = null)
        {
            var genderLists = new List<int>();
            try
            {
                if (genderList != null)
                {
                    if (genderList.Contains(0))
                    {
                        genderLists.Add(Convert.ToInt32(Gender.Male));
                        genderLists.Add(Convert.ToInt32(Gender.Female));
                        genderLists.Add(Convert.ToInt32(Gender.Combined));
                    }
                    else
                    {
                        if (genderList.Contains(1))
                            genderLists.Add(Convert.ToInt32(Gender.Male));
                        if (genderList.Contains(2))
                            genderLists.Add(Convert.ToInt32(Gender.Female));
                        if (genderList.Contains(3))
                            genderLists.Add(Convert.ToInt32(Gender.Combined));
                        if (!genderList.Contains(3))
                            genderLists.Add(Convert.ToInt32(Gender.Combined));
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return genderLists;
        }

        private List<int> GetVersionOfStudyLists(List<int> versionOfStudy = null)
        {
            var versionOfStudyLists = new List<int>();
            try
            {
                if (versionOfStudy != null)
                {
                    if (versionOfStudy.Contains(0))
                    {
                        versionOfStudyLists.Add(Convert.ToInt32(VersionOfStudy.Bangla));
                        versionOfStudyLists.Add(Convert.ToInt32(VersionOfStudy.English));
                        versionOfStudyLists.Add(Convert.ToInt32(VersionOfStudy.Combined));
                    }
                    else
                    {
                        if (versionOfStudy.Contains(1))
                        {
                            versionOfStudyLists.Add(Convert.ToInt32(VersionOfStudy.Bangla));
                        }
                        if (versionOfStudy.Contains(2))
                        {
                            versionOfStudyLists.Add(Convert.ToInt32(VersionOfStudy.English));
                        }

                        if (versionOfStudy.Contains(3))
                        {
                            versionOfStudyLists.Add(Convert.ToInt32(VersionOfStudy.Combined));
                        }
                        if (!versionOfStudy.Contains(3))
                            versionOfStudyLists.Add(Convert.ToInt32(VersionOfStudy.Combined));
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return versionOfStudyLists;
        }

        public IList<string> LoadBatchGroupByDays(List<long> organizationIds, List<long> programIds, List<long> branchIds, List<long> sessionIds, List<long> campusIds, List<int> selectedVersions, List<int> selectedGenders)
        {
            IList<string> batchDayList;
            try
            {
                var versionOfStudyLists = GetVersionOfStudyLists(selectedVersions);
                var genderLists = GetGenderLists(selectedGenders);
                var batchList = _batchDao.LoadBatch(organizationIds, programIds, sessionIds, branchIds, campusIds, null, null, null, genderLists);
                batchDayList = (from x in batchList select x.Days).Distinct().ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchDayList;
        }

        public IList<string> LoadBatchGroupByTimes(List<long> organizationIds, List<long> programIds, List<long> branchIds, List<long> sessionIds, List<long> campusIds, string[] batchDays, List<int> selectedVersions, List<int> selectedGenders)
        {
            IList<string> batchTimeList;
            try
            {

                var versionOfStudyLists = GetVersionOfStudyLists(selectedVersions);
                var genderLists = GetGenderLists(selectedGenders);
                var batchList = _batchDao.LoadBatch(organizationIds, programIds, sessionIds, branchIds, campusIds, null, null, null, genderLists);
                if (batchDays != null)
                {
                    //calling private function
                    batchList = LoadBatch(batchList, batchDays);
                }
                batchTimeList = (from x in batchList select x.FormatedBatchTime).Distinct().ToList();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchTimeList;
        }

        public IList<string> LoadAuthorizeBatchGroupByDays(List<UserMenu> userMenus, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null, List<long> campusIdList = null, List<int> versionOfStudyList = null, List<int> genderList = null)
        {
            IList<string> batchDayList;
            try
            {
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList), CommonHelper.ConvertSelectedAllIdList(programIdList));
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(programIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList));
                var versionOfStudyLists = GetVersionOfStudyLists(versionOfStudyList);
                var genderLists = GetGenderLists(genderList);
                var batchList = _batchDao.LoadAuthorizeBatch(authProgramIdList, authBranchIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList), CommonHelper.ConvertSelectedAllIdList(campusIdList), null, null, versionOfStudyLists, genderLists);
                batchDayList = (from x in batchList select x.Days).Distinct().ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchDayList;
        }

        public IList<string> LoadAuthorizeBatchGroupByTimes(List<UserMenu> userMenus, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null, List<long> campusIdList = null, string[] batchDaysList = null, List<int> versionOfStudyList = null, List<int> genderList = null)
        {
            IList<string> batchTimeList;
            try
            {
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList), CommonHelper.ConvertSelectedAllIdList(programIdList));
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(programIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList));

                var versionOfStudyLists = GetVersionOfStudyLists(versionOfStudyList);
                var genderLists = GetGenderLists(genderList);
                var batchList = _batchDao.LoadAuthorizeBatch(authProgramIdList, authBranchIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList), CommonHelper.ConvertSelectedAllIdList(campusIdList), batchDaysList, null, versionOfStudyLists, genderLists);
                if (batchDaysList != null)
                {
                    //calling private function
                    batchList = LoadAuthorizeBatch(batchList, batchDaysList);
                }
                batchTimeList = (from x in batchList select x.FormatedBatchTime).Distinct().ToList();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchTimeList;
        }

        private IList<Batch> LoadAuthorizeBatch(IList<Batch> batchList, string[] batchDaysList, string[] batchTimeList = null)
        {
            try
            {
                if (batchDaysList != null)
                {
                    if (!batchDaysList.Contains(SelectionType.SelelectAll.ToString()))
                        batchList = (from x in batchList where batchDaysList.Any(y => y == x.Days) select x).ToList();
                }
                if (batchTimeList != null)
                {
                    if (!batchTimeList.Contains(SelectionType.SelelectAll.ToString()))
                        batchList = (from x in batchList where batchTimeList.Any(y => y == x.FormatedBatchTime) select x).ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchList;
        }

        private IList<Batch> LoadBatch(IList<Batch> batchList, string[] batchDaysList, string[] batchTimeList = null)
        {
            try
            {
                if (batchDaysList != null)
                {
                    if (!batchDaysList.Contains(SelectionType.SelelectAll.ToString()))
                        batchList = (from x in batchList where batchDaysList.Any(y => y == x.Days) select x).ToList();
                }
                if (batchTimeList != null)
                {
                    if (!batchTimeList.Contains(SelectionType.SelelectAll.ToString()))
                        batchList = (from x in batchList where batchTimeList.Any(y => y == x.FormatedBatchTime) select x).ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchList;
        }

        #endregion

        #region Public API

        public IList<string> LoadPublicBatchDay(string orgBusinessId, long programId, long sessionId, long branchId, long campusId, long versionOfStudy = 0, long gender = 0)
        {
            IList<string> batchDayList;
            try
            {
                var batchList = _batchDao.LoadPublicBatch(orgBusinessId, programId, sessionId, branchId, campusId, versionOfStudy, gender);
                batchDayList = (from x in batchList select x.Days).Distinct().ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchDayList;
        }

        public IList<string> LoadPublicBatchTime(string orgBusinessId, long programId, long sessionId, long branchId, long campusId, string[] batchDaysList, long versionOfStudy = 0, long gender = 0)
        {
            IList<string> batchTimeList;
            try
            {
                var batchList = _batchDao.LoadPublicBatch(orgBusinessId, programId, sessionId, branchId, campusId, versionOfStudy, gender);
                //calling private function
                batchList = LoadAuthorizeBatch(batchList, batchDaysList);
                batchTimeList = (from x in batchList select x.FormatedBatchTime).Distinct().ToList();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchTimeList;
        }

        public IList<Batch> LoadPublicBatch(string orgBusinessId, long programId, long sessionId, long branchId, long campusId, string[] batchDaysList, string[] batchTimeList, long versionOfStudy = 0, long gender = 0)
        {
            IList<Batch> batchList;
            try
            {
                batchList = _batchDao.LoadPublicBatch(orgBusinessId, programId, sessionId, branchId, campusId, versionOfStudy, gender);
                //calling private function
                batchList = LoadAuthorizeBatch(batchList, batchDaysList, batchTimeList);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchList;
        }

        #endregion
    }
}