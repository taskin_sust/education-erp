﻿using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MenuTest
{
    interface IMenuDeleteTest
    {
        #region Basic Test
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Menu")]
    public class MenuDeleteTest : MenuBaseTest, IMenuDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            var menuFactory =
                _menuFactory.Create().WithNameAndRank(_name, 1).WithMenuGroup().WithAction()
                    .Persist();
            Menu menu = _menuService.MenuLoadById(menuFactory.Object.Id);
            var success = _menuService.Delete(menu);
            Assert.True(success);
            menu = _menuService.MenuLoadById(menuFactory.Object.Id);
            Assert.Null(menu);

        }

        [Fact]
        public void Delete_Should_Check_Dependency()
        {
            var menuFactory =
                _menuFactory.Create().WithNameAndRank(_name, 1).WithMenuGroup().WithAction()
                    .Persist();
            //bool succe = _menuService.Delete(menuFactory.Object);
            menuFactory.Object.Menus=new List<Menu>()
            {
                new Menu()
            };
            Assert.Throws<DependencyException>(() => _menuService.Delete(menuFactory.Object));
        }
        #endregion

    }
}
