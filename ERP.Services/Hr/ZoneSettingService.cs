﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;

namespace UdvashERP.Services.Hr
{
    public interface IZoneSettingService : IBaseService
    {

        #region Operational Function

        bool Save(ZoneSetting zoneSetting);
        bool Update(long id,string name, string organizationId, string toleranceTime, string toleranceDay, string deductionMultiplier, string status);
        bool Delete(long id);

        #endregion

        #region Single Instances Loading Function

        ZoneSetting LoadById(long id);

        #endregion

        #region List Loading Function

        IList<ZoneSetting> LoadZoneSetting(List<UserMenu> userMenu,string orderBy, string direction, long? organizationId);

        IList<BusinessModel.Dto.Hr.TeamMemberZoneDto> TeamMemberZoneList(DateTime startDate, DateTime endTime, long teamMemberId, int employmentStatus
                        , decimal salary, decimal basicSalary, int monthlyWorkingDay,long orgId);

        #endregion

        #region Others Function

        int GetZoneSettingRowCount(List<UserMenu> userMenu ,string orderBy, string direction, long? organizationId);

        #endregion

        #region Helper Function

        #endregion
        
    }
    public class ZoneSettingService : BaseService, IZoneSettingService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IZoneSettingDao _hrZoneSettingDao;
        private readonly IOrganizationDao _organizationDao;
        public ZoneSettingService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrZoneSettingDao = new ZoneSettingDao { Session = session };
            _organizationDao = new OrganizationDao{Session =  session};
        }

        #endregion

        #region Operational Functions

        public bool Save(ZoneSetting zoneSetting)
        {

            if (zoneSetting == null)
            {
                throw new NullObjectException("zone can not be null.");
            }
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    string name = zoneSetting.Name.Trim();
                    string organizationId = zoneSetting.Organization.Id.ToString();
                    string toleranceTime=zoneSetting.ToleranceTime.ToString();
                    string toleranceDay = zoneSetting.ToleranceDay.ToString();
                    string deductionMultiplier=zoneSetting.DeductionMultiplier.ToString();
                    //check before save
                    CheckEmptyField(name, organizationId, toleranceTime, toleranceDay, deductionMultiplier);
                    CheckBeforeSave(name, organizationId, toleranceTime);
                    ZoneSetting hrZoneSetting = zoneSetting;
                    hrZoneSetting.Rank = 10;
                    if (!String.IsNullOrEmpty(toleranceDay))
                        hrZoneSetting.ToleranceDay = Convert.ToInt32(toleranceDay);
                    else
                    {
                        hrZoneSetting.ToleranceDay = null;
                    }
                    if (!String.IsNullOrEmpty(deductionMultiplier))
                        hrZoneSetting.DeductionMultiplier = Convert.ToDecimal(deductionMultiplier);
                    else
                    {
                        hrZoneSetting.DeductionMultiplier = null;
                    }
                    _hrZoneSettingDao.Save(hrZoneSetting);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool Update(long id, string name, string organizationId, string toleranceTime, string toleranceDay, string deductionMultiplier, string status)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    //check before save
                    CheckEmptyField(name, organizationId, toleranceTime, toleranceDay, deductionMultiplier);
                    CheckBeforeSave(name, organizationId, toleranceTime, id);
                    var organization = _organizationDao.LoadById(Convert.ToInt64(organizationId));
                    ZoneSetting hrZoneSetting = _hrZoneSettingDao.LoadById(id);
                    hrZoneSetting.Name = name;
                    hrZoneSetting.Organization = organization;
                    hrZoneSetting.ToleranceTime = Convert.ToInt32(toleranceTime);
                    if (!String.IsNullOrEmpty(toleranceDay))
                        hrZoneSetting.ToleranceDay = Convert.ToInt32(toleranceDay);
                    else
                    {
                        hrZoneSetting.ToleranceDay = null;
                    }
                    if (!String.IsNullOrEmpty(deductionMultiplier))
                        hrZoneSetting.DeductionMultiplier = Convert.ToDecimal(deductionMultiplier);
                    else
                    {
                        hrZoneSetting.DeductionMultiplier = null;
                    }
                    // TODO only inactive if 

                    if (Convert.ToInt32(status) == 1 || Convert.ToInt32(status) == -1)
                    {
                        hrZoneSetting.Status = Convert.ToInt32(status);
                    }
                    _hrZoneSettingDao.Update(hrZoneSetting);
                    transaction.Commit();
                    return true;
                }
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool Delete(long id)
        {
            if (id == 0)
            {
                throw new NullObjectException("id can not be zero.");
            }
            ITransaction transaction = null;
            try
            {
                var tempZoneSettingObj = _hrZoneSettingDao.LoadById(id);  
                // TODO check if any member has this Zone can't delete it

                int attendanceSummaryCount = tempZoneSettingObj
                    .AttendanceSummary
                    .Where(x => x.Status != AttendanceSummary.EntityStatus.Delete)
                    .ToList()
                    .Count();
                if (attendanceSummaryCount > 0)
                {
                    throw new DependencyException("This Zone has total " + attendanceSummaryCount.ToString() +
                                                  " record(s). You can't delete this zone right now.");
                }

                using (transaction = Session.BeginTransaction())
                {
                    tempZoneSettingObj.Status = ZoneSetting.EntityStatus.Delete;
                    _hrZoneSettingDao.Update(tempZoneSettingObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        
        #endregion

        #region Single Instances Loading Function

        public ZoneSetting LoadById(long id)
        {
            ZoneSetting heZoneSetting;
            try
            {
                if (id <= 0)
                {
                    throw new InvalidDataException("id can not be zero or less");
                }
                heZoneSetting = _hrZoneSettingDao.LoadById(id);
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return heZoneSetting;
        }

        #endregion

        #region List Loading Function

        public IList<ZoneSetting> LoadZoneSetting(string orderBy, string direction, long? organizationId)
        {
            IList<ZoneSetting> zoneSetting;
            try
            {
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                if (convertedOrganizationId != null)
                   return zoneSetting = _hrZoneSettingDao.LoadZoneSetting(orderBy, direction, _commonHelper.ConvertIdToList(convertedOrganizationId.Value));
                 
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return new List<ZoneSetting>();
        }
        public IList<ZoneSetting> LoadZoneSetting(List<UserMenu> userMenu ,string orderBy, string direction, long? organizationId)
        {
            IList<ZoneSetting> zoneSetting;
            try
            {
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                zoneSetting = _hrZoneSettingDao.LoadZoneSetting(orderBy, direction, organizationIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return zoneSetting;
        }

        public IList<TeamMemberZoneDto> TeamMemberZoneList(DateTime startDate, DateTime endTime, long teamMemberId, int employmentStatus, decimal salary,
            decimal basicSalary, int monthlyWorkingDay,long orgId)
        {
            try
            {
                return _hrZoneSettingDao.TeamMemberZoneList(startDate, endTime, teamMemberId, employmentStatus, salary,
                    basicSalary, monthlyWorkingDay, orgId);
                
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        #endregion

        #region Others Function

        public int GetZoneSettingRowCount(List<UserMenu> userMenu ,string orderBy, string direction, long? organizationId)
        {
            int zoneSettingCount;
            try
            {
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                zoneSettingCount = _hrZoneSettingDao.GetZoneSettingRowCount(orderBy, direction, organizationIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return zoneSettingCount;
        }

        #endregion

        #region Helper function

        private void CheckBeforeSave(string name, string organizationId, string toleranceTime, long? id = null)
        {
            try
            {
                long convertedOrganizationId = Convert.ToInt64(organizationId);
                var convertedtoleranceTime = Convert.ToInt32(toleranceTime);
                var nameExists = _hrZoneSettingDao.CheckDublicateName(name, convertedOrganizationId, id);
                if (nameExists)
                    throw new DuplicateEntryException("Zone Setting name already remain.");
                var toleranceTimeExists = _hrZoneSettingDao.CheckDublicateToleranceTime(convertedtoleranceTime,
                    convertedOrganizationId, id);
                if (toleranceTimeExists)
                    throw new DuplicateEntryException("Tolerance Start & End Time already remain");
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        private void CheckEmptyField(string name, string organizationId, string toleranceTime, string toleranceDay, string deductionMultiplier)
        {
            
            if (String.IsNullOrEmpty(name))
                throw new EmptyFieldException("Name is empty");
            if (String.IsNullOrEmpty(organizationId))
                throw new EmptyFieldException("No organization is empty");
            else
            {
                var organization = _organizationDao.LoadById(Convert.ToInt64(organizationId));
                if (organization == null)
                    throw new EmptyFieldException("No organization found!");
            }
            if (!String.IsNullOrEmpty(toleranceTime))
            {
                if (Convert.ToInt32(toleranceTime) < 0)
                    throw new EmptyFieldException("Invalid Telerance Time!");
                if(Convert.ToInt32(toleranceTime) >= 24*60)
                    throw new EmptyFieldException("Telerance Time can't be greater then one day!");
            }
            if (!String.IsNullOrEmpty(toleranceDay))
            {
                if (Convert.ToInt32(toleranceDay) < 0)
                    throw new EmptyFieldException("Invalid Telerance Day!");
                if (Convert.ToInt32(toleranceDay) > 4)
                    throw new EmptyFieldException("Telerance Day can't be greater then 5 days!");
            }

            if (!String.IsNullOrEmpty(deductionMultiplier))
            {
                if (Convert.ToDecimal(deductionMultiplier) < 0)
                    throw new EmptyFieldException("Invalid Deduction Multiplier!");
                if (Convert.ToDecimal(deductionMultiplier) > 2)
                    throw new EmptyFieldException("Deduction Multiplier can't be greater then 2 times!");
            }

        }

        #endregion
    }
}
