﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.CourseTest
{
    public interface ICourseSaveTest
    {
        #region Basic test

        [Fact]
        void Should_Return_Null_Exception();

        [Fact]
        void Should_Return_Model_inValid();

        [Fact]
        void Should_Save_Successfully();

        #endregion

        #region Business Logic Test

        [Fact]
        void Should_Check_Duplicate();

        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Course")]
    public class CourseSaveTest : CourseBaseTest, ICourseSaveTest
    {
        #region Basic test
        [Fact]
        public void Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _courseService.CourseSaveOrUpdate(null));
        }

        [Fact]
        public void Should_Return_Model_inValid()
        {
            List<Subject> subjectList = _subjectFactory.CreateMore(5).Persist().GetLastCreatedObjectList();
            var subjectAssign = new SubjectAssign
                        {
                            SelectedSubjects = subjectList.Select(x => Convert.ToInt32(x.Id)).ToArray()
                        };
            _courseFactory.Create().WithName(_name).WithStartDateAndEndDate(DateTime.Now.AddDays(5), DateTime.Now.AddDays(3))
                           .WithOrganization().WithProgram().WithSession()
                           .WithMaxMinSubject(4, 2, 2);
            var course = _courseFactory.Object;
            Program program = course.Program;
            Session sessionObj = course.RefSession;
            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, program.Id, sessionObj.Id);
            _courseSubjectFactory.CreateMore(course, subjectList);
            _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList());
            Assert.Throws<InvalidDataException>(() => _courseFactory.Persist());
            _courseFactory.WithName("").WithStartDateAndEndDate(DateTime.Now.AddDays(5), DateTime.Now.AddDays(35));
            Assert.Throws<InvalidDataException>(() => _courseFactory.Persist());
            _courseFactory.WithProgram(null).WithName(_name);
            Assert.Throws<InvalidDataException>(() => _courseFactory.Persist());
            CleanUpProgramSessionSubject(program.Id, sessionObj.Id);
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            List<Subject> subjectList = _subjectFactory.CreateMore(5).Persist().GetLastCreatedObjectList();
            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectList.Select(x => Convert.ToInt32(x.Id)).ToArray()
            };
            _courseFactory.Create().WithName(_name).WithStartDateAndEndDate(DateTime.Now.AddDays(3), DateTime.Now.AddDays(5))
                           .WithOrganization().WithProgram().WithSession()
                           .WithMaxMinSubject(4, 2, 2);
            var course = _courseFactory.Object;
            Program program = course.Program;
            Session sessionObj = course.RefSession;
            Organization organization = course.Organization;
            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, program.Id, sessionObj.Id);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList()).Persist();
            Assert.NotEqual(_courseFactory.Object.Id, 0);
            Assert.NotNull(_courseFactory.Object.Id);
            CleanUpProgramSessionSubject(program.Id, sessionObj.Id);
        }

        #endregion

        #region Business Logic Test
        [Fact]
        public void Should_Check_Duplicate()
        {
            List<Subject> subjectList = _subjectFactory.CreateMore(5).Persist().GetLastCreatedObjectList();
            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectList.Select(x => Convert.ToInt32(x.Id)).ToArray()
            };
            _courseFactory.Create().WithName(_name).WithStartDateAndEndDate(DateTime.Now.AddDays(3), DateTime.Now.AddDays(5))
                           .WithOrganization().WithProgram().WithSession()
                           .WithMaxMinSubject(4, 2, 2);
            var course = _courseFactory.Object;
            Program program = course.Program;
            Session sessionObj = course.RefSession;
            Organization organization = course.Organization;
            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, program.Id, sessionObj.Id);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList()).Persist();
            _courseFactory.Create().WithName(_name).WithStartDateAndEndDate(DateTime.Now.AddDays(5), DateTime.Now.AddDays(35))
                            .WithOrganization(organization).WithProgram(program).WithSession(sessionObj)
                            .WithMaxMinSubject(4, 2, 2);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList());
            Assert.Throws<DuplicateEntryException>(() => _courseFactory.Persist());
            CleanUpProgramSessionSubject(program.Id, sessionObj.Id);
        } 
        #endregion

    }
}
