using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
using UdvashERP.BusinessRules;
using NHibernate.Criterion;
using UdvashERP.BusinessRules.Hr;
namespace UdvashERP.Services.Hr
{
    public interface IOvertimeAllowanceSettingService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(List<UserMenu> userMenus, OvertimeAllowanceSetting overtimeAllowanceSetting);
        bool Delete(OvertimeAllowanceSetting entity);

        #endregion

        #region Single Instances Loading Function

        OvertimeAllowanceSetting GetOvertimeAllowanceSetting(long id);
        OvertimeAllowanceSetting GetOvertimeAllowanceSetting(List<UserMenu> userMenus, long id);

        #endregion

        #region List Loading Function

        IList<OvertimeAllowanceSetting> LoadOvertimeAllowanceSetting(List<UserMenu> userMenu, int start, int length, string orderBy, string orderDir, long organizationId, string employmentTypeStatus, string overtimeType);

        #endregion

        #region Others Function

        int GetOvertimeAllowanceSettingCount(List<UserMenu> userMenus, string orderBy, string orderDir, long organizationId, string employmentTypeStr, string holidayTypeStr);
        bool IsOvertimeAllowanceSettingNameExist(string name, long organization, long? id);

        #endregion

        #region Helper Function

        #endregion
    }
    public class OvertimeAllowanceSettingService : BaseService, IOvertimeAllowanceSettingService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("PayrollService");
        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IOvertimeAllowanceSettingDao _overtimeAllowanceDao;
        IAllowanceSheetDao _allowanceSheetDao;
        IAllowanceSheetFirstDetailsDao _allowanceSheetFirstDetailDao;

        public OvertimeAllowanceSettingService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _overtimeAllowanceDao = new OvertimeAllowanceSettingDao { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao { Session = session };
            _allowanceSheetFirstDetailDao = new AllowanceSheetFirstDetailsDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(List<UserMenu> userMenus, OvertimeAllowanceSetting overtimeAllowanceSetting)
        {
            bool returnValue = false;
            ITransaction trans = null;
            try
            {
                DoBeforeSaveOrUpdate(overtimeAllowanceSetting);
                using (trans = Session.BeginTransaction())
                {
                    if (overtimeAllowanceSetting.Id > 0)
                    {
                        var oldData = _overtimeAllowanceDao.LoadById(overtimeAllowanceSetting.Id);
                        oldData.Name = overtimeAllowanceSetting.Name;
                        oldData.IsProbation = overtimeAllowanceSetting.IsProbation;
                        oldData.IsPermanent = overtimeAllowanceSetting.IsPermanent;
                        oldData.IsContractual = overtimeAllowanceSetting.IsContractual;
                        oldData.IsIntern = overtimeAllowanceSetting.IsIntern;
                        oldData.IsWorkingDay = overtimeAllowanceSetting.IsWorkingDay;
                        oldData.IsManagementWeekend = overtimeAllowanceSetting.IsManagementWeekend;
                        oldData.IsGazzeted = overtimeAllowanceSetting.IsGazzeted;
                        oldData.CalculationOn = overtimeAllowanceSetting.CalculationOn;
                        oldData.MultiplyingFactor = overtimeAllowanceSetting.MultiplyingFactor;
                        oldData.EffectiveDate = overtimeAllowanceSetting.EffectiveDate;
                        oldData.ClosingDate = overtimeAllowanceSetting.ClosingDate;
                        oldData.ModifyBy = GetCurrentUserId();
                        oldData.ModificationDate = DateTime.Now;
                        _overtimeAllowanceDao.Update(oldData);
                    }
                    else
                    {
                        _overtimeAllowanceDao.SaveOrUpdate(overtimeAllowanceSetting);
                    }

                    trans.Commit();
                    returnValue = true;
                }
            }
            catch (DuplicateEntryException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (MessageException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return returnValue;
        }

        public bool Delete(OvertimeAllowanceSetting entity)
        {
            ITransaction transaction = null;
            try
            {
                DoBeforeDelete(entity);
                using (transaction = Session.BeginTransaction())
                {
                    entity.Status = OvertimeAllowanceSetting.EntityStatus.Delete;
                    entity.ModifyBy = GetCurrentUserId();
                    entity.ModificationDate = DateTime.Now;
                    _overtimeAllowanceDao.Update(entity);
                    transaction.Commit();
                    return true;
                }
            }
            catch (MessageException ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public OvertimeAllowanceSetting GetOvertimeAllowanceSetting(long id)
        {
            OvertimeAllowanceSetting overtimeAllowanceSetting = null;
            try
            {
                overtimeAllowanceSetting = _overtimeAllowanceDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return overtimeAllowanceSetting != null ? overtimeAllowanceSetting : null;
        }

        public OvertimeAllowanceSetting GetOvertimeAllowanceSetting(List<UserMenu> userMenus, long id)
        {
            try
            {
                var organizationList = AuthHelper.LoadOrganizationIdList(userMenus);
                if (organizationList.Count <= 0)
                    throw new InvalidDataException("Permission Denied for selected Organization!");
                var entity = _overtimeAllowanceDao.LoadById(id);
                if (entity == null)
                {
                    throw new NullObjectException("Null object found");
                }
                if (!organizationList.Contains(entity.Organization.Id))
                {
                    throw new InvalidDataException("Permission Error!");
                }
                return entity ?? null;
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<OvertimeAllowanceSetting> LoadOvertimeAllowanceSetting(List<UserMenu> userMenu, int start, int length, string orderBy, string orderDir, long organizationId, string employmentTypeStatus, string overtimeType)
        {
            IList<OvertimeAllowanceSetting> overtimeAllowanceSetting;
            try
            {
                List<long> authorizedOrganization = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId == 0) ? null : _commonHelper.ConvertIdToList((long)organizationId));

                if (authorizedOrganization == null || !EnumerableExtensions.Any(authorizedOrganization))
                    throw new InvalidDataException("Invalid Organization");

                var employmentStatusString = (MemberEmploymentStatus)(!string.IsNullOrEmpty(employmentTypeStatus) ? Enum.Parse(typeof(MemberEmploymentStatus), employmentTypeStatus) : 0);
                var overtimeTypeString = (AllowanceSettingOverTimeType)(!string.IsNullOrEmpty(overtimeType) ? Enum.Parse(typeof(AllowanceSettingOverTimeType), overtimeType) : 0);

                bool isProbation, isPermanent, isPartTime, isContractual, isIntern = false;
                isProbation = employmentStatusString == MemberEmploymentStatus.Probation ? true : false;
                isPermanent = employmentStatusString == MemberEmploymentStatus.Permanent ? true : false;
                isPartTime = employmentStatusString == MemberEmploymentStatus.PartTime ? true : false;
                isContractual = employmentStatusString == MemberEmploymentStatus.Contractual ? true : false;
                isIntern = employmentStatusString == MemberEmploymentStatus.Intern ? true : false;

                bool isRegular, isManagementWeekend, isGazetted = false;
                isRegular = overtimeTypeString == AllowanceSettingOverTimeType.Regular ? true : false;
                isManagementWeekend = overtimeTypeString == AllowanceSettingOverTimeType.Management ? true : false;
                isGazetted = overtimeTypeString == AllowanceSettingOverTimeType.Gazetted ? true : false;

                overtimeAllowanceSetting = _overtimeAllowanceDao.LoadOvertimeAllowanceSetting(authorizedOrganization, start, length, orderBy, orderDir, organizationId, isProbation, isPermanent, isPartTime, isContractual, isIntern, isRegular, isGazetted, isManagementWeekend);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return overtimeAllowanceSetting;
        }

        #endregion

        #region Others Function

        public bool IsOvertimeAllowanceSettingNameExist(string name, long organization, long? id)
        {
            return _overtimeAllowanceDao.IsOvertimeAllowanceSettingNameExist(name, organization, id);
        }

        public int GetOvertimeAllowanceSettingCount(List<UserMenu> userMenus, string orderBy, string orderDir, long organizationId, string employmentTypeStr, string holidayTypeStr)
        {
            int returnValue = 0;
            try
            {
                List<long> authorizedOrganization = AuthHelper.LoadOrganizationIdList(userMenus, (organizationId == 0) ? null : _commonHelper.ConvertIdToList((long)organizationId));

                if (authorizedOrganization == null || !EnumerableExtensions.Any(authorizedOrganization))
                    throw new InvalidDataException("Invalid Organization");

                var employmentStatusString = (MemberEmploymentStatus)(!string.IsNullOrEmpty(employmentTypeStr) ? Enum.Parse(typeof(MemberEmploymentStatus), employmentTypeStr) : 0);

                var overtimeTypeString = (AllowanceSettingOverTimeType)(!string.IsNullOrEmpty(holidayTypeStr) ? Enum.Parse(typeof(AllowanceSettingOverTimeType), holidayTypeStr) : 0);

                bool isProbation, isPermanent, isPartTime, isContractual, isIntern = false;
                isProbation = employmentStatusString == MemberEmploymentStatus.Probation ? true : false;
                isPermanent = employmentStatusString == MemberEmploymentStatus.Permanent ? true : false;
                isPartTime = employmentStatusString == MemberEmploymentStatus.PartTime ? true : false;
                isContractual = employmentStatusString == MemberEmploymentStatus.Contractual ? true : false;
                isIntern = employmentStatusString == MemberEmploymentStatus.Intern ? true : false;

                bool isRegular, isManagementWeekend, isGazetted = false;
                isRegular = overtimeTypeString == AllowanceSettingOverTimeType.Regular ? true : false;
                isManagementWeekend = overtimeTypeString == AllowanceSettingOverTimeType.Management ? true : false;
                isGazetted = overtimeTypeString == AllowanceSettingOverTimeType.Gazetted ? true : false;

                returnValue = _overtimeAllowanceDao.GetOvertimeAllowanceSettingCount(orderBy, orderDir, organizationId, isProbation, isPermanent, isPartTime, isContractual, isIntern, isRegular, isGazetted, isManagementWeekend);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return returnValue;
        }

        #endregion

        #region Helper function

        private void ObjectValidationChecker(OvertimeAllowanceSetting overtimeAllowanceSetting)
        {
            var validationResult = ValidationHelper.ValidateEntity<OvertimeAllowanceSetting, OvertimeAllowanceSettingMetaData>(overtimeAllowanceSetting);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private void DoBeforeSaveOrUpdate(OvertimeAllowanceSetting overtimeAllowanceSetting)
        {
            #region Basic Check

            if (overtimeAllowanceSetting == null)
            {
                throw new NullObjectException("Null object reference found.");
            }

            if (overtimeAllowanceSetting.Organization == null)
            {
                throw new InvalidDataException("Organization not found.");
            }

            var employmentStatus = new[] { overtimeAllowanceSetting.IsProbation, overtimeAllowanceSetting.IsPermanent, overtimeAllowanceSetting.IsPartTime, overtimeAllowanceSetting.IsContractual, overtimeAllowanceSetting.IsIntern };
            if (employmentStatus.Count(b => b == true) == 0)
            {
                throw new InvalidDataException("Employement Status Not Selected.");
            }

            var overtimeType = new[] { overtimeAllowanceSetting.IsWorkingDay, overtimeAllowanceSetting.IsManagementWeekend, overtimeAllowanceSetting.IsGazzeted };
            if (overtimeType.Count(b => b == true) == 0)
            {
                throw new InvalidDataException("Overtime type Not Selected.");
            }


            var calculationOn = overtimeAllowanceSetting.CalculationOn;
            if (!Enum.IsDefined(typeof(CalculationOn), calculationOn))
                throw new InvalidDataException("Invalid calculation on data");

            ObjectValidationChecker(overtimeAllowanceSetting);

            //if (overtimeAllowanceSetting.ClosingDate != null)
            //{
            //    if (DateTime.Compare(overtimeAllowanceSetting.EffectiveDate, overtimeAllowanceSetting.ClosingDate.Value.Date) >= 0)
            //    {
            //        throw new InvalidDataException("Effective date can't be less than or equal closing date!");
            //    }
            //}

            #endregion

            #region Is Post

            AllowanceSheet IsPostAllowanceSheet = _allowanceSheetDao.GetEligibleAllowanceSheetByOrganizationId(overtimeAllowanceSetting.Organization.Id);
            if (IsPostAllowanceSheet != null)
            {
                if (overtimeAllowanceSetting.Id == 0)
                {
                    if (overtimeAllowanceSetting.EffectiveDate <= IsPostAllowanceSheet.DateTo)
                    {
                        throw new MessageException("You can't create this overtime allowance in this date range");
                    }
                }
                else
                {
                    var oldOvertimeSettings = _overtimeAllowanceDao.LoadById(overtimeAllowanceSetting.Id);

                    if (overtimeAllowanceSetting.EffectiveDate <= IsPostAllowanceSheet.DateTo)
                    {
                        CompareWithOldObject(overtimeAllowanceSetting, oldOvertimeSettings);
 
                        //testing purpose 
                        if (IsPostAllowanceSheet.DateTo <= overtimeAllowanceSetting.EffectiveDate)
                        {
                            throw new MessageException("You can't change the effective date in this date range");
                        }
                    }

                    if (overtimeAllowanceSetting.ClosingDate != null)
                    {
                        if (IsPostAllowanceSheet.DateTo >= overtimeAllowanceSetting.ClosingDate)
                        {
                            throw new MessageException("You can't change the closing date in this date range");
                        }
                    } 
                }
            }

            #endregion

            #region Combination Checking

            if (_overtimeAllowanceDao.HasDuplicate(overtimeAllowanceSetting))
            {
                throw new DuplicateEntryException("Already found this combination of data.");
            }
            
            #endregion

        }

        private void CompareWithOldObject(OvertimeAllowanceSetting overtimeAllowanceSetting, OvertimeAllowanceSetting oldOvertimeSettings)
        {
            if (overtimeAllowanceSetting.IsProbation != oldOvertimeSettings.IsProbation)
                throw new MessageException("You can't update this overtime allowance in this date range");
            else if (overtimeAllowanceSetting.IsPermanent != oldOvertimeSettings.IsPermanent)
                throw new MessageException("You can't update this overtime allowance in this date range");
            else if (overtimeAllowanceSetting.IsPartTime != oldOvertimeSettings.IsPartTime)
                throw new MessageException("You can't update this overtime allowance in this date range");
            else if (overtimeAllowanceSetting.IsContractual != oldOvertimeSettings.IsContractual)
                throw new MessageException("You can't update this overtime allowance in this date range");
            else if (overtimeAllowanceSetting.IsIntern != oldOvertimeSettings.IsIntern)
                throw new MessageException("You can't update this overtime allowance in this date range");

            if (overtimeAllowanceSetting.IsWorkingDay != oldOvertimeSettings.IsWorkingDay)
                throw new MessageException("You can't update this overtime allowance in this date range");
            else if (overtimeAllowanceSetting.IsManagementWeekend != oldOvertimeSettings.IsManagementWeekend)
                throw new MessageException("You can't update this overtime allowance in this date range");
            else if (overtimeAllowanceSetting.IsGazzeted != oldOvertimeSettings.IsGazzeted)
                throw new MessageException("You can't update this overtime allowance in this date range");

            if(overtimeAllowanceSetting.CalculationOn!=oldOvertimeSettings.CalculationOn)
                throw new MessageException("You can't update this overtime allowance in this date range");
            if(overtimeAllowanceSetting.MultiplyingFactor!=oldOvertimeSettings.MultiplyingFactor)
                throw new MessageException("You can't update this overtime allowance in this date range");

            if (overtimeAllowanceSetting.EffectiveDate != oldOvertimeSettings.EffectiveDate)
            {
                throw new MessageException("You can't change the Effective date.");
            }

        }

        private void DoBeforeDelete(OvertimeAllowanceSetting entity)
        {
            try
            {
                var allowanceSheetFirstDetail = _allowanceSheetFirstDetailDao.GetAllowanceSheetFirstDetailByOvertimeAllowanceSheet(entity.Id);
                if (allowanceSheetFirstDetail != null && allowanceSheetFirstDetail.Any())
                {
                    throw new MessageException("You can't delete this overtime allowance settings.");
                }
            }
            catch (MessageException ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}

