﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.LeaveSettingTest
{
    public class LeaveSettingBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initialization

        protected readonly ILeaveService _leaveService;
        protected List<long> idList;

        public LeaveSettingBaseTest()
        {
            _leaveService = new LeaveService(Session);
            idList = new List<long>();
        }

        #endregion
        
        public void Dispose()
        {
            leaveFactory.Cleanup();
            base.Dispose();
        }
    }
}
