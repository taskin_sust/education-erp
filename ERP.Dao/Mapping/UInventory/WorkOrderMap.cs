using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class WorkOrderMap : BaseClassMap<WorkOrder, long>
    {

        public WorkOrderMap()
        {
            Table("UINV_WorkOrder");
            LazyLoad();
            References(x => x.Supplier).Column("SupplierId");
            References(x => x.Quotation).Column("QuotationId");
            References(x => x.Branch).Column("BranchId");
            //Map(x => x.Rank).Column("Rank").Not.Nullable();
            //Map(x => x.Name).Column("Name").Not.Nullable();
            Map(x => x.WorkOrderStatus).Column("WorkOrderStatus").Not.Nullable();
            Map(x => x.DeliveryDate).Column("DeliveryDate");
            Map(x => x.OrderedQuantity).Column("OrderedQuantity");
            Map(x => x.TotalCost).Column("TotalCost");
            Map(x => x.WorkOrderDate).Column("WorkOrderDate");
            Map(x => x.WorkOrderNo).Column("WorkOrderNo");
            Map(x => x.Remarks).Column("Remarks");
            HasMany(x => x.GoodsReceiveList).KeyColumn("WorkOrderId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.WorkOrderDetails).KeyColumn("WorkOrderId").Cascade.AllDeleteOrphan().Inverse(); ;
        }
    }
}
