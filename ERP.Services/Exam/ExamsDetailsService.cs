﻿using System;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Exam;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Exam
{
    public interface IExamsDetailsService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        ExamsDetails LoadById(long id);
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

    }
    public class ExamsDetailsService : BaseService, IExamsDetailsService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IExamsDetailsDao _examsDetailsDao;
        public ExamsDetailsService(ISession session)
        {
            Session = session;
            _examsDetailsDao = new ExamsDetailsDao() { Session = Session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public ExamsDetails LoadById(long id)
        {
            ExamsDetails examsDetails;
            try
            {
                examsDetails = _examsDetailsDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return examsDetails;
        }
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
