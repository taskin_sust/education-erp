﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Administration
{
    public interface ICourseDao : IBaseDao<Course, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<Course> LoadCourse();
        IList<Course> LoadCourse(long[] courseIds);
        IList<Course> LoadCourse(long programId, long sessionId);
        IList<Course> LoadCourse(List<long> programIds, List<long> sessionIds);
        IList<Course> LoadCourse(long organizatonId, long programId, long sessionId, long[] courseIds);
        IList<Course> LoadCourse(List<long> organizationIds, List<long> programIds, List<long> sessionIds);
        IList<Course> LoadComplementaryCourse(List<long> organizationIds, List<long> programIds, List<long> sessionIds, long courseId);
        //IList<Course> LoadCourse(int start, int length, string orderBy, string orderDir, string organizationId, string programId, string sessionName, string name, string rank, string status);
        IList<Course> LoadCourse(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionName, string name, string rank, string status);
        IList<Course> LoadCourse(string orgBusinessId, string prnNo);

        #endregion

        #region Others Function
        bool IsDuplicateCourse(string courseName, long programId, long sessionId, long? courseId = null);

        bool IsCourseAssignByStudent(long courseId);
        //int GetCourseCount(string orderBy, string orderDir, string organizationId, string programId, string sessionName, string name, string rank, string status);
        int GetCourseCount(string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionName, string name, string rank, string status);
        #endregion

        #region Public API
        IList<Course> LoadCourse(string organizationBusinessId, long programId, long sessionId);
        #endregion

    }
    public class CourseDao : BaseDao<Course, long>, ICourseDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<Course> LoadCourse(List<long> programIds, List<long> sessionIds)
        {
            ICriteria criteria = Session.CreateCriteria<Course>().Add(Restrictions.Eq("Status", Course.EntityStatus.Active)).AddOrder(Order.Asc("Rank"));
            criteria.CreateCriteria("Program", "program").Add(Restrictions.Eq("program.Status", Program.EntityStatus.Active));
            //criteria.CreateCriteria("Organization", "organization").Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active));

            criteria.CreateCriteria("RefSession", "session").Add(Restrictions.Eq("session.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            if (!programIds.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("program.Id", programIds));
            }

            if (!sessionIds.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("session.Id", sessionIds));
            }

            return criteria.List<Course>();
        }

        public IList<Course> LoadCourse(List<long> organizationIds, List<long> programIds, List<long> sessionIds)
        {
            ICriteria criteria = Session.CreateCriteria<Course>().Add(Restrictions.Eq("Status", Course.EntityStatus.Active)).AddOrder(Order.Asc("Rank"));
            criteria.CreateCriteria("Program", "program").Add(Restrictions.Eq("program.Status", Program.EntityStatus.Active));
            criteria.CreateCriteria("program.Organization", "organization").Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active));
            criteria.CreateCriteria("RefSession", "session").Add(Restrictions.Eq("session.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));

            if (organizationIds != null && organizationIds.Any() && !organizationIds.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("organization.Id", organizationIds));
            }

            if (programIds != null && programIds.Any() && !programIds.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("program.Id", programIds));
            }

            if (sessionIds != null && sessionIds.Any() && !sessionIds.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("session.Id", sessionIds));
            }

            return criteria.List<Course>();
        }

        public IList<Course> LoadComplementaryCourse(List<long> organizationIds, List<long> programIds, List<long> sessionIds, long courseId)
        {
            ICriteria criteria = Session.CreateCriteria<Course>().Add(Restrictions.Eq("Status", Course.EntityStatus.Active)).AddOrder(Order.Asc("Rank"));
            criteria.CreateCriteria("Program", "program").Add(Restrictions.Eq("program.Status", Program.EntityStatus.Active));
            criteria.CreateCriteria("program.Organization", "organization").Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active));
            criteria.CreateCriteria("RefSession", "session").Add(Restrictions.Eq("session.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));

            if (!organizationIds.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("organization.Id", organizationIds));
            }

            if (!programIds.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("program.Id", programIds));
            }

            if (!sessionIds.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("session.Id", sessionIds));
            }
            criteria.Add(Restrictions.Not(Restrictions.Eq("Id", courseId)));

            //DetachedCriteria complementaryMainCourse = DetachedCriteria.For<ComplementaryCourse>().SetProjection(Projections.Distinct(Projections.Property("Course.Id")));
            //complementaryMainCourse.Add(Restrictions.In("Session.Id", sessionIds));
            //complementaryMainCourse.Add(Restrictions.In("Program.Id", programIds));
            //criteria.Add(Subqueries.PropertyNotIn("Id", complementaryMainCourse));

            //if (!courseIds.Contains(SelectionType.SelelectAll))
            //{
            //    criteria.Add(Restrictions.Not(Restrictions.In("Id", courseIds)));

            //    //DetachedCriteria complementaryCourse = DetachedCriteria.For<ComplementaryCourse>().SetProjection(Projections.Distinct(Projections.Property("CompCourse.Id"))).CreateAlias("Course", "course").Add(Restrictions.Eq("course.Status", Branch.EntityStatus.Active));
            //    //complementaryCourse.Add(Restrictions.In("CompCourse.Id", courseIds));
            //    //criteria.Add(Subqueries.PropertyNotIn("Id", complementaryCourse));
            //}




            return criteria.List<Course>();
        }

        public IList<Course> LoadCourse()
        {
            ICriteria criteria = Session.CreateCriteria<Course>().Add(Restrictions.Eq("Status", Course.EntityStatus.Active)).AddOrder(Order.Asc("Rank"));
            return criteria.List<Course>();
        }

        public IList<Course> LoadCourse(long[] courseIds)
        {
            if (courseIds.Length == 1 && courseIds[0] == 0 || courseIds.Contains(0))
            {
                return Session.QueryOver<Course>().Where(c => c.Status == Course.EntityStatus.Active).OrderBy(x => x.Rank).Asc.List<Course>();
            }
            else
            {
                return Session.QueryOver<Course>().Where(x => x.Id.IsIn(courseIds) && x.Status == Course.EntityStatus.Active).OrderBy(x => x.Rank).Asc.List<Course>();
            }

        }

        public IList<Course> LoadCourse(long programId, long sessionId)
        {
            ICriteria criteria = Session.CreateCriteria<Course>().Add(Restrictions.Eq("Status", Course.EntityStatus.Active)).AddOrder(Order.Asc("Rank"));
            criteria.CreateCriteria("Program", "program").Add(Restrictions.Eq("program.Status", Program.EntityStatus.Active));
            criteria.CreateCriteria("RefSession", "session").Add(Restrictions.Eq("session.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            if (programId != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("program.Id", programId));
            }

            criteria.Add(Restrictions.Eq("session.Id", sessionId));

            return criteria.List<Course>();
        }

        public IList<Course> LoadCourse(long organizatonId, long programId, long sessionId, long[] courseIds)
        {
            ICriteria criteria = Session.CreateCriteria<Course>().Add(Restrictions.Not(Restrictions.Eq("Status", Course.EntityStatus.Delete)));
            criteria.CreateAlias("Program", "p");
            criteria.CreateAlias("p.Organization", "organization");
            criteria.Add(Restrictions.Eq("p.Id", programId));
            criteria.Add(Restrictions.Eq("RefSession.Id", sessionId));
            criteria.Add(Restrictions.Eq("organization.Id", organizatonId));
            if (!courseIds.Contains(0))
            {
                criteria.Add(Restrictions.In("Id", courseIds));
            }
            return criteria.List<Course>();
        }

        //public IList<Course> LoadCourse(int start, int length, string orderBy, string orderDir, string organizationId, string programId, string sessionName, string name, string rank, string status)
        public IList<Course> LoadCourse(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionName, string name, string rank, string status)
        {
            //var criteria = GetCourseCriteria(organizationId, programId, sessionName, name, rank, status);
            var criteria = GetCourseCriteria(organizationIdList, programIdList, sessionName, name, rank, status);
            if (!String.IsNullOrEmpty(orderBy))
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Course>();
        }
        public IList<Course> LoadCourse(string orgBusinessId, string prnNo)
        {
            ICriteria criteria = Session.CreateCriteria<StudentCourseDetail>();
            criteria.CreateAlias("CourseSubject", "cs");
            criteria.CreateAlias("cs.Course", "c").Add(Restrictions.Eq("sp.Status", Course.EntityStatus.Active));
            criteria.CreateAlias("c.Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("p.Organization", "o").Add(Restrictions.Eq("o.BusinessId", orgBusinessId));
            criteria.CreateAlias("StudentProgram", "sp").Add(Restrictions.Eq("sp.PrnNo", prnNo)).Add(Restrictions.Eq("sp.Status", StudentProgram.EntityStatus.Active));
            criteria.SetProjection(Projections.Distinct(Projections.Property("cs.Course")));
            //criteria.SetProjection(Projections.Property("cs.Course"));
            return criteria.List<Course>();
        }

        #endregion

        #region Others Function
        //refactored
        public bool IsCourseAssignByStudent(long courseId)
        {
            ICriteria criteria = Session.CreateCriteria<Course>().Add(Restrictions.Eq("Status", Course.EntityStatus.Active));
            criteria.CreateCriteria("CourseSubjects", "cs", JoinType.InnerJoin);
            criteria.CreateCriteria("cs.StudentCourseDetails", "scd", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("Id", courseId));
            criteria.SetProjection(Projections.RowCount());
            var query = Convert.ToInt32(criteria.UniqueResult());
            return query > 0;
        }
        //public int GetCourseCount(string orderBy, string orderDir, string organizationId, string programId, string sessionName, string name, string rank, string status)
        public int GetCourseCount(string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionName, string name, string rank, string status)
        {
            //var criteria = GetCourseCriteria(organizationId, programId, sessionName, name, rank, status);
            var criteria = GetCourseCriteria(organizationIdList, programIdList, sessionName, name, rank, status);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function
        public bool IsDuplicateCourse(string courseName, long programId, long sessionId, long? courseId = null)
        {
            int rowCount = 0;
            if (courseId == null || courseId == 0)
            {
                rowCount = Session.QueryOver<Course>().Where(x => x.Name == courseName && x.Program.Id == programId && x.RefSession.Id == sessionId && x.Status != Course.EntityStatus.Delete).RowCount();
            }
            else
            {
                rowCount = Session.QueryOver<Course>().Where(x => x.Name == courseName && x.Program.Id == programId && x.RefSession.Id == sessionId && x.Status != Course.EntityStatus.Delete && x.Id != courseId).RowCount();
            }

            if (rowCount > 0)
            {
                return true;
            }
            return false;
        }

        //private ICriteria GetCourseCriteria(string organizationId, string programId, string sessionName, string name, string rank, string status)
        private ICriteria GetCourseCriteria(List<long> organizationIdList, List<long> programIdList, string sessionName, string name, string rank, string status)
        {
            ICriteria criteria = Session.CreateCriteria<Course>().Add(Restrictions.Not(Restrictions.Eq("Status", Course.EntityStatus.Delete)));
            criteria.CreateAlias("Program", "program");
            criteria.CreateAlias("RefSession", "refSession");
            criteria.CreateAlias("program.Organization", "organization");

            //if (!String.IsNullOrEmpty(organizationId))
            //{
            //    criteria.Add(Restrictions.Eq("program.Organization.Id", Convert.ToInt64(organizationId.Trim())));
            //}
            //if (!String.IsNullOrEmpty(programId))
            //{
            //    criteria.Add(Restrictions.Eq("Program.Id", Convert.ToInt64(programId.Trim())));
            //}
            if (organizationIdList != null)
            {
                criteria.Add(Restrictions.In("program.Organization.Id", organizationIdList));
            }
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("Program.Id", programIdList));
            }

            if (!String.IsNullOrEmpty(sessionName))
            {
                criteria.Add(Restrictions.Like("refSession.Name", sessionName.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.InsensitiveLike("Name", name.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            //if (!countQuery)
            //{
            //    if (!String.IsNullOrEmpty(orderBy))
            //        criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            //}
            return criteria;
        }

        #endregion

        #region Public API
        public IList<Course> LoadCourse(string organizationBusinessId, long programId, long sessionId)
        {
            ICriteria criteria = Session.CreateCriteria<Course>()
                .Add(Restrictions.Eq("Status", Course.EntityStatus.Active))
                .CreateAlias("RefSession", "s")
                .Add(Restrictions.Eq("s.Id", sessionId))
                .Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active))
                .CreateAlias("Program", "p")
                .Add(Restrictions.Eq("p.Id", programId))
                .Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active))
                .CreateAlias("p.Organization", "o")
                .Add(Restrictions.Eq("o.BusinessId", organizationBusinessId))
                .Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active))
                .CreateAlias("p.ProgramBranchSessions", "pbs")
                .Add(Restrictions.Eq("pbs.IsPublic", true))
                .Add(Restrictions.Eq("pbs.Status", ProgramBranchSession.EntityStatus.Active));

            return criteria.List<Course>().OrderBy(x => x.Rank).Distinct().ToList();
        }
        #endregion
    }
}