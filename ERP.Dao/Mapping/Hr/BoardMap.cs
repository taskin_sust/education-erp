using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class BoardMap : BaseClassMap<Board,long> {

        public BoardMap()
        {
            Table("HR_Board");
			LazyLoad();
			Map(x => x.Name).Column("Name");
            HasMany(x => x.AcademicInfo).KeyColumn("BoardId"); 
        }
    }
}
