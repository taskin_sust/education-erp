﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{
    public interface IAttendanceSynchronizerSettingService : IBaseService
    {
        #region Operational Function
        bool Save(AttendanceSynchronizerSetting hrAttendanceSynchronizerSettingObj);
        bool Update(AttendanceSynchronizerSetting hrAttendanceSynchronizerSetting, long id);
        void Delete(AttendanceSynchronizerSetting hrAttendanceSynchronizerSetting);
        #endregion

        #region Single Instances Loading Function
        AttendanceSynchronizerSetting GetAttendanceSynchronizerSetting(long id);
        AttendanceSynchronizerSetting GetNextAttendanceSynchronizerSetting(double version);
        #endregion

        #region List Loading Function

        List<AttendanceSynchronizerSetting> LoadSynchronizerSetting(int start, int length, string orderBy,
            string orderDir);
        #endregion

        #region Others Function
        int SynchronizerSettingRowCount();
        #endregion

        #region Helper Function

        #endregion



    }
    public class AttendanceSynchronizerSettingService : BaseService, IAttendanceSynchronizerSettingService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IAttendanceSynchronizerSettingDao _hrAttendanceSynchronizerSettingDao;
        public AttendanceSynchronizerSettingService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrAttendanceSynchronizerSettingDao = new AttendanceSynchronizerSettingDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool Save(AttendanceSynchronizerSetting hrAttendanceSynchronizerSettingObj)
        {
            if (hrAttendanceSynchronizerSettingObj.Id > 0) throw new UdvashERP.MessageExceptions.InvalidDataException("Invalid Object");

            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    DuplicatationCheck(hrAttendanceSynchronizerSettingObj.UpdateUrl);
                    SynchronizerVersionCheck(hrAttendanceSynchronizerSettingObj.SynchronizerVersion, hrAttendanceSynchronizerSettingObj.Id);
                    _hrAttendanceSynchronizerSettingDao.Save(hrAttendanceSynchronizerSettingObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (UdvashERP.MessageExceptions.InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }

        public bool Update(AttendanceSynchronizerSetting hrAttendanceSynchronizerSetting, long id)
        {
            if (hrAttendanceSynchronizerSetting.Id <= 0) throw new UdvashERP.MessageExceptions.InvalidDataException("Invalid Object");

            ITransaction transaction = null;
            try
            {
                DuplicatationCheck(hrAttendanceSynchronizerSetting.UpdateUrl, id);
                using (transaction = Session.BeginTransaction())
                {
                    var obj = _hrAttendanceSynchronizerSettingDao.LoadById(id);
                    if (!String.IsNullOrEmpty(hrAttendanceSynchronizerSetting.Name))
                        obj.Name = hrAttendanceSynchronizerSetting.Name;
                    //obj.SynchronizerVersion = hrAttendanceSynchronizerSetting.SynchronizerVersion;
                    if (!String.IsNullOrEmpty(hrAttendanceSynchronizerSetting.Name))
                        obj.UpdateUrl = hrAttendanceSynchronizerSetting.UpdateUrl;
                    obj.ReleaseDate = hrAttendanceSynchronizerSetting.ReleaseDate;
                    obj.Description = hrAttendanceSynchronizerSetting.Description;
                    _hrAttendanceSynchronizerSettingDao.Update(obj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }

        public void Delete(AttendanceSynchronizerSetting hRAttendanceSynchronizerSetting)
        {
            if (hRAttendanceSynchronizerSetting.Id <= 0) throw new UdvashERP.MessageExceptions.InvalidDataException("Invalid Object");

            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    hRAttendanceSynchronizerSetting.Status = AttendanceSynchronizer.EntityStatus.Delete;
                    _hrAttendanceSynchronizerSettingDao.Update(hRAttendanceSynchronizerSetting);
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public AttendanceSynchronizerSetting GetAttendanceSynchronizerSetting(long id)
        {
            try
            {
                if (id <= 0) throw new UdvashERP.MessageExceptions.InvalidDataException("Invalid Id");
                var attendanceSynchronizerSetting = Session.QueryOver<AttendanceSynchronizerSetting>().Where(x => x.Id == id).SingleOrDefault();
                return attendanceSynchronizerSetting;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public AttendanceSynchronizerSetting GetNextAttendanceSynchronizerSetting(double version)
        {
            try
            {
                var attendanceSynchronizerSetting = Session.QueryOver<AttendanceSynchronizerSetting>().
                    Where(x => x.SynchronizerVersion > version).OrderBy(x => x.SynchronizerVersion).Asc.Take(1).SingleOrDefault();
                return attendanceSynchronizerSetting;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion

        #region List Loading Function
        public List<AttendanceSynchronizerSetting> LoadSynchronizerSetting(int start, int length, string orderBy, string orderDir)
        {
            try
            {
                return _hrAttendanceSynchronizerSettingDao.LoadSynchronizerSetting(start, length, orderBy, orderDir);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function

        public int SynchronizerSettingRowCount()
        {
            try
            {
                var rowCount =
                        Session.QueryOver<AttendanceSynchronizerSetting>()
                            .Where(x => x.Status == AttendanceSynchronizerSetting.EntityStatus.Active)
                            .RowCount();
                return rowCount;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        #endregion

        #region Helper function

        private void DuplicatationCheck(string url, long id = 0)
        {
            try
            {
                var data = _hrAttendanceSynchronizerSettingDao.GetSynchronizerSetting(url, id);
                if (data != null)
                    throw new DuplicateEntryException("Duplicate Synchronizer Setting found");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        private void SynchronizerVersionCheck(double syncVersion, long id = 0)
        {
            try
            {
                bool isLowerVersion = _hrAttendanceSynchronizerSettingDao.SynchronizerVersionCheck(syncVersion, id);
                if (isLowerVersion)
                    throw new UdvashERP.MessageExceptions.InvalidDataException("Lower version of Synchronizer given.");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion
    }
}
