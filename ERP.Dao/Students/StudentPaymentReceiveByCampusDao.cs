﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Students
{
    public interface IStudentPaymentReceiveByCampusDao : IBaseDao<StudentPaymentReceiveByCampus, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<StudentPaymentReceiveByCampus> LoadTransactionReceiveReportList(IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet, int start, int length);
        #endregion

        #region Others Function
        int GetTransactionReceiveReportCount(IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet);
        #endregion


    }

    public class StudentPaymentReceiveByCampusDao : BaseDao<StudentPaymentReceiveByCampus, long>, IStudentPaymentReceiveByCampusDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function


        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        public IList<StudentPaymentReceiveByCampus> LoadTransactionReceiveReportList(IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet, int start,
            int length)
        {
            var transactionReportCriteria = GetTransactionReceiveReportCriteria(users, userIds, datef, datet);
            if (length > 0)
            {
                return transactionReportCriteria.AddOrder(Order.Desc("ReceiveDate")).SetFirstResult(Convert.ToInt32(start)).SetMaxResults(Convert.ToInt32(length)).List<StudentPaymentReceiveByCampus>();
            }
            else
            {
                return transactionReportCriteria.AddOrder(Order.Desc("ReceiveDate")).List<StudentPaymentReceiveByCampus>();
            }
        }

        public int GetTransactionReceiveReportCount(IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet)
        {
            var transactionReportCriteria = GetTransactionReceiveReportCriteria(users, userIds, datef, datet);
            var rowCount = transactionReportCriteria.SetProjection(Projections.RowCount()).UniqueResult<int>();
            return rowCount;
        }
        #endregion

        #region Helper Function
        private ICriteria GetTransactionReceiveReportCriteria(IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet)
        {
            ICriteria criteria = Session.CreateCriteria<StudentPaymentReceiveByCampus>()
                                .Add(Restrictions.Not(Restrictions.Eq("Status", StudentPaymentReceiveByCampus.EntityStatus.Delete)));

            // long[] userProfileId = new[users.Count()];
            var userProfileIds = users.Select(x => x.Id).ToArray();
            criteria.CreateAlias("ReceiveFromUser", "rfu");

            datet = datet.AddDays(1);
            criteria.Add(Restrictions.Ge("ReceiveDate", datef));
            criteria.Add(Restrictions.Le("ReceiveDate", datet));
            criteria.Add(Restrictions.In("rfu.Id", userProfileIds));
            criteria.Add(Restrictions.In("ReceiveByUser.Id", userIds));
            //criteria.CreateAlias("ReceiveFromCampus", "rfc");
            //criteria.CreateAlias("rfc.Branch", "rfbr");
            return criteria;
        }
        #endregion
    }
}