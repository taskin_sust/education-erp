﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BatchTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Batch")]
    public class BatchLoadActiveTest : BatchBaseTest
    {
        #region Basic Test

        [Fact]
        public void LoadActive_should_return_list()
        {
            _batchFactory.CreateMore(100).Persist();
            var batch = _batchFactory.GetLastCreatedObjectList()[0];
            var organization = batch.Organization;
            var branch = batch.Branch;
            var program = batch.Program;
            var usermenu = BuildUserMenu(organization, program, branch);
            var batchList = _batchService.LoadBatch(usermenu, 0, 100, "", "", _batchFactory.GetLastCreatedObjectList()[0].Organization.Id, null, null, null, null, null, "1");
            Assert.Equal(100, batchList.Count);
        }

        [Fact(Skip = "decision pending")]
        public void LoadAuthorizeBatch_should_return_list()
        {
            _batchFactory.CreateMore(50).Persist();
            var batch = _batchFactory.GetLastCreatedObjectList()[0];
            var organization = batch.Organization;
            var branch = batch.Branch;
            var program = batch.Program;
            var usermenu = BuildUserMenu(organization, program, branch);
            var batchlist = _batchService.LoadAuthorizeBatch(usermenu, null, new List<long>() { _batchFactory.GetLastCreatedObjectList()[0].Program.Id });
            Assert.Equal(50, batchlist.Count);
        }

        [Fact(Skip = "decision pending")]
        public void LoadBatch_should_return_list()
        {

        }

        [Fact(Skip = "decision pending")]
        public void LoadByBatchIds_should_return_list()
        {

        }

        #endregion
    }
}
