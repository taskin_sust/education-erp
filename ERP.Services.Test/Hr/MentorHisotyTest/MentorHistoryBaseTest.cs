﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.MentorHisotyTest
{
    public class MentorHistoryBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initialization

        public const int MentorId = 2;
        private ISession _session;
        protected readonly CommonHelper _commonHelper;
        protected readonly IMentorHistoryService _mentorHistoryService;
        protected readonly MentorHistoryFactory _mentorHistoryFactory;
        protected readonly ITeamMemberService _teamMemberService;
        protected readonly DepartmentFactory _mentorDepartmentFactory;
        protected readonly DesignationFactory _mentorDesignationFactory;
        protected readonly OrganizationFactory _mentorOrganizationFactory;

        public MentorHistoryBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _teamMemberService = new TeamMemberService(_session);
            _mentorHistoryService = new MentorHistoryService(_session);
            _mentorDepartmentFactory = new DepartmentFactory(null, _session);
            _mentorDesignationFactory = new DesignationFactory(null, _session);
            _mentorOrganizationFactory = new OrganizationFactory(null, _session);
            _mentorHistoryFactory = new MentorHistoryFactory(null, _session);

        }

        public void Dispose()
        {
            _mentorHistoryFactory.CleanUp();
            base.Dispose();

        }

        
        #endregion
    }
}
