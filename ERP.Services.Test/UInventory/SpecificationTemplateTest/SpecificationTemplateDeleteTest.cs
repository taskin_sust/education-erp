﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.SpecificationTemplateTest
{
    public interface ISpecificationTemplateDeleteTest
    {
        
        #region Basic Test
        
        void Should_Delete_Successfully(); 
        
        #endregion

        #region Business Logic Test
        
        void Delete_Should_Throw_Dependency_Exception(); 
        
        #endregion

    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "SpecificationCriteria")]
    public class SpecificationTemplateDeleteTest : SpecificationTemplateBaseTest, ISpecificationTemplateDeleteTest
    {
        
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name).Persist();
            var st = _specificationTemplateFactory.Object;
            var success = _specificationTemplateService.Delete(st.Id);
            Assert.True(success);
            var criteriaList = _specificationTemplateFactory.Object.SpecificationCriteriaList;
            CleanUpSpecificationCriteria(criteriaList);
        }

        #endregion

        #region Business Logic Test
       
        [Fact]
        public void Delete_Should_Throw_Dependency_Exception()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name).Persist();
            var st = _specificationTemplateFactory.Object;
            st.Items = new List<Item>()
            {
                new Item()
            };
            Assert.Throws<DependencyException>(() => _specificationTemplateService.Delete(st.Id));
            var criteriaList = _specificationTemplateFactory.Object.SpecificationCriteriaList;
            CleanUpSpecificationCriteria(criteriaList);
        }
       
        #endregion

    }
}
