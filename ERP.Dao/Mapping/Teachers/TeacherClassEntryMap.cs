﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherClassEntryMap : BaseClassMap<TeacherClassEntry, long>
    {
        public TeacherClassEntryMap()
        {
            Table("TeacherClassEntry");
            LazyLoad();
            References(x => x.Organization).Column("OrganizationId");
            References(x => x.TeacherClassType).Column("TeacherClassTypeId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.Branch).Column("HeldBranchId");
            References(x => x.Campus).Column("HeldCampusId");
            Map(x => x.SerialNumber).Column("SerialNumber");
            Map(x => x.HeldDate).Column("HeldDate");
            HasMany(x => x.TeacherClassEntryDetails).KeyColumn("TeacherClassEntryId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
