﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.SupplierTest
{

    [Trait("Area", "UInventory")]
    [Trait("Service", "Supplier")]
    public class SupplierSaveTest : SupplierBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Save_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _supplierService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Return_Save_Successfully()
        {
            var bankDetails = _supplierBankDetailsFactory.Create().WithBankBranch();
            var supplierFactory = _supplierFactory.Create().CreateWithSupplierBankDetails(bankDetails.SingleObjectList).Persist();
            Assert.NotNull(supplierFactory.Object);
        }

        [Fact]
        public void SaveOrUpdate_Should_Return_Tin_Duplicate()
        {
            var supplierFactory = _supplierFactory.CreateMore(2);
            supplierFactory.ObjectList[1].SupplierBankDetails.Add(_supplierBankDetailsFactory.Create().WithBankBranch().Object);
            supplierFactory.ObjectList[0].SupplierBankDetails.Add(_supplierBankDetailsFactory.Create().WithBankBranch().Object);

            _supplierFactory.Persist();
            supplierFactory.ObjectList[1].Tin = supplierFactory.ObjectList[0].Tin;
            Assert.Throws<DuplicateEntryException>(() => _supplierService.SaveOrUpdate(supplierFactory.ObjectList[1]));
        }

        [Fact]
        public void SaveOrUpdate_Should_Return_TlnInc_Duplicate()
        {
            var supplierFactory = _supplierFactory.CreateMore(2);
            supplierFactory.ObjectList[1].SupplierBankDetails.Add(_supplierBankDetailsFactory.Create().WithBankBranch().Object);
            supplierFactory.ObjectList[0].SupplierBankDetails.Add(_supplierBankDetailsFactory.Create().WithBankBranch().Object);
            _supplierFactory.Persist();

            supplierFactory.ObjectList[1].TlnInc = supplierFactory.ObjectList[0].TlnInc;
            Assert.Throws<DuplicateEntryException>(() => _supplierService.SaveOrUpdate(supplierFactory.ObjectList[1]));
        }

        [Fact]
        public void SaveOrUpdate_Should_Return_VatRegistrationNo_Duplicate()
        {
            var supplierFactory = _supplierFactory.CreateMore(2);
            supplierFactory.ObjectList[1].SupplierBankDetails.Add(_supplierBankDetailsFactory.Create().WithBankBranch().Object);
            supplierFactory.ObjectList[0].SupplierBankDetails.Add(_supplierBankDetailsFactory.Create().WithBankBranch().Object);
            _supplierFactory.Persist();
            supplierFactory.ObjectList[1].VatRegistrationNo = supplierFactory.ObjectList[0].VatRegistrationNo;
            Assert.Throws<DuplicateEntryException>(() => _supplierService.SaveOrUpdate(supplierFactory.ObjectList[1]));
        }

        #endregion

        #region Supplier Bank Details Test

        [Fact]
        public void SaveOrUpdate_Should_Return_BankBranch_Exist()
        {
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierBankDetailsFactory.Create().WithBankBranch(_supplierBankDetailsFactory.SingleObjectList[0].BankBranch);

            var supplierFactory = _supplierFactory.Create();
            supplierFactory.Object.SupplierBankDetails.Add(_supplierBankDetailsFactory.SingleObjectList[0]);
            supplierFactory.Persist();

            var supplierFactory1 = _supplierFactory.Create();
            _supplierBankDetailsFactory.SingleObjectList[1].AccountNo = _supplierBankDetailsFactory.SingleObjectList[0].AccountNo;
            supplierFactory1.Object.SupplierBankDetails.Add(_supplierBankDetailsFactory.SingleObjectList[1]);

            Assert.Throws<InvalidDataException>(() => supplierFactory1.Persist());
            Assert.Throws<InvalidDataException>(() => _supplierService.SaveOrUpdate(supplierFactory1.Object));
        }
        
        [Fact]
        public void SaveOrUpdate_Should_Return_Duplicate_BankBranch()
        {
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierBankDetailsFactory.Create().WithBankBranch(_supplierBankDetailsFactory.SingleObjectList[0].BankBranch);
            _supplierBankDetailsFactory.SingleObjectList[1].AccountNo = _supplierBankDetailsFactory.SingleObjectList[0].AccountNo;

            var supplierFactory = _supplierFactory.Create();
            supplierFactory.Object.SupplierBankDetails.Add(_supplierBankDetailsFactory.SingleObjectList[0]);
            supplierFactory.Object.SupplierBankDetails.Add(_supplierBankDetailsFactory.SingleObjectList[1]);
            Assert.Throws<InvalidDataException>(() => _supplierService.SaveOrUpdate(supplierFactory.Object));
        }

        #endregion
    }
}
