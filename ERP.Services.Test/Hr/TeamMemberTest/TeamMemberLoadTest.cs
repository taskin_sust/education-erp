﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Helper;
using Xunit;

namespace UdvashERP.Services.Test.Hr.TeamMemberTest
{
    interface ITeamMemberLoadTest
    {

        #region GetByPin

        void Should_Return_GetByPin_Null_Check();
        void Should_Return_GetByPin_Not_Check();

        #endregion

        #region LoadById

        void Should_Return_LoadById_Null_Check();
        void Should_Return_LoadById_Not_Check();
        
        #endregion

        #region GetTeamMemberByAspNetUserId



        #endregion

        #region GetMember

        void Should_Return_GetMember_Null_Check();
        void Should_Return_GetMember_Not_Null_Check();  
        
        #endregion

        #region GetMemberWeekEnd

        void Should_Return_GetMemberWeekend_Null_Check();
        void Should_Return_GetMemberWeekend_Not_Null_Check();  
        
        #endregion

        #region GetTeamMemberSalaryOrganization

        //void Save_Should_Return_GetTeamMemberSalaryOrganization_Successfully();

        #endregion

        #region GetTeamMemberSalaryOrganization



        #endregion

        #region GetTeamMemberOrganization

        void Should_Return_GetTeamMemberOrganization_Null_Check();
        void Should_Return_GetTeamMemberOrganization_Not_Null_Check();  

        #endregion

        #region GetTeamMemberSalaryBranch
        #endregion

        #region GetTeamMemberBranch
        #endregion

        #region GetTeamMemberCampus

        void Should_Return_GetTeamMemberCampus_Null_Check();
        void Should_Return_GetTeamMemberCampus_Not_Null_Check();

        #endregion

        #region GetTeamMemberDepartment

        void GetTeamMemberDepartment_Null_Check();
        void GetTeamMemberDepartment_Not_Null_Check();

        #endregion

        #region GetTeamMemberSalaryDepartment

        void Should_Return_GetTeamMemberSalaryDepartment_Successfully();
        void Should_Return_GetTeamMemberSalaryDepartment_InvalidException();

        #endregion

        #region GetTeamMemberDesignation

        void Should_Return_GetTeamMemberDesignation_Null_Check();
        void Should_Return_GetTeamMemberDesignation_Not_Null_Check(); 

        #endregion

        #region GetTeamMemberSalaryDesignation

        void Should_Return_GetTeamMemberSalaryDesignation_Successfully();
        void Should_Return_GetTeamMemberSalaryDesignation_InvalidException();

        #endregion

        #region GetTeamMemberTransferInfo



        #endregion

        #region LoadHrAuthorizedTeamMemberId

        void Should_Return_LoadHrAuthorizedTeamMemberId_Successfully();
        void Should_Return_LoadHrAuthorizedTeamMemberId_InvalidException_For_Usermenu();

        #endregion

        #region LoadHrAuthorizedTeamMember



        #endregion

        #region LoadMembersPendingLeave



        #endregion

        #region LoadTeamMemberDirectory



        #endregion

        #region LoadTeamMemberListReport

        void Should_Return_LoadTeamMemberListReport_InvalidException_For_UserMenu();
        void Should_Return_LoadTeamMemberListReport_Successfully();

        #endregion

        #region LoadTeamMemberSearchListReport

        void Should_Return_LoadTeamMemberSearchListReport_InvalidException_For_UserMenu();
        void Should_Return_LoadTeamMemberSearchListReport_Successfully();

        #endregion

        #region LoadSearchTeammemberList

        void Should_Return_LoadSearchTeammemberList_Successfully();
        void Should_Return_LoadSearchTeammemberList_InvalidException_For_UserMenu();

        #endregion

        #region LoadTeamMemberBatchShiftDto

        void Should_Return_LoadTeamMemberBatchShiftDto_InvalidException_For_UserMenu();
        void Should_Return_LoadTeamMemberBatchShiftDto_Successfully();

        #endregion

        #region LoadTeamMemberBatchSalaryHistoryDto

        void Should_Return_LoadTeamMemberBatchSalaryHistoryDto_Successfully();
        void Should_Return_LoadTeamMemberBatchSalaryHistoryDto_InvalidException_For_UserMenu();

        #endregion

        #region AssignTeamMemberToUser

        void AssignTeamMemberToUser_Should_Paremeter_Check();
        //void AssignTeamMemberToUser_Should_Return_Value();

        #endregion

        #region GetCurrentTeamMemberNomineeInfo



        #endregion

        #region CountTeamMemberDirectory

        void Should_Return_CountTeamMemberDirectory_Successfully();
        void Should_Return_CountTeamMemberDirectory_InvalidException_For_UserMenu();

        #endregion

        #region CountTeamMemberListReport

        void Should_Return_CountTeamMemberListReport_Successfully();
        void Should_Return_CountTeamMemberListReport_InvalidException_For_UserMenu();

        #endregion

        #region CountTeammemberSearchListReport

        void Should_Return_CountTeammemberSearchListReport_Successfully();
        void Should_Return_CountTeammemberSearchListReport_InvalidException_For_UserMenu();

        #endregion

        #region GetCountSearchTeammemberList

        void Should_Return_GetCountSearchTeammemberList_InvalidException_For_UserMenu();
        void Should_Return_GetCountSearchTeammemberList_Successfully();

        #endregion

        #region CheckHrAuthorizedTeamMember

        void Should_Return_CheckHrAuthorizedTeamMember_InvalidException_For_UserMenu();
        void Should_Return_CheckHrAuthorizedTeamMember_Successfully();

        #endregion

        #region GetCurrentInfos



        #endregion

        #region LoadTeamMemberListForFbSheet

        void Should_Return_LoadTeamMemberListForFbSheet_Successfully();
        void Should_Return_LoadTeamMemberListForFbSheet_Invalid_Exception_For_Usermenu();
        void Should_Return_LoadTeamMemberListForFbSheet_Invalid_Exception_For_Object();
        void Should_Return_LoadTeamMemberListForFbSheet_Invalid_Exception_For_religion();

        #endregion
    }

    [Trait("Area", "Hr")]
    [Trait("Service", "TeamMember")]
    public class TeamMemberLoadTest : TeamMemberBaseTest, ITeamMemberLoadTest
    {

        #region GetByPin

        [Fact]
        public void Should_Return_GetByPin_Null_Check()
        {
            DateTime joiningDate = DateTime.Now;
            var tm = PersistTeamMember();
            var getObj = teamMemberService.GetByPin(tm.Pin, joiningDate);
            Assert.NotNull(getObj);

        }
        [Fact]
        public void Should_Return_GetByPin_Not_Check()
        {
            var getObj = teamMemberService.GetByPin(-1);
            Assert.Null(getObj);
        }

        #endregion

        #region LoadById

        [Fact]
        public void Should_Return_LoadById_Null_Check()
        {
            var teamMember = PersistTeamMember();
            var getObj = teamMemberService.LoadById(teamMember.Id);
            Assert.NotNull(getObj);
        }
        [Fact]
        public void Should_Return_LoadById_Not_Check()
        {
            var getObj = teamMemberService.LoadById(-1);
            Assert.Null(getObj);
        }
        
        #endregion

        #region GetTeamMemberByAspNetUserId

        

        #endregion

        #region GetMember

        [Fact]
        public void Should_Return_GetMember_Null_Check()
        {
            var tm = PersistTeamMember();
            var teamMember = teamMemberService.GetMember(tm.Pin + 1);
            Assert.Null(teamMember);
        }
        [Fact]
        public void Should_Return_GetMember_Not_Null_Check()
        {
            var tm = PersistTeamMember();
            var teamMember = teamMemberService.GetMember(tm.Pin);
            Assert.NotNull(teamMember);
            Assert.NotEqual(teamMember.Id, 0);
        }

        #endregion

        #region GetMemberWeekEnd

        [Fact]
        public void Should_Return_GetMemberWeekend_Null_Check()
        {
            var tm = PersistTeamMember();
            var shiftWeekend = teamMemberService.GetMemberWeekEnd(tm.Id + 1);
            Assert.NotEqual(tm.ShiftWeekendHistory.Select(x => x.Weekend).FirstOrDefault(), shiftWeekend);
        }
        [Fact]
        public void Should_Return_GetMemberWeekend_Not_Null_Check()
        {
            var tm = PersistTeamMember();
            var shiftWeekend = teamMemberService.GetMemberWeekEnd(tm.Id);
            Assert.Equal(tm.ShiftWeekendHistory.Select(x => x.Weekend).FirstOrDefault(), shiftWeekend);
        }

        #endregion
        
        #region GetTeamMemberOrganization

        [Fact]
        public void Should_Return_GetTeamMemberOrganization_Null_Check()
        {
            //PersistTeamMember();
            Assert.Throws<InvalidDataException>(() => teamMemberService.GetTeamMemberOrganization());
        }
        [Fact]
        public void Should_Return_GetTeamMemberOrganization_Not_Null_Check()
        {
            var tm = PersistTeamMember();
            var org = teamMemberService.GetTeamMemberOrganization(null, tm.Id, tm.Pin);
            Assert.NotNull(org);
        }

        #endregion
        
        #region GetTeamMemberCampus

        [Fact]
        public void Should_Return_GetTeamMemberCampus_Null_Check()
        {
            //PersistTeamMember();
            Assert.Throws<InvalidDataException>(() => teamMemberService.GetTeamMemberCampus());
        }
        [Fact]
        public void Should_Return_GetTeamMemberCampus_Not_Null_Check()
        {
            var tm = PersistTeamMember();
            var campus = teamMemberService.GetTeamMemberCampus(null, tm.Id, tm.Pin);
            Assert.NotNull(campus);
        }

        #endregion

        #region GetTeamMemberDepartment

        [Fact]
        public void GetTeamMemberDepartment_Null_Check()
        {
            PersistTeamMember();
            Assert.Throws<InvalidDataException>(() => teamMemberService.GetTeamMemberDepartment());
        }
        [Fact]
        public void GetTeamMemberDepartment_Not_Null_Check()
        {
            var tm = PersistTeamMember();
            var dept = teamMemberService.GetTeamMemberDepartment(null, tm.Id, tm.Pin);
            Assert.NotNull(dept);
        }

        #endregion

        #region GetTeamMemberSalaryDepartment

        [Fact]
        public void Should_Return_GetTeamMemberSalaryDepartment_Successfully()
        {
            var tm = PersistTeamMember();
            _salaryHistoryFactory.Create().WithTeamMember(tm)
                .WithCampus(tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault())
                .WithDepartment(tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault())
                .WithOrganization(tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault())
                .WithDesignation(tm.EmploymentHistory.Select(x=>x.Designation).FirstOrDefault())
                .Persist();
            var department = teamMemberService.GetTeamMemberSalaryDepartment(null,tm.Id, tm.Pin);
            Assert.Equal(tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault().Name, department.Name);
        }

        [Fact]
        public void Should_Return_GetTeamMemberSalaryDepartment_InvalidException()
        {
            Assert.Throws<InvalidDataException>(
                () => teamMemberService.GetTeamMemberSalaryDesignation(DateTime.Now.AddDays(-15).ToString()));
        }

        #endregion

        #region GetTeamMemberDesignation

        [Fact]
        public void Should_Return_GetTeamMemberDesignation_Null_Check()
        {
            PersistTeamMember();
            Assert.Throws<InvalidDataException>(() => teamMemberService.GetTeamMemberDesignation());
        }
        [Fact]
        public void Should_Return_GetTeamMemberDesignation_Not_Null_Check()
        {
            var tm = PersistTeamMember();
            var designation = teamMemberService.GetTeamMemberDesignation(null, tm.Id, tm.Pin);
            Assert.NotNull(designation);
        }

        #endregion

        #region GetTeamMemberSalaryDesignation

        [Fact]
        public void Should_Return_GetTeamMemberSalaryDesignation_Successfully()
        {
            var tm = PersistTeamMember();
            var designation = teamMemberService.GetTeamMemberSalaryDesignation(DateTime.Now.ToString(),
                tm.Id, tm.Pin);
            Assert.Equal(tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault().Name, designation.Name);
        }

        [Fact]
        public void Should_Return_GetTeamMemberSalaryDesignation_InvalidException()
        {
            Assert.Throws<InvalidDataException>(
                () => teamMemberService.GetTeamMemberSalaryDesignation(DateTime.Now.ToString()));
        }

        #endregion

        #region GetTeamMemberTransferInfo



        #endregion

        #region LoadHrAuthorizedTeamMemberId
        
        [Fact]
        public void Should_Return_LoadHrAuthorizedTeamMemberId_Successfully()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            var pin = tm.Pin;
            var userMenu = BuildUserMenu(org, (Program) null, branch);

            var list = teamMemberService.LoadHrAuthorizedTeamMemberId(userMenu,DateTime.Now,commonHelper.ConvertIdToList(org.Id),
                commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(department.Id),
                commonHelper.ConvertIdToList(designation.Id), commonHelper.ConvertIdToList(pin));
            
            Assert.NotEqual(0, list.Count());
        }

        [Fact]
        public void Should_Return_LoadHrAuthorizedTeamMemberId_InvalidException_For_Usermenu()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            var pin = tm.Pin;
           // var userMenu = BuildUserMenu(org, (Program)null, branch);
            
            Assert.Throws<InvalidDataException>(() => teamMemberService.LoadHrAuthorizedTeamMemberId(null, DateTime.Now, commonHelper.ConvertIdToList(org.Id),
                commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(department.Id),
                commonHelper.ConvertIdToList(designation.Id), commonHelper.ConvertIdToList(pin)));
        }

        #endregion

        #region LoadHrAuthorizedTeamMember

        [Fact]
        public void Should_Return_LoadHrAuthorizedTeamMember_Successfully()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            var pin = tm.Pin;
            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var list = teamMemberService.LoadHrAuthorizedTeamMember(userMenu, DateTime.Now, commonHelper.ConvertIdToList(org.Id),
                commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(department.Id),
                commonHelper.ConvertIdToList(designation.Id), commonHelper.ConvertIdToList(pin));

            Assert.NotEqual(0, list.Count());
        }

        [Fact]
        public void Should_Return_LoadHrAuthorizedTeamMember_InvalidException_For_Usermenu()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            var pin = tm.Pin;
            // var userMenu = BuildUserMenu(org, (Program)null, branch);

            Assert.Throws<InvalidDataException>(() => teamMemberService.LoadHrAuthorizedTeamMember(null, DateTime.Now, commonHelper.ConvertIdToList(org.Id),
                commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(department.Id),
                commonHelper.ConvertIdToList(designation.Id), commonHelper.ConvertIdToList(pin)));
        }

        #endregion

        #region LoadMembersPendingLeave



        #endregion

        #region LoadTeamMemberListReport

        [Fact]
        public void Should_Return_LoadTeamMemberListReport_Successfully()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            
            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var list = teamMemberService.LoadTeamMemberListReport(10,0,"","",100,userMenu,org.Id,department.Id,
                commonHelper.ConvertIdToList(branch.Id).ToArray()
                ,commonHelper.ConvertIdToList(campus.Id).ToArray(),null,null);

            Assert.NotEqual(0, list.Count());
        }

        [Fact]
        public void Should_Return_LoadTeamMemberListReport_InvalidException_For_UserMenu()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            
            Assert.Throws<InvalidDataException>(() => teamMemberService.LoadTeamMemberListReport(10, 0, "", "", 100,
                null, org.Id, department.Id, commonHelper.ConvertIdToList(branch.Id).ToArray()
                , commonHelper.ConvertIdToList(campus.Id).ToArray(), null, null));
        }


        #endregion

        #region LoadTeamMemberSearchListReport

        [Fact]
        public void Should_Return_LoadTeamMemberSearchListReport_Successfully()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
           // var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var list = teamMemberService.LoadTeamMemberSearchListReport(0,10,"","",userMenu,org.Id,department.Id
                ,null,null,tm.Pin.ToString(),null);

            Assert.NotEqual(0, list.Count());
        }

        [Fact]
        public void Should_Return_LoadTeamMemberSearchListReport_InvalidException_For_UserMenu()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            Assert.Throws<InvalidDataException>(() => teamMemberService.LoadTeamMemberSearchListReport(0, 10, "", "",
                null,org.Id, department.Id, null, null, tm.Pin.ToString(), null));
        }

        #endregion

        #region LoadSearchTeammemberList

        [Fact]
        public void Should_Return_LoadSearchTeammemberList_Successfully()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var list = teamMemberService.LoadSearchTeammemberList(0,10,"","",userMenu,org.Id,
                commonHelper.ConvertIdToList(department.Id),commonHelper.ConvertIdToList(branch.Id)
                , commonHelper.ConvertIdToList(campus.Id), null, null, tm.Pin.ToString(), new List<string>() { TeamMemberReportConstants.Pin });

            Assert.NotEqual(0, list.Count());
        }

        [Fact]
        public void Should_Return_LoadSearchTeammemberList_InvalidException_For_UserMenu()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            Assert.Throws<InvalidDataException>(() => teamMemberService.LoadSearchTeammemberList(0, 10, "", "", null, org.Id, 
                commonHelper.ConvertIdToList(department.Id), commonHelper.ConvertIdToList(branch.Id),
                commonHelper.ConvertIdToList(campus.Id), null, null, tm.Pin.ToString(), new List<string>() { TeamMemberReportConstants.Pin }));
        }

        #endregion

        #region LoadTeamMemberBatchShiftDto

        [Fact]
        public void Should_Return_LoadTeamMemberBatchShiftDto_Successfully()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            var shift = tm.ShiftWeekendHistory.Select(x => x.Shift).FirstOrDefault();
            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var list = teamMemberService.LoadTeamMemberBatchShiftDto(userMenu,org.Id,commonHelper.ConvertIdToList(branch.Id),
                commonHelper.ConvertIdToList(campus.Id),null,commonHelper.ConvertIdToList(department.Id),
                commonHelper.ConvertIdToList(designation.Id),commonHelper.ConvertIdToList(shift.Id),null);

            Assert.NotEqual(0, list.Count());
        }

        [Fact]
        public void Should_Return_LoadTeamMemberBatchShiftDto_InvalidException_For_UserMenu()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            var shift = tm.ShiftWeekendHistory.Select(x => x.Shift).FirstOrDefault();
            //var userMenu = BuildUserMenu(org, (Program)null, branch);

            Assert.Throws<InvalidDataException>(() => teamMemberService.LoadTeamMemberBatchShiftDto(null, org.Id, commonHelper.ConvertIdToList(branch.Id),
                commonHelper.ConvertIdToList(campus.Id), null, commonHelper.ConvertIdToList(department.Id),
                commonHelper.ConvertIdToList(designation.Id), commonHelper.ConvertIdToList(shift.Id), null));
        }

        #endregion

        #region LoadTeamMemberBatchSalaryHistoryDto

        [Fact]
        public void Should_Return_LoadTeamMemberBatchSalaryHistoryDto_Successfully()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            //var shift = tm.ShiftWeekendHistory.Select(x => x.Shift).FirstOrDefault();
            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var list = teamMemberService.LoadTeamMemberBatchSalaryHistoryDto(userMenu,org.Id,commonHelper.ConvertIdToList(branch.Id)
                ,commonHelper.ConvertIdToList(campus.Id),null,commonHelper.ConvertIdToList(department.Id),
                commonHelper.ConvertIdToList(designation.Id)
                ,true);

            Assert.NotEqual(0, list.Count());
        }

        [Fact]
        public void Should_Return_LoadTeamMemberBatchSalaryHistoryDto_InvalidException_For_UserMenu()
        {
            var tm = PersistTeamMember();
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            //var shift = tm.ShiftWeekendHistory.Select(x => x.Shift).FirstOrDefault();
            //var userMenu = BuildUserMenu(org, (Program)null, branch);

            Assert.Throws<InvalidDataException>(() => teamMemberService.LoadTeamMemberBatchSalaryHistoryDto(null, org.Id, commonHelper.ConvertIdToList(branch.Id)
                , commonHelper.ConvertIdToList(campus.Id), null, commonHelper.ConvertIdToList(department.Id), 
                commonHelper.ConvertIdToList(designation.Id)
                , true));
        }

        #endregion

        #region AssignTeamMemberToUser

        [Fact]
        public void AssignTeamMemberToUser_Should_Paremeter_Check()
        {
            DateTime startDate = new DateTime(2016,6,1);
            DateTime endDate = new DateTime(2016,6,1);
            var tm = PersistTeamMember(startDate, endDate);

            AspNetUser baseUser = userService.LoadbyAspNetUserId(72);
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            userProfileFactory.Create().WithAspNetUser(baseUser).WithCampus(campus).WithBranch(campus.Branch).Persist();
            Assert.Throws<InvalidDataException>(() => teamMemberService.AssignTeamMemberToUser(tm.Id + 1, userProfileFactory.Object.Id + 1));
        }

        #region Discussion
        //[Fact]
        //public void AssignTeamMemberToUser_Should_Return_Value()
        //{
        //    DateTime startDate = DateTime.Now.AddYears(-3);
        //    DateTime endDate = DateTime.Now.AddYears(-1);
        //    AspNetUser baseUser = userService.LoadbyAspNetUserId(72);
        //    //TODO:Initially create team member without mentor for testing. Mentor as a team member  
        //    var mentorObj = teamMemberService.GetMember(160);
        //    var mentorEmployment = mentorObj.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

        //    _organizationFactory.Create().Persist();
        //    _employmentHistoryFactory.CreateMore(1, _organizationFactory.Object);
        //    _maritalInfoFactory.CreateMore(1);
        //    _shiftWeekendHistoryFactory.CreateMore(1, _organizationFactory.Object);

        //    var dept = _employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
        //    var designation = _employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();
        //    var campus = _employmentHistoryFactory.ObjectList.Select(x => x.Campus).FirstOrDefault();
        //    _mentorHistoryFactory.CreateMore(1, mentorObj, dept, designation, _organizationFactory.Object
        //        , mentorEmployment.Department, mentorEmployment.Designation, mentorEmployment.Department.Organization);
        //    _userProfileFactory.Create().WithAspNetUser(baseUser).WithCampus(campus).WithBranch(campus.Branch).Persist();
        //    _teamMemberFactory.Create()
        //        .WithEmploymentHistory(_employmentHistoryFactory.ObjectList)
        //        .WithShiftWeekend(_shiftWeekendHistoryFactory.ObjectList)
        //        .WithEffectiveStartAndEndDate(startDate, endDate)
        //        .WithMaritalInfo(_maritalInfoFactory.ObjectList)
        //        .WithMentorHistory(_mentorHistoryFactory.ObjectList)
        //        .Persist();
        //    var returnTrue = teamMemberService.AssignTeamMemberToUser(_teamMemberFactory.Object.Id, _userProfileFactory.Object.AspNetUser.Id);
        //    Assert.True(returnTrue);
        //}
        #endregion

        #endregion
        
        #region GetCurrentTeamMemberNomineeInfo



        #endregion

        #region CountTeamMemberDirectory

        [Fact]
        public void Should_Return_CountTeamMemberDirectory_Successfully()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(startDate, endDate);
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            //var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            var pin = tm.Pin;
            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var listCount = teamMemberService.CountTeamMemberDirectory(userMenu, org.Id, branch.Id, campus.Id,
                department.Id, tm.Name, pin, tm.BloodGroup);

            Assert.NotEqual(0, listCount);
        }

        [Fact]
        public void Should_Return_CountTeamMemberDirectory_InvalidException_For_UserMenu()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(startDate, endDate);
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            //var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            var pin = tm.Pin;
            //var userMenu = BuildUserMenu(org, (Program)null, branch);

            Assert.Throws<InvalidDataException>(() => teamMemberService.CountTeamMemberDirectory(null, org.Id, branch.Id, campus.Id,
                department.Id, tm.Name, pin, null));
        }

        #endregion

        #region CountTeamMemberListReport

        [Fact]
        public void Should_Return_CountTeamMemberListReport_Successfully()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(startDate, endDate);
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            //var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            //var pin = tm.Pin;
            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var listCount = teamMemberService.CountTeamMemberListReport(userMenu,org.Id,department.Id,
                commonHelper.ConvertIdToList(branch.Id).ToArray(),commonHelper.ConvertIdToList(campus.Id).ToArray(),null,null);

            Assert.NotEqual(0, listCount);
        }

        [Fact]
        public void Should_Return_CountTeamMemberListReport_InvalidException_For_UserMenu()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(startDate, endDate);
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            
            Assert.Throws<InvalidDataException>(() => teamMemberService.CountTeamMemberListReport(null, org.Id, department.Id,
                commonHelper.ConvertIdToList(branch.Id).ToArray(), commonHelper.ConvertIdToList(campus.Id).ToArray(), null, null));
        }

        #endregion

        #region CountTeammemberSearchListReport

        [Fact]
        public void Should_Return_CountTeammemberSearchListReport_Successfully()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(startDate, endDate);
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            //var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            //var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            //var pin = tm.Pin;
            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var listCount = teamMemberService.CountTeammemberSearchListReport(userMenu,org.Id,department.Id,null,null,"",null);

            Assert.NotEqual(0, listCount);
        }

        [Fact]
        public void Should_Return_CountTeammemberSearchListReport_InvalidException_For_UserMenu()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(startDate, endDate);
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            //var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            //var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            Assert.Throws<InvalidDataException>(() => teamMemberService.CountTeammemberSearchListReport(null, org.Id,
                department.Id, null, null, "", null));
        }

        #endregion

        #region GetCountSearchTeammemberList

        [Fact]
        public void Should_Return_GetCountSearchTeammemberList_Successfully()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(startDate, endDate);
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            //var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            //var pin = tm.Pin;
            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var listCount = teamMemberService.GetCountSearchTeammemberList(userMenu,org.Id,commonHelper.ConvertIdToList(department.Id)
                ,commonHelper.ConvertIdToList(branch.Id),commonHelper.ConvertIdToList(campus.Id),null,null,"",null);

            Assert.NotEqual(0, listCount);
        }

        [Fact]
        public void Should_Return_GetCountSearchTeammemberList_InvalidException_For_UserMenu()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(startDate, endDate);
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            Assert.Throws<InvalidDataException>(() => teamMemberService.GetCountSearchTeammemberList(null, org.Id, 
                commonHelper.ConvertIdToList(department.Id), commonHelper.ConvertIdToList(branch.Id),
                commonHelper.ConvertIdToList(campus.Id), null, null, "", null));
        }

        #endregion

        #region CheckHrAuthorizedTeamMember

        [Fact]
        public void Should_Return_CheckHrAuthorizedTeamMember_Successfully()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(startDate, endDate);
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            //var pin = tm.Pin;
            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var list = teamMemberService.CheckHrAuthorizedTeamMember(userMenu,tm.Pin,null,commonHelper.ConvertIdToList(org.Id)
                ,commonHelper.ConvertIdToList(branch.Id),commonHelper.ConvertIdToList(campus.Id)
                ,commonHelper.ConvertIdToList(department.Id),commonHelper.ConvertIdToList(designation.Id));

            Assert.True(list);
        }
        
        [Fact]
        public void Should_Return_CheckHrAuthorizedTeamMember_InvalidException_For_UserMenu()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(startDate, endDate);
            var org = tm.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = tm.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();

            Assert.Throws<InvalidDataException>(() => teamMemberService.CheckHrAuthorizedTeamMember(null, tm.Pin, null, commonHelper.ConvertIdToList(org.Id)
                , commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id)
                , commonHelper.ConvertIdToList(department.Id), commonHelper.ConvertIdToList(designation.Id)));
        }

        #endregion

        #region GetCurrentInfos



        #endregion

        #region LoadTeamMemberListForFbSheet

        [Fact]
        public void Should_Return_LoadTeamMemberListForFbSheet_Successfully()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var teamMember = PersistTeamMember(startDate, endDate);
            var department = teamMember.EmploymentHistory.FirstOrDefault().Department;
            var org = teamMember.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory.FirstOrDefault().Campus;

            _festivalBonusSettingFactory.CreateWith(0, "TestFestivalBonus", DateTime.Now.AddDays(3), DateTime.Now.AddDays(4),true,false,false,false,false)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .WithOrganization(department.Organization).Persist();

            _salaryHistoryFactory.CreateWith(100, 80, 20, new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
                .WithCampus(campus)
                .WithDepartment(department)
                .WithOrganization(org)
                .WithDesignation(teamMember.EmploymentHistory.Select(x => x.Designation).FirstOrDefault())
                .WithTeamMember(teamMember).Persist();

            var emplomentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var userMenu = BuildUserMenu(department.Organization, (Program) null, campus.Branch);
            var viewModel = new FestivalBonusSheetFormViewModel()
            {
                BonusMonth = emplomentHistory.EffectiveDate.Month,
                BonusYear = emplomentHistory.EffectiveDate.Year,
                BranchId = campus.Branch.Id,
                CampusId = campus.Id,
                DepartmentId = department.Id,
                FestivalBonusSettingId = _festivalBonusSettingFactory.Object.Id,
                OrganizationId = department.Organization.Id,
                OrganizationType = OrganizationType.JobOrganization
            };
            var lastDayOfMonth = DateTime.DaysInMonth(viewModel.BonusYear, viewModel.BonusMonth);
            var list = teamMemberService.LoadTeamMemberListForFbSheet(userMenu,
                new DateTime(viewModel.BonusYear, viewModel.BonusMonth, 1)
                , new DateTime(viewModel.BonusYear, viewModel.BonusMonth, lastDayOfMonth), new List<int>() {1},
                viewModel);
            Assert.Equal(1,list.Count());
        }

        [Fact]
        public void Should_Return_LoadTeamMemberListForFbSheet_Invalid_Exception_For_Usermenu()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var teamMember = PersistTeamMember(startDate, endDate);
            var department = teamMember.EmploymentHistory.FirstOrDefault().Department;
            var org = teamMember.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory.FirstOrDefault().Campus;

            _festivalBonusSettingFactory.CreateWith(0, "TestFestivalBonus", DateTime.Now.AddDays(3), DateTime.Now.AddDays(4), true, false, false, false, false)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .WithOrganization(department.Organization).Persist();

            _salaryHistoryFactory.CreateWith(100, 80, 20, new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
                .WithCampus(campus)
                .WithDepartment(department)
                .WithOrganization(org)
                .WithDesignation(teamMember.EmploymentHistory.Select(x => x.Designation).FirstOrDefault())
                .WithTeamMember(teamMember).Persist();

            var emplomentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            //var userMenu = BuildUserMenu(department.Organization, (Program)null, campus.Branch);
            var viewModel = new FestivalBonusSheetFormViewModel()
            {
                BonusMonth = emplomentHistory.EffectiveDate.Month,
                BonusYear = emplomentHistory.EffectiveDate.Year,
                BranchId = campus.Branch.Id,
                CampusId = campus.Id,
                DepartmentId = department.Id,
                FestivalBonusSettingId = _festivalBonusSettingFactory.Object.Id,
                OrganizationId = department.Organization.Id,
                OrganizationType = OrganizationType.JobOrganization
            };
            var lastDayOfMonth = DateTime.DaysInMonth(viewModel.BonusYear, viewModel.BonusMonth);
            
            Assert.Throws<InvalidDataException>(() => teamMemberService.LoadTeamMemberListForFbSheet(null,
                new DateTime(viewModel.BonusYear, viewModel.BonusMonth, 1)
                , new DateTime(viewModel.BonusYear, viewModel.BonusMonth, lastDayOfMonth), new List<int>() {1},
                viewModel));
        }
        [Fact]
        public void Should_Return_LoadTeamMemberListForFbSheet_Invalid_Exception_For_Object()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var teamMember = PersistTeamMember(startDate, endDate);
            var department = teamMember.EmploymentHistory.FirstOrDefault().Department;
            var org = teamMember.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory.FirstOrDefault().Campus;

            _festivalBonusSettingFactory.CreateWith(0, "TestFestivalBonus", DateTime.Now.AddDays(3), DateTime.Now.AddDays(4), true, false, false, false, false)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .WithOrganization(department.Organization).Persist();

            _salaryHistoryFactory.CreateWith(100, 80, 20, new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
                .WithCampus(campus)
                .WithDepartment(department)
                .WithOrganization(org)
                .WithDesignation(teamMember.EmploymentHistory.Select(x => x.Designation).FirstOrDefault())
                .WithTeamMember(teamMember).Persist();

            var emplomentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var userMenu = BuildUserMenu(department.Organization, (Program)null, campus.Branch);
            var viewModel = new FestivalBonusSheetFormViewModel()
            {
                BonusMonth = emplomentHistory.EffectiveDate.Month,
                BonusYear = emplomentHistory.EffectiveDate.Year,
                BranchId = campus.Branch.Id,
                CampusId = campus.Id,
                DepartmentId = department.Id,
                FestivalBonusSettingId = _festivalBonusSettingFactory.Object.Id,
                OrganizationId = department.Organization.Id,
                OrganizationType = OrganizationType.JobOrganization
            };
            var lastDayOfMonth = DateTime.DaysInMonth(viewModel.BonusYear, viewModel.BonusMonth);

            Assert.Throws<InvalidDataException>(() => teamMemberService.LoadTeamMemberListForFbSheet(userMenu,
                new DateTime(viewModel.BonusYear, viewModel.BonusMonth, 1)
                , new DateTime(viewModel.BonusYear, viewModel.BonusMonth, lastDayOfMonth), new List<int>() { 1 },
                null));
        }
        [Fact]
        public void Should_Return_LoadTeamMemberListForFbSheet_Invalid_Exception_For_religion()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            DateTime endDate = new DateTime(2016, 6, 1);
            var teamMember = PersistTeamMember(startDate, endDate);
            var department = teamMember.EmploymentHistory.FirstOrDefault().Department;
            var org = teamMember.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            var campus = teamMember.EmploymentHistory.FirstOrDefault().Campus;

            _festivalBonusSettingFactory.CreateWith(0, "TestFestivalBonus", DateTime.Now.AddDays(3), DateTime.Now.AddDays(4), true, false, false, false, false)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .WithOrganization(department.Organization).Persist();

            _salaryHistoryFactory.CreateWith(100, 80, 20, new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
                .WithCampus(campus)
                .WithDepartment(department)
                .WithOrganization(org)
                .WithDesignation(teamMember.EmploymentHistory.Select(x => x.Designation).FirstOrDefault())
                .WithTeamMember(teamMember).Persist();

            var emplomentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var userMenu = BuildUserMenu(department.Organization, (Program)null, campus.Branch);
            var viewModel = new FestivalBonusSheetFormViewModel()
            {
                BonusMonth = emplomentHistory.EffectiveDate.Month,
                BonusYear = emplomentHistory.EffectiveDate.Year,
                BranchId = campus.Branch.Id,
                CampusId = campus.Id,
                DepartmentId = department.Id,
                FestivalBonusSettingId = _festivalBonusSettingFactory.Object.Id,
                OrganizationId = department.Organization.Id,
                OrganizationType = OrganizationType.JobOrganization
            };
            var lastDayOfMonth = DateTime.DaysInMonth(viewModel.BonusYear, viewModel.BonusMonth);

            Assert.Throws<InvalidDataException>(() => teamMemberService.LoadTeamMemberListForFbSheet(userMenu,
                new DateTime(viewModel.BonusYear, viewModel.BonusMonth, 1)
                , new DateTime(viewModel.BonusYear, viewModel.BonusMonth, lastDayOfMonth), new List<int>() {  },
                viewModel));
        }

        #endregion
    }
}
