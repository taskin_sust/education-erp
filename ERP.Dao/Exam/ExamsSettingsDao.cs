﻿using System.Collections.Generic;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Exam;

namespace UdvashERP.Dao.Exam
{
    public interface IExamsSettingsDao : IBaseDao<ExamsSettings, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Functions
        IList<ExamsSettings> LoadByType(int type); 
        #endregion

        #region Others Function
        #endregion
    }
    public class ExamsSettingsDao : BaseDao<ExamsSettings, long>, IExamsSettingsDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Functions
        public IList<ExamsSettings> LoadByType(int type)
        {
            return Session.QueryOver<ExamsSettings>().Where(x => x.Type == type).List<ExamsSettings>();
        }
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion
    }
}
