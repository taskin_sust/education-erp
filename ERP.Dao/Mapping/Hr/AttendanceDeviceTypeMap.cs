﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Hr
{
    public class AttendanceDeviceTypeMap : BaseClassMap<AttendanceDeviceType, long>
    {
        public AttendanceDeviceTypeMap()
        {
            Table("HR_AttendanceDeviceType");
            LazyLoad();
            Map(x => x.Name).Column("Name");
            Map(x => x.Code).Column("Code");
            Map(x => x.IsFinger).Column("IsFinger");
            Map(x => x.FirmwareVersion).Column("FirmwareVersion");
            Map(x => x.AlgorithmVersion).Column("AlgorithmVersion");
            Map(x => x.AttendanceLogCapacity).Column("AttendanceLogCapacity");
            Map(x => x.UserCapacity).Column("UserCapacity");
            Map(x => x.TemplateCapacity).Column("TemplateCapacity");
            Map(x => x.MaxDisplayName).Column("MaxDisplayName");
            //HasMany(x => x.MemberFingerPrints).KeyColumn("AttendanceDeviceTypeId").Cascade.AllDeleteOrphan();
            HasMany(x => x.AttendanceDevices).KeyColumn("AttendanceDeviceTypeId");
        }
    }
}
