﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.NightWorkALlowanceSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "NightWorkAllowanceSettingService")]

    public class NightWorkAllowanceLoadTest : NightWorkAllowanceSettingBaseTest
    {
        #region List Load Test

        [Fact]
        public void Should_Return_Invalid_Permission_Exception()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);

            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.LoadAll(0, 0, int.MaxValue, null, organizationFactory.Object.Id, (int)MemberEmploymentStatus.Contractual));
        }

        [Fact]
        public void Should_Return_UnAuthorise_UserPermission_Of_Organization_Successfully()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            var newMenu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Create().Persist().Object));
            var list = _nightWorkAllowanceSettingService.LoadAll(0, 0, int.MaxValue, newMenu, organizationFactory.Object.Id, (int)MemberEmploymentStatus.Contractual);
            Assert.Equal(list.Count, 0);
        }

        #endregion

        #region Single Object Load

        //TODO: Try to load an organization's night work allowance which user have not permission
        //Service does not contain any method which will return single allowance object for organization
        //There is a method named LoadAll which will serve this purpose and have already written this test case 
        
        public void Should_Return_Invalid_Data_Permission()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);

            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.GetById(0));
        }


        [Fact]
        public void Should_Return_Invalid_Data_Exception()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);

            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.GetById(0));
        }

        [Fact]
        public void Should_Return_Obj_Successfully()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);

            Assert.Equal(_nightworkAllowanceSettingFactory.Object.Id, _nightWorkAllowanceSettingService.GetById(_nightworkAllowanceSettingFactory.Object.Id).Id);
        }

        #endregion

        #region Count Test

        [Fact]
        public void Should_Return_Invalid_Data_Exception_ForCount()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            //var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object)); //TODO: if you do not pass this then do not need to create it.
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.AllowanceCount(null, _nightworkAllowanceSettingFactory.Object.Organization.Id, (int)MemberEmploymentStatus.Contractual));
        }

        [Fact]
        public void Should_Return_One_Data_Successfully()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            var count = _nightWorkAllowanceSettingService.AllowanceCount(menu,
                _nightworkAllowanceSettingFactory.Object.Organization.Id, (int)MemberEmploymentStatus.Contractual);
            Assert.True(count > 0);
        }

        //TODO: Write test for loading settings by page.

        #endregion
    }
}
