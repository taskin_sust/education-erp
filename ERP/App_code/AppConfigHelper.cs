﻿using System;
using System.Configuration;
using System.Drawing;
using System.Text;

namespace UdvashERP.App_code 
{
    public static class AppConfigHelper
    {
        public static bool GetBoolean(string key)
        {
            return GetBoolean(key, false);
        }

        public static bool GetBoolean(string key, bool defaultValue)
        {
            bool result = defaultValue;
            if (bool.TryParse(GetString(key), out result) == false)
                result = defaultValue;
            return result;
        }


        public static int GetInt32(string key)
        {
            return GetInt32(key, 0);
        }

        public static int GetInt32(string key, int defaultValue)
        {
            int result = defaultValue;
            if (int.TryParse(GetString(key), out result) == false)
                result = defaultValue;
            return result;
        }


        public static decimal GetDecimal(string key)
        {
            return GetDecimal(key, 0);
        }

        public static decimal GetDecimal(string key, decimal defaultValue)
        {
            decimal result = defaultValue;
            if (decimal.TryParse(GetString(key), out result) == false)
                result = defaultValue;
            return result;
        }


        public static object GetEnum(string key, Type enumType, object defaultValue)
        {
            object result = null;
            try
            {
                result = Enum.Parse(enumType, GetString(key), true);
            }
            catch { }
            if (result == null)
                result = defaultValue;
            return result;
        }


        public static string GetString(string key)
        {
                return GetString(key, "");
        }

        public static string GetString(string key, string defaultValue)
        {
            if (ConfigurationManager.AppSettings[key] == null)
                return defaultValue;
            else
                return ConfigurationManager.AppSettings[key];
        }

        public static Color GetColorByArgb(string key, bool isCsv)
        {
            return GetColorByArgb(key, Color.Black.ToArgb().ToString(), isCsv);
        }
        public static Color GetColorByArgb(string key, string defaultValue, bool isCsv)
        {
            if (isCsv)
            {
                try
                {
                    var argb = GetString(key, defaultValue).Split(',');
                    return Color.FromArgb(int.Parse(argb[0]), int.Parse(argb[1]), int.Parse(argb[2]), int.Parse(argb[3]));
                }
                catch (Exception)
                {
                    return Color.FromArgb(int.Parse(defaultValue));
                }
            }
            else
            {
                int argb = GetInt32(key, int.Parse(defaultValue));
                return Color.FromArgb(argb);
            }
        }

        public static Color GetColorByName(string key)
        {
            return GetColorByName(key, Color.Black.ToString());
        }

        public static Color GetColorByName(string key, string defaultValue)
        {
            string colorName = GetString(key, defaultValue);
            try
            {
                return Color.FromName(colorName);
            }
            catch (Exception)
            {
                return Color.FromName(defaultValue);
            }
        }

        public static string ColorArgbToCsv(Color color)
        {
            var sb = new StringBuilder();
            sb.Append(color.A);
            sb.Append(",");
            sb.Append(color.R);
            sb.Append(",");
            sb.Append(color.G);
            sb.Append(",");
            sb.Append(color.B);
            return sb.ToString();
        }
    }
}
