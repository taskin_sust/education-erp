using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using NHibernate.Action;
using NHibernate.Mapping;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface IGoodsReceiveDetailService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(GoodsReceiveDetails goodsReceiveDetail);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<dynamic> LoadGoodReceiveBranchWise(AuthorizeBranchNameDelegate branchNameDelegate, int start, int length, List<UserMenu> userMenus, long organizationId, List<long> branchIdList,
            List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierId, int receiveType, int reportType, string dateFrom, string dateTo, List<string> programsession, bool isOnlyNa = false);
        IList<dynamic> LoadGoodReceiveProgramWise(int start, int length, List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList,
            List<long> sessionIdList, List<long> supplierIdList, int receiveType, int reportType, string dateFrom, string dateTo, List<string> programsession, bool isOnlyNa = false);

        #endregion

        #region Others Function

        long GetPrevReceiveAmountByReceiveDetailsId(long id);
        long GetPrevSumReceiveAmountByWorkOrderDetailId(long id);
        long GetLastReceivedAmountByReceiveDetailsId(long id);
        long GetPrevSumReceiveAmountByGoodsReceiveDetailsId(long id);
        long GetPrevReceivedAmountFromSelectedReceivedNo(string goodsRecNo);

        int CountGoodReceiveBranchWise(AuthorizeBranchNameDelegate branchNameDelegate, List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<int> purposeList, List<long> itemGroupIdList,
            List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierId, int receiveType, int reportType, string dateFrom, string dateTo, List<string> programsession, bool isOnlyNa = false);
        int CountGoodReceiveProgramWise(List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList,
            List<long> sessionIdList, List<long> supplierIdList, int receiveType, int reportType, string dateFrom, string dateTo, List<string> programsession, bool isOnlyNa = false);

        string[] BuildAuthProgramSession(List<UserMenu> userMenu, long orgId, long[] branchIds, long[] itemIds, string[] programIds = null);

        #endregion


        #region Helper Function

        #endregion

    }
    public class GoodsReceiveDetailsService : BaseService, IGoodsReceiveDetailService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("InventoryService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IGoodsReceiveDetailDao _goodsReceiveDetailDao;
        private readonly IBranchDao _branchDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        private readonly ProgramBranchSessionDao _programBranchSessionDao;
        public GoodsReceiveDetailsService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _goodsReceiveDetailDao = new GoodsReceiveDetailsDao { Session = session };
            _branchDao = new BranchDao() { Session = session };
            _programDao = new ProgramDao() { Session = session };
            _sessionDao = new SessionDao() { Session = session };
            _programBranchSessionDao = new ProgramBranchSessionDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(GoodsReceiveDetails goodsReceiveDetail)
        {

            return true;
        }



        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<dynamic> LoadGoodReceiveBranchWise(AuthorizeBranchNameDelegate branchNameDelegate, int start, int length, List<UserMenu> userMenus, long organizationId, List<long> branchIdList,
          List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierId, int receiveType,
          int reportType, string dateFrom, string dateTo, List<string> programsession, bool isOnlyNa = false)
        {
            try
            {
                if (organizationId == null || organizationId <= 0)
                    throw new InvalidDataException("Invalid Organization");
                if (!branchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");
                if (userMenus == null || !userMenus.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> brIds = branchIdList.ToList();
                if (brIds.Contains(SelectionType.SelelectAll))
                    brIds = null;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, programIdList,
                    brIds);
                List<long> authProgramList = AuthHelper.LoadProgramIdList(userMenus, organizationIdList,
                    authBranchIdList, programIdList);
                string branchNameStr = GetBranchNameByAuthorizedBranch(branchNameDelegate, userMenus, organizationIdList,
                    authBranchIdList);
                return _goodsReceiveDetailDao.LoadGoodReceive(start, length, authBranchIdList, purposeList,
                    itemGroupIdList, itemIdList, authProgramList,
                    sessionIdList, supplierId, receiveType, reportType, dateFrom, dateTo, branchNameStr, programsession,
                    isOnlyNa);
            }
            catch (InvalidDataException ide)
            {
                _logger.Error(ide);
                throw ide;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<dynamic> LoadGoodReceiveProgramWise(int start, int length, List<UserMenu> userMenu, long organizationId, List<long> branchIdList,
            List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList,
            int receiveType, int reportType, string dateFrom, string dateTo, List<string> programsession, bool isOnlyNa = false)
        {
            try
            {
                if (organizationId == null || organizationId <= 0)
                    throw new InvalidDataException("Invalid Organization");
                if (!branchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");
                if (userMenu == null || !userMenu.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> brIds = branchIdList.ToList();
                if (brIds.Contains(SelectionType.SelelectAll))
                    brIds = null;

                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, programIdList, brIds);
                List<long> authProgramList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, authBranchIdList, programIdList);
                string programSessionNameStr = GetProgramNameByAuthorizedProgramSession(programsession);
                return _goodsReceiveDetailDao.LoadGoodReceiveProgramWise(start, length, authBranchIdList, purposeList, itemGroupIdList, itemIdList, authProgramList,
                    sessionIdList, supplierIdList, receiveType, dateFrom, dateTo, programSessionNameStr, programsession, isOnlyNa);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public long GetPrevSumReceiveAmountByWorkOrderDetailId(long workOrderDetailid)
        {
            try
            {
                if (workOrderDetailid < 0) throw new InvalidDataException("invalid work order detail id");
                return _goodsReceiveDetailDao.GetPrevSumReceiveAmountByWorkOrderDetailId(workOrderDetailid);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw e;
            }

        }

        public long GetLastReceivedAmountByReceiveDetailsId(long id)
        {
            try
            {
                if (id < 0) throw new InvalidDataException("invalid detail id");
                return _goodsReceiveDetailDao.GetLastReceivedAmountByReceiveDetailsId(id);

            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw e;
            }

        }

        public long GetPrevSumReceiveAmountByGoodsReceiveDetailsId(long id)
        {
            try
            {
                if (id < 0) throw new InvalidDataException("invalid detail id");
                return _goodsReceiveDetailDao.GetPrevSumReceiveAmountByGoodsReceiveDetailsId(id);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public long GetPrevReceivedAmountFromSelectedReceivedNo(string goodsRecNo)
        {
            try
            {
                if (String.IsNullOrEmpty(goodsRecNo)) throw new InvalidDataException("invalid good receive no.");
                if (goodsRecNo.Length > 13 || goodsRecNo.Length < 13) throw new InvalidDataException("invalid length of goods receive no.");
                return _goodsReceiveDetailDao.GetPrevReceivedAmountFromSelectedReceivedNo(goodsRecNo);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public int CountGoodReceiveBranchWise(AuthorizeBranchNameDelegate branchNameDelegate, List<UserMenu> userMenus, long organizationId, List<long> branchIdList, List<int> purposeList, List<long> itemGroupIdList,
            List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierId, int receiveType, int reportType,
            string dateFrom, string dateTo, List<string> programsession, bool isOnlyNa = false)
        {
            try
            {
                if (organizationId == null || organizationId <= 0)
                    throw new InvalidDataException("Invalid Organization");
                if (!branchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");
                if (userMenus == null || !userMenus.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> brIds = branchIdList.ToList();
                if (brIds.Contains(SelectionType.SelelectAll))
                    brIds = null;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, programIdList, brIds);
                string branchNameStr = GetBranchNameByAuthorizedBranch(branchNameDelegate, userMenus, organizationIdList, authBranchIdList);
                return _goodsReceiveDetailDao.CountGoodReceive(authBranchIdList, purposeList, itemGroupIdList, itemIdList, programIdList,
                    sessionIdList, supplierId, receiveType, reportType, dateFrom, dateTo, branchNameStr, programsession, isOnlyNa);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public int CountGoodReceiveProgramWise(List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<int> purposeList,
            List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, int receiveType,
            int reportType, string dateFrom, string dateTo, List<string> programsession, bool isOnlyNa = false)
        {
            try
            {
                if (organizationId == null || organizationId <= 0)
                    throw new InvalidDataException("Invalid Organization");
                if (!branchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");
                if (userMenu == null || !userMenu.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> brIds = branchIdList.ToList();
                if (brIds.Contains(SelectionType.SelelectAll))
                    brIds = null;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, programIdList, brIds);
                string programSessionNameStr = GetProgramNameByAuthorizedProgramSession(programsession);
                return _goodsReceiveDetailDao.CountGoodReceiveProgramWise(authBranchIdList, purposeList, itemGroupIdList, itemIdList, programIdList,
                    sessionIdList, supplierIdList, receiveType, reportType, dateFrom, dateTo, programSessionNameStr, programsession, isOnlyNa);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public string[] BuildAuthProgramSession(List<UserMenu> authMenu, long organizationId, long[] branchIds, long[] itemIds, string[] programSessionIds = null)
        {
            try
            {
                List<long> authProIdList = new List<long>();
                List<long> brIds = branchIds.ToList();
                List<long> itemList = itemIds.ToList();
                if (itemList.Count == 1 && itemList.Contains(0)) itemList = null;
                if (brIds.Count == 1 && brIds.Contains(0)) brIds = null;
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authMenu, (organizationId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(organizationId) : null);
                List<long> authBr = AuthHelper.LoadBranchIdList(authMenu, authOrgList, null, brIds);
                if (programSessionIds == null) authProIdList = AuthHelper.LoadProgramIdList(authMenu, authOrgList, authBr);
                else
                {
                    foreach (var programsession in programSessionIds)
                    {
                        string[] programSessionArray = programsession.Split(new string[] { "::" }, StringSplitOptions.None);
                        Program program = _programDao.LoadById(Convert.ToInt64(programSessionArray[0]));
                        if (program != null)
                            authProIdList.Add(program.Id);
                    }
                    authProIdList = AuthHelper.LoadProgramIdList(authMenu, authOrgList, authBr, authProIdList);
                }

                if (!authProIdList.Any())
                    throw new InvalidDataException("Program can't empty");
                var authbranchIdlist = new List<long>();
                foreach (var branchId in authBr)
                {
                    if (branchId > 0)
                    {
                        var branch = _branchDao.LoadById((long)branchId);
                        if (branch.IsCorporate)
                        {
                            authbranchIdlist = null;
                            break;
                        }
                        else
                            authbranchIdlist.Add((long)branchId);
                    }
                }

                IList<ProgramSessionDto> proSesDtos = _programBranchSessionDao.LoadProgramSession(authOrgList, authbranchIdlist, authProIdList, itemList);
                var ary = new string[proSesDtos.Count];
                int index = 0;
                foreach (var programSessionDto in proSesDtos)
                {
                    ary[index] = programSessionDto.ProgramAndSessionId;
                    index++;
                }
                return ary;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        private string GetBranchNameByAuthorizedBranch(AuthorizeBranchNameDelegate branchNameDelegate, List<UserMenu> userMenus, List<long> organizationIdList, List<long> authBranchIdList)
        {
            IList<string> branchNameList = branchNameDelegate(userMenus, organizationIdList, authBranchIdList);
            //IList<string> branchNameList = _branchDao.LoadBranch(null, null, null, authBranchIdList).Select(x => x.Name).ToList();
            string branchNameStr = "";
            for (int i = 0; i < branchNameList.Count; i++)
            {
                branchNameStr += "[" + branchNameList[i].Trim() + "]";
                if (i < branchNameList.Count - 1)
                    branchNameStr += ",";
            }
            return branchNameStr;
        }


        private string GetProgramNameByAuthorizedProgramSession(List<string> authProgramSessionList)
        {
            string programSession = "";
            int count = 0;
            if (authProgramSessionList.Contains("-101"))
            {
                programSession = "[N/A],";
                authProgramSessionList = authProgramSessionList.Skip(1).Take(authProgramSessionList.Count - 1).ToList();
                if (authProgramSessionList.Count == 0) programSession = "[N/A]";
            }
            foreach (var programsession in authProgramSessionList)
            {
                string[] programSessionArray = programsession.Split(new string[] { "::" }, StringSplitOptions.None);
                Program program = _programDao.LoadById(Convert.ToInt64(programSessionArray[0]));
                Session session = _sessionDao.LoadById(Convert.ToInt64(programSessionArray[1]));

                string programName = program.ShortName;
                string sessionName = session.Name;
                string programSessionName = "[" + programName + " & " + sessionName + "]";
                if (count < authProgramSessionList.Distinct().ToList().Count - 1)
                    programSessionName += ",";
                programSession += programSessionName;
                count++;
            }
            return programSession;

        }

        public long GetPrevReceiveAmountByReceiveDetailsId(long id)
        {
            try
            {
                if (id < 0) throw new InvalidDataException("invalid detail id");
                return _goodsReceiveDetailDao.GetPrevReceiveAmountByReceiveDetailsId(id);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }

        }

        #endregion

        #region Helper function

        #endregion
    }
}

