﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using UdvashERP.Dao.Sms;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Sms
{
    public interface ISmsHistoryServices : IBaseService
    {
        bool SendPendingSms(BusinessModel.Dto.WsSmsLocal[] wsSmsesList, IList<SmsSender> pendinSmsSenders);
    }
    public class SmsHistoryServices : BaseService, ISmsHistoryServices
    {
        private readonly ISmsHistoryDao _smsHistoryDao;

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        private readonly IStudentProgramService _studentProgramService;
        private readonly ISmsSenderDao _smsSenderDao;
        private readonly ISmsSenderService _smsSenderService;
        private readonly IOrganizationService _organizationService;
        public SmsHistoryServices()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _smsSenderDao = new SmsSenderDao() { Session = session };
            _smsHistoryDao = new SmsHistoryDao() { Session = session };
            _studentProgramService = new StudentProgramService(session);
            _organizationService = new OrganizationService(session);
            _smsSenderService = new SmsSenderService();
        }

        public bool SendPendingSms(WsSmsLocal[] wsSmsesList, IList<SmsSender> pendinSmsSenders)
        {
            try
            {
                foreach (WsSmsLocal singleSms in wsSmsesList) //sending sms 
                {
                    //if (singleSms == null) continue;
                    if (singleSms != null)
                    {
                        var smsHistory = new SmsHistory();
                        StudentProgram stdProgram =
                            _studentProgramService.GetStudentProgram(singleSms.PrnNo);
                        if (stdProgram != null)
                        {
                            smsHistory.Student = stdProgram.Student;
                            smsHistory.Program = stdProgram.Program;
                            smsHistory.Organization = stdProgram.Program.Organization;
                            smsHistory.Mask = stdProgram.Program.Organization.SmsMasks[0].Name;
                            smsHistory.ReceiverNumber = singleSms.MobileNumber;
                            smsHistory.Sms = singleSms.SmsText;
                            smsHistory.Batch = stdProgram.Batch;
                            smsHistory.Type = 11;//can't find SMS type
                            smsHistory.Priority = 1;
                            smsHistory.Status = SmsHistory.EntityStatus.Pending;
                            _smsHistoryDao.Save(smsHistory);
                        }
                        else
                        {
                            smsHistory.ReceiverNumber = singleSms.MobileNumber;
                            smsHistory.Sms = singleSms.SmsText;
                            smsHistory.Type = 11;
                            smsHistory.Priority = 1;
                            smsHistory.Organization = _organizationService.LoadById(1);
                            smsHistory.Mask = "UMS";
                            smsHistory.Status = SmsHistory.EntityStatus.Pending;
                            _smsHistoryDao.Save(smsHistory);
                        }
                    }
                }
                _smsSenderService.Update(pendinSmsSenders);
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }
    }
}
