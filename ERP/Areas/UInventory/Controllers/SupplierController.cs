﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services.Description;
using log4net;
using Newtonsoft.Json;
using NHibernate.Dialect.Function;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.Areas.UInventory.Models;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("UInventory")]
    public class SupplierController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ISupplierService _supplierService;
        private readonly IBankService _bankService;
        private readonly IBankBranchService _bankBranchService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IItemGroupService _itemGroupService;
        private readonly IItemService _itemService;
        private readonly ISupplierItemService _supplierItemService;
        private readonly ISupplierBankDetailService _supplierBankBranchDetails;
        
        public SupplierController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _supplierService = new SupplierService(session);
                _bankService = new BankService(session);
                _bankBranchService = new BankBranchService(session);
                _organizationService = new OrganizationService(session);
                _itemGroupService = new ItemGroupService(session);
                _itemService = new ItemService(session);
                _supplierItemService = new SupplierItemService(session);
                _supplierBankBranchDetails = new SupplierBankDetailsService(session);

                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Details/Manage Page

        public ActionResult Manage(string message = "", string type = "")
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(message))
                {
                    if (type == "e")
                    {
                        ViewBag.ErrorMessage = message;
                    }
                    else if (type == "s")
                    {
                        ViewBag.SuccessMessage = message;
                    }
                    else if (type == "i")
                    {
                        ViewBag.InfoMessage = message;
                    }
                }
                ViewBag.StatusText = _commonHelper.GetStatus();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View();
            }
        }

        public ActionResult Details(long id)
        {
            try
            {
                if (id <= 0)
                {
                    return HttpNotFound();
                }
                var supplierObj = _supplierService.LoadById(id);
                if (supplierObj == null)
                    return RedirectToAction("Manage", new { message = "Supplier Not Found! Please try for another!", type = "i" });
                var supplierViewModel = new SupplierViewModel()
                {
                    Id = supplierObj.Id,
                    SupplierName = supplierObj.Name,
                    OwnerName = supplierObj.OwnerName,
                    RegistrationAddress = supplierObj.RegistrationAddress,
                    ContactPerson = supplierObj.ContactPerson,
                    MobileNo = supplierObj.MobileNo,
                    TelephoneNo = supplierObj.TelephoneNo,
                    Email = supplierObj.Email,
                    WebsiteAdress = supplierObj.WebsiteAdress,
                    TlnInc = supplierObj.TlnInc,
                    VatRegistrationNo = supplierObj.VatRegistrationNo,
                    Tin = supplierObj.Tin,
                    VatCircle = supplierObj.VatCircle,
                    VatDivision = supplierObj.VatDivision,
                    TaxCircle = supplierObj.TaxCircle,
                    TaxZone = supplierObj.TaxZone,
                    BankInfoDetails = (from bankDetail in supplierObj.SupplierBankDetails
                                       where bankDetail.Status != SupplierBankDetails.EntityStatus.Delete
                                       select new BankInfoView()
                                       {
                                           BankName = bankDetail.BankBranch.Bank.Name,
                                           BranchName = bankDetail.BankBranch.Name,
                                           AccountTitle = bankDetail.AccountTitle,
                                           AccountNo = bankDetail.AccountNo
                                       }).ToList()
                };
                return View(supplierViewModel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return RedirectToAction("Manage", new { message = WebHelper.CommonErrorMessage, type = "e" });
            }
        }

        #endregion

        #region Operational Function

        #region Create Operation

        public ActionResult Create(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                ViewBag.SuccessMessage = message;
            }
            var bankList = _bankService.LoadBankList();
            ViewBag.BankList = new SelectList(bankList.OrderBy(x=>x.Rank), "Id", "Name").ToList<SelectListItem>();
            return View(new SupplierForm());
        }

        [HttpPost]
        public ActionResult Create(SupplierForm model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewBagForSupplierCreate(model);
                    return View(model);
                }
                var newEntity = new Supplier()
                {
                    ContactPerson = model.ContactPerson ?? "",
                    Email = model.Email ?? "",
                    MobileNo = model.MobileNo ?? "",
                    Name = model.SupplierName ?? "",
                    OwnerName = model.OwnerName ?? "",
                    RegistrationAddress = model.RegistrationAddress ?? "",
                    WebsiteAdress = model.WebsiteAdress ?? "",
                    TaxCircle = model.TaxCircle ?? "",
                    TaxZone = model.TaxZone ?? "",
                    TelephoneNo = model.TelephoneNo ?? "",
                    Tin = model.Tin ?? "",
                    TlnInc = model.TlnInc ?? "",
                    VatCircle = model.VatCircle ?? "",
                    VatDivision = model.VatDivision ?? "",
                    VatRegistrationNo = model.VatRegistrationNo ?? ""
                };

                int skipedBankDetailsRow = 0;
                int duplicateBankDetailsRow = 0;
                var bBList = new List<SupplierBankDetails>();
                foreach (var bankDetail in model.BankDetails)
                {
                    if (bankDetail.BankBranch == null || string.IsNullOrWhiteSpace(bankDetail.AcNumber) || string.IsNullOrWhiteSpace(bankDetail.AcTitle))
                    {
                        skipedBankDetailsRow += 1;
                        continue;
                    }
                    var isExist = _supplierService.IsBankAccountExistsByBankBranch(bankDetail.Id, bankDetail.AcNumber, bankDetail.BankBranch.Value);
                    if (!isExist)
                    {
                        var bankBranch = _bankBranchService.GetBankBranch(bankDetail.BankBranch.Value);
                        bBList.Add(new SupplierBankDetails()
                        {
                            Id = bankDetail.Id,
                            BankBranch = bankBranch,
                            AccountNo = bankDetail.AcNumber,
                            AccountTitle = bankDetail.AcTitle
                        });
                    }
                    else
                    {
                        duplicateBankDetailsRow += 1;
                    }
                }
                if (bBList.Count > 0)
                    newEntity.SupplierBankDetails = bBList;

                bool isSuccess = _supplierService.SaveOrUpdate(newEntity);
                if (isSuccess)
                {
                    string message = "Supplier successfully added.";
                    if (skipedBankDetailsRow > 0)
                    {
                        message += " " + skipedBankDetailsRow + " Bank Details Row was skipped.";
                    }
                    if (duplicateBankDetailsRow > 0)
                    {
                        message += " " + duplicateBankDetailsRow + " Bank Details Row had duplicate account.";
                    }
                    return RedirectToAction("Create", new { message = message });
                }
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            ViewBagForSupplierCreate(model);
            return View(model);
        }

        #endregion

        #region Update

        public ActionResult Update(long id)
        {
            try
            {
                var supplierObj = _supplierService.LoadById(id);

                var bBbList = (from bankDetail in supplierObj.SupplierBankDetails
                               where bankDetail.Status != SupplierBankDetails.EntityStatus.Delete
                               select new BankDetail()
                               {
                                   Id = bankDetail.Id,
                                   Bank = bankDetail.BankBranch.Bank.Id,
                                   BankBranch = bankDetail.BankBranch.Id,
                                   AcTitle = bankDetail.AccountTitle,
                                   AcNumber = bankDetail.AccountNo
                               }).ToList();
                var supplierCreateViewModel = new SupplierForm()
                {
                    Id = supplierObj.Id,
                    SupplierName = supplierObj.Name,
                    OwnerName = supplierObj.OwnerName,
                    RegistrationAddress = supplierObj.RegistrationAddress,
                    ContactPerson = supplierObj.ContactPerson,
                    MobileNo = _commonHelper.ExtractCountryCodeFromMobileNumber(supplierObj.MobileNo),
                    TelephoneNo = supplierObj.TelephoneNo,
                    Email = supplierObj.Email,
                    WebsiteAdress = supplierObj.WebsiteAdress,
                    TlnInc = supplierObj.TlnInc,
                    VatRegistrationNo = supplierObj.VatRegistrationNo,
                    Tin = supplierObj.Tin,
                    VatCircle = supplierObj.VatCircle,
                    VatDivision = supplierObj.VatDivision,
                    TaxCircle = supplierObj.TaxCircle,
                    TaxZone = supplierObj.TaxZone,
                    Status = supplierObj.Status,
                    BankDetails = bBbList.Count > 0 ? bBbList : null
                };
                ViewBagForSupplierUpdate(bBbList);
                return View(supplierCreateViewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("Manage", new { message = WebHelper.CommonErrorMessage, type = "e" });
            }
        }

        [HttpPost]
        public ActionResult Update(long id, SupplierForm newObj)
        {
            var bBbList = new List<BankDetail>();
            if (newObj.BankDetails != null && newObj.BankDetails.Count > 0)
            {
                bBbList = (from bankDetail in newObj.BankDetails
                           where !string.IsNullOrWhiteSpace(bankDetail.AcNumber)
                                 && !string.IsNullOrWhiteSpace(bankDetail.AcTitle)
                                 && bankDetail.Bank > 0
                                 && bankDetail.BankBranch > 0
                           select new BankDetail()
                           {
                               Id = bankDetail.Id,
                               Bank = bankDetail.Bank,
                               BankBranch = bankDetail.BankBranch,
                               AcTitle = bankDetail.AcTitle,
                               AcNumber = bankDetail.AcNumber
                           }).ToList();
            }
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.ErrorMessage = "Form not valid! Please try again!";
                    ViewBagForSupplierUpdate(bBbList);
                    return View(newObj);
                }

                Supplier oldSupplier = _supplierService.LoadById(id);
                if (oldSupplier == null)
                {
                    return HttpNotFound();
                }

                
                oldSupplier.ContactPerson = newObj.ContactPerson;
                if (newObj.MobileNo != null && !newObj.MobileNo.Equals(""))
                {
                    oldSupplier.MobileNo = _commonHelper.AddCountryCodeWithMobileNumber(newObj.MobileNo);
                }

                oldSupplier.Email = newObj.Email;
                oldSupplier.Name = newObj.SupplierName;
                oldSupplier.OwnerName = newObj.OwnerName;
                oldSupplier.RegistrationAddress = newObj.RegistrationAddress;
                oldSupplier.WebsiteAdress = newObj.WebsiteAdress;
                oldSupplier.TaxCircle = newObj.TaxCircle;
                oldSupplier.TaxZone = newObj.TaxZone;
                oldSupplier.TelephoneNo = newObj.TelephoneNo;
                oldSupplier.Tin = newObj.Tin;
                oldSupplier.TlnInc = newObj.TlnInc;
                oldSupplier.VatCircle = newObj.VatCircle;
                oldSupplier.VatDivision = newObj.VatDivision;
                oldSupplier.VatRegistrationNo = newObj.VatRegistrationNo;
                oldSupplier.Status = newObj.Status;

                int skipedBankDetailsRow = 0;
                int duplicateBankDetailsRow = 0;

                var bbList = new List<SupplierBankDetails>();
                if (newObj.BankDetails != null)
                {
                    foreach (var bankDetail in newObj.BankDetails)
                    {
                        if (bankDetail.BankBranch == null || string.IsNullOrWhiteSpace(bankDetail.AcNumber) || string.IsNullOrWhiteSpace(bankDetail.AcTitle))
                        {
                            skipedBankDetailsRow += 1;
                            continue;
                        }
                        var isExist = _supplierService.IsBankAccountExistsByBankBranch(bankDetail.Id, bankDetail.AcNumber, bankDetail.BankBranch.Value);
                        if (!isExist)
                        {
                            var bankBranch = _bankBranchService.GetBankBranch(bankDetail.BankBranch.Value);

                            bbList.Add(new SupplierBankDetails()
                            {
                                Id = bankDetail.Id,
                                BankBranch = bankBranch,
                                AccountNo = bankDetail.AcNumber,
                                AccountTitle = bankDetail.AcTitle
                            });
                        }
                        else
                        {
                            duplicateBankDetailsRow += 1;
                        }
                    }
                }
                oldSupplier.SupplierBankDetails.Clear();
                foreach (var sd in bbList)
                {
                    oldSupplier.SupplierBankDetails.Add(sd);
                }

                bool isUpdated = _supplierService.SaveOrUpdate(oldSupplier);
                if (isUpdated)
                {
                    string message = "Supplier information successfully updated.";
                    if (skipedBankDetailsRow > 0)
                    {
                        message += " " + skipedBankDetailsRow + " Bank Details Row was skipped.";
                    }
                    if (duplicateBankDetailsRow > 0)
                    {
                        message += " " + duplicateBankDetailsRow + " Bank Details Row had duplicate account.";
                    }
                    return RedirectToAction("Manage", new { message = message, type = "s" });
                }
                else
                {
                    ViewBag.ErrorMessage = "Problem occurred, during Supplier information update.";
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                ViewBagForSupplierUpdate(bBbList);
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                ViewBagForSupplierUpdate(bBbList);
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                ViewBagForSupplierUpdate(bBbList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                ViewBagForSupplierUpdate(bBbList);
            }
            return View(newObj);
        }

        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                Supplier deleteSupplier = _supplierService.LoadById(id);
                _supplierService.Delete(deleteSupplier);
                return Json(new { isSuccess = true, Text = "Supplier Delete Successful!" });
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new { isSuccess = false, Text = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { isSuccess = false, Text = "Supplier Delete Failed!" });
            }
        }
        #endregion

        #endregion

        #region Ajax functions

        [HttpGet]
        public ActionResult AddRowBankInfo(int index)
        {
            try
            {
                var bankList = _bankService.LoadBankList();
                ViewBag.BankList = new SelectList(bankList, "Id", "Name").ToList<SelectListItem>();
                ViewBag.Index = index + 1;
                return PartialView("_partial/_AddRowBankInfo");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return RedirectToAction("Create");
            }
        }

        public JsonResult BranchListByBank(string bankId)
        {
            long id = Convert.ToInt64(bankId);
            var ids = new long[] { id };
            var branchList = _bankBranchService.LoadBankBranchList(ids);
            var list = new SelectList(branchList, "Id", "Name");

            return Json(new { returnList = list, IsSuccess = true });
        }

        public JsonResult RemoveBankDetails(long id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    SupplierBankDetails deleteSupplier = _supplierBankBranchDetails.LoadById(id);
                    _supplierBankBranchDetails.Delete(deleteSupplier);
                    return Json(new { isSuccess = true, Text = "Supplier Bank Delete Successful!" });
                }
                catch (NullObjectException ex)
                {
                    _logger.Error(ex);
                    return Json(new { isSuccess = false, Text = ex.Message });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new { isSuccess = false, Text = "Supplier Bank Delete Failed!" });
                }
            }
            return Json(false);
        }

        #region Remote Unique Check Functions

        public JsonResult DoesSupplierNameExist(string supplierName, long? id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool d = _supplierService.IsSupplierNameExist(supplierName.Trim(), id);
                    return Json(d);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            return Json(false);
        }

        public JsonResult DoesTlnIncExist(string tlnInc, long? id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool d = _supplierService.IsTlnIncExist(tlnInc.Trim(), id);
                    return Json(d);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            return Json(false);
        }

        public JsonResult DoesTinExist(string tin, long? id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool d = _supplierService.IsTinExist(tin.Trim(), id);
                    return Json(d);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            return Json(false);
        }

        public JsonResult DoesVatRegistrationNoExist(string vatRegistrationNo, long? id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool d = _supplierService.IsVatRegistrationNoExist(vatRegistrationNo.Trim(), id);
                    return Json(d);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            return Json(false);
        }

        public JsonResult DoesBankAccountExistForBranch([ModelBinder(typeof(JsonBankDetailModelBinder))] BankDetail bankDetail)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (bankDetail.BankBranch == null)
                    {
                        bankDetail.BankBranch = 0;
                    }
                    if (bankDetail.BankBranch > 0)
                    {
                        bool isExist = _supplierService.IsBankAccountExistsByBankBranch(bankDetail.Id,
                            bankDetail.AcNumber,
                            (long) bankDetail.BankBranch);
                        bool result = !isExist;
                        return !result ? Json("Account Number already exists.") : Json(true);
                    }
                    return Json("Bank branch not found.");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            return Json(true);
        }

        #endregion

        #region Render Data Table

        [HttpPost]
        public ActionResult SupplierList(int draw, int start, int length, string name, string status)
        {
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "MobileNo";
                            break;
                        case "2":
                            orderBy = "TelephoneNo";
                            break;
                        case "3":
                            orderBy = "Email";
                            break;
                        case "4":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                #endregion
                var supplierList = _supplierService.LoadSupplierList(start, length, orderBy, orderDir.ToUpper(), name,
                    status);

                int recordsTotal = _supplierService.GetSupplierCount(name, status);
                long recordsFiltered = recordsTotal;

                foreach (var supplier in supplierList)
                {
                    var str = new List<string>
                    {
                        supplier.Name,
                        supplier.MobileNo,
                        supplier.TelephoneNo,
                        supplier.Email
                    };
                    str.Add(StatusTypeText.GetStatusText(supplier.Status));
                    str.Add(LinkGenerator.GetGeneratedLink("Details", "Update", "Delete", "Supplier", supplier.Id, supplier.Name));
                    getData.Add(str);
                }
                return Json(new
                {
                    draw,
                    recordsTotal,
                    recordsFiltered,
                    start,
                    length,
                    data = getData,
                    isSuccess = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start,
                length,
                data = getData,
                isSuccess = false
            });
        }

        #endregion

        #endregion

        #region Helper functions

        private void ViewBagForSupplierCreate(SupplierForm model)
        {
            var bankList = _bankService.LoadBankList();
            ViewBag.BankList = new SelectList(bankList, "Id", "Name").ToList<SelectListItem>();

            IList<SelectList> selectListArray = new List<SelectList>();
            foreach (var bd in model.BankDetails)
            {
                long id = 0;
                if (bd.Bank != null)
                {
                    id = bd.Bank.Value;
                }
                var ids = new long[] { id };
                var branchList = _bankBranchService.LoadBankBranchList(ids);
                var list = new SelectList(branchList, "Id", "Name");
                selectListArray.Add(list);
            }
            ViewBag.BankBranch = selectListArray;
        }

        private void ViewBagForSupplierUpdate(IEnumerable<BankDetail> bBbList)
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            var bankList = _bankService.LoadBankList();
            ViewBag.BankList = new SelectList(bankList.OrderBy(x => x.Rank), "Id", "Name").ToList<SelectListItem>();

            IList<SelectList> selectListArray = new List<SelectList>();
            if (bBbList != null)
            {
                foreach (var bd in bBbList)
                {
                    long id = 0;
                    if (bd.Bank != null)
                    {
                        id = bd.Bank.Value;
                    }
                    var ids = new long[] { id };
                    var branchList = _bankBranchService.LoadBankBranchList(ids);
                    var list = new SelectList(branchList, "Id", "Name");
                    selectListArray.Add(list);
                }
            }
            ViewBag.BankBranch = selectListArray;
        }

        #endregion

        #region Assign Supplier

        public ActionResult AssignSupplier()
        {
            try
            {
                var usermenus = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.orgList = new SelectList(_organizationService.LoadAuthorizedOrganization(usermenus), "Id", "ShortName");
                ViewBag.itemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
                ViewBag.supplierList = _supplierService.LoadSupplierList(0, int.MaxValue, "", "", "", Supplier.EntityStatus.Active.ToString());
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
            }
            return View();
        }

        [HttpPost]
        public JsonResult AssignSupplier(string[] itemIds, string[] supplierIds)
        {
            try
            {
                bool isSuccess = _supplierItemService.SaveOrUpdate(itemIds, supplierIds);
                if (isSuccess)
                    return Json(new Response(true, "Assigned successfully"));
                return Json(new Response(false, "Failed to assign"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
        }

        [HttpPost]
        public ActionResult GetItemSupplier(string[] itemIds)
        {
            try
            {
                IList<SupplierModel> supplierModels = new List<SupplierModel>();
                IList<Supplier> allSuppliers = _supplierService.LoadSupplierList(0, int.MaxValue, "", "", "", Supplier.EntityStatus.Active.ToString());
                if (itemIds == null)
                    return PartialView("_partial/_supplierItems", CastToModel(allSuppliers));

                IList<Supplier> selectedSuppliers = _supplierService.LoadOnlyCommonSuppliersByItemIds(itemIds);
                foreach (var supplier in allSuppliers)
                {
                    var supplierModel = new SupplierModel();
                    foreach (var selectedSupplier in selectedSuppliers)
                    {
                        if (selectedSupplier.Id == supplier.Id)
                        {
                            supplierModel.IsSelected = true;
                        }
                    }
                    supplierModel.Id = supplier.Id;
                    supplierModel.Name = supplier.Name;
                    supplierModels.Add(supplierModel);
                }
                return PartialView("_partial/_supplierItems", supplierModels);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        private object CastToModel(IList<Supplier> allSuppliers)
        {
            try
            {
                IList<SupplierModel> supplierModels = new List<SupplierModel>();
                foreach (var supplier in allSuppliers)
                {
                    var supplierModel = new SupplierModel();
                    supplierModel.Id = supplier.Id;
                    supplierModel.Name = supplier.Name;
                    supplierModels.Add(supplierModel);
                }
                return supplierModels;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion
    }
}
