using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class EmploymentHistoryLogMap : ClassMap<EmploymentHistoryLog> {
        
        public EmploymentHistoryLogMap() {
			Table("HR_EmploymentHistoryLog");
			LazyLoad();
            //References(x => x.Campus).Column("CampusId");//old
            //References(x => x.Designation).Cascade.All().Column("DesignationId");//old
            //References(x => x.Department).Cascade.All().Column("DepartmentId");//old
            //References(x => x.TeamMember).Column("TeamMemberId");//old
            //References(x => x.EmploymentHistory).Column("EmploymentHistoryId");//old
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
            Map(x => x.CampusId);
            Map(x => x.DesignationId);
            Map(x => x.DepartmentId);
            Map(x => x.TeamMemberId);
            Map(x => x.EmploymentHistoryId);
            Map(x => x.EmploymentStatus).Column("EmploymentStatus");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
            Map(x => x.LeaveDate).Column("LeaveDate");
        }
    }
}
