﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Sms;
using NHibernate;

namespace UdvashERP.Dao.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public interface ISmsSettingsDao : IBaseDao<SmsSettings, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        SmsSettings LoadSmsSettingsBySmsTypePrgram(long smsTypeId,long organizationId, long programId);
        #endregion

        #region List Loading Function
        IList<SmsSettings> SmsSettingsList(List<string> smsTypes);
        IList<SmsSettings> SmsSettingsList(int start, int length, string orderBy, string orderDir, string programId, string smsTypeId, string status, long? organizationId);
        //IList<SmsSettings> SmsSettingsList(int start, int length, string orderBy, string orderDir, List<long> programIdList, string smsTypeId, string status, List<long> organizationIdList);
        #endregion

        #region Others Function
        int GetMenuMaximumRank();
        bool DuplicationCheck(Organization organization, Program programId, long smsType);
        int SmsSettingsRowCount(string programId, string smsTypeId, string status, long? organizationId);
        //int SmsSettingsRowCount(List<long> programIdList, string smsTypeId, string status, List<long> organizationIdList);

        #endregion

        IList<ScheduleSmsDto> LoadBirthdaySmsStudentList(long organizationId, List<long> programList, SmsSettings smsSettings, bool isIn);
        IList<ScheduleSmsDto> LoadDueSmsStudentList(long organizationId, List<long> programList, SmsSettings smsSettings, bool isIn);
    }
    public class SmsSettingsDao : BaseDao<SmsSettings, long>, ISmsSettingsDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public SmsSettings LoadSmsSettingsBySmsTypePrgram(long smsTypeId,long organizationId, long programId)
        {
            //Session.Clear();
            //var query = Session.QueryOver<SmsSettings>();
            //query.Where(s => s.Status == SmsSettings.EntityStatus.Active);
            //query.Where(p => p.Program.Id == programId);
            //query.Where(p => p.SmsType.Id == smsTypeId);
            //var data = query.List<SmsSettings>();

            //if ()
            //{

            //}

            //SmsSettings ss = new SmsSettings();
            //if (data.Count > 0)
            //{
            //    ss = data.Where(x=>x.Program.Id)
            //}

            var data = Session.QueryOver<SmsSettings>()
                .Where(
                    x =>
                        x.Status == SmsSettings.EntityStatus.Active && x.Organization.Id == organizationId && x.Program.Id == programId && x.SmsType.Id == smsTypeId).Take(1).SingleOrDefault();

            if (data != null)
            {
                return data;
            }
            else
            {
                var dataAll = Session.QueryOver<SmsSettings>()

               .Where(
                   x =>
                       x.Status == SmsSettings.EntityStatus.Active && x.Organization.Id == organizationId && x.Program == null && x.SmsType.Id == smsTypeId).Take(1).SingleOrDefault();
                if (dataAll != null)
                {
                    return dataAll;
                }
                else
                {
                    return null;
                }
            }

        }

        

        #endregion

        #region List Loading Function

        public IList<SmsSettings> SmsSettingsList(List<string> smsTypes)
        {
            ICriteria criteria = Session.CreateCriteria<SmsSettings>()
                .Add(Restrictions.Eq("Status", SmsSettings.EntityStatus.Active));
            criteria.CreateAlias("SmsType", "st");
            criteria.Add(Restrictions.In("st.Name", smsTypes));
            var data = criteria.List<SmsSettings>();
            return data;
        }

        public IList<SmsSettings> SmsSettingsList(int start, int length, string orderBy, string orderDir, string programId,
                  string smsTypeId, string status, long? organizationId)
        //public IList<SmsSettings> SmsSettingsList(int start, int length, string orderBy, string orderDir, List<long> programIdList,
        //          string smsTypeId, string status, List<long> organizationIdList)
        {
            var query = Session.QueryOver<SmsSettings>();
            query.Where(s => s.Status != SmsSettings.EntityStatus.Delete);
            if (!String.IsNullOrEmpty(programId) && programId != "All")
            {
                query.Where(p => p.Program.Id == Convert.ToInt64(programId));
            }
            if (organizationId != null)
            {
                query.Where(o => o.Organization.Id == organizationId);
            }
            //if (programIdList != null)
            //{
            //    query.Where(p => p.Program.Id.IsIn(programIdList));
            //}
            //if (organizationIdList != null)
            //{
            //    query.Where(o => o.Organization.Id.IsIn(organizationIdList));
            //}

            if (!String.IsNullOrEmpty(smsTypeId))
            {
                query.Where(sms => sms.SmsType.Id == Convert.ToInt64(smsTypeId));
            }
            if (!String.IsNullOrEmpty(status))
            {
                query.Where(ss => ss.Status == Convert.ToInt64(status));
            }

            query.Skip(start).Take(length);

            var data = query.List<SmsSettings>();
            return data;
        }

        #endregion

        #region Others Function
        public int GetMenuMaximumRank()
        {
            try
            {
                var max = 0;
                max = Session.QueryOver<SmsSettings>().Where(x => x.Status != SmsSettings.EntityStatus.Delete).Select(Projections.Max<SmsSettings>(x => x.Rank)).SingleOrDefault<int>();
                return max;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool DuplicationCheck(Organization organization, Program program, long smsType)
        {
            ICriteria criteria = Session.CreateCriteria<SmsSettings>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", SmsSettings.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("SmsType.Id", smsType));

            if (organization != null)
                criteria.Add(Restrictions.Eq("Organization", organization));
            else
                criteria.Add(Restrictions.IsNull("Organization"));
            if (program != null)
                criteria.Add(Restrictions.Eq("Program", program));
            else
                criteria.Add(Restrictions.IsNull("Program"));

            criteria.SetProjection(Projections.RowCount());

            if (Convert.ToInt32(criteria.UniqueResult()) > 0)
            {
                return true;
            }
            return false;
        }

        public int SmsSettingsRowCount(string programId, string smsTypeId, string status, long? organizationId)
        //public int SmsSettingsRowCount(List<long> programIdList, string smsTypeId, string status, List<long> organizationIdList)
        {
            var query = Session.QueryOver<SmsSettings>();
            query.Where(s => s.Status != SmsSettings.EntityStatus.Delete);
            if (!String.IsNullOrEmpty(programId) && programId != "All")
            {
                query.Where(p => p.Program.Id == Convert.ToInt64(programId));
            }
            if (organizationId != null)
            {
                query.Where(o => o.Organization.Id == organizationId);
            }
            //if (programIdList != null)
            //{
            //    query.Where(p => p.Program.Id.IsIn(programIdList));
            //}
            //if (organizationIdList != null)
            //{
            //    query.Where(o => o.Organization.Id.IsIn(organizationIdList));
            //}

            if (!String.IsNullOrEmpty(smsTypeId))
            {
                query.Where(sms => sms.SmsType.Id == Convert.ToInt64(smsTypeId));
            }
            if (!String.IsNullOrEmpty(status))
            {
                query.Where(ss => ss.Status == Convert.ToInt64(status));
            }
            var count = 0;
            count = query.Select(Projections.RowCount()).FutureValue<int>().Value;
            return count;
        }

        public IList<ScheduleSmsDto> LoadBirthdaySmsStudentList(long organizationId, List<long> programList, SmsSettings smsSettings, bool isIn)
        {

           // string smsHistoryjoinCondition = "AND sh.SmsSettingsId = " + smsSettings.Id+ " AND sh.ProgramId = sp.ProgramId AND (sh.ErrorCode='1900' OR sh.Status="+SmsHistory.EntityStatus.Pending+") ";
            string programFilter = "";
            string dayMonthFilter = "";
            string smsHistoryFilter = "";
            int dayBefore = 0;
            int dayAfter = 0;
            if (smsSettings.DayBefore != null)
            {
                dayBefore = (int)smsSettings.DayBefore;
            }
            if (smsSettings.DayAfter != null)
            {
                dayAfter = (int)smsSettings.DayAfter;
            }

            DateTime today = DateTime.Today;
            if (smsSettings.IsRepeat)
            {
                int repeatD = 1;
                if (smsSettings.RepeatDuration != null && smsSettings.RepeatDuration > 0)
                    repeatD = (int)smsSettings.RepeatDuration;
                if (smsSettings.DayBefore != null && smsSettings.DayBefore>0)
                {
                   
                    dayMonthFilter = " AND (";
                    for (int i = 0; i <= smsSettings.DayBefore; i += repeatD)
                    {
                        dayMonthFilter +=" AND DATEPART(mm,s.Dob)=DATEPART(mm,DATEADD(day," + i + ",@today)) AND DATEPART(dd,s.Dob)=DATEPART(dd,DATEADD(day," + i + ",@today))";
                    }
                    dayMonthFilter += " ) ";
                    //criteria.Add(disjunction);
                }
                else
                {
                    dayMonthFilter =
                        "AND DATEPART(mm,s.Dob)=DATEPART(mm,@today) AND DATEPART(dd,s.Dob)=DATEPART(dd,@today)";
                }

                smsHistoryFilter = " AND DATEDIFF(day,CAST(CreationDate as DATE),@today)<" + repeatD;
            }
            else
            {
                if (smsSettings.DayBefore != null)
                {
                    dayMonthFilter =
                   "AND DATEPART(mm,s.Dob)=DATEPART(mm,DATEADD(day," + smsSettings.DayBefore + ",@today)) AND DATEPART(dd,s.Dob)=DATEPART(dd,DATEADD(day," + smsSettings.DayBefore + ",@today))";
                }
                else
                {
                    dayMonthFilter =
                   "AND DATEPART(mm,s.Dob)=DATEPART(mm,DATEADD(day,0,@today)) AND DATEPART(dd,s.Dob)=DATEPART(dd,DATEADD(day,0,@today))";
                }
                smsHistoryFilter = " AND CAST(CreationDate as DATE) = @today";
            }

            if (programList.Any())
            {
                if (isIn) programFilter = " AND p.Id IN(" + String.Join(",", programList) + ")";
                else programFilter = " AND p.Id NOT IN(" + String.Join(",", programList) + ")";
            }
            string query = @"DECLARE @today as date,@fromdate as date,@todate as date, @month as int, @days as int
            SET @today =  GETDATE();
            SET @month = DATEPART(mm,@today);
            SET @days = DATEPART(dd,@today);
            SET @fromdate  = DATEADD(day,-" + dayAfter + @",@today)
            SET @todate  = DATEADD(day," + dayBefore + @",@today)
            Select a.NickName
                ,a.OrganizationId
                ,a.ProgramId
                ,a.DueAmount
                ,a.NextReceivedDate
                ,a.StudentId
                ,a.Dob
                ,a.PersonalMobile
		        ,a. FatherMobile
		        ,a. MothersMobile
		        ,a.CampusContact from(

            select 
                s.Id as StudentId
	            ,s.NickName as NickName
	            ,o.Id as OrganizationId
                ,p.Id as ProgramId
	            ,s.Dob as Dob
                ,null as DueAmount
                ,convert(datetime, '2000-01-01 00:00:00.000', 121)  as NextReceivedDate
                ,s.Mobile as PersonalMobile
		        ,s.GuardiansMobile1 as FatherMobile
		        ,s.GuardiansMobile2 as MothersMobile
		        ,c.ContactNumber as CampusContact
                ,sh.StudentId as shStudentId
            from [dbo].[StudentProgram] as sp
            Left join [dbo].[Program] as p ON p.Id = sp.ProgramId AND p.Status = " + Program.EntityStatus.Active+@"
            Left join [dbo].[Organization] as o ON o.Id = p.OrganizationId AND o.Status =  " + Organization.EntityStatus.Active + @"
            left join [dbo].[Student] as s ON s.Id = sp.StudentId AND s.Status =  " + Student.EntityStatus.Active + @"
            left join (
				Select distinct StudentId,ProgramId  from [dbo].[SmsHistory] 
					WHERE SmsSettingsId = " + smsSettings.Id + " " + smsHistoryFilter + @"
			) as sh ON sh.StudentId = s.Id  AND sh.ProgramId = sp.ProgramId
            left join [dbo].[Batch] as b ON b.Id = sp.BatchId AND b.Status = " + Batch.EntityStatus.Active + @"
	        left join [dbo].[Campus] as c ON c.Id = b.CampusId AND c.Status =  " + Campus.EntityStatus.Active + @"
            Where  sp.Status = " + StudentProgram.EntityStatus.Active + @"
            AND o.Id = " + organizationId + programFilter+dayMonthFilter+@"
            --AND s.Id = 56755 
            ) as a WHERE a.shStudentId is null";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ScheduleSmsDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<ScheduleSmsDto>().ToList();
            return list;
        }

        public IList<ScheduleSmsDto> LoadDueSmsStudentList(long organizationId, List<long> programList, SmsSettings smsSettings, bool isIn)
        {
            string query = "";
            //string smsHistoryjoinCondition = "AND sh.SmsSettingsId = " + smsSettings.Id + " AND sh.ProgramId = sp.ProgramId";
            string programFilter = "";
            string dayMonthFilter = "";
            string smsHistoryFilter = "";
            int dayBefore = 0;
            int dayAfter = 0; 
            if (smsSettings.DayBefore != null)
            {
                dayBefore = (int)smsSettings.DayBefore;
            }
            if (smsSettings.DayAfter != null)
            {
                dayAfter = (int)smsSettings.DayAfter;
            }

            DateTime today = DateTime.Today;
            if (smsSettings.IsRepeat)
            {
                int repeatD = 1;
                if (smsSettings.RepeatDuration != null && smsSettings.RepeatDuration > 0)
                    repeatD = (int)smsSettings.RepeatDuration;
                
                    dayMonthFilter = " AND (";
                    for (int i = 0; i <= dayBefore+dayAfter; i += repeatD)
                    {
                        if (i == 0)
                        {
                            dayMonthFilter += " spay.NextReceivedDate =DATEADD(day," + i + @",@fromdate) ";
                        }
                        else
                        {
                            dayMonthFilter += " OR spay.NextReceivedDate =DATEADD(day," + i + @",@fromdate) ";
                        }
                        
                    }
                    dayMonthFilter += " ) ";


                    smsHistoryFilter = " AND DATEDIFF(day,CAST(CreationDate as DATE),@today)<" + repeatD;
            }
            else
            {
               dayMonthFilter = " AND (spay.NextReceivedDate = @fromdate OR  spay.NextReceivedDate = @todate )";
               smsHistoryFilter = " AND CAST(CreationDate as DATE) = @today";
            }

            if (programList.Any())
            {
                if (isIn) programFilter = " AND p.Id IN(" + String.Join(",", programList) + ")";
                else programFilter = " AND p.Id NOT IN(" + String.Join(",", programList) + ")";
            }
            query = @"DECLARE @today as date, @fromdate as date, @todate as date;
                    SET @today =  GETDATE();
                    SET @fromdate  = DATEADD(day,-" + dayAfter + @",@today)
                    SET @todate  = DATEADD(day," +dayBefore+ @",@today)


            select a.NickName
                ,a.OrganizationId
                ,a.ProgramId
                ,a.DueAmount
                ,a.NextReceivedDate
                ,a.StudentId
                ,a.Dob
                ,a.PersonalMobile
		        ,a. FatherMobile
		        ,a. MothersMobile
		        ,a.CampusContact from (
            select s.NickName
                ,o.Id as OrganizationId
                ,p.Id as ProgramId
                ,spay.DueAmount
                ,spay.NextReceivedDate
                ,s.Id as StudentId
                ,s.Dob as Dob
                 ,s.Mobile as PersonalMobile
		        ,s.GuardiansMobile1 as FatherMobile
		        ,s.GuardiansMobile2 as MothersMobile
		        ,c.ContactNumber as CampusContact
                ,sh.StudentId as shStudentId
            from (
            select a.StudentProgramId, a.DueAmount,a.NextReceivedDate from (
                SELECT Id, StudentProgramId, NextReceivedDate,DueAmount, RANK() OVER(PARTITION BY StudentProgramId ORDER BY ReceivedDate DESC) as R 
                FROM StudentPayment) as a 
                where a.R=1 AND a.NextReceivedDate>=@fromdate AND a.NextReceivedDate<=@todate
                AND a.DueAmount>0 
            ) as spay  
            left join [dbo].[StudentProgram] as sp ON sp.Id = spay.StudentProgramId  AND sp.Status = " + StudentProgram.EntityStatus.Active + @"
            Left join [dbo].[Program] as p ON p.Id = sp.ProgramId AND p.Status = " + Program.EntityStatus.Active + @"
            Left join [dbo].[Organization] as o ON o.Id = p.OrganizationId AND o.Status =  " + Organization.EntityStatus.Active + @"
            left join [dbo].[Student] as s ON s.Id = sp.StudentId AND s.Status =  " + Student.EntityStatus.Active + @"
            left join (
				Select distinct StudentId,ProgramId  from [dbo].[SmsHistory] 
					WHERE SmsSettingsId = " + smsSettings.Id + " " + smsHistoryFilter + @"
			) as sh ON sh.StudentId = s.Id  AND sh.ProgramId = sp.ProgramId
            left join [dbo].[Batch] as b ON b.Id = sp.BatchId AND b.Status = " + Batch.EntityStatus.Active + @"
	        left join [dbo].[Campus] as c ON c.Id = b.CampusId AND c.Status =  " + Campus.EntityStatus.Active + @"
            Where o.Id = " + organizationId + programFilter + dayMonthFilter + @"
            ) as a WHERE a.shStudentId is null";



            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ScheduleSmsDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<ScheduleSmsDto>().ToList();
            return list;
        }

        #endregion

        #region Helper Function

        #endregion




    }
}
