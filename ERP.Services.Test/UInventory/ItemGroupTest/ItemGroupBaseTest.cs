﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.UInventory.ItemGroupTest
{
    public class ItemGroupBaseTest:TestBase,IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal ItemGroupService _itemGroupService;
        internal ItemGroupFactory _itemGroupFactory;
        internal ItemFactory _ItemFactory;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";

        #endregion

        public ItemGroupBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
             CommonHelper = new CommonHelper();
            _itemGroupFactory = new ItemGroupFactory(null, _session);
            _ItemFactory = new ItemFactory(null,_session);
            _itemGroupService = new ItemGroupService(_session);
        }
        
        public void Dispose()
        {
            _itemGroupFactory.CleanupItem(_ItemFactory.SingleObjectList.Select(x => x.Id).ToList());
            _itemGroupFactory.CleanUp();
            base.Dispose();
        }
    }
}
