﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.DirectWorkOrderTest
{
    interface IDirectWorkOrderLoadTest
    {
        #region Basic Test

        #region WorkOrderLoadActive
        void Load_WorkOrder_Should_Return_List();

        #endregion

        #region WorkOrderLoadById

        void GetWorkOrder_NotNull_Check();

        #endregion

        #region WorkOrderReport
        void WorkOrderReport_Count_Should_Be_Greater_Than_Zero();

        #endregion

        #endregion

        #region Business Logic Test

        #region WorkOrderCount
        void WorkOrder_Count_Should_Be_Greater_Than_Zero();
        void WorkOrder_Count_Should_Not_Be_Greater_Than_Zero();
        void WorkOrder_Count_Authorization_Check();
        void Should_Return_WorkOrder_Count_Invalid_Usermenu();
        void Should_Return_WorkOrder_Count_Invalid_organization();
        void Should_Return_WorkOrder_Count_Invalid_branch();

        #endregion

        #region WorkOrderLoadActive

        void Load_WorkOrder_Should_Return_List_For_Item_Name();
        void Load_WorkOrder_Should_Return_Empty_UserMenu();
        void Load_WorkOrder_Should_Not_Return_List_For_Invalid_Organization();
        void Load_WorkOrder_Should_Not_Return_List_For_Invalid_Branch();
        
        #endregion

        #region WorkOrderLoadById

        void LoadById_Null_Check();

        #endregion

        #region WorkOrderReport

        void Load_Work_Order_Report_Should_Return_List();
        void Should_Return_Load_Work_Order_Report_Invalid_userMenu();
        void Should_Return_Load_Work_Order_Report_Invalid_Organization();
        void Should_Return_Load_Work_Order_Report_Invalid_Branch();

        #endregion

        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "WorkOrder")]
    public class DirectWorkOrderLoadTest : DirectWorkOrderBaseTest, IDirectWorkOrderLoadTest
    {

        #region Basic Test

        #region WorkOrderLoadActive

        [Fact]
        public void Load_WorkOrder_Should_Return_List()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = workOrder.WorkOrderDetails[0].Program;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            var woList = _workOrderService.LoadWorkOrder(0, 10, "", "", userMenu, organizationId, branchId, "", WorkOrder.EntityStatus.Active);
            Assert.True(woList.Count >= 10);
        }
        
        #endregion

        #region WorkOrderLoadById

        [Fact]
        public void GetWorkOrder_NotNull_Check()
        {
            _directWorkOrderFactory.Create().WithBranch().WithSupplier().WithWorkOrderDetails().Persist();
            var workOrder = _workOrderService.GetWorkOrder(_directWorkOrderFactory.Object.Id);
            Assert.NotNull(workOrder);
            Assert.True(workOrder.Id > 0);
        }

        #endregion

        #region WorkOrder Report

        [Fact]
        public void WorkOrderReport_Count_Should_Be_Greater_Than_Zero()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var purposeList = new List<long>() { (int)workOrder.WorkOrderDetails[0].Purpose };
            var itemGroupIdList = new List<long>() { workOrder.WorkOrderDetails[0].Item.ItemGroupId };
            var itemIdList = new List<long>() { workOrder.WorkOrderDetails[0].Item.Id };
            var program = workOrder.WorkOrderDetails[0].Program;
            var programIdList = new List<long>() { program.Id };
            var sessionIdList = new List<long>() { workOrder.WorkOrderDetails[0].Session.Id };
            var workOrderStatus = workOrder.WorkOrderStatus;
            var dateFrom = workOrder.CreationDate;
            var dateTo = DateTime.Now;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            var woCount = _workOrderService.GetWorkOrderReportCount(userMenu, organizationId
                , new List<long>() { branchId }, (int)WorkOrderType.DirectWorkOrder, purposeList, itemGroupIdList
                , itemIdList, programIdList, sessionIdList, (int)workOrderStatus, dateFrom, dateTo);
            Assert.True(woCount >= 10);
        }

        [Fact]
        public void Load_Work_Order_Report_Should_Return_List()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var purposeList = new List<long>() { (int)workOrder.WorkOrderDetails[0].Purpose };
            var itemGroupIdList = new List<long>() { workOrder.WorkOrderDetails[0].Item.ItemGroupId };
            var itemIdList = new List<long>() { workOrder.WorkOrderDetails[0].Item.Id };
            var program = workOrder.WorkOrderDetails[0].Program;
            var programIdList = new List<long>() { program.Id };
            var sessionIdList = new List<long>() { workOrder.WorkOrderDetails[0].Session.Id };
            var workOrderStatus = workOrder.WorkOrderStatus;
            var dateFrom = workOrder.CreationDate;
            var dateTo = DateTime.Now;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            var woList = _workOrderService.LoadWorkOrderReport(0, 10, userMenu, organizationId
                , new List<long>() { branchId }, (int)WorkOrderType.DirectWorkOrder, purposeList, itemGroupIdList
                , itemIdList, programIdList, sessionIdList, (int)workOrderStatus, dateFrom, dateTo);
            Assert.True(woList.Count >= 10);
        }

        [Fact]
        public void Should_Return_Load_Work_Order_Report_Invalid_userMenu()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var purposeList = new List<long>() { (int)workOrder.WorkOrderDetails[0].Purpose };
            var itemGroupIdList = new List<long>() { workOrder.WorkOrderDetails[0].Item.ItemGroupId };
            var itemIdList = new List<long>() { workOrder.WorkOrderDetails[0].Item.Id };
            var program = workOrder.WorkOrderDetails[0].Program;
            var programIdList = new List<long>() { program.Id };
            var sessionIdList = new List<long>() { workOrder.WorkOrderDetails[0].Session.Id };
            var workOrderStatus = workOrder.WorkOrderStatus;
            var dateFrom = workOrder.CreationDate;
            var dateTo = DateTime.Now;
            //var userMenu = BuildUserMenu(branch.Organization, program, branch);
            
            Assert.Throws<MemberAccessException>(() => _workOrderService.LoadWorkOrderReport(0, 10, null, organizationId
                , new List<long>() { branchId }, (int)WorkOrderType.DirectWorkOrder, purposeList, itemGroupIdList
                , itemIdList, programIdList, sessionIdList, (int)workOrderStatus, dateFrom, dateTo));
        }

        [Fact]
        public void Should_Return_Load_Work_Order_Report_Invalid_Organization()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var purposeList = new List<long>() { (int)workOrder.WorkOrderDetails[0].Purpose };
            var itemGroupIdList = new List<long>() { workOrder.WorkOrderDetails[0].Item.ItemGroupId };
            var itemIdList = new List<long>() { workOrder.WorkOrderDetails[0].Item.Id };
            var program = workOrder.WorkOrderDetails[0].Program;
            var programIdList = new List<long>() { program.Id };
            var sessionIdList = new List<long>() { workOrder.WorkOrderDetails[0].Session.Id };
            var workOrderStatus = workOrder.WorkOrderStatus;
            var dateFrom = workOrder.CreationDate;
            var dateTo = DateTime.Now;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);

            Assert.Throws<MemberAccessException>(() => _workOrderService.LoadWorkOrderReport(0, 10, userMenu, organizationId+1
                , new List<long>() { branchId }, (int)WorkOrderType.DirectWorkOrder, purposeList, itemGroupIdList
                , itemIdList, programIdList, sessionIdList, (int)workOrderStatus, dateFrom, dateTo));
        }

        [Fact]
        public void Should_Return_Load_Work_Order_Report_Invalid_Branch()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var purposeList = new List<long>() { (int)workOrder.WorkOrderDetails[0].Purpose };
            var itemGroupIdList = new List<long>() { workOrder.WorkOrderDetails[0].Item.ItemGroupId };
            var itemIdList = new List<long>() { workOrder.WorkOrderDetails[0].Item.Id };
            var program = workOrder.WorkOrderDetails[0].Program;
            var programIdList = new List<long>() { program.Id };
            var sessionIdList = new List<long>() { workOrder.WorkOrderDetails[0].Session.Id };
            var workOrderStatus = workOrder.WorkOrderStatus;
            var dateFrom = workOrder.CreationDate;
            var dateTo = DateTime.Now;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);

            Assert.Throws<MemberAccessException>(() => _workOrderService.LoadWorkOrderReport(0, 10, userMenu, organizationId
                , new List<long>() { branchId+1 }, (int)WorkOrderType.DirectWorkOrder, purposeList, itemGroupIdList
                , itemIdList, programIdList, sessionIdList, (int)workOrderStatus, dateFrom, dateTo));
        }

        #endregion

        #endregion

        #region Business Logic Test

        #region WorkOrderCount

        [Fact]
        public void Should_Return_WorkOrder_Count_Invalid_Usermenu()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            //var userMenu = BuildUserMenu(branch.Organization, program, branch);
            Assert.Throws<MemberAccessException>(()=>_workOrderService.WorkOrderCount(null, organizationId, branchId, "", WorkOrder.EntityStatus.Active));
        }

        [Fact]
        public void Should_Return_WorkOrder_Count_Invalid_organization()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = workOrder.WorkOrderDetails[0].Program;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            Assert.Throws<MemberAccessException>(() => _workOrderService.WorkOrderCount(userMenu, organizationId+1, branchId, "", WorkOrder.EntityStatus.Active));
        }

        [Fact]
        public void Should_Return_WorkOrder_Count_Invalid_branch()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = workOrder.WorkOrderDetails[0].Program;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            Assert.Throws<MemberAccessException>(() => _workOrderService.WorkOrderCount(userMenu, organizationId, branchId + 1, "", WorkOrder.EntityStatus.Active));
        }

        [Fact]
        public void WorkOrder_Count_Should_Be_Greater_Than_Zero()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = workOrder.WorkOrderDetails[0].Program;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            var woCount = _workOrderService.WorkOrderCount(userMenu, organizationId, branchId, "", WorkOrder.EntityStatus.Active);
            Assert.True(woCount >= 10);
        }

        [Fact]
        public void WorkOrder_Count_Should_Not_Be_Greater_Than_Zero()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = workOrder.WorkOrderDetails[0].Program;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            Assert.Throws<MemberAccessException>(() => _workOrderService.WorkOrderCount(userMenu, -1, branchId, "", WorkOrder.EntityStatus.Active));
        }

        [Fact]
        public void WorkOrder_Count_Authorization_Check()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = workOrder.WorkOrderDetails[0].Program;
            Assert.Throws<MemberAccessException>(() => _workOrderService.WorkOrderCount(new List<UserMenu>(), -1, branchId, "", WorkOrder.EntityStatus.Active));
        }

        #endregion

        #region WorkOrderLoadActive
        [Fact]
        public void Load_WorkOrder_Should_Return_List_For_Item_Name()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = workOrder.WorkOrderDetails[0].Program;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            var itemName = workOrder.WorkOrderDetails[0].Item.Name;
            var woList = _workOrderService.LoadWorkOrder(0, 10, "", "", userMenu, organizationId, branchId, itemName, WorkOrder.EntityStatus.Active);
            Assert.True(woList.Count >= 10);
        }

        [Fact]
        public void Load_WorkOrder_Should_Return_Empty_UserMenu()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            Assert.Throws<InvalidDataException>(
                () =>
                    _workOrderService.LoadWorkOrder(0, 10, "", "", null, organizationId, branchId, "",
                        WorkOrder.EntityStatus.Active));
        }

        [Fact]
        public void Load_WorkOrder_Should_Not_Return_List_For_Invalid_Organization()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            //var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = workOrder.WorkOrderDetails[0].Program;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            Assert.Throws<InvalidDataException>(() => _workOrderService.LoadWorkOrder(0, 10, "", "", userMenu, -1, branchId, "", WorkOrder.EntityStatus.Active));
        }

        [Fact]
        public void Load_WorkOrder_Should_Not_Return_List_For_Invalid_Branch()
        {
            _directWorkOrderFactory.CreateMore(10).Persist();
            var workOrder = _directWorkOrderFactory.GetLastCreatedObjectList()[0];
            var branch = workOrder.Branch;
            var organizationId = branch.Organization.Id;
            //var branchId = branch.Id;
            var program = workOrder.WorkOrderDetails[0].Program;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            Assert.Throws<InvalidDataException>(() => _workOrderService.LoadWorkOrder(0, 10, "", "", userMenu, organizationId, -1, "", WorkOrder.EntityStatus.Active));
        }

        #endregion

        #region WorkOrderLoadById

        [Fact]
        public void LoadById_Null_Check()
        {
            Assert.Throws<InvalidDataException>(() => _workOrderService.GetWorkOrder(-1));
        }

        #endregion

        #endregion
    }
}