﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.AdminisTration.AreaControllersTest;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.AreaControllersTest{
    
    [Trait("Area", "Administration")]
    [Trait("Service", "AreaControllers")]
    public class AreaControllersLoadActiveTest : AreaControllersBaseTest
    {
        #region Basic Test
        
        [Fact]
        public void LoadActive_should_return_list()
        {
            _areaControllersFactory.CreateMore(100).Persist();
            var areaControllersList = _areaControllersService.LoadActive(0,100,"","","","","","");
            Assert.Equal(100, areaControllersList.Count);
        }

        #endregion
    }
}
