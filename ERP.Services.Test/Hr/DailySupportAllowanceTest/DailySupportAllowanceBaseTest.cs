﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.DailySupportAllowanceTest
{
    public class DailySupportAllowanceBaseTest : TestBase, IDisposable
    {
        #region Properties & Object Initialization

        protected readonly CommonHelper _commonHelper;
        protected readonly DailySupportAllowanceSettingFactory _dailySupportAllowanceSettingFactory;
        protected readonly OrganizationFactory _organizationFactory;
        protected readonly DesignationFactory _designationFactory;
        protected readonly IDailySupportAllowanceSettingService _dailySupportAllowanceService;
        protected readonly IAllowanceSheetService _allowanceSheetService;
        protected readonly IAttendanceAdjustmentService _attendanceAdjustmentService;
        #endregion

        #region Constructor
        public DailySupportAllowanceBaseTest()
        {
            _commonHelper = new CommonHelper();
            _dailySupportAllowanceSettingFactory = new DailySupportAllowanceSettingFactory(null, Session);
            _organizationFactory = new OrganizationFactory(null, Session);
            _designationFactory = new DesignationFactory(null, Session);

            _dailySupportAllowanceService = new DailySupportAllowanceSettingService(Session);
            _allowanceSheetService = new AllowanceSheetService(Session);
            _attendanceAdjustmentService = new AttendanceAdjustmentService(Session);
        }
        #endregion

        #region disposal

        public void Dispose()
        {
            _dailySupportAllowanceSettingFactory.CleanUp();
            _designationFactory.Cleanup();
            programFactory.Cleanup();
            _organizationFactory.Cleanup();
        }

        #endregion
    }
}
