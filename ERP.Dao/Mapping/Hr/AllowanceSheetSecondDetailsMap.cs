using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class AllowanceSheetSecondDetailsMap : BaseClassMap<AllowanceSheetSecondDetails,long> {

        public AllowanceSheetSecondDetailsMap()
        {
			Table("HR_AllowanceSheetSecondDetails");
			LazyLoad();
            References(x => x.AllowanceSheet).Column("AllowanceSheetId");
            References(x => x.TeamMemberDepartment).Column("TeamMemberDepartmentId");
            References(x => x.TeamMemberDesignation).Column("TeamMemberDesignationId");
            References(x => x.TeamMemberBranch).Column("TeamMemberBranchId");
            References(x => x.TeamMemberCampus).Column("TeamMemberCampusId");
            References(x => x.ChildrenInfo).Column("ChildrenInfoId");
            References(x => x.ChildrenAllowanceSetting).Column("ChildrenAllowanceSettingId");
            References(x => x.SpecialAllowanceSetting).Column("SpecialAllowanceSettingId");
            References(x => x.MemberLoanZakat).Column("MemberLoanZakatId");
            References(x => x.MemberLoanCsr).Column("MemberLoanCsrId");
            Map(x => x.ChildrenAllowanceAmount).Column("ChildrenAllowanceAmount");
            Map(x => x.SpecialAllowanceAmount).Column("SpecialAllowanceAmount");
            Map(x => x.MemberLoanZakatAmount).Column("MemberLoanZakatAmount");
            Map(x => x.MemberLoanCsrAmount).Column("MemberLoanCsrAmount");
            Map(x => x.TotalAllowanceAmount).Column("TotalAllowanceAmount");
            Map(x => x.AllowanceDate).Column("AllowanceDate");
        }
    }
}
