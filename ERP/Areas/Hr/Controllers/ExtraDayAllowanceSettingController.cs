﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Microsoft.Ajax.Utilities;
using NHibernate;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class ExtraDayAllowanceSettingController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IExtradayAllowanceSettingService _extradayAllowanceSettingService;
        private readonly IUserService _userService;

        public ExtraDayAllowanceSettingController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _extradayAllowanceSettingService = new ExtradayAllowanceSettingService(session);
            _userService = new UserService(session);
        }

        #endregion

        #region Details/Index

        public ActionResult Index(string message, string type)
        {
            try
            {
                switch (type)
                {
                    case "s":
                        ViewBag.SuccessMessage = message;
                        break;
                    case "e":
                        ViewBag.ErrorMessage = message;
                        break;
                }
                InitializeIndex();
                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return HttpNotFound(e.Message);
            }
        }

        public ActionResult Details(long id)
        {
            try
            {
                var entity = _extradayAllowanceSettingService.GetExtradayAllowanceSetting(id);
                if (entity != null)
                {
                    return View(DetailsViewModel(entity));
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return RedirectToAction("Index");
        }

        #endregion

        #region Operational Functions

        #region Create

        public ActionResult Create(string message)
        {
            try
            {
                if (!string.IsNullOrEmpty(message))
                {
                    ViewBag.SuccessMessage = message;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            finally
            {
                Initialize();
            }
            return View(new ExtraDayAllowanceSettingForm());
        }

        [HttpPost]
        public ActionResult Create(ExtraDayAllowanceSettingForm vm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    Initialize();
                    return View(vm);
                }
                var userMenus = (List<UserMenu>) ViewBag.UserMenu;
                var authOrgId = AuthHelper.LoadOrganizationIdList(userMenus,
                    _commonHelper.ConvertIdToList(vm.OrganizationId));
                if (authOrgId.Count <= 0)
                    ViewBag.ErrorMessage = "Permission Denied For Organization";
                var entity = FillEntityFormViewModel(vm);
                var result = _extradayAllowanceSettingService.SaveOrUpdate(entity, userMenus);
                if (result)
                {
                    return RedirectToAction("Index", "ExtraDayAllowanceSetting",
                        new {message = "Settings Successfully saved!", type = "s"});
                }
                return RedirectToAction("Index", "ExtraDayAllowanceSetting",
                    new {message = WebHelper.CommonErrorMessage, type = "e"});
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            finally
            {
                Initialize();
            }
            return View(vm);
        }

        #endregion

        #region Update

        public ActionResult Edit(long id)
        {
            try
            {
                var editType = (int)AllowanceSettingsEditType.Uneditable;
                if (id <= 0)
                {
                    return RedirectToAction("Index", "Invalid request submitted. Please try again!", "e");
                }
                var entity = _extradayAllowanceSettingService.GetExtradayAllowanceSetting(id);
                if (entity == null)
                {
                    return RedirectToAction("Index", WebHelper.CommonErrorMessage, "e");
                }
                var authOrg = AuthHelper.LoadOrganizationIdList((List<UserMenu>)ViewBag.UserMenu, _commonHelper.ConvertIdToList(entity.Organization.Id));
                if (authOrg.Count <= 0)
                {
                    return RedirectToAction("Index", "ExtraDayAllowanceSetting", new { message = "You don't have enough permission", type = "e" });
                }
                DateTime? maxDate;
                _extradayAllowanceSettingService.GetEditTypeAndMaxDate(entity, out editType, out maxDate);

                ExtraDayAllowanceSettingForm vm = null;
                if (editType == (int) AllowanceSettingsEditType.Uneditable)
                {
                    return RedirectToAction("Index", "ExtraDayAllowanceSetting", new { message = "This settings is not editable!", type = "e" });
                }
                if (editType == (int)AllowanceSettingsEditType.MinimumEditable)
                {
                    vm = EditViewModel(entity);
                    return View(vm);
                }
                if (editType == (int)AllowanceSettingsEditType.MaximumEditable)
                {
                    vm = UpdateViewModel(entity);
                    return View("Update", vm);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return RedirectToAction("Index", "ExtraDayAllowanceSetting", new { message = WebHelper.CommonErrorMessage, type = "e" });
        }

        [HttpPost]
        public ActionResult Edit(long id, ExtraDayAllowanceSettingForm vm)
        {
            string message = "";
            if (id <= 0)
            {
                return HttpNotFound();
            }
            try
            {
                var entity = FillEntityFormViewModel(vm);
                if (entity == null)
                {
                    throw new InvalidDataException("Submitted setting not found!");
                }
                var userMenus = (List<UserMenu>)ViewBag.UserMenu;
                var authOrgId = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(entity.Organization.Id));
                if (authOrgId.Count <= 0)
                    ViewBag.ErrorMessage = "Permission Denied for Organization";
                var result = _extradayAllowanceSettingService.SaveOrUpdate(entity, userMenus, false);
                if (result)
                {
                    message = "Settings Successfully updated!";
                    return RedirectToAction("Index", "ExtraDayAllowanceSetting", new { message = message, type = "s" });
                }
            }
            catch (NullReferenceException ex)
            {
                message = ex.Message;
            }
            catch (NullObjectException ex)
            {
                message = ex.Message;
            }
            catch (ArgumentNullException ex)
            {
                message = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (DependencyException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = WebHelper.CommonErrorMessage;
            }
            return RedirectToAction("Index", "ExtraDayAllowanceSetting", new { message = message, type = "e" });
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(long id)
        {
            bool status = false;
            string message = "";
            try
            {
                var entity = _extradayAllowanceSettingService.GetExtradayAllowanceSetting(id);
                var userMenus = (List<UserMenu>)ViewBag.UserMenu;
                var authOrgId = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(entity.Organization.Id));
                if (authOrgId.Count <= 0)
                    ViewBag.ErrorMessage = "Permission Denied for Organization";
                _extradayAllowanceSettingService.Delete(entity, userMenus);
                status = true;
                message = "Settings Successfully Delete!";
            }
            catch (DependencyException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { isSuccess = status, Text = message });
        }

        #endregion

        #endregion

        #region Ajax Function

        [HttpPost]
        public ActionResult SettingList(int draw, int start, int length, string organizationIdStr, string employementStatusStr, string holidayTypeStr)
        {
            int recordsFiltered = 0;
            int recordsTotal = 0;
            bool isSuccess = true;
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "1":
                            orderBy = "Organization";
                            break;
                        case "2":
                            orderBy = "Name";
                            break;
                        case "5":
                            orderBy = "CalculationOn";
                            break;
                        case "6":
                            orderBy = "MultiplyingFactor";
                            break;
                        case "7":
                            orderBy = "EffectiveDate";
                            break;
                        default:
                            orderBy = "Organization";
                            break;
                    }
                }
                #endregion

                #region Holiday Types

                var holidayType = (AllowanceSettingHolidayType)(!string.IsNullOrEmpty(holidayTypeStr) ? Enum.Parse(typeof(AllowanceSettingHolidayType), holidayTypeStr) : 0);
                var holidayTypeList = new List<AllowanceSettingHolidayType>();
                if (holidayType != 0)
                {
                    switch (holidayType)
                    {
                        case AllowanceSettingHolidayType.Gazetted:
                            holidayTypeList.Add(AllowanceSettingHolidayType.Gazetted);
                            break;
                        case AllowanceSettingHolidayType.Management:
                            holidayTypeList.Add(AllowanceSettingHolidayType.Management);
                            break;
                        case AllowanceSettingHolidayType.Weekend:
                            holidayTypeList.Add(AllowanceSettingHolidayType.Weekend);
                            break;
                    }
                }
                
                #endregion

                #region Employement Status
                var empStatus = (MemberEmploymentStatus)(!string.IsNullOrEmpty(employementStatusStr) ? Enum.Parse(typeof(MemberEmploymentStatus), employementStatusStr) : 0);
                var employmentTypeList = new List<MemberEmploymentStatus>();
                if (empStatus != 0)
                {
                    switch (empStatus)
                    {
                        case MemberEmploymentStatus.Contractual:
                            employmentTypeList.Add(MemberEmploymentStatus.Contractual);
                            break;
                        case MemberEmploymentStatus.PartTime:
                            employmentTypeList.Add(MemberEmploymentStatus.PartTime);
                            break;
                        case MemberEmploymentStatus.Permanent:
                            employmentTypeList.Add(MemberEmploymentStatus.Permanent);
                            break;
                        case MemberEmploymentStatus.Probation:
                            employmentTypeList.Add(MemberEmploymentStatus.Probation);
                            break;
                        case MemberEmploymentStatus.Intern:
                            employmentTypeList.Add(MemberEmploymentStatus.Intern);
                            break;
                    }
                }
                #endregion

                long organizationId = !string.IsNullOrEmpty(organizationIdStr) ? Convert.ToInt64(organizationIdStr) : 0;
                var list = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(start, length, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu, organizationId,
                                                                           holidayTypeList, employmentTypeList);
                recordsTotal = _extradayAllowanceSettingService.GetExtradayAllowanceSettingsCount((List<UserMenu>)ViewBag.UserMenu, organizationId,
                                                                           holidayTypeList, employmentTypeList);
                recordsFiltered = recordsTotal;
                int sl = start + 1;
                foreach (var row in list)
                {
                    var actionString = "";
                    var str = new List<string>()
                    {
                        /*sl.ToString(CultureInfo.InvariantCulture),*/
                        row.Organization.ShortName, 
                        row.Name, GetEmploymentStatusString(row.IsContractual, row.IsIntern, row.IsPartTime, row.IsPermanent, row.IsProbation),
                        GetHolidayTypeString(row.IsGazetted, row.IsManagement, row.IsWeekend),
                        row.CalculationOn == 1 ? "Basic Salary" : "Gross Salary",
                        row.MultiplyingFactor.ToString("N1"),
                        String.Format("{0:MMM d, yyyy}", row.EffectiveDate),
                        row.ClosingDate != null ? String.Format("{0:MMM d, yyyy}", row.ClosingDate.Value) : ""
                    };
                    actionString += LinkGenerator.GetDynamicLink("Details", "ExtraDayAllowanceSetting", row.Id, "glyphicon-list");
                    actionString += LinkGenerator.GetDynamicLink("Edit", "ExtraDayAllowanceSetting", row.Id, "glyphicon-edit");
                    actionString += LinkGenerator.GetDynamicLink("Delete", "ExtraDayAllowanceSetting", row.Id, "glyphicon-trash");
                    str.Add(actionString);
                    getData.Add(str);
                    //sl++;
                }
            }
            catch (NullObjectException ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        #endregion

        #region Helper Functions

        private void Initialize()
        {
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization((List<UserMenu>)ViewBag.UserMenu), "Id", "ShortName");
            ViewBag.holidayTypeList = _commonHelper.LoadEmumToDictionary<AllowanceSettingHolidayType>();
            ViewBag.employmentStatus = _commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int>() { (int)MemberEmploymentStatus.Retired });
            var calculationOnList = Enum.GetValues(typeof(CalculationOnTypeEnum)).Cast<CalculationOnTypeEnum>()
                                    .Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString(CultureInfo.InvariantCulture) }).ToList();
            ViewBag.CalculationOnList = new SelectList(calculationOnList, "Value", "Text");
            var multipleFactor = new Dictionary<string, decimal> { { "1.0", 1.0M }, { "1.5", 1.5M }, { "2.0", 2.0M }, { "2.5", 2.5M } };
            ViewBag.MultipleFactorList = new SelectList(multipleFactor.ToList().OrderBy(x => x.Key), "Key", "Value");
        }

        private void InitializeIndex()
        {
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization((List<UserMenu>)ViewBag.UserMenu), "Id", "ShortName");
            ViewBag.HolidayTypeList = _commonHelper.LoadEmumToDictionary<AllowanceSettingHolidayType>();
            ViewBag.EmploymentStatus = _commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int>() { (int)MemberEmploymentStatus.Retired });
        }

        private ExtraDayAllowanceSettingForm EditViewModel(ExtradayAllowanceSetting entity)
        {
            ViewBag.EmploymentStatusList = new List<MemberEmploymentStatus>()
                                        {
                                            MemberEmploymentStatus.Contractual,
                                            MemberEmploymentStatus.Intern,
                                            MemberEmploymentStatus.PartTime,
                                            MemberEmploymentStatus.PartTime,
                                            MemberEmploymentStatus.Permanent,
                                            MemberEmploymentStatus.Probation
                                        };
            ViewBag.HolidayTypeList = new List<AllowanceSettingHolidayType>()
                                        {
                                            AllowanceSettingHolidayType.Gazetted,
                                            AllowanceSettingHolidayType.Management,
                                            AllowanceSettingHolidayType.Weekend
                                        };
            var model = new ExtraDayAllowanceSettingForm
            {
                Id = entity.Id,
                ClosingDate = entity.ClosingDate,
                EffectiveDate = entity.EffectiveDate,
                Name = entity.Name,
                CalculationOn = entity.CalculationOn,
                MultiplyingFactor = entity.MultiplyingFactor,
                EmployeeStatuses = new List<MemberEmploymentStatus>(),
                HolidayTypes = new List<AllowanceSettingHolidayType>()
            };
            GetEmploymentStatusAndHolidayTypes(entity, model);
            var calculationOn = Enum.Parse(typeof(CalculationOnTypeEnum), entity.CalculationOn.ToString(CultureInfo.InvariantCulture));
            ViewBag.CalculationOn = calculationOn;
            var multipleFactor = String.Format("{0:0.0#}", entity.MultiplyingFactor);
            ViewBag.MultipleFactor = multipleFactor;

            var organizationId = AuthHelper.LoadOrganizationIdList((List<UserMenu>)ViewBag.UserMenu, _commonHelper.ConvertIdToList(entity.Organization.Id));
            var organizationName = "";
            if (organizationId.Count > 0)
            {
                organizationName = _organizationService.LoadById(organizationId[0]).ShortName;
            }
            ViewBag.OrganizationName = organizationName;
            string holidayTypeStr = string.Join(", ", model.HolidayTypes);
            ViewBag.HolidayTypes = holidayTypeStr;
            string employementStatusStr = string.Join(", ", model.EmployeeStatuses);
            ViewBag.EmployementStatus = employementStatusStr;
            return model;
        }

        private ExtraDayAllowanceSettingForm UpdateViewModel(ExtradayAllowanceSetting entity)
        {
            ViewBag.holidayTypeList = _commonHelper.LoadEmumToDictionary<AllowanceSettingHolidayType>();
            ViewBag.employmentStatus = _commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int>() { (int)MemberEmploymentStatus.Retired });
            var calculationOnList = Enum.GetValues(typeof(CalculationOnTypeEnum)).Cast<CalculationOnTypeEnum>()
                                    .Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString(CultureInfo.InvariantCulture) }).ToList();
            ViewBag.CalculationOnList = new SelectList(calculationOnList, "Value", "Text", entity.CalculationOn);
            var multipleFactor = new Dictionary<string, decimal> { { "1.00", 1.0M }, { "1.50", 1.5M }, { "2.00", 2.0M }, { "2.50", 2.5M } };
            ViewBag.MultipleFactorList = new SelectList(multipleFactor.ToList().OrderBy(x => x.Key), "Key", "Value", entity.MultiplyingFactor.ToString("#.##"));

            var organizationId = AuthHelper.LoadOrganizationIdList((List<UserMenu>)ViewBag.UserMenu, _commonHelper.ConvertIdToList(entity.Organization.Id));
            var organizationName = "";
            if (organizationId.Count > 0)
            {
                organizationName = _organizationService.LoadById(organizationId[0]).ShortName;
            }
            ViewBag.OrganizationName = organizationName;

            var model = new ExtraDayAllowanceSettingForm
            {
                Id = entity.Id,
                ClosingDate = entity.ClosingDate,
                EffectiveDate = entity.EffectiveDate,
                Name = entity.Name,
                OrganizationId = organizationId[0],
                CalculationOn = entity.CalculationOn,
                MultiplyingFactor = entity.MultiplyingFactor,
                EmployeeStatuses = new List<MemberEmploymentStatus>(),
                HolidayTypes = new List<AllowanceSettingHolidayType>()
            };
            GetEmploymentStatusAndHolidayTypes(entity, model);
            return model;
        }

        private ExtraDayAllowanceSettingForm DetailsViewModel(ExtradayAllowanceSetting entity)
        {
            ViewBag.EmploymentStatusList = new List<MemberEmploymentStatus>()
                                        {
                                            MemberEmploymentStatus.Contractual,
                                            MemberEmploymentStatus.Intern,
                                            MemberEmploymentStatus.PartTime,
                                            MemberEmploymentStatus.PartTime,
                                            MemberEmploymentStatus.Permanent,
                                            MemberEmploymentStatus.Probation
                                        };
            ViewBag.HolidayTypeList = new List<AllowanceSettingHolidayType>()
                                        {
                                            AllowanceSettingHolidayType.Gazetted,
                                            AllowanceSettingHolidayType.Management,
                                            AllowanceSettingHolidayType.Weekend
                                        };
            var model = new ExtraDayAllowanceSettingForm
            {
                Id = entity.Id,
                ClosingDate = entity.ClosingDate,
                EffectiveDate = entity.EffectiveDate,
                Name = entity.Name,
                EmployeeStatuses = new List<MemberEmploymentStatus>(),
                HolidayTypes = new List<AllowanceSettingHolidayType>()
            };
            GetEmploymentStatusAndHolidayTypes(entity, model);
            var calculationOn = Convert.ToString((Enum.Parse(typeof(CalculationOnTypeEnum), entity.CalculationOn.ToString(CultureInfo.InvariantCulture))));
            ViewBag.CalculationOnVal = calculationOn;
            var multipleFactorVal = String.Format("{0:0.0#}", entity.MultiplyingFactor);
            ViewBag.MultipleFactorVal = multipleFactorVal;
            var organizationId = AuthHelper.LoadOrganizationIdList((List<UserMenu>)ViewBag.UserMenu, _commonHelper.ConvertIdToList(entity.Organization.Id));
            var organizationName = "";
            if (organizationId.Count > 0)
            {
                organizationName = _organizationService.LoadById(organizationId[0]).ShortName;
            }
            ViewBag.OrganizationName = organizationName;
            ViewBag.EffectiveFrom = entity.EffectiveDate.ToString("d MMM, yyyy");
            ViewBag.ClosingDate = entity.ClosingDate != null ? entity.ClosingDate.Value.ToString("d MMM, yyyy") : string.Empty;
            ViewBag.CreateByStr = _userService.GetByAspNetUser(entity.CreateBy).AspNetUser.Email;
            ViewBag.ModifiedByStr = _userService.GetByAspNetUser(entity.ModifyBy).AspNetUser.Email;
            ViewBag.CreationDateStr = entity.CreationDate.ToString("d MMM, yyyy");
            ViewBag.ModifiedDateStr = entity.ModificationDate.ToString("d MMM, yyyy");
            return model;
        }

        private ExtradayAllowanceSetting FillEntityFormViewModel(ExtraDayAllowanceSettingForm vm)
        {
            ExtradayAllowanceSetting entity = null;
            if (vm.Id > 0)
            {
                entity = _extradayAllowanceSettingService.GetExtradayAllowanceSetting(vm.Id);
                if (entity != null)
                {
                    int editType = -1;
                    DateTime? maxDate;

                    _extradayAllowanceSettingService.GetEditTypeAndMaxDate(entity, out editType, out maxDate);
                    if (editType == 1)
                    {
                        entity.ClosingDate = vm.ClosingDate;
                    }
                    else if (editType == 2)
                    {
                        entity.Name = vm.Name;
                        entity.Organization = _organizationService.LoadById(vm.OrganizationId);
                        entity.CalculationOn = vm.CalculationOn;
                        entity.MultiplyingFactor = vm.MultiplyingFactor;
                        entity.EffectiveDate = vm.EffectiveDate;
                        entity.ClosingDate = vm.ClosingDate;
                        SetEmploymentStatusAndHolidayTypes(entity, vm);
                    }
                }
            }
            else if (vm.Id == 0)
            {
                var model = new ExtradayAllowanceSetting()
                {
                    Name = vm.Name,
                    Organization = _organizationService.LoadById(vm.OrganizationId),
                    CalculationOn = vm.CalculationOn,
                    MultiplyingFactor = vm.MultiplyingFactor,
                    EffectiveDate = vm.EffectiveDate,
                    ClosingDate = vm.ClosingDate
                };
                SetEmploymentStatusAndHolidayTypes(model, vm);
                entity = model;
            }
            return entity;
        }

        private static void GetEmploymentStatusAndHolidayTypes(ExtradayAllowanceSetting entity, ExtraDayAllowanceSettingForm model)
        {
            var employementlist = new List<MemberEmploymentStatus>();
            model.EmployeeStatuses = new List<MemberEmploymentStatus>();
            if (entity.IsContractual)
            {
                employementlist.Add(MemberEmploymentStatus.Contractual);
            }
            if (entity.IsPartTime)
            {
                employementlist.Add(MemberEmploymentStatus.PartTime);
            }
            if (entity.IsPermanent)
            {
                employementlist.Add(MemberEmploymentStatus.Permanent);
            }
            if (entity.IsProbation)
            {
                employementlist.Add(MemberEmploymentStatus.Probation);
            }
            if (entity.IsIntern)
            {
                employementlist.Add(MemberEmploymentStatus.Intern);
            }
            model.EmployeeStatuses = employementlist;

            model.HolidayTypes = new List<AllowanceSettingHolidayType>();
            var holidays = new List<AllowanceSettingHolidayType>();
            if (entity.IsGazetted)
            {
                holidays.Add(AllowanceSettingHolidayType.Gazetted);
            }
            if (entity.IsManagement)
            {
                holidays.Add(AllowanceSettingHolidayType.Management);
            }
            if (entity.IsWeekend)
            {
                holidays.Add(AllowanceSettingHolidayType.Weekend);
            }
            model.HolidayTypes = holidays;

        }

        private static void SetEmploymentStatusAndHolidayTypes(ExtradayAllowanceSetting entity,
            ExtraDayAllowanceSettingForm vm)
        {
            bool isGazetted = false;
            bool isManagement = false;
            bool isWeekend = false;
            if (vm.HolidayTypes != null)
            {
                foreach (var hType in vm.HolidayTypes)
                {
                    switch (hType)
                    {
                        case AllowanceSettingHolidayType.Gazetted:
                            isGazetted = true;
                            break;
                        case AllowanceSettingHolidayType.Management:
                            isManagement = true;
                            break;
                        case AllowanceSettingHolidayType.Weekend:
                            isWeekend = true;
                            break;
                    }
                }
            }

            bool isContractual = false;
            bool isPartTime = false;
            bool isProbation = false;
            bool isPermanent = false;
            bool isIntern = false;
            if (vm.EmployeeStatuses != null)
            {
                foreach (var empStatus in vm.EmployeeStatuses)
                {
                    switch (empStatus)
                    {
                        case MemberEmploymentStatus.Contractual:
                            isContractual = true;
                            break;
                        case MemberEmploymentStatus.PartTime:
                            isPartTime = true;
                            break;
                        case MemberEmploymentStatus.Probation:
                            isProbation = true;
                            break;
                        case MemberEmploymentStatus.Permanent:
                            isPermanent = true;
                            break;
                        case MemberEmploymentStatus.Intern:
                            isIntern = true;
                            break;
                    }
                }
            }
            entity.IsGazetted = isGazetted;
            entity.IsManagement = isManagement;
            entity.IsWeekend = isWeekend;
            entity.IsContractual = isContractual;
            entity.IsPartTime = isPartTime;
            entity.IsPermanent = isPermanent;
            entity.IsProbation = isProbation;
            entity.IsIntern = isIntern;
        }

        private static string GetHolidayTypeString(bool isGazetted, bool isManagement, bool isWeekend)
        {
            var holidayTypes = new List<AllowanceSettingHolidayType>();

            if (isGazetted)
            {
                holidayTypes.Add(AllowanceSettingHolidayType.Gazetted);
            }
            if (isManagement)
            {
                holidayTypes.Add(AllowanceSettingHolidayType.Management);
            }
            if (isWeekend)
            {
                holidayTypes.Add(AllowanceSettingHolidayType.Weekend);
            }
            return string.Join(", ", holidayTypes);
        }

        private static string GetEmploymentStatusString(bool isContractual, bool isIntern, bool isPartTime, bool isPermanent, bool isProbation)
        {
            var emplist  = new List<MemberEmploymentStatus>();

            if (isProbation)
            {
                emplist.Add(MemberEmploymentStatus.Probation);
            }
            if (isPermanent)
            {
                emplist.Add(MemberEmploymentStatus.Permanent);
            }
            if (isPartTime)
            {
                emplist.Add(MemberEmploymentStatus.PartTime);
            }
            if (isContractual)
            {
                emplist.Add(MemberEmploymentStatus.Contractual);
            }
            if (isIntern)
            {
                emplist.Add(MemberEmploymentStatus.Intern);
            }
            return string.Join(", ", emplist);
        }

        #endregion
    }
}
