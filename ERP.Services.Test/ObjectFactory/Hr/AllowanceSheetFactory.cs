﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class AllowanceSheetFactory : ObjectFactoryBase<AllowanceSheet>
    {
        public AllowanceSheetFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

        }

        #region Create
        #endregion

        #region Others
        #endregion

        #region CleanUp

        #endregion

        public AllowanceSheetFactory CleanUpByTeamMember(long teamMemberId)
        {
            var ids = _session.QueryOver<AllowanceSheet>().Where(x => x.TeamMember.Id == teamMemberId).List<AllowanceSheet>().Select(team => team.Id).ToList();
            if (ids != null && ids.Count > 0)
            {
                string queryF = "delete from HR_AllowanceSheetFirstDetails where AllowanceSheetId in ( " + string.Join(",", ids) + ")";
                string queryS = "delete from HR_AllowanceSheetSecondDetails where AllowanceSheetId in ( " + string.Join(",", ids) + ")";
                _session.CreateSQLQuery(queryF).ExecuteUpdate();
                _session.CreateSQLQuery(queryS).ExecuteUpdate();
                string queryFinal = "delete from HR_AllowanceSheet where Id in ( " + string.Join(",", ids) + ")";
                _session.CreateSQLQuery(queryFinal).ExecuteUpdate();
            }
            return this;
        }
    }
}
