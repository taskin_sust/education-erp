﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{

    public interface IMemberOfficialDetailService : IBaseService 
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        //MemberOfficialDetail GetMemberOfficcalDetail(long? teamMemberId = null, int? pin = null);

        #endregion

        #region List Loading Function
        IList<MemberOfficialDetail> GetTeamMemberLists(string synchronzerKey, bool isReset);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        string GetNewCardNumber();

        #endregion

    }
    public class MemberOfficialDetailService : BaseService, IMemberOfficialDetailService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IMemberOfficialDetailDao _memberOfficialDetailDao;
        public MemberOfficialDetailService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _memberOfficialDetailDao = new MemberOfficialDetailDaoDao { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool Save()
        {

            return true;
        }

        #endregion

        #region Single Instances Loading Function
        //public MemberOfficialDetail GetMemberOfficcalDetail(long? teamMemberId = null, int? pin = null)
        //{
        //    try
        //    {
        //        return _memberOfficialDetailDao.GetMemberOfficcalDetail(teamMemberId,pin);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw ex;
        //    }
        //}
        #endregion

        #region List Loading Function

        public IList<MemberOfficialDetail> GetTeamMemberLists(string synchronzerKey, bool isReset)
        {
            try
            {
                return _memberOfficialDetailDao.GetTeamMemberLists(synchronzerKey, isReset);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetNewCardNumber()
        {
            return _memberOfficialDetailDao.GetNewCardNumber();
        }

        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
        
    }
}
