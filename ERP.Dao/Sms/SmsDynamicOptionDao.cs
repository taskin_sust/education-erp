﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public interface ISmsDynamicOptionDao : IBaseDao<SmsDynamicOption, long>
    {
        
    }
    public class SmsDynamicOptionDao:BaseDao<SmsDynamicOption,long>,ISmsDynamicOptionDao
    {
       
    }
}
