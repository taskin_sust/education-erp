﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Teachers;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Teachers.Controllers
{
    [UerpArea("Teachers")]
    [Authorize]
    [AuthorizeAccess]
    public class TeacherPaymentController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeachersArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly IOrganizationService _organizationService;
        private readonly IBranchService _branchService; 
        private readonly ITeacherPaymentService _teacherPaymentService;       
        private readonly ITeacherClassEntryDetailsService _teacherClassEntryDetailsService; 
        private readonly ITeacherScriptEvaluationDetailsService _teacherScriptEvaluationDetailsService;
        private readonly ITeacherAcademicMaterialDetailsService _teacherAcademicMaterialDetailsService;
        private readonly IUserService _userService;
        private List<UserMenu> _userMenu;
        public TeacherPaymentController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _organizationService = new OrganizationService(session);
                _branchService = new BranchService(session);
                _teacherPaymentService = new TeacherPaymentService(session);
                _teacherClassEntryDetailsService = new TeacherClassEntryDetailsService(session);
                _teacherScriptEvaluationDetailsService = new TeacherScriptEvaluationDetailsService(session);
                _teacherAcademicMaterialDetailsService = new TeacherAcademicMaterialDetailsService(session);
                _userService = new UserService(session);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Teacher Payment 

        public ActionResult Index(string message="")
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                //string teacherName, string heldDate, string activityType
                if (!string.IsNullOrEmpty(message))
                {
                    ViewBag.SuccessMessage = message.Trim(new []{' ',','});
                }
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.CampusList = new SelectList(string.Empty, "Value", "Text");
                var paymentStatus = new SelectList(TeacherPaymentStatus.GetTeacherPaymentStatus(), "Key", "Value");
                ViewBag.PaymentStatusList = new SelectList(paymentStatus, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return HttpNotFound();
            }
        }

        [HttpPost]
        public JsonResult GenerateTeacherPaymentList(int draw, int start, int length, string teacherName, string heldDate, string classType, string organization, /*string paymentType*/ string program, string session,string branch,string campus,string paymentStatus, string tpin, int searchOn = 1) 
        { 
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                            default:
                                orderBy = "";
                                break;
                        }
                    }
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                   // var authorizedBranchList = _branchService.LoadAuthorizedBranch(_userMenu).Select(x=>x.Id).ToArray(); 

                    long recordsTotal = _teacherPaymentService.TeacherPaymentListRowCount(_userMenu, teacherName, heldDate, classType, organization, program, session, branch, campus, paymentStatus, tpin, searchOn);
                    List<TeacherPaymentListDto> teacherPaymentListDto = _teacherPaymentService.LoadTeacherPaymentList(start, length, orderBy, orderDir.ToUpper(), _userMenu, teacherName, heldDate, classType, organization, program, session, branch, campus, paymentStatus, tpin, searchOn).ToList();
                    long recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var tpl in teacherPaymentListDto) 
                    {
                        var str = new List<string>();
                        // str.Add(sl.ToString());
                        str.Add(tpl.TeacherTpin.ToString());   
                        str.Add(tpl.Teacher);                        
                        str.Add(tpl.HeldDate != null ? tpl.HeldDate.Value.ToString("dd/MM/yyyy") : "");
                        str.Add(tpl.Branch ?? "Common");
                        if (tpl.ActivityType == "Class")
                        {
                            str.Add(tpl.Campus ?? "Common");
                        }
                        else
                        {
                            str.Add("-");
                        }
                        
                        str.Add(tpl.ActivityType);
                        str.Add(tpl.ClassType);
                        str.Add(tpl.Organization);
                        str.Add(tpl.Program ?? "Common");
                        str.Add(tpl.SessionName ?? "Common");
                        str.Add(tpl.Description);
                        str.Add(tpl.ActivityType == "Materials Development" ? "-" : tpl.Quantity.ToString());

                        str.Add(tpl.Amount.ToString());
                        str.Add(tpl.PaidBy != null ? _userService.GetUserNameByAspNetUserId((long)tpl.PaidBy) : "-");
                        str.Add(CommonHelper.GetReportCommonDateTimeFormat(tpl.PaidDate));
                        string actionString = "";
                        if (tpl.PaymentStatus == TeacherPaymentStatus.Due)
                        {
                            actionString = "<input type='checkbox' id='" + tpl.Id + "' data-organization='"+tpl.OrganizationId+"' data-teacher='"+tpl.TeacherId+"' data-activityType='" + tpl.ActivityType + "' class='payNow' /> Pay Now";
                            //str.Add("Due");
                        }
                        else if (tpl.PaymentStatus == TeacherPaymentStatus.Paid)
                        {
                            actionString = "<span class='glyphicon glyphicon-ok'></span>&nbsp; Paid";
                            //str.Add("Paid");
                        }

                        str.Add(actionString);
                        data.Add(str);
                        sl++;
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return Json(HttpNotFound());
            }
        }

        public ActionResult ExportTeacherPaymentList(string teacherName, string heldDate, string classType, string organization, /*string paymentType*/ string program, string session, string branch, string campus, string paymentStatus, string tpin, int searchOn = 1)
        {
            try
                {
                    var headerList = new List<string>();
                    headerList.Add("Teacher Payment List");
                    var columnList = new List<string>();
                    columnList.Add("TPIN");
                    columnList.Add("Teacher Name");
                    columnList.Add("Held Date");
                    columnList.Add("Held Branch");
                    columnList.Add("Held Campus");
                    columnList.Add("Activity Type");
                    columnList.Add("Class Type");
                    columnList.Add("Organization");
                    columnList.Add("Program");
                    columnList.Add("Session");
                    columnList.Add("Description");
                    columnList.Add("Quantity");
                    columnList.Add("Amount");
                    columnList.Add("Paid By");
                    columnList.Add("Paid Date");
                    columnList.Add("Action");
                    var footerList = new List<string>();
                    footerList.Add("");
                    var paymentExcelList = new List<List<object>>();
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    List<TeacherPaymentListDto> teacherPaymentListDto = _teacherPaymentService.LoadTeacherPaymentList(0, 0, "", "ASC", _userMenu, teacherName, heldDate, classType, organization, program, session,branch,campus,paymentStatus, tpin).ToList();
                    foreach (var tpl in teacherPaymentListDto)
                    {
                        var str = new List<object>();
                        // str.Add(sl.ToString());
                        str.Add(tpl.TeacherTpin.ToString());
                        str.Add(tpl.Teacher);
                        str.Add(tpl.HeldDate != null ? tpl.HeldDate.Value.ToString("dd/MM/yyyy") : "");
                        str.Add(tpl.Branch ?? "Common");
                        if (tpl.ActivityType == "Class")
                        {
                            str.Add(tpl.Campus ?? "Common");
                        }
                        else
                        {
                            str.Add("-");
                        }
                        str.Add(tpl.ActivityType);
                        str.Add(tpl.ClassType);
                        str.Add(tpl.Organization);
                        str.Add(tpl.Program ?? "Common");
                        str.Add(tpl.SessionName ?? "Common");
                        str.Add(tpl.Description);
                        str.Add(tpl.ActivityType == "Materials Development" ? "-" : tpl.Quantity.ToString());
                        str.Add(tpl.Amount.ToString());
                        str.Add(tpl.PaidBy != null ? _userService.GetUserNameByAspNetUserId((long)tpl.PaidBy) : "-");
                        str.Add(CommonHelper.GetReportCommonDateTimeFormat(tpl.PaidDate));
                        string actionString = "";
                        if (tpl.PaymentStatus == TeacherPaymentStatus.Due)
                        {
                            actionString = "Unpaid";
                        }
                        else if (tpl.PaymentStatus == TeacherPaymentStatus.Paid)
                        {
                            actionString = "Paid";
                        }

                        str.Add(actionString);
                        paymentExcelList.Add(str);
                        
                    }
                    ExcelGenerator.GenerateExcel(headerList, columnList, paymentExcelList, footerList, "teacher-payment-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                    return View("AutoClose");
                }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        } 
        
        [HttpPost]
        public ActionResult TeacherPaymentSecond(string teacherPaymentViewModels,long fromPaidBranch = 0,string actionString=null)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var teacherPaymentViewModel =
                        new JavaScriptSerializer().Deserialize<TeacherPaymentViewModel>(teacherPaymentViewModels);
                    if (teacherPaymentViewModel == null)
                        return Json(new Response(false, WebHelper.CommonErrorMessage));
                    if (teacherPaymentViewModel.TeacherId == 0)
                        return Json(new Response(false, WebHelper.CommonErrorMessage));
                    if (teacherPaymentViewModel.OrganizationId == 0)
                        return Json(new Response(false, WebHelper.CommonErrorMessage));
                    var organizationList = new List<long>();
                    organizationList.Add(teacherPaymentViewModel.OrganizationId);
                    ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu,organizationList).OrderBy(x => x.Rank).ToList(),
                        "Id", "Name");

                    if (!teacherPaymentViewModel.TeacherPaymentDetailsViewModel.Any())
                        return Json(new Response(false, WebHelper.CommonErrorMessage));
                    var teacherPaymentListDtoList = new List<TeacherPaymentListDto>();
                    int i = 0;
                    foreach (var teacherPaymentDetails in teacherPaymentViewModel.TeacherPaymentDetailsViewModel)
                    {
                        var teacherPaymentListDto = new TeacherPaymentListDto();
                        if (teacherPaymentDetails.Id == 0)
                            return Json(new Response(false, WebHelper.CommonErrorMessage));
                        if (String.IsNullOrEmpty(teacherPaymentDetails.ActivityType))
                            return Json(new Response(false, WebHelper.CommonErrorMessage));
                        //if(teacherPaymentDetails.ActivityType != "Class" || teacherPaymentDetails.ActivityType != "Script Evaluation" || teacherPaymentDetails.ActivityType != "Materials Development")
                        //    return Json(new Response(false, WebHelper.CommonErrorMessage));


                        if (teacherPaymentDetails.ActivityType == "Class")
                        {
                            var dataDetails = _teacherClassEntryDetailsService.GetTeacherClassEntryDetails(teacherPaymentDetails.Id);
                            if (dataDetails == null)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));

                            if (teacherPaymentViewModel.TeacherId != dataDetails.Teacher.Id)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));

                            if (teacherPaymentViewModel.OrganizationId != dataDetails.TeacherClassEntry.Organization.Id)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));

                            if (dataDetails.PaymentStatus == TeacherPaymentStatus.Paid)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));
                            
                            teacherPaymentListDto.Id = dataDetails.Id;
                            teacherPaymentListDto.Organization = dataDetails.TeacherClassEntry.Organization.ShortName;
                            teacherPaymentListDto.OrganizationId = dataDetails.TeacherClassEntry.Organization.Id;
                            teacherPaymentListDto.ActivityType = teacherPaymentDetails.ActivityType;
                            teacherPaymentListDto.TeacherTpin = dataDetails.Teacher.Tpin;
                            teacherPaymentListDto.Teacher = dataDetails.Teacher.DisplayTeacherName;
                            teacherPaymentListDto.TeacherId = dataDetails.Teacher.Id;
                            teacherPaymentListDto.Description = dataDetails.Description;
                            teacherPaymentListDto.Amount = dataDetails.Amount;
                            teacherPaymentListDto.PaymentStatus = dataDetails.PaymentStatus;
                            teacherPaymentListDto.HeldDate = dataDetails.TeacherClassEntry.HeldDate;
                            teacherPaymentListDto.RowNum = ++i;
                        }
                        else if (teacherPaymentDetails.ActivityType == "Script Evaluation")
                        {
                            var dataDetails = _teacherScriptEvaluationDetailsService.GetTeacherScriptEvaluationDetailById(teacherPaymentDetails.Id);
                            if (dataDetails == null)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));

                            if (teacherPaymentViewModel.TeacherId != dataDetails.Teacher.Id)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));

                            if (teacherPaymentViewModel.OrganizationId !=
                                dataDetails.TeacherScriptEvaluation.Organization.Id)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));
                            if (dataDetails.PaymentStatus == TeacherPaymentStatus.Paid)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));

                            teacherPaymentListDto.Id = dataDetails.Id;
                            teacherPaymentListDto.Organization =
                                dataDetails.TeacherScriptEvaluation.Organization.ShortName;
                            teacherPaymentListDto.OrganizationId = dataDetails.TeacherScriptEvaluation.Organization.Id;
                            teacherPaymentListDto.ActivityType = teacherPaymentDetails.ActivityType;
                            teacherPaymentListDto.TeacherTpin = dataDetails.Teacher.Tpin;
                            teacherPaymentListDto.Teacher = dataDetails.Teacher.DisplayTeacherName;
                            teacherPaymentListDto.TeacherId = dataDetails.Teacher.Id;
                            teacherPaymentListDto.Description = dataDetails.Description;
                            teacherPaymentListDto.Amount = dataDetails.Amount;
                            teacherPaymentListDto.PaymentStatus = dataDetails.PaymentStatus;
                            teacherPaymentListDto.HeldDate = dataDetails.TeacherScriptEvaluation.SubmissionDate;
                            teacherPaymentListDto.RowNum = ++i;
                        }
                        else if (teacherPaymentDetails.ActivityType == "Materials Development")
                        {
                            var dataDetails =_teacherAcademicMaterialDetailsService.GetTeacherAcademicMaterialsDetailById(teacherPaymentDetails.Id);
                            if (dataDetails == null)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));

                            if (teacherPaymentViewModel.TeacherId != dataDetails.Teacher.Id)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));

                            if (teacherPaymentViewModel.OrganizationId !=
                                dataDetails.TeacherAcademicMaterials.Organization.Id)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));
                            if (dataDetails.PaymentStatus == TeacherPaymentStatus.Paid)
                                return Json(new Response(false, WebHelper.CommonErrorMessage));

                            teacherPaymentListDto.Id = dataDetails.Id;
                            teacherPaymentListDto.Organization = dataDetails.TeacherAcademicMaterials.Organization.ShortName;
                            teacherPaymentListDto.OrganizationId = dataDetails.TeacherAcademicMaterials.Organization.Id;
                            teacherPaymentListDto.ActivityType = teacherPaymentDetails.ActivityType;
                            teacherPaymentListDto.TeacherTpin = dataDetails.Teacher.Tpin;
                            teacherPaymentListDto.Teacher = dataDetails.Teacher.DisplayTeacherName;
                            teacherPaymentListDto.TeacherId = dataDetails.Teacher.Id;
                            teacherPaymentListDto.Description = dataDetails.Description;
                            teacherPaymentListDto.Amount = dataDetails.Amount;
                            teacherPaymentListDto.PaymentStatus = dataDetails.PaymentStatus;
                            teacherPaymentListDto.HeldDate = dataDetails.TeacherAcademicMaterials.SubmissionDate;
                            teacherPaymentListDto.RowNum = ++i;
                        }
                        teacherPaymentListDtoList.Add(teacherPaymentListDto);
                    }

                    if (!teacherPaymentListDtoList.Any())
                        return Json(new Response(false, WebHelper.CommonErrorMessage));

                    if (actionString == "finalSubmission")
                    {
                        var paidFromBranch = _branchService.GetBranch(fromPaidBranch);
                        if (paidFromBranch ==null)
                            return Json(new Response(false, WebHelper.CommonErrorMessage));

                        string successMessageFinal = "";
                        string voucherNo = "";
                        _teacherPaymentService.Payment(teacherPaymentListDtoList, paidFromBranch, out successMessageFinal, out voucherNo);
                       // return Json(new Response(true, "Success", successMessageFinal, voucherNo));
                        return Json(new { IsSuccess = true, successMessageFinal = successMessageFinal, voucherNo = voucherNo });
                    }
                    else
                    {
                        ViewBag.TeacherPaymentListDto = teacherPaymentListDtoList;
                        ViewData["teacherPaymentViewModels"] = teacherPaymentViewModels;
                        return PartialView("_TeacherPaymentSecond");
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    //return Json(new Response(false, WebHelper.CommonErrorMessage));
                    return Json(new { IsSuccess = false, successMessageFinal = WebHelper.SetExceptionMessage(ex) });
                }
                
            }
            else
            {
                return Json(HttpNotFound());
            }
        }

        public ActionResult ViewTeacherPayment(string voucherNo = "")
        {
            TeacherPayment teacherPayment = null;
            ViewBag.UserFullName = "-";
            try
            {
                if (!String.IsNullOrEmpty(voucherNo))
                {
                    teacherPayment = _teacherPaymentService.GetTeacherPaymentByVoucherNo(voucherNo);
                    if (teacherPayment != null)
                    {
                        var paymentCreator = _userService.LoadAspNetUserById(teacherPayment.CreateBy);
                        if (paymentCreator != null)
                            ViewBag.UserFullName = paymentCreator.FullName;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return View(teacherPayment);
        }

        #endregion
    }
}