﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DesignationTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Designation")]
    public class DesignationUpdateTest : DesignationBaseTest 
    {
        #region Basic Test
        [Fact]
        public void Designation_Update_Should_Return_True()
        {
            _designationFactory.Create().WithOrganization().WithNameAndRank(TestName, ShortName, TestRank).Persist();
            var oldObj = _designationService.LoadById(_designationFactory.Object.Id);
            oldObj.Name = UpdatingTestName;
            oldObj.ShortName = UpdatingShortName;
            oldObj.Rank = UpdatingTestRank;

            bool isSuccess = _designationService.DesignationSaveUpdate(oldObj);
            Assert.True(isSuccess);
        }
        [Fact]
        public void Designation_Update_Should_Return_Null()
        {
            Assert.Throws<NullObjectException>(() => _designationService.DesignationSaveUpdate(null));
        }
        #endregion

        #region Business Logic Test
        [Fact]
        public void Validation_Error_For_Null_Organization()
        {
            var objFactory = _designationFactory.Create()
                                .WithOrganization()
                                .WithNameAndRank(TestName, ShortName, TestRank).Persist();
            objFactory.Object.Organization = null;
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }

        [Fact]
        public void Validation_Error_For_Null_Name()
        {
            var objFactory = _designationFactory.Create().WithOrganization()
                                .WithNameAndRank(TestName, ShortName, TestRank).Persist();
            objFactory.Object.Name = null;
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }
        [Fact]
        public void Validation_Error_For_Empty_Name()
        {
            var objFactory = _designationFactory.Create().WithOrganization()
                                .WithNameAndRank(TestName, ShortName, TestRank).Persist();
            objFactory.Object.Name = string.Empty;
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }
        [Fact]
        public void Validation_Error_For_Duplicate_Name()
        {
            var objFactory = _designationFactory.CreateMore(2).Persist();
            objFactory.ObjectList[1].Name = objFactory.ObjectList[0].Name;
            Assert.Throws<DuplicateEntryException>(() => _designationService.DesignationSaveUpdate(objFactory.ObjectList[1]));
        }
        [Fact]
        public void Validation_Error_For_ShortName_Null()
        {
            var objFactory = _designationFactory.Create().WithOrganization()
                                .WithNameAndRank(TestName, ShortName, TestRank).Persist();
            objFactory.Object.ShortName = null;
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }
        [Fact]
        public void Validation_Error_For_ShortName_Empty()
        {
            var objFactory = _designationFactory.Create().WithOrganization()
                                .WithNameAndRank(TestName, ShortName, TestRank).Persist();
            objFactory.Object.ShortName = string.Empty;
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }

        [Fact]
        public void Designation_Update_Should_Return_Rank_Invalid()
        {
            var objFactory = _designationFactory.Create()
                                .WithOrganization()
                                .WithNameAndRank(TestName, ShortName, TestRank).Persist();
            objFactory.Object.Rank = 0;
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }

        [Fact]
        public void Validation_Error_For_Rank_Null()
        {
            var objFactory = _designationFactory.Create()
                                 .WithOrganization()
                                 .WithNameAndRank(TestName, ShortName, TestRank).Persist();
            objFactory.Object.Rank = null;
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }
        [Fact]
        public void Designation_Update_Should_Return_Duplicate_Rank_Found()
        {
            var objFactory = _designationFactory.CreateMore(2).Persist();
            objFactory.ObjectList[1].Rank = objFactory.ObjectList[0].Rank;
            Assert.Throws<DuplicateEntryException>(() => _designationService.DesignationSaveUpdate(objFactory.ObjectList[1]));
        }
        #endregion
    }
}
