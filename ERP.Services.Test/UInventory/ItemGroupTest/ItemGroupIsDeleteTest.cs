﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.ItemGroupTest
{
    interface IItemGroupIsDeleteTest 
    {

        #region Basic Test
        void Should_Delete_Successfully();
        void Should_Delete_Return_Null_Exception();
        void Should_Delete_Dependency_Exception();

        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "ItemGroup")]
    public class ItemGroupIsDeleteTest : ItemGroupBaseTest, IItemGroupIsDeleteTest
    {
        [Fact]
        public void Should_Delete_Successfully()
        {
            var itemGroupFactory = _itemGroupFactory.Create().Persist();
            _itemGroupService.IsDelete(itemGroupFactory.Object.Id);
            var status = _itemGroupService.LoadById(itemGroupFactory.Object.Id);
            Assert.Equal(null, status);
        }

        [Fact]
        public void Should_Delete_Return_Null_Exception()
        {
            var itemGroupFactory = _itemGroupFactory.Create().Persist();
            Assert.Throws<NullObjectException>(() => _itemGroupService.IsDelete(itemGroupFactory.Object.Id + 1));
        }

        [Fact]
        public void Should_Delete_Dependency_Exception()
        {
            var itemGroupFactory = _itemGroupFactory.Create().Persist();
            var itemGroup = itemGroupFactory.Object;
            _ItemFactory.Create().WithOrganization().WithItemGroup(itemGroup).Persist();
            Assert.Throws<DependencyException>(() => _itemGroupService.IsDelete(itemGroup.Id));
        }

    }
}
