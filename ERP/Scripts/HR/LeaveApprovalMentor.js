﻿$(document).ready(function () {

    //$(document).on('change', '.form_datetime', function (event) {

    //    var decisionMake = $(this).attr('id');
    //    var df, dt, dff, dtt, name, daysDiff;
    //    if (decisionMake.length <= 0) {
    //        //newly added rows validation goes here   
    //        var allclass = $(event.target).attr('class');
    //        //alert(allclass);
    //        var fVal = $(this).val();
    //        if (fVal.length > 0) {
    //            $(this).removeClass('highlight');
    //            if (allclass.search('fdate') != -1) {
    //                //alert("FDATE");
    //                $(this).siblings().each(function () {
    //                    $('span').remove();
    //                });
    //                df = $(this).val();
    //                dt = $(this).parent().parent().find('.5 input[type=text]').val();
    //                //alert(df + "-->>" + dt);
    //            }
    //            if (allclass.search('tdate') != -1) {
    //                //alert("TDATE");
    //                $(this).siblings().each(function () {
    //                    $('span').remove();
    //                });
    //                dt = $(this).val();
    //                //alert($(this).closest('.4').attr('class'));
    //                //var cc = $(this).parent().attr('class');
    //                df = $(this).parent().parent().find('.4 input[type=text]').val();
    //                //alert(df + "-->>" + dt);
    //            }
    //            if (allclass.search('cause') != -1) {
    //                $(this).siblings().each(function () {
    //                    $('span').remove();
    //                });
    //            }

    //            if (df.length > 0 && dt.length > 0) {
    //                // alert("NOT UNDE");
    //                dtt = new Date(dt);
    //                dff = new Date(df);
    //                //alert(dff + "--->>>" + dtt);
    //                if (dtt < dff) {
    //                    showErrorMessageBelowCtrl($(this), "", false);
    //                    showErrorMessageBelowCtrl($(this), "Invalid Date", true);
    //                } else {
    //                    daysDiff = DateDiff(dff, dtt);
    //                    $(this).parent().parent().find('.6 input[type=text]').val(daysDiff);

    //                }
    //            }
    //        }
    //    } else {
    //        if (decisionMake.search('date2') != -1) {
    //            dt = $(this).val();
    //            df = $(this).parent().parent().find('.4 input[type=text]').val();
    //        } else {
    //            df = $(this).val();
    //            dt = $(this).parent().parent().find('.5 input[type=text]').val();
    //        }
    //        console.log('from-->' + df);
    //        console.log('to-->' + dt);
    //        dtt = new Date(dt);
    //        dff = new Date(df);
    //        name = $(this).attr('id');

    //        showErrorMessageBelowCtrl(name, "", false);
    //        if (dtt < dff) {
    //            showErrorMessageBelowCtrl(name, "", false);
    //            showErrorMessageBelowCtrl(name, "Invalid Date", true);
    //        } else {
    //            daysDiff = DateDiff(dff, dtt);
    //            $(this).parent().parent().find('.6 input[type=text]').val(daysDiff);

    //        }
    //    }
    //});

    $(document).on('click', '#appbtn', function () {
        var divParent = $(this).parent().parent().attr('class');
        var tableId = $('.' + divParent).prev().find("table").attr('id');
        console.log(tableId);
        var toServerAry = [];
        var index = 0;
        var isReqSendToServer = true;
        $('#' + tableId + ' tr').each(function () {
            if (index != 0) {
                if (ValidateLastRow(divParent)) {
                    var leaveAppId = $(this).find('.0 input[type=text]').val();
                    var memPin = $(this).find('.1 input[type=text]').val();
                    var memName = $(this).find('.2 input[type=text]').val();
                    var memLeaveType = $(this).find('.3 option:selected').val();
                    var df = $(this).find('.4 input[type=text]').val();
                    var dt = $(this).find('.5 input[type=text]').val();
                    var reason = $(this).find('.7 input[type=text]').val();
                    var rPin = $(this).find('.8 input[type=text]').val();
                    var rName = $(this).find('.9 input[type=text]').val();

                    var lplObj = new LeavePendingListDto();
                    lplObj.LeaveAppId = leaveAppId;
                    if (lplObj.LeaveAppId <= 0) {
                        lplObj.LeaveAppId = 999999999;
                    }
                    lplObj.Pin = memPin.toNum();
                    lplObj.MemberName = memName;
                    //lplObj.LeaveAppId = memLeaveType;
                    lplObj.DateFrom = df;
                    lplObj.DateTo = dt;
                    lplObj.Reason = reason;
                    lplObj.ResponsiblePin = rPin.toNum();
                    lplObj.RName = rName;
                    lplObj.Day = DateDiff(new Date(lplObj.DateFrom), new Date(lplObj.DateTo));
                    var lvs = new Leaves();
                    lvs.LeaveId = memLeaveType;
                    if (lvs.LeaveId <= 0) {
                        lvs.LeaveId = 999999999;
                    }
                    lplObj.Leaves.push(lvs);
                    toServerAry.push(lplObj);
                } else {
                    isReqSendToServer = false;
                    //return false;
                }
            }
            index++;
        });
        if (isReqSendToServer == true) {
            var clean = remove_duplicates(toServerAry);
            var cleanAry = JSON.stringify(clean);
            //alert(cleanAry);
            var url = "/Hr/LeaveApprovalMentor/AllLeaveApprove";
            $.ajax({
                url: url,
                type: "POST",
                data: { cleanAry: cleanAry },
                //async: false,
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    $.unblockUI();
                    var errMess = "";
                    //index = 0;
                    $.each(data.Err, function (index) {
                        var leave = data.Err[index].Type;
                        var cause = data.Err[index].Message;
                        var pin = data.Err[index].Pin;
                        
                        if (cause != "Confirmed") {
                            errMess += '' + pin + ' possible cause ' + cause + ' leave type ' + leave;
                        }
                        //errMess += '' + pin + ' possible cause ' + cause + ' leave type ' + leave;
                        console.log(errMess);
                        index++;
                    });

                    if (errMess == "") {
                        var rurll = "/Hr/LeaveApprovalMentor/LeaveApprovedByMentor?message=All Leave Approved&success=true";
                        window.location.href = rurll;
                        // showMessage("All Leave Approved", "success");
                    } else {
                        var redurl = "/Hr/LeaveApprovalMentor/LeaveApprovedByMentor?message=" + errMess + "&success=" + false;
                        window.location.href = redurl;
                        //showMessage(errMess, "error");
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }

            });
            console.log(toServerAry);
        }
    });

    //$(document).on('click', '#addbtn', function () {
    //    //find last row
    //    var divParent = $(this).parent().parent().attr('class');
    //    var tableId = $('.' + divParent).prev().find("table").attr('id');
    //    if (ValidateLastRow(divParent)) {
    //        //new dynamic row add here 
    //        var lastRow = $('#' + tableId + ' tr:last');
    //        var content = '<tr>' +
    //            '<td class="1"><input id="" style="width:60px;" class="form-control pinNum" value=""  type="text"></td>' +
    //            '<td class="2"><input id="" style="width:150px;" class="form-control" value="" disabled="disabled" type="text"></td>' +
    //            '<td class="3"><select id="" style="width:200px;" class="form-control" name=""></select></td>' +
    //            '<td class="4"><input style="width:118px;" id="" name="fdate" class="form-control form_datetime fixedwidthmax fdate" value="" readonly="readonly" type="text"></td>' +
    //            '<td class="5"><input style="width:118px;" id="" name="tdate" class="form-control form_datetime fixedwidthmax tdate" value="" readonly="readonly" type="text"></td>' +
    //            '<td class="6"><input style="width:65px;" class="form-control" value="" readonly="readonly" type="text"></td>' +
    //            '<td class="7"><input style="width:300px;" name="cause" id="" class="form-control cause" value="" type="text"></td>' +
    //            '<td class="8"><input style="width:65px;" id="" class="form-control res-mem-pin" value="" type="text"></td>' +
    //            '<td class="9"><input style="width:150px;" id="" class="form-control" value="" readonly="readonly" type="text"> </td>' +
    //            '<td class="10">' +
    //            '<a href="#" data-id="" class="glyphicon glyphicon-remove removeBtn"> </a>' +
    //            '</td>' +
    //            '</tr>';
    //        lastRow.after(content);
    //        console.log(tableId);
    //        console.log(lastRow);
    //    }
    //});

    //$(document).on('click', '.removeBtn', function () {
    //    try {
    //        var ctr = $(this).parent().parent();
    //        $(ctr).remove();
    //    } catch (e) {

    //    }
    //});

    $('#lApproval a').click(function (e) {
        //alert("YES FROM");
        e.preventDefault();
        $(this).tab('show');
        var controls = $(this).attr("aria-controls");
        if (controls == "rla") {
            var url = "/Hr/LeaveApprovalMentor/LeaveHistory";
            $.ajax({
                url: url,
                type: "POST",
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    $.unblockUI();
                    $('#rla').html(data);
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }

            });
        }
    });

    //$(document).on("click", ".approvedbtn, .rejectedbtn", function (event) {
    //    try {
    //        var memPin = $(this).parent().parent().find('.1 input[type=text]').val();
    //        var memName = $(this).parent().parent().find('.2 input[type=text]').val();
    //        var memLeaveType = $(this).parent().parent().find('.3 option:selected').val();
    //        var df = $(this).parent().parent().find('.4 input[type=text]').val();
    //        var dt = $(this).parent().parent().find('.5 input[type=text]').val();
    //        var reason = $(this).parent().parent().find('.7 input[type=text]').val();
    //        var rPin = $(this).parent().parent().find('.8 input[type=text]').val();
    //        var rName = $(this).parent().parent().find('.9 input[type=text]').val();

    //        if (memPin.length <= 0) {
    //            var findIndex = $(this).attr('data-mempin');
    //            var ctid = $(this).parent().parent().find('#' + findIndex).attr('id');
    //            window.showErrorMessageBelowCtrl(ctid, "", false);
    //            window.showErrorMessageBelowCtrl(ctid, "Please Select Pin", true);
    //            return false;
    //        }
    //        if (memName.length <= 0) {
    //            var findIndex1 = $(this).attr('data-mem-name');
    //            var ctid1 = $(this).parent().parent().find('#' + findIndex1).attr('id');
    //            window.showErrorMessageBelowCtrl(ctid1, "", false);
    //            window.showErrorMessageBelowCtrl(ctid1, "Please Select Member", true);
    //            return false;
    //        }
    //        if (memLeaveType.length <= 0) {
    //            var findIndex2 = $(this).attr('data-leavetype-id');
    //            var ctid2 = $(this).parent().parent().find('#' + findIndex2).attr('id');
    //            window.showErrorMessageBelowCtrl(ctid2, "", false);
    //            window.showErrorMessageBelowCtrl(ctid2, "Please Select LeaveType", true);
    //            return false;
    //        }
    //        if (df.length <= 0) {
    //            var findIndex3 = $(this).attr('data-from-date');
    //            var ctid3 = $(this).parent().parent().find('#' + findIndex3).attr('id');
    //            window.showErrorMessageBelowCtrl(ctid3, "", false);
    //            window.showErrorMessageBelowCtrl(ctid3, "Please Select Date", true);
    //            return false;
    //        }
    //        if (dt.length <= 0) {
    //            var findIndex4 = $(this).attr('data-to-date');
    //            var ctid4 = $(this).parent().parent().find('#' + findIndex4).attr('id');
    //            window.showErrorMessageBelowCtrl(ctid4, "", false);
    //            window.showErrorMessageBelowCtrl(ctid4, "Please Select Date", true);
    //            return false;
    //        }
    //        if (reason.length <= 0) {
    //            var findIndex5 = $(this).attr('data-reason');
    //            var ctid5 = $(this).parent().parent().find('#' + findIndex5).attr('id');
    //            window.showErrorMessageBelowCtrl(ctid5, "", false);
    //            window.showErrorMessageBelowCtrl(ctid5, "Please Select Reason", true);
    //            return false;
    //        }
    //        if (rPin.length <= 0) {
    //            var findIndex6 = $(this).attr('data-respin');
    //            var ctid6 = $(this).parent().parent().find('#' + findIndex6).attr('id');
    //            window.showErrorMessageBelowCtrl(ctid6, "", false);
    //            window.showErrorMessageBelowCtrl(ctid6, "Please Select responsible pin", true);
    //            return false;
    //        }
    //        if (rName.length <= 0) {
    //            var findIndex7 = $(this).attr('data-res-name');
    //            var ctid7 = $(this).parent().parent().find('#' + findIndex7).attr('id');
    //            window.showErrorMessageBelowCtrl(ctid7, "", false);
    //            window.showErrorMessageBelowCtrl(ctid7, "Please Select name", true);
    //            return false;
    //        }
            
    //        var leaveappsId = $(this).attr('data-leaveId');
    //        var isFromReject = false;
    //        var allclass = $(event.target).attr('class');
    //        if (allclass.search('approvedbtn') != -1) {
    //            var message = "Are you sure you want to approve this/these Leave(s)?";
    //            bootbox.confirm(message, function (result) {
    //                if (result) {
    //                    var url = "/Hr/LeaveApprovalMentor/LeaveApprove";
    //                    var data = { leaveappsId: leaveappsId, memPin: memPin, memName: memName, memLeaveType: memLeaveType, df: df, dt: dt, reason: reason, rPin: rPin, rName: rName, isFromReject: false };
    //                    $.ajax({
    //                        url: url,
    //                        type: "POST",
    //                        data: data,
    //                        beforeSend: function () {
    //                            $.blockUI({
    //                                timeout: 0,
    //                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //                            });
    //                        },
    //                        success: function (data) {
    //                            $.unblockUI();
    //                            if (data.IsSuccess) {
    //                                window.location = "/Hr/LeaveApprovalMentor/LeaveApprovedByMentor?message=Leave approved successfully&success=true";
    //                            } else {
    //                                showMessage(data.Message, "error");
    //                            }
    //                        },
    //                        complete: function () {
    //                            $.unblockUI();
    //                        },
    //                        error: function (data) {
    //                            $.unblockUI();
    //                            bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
    //                        }
    //                    });
    //                }
    //            }).css({
    //                'color': 'red', 'font-size': '30px', 'margin-top': (($(window).height() / 4))
    //            });
    //        }
    //        if (allclass.search('rejectedbtn') != -1) {
    //            var messages = "Are you sure you want to reject this/these Leave(s)?";
    //            bootbox.confirm(messages, function (result) {
    //                if (result) {
    //                    var url = "/Hr/LeaveApprovalMentor/LeaveApprove";
    //                    var data = { leaveappsId: leaveappsId, memPin: memPin, memName: memName, memLeaveType: memLeaveType, df: df, dt: dt, reason: reason, rPin: rPin, rName: rName, isFromReject: true };
    //                    $.ajax({
    //                        url: url,
    //                        type: "POST",
    //                        data: data,
    //                        beforeSend: function () {
    //                            $.blockUI({
    //                                timeout: 0,
    //                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //                            });
    //                        },
    //                        success: function (data) {
    //                            $.unblockUI();
    //                            if (data.IsSuccess) {
    //                                window.location = "/Hr/LeaveApprovalMentor/LeaveApprovedByMentor?message=Leave rejected successfully&success=true";
    //                            } else {
    //                                showMessage(data.Message, "error");
    //                            }
    //                        },
    //                        complete: function () {
    //                            $.unblockUI();
    //                        },
    //                        error: function (data) {
    //                            $.unblockUI();
    //                            bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
    //                        }
    //                    });
    //                }
    //            }).css({
    //                'color': 'red', 'font-size': '30px', 'margin-top': (($(window).height() / 4))
    //            });
    //        }

    //        //}

    //    } catch (e) {
    //        console.log(e);
    //    }
    //});

});