﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.MaterialTest
{
    public class MaterialBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal MaterialFactory _materialFactory;
        internal IOrganizationService _organizationService;
        internal IProgramService _programService;
        internal ISessionService _sessionService;
        internal IMaterialTypeService _materialTypeService;
        internal IMaterialService _materialService;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        #endregion

        public MaterialBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _materialFactory = new MaterialFactory(null, _session);
            _organizationService=new OrganizationService(Session);
            _programService = new ProgramService(_session);
            _sessionService = new SessionService(_session);
            _materialTypeService=new MaterialTypeService(_session);
            _materialService=new MaterialService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _materialFactory.Cleanup();
            base.Dispose();
        }
    }
}
