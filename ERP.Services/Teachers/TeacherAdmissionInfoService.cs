﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherAdmissionInfoService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<TeacherAdmissionInfo> LoadTeacherAdmissionInfo(long teacherId);
        #endregion

        #region Others Function

        #endregion
    }
    public class TeacherAdmissionInfoService : BaseService, ITeacherAdmissionInfoService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ITeacherAdmissionInfoDao _teacherAdmissionInfoDao;
        public TeacherAdmissionInfoService(ISession session)
        {
            Session = session;
            _teacherAdmissionInfoDao = new TeacherAdmissionInfoDao() { Session = session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<TeacherAdmissionInfo> LoadTeacherAdmissionInfo(long teacherId)
        {
            try
            {
               return _teacherAdmissionInfoDao.LoadTeacherAdmissionInfo(teacherId);
            }
            catch (Exception ex)
            {
               _logger.Error(ex); 
                throw;
            }
        }
        #endregion

        #region Others Function

        #endregion
    }
}
