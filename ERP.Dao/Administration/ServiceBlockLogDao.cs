﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IServiceBlockLogDao : IBaseDao<ServiceBlockLog, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion

        #region Public API

        #endregion
    }

    public class ServiceBlockLogDao : BaseDao<ServiceBlockLog, long>, IServiceBlockLogDao
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion

        #region Public API

        #endregion
    }
}
