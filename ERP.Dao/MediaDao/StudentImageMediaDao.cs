﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using UdvashERP.BusinessModel.Entity.MediaDb;

namespace UdvashERP.Dao.MediaDao
{
    public interface IStudentImageMediaDao
    {

        BusinessModel.Entity.MediaDb.StudentMediaImage GetStudentImageMediaByRefId(long id);

        void DeleteMediaImage(long refId);
    }
    public class StudentImageMediaDao : IStudentImageMediaDao
    {
        private ISession Session = null;
        public StudentImageMediaDao(ISession session)
        {
            Session = session;
        }

        public StudentMediaImage GetStudentImageMediaByRefId(long id)
        {
            return Session.QueryOver<StudentMediaImage>().Where(x => x.ImageRef == id).SingleOrDefault<StudentMediaImage>();
        }

        public void DeleteMediaImage(long refId)
        {
            var studentMediaImage =
                Session.QueryOver<StudentMediaImage>().Where(x => x.ImageRef == refId).SingleOrDefault<StudentMediaImage>();
            Session.Delete(studentMediaImage);
        }
    }
}
