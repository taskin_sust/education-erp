using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students {


    public partial class StudentTransferMap : BaseClassMap<StudentTransfer, long>
    {
        
        public StudentTransferMap() {
			Table("StudentTransfer");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
			
            Map(x => x.TransferDate).Column("TransferDate");
            Map(x => x.Payable).Column("Payable");
            Map(x => x.Receiveable).Column("Receiveable");
            Map(x => x.IsSettled).Column("IsSettled");
            Map(x => x.DueAmount).Column("DueAmount");
            Map(x => x.DueClassExam).Column("DueClassExam");
            References(x => x.StudentProgram).Column("StudentProgramId");
            References(x => x.FromBatch).Column("FromBatchId");
            References(x => x.ToBatch).Column("ToBatchId");
        }
    }
}
