﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.TeamMemberTest
{
    public class TeamMemberBaseTest:TestBase,IDisposable
    {
        internal readonly SalaryHistoryFactory _salaryHistoryFactory;
        internal readonly ISalaryHistoryService _salaryHistoryService;
        internal readonly FestivalBonusSettingFactory _festivalBonusSettingFactory;
        public TeamMemberBaseTest()
        {
            _salaryHistoryFactory = new SalaryHistoryFactory(null,Session);
            _salaryHistoryService = new SalaryHistoryService(Session);
            _festivalBonusSettingFactory = new FestivalBonusSettingFactory(null,Session);
        }

        public void Dispose()
        {
            _salaryHistoryFactory.CleanupTeamMemberSalaryHistoryLog(_salaryHistoryFactory);
            _salaryHistoryFactory.CleanUp();
            leaveApplicationFactory.CleanUp();
            _festivalBonusSettingFactory.FestivalBonusSettingCalculationCleanUp(_festivalBonusSettingFactory);
            _festivalBonusSettingFactory.CleanUp();
            base.Dispose();
        }
    }
}
