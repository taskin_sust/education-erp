﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate;
using Rotativa;
using Rotativa.Options;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Exam;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Exam;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;
using Winnovative;
using ZXing;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Areas.Exam.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Exam")]
    public class QuestionController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("ExamArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly IBatchService _batchService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly ICourseService _courseService;
        private readonly IExamsService _examsService;
        private readonly IOrganizationService _organizationService;
        private readonly IExamsDetailsService _examsDetailsService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _userMenu;

        private readonly ISubjectService _subjectService;
        private readonly IExamMcqQuestionService _examMcqQuestionService;
        public QuestionController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _sessionService = new SessionService(session);
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _batchService = new BatchService(session);
                _courseService = new CourseService(session);
                _examsService = new ExamsService(session);
                _examsDetailsService = new ExamsDetailsService(session);
                _organizationService = new OrganizationService(session);
                _userService = new UserService(session);
                _commonHelper = new CommonHelper();
                _subjectService = new SubjectService(session);
                _examMcqQuestionService = new ExamMcqQuestionService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Manage

        public ActionResult Index(string editSuccess = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(editSuccess))
                {
                    ViewBag.SuccessMessage = editSuccess;
                }
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                organizations.Insert(0, new Organization() { ShortName = "All", Id = 0 });
                ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");
                var examVersionDictonary = _commonHelper.LoadEmumToDictionary<ExamVersion>();
                ViewBag.ExamVersionList = new SelectList(examVersionDictonary, "Key", "Value");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            return View();
        }

        [HttpPost]
        public ActionResult ManageQuestionResult(int draw, int start, int length, string organizationId, string programId, string sessionId, string courseId, string examVersion, string keyword, string uniqueSetNo)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    string selectAll = SelectionType.SelelectAll.ToString();
                    var convertedOrganizationIdList = String.IsNullOrEmpty(organizationId) || organizationId == selectAll ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId));
                    var convertedProgramIdList = String.IsNullOrEmpty(programId) || programId == selectAll ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(programId));
                    var convertedSessionIdList = String.IsNullOrEmpty(sessionId) || sessionId == selectAll ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(sessionId));
                    var convertedCourseIdList = String.IsNullOrEmpty(courseId) || courseId == selectAll ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(courseId));
                    int? examVer = null;
                    if (!string.IsNullOrEmpty(examVersion) && examVersion != selectAll)
                    {
                        examVer = Convert.ToInt32(examVersion);
                    }
                    int recordsTotal = _examMcqQuestionService.GetExamMcqQuestionCount(_userMenu, convertedOrganizationIdList, convertedProgramIdList, convertedSessionIdList, convertedCourseIdList, examVer, keyword, uniqueSetNo);
                    List<ExamMcqQuestion> examMcqQuestionList = _examMcqQuestionService.GetExamMcqQuestionList(start, length, _userMenu, convertedOrganizationIdList, convertedProgramIdList, convertedSessionIdList, convertedCourseIdList, examVer, keyword, uniqueSetNo).ToList();

                    int recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var c in examMcqQuestionList)
                    {
                        var str = new List<string>();
                        string questionVersion = _commonHelper.GetEmumIdToValue<ExamVersion>(c.Version);
                        //str.Add(sl++.ToString());
                        str.Add(c.Exams.Name);
                        str.Add(c.UniqueSet.ToString());
                        str.Add(c.QuestionSerial.ToString());

                        //if (c.Version == (int)ExamVersion.Bangla)
                        //{
                           
                        //    str.Add("Bangla");
                        //}
                        //else if (c.Version == (int)ExamVersion.English)
                        //{
                        //    str.Add("English");
                        //}
                        str.Add(questionVersion);
                        str.Add("<p style='text-align:justify;'>" + c.QuestionText + "</p>");
                        var optionAText = GetOptionHtmlText("A", c.OptionA);
                        var optionBText = GetOptionHtmlText("B", c.OptionB);
                        var optionCText = GetOptionHtmlText("C", c.OptionC);
                        var optionDText = GetOptionHtmlText("D", c.OptionD);
                        var optionEText = GetOptionHtmlText("E", c.OptionE);
                        var optionText = optionAText + optionBText + optionCText + optionDText + optionEText;
                        str.Add(optionText);
                        str.Add(c.CorrectAnswer);
                        var entryBy = _userService.LoadAspNetUserById(c.CreateBy);
                        str.Add(entryBy.Email);
                        str.Add(c.CreationDate.ToString("dd MMM, yyyy")); 
                        str.Add(c.Exams.Program.Organization.ShortName);
                        str.Add(c.Exams.Program.Name);
                        str.Add(c.Exams.Session.Name);
                        string correctAns = "<a title='" + questionVersion + "' href='" + new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext).Action("DisplayQuestion", "Question") + "?examId=" + c.Exams.Id + "&uniqueSet=" + c.UniqueSet + "&version=" + c.Version + "&showCorrectAnswer=1' data-id='" + c.Id + "' class='glyphicon glyphicon-ok' target='_blank'></a>&nbsp;&nbsp;";
                        string pre = "<a title='" + questionVersion + "' href='" + new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext).Action("DisplayQuestion", "Question") + "?examId=" + c.Exams.Id + "&uniqueSet=" + c.UniqueSet + "&version=" + c.Version + "' data-id='" + c.Id + "' class='glyphicon glyphicon-eye-open' target='_blank'></a>&nbsp;&nbsp;";
                        str.Add(pre + correctAns + LinkGenerator.GetGeneratedLink("Edit", "Delete", "Question", c.Id, null));
                        data.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return HttpNotFound();
            }
        }


        #endregion

        #region Create Function
        public ActionResult Create()
        {
            Initialization();
            return View();
        }

        public ActionResult SubmitQuestion(McqQuestionFilterViewModel data)
        {
            try
            {


                if (data == null && !data.McqQuestionList.Any())
                    throw new NullObjectException("Question Exam is null");

                if (!data.McqQuestionList.Any())
                    throw new NullObjectException("Question Information is null");


                if (!ModelState.IsValid)
                    throw new InvalidDataException("Question Information is Invalid");


                List<ExamMcqQuestion> examMcqQuestions = CastViewModelToExamMcqQuestion(data);

                _examMcqQuestionService.SaveOrUpdate(examMcqQuestions);

                return Json(new Response(true, "Question Create Success"));
            }

            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }

        #endregion

        #region Edit

        public ActionResult Edit(long id)
        {
            McqQuestionFilterViewModel mcqQuestionFilterViewModel = new McqQuestionFilterViewModel();
            try
            {
                var examMcqQuestion = _examMcqQuestionService.GetExamMcqQuestion(id);
                var exams = examMcqQuestion.Exams;
                var examsProgram = exams.Program;
                var organization = examsProgram.Organization;
                var examsSession = exams.Session;
                var examsCourse = exams.Course;
                var subject = examMcqQuestion.Subject;
                mcqQuestionFilterViewModel.Organization = organization.Id;
                mcqQuestionFilterViewModel.Program = examsProgram.Id;
                mcqQuestionFilterViewModel.Session = examsSession.Id;
                mcqQuestionFilterViewModel.Course = examsCourse.Id;
                mcqQuestionFilterViewModel.ExamId = exams.Id;
                mcqQuestionFilterViewModel.SubjectId = subject.Id;
                ViewBag.OrganizationName = organization.ShortName;
                ViewBag.ProgramName = examsProgram.Name;
                ViewBag.SessionName = examsSession.Name;
                ViewBag.CourseName = examsCourse.Name;
                mcqQuestionFilterViewModel.ExamCode = ViewBag.ExamCode = exams.Code;
                mcqQuestionFilterViewModel.ExamName = ViewBag.ExamName = exams.Name;
                ViewBag.SubjectName = subject.Name;
                mcqQuestionFilterViewModel.UniqueSet = ViewBag.UniqueSet = examMcqQuestion.UniqueSet;
                ViewBag.QuestionSerial = examMcqQuestion.QuestionSerial;
                var orgList = new List<SelectListItem> { new SelectListItem { Value = organization.Id.ToString(), Text = organization.ShortName } };
                ViewBag.OrganizationList = new SelectList(orgList, "Value", "Text", organization.Id);
                var programList = new List<SelectListItem> { new SelectListItem { Value = examsProgram.Id.ToString(), Text = examsProgram.Name } };
                ViewBag.ProgramList = new SelectList(programList, "Value", "Text", examsProgram.Id);
                var sessionList = new List<SelectListItem> { new SelectListItem { Value = examsSession.Id.ToString(), Text = examsSession.Name } };
                ViewBag.SessionList = new SelectList(sessionList, "Value", "Text", examsSession.Id);
                var courseList = new List<SelectListItem> { new SelectListItem { Value = examsCourse.Id.ToString(), Text = examsCourse.Name } };
                ViewBag.CourseList = new SelectList(courseList, "Value", "Text", examsCourse.Id);
                var subjectList = new List<SelectListItem> { new SelectListItem { Value = subject.Id.ToString(), Text = subject.Name } };
                ViewBag.SubjectList = new SelectList(subjectList, "Value", "Text", subject.Id);
                var uniqueSetList = new List<SelectListItem> { new SelectListItem { Value = examMcqQuestion.UniqueSet.ToString(), Text = examMcqQuestion.UniqueSet.ToString() } };
                ViewBag.UniqueList = new SelectList(uniqueSetList, "Value", "Text", examMcqQuestion.UniqueSet.ToString());
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View(mcqQuestionFilterViewModel);
        }

        public ActionResult UpdateQuestion(McqQuestionFilterViewModel data)
        {
            try
            {
                if (data == null && !data.McqQuestionList.Any())
                    throw new NullObjectException("Question Exam is null");

                if (!data.McqQuestionList.Any())
                    throw new NullObjectException("Question Information is null");


                if (!ModelState.IsValid)
                    throw new InvalidDataException("Question Information is Invalid");

                List<ExamMcqQuestion> examMcqQuestions = CastViewModelToExamMcqQuestion(data);

                _examMcqQuestionService.SaveOrUpdate(examMcqQuestions);

                return Json(new Response(true, "Question Create Success"));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }


        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isSuccess = _examMcqQuestionService.Delete(id);
                return Json(isSuccess ? new Response(true, "Question sucessfully deleted.") : new Response(false, WebHelper.CommonErrorMessage));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return Json(new Response(false, "Question Delete Fail !"));
            }
        }

        #endregion

        #region Display

        [AllowAnonymous]
        public ActionResult DisplayQuestion(long examId, int uniqueSet, int version, double textFontSize = 5, double eqFontSize = 5, int targetPage = 2, int showCorrectAnswer = 0)
        {
            ViewBag.textFontSize = textFontSize + "mm";
            ViewBag.textFontSizeInt = textFontSize;
            ViewBag.eqFontSize = eqFontSize + "mm";
            ViewBag.eqFontSizeInt = eqFontSize;
            ViewBag.targetPage = targetPage;
            ViewBag.examId = examId;
            ViewBag.setId = 0;
            ViewBag.version = version;
            ViewBag.uniqueSet = uniqueSet;
            ViewBag.ShowCorrectAnswer = showCorrectAnswer;

            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var questions = _examMcqQuestionService.LoadExamMcqQuestion(_userMenu, examId, uniqueSet, version);

                var questionList = questions.Select(q => new Question()
                {
                    Version = q.Version, 
                    QuestionText = q.QuestionText, 
                    OptionA = q.OptionA, 
                    OptionB = q.OptionB, 
                    OptionC = q.OptionC, 
                    OptionD = q.OptionD, 
                    OptionE = q.OptionE,
                    CorrectAnswer = q.CorrectAnswer
                }).ToList();

                var exam = _examsService.GetExamById(examId);

                var examMcqQuestionSetDto = new ExamMcqQuestionSetPdfDto
                {
                    ExamName = exam.Name,
                    Program = exam.Program.Name,
                    Session = exam.Session.Name,
                    Fullmarks = exam.McqFullMarks.ToString(),
                    DurationHour = exam.DurationHour,
                    DurationMinute = exam.DurationMinute,
                    QuestionList = questionList
                };

                var content = "ExamId-" + examId + "\nVersion-" + version + "\nUniqueset-" + uniqueSet;
                ViewBag.Barcode = "data:image/gif;base64," + _commonHelper.GetBarCodeBase64Data(content);


                ViewBag.ExamMcqQuestionSetPdfDto = examMcqQuestionSetDto;
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            if (showCorrectAnswer == 1)
                return View("~/Views/Shared/_CorrectAnswerQuestionPdf.cshtml");

            return View("~/Views/Shared/_QuestionPdf.cshtml");
         //   return View("DownloadQuestionNewTest");
        }
        
        public ActionResult PDFFooter()
        {
            return View();
        }

        public ActionResult DownloadQuestion(long examId, int uniqueSet, int version)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var questions = _examMcqQuestionService.LoadExamMcqQuestion(_userMenu, examId, uniqueSet, version);
                var exam = _examsService.GetExamById(examId);

                ViewBag.Questions = questions;
                ViewBag.Exam = exam;

               // string link = "http://localhost:4412/" + Url.Action("PDFFooter", "Question");
                //string cusomtSwitches = string.Format("--no-stop-slow-scripts --javascript-delay 60000 --disable-smart-shrinking --encoding utf-8");
                //return new Rotativa.ViewAsPdf("DownloadQuestionNewTest")
                //{
                //    FileName = "Question.pdf",
                //    PageSize = Size.A4,
                //    CustomSwitches = cusomtSwitches,
                //    PageMargins = { Top = 0, Bottom = 0, Left = 6, Right = 6 }
                //};


                #region Save as Pdf/don't delete this
                //var viewResult = new ViewAsPdf("DownloadQuestion")
                //{
                //    FileName = "Question.pdf",
                //    PageSize = Size.A4,
                //    CustomSwitches = cusomtSwitches,
                //    PageMargins = { Top = 0, Bottom = 0, Left = 6, Right = 6 }
                //};

                //var byteArray = actionResult.BuildPdf(ControllerContext);
                //var fileStream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Uploads\Question\ExamId\Version\\qq.pdf",
                //    FileMode.Create, FileAccess.Write);
                //fileStream.Write(byteArray, 0, byteArray.Length);
                //fileStream.Close();
                #endregion


            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return RedirectToAction("Index", "Question");
        }

        [AllowAnonymous]
        public ActionResult ConvertHtmlToPdf(long examId, int uniqueSet, int version)
        {

            //_userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //var questions = _examMcqQuestionService.LoadExamMcqQuestion(_userMenu, examId, uniqueSet, version);
            //var exam = _examsService.GetExamById(examId);

            //ViewBag.Questions = questions;
            //ViewBag.Exam = exam;


            // Create a HTML to PDF converter object with default settings
            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();

            // Set HTML Viewer width in pixels which is the equivalent in converter of the browser window width
            htmlToPdfConverter.HtmlViewerWidth = 800;

            // Set HTML viewer height in pixels to convert the top part of a HTML page 
            // Leave it not set to convert the entire HTML
            //if (collection["htmlViewerHeightTextBox"].Length > 0)
            //    htmlToPdfConverter.HtmlViewerHeight = int.Parse(collection["htmlViewerHeightTextBox"]);

            // Set PDF page size which can be a predefined size like A4 or a custom size in points 
            // Leave it not set to have a default A4 PDF page
            //htmlToPdfConverter.PdfDocumentOptions.PdfPageSize = SelectedPdfPageSize(collection["pdfPageSizeDropDownList"]);

            // Set PDF page orientation to Portrait or Landscape
            // Leave it not set to have a default Portrait orientation for PDF page
            //htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = SelectedPdfPageOrientation(collection["pdfPageOrientationDropDownList"]);

            // Set the maximum time in seconds to wait for HTML page to be loaded 
            // Leave it not set for a default 60 seconds maximum wait time
            htmlToPdfConverter.NavigationTimeout = 60;

            // Set an adddional delay in seconds to wait for JavaScript or AJAX calls after page load completed
            // Set this property to 0 if you don't need to wait for such asynchcronous operations to finish
            //if (collection["conversionDelayTextBox"].Length > 0)
            //    htmlToPdfConverter.ConversionDelay = int.Parse(collection["conversionDelayTextBox"]);

            string url = "/";
            //DisplayQuestion(long examId, int uniqueSet, int version
            // Convert the HTML page given by an URL to a PDF document in a memory buffer
            byte[] outPdfBuffer = htmlToPdfConverter.ConvertUrl(string.Format("http://erp.osl.team/Exam/Question/DisplayQuestion?examId={0}&uniqueSet={1}&version={2}", examId, uniqueSet, version));

            // Send the PDF file to browser
            FileResult fileResult = new FileContentResult(outPdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "Getting_Started.pdf";

            return fileResult;
        }

        #endregion

        #region Helper Function

        private string GetOptionHtmlText(string option, string html)
        {
            if (string.IsNullOrEmpty(html))
                return "";
            //return "<p style='text-align:left;'>" + option + ". " + "</p>";
            return "<p style='text-align:left;'>" + option + ". " + HttpUtility.HtmlDecode(
                Regex.Replace(html, "<(.|\n)*?>", "")) + "</p>";
        }

        private void Initialization()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
            ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.CourseList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.SubjectList = new SelectList(string.Empty, "Value", "Text");

            ViewBag.UniqueList = new SelectList(string.Empty, "Value", "Text");
        }

        private List<ExamMcqQuestion> CastViewModelToExamMcqQuestion(McqQuestionFilterViewModel data)
        {
            var examMcqQuestionList = new List<ExamMcqQuestion>();

            if (data.McqQuestionList.Any())
            {
                var subject = _subjectService.GetSubject(data.SubjectId);
                var exam = _examsService.GetExamById(data.ExamId);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var lastQuestionSerial = _examMcqQuestionService.GetLastQuestionSerial(_userMenu, data.ExamId, data.UniqueSet);
                foreach (var d in data.McqQuestionList)
                {
                    var examMcqQuestion = new ExamMcqQuestion
                    {
                        Id = d.Version == (int)ExamVersion.Bangla ? data.BanglaQuestionId : data.EnglishQuestionId,
                        Subject = subject,
                        Exams = exam,
                        UniqueSet = data.UniqueSet,
                        Version = d.Version,
                        QuestionSerial = lastQuestionSerial + 1,
                        QuestionText = d.QuestionText,
                        OptionA = d.OptionA,
                        OptionB = d.OptionB,
                        OptionC = d.OptionC,
                        OptionD = d.OptionD,
                        OptionE = d.OptionE,
                        CorrectAnswer = d.CorrectAnswer,
                        IsShuffle = d.IsShuffle
                    };
                    examMcqQuestionList.Add(examMcqQuestion);
                }
            }
            return examMcqQuestionList;
        }

        #endregion

        #region Ajax Request Function
        [HttpPost]
        public ActionResult AjaxRequestForExamCode(string query, string programId, string sessionId, string courseId, bool isQuestion = false)
        {
            //bool? examTypeValue = (isMcq != "") ? Convert.ToBoolean(isMcq) : (bool?)null;
            try
            {
                if (!String.IsNullOrEmpty(programId) && !String.IsNullOrEmpty(sessionId) &&
                    !String.IsNullOrEmpty(courseId))
                {
                    programId = programId.Trim();
                    sessionId = sessionId.Trim();
                    courseId = courseId.Trim();
                }
                else
                {
                    programId = "-1";
                    sessionId = "-1";
                    courseId = "-1";
                }
                SelectList examsList = new SelectList(_examsService.GetExamByProgramSessionCourseAutoComplete(
                    query, Convert.ToInt64(programId), Convert.ToInt64(sessionId), Convert.ToInt64(courseId), true, isQuestion), "Id", "Code");
                List<SelectListItem> selectExamList = examsList.ToList();
                return Json(new { returnList = selectExamList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult AjaxRequestForExamInformation(long id)
        {
            try
            {
                Exams exam = _examsService.GetExamById(id);

                var examinfo = new Dictionary<string, string>();

                if (exam != null)
                {
                    examinfo.Add("Name", exam.Name);

                    examinfo.Add("TotalUniqueSet", exam.TotalUniqueSet.ToString());
                    //return Json(new { returnList = examinfo, IsSuccess = true });

                    var examDetails = exam.ExamDetails;
                    var firstExamDetails = examDetails.FirstOrDefault();

                    var subjectList = new List<Subject>();
                    if (firstExamDetails != null && firstExamDetails.SubjectType == SubjectType.Combined)
                    {
                        subjectList.AddRange(exam.Course.CourseSubjects.Select(cs => cs.Subject));
                    }
                    else
                    {
                        subjectList.AddRange(examDetails.Where(x => x.ExamType == ExamType.Mcq).Select(ed => ed.Subject));
                    }

                    var subjects = new SelectList(subjectList.ToList(), "Id", "Name");
                    return Json(new { returnList = examinfo, subjectList = subjects, IsSuccess = true });
                }
                else
                {
                    return Json(new { returnList = examinfo, IsSuccess = false });
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public ActionResult AjaxRequestForDrawingQuestionEntryDiv(long id, int uniqueset)
        {
            try
            {
                Exams exam = _examsService.GetExamById(id);

                if (exam != null && exam.IsMcq)
                {
                    ViewBag.Exam = exam;
                    ViewBag.UniqueSet = uniqueset;
                    return PartialView("_QuestionEntryDiv");
                }
                else
                {
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public ActionResult AjaxRequestForDrawingQuestionEditDiv(long examId, int uniqueSet, long subjectId, int questionSerial)
        {
            try
            {
                Exams exam = _examsService.GetExamById(examId);
                List<ExamMcqQuestion> examMcqQuestionList = _examMcqQuestionService.GetExamMcqQuestion(examId, uniqueSet,
                subjectId, questionSerial);
                if (exam != null && examMcqQuestionList != null && examMcqQuestionList.Any())
                {
                    ViewBag.Exam = exam;
                    ViewBag.UniqueSet = uniqueSet;
                    ViewBag.SubjectId = subjectId;
                    ViewBag.QuestionSerial = questionSerial;
                    ViewBag.CorrectAnswer = examMcqQuestionList[0].CorrectAnswer;
                    ViewBag.IsShuffled = examMcqQuestionList[0].IsShuffle;
                    return PartialView("_QuestionEditPartialView");
                }
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Ckeditor Image Server
        public ActionResult UploadPartial()
        {
            var appData = Server.MapPath("~/Uploads/CkEditor_Images");
            var images = Directory.GetFiles(appData).Select(x => new CkEditorImagesViewModel
            {
                Url = Url.Content("/Uploads/CkEditor_Images/" + Path.GetFileName(x))
            });
            return View(images);
        }


        public void UploadNow(HttpPostedFileWrapper upload)
        {
            if (upload != null)
            {
                string ImageName = upload.FileName;
                string path = System.IO.Path.Combine(Server.MapPath("~/Uploads/CkEditor_Images"), ImageName);
                upload.SaveAs(path);
            }
        }
        #endregion
    }
}