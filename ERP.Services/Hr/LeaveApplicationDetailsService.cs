﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public
        interface ITeamMemberLeaveDetailsService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class TeamMemberLeaveDetailsService : BaseService, ITeamMemberLeaveDetailsService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly ILeaveApplicationDetailsDao _teamMemberLeaveDetailDao;
        public TeamMemberLeaveDetailsService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _teamMemberLeaveDetailDao = new LeaveApplicationDetailsDao() { Session = session };
        }
        #endregion

        #region Operational Functions


        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}
