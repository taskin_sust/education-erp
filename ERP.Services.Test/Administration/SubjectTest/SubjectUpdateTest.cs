﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SubjectTest
{
    public interface ISubjectUpdateTest
    {
        #region basic test
        [Fact]
        void Update_Should_Return_Null_Exception();

        [Fact]
        void Should_Update_Successfully();

        [Fact]
        void Should_Update_Rank_Successfully();
        
        [Fact]
        void Should_Not_Update_Empty_Field(int index);
        
        #endregion

        #region business logic test
        [Fact]
        void Update_Should_Check_Duplicate_Name_Entry();

        [Fact]
        void Update_Should_Check_Duplicate_ShortName_Entry();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Subject")]
    public class SubjectUpdateTest : SubjectBaseTest, ISubjectUpdateTest
    {
        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var subjectFactory =
                 _subjectFactory.Create().WithNameAndRank(_name, 1).WithProperties(_shortName)
                     .Persist();
            Assert.Throws<NullObjectException>(() => _subjectService.SubjectSaveOrUpdate(null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var subjectFactory =
                _subjectFactory.Create().WithNameAndRank(_name, 1).WithProperties(_shortName)
                    .Persist();
            Subject subjectObj = _subjectService.GetSubject(subjectFactory.Object.Id);
            subjectObj.Name = subjectObj.Name + " DEF GHI";
            subjectObj.ShortName = subjectObj.ShortName + " FGH";
            var success = _subjectService.SubjectSaveOrUpdate(subjectObj);
            Assert.True(success);
        }

        [Fact]
        public void Should_Update_Rank_Successfully()
        {
            var subjectFactory = _subjectFactory.CreateMore(2).Persist();
            var subject1 = subjectFactory.GetLastCreatedObjectList()[0];
            var subject2 = subjectFactory.GetLastCreatedObjectList()[1];
            var tempRank = subject1.Rank;
            subject1.Rank = subject2.Rank;
            subject2.Rank = tempRank;
            _subjectService.UpdateRank(subject1, subject2);
            Assert.True(subject1.Rank>subject2.Rank);
        }
        
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void Should_Not_Update_Empty_Field(int index)
        {
            _subjectFactory.Create().WithNameAndRank(_name, 1).WithProperties(_shortName)
                     .Persist();
            Subject subject = _subjectFactory.Object;
            if (index == 1)
            {

                subject.Name = "";
            }
            else if (index == 2)
            {
                subject.ShortName = "";
            }
            Assert.Throws<InvalidDataException>(() => _subjectService.SubjectSaveOrUpdate(subject));
        }

        #endregion

        #region Business Logic Test
        [Fact]
        public void Update_Should_Check_Duplicate_Name_Entry()
        {
            var subjectFactory = _subjectFactory.CreateMore(2).Persist();
            var subject1 = subjectFactory.GetLastCreatedObjectList()[0];
            var subject2 = subjectFactory.GetLastCreatedObjectList()[1];
            subject2.Name = subject1.Name;
            Assert.Throws<DuplicateEntryException>(() => _subjectService.SubjectSaveOrUpdate(subject2));
        }

        [Fact]
        public void Update_Should_Check_Duplicate_ShortName_Entry()
        {
            var subjectFactory = _subjectFactory.CreateMore(2).Persist();
            var subject1 = subjectFactory.GetLastCreatedObjectList()[0];
            var subject2 = subjectFactory.GetLastCreatedObjectList()[1];
            subject2.ShortName = subject1.ShortName;
            Assert.Throws<DuplicateEntryException>(() => _subjectService.SubjectSaveOrUpdate(subject2));
        }
        #endregion
    }
}
