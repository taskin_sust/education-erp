﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DesignationTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Designation")]
    public class DesignationLoadTest : DesignationBaseTest
    {
        [Fact]
        public void Designation_LoadById_Should_Return_Null()
        {
            const long searchId = -1;
            Assert.Throws<NullObjectException>(() => _designationService.LoadById(searchId));
        }
        [Fact]
        public void Designation_LoadById_Should_Return_Invalid()
        {
            Assert.Throws<NullObjectException>(() => _designationService.LoadById(0));
        }
        [Fact]
        public void Designation_HasDuplicateByName_Should_Return_False()
        {
            var objFactory = _designationFactory.Create().WithOrganization().WithNameAndRank(TestName, ShortName, TestRank).Persist();
            Assert.False(_designationService.HasDuplicateByDesignationName(objFactory.Object.Name, objFactory.Object.Organization.Id, null));
        }
        [Fact]
        public void Designation_HasDuplicateByName_WithId_Should_Return_False()
        {
            var objFactory = _designationFactory.Create().WithOrganization().WithNameAndRank(TestName, ShortName, TestRank).Persist();
            Assert.False(_designationService.HasDuplicateByDesignationName
                                    (   objFactory.Object.Name
                                        , objFactory.Object.Organization.Id
                                        , objFactory.Object.Id + 1
                                    )
                        );
        }
        [Fact]
        public void Designation_HasDuplicateByName_WithId_Should_Return_True()
        {
            var objFactory = _designationFactory.Create()
                                .WithOrganization()
                                .WithNameAndRank(TestName, ShortName, TestRank).Persist();
            objFactory.Object.Name = UpdatingTestName;
            Assert.True(_designationService.HasDuplicateByDesignationName(objFactory.Object.Name, objFactory.Object.Organization.Id, objFactory.Object.Id));
        }

        [Fact]
        public void Designation_Load_Test()
        {
            const int count = 10;
            var factory = _designationFactory.CreateMore(count).Persist();
            var userMenu = BuildUserMenu(_commonHelper.ConvertIdToList(factory.ObjectList[0].Organization));
            
            const string orderBy = "Organization.ShortName";
            var list = _designationService.LoadActive(userMenu, 0, 10, orderBy, "", "", "");
            Assert.Equal(list.Count, count);
        }
    }
}
