﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text.pdf.qrcode;
using log4net;
using Microsoft.Ajax.Utilities;
using NHibernate.Mapping;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.UserAuth;
using ReportType = UdvashERP.BusinessRules.ReportType;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class DirectGoodsIssueController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private GoodsIssueService _goodsIssueService;
        private readonly ICommonHelper _commonHelper;
        private readonly IItemGroupService _itemGroupService;
        private readonly ItemService _itemService;
        private List<UserMenu> _userMenu;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly IBranchService _branchService;
        public readonly IProgramSessionItemService _itemProgramSessionService;
        private readonly ISessionService _sessionService;
        private readonly IGoodsIssueDetailsService _goodsIssueDetailsService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private AuthorizeBranchNameDelegate branchNameDelegate;

        public DirectGoodsIssueController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _itemGroupService = new ItemGroupService(session);
            _itemService = new ItemService(session);
            _organizationService = new OrganizationService(session);
            _programService = new ProgramService(session);
            _itemProgramSessionService = new ProgramSessionItemService(session);

            _branchService = new BranchService(session);
            _sessionService = new SessionService(session);
            _goodsIssueService = new GoodsIssueService(session);
            _goodsIssueDetailsService = new GoodsIssueDetailsService(session);
            _programBranchSessionService = new ProgramBranchSessionService(session);
            branchNameDelegate = new AuthorizeBranchNameDelegate(OnGettingAuthorizeBranchNameList);

        }

        #endregion

        #region Operational functions

        #region create
        public ActionResult DirectGoodsIssues(string message = "", string type = "")
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(message))
                {
                    if (type.Equals("s"))
                    {
                        ViewBag.SuccessMessage = message;
                    }
                    else if (type.Equals("e"))
                    {
                        ViewBag.ErrorMessage = message;
                    }
                }
                InitializeView(new DirectGoodsIssueViewModel());
                return View("DirectGoodsIssues", new DirectGoodsIssueViewModel());
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        [HttpPost]
        public ActionResult DirectGoodsIssue(DirectGoodsIssueViewModel goodsIssueViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    InitializeView(goodsIssueViewModel);
                }

                var goodsDetailsList = new List<GoodsIssueDetails>();
                
                foreach (var row in goodsIssueViewModel.DirectGoodsIssueDetails)
                {
                    string[] psIds = row.ProgramSession.Split(new string[] { "::" }, StringSplitOptions.None);
                    Program program = null;
                    Session session = null;
                    if (!String.IsNullOrEmpty(row.ProgramSession))
                    {
                        program = _programService.GetProgram(Convert.ToInt64(psIds[0]));
                        session = _sessionService.LoadById(Convert.ToInt64(psIds[1]));
                    }
                    var item = _itemService.LoadById(Convert.ToInt64(row.ItemId));
                    int purpose = Convert.ToInt32(row.Purpose);
                    var itemDetailsModel = new GoodsIssueDetails()
                    {
                        Session = session,
                        Program = program,
                        PurposeSessionId = session,
                        PurposeProgramId = program,
                        Item = item,
                        Purpose = purpose,
                        IssuedQuantity = row.IssuedQuantity
                    };
                    goodsDetailsList.Add(itemDetailsModel);
                }
                var branch = _branchService.GetBranch(goodsIssueViewModel.BranchId);
                var model = new GoodsIssue()
                {
                    GoodsIssueNo = GetGoodsIssueNo(branch),
                    Branch = branch,
                    Remarks = goodsIssueViewModel.Remarks,
                    GoodsIssueDetails = goodsDetailsList
                };
                bool isSuccess = _goodsIssueService.SaveOrUpdate(model);
                if (isSuccess)
                {
                    return RedirectToAction("Details", new { id = model.Id, message = "Goods Issued save sucessfully." });
                }
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            InitializeView(goodsIssueViewModel);
            return View(goodsIssueViewModel);
        }

        private void InitializeView(DirectGoodsIssueViewModel goodsIssueViewModel)
        {
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization((List<UserMenu>)ViewBag.UserMenu), "Id", "ShortName");
            ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
            if (goodsIssueViewModel.DirectGoodsIssueDetails != null && goodsIssueViewModel.DirectGoodsIssueDetails.Count > 0)
            {
                ViewBagForListOfItemDetailsRow(goodsIssueViewModel);
            }
            else
            {
                goodsIssueViewModel.DirectGoodsIssueDetails = null;
                ViewBagForItemDetailsRow(null, null);
            }
        }

        private void ViewBagForListOfItemDetailsRow(DirectGoodsIssueViewModel goodsIssueViewModel)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var purposeList = _commonHelper.LoadEmumToDictionary<Purpose>().ToList();
            ViewBag.purposeList = new SelectList(purposeList.OrderBy(x => x.Key), "Key", "Value");
            ViewBag.ItemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
            ViewBag.ProgramSessionIdlist = new SelectList(string.Empty, "Value", "Text");

            long organizationId = goodsIssueViewModel.OrganizationId;
            var organizationIds = new List<long>() { organizationId };
            var branchList = _branchService.LoadBranch(organizationIds);
            ViewBag.BranchList = new SelectList(branchList, "Id", "Name", goodsIssueViewModel.BranchId);

            IList<SelectList> itemNameListArray = new List<SelectList>();
            IList<SelectList> itemProgramSessionListArray = new List<SelectList>();
            foreach (var row in goodsIssueViewModel.DirectGoodsIssueDetails)
            {
                long itemGroupId = row.ItemGroupId;
                long itemId = row.ItemId;

                var itemList = _itemService.LoadItem(Convert.ToInt64(organizationId), Convert.ToInt64(itemGroupId));
                var itemSelectList = new SelectList(itemList.ToList(), "Id", "Name");
                itemNameListArray.Add(itemSelectList);

                var itemProgramSessionList = _programBranchSessionService.LoadProgramSession(_userMenu, organizationId, branchList.Select(x => x.Id).ToList(), new List<long>() { itemId });
                    
                    //_itemProgramSessionService.LoadProgramSessionItemDtoByItemId(new long[] { itemId });
                var list = new SelectList(itemProgramSessionList, "ProgramAndSessionId", "ProgramAndSessionName");
                //if (row.Purpose > 3)
                //{
                //    var pList = new SelectListItem() { Text = "N/A", Value = "0" };
                //    List<SelectListItem> newList = list.ToList();
                //    newList.Insert(0, pList);
                //    list = new SelectList(newList, "Value", "Text", row.Purpose);
                //}
                itemProgramSessionListArray.Add(list);
            }
            ViewBag.ItemNameListArray = itemNameListArray;
            ViewBag.ItemProgramSessionListArray = itemProgramSessionListArray;
        }

        [HttpPost]
        public ActionResult AddGoodsIssueDetailsRow(int index, long? organizationId, long? branchId)
        {
            try
            {
                ViewBagForItemDetailsRow(organizationId, branchId);
                ViewBag.Index = index + 1;
                return PartialView("Partial/_AddGoodsIssueDetails");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return RedirectToAction("DirectGoodsIssue");
            }
        }

        private void ViewBagForItemDetailsRow(long? organizationId, long? branchId)
        {
            var purposeList = _commonHelper.LoadEmumToDictionary<Purpose>().ToList();
            //purposeList.Add(new KeyValuePair<int, string>(0, "N/A"));
            ViewBag.purposeList = new SelectList(purposeList.OrderBy(x => x.Key), "Key", "Value");
            ViewBag.ItemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
            ViewBag.programSessionList = new SelectList(String.Empty, "Value", "Text");

        }

        private string GetGoodsIssueNo(Branch branch)
        {
            string name = "";
            try
            {
                var organizationName = branch.Organization.ShortName.Substring(0, 3);
                var branchCode = branch.Code.ToString(CultureInfo.CurrentCulture);

                var lastGoodsIssueNo = _goodsIssueService.GetGoodsIssueNo(branch.Id);

                string goodsIssueNo = "000001";

                if (lastGoodsIssueNo != "")
                {
                    string newSl = (Convert.ToInt32(lastGoodsIssueNo.Substring(lastGoodsIssueNo.Length - 6, 6)) + 1).ToString(CultureInfo.CurrentCulture);
                    int lengthDiff = 6 - newSl.Length;
                    for (int i = 0; i < lengthDiff; i++)
                    {
                        newSl = "0" + newSl;
                    }
                    goodsIssueNo = newSl;
                }
                name = organizationName + branchCode + "GI" + goodsIssueNo;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return name;
        }

        #endregion

        #region Details

        [HttpGet]
        public ActionResult Details(long id, string message = "")
        {
            try
            {
                if (id <= 0)
                {
                    return HttpNotFound();
                }
                var goodsIssueObj = _goodsIssueService.LoadById(id);
                if (goodsIssueObj == null)
                    return RedirectToAction("Index", new { message = "Goods Issue Recoad Not Found! Please try for another!", type = "i" });
                //ViewBag.GoodsIssue = goodsIssueObj;
                var viewModel = InitialGoodsIssueDetailsViewModel(goodsIssueObj);
                if (message != null && !string.IsNullOrEmpty(message))
                {
                    ViewBag.SuccessMessage = message;
                }

                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("Index", new { message = WebHelper.CommonErrorMessage, type = "e" });
            }
        }

        private GoodsIssueDto InitialGoodsIssueDetailsViewModel(GoodsIssue goodsIssueObj)
        {
            var model = new GoodsIssueDto();
            model.Organization = goodsIssueObj.Branch.Organization.ShortName;
            model.OrganizationFullName = goodsIssueObj.Branch.Organization.Name;
            model.Branch = goodsIssueObj.Branch.Name;
            model.Remarks = goodsIssueObj.Remarks;
            model.GoodsIssueNo = goodsIssueObj.GoodsIssueNo;
            model.CreateDate = goodsIssueObj.CreationDate;
            List<GoodsIssueDetailsDto> goodsIssueDetailsDtos = new List<GoodsIssueDetailsDto>();
            foreach (var row in goodsIssueObj.GoodsIssueDetails)
            {
                var details = new GoodsIssueDetailsDto();
                details.Purpose = row.Purpose != null ? _commonHelper.GetEmumIdToValue<Purpose>(Convert.ToInt32(row.Purpose)) : "N/A";
                details.Program = row.Program != null ? row.Program.Name : "N/A";
                details.Session = row.Session != null ? row.Session.Name : "";
                details.ItemGroup = row.Item.ItemGroup.Name;
                details.ItemName = row.Item.Name;
                details.IssuedQuantity = Convert.ToInt32(row.IssuedQuantity.ToString());
                goodsIssueDetailsDtos.Add(details);
                // model.GoodsIssueDetailsDtos.Add(details);
            }
            model.GoodsIssueDetailsDtos = goodsIssueDetailsDtos;
            return model;
        }

        #endregion
        
        #endregion

        #region Index/Manage functionalities

        public ActionResult Index()
        {
            GetStatusText();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
               
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        #region Render Goods Issue Data Table

        [HttpPost]
        public JsonResult GoodsIssuedList(int draw, int start, int length, long? organizationId = null, long? branchId = null, string item = "", DateTime? issuedDate = null, string goodsIssueNo = "", int? status = null)
        {
            var getData = new List<object>();
            long recordsTotal = 0;
            long recordsFiltered = 0;

            try
            {
                #region OrderBy and Direction
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                //if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                //{
                //    orderBy = nvc["order[0][column]"];
                //    orderDir = nvc["order[0][dir]"];
                //    switch (orderBy)
                //    {
                //        case "0":
                //            orderBy = "Name";
                //            break;
                //        case "1":
                //            orderBy = "Code";
                //            break;
                //        case "2":
                //            orderBy = "Rank";
                //            break;
                //        case "3":
                //            orderBy = "Status";
                //            break;
                //        default:
                //            orderBy = "";
                //            break;
                //    }
                //}

                #endregion

                recordsTotal = _goodsIssueService.GetGoodsIssueCount(_userMenu, organizationId, branchId, item, issuedDate, goodsIssueNo, status);
                recordsFiltered = recordsTotal;
                List<IGrouping<string, GoodsIssueDto>> goodsIssuedList = _goodsIssueService.LoadGoodsIssuedList(start, length, orderBy, orderDir.ToUpper(),_userMenu, organizationId, branchId, item, issuedDate, goodsIssueNo, status).ToList().GroupBy(x => x.GoodsIssueNo).ToList();

                foreach (var goodsList in goodsIssuedList)
                {
                    string itemName = "";
                    foreach (var goodsIssueDto in goodsList)
                    {
                        itemName += goodsIssueDto.ItemName + ", ";
                    }
                    itemName = itemName.Trim(' ', ',');
                    getData.Add(
                        new List<string>()
                        {
                             goodsList.First().Organization, goodsList.First().Branch, itemName,
                             goodsList.First().CreateDate.ToString("MMM dd,yyyy"), goodsList.First().GoodsIssueNo,
                             //_commonHelper.GetEmumIdToValue<GoodsIssuedStatus>(goodsList.First().Status),
                             LinkGenerator.GetDetailsLink("Details", "DirectGoodsIssue", goodsList.First().Id)
                        }
                    );
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
            });
        }

        #endregion
        
        #endregion

        #region Report
       
        [HttpGet]
        public ActionResult GoodsIssueReport()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var authorgList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            ViewBag.organizationList = authorgList;
            ViewBag.branchIdList = new SelectList(string.Empty, "Value", "Text");

            var list = _itemGroupService.LoadItemGroup(0, int.MaxValue, "", "", "", ItemGroup.EntityStatus.Active.ToString());
            list.Insert(0, new ItemGroup() { Id = 0, Name = "All Selected" });
            ViewBag.itemGroupList = new SelectList(list, "Id", "Name");

            ViewBag.itemList = new SelectList(string.Empty, "Value", "Text");

            var psList = new List<ProgramSessionDto>();
            psList.Insert(0, new ProgramSessionDto() { ProgramAndSessionId = SelectionType.SelelectAll.ToString(), ProgramAndSessionName = "All Selected" });
            ViewBag.programSessionList = new SelectList(psList, "ProgramAndSessionId", "ProgramAndSessionName");

            var pup = _commonHelper.LoadEmumToDictionary<Purpose>();
            pup.Add(0, "All Selected");
            ViewBag.perposeList = new SelectList(pup, "Key", "Value");
            var goodsIsueTypeDictionary = _commonHelper.LoadEmumToDictionary<GoodsIssueType>();
            ViewBag.GoodsIssueTypeList = new SelectList(goodsIsueTypeDictionary, "Key", "Value");
            var reportType = _commonHelper.LoadEmumToDictionary<ReportType>();
            ViewBag.reportType = new SelectList(reportType, "key", "value");
            return View();

        }

        [HttpPost]
        public ActionResult GoodsIssueReport(GoodsIssueReportFormViewModel goodsIssueReport)
        {
            if (ModelState.IsValid)
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (goodsIssueReport.BranchId == null) goodsIssueReport.BranchId = new long[] { 0 };
                if (goodsIssueReport.Purpose == null) goodsIssueReport.Purpose = new Purpose[] { 0 };
                if (goodsIssueReport.ItemGroupId == null) goodsIssueReport.ItemGroupId = new long[] { 0 };
                if (goodsIssueReport.ItemId == null) goodsIssueReport.ItemId = new long[] { 0 };
                if (goodsIssueReport.ProgramSession == null) goodsIssueReport.ProgramSession = new string[] { SelectionType.SelelectAll.ToString() };

                List<long> branchIds = new List<long>(goodsIssueReport.BranchId.ToArray());
                ViewBag.organizationName = _organizationService.LoadById(goodsIssueReport.OrganizationId).Name;
                ViewBag.OrganizationShortName = _organizationService.LoadById(goodsIssueReport.OrganizationId).ShortName;
                var branchName = !goodsIssueReport.BranchId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _branchService.LoadBranch(goodsIssueReport.BranchId).Select(x => x.Name).ToList()) : "All Branch";
                var organizationIdList = _commonHelper.ConvertIdToList(goodsIssueReport.OrganizationId);

                var bName = !goodsIssueReport.BranchId.Contains(SelectionType.SelelectAll) ? string.Join(", ",
                   _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, goodsIssueReport.BranchId.Skip(0).Take(goodsIssueReport.BranchId.Length).ToList())
                   ) : string.Join(", ", _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds).ToList());
                ViewBag.branchAry = bName.Split(',');

                ViewBag.branchName = branchName;
                ViewBag.goodIssueTypeList = _commonHelper.GetEmumIdToValue<GoodsIssueType>(goodsIssueReport.GoodsIssueType);
                ViewBag.itemGroup = !goodsIssueReport.ItemGroupId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _itemGroupService.LoadItemGroup(goodsIssueReport.ItemGroupId.ToList()).Select(x => x.Name).ToList()) : "All Item Groups";
                ViewBag.item = !goodsIssueReport.ItemId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _itemService.LoadItemList(goodsIssueReport.ItemId).Select(x => x.Name).ToList()) : "All Items";
                ViewBag.dateFrom = goodsIssueReport.DateFrom.ToString("MMM dd, yyyy");
                ViewBag.dateTo = goodsIssueReport.DateTo.ToString("MMM dd, yyyy");
                string programSession = "";
                int countshouldhave = CountProgramAndSession(goodsIssueReport.OrganizationId, branchIds, goodsIssueReport.ItemId.ToList());
                int counthave = goodsIssueReport.ProgramSession.Count();
                if ((counthave == 1 && goodsIssueReport.ProgramSession[0] == SelectionType.SelelectAll.ToString()) ||
                    ((counthave - 1) == countshouldhave))
                {
                    programSession = "All Program & Session";
                    var programSessionColName = GetProgramSessionName(goodsIssueReport.ProgramSession, goodsIssueReport.OrganizationId, goodsIssueReport.BranchId, goodsIssueReport.ItemId);
                    ViewBag.programSessionAry = programSessionColName.Split(',');
                }
                else
                {
                    programSession = GetProgramSessionName(goodsIssueReport.ProgramSession, goodsIssueReport.OrganizationId, goodsIssueReport.BranchId, goodsIssueReport.ItemId);
                    ViewBag.programSessionAry = programSession.Split(',');
                }
                string purpose = GetPurpose(goodsIssueReport.Purpose);
                ViewBag.programSession = programSession;
                ViewBag.purpose = purpose;
                return PartialView("Partial/_GoodsIssueReportPartialView", goodsIssueReport);

            }
            return View();
        }

        public int CountProgramAndSession(long organizationId, List<long> branchIds, List<long> itemIdList)
        {
            IList<ProgramSessionDto> list = new List<ProgramSessionDto>();
            try
            {
                if (branchIds.Count == 1 && branchIds.Contains(0)) branchIds = null;
                if (itemIdList.Count == 1 && itemIdList.Contains(0)) itemIdList = null;
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                list = _programBranchSessionService.LoadProgramSession(_userMenu, organizationId, branchIds, itemIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return list.Count();
        }

        private IList<string> OnGettingAuthorizeBranchNameList(List<UserMenu> userMenu, List<long> organizationIdList, List<long> branchIds)
        {
            try
            {
                var bName = !branchIds.Contains(SelectionType.SelelectAll) ? string.Join(", ",
                   _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds.Skip(0).Take(branchIds.Count).ToList())
                   ) : string.Join(", ", _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds).ToList());
                return bName.Split(',');
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #region Render Data Table

        [HttpPost]
        public JsonResult GoodsIssueReportAjaxRequest(int draw, int start, int length, long organizationId, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, string dateFrom, string dateTo, List<string> programsession)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (reportType == (int) ReportType.Branchwise)
                    return GoodsIssueBranchWiseReportAjaxRequest(draw, start, length, userMenu, organizationId, branchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, dateFrom, dateTo, programsession);
                return GoodsIssueProgramWiseReportAjaxRequest(draw, start, length, userMenu, organizationId, branchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, dateFrom, dateTo, programsession);
                
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                throw ex;
            }
        }
        private JsonResult GoodsIssueBranchWiseReportAjaxRequest(int draw, int start, int length, List<UserMenu> userMenu, long organizationId, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, string dateFrom, string dateTo, List<string> programsession)
        {
            try
            {
                DateTime fromDate = Convert.ToDateTime(dateFrom);
                DateTime toDate = Convert.ToDateTime(dateTo);
                bool isAddNa = false;
                bool isOnlyNa = false;
                string[] psAry = programsession.ToArray();
                if (psAry.Length > 0)
                    psAry = psAry.Distinct().ToArray();

                // selectNone means N/A
                if (psAry.Contains(SelectionType.SelectNone.ToString()))
                {
                    isAddNa = true;
                    psAry = psAry.Skip(1).Take(psAry.Length - 1).ToArray();
                    if (psAry.Length <= 0) isOnlyNa = true;
                }
                if (psAry.Length == 1 && psAry.Contains(SelectionType.SelelectAll.ToString()))
                    isAddNa = true;

                string[] programSessionAuthArrayList = _goodsIssueDetailsService.BuildAuthProgramSession(userMenu, organizationId, branchIdList.ToArray(), itemIdList.ToArray(), psAry);
                var proSessionList = programSessionAuthArrayList.ToList();
                if (isOnlyNa) { proSessionList = new List<string>(); }
                if (isAddNa) { proSessionList.Insert(0, SelectionType.SelectNone.ToString()); }

                IList<dynamic> goodIssueList = _goodsIssueService.LoadGoodsIssueBranchWiseReport(branchNameDelegate, start, length, userMenu, organizationId, branchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, fromDate, toDate, programsession);
                int count = _goodsIssueService.LoadGoodsIssueBranchWiseReportCount(branchNameDelegate, userMenu, organizationId, branchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, fromDate, toDate, programsession);
                var data = new List<object>();
                int sl = start + 1;
                foreach (var goodIssueObj in goodIssueList)
                {
                    var str = new List<object>();
                    str.Add(sl.ToString());
                    IDictionary<string, object> dic;
                    str.AddRange(from entry in dic = goodIssueObj select entry.Value);
                    sl++;
                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = count,
                    recordsFiltered = count,
                    start = start,
                    length = length,
                    data = data,

                });
                
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                throw ex;
            }
        }

        private JsonResult GoodsIssueProgramWiseReportAjaxRequest(int draw, int start, int length, List<UserMenu> userMenu, long organizationId, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, string dateFrom, string dateTo, List<string> programSessionArrayList)
        {
            try
            {
                DateTime fromDate = Convert.ToDateTime(dateFrom);
                DateTime toDate = Convert.ToDateTime(dateTo);
                bool isAddNa = false;
                bool isOnlyNa = false;
                string[] psAry = programSessionArrayList.ToArray();
                if (psAry.Length > 0)
                    psAry = psAry.Distinct().ToArray();

                if (psAry.Contains(SelectionType.SelectNone.ToString()))
                {
                    isAddNa = true;
                    psAry = psAry.Skip(1).Take(psAry.Length - 1).ToArray();
                    if (psAry.Length <= 0) isOnlyNa = true;
                }
                if (psAry.Length == 1 && psAry.Contains(SelectionType.SelelectAll.ToString()))
                    isAddNa = true;

                string[] programSessionAuthArrayList = _goodsIssueDetailsService.BuildAuthProgramSession(userMenu, organizationId, branchIdList.ToArray(), itemIdList.ToArray(), psAry);
                var proSessionList = programSessionAuthArrayList.ToList();
                if (isOnlyNa) { proSessionList = new List<string>(); }
                if (isAddNa) { proSessionList.Insert(0, SelectionType.SelectNone.ToString()); }
                IList<dynamic> goodIssueList = _goodsIssueService.LoadGoodsIssueProgramWiseReport(start, length, userMenu, organizationId, branchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, fromDate, toDate, proSessionList,isAddNa);
                int count = _goodsIssueService.LoadGoodsIssueProgramWiseReportCount(userMenu, organizationId, branchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, fromDate, toDate, proSessionList,isAddNa);
                int sl = start+1;
                var data = new List<object>();
                foreach (var goodIssueObj in goodIssueList)
                {
                    var str = new List<object>();
                    str.Add(sl.ToString());
                    IDictionary<string, object> dic;
                    str.AddRange(from entry in dic = goodIssueObj select entry.Value);
                    sl++;
                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = count,
                    recordsFiltered = count,
                    start = start,
                    length = length,
                    data = data,

                });

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                throw ex;
            }
        }


        public ActionResult ExportReport(long organizationId, List<long> branchIdList, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int goodsIssueType, int reportType, string dateFrom, string dateTo, List<string> programsession)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                int start = 0;
                int length = int.MaxValue;
                IList<dynamic> goodsIssueList = new List<dynamic>();
                string organizationName = _organizationService.LoadById(organizationId).Name;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);

                List<string> headerList = new List<string>();
                var bName = !branchIdList.Contains(SelectionType.SelelectAll) ? string.Join(", ",
                     _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIdList.Skip(0).Take(branchIdList.Count).ToList())
                     ) : string.Join(", ", _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIdList).ToList());
                var branchAry = bName.Split(',');

                bool isAddNa = false;
                bool isOnlyNa = false;
                string[] psAry = programsession.ToArray();
                if (psAry.Length > 0)
                    psAry = psAry.Distinct().ToArray();

                if (psAry.Contains(SelectionType.SelectNone.ToString()))
                {
                    isAddNa = true;
                    psAry = psAry.Skip(1).Take(psAry.Length - 1).ToArray();
                    if (psAry.Length <= 0) isOnlyNa = true;
                }
                if (psAry.Length == 1 && psAry.Contains(SelectionType.SelelectAll.ToString()))
                    isAddNa = true;

                string programSessionColName = "";
                int countshouldhave = CountProgramAndSession(organizationId, branchIdList, itemIdList);
                int counthave = programsession.Count();
                if ((counthave == 1 && programsession[0] == SelectionType.SelelectAll.ToString()) || ((counthave - 1) == countshouldhave))
                    programSessionColName = GetProgramSessionName(programsession.ToArray(), organizationId, branchIdList.ToArray(), itemIdList.ToArray());
                else
                    programSessionColName = GetProgramSessionName(programsession.ToArray(), organizationId, branchIdList.ToArray(), itemIdList.ToArray());

                var programSessionAry = programSessionColName.Split(',');

                string[] programSessionAuthArrayList = _goodsIssueDetailsService.BuildAuthProgramSession(userMenu, organizationId, branchIdList.ToArray(), itemIdList.ToArray(), psAry);
                var proSessionList = programSessionAuthArrayList.ToList();

                if (isOnlyNa) { proSessionList = new List<string>(); }
                if (isAddNa) { proSessionList.Insert(0, SelectionType.SelectNone.ToString()); }
                DateTime fromDate = Convert.ToDateTime(dateFrom);
                DateTime toDate = Convert.ToDateTime(dateTo);
                headerList.Add(organizationName);
                headerList.Add("Goods Issue Report");
                headerList.Add(dateFrom + " to " + dateTo);
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                if (reportType == (int)ReportType.Branchwise)
                {
                    columnList.Add("Item Group");
                    columnList.Add("Item Name");
                    columnList.Add("Program & Session");
                    columnList.AddRange(branchAry);
                    goodsIssueList = _goodsIssueService.LoadGoodsIssueBranchWiseReport(branchNameDelegate, start, length, userMenu, organizationId, branchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, fromDate, toDate, programsession);
                }
                else
                {
                    columnList.Add("Item Name");
                    columnList.AddRange(programSessionAry);
                    goodsIssueList = _goodsIssueService.LoadGoodsIssueProgramWiseReport(start, length, userMenu, organizationId, branchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, fromDate, toDate, proSessionList, isAddNa);
                }
                columnList.Add("Total");
                var transactionXlsList = new List<List<object>>();
                foreach (var goodsIssueObj in goodsIssueList)
                {
                    var str = new List<object>();
                    IDictionary<string, object> dic;
                    str.AddRange(from entry in dic = goodsIssueObj select entry.Value);
                    transactionXlsList.Add(str);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "GIReport__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("Autoclose");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion

        #endregion

        #region Helper functions

        #region Ajax function

        public JsonResult CurrentStockQuantityCheck(long branchId, long itemId, int? stockQuantityStaus = null)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var currentStockAmount = _goodsIssueService.LoadCurrentStockQuantity(branchId, itemId, stockQuantityStaus);
                    return Json(new { stockAmount = currentStockAmount, IsSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            return Json(false);
        }

        //Item Check with Item Type
        [HttpPost]
        public ActionResult LoadItem(long? organizationId, long[] itemGroupIds = null)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (!Request.IsAjaxRequest())
                    return Json(new Response(false, "Invalid Ajax Request !"));
                if (itemGroupIds == null)
                    return Json(new Response(false, "Invalid ItemGroup !"));
                if (itemGroupIds.Contains(0)) itemGroupIds = null;
                var itemList = _itemService.LoadItemForOrganizationOrGroup(userMenu, organizationId, itemGroupIds);

            //    var itemSelectList = new SelectList(itemList.ToList(), "Id", "Name");

                var itemSelectList = new SelectList((from s in itemList.ToList()
                                                    select new
                                                    {
                                                        Id = s.Id,
                                                        ItemType = s.Name + ":" + s.ItemType
                                                    }),"Id","ItemType",null);

                return Json(new { returnResult = itemSelectList, IsSuccess = true }); 
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { returnResult = new List<SelectListItem>(), IsSuccess = false });
            }
        }

        #endregion

        private string GetProgramSessionName(string[] programSessionAryStrings, long organizationId, long[] branchId, long[] itemId)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<long> programIdList = new List<long>();
            IList<long> sessionIdList = new List<long>();

            var programSession = "";
            string[] programSessionArrayList;
            if (programSessionAryStrings.Contains(SelectionType.SelectNone.ToString()))
            {
                programSession = "N/A,";
                programSessionArrayList = programSessionAryStrings.Skip(1).Take(programSessionAryStrings.Length - 1).ToArray();
            }
            else
                programSessionArrayList = programSessionAryStrings;
            if (programSessionAryStrings.Contains(SelectionType.SelelectAll.ToString()))
            {
                programSession = "N/A,";
                programSessionArrayList = _goodsIssueDetailsService.BuildAuthProgramSession(userMenu, organizationId, branchId, itemId);
            }
            foreach (var programsession in programSessionArrayList)
            {
                string[] programSessionArray = programsession.Split(new string[] { "::" }, StringSplitOptions.None);
                Program program = _programService.GetProgram(Convert.ToInt64(programSessionArray[0]));
                Session session = _sessionService.LoadById(Convert.ToInt64(programSessionArray[1]));
                if (program != null && session != null)
                {
                    programIdList.Add(program.Id);
                    sessionIdList.Add(session.Id);
                    string programName = program.ShortName;
                    string sessionName = session.Name;
                    string programSessionName = programName + " & " + sessionName + ", ";
                    programSession += programSessionName;
                }
            }
            programSession = programSession.Trim(' ', ',');
            ViewBag.ProgramIdList = programIdList;
            ViewBag.SessionIdList = sessionIdList;
            return programSession;
        }

        private string GetPurpose(Purpose[] purposes)
        {
            string purpose = "";
            if (!purposes.Where(x => (int)x == SelectionType.SelelectAll).Any())
            {
                foreach (var purposeValue in purposes)
                {
                    purpose += _commonHelper.GetEmumIdToValue<Purpose>((int)purposeValue) + ", ";
                }
                purpose = purpose.Trim(' ', ',');
            }
            else
                purpose = "All Purposes";
            return purpose;
        }

        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.LoadEmumToDictionary<GoodsIssuedStatus>();
        }

        #endregion

    }
}