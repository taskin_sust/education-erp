﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.SubjectTest
{
    public class SubjectBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal ISubjectService _subjectService;
        internal IProgramSessionSubjectService _programSessionSubjectService;
        private ITestBaseService<ProgramSessionSubject> _TestBaseService;
        internal List<long> IdList = new List<long>();
        private ISession _session;
        internal SubjectFactory _subjectFactory;
        internal OrganizationFactory _organizationFactory;
        internal ProgramFactory _programFactory;
        internal SessionFactory _sessionFactory;
        internal readonly string _name = Guid.NewGuid().ToString().Substring(0,15) + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal readonly string _shortName = Guid.NewGuid().ToString().Substring(0, 15) + "_(ABC CDE FGH)";
        internal const long OrgId = 1;
        internal DateTime _startTime = DateTime.Now.AddDays(1).AddHours(1);
        internal DateTime _endTime = DateTime.Now.AddDays(1).AddHours(10);
        #endregion

        public SubjectBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _subjectFactory = new SubjectFactory(null, _session);
            _organizationFactory=new OrganizationFactory(null,_session);
            _programFactory=new ProgramFactory(null,_session);
            _sessionFactory = new SessionFactory(null, _session);
            _subjectService = new SubjectService(_session);
            _programSessionSubjectService=new ProgramSessionSubjectService(_session);
            _TestBaseService=new TestBaseService<ProgramSessionSubject>(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }
        public void CleanUpProgramSessionSubject(long programId,long sessionId, List<long>subjectIdList)
        {
            _TestBaseService.DeleteProgramSessionSubject(programId, sessionId, subjectIdList);
        }
        public void Dispose()
        {
            _subjectFactory.Cleanup();
            _programFactory.Cleanup();
            _sessionFactory.Cleanup();
            _organizationFactory.Cleanup();
            base.Dispose();
        }
    }
}
