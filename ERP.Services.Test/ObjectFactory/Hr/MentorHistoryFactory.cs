﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class MentorHistoryFactory : ObjectFactoryBase<MentorHistory>
    {
        private DesignationFactory _designationFactory;
        private DepartmentFactory _departmentFactory;
        private OrganizationFactory _organizationFactory;
        private TeamMemberFactory _teamMemberFactory;
        private DesignationFactory _mentordesignationFactory;
        private DepartmentFactory _mentordepartmentFactory;
        private OrganizationFactory _mentororganizationFactory;
        private TeamMemberFactory _mentorteamMemberFactory;
        private readonly IMentorHistoryService _mentorHistoryService;
        private readonly TestBaseService<MentorHistorylogService> _mentorHistorylogService;
        public MentorHistoryFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _designationFactory = new DesignationFactory(this, session);
            _departmentFactory = new DepartmentFactory(this, session);
            _organizationFactory = new OrganizationFactory(this, session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _mentorHistoryService = new MentorHistoryService(session);
            _mentorHistorylogService = new TestBaseService<MentorHistorylogService>(session);
        }

        #region Create
        public MentorHistoryFactory Create()
        {
            Object = new MentorHistory()
            {
                //Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Pin = 1,
              //  MentorName = "Shohag",
                EffectiveDate = DateTime.Now
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public MentorHistoryFactory CreateWith(int pin, string mentorName, DateTime effectiveDate)
        {
            Object = new MentorHistory()
            {
                //Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Pin = pin,
             //   MentorName = mentorName,
                EffectiveDate = effectiveDate
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public MentorHistoryFactory WithDesignation()
        {
            Object.Designation = _designationFactory.Create().Persist().Object;
            return this;
        }
        public MentorHistoryFactory WithDesignation(Designation designation)
        {
            Object.Designation = designation;
            return this;
        }
        public MentorHistoryFactory WithDepartment()
        {
            Object.Department = _departmentFactory.Create().Persist().Object;
            return this;
        }
        public MentorHistoryFactory WithDepartment(Department department)
        {
            Object.Department = department;
            return this;
        }
        public MentorHistoryFactory WithOrganization()
        {
            Object.Organization = _organizationFactory.Create().Persist().Object;
            return this;
        }
        public MentorHistoryFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }
        public MentorHistoryFactory WithTeamMember()
        {
            Object.TeamMember = _teamMemberFactory.Create().Persist().Object;
            return this;
        }
        public MentorHistoryFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }
        public MentorHistoryFactory WithMentor()
        {
            Object.Mentor = _teamMemberFactory.Create().Persist().Object;
            return this;
        }
        public MentorHistoryFactory WithMentor(TeamMember mentor)
        {
            Object.Mentor = mentor;
            return this;
        }
        public MentorHistoryFactory WithMentorDesignation()
        {
            Object.MentorDesignation = _mentordesignationFactory.Create().Object;
            return this;
        }
        public MentorHistoryFactory WithMentorDesignation(Designation designation)
        {
            Object.MentorDesignation = designation;
            return this;
        }
        public MentorHistoryFactory WithMentorDepartment()
        {
            Object.MentorDepartment = _mentordepartmentFactory.Create().Persist().Object;
            return this;
        }
        public MentorHistoryFactory WithMentorDepartment(Department department)
        {
            Object.MentorDepartment = department;
            return this;
        }
        public MentorHistoryFactory WithMentorOrganization()
        {
            Object.MentorOrganization = _mentororganizationFactory.Create().Persist().Object;
            return this;
        }
        public MentorHistoryFactory WithMentorOrganization(Organization organization)
        {
            Object.MentorOrganization = organization;
            return this;
        }
        public MentorHistoryFactory Persist()
        {
            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    _mentorHistoryService.Save(x.Mentor.Id, x);
                }
            });            

            ObjectList.ForEach(item =>
            {
                if (item.Id == 0)
                {
                    _mentorHistoryService.Save(item.Mentor.Id, item);
                }
            });
            
            return this;
        }
       
        public MentorHistoryFactory CreateMore(int num,TeamMember mentor,Department dept,Designation designation,Organization org,Department mentorDepartment,
            Designation mentorDesignation,Organization mentorOrganization, DateTime? effectiveDate = null)
        {
            for (int i = 0; i < num; i++)
            {
                var obj = new MentorHistory()
                {
                    //Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = Name +"_"+ i,
                    Rank = 1,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Pin = mentor.Pin,
                    //MentorName = mentor.Name,
                    EffectiveDate = effectiveDate ?? DateTime.Now.AddYears(-1),
                    Mentor = mentor,
                    Department = dept,
                    Designation = designation,
                    Organization = org,
                    MentorDepartment = mentorDepartment,
                    MentorDesignation = mentorDesignation,
                    MentorOrganization = mentorOrganization
                };
                ObjectList.Add(obj);
            }

            return this;
        }
        #endregion

        #region Others
        #endregion

        #region CleanUp
        public MentorHistoryFactory CleanUp()
        {
            //var emHistoryIdList =
            //    _session.QueryOver<MentorHistory>()
            //        .Select(x => x.Id)
            //        .Where(x => x.BusinessId == GetPcUserName().ToString())
            //        .List<long>();
            //if (emHistoryIdList.Count > 0)
            //    _mentorHistorylogService.DeleteByIdList(emHistoryIdList.ToList());
            DeleteAll();
            //_designationFactory.Cleanup();
            //_departmentFactory.Cleanup();
            //_teamMemberFactory.Cleanup();
            //_mentordesignationFactory.Cleanup();
            //_mentordepartmentFactory.Cleanup();
            //_mentorteamMemberFactory.Cleanup();
            return this;
        }

        public void LogCleanUp(TeamMember teamMember)
        {
           //_mentorHistorylogService.DeleteMentorHistoryLogByMentorHistoryIdList(teamMember);
            //dao.Session.Delete(string.Format("from HR_MentorHistoryLog where MentorHistoryId in ({0})", string.Join(",", ids.ToArray())));
            var list = teamMember.MentorHistory.Select(x => x.Id).ToList();
            var logHistoryIdList =
               _session.QueryOver<MentorHistoryLog>()
                //.Select(x => x.Id)
                   .Where(x => x.TeamMemberId == teamMember.Id || x.MentorHistoryId.IsIn(list.ToArray()) || x.TeamMemberId == null)
                   .List<MentorHistoryLog>();
            if (logHistoryIdList.Count > 0)
            {
                foreach (var mhl in logHistoryIdList)
                {
                    string a = "DELETE FROM [HR_MentorHistoryLog] WHERE [Id] = " + mhl.Id + ";";
                    _session.CreateSQLQuery(a).ExecuteUpdate();
                }
                //dao.Session.Flush();
                //string a = "DELETE FROM [HR_MentorHistoryLog] WHERE [Id] IN (" + string.Join(",", logHistoryIdList) + "); ";
                //dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                //dao.Session.Flush();
            }
        }

        #endregion
    }
}
