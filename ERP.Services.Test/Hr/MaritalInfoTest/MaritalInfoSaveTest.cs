﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MaritalInfoTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "MaritalInfo")]
    public class MaritalInfoSaveTest : MaritalInfoBaseTest
    {
        #region Basic Test
        [Fact]
        public void SaveMaritalInformation_Should_Successful()
        {
            var teamMember = PersistTeamMember(MentorId);
            using (ITransaction trans = Session.BeginTransaction())
            {
                teamMember.MaritalInfo.RemoveAt(0);
                trans.Commit();
            }
            bool result = _maritalInfoService.UpdateMaritalInformation(teamMember, (int)MaritalType.Single,
                                                                    DateTime.Now.AddYears(-1).ToString(), SpouseName, SpouseContacts);
            Assert.True(true);
        }
        [Fact]
        public void UpdateMaritalInformation_Should_Successful()
        {
            var teamMember = PersistTeamMember(MentorId);
            bool result = _maritalInfoService.UpdateMaritalInformation(teamMember, (int)MaritalType.Single,
                                                                    DateTime.Now.AddYears(-1).ToString(), SpouseName, SpouseContacts);
            Assert.True(true);
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void InvalidDataException_UpdateMaritalInformation_For_Incorrect_MobileNumber()
        {
            var teamMember = PersistTeamMember(MentorId);
            Assert.Throws<InvalidDataException>(() => _maritalInfoService.UpdateMaritalInformation(teamMember, (int)MaritalType.Single,
                                                                    DateTime.Now.AddYears(-1).ToString(), SpouseName, "ABCD"));
        }

        [Fact]
        public void InvalidDataException_UpdateMaritalInformation_For_Incorrect_MaritalType()
        {
            var teamMember = PersistTeamMember(MentorId);
            Assert.Throws<InvalidDataException>(() => _maritalInfoService.UpdateMaritalInformation(teamMember, 0,
                                                                    DateTime.Now.AddYears(-1).ToString(), SpouseName, SpouseContacts));
        }

        #endregion
    }
}
