using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface ISupplierDao : IBaseDao<Supplier, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        Supplier LoadByTlnInc(string tlnInc, long? id);
        Supplier LoadByTin(string tin, long? id);
        Supplier LoadByVatRegNo(string vatRegNo, long? id);
        Supplier SupplierByName(string supplierName, long? id);

        #endregion

        #region List Loading Function

        IList<Supplier> LoadSupplier(int start, int length, string orderBy, string orderDir, string name, string status);
        IList<Supplier> LoadByIds(string[] supplierIds);
        IList<Supplier> LoadSupplierByIds(long[] supplierIds);

        #endregion

        #region Others Function
        bool CheckDuplicateSupplier(out string fieldName, string name = null, string tin = null, string tlnInc = null, string vatRegNo = null, long id = 0);
        int GetSupplierCount(string name, string status);
        #endregion


    }

    public class SupplierDao : BaseDao<Supplier, long>, ISupplierDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public Supplier SupplierByName(string supplierName, long? id)
        {
            var entity = Session.QueryOver<Supplier>().Where(x => x.Name == supplierName).Where(x => x.Id != id).SingleOrDefault<Supplier>();
            return entity ?? null;
        }

        public Supplier LoadByTlnInc(string tlnInc, long? id)
        {
            var entity = Session.QueryOver<Supplier>().Where(x => x.TlnInc == tlnInc).Where(x => x.Id != id).SingleOrDefault<Supplier>();
            return entity ?? null;
        }

        public Supplier LoadByTin(string tin, long? id)
        {
            var entity = Session.QueryOver<Supplier>().Where(x => x.Tin == tin).Where(x => x.Id != id).SingleOrDefault<Supplier>();
            return entity ?? null;
        }

        public Supplier LoadByVatRegNo(string vatRegNo, long? id)
        {
            var entity = Session.QueryOver<Supplier>().Where(x => x.VatRegistrationNo == vatRegNo).Where(x => x.Id != id).SingleOrDefault<Supplier>();
            return entity ?? null;
        }

        #endregion

        #region List Loading Function

        public IList<Supplier> LoadSupplier(int start, int length, string orderBy, string orderDir, string name, string status)
        {
            ICriteria criteria = GetSupplierCriteria(name, status);
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Supplier>();
        }

        public IList<Supplier> LoadByIds(string[] supplierIds)
        {
            ICriteria criteria = Session.CreateCriteria<Supplier>().Add(Restrictions.Eq("Status", Supplier.EntityStatus.Active));
            criteria.Add(Restrictions.In("Id", supplierIds));
            return criteria.List<Supplier>();
        }

        public IList<Supplier> LoadSupplierByIds(long[] supplierIds)
        {
            return Session.QueryOver<Supplier>().Where(Restrictions.On<Supplier>(y => y.Id).IsIn(supplierIds)).Where(x => x.Status == Supplier.EntityStatus.Active).List<Supplier>();
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        public bool CheckDuplicateSupplier(out string fieldName, string name = null, string tin = null, string tlnInc = null,
            string vatRegNo = null, long id = 0)
        {
            fieldName = "";

            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<Supplier>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Supplier.EntityStatus.Delete)));
            if (id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (name != null)
            {
                criteria.Add(Restrictions.Eq("Name", name));
                fieldName = "Name";
            }
            else if (tin != null)
            {
                criteria.Add(Restrictions.Eq("Tin", tin));
                fieldName = "Tin";
            }
            else if (tlnInc != null)
            {
                criteria.Add(Restrictions.Eq("TlnInc", tlnInc));
                fieldName = "TlnInc";
            }
            else if (vatRegNo != null)
            {
                criteria.Add(Restrictions.Eq("VatRegistrationNo", vatRegNo));
                fieldName = "VatRegistrationNo";
            }
            var supplierList = criteria.List<Supplier>();
            if (supplierList != null && supplierList.Count > 0)
            {
                checkValue = true;
            }
            return checkValue;
        }

        public int GetSupplierCount(string name, string status)
        {
            ICriteria criteria = GetSupplierCriteria(name, status);
            criteria.SetProjection(Projections.RowCount());
            return (Convert.ToInt32(criteria.UniqueResult()));
        }

        private ICriteria GetSupplierCriteria(string name, string status)
        {
            var criteria = Session.CreateCriteria<Supplier>().Add(Restrictions.Not(Restrictions.Eq("Status", Supplier.EntityStatus.Delete)));
            if (!string.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.InsensitiveLike("Name", name.Trim(), MatchMode.Anywhere));
            }
            if (!string.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status.Trim())));
            }
            return criteria;
        }

        #endregion
    }
}
