using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class GoodsReceiveFactory : ObjectFactoryBase<GoodsReceive>
    {
        #region Object Initialization

        private BranchFactory _branchFactory;
        //private GoodsReceiveDetailsFactory _goodsReceiveDetailsFactory;
        private readonly SupplierFactory _supplierFactory;
        private readonly SupplierBankDetailsFactory _supplierBankDetailsFactory;
        private readonly IGoodsReceiveService _goodsReceiveService;
        private readonly ISupplierService _supplierService;

        public GoodsReceiveFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _branchFactory = new BranchFactory(this, _session);
            //_goodsReceiveDetailsFactory = new GoodsReceiveDetailsFactory(null, _session);
            _supplierFactory = new SupplierFactory(this, _session);
            _supplierBankDetailsFactory = new SupplierBankDetailsFactory(this, _session);

            _goodsReceiveService = new GoodsReceiveService(session);
            _supplierService = new SupplierService(session);
        }

        #endregion

        #region Create

        public GoodsReceiveFactory Create()
        {
            Object = new GoodsReceive()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                TotalCost = 200,
                GoodsReceivedDate = DateTime.Now,
                GoodsReceiveStatus = (int)GoodsReceivedStatus.Received,
                Remarks = "Opening Balance",
                GoodsReceivedNo = "ORGBRGR000001",
                CreateBy = 1,
                ModifyBy = 1,
                Status = Item.EntityStatus.Active,


            };

            SingleObjectList.Add(Object);
            return this;
        }



        public GoodsReceiveFactory CreateMore(int num)
        {
            for (int i = 0; i < num; i++)
            {
                var obj = new GoodsReceive()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    TotalCost = 200,
                    GoodsReceivedDate = DateTime.Now,
                    GoodsReceiveStatus = (int)GoodsReceivedStatus.Received,
                    Remarks = "Opening Balance",
                    GoodsReceivedNo = "ORGBRGR00000" + i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Item.EntityStatus.Active
                };
                ObjectList.Add(obj);
            }
            return this;
        }

        public GoodsReceiveFactory WithBranch()
        {
            _branchFactory.Create().WithOrganization().Persist();
            Object.Branch = _branchFactory.Object;
            return this;
        }

        public GoodsReceiveFactory WithBranch(Organization organization)
        {
            _branchFactory.Create().WithOrganization(organization).Persist();
            Object.Branch = _branchFactory.Object;
            return this;
        }

        public GoodsReceiveFactory WithBranch(Branch branch)
        {
            Object.Branch = branch;
            return this;
        }

        public GoodsReceiveFactory WithSupplier()
        {
            var bankDetails = _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(bankDetails.SingleObjectList).Persist();
            Object.Supplier = _supplierFactory.Object;
            return this;
        }

        public GoodsReceiveFactory WithSupplier(Supplier supplier)
        {
            Object.Supplier = supplier;
            return this;
        }

        public GoodsReceiveFactory WithWorkOrder(WorkOrder workOrder)
        {
            Object.Workorder = workOrder;
            return this;
        }

        //public GoodsReceiveFactory WithGoodsReceiveDetails(Organization organization, Session session, Item item, int purpose, GoodsReceive goodsReceive)
        //{
        //    _goodsReceiveDetailsFactory.Create()
        //        .WithProgram(organization)
        //        .WithSession(session)
        //        .WithItem(item)
        //        .WithPurpose(purpose)
        //        .WithGoodsReceive(false, goodsReceive);

        //    Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
        //    return this;

        //}


        public object WithGoodsReceiveDetails(GoodsReceiveDetails goodsReceiveDetails)
        {
            Object.GoodsReceiveDetails.Add(goodsReceiveDetails);
            return this;
        }

        internal GoodsReceiveFactory CreateMoreDetails(List<GoodsReceiveDetails> list)
        {
            Object.GoodsReceiveDetails = list;
            return this;
        }

        public GoodsReceiveFactory Persist(bool isOpening)
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _goodsReceiveService.SaveOrUpdate(x, isOpening); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x => { if (x.Id == 0)_goodsReceiveService.SaveOrUpdate(x, isOpening); });
            }
            return this;
        }


        #endregion

        #region CleanUp

        public GoodsReceiveFactory CleanUp()
        {
            DeleteAll();
            _branchFactory.Cleanup();
            _supplierBankDetailsFactory.CleanUp();
            _supplierFactory.CleanUp();
            return this;
        }
        public GoodsReceiveFactory OnlyGoodsReceiveFactoryCleanUp()
        {
            DeleteAll();
            return this;
        }
        #endregion
 
    }
}
