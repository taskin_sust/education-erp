﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
   public class StudentClassAttendanceDetailsMap : BaseClassMap<StudentClassAttendanceDetails, long>
    {
       public StudentClassAttendanceDetailsMap()
       {
           Table("StudentClassAttendanceDetails");
           LazyLoad();
           References(x => x.StudentClassAttendence).Column("StudentClassAttendanceId");
           References(x => x.StudentProgram).Column("StudentProgramId");
           Map(x => x.StudentFeedback).Column("StudentFeedBack");
           Map(x => x.IsRegistered).Column("IsRegistered");
       }
    }
}
