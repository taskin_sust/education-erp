using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface IProgramSessionItemService : IBaseService
    {

        #region Operational Function

        bool SaveOrUpdate(List<ProgramSessionItem> programSessionItemList);

        #endregion

        #region Single Instances Loading Function

        ProgramSessionItem CheckItem(long itemId, long programId, long sessionId);
        ProgramSessionItem CheckNullableProgramSessionItem(long itemId, long itemGroupId, int itemType);

        #endregion

        #region List Loading Function

        IList<ProgramSessionItem> LoadProgramSessionItemByItemId(long? itemId);
        IList<ProgramSessionItemDto> LoadProgramSessionItemDtoByItemId(long[] itemId);

        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion
        
    }
    public class ProgramSessionItemService : BaseService, IProgramSessionItemService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");

        #endregion

        #region Propertise & Object Initialization

        private readonly IProgramSessionItemDao _programSessionItemDao;
        public ProgramSessionItemService(ISession session)
        {
            Session = session;
            _programSessionItemDao = new ProgramSessionItemDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(List<ProgramSessionItem> programSessionItemList)
        {
            ITransaction transaction = null;
            try
            {
                if (programSessionItemList == null)
                    throw new NullObjectException("Item list can't be Empty");

                List<long> listId = programSessionItemList.Where(x => x.Status == ProgramSessionItem.EntityStatus.Delete).Select(x => x.Id).ToList();
                List<ProgramSessionItem> listObj = programSessionItemList.Where(x => x.Status == ProgramSessionItem.EntityStatus.Active).ToList();
                
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var singleValue in listObj)
                    {
                        ProgramSessionItem check;
                        if (singleValue.Item == null)
                            throw new NullObjectException("Item can't be Empty");

                        if (singleValue.Item.ItemType == (int) ItemType.ProgramItem && singleValue.Program != null &&
                            singleValue.Session != null)
                        {
                            check = CheckItem(singleValue.Item.Id, singleValue.Program.Id, singleValue.Session.Id);
                        }

                        else if (singleValue.Item.ItemType == (int) ItemType.CommonItem && singleValue.Program == null &&
                                 singleValue.Session == null)
                        {
                            check = CheckNullableProgramSessionItem(singleValue.Item.Id, singleValue.Item.ItemGroup.Id,singleValue.Item.ItemType);
                        }

                        else throw new NullObjectException("If item type is selected by program item while Program & Session must be selected");

                        if (check==null)
                            _programSessionItemDao.SaveOrUpdate(singleValue);
                    }
                    transaction.Commit();
                }
                if (listId.Count > 0)
                {
                    _programSessionItemDao.IsDelete(listId);
                }
                return true;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
            return false;
        }
        
        #endregion

        #region Single Instances Loading Function

        public ProgramSessionItem CheckItem(long itemId, long programId, long sessionId)
        {
            try
            {
                return Session.QueryOver<ProgramSessionItem>().Where(x => x.Item.Id == itemId && x.Program.Id == programId && x.Session.Id == sessionId)
                    .List<ProgramSessionItem>().OrderByDescending(x => x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public ProgramSessionItem CheckNullableProgramSessionItem(long itemId, long itemGroupId, int itemType)
        {
            try
            {
                Item item = null;
                ItemGroup itemGroup = null;

                return  Session.QueryOver<ProgramSessionItem>().JoinAlias(x => x.Item, () => item).Where(()=>item.Id==itemId && item.ItemType==itemType)
                    .JoinAlias(() => item.ItemGroup, () => itemGroup)
                    .Where(() => itemGroup.Id == itemGroupId)
                    .List<ProgramSessionItem>().OrderByDescending(x => x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<ProgramSessionItem> LoadProgramSessionItemByItemId(long? itemId)
        {
            try
            {
                return _programSessionItemDao.LoadProgramSessionItemByItemId(itemId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ProgramSessionItemDto> LoadProgramSessionItemDtoByItemId(long[] itemId)
        {
            try
            {
                if (itemId == null || itemId.Count() < 1)
                    throw new NullObjectException("Item id must be selected");
                
                return _programSessionItemDao.LoadProgramSessionItemDtoByItemId(itemId);
                 
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper function
        #endregion
    }
}

