﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Util;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;

namespace UdvashERP.Dao.Administration
{
    public interface IExamProgressDao : IBaseDao<ExamProgress, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        List<ExamProgress> LoadExamProgress(int start, int length, string branch, string campus, string batch, string subject, string exam, IList<BusinessModel.Entity.Exam.Exams> examList, List<Batch> batchList);
        #endregion

        #region Others Function
        
        int GetExamProgressCount(string branch, string campus, string batch, string subject, string exam,
            IList<Exams> examList, List<Batch> batchList);
        IList<DateTime> GetExamDate(long id, long? batchId = null);
       #endregion

        #region Helper Function

        #endregion

        
    }
    public class ExamProgressDao : BaseDao<ExamProgress, long>, IExamProgressDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function

       public List<ExamProgress> LoadExamProgress(int start, int length, string branch, string campus, string batch,
            string subject, string exam, IList<BusinessModel.Entity.Exam.Exams> examList, List<Batch> batchList)
        {
            var criteria = LoadExamProgressCriteria(branch, campus, batch, subject, exam, examList, batchList);
            var examProgressList=new List<ExamProgress>();
            if (!string.IsNullOrEmpty(subject))
            {
                var examProgresses = new List<ExamProgress>();
                examProgressList = criteria.List<ExamProgress>().ToList();
                if (subject.ToLower() == "combined")
                {
                    
                    
                    foreach (var examProgress in examProgressList)
                    {
                        foreach (var ed in examProgress.Exam.ExamDetails)
                        {
                            if (ed.SubjectType == 3)
                            {
                                examProgresses.Add(examProgress);
                                break;
                            }
                        }
                    }
                   
                }
                else
                {
                    foreach (var examProgress in examProgressList)
                    {
                        foreach (var ed in examProgress.Exam.ExamDetails)
                        {
                            if (ed.Subject.Name.ToLower().Contains(subject.ToLower()))
                            {
                                examProgresses.Add(examProgress);
                                break;
                            }
                        }
                    }
                }
                if (length > 0)
                {
                    return examProgresses.Distinct().Skip(start).Take(length).ToList();
                }
                return examProgresses.Distinct().ToList();
            }
            if (length > 0)
            {
                return criteria.SetTimeout(2700).AddOrder(Order.Asc("HeldDate")).SetFirstResult(start).SetMaxResults(length).List<ExamProgress>().ToList();
            }
            return criteria.SetTimeout(2700).AddOrder(Order.Asc("HeldDate")).List<ExamProgress>().ToList();
        }

        private ICriteria LoadExamProgressCriteria(string branch, string campus, string batch, string subject, string exam, IList<BusinessModel.Entity.Exam.Exams> examList, List<Batch> batchList)
        {
            var criteria = Session.CreateCriteria<ExamProgress>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", ExamProgress.EntityStatus.Delete)));
            criteria.CreateAlias("Batch", "b").Add(Restrictions.Eq("b.Status", ExamProgress.EntityStatus.Active));
            criteria.CreateAlias("Exam", "e").Add(Restrictions.Eq("e.Status", ExamProgress.EntityStatus.Active));
            criteria.Add(Restrictions.In("b.Id", batchList.Select(x => x.Id).ToArray()));
            criteria.Add(Restrictions.In("e.Id", examList.Select(x => x.Id).ToArray()));
            if (!string.IsNullOrEmpty(branch))
            {
                criteria.CreateAlias("b.Branch", "br");
                criteria.Add(Restrictions.InsensitiveLike("br.Name", branch.ToLower(), MatchMode.Anywhere));
            }
            if (!string.IsNullOrEmpty(campus))
            {
                criteria.CreateAlias("b.Campus", "Camp");
                criteria.Add(Restrictions.InsensitiveLike("Camp.Name", campus.ToLower(), MatchMode.Anywhere));
            }
            if (!string.IsNullOrEmpty(batch))
            {
                criteria.Add(Restrictions.InsensitiveLike("b.Name", batch.ToLower(), MatchMode.Anywhere));
            }
            if (!string.IsNullOrEmpty(exam))
            {
                criteria.Add(Restrictions.InsensitiveLike("e.Name", exam, MatchMode.Anywhere));
            }
            
            return criteria;
        }
        

        #endregion

        #region Others Function
        
        public int GetExamProgressCount(string branch, string campus, string batch, string subject, string exam,
            IList<Exams> examList, List<Batch> batchList)
        {
            var criteria = LoadExamProgressCriteria(branch, campus, batch, subject, exam, examList, batchList);
            var examProgressList = new List<ExamProgress>();
            if (!string.IsNullOrEmpty(subject))
            {
                var examProgresses = new List<ExamProgress>();
                examProgressList = criteria.List<ExamProgress>().ToList();
                if (subject.ToLower() == "combined")
                {


                    foreach (var examProgress in examProgressList)
                    {
                        foreach (var ed in examProgress.Exam.ExamDetails)
                        {
                            if (ed.SubjectType == 3)
                            {
                                examProgresses.Add(examProgress);
                                break;
                            }
                        }
                    }

                }
                else
                {
                    foreach (var examProgress in examProgressList)
                    {
                        foreach (var ed in examProgress.Exam.ExamDetails)
                        {
                            if (ed.Subject.Name.ToLower().Contains(subject.ToLower()))
                            {
                                examProgresses.Add(examProgress);
                                break;
                            }
                        }
                    }
                }
                return examProgresses.Distinct().Count();
            }
            return criteria.SetProjection(Projections.RowCount()).UniqueResult<int>();
        }
        public IList<DateTime> GetExamDate(long id, long? batchId = null)
        {
            var examProgress =
               Session.Query<ExamProgress>()
                   .Where(x => x.Exam.Id == id);
            if (batchId != null)
            {
                examProgress = examProgress.Where(x => x.Batch.Id == batchId);
            }

            var heldDate = examProgress.Select(x => x.HeldDate).ToList();
            return heldDate;
        }
        #endregion

        #region Helper Function
        
        #endregion

        
    }
}
