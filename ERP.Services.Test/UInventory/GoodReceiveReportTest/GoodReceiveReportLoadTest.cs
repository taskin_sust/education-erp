﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FluentNHibernate.Testing.Values;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Helper;
using Xunit;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Services.Test.UInventory.GoodReceiveReportTest
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReceiveDetails")]
    public class GoodReceiveReportLoadTest : GoodReceiveReportBaseTest
    {
        private AuthorizeBranchNameDelegate branchNameDelegate;

        public GoodReceiveReportLoadTest()
        {
            branchNameDelegate = new AuthorizeBranchNameDelegate(OnGettingAuthorizeBranchNameList);
        }

        #region Program wise Report

        [Fact]
        public void Should_Return_ProgramWiseReport_Successfully()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            IList<dynamic> list = _goodsReceiveDetailsService.LoadGoodReceiveProgramWise(0, 10, usermenu, _organizationFactory.Object.Id,
                _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
                _commonHelper.ConvertEnumToIdList<Purpose>(), _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
                _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
                _commonHelper.ConvertIdToList(_programFactory.Object.Id),
                _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
                new List<long>() { 0 }, (int)ReceiveType.Direct,
                (int)ReportType.ProgramWise, DateTime.Now.ToString(), DateTime.Now.ToString(), programSessionList);

            Assert.True(list.Count > 0);
        }

        [Fact]
        public void Should_Return_ProgramWiseReport_InvalidOrganization()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<InvalidDataException>(
                () => _goodsReceiveDetailsService.LoadGoodReceiveProgramWise(0, 10, usermenu, 0,
                    _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
                    _commonHelper.ConvertEnumToIdList<Purpose>(),
                    _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
                    _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
                    _commonHelper.ConvertIdToList(_programFactory.Object.Id),
                    _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
                    new List<long>() { 0 }, (int)ReceiveType.Direct,
                    (int)ReportType.ProgramWise, DateTime.Now.ToString(), DateTime.Now.ToString(), programSessionList));
        }

        [Fact]
        public void Should_Return_ProgramWiseReport_InvalidBranch()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
                () => _goodsReceiveDetailsService.LoadGoodReceiveProgramWise(0, 10, usermenu, _organizationFactory.Object.Id,
                    new List<long>(),
                    _commonHelper.ConvertEnumToIdList<Purpose>(),
                    _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
                    _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
                    _commonHelper.ConvertIdToList(_programFactory.Object.Id),
                    _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
                    new List<long>() { 0 }, (int)ReceiveType.Direct,
                    (int)ReportType.ProgramWise, DateTime.Now.ToString(), DateTime.Now.ToString(), programSessionList));
        }

        [Fact]
        public void Should_Return_ProgramWiseReport_InvalidUserMenu()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
                () => _goodsReceiveDetailsService.LoadGoodReceiveProgramWise(0, 10, null, _organizationFactory.Object.Id,
                    _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
                    _commonHelper.ConvertEnumToIdList<Purpose>(),
                    _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
                    _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
                    _commonHelper.ConvertIdToList(_programFactory.Object.Id),
                    _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
                    new List<long>() { 0 }, (int)ReceiveType.Direct,
                    (int)ReportType.ProgramWise, DateTime.Now.ToString(), DateTime.Now.ToString(), programSessionList));
        }

        #endregion

        #region Branch wise Report

        [Fact]
        public void Should_Return_BranchWiseReport_Successfully()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            IList<dynamic> list = _goodsReceiveDetailsService.LoadGoodReceiveBranchWise(branchNameDelegate, 0, 10, usermenu, _organizationFactory.Object.Id,
                _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
                _commonHelper.ConvertEnumToIdList<Purpose>(), _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
                _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
                _commonHelper.ConvertIdToList(_programFactory.Object.Id),
                _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
                new List<long>() { 0 }, (int)ReceiveType.Direct,
                (int)ReportType.BranchWise, DateTime.Now.ToString(), DateTime.Now.ToString(), programSessionList);

            Assert.True(list.Count > 0);
        }

        [Fact]
        public void Should_Return_BranchWiseReport_InvalidOrganization()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
               () => _goodsReceiveDetailsService.LoadGoodReceiveBranchWise(branchNameDelegate, 0, 10, usermenu, 0,
               _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
               _commonHelper.ConvertEnumToIdList<Purpose>(), _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
               _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
               _commonHelper.ConvertIdToList(_programFactory.Object.Id),
               _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
               new List<long>() { 0 }, (int)ReceiveType.Direct,
               (int)ReportType.BranchWise, DateTime.Now.ToString(), DateTime.Now.ToString(), programSessionList));

        }

        [Fact]
        public void Should_Return_BranchWiseReport_InvalidBranch()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
               () => _goodsReceiveDetailsService.LoadGoodReceiveBranchWise(branchNameDelegate, 0, 10, usermenu, _organizationFactory.Object.Id,
              new List<long>(),
               _commonHelper.ConvertEnumToIdList<Purpose>(), _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
               _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
               _commonHelper.ConvertIdToList(_programFactory.Object.Id),
               _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
               new List<long>() { 0 }, (int)ReceiveType.Direct,
               (int)ReportType.BranchWise, DateTime.Now.ToString(), DateTime.Now.ToString(), programSessionList));
        }

        [Fact]
        public void Should_Return_BranchWiseReport_InvalidUserMenu()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
               () => _goodsReceiveDetailsService.LoadGoodReceiveBranchWise(branchNameDelegate, 0, 10, null, _organizationFactory.Object.Id,
              _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
               _commonHelper.ConvertEnumToIdList<Purpose>(), _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
               _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
               _commonHelper.ConvertIdToList(_programFactory.Object.Id),
               _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
               new List<long>() { 0 }, (int)ReceiveType.Direct,
               (int)ReportType.BranchWise, DateTime.Now.ToString(), DateTime.Now.ToString(), programSessionList));
        }

        #endregion
    }
}
