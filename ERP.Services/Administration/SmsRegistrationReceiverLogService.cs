﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ISmsRegistrationReceiverLogService : IBaseService
    {
        #region Operational Function
        bool Save(SmsRegistrationReceiverLog srl);
        
        #endregion

        #region Single Instances Loading Function
       
        #endregion

        #region List Loading Function
        
        #endregion

        #region Others Function
       
        #endregion

        #region Helper Function
       
        #endregion
    }
    public class SmsRegistrationReceiverLogService : BaseService, ISmsRegistrationReceiverLogService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly SmsRegistrationReceiverLogDao _srlDao;
        public SmsRegistrationReceiverLogService(ISession session)
        {
            Session = session;
            _srlDao = new SmsRegistrationReceiverLogDao { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool Save(SmsRegistrationReceiverLog srl)
        {
            ITransaction trans = null;
            try
            {
               using (trans = Session.BeginTransaction())
                {
                    _srlDao.Save(srl);
                    trans.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        
        #endregion

        #region Single Instances Loading Function

        
        #endregion

        #region List Loading Function

        
        #endregion

        #region Others Function

       
        #endregion

        #region Helper function
        
        #endregion

        #region Public API

        #endregion
    }
}