﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.AcademicInfoTest
{
    public class AcademicInfoBaseTest:TestBase,IDisposable
    {
        internal ExamService _hrExamService;
        internal HrBoardService _hrBoardService;
        internal InstituteFactory _InstituteFactory;
        internal AcademicInfoService _academicInfoService;
        internal AcademicInfoFactory _academicInfoFactory;
        
        public AcademicInfoBaseTest()
        {
            _hrExamService = new ExamService(Session);
            _hrBoardService = new HrBoardService(Session);
            _InstituteFactory = new InstituteFactory(null, Session);
            _academicInfoService = new AcademicInfoService(Session);
            _academicInfoFactory = new AcademicInfoFactory(null, Session);
        }

        public void Dispose()
        {   
            _academicInfoFactory.CleanUp();
            teamMemberFactory.TeamMemberChieldCleanup();
            _InstituteFactory.Cleanup();
            base.Dispose();
        }
    }
}
