﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MenuGroupTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "MenuGroup")]
    public class MenuGroupUpdateTest : MenuGroupBaseTest
    {
        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var menugroupFactory =
                 _menugroupFactory.Create().WithNameAndRank(_name, 1).WithDisplayText(_displaytext)
                     .Persist();
            Assert.Throws<NullObjectException>(() => _menuService.Update(menuGroup:null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var menugroupFactory =
                _menugroupFactory.Create().WithNameAndRank(_name, 1).WithDisplayText(_displaytext)
                    .Persist();
            MenuGroup menuGroup = _menuService.LoadById(menugroupFactory.Object.Id);
            menuGroup.Name = menuGroup.Name + " DEF GHI";
            menuGroup.DisplayText = menuGroup.DisplayText + " 91011";
            var success = _menuService.Update(menuGroup);
            Assert.True(success);
        }

        //[Fact]
        //public void Update_Should_Check_Duplicate_Entry()
        //{
        //    MenuGroupFactory menugroupFactory =
        //        _menugroupFactory.Create()
        //            .WithNameAndRank(_name, 1)
        //            .WithDisplayText(_displaytext).Persist();
        //    var menugroup2 = new MenuGroup();
        //    menugroup2.Name = menugroupFactory.Object.Name;
        //    menugroup2.DisplayText = menugroupFactory.Object.DisplayText;
        //    menugroup2.BusinessId = menugroupFactory.Object.BusinessId;
        //    menugroup2.Id = 0;
        //    menugroup2.Name = menugroupFactory.Object.Name + " HJKHJKJK";
        //    menugroup2.DisplayText = menugroupFactory.Object.DisplayText + " 12445";
        //    _menuService.Save(menugroup2);
        //    menugroup2.Name = menugroupFactory.Object.Name;
        //    Assert.Throws<DuplicateEntryException>(() => _menuService.Update(menugroup2));
        //    _menugroupFactory.CleanupMenuGroup(new List<long>() { menugroup2.Id });
        //}
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Not_Update_Empty_Name()
        {
            var menugroupFactory =
                _menugroupFactory.Create().WithNameAndRank(_name, 1).WithDisplayText(_displaytext)
                    .Persist();
            menugroupFactory.Object.Name = "";
            Assert.Throws<EmptyFieldException>(() => _menuService.Update(menugroupFactory.Object));
        }
        [Fact]
        public void Should_Not_Update_Empty_displaytext()
        {
            var menugroupFactory =
                 _menugroupFactory.Create().WithNameAndRank(_name, 1).WithDisplayText(_displaytext)
                     .Persist();
            menugroupFactory.Object.DisplayText = "";
            Assert.Throws<EmptyFieldException>(() => _menuService.Update(menugroupFactory.Object));
        }

        #endregion
    }
}
