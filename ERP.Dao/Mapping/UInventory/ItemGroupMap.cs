using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class ItemGroupMap : BaseClassMap<ItemGroup, long>
    {
        public ItemGroupMap()
        {
            Table("UINV_ItemGroup");
            LazyLoad();
            Map(x => x.Rank).Column("Rank").Not.Nullable();
            Map(x => x.Name).Column("Name").Not.Nullable();
            HasMany(x => x.Items).KeyColumn("ItemGroupId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
