


function responseReturn() {
    
    var check = true;
    var programId = $("#Program_Id").val();
    var MaskNameValue = $("#MaskName").val();
    if (programId == "") {
        $("#Program_Id").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Program is required.");
        check = false;
    }
    else {
        $("#Program_Id").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
    if (MaskNameValue == "") {
        $("#MaskName").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Mask Name is required.");
        check = false;
    }
    else {
        $("#MaskName").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
    //var sessionId = $("#Session_Id").val();
    //if (sessionId == "") {
    //    $("#Session_Id").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Session is required.");
    //    check = false;
    //}
    //else {
    //    $("#Session_Id").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    //}
    var smsTypeId = $("#SmsType_Id").val();
    if (smsTypeId == "") {
        $("#SmsType_Id").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("SMS Type is required.");
        check = false;
    }
    else {
        $("#SmsType_Id").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }

    //if (smsTypeId > 6) {
    if ($('#SmsType_Id').attr('businessid') == 2) {
        var SmsTime_Hour = $("#SmsTime_Hour").val();
        var SmsTime_Time = $("#SmsTime_Time").val();
        if (SmsTime_Hour == "" || SmsTime_Time == "") {
            $("#SmsTime_Hour").parent().parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("SMS Time is required.");
            check = false;
        }
        else {
            $("#SmsTime_Hour").parent().parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        }
        var DayBefore = $("#DayBefore").val();
        if (DayBefore == "") {
            $("#DayBefore").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Day Before is required.");
            check = false;
        }
        else {
            $("#DayBefore").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        }
        var IsRepeat = $("#IsRepeat").is(':checked');
        if (IsRepeat == true) {
            var RepeatDuration = $("#RepeatDuration").val();
            var DayAfter = $("#DayAfter").val();
            if (RepeatDuration == "") {
                $("#RepeatDuration").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Repeat Duration is required.");
                check = false;
            }
            else {
                $("#RepeatDuration").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
            }
            if (DayAfter == "") {
                $("#DayAfter").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Day After is required.");
                check = false;
            }
            else {
                $("#DayAfter").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
            }
        }
        else {
            $("#RepeatDuration").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
            $("#DayAfter").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        }
    }

    var smsReceivers = $("#SmsReceivers").val();
    if (smsReceivers == null) {
        $("#SmsReceivers").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Please select one or more SMS receiver.");
        check = false;
    }
    else {
        $("#SmsReceivers").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
    var template = $("#Template").val();
    if (template == "") {
        $("#Template").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Template is required.");
        check = false;
    }
    else {
        $("#Template").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
    if (check) {
        console.log("SUBMIT");
        $("#smsForm").get(0).submit();
    }
}

$(document).on("change", "#Program_Id, #SmsType_Id", function (e) {
    var val = $(this).val();
    var attr = $(this).attr("id");
    var m = attr.slice(0, -3);
    if (val == "") {
        $(this).parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html(m+" is required.");      
    }
    else {
        $(this).parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
});

$(document).on("keyup", "#Template", function (e) {
    var val = $(this).val();
    var attr = $(this).attr("id");
    if (val == "") {
        $(this).parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Template is required.");
    }
    else {
        $(this).parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
});

$(document).on("change", "#SmsReceivers", function (e) {
    var val = $(this).val();
    var attr = $(this).attr("id");
    if (val == null) {
        $(this).parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Please select one or more SMS receiver.");
    }
    else {
        $(this).parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
});


//$(document).bind('keydown keyup', function (e) {
//    if (e.which === 116) {
//        console.log('blocked');
//        return false;
//    }
//    if (e.which === 82 && e.ctrlKey) {
//        console.log('blocked');
//        return false;
//    }
//});


$(document).on("click", "#DynamicOptions", function (e) {
   var text= $("#DynamicOptions option:selected").text();  
   var cursorPosition = $('#Template').prop("selectionStart");
   var message = $("#Template").val();
   var start = $('#Template').get(0).selectionStart;
   var finish = $('#Template').get(0).selectionEnd;
   var sel = message.substring(start, finish);
   if (sel == "")
   {     
       if (message.length == cursorPosition) {
           text = "[[{{" + text + "}}]]";
       }
       else {
           text = "[[{{" + text + "}}]]";
       }      
   }
   else
   {
       message = message.replace(sel, "");
       text = "[[{{" + text + "}}]]";
   }
  
    message = [message.slice(0, cursorPosition), text, message.slice(cursorPosition)].join(''); 
  // message = message + " " + text;
   $("#Template").val(message);
   $("#Template").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
   
});


$(document).ready(function () {
    var hideField = $("#hideField").val();
    if (hideField != undefined && hideField != null && hideField!="")
    {
        $(document).bind('keydown keyup', function (e) {
            if (e.which === 116) {
                console.log('blocked');
                return false;
            }
            if (e.which === 82 && e.ctrlKey) {
                console.log('blocked');
                return false;
            }
        });
    }
    
    //$('select').removeAttr('multiple');
    //$("select").attr("size", 5);

    $("#smsForm").submit(function() {
        return false;
    });


    //$('body').keydown(function (e) {
    //    if (e.keyCode == 13) {
    //        //responseReturn();
    //        //return false;
    //    }
    //});


    $("#submitForm").click(function () {        
        responseReturn();
    });

   


    //$("#Program_Id").change(function () {
    //    $("#Session_Id option").remove();
    //    $('#Session_Id').append("<option value=''>--Select Session--</option>");

    //    $.ajax({
    //        type: "post",
    //        url: $("body").attr("data-project-root") + "Administration/Sms/GetSesstion",
    //        cache: false,
    //        async: true,
    //        data: { "pId": $(this).val() },
    //        beforeSend: function () {
    //            $.blockUI({
    //                timeout: 0,
    //                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //            });
    //        },
    //        success: function (result) {
    //            $.unblockUI();
    //            if (result.IsSuccess) {
    //                $.each(result.returnList, function (i, v) {
    //                    $('#Session_Id').append($('<option>').text(v.Text).attr('value', v.Value));
    //                });
    //            }               
    //        },
    //        complete: function () {
    //            $.unblockUI();
    //        },
    //        error: function (result) {
    //            $.unblockUI();
    //            console.log("Failed -----");
    //            //$.fn.customMessage({
    //            //    displayMessage: "Menu Delete Failed",
    //            //    displayMessageType: "error",
    //            //});
    //        }
    //    });
    //});
    $('#IsRepeat').click(function () {
        var IsRepeat = $("#IsRepeat").is(':checked');
        if (IsRepeat == true) {
            $('#RepeatDuration').prop('disabled', false);
            $('#DayAfter').prop('disabled', false);
        }
        else {
            $("#RepeatDuration").val($("#target option:first").val());
            $('#RepeatDuration').prop('disabled', 'disabled');
            $("#DayAfter").val($("#target option:first").val());
            $('#DayAfter').prop('disabled', 'disabled');
        }
    });

    $("#SmsType_Id").change(function () {
        $("#DynamicOptions option").remove();
        
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/Sms/GetDynamicOptionsSmsType",
            cache: false,
            async: true,
            data: { "smsType": $(this).val() },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.each(result.returnList, function (i, v) {
                        $('#DynamicOptions').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                    if (result.smsTypeBusinessId == "2") {
                        $('#SmsType_Id').attr('businessid', 2);
                        $('#SmsTime_Hour').prop('disabled', false);
                        $('#SmsTime_Time').prop('disabled', false);
                        $('#DayBefore').prop('disabled', false);
                        $('#IsRepeat').prop('disabled', false);
                    }
                    else {
                        $('#SmsType_Id').attr('businessid', 1);
                        $("#SmsTime_Hour").val($("#target option:first").val());
                        $('#SmsTime_Hour').prop('disabled', 'disabled');
                        $("#SmsTime_Time").val($("#target option:first").val());
                        $('#SmsTime_Time').prop('disabled', 'disabled');
                        $("#DayBefore").val($("#target option:first").val());
                        $('#DayBefore').prop('disabled', 'disabled');
                        $('#IsRepeat').attr('checked', false);
                        $('#IsRepeat').prop('disabled', 'disabled');
                        $("#RepeatDuration").val($("#target option:first").val());
                        $('#RepeatDuration').prop('disabled', 'disabled');
                        $("#DayAfter").val($("#target option:first").val());
                        $('#DayAfter').prop('disabled', 'disabled');
                    }
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    });

    $("#Organization_Id").change(function () {
        $("#Program_Id option").remove();
        $('#Program_Id').append("<option value='All'>All</option>");
        var organizationId = $(this).val();
        if (organizationId != "" && organizationId != null) {
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Administration/Sms/GetProgramByOrganization",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                cache: false,
                async: true,
                data: { organizationIds: $(this).val(), isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.returnProgramList, function (i, v) {
                            $('#Program_Id').append($('<option>').text(v.Text).attr('value', v.Value));
                        });

                        //load Mask Name Start
                        $("#MaskName option").remove();
                        $.ajax({
                            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSmsMaskName",
                            type: "POST",
                            data: { organizationIds: organizationId, status: 1 },

                            beforeSend: function() {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function(responses) {
                                if (responses.IsSuccess) {
                                    $('#MaskName').append('<option value="">Select Mask Name</option>');
                                    $.each(responses.returnSmsMaskName, function (i, v) {
                                        $('#MaskName').append($('<option>').text(v.Text).attr('value', v.Value));
                                    });
                                }
                            },
                            complete: function() {
                                $.unblockUI();
                            },
                            error: function(responses) {
                                $.unblockUI();
                            }
                        });
                        //Load Mask Name End 
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
        
    });

});

