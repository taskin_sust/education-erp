﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Helper;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.CurrentStockReport
{

    [Trait("Area", "UInventory")]
    [Trait("Service", "CurrentStockSummary")]
    public class CurrentStockReportCountTest : CurrentStockReportBaseTest
    {
        private AuthorizeBranchNameDelegate branchNameDelegate;

        public CurrentStockReportCountTest()
        {
            branchNameDelegate = new AuthorizeBranchNameDelegate(OnGettingAuthorizeBranchNameList);
        }

        public void Should_Return_Successfully()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().WithCode(new Random().Next(99).ToString()).Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            int count = _currentStockSummaryService.CountCurrentStock(branchNameDelegate, usermenu,
                _organizationFactory.Object.Id,
                _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
                _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
                _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
                _commonHelper.ConvertIdToList(_programFactory.Object.Id),
                _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
                 programSessionList);

            Assert.True(count > 0);
        }

        [Fact]
        public void Should_Return_InvalidOrganization()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
               () => _currentStockSummaryService.CountCurrentStock(branchNameDelegate, usermenu, 0,
              _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
               _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
               _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
               _commonHelper.ConvertIdToList(_programFactory.Object.Id),
               _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
                programSessionList));

        }

        [Fact]
        public void Should_Return_BranchWiseReport_InvalidBranch()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
            () => _currentStockSummaryService.CountCurrentStock(branchNameDelegate, usermenu,
            _organizationFactory.Object.Id,
           new List<long>(),
            _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
            _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
            _commonHelper.ConvertIdToList(_programFactory.Object.Id),
            _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
             programSessionList));
        }

        [Fact]
        public void Should_Return_BranchWiseReport_InvalidUserMenu()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
            () => _currentStockSummaryService.CountCurrentStock(branchNameDelegate, null,
            _organizationFactory.Object.Id,
            _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
            _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
            _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
            _commonHelper.ConvertIdToList(_programFactory.Object.Id),
            _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
             programSessionList));
        }
    }
}
