﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class LeaveSettingController : Controller
    {
        #region Logger

        private ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ILeaveService _hrLeaveService;
        private readonly IOrganizationService _organizationService;
        private ISession _session;
        private readonly ICommonHelper _commonHelper;
        private readonly ILeaveApplicationService _hrLeaveApplicationService;
        private readonly ITeamMemberService _hrMemberService;
        private readonly IMembersLeaveSummaryService _hrMembersLeaveSummaryService;
        private readonly IEmploymentHistoryService _employmentHistoryService;
        private readonly IMaritalInfoService _hrMaritalInfoService;
        private List<UserMenu> _userMenu;
        // GET: HR/LeaveSetting
        
        public LeaveSettingController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _hrLeaveService = new LeaveService(_session);
            _organizationService = new OrganizationService(_session);
            _hrLeaveApplicationService = new LeaveApplicationService(_session);
            _hrMemberService = new TeamMemberService(_session);
            _hrMembersLeaveSummaryService = new MembersLeaveSummaryService(_session);
            _employmentHistoryService = new EmploymentHistoryService(_session);
            _hrMaritalInfoService = new MaritalInfoService(_session);
        }

        #endregion

        #region Administrative Leave Part

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LeaveSetting()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");

                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        [HttpPost]
        public JsonResult LeaveSetting(long org, string leaveName, string isPub, string payType, string repeatType, string noOfDays, string startFrom, string effectiveDate, string[] genAry, string[] empStatus, string[] maritalStatus, string isForward, string maxForward, string isTakingLimit, string maxTaking, string appType, string appTypeDays, string isEncash, string maxEncash, string closingDate = "")
        {
            try
            {
                string message;
                _hrLeaveService.LeaveSettingCheckAndSave(org, leaveName, isPub, payType, repeatType, noOfDays, startFrom, effectiveDate, genAry, empStatus, maritalStatus, isForward, maxForward, isTakingLimit, maxTaking, appType, appTypeDays, isEncash, maxEncash, closingDate = "");

                return Json(new { IsSuccess = true, Message = "Leave Setting Save Successfully" });
            }
            catch (InvalidDataException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new { IsSuccess = false, Message = WebHelper.SetExceptionMessage(e) });
            }
        }

        [HttpPost]
        public JsonResult LeaveSettingWithoutPay(long orgP, string leaveNameP, string isPubP, string payType = "", string[] genAryP = null, string appTypeP = "", string appTypeDaysP = "")
        {
            try
            {
                var leave = new Leave();
                if (orgP > 0)
                {
                    var organization = _organizationService.LoadById(Convert.ToInt64(orgP));
                    leave.Organization = organization;
                    if (!String.IsNullOrEmpty(leaveNameP)) { leave.Name = leaveNameP.Trim(); }
                    if (!String.IsNullOrEmpty(appTypeP))
                    {
                        leave.Applicationtype = Convert.ToInt32(appTypeP);
                        if (!String.IsNullOrEmpty(appTypeDaysP))
                            leave.ApplicationDays = Convert.ToInt32(appTypeDaysP.Trim());
                    }
                    leave.IsPublic = isPubP.Equals("1");
                    if (!String.IsNullOrEmpty(payType))
                    {
                        leave.PayType = payType.Equals("1") ? 1 : 0;
                    }
                }
                _hrLeaveService.Save(leave);
                return Json(new { IsSuccess = true, Message = "Leave Setting Save Successfully" });
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new { IsSuccess = false, Message = WebHelper.SetExceptionMessage(e) });
            }
        }

        public ActionResult ManageLeave(string message = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(message))
                {
                    ViewBag.SuccessMessage = message;
                }
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");

                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public ActionResult LeaveDatatableRender(int draw, int start, int length, long organizationId = 0)
        {
            var data = new List<object>();
            int recordsTotal = 0;
            int recordsFiltered = 0;
            try
            {
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                //if (organizationId > 0)
                //{
                //    leaveList = _hrLeaveService.GetLeaves(_userMenu, draw, start, length, organizationId);
                //}
                //else
                //{
                //    leaveList = _hrLeaveService.GetLeaves(_userMenu, draw, start, length, organizationId);
                //}
                var leaveList = _hrLeaveService.LoadLeave(_userMenu, organizationId, draw, start, length);

                //var recordsTotal = _hrLeaveService.GetLeaves(_userMenu, organizationId).Count;
                recordsTotal = _hrLeaveService.GetLeaveCount(_userMenu, organizationId);
                recordsFiltered = recordsTotal;
                foreach (var leaveObj in leaveList)
                {
                    var str = new List<string>();
                    str.Add(leaveObj.Organization.ShortName);
                    str.Add(!String.IsNullOrEmpty(leaveObj.Name) ? leaveObj.Name : "");
                    if (!String.IsNullOrEmpty(leaveObj.NoOfDays.ToString()))
                    {
                        str.Add(leaveObj.NoOfDays.ToString());
                    }
                    else
                    {
                        str.Add("");
                    }

                    if (leaveObj.RepeatType == 1)
                    {
                        str.Add("Every Year");
                    }
                    else if (leaveObj.RepeatType == 2)
                    {
                        str.Add("Once");
                    }
                    else if (leaveObj.RepeatType == 3)
                    {
                        str.Add("Max Two");
                    }
                    else
                    {
                        str.Add("");
                    }

                    if (leaveObj.PayType == 1)
                    {
                        str.Add("Yes");
                    }
                    else if (leaveObj.PayType == 0)
                    {
                        str.Add("No");
                    }
                    else
                    {
                        str.Add("");
                    }
                    if (leaveObj.IsCarryforward == true)
                    {
                        str.Add("Yes");
                    }
                    else if (leaveObj.IsCarryforward == false)
                    {
                        str.Add("No");
                    }
                    else
                    {
                        str.Add("");
                    }
                    str.Add(leaveObj.EffectiveDate != null ? leaveObj.EffectiveDate.Value.ToString("yyyy-MM-dd") : "");
                    str.Add(leaveObj.ClosingDate != null ? leaveObj.ClosingDate.Value.ToString("yyyy-MM-dd") : "");
                    str.Add(LinkGenerator.GetEditLink("EditLeaveSetting", "LeaveSetting", leaveObj.Id) +
                            LinkGenerator.GetDeleteLinkForModal(leaveObj.Id, leaveObj.Name));
                    data.Add(str);
                }
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });

        }

        #region Operation Function

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                var leave = _hrLeaveService.GetLeave(id);
                bool isSuccess = _hrLeaveService.Delete(id);

                if (isSuccess)
                {
                    ViewBag.SuccessMessage = "Leave deleted Successfully";
                    return Json(new Response(true, "Leave sucessfully deleted."));
                }
                else
                {
                    ViewBag.ErrorMessage = "Leave delete fail";
                    return Json(new Response(false, "Leave delete failed"));
                }
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, ex.Message));
            }
            catch (ValueNotAssignedException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, ex.Message));
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region update operation

        public ActionResult EditLeaveSetting(long id)
        {

            var dbLeave = _hrLeaveService.GetLeave(id);
            ViewBag.Organization = new SelectList(_organizationService.LoadOrganization(), "Id", "ShortName", dbLeave.Organization.Id);
            ViewBag.PublicOrPrivateType = new SelectList(LeaveSettingHelper.GetPublicOrPrivateLeaveType(), "Value", "Text", Convert.ToInt32(dbLeave.IsPublic).ToString());
            ViewBag.PayType = new SelectList(LeaveSettingHelper.GetPayType(), "Value", "Text", Convert.ToInt32(dbLeave.PayType).ToString());
            ViewBag.RepeatType = new SelectList(LeaveSettingHelper.RepeatType(), "Value", "Text", Convert.ToInt32(dbLeave.RepeatType).ToString());
            ViewBag.StartingFrom = new SelectList(LeaveSettingHelper.StartingFrom(), "Value", "Text", Convert.ToInt32(dbLeave.StartFrom).ToString());
            return View(dbLeave);
        }

        [HttpPost]
        public JsonResult EditLeaveSetting(long id, long org, string leaveName, string isPub, string payType, string repeatType, string noOfDays, string startFrom, string effectiveDate, string[] genAry, string[] empStatus, string[] maritalStatus, string isForward, string maxForward, string isTakingLimit, string maxTaking, string appType, string appTypeDays, string isEncash, string maxEncash, string closingDate = "")
        {
            try
            {
                var success = _hrLeaveService.Update(id, org, leaveName, isPub, payType, repeatType, noOfDays, startFrom, effectiveDate, genAry, empStatus, maritalStatus, isForward, maxForward, isTakingLimit, maxTaking, appType, appTypeDays, isEncash, maxEncash, closingDate);

                return Json(new { IsSuccess = true, Message = "Leave Setting edited Successfully" });
            }
            catch (MessageException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (ValueNotAssignedException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (DependencyException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, Message = WebHelper.SetExceptionMessage(ex) });
            }

        }

        #endregion

        #endregion

        #endregion
        
        #region Individual Leave Settings

        public ActionResult IndividualLeaveSetting(string message = "")
        {
            if (!String.IsNullOrEmpty(message))
            {
                ViewBag.ErrorMessage = message;
            }
            return View();
        }

        [HttpGet]
        public ActionResult IndividualLeave(string memPin, string message = "")
        {
            if (message.Trim() != "")
            {
                ViewBag.SuccessMessage = message;
            }
            try
            {
                //1.get member
                //calculate his/her leave summary if exits
                //else calculate his/her leave from HRleave table and save to HRMemberLeaveSummary and view this data
                IList<MemberLeaveSummary> previousLeaveSummaries = new List<MemberLeaveSummary>();
                IList<MemberLeaveSummary> memberLeaveSummaries = new List<MemberLeaveSummary>();
                int outerNum;
                var isValidmemberPin = int.TryParse(memPin.Trim(), out outerNum);
                Organization org;
                if (isValidmemberPin)
                {
                    TeamMember member = _hrMemberService.GetByPin(outerNum);
                    ViewBag.memberName = member.FullNameEng + " (" + member.Name + ")";
                    if (member != null)
                    {
                        var hrEmploymentHistory = member.EmploymentHistory.Where(x => x.EffectiveDate <= DateTime.Now && x.Status != EmploymentHistory.EntityStatus.Delete)
                                                    .OrderByDescending(x => x.EffectiveDate)
                                                    .Take(1)
                                                    .FirstOrDefault();
                        if (hrEmploymentHistory != null)
                        {
                            org = hrEmploymentHistory.Campus.Branch.Organization;
                            ViewBag.memberOrg = org.Name;
                            ViewBag.memberDept = hrEmploymentHistory.Department.Name;
                            ViewBag.memberDesig = hrEmploymentHistory.Designation.Name;
                            //int employeStatus = _employmentHistoryService.GetCurrentEmployementStatus(member.Id);
                            int employeStatus = hrEmploymentHistory.EmploymentStatus;
                            int maritalStatus = _hrMaritalInfoService.GetCurrentMaritalStatus(member.Id);
                            IList<MemberLeaveSummary> hrMemberLeaveSummaries = _hrMembersLeaveSummaryService.LoadByMemberAndYear(member.Id, DateTime.Now.Year);
                            IList<Leave> leaveTypesList = _hrLeaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(org.Id, member.Gender, employeStatus, maritalStatus);

                            if (!hrMemberLeaveSummaries.Any())
                            {
                                //calculate all leavetype that suits to this member
                                if (employeStatus != -404 && maritalStatus != -404)
                                {
                                    foreach (var hrLeav in leaveTypesList)
                                    {
                                        if (hrLeav.EffectiveDate != null)
                                        {
                                            var newSummary = new MemberLeaveSummary
                                            {
                                                //Name = hrLeav.Name,
                                                Leave = hrLeav,
                                                CarryForwardLeave = 0,
                                                CurrentYearLeave = hrLeav.NoOfDays.Value,
                                                EncashLeave = 0,
                                                VoidLeave = 0,
                                                TotalLeaveBalance = hrLeav.NoOfDays.Value,
                                                ApprovedLeave = 0,
                                                PendingLeave = 0,
                                                AvailableBalance = hrLeav.NoOfDays.Value,
                                                Year = hrLeav.EffectiveDate.Value.Year
                                            };
                                            memberLeaveSummaries.Add(newSummary);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (var previousLeaveSummary in hrMemberLeaveSummaries.Where(x => x.Leave.Organization.Id != org.Id).ToList())
                                {                            
                                    previousLeaveSummaries.Add(previousLeaveSummary);
                                }

                                foreach (var hrLeav in leaveTypesList)
                                {
                                    MemberLeaveSummary summary = _hrMembersLeaveSummaryService.GetLeaveSummary(org.Id, member.Id, hrLeav.Id);
                                    if (summary != null) { memberLeaveSummaries.Add(summary); }
                                    else
                                    {
                                        if (hrLeav.EffectiveDate != null)
                                        {
                                            var newSummary = new MemberLeaveSummary
                                            {
                                                //Name = hrLeav.Name,
                                                Leave = hrLeav,
                                                CarryForwardLeave = 0,
                                                CurrentYearLeave = hrLeav.NoOfDays.Value,
                                                EncashLeave = 0,
                                                VoidLeave = 0,
                                                TotalLeaveBalance = hrLeav.NoOfDays.Value,
                                                ApprovedLeave = 0,
                                                PendingLeave = 0,
                                                AvailableBalance = hrLeav.NoOfDays.Value,
                                                Year = hrLeav.EffectiveDate.Value.Year
                                            };
                                            memberLeaveSummaries.Add(newSummary);
                                        }
                                    }
                                }
                            }
                            ViewBag.Org = org;
                        }
                        ViewBag.Member = member;
                        ViewBag.Pin = memPin;
                        ViewBag.IndividualLeave = memberLeaveSummaries;
                        ViewBag.PreviousLeave = previousLeaveSummaries;
                        return View();
                    }
                    return RedirectToAction("IndividualLeaveSetting", new { message = "Invalid PIN!!" });
                    //
                }
                return RedirectToAction("IndividualLeaveSetting", new { message = "Invalid PIN!!" });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public JsonResult SetIndividualLeaveSetting(long orgId, long memberId, long[] leaveOrgId, long[] leaveId, int[] carryForwardLeave, int[] currentYearLeave, int[] encashLeave, int[] voidLeave, int[] totalLeaveBalance, int[] approvedLeave, int[] pendingLeave)
        {
            try
            {
                List<MemberLeaveSummary> addList = new List<MemberLeaveSummary>();
                List<MemberLeaveSummary> updateList = new List<MemberLeaveSummary>();
                TeamMember member = _hrMemberService.LoadById(memberId);
                if (member != null)
                {
                    var hrEmploymentHistory = member.EmploymentHistory.Where(x => x.EffectiveDate <= DateTime.Now)
                        .OrderByDescending(x => x.EffectiveDate)
                        .Take(1)
                        .FirstOrDefault();
                    if (hrEmploymentHistory != null)
                    {
                        //if (orgId != hrEmploymentHistory.Campus.Branch.Organization.Id)
                        //    throw new Exception("Data tampering occurred");
                        //int employeStatus = hrEmploymentHistory.EmploymentStatus;
                        //int maritalStatus = _hrMaritalInfoService.GetCurrentMaritalStatus(member.Id);
                        //IList<MemberLeaveSummary> hrMemberLeaveSummaries = _hrMembersLeaveSummaryService.LoadByMemberAndYear(member.Id, DateTime.Now.Year);
                        //IList<Leave> leaveTypesList = _hrLeaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(orgId, member.Gender, employeStatus, maritalStatus);
                        for (int i = 0; i < leaveId.Length; i++)
                        {
                            Leave tempLeave = _hrLeaveService.GetById(leaveId[i]);
                            if (tempLeave.IsPublic == false || tempLeave.PayType == (int) PayType.WithoutPay)
                                continue;
                            totalLeaveBalance[i] = (currentYearLeave[i] + carryForwardLeave[i]) - encashLeave[i];

                            if (tempLeave.IsCarryforward == true && totalLeaveBalance[i] > tempLeave.MaxCarryDays)
                                throw new Exception("Total Leave must be Less then or equal to " +
                                                    tempLeave.MaxCarryDays.ToString() + " for leave " + tempLeave.Name);
                            else if (tempLeave.IsCarryforward == false && totalLeaveBalance[i] > tempLeave.NoOfDays)
                                throw new Exception("Total Leave must be Less then or equal to " +
                                                    tempLeave.NoOfDays.ToString() + " for leave " + tempLeave.Name);

                            if (currentYearLeave[i] > tempLeave.NoOfDays)
                                throw new Exception("Current Year Leave must be Less then or equal to " +
                                                    tempLeave.NoOfDays.ToString() + " for leave " + tempLeave.Name);
                            if (encashLeave[i] > 0 && tempLeave.IsEncash == false)
                                throw new Exception("Encash is not possible for leave " + tempLeave.Name);
                            //if (tempLeave.IsEncash == true && encashLeave[i] > 0 &&
                            //    carryForwardLeave[i] - tempLeave.MinEncashReserveDays < encashLeave[i] + voidLeave[i])
                            //    throw new Exception("Encash or void calculation is not correct for leave " +
                            //                        tempLeave.Name);
                            if (tempLeave.IsEncash == true && encashLeave[i] > 0 && carryForwardLeave[i] < tempLeave.MinEncashReserveDays )
                                throw new Exception("Carryforoward must be greater then or equal to "+tempLeave.MinEncashReserveDays+" for leave " + tempLeave.Name);
                            if (totalLeaveBalance[i] < (approvedLeave[i] + pendingLeave[i]))
                                throw new Exception(
                                    "Total balance must be grater then or equal to the sum of taken and pending leave" +
                                    " for leave " + tempLeave.Name);
                            if (carryForwardLeave[i] > 0 && tempLeave.IsCarryforward == false)
                                throw new Exception("Carry forward is not available for leave " + tempLeave.Name);

                            //if ((currentYearLeave[i] + carryForwardLeave[i]) - (encashLeave[i] - voidLeave[i]) != totalLeaveBalance[i])
                            //    throw new Exception("Incorrect calculation for leave " + tempLeave.Name);


                            //MemberLeaveSummary leaveSummary = _hrMembersLeaveSummaryService.GetLeaveSummary(orgId, memberId, leaveId[i]);
                            MemberLeaveSummary leaveSummary =
                                _hrMembersLeaveSummaryService.GetLeaveSummary(leaveOrgId[i], memberId, leaveId[i]);
                            if (leaveSummary == null)
                            {
                                //add new leave summary
                                MemberLeaveSummary newSummary = new MemberLeaveSummary();
                                newSummary.CarryForwardLeave = carryForwardLeave[i];
                                newSummary.CurrentYearLeave = currentYearLeave[i];
                                newSummary.EncashLeave = encashLeave[i];
                                newSummary.VoidLeave = voidLeave[i];
                                newSummary.TotalLeaveBalance = carryForwardLeave[i]+currentYearLeave[i];//totalLeaveBalance[i];
                                newSummary.ApprovedLeave = 0;
                                newSummary.PendingLeave = 0;
                                newSummary.AvailableBalance = carryForwardLeave[i]+currentYearLeave[i];//totalLeaveBalance[i] - (approvedLeave[i] + pendingLeave[i]);

                                newSummary.Leave = tempLeave;
                                newSummary.TeamMember = _hrMemberService.LoadById(memberId);
                                newSummary.Organization = _organizationService.LoadById(orgId);
                                newSummary.Status = MemberLeaveSummary.EntityStatus.Active;
                                newSummary.Year = DateTime.Now.Year; //newSummary.Leave.EffectiveDate.Value.Year;
                                addList.Add(newSummary);
                            }
                            else
                            {
                                //update leave summary
                                leaveSummary.CarryForwardLeave = carryForwardLeave[i];
                                leaveSummary.CurrentYearLeave = currentYearLeave[i];
                                leaveSummary.EncashLeave = encashLeave[i];
                                leaveSummary.VoidLeave = voidLeave[i];
                                leaveSummary.TotalLeaveBalance = carryForwardLeave[i] + currentYearLeave[i];//totalLeaveBalance[i];
                                leaveSummary.AvailableBalance = (carryForwardLeave[i] + currentYearLeave[i]) - (leaveSummary.ApprovedLeave + leaveSummary.PendingLeave); //totalLeaveBalance[i] - (leaveSummary.ApprovedLeave + leaveSummary.PendingLeave);
                                updateList.Add(leaveSummary);
                            }
                        }

                    }
                }

                #region OLD Code

                //List<Dictionary<long, int>> listOfDic = new List<Dictionary<long, int>>();
                //foreach (var leaveTypeVal in genAryP)
                //{
                //    Dictionary<long, int> dic = new Dictionary<long, int>();
                //    var arr = leaveTypeVal.Split('#');
                //    long leaveId;
                //    int totalLeave;
                //    bool isValidLeaveId = long.TryParse(arr[0], out leaveId);
                //    bool isValidTotalLeave = int.TryParse(arr[1], out totalLeave);
                //    if (isValidLeaveId && isValidTotalLeave)
                //    {
                //        dic[leaveId] = totalLeave;
                //        listOfDic.Add(dic);
                //    }
                //}
                //foreach (var dicObj in listOfDic)
                //{
                //    foreach (KeyValuePair<long, int> keyValuePair in dicObj)
                //    {
                //        MemberLeaveSummary leaveSummary = _hrMembersLeaveSummaryService.GetLeaveSummary(orgId,
                //            memberId, keyValuePair.Key);
                //        if (leaveSummary == null)
                //        {
                //            //add new leave summary
                //            MemberLeaveSummary newSummary = new MemberLeaveSummary();
                //            newSummary.ApprovedLeave = 0;
                //            newSummary.CarryForwardLeave = 0;
                //            newSummary.CurrentYearLeave = keyValuePair.Value;
                //            newSummary.EncashLeave = 0;
                //            newSummary.Leave = _hrLeaveService.GetById(keyValuePair.Key);
                //            newSummary.TeamMember = _hrMemberService.LoadById(memberId);
                //            newSummary.Organization = _organizationService.LoadById(orgId);
                //            newSummary.PendingLeave = 0;
                //            newSummary.Status = MemberLeaveSummary.EntityStatus.Active;
                //            newSummary.TotalLeaveBalance = keyValuePair.Value;
                //            newSummary.AvailableBalance = keyValuePair.Value;
                //            //Debug.Assert(newSummary.Leave.EffectiveDate != null, "newSummary.Leave.EffectiveDate != null");
                //            newSummary.Year = DateTime.Now.Year;//newSummary.Leave.EffectiveDate.Value.Year;
                //            addList.Add(newSummary);
                //        }
                //        else
                //        {
                //            //update leave summary
                //            leaveSummary.TotalLeaveBalance = keyValuePair.Value;
                //            leaveSummary.AvailableBalance = keyValuePair.Value;
                //            leaveSummary.CurrentYearLeave = keyValuePair.Value;
                //            updateList.Add(leaveSummary);
                //        }
                //    }
                //}

                #endregion

                List<String> errorMessage = _hrMembersLeaveSummaryService.SaveAndUpdateIndividualLeave(addList,
                    updateList);
                if (errorMessage.Count > 0)
                {
                    var mess = errorMessage.Aggregate("", (current, message) => current + (message + " \n "));
                    return Json(new Response(false, mess));
                }
                return Json(new Response(true, "Successfully Leave Set"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, WebHelper.SetExceptionMessage(e, e.Message)));
            }
        }

        #endregion
    }
}