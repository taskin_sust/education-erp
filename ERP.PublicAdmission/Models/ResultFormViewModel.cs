﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UdvashERP.PublicAdmission.Models
{
    public class ResultFormViewModel
    {
        [Required]
        [Display(Name = "Program Roll :")]
        public virtual string PrnNo { get; set; }
        [Required]
        [Display(Name = "Course :")]
        public virtual long SelectedCourse { get; set; }

        [Required]
        [Display(Name = "Exam :")]
        public virtual long ExamId { get; set; }

    }
}