﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class LectureController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ICommonHelper _commonHelper;
        private readonly ICourseSubjectService _courseSubjectService;
        private readonly IOrganizationService _organizationService;
        private readonly ILectureService _lectureService;
        private List<UserMenu> _userMenu;
        private ILectureSettingService _lectureSettingService;

        public LectureController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _courseService = new CourseService(session);
                _commonHelper = new CommonHelper();
                _courseSubjectService = new CourseSubjectService(session);
                _organizationService = new OrganizationService(session);
                _lectureService = new LectureService(session);
                _lectureSettingService = new LectureSettingService(session);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        #endregion

        #region Index/Manage Page

        public ActionResult Manage()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                var organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.Organization = organization;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            ViewBag.PageSize = Constants.PageSize;
            return View();

        }

        [HttpPost]
        public JsonResult LectureListResult(int draw, int start, int length, string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix)
        {
            var data = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    NameValueCollection nvc = Request.Form;

                    string orderBy = "";
                    string orderDir = "";
                    
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                            case "1":
                                orderBy = "Organization";
                                break;
                            case "2":
                                orderBy = "Program";
                                break;
                            case "3":
                                orderBy = "Session";
                                break;
                            case "4":
                                orderBy = "Course";
                                break;
                            case "5":
                                orderBy = "Subject";
                                break;
                            case "6":
                                orderBy = "NumberOfClasses";
                                break;
                            case "7":
                                orderBy = "ClassNamePrefix";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }
                    recordsTotal = _lectureSettingService.LectureSettingsRowCount(_userMenu, organization, program, session, course, numberOfClasses, classNamePrefix);
                    recordsFiltered = recordsTotal;
                    List<LectureSettings> lectureSettingsList = _lectureSettingService.GetLectureSettingsList(start, length, orderBy, orderDir.ToUpper(), _userMenu, organization, program, session, course, numberOfClasses, classNamePrefix).ToList();
                    int sl = start + 1;
                    foreach (var l in lectureSettingsList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        str.Add(l.Course.Program.Organization.ShortName);
                        str.Add(l.Course.Program.Name);
                        str.Add(l.Course.RefSession.Name);
                        str.Add(l.Course.Name);
                        if (l.CourseSubject != null)
                        {
                            str.Add(l.CourseSubject.Subject.Name);
                        }
                        else
                        {
                            str.Add("Combined");
                        }
                        str.Add(l.ClassNamePrefix);
                        str.Add(l.NumberOfClasses.ToString());
                        str.Add(LinkGenerator.GetEditLink("Edit", "Lecture", l.Id) + LinkGenerator.GetDeleteLinkForModal(l.Id, ""));
                        //<a href='" + Url.Action("Details", "Lecture") + "?id=" + l.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;
                        //str.Add("<a href='" + Url.Action("Edit", "Lecture") + "?id=" + l.Id.ToString() + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;<a id='" + l.Id.ToString() + "' href='#'  class='glyphicon glyphicon-trash'> </a>");
                        data.Add(str);
                        sl++;
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }
        #endregion

        #region Operational Function

        #region Save Operation

        [HttpGet]
        public ActionResult LecturePlan()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.Organization = organization;
                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult SaveLecturePlan(IList<LectureSettingsViewModel> data)
        {
            if (data.Any())
            {
                try
                {
                    IList<LectureSettings> lectureSettingList = new List<LectureSettings>();
                    foreach (var d in data)
                    {
                        LectureSettings lectureSetting = new LectureSettings();
                        lectureSetting.CourseSubject = _courseSubjectService.LoadCourseSubjectById(Convert.ToInt64(d.CourseSubjectId));
                        lectureSetting.NumberOfClasses = Convert.ToInt32(d.NumberOfClasses);
                        lectureSetting.ClassNamePrefix = d.ClassNamePrefix;
                        lectureSetting.Course =
                            _courseService.LoadCourse(new[] { Convert.ToInt64(d.CourseId) }).SingleOrDefault();
                        lectureSettingList.Add(lectureSetting);
                    }
                    _lectureService.Save(lectureSettingList);
                    return Json(new Response(true, "Lecture Plan Save Successful"));
                }
                catch (DuplicateEntryException ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, ex.Message));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            return Json(new Response(true, WebHelper.CommonErrorMessage));
        }

        #endregion

        #region Update Operation

        public ActionResult Edit(long id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                LectureSettings lectureSettingsModel = _lectureSettingService.LoadById(id);
                ViewBag.LectureSettingsModel = lectureSettingsModel;
                return View();
            }
            catch (Exception ex)
            {
                var lectureSettingsModel = new LectureSettings();
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                ViewBag.LectureSettingsModel = lectureSettingsModel;
                return View();
            }
        }

        [HttpPost]
        public ActionResult Edit(long id, string classNamePrefix, string numberOfClasses)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (id == 0)
                        return Json(new Response(false, "Lecture Plan Update fail"));
                    if (String.IsNullOrEmpty(classNamePrefix))
                        return Json(new Response(false, "Lecture Name Prefix is required"));
                    if (String.IsNullOrEmpty(numberOfClasses))
                        return Json(new Response(false, "Number Of Classes is required"));

                    int numberOfClassesInt;
                    bool isNumerical = int.TryParse(numberOfClasses, out numberOfClassesInt);

                    if (!(isNumerical) || numberOfClassesInt <= 0)
                        return Json(new Response(false, "Number Of Classes is Invalid"));
                    LectureSettings lectureSettingsModel = _lectureSettingService.LoadById(id);
                    if (lectureSettingsModel == null)
                        return Json(new Response(false, WebHelper.CommonErrorMessage));
                    _lectureService.Update(lectureSettingsModel, numberOfClassesInt, classNamePrefix);
                    return Json(new Response(true, "Lecture Plan Update Successful"));
                }
                catch (DependencyException ex)
                {
                    return Json(new Response(false, ex.Message));
                }
                catch (DuplicateEntryException ex)
                {
                    return Json(new Response(false, ex.Message));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                    return View();
                }
            }
            else
            {
                return Json(HttpNotFound());
            }
        }

        #endregion

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                LectureSettings deleteLecture = _lectureSettingService.LoadById(id);
                deleteLecture.Status = LectureSettings.EntityStatus.Delete;
                _lectureService.Delete(deleteLecture);
                return Json(new Response(true, "Lecture Settings Delete Successful !"));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #endregion

        #region Helper Function

        [HttpPost]
        public ActionResult GetSubjectByCourseSessionProgram(string programId, string sessionId, string courseId, string action = null)
        {
            try
            {
                if (String.IsNullOrEmpty(programId)) return Json(new Response(false, "Invalid Program"));
                if (String.IsNullOrEmpty(sessionId)) return Json(new Response(false, "Invalid Session"));
                if (String.IsNullOrEmpty(courseId)) return Json(new Response(false, "Invalid Course"));

                Course courseinformation = _courseService.GetCourse(Convert.ToInt64(courseId));

                if (!string.IsNullOrEmpty(action))
                {
                    ViewBag.Courseinformation = courseinformation;
                    return PartialView("Partial/_SubjectList");
                }
                else
                {
                    IList<SelectListItem> subjectList = new SelectList(courseinformation.CourseSubjects, "Subject.Id", "Subject.Name").ToList();
                    return Json(new { returnList = subjectList, IsSuccess = true });
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion
    }
}