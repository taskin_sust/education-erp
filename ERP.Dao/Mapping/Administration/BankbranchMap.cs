using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class BankbranchMap : BaseClassMap<BankBranch, long> {
        
        public BankbranchMap() {
			Table("BankBranch");
			LazyLoad();
			References(x => x.Bank).Column("BankId");
			Map(x => x.Rank).Column("Rank").Not.Nullable();
			Map(x => x.Name).Column("Name").Not.Nullable();
			Map(x => x.ShortName).Column("ShortName");
			Map(x => x.ContactPerson).Column("ContactPerson");
			Map(x => x.ContactPersonMobile).Column("ContactPersonMobile");
			Map(x => x.RoutingNo).Column("RoutingNo");
			Map(x => x.SwiftCode).Column("SwiftCode");
            HasMany(x => x.SupplierBankDetails).KeyColumn("BankBranchId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
