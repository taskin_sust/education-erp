﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using Microsoft.AspNet.Identity;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Teachers;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Teachers.Controllers
{
    [UerpArea("Teachers")]
    [Authorize]
    [AuthorizeAccess]
    public class TeacherScriptEvaluationController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeachersArea");
        private void LogError(string message, Exception exception)
        {
            if (exception != null)
            {
                _logger.Error(message, exception);
            }
            else
            {
                _logger.Error(message);
            }
        }
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ITeacherService _teacherService;
        private readonly IBranchService _branchService;
        private readonly ITeacherScriptEvaluationService _teacherScriptEvaluationService;
        private readonly ITeacherScriptEvaluationDetailsService _teacherScriptEvaluationDetailsService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserService _userService;
        private List<UserMenu> _userMenu;

        public TeacherScriptEvaluationController()
        {
            var nHSession = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(nHSession);
            _programService =new ProgramService(nHSession);
            _sessionService = new SessionService(nHSession);
            _courseService = new CourseService(nHSession);
            _teacherService = new TeacherService(nHSession);
            _branchService = new BranchService(nHSession);
            _teacherScriptEvaluationService = new TeacherScriptEvaluationService(nHSession);
            _teacherScriptEvaluationDetailsService = new TeacherScriptEvaluationDetailsService(nHSession);
            _userService = new UserService(nHSession);
            _commonHelper = new CommonHelper();

        }
        #endregion

        #region Manage/Index 

        [HttpGet]
        public ActionResult Index()
        {
            InitializeIndex();
            return View();
        }

        [HttpPost]
        public ActionResult Index(TeacherScriptEvaluationManageViewModel teacherScriptEvaluationManageViewModel)
        {
            InitializeIndex();
            try
            {
                if (teacherScriptEvaluationManageViewModel != null)
                {
                    
                    var json = new JavaScriptSerializer().Serialize(teacherScriptEvaluationManageViewModel);

                    var cList = new SelectList(_courseService.LoadCourse(_commonHelper.ConvertIdToList(teacherScriptEvaluationManageViewModel.OrganizationId), _commonHelper.ConvertIdToList(teacherScriptEvaluationManageViewModel.ProgramId), _commonHelper.ConvertIdToList(teacherScriptEvaluationManageViewModel.SessionId)), "Id", "Name");

                    ViewData["CourseName"] = cList;

                    var bList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(teacherScriptEvaluationManageViewModel.OrganizationId), _commonHelper.ConvertIdToList(teacherScriptEvaluationManageViewModel.ProgramId), _commonHelper.ConvertIdToList(teacherScriptEvaluationManageViewModel.SessionId)), "Id", "Name");

                    List<SelectListItem> branchList = bList.ToList();
                    branchList.Insert(0, new SelectListItem() { Value = "-1", Text = "Common" });
                    ViewData["BranchIds"] = new SelectList(branchList, "Value", "Text"); 
                    
                    ViewData["teacherScriptEvaluationManageViewModel"] = json;
                    return View("teacherScriptEvaluationReport");
                }
                
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #endregion

        #region Details
        public ActionResult Details(int id)
        {
            TeacherScriptEvaluationDetails teacherScDetail = _teacherScriptEvaluationDetailsService.GetTeacherScriptEvaluationDetailById(Convert.ToInt64(id));
            if (teacherScDetail != null)
            {
                teacherScDetail.CreateByText =_userService.GetUserNameByAspNetUserId(teacherScDetail.CreateBy);
                teacherScDetail.ModifyByText = _userService.GetUserNameByAspNetUserId(teacherScDetail.ModifyBy);
            }
            return View(teacherScDetail);
        }
        #endregion

        #region Save Operation
        public ActionResult Create()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.CourseId = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.BranchId = new SelectList(new List<Branch>(), "Id", "Name");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                
            }
            return View();
        }
        [HttpPost]
        public ActionResult Create(TeacherScriptEvaluationViewModel teacherScriptEvaluationViewModel)
        {
            try
            {
                if(teacherScriptEvaluationViewModel==null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                if(teacherScriptEvaluationViewModel.TeacherScriptEvaluationDetailsViewModels==null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                Organization organization = _organizationService.LoadById(teacherScriptEvaluationViewModel.OrganizationId);
                if(organization==null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));

                Program program = _programService.GetProgram(teacherScriptEvaluationViewModel.ProgramId);
                if(program==null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));

                Session session = _sessionService.LoadById(teacherScriptEvaluationViewModel.SessionId);
                if(session==null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));

                Course course = _courseService.GetCourse(teacherScriptEvaluationViewModel.CourseId);
                if (course==null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));

                var teacherScriptEvaluation = new TeacherScriptEvaluation();
                teacherScriptEvaluation.Organization = organization;
                teacherScriptEvaluation.Program = program;
                teacherScriptEvaluation.Session = session;
                teacherScriptEvaluation.Course = course;
                teacherScriptEvaluation.SerialNumber = _teacherScriptEvaluationService.GetSerialNumber();
                teacherScriptEvaluation.SubmissionDate = teacherScriptEvaluationViewModel.SubmissionDate;
                
                foreach (var detailsViewModel in teacherScriptEvaluationViewModel.TeacherScriptEvaluationDetailsViewModels)
                {
                    var teacherScriptEvaluationDetails = new TeacherScriptEvaluationDetails();
                    teacherScriptEvaluationDetails.TeacherScriptEvaluation = teacherScriptEvaluation;
                    teacherScriptEvaluationDetails.Status = TeacherAcademicMaterialsDetails.EntityStatus.Active;
                    teacherScriptEvaluationDetails.CreateBy =
                        Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    teacherScriptEvaluationDetails.ModifyBy =
                        Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    Teacher teacher = _teacherService.GetTeacher(detailsViewModel.TeacherId);
                    if (teacher == null)
                        return Json(new Response(false, "'" + detailsViewModel.TeacherName + "' invalid teacher.Please correct teacher name."));

                    teacherScriptEvaluationDetails.Teacher = teacher;//_teacherService.GetTeacher(detailsViewModel.TeacherId);
                    //for common branch start
                    if (detailsViewModel.Branch != -1)
                    {
                        Branch branch = _branchService.GetBranch(detailsViewModel.Branch);
                        if (branch == null)
                            return Json(new Response(false, WebHelper.CommonErrorMessage));
                        teacherScriptEvaluationDetails.ForBranch = branch;//_branchService.GetBranch(detailsViewModel.Branch);
                    }
                    else if (detailsViewModel.Branch == -1)
                    {
                        teacherScriptEvaluationDetails.ForBranch = null;
                    }
                    else
                    {
                        return Json(new Response(false, WebHelper.CommonErrorMessage));
                    }
                    //for common branch end
                    
                    teacherScriptEvaluationDetails.Description = detailsViewModel.Description;
                    if (detailsViewModel.Quantity>99999999)
                        return Json(new Response(false, "Quantity must be 8 digit"));
                    teacherScriptEvaluationDetails.Quantity = detailsViewModel.Quantity;
                    if (detailsViewModel.Amount>99999999)
                        return Json(new Response(false, "Amount must be 8 digit"));
                    teacherScriptEvaluationDetails.Amount = detailsViewModel.Amount;
                    teacherScriptEvaluationDetails.PaymentStatus = TeacherPaymentStatus.Due;
                    teacherScriptEvaluation.TeacherScriptEvaluationDetails.Add(teacherScriptEvaluationDetails);
                }
                bool isSuccess = _teacherScriptEvaluationService.Save(teacherScriptEvaluation);
                if(isSuccess)
                    return Json(new { returnSuccess = "Successfully Save. Tracking Id: S" + teacherScriptEvaluation.Id, IsSuccess = true });

                else return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(int id)
        {
            #region old code
            //try
            //{
            //    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //    TeacherScriptEvaluationDetails teacherScDetail = _teacherScriptEvaluationDetailsService.GetTeacherScriptEvaluationDetailById(Convert.ToInt64(id));
            //    if (teacherScDetail != null)
            //    {
            //        var teacherScEval = new TeacherScriptEvaluationViewModel();
            //        teacherScEval.OrganizationId = teacherScDetail.ForBranch.Organization.Id;
            //        teacherScEval.ProgramId = teacherScDetail.TeacherScriptEvaluation.Program.Id;
            //        teacherScEval.SessionId = teacherScDetail.TeacherScriptEvaluation.Session.Id;
            //        teacherScEval.CourseId = teacherScDetail.TeacherScriptEvaluation.Course.Id;
            //        teacherScEval.SubmissionDate = teacherScDetail.TeacherScriptEvaluation.SubmissionDate;
            //        teacherScEval.SerialNumber = teacherScDetail.TeacherScriptEvaluation.SerialNumber;
            //        teacherScEval.FBranchId = teacherScDetail.ForBranch.Id;

            //        var orgList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu),
            //            "Id", "ShortName", teacherScDetail.TeacherScriptEvaluation.Program.Organization.Id);
            //        List<SelectListItem> listOrg = orgList.ToList();
            //        ViewBag.OrganizationIds = new SelectList(listOrg,"Value","Text");


            //        var prList = new SelectList(_programService.LoadAuthorizedProgram(_userMenu,
            //            _commonHelper.ConvertIdToList(teacherScDetail.TeacherScriptEvaluation.Program.Organization.Id)), "Id", "Name", teacherScDetail.TeacherScriptEvaluation.Program.Id);
                    
            //        ViewBag.ProgramIds = prList;
                    
            //        var sessionList = new SelectList(_sessionService.LoadAuthorizedSession(_userMenu,
            //            _commonHelper.ConvertIdToList(teacherScDetail.TeacherScriptEvaluation.Program.Organization.Id)), "Id", "Name", teacherScDetail.TeacherScriptEvaluation.Session.Id);
            //         ViewBag.SessionIds = sessionList;

            //        var courseList = new SelectList(_courseService.LoadCourse(_commonHelper.ConvertIdToList(teacherScDetail.TeacherScriptEvaluation.Program.Organization.Id), _commonHelper.ConvertIdToList(teacherScDetail.TeacherScriptEvaluation.Program.Id), _commonHelper.ConvertIdToList(teacherScDetail.TeacherScriptEvaluation.Session.Id)), "Id", "Name",
            //            teacherScDetail.TeacherScriptEvaluation.Course.Id);
            //        ViewBag.CourseIds = courseList;
            //        var brList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(teacherScDetail.TeacherScriptEvaluation.Program.Organization.Id),
            //            _commonHelper.ConvertIdToList(teacherScDetail.TeacherScriptEvaluation.Program.Id), _commonHelper.ConvertIdToList(teacherScDetail.TeacherScriptEvaluation.Session.Id)), 
            //            "Id", "Name", teacherScDetail.ForBranch.Id);

            //        ViewBag.FBranchIds = brList;
                    
            //        var teacherScEvalDetails = new TeacherScriptEvaluationDetailsViewModel();
            //        teacherScEval.TeacherScriptEvaluationDetailsViewModels=new List<TeacherScriptEvaluationDetailsViewModel>();
            //        teacherScEvalDetails.dId = teacherScDetail.Id;
            //        teacherScEvalDetails.TeacherId = teacherScDetail.Teacher.Id;
            //        teacherScEvalDetails.TeacherName = teacherScDetail.Teacher.FullName;
            //        teacherScEvalDetails.Branch = teacherScDetail.ForBranch.Id;
            //        teacherScEvalDetails.Description = teacherScDetail.Description;
            //        teacherScEvalDetails.Quantity = teacherScDetail.Quantity;
            //        teacherScEvalDetails.Amount = teacherScDetail.Amount;
            //        teacherScEval.TeacherScriptEvaluationDetailsViewModels.Add(teacherScEvalDetails);
                    

            //        return View(teacherScEval);
            //    }
            //    else
            //    {
            //        return HttpNotFound();
            //    }

            //}
            //catch (Exception ex) 
            //{
            //    _logger.Error(ex);
            //    throw;
            //}
            #endregion
            try
            {
                long cId;
                bool isNum = long.TryParse(id.ToString(), out cId);
                if (!isNum)
                    return HttpNotFound();

                var teacherScDetail = _teacherScriptEvaluationDetailsService.GetTeacherScriptEvaluationDetailById(Convert.ToInt64(id));
                return View(teacherScDetail);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        [HttpPost]
        public ActionResult Edit(long id, string description,string quantity, string amount)//TeacherScriptEvaluationViewModel teacherScViewModel)
        {
            
            long cId;
            bool isNum = long.TryParse(id.ToString(), out cId);
            if (!isNum)
            {
                return HttpNotFound();
            }
            TeacherScriptEvaluationDetails teacherScDetail = _teacherScriptEvaluationDetailsService.GetTeacherScriptEvaluationDetailById(id);
            try
            {
                if (!String.IsNullOrEmpty(description) && !String.IsNullOrEmpty(quantity) &&
                    !String.IsNullOrEmpty(amount))
                {
                    teacherScDetail.Description = description;
                    teacherScDetail.Quantity = Convert.ToInt32(quantity);
                    teacherScDetail.Amount = Convert.ToDecimal(amount);
                    bool isSuccess = _teacherScriptEvaluationService.Update(teacherScDetail);
                    if (isSuccess)
                        ViewBag.SuccessMessage = "Successfully updated.";
                    else
                        ViewBag.ErrorMessage = "Problem Occurred, during update.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Problem Occurred, during update.";
                }

                #region old code
                //if (teacherScViewModel != null)
            //{
            //    foreach (var data in teacherScViewModel.TeacherScriptEvaluationDetailsViewModels)
            //    {
            //        TeacherScriptEvaluationDetails teacherScDetail =
            //            _teacherScriptEvaluationDetailsService.GetTeacherScriptEvaluationDetailById(
            //                Convert.ToInt64(data.dId));
            //        teacherScDetail.Description = data.Description;
            //        teacherScDetail.Quantity = data.Quantity;
            //        teacherScDetail.Amount = data.Amount;
            //        bool isSuccess = _teacherScriptEvaluationService.Update(teacherScDetail);
            //        if (isSuccess)
            //            ViewBag.SuccessMessage = "Successfully updated.";
            //        else
            //            ViewBag.ErrorMessage = "Problem Occurred, during update.";
            //    }

            //}

                //return View(teacherScViewModel);
                #endregion
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
            }
            return View(teacherScDetail);
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                bool isSuccess = _teacherScriptEvaluationService.Delete(id);
                return Json(isSuccess ? new Response(true, "Script Evaluation sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Teacher Script Evaluation Delete Fail !"));
            }
        }
        #endregion

        #region AjaxRequest

        [HttpPost]
        public JsonResult AjaxRequestForTeacherScriptEvaluationReport(int draw, int start, int length, string teacherScriptEvaluationManageViewModel, string date, long? selectCourseId, string teacherName = "", long? selectBranchId = null, string trackingId = "", string tpin = "")
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (!String.IsNullOrEmpty(teacherScriptEvaluationManageViewModel))
                {
                    TeacherScriptEvaluationManageViewModel teacherScriptEvaluationManageViewModels = new JavaScriptSerializer().Deserialize<TeacherScriptEvaluationManageViewModel>(teacherScriptEvaluationManageViewModel);

                    long totalScriptEvaluation = _teacherScriptEvaluationDetailsService.CountTeacherScriptEvaluation(_userMenu, teacherScriptEvaluationManageViewModels.OrganizationId, teacherScriptEvaluationManageViewModels.ProgramId, teacherScriptEvaluationManageViewModels.SessionId, teacherScriptEvaluationManageViewModels.PaymentStatus, date, selectCourseId, teacherName, selectBranchId, trackingId, tpin);
                    
                    long recordsTotal = totalScriptEvaluation;
                    long recordsFiltered = recordsTotal;

                    IList<TeacherScriptEvaluationDetails> teacherScriptEvaluationsDetails = _teacherScriptEvaluationDetailsService.TeacherScriptEvaluationsList(start, length, _userMenu, teacherScriptEvaluationManageViewModels.OrganizationId, teacherScriptEvaluationManageViewModels.ProgramId, teacherScriptEvaluationManageViewModels.SessionId, teacherScriptEvaluationManageViewModels.PaymentStatus, date, selectCourseId, teacherName, selectBranchId, trackingId, tpin);

                    var data = new List<object>();
                    int sl = start + 1;

                    #region DataTable
                    foreach (var value in teacherScriptEvaluationsDetails)
                    {
                        var str = new List<string>();

                        str.Add((value.TeacherScriptEvaluation.SubmissionDate != null) ? value.TeacherScriptEvaluation.SubmissionDate.Value.ToString("dd/MM/yyy") : "");
                        str.Add(value.TeacherScriptEvaluation.Course.Name); 
                        str.Add(value.Teacher.Tpin.ToString());
                        str.Add(value.Teacher.DisplayTeacherName);
                        str.Add(value.ForBranch != null ? value.ForBranch.Name : "Common");
                        str.Add(value.Description);
                        str.Add(value.Amount.ToString());
                        str.Add(value.PaymentStatus == TeacherPaymentStatus.Due ? "Due" : "Paid");
                        str.Add("S" + value.TeacherScriptEvaluation.Id);
                        if (value.PaymentStatus == TeacherPaymentStatus.Paid)
                        {
                            str.Add("<a href='" + Url.Action("Details", "TeacherScriptEvaluation") + "?Id=" + value.Id + "' data-id='" + value.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;");
                        }
                        else
                        {
                            str.Add("<a href='" + Url.Action("Details", "TeacherScriptEvaluation") + "?Id=" + value.Id + "' data-id='" + value.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;" +
                        "<a href='" + Url.Action("Edit", "TeacherScriptEvaluation") + "?Id=" + value.Id + "' data-id='" + value.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; "
                        + "<a href='#' Id='" + value.Id + "' data-id='" + value.Id + "' class='glyphicon glyphicon-trash'> </a>&nbsp;&nbsp; ");
                        }
                        
                        sl++;
                        data.Add(str);
                    }
                    #endregion
                    
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                else
                {
                    return Json(HttpNotFound());
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        
        #endregion

        #region Helper Function

        private void InitializeIndex()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            var paymentStatus = new SelectList(TeacherPaymentStatus.GetTeacherPaymentStatus(), "Key", "Value");

            ViewBag.PaymentStatus = new SelectList(paymentStatus, "Value", "Text");
        }

        #endregion
    }
}
