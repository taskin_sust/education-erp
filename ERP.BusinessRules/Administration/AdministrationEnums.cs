﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.BusinessRules.Administration
{
    public enum LoginOperationStatus
    {
        [Description("Success")]
        Success = 1,
        [Description("Fail")]
        Fail = 2,
        [Description("Change Password")]
        ChangePassword = 3,
        [Description("Lock")]
        Lock = 4
    }

}
