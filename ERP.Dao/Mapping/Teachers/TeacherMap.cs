﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public partial class TeacherMap : BaseClassMap<Teacher, long>
    {
        public TeacherMap()
        {
            Table("Teacher");
            LazyLoad();
            Map(x => x.Rank);
            Map(x => x.Name);
            Map(x => x.FullName);
            Map(x => x.NickName);
            Map(x => x.FatherName);
            Map(x => x.Religion);
            Map(x => x.Gender);
            Map(x => x.DateOfBirth);
            Map(x => x.HscPassingYear);
            Map(x => x.PersonalMobile);
            Map(x => x.AlternativeMobile);
            Map(x => x.FathersMobile);
            Map(x => x.MothersMobile);
            Map(x => x.RoomMatesMobile);
            Map(x => x.Email);
            Map(x => x.Image);
            Map(x => x.FbId);
            Map(x => x.PresentAddress);
            Map(x => x.Area);
            Map(x => x.PermanentAddress);
            Map(x => x.Institute);
            Map(x => x.Department);
            Map(x => x.BloodGroup);
            Map(x => x.District);
            Map(x => x.Tpin);
            
            HasMany(x => x.TeacherActivityPriorities).KeyColumn("TeacherId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.TeacherExtraCurricularActivities).KeyColumn("TeacherId").Cascade.AllDeleteOrphan().Inverse();
            //HasMany(x => x.Organizations).KeyColumn("TeacherId").Cascade.AllDeleteOrphan().Inverse();
            //HasManyToMany(x => x.Organizations).Table("TeacherOrganization").Cascade.AllDeleteOrphan().Inverse();

            HasManyToMany(x => x.Organizations).Table("TeacherOrganization")
                .ParentKeyColumn("TeacherId").ChildKeyColumn("OrganizationId")
                //.Inverse();
                .Cascade.SaveUpdate();

            HasMany(x => x.TeacherAdmissionInfos).KeyColumn("TeacherId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.TeacherSubjectPriorities).KeyColumn("TeacherId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.TeacherVersionOfStudyPriorities).KeyColumn("TeacherId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentClassAttendence).KeyColumn("TeacherId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.TeacherImageses).KeyColumn("TeacherId").Cascade.All().Inverse();
           }
    }
}
