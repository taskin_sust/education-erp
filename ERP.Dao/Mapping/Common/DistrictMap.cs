using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Common;

namespace UdvashERP.Dao.Mapping.Common
{
    public partial class DistrictMap : BaseClassMap<District, long>
    {
        public DistrictMap()
        {
            Table("District");
            LazyLoad();
            Map(x => x.Name);
            References(x => x.Division).Column("DivisionId");
            HasMany(x => x.StudentList).KeyColumn("DistrictId").Cascade.AllDeleteOrphan().Inverse();

        }
    }
}
