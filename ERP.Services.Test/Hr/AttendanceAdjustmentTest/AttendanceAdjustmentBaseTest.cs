﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Hr.AttendanceSummaryServices;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.Hr.AttendanceAdjustmentTest
{
    public class AttendanceAdjustmentBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal AttendanceAdjustmentFactory _attendanceAdjustmentFactory;
        internal AttendanceSummaryServiceHelper _attendanceSummaryServiceHelper;
        internal CampusFactory _campusFactory;
        internal CampusRoomFactory _campusRoomFactory;
        internal DayOffAdjustmentFactory _dayOffAdjustmentFactory;
        internal DepartmentFactory _departmentFactory;
        internal DesignationFactory _designationFactory;
        internal EmployeeBenefitsFundSettingFactory _employeeBenefitsFundSettingFactory;//
        internal EmployeeBenefitsFundSettingEntitlementFactory _employeeBenefitsFundSettingEntitlementFactory;//
        internal HolidayWorkFactory _holidayWorkFactory;//
        internal IAttendanceAdjustmentLogService _attendanceAdjustmentLogService;
        internal IDayOffAdjustmentService _dayOffAdjustmentService;
        internal IHolidaySettingService _holidaySettingService;
        internal IOrganizationService _organizationService;
        internal ISalarySheetService _salarySheetService;
        internal MaritalInfoFactory _maritalInfoFactory;
        internal MemberEbfHistoryFactory _memberEbfHistoryFactory;//
        internal MemberLoanFactory _memberLoanFactory;
        internal MemberLoanApplicationFactory _memberLoanApplicationFactory;//
        internal MemberLoanRefundFactory _memberLoanRefundFactory;//
        internal readonly AttendanceAdjustmentService _attendanceAdjustmentService;
        internal readonly CommonHelper CommonHelper;
        internal SalarySheetFactory _salarySheetFactory;
        internal SalarySheetDetailsFactory _salarySheetDetailsFactory;//
        internal ShiftWeekendHistoryFactory _shiftWeekendHistoryFactory;
        
        #endregion

        public AttendanceAdjustmentBaseTest()
        {
            CommonHelper = new CommonHelper();
            _attendanceAdjustmentFactory = new AttendanceAdjustmentFactory(null, Session);
            _attendanceAdjustmentLogService = new AttendanceAdjustmentLogService(Session);
            _attendanceAdjustmentService = new AttendanceAdjustmentService(Session);
            _attendanceSummaryServiceHelper = new AttendanceSummaryServiceHelper(Session);
            _campusFactory = new CampusFactory(null, Session);
            _campusRoomFactory = new CampusRoomFactory(null, Session);
            _dayOffAdjustmentFactory = new DayOffAdjustmentFactory(null,Session);
            _dayOffAdjustmentService = new DayOffAdjustmentService(Session);
            _departmentFactory = new DepartmentFactory(null, Session);
            _employeeBenefitsFundSettingFactory = new EmployeeBenefitsFundSettingFactory(null,Session);
            _employeeBenefitsFundSettingEntitlementFactory = new EmployeeBenefitsFundSettingEntitlementFactory(null,Session);
            _holidaySettingService = new HolidaySettingService(Session);
            _holidayWorkFactory = new HolidayWorkFactory(null,Session);
            _memberLoanApplicationFactory = new MemberLoanApplicationFactory(null,Session);
            _memberLoanFactory = new MemberLoanFactory(null,Session);
            _memberLoanRefundFactory = new MemberLoanRefundFactory(null,Session);
            _memberEbfHistoryFactory = new MemberEbfHistoryFactory(null, Session);
            _maritalInfoFactory = new MaritalInfoFactory(null, Session);
            _organizationService = new OrganizationService(Session);
            _salarySheetService = new SalarySheetService(Session);
            _salarySheetFactory = new SalarySheetFactory(null,Session);
            _salarySheetDetailsFactory = new SalarySheetDetailsFactory(null,Session);
            _shiftWeekendHistoryFactory = new ShiftWeekendHistoryFactory(null, Session);
        }
        
        public void Dispose()
        {
            _salarySheetDetailsFactory.CleanUp();
            _memberEbfHistoryFactory.CleanUp();
            _salarySheetFactory.CleanUp();
            _maritalInfoFactory.CleanUp();
            _shiftWeekendHistoryFactory.CleanUp();
            _attendanceAdjustmentFactory.LogCleanUp(_attendanceAdjustmentFactory);
            _attendanceAdjustmentFactory.CleanUp(_attendanceAdjustmentFactory);
            leaveApplicationFactory.LogCleanUp(leaveApplicationFactory.SingleObjectList);
            leaveApplicationFactory.CleanUp();
            _employeeBenefitsFundSettingEntitlementFactory.CleanUp();
            _employeeBenefitsFundSettingFactory.CleanUp();
            _holidayWorkFactory.LogCleanUp(_holidayWorkFactory);
            _holidayWorkFactory.CleanUpFactory();
            _memberEbfHistoryFactory.CleanUp();
            _memberLoanRefundFactory.CleanUp();
            _memberLoanFactory.CleanUp();
            _memberLoanApplicationFactory.CleanUp();
            salaryHistoryFactory.LogCleanUp(salaryHistoryFactory);
            salaryHistoryFactory.CleanUp();
            _dayOffAdjustmentFactory.LogCleanUp(_dayOffAdjustmentFactory);
            _dayOffAdjustmentFactory.CleanUp();
            _departmentFactory.Cleanup();
            _campusRoomFactory.CleanUp();
            _campusFactory.Cleanup();
            base.Dispose();
        }
    }
}