using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{
    
    
    public partial class EmployeeBenefitsFundSettingMap : BaseClassMap<EmployeeBenefitsFundSetting,long> {

        public EmployeeBenefitsFundSettingMap()
        {
			Table("HR_EmployeeBenefitsFundSetting");
			LazyLoad();
			References(x => x.Organization).Column("OrganizationId");
            Map(x => x.Name);
			Map(x => x.EmploymentStatus).Column("EmploymentStatus").Not.Nullable();
			Map(x => x.CalculationOn).Column("CalculationOn").Not.Nullable();
			Map(x => x.EmployeeContribution).Column("EmployeeContribution").Not.Nullable();
			Map(x => x.EmployerContribution).Column("EmployerContribution").Not.Nullable();
			Map(x => x.EffectiveDate).Column("EffectiveDate").Not.Nullable();
			Map(x => x.ClosingDate).Column("ClosingDate");
            HasMany(x => x.EmployeeBenefitsFundSettingEntitlement).KeyColumn("EmployeeBenefitsFundSettingId").Cascade.AllDeleteOrphan().Inverse().Where(s=>s.Status!=EmployeeBenefitsFundSetting.EntityStatus.Delete).Not.LazyLoad(); 
        }
    }
}
