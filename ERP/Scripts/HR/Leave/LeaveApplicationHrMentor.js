﻿$(document).ready(function () {
    if ($("table#leaveApplicationHrTable").length) {
        $("#leaveApplicationHrTable").tableHeadFixer({ "left": 2, "z-index": 2 });
        setLeaveApplicationRowCount('leaveApplicationHrTable');
        console.log("Hr");
    }
    if ($("table#leaveApplicationMentorTable").length) {
        $("#leaveApplicationMentorTable").tableHeadFixer({ "left": 2, "z-index": 2 });
        setLeaveApplicationRowCount('leaveApplicationMentorTable');
        console.log("Mentor");
    }
    
    $("#leaveApplicationHrTable").tableHeadFixer({ "left": 2, "z-index": 2 });

    $(document).on("focus", "input.modal_form_datetime", function () {
        //var today = new Date();
        //var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();
        ////var month = today.getMonth();
        ////if (month == 0)
        ////    month = 1;
        ////var startDate = today.getFullYear().toString() + "-" + (month) + "-" + "01";
        //var startDate = today.getFullYear().toString() + "-" + (1) + "-" + "01";
        //var endDate = today.getFullYear().toString() + "-" + (12) + "-" + "31";;
        var today = new Date();
        var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();

        var pastMonth = new Date(today.setMonth(today.getMonth() - 1, 1));
        var startDate = pastMonth.getFullYear().toString() + "-" + (pastMonth.getMonth() + 1) + "-" + "01";

        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: initialDate,
            startDate: startDate,
            endDate: endDate,
            startView: 2,
            minView: 2,
            maxView: 4
        });
    });

    $('#laId').DataTable({
        "scrollX": true
    });
    var applicationId = 0;
    $(document).on('click', '#addLeaveApplicationRowByMentor', function () {
        applicationId++;
        var tableId = 'leaveApplicationMentorTable';
        //new dynamic row add here 
        var rowCount = $('#' + tableId + ' > tbody > tr').length;
        var lastRow;
        if (rowCount > 0) {
            lastRow = $('#' + tableId + ' > tbody > tr:last');
        } else {
            lastRow = $('#' + tableId + ' > tbody');
        }
        var content = '<tr>' +
                '<td class="1"><input id="' + applicationId + '" type="hidden" class="form-control leaveApplicationId" value="0" disabled="disabled" /><input id="" style="width:60px;" class="form-control memberPin" value=""  type="text"></td>' +
                '<td class="2"><input id="" style="width:150px;" class="form-control memberName" value="" disabled="disabled" type="text"></td>' +
                '<td class="3"><select id="" style="width:200px;" class="form-control memberLeaveType" name=""></select></td>' +
                '<td class="4"><input style="width:118px;" id="" name="memberLeaveDateFrom" class="form-control memberLeaveDateFrom form_datetime fixedwidthmax" value="" readonly="readonly" type="text"></td>' +
                '<td class="5"><input style="width:118px;" id="" name="memberLeaveDateTo" class="form-control memberLeaveDateTo form_datetime fixedwidthmax" value="" readonly="readonly" type="text"></td>' +
                '<td class="6"><input style="width:65px;" class="form-control  memberLeaveDayCount" value="" readonly="readonly" type="text"></td>' +
                '<td class="7"><input style="width:300px;" name="cause" id="" class="form-control  memberLeaveNote" value="" type="text"></td>' +
                '<td class="8"><input style="width:65px;" id="" class="form-control  memberLeaveResMemPin" value="" type="text"></td>' +
                '<td class="9"><input style="width:150px;" id="" class="form-control  memberLeaveResMemName" value="" readonly="readonly" type="text"> </td>' +
                '<td class="10">' +
                '<a href="#" data-id="" class="glyphicon glyphicon-ok approvedbtn"> </a> ' +
                '<a href="#" data-id="" class="glyphicon glyphicon-remove removeBtn"> </a>' +
                '</td>' +
                '</tr>';
        if (rowCount > 0) {
            lastRow.after(content);
            $("#leaveApplicationHrTable").tableHeadFixer({ "left": 2, "z-index": 2 });
        } else {
            lastRow.append(content);
            $("#leaveApplicationHrTable").tableHeadFixer({ "left": 2, "z-index": 2 });
        }
        $(".displayData").animate({ scrollTop: $('.displayData').prop("scrollHeight") }, 1000);
        setLeaveApplicationRowCount('leaveApplicationMentorTable');
       
    });

    $(document).on('click', '#addLeaveApplicationRowByHr', function () {
        applicationId++;
        var tableId = 'leaveApplicationHrTable';
        var rowCount = $('#' + tableId + ' > tbody > tr').length;
        //console.log(rowCount);
        var lastRow;
        if (rowCount > 0) {
            lastRow = $('#' + tableId + ' > tbody > tr:last');
        } else {
            lastRow = $('#' + tableId + ' > tbody');
        }
        var content = '<tr>' +
                '<td class="1"><input id="' + applicationId + '" type="hidden" class="form-control leaveApplicationId" value="0" disabled="disabled" /><input id="" style="width:57px;" class="form-control memberPin pinNumHr" value=""  type="text"></td>' +
                '<td class="2"><input id="" style="width:100px;" class="form-control memberName" value="" disabled="disabled" type="text"></td>' +
                '<td class="3"><p style="width:200px; padding-top:4px; text-align:center;" class="department"></p></td>' +
                '<td class="4"><p style="width:100px; padding-top:4px; text-align: center;"  class="organization"></p></td>' +
                '<td class="5"><p class="branch"></p></td>' +
                '<td class="6"><p class="campus"></p></td>' +
                '<td class="7"><select id="" style="width:195px;" class="form-control memberLeaveType" name=""></select></td>' +
                '<td class="8"><input style="width:100px;" id="" name="memberLeaveDateFrom" class="form-control memberLeaveDateFrom form_datetime fixedwidthmax" value="" readonly="readonly" type="text"></td>' +
                '<td class="9"><input style="width:100px;" id="" name="memberLeaveDateTo" class="form-control memberLeaveDateTo form_datetime fixedwidthmax" value="" readonly="readonly" type="text"></td>' +
                '<td class="10"><input style="width:55px;text-align:center" class="form-control  memberLeaveDayCount" value="" readonly="readonly" type="text"></td>' +
                '<td class="11"><p style="width:100px;" class="memberLeaveApplicationDate" readonly="readonly"></p></td>' +
                '<td class="12"><input style="width:400px;" name="cause" id="" class="form-control  memberLeaveNote" value="" type="text"></td>' +
                '<td class="13"><input style="width:60px;" id="" class="form-control  memberLeaveResMemPin" value="" type="text"></td>' +
                '<td class="14"><input style="width:135px;" id="" class="form-control  memberLeaveResMemName" value="" readonly="readonly" type="text"> </td>' +
                '<td class="15">' +
                '<a href="#" data-id="" class="glyphicon glyphicon-ok approvedbtnHr"> </a> ' +
                '<a href="#" data-id="" class="glyphicon glyphicon-remove removeBtn"> </a>' +
                '</td>' +
                '</tr>';
        if (rowCount > 0) {
            lastRow.after(content);
            $("#leaveApplicationHrTable").tableHeadFixer({ "left": 2, "z-index": 2 });
        } else {
            lastRow.append(content);
            $("#leaveApplicationHrTable").tableHeadFixer({ "left": 2, "z-index": 2 });
        }
        $(".displayData").animate({ scrollTop: $('.displayData').prop("scrollHeight") }, 1000);
        setLeaveApplicationRowCount('leaveApplicationHrTable');
    });

    $(document).on('click', '.removeBtn', function () {
        try {
            var ctr = $(this).parent().parent();
            $(ctr).remove();
            if ($("table#leaveApplicationHrTable").length) {
                setLeaveApplicationRowCount('leaveApplicationHrTable');
            }
            if ($("table#leaveApplicationMentorTable").length) {
                setLeaveApplicationRowCount('leaveApplicationMentorTable');
            }
        } catch (e) {

        }
    });

    $(document).on("blur", ".memberPin, .memberLeaveResMemPin", function (event) {
        $(this).removeClass('highlight');
        var allclass = $(event.target).attr('class');
        var parentTr = $(this).parent().parent();
        var pin = $(this).val();
        var isMentor = false;
        var isHr = false;
        var onlyName = false;
        var isLeaveSummaryInfo = false;
        var isOrganization = false;
        var isBranch = false;
        var isCampus = false;
        var isDepartment = false;
       
        if (allclass.search('memberLeaveResMemPin') !== -1) {
            onlyName = true;
        } else {
            isLeaveSummaryInfo = true;
            if (allclass.search('pinNumHr') !== -1) {
                isHr = true;
                isOrganization = isBranch = isCampus = isDepartment = true;
            } else {
                isMentor = true;
            }
        }
        $.ajax({
            //url: $("body").attr("data-project-root") + "Hr/LeaveApplication/GetTeamMemberInfo",
            url: $("body").attr("data-project-root") + "Hr/CommonAjaxHr/GetTeamMemberInformation",
            type: "POST",
            data: { pin: pin, isMentor: isMentor, isHr: isHr, isLeaveSummaryInfo: isLeaveSummaryInfo, isOrganization: isOrganization, isBranch: isBranch, isCampus: isCampus, isDepartment: isDepartment },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $.unblockUI();
                if (data.IsSuccess) {
                    if (onlyName === true) {
                        parentTr.find('.memberLeaveResMemName').val(data.teamMemberName);
                    } else {
                        parentTr.find('.memberName').val(data.teamMemberName);
                        parentTr.find('.memberLeaveType').html(data.leaveTypelist);
                        if (isOrganization === true) {
                            parentTr.find('.organization').html(data.organizationName);
                        }
                        if (isBranch === true) {
                            parentTr.find('.branch').html(data.branchName);
                        }
                        if (isCampus === true) {
                            parentTr.find('.campus').html(data.campusName);
                        }
                        if (isDepartment === true) {
                            parentTr.find('.department').html(data.departmentName);
                        }
                    }

                } else {
                    if (onlyName === true) {
                        parentTr.find('.memberLeaveResMemName').val("");
                    } else {
                        parentTr.find('.memberName').val("");
                        parentTr.find('.memberLeaveType').html("");
                        if (isOrganization === true) {
                            parentTr.find('.organization').html("");
                        }
                        if (isBranch === true) {
                            parentTr.find('.branch').html("");
                        }
                        if (isCampus === true) {
                            parentTr.find('.campus').html("");
                        }
                        if (isDepartment === true) {
                            parentTr.find('.department').html("");
                        }
                    }
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.unblockUI();
                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
            }
        });

    });

    $(document).on('change', '.memberLeaveType', function () {
        $(this).removeClass('highlight');
        var memberLeaveType = $(this).val();
        if (memberLeaveType.length <= 0 || memberLeaveType == '') {
            $(this).addClass('highlight');
        }
    });

    $(document).on('change', '.memberLeaveNote', function () {
        $(this).removeClass('highlight');
        var memberLeaveNote = $(this).val();
        if (memberLeaveNote.length < 5 || memberLeaveNote == '') {
            $(this).addClass('highlight');
        }
    });

    $(document).on('change', '.memberPin', function () {
        $(this).removeClass('highlight');
        var memberPin = $(this).val();
        if (memberPin.length <= 0 || memberPin == '') {
            $(this).addClass('highlight');
        }
    });

    $(document).on('change', '.modal_form_datetime', function (event) {
        var parentDiv = $(this).parent().parent().parent();
        console.log(parentDiv);
        var fromDate = '', toDate = '', daysDiff;
        var fromDateFormated, toDateFormated;
        var allclass = $(event.target).attr('class');
        if (allclass.search('modalMemberLeaveDateFrom') !== -1) {
            fromDate = $(this).val();
            toDate = $(this).parent().parent().parent().find('.modalMemberLeaveDateTo').val();
        }
        if (allclass.search('modalMemberLeaveDateTo') !== -1) {
            fromDate = $(this).parent().parent().parent().find('.modalMemberLeaveDateFrom').val();
            toDate = $(this).val();
        }
        if (fromDate.length > 0 && toDate.length > 0) {
            fromDateFormated = new Date(fromDate);
            toDateFormated = new Date(toDate);
            if (fromDateFormated > toDateFormated) {
                parentDiv.find('.modalMemberLeaveDateFrom').addClass('highlight');
                parentDiv.find('.modalMemberLeaveDateTo').addClass('highlight');
                $(this).parent().parent().parent().find('.modalMemberLeaveDayCount').val('');
            } else {
                parentDiv.find('.modalMemberLeaveDateFrom').removeClass('highlight');
                parentDiv.find('.modalMemberLeaveDateTo').removeClass('highlight');
                daysDiff = DateDiff(fromDateFormated, toDateFormated);
                $(this).parent().parent().parent().find('.modalMemberLeaveDayCount').val(daysDiff);
            }
        }
    });

    $(document).on('change', '.form_datetime', function (event) {
        var parentTr = $(this).parent().parent();
        var fromDate = '', toDate = '', daysDiff;
        var fromDateFormated, toDateFormated;
        var tempVar;
        var allclass = $(event.target).attr('class');
        if (allclass.search('memberLeaveDateFrom') !== -1) {
            fromDate = $(this).val();
            toDate = $(this).parent().parent().find('.memberLeaveDateTo').val();
        }
        if (allclass.search('memberLeaveDateTo') !== -1) {
            fromDate = $(this).parent().parent().find('.memberLeaveDateFrom').val();
            toDate = $(this).val();
        }
        if (fromDate.length > 0 && toDate.length > 0) {
            fromDateFormated = new Date(fromDate);
            toDateFormated = new Date(toDate);
            if (fromDateFormated > toDateFormated) {
                parentTr.find('.memberLeaveDateFrom').addClass('highlight');
                parentTr.find('.memberLeaveDateTo').addClass('highlight');
                $(this).parent().parent().find('.memberLeaveDayCount').val('');
            } else {
                parentTr.find('.memberLeaveDateFrom').removeClass('highlight');
                parentTr.find('.memberLeaveDateTo').removeClass('highlight');
                daysDiff = DateDiff(fromDateFormated, toDateFormated);
                $(this).parent().parent().find('.memberLeaveDayCount').val(daysDiff);
            }
        }
    });
    
    $(document).on("click", ".approvedbtn, .rejectedbtn", function (event) {
        var allclass = $(event.target).attr('class');
        var text = 'Approved';
        var parentTr = $(this).parent().parent();
        var leaveStatus = 0;
        if (allclass.search('approvedbtn') !== -1) {
            leaveStatus = 2;
        }
        else if (allclass.search('rejectedbtn') !== -1) {
            leaveStatus = 3;
            text = 'Rejected';
        }

        if (allclass.search('approvedbtn') !== -1 || allclass.search('rejectedbtn') !== -1 ) {
            var message = "Are you sure you want to " + text + " this/these Leave(s)?";
            bootbox.confirm(message, function(result) {
                if (result) {
                    var leaveApplicationId = parentTr.find('.leaveApplicationId').val();
                    var memberPin = parentTr.find('.memberPin').val();
                    //var memberName = parentTr.find('.memberName').val();
                    var memberLeaveType = parentTr.find('.memberLeaveType').val();
                    var memberLeaveDateFrom = parentTr.find('.memberLeaveDateFrom').val();
                    var memberLeaveDateTo = parentTr.find('.memberLeaveDateTo').val();
                    //var memberLeaveDayCount = parentTr.find('.memberLeaveDayCount').val();
                    var memberLeaveNote = parentTr.find('.memberLeaveNote').val();
                    var memberLeaveResMemPin = parentTr.find('.memberLeaveResMemPin').val();
                    //var memberLeaveResMemName = parentTr.find('.memberLeaveResMemName').val();
                    var leaveApplicationObjArray = [];
                    if (validateApplicationRow(parentTr)) {
                        var leaveApplicationObj = new LeaveApplicationViewModel();
                        leaveApplicationObj.LeaveApplicationId = leaveApplicationId;
                        leaveApplicationObj.TeamMemberPin = memberPin;
                        leaveApplicationObj.DateFrom = memberLeaveDateFrom;
                        leaveApplicationObj.DateTo = memberLeaveDateTo;
                        leaveApplicationObj.LeaveNote = memberLeaveNote;
                        leaveApplicationObj.ResponsibleMemberPin = memberLeaveResMemPin;
                        leaveApplicationObj.LeaveId = memberLeaveType;
                        leaveApplicationObj.LeaveStatus = leaveStatus;
                        leaveApplicationObjArray.push(leaveApplicationObj);
                    }
                    var leaveApplicationViewModelList = leaveApplicationObjArray;
                    if (leaveApplicationObjArray.length > 0) {
                        $.ajax({
                            url: $("body").attr("data-project-root") + "Hr/LeaveApprovalMentor/MentorLeaveApprove",
                            type: "POST",
                            data: { "leaveApplicationViewModelList": leaveApplicationViewModelList },
                            beforeSend: function() {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function(data) {
                                $.unblockUI();
                                if (data.IsSuccess) {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "s"
                                    });
                                    parentTr.remove();
                                } else {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "e"
                                    });
                                }
                                setLeaveApplicationRowCount('leaveApplicationMentorTable');
                            },
                            complete: function() {
                                $.unblockUI();
                            },
                            error: function(data) {
                                $.unblockUI();
                                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                            }
                        });
                    }

                }
            }).css({
                'color': 'red',
                'font-size': '30px',
                'margin-top': (($(window).height() / 4))
            });
        }
    });

    $(document).on("click", "#leaveApplicationApproveAllByMentor", function (event) {
        var message = "Are you sure you want to Approved All Leave(s)?";
        bootbox.confirm(message, function(result) {
            if (result === true) {
                var isSuccess = true;
                var tableId = "leaveApplicationMentorTable";
                var rowCount = $('#' + tableId + ' > tbody > tr').length;
                if (rowCount > 0) {
                    var leaveApplicationObjArray = [];
                    var leaveApplicationObjStatusArray = [];
                    $('#' + tableId + ' > tbody > tr').each(function () {
                        var parentTr = $(this);
                        console.log(parentTr);
                        var leaveApplicationId = parentTr.find('.leaveApplicationId').val();
                        var memberPin = parentTr.find('.memberPin').val();
                        //var memberName = parentTr.find('.memberName').val();
                        var memberLeaveType = parentTr.find('.memberLeaveType').val();
                        var memberLeaveDateFrom = parentTr.find('.memberLeaveDateFrom').val();
                        var memberLeaveDateTo = parentTr.find('.memberLeaveDateTo').val();
                        //var memberLeaveDayCount = parentTr.find('.memberLeaveDayCount').val();
                        var memberLeaveNote = parentTr.find('.memberLeaveNote').val();
                        var memberLeaveResMemPin = parentTr.find('.memberLeaveResMemPin').val();
                        //var memberLeaveResMemName = parentTr.find('.memberLeaveResMemName').val();
                        if (validateApplicationRow(parentTr)) {
                            var leaveApplicationObj = new LeaveApplicationViewModel();
                            leaveApplicationObj.LeaveApplicationId = leaveApplicationId;
                            leaveApplicationObj.TeamMemberPin = memberPin;
                            leaveApplicationObj.DateFrom = memberLeaveDateFrom;
                            leaveApplicationObj.DateTo = memberLeaveDateTo;
                            leaveApplicationObj.LeaveNote = memberLeaveNote;
                            leaveApplicationObj.ResponsibleMemberPin = memberLeaveResMemPin;
                            leaveApplicationObj.LeaveId = memberLeaveType;
                            leaveApplicationObj.LeaveStatus = 2; //Approved
                            leaveApplicationObjArray.push(leaveApplicationObj);

                            console.log(leaveApplicationObj);
                        } else {
                            leaveApplicationObjStatusArray.push(1);
                        }
                    });
                    var leaveApplicationViewModelList = leaveApplicationObjArray;
                    if (leaveApplicationObjStatusArray.length <=0 ) {
                        $.ajax({
                            url: $("body").attr("data-project-root") + "Hr/LeaveApprovalMentor/MentorLeaveApprove",
                            type: "POST",
                            data: { "leaveApplicationViewModelList": leaveApplicationViewModelList },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data) {
                                $.unblockUI();
                                if (data.IsSuccess) {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "s"
                                    });
                                    $("#leaveApplicationMentorTable > tbody").html('');
                                } else {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "e"
                                    });
                                }
                                setLeaveApplicationRowCount('leaveApplicationMentorTable');
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (data) {
                                $.unblockUI();
                                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                            }
                        });
                    }
                }
            }
        }).css({
            'color': 'red',
            'font-size': '30px',
            'margin-top': (($(window).height() / 4))
        });
    
    });

    $(document).on("click", ".historyReject, .historyApproved", function (event) {
        var text = 'Approved';
        var url = '';
        var allclass = $(event.target).attr('class');
        var parentTr = $(this);
        var leaveStatus = 0;
        if (allclass.search('historyApproved') != -1) {
            leaveStatus = 2;
        }
        else if (allclass.search('historyReject') != -1) {
            leaveStatus = 3;
            text = 'Rejected';
        }
        var leaveApplicationId = parentTr.attr('data-id');
        var memberPin = parentTr.attr('data-pin');
        var memberLeaveType = parentTr.attr('data-leave');
        var memberLeaveNote = parentTr.attr('data-note');
        console.log(parentTr);
        var leaveApplicationObjArray = [];
        var leaveApplicationObj = new LeaveApplicationViewModel();
        leaveApplicationObj.LeaveApplicationId = leaveApplicationId;
        leaveApplicationObj.TeamMemberPin = memberPin;
        leaveApplicationObj.LeaveId = memberLeaveType;
        leaveApplicationObj.LeaveStatus = leaveStatus;
        leaveApplicationObj.LeaveNote = memberLeaveNote;
        leaveApplicationObjArray.push(leaveApplicationObj);

        if (leaveStatus === 2) {
            url = "<a title='Reject' href='#' data-id='" + leaveApplicationId + "' data-pin='" + memberPin + "' data-leave='" + memberLeaveType + "' data-leaveStatus='3' data-note='" + memberLeaveNote + "' class='glyphicon glyphicon-remove historyReject'> </a >";
        }
        else if (leaveStatus ===3) {
            url = "<a title='Approve' href='#' data-id='" + leaveApplicationId + "' data-pin='" + memberPin + "' data-leave='" + memberLeaveType + "' data-leaveStatus='2' data-note='" + memberLeaveNote + "' class='glyphicon glyphicon-ok historyApproved'> </a >";
        }

        var leaveApplicationViewModelList = leaveApplicationObjArray;//leaveApplicationObjArray;
        console.log(leaveApplicationId);
        if (allclass.search('historyApproved') !== -1 || allclass.search('historyReject') !== -1) {
            var message = "Are you sure you want to " + text + " this/these Leave(s)?";
            bootbox.confirm(message, function (result) {
                if (result) {
                    $.ajax({
                        url: $("body").attr("data-project-root") + "Hr/LeaveApprovalMentor/MentorLeaveApprove",
                        type: "POST",
                        data: { "leaveApplicationViewModelList": leaveApplicationViewModelList },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (data) {
                            $.unblockUI();
                            if (data.IsSuccess) {
                                $.fn.customMessage({
                                    displayMessage: data.Message,
                                    displayMessageType: "s"
                                });
                                $("#leaveStatus_" + leaveApplicationId).html(text);
                                console.log(url);
                                $("#leaveStatusActionLink_" + leaveApplicationId).html(url);
                            } else {
                                $.fn.customMessage({
                                    displayMessage: data.Message,
                                    displayMessageType: "e"
                                });
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.unblockUI();
                            bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                        }
                    });
                }
            }).css({
                'color': 'red',
                'font-size': '30px',
                'margin-top': (($(window).height() / 4))
            });
        }
    });

    $(document).on("click", ".approvedbtnHr, .rejectedbtnHr", function (event) {
        var allclass = $(event.target).attr('class');
        var text = 'Approved';
        var parentTr = $(this).parent().parent();
        var leaveStatus = 0;
        if (allclass.search('approvedbtn') !== -1) {
            leaveStatus = 2;
        }
        else if (allclass.search('rejectedbtn') !== -1) {
            leaveStatus = 3;
            text = 'Rejected';
        }

        if (allclass.search('approvedbtnHr') !== -1 || allclass.search('rejectedbtnHr') !== -1) {
            var message = "Are you sure you want to " + text + " this/these Leave(s)?";
            bootbox.confirm(message, function (result) {
                if (result) {
                    var leaveApplicationId = parentTr.find('.leaveApplicationId').val();
                    var memberPin = parentTr.find('.memberPin').val();
                    //var memberName = parentTr.find('.memberName').val();
                    var memberLeaveType = parentTr.find('.memberLeaveType').val();
                    var memberLeaveDateFrom = parentTr.find('.memberLeaveDateFrom').val();
                    var memberLeaveDateTo = parentTr.find('.memberLeaveDateTo').val();
                    //var memberLeaveDayCount = parentTr.find('.memberLeaveDayCount').val();
                    var memberLeaveNote = parentTr.find('.memberLeaveNote').val();
                    var memberLeaveResMemPin = parentTr.find('.memberLeaveResMemPin').val();
                    //var memberLeaveResMemName = parentTr.find('.memberLeaveResMemName').val();
                    var leaveApplicationObjArray = [];
                    if (validateApplicationRow(parentTr)) {
                        var leaveApplicationObj = new LeaveApplicationViewModel();
                        leaveApplicationObj.LeaveApplicationId = leaveApplicationId;
                        leaveApplicationObj.TeamMemberPin = memberPin;
                        leaveApplicationObj.DateFrom = memberLeaveDateFrom;
                        leaveApplicationObj.DateTo = memberLeaveDateTo;
                        leaveApplicationObj.LeaveNote = memberLeaveNote;
                        leaveApplicationObj.ResponsibleMemberPin = memberLeaveResMemPin;
                        leaveApplicationObj.LeaveId = memberLeaveType;
                        leaveApplicationObj.LeaveStatus = leaveStatus;
                        leaveApplicationObjArray.push(leaveApplicationObj);
                    }
                    var leaveApplicationViewModelList = leaveApplicationObjArray;
                    if (leaveApplicationObjArray.length > 0) {
                        $.ajax({
                            url: $("body").attr("data-project-root") + "Hr/LeaveApprovalHr/HrLeaveApprove",
                            type: "POST",
                            data: { "leaveApplicationViewModelList": leaveApplicationViewModelList },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data) {
                                $.unblockUI();
                                if (data.IsSuccess) {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "s"
                                    });
                                    parentTr.remove();
                                } else {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "e"
                                    });
                                }
                                setLeaveApplicationRowCount('leaveApplicationHrTable');
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (data) {
                                $.unblockUI();
                                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                            }
                        });
                    }

                }
            }).css({
                'color': 'red',
                'font-size': '30px',
                'margin-top': (($(window).height() / 4))
            });
        }
    });

    $(document).on("click", ".modalApprove, .modalReject", function (event) {
        $("#modalCustomMessage").html("");
        var success = true;
        var url = '';
        var allclass = $(event.target).attr('class');
        var text = 'Approved';
        var parentDiv = $(this).parent().parent().parent();
        console.log(parentDiv);
        var leaveStatus = 0;
        if (allclass.search('modalApprove') !== -1) {
            leaveStatus = 2;
        }
        else if (allclass.search('modalReject') !== -1) {
            leaveStatus = 3;
            text = 'Rejected';
        }

        var leaveApplicationId = parentDiv.find('.modalLeaveApplicationId').val();
        var memberPin = parentDiv.find('.modalMemberPin').val();
        var memberLeaveType = parentDiv.find('.modalMemberLeaveType').val();
        var memberLeaveDateFrom = parentDiv.find('.modalMemberLeaveDateFrom').val();
        var memberLeaveDateTo = parentDiv.find('.modalMemberLeaveDateTo').val();
        var memberLeaveNote = parentDiv.find('.modalMemberLeaveNote').val();
        var memberLeaveResMemPin = parentDiv.find('.modalMemberLeaveResMemPin').val();
        var memberLeaveReason = parentDiv.find('.modalMemberLeaveReason').val();

        if (leaveStatus === 2) {
            url = "<a title='Reject' href='#' data-id='" + leaveApplicationId + "' data-pin='" + memberPin + "' data-leave='" + memberLeaveType + "' data-leaveStatus='3' data-note='" + memberLeaveNote + "' class='glyphicon glyphicon-remove hrHistoryReject'> </a >";
        }
        else if (leaveStatus === 3) {
            url = "<a title='Approve' href='#' data-id='" + leaveApplicationId + "' data-pin='" + memberPin + "' data-leave='" + memberLeaveType + "' data-leaveStatus='2' data-note='" + memberLeaveNote + "' class='glyphicon glyphicon-ok hrHistoryApproved'> </a >";
        }
        var leaveApplicationObjArray = [];
        if (validateApplicationModalRow(parentDiv)) {
            var leaveApplicationObj = new LeaveApplicationViewModel();
            leaveApplicationObj.LeaveApplicationId = leaveApplicationId;
            leaveApplicationObj.TeamMemberPin = memberPin;
            leaveApplicationObj.DateFrom = memberLeaveDateFrom;
            leaveApplicationObj.DateTo = memberLeaveDateTo;
            leaveApplicationObj.LeaveNote = memberLeaveNote;
            leaveApplicationObj.LeaveReason = memberLeaveReason;
            leaveApplicationObj.ResponsibleMemberPin = memberLeaveResMemPin;
            leaveApplicationObj.LeaveId = memberLeaveType;
            leaveApplicationObj.LeaveStatus = leaveStatus;
            leaveApplicationObjArray.push(leaveApplicationObj);
        }
        var leaveApplicationViewModelList = leaveApplicationObjArray;
        if (leaveApplicationObjArray.length > 0) {
            $.ajax({
                url: $("body").attr("data-project-root") + "Hr/LeaveApprovalHr/HrLeaveApprove",
                type: "POST",
                data: { "leaveApplicationViewModelList": leaveApplicationViewModelList },
                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function(data) {
                    $.unblockUI();
                    if (data.IsSuccess) {
                        $('#myModal').modal('hide');
                        $("#leaveStatus_" + leaveApplicationId).html(text);
                        $("#leaveStatusActionLink_" + leaveApplicationId).html(url);
                        $.fn.customMessage({
                            displayMessage: data.Message,
                            displayMessageType: "s"
                        });
                    } else {
                        $("#modalCustomMessage").html("");
                        $("#modalCustomMessage").html("<div class='alert alert-danger'><a class='close' data-dismiss='alert'>×</a>" + data.Message + "</div>");
                        setTimeout(function () {
                            $("#modalCustomMessage").find(".alert-danger").fadeOut(2000, function () { $(this).remove(); });
                        }, 30000);
                        //$.fn.customMessage({
                        //    displayMessage: data.Message,
                        //    displayMessageType: "e"
                        //});
                    }
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function(data) {
                    $.unblockUI();
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    $(document).on("click", "#leaveApplicationApproveAllByHr", function (event) {
        var message = "Are you sure you want to Approved All Leave(s)?";
        bootbox.confirm(message, function (result) {
            if (result === true) {
                var isSuccess = true;
                var tableId = "leaveApplicationHrTable";
                var rowCount = $('#' + tableId + ' > tbody > tr').length;
                if (rowCount > 0) {
                    var leaveApplicationObjArray = [];
                    var leaveApplicationObjStatusArray = [];
                    $('#' + tableId + ' > tbody > tr').each(function () {
                        var parentTr = $(this);
                        console.log(parentTr);
                        var leaveApplicationId = parentTr.find('.leaveApplicationId').val();
                        var memberPin = parentTr.find('.memberPin').val();
                        //var memberName = parentTr.find('.memberName').val();
                        var memberLeaveType = parentTr.find('.memberLeaveType').val();
                        var memberLeaveDateFrom = parentTr.find('.memberLeaveDateFrom').val();
                        var memberLeaveDateTo = parentTr.find('.memberLeaveDateTo').val();
                        //var memberLeaveDayCount = parentTr.find('.memberLeaveDayCount').val();
                        var memberLeaveNote = parentTr.find('.memberLeaveNote').val();
                        var memberLeaveResMemPin = parentTr.find('.memberLeaveResMemPin').val();
                        //var memberLeaveResMemName = parentTr.find('.memberLeaveResMemName').val();
                        if (validateApplicationRow(parentTr)) {
                            var leaveApplicationObj = new LeaveApplicationViewModel();
                            leaveApplicationObj.LeaveApplicationId = leaveApplicationId;
                            leaveApplicationObj.TeamMemberPin = memberPin;
                            leaveApplicationObj.DateFrom = memberLeaveDateFrom;
                            leaveApplicationObj.DateTo = memberLeaveDateTo;
                            leaveApplicationObj.LeaveNote = memberLeaveNote;
                            leaveApplicationObj.ResponsibleMemberPin = memberLeaveResMemPin;
                            leaveApplicationObj.LeaveId = memberLeaveType;
                            leaveApplicationObj.LeaveStatus = 2; //Approved
                            leaveApplicationObjArray.push(leaveApplicationObj);

                            console.log(leaveApplicationObj);
                        } else {
                            leaveApplicationObjStatusArray.push(1);
                        }
                    });
                    var leaveApplicationViewModelList = leaveApplicationObjArray;
                    if (leaveApplicationObjStatusArray.length <= 0) {
                        $.ajax({
                            url: $("body").attr("data-project-root") + "Hr/LeaveApprovalHr/HrLeaveApprove",
                            type: "POST",
                            data: { "leaveApplicationViewModelList": leaveApplicationViewModelList },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data) {
                                $.unblockUI();
                                if (data.IsSuccess) {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "s"
                                    });
                                    $("#leaveApplicationHrTable > tbody").html('');
                                } else {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "e"
                                    });
                                }
                                setLeaveApplicationRowCount('leaveApplicationHrTable');
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (data) {
                                $.unblockUI();
                                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                            }
                        });
                    }
                }
            }
        }).css({
            'color': 'red',
            'font-size': '30px',
            'margin-top': (($(window).height() / 4))
        });

    });

    $(document).on("click", ".hrHistoryReject, .hrHistoryApproved", function (event) {
        var text = 'Approved';
        var url = '';
        var allclass = $(event.target).attr('class');
        var parentTr = $(this);
        var leaveStatus = 0;
        if (allclass.search('hrHistoryApproved') != -1) {
            leaveStatus = 2;
        }
        else if (allclass.search('hrHistoryReject') != -1) {
            leaveStatus = 3;
            text = 'Rejected';
        }
        var leaveApplicationId = parentTr.attr('data-id');
        var memberPin = parentTr.attr('data-pin');
        var memberLeaveType = parentTr.attr('data-leave');
        var memberLeaveNote = parentTr.attr('data-note');
        console.log(parentTr);
        var leaveApplicationObjArray = [];
        var leaveApplicationObj = new LeaveApplicationViewModel();
        leaveApplicationObj.LeaveApplicationId = leaveApplicationId;
        leaveApplicationObj.TeamMemberPin = memberPin;
        leaveApplicationObj.LeaveId = memberLeaveType;
        leaveApplicationObj.LeaveStatus = leaveStatus;
        leaveApplicationObj.LeaveNote = memberLeaveNote;
        leaveApplicationObjArray.push(leaveApplicationObj);

        if (leaveStatus === 2) {
            url = "<a title='Reject' href='#' data-id='" + leaveApplicationId + "' data-pin='" + memberPin + "' data-leave='" + memberLeaveType + "' data-leaveStatus='3' data-note='" + memberLeaveNote + "' class='glyphicon glyphicon-remove hrHistoryReject'> </a >";
        }
        else if (leaveStatus === 3) {
            url = "<a title='Approve' href='#' data-id='" + leaveApplicationId + "' data-pin='" + memberPin + "' data-leave='" + memberLeaveType + "' data-leaveStatus='2' data-note='" + memberLeaveNote + "' class='glyphicon glyphicon-ok hrHistoryApproved'> </a >";
        }

        var leaveApplicationViewModelList = leaveApplicationObjArray;//leaveApplicationObjArray;
        console.log(text);
        if (allclass.search('hrHistoryReject') !== -1 || allclass.search('hrHistoryApproved') !== -1) {
            console.log(leaveApplicationId);
            var message = "Are you sure you want to " + text + " this/these Leave(s)?";
            bootbox.confirm(message, function (result) {
                if (result) {
                    $.ajax({
                        url: $("body").attr("data-project-root") + "Hr/LeaveApprovalHr/HrLeaveApprove",
                        type: "POST",
                        data: { "leaveApplicationViewModelList": leaveApplicationViewModelList },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (data) {
                            $.unblockUI();
                            if (data.IsSuccess) {
                                $.fn.customMessage({
                                    displayMessage: data.Message,
                                    displayMessageType: "s"
                                });
                                $("#leaveStatus_" + leaveApplicationId).html(text);
                                console.log(url);
                                $("#leaveStatusActionLink_" + leaveApplicationId).html(url);
                            } else {
                                $.fn.customMessage({
                                    displayMessage: data.Message,
                                    displayMessageType: "e"
                                });
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.unblockUI();
                            bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                        }
                    });
                }
            }).css({
                'color': 'red',
                'font-size': '30px',
                'margin-top': (($(window).height() / 4))
            });
        }
    });
    
    $(document).on('change', '#organizationId, #recentOrganizationId', function (event) {
        var allclass = $(event.target).attr('class');
        var getBranchId = "branchId";
        var getDepartmentId = "departmentId";
        var getCampusId = "campusId";
        var getOrganizationId = "organizationId";
        if (allclass.search('recent') !== -1) {
            getBranchId = "recentBranchId";
            getDepartmentId = "recentDepartmentId";
            getCampusId = "recentCampusId";
            getOrganizationId = "recentOrganizationId";
        }
        //remove all dependant dropdownlist & selection option
        $("#" + getBranchId + " option").remove();
        $("#" + getCampusId + " option").remove();
        $("#" + getDepartmentId).empty();
        $('#' + getCampusId).append("<option value='0'>All</option>");
        var organizationId = $(this).val();
        if (organizationId !== "" && organizationId != null) {
            $.ajax({
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadDepartment",
                type: "POST",
                data: { organizationIds: organizationId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing... </h1>'
                    });
                },
                success: function (response) {
                    $('#' + getDepartmentId).append("<option value='0'>All</option>");
                    $.each(response.returndepartmentList, function (i, v) {
                        $('#' + getDepartmentId).append($('<option>').text(v.Text).attr('value', v.Value));
                    });

                    $.ajax({
                        url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                        type: "POST",
                        data: { organizationIds: organizationId, isAuthorized: true, isProgramBranSession: false },
                        //beforeSend: function () {
                        //    $.blockUI({
                        //        timeout: 0,
                        //        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing... </h1>'
                        //    });
                        //},
                        success: function (response2) {
                            $('#' + getBranchId).append("<option value='0'>All</option>");
                            $.each(response2.returnBranchList, function (i, v) {
                                $('#' + getBranchId).append($('<option>').text(v.Text).attr('value', v.Value));
                            });
                            //$.unblockUI();
                        },
                        complete: function () {
                            //$.unblockUI();
                        },
                        error: function () {
                        }
                    });

                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function () {
                }
            });
        }
    });

    $(document).on('change', '#recentBranchId, #branchId', function (event) {

        var allclass = $(event.target).attr('class');
        var getBranchId = "branchId";
        var getDepartmentId = "departmentId";
        var getCampusId = "campusId";
        var getOrganizationId = "organizationId";
        if (allclass.search('recent') !== -1) {
            getBranchId = "recentBranchId";
            getDepartmentId = "recentDepartmentId";
            getCampusId = "recentCampusId";
            getOrganizationId = "recentOrganizationId";
        }
        $("#" + getCampusId + " option").remove();
        var branchId = $(this).val();
        var organizationId = $("#" + getOrganizationId).val();
        if (branchId != null && branchId !== "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                cache: false,
                async: true,
                data: { organizationIds: organizationId, branchIds: branchId, isAuthorized: true, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $('#' + getCampusId).append("<option value='0'>All</option>");
                        $.each(response.returnCampusList, function (i, v) {
                            $('#' + getCampusId).append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function () {
                    $.unblockUI();
                }
            });
        }
    });

    $(document).on('click', '.editHrLeaveApplication', function () {
        try {
            $('#leaveContent').html("");
            $("#modalCustomMessage").html("");
            var leaveApplicationId = $(this).attr("data-laid");
            $.ajax({
                url: $("body").attr("data-project-root") + "Hr/LeaveApprovalHr/GenerateLeaveApplicationModalView",
                type: "POST",
                data: { leaveApplicationId: leaveApplicationId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    $.unblockUI();
                    $('#leaveContent').html(data);
                    $('#myModal').modal({ 'show': true });
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }

            });

        } catch (e) {
            console.log(e);
        }
    });
    
});

function validateApplicationModalRow(parentDiv) {
    console.log(parentDiv.find('.modalMemberPin').val());
    var success = true;
    var memberPin = parentDiv.find('.modalMemberPin').val();
    var memberLeaveTypeOptionSize = parentDiv.find('.modalMemberLeaveType option').size();
    var memberLeaveType = parentDiv.find('.modalMemberLeaveType').val();
    var memberLeaveDateFrom = parentDiv.find('.modalMemberLeaveDateFrom').val();
    var memberLeaveDateTo = parentDiv.find('.modalMemberLeaveDateTo').val();
    var memberLeaveNote = parentDiv.find('.modalMemberLeaveNote').val();

    if (memberPin.length <= 0 || memberPin === '') {
        success = false;
        parentDiv.find('.modalMemberPin').addClass('highlight');
    } else {
        parentDiv.find('.modalMemberPin').removeClass('highlight');
    }
    if (memberLeaveTypeOptionSize === 0 || (memberLeaveTypeOptionSize > 0 && (memberLeaveType.length <= 0 || memberLeaveType === ''))) {
        success = false;
        parentDiv.find('.modalMemberLeaveType').addClass('highlight');
    } else {
        parentDiv.find('.modalMemberLeaveType').removeClass('highlight');
    }
    if (memberLeaveDateFrom.length <= 0 || memberLeaveDateFrom === '') {
        success = false;
        parentDiv.find('.modalMemberLeaveDateFrom').addClass('highlight');
    } else {
        parentDiv.find('.modalMemberLeaveDateFrom').removeClass('highlight');
    }
    if (memberLeaveDateTo.length <= 0 || memberLeaveDateTo === '') {
        success = false;
        parentDiv.find('.modalMemberLeaveDateTo').addClass('highlight');
    } else {
        parentDiv.find('.modalMemberLeaveDateTo').removeClass('highlight');
    }
    if (memberLeaveNote.length < 5 || memberLeaveNote === '') {
        success = false;
        parentDiv.find('.modalMemberLeaveNote').addClass('highlight');
    } else {
        parentDiv.find('.modalMemberLeaveNote').removeClass('highlight');
    }
    return success;
}

function validateApplicationRow(parentTr) {
    var success = true;
    var memberPin = parentTr.find('.memberPin').val();
    //var memberName = parentTr.find('.memberName').val();
    var memberLeaveTypeOptionSize = parentTr.find('.memberLeaveType option').size();
    var memberLeaveType = parentTr.find('.memberLeaveType').val();
    var memberLeaveDateFrom = parentTr.find('.memberLeaveDateFrom').val();
    var memberLeaveDateTo = parentTr.find('.memberLeaveDateTo').val();
    //var memberLeaveDayCount = parentTr.find('.memberLeaveDayCount').val();
    var memberLeaveNote = parentTr.find('.memberLeaveNote').val();
    //var memberLeaveResMemPin = parentTr.find('.memberLeaveResMemPin').val();
    //var memberLeaveResMemName = parentTr.find('.memberLeaveResMemName').val();

    if (memberPin.length <= 0 || memberPin === '') {
        success = false;
        parentTr.find('.memberPin').addClass('highlight');
    } else {
        parentTr.find('.memberPin').removeClass('highlight');
    }
    if (memberLeaveTypeOptionSize === 0 || (memberLeaveTypeOptionSize > 0 && (memberLeaveType.length <= 0 || memberLeaveType === ''))) {
        success = false;
        parentTr.find('.memberLeaveType').addClass('highlight');
    } else {
        parentTr.find('.memberLeaveType').removeClass('highlight');
    }
    if (memberLeaveDateFrom.length <= 0 || memberLeaveDateFrom === '') {
        success = false;
        parentTr.find('.memberLeaveDateFrom').addClass('highlight');
    } else {
        parentTr.find('.memberLeaveDateFrom').removeClass('highlight');
    }
    if (memberLeaveDateTo.length <= 0 || memberLeaveDateTo === '') {
        success = false;
        parentTr.find('.memberLeaveDateTo').addClass('highlight');
    } else {
        parentTr.find('.memberLeaveDateTo').removeClass('highlight');
    }
    if (memberLeaveNote.length < 5 || memberLeaveNote === '') {
        success = false;
        parentTr.find('.memberLeaveNote').addClass('highlight');
    } else {
        parentTr.find('.memberLeaveNote').removeClass('highlight');
    }
    return success;
}

function DateDiff(start, end) {

    var df = Math.floor(start / (3600 * 24 * 1000)); //days as integer from..
    var dt = Math.floor(end / (3600 * 24 * 1000)); //days as integer from..
    var daysDiff = (dt - df) + 1; // exact dates
    return daysDiff;
}

function LeaveApplicationViewModel() {
    this.LeaveApplicationId = 0;
    this.TeamMemberPin = 0;
    //this.MemberName = "";
    this.DateFrom = "";
    this.DateTo = "";
    //this.Day = 0;
    this.LeaveNote = "";
    this.LeaveReason = "";
    this.ResponsibleMemberPin = 0;
    //this.RName = "";
    //this.Designation = "";
    this.LeaveStatus = 0;
    this.LeaveId = 0;
    //this.Leaves = [];
    this.LeaveApplicationViewModel = function () { };
}

function setLeaveApplicationRowCount(id) {
    var attendanceAdjustmentCount = $('#' + id + ' >tbody >tr').length;
    $("#leaveApplicationCount").html(attendanceAdjustmentCount);
}