﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentAcademicInfoExamSubjectDao : IBaseDao<StudentAcademicInfoExamSubject, long>
    {
        #region Operational Function
        bool SaveBoardSubject(StudentAcademicInfoExamSubject infoExamSubject);
        #endregion

        #region Single Instances Loading Function
        StudentAcademicInfoExamSubject LoadExamSubject(string name, string code, long studentExamId);
        #endregion

        #region List Loading Function
        IList<StudentAcademicInfoExamSubject> LoadExamSubject(long studentExamId);
        #endregion

        #region Others Function

        #endregion        
    }

    public class StudentAcademicInfoExamSubjectDao : BaseDao<StudentAcademicInfoExamSubject, long>, IStudentAcademicInfoExamSubjectDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function
        public bool SaveBoardSubject(StudentAcademicInfoExamSubject infoExamSubject)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Session.Save(infoExamSubject);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                return false;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public StudentAcademicInfoExamSubject LoadExamSubject(string name, string code, long studentExamId)
        {
            return Session.QueryOver<StudentAcademicInfoExamSubject>().Where(x => x.Name == name && x.Code == code && x.StudentExam.Id == studentExamId).SingleOrDefault<StudentAcademicInfoExamSubject>();
        }
        #endregion

        #region List Loading Function
        public IList<StudentAcademicInfoExamSubject> LoadExamSubject(long studentExamId)
        {
            return Session.QueryOver<StudentAcademicInfoExamSubject>().Where(x => x.StudentExam.Id == studentExamId).List<StudentAcademicInfoExamSubject>();
        }
        #endregion

        #region Others Function

        #endregion       
        
        #region Helper Function

        #endregion
    }
}
