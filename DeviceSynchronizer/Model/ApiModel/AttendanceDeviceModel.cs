﻿using System.Collections.Generic;
using DeviceSynchronizer.Helper;

namespace DeviceSynchronizer.Model.ApiModel
{
    public class AttendanceDeviceModel
    {
        public int Id { get; set; }
        public virtual string DeviceTypeCode { get; set; }
        public string Name { get; set; }
        public string DeviceModelNo { get; set; }
        public DeviceCommunicationType CommunicationType { get; set; }
        public string IPAddress { get; set; }
        public int Port { get; set; }
        public string CommunicationKey { get; set; }
        public int MachineNo { get; set; }
        public List<TeamMemberModel> TeamMembers { get; set; }
       // public int VersionNumber { get; set; }
        public bool IsReset { get; set; }
        public string LastUpdateTime { get; set; }
    }
}