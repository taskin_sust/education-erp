﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IBoardInformationService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<StudentProgram> LoadStudentProgram(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long studentExamId, string[] informationViewList);
        IList<MissingBoardRollListDto> GetMissingBoardRollStudents(List<UserMenu> userMenu, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null);
        IList<StudentProgram> GetBoardResultStudent(List<UserMenu> userMenu, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null);
        IList<StudentProgram> LoadStudentProgramForBoardResult(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId);
        IList<ViewBoardInfoSynchronizerDto> LoadStudentForInfoSynchronizer(List<UserMenu> userMenu, int start, int length, long programId, long sessionId, long studentExamId, string year, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batch, int? syncStatus, string prnNo, string name, string mobile);
        List<StudentProgram> LoadStudentProgramForUpdateStudentInfo(List<UserMenu> userMenu, long programId, long sessionId, long[] selectedBranch, long[] selectedCampus, string[] selectedBatchDays, string[] selectedBatchTime, long[] selectedBatch, long studentExamId, int? selectStatus);
        IList<StudentProgram> LoadStudentCompareWithBoard(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long studentExamId, string year, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, int[] religion, int gender, int version, string prnNo, string fullName, string fatherName, string institute);
        IList<ViewBoardRollListDto> LoadAuthorizedStudents(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, int selectStatus, string orderBy, string orderDir, int start, int length, string prnNo, string name, string mobile, string[] informationViewList);

        #endregion

        #region Others Function

        long GetMissingBoardRollStudentCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null);
        long GetBoardResultStudentCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null);
        long StudentForInfoSynchronizerCount(List<UserMenu> userMenu, long programId, long sessionId, long studentExamId, string year, long[] branch, long[] campus, string[] batchDays, string[] batchTimes, long[] batch, int? status);
        long CountStudentCompareWithBoard(List<UserMenu> userMenu, long programId, long sessionId, long studentExamId, string year, long[] branch, long[] campus, string[] batchDays, string[] batchTimes, long[] batch, int[] religion, int? gender, int? versionOfStudy);
        long GetBoardResultStatus(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, int? status);
        long GetAuthorizedStudentsCount(List<UserMenu> _userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, int selectStatus, string prnNo, string name, string mobile, string[] informationViewList);

        #endregion        
    
    }
    public class BoardInformationService : BaseService, IBoardInformationService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IStudentDao _studentDao;
        private readonly CommonHelper _commonHelper;
        private readonly IStudentAcademicInfoDao _studentAcademicInfoDao;
        public BoardInformationService(ISession session)
        {
            Session = session;
            _studentProgramDao = new StudentProgramDao() { Session = session };
            _studentDao = new StudentDao() { Session = session }; 
            _commonHelper = new CommonHelper();
            _studentAcademicInfoDao = new StudentAcademicInfoDao() { Session = session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<StudentProgram> LoadStudentProgram(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId
      , string[] batchDays, string[] batchTime, long[] batch, long studentExamId, string[] informationViewList)
        {
            IList<StudentProgram> studentPrograms;
            try
            {
                //bool allBranch = false;
                //bool allProgram = false;
                //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);

                studentPrograms = _studentProgramDao.LoadStudentProgram(programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batch, studentExamId, informationViewList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentPrograms;

        }

        public IList<ViewBoardInfoSynchronizerDto> LoadStudentForInfoSynchronizer(List<UserMenu> userMenu, int start, int length, long programId, long sessionId, long studentExamId,
             string year, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batch, int? syncStatus, string prnNo, string name, string mobile)
        {
            IList<ViewBoardInfoSynchronizerDto> students;
            try
            {

                List<long> authoOrganizationList = AuthHelper.LoadOrganizationIdList(userMenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationList, null, (branchId.ToList().Contains(SelectionType.SelelectAll)) ? null : branchId.ToList());
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, authoOrganizationList, branchIdList, (programId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(programId) : null);

                students = _studentAcademicInfoDao.LoadStudentForInfoSynchronizer(programIdList, branchIdList, start, length, sessionId, studentExamId, year, campusId, batchDays, batchTimes, batch, syncStatus, prnNo, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return students;

        }
        public List<StudentProgram> LoadStudentProgramForUpdateStudentInfo(List<UserMenu> userMenu, long programId, long sessionId, long[] selectedBranch,
           long[] selectedCampus, string[] selectedBatchDays, string[] selectedBatchTime, long[] selectedBatch,
           long studentExamId, int? selectStatus)
        {
            List<StudentProgram> studentPrograms;
            try
            {
                //bool allBranch = false;
                //bool allProgram = false;
                //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                studentPrograms = _studentProgramDao.LoadStudentProgramForUpdateStudentInfo(programIdList, branchIdList, programId, sessionId, selectedBranch, selectedCampus, selectedBatchDays, selectedBatchTime, selectedBatch, studentExamId, selectStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentPrograms;
        }
        public IList<StudentProgram> LoadStudentCompareWithBoard(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long studentExamId, string year,
            long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, int[] religion,
            int gender, int version, string prnNo, string fullName, string fatherName, string institute)
        {
            IList<StudentProgram> studentPrograms;
            try
            {
                //bool allBranch = false;
                //bool allProgram = false;
                //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                studentPrograms = _studentProgramDao.LoadStudentCompareWithBoard(start, length, programIdList, branchIdList, programId, sessionId, studentExamId, year, branchId, campusId, batchDays, batchTime, batchName, religion, gender, version, prnNo, fullName, fatherName, institute);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentPrograms;
        }

        public IList<MissingBoardRollListDto> GetMissingBoardRollStudents(List<UserMenu> userMenu, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null)
        {
            List<MissingBoardRollListDto> getData;
            try
            {
                //bool allBranch = false;
                //bool allProgram = false;
                //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
                //List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                //List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                
                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu);
                List<long> authoProgramIdList = AuthHelper.LoadProgramIdList(userMenu, authoOrganizationIdList, null, (programId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(programId) : null);
                List<long> authoBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationIdList, authoProgramIdList, (branchId.ToList().Contains(SelectionType.SelelectAll)) ? null : branchId.ToList());

                getData = _studentProgramDao.GetMissingBoardRollStudent(authoProgramIdList, authoBranchIdList, start, length, sessionId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return getData;
        }

        public long GetBoardResultStudentCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId
            , string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null
            , string mobile = null)
        {
            long boardResultStudentCount;
            try
            {
                //bool allBranch = false;
                //bool allProgram = false;
                //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                boardResultStudentCount = _studentProgramDao.GetBoardResultStudentCount(programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return boardResultStudentCount;
        }


        public IList<StudentProgram> GetBoardResultStudent(List<UserMenu> userMenu, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null
           , string mobile = null)
        {
            IList<StudentProgram> studentProgramsBoardResult;
            try
            {
                //bool allBranch = false;
                //bool allProgram = false;
                //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                studentProgramsBoardResult = _studentProgramDao.GetBoardResultStudent(programIdList, branchIdList, start, length, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentProgramsBoardResult;
        }

        public IList<StudentProgram> LoadStudentProgramForBoardResult(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId
            , string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId)
        {
            List<StudentProgram> getData;
            try
            {
                //bool allBranch = false;
                //bool allProgram = false;
                //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);

                getData = _studentProgramDao.LoadStudentProgramForBoardResult(programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return getData;
        }

        #endregion

        #region Others Function

        public long StudentForInfoSynchronizerCount(List<UserMenu> userMenu, long programId, long sessionId, long studentExamId, string year, long[] branch, long[] campus, string[] batchDays, string[] batchTimes, long[] batch, int? status)
        {
            long studentInfoCount;
            try
            {
                List<long> authoOrganizationList = AuthHelper.LoadOrganizationIdList(userMenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationList, null, (branch.ToList().Contains(SelectionType.SelelectAll)) ? null : branch.ToList());
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, authoOrganizationList, branchIdList, (programId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(programId) : null);

                studentInfoCount = _studentAcademicInfoDao.StudentForInfoSynchronizerCount(programIdList, branchIdList, sessionId, studentExamId, year, campus, batchDays, batchTimes, batch, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentInfoCount;
        }

        public long CountStudentCompareWithBoard(List<UserMenu> userMenu, long programId, long sessionId, long studentExamId, string year,
            long[] branch, long[] campus, string[] batchDays, string[] batchTimes, long[] batch, int[] religion, int? gender,
            int? versionOfStudy)
        {
            long studentCountForCompareData;
            try
            {
                //bool allBranch = false;
                //bool allProgram = false;
                //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                studentCountForCompareData = _studentProgramDao.CountStudentCompareWithBoard(programIdList, branchIdList, programId,
                 sessionId, studentExamId, year, branch, campus, batchDays, batchTimes, batch, religion, gender, versionOfStudy);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentCountForCompareData;
        }
      
        public long GetBoardResultStatus(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, int? status)
        {
            long boardResultStatus;
            try
            {
                //bool allBranch = false;
                //bool allProgram = false;
                //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                boardResultStatus = _studentProgramDao.GetBoardResultStatus(programIdList,branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return boardResultStatus;

        }
        
        public long GetMissingBoardRollStudentCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId , string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null , string name = null, string mobile = null)
        {
            long getData;
            try
            {
                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu);
                List<long> authoProgramIdList = AuthHelper.LoadProgramIdList(userMenu, authoOrganizationIdList, null, (programId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(programId) : null);
                List<long> authoBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationIdList, authoProgramIdList, (branchId.ToList().Contains(SelectionType.SelelectAll)) ? null : branchId.ToList());

                //getData = _studentProgramDao.GetMissingBoardRollStudentCount(programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);
                getData = _studentProgramDao.GetMissingBoardRollStudentCount(authoProgramIdList, authoBranchIdList, sessionId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return getData;
        }


        public long GetAuthorizedStudentsCount(List<UserMenu> _userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, int selectStatus, string prnNo, string name, string mobile, string[] informationViewList)
        {
            try
            {
                List<long> authorizedOrganization = AuthHelper.LoadOrganizationIdList(_userMenu);//by menu
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(_userMenu, authorizedOrganization, null, (programId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(programId) : null);//by menu
                List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(_userMenu, authorizedOrganization, authorizedProgramLists, (branchId.ToList().Contains(SelectionType.SelelectAll)) ? null : branchId.ToList());

                return _studentAcademicInfoDao.GetAuthorizedStudentsCount(authorizedProgramLists, authorizedBranchIdList, sessionId, campusId, batchDays, batchTime, batchId, selectStatus, prnNo, name, mobile, informationViewList);
                //return _studentAcademicInfoDao.GetAuthorizedStudentsCount(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, selectStatus, prnNo, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ViewBoardRollListDto> LoadAuthorizedStudents(List<UserMenu> _userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, int selectStatus, string orderBy, string orderDir, int start, int length, string prnNo, string name, string mobile, string[] informationViewList)
        {
            try
            {
                //List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu); 
                //List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> authorizedOrganization = AuthHelper.LoadOrganizationIdList(_userMenu);//by menu
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(_userMenu, authorizedOrganization, null, (programId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(programId) : null);//by menu
                List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(_userMenu, authorizedOrganization, authorizedProgramLists, (branchId.ToList().Contains(SelectionType.SelelectAll)) ? null : branchId.ToList());

                return _studentAcademicInfoDao.LoadAuthorizedStudents(authorizedProgramLists, authorizedBranchIdList, sessionId, campusId, batchDays, batchTime, batchId, selectStatus, orderBy, orderDir, start, length, prnNo, name, mobile, informationViewList);
                //return _studentAcademicInfoDao.LoadAuthorizedStudents(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, selectStatus, orderBy, orderDir, start, length, prnNo, name, mobile);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper Function

        #endregion
    }
}
