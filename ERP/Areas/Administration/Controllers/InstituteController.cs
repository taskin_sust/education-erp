﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules;
using UdvashERP.App_Start;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class InstituteController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea"); 
        #endregion
        
        #region Objects/Properties/Services/Dao & Initialization
        private readonly IInstitutionService _instituteService;
        private IInstitutionCategoryService _institutionCategoryService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        public InstituteController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _institutionCategoryService=new InstitutionCategoryService(session);
                _instituteService = new InstituteService(session);
                _userService = new UserService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
        } 
        #endregion
       
        #region Index/ Manage Page
        public ActionResult ManageInstitute()
        {
            try
            {
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            return View();
        }


        #region Render Data Table
        [HttpPost]
        public ActionResult InstituteListResult(int draw, int start, int length, string Name, string Category, string District, string Eiin, string Status, string Rank)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    int sr = start + 1;
                    string orderBy = "";
                    string orderDir = "";
                    #region OrderBy and Direction
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "1":
                                orderBy = "Name";
                                break;
                            case "2":
                                orderBy = "Category";
                                break;
                            case "3":
                                orderBy = "District";
                                break;
                            case "4":
                                orderBy = "Eiin";
                                break;
                            case "5":
                                orderBy = "Rank";
                                break;
                            case "6":
                                orderBy = "Status";
                                break;

                            default:
                                orderBy = "";
                                break;
                        }
                    }
                    #endregion
                    var institute = new Institute();
                    List<Institute> instituteList = _instituteService.GetInstituteList(start, length, orderBy, orderDir.ToUpper(), Name, Category, District, Eiin, Status, Rank).ToList();
                    if (orderBy == "Category")
                    {
                        if (orderDir.ToUpper() == "ASC")
                        {
                            instituteList = instituteList.OrderBy(x => x.InstituteCategory.Name).ToList();
                        }
                        else
                        {
                            instituteList = instituteList.OrderByDescending(x => x.InstituteCategory.Name).ToList();
                        }

                    }
                    if (!string.IsNullOrEmpty(Category))
                    {
                        instituteList = instituteList.Where(x => x.InstituteCategory.Name.ToLower().Contains(Category.ToLower())).ToList();
                    }
                    int recordsTotal = instituteList.Count;
                    long recordsFiltered = recordsTotal;
                    instituteList = instituteList.Skip(start).Take(length).ToList();

                    int[] MaxMinRank = new int[2];
                    MaxMinRank[0] = _instituteService.GetMinRank(institute); //1;
                    MaxMinRank[1] = _instituteService.GetMaximumRank(institute);
                    var data = new List<object>();
                    foreach (var c in instituteList)
                    {
                        var str = new List<string>();
                        str.Add((sr++).ToString());
                        str.Add(c.Name);
                        str.Add(c.InstituteCategory.Name);
                        str.Add(c.District);
                        str.Add(c.Eiin);
                        string rankbtn = c.Rank.ToString();
                        if (c.Rank > MaxMinRank[0])
                        {
                            rankbtn += "<a href='#'  id='" + c.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                        }
                        if (c.Rank < MaxMinRank[1])
                        {
                            rankbtn += "<a href='#'  id='" + c.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                        }
                        str.Add(rankbtn);
                        str.Add(c.Status == 1 ? "Active" : "Inactive");
                        if (c.InstituteCategory != null)
                        {
                            if (c.InstituteCategory.IsUniversity == true)
                            {
                                str.Add("<a href='" + Url.Action("Details", "Institute") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;<a href='" + Url.Action("EditUniversity", "Institute") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;<a id='" + c.Id.ToString() + "' href='#' data-name='" + c.Name.ToString() + "' class='glyphicon glyphicon-trash'> </a>");
                            }
                            else
                            {
                                str.Add("<a href='" + Url.Action("Details", "Institute") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;<a href='" + Url.Action("EditSchoolCollege", "Institute") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;<a id='" + c.Id.ToString() + "' href='#' data-name='" + c.Name.ToString() + "' class='glyphicon glyphicon-trash'> </a>");
                            }
                        }
                        data.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return HttpNotFound();

        }
        
        #endregion
        #endregion

        #region Operational Function

        #region Save Operation
        public ActionResult CreateSchoolCollege()
        {
            try
            {
                IList<InstituteCategory> schoolCollegeCategories = _institutionCategoryService.GetInstituteCategoryList(false);
                ViewBag.SchoolCollegeCategories = new SelectList(schoolCollegeCategories, "Id", "Name");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }


        [HttpPost]
        public ActionResult CreateSchoolCollege(Institute institute, long categoryId)
        {
            try
            {
                var schoolCollegeCategories = _institutionCategoryService.GetInstituteCategoryList(false);
                ViewBag.SchoolCollegeCategories = new SelectList(schoolCollegeCategories, "Id", "Name");
                InstituteCategory instituteCategory = _institutionCategoryService.LoadInstituteCategoryById(categoryId);
                // IList<Institute> instituteByEiin = _instituteService.LoadInstituteInstituteByEiin(institute.Eiin, institute.Id);
                institute.InstituteCategory = instituteCategory;
                IList<Institute> instituteByName = _instituteService.LoadInstituteByName(institute.Id, institute.Name, institute.ShortName, categoryId, institute.District, institute.Eiin);
                int _messageCode = 1;
                if (instituteByName.Count == 0)
                {
                    _messageCode = _instituteService.Save(institute);
                    if (_messageCode == Constants.MessageSuccess)
                    {
                        ViewBag.SuccessMessage = "School/College Addded Successfully";
                        ModelState.Clear();
                        return View(new Institute());
                    }
                    else if (_messageCode == Constants.MessageError)
                    {
                        ViewBag.ErrorMessage = "School/College Addition Fail";
                    }
                    else
                    {
                        ViewBag.InfoMessage = "";
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "School/College already exists";
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }
        public ActionResult CreateUniversity()
        {
            try
            {
                IList<InstituteCategory> universityCategory = _institutionCategoryService.GetInstituteCategoryList(true);
                ViewBag.SchoolCollegeCategories = new SelectList(universityCategory, "Id", "Name");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }


        [HttpPost]
        public ActionResult CreateUniversity(Institute institute, long categoryId)
        {
            try
            {
                var universityCategory = _institutionCategoryService.GetInstituteCategoryList(true);
                ViewBag.SchoolCollegeCategories = new SelectList(universityCategory, "Id", "Name");
                InstituteCategory instituteCategory = _institutionCategoryService.LoadInstituteCategoryById(categoryId);
                institute.InstituteCategory = instituteCategory;
                // IList<Institute> instituteByEiin = _instituteService.LoadInstituteInstituteByEiin(institute.Eiin, institute.Id);
                IList<Institute> instituteByName = _instituteService.LoadInstituteByName(institute.Id, institute.Name, institute.ShortName, categoryId, institute.District, institute.Eiin);

                int _messageCode = 1;
                if (instituteByName.Count == 0)
                {
                    _messageCode = _instituteService.Save(institute);

                    if (_messageCode == Constants.MessageSuccess)
                    {
                        ViewBag.SuccessMessage = "University Addded Successfully";
                        ModelState.Clear();
                        return View(new Institute());
                    }
                    else if (_messageCode == Constants.MessageError)
                    {
                        ViewBag.ErrorMessage = "University Addition Fail";
                    }
                    else
                    {
                        ViewBag.InfoMessage = "";
                    }

                }
                else
                {

                    ViewBag.ErrorMessage = "University already exists";
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        #endregion

        #region Update Operation
        public ActionResult EditSchoolCollege(long id)
        {
            var institute = new Institute();
            try
            {
                IList<InstituteCategory> instituteCategories = _institutionCategoryService.GetInstituteCategoryList().Where(x => x.IsUniversity == false).ToList();
                institute = _instituteService.LoadById(id);
                long categoryId = institute.InstituteCategory.Id;
                ViewBag.InstituteCategories = new SelectList(instituteCategories, "Id", "Name", categoryId.ToString());
                if (institute == null)
                {
                    return HttpNotFound();
                }

                ViewBag.STATUSTEXT = _commonHelper.GetStatus();

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View(institute);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSchoolCollege(long id, Institute institute, long categoryId)
        {
            try
            {

                IList<InstituteCategory> instituteCategories = _institutionCategoryService.GetInstituteCategoryList().Where(x => x.IsUniversity == false).ToList();
                ViewBag.InstituteCategories = new SelectList(instituteCategories, "Id", "Name", categoryId.ToString());
                bool messageCode = _instituteService.Update(institute,categoryId);

                    if (messageCode)
                    {
                        ViewBag.SuccessMessage = "School/College Updated Successfully";
                        //  return RedirectToAction("ManageInstitute");
                    }
                    ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                    return View(institute);
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View(institute);
        }

        public ActionResult EditUniversity(long id)
        {
            var institute = new Institute();
            try
            {
                IList<InstituteCategory> instituteCategories = _institutionCategoryService.GetInstituteCategoryList().Where(x => x.IsUniversity == true).ToList();
                institute = _instituteService.LoadById(id);
                long categoryId = institute.InstituteCategory.Id;
                ViewBag.InstituteCategories = new SelectList(instituteCategories, "Id", "Name", categoryId.ToString());
                if (institute == null)
                {
                    return HttpNotFound();
                }

                ViewBag.STATUSTEXT = _commonHelper.GetStatus();

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View(institute);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUniversity(long id, Institute institute, long categoryId)
        {
            try
            {

                IList<InstituteCategory> instituteCategories = _institutionCategoryService.GetInstituteCategoryList().Where(x => x.IsUniversity == true).ToList();
                ViewBag.InstituteCategories = new SelectList(instituteCategories, "Id", "Name", categoryId.ToString());
                bool messageCode = _instituteService.Update(institute, categoryId);

                if (messageCode)
                {
                    ViewBag.SuccessMessage = "University Updated Successfully";
                }
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                return View(institute);
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View(institute);
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                Institute deleteInstitute = _instituteService.LoadById(id);
                deleteInstitute.Status = Institute.EntityStatus.Delete;
                _instituteService.Delete(deleteInstitute);
                return Json(new Response(true, "Institute Delete Successful !"));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return Json(new Response(false, "Institute Delete Fail !"));
        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            var institute = new Institute();
            try
            {
                institute = _instituteService.LoadById(id);
                if (institute != null)
                {
                    institute.CreateByText = _userService.GetUserNameByAspNetUserId(institute.CreateBy);
                    institute.ModifyByText = _userService.GetUserNameByAspNetUserId(institute.ModifyBy);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            if (institute == null)
            {
                return HttpNotFound();
            }
            return View(institute);
        }
        #endregion

        #region Rank Operation
        public ActionResult RankChange(int id, int current, string action)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    Institute instituteUpdateObj = _instituteService.LoadById(Convert.ToInt64(id));
                    if (instituteUpdateObj == null)
                    {
                        return HttpNotFound();
                    }
                    int newRank;
                    if (action == "up")
                        newRank = instituteUpdateObj.Rank - 1;
                    else
                        newRank = instituteUpdateObj.Rank + 1;
                    var instituteOldObj = _instituteService.LoadByRankNextOrPrevious(newRank, action);//LoadByRank(newRank);
                    newRank = instituteOldObj.Rank;
                    instituteOldObj.Rank = instituteUpdateObj.Rank;
                    instituteUpdateObj.Rank = newRank;
                    bool isSuccessfullyUpdateRank = _instituteService.UpdateRank(instituteOldObj, instituteUpdateObj);
                    if (isSuccessfullyUpdateRank)
                    {
                        return Json("Successfully changed");
                    }
                    return Json("Something Wrong Occured!!");
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                }
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return HttpNotFound();
        }
        #endregion

        #endregion  
    }
}
