function mobileCheck(mobileNumber) {
    mobileNumber = mobileNumber.replace(/\W+/g, "");
    var chkVal = mobileNumber.match(/^(?:\+?88)?0?1[15-9]\d{8}$/);
    if (chkVal != null) {
        if (mobileNumber.length == 11) {
            chkVal = '88' + chkVal;
        }
        else if (mobileNumber.length == 10) {
            chkVal = '880' + chkVal;
        }
    }
    return chkVal;
}
$(document).ready(function () {
    //$("#courseError").hide();
    $("#courseError").hide();
    $("#RegistrationSubmitForm").submit(function () {
        return false;
    });


    $('body').keydown(function (e) {
        if (e.keyCode == 13) {
            responseReturn();
        }
    });  
});

$(document).on('change', '#Program', function (event) {
    $("#Session option").remove();
    $('#Session').append("<option value=''>Select Session</option>");
    $("#CourseList").html("");
    $("#Branch option").remove();
    $('#Branch').append("<option value=''>Select Branch</option>");
    $("#Campus option").remove();
    $('#Campus').append("<option value=''>Select Campus</option>");
    $("#BatchDays option").remove();
    $('#BatchDays').append("<option value=''>Select Batch Days</option>");
    $("#BatchTime option").remove();
    $('#BatchTime').append("<option value=''>Select Batch Time</option>");
    $("#Batch option").remove();
    $('#Batch').append("<option value=''>Select Batch</option>");
    var programId = $(this).val();
    if (programId != "" && (/^\d+$/.test(programId))) {
        $("#Program").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Program").removeClass("highlight");
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForSession",
            cache: false,
            async: true,
            data: { "programId": programId },
            beforeSend: function() {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function(result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.each(result.returnList, function(i, v) {
                        $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function() {
                $.unblockUI();
            },
            error: function(result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    } else {
        $("#Program").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Program field is required.");
        $("#Program").addClass("highlight");
    }
});
$(document).on('change', '#Session', function (event) {
    $("#Branch option").remove();
    $('#Branch').append("<option value=''>Select Branch</option>");
    $("#Campus option").remove();
    $('#Campus').append("<option value=''>Select Campus</option>");
    $("#BatchDays option").remove();
    $('#BatchDays').append("<option value=''>Select Batch Days</option>");
    $("#BatchTime option").remove();
    $('#BatchTime').append("<option value=''>Select Batch Time</option>");
    $("#Batch option").remove();
    $('#Batch').append("<option value=''>Select Batch</option>");
    var programId = $("#Program").val();
    var sessionId = $(this).val();
    if (sessionId != "" && (/^\d+$/.test(sessionId)))
    {
        $("#Session").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Session").removeClass("highlight");
    }
    else
    {
        $("#Session").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Session field is required.");
        $("#Session").addClass("highlight");
    }

    if (programId != "" && (/^\d+$/.test(programId)) && sessionId != "" && (/^\d+$/.test(sessionId))) {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForBranch",
            cache: false,
            async: true,
            data: { "programId": programId, "sessionId": sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.each(result.returnList, function (i, v) {
                        $('#Branch').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });

        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForCourseInformation",
            cache: false,
            async: true,
            data: { "programId": programId, "sessionId": sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.hasOwnProperty("IsSuccess")) {

                } else {
                    $("#CourseList").html(result);
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });

        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForExamBoard",
            cache: false,
            async: true,
            data: { "programId": programId, "sessionId": sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.hasOwnProperty("IsSuccess")) {

                } else {
                    $("#ExamBoard").html(result);
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});
$(document).on('click', '.course-subject-check', function (event) {
    var totalChecked = $(this).parent().find(".course-subject-check:checked").length;
    //console.log(totalChecked);
    if (totalChecked == 0) {
        $(this).parents(".panel-default").find(".course-name-check").prop('checked', false);
    }
    else {
        $(this).parents(".panel-default").find(".course-name-check").prop('checked', true);
    }

    var courseError = 0;
    var message1 = '';
    $(".course-name-check").each(function () {
        var indCourseError = 0;
        var totalChecked = $(this).parents(".panel-default").find(".course-subject-check:checked").length;
        var maxsubject = $(this).attr("data-maxsubject");
        var minsubject = $(this).attr("data-minsubject");
        var isCompulsary = $(this).attr("data-is-compulsary");
        var courseName = $(this).attr("data-course-name");
        console.log(minsubject);
        console.log(totalChecked);
        if ($(this).is(':checked')) {
            if (totalChecked > maxsubject) {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select maximum ' + maxsubject + ' subjects for course: ' + courseName + ' </div>';
                indCourseError = 1;
            }
            else if (totalChecked < minsubject) {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select minimum ' + minsubject + ' subjects for course: ' + courseName + '</div>';
                indCourseError = 1;
            }           
        }
        else {
            if (isCompulsary == "True") {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> ' + courseName + ' is a Compulsary Course</div>';
                indCourseError = 1;
            }            
        }

        if (indCourseError == 1)
            courseError = 1;

    });

    if (courseError == 1) {
        $("#courseError").show().html(message1);
    }
    else {
        var totalChecked = $(".course-name-check:checked").length;
        if (totalChecked == 0) {
            $("#courseError").show().html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You must select one or more Course </div>');
        }
        else {
            $("#courseError").empty().hide();     
        }
    }
});
$(document).on('click', '.course-name-check', function (event) {
   // alert("course-name-check");
    event.stopPropagation();
    if ($(this).is(':checked')) {
        $(this).parents(".panel-default").find(".course-subject-check").each(function() {
            $(this).prop('checked', true);
        });
    }
    else
    {
        $(this).parents(".panel-default").find(".course-subject-check").each(function () {
            $(this).prop('checked', false);
        });
    }

    var courseError = 0;
    var message1 = '';
    $(".course-name-check").each(function () {
        var indCourseError = 0;
        var totalChecked = $(this).parents(".panel-default").find(".course-subject-check:checked").length;
        var maxsubject = $(this).attr("data-maxsubject");
        var minsubject = $(this).attr("data-minsubject");
        var isCompulsary = $(this).attr("data-is-compulsary");
        var courseName = $(this).attr("data-course-name");
        console.log(minsubject);
        console.log(totalChecked);
        if ($(this).is(':checked')) {
            if (totalChecked > maxsubject) {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select maximum ' + maxsubject + ' subjects for course: ' + courseName + ' </div>';
                indCourseError = 1;
            }
            else if (totalChecked < minsubject) {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select minimum ' + minsubject + ' subjects for course: ' + courseName + '</div>';
                indCourseError = 1;
            }
        }
        else {
            if (isCompulsary == "True") {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> ' + courseName + ' is a Compulsary Course</div>';
                indCourseError = 1;
            }
        }

        if (indCourseError == 1)
            courseError = 1;

    });

    if (courseError == 1) {
        $("#courseError").show().html(message1);
    }
    else {
        var totalChecked = $(".course-name-check:checked").length;
        if (totalChecked == 0) {
            $("#courseError").show().html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You must select one or more Course </div>');
        }
        else {
            $("#courseError").empty().hide();
        }
    }
});
$(document).on('change', '#Branch', function(event) {
    $("#Campus option").remove();
    $('#Campus').append("<option value=''>Select Campus</option>");
    $("#BatchDays option").remove();
    $('#BatchDays').append("<option value=''>Select Batch Days</option>");
    $("#BatchTime option").remove();
    $('#BatchTime').append("<option value=''>Select Batch Time</option>");
    $("#Batch option").remove();
    $('#Batch').append("<option value=''>Select Batch</option>");
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var branchId = $(this).val();

    if (branchId != "" && (/^\d+$/.test(branchId))) {
        $("#Branch").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Branch").removeClass("highlight");
    }
    else {
        $("#Branch").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Branch field is required.");
        $("#Branch").addClass("highlight");
    }

    if (programId != "" && (/^\d+$/.test(programId)) && sessionId != "" && (/^\d+$/.test(sessionId)) && branchId != "" && (/^\d+$/.test(branchId))) {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForCampus",
            cache: false,
            async: true,
            data: { "programId": programId, "sessionId": sessionId, "branchId": branchId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.each(result.returnList, function (i, v) {
                        $('#Campus').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});
$(document).on('change', '#Campus', function(event) {
    $("#BatchDays option").remove();
    $('#BatchDays').append("<option value=''>Select Batch Days</option>");
    $("#BatchTime option").remove();
    $('#BatchTime').append("<option value=''>Select Batch Time</option>");
    $("#Batch option").remove();
    $('#Batch').append("<option value=''>Select Batch</option>");

    
    
    var versionOfStudy = $("#VersionOfStudy").val();
    var gender = $("#Gender").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var branchId = $("#Branch").val();
    var campusId = $(this).val();
    if (campusId != "" && (/^\d+$/.test(campusId))) {
        $("#Campus").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Campus").removeClass("highlight");
    }
    else {
        $("#Campus").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Campus field is required.");
        $("#Campus").addClass("highlight");
    }
    if (programId != "" && (/^\d+$/.test(programId)) &&
        sessionId != "" && (/^\d+$/.test(sessionId)) &&
        branchId != "" && (/^\d+$/.test(branchId)) &&
        campusId != "" && (/^\d+$/.test(campusId)) && 
        versionOfStudy != "" && (/^\d+$/.test(versionOfStudy)) &&
        gender != "" && (/^\d+$/.test(gender))
        ) {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForBatchDays",
            cache: false,
            async: true,
            data: { "programId": programId, "sessionId": sessionId, "branchId": branchId, "campusId": campusId, "versionOfStudy": versionOfStudy, "gender": gender },
            beforeSend: function() {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function(result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.each(result.returnList, function(i, v) {
                        $('#BatchDays').append($('<option>').text(v.Text).attr('value', v.Text));
                    });
                }
            },
            complete: function() {
                $.unblockUI();
            },
            error: function(result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});
$(document).on('change', '#BatchDays', function (event) {
    $("#BatchTime option").remove();
    $('#BatchTime').append("<option value=''>Select Batch Time</option>");
    $("#Batch option").remove();
    $('#Batch').append("<option value=''>Select Batch</option>");
    var versionOfStudy = $("#VersionOfStudy").val();
    var gender = $("#Gender").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var branchId = $("#Branch").val();
    var campusId = $("#Campus").val();
    var batchDays = $(this).val(); 

    if (batchDays != "") {
        $("#BatchDays").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#BatchDays").removeClass("highlight");
    }
    else {
        $("#BatchDays").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Batch Days field is required.");
        $("#BatchDays").addClass("highlight");
    }
    
    if (programId != "" && (/^\d+$/.test(programId)) &&
        sessionId != "" && (/^\d+$/.test(sessionId)) &&
        branchId != "" && (/^\d+$/.test(branchId)) &&
        campusId != "" && (/^\d+$/.test(campusId)) && batchDays != "" && 
        versionOfStudy != "" && (/^\d+$/.test(versionOfStudy)) &&
        gender != "" && (/^\d+$/.test(gender))
        ) {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForBatchTime",
            cache: false,
            async: true,
            data: { "programId": programId, "sessionId": sessionId, "branchId": branchId, "campusId": campusId, "batchDays": batchDays, "versionOfStudy": versionOfStudy, "gender": gender },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.each(result.returnList, function (i, v) {
                        $('#BatchTime').append($('<option>').text(v.Text).attr('value', v.Text));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});
$(document).on('change', '#BatchTime', function (event) {
    $("#Batch option").remove();
    $('#Batch').append("<option value=''>Select Batch</option>");
    var versionOfStudy = $("#VersionOfStudy").val();
    var gender = $("#Gender").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var branchId = $("#Branch").val();
    var campusId = $("#Campus").val();
    var batchDays = $("#BatchDays").val();
    var batchTime = $(this).val();
    if (batchTime != "") {
        $("#BatchTime").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#BatchTime").removeClass("highlight");
    }
    else {
        $("#BatchTime").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Batch Time field is required.");
        $("#BatchTime").addClass("highlight");
    }
    if (programId != "" && (/^\d+$/.test(programId)) &&
        sessionId != "" && (/^\d+$/.test(sessionId)) &&
        branchId != "" && (/^\d+$/.test(branchId)) &&
        campusId != "" && (/^\d+$/.test(campusId)) &&
        batchDays != "" && batchTime != "" && 
        versionOfStudy != "" && (/^\d+$/.test(versionOfStudy)) &&
        gender != "" && (/^\d+$/.test(gender))
        ) {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForBatch",
            cache: false,
            async: true,
            data: { "programId": programId, "sessionId": sessionId, "branchId": branchId, "campusId": campusId, "batchDays": batchDays, "batchTime": batchTime, "versionOfStudy": versionOfStudy, "gender": gender },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.each(result.returnList, function (i, v) {
                        $('#Batch').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});
$(document).on("input propertychange", "#Name", function (event) {
    if ($(this).val() == "") {
        $("#Name").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Name field is required.");
        $("#Name").addClass("highlight");
    }
    else {
        $("#Name").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Name").removeClass("highlight");
    }
});
$(document).on("input propertychange", "#MobNumber", function (event) {
    var mobNumber = $("#MobNumber").val();
    mobNumber = mobNumber.replace(/\W+/g, "");
    var chkVal = mobNumber.match(/^(?:\+?88)?0?1[15-9]\d{8}$/);
    if (mobNumber == "") {
        $("#MobNumber").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Mobile field is required.");
        $("#MobNumber").addClass("highlight");
    }
    else if (chkVal == null) {
        $("#MobNumber").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Mobile number is not valid.");
        $("#MobNumber").addClass("highlight");
    }
    else {
        if (mobNumber.length == 11) {
            chkVal = '88' + chkVal;
        }
        else if (mobNumber.length == 10) {
            chkVal = '880' + chkVal;
        }
        $("#MobNumber").val(chkVal);
        $("#MobNumber").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#MobNumber").removeClass("highlight");
    }
});
$(document).on('change', '#VersionOfStudy', function (event) {
    $("#Campus").val($("#Campus option:first").val());
    $("#BatchDays option").remove();
    $('#BatchDays').append("<option value=''>Select Batch Days</option>");
    $("#BatchTime option").remove();
    $('#BatchTime').append("<option value=''>Select Batch Time</option>");
    $("#Batch option").remove();
    $('#Batch').append("<option value=''>Select Batch</option>");
    var versionOfStudy = $(this).val();
    if (versionOfStudy != "" && (/^\d+$/.test(versionOfStudy))) {
        $("#VersionOfStudy").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#VersionOfStudy").removeClass("highlight");
    }
    else {
        $("#VersionOfStudy").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Version Of Study field is required.");
        $("#VersionOfStudy").addClass("highlight");
    }
});
$(document).on('change', '#Gender', function (event) {
    $("#Campus").val($("#Campus option:first").val());
    $("#BatchDays option").remove();
    $('#BatchDays').append("<option value=''>Select Batch Days</option>");
    $("#BatchTime option").remove();
    $('#BatchTime').append("<option value=''>Select Batch Time</option>");
    $("#Batch option").remove();
    $('#Batch').append("<option value=''>Select Batch</option>");
    var gender = $(this).val();
    if (gender != "" && (/^\d+$/.test(gender))) {
        $("#Gender").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Gender").removeClass("highlight");
    }
    else {
        $("#Gender").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Gender field is required.");
        $("#Gender").addClass("highlight");
    }
});
$(document).on('change', '#Religion', function (event) {
    var religion = $(this).val();
    if (religion != "" && (/^\d+$/.test(religion))) {
        $("#Religion").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Religion").removeClass("highlight");
    }
    else {
        $("#Religion").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Religion field is required.");
        $("#Religion").addClass("highlight");
    }
});
$(document).on('change', '#Batch', function (event) {
    var batch = $(this).val();
    if (batch != "" && (/^\d+$/.test(batch))) {
        $("#Batch").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Batch").removeClass("highlight");
    }
    else {
        $("#Batch").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Batch field is required.");
        $("#Batch").addClass("highlight");
    }
});
$(document).on('click', '#displayPaymentInfo', function (event) {
    studentInfo = {};
    var isSuccess = 0;
    var name        = $("#Name").val();
    var mobNumber   = $("#MobNumber").val(); 
    var versionOfStudy  = $("#VersionOfStudy").val();
    var gender        = $("#Gender").val();
    var religion        = $("#Religion").val(); 

    var programId   = $("#Program").val();
    var sessionId   = $("#Session").val();
    var branchId    = $("#Branch").val();
    var campusId    = $("#Campus").val();
    var batchDays   = $("#BatchDays").val();
    var batchTime   = $("#BatchTime").val();
    var batch       = $("#Batch").val();

    if (name == "") {
        //var message = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> </div>';
        $("#Name").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Name field is required.");
        $("#Name").addClass("highlight");
        isSuccess = isSuccess+1;
    }
    else {
        $("#Name").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Name").removeClass("highlight");
        //isSuccess = 1;       
    }
    
    mobNumber = mobNumber.replace(/\W+/g, "");
    var chkVal = mobNumber.match(/^(?:\+?88)?0?1[15-9]\d{8}$/);
    if (mobNumber == "") {
        $("#MobNumber").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Mobile field is required.");
        $("#MobNumber").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (chkVal == null) {
        $("#MobNumber").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Mobile number is not valid.");
        $("#MobNumber").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#MobNumber").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#MobNumber").removeClass("highlight");
        //isSuccess = 1;
    }

    if (versionOfStudy == "") {
        $("#VersionOfStudy").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Version Of Study field is required.");
        $("#VersionOfStudy").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(versionOfStudy))) {
        $("#VersionOfStudy").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Version Of Study field is required.");
        $("#VersionOfStudy").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#VersionOfStudy").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#VersionOfStudy").removeClass("highlight");
        //isSuccess = 1;
    }

    if (gender == "") {
        $("#Gender").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Gender field is required.");
        $("#Gender").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(gender))) {
        $("#Gender").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Gender field is required.");
        $("#Gender").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Gender").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Gender").removeClass("highlight");
       // isSuccess = 1;
    }

    if (religion == "") {
        $("#Religion").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Religion field is required.");
        $("#Religion").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(religion))) {
        $("#Religion").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Religion field is required.");
        $("#Religion").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Religion").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Religion").removeClass("highlight");        
    }

    if (programId=="") {
        $("#Program").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Program field is required.");
        $("#Program").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(programId))) {
        $("#Program").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Program field is required.");
        $("#Program").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Program").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Program").removeClass("highlight");
        //isSuccess = 1;
    }

    if (sessionId == "") {
        $("#Session").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Session field is required.");
        $("#Session").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(sessionId))) {
        $("#Session").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Session field is required.");
        $("#Session").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Session").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Session").removeClass("highlight");
        
    }
    
    if (branchId == "") {
        $("#Branch").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Branch field is required.");
        $("#Branch").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(branchId))) {
        $("#Branch").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Branch field is required.");
        $("#Branch").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Branch").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Branch").removeClass("highlight");
      //  isSuccess = 1;
    }

    if (campusId == "") {
        $("#Campus").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Campus field is required.");
        $("#Campus").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(campusId))) {
        $("#Campus").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Campus field is required.");
        $("#Campus").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Campus").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Campus").removeClass("highlight");
       // isSuccess = 1;
    }

    if (batchDays == "") {
        $("#BatchDays").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The BatchDays field is required.");
        $("#BatchDays").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#BatchDays").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#BatchDays").removeClass("highlight");
        //isSuccess = 1;
    }

    if (batchTime == "") {
        $("#BatchTime").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Batch Time field is required.");
        $("#BatchTime").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#BatchTime").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#BatchTime").removeClass("highlight");
        //isSuccess = 1;
    }

    if (batch == "") {
        $("#Batch").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Batch field is required.");
        $("#Batch").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(batch))) {
        $("#Batch").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Batch field is required.");
        $("#Batch").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Batch").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Batch").removeClass("highlight");
        //isSuccess = 1;
    }

    var boardExamError = 0;
    var boardExamList = [];
    var message = '';
    ExamBoardViewModelList = {};
    //console.log("ExamIds Start");
    $(".examIdInput").each(function () {
        //boardExamList.push($(this).val());
        var examId = $(this).val();
        var examYear = $("#examYear_" + examId).val();
        var examBoard = $("#examBoard_" + examId).val();
        var boardRoll = $("#boardRoll_" + examId).val();
        var boardRegistration = $("#boardRegistration_" + examId).val();
        if ($.isNumeric(examYear) && examYear.length == 4 && $.isNumeric(boardRoll) && $.isNumeric(boardRegistration)) {
            boardExam = {};

            boardExam["Id"] = 0;
            boardExam["Name"] = "";
            boardExam["Year"] = examYear;
            boardExam["Board"] = examBoard;
            boardExam["Roll"] = boardRoll;
            boardExam["RegistrationNumber"] = boardRegistration;
            boardExam["ProgramId"] = 0;
            boardExam["StudentExamId"] = examId;
            boardExam["StudentExamName"] = "";
            boardExam["SessionId"] = 0;
            //console.log(boardExam);
            boardExamList.push(boardExam);
            boardExamError = 2;
        }
        else if (boardExamError != 2) {
            boardExamError = 1;
        }
    });
    //console.log("ExamIds End");
    if (boardExamError == 1) {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You must enter at least one Valid board info</div>';
        $("#examBoardError").show().html(message);
        isSuccess = isSuccess + 1;
    }
    else {
        $("#examBoardError").empty().hide();
    }

    var courseError = 0;
    var courseList = [];
    //var message = '';
    $(".course-name-check").each(function () {
        var indCourseError = 0;
        var totalChecked = $(this).parents(".panel-default").find(".course-subject-check:checked").length;
        var maxsubject = $(this).attr("data-maxsubject");
        //var minsubject = $(this).attr("data-minsubject");
        var minsubject = $(this).attr("data-minsubject");
        var isCompulsary = $(this).attr("data-is-compulsary");
        var courseName = $(this).attr("data-course-name");
        var courseId = $(this).attr("data-course-id");
        //console.log(minsubject);       
        //console.log(totalChecked);
        if ($(this).is(':checked')) {
            //$(this).parents(".panel-default").find(".course-subject-check").each(function () {
            //    $(this).prop('checked', true);
            //});
            if (totalChecked > maxsubject) {
                message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select maximum ' + maxsubject + ' subjects for course: ' + courseName + ' </div>';
                indCourseError = 1;
            }
            else if (totalChecked < minsubject) {
                message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select minimum ' + minsubject + ' subjects for course: ' + courseName + '</div>';
                indCourseError = 1;
            }
            else {
                course = {}; 
                course["Id"] = courseId;
                course["Name"] = courseName;
                course["ProgramId"] = programId;
                course["SessionId"] = sessionId;
                course["MaxSubject"] = maxsubject;
                course["PublicMinSubject"] = minsubject;
                course["PublicMinPayment"] = 0;
                course["IsPublicCompulsary"] = "True";
                course["Id"] = courseId;
                var subjectList = []; 
                $(this).parents(".panel-default").find(".course-subject-check").each(function () {                   
                    if ($(this).is(':checked')) {
                        subject = {};
                        subject["Id"] = $(this).attr("data-subject-id");
                        subject["Name"] = $(this).attr("data-subject-name");
                        subject["ShortName"] = $(this).attr("data-subject-shortname");
                        subjectList.push(subject);
                    }
                });
                course["SubjectViewModels"] = subjectList;
                courseList.push(course);
                indCourseError = 0;
            }

        }
        else {
            if (isCompulsary == "True") {
                message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> ' + courseName + ' is a Compulsary Course</div>';
                indCourseError = 1;
            }
            else {
                indCourseError = 0;
            }
        }

        if (indCourseError == 1)
            courseError = 1;

    });

    if (courseError == 1) {
        $("#courseError").show().html(message);
        isSuccess = isSuccess + 1;
    }
    else {
        var totalChecked = $(".course-name-check:checked").length;
        //console.log(totalChecked + " J " + isSuccess);
        if (totalChecked==0) {
            $("#courseError").show().html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You must select one or more Course </div>');
            //isSuccess = 0;
            isSuccess = isSuccess + 1;
        }
        else {
            $("#courseError").empty().hide();
            //isSuccess = 1;
            //console.log(totalChecked + " k " + isSuccess);            
        }
       
    }


    var paymentMethod = $("#PaymentMethod").val();
    if (paymentMethod == "") {
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(paymentMethod))) {
        isSuccess = isSuccess + 1;
    }
    else {
        
    }

    var transactionList = [];
    var totalTransactionIds = 0;
    $(".transactionClass").each(function() {
        if ($(this).val()!="") {
            transaction = {}; 
            transaction["Referance"] = null;
            transaction["TransactionId"] = $(this).val();
            transaction["SenderMobile"] = null;
            transaction["TransactionTime"] = null;
            transaction["Amount"] = null;
            transactionList.push(transaction);
            totalTransactionIds = totalTransactionIds + 1;
        }
    });
    if (totalTransactionIds == 0) {
        isSuccess = isSuccess + 1;
        //$(".customMessage").html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> Transaction Id is empty</div>');
    } 
    else {
        //isSuccess = 1;        
    }
   // class="course-name-check" type="checkbox" data-is-compulsary="True" data-minsubject="0" data-maxsubject="4"
    console.log(isSuccess);
    if (isSuccess == 0) {

        studentPayment = {};
        studentPayment["OfferedDiscount"] = 0;
        studentPayment["NetReceivable"] = 0;
        studentPayment["CashBackAmount"] = 0;
        studentPayment["ReceivableAmount"] = 0;
        studentPayment["ConsiderationAmount"] = 0;
        studentPayment["PayableAmount"] = 0;
        studentPayment["DiscountAmount"] = 0;
        studentPayment["CourseFee"] = 0;
        studentPayment["ReceivedAmount"] = 0;
        studentPayment["PaymentMethod"] = paymentMethod;
        studentPayment["SpDiscountAmount"] = 0;
        studentPayment["ReferrerId"] = 0;
        studentPayment["Remarks"] = "";
        studentPayment["NextReceivedDate"] = "";
        studentPayment["NextRecDate"] = new Date();
        studentPayment["CourseViewModels"] = courseList;
        studentPayment["PaymentTransactionViewModels"] = transactionList;
        studentPayment["MinPaymentAmount"] = 0;



        //studentInfo = {};
        studentInfo["Id"] = 0;
        studentInfo["PrnNo"] = "";
        studentInfo["ErrorMessage"] = "";
        studentInfo["Name"] = name;
        studentInfo["MobNumber"] = mobNumber;
        studentInfo["VersionOfStudy"] = versionOfStudy;
        studentInfo["Gender"] = gender;
        studentInfo["Religion"] = religion;        
        studentInfo["Program"] = programId;
        studentInfo["Session"] = sessionId;
        studentInfo["Branch"] = branchId;
        studentInfo["Campus"] = campusId;
        studentInfo["BatchDays"] = batchDays;
        studentInfo["BatchTime"] = batchTime;
        studentInfo["Batch"] = batch;
        studentInfo["CourseViewModels"] = courseList;
        studentInfo["PaymentTransactionViewModel"] = transactionList;
        studentInfo["StudentPayment"] = studentPayment;
        studentInfo["ExamBoardViewModelList"] = boardExamList;

        console.log(studentInfo);
        var studentInfostringify = JSON.stringify(studentInfo);
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForSaveInformation",
            cache: false,
            async: true,
            //dataType: "json",
            contentType: "application/json",
            data: studentInfostringify,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.hasOwnProperty("IsSuccess")) {
                    $(".customMessage").html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> ' + result.returnList + '</div>');
                }
                else {
                    $("#paymentInfo").html(result);
                    
                    var today = new Date();
                    var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();
                    var startDate = initialDate;
                    var year = today.getFullYear();
                    var month = today.getMonth() + 3;
                    if (month> 12) {
                        year = year + 1;
                        month = month - 12;
                    }
                    var endDate = year.toString() + "-" + month.toString() + "-" + today.getDate().toString();

                    console.log(initialDate);
                    console.log(startDate);
                    console.log(endDate);

                    $('.form-date').datetimepicker({
                        format: "yyyy-mm-dd",
                        autoclose: true,
                        todayBtn: false,
                        showMeridian: true,
                        initialDate: initialDate,
                        startDate: startDate,
                        endDate: endDate,
                        startView: 2,
                        minView: 2,
                        maxView: 4
                    });

                    $("#displayPaymentInfo").hide();
                    $("#newAdmissionForm input[type=text]").attr('readonly', 'readonly');
                    //$("#newAdmissionForm select").attr('readonly', 'readonly');
                    //$('#CouseCheckList input[type=checkbox]').attr('readonly', 'readonly');

                    $("#newAdmissionForm select").attr('disabled', true);
                    $('#CouseCheckList input[type=checkbox]').attr('disabled', true);

                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                console.log(result);
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
    //customMessage
});
$(document).on("click", "#editStudentInfo", function(event) {
    $("#paymentInfo").html("");
    $("#displayPaymentInfo").show();   
    $("#newAdmissionForm input[type=text]").removeAttr('readonly');
    $("#CouseCheckList input[type=checkbox]").removeAttr('disabled');
    $("#newAdmissionForm select").removeAttr('disabled');

   // $("#newAdmissionForm select").attr('disabled', true);
   // $('#CouseCheckList input[type=checkbox]').attr('disabled', 'true');
});
$(document).on('click', '#displayMoneyReceiptInfo', function (event) {
    studentInfo = {};
    var isSuccess = 0;
    var name = $("#Name").val();
    var mobNumber = $("#MobNumber").val();
    var versionOfStudy = $("#VersionOfStudy").val();
    var gender = $("#Gender").val();
    var religion = $("#Religion").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var branchId = $("#Branch").val();
    var campusId = $("#Campus").val();
    var batchDays = $("#BatchDays").val();
    var batchTime = $("#BatchTime").val();
    var batch = $("#Batch").val();
    var nextPayemtDay = $("#NextReceivedDate").val();
    var message = '';
    if (name == "") {
        message+='<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> Name Error</div>';
        isSuccess = isSuccess + 1;
    }


    mobNumber = mobNumber.replace(/\W+/g, "");
    var chkVal = mobNumber.match(/^(?:\+?88)?0?1[15-9]\d{8}$/);
    if (mobNumber == "") {       
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Mobile field is required</div>';
        isSuccess = isSuccess + 1;
    }
    else if (chkVal == null) {        
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Mobile number is not valid.</div>';
        isSuccess = isSuccess + 1;
    }
   
    if (versionOfStudy == "") {       
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Version Of Study field is required.</div>';
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(versionOfStudy))) {        
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Version Of Study field is required.</div>';
        isSuccess = isSuccess + 1;
    }   

    if (gender == "") {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Gender field is required.</div>';
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(gender))) {       
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Gender field is required.</div>';
        isSuccess = isSuccess + 1;
    }   

    if (religion == "") {     
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Religion field is required.</div>';
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(religion))) {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Religion field is required.</div>';
        isSuccess = isSuccess + 1;
    }   

    if (programId == "") {       
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Program field is required.</div>';
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(programId))) {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Program field is required.</div>';
        isSuccess = isSuccess + 1;
    }   

    if (sessionId == "") {      
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Session field is required.</div>';
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(sessionId))) {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Session field is required.</div>';
        isSuccess = isSuccess + 1;
    }   

    if (branchId == "") {        
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Branch field is required.</div>';
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(branchId))) {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Branch field is required.</div>';
        isSuccess = isSuccess + 1;
    }
    
    if (campusId == "") {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Campus field is required.</div>';
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(campusId))) {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Campus field is required.</div>';
        isSuccess = isSuccess + 1;
    }  

    if (batchDays == "") {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The BatchDays field is required.</div>';
        isSuccess = isSuccess + 1;
    }
   
    if (batchTime == "") {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Batch Time field is required.</div>';
        isSuccess = isSuccess + 1;
    }    

    if (batch == "") {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Batch field is required.</div>';
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(batch))) {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> The Batch field is required.</div>';
        isSuccess = isSuccess + 1;
    }
    

    var boardExamError = 0;
    var boardExamList = [];
    var message = '';
    boardExam = {};
    //console.log("ExamIds Start");
    $(".examIdInput").each(function () {
        //boardExamList.push($(this).val());
        var examId = $(this).val();
        var examYear = $("#examYear_" + examId).val();
        var examBoard = $("#examBoard_" + examId).val();
        var boardRoll = $("#boardRoll_" + examId).val();
        var boardRegistration = $("#boardRegistration_" + examId).val();
        if ($.isNumeric(examYear) && examYear.length == 4 && $.isNumeric(boardRoll) && $.isNumeric(boardRegistration)) {
            boardExam = {};

            boardExam["Id"] = 0;
            boardExam["Name"] = "";
            boardExam["Year"] = examYear;
            boardExam["Board"] = examBoard;
            boardExam["Roll"] = boardRoll;
            boardExam["RegistrationNumber"] = boardRegistration;
            boardExam["ProgramId"] = 0;
            boardExam["StudentExamId"] = examId;
            boardExam["StudentExamName"] = "";
            boardExam["SessionId"] = 0;
            //console.log(boardExam);
            boardExamList.push(boardExam);
            boardExamError = 2;
        }
        else if (boardExamError != 2) {
            boardExamError = 1;
        }
    });
    //console.log("ExamIds End");
    if (boardExamError == 1) {
        message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You must enter at least one Valid board info</div>';
        $("#examBoardError").show().html(message);
        isSuccess = isSuccess + 1;
    }
    else {
        $("#examBoardError").empty().hide();
    }

    var courseError = 0;
    var courseList = [];
    var message1 = '';
    $(".course-name-check").each(function () {
        var indCourseError = 0;
        var totalChecked = $(this).parents(".panel-default").find(".course-subject-check:checked").length;
        var maxsubject = $(this).attr("data-maxsubject");
        //var minsubject = $(this).attr("data-minsubject");
        var minsubject = $(this).attr("data-minsubject");
        var isCompulsary = $(this).attr("data-is-compulsary");
        var courseName = $(this).attr("data-course-name");
        var courseId = $(this).attr("data-course-id");
        console.log(minsubject);
        console.log(totalChecked);
        if ($(this).is(':checked')) {
            //$(this).parents(".panel-default").find(".course-subject-check").each(function () {
            //    $(this).prop('checked', true);
            //});
            if (totalChecked > maxsubject) {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select maximum ' + maxsubject + ' subjects for course: ' + courseName + ' </div>';
                indCourseError = 1;
            }
            else if (totalChecked < minsubject) {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select minimum ' + minsubject + ' subjects for course: ' + courseName + '</div>';
                indCourseError = 1;
            }
            else {
                course = {};
                course["Id"] = courseId;
                course["Name"] = courseName;
                course["ProgramId"] = programId;
                course["SessionId"] = sessionId;
                course["MaxSubject"] = maxsubject;
                course["PublicMinSubject"] = minsubject;
                course["PublicMinPayment"] = 0;
                course["IsPublicCompulsary"] = "True";
                course["Id"] = courseId;
                var subjectList = [];
                $(this).parents(".panel-default").find(".course-subject-check").each(function () {
                    if ($(this).is(':checked')) {
                        subject = {};
                        subject["Id"] = $(this).attr("data-subject-id");
                        subject["Name"] = $(this).attr("data-subject-name");
                        subject["ShortName"] = $(this).attr("data-subject-shortname");
                        subjectList.push(subject);
                    }
                });
                course["SubjectViewModels"] = subjectList;
                courseList.push(course);
                indCourseError = 0;
            }

        }
        else {
            if (isCompulsary == "True") {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> ' + courseName + ' is a Compulsary Course</div>';
                indCourseError = 1;
            }
            else {
                indCourseError = 0;
            }
        }

        if (indCourseError == 1)
            courseError = 1;

    });

    if (courseError == 1) {
        $("#courseError").show().html(message);
        isSuccess = isSuccess + 1;
    }
    else {
        var totalChecked = $(".course-name-check:checked").length;
        if (totalChecked == 0) {
            $("#courseError").show().html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You must select one or more Course </div>');
            isSuccess = isSuccess + 1;
        }
        else {
            $("#courseError").empty().hide();         
        }
    }


    var paymentMethod = $("#PaymentMethod").val();
    if (paymentMethod == "") {
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(paymentMethod))) {
        isSuccess = isSuccess + 1;
    }
    

    var transactionList = [];
    var totalTransactionIds = 0;
    $(".transactionClass").each(function () {
        if ($(this).val() != "") {
            transaction = {};
            transaction["Referance"] = null;
            transaction["TransactionId"] = $(this).val();
            transaction["SenderMobile"] = null;
            transaction["TransactionTime"] = null;
            transaction["Amount"] = null;
            transactionList.push(transaction);
            totalTransactionIds = totalTransactionIds + 1;
        }
    });

    if (totalTransactionIds == 0) {
        isSuccess = isSuccess + 1;
    }

    if (nextPayemtDay == "") {
       // message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong>Next Payment Date is requird field.</div>';
        isSuccess = isSuccess + 1;
    }
    if (nextPayemtDay == "") {
        $("#NextReceivedDate").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Next Payment field is required.");
        $("#NextReceivedDate").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#NextReceivedDate").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#NextReceivedDate").removeClass("highlight");
        //isSuccess = 1;
    }
   
    if (isSuccess == 0) {
        studentPayment = {};
        studentPayment["OfferedDiscount"] = 0;
        studentPayment["NetReceivable"] = 0;
        studentPayment["CashBackAmount"] = 0;
        studentPayment["ReceivableAmount"] = 0;
        studentPayment["ConsiderationAmount"] = 0;
        studentPayment["PayableAmount"] = 0;
        studentPayment["DiscountAmount"] = 0;
        studentPayment["CourseFee"] = 0;
        studentPayment["ReceivedAmount"] = 0;
        studentPayment["PaymentMethod"] = paymentMethod;
        studentPayment["SpDiscountAmount"] = 0;
        studentPayment["ReferrerId"] = 0;
        studentPayment["Remarks"] = "";
        studentPayment["NextReceivedDate"] = "";
        studentPayment["NextRecDate"] = nextPayemtDay;
        studentPayment["CourseViewModels"] = courseList;
        studentPayment["PaymentTransactionViewModels"] = transactionList;
        studentPayment["MinPaymentAmount"] = 0;

        studentInfo = {};
        studentInfo["Id"] = 0;
        studentInfo["PrnNo"] = "";
        studentInfo["ErrorMessage"] = "";
        studentInfo["Name"] = name;
        studentInfo["MobNumber"] = mobNumber;
        studentInfo["VersionOfStudy"] = versionOfStudy;
        studentInfo["Gender"] = gender;
        studentInfo["Religion"] = religion;
        studentInfo["Program"] = programId;
        studentInfo["Session"] = sessionId;
        studentInfo["Branch"] = branchId;
        studentInfo["Campus"] = campusId;
        studentInfo["BatchDays"] = batchDays;
        studentInfo["BatchTime"] = batchTime;
        studentInfo["Batch"] = batch;
        studentInfo["CourseViewModels"] = courseList;
        studentInfo["PaymentTransactionViewModel"] = transactionList;
        studentInfo["StudentPayment"] = studentPayment;
        studentInfo["ExamBoardViewModelList"] = boardExamList;

        console.log(studentInfo);
        var studentInfostringify = JSON.stringify(studentInfo);
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForSaveInformationFinal",
            cache: false,
            async: true,
            //dataType: "json",
            contentType: "application/json",
            data: studentInfostringify,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $("#RegistrationSubmitForm").empty();
                    $("#headerTitle").empty();
                    
                    $(".customMessage").html('<div class="alert alert-success"><a class="close" data-dismiss="alert">x</a><strong>Success!</strong> ' + result.returnList + '</div>');
                }
                else {
                    $(".customMessage").html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> ' + result.returnList + '</div>');
                    //$("#paymentInfo").html(result);

                    //var today = new Date();
                    //var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();
                    //var startDate = initialDate;
                    //var endDate = today.getFullYear().toString() + "-" + (today.getMonth() + 3).toString() + "-" + today.getDate().toString();

                    //console.log(initialDate);
                    //console.log(startDate);
                    //console.log(endDate);

                    //$('.form-date').datetimepicker({
                    //    format: "yyyy-mm-dd",
                    //    autoclose: true,
                    //    todayBtn: false,
                    //    showMeridian: true,
                    //    initialDate: initialDate,
                    //    startDate: startDate,
                    //    endDate: endDate,
                    //    startView: 2,
                    //    minView: 2,
                    //    maxView: 4
                    //});

                    //$("#displayPaymentInfo").hide();
                    //$("#newAdmissionForm input[type=text]").attr('readonly', 'readonly');
                    //$("#newAdmissionForm select").attr('readonly', 'readonly');
                    //$('#CouseCheckList input[type=checkbox]').attr('readonly', 'readonly');
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                console.log(result);
                $.unblockUI();
                console.log("Failed -----");
            }
        });

    }
});

$(document).on('click', '.excolsHead', function (event) {
    $(".excolsHead").each(function() {
        if ($(this).hasClass("collapsed")) {
            $(this).find(".glyphicon-minus-sign").removeClass("glyphicon-minus-sign").addClass("glyphicon-plus-sign");
        } else {
            $(this).find(".glyphicon-plus-sign").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
        }
            
    });
});

