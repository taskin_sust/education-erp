﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ZoneSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ZoneSetting")]
    public class ZoneSettingLoadTest : ZoneSettingBaseTest
    {
        #region Basic Test



        #endregion

        #region LoadById Test

        [Fact]
        public void GetZoneSetting_Null_Check()
        {
            _zoneSettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1).WithToleranceTimeToleranceDayDeductionMultiplier(1, 1, 0.5m).Persist();
            var obj = _zoneSettingService.LoadById(_zoneSettingFactory.Object.Id+1);
            Assert.Null(obj);
        }

        [Fact]
        public void GetZoneSetting_Should_Return_Invalid()
        {
            Assert.Throws<InvalidDataException>(() => _zoneSettingService.LoadById(0));
        }

        [Fact]
        public void GetZoneSetting_Should_Return_Object()
        {
            var dFactory = _zoneSettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1).WithToleranceTimeToleranceDayDeductionMultiplier(1, 1, 0.5m).Persist();
            var obj = _zoneSettingService.LoadById(dFactory.Object.Id);
            Assert.NotNull(obj);
        }

        #endregion

        #region Load Active Test

        [Fact]
        public void LoadZoneSetting_Should_Retrun_List()
        {
            _zoneSettingFactory.CreateMore(10).Persist();
            IList<ZoneSetting> zoneList = _zoneSettingService.LoadZoneSetting("", "Asc", _zoneSettingFactory.GetLastCreatedObjectList()[0].Organization.Id);
            Assert.Equal(10, zoneList.Count);
        }

        [Fact]
        public void LoadZoneSetting_Not_Retrun_List()
        {
            int totalrowCount = _zoneSettingService.GetZoneSettingRowCount(new List<UserMenu>(), "", "Asc", -1);
            IList<ZoneSetting> zoneList = _zoneSettingService.LoadZoneSetting(new List<UserMenu>(), "", "Asc", -1);
            Assert.Equal(0, totalrowCount);
            Assert.Equal(0, zoneList.Count);
        }

        #endregion


    }
}
