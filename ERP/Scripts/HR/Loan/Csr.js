﻿//zakat
var approvedRefundsuccess = true;
var zakatEdCdSuccess;
var csrEdCdSuccess;
//parse string to date
function ParseDateString(dateString) {
    console.log(dateString);
    var dateArray = dateString.split(" ");
    var month = dateArray[0];
    var year = dateArray[1];
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var monthNumber = $.inArray(month, months);
    var date = new Date(year, monthNumber, "01");
    console.log(date);
    date.setHours(0, 0, 0, 0);
    return date;
}

//find date difference by month
function FindMonthDifferenceOfDates(dateFrom, dateTo) {
    console.log("hello =>>> " + (dateTo.getMonth() - dateFrom.getMonth() + 1)
        + (12 * (dateTo.getFullYear() - dateFrom.getFullYear())));

    return (dateTo.getMonth() - dateFrom.getMonth() + 1)
        + (12 * (dateTo.getFullYear() - dateFrom.getFullYear()));

}

function ParseFormValidation() {
    $("#createLoanApplicationForm").data("validator", null);
    $.validator.unobtrusive.parse($("#createLoanApplicationForm"));
}
//disable closing date field
function DisableClosingDateField(dateField) {
    dateField.prop('disabled', true);
}

//enable closing date field
function EnableClosingDateField(dateField) {
    dateField.prop('disabled', false);
}
//CalculateCsrAmount
function CalculateCsrTotalAmount() {
    var csrTotalAmount = 0;
    var csrAmount = 0;
    var csrAmountField = $('#csrPanelBody').find("#CsrAmount");
    if (csrAmountField.val() != null && csrAmountField.val() !== "" && !isNaN(csrAmountField.val())) {
        csrAmount = parseFloat(csrAmountField.val());
    }
    var csrEffectiveDateField = $('#csrPanelBody').find("#CsrEffectiveDate");
    var csrClosingDateField = $('#csrPanelBody').find("#CsrClosingDate");
    var csrEffectiveDate = ParseDateString(csrEffectiveDateField.val());
    var csrClosingDate = ParseDateString(csrClosingDateField.val());
    if (csrEffectiveDate <= csrClosingDate) {
        var monthDiff = FindMonthDifferenceOfDates(csrEffectiveDate, csrClosingDate);
        var csrpaymentType = $('#csrPanelBody').find("#CsrPaymentType").val();

        if (csrpaymentType == 1) { csrTotalAmount = parseFloat(csrAmount); }
        else if (csrpaymentType == 3) { csrTotalAmount = parseFloat(csrAmount * monthDiff); }
        else if (csrpaymentType == 4) {
            if (monthDiff > 12 && Math.ceil(monthDiff / 12) > 1) {
                csrTotalAmount = parseFloat(csrAmount * Math.ceil(monthDiff / 12));
            } else {
                csrTotalAmount = parseFloat(csrAmount);
            }
        }
        console.log("cta:" + csrTotalAmount);
    }
    $('#csrPanelBody').find("#CsrTotalAmount").val(csrTotalAmount);
}

$(document).ready(function () {
    $('#csrCheckbox').change(function () {
        var chkbox = $(this);
        if (chkbox.is(':checked')) {
            var csrDivHtml = $("#csrDiv").html();
            $("#csrPanelBody").html(csrDivHtml);
            //bind datepicker
            $('#csrPanelBody').on('focus', '#CsrEffectiveDate', function () {
                var $this = $(this);
                $this.datetimepicker({
                    startDate: new Date(),
                    format: "MM yyyy",
                    startView: "year",
                    minView: "year",
                    autoclose: true
                }).on('changeDate', function (ev) {
                    CalculateCsrTotalAmount();
                });
            });

            $('#csrPanelBody').on('focus', '#CsrClosingDate', function () {
                var $this = $(this);
                $this.datetimepicker({
                    startDate: new Date(),
                    format: "MM yyyy",
                    startView: "year",
                    minView: "year",
                    autoclose: true
                }).on('changeDate', function (ev) {
                    CalculateCsrTotalAmount();
                });
            });
            $("#csrPanelBody").show();
        } else {
            $("#csrPanelBody").html("");
            $("#csrPanelBody").hide();
        }
        ParseFormValidation();
    });
    //Csr amount on change
    $('#csrPanelBody').on('input propertychange', '#CsrAmount', function () {
        CalculateCsrTotalAmount();
    });
    //Csr payment type on change
    $('#csrPanelBody').on('change', '#CsrPaymentType', function () {
        CalculateCsrTotalAmount();
    });

});