﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class LoginActivityLogController : Controller
    {
        #region Objects/Propertise/Services/Dao & Initialization

        private ILog logger = LogManager.GetLogger("AdministrationArea");
        private readonly ICommonHelper _commonHelper;
        private readonly ILoginActivityLogService _loginActivityLogService;
        public LoginActivityLogController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _loginActivityLogService = new LoginActivityLogService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
            }

        }

        #endregion

        #region Index/Manage

        public ActionResult Index()
        {
            var operationStatusList = _commonHelper.LoadEmumToDictionary<LoginOperationStatus>().ToList();
            operationStatusList.Add(new KeyValuePair<int, string>((int)SelectionType.SelelectAll, "Operation Status"));
            ViewBag.LoginOperationStatusList = new SelectList(operationStatusList.OrderBy(x => x.Key), "Key", "Value");
            return View();
        }

        #endregion

        #region Render Data Table

        public JsonResult LoginActivityLogList(int draw, int start, int length, string userName, int operationStatus, string ipAddress)
        {
            NameValueCollection nvc = Request.Form;

            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "CreationDate";
                            break;
                        case "1":
                            orderBy = "IpAddress";
                            break;
                        case "2":
                            orderBy = "Device";
                            break;
                        case "3":
                            orderBy = "OsName";
                            break;
                        case "4":
                            orderBy = "Browser";
                            break;
                        case "5":
                            orderBy = "UserName";
                            break;
                        case "6":
                            orderBy = "OperationStatus";
                            break;
                        default:
                            orderBy = "CreationDate";
                            break;
                    }
                }
                int recordsTotal = _loginActivityLogService.LoginActivityLogRowCount(userName, operationStatus, ipAddress);
                long recordsFiltered = recordsTotal;

                var list = _loginActivityLogService.LoadLoginActivityLog(start, length, orderBy, orderDir.ToUpper(), userName, operationStatus, ipAddress).ToList();

                
                foreach (var log in list)
                {
                    var str = new List<string>();
                    str.Add(log.CreationDate.ToString());
                    str.Add(log.IpAddress);
                    str.Add(log.Device);
                    str.Add(log.OsName);
                    str.Add(log.Browser);
                    str.Add(log.UserName);
                    str.Add(_commonHelper.GetEmumIdToValue<LoginOperationStatus>(log.OperationStatus));
                    str.Add(log.Remarks);
                    getData.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData

                });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });


        }

        #endregion
        
        #region Helper Function
        #endregion

    }
}
