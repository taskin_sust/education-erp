﻿using System;
using System.Collections.Generic;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.HolidaySettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "HolidaySetting")]
    public class HolidaySettingLoadTest : HolidaySettingBaseTest
    {

        #region Basic test
        
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _holidaySettingService.GetHolidaySetting(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_Should_Return_Invalid()
        {
            Assert.Throws<InvalidDataException>(() => _holidaySettingService.GetHolidaySetting(0));
        }

        [Fact]
        public void LoadById_Should_Return_Object()
        {
            var dFactory = _holidaySettingFactory.Create().WithOrganization()
                                .WithNameAndRank(_name, 1).WithHolidayRepeatationType((int)HolidayRepeatationType.Once)
                                .WithHolidayType((int)HolidayTypeEnum.Gazetted).Persist();
            var obj = _holidaySettingService.GetHolidaySetting(dFactory.Object.Id);
            Assert.NotNull(obj);
        }

        #endregion

        #region Load Active Test
        
        [Fact]
        public void LoadActive_should_return_list()
        {
            _holidaySettingFactory.CreateMore(100).Persist();
            IList<HolidaySetting> lists = _holidaySettingService.LoadHoliday(0, 100, "", "", new List<long>(), null, null);
            Assert.Equal(100, lists.Count);
        }

        [Fact]
        public void LoadActive_should_Not_return_list()
        {
            _holidaySettingFactory.CreateMore(100).Persist();
            IList<HolidaySetting> lists = _holidaySettingService.LoadHoliday(0, 10, "", "", new List<long>() { -1 }, null, null);
            Assert.Equal(0, lists.Count);
        }

        #endregion

        #region Count Test

        [Fact]
        public void Should_Return_Successfully()
        {
            _holidaySettingFactory.CreateMore(10).Persist();
            var orgList = commonHelper.ConvertIdToList(_holidaySettingFactory.ObjectList.Select(x => x.Organization).FirstOrDefault());
            var userMenu = BuildUserMenu(orgList);
            Assert.Equal(10, _holidaySettingService.GetHolidayCount(userMenu, orgList.Select(x => x.Id).ToList(), null, null));
        }
        [Fact]
        public void Should_Return_Invalid_usermenu()
        {
            _holidaySettingFactory.CreateMore(10).Persist();
            var orgList = commonHelper.ConvertIdToList(_holidaySettingFactory.ObjectList.Select(x => x.Organization).FirstOrDefault());
            Assert.Throws<InvalidDataException>(
                () => _holidaySettingService.GetHolidayCount(null, orgList.Select(x => x.Id).ToList(), null, null));
        }

        [Fact]
        public void Should_Return_Invalid_Organization()
        {
            _holidaySettingFactory.CreateMore(10).Persist();
            var orgList = commonHelper.ConvertIdToList(_holidaySettingFactory.ObjectList.Select(x => x.Organization).FirstOrDefault());
            var userMenu = BuildUserMenu(orgList);
            Assert.Throws<InvalidDataException>(
                () => _holidaySettingService.GetHolidayCount(userMenu, new List<long>() { _holidaySettingFactory.ObjectList.Select(x => x.Organization.Id).FirstOrDefault() + 1 }, null, null));
        }

        #endregion
    }
}
