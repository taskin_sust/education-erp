﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Cache;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.JobExperienceTest
{
    public class JobExperienceBaseTest : TestBase, IDisposable
    {
        #region Properties & Object Initialization

        protected readonly CommonHelper CommonHelper;
        private ISession _session;
        protected List<long> IdList = new List<long>();
        internal JobExperienceFactory jobExperienceFactory;
        internal readonly DepartmentFactory departmentFactory;
        protected readonly IJobExperienceService jobExperienceService;

        #endregion

        public JobExperienceBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            departmentFactory = new DepartmentFactory(null, _session);

            jobExperienceService = new JobExperienceService(_session);
            jobExperienceFactory = new JobExperienceFactory(null, _session);
        }

        #region Custom Error Text

        public const string MemberEntityNotFound = "MemberEntityNotFound";
        public const string NullObjectError = "NullObject";
        public const string CompanyNameNullError = "Company Name Not Found!";
        public const string CompanyTypeNullError = "Company Type Not Found!";
        public const string CompanyLocationNullError = "Company Location Not Found!";
        public const string DepartmentNameNullError = "Department Name Not Found!";
        public const string ExperienceAreaNullError = "Experience Area Not Found!";
        public const string JobPositionNullError = "Job Position Not Found!";
        public const string JobDurationNullError = "Job Duration Not Found!";

        #endregion

        public void Dispose()
        {
            jobExperienceFactory.CleanUp();
            base.Dispose();
        }

    }
}
