﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;

namespace UdvashERP.Areas.Exam.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Exam")]
    public class ExamsController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly IBatchService _batchService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly ICourseService _courseService;
        private readonly IExamsService _examsService;
        private readonly IOrganizationService _organizationService;
        private readonly IExamsDetailsService _examsDetailsService;
        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _userMenu;
        public ExamsController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _sessionService = new SessionService(session);
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _batchService = new BatchService(session);
                _courseService = new CourseService(session);
                _examsService = new ExamsService(session);
                _examsDetailsService = new ExamsDetailsService(session);
                _organizationService = new OrganizationService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Manage

        [HttpGet]
        public ActionResult ManageExam(string editSuccess = "")
        {
            try
            {
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                if (!string.IsNullOrEmpty(editSuccess))
                {
                    ViewBag.SuccessMessage = editSuccess;
                }
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.Program = new SelectList(_programService.LoadAuthorizedProgram(userMenu), "Id", "Name");
                ViewBag.Session = new SelectList(_sessionService.LoadAuthorizedSession(userMenu), "Id", "Name");
                ViewBag.Course = new SelectList(_courseService.LoadCourse());
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            return View();
        }


        [HttpPost]
        public ActionResult ManageExamResult(int draw, int start, int length, string organization, string program, string session, string course, string name)
        {
            long id = 0;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    int sr = start + 1;
                    string orderBy = "";
                    string orderDir = "";

                    #region OrderBy and Direction
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "1":
                                orderBy = "o.Name";
                                break;
                            case "2":
                                orderBy = "p.Name";
                                break;
                            case "3":
                                orderBy = "s.Name";
                                break;
                            case "4":
                                orderBy = "c.Name";
                                break;
                            case "5":
                                orderBy = "NumberOfExam";
                                break;
                            case "6":
                                orderBy = "ExamNamePrefix";
                                break;

                            default:
                                orderBy = "";
                                break;
                        }
                    }


                    #endregion

                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;


                    var authorizeOrg = _organizationService.LoadAuthorizedOrganization(userMenu);
                    var authorizeProgram = _programService.LoadAuthorizedProgram(userMenu);
                    var authorizeSession = _sessionService.LoadAuthorizedSession(userMenu);

                    if (authorizeOrg.Any() && authorizeProgram.Any() && authorizeSession.Any())
                    {
                        var authorizeOrgIds = authorizeOrg.Select(x => x.Id).ToArray();
                        var authorizeProgramIds = authorizeProgram.Select(x => x.Id).ToArray();
                        var authorizeSessionIds = authorizeSession.Select(x => x.Id).ToArray();

                        int recordsTotal = _examsService.GetExamParentRowCount(authorizeOrgIds, authorizeProgramIds, authorizeSessionIds, organization, program, session,
                             course, name);
                        List<ExamParent> examParentList =
                            _examsService.LoadExamParentList(authorizeOrgIds, authorizeProgramIds, authorizeSessionIds, start, length, orderBy, orderDir.ToUpper(),
                                organization, program, session, course, name).ToList();

                        int recordsFiltered = recordsTotal;
                        var data = new List<object>();
                        //int sr = 1;

                        foreach (var c in examParentList)
                        {
                            id = c.Id;
                            var str = new List<string>();
                            str.Add(sr++.ToString());

                            str.Add(c.Course.Program.Organization.ShortName);
                            str.Add(c.Course.Program.ShortName);
                            str.Add(c.Course.RefSession.Name);
                            str.Add(c.Course.Name);
                            str.Add(c.NumberOfExam.ToString());
                            str.Add(c.ExamNamePrefix);
                            //str.Add(c.ShortName);
                            //str.Add(c.Code);
                            //str.Add(c.McqFullMarks != 0 ? c.McqFullMarks.ToString() : "");
                            //str.Add(c.WrittenFullMark != 0 ? c.WrittenFullMark.ToString() : "");


                            //str.Add("<a href='" + Url.Action("Details", "Exams") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;<a href='" + Url.Action("Edit", "Exams") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;<a id='" + c.Id.ToString() + "' href='#' data-name='" + c.Name.ToString() + "' class='glyphicon glyphicon-trash'> </a>");
                            var editLink = "";
                            if (id == 133)
                            {
                                int y = 2;
                            }
                            if (!_examsService.IsChildrenExamModified(c))
                            {
                                editLink = "<a href='" +
                                           Url.Action("Edit", "Exams") + "?id=" + c.Id +
                                           "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;";
                            }
                            str.Add("<a href='" + Url.Action("Details", "Exams") + "?id=" + c.Id +
                                    "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;" + editLink + " <a id='" + c.Id +
                                    "' href='#' data-name='" + c.ExamNamePrefix +
                                    "' class='glyphicon glyphicon-trash'> </a>");
                            data.Add(str);
                        }

                        return Json(new
                        {
                            draw = draw,
                            recordsTotal = recordsTotal,
                            recordsFiltered = recordsFiltered,
                            start = start,
                            length = length,
                            data = data
                        });
                    }
                    else
                    {
                        var data = new List<object>();
                        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;

                        return Json(new
                        {
                            draw = draw,
                            recordsTotal = 0,
                            recordsFiltered = 0,
                            start = start,
                            length = length,
                            data = data
                        });
                    }


                }
                catch (Exception ex)
                {
                    var x = id;
                    var data = new List<object>();
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpGet]
        public ActionResult ExamList(string editSuccess = "")
        {

            try
            {
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                if (!string.IsNullOrEmpty(editSuccess))
                {
                    ViewBag.SuccessMessage = editSuccess;
                }
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.Program = new SelectList(_programService.LoadAuthorizedProgram(userMenu), "Id", "Name");
                ViewBag.Session = new SelectList(_sessionService.LoadAuthorizedSession(userMenu), "Id", "Name");
                ViewBag.Course = new SelectList(_courseService.LoadCourse());
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            return View();
        }

        #region Render Data Table
        [HttpPost]
        public ActionResult ExamListResult(int draw, int start, int length, string organization, string program, string session, string course, string name, string shortName, string code, string status, string rank)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    int sr = start + 1;
                    string orderBy = "";
                    string orderDir = "";
                    #region OrderBy and Direction
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "1":
                            case "2":
                                orderBy = "p.Name";
                                break;
                            case "3":
                                orderBy = "s.Name";
                                break;
                            case "4":
                                orderBy = "c.Name";
                                break;
                            case "5":
                                orderBy = "Name";
                                break;
                            case "6":
                                orderBy = "Code";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }


                    #endregion
                    Exams exams = new Exams();
                    List<Exams> examList = _examsService.GetExamList(start, length, orderBy, orderDir.ToUpper(), organization, program, session, course, name, shortName, code, status, rank).ToList();
                    int recordsTotal = examList.Count;
                    long recordsFiltered = recordsTotal;
                    examList = examList.Skip(start).Take(length).ToList();

                    var data = new List<object>();
                    foreach (var c in examList)
                    {
                        var isSingleOrCombinedExamType = false;
                        if (c.IsMcq)
                        {
                            var mcqDetails = c.ExamDetails.FirstOrDefault(x => x.ExamType == ExamType.Mcq);
                            if (mcqDetails != null && (mcqDetails.SubjectType == SubjectType.Single || mcqDetails.SubjectType == SubjectType.Combined)) isSingleOrCombinedExamType = true;
                        }

                        var str = new List<string>();
                        str.Add((sr++).ToString());
                        str.Add(c.Program.Organization.ShortName);
                        str.Add(c.Program.Name);
                        str.Add(c.Session.Name);
                        str.Add(c.Course.Name);
                        str.Add(c.Name);
                        str.Add(c.Code);
                        if (isSingleOrCombinedExamType)
                        {
                            str.Add("<a href='" + Url.Action("EditIndividualExam", "Exams") + "?id=" + c.Id +
                                                        "' class='glyphicon glyphicon-pencil'> </a>");
                        }
                        else
                        {
                            str.Add("");
                        }
                        data.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        #endregion

        #region export exam list
        public ActionResult ExportExamList(string organization, string program, string session, string course, string name, string shortName, string code, string status, string rank)
        {
            try
            {
                List<string> headerList = new List<string>();
                headerList.Add("Exam List Report");
                headerList.Add("");
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                columnList.Add("Organization");
                columnList.Add("Program");
                columnList.Add("Session");
                columnList.Add("Course");
                columnList.Add("Exam Name");
                columnList.Add("Exam Code");
                List<Exams> examList = _examsService.GetExamList(0, 0, "ShortName", "ASC", organization, program, session, course, name, shortName, code, status, rank).ToList();
                var data = new List<List<object>>();
                foreach (var c in examList)
                {
                    var str = new List<object>();
                    str.Add(c.Program.Organization.ShortName);
                    str.Add(c.Program.Name);
                    str.Add(c.Session.Name);
                    str.Add(c.Course.Name);
                    str.Add(c.Name);
                    str.Add(c.Code);
                    data.Add(str);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, data, footerList, "exam-list-report__"
                   + DateTime.Now.ToString("yyyyMMdd-HHmmss"), false);

                return View("AutoClose");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }
        }
        #endregion
        #endregion

        #region Save Operation
        public ActionResult CreateExam()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateExam(Object[] exam, long[] mcqSubject = null, int[] mcqTotalQuestions = null, decimal[] mcqMarksPerQuestion = null,
            decimal[] mcqNegativeMark = null, decimal[] mcqPassMark = null, int[] mcqQuestionFrom = null,
            int[] mcqQuestionTo = null, int[] mcqIsCompulsory = null, int mcqMaxSubCount = 0, long[] writtenSubject = null,
            decimal[] writtenFullMarks = null, decimal[] writtenPassMark = null, int[] writtenIsCompulsory = null, int writtenMaxSubCount = 0,
            int mcqsubType = 0, int writtensubType = 0, int isAcceptPartialAnswer = 0)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    bool success = false;
                    string message = "";
                    ViewBag.programList = new SelectList(_programService.LoadAuthorizedProgram(_userMenu).ToList(), "Id", "Name");
                    if (mcqSubject != null)
                    {
                        mcqSubject = mcqSubject.Where(x => x != 0).ToArray();
                    }
                    if (writtenSubject != null)
                    {
                        writtenSubject = writtenSubject.Where(x => x != 0).ToArray();
                    }
                    var examObj = new Exams();
                    var responseMessage = new ResponseMessage();
                    examObj.Branch = new Branch { Id = Convert.ToInt64(exam[2]) };
                    examObj.Batch = new Batch { Id = Convert.ToInt64(exam[3]) };
                    examObj.Program = new Program { Id = Convert.ToInt64(exam[0]) };
                    examObj.Session = new Session { Id = Convert.ToInt64(exam[1]) };
                    examObj.Course = new Course { Id = Convert.ToInt64(exam[4]) };
                    var examNamePrefix = exam[5].ToString();
                    var numberOfExam = Convert.ToInt32(exam[6]);
                    var examShortNamePrefix = exam[7].ToString();
                    var mcqFullMark = exam[8].ToString();
                    var writtenFullMark = exam[9].ToString();
                    bool isTakeAnswerSheetWhenUploadMarks = exam[10].ToString() == "1";

                    examObj.TotalUniqueSet = string.IsNullOrEmpty(exam[11].ToString()) ? 0 : Convert.ToInt32(exam[11]);
                    //Convert.ToInt32(string.IsNullOrEmpty());
                    examObj.DurationHour = exam[12].ToString();
                    examObj.DurationMinute = exam[13].ToString();
                    examObj.IsBanglaVersion = Convert.ToBoolean(exam[14]);
                    examObj.IsEnglishversion = Convert.ToBoolean(exam[15]);

                    success = _examsService.Save(examObj, examNamePrefix, numberOfExam,
                        examShortNamePrefix, mcqFullMark, writtenFullMark, isTakeAnswerSheetWhenUploadMarks,
                        isAcceptPartialAnswer, mcqSubject, mcqMaxSubCount, mcqTotalQuestions, writtenSubject, writtenMaxSubCount, writtenFullMarks, mcqMarksPerQuestion, mcqNegativeMark, mcqPassMark, mcqQuestionFrom, mcqQuestionTo, mcqIsCompulsory, writtenPassMark, writtenIsCompulsory, mcqsubType, writtensubType, out message);

                    if (success)
                    {
                        responseMessage.SuccessMessage = "Exam Successfully Created";
                    }
                    else
                    {
                        responseMessage.ErrorMessage = message;
                    }
                    return Json(responseMessage);

                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }

            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }
        #endregion

        #region Update Operation

        public ActionResult Edit(long id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var examParent = new ExamParent();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.id = id;
                examParent = _examsService.GetExamParentById(id);

                if (examParent == null)
                {
                    return HttpNotFound();
                }

                var examUnderThisParent = new Exams();
                if (examParent.Examses.Any())
                {
                    examUnderThisParent = examParent.Examses.OrderByDescending(x => x.Id).FirstOrDefault();
                }
                ViewBag.ExamUnderThisParent = examUnderThisParent;

                int mcqSubjectType = 0;
                int writtenSubjectType = 0;
                int answerScript = 0;
                int studentMarks = 0;

                answerScript = _examsService.GetAnswerScriptCount(id);
                studentMarks = _examsService.GetStudentMarksCount(id);

                var firstExamDetailsMcq = new ExamsDetails();
                var firstExamDetailsWritten = new ExamsDetails();

                if (examUnderThisParent != null && examUnderThisParent.ExamDetails.Any())
                {
                    if (examUnderThisParent.IsMcq)
                    {
                        firstExamDetailsMcq = examUnderThisParent.ExamDetails.FirstOrDefault(x => x.ExamType == ExamType.Mcq);
                        if (firstExamDetailsMcq != null) mcqSubjectType = firstExamDetailsMcq.SubjectType;
                    }
                    if (examUnderThisParent.IsWritten)
                    {
                        firstExamDetailsWritten = examUnderThisParent.ExamDetails.FirstOrDefault(x => x.ExamType == ExamType.Written);
                        if (firstExamDetailsWritten != null) writtenSubjectType = firstExamDetailsWritten.SubjectType;
                    }
                }
                ViewBag.McqSubjectType = mcqSubjectType;
                ViewBag.WrittenSubjectType = writtenSubjectType;
                var courseSubject = examParent.Course.CourseSubjects.ToList();
                var examFirstOrDefault = examParent.Examses.FirstOrDefault();
                IList<long> examSubjectIds = null;
                if (examFirstOrDefault != null)
                {
                    var examSubjects = examFirstOrDefault.ExamDetails.Where(x => x.Subject != null).Select(x => x.Subject).ToList();
                    if (examSubjects.Count > 0)
                    {
                        examSubjectIds = examSubjects.Select(x => x.Id).ToList();
                    }
                }
                var subjects = new List<Subject>();
                if (courseSubject.Count > 0)
                {
                    subjects.AddRange(from subject in courseSubject where examSubjectIds != null && (subject.Status == Subject.EntityStatus.Active && subject.Subject.Id.In(examSubjectIds.ToArray())) select subject.Subject);
                }
                //ViewBag.SubjectList = new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name");
                ViewBag.SubjectList = subjects;
                ViewBag.FirstExamDetailsMcq = firstExamDetailsMcq;
                ViewBag.FirstExamDetailsWritten = firstExamDetailsWritten;
                ViewBag.AnswerScriptCount = answerScript;
                ViewBag.StudentMarksCount = studentMarks;
                ViewBag.ExamParent = examParent;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            ViewBag.ExamParent = examParent;
            return View();
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult Edit(UpdateExamViewModel data)
        {
            try
            {
                _examsService.Update(data);
                return Json(new Response(true, "Successfully Save Exam Information."));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (MessageException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

            //return Json(new Response(false, WebHelper.CommonErrorMessage));
        }

        public ActionResult EditIndividualExam(long id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var examParent = new ExamParent();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.id = id;
                var exam = _examsService.LoadById(id);
                examParent = exam.ExamParent;
                if (examParent == null)
                {
                    return HttpNotFound();
                }

                var examUnderThisParent = new Exams();
                if (examParent.Examses.Any())
                {
                    examUnderThisParent = examParent.Examses.FirstOrDefault(x => x.Id == id);
                }
                ViewBag.ExamUnderThisParent = examUnderThisParent;

                int mcqSubjectType = 0;
                int writtenSubjectType = 0;
                int answerScript = 0;
                int studentMarks = 0;

                answerScript = _examsService.GetAnswerScriptCount(id);
                studentMarks = _examsService.GetStudentMarksCount(id);

                var firstExamDetailsMcq = new ExamsDetails();
                var firstExamDetailsWritten = new ExamsDetails();

                if (examUnderThisParent != null && examUnderThisParent.ExamDetails.Any())
                {
                    if (examUnderThisParent.IsMcq)
                    {
                        firstExamDetailsMcq = examUnderThisParent.ExamDetails.FirstOrDefault(x => x.ExamType == ExamType.Mcq);
                        if (firstExamDetailsMcq != null) mcqSubjectType = firstExamDetailsMcq.SubjectType;
                    }
                    if (examUnderThisParent.IsWritten)
                    {
                        firstExamDetailsWritten = examUnderThisParent.ExamDetails.FirstOrDefault(x => x.ExamType == ExamType.Written);
                        if (firstExamDetailsWritten != null) writtenSubjectType = firstExamDetailsWritten.SubjectType;
                    }
                }
                ViewBag.McqSubjectType = mcqSubjectType;
                ViewBag.WrittenSubjectType = writtenSubjectType;
                var courseSubject = examParent.Course.CourseSubjects.ToList();
                var examFirstOrDefault = examParent.Examses.FirstOrDefault();
                IList<long> examSubjectIds = null;
                if (examFirstOrDefault != null)
                {
                    var examSubjects = examFirstOrDefault.ExamDetails.Where(x => x.Subject != null).Select(x => x.Subject).ToList();
                    if (examSubjects.Count > 0)
                    {
                        examSubjectIds = examSubjects.Select(x => x.Id).ToList();
                    }
                }
                var subjects = new List<Subject>();
                if (courseSubject.Count > 0)
                {
                    subjects.AddRange(from subject in courseSubject where examSubjectIds != null && (subject.Status == Subject.EntityStatus.Active && subject.Subject.Id.In(examSubjectIds.ToArray())) select subject.Subject);
                }
                //ViewBag.SubjectList = new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name");
                ViewBag.SubjectList = subjects;
                ViewBag.FirstExamDetailsMcq = firstExamDetailsMcq;
                ViewBag.FirstExamDetailsWritten = firstExamDetailsWritten;
                ViewBag.AnswerScriptCount = answerScript;
                ViewBag.StudentMarksCount = studentMarks;
                ViewBag.ExamParent = examParent;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            ViewBag.ExamParent = examParent;
            return View();
        }

        [HttpPost]
        public ActionResult EditIndividualExam(UpdateExamViewModel data)
        {
            try
            {
                _examsService.UpdateIndividualExam(data);
                return Json(new Response(true, "Exam information updated successfully."));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
        }

        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                _examsService.Delete(id);
                return Json(new Response(true, "Exam Delete Successful"));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }

        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var examParent = new ExamParent();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.id = id;
                examParent = _examsService.GetExamParentById(id);

                if (examParent == null)
                {
                    return HttpNotFound();
                }

                var examUnderThisParent = new Exams();
                if (examParent.Examses.Any())
                {
                    examUnderThisParent = examParent.Examses.OrderByDescending(x => x.Id).FirstOrDefault();
                }
                ViewBag.ExamUnderThisParent = examUnderThisParent;

                int mcqSubjectType = 0;
                int writtenSubjectType = 0;
                int answerScript = 10;
                int studentMarks = 10;

                //answerScript = _examsService.GetAnswerScriptCount(id);
                //studentMarks = _examsService.GetStudentMarksCount(id);

                var firstExamDetailsMcq = new ExamsDetails();
                var firstExamDetailsWritten = new ExamsDetails();

                if (examUnderThisParent != null && examUnderThisParent.ExamDetails.Any())
                {
                    if (examUnderThisParent.IsMcq)
                    {
                        firstExamDetailsMcq = examUnderThisParent.ExamDetails.FirstOrDefault(x => x.ExamType == ExamType.Mcq);
                        if (firstExamDetailsMcq != null) mcqSubjectType = firstExamDetailsMcq.SubjectType;
                    }
                    if (examUnderThisParent.IsWritten)
                    {
                        firstExamDetailsWritten = examUnderThisParent.ExamDetails.FirstOrDefault(x => x.ExamType == ExamType.Written);
                        if (firstExamDetailsWritten != null) writtenSubjectType = firstExamDetailsWritten.SubjectType;
                    }
                }
                ViewBag.McqSubjectType = mcqSubjectType;
                ViewBag.WrittenSubjectType = writtenSubjectType;

                var courseSubject = examParent.Course.CourseSubjects.ToList();
                var subjects = new List<Subject>();
                if (courseSubject.Count > 0)
                {
                    subjects.AddRange(from subject in courseSubject where subject.Status == Subject.EntityStatus.Active select subject.Subject);
                }
                //ViewBag.SubjectList = new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name");
                ViewBag.SubjectList = subjects;
                ViewBag.FirstExamDetailsMcq = firstExamDetailsMcq;
                ViewBag.FirstExamDetailsWritten = firstExamDetailsWritten;
                ViewBag.AnswerScriptCount = answerScript;
                ViewBag.StudentMarksCount = studentMarks;
                ViewBag.ExamParent = examParent;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            ViewBag.ExamParent = examParent;
            return View();
        }
        #endregion

        #region Rank Operation
        [HttpPost]
        public ActionResult RankChange(int id, int current, string action)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    Exams examUpdateObj = _examsService.LoadById(Convert.ToInt64(id));
                    if (examUpdateObj == null)
                    {
                        return HttpNotFound();
                    }
                    else
                    {
                        int newRank;

                        if (action == "up")
                            newRank = examUpdateObj.Rank - 1;
                        else
                            newRank = examUpdateObj.Rank + 1;

                        var examOldObj = _examsService.LoadByRankNextOrPrevious(newRank, action);//LoadByRank(newRank);
                        newRank = examOldObj.Rank;

                        examOldObj.Rank = examUpdateObj.Rank;
                        examUpdateObj.Rank = newRank;

                        bool isSuccessfullyUpdateRank = _examsService.UpdateRank(examOldObj, examUpdateObj);
                        if (isSuccessfullyUpdateRank)
                        {
                            return Json(new Response(true, "Successfully changed"));
                        }
                        return Json(new Response(false, "Something Wrong Occured!!"));
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                }
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            else
            {
                return HttpNotFound();
            }

        }
        #endregion

        #region Render Partial Views
        [HttpPost]
        public ActionResult GetPartialView(long courseId, string viewName)
        {
            try
            {
                var courseList = _courseService.GetCourse(courseId);
                var courseSubject = courseList.CourseSubjects.ToList();
                List<Subject> subjects = new List<Subject>();
                if (courseSubject.Count > 0)
                {
                    foreach (var subject in courseSubject)
                    {
                        if (subject.Status == Subject.EntityStatus.Active)
                        {
                            subjects.Add(subject.Subject);
                        }
                    }
                }
                ViewBag.subjectList = new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name");
                ViewBag.subjectCount = subjects.Count;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

            }
            return PartialView("Partial/" + viewName);
        }
        [HttpPost]
        public ActionResult GetPartialViewForEdit(long courseId, long examId, string viewName, bool isMcq = false, bool iswritten = false)
        {
            try
            {
                var exam = _examsService.LoadById(examId);
                var examDetails = exam.ExamDetails.ToList();
                var mcqSingle = new List<ExamsDetails>();
                var mcqMultiple = new List<ExamsDetails>();
                var mcqCombined = new List<ExamsDetails>();
                var writtenSingle = new List<ExamsDetails>();
                var writtenMultiple = new List<ExamsDetails>();
                var writtenCombined = new List<ExamsDetails>();
                var courseList = _courseService.GetCourse(courseId);
                var courseSubject = courseList.CourseSubjects.ToList();
                List<Subject> subjects = new List<Subject>();
                if (courseSubject.Count > 0)
                {
                    foreach (var subject in courseSubject)
                    {
                        if (subject.Status == Subject.EntityStatus.Active)
                        {
                            subjects.Add(subject.Subject);
                        }
                    }
                }
                foreach (var examsDetailse in examDetails)
                {
                    if (examsDetailse.ExamType == ExamType.Mcq)
                    {
                        if (examsDetailse.SubjectType == SubjectType.Single)
                        {
                            mcqSingle.Add(examsDetailse);
                        }
                        if (examsDetailse.SubjectType == SubjectType.Multiple)
                        {
                            mcqMultiple.Add(examsDetailse);
                        }
                        if (examsDetailse.SubjectType == SubjectType.Combined)
                        {
                            mcqCombined.Add(examsDetailse);
                        }
                    }
                    if (examsDetailse.ExamType == ExamType.Written)
                    {
                        if (examsDetailse.SubjectType == SubjectType.Single)
                        {
                            writtenSingle.Add(examsDetailse);
                        }
                        if (examsDetailse.SubjectType == SubjectType.Multiple)
                        {
                            writtenMultiple.Add(examsDetailse);
                        }
                        if (examsDetailse.SubjectType == SubjectType.Combined)
                        {
                            writtenCombined.Add(examsDetailse);
                        }
                    }
                }
                if (mcqSingle.Count > 0)
                {
                    ViewBag.mcqSingleList = mcqSingle[0];
                    ViewBag.mcqSingleSubjectList = new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name", mcqSingle[0].Subject.Id);
                }
                if (mcqMultiple.Count > 0)
                {
                    ViewBag.mcqMultipleList = mcqMultiple;
                    //var ed0 = mcqMultiple[0];
                    //var marks0 = mcqMultiple[0].ExamsStudentMarks.ToList();
                    IList<SelectList> mcqMultipleSubject = new List<SelectList>();
                    foreach (var mm in mcqMultiple)
                    {
                        mcqMultipleSubject.Add(new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name", mm.Subject.Id));
                    }
                    ViewBag.mcqMultipleSubjectList = mcqMultipleSubject;
                    ViewBag.MaximumSubjectCount = exam.McqMaxSubjectCount;
                }
                if (mcqCombined.Count > 0)
                {
                    ViewBag.mcqCombinedList = mcqCombined[0];
                    //ViewBag.mcqSingleSubjectList = new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name", mcqSingle[0].Subject.Id);
                }
                ViewBag.subjectList = new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name");
                ViewBag.subjectCount = subjects.Count;
                ViewBag.leftSubjects = subjects.Count - mcqMultiple.Count;

                //written
                if (writtenSingle.Count > 0)
                {
                    ViewBag.writtenSingleList = writtenSingle[0];
                    ViewBag.writtenSingleSubjectList = new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name", writtenSingle[0].Subject.Id);
                }
                if (writtenMultiple.Count > 0)
                {
                    ViewBag.writtenMultipleList = writtenMultiple;
                    IList<SelectList> writtenMultipleSubject = new List<SelectList>();
                    foreach (var wm in writtenMultiple)
                    {
                        writtenMultipleSubject.Add(new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name", wm.Subject.Id));
                    }
                    ViewBag.writtenMultipleSubjectList = writtenMultipleSubject;
                    ViewBag.writtenMaximumSubjectCount = exam.WrittenMaxSubjectCount;
                }
                if (writtenCombined.Count > 0)
                {
                    ViewBag.writtenCombinedList = writtenCombined[0];
                    //ViewBag.mcqSingleSubjectList = new SelectList(subjects.OrderBy(x => x.Rank).ToList(), "Id", "Name", mcqSingle[0].Subject.Id);
                }

                ViewBag.leftWrittenSubjects = subjects.Count - writtenMultiple.Count;
                ViewBag.ExamStdMarksCount = (examDetails.Select(x => x.ExamsStudentMarks).ToList()).Count;
                ViewBag.ExamStdAnswerScriptCount = (examDetails.Select(x => x.ExamsStudentAnswerScripts).ToList()).Count;


            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return PartialView("Partial/" + viewName);
        }
        #endregion

        #region Ajax Request Function

        #endregion

    }
}
