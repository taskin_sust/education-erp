﻿using System;

namespace UdvashERP.MessageExceptions
{
    public class MessageException: Exception
    {
        public MessageException():base("Message Data")
        {

        }

        public MessageException(string message)
            : base(message)
        {

        }

    }
}
