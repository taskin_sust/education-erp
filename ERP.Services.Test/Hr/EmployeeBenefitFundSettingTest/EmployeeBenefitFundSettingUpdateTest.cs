﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.EmployeeBenefitFundSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "EmployeeBenefitsFundSetting")]
    public class EmployeeBenefitFundSettingUpdateTest : EmployeeBenefitFundSettingBaseTest
    {

        [Fact]
        public void Update_Should_Be_Successfully()
        {
            var organization = _organizationFactory.Create().Persist();
            var branch = branchFactory.Create().WithOrganization(organization.Object).Persist();
            var userMenu = BuildUserMenu(organization.Object, (Program)null, branch.Object);

            string name = "Permanent";
            DateTime effectiveDate = DateTime.Parse("2016-09-01");
            DateTime closingDate = DateTime.Parse("2016-09-10");
            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);
            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);
            _employeeBenefitsFundSettingFactory.Create(name).WithOrganization(organization.Object).WithEmploymentStatus(MemberEmploymentStatus.Permanent).WithCalculationOn(CalculationOn.Basic)
             .WithEmployeeEmployerContribution(10, 25).WithEffectiveClosingDate(effectiveDate, closingDate).WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList).Persist(userMenu);

            EmployeeBenefitsFundSetting employeeBenefitsFundSetting = _employeeBenefitsFundSettingService.GetEmployeeBenefitFundSetting(_employeeBenefitsFundSettingFactory.Object.Id);

            const string updatedName = "PermanentUpdate";
            employeeBenefitsFundSetting.Name = updatedName; _employeeBenefitsFundSettingFactory.Update(userMenu, employeeBenefitsFundSetting.Id, employeeBenefitsFundSetting);
            EmployeeBenefitsFundSetting updatedEmployeeBenefitsFundSetting = _employeeBenefitsFundSettingService.GetEmployeeBenefitFundSetting(employeeBenefitsFundSetting.Id);
            Assert.NotNull(updatedEmployeeBenefitsFundSetting);
            Assert.True(updatedEmployeeBenefitsFundSetting.Name == updatedName);

        }
    }
}
