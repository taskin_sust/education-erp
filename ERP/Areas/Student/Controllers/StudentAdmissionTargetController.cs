﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using NHibernate.Hql.Ast.ANTLR;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class StudentAdmissionTargetController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentArea");
        #endregion
        
        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IBranchService _branchService;
        private readonly IStudentAdmissionTargetService _studentAdmissionTargetService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly IProgramSettingService _programSettingService;
        private readonly IStudentAdmissionBranchTarget _studentAdmissionBranchTarget;
        private readonly IStudentProgramService _studentProgramService;
        private List<UserMenu> _userMenu;
        public StudentAdmissionTargetController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _organizationService = new OrganizationService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _branchService = new BranchService(session);
                _studentAdmissionTargetService = new StudentAdmissionTargetService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _studentAdmissionBranchTarget = new StudentAdmissionBranchTarget(session);
                _studentProgramService = new StudentProgramService(session);
                _programSettingService = new ProgramSettingService(session);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        } 
        #endregion
       
        #region Save Operation
        public ActionResult TargetEntry()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //ViewBag.OrganizationId = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            StudentAdmissionTargetViewModel studentAdmissionTargetObj = new StudentAdmissionTargetViewModel();
            return View(studentAdmissionTargetObj);
        }
        [HttpPost]
        public ActionResult TargetEntry(StudentAdmissionTargetViewModel studentAdmissionTargetObj)
        {
            try
            {
                if (studentAdmissionTargetObj.OrganizationId == 0)
                {
                    return Json(new Response(false, "You need to select organization."));
                }
                if (studentAdmissionTargetObj.ProgramId == 0)
                {
                    return Json(new Response(false, "You need to select program."));
                }
                if (studentAdmissionTargetObj.SessionId == 0)
                {
                    return Json(new Response(false, "You need to select session."));
                }
                if (String.IsNullOrEmpty(studentAdmissionTargetObj.DeadLine.ToString()))
                {
                    return Json(new Response(false, "Dead Line Is required"));
                }
                if (String.IsNullOrEmpty(studentAdmissionTargetObj.DeadLineTitle))
                {
                    return Json(new Response(false, "Dead Line Title Is required"));
                }
                if (!studentAdmissionTargetObj.BranchStudentAdmissionTargetList.Any())
                {
                    return Json(new Response(false, "You must give at least one branch target."));
                }
                var deadLineGreaterThanToday = _studentAdmissionTargetService.GetByProgramAndSessionId(studentAdmissionTargetObj.ProgramId, studentAdmissionTargetObj.SessionId, DateTime.Now);
                if (deadLineGreaterThanToday != null)
                {
                    return Json(new Response(false, "There is already an active dead line."));
                }
                Program program = _programService.GetProgram(studentAdmissionTargetObj.ProgramId);
                Session session = _sessionService.LoadById(studentAdmissionTargetObj.SessionId);
                StudentAdmissionTarget studentAdmissionTarget = new StudentAdmissionTarget();
                studentAdmissionTarget.Program = program;
                studentAdmissionTarget.Session = session;
                studentAdmissionTarget.DeadTime = (DateTime)studentAdmissionTargetObj.DeadLine;
                studentAdmissionTarget.DeadLineTitle = studentAdmissionTargetObj.DeadLineTitle;
                IList<StudentBranchTarget> studentBranchTargetList = new List<StudentBranchTarget>();
                foreach (var bsat in studentAdmissionTargetObj.BranchStudentAdmissionTargetList)
                {
                    var studentCount = _studentProgramService.GetAdmittedStudentCountByBranchProgramSession(bsat.BranchId, program.Id, session.Id);
                    if (studentCount > bsat.Target)
                    {
                        Branch branch = _branchService.GetBranch(bsat.BranchId);
                        throw new DependencyException(branch.Name + " has less Target(" + bsat.Target + ") then current student count(" + studentCount + ").");
                    }
                    StudentBranchTarget studentBranchTarget = new StudentBranchTarget();
                    studentBranchTarget.Branch = _branchService.GetBranch(bsat.BranchId);
                    studentBranchTarget.Target = bsat.Target;
                    studentBranchTarget.StudentAdmissionTarget = studentAdmissionTarget;
                    studentBranchTarget.CreateBy = Convert.ToInt64(IdentityExtensions.GetUserId(User.Identity));
                    studentBranchTarget.ModifyBy = Convert.ToInt64(IdentityExtensions.GetUserId(User.Identity));
                    studentBranchTarget.Status = StudentBranchTarget.EntityStatus.Active;
                    studentBranchTargetList.Add(studentBranchTarget);
                }
                studentAdmissionTarget.StudentBranchTargets = studentBranchTargetList;
                _studentAdmissionTargetService.Save(studentAdmissionTarget);
                return Json(new Response(true, "Student Admission Target Save Successful"));
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        } 
        #endregion
       
        #region Manage
        public ActionResult ManageTarget()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //ViewBag.OrganizationId = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            StudentAdmissionTargetViewModel studentAdmissionTargetObj = new StudentAdmissionTargetViewModel();
            return View(studentAdmissionTargetObj);
        }

        [HttpPost]
        public JsonResult ManageTarget(long programId, long sessionId, string deadLine, string deadLineTitle, string deadLineTitleModified, string deadLineModified, long[] brancheIds = null, int[] targets = null)
        {
            try
            {
                if (targets== null ||targets.Length < 1)
                {
                    return Json(new Response(false, "You must assign target for at least one branch"));
                }
                DateTime _deadLine = DateTime.ParseExact(deadLine, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime _deadLineModified = DateTime.ParseExact(deadLineModified, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                var success = _studentAdmissionTargetService.Update(programId, sessionId, _deadLine, deadLineTitle, deadLineTitleModified, _deadLineModified, brancheIds, targets);
                if (success)
                {
                    return Json(new Response(true, "Student Admission Target Update Successful"));
                }
                return Json(new Response(false, "Student Admission Target Update Fail"));
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult DeleteTarget(long programId, long sessionId, string deadLine, string deadLineTitle)
        {
            try
            {
                DateTime _deadLine = DateTime.ParseExact(deadLine, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                bool isSuccess = _studentAdmissionTargetService.Delete(programId, sessionId, _deadLine, deadLineTitle);
                return Json(isSuccess ? new Response(true, "Student Admission Target deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Student Admission Target Delete Fail !"));
            }
        }
        #endregion

        #region Achievement Report
        public ActionResult AchievementReport()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                SelectList reportType = new SelectList(_commonHelper.LoadEmumToDictionary<ReportType>(), "Key", "Value");
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                var selectList = new SelectList(organizations, "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.orgList = selectList;
                ViewBag.DeadLineTitle = new SelectList(new List<StudentAdmissionTarget>(), "Id", "DeadLineTitle");
                ViewBag.ReportType = reportType;
                ViewBag.Branch = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu), "Id", "Name");
                var studentAdmissionTarget = new SelectList(new List<StudentAdmissionTarget>(), "Id","DeadLineTitle");
                ViewBag.StudentTarget = studentAdmissionTarget;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            return View();
        }

        [HttpPost]
        public JsonResult LoadDeadline(long organizationId = 0, long programId = 0, long sessionId = 0)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var deadline = _studentAdmissionTargetService.LoadDeadLine(organizationId, programId, sessionId);
                var deadLineTitleSelectList = new SelectList(deadline, "Id", "DeadLineTitle");
                return Json(new { deadline = deadLineTitleSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
     
        [HttpPost]
        public ActionResult AchievementReport(StudentAdmissionReportTargetViewModel studentAdmissionReportTargetViewModel)
        {
            try
            {
                var organization = _organizationService.LoadById(studentAdmissionReportTargetViewModel.OrganizationId);

                if (studentAdmissionReportTargetViewModel.SessionId > 0 && studentAdmissionReportTargetViewModel.ProgramId > 0)
                {
                    var program = _programService.GetProgram(studentAdmissionReportTargetViewModel.ProgramId);
                    var session = _sessionService.LoadById(studentAdmissionReportTargetViewModel.SessionId);
                    ViewBag.Session = session.Name;
                    ViewBag.Program = program.ShortName;
                }
                if (studentAdmissionReportTargetViewModel.StudentAdmissionTarget != 0)
                {
                    StudentAdmissionTarget deadLineTitle = _studentAdmissionTargetService.GetById(studentAdmissionReportTargetViewModel.StudentAdmissionTarget);
                    ViewBag.Target = deadLineTitle.DeadLineTitle;
                }
                else
                {
                    ViewBag.Target = "Last";
                }
                var json = new JavaScriptSerializer().Serialize(studentAdmissionReportTargetViewModel);
                ViewData["studentAdmissionReportTargetViewModel"] = json;


                ViewBag.Organization = organization.Name;
                if (studentAdmissionReportTargetViewModel.ReportType == 1)
                {
                    ViewBag.informationViewList = new string[] { "Branch" };
                }
                else
                {
                    var branch = _branchService.GetBranch(studentAdmissionReportTargetViewModel.BranchId);
                    ViewBag.Branch = branch.Name;
                    ViewBag.informationViewList = new string[] { "Program"};
                }


                return View("AchievementReportPost");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public ActionResult AchievementReportPost(int draw, int start, int length, string studentAdmissionReportTargetViewModel)
        {
            try
            {
                if (!String.IsNullOrEmpty(studentAdmissionReportTargetViewModel))
                {
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    StudentAdmissionReportTargetViewModel studentAdmissionReportTarget = new JavaScriptSerializer().Deserialize<StudentAdmissionReportTargetViewModel>(studentAdmissionReportTargetViewModel);
                    IList<AchievementReportViewModel> achievementReportViewModels = new List<AchievementReportViewModel>();
                    if (studentAdmissionReportTarget.ReportType == 1)
                    {
                        IList<StudentBranchTarget> particularadmissionBranchTargets = _studentAdmissionBranchTarget.LoadByAdmissionTarget(studentAdmissionReportTarget.StudentAdmissionTarget);
                        achievementReportViewModels = GetAchievementReportViewModelList(particularadmissionBranchTargets, studentAdmissionReportTarget.ProgramId, studentAdmissionReportTarget.SessionId, userMenu);
                        ViewBag.informationViewList = new string[] { "Branch" };
                    }
                    if (studentAdmissionReportTarget.ReportType == 2)
                    {
                        if (studentAdmissionReportTarget.StudentAdmissionTarget == 0)
                        {
                            achievementReportViewModels = GetAchievementReportBranchViewModelList(studentAdmissionReportTarget.OrganizationId, studentAdmissionReportTarget.BranchId);
                        }
                        else if (studentAdmissionReportTarget.StudentAdmissionTarget != 0)
                        {
                            achievementReportViewModels = GetAchievementReportBranchViewModelList(studentAdmissionReportTarget.OrganizationId, studentAdmissionReportTarget.BranchId, studentAdmissionReportTarget.StudentAdmissionTarget);
                        }
                        ViewBag.informationViewList = new string[] { "Program" };
                        var branch = _branchService.GetBranch(studentAdmissionReportTarget.BranchId);
                        ViewBag.Branch = branch.Name;
                    }
                    int recordsTotal = achievementReportViewModels.Count;
                    long recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    int previousSessionStudentCount = 0;
                    int targetStudentCount = 0;
                    int achievementSessionStudentCount = 0;
                    decimal achievement = 0;
                    int numOfRow = 0;
                    foreach (var datareportAdmissionTarget in achievementReportViewModels)
                    {
                        var str = new List<string>();
                        var className = "";
                        if (datareportAdmissionTarget.AchievementPercentage >= 100)
                        {
                            className = "achievementSuccess";
                        }
                        if (!String.IsNullOrEmpty(datareportAdmissionTarget.BranchName))
                        {
                            str.Add("<span class='" + className + "'>" + datareportAdmissionTarget.BranchName.ToString() + "</span>");
                        }
                        if (!String.IsNullOrEmpty(datareportAdmissionTarget.ProgramName))
                        {
                            str.Add("<span class='" + className + "'>" + datareportAdmissionTarget.ProgramName.ToString() + "</span>");
                        }
                        str.Add("<span class='" + className + "'>" + datareportAdmissionTarget.PreviousSessionStudentCount.ToString() + "</span>");
                        str.Add("<span class='" + className + "'>" + datareportAdmissionTarget.Target.ToString() + "</span>");
                        str.Add("<span class='" + className + "'>" + datareportAdmissionTarget.Achievement.ToString() + "</span>");
                        str.Add("<span class='" + className + "'>" + datareportAdmissionTarget.AchievementPercentage.ToString() + "% </span>");
                        if (!String.IsNullOrEmpty(datareportAdmissionTarget.DeadLine))
                        {
                            str.Add("<span class='" + className + "'>" + datareportAdmissionTarget.DeadLine.ToString() + "</span>");
                        }
                        data.Add(str);

                        previousSessionStudentCount += (datareportAdmissionTarget.PreviousSessionStudentCount != "") ? Convert.ToInt32(datareportAdmissionTarget.PreviousSessionStudentCount) : 0;
                        targetStudentCount += datareportAdmissionTarget.Target;
                       achievementSessionStudentCount += datareportAdmissionTarget.Achievement;
                        achievement += datareportAdmissionTarget.AchievementPercentage;
                        numOfRow ++;
                    }

                  
                    if (studentAdmissionReportTarget.ReportType == 1)
                    {
                        var str2 = new List<string>();
                        str2.Add("<span class='achievementTotal'>" + "Total" + "</span>");
                        str2.Add("<span class='achievementTotal'>" + previousSessionStudentCount.ToString() + "</span>");
                        str2.Add("<span class='achievementTotal'>" + targetStudentCount.ToString() + "</span>");
                        str2.Add("<span class='achievementTotal'>" + achievementSessionStudentCount.ToString() + "</span>");
                        str2.Add("<span class='achievementTotal'>" + decimal.Round((((decimal)achievementSessionStudentCount / (decimal)targetStudentCount) * 100), 0, MidpointRounding.AwayFromZero).ToString() + "%" + "</span>");
                        data.Add(str2);
                    }
                   

                    data = data.Skip(start).Take(length).ToList();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 1,
                    recordsFiltered = 1,
                    start = start,
                    length = length,
                    data = new List<String>()
                });
            }
            catch (Exception e)
            {
                throw;
            }
            //return View();
        }

        private IList<AchievementReportViewModel> GetAchievementReportViewModelList(IList<StudentBranchTarget> studentadmissionBranchTargets, long programId, long sessionId, List<UserMenu> userMenu)
        {
            try
            {
                IList<AchievementReportViewModel> achievementReportViewModels = new List<AchievementReportViewModel>();
                var targetId = studentadmissionBranchTargets[0].StudentAdmissionTarget.Id;
                var deadLine = studentadmissionBranchTargets[0].StudentAdmissionTarget.DeadTime;
                var session = _sessionService.LoadById(sessionId);
                var queryResult = _studentAdmissionTargetService.LoadProgramwiseStudentTarget(targetId, programId, sessionId, deadLine, session.Rank);
                foreach (var obj in queryResult)
                {
                    IDictionary<string, object> dic;
                    var noxiousAchievementReport = new AchievementReportViewModel();
                    foreach (KeyValuePair<string, object> entry in dic = obj)
                    {
                        string defaultValue = entry.Key;
                        switch (defaultValue)
                        {
                            case "BranchName":
                                {
                                    noxiousAchievementReport.BranchName = (entry.Value != null) ?entry.Value.ToString() :" No Branch Name Found!";
                                    break;
                                }
                            case "Target":
                                {
                                    noxiousAchievementReport.Target = (entry.Value != null)?Convert.ToInt32(entry.Value.ToString().Trim()): 0;
                                    break;
                                }
                            case "Achievement":
                                {
                                    noxiousAchievementReport.Achievement = (entry.Value != null)?Convert.ToInt32(entry.Value.ToString().Trim()):0;
                                    break;
                                }
                            case "AchievementPercentage":
                                {
                                    if (entry.Value == null)
                                    {
                                        noxiousAchievementReport.AchievementPercentage = 0;
                                        break;
                                    }
                                    noxiousAchievementReport.AchievementPercentage = Convert.ToDecimal(entry.Value.ToString().Trim());
                                    break;
                                }
                            case "PreviousSessionStudentCount":
                                {
                                    noxiousAchievementReport.PreviousSessionStudentCount = (entry.Value != null) ? entry.Value.ToString() : "N/A";
                                    break;
                                }
                            default: { break; }

                        }
                    }
                    achievementReportViewModels.Add(noxiousAchievementReport);
                }

                return achievementReportViewModels;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private IList<AchievementReportViewModel> GetAchievementReportBranchViewModelList(long organizationId , long branchId, long? studentAdmissionProgramId = null )
        {
            try
            {
                IList<AchievementReportViewModel> achievementReportViewModels = new List<AchievementReportViewModel>();
                var queryResult = _studentAdmissionTargetService.LoadBranchwiseStudentTarget(organizationId, branchId, studentAdmissionProgramId);
                foreach (var obj in queryResult)
                {
                    IDictionary<string, object> dic;
                    var noxiousAchievementReport = new AchievementReportViewModel();
                    foreach (KeyValuePair<string, object> entry in dic = obj)
                    {
                        string defaultValue = entry.Key;
                        switch (defaultValue)
                        {
                            case "ProgramName":
                                {
                                    noxiousAchievementReport.ProgramName = (entry.Value !=null)?entry.Value.ToString():"N/A";
                                    break;
                                }
                            case "DeadLine":
                                {
                                    noxiousAchievementReport.DeadLine = (entry.Value !=null)? Convert.ToDateTime(entry.Value.ToString()).ToString("dd MMM, yyyy"): "N/A";
                                    break;
                                }
                            case "Target":
                                {
                                    if (entry.Value == null)
                                    {
                                        noxiousAchievementReport.Target = 0;
                                        break;
                                    }
                                    noxiousAchievementReport.Target = Convert.ToInt32(entry.Value.ToString().Trim());
                                    break;
                                }
                            case "Achievement":
                                {
                                    if (entry.Value == null)
                                    {
                                        noxiousAchievementReport.Achievement = 0;
                                        break;
                                    }
                                    noxiousAchievementReport.Achievement = Convert.ToInt32(entry.Value.ToString().Trim());
                                    break;
                                }
                            case "AchievementPercentage":
                                {
                                    if (entry.Value == null)
                                    {
                                        noxiousAchievementReport.AchievementPercentage = 0;
                                        break;
                                    }
                                    noxiousAchievementReport.AchievementPercentage = Convert.ToDecimal(entry.Value.ToString().Trim());
                                    break;
                                }
                            case "PreviousSessionStudentCount":
                                {
                                    noxiousAchievementReport.PreviousSessionStudentCount = (entry.Value != null) ? entry.Value.ToString() : "N/A";
                                    break;
                                }
                            default: { break; }

                        }
                       

                    }
                    achievementReportViewModels.Add(noxiousAchievementReport);
                }

                return achievementReportViewModels;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        #endregion
        
        #region Ajax Request Function
        public ActionResult AjaxRequestForDrawingBranchTable(long programId, long sessionId)
        {
            try
            {
                ViewBag.TotalCurrentStudent = 0;
                var deadLineGreaterThanToday = _studentAdmissionTargetService.GetByProgramAndSessionId(programId, sessionId, DateTime.Now);
                if (deadLineGreaterThanToday != null)
                {
                    return Json(new Response(false, "DeadLine already exists."));
                }
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var allBranches = new List<Branch>();
                var branchStudentCount = new List<int>();
                allBranches = _branchService.LoadBranch(null, _commonHelper.ConvertIdToList(programId),_commonHelper.ConvertIdToList(sessionId)).ToList();
                allBranches = allBranches.DistinctBy(x => x.Id).OrderBy(x => x.Rank).ToList();
                ViewBag.BranchList = allBranches;
                foreach (var allBranch in allBranches)
                {
                    var studentCount = _studentProgramService.GetAdmittedStudentCountByBranchProgramSession(allBranch.Id, programId, sessionId);
                    branchStudentCount.Add(studentCount);
                    ViewBag.TotalCurrentStudent += studentCount;
                }
                ViewBag.BranchStudentCount = branchStudentCount;
                return PartialView("_BranchTable");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }
        public ActionResult AjaxRequestForDrawingBranchTableForManage(long programId, long sessionId, string deadLineTitle)
        {
            try
            {
                ViewBag.TotalCurrentStudent = 0;
                ViewBag.TotalCurrentTargetStudent = 0;
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var deadlineDate = new DateTime();
                List<Branch> allBranches = _branchService.LoadBranch(null, _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId)).ToList();
                var branchList = allBranches.DistinctBy(x => x.Id).ToList();
                IList<StudentBranchTarget> branchTargetList =_studentAdmissionTargetService.LoadBranchTargets(userMenu, programId, sessionId, deadLineTitle);
                branchList = branchList.Where(x => !x.Id.In(branchTargetList.Select(y => y.Branch.Id).ToArray())).ToList();
                var program = _programService.GetProgram(programId);
                ViewBag.ProgramName = program.Name;
                ViewBag.OrganizationName = program.Organization.ShortName;
                ViewBag.SessionName = _sessionService.LoadById(sessionId).Name;
                deadlineDate = _studentAdmissionTargetService.LoadDeadLine(programId, sessionId, deadLineTitle);
                ViewBag.DeadLine = deadlineDate.ToString("dd-MM-yyyy");
                ViewBag.DeadLineTitle = deadLineTitle.Trim();
                ViewBag.BranchTargetList = branchTargetList.ToList();
                ViewBag.IsDisable = (deadlineDate.AddDays(1) < DateTime.Now) ? true : false;
                var studentTargetCheckList = new List<StudentTargetCheckingViewModel>();
                foreach (var branchTarget in branchTargetList)
                {
                    var studentTargetCheck = new StudentTargetCheckingViewModel();
                    var branchWithTarget = branchTarget.Branch;
                    var studentCount =_studentProgramService.GetAdmittedStudentCountByBranchProgramSession(branchWithTarget.Id,programId, sessionId);
                    studentTargetCheck.CurrentStudentCount = studentCount;
                    studentTargetCheck.Branch = branchWithTarget;
                    studentTargetCheck.Target = branchTarget.Target;
                    studentTargetCheckList.Add(studentTargetCheck);
                    ViewBag.TotalCurrentStudent += studentCount;
                    ViewBag.TotalCurrentTargetStudent += branchTarget.Target;
                }
                foreach (var branchWithoutTarget in branchList)
                {
                    var studentTargetCheck = new StudentTargetCheckingViewModel();
                    var studentCount =_studentProgramService.GetAdmittedStudentCountByBranchProgramSession(branchWithoutTarget.Id,programId, sessionId);
                    studentTargetCheck.CurrentStudentCount = studentCount;
                    studentTargetCheck.Branch = branchWithoutTarget;
                    studentTargetCheck.Target = 0;
                    studentTargetCheckList.Add(studentTargetCheck);
                    ViewBag.TotalCurrentStudent += studentCount;
                    ViewBag.TotalCurrentTargetStudent += 0;
                }
                ViewBag.BranchList = studentTargetCheckList.OrderBy(x => x.Branch.Rank).ToList();
                return PartialView("Partial/_BranchTableForManage");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }
        public JsonResult GetProgramByOrg(string organizatonId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<SelectListItem> programList = new SelectList(_programService.LoadAuthorizedProgram(userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(organizatonId))), "Id", "Name").ToList();
                return Json(new { returnList = programList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Program load failed";
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult GetSession(long programId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var sessionSelectList = new SelectList(_sessionService.LoadAuthorizedSession(_userMenu, null, _commonHelper.ConvertIdToList(programId)), "Id", "Name");
                List<SelectListItem> sessionList = sessionSelectList.ToList();
                return Json(new { sessionList = sessionList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult GetDeadLineTitle(long programId, long sessionId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var deadLineTitle = _studentAdmissionTargetService.LoadDeadLineTitles(programId, sessionId).FirstOrDefault();
                var deadLineTitleList = new List<SelectListItem>();
                if (deadLineTitle != null)
                {
                    deadLineTitleList = new List<SelectListItem>()
                {
                    new SelectListItem() { Text = deadLineTitle, Value = deadLineTitle }
                };
                }
                return Json(new { deadLineTitleList = deadLineTitleList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }


        public ActionResult CheckValidStudentInternalTarget(long programId, long sessionId, long branchId, int? internalTargetAmount)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool isValidInternalTargetAmount = true;
                    var previousProgramSettings = _programSettingService.LoadPreviousProgramSettings(programId, sessionId);
                    int totalStudentProgramCount = 0;
                    foreach (var previousProgramSetting in previousProgramSettings)
                    {
                        var studentProgramCount = _studentProgramService.GetAdmittedStudentCountByBranchProgramSession(branchId, previousProgramSetting.Program.Id, previousProgramSetting.Session.Id);
                        totalStudentProgramCount += studentProgramCount;
                    }
                    if (internalTargetAmount > totalStudentProgramCount)
                    {
                        isValidInternalTargetAmount = false;
                    }
                    return Json(isValidInternalTargetAmount);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion
       
       
    }
}