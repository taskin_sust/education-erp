﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ProgramTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Program")]
    public class ProgramLoadByIdTest : ProgramBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _programService.GetProgram(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            
            var programFactory =
                _programFactory.Create().WithOrganization().WithNameAndRank(_programName, 1).WithProperties(_programName, _shortName,_programFactory.GetRandomNumber(), _paidProgram)
                    .Persist();
            var obj = _programService.GetProgram(programFactory.Object.Id);
            Assert.NotNull(obj);
        }

    }
}
