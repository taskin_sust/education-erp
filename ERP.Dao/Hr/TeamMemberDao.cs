﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface ITeamMemberDao : IBaseDao<TeamMember, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        TeamMember GetMember(int? pin, DateTime? joiningDate = null);
        TeamMember GetTeamMemberByProfileId(long profileId, long teamMemberId);
        TeamMember LoadByUserProfile(UserProfile user);
        TeamMemberTransferViewModel GetTeamMemberTransferInfo(long teamMemberId);
        int GetMemberWeekend(long memberId);
        Organization GetTeamMemberSalaryOrganization(DateTime searchingDate, long? teamMemberId, int? teamMemberPin);
        Organization GetTeamMemberOrganization(DateTime searchingDate, long? teamMemberId, int? teamMemberPin);
        Branch GetTeamMemberSalaryBranch(DateTime searchingConvertedDate, long? teamMemberId, int? teamMemberPin);
        Branch GetTeamMemberBranch(DateTime searchingConvertedDate, long? teamMemberId, int? teamMemberPin);
        Campus GetTeamMemberSalaryCampus(DateTime searchingConvertedDate, long? teamMemberId, int? teamMemberPin);
        Campus GetTeamMemberCampus(DateTime searchingConvertedDate, long? teamMemberId, int? teamMemberPin);
        Department GetTeamMemberDepartment(DateTime searchingConvertedDate, long? teamMemberId, int? teamMemberPin);
        Department GetTeamMemberSalaryDepartment(DateTime searchingConvertedDate, long? teamMemberId, int? teamMemberPin);
        Designation GetTeamMemberDesignation(DateTime searchingConvertedDate, long? teamMemberId, int? teamMemberPin);

        #endregion

        #region List Loading Function

        IList<TeamMember> GetPinByAutoComplete(string query, TeamMember member, string date);
        IList<TeamMember> LoadHrAuthorizedTeamMember(DateTime? searchingDate = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, List<int> pinList = null, bool withRetired = true, int searchTeamMemberOn = 1);
        List<long> LoadHrAuthorizedTeamMemberId(DateTime? searchingDate, List<long> authBranchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, List<int> pinList = null, bool withRetired = true, int searchTeamMemberOn = 1);
        IList<TeamMember> LoadMemberByUserList(List<long> userList);
        List<BusinessModel.Dto.Hr.LeaveTypeDtoList> LoadMembersPendingLeave(long memId, bool isHr = false, int leaveYear = 0);
        IList<TeamMember> LoadTeamMemberDirectory(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> branchIdList, List<long> campusIdList, long? departmentId, string name, int? pin, int? bloodGroup);
        IList<MemberSearchListDto> LoadTeamMemberListReport(int draw, int start, string orderBy, string orderDir, int length, List<long> organizationIdList, long? departmentId, List<long> branchIdList, List<long> campusIdList, int? memberTypes, int? memberStatus);
        IList<MemberSearchListDto> LoadTeamMemberSearchListReport(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> branchIdList, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList);
        IList<MemberSearchListDto> LoadSearchTeammemberList(int start, int length, string orderBy, string orderDir, List<long> authoBrnachIdList, List<long> departmentIdList, List<long> campusIdList, List<int> memberTypeList, List<int> memberStatuList, string keyword, List<string> informationViewList, List<int> employmentStatusIdList, List<int> genderIdList, List<int> religionIdList, List<int> bloodGroupIdList, List<int> maritalStatusIdList);
        IList<TeamMemberBatchShiftDto> LoadTeamMemberBatchShiftDto(List<long> branchIdList, List<long> campusIdList, List<int> employmentStatusList, List<long> departmntIdList, List<long> designationIdList, List<long> shiftIdList, DateTime? searchingEffectiveDate);
        IList<TeamMemberBatchSalaryHistoryDto> LoadTeamMemberBatchSalaryHistoryDto(List<long> authbranchIdList, List<long> campusIds, List<int> employmentStatusList, List<long> departmentIds, List<long> designationIds, bool isSearchOnEmploymetHistory);
        List<FestivalBonusSheetDto> LoadTeamMemberListForFbSheet(DateTime startDate, DateTime endDate, List<int> religionList, FestivalBonusSheetFormViewModel viewModel, List<long> authOrganizationIdList, List<long> authBranchIdList);
        List<TeamMemberInfoForLeaveSummaryDto> LoadTeamMemberForLeaveSummary(int start, int length, string orderBy, string orderDir, DateTime searchingEffectiveDate, List<long> branchIdList, List<long> campusIdList = null, List<long> departmntIdList = null, List<long> designationIdList = null, List<int> pinList = null);

        #endregion

        #region Others Function
        int CountTeamMemberDirectory(List<long> organizationIdList, List<long> branchIdList, List<long> campusIdList, long? departmentId, string name, int? pin, int? bloodGroup);
        long CountTeamMemberListReport(List<long> organizationIdList, long? departmentId, List<long> branchIdList, List<long> campusIdList, int? memberTypes, int? memberStatus);
        long CountTeammemberSearchListReport(List<long> organizationIdList, List<long> branchIdList, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList);
        int GetCountSearchTeammemberList(List<long> authoBrnachIdList, List<long> departmentIdList, List<long> campusIdList, List<int> memberTypeList, List<int> memberStatuList, string keyword, List<string> informationViewList, List<int> employmentStatusIdList, List<int> genderIdList, List<int> religionIdList, List<int> bloodGroupIdList, List<int> maritalStatusIdList);
        bool HasTinNumberAlready(string tinNo, long id = 0);
        int GetTeamMemberForLeaveSummaryCount(DateTime searchingEffectiveDate, List<long> branchIdList, List<long> campusIdList = null, List<long> departmntIdList = null, List<long> designationIdList = null, List<int> pinList = null);

        #endregion

        #region Helper Function
        bool IsDuplicatePersonalContact(string personalContact, long? id);
        void GetCurrentInfos(long hrMemberId, out object campus, out string department, out string designation, out int empStatus,
            out string joindate, out string todate, out string officeShift, out int weekend, out int mentorPin);
        int GetNewPin();
        #endregion
        
    }
    public class TeamMemberDao : BaseDao<TeamMember, long>, ITeamMemberDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public TeamMember GetMember(int? pin, DateTime? joiningDate = null)
        {
            ICriteria criteria = Session.CreateCriteria<TeamMember>().Add(Restrictions.Eq("Status", TeamMember.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Pin", pin));
            if (joiningDate != null)
            {
                DetachedCriteria teamMemberEmployementHistory = DetachedCriteria.For<EmploymentHistory>().SetProjection(Projections.Distinct(Projections.Property("TeamMember.Id")));
                teamMemberEmployementHistory.CreateAlias("TeamMember", "tem").Add(Restrictions.Eq("tem.Pin", pin));
                teamMemberEmployementHistory.Add(Restrictions.Le("EffectiveDate", joiningDate));
                criteria.Add(Subqueries.PropertyIn("Id", teamMemberEmployementHistory));
            }
            return criteria.SetMaxResults(1).UniqueResult<TeamMember>();
        }

        public TeamMember GetTeamMemberByProfileId(long profileId, long teamMemberId)
        {
            var memeber = Session.QueryOver<TeamMember>().Where(x => x.UserProfile.Id == profileId && x.Id != teamMemberId).SingleOrDefault<TeamMember>();
            return memeber;
        }

        public TeamMember LoadByUserProfile(UserProfile user)
        {
            return Session.QueryOver<TeamMember>().Where(x => x.UserProfile.Id == user.Id).Take(1).SingleOrDefault<TeamMember>();
        }
        //public TeamMember GetTeamMemberByAspNetUserId(long aspNetUserId)
        //{
        //    return Session.QueryOver<TeamMember>().Where(x => x.UserProfile != null && x.UserProfile.AspNetUser.Id == aspNetUserId).Take(1).SingleOrDefault<TeamMember>();
        //}
        public Organization GetTeamMemberSalaryOrganization(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = GetTeamMemberSalarySearchingDateCriteria(searchingDate, teamMemberId, teamMemberPin);
            criteria.CreateAlias("SalaryDepartment", "dep").Add(Restrictions.Eq("dep.Status", Department.EntityStatus.Active));
            criteria.CreateAlias("dep.Organization", "org").Add(Restrictions.Eq("org.Status", Organization.EntityStatus.Active));
            criteria.SetProjection(Projections.Property("dep.Organization"));
            Organization organization = criteria.UniqueResult<Organization>();

            return organization;
        }

        public Organization GetTeamMemberOrganization(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = GetTeamMemberSearchingDateCriteria(searchingDate, teamMemberId, teamMemberPin);
            criteria.CreateAlias("Department", "dep").Add(Restrictions.Eq("dep.Status", Department.EntityStatus.Active));
            criteria.CreateAlias("dep.Organization", "org").Add(Restrictions.Eq("org.Status", Organization.EntityStatus.Active));
            criteria.SetProjection(Projections.Property("dep.Organization"));
            Organization organization = criteria.UniqueResult<Organization>();

            return organization;
        }

        public Branch GetTeamMemberSalaryBranch(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = GetTeamMemberSalarySearchingDateCriteria(searchingDate, teamMemberId, teamMemberPin);
            criteria.CreateAlias("SalaryCampus", "cm").Add(Restrictions.Eq("cm.Status", Campus.EntityStatus.Active));
            criteria.CreateAlias("cm.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            criteria.SetProjection(Projections.Property("cm.Branch"));
            Branch branch = criteria.UniqueResult<Branch>();

            return branch;
        }

        public Branch GetTeamMemberBranch(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = GetTeamMemberSearchingDateCriteria(searchingDate, teamMemberId, teamMemberPin);

            criteria.CreateAlias("Campus", "cm").Add(Restrictions.Eq("cm.Status", Campus.EntityStatus.Active));
            criteria.CreateAlias("cm.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            criteria.SetProjection(Projections.Property("cm.Branch"));
            Branch branch = criteria.UniqueResult<Branch>();

            return branch;
        }

        public Campus GetTeamMemberSalaryCampus(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = GetTeamMemberSalarySearchingDateCriteria(searchingDate, teamMemberId, teamMemberPin);

            criteria.CreateAlias("SalaryCampus", "cm").Add(Restrictions.Eq("cm.Status", Campus.EntityStatus.Active));
            criteria.SetProjection(Projections.Property("SalaryCampus"));
            Campus campus = criteria.UniqueResult<Campus>();

            return campus;
        }

        public Campus GetTeamMemberCampus(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = GetTeamMemberSearchingDateCriteria(searchingDate, teamMemberId, teamMemberPin);

            criteria.CreateAlias("Campus", "cm").Add(Restrictions.Eq("cm.Status", Campus.EntityStatus.Active));
            criteria.SetProjection(Projections.Property("Campus"));
            Campus campus = criteria.UniqueResult<Campus>();

            return campus;
        }

        public Department GetTeamMemberSalaryDepartment(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = GetTeamMemberSalarySearchingDateCriteria(searchingDate, teamMemberId, teamMemberPin);

            criteria.CreateAlias("SalaryDepartment", "dep").Add(Restrictions.Eq("dep.Status", Department.EntityStatus.Active));
            criteria.SetProjection(Projections.Property("SalaryDepartment"));
            Department department = criteria.UniqueResult<Department>();

            return department;
        }

        public Department GetTeamMemberDepartment(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = GetTeamMemberSearchingDateCriteria(searchingDate, teamMemberId, teamMemberPin);

            criteria.CreateAlias("Department", "dep").Add(Restrictions.Eq("dep.Status", Department.EntityStatus.Active));
            criteria.SetProjection(Projections.Property("Department"));
            Department department = criteria.UniqueResult<Department>();

            return department;
        }

        public Designation GetTeamMemberDesignation(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = GetTeamMemberSearchingDateCriteria(searchingDate, teamMemberId, teamMemberPin);

            criteria.CreateAlias("Designation", "des").Add(Restrictions.Eq("des.Status", Designation.EntityStatus.Active));
            criteria.SetProjection(Projections.Property("Designation"));
            Designation designation = criteria.UniqueResult<Designation>();

            return designation;
        }

        public TeamMemberTransferViewModel GetTeamMemberTransferInfo(long teamMemberId)
        {

            string query = "";
            query = @"DECLARE @TeamMemberId as int;
                    set @TeamMemberId = " + teamMemberId + @";
                    Select 
                    tm.Id
                    , tm.Pin
                    , tm.Name
                    , tm.PersonalContact as PersonalContact
                    , emh.*
                    , emhJoiningDate.JoiningDate as JoiningDate
                    , swh.OfficeShiftName as OfficeShiftName
                    , swh.ShiftWeekendId as OfficeShiftId
                    , swh.Weekend as Weekend
                    , case when swh.Weekend = 1 then 'Saturday' when swh.Weekend = 2 then 'Sunday'when swh.Weekend = 3 then 'Monday'when swh.Weekend = 4 then 'Tuesday'when swh.Weekend = 5 then 'Wednesday' when swh.Weekend = 6 then 'Thursday' when swh.Weekend = 7 then 'Friday' end AS WeekendName
                    , mentorHistory.MentorPin
                    , mentorHistory.MentorId
                    , mentorHistory.MentorName
                    , offical.OfficialContact 
                    , offical.OfficialEmail
                    , offical.CardNo AS CardNumber
                    , offical.Id AS OfficalDetailsId
                    , offical.[Status] AS OfficalDetailsStatus
                    from HR_TeamMember as tm
                    inner join 
	                    (Select top 1 
			                    CONVERT(VARCHAR(19),eh.EffectiveDate,103) as DateTo
			                    , eh.TeamMemberId
			                    , o.ShortName as OrganizationName
			                    , o.Id AS OrganizationId
			                    , b.Name as BranchName
			                    , b.Id as BranchId
			                    , c.Name as CampusName
			                    , c.Id as CampusId
			                    , d.Name as DepartmentName
			                    , d.Id as DepartmentId
			                    , desig.Name as DesignationName
			                    , desig.Id as DesignationId
			                    , eh.EmploymentStatus as EmploymentStatusId
			                    , CASE when eh.EmploymentStatus = 1 then 'Probation' when eh.EmploymentStatus = 2 then 'Permanent' when eh.EmploymentStatus = 3 then 'PartTime' when eh.EmploymentStatus = 4 then 'Contractual' when eh.EmploymentStatus = 5 then 'Intern' when eh.EmploymentStatus = 6 then 'Retired' End  AS EmploymentStatus
		                    from  HR_EmploymentHistory as eh
		                    inner join Campus as c on c.Id = eh.CampusId and c.Status = 1
		                    inner join Branch as b on b.Id = c.BranchId and b.Status = 1
		                    inner join HR_Department as d on d.Id = eh.DepartmentId and d.Status = 1
		                    inner join HR_Designation as desig on desig.Id = eh.DesignationId and desig.Status = 1
		                    inner join Organization as o on o.Id = d.OrganizationId and o.Status = 1
		                    where 1=1
		                    and eh.Status = 1 
		                    and eh.EffectiveDate <= GETDATE()
		                    and eh.TeamMemberId = @TeamMemberId
		                    order by eh.EffectiveDate DESC, eh.Id DESC
	                    ) as emh on emh.TeamMemberId = tm.Id
                    inner join 
	                    (Select top 1 CONVERT(VARCHAR(19),EffectiveDate,103) as JoiningDate, TeamMemberId from  HR_EmploymentHistory 
		                    where 1=1
		                    and Status = 1 
		                    and TeamMemberId = @TeamMemberId
		                    order by EffectiveDate ASC, Id ASC
	                    ) as emhJoiningDate on emhJoiningDate.TeamMemberId = tm.Id
                    inner join 
	                    (Select top 1 mh.TeamMemberId
	                    , tm.pin As MentorPin
	                    , tm.Id As MentorId
	                    , tm.Name As MentorName
	                    from  HR_MentorHistory mh
	                    inner join HR_TeamMember as tm on tm.id = mh.MentorId and tm.Status = 1
		                    where 1=1
		                    and mh.Status = 1 
		                    and mh.TeamMemberId = @TeamMemberId
		                    and mh.EffectiveDate <= GETDATE()
		                    order by mh.EffectiveDate DESC, mh.Id DESC
	                    ) as mentorHistory on mentorHistory.TeamMemberId = tm.Id
                    inner join 
	                    (Select top 1 swh.Id as ShiftWeekendId
		                    , swh.TeamMemberId 
		                    , s.Name as OfficeShiftName 
		                    , swh.Weekend as Weekend 
		                    from  HR_ShiftWeekendHistory as swh
                            inner join HR_Shift as s on s.Id = swh.ShiftId and s.Status = 1
		                    where 1=1
		                    and swh.Status = 1 
		                    and swh.TeamMemberId = @TeamMemberId
		                    and swh.EffectiveDate <= GETDATE()
		                    order by swh.EffectiveDate DESC, swh.Id DESC
	                    ) as swh on swh.TeamMemberId = tm.Id
                    left join 
	                    (Select top 1 OfficialContact
	                    , OfficialEmail
	                    , CardNo
	                    , TeamMemberId
	                    , Id
	                    , [Status]
	                    from  HR_MemberOfficialDetail
		                    where 1=1
		                    and Status != -404 
		                    and TeamMemberId = @TeamMemberId
		                    order by Id DESC
	                    ) as offical on offical.TeamMemberId = tm.Id
                    where 1=1
                    and tm.Status = 1
                    and tm.Id = @TeamMemberId";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberTransferViewModel>());
            TeamMemberTransferViewModel teamMemberTransferInfo = iQuery.UniqueResult<TeamMemberTransferViewModel>();
            return teamMemberTransferInfo;
        }

        #endregion

        #region List Loading Function

        public IList<TeamMember> GetPinByAutoComplete(string query, TeamMember member, string date)
        {
            ICriteria criteria = Session.CreateCriteria<TeamMember>().Add(Restrictions.Eq("Status", TeamMember.EntityStatus.Active));
            criteria.CreateAlias("MentorHistory", "mh")
                .Add(Restrictions.Eq("mh.Status", MentorHistory.EntityStatus.Active));
            criteria.Add(Expression.Like(Projections.Cast(NHibernateUtil.String, Projections.Property("Pin")), query, MatchMode.Anywhere));
            //criteria.Add(Restrictions.Eq("mh.Mentor.Id", Convert.ToInt64(1)));
            criteria.Add(Restrictions.Eq("mh.Mentor.Id", member.Id));
            criteria.Add(Restrictions.Le("mh.EffectiveDate", Convert.ToDateTime(date)));
            var result = criteria.List<TeamMember>().ToList();
            return result;
        }

        public IList<TeamMember> LoadHrAuthorizedTeamMember(DateTime? searchingDate = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, List<int> pinList = null, bool withRetired = true, int searchTeamMemberOn = 1)
        {
            string query = "";
            query = GetHrAuthorizedTeamMemberQuery(searchingDate, branchIdList, campusIdList, departmentIdList, designationIdList, pinList, true, withRetired, searchTeamMemberOn);
            IQuery iQuery = Session.CreateSQLQuery(query).AddEntity("Hrm", typeof(TeamMember));

            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberQueryTransfer>());
            var res = iQuery.List<TeamMemberQueryTransfer>().ToList();
            return res.Select(teamMemberDto => teamMemberDto.Hrm).ToList();
        }


        private string GetHrAuthorizedTeamMemberQuery(DateTime? searchingDate = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, List<int> pinList = null, bool isObject = true, bool withRetired = true, int searchTeamMemberOn = 1)
        {
            string query = "";
            string filterQuery = "";
            string filterQuery2 = "";
            string filterQuery3 = "";
            string filterRetired = "";
            string searchOnQueryString = "";
            string designationLeftJoing = "";
            if (searchingDate != null)
            {
                filterQuery += @" AND EffectiveDate <= '" + searchingDate + "' ";
            }
            if (branchIdList != null && !branchIdList.Contains(0))
            {
                filterQuery2 += @" AND br.Id IN( " + string.Join(",", branchIdList) + ") ";
            }
            if (pinList != null && !pinList.Contains(0))
            {
                filterQuery2 += @" AND hrm.Pin IN( " + string.Join(",", pinList) + ") ";
            }
            if (campusIdList != null && !campusIdList.Contains(0))
            {
                filterQuery3 = @" AND CampusId IN ( " + string.Join(",", campusIdList) + ") ";
            }
            if (departmentIdList != null && !departmentIdList.Contains(0))
            {
                filterQuery3 += @" AND DepartmentId IN (" + string.Join(",", departmentIdList) + ") ";
            }
            if (designationIdList != null && !designationIdList.Contains(0) && searchTeamMemberOn == 1)
            {
                filterQuery3 += @" AND DesignationId IN ( " + string.Join(",", designationIdList) + " )  ";
            }
            string selectQuery = " hrm.Id AS TeamMemberId";
            if (isObject)
            {
                selectQuery = " {Hrm.*}";
            }
            if (withRetired == false && searchTeamMemberOn == 1)
            {
                filterRetired = " AND A.EmploymentStatus <> " + ((int)MemberEmploymentStatus.Retired).ToString() + " ";
            }
            if (searchTeamMemberOn == 1) //Employment Histroy
            {
                searchOnQueryString = @"Left Join (
	                                        Select * from (
		                                        Select [CampusId]
			                                        ,[DesignationId]
			                                        ,[DepartmentId]
			                                        ,[TeamMemberId]
			                                        ,[EffectiveDate]
			                                        ,[EmploymentStatus]
			                                        ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC, Id DESC) AS R  
			                                        from [dbo].[HR_EmploymentHistory] 
			                                        Where [Status] = 1
			                                        " + filterQuery + @"
	                                        ) AS A Where A.R = 1 " + filterRetired + filterQuery3 + @"
                                        ) AS currentPosition ON currentPosition.TeamMemberId  = hrm.Id"
                      ;
                designationLeftJoing = "Left Join [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  1";
            }
            else if (searchTeamMemberOn == 2) //Job History
            {
                searchOnQueryString = @"Left Join (
	                                        Select * from (
		                                        Select SalaryCampusId AS [CampusId]
			                                        ,SalaryDepartmentId AS[DepartmentId]
			                                        ,TeamMemberId AS[TeamMemberId]
			                                        ,EffectiveDate AS [EffectiveDate]
			                                        ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC, Id DESC) AS R  
			                                        from [dbo].[HR_SalaryHistory] 
			                                        Where [Status] = 1
			                                        " + filterQuery + @"
	                                        ) AS A Where A.R = 1 " + filterRetired + filterQuery3 + @"
                                        ) AS currentPosition ON currentPosition.TeamMemberId  = hrm.Id"
                      ;

            }
            query += @"select " + selectQuery + @" from 
                     [dbo].[HR_TeamMember] AS hrm 
                     " + searchOnQueryString + @" 
                    Left Join [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status = 1
                    Left Join [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status =  1
                    Left Join [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  1
                    Left Join [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  1
                     " + designationLeftJoing + @" 
                    Where hrm.Status = 1
                    " + filterQuery2 + @"
                    ";

            return query;
        }

        public List<long> LoadHrAuthorizedTeamMemberId(DateTime? searchingDate = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, List<int> pinList = null, bool withRetired = true, int searchTeamMemberOn = 1)
        {
            string query = "";
            query = GetHrAuthorizedTeamMemberQuery(searchingDate, branchIdList, campusIdList, departmentIdList, designationIdList, pinList, false, withRetired, searchTeamMemberOn);
            IQuery iQuery = Session.CreateSQLQuery(query);
            return iQuery.List<long>().ToList();
        }


        public IList<TeamMember> LoadMemberByUserList(List<long> userList)
        {
            ICriteria criteria = Session.CreateCriteria<TeamMember>().Add(Restrictions.Eq("Status", TeamMember.EntityStatus.Active));
            criteria.Add(Restrictions.In("UserProfile.Id", userList));
            var result = criteria.List<TeamMember>().ToList();
            return result;
        }
        public IList<TeamMember> LoadTeamMemberDirectory(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> branchIdList, List<long> campusIdList, long? departmentId, string name, int? pin, int? bloodGroup)
        {
            string query = GetTeamMemberDirectorySqlQuery(organizationIdList, branchIdList, campusIdList, departmentId, name, pin, bloodGroup, false);
            //Order by PIN
            //query = @"Select * from (" + query + ")x order by x.Pin asc";
            query = @"Select * from (" + query + ")x order by x." + orderBy + " " + orderDir + " ";
            IQuery iQuery = Session.CreateSQLQuery(query).AddEntity("Hrm", typeof(TeamMember));

            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberQueryTransfer>());
            if (length > 0)
            {
                iQuery.SetFirstResult(start);
                iQuery.SetMaxResults(length);
            }
            var res = iQuery.List<TeamMemberQueryTransfer>().ToList();
            return res.Select(teamMemberDto => teamMemberDto.Hrm).ToList();
        }

        public IList<MemberSearchListDto> LoadTeamMemberListReport(int draw, int start, string orderBy, string orderDir, int length, List<long> organizationIdList, long? departmentId, List<long> branchIdList, List<long> campusIdList, int? memberTypes, int? memberStatus)
        {
            string query = GetTeamMemberListSqlQueryNew(organizationIdList, departmentId, branchIdList, campusIdList, memberTypes, memberStatus, false);
            //if (orderDir == "desc")
            //{
            query += " order by b.Pin ASC ";
            //}
            if (length > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }
            query += " IF EXISTS(Select * from #aaa) drop table #aaa";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<MemberSearchListDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<MemberSearchListDto>().ToList();
            return list;
        }

        public IList<MemberSearchListDto> LoadTeamMemberSearchListReport(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> branchIdList, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList)
        {
            string query = GetTeamMemberSearchListSqlQueryNew(organizationIdList, branchIdList, departmentId, memberTypes, memberStatus, keyword, informationViewList, false);
            //if (orderDir == "desc")
            //{
            query += " order by b.Pin ASC ";
            //}
            if (length > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }
            query += " IF EXISTS(Select * from #aaa) drop table #aaa";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<MemberSearchListDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<MemberSearchListDto>().ToList();
            return list;
        }

        public IList<MemberSearchListDto> LoadSearchTeammemberList(int start, int length, string orderBy, string orderDir, List<long> authoBrnachIdList, List<long> departmentIdList, List<long> campusIdList, List<int> memberTypeList, List<int> memberStatuList, string keyword, List<string> informationViewList, List<int> employmentStatusIdList, List<int> genderIdList, List<int> religionIdList, List<int> bloodGroupIdList, List<int> maritalStatusIdList)
        {
            string query = GetSearchTeamMemberListQuery(authoBrnachIdList, departmentIdList, campusIdList, memberTypeList, memberStatuList, keyword, informationViewList, employmentStatusIdList, genderIdList, religionIdList, bloodGroupIdList, maritalStatusIdList);
            string listQuery = "Select * from (" + query + " ) as aa ";
            if (!String.IsNullOrEmpty(orderBy))
                listQuery += " order by " + orderBy + " " + orderDir;

            else listQuery += " order by aa.Pin ASC ";
            if (length > 0)
            {
                listQuery += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }

            IQuery iQuery = Session.CreateSQLQuery(listQuery);
            //query += " order by tm.Pin ASC ";
            //if (length > 0)
            //{
            //    query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            //}

            //IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<MemberSearchListDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<MemberSearchListDto>().ToList();
            return list;
        }

        public IList<TeamMemberBatchShiftDto> LoadTeamMemberBatchShiftDto(List<long> branchIdList, List<long> campusIdList, List<int> employmentStatusList, List<long> departmntIdList, List<long> designationIdList, List<long> shiftIdList, DateTime? searchingEffectiveDate)
        {
            string query = GetQueryForTeamMemberWithCurrentNextShiftInfor(branchIdList, campusIdList, employmentStatusList, departmntIdList, designationIdList, shiftIdList, searchingEffectiveDate);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberBatchShiftDto>());
            List<TeamMemberBatchShiftDto> list = iQuery.List<TeamMemberBatchShiftDto>().ToList();
            return list;
        }

        public IList<TeamMemberBatchSalaryHistoryDto> LoadTeamMemberBatchSalaryHistoryDto(List<long> branchIdList, List<long> campusIdList, List<int> employmentStatusList, List<long> departmntIdList, List<long> designationIdList, bool isSearchOnEmploymetHistory)
        {
            string query = GetQueryForTeamMemberWithLastNextSalaryHistoryInfor(branchIdList, campusIdList, employmentStatusList, departmntIdList, designationIdList, isSearchOnEmploymetHistory);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberBatchSalaryHistoryDto>());
            List<TeamMemberBatchSalaryHistoryDto> list = iQuery.List<TeamMemberBatchSalaryHistoryDto>().ToList();
            return list;
        }

        public List<FestivalBonusSheetDto> LoadTeamMemberListForFbSheet(DateTime startDate, DateTime endDate, List<int> religionList, FestivalBonusSheetFormViewModel viewModel,
            List<long> authOrganizationIdList, List<long> authBranchIdList)
        {

            var query = @"Select hrm.Id as TeamMemberId,hrm.Pin as Pin,hrm.Name as Name,designation.Name as Designation,
                CASE 
                    WHEN cp.EmploymentStatus =  1 THEN 'Probation'
                    WHEN cp.EmploymentStatus =  2 THEN 'Permanent'
                    WHEN cp.EmploymentStatus =  3 THEN 'PartTime'
                    WHEN cp.EmploymentStatus =  4 THEN 'Contractual'
                    ELSE 'Intern'
                END as EmploymentStatus
                --,fb.BonusPercentage as BonusPercentage,fb.CalculationOn as CalculationOn ,sh.BankAmount as BankAmount,sh.CashAmount as CashAmount,sh.Salary as Salary
                ,sh.SalaryCampusId as SalaryCampusId,sh.SalaryDepartmentId as SalaryDepartmentId,sh.SalaryOrganizationId as SalaryOrganizationId,sh.SalaryBranchId
                ,sh.SalaryDesignationId
                ,ISNULL(CASE 
                WHEN fb.CalculationOn = 1 THEN cast(((BankAmount * organization.BasicSalary/100)*(fb.BonusPercentage/100)) as decimal(10,2))
	            WHEN fb.CalculationOn = 2 THEN cast((BankAmount*(fb.BonusPercentage/100)) as decimal(10,2))
	            ELSE 0
	            END,0) as BankAmount
	            ,ISNULL(CASE 
                WHEN fb.CalculationOn = 1 THEN cast(((CashAmount * organization.BasicSalary/100)*(fb.BonusPercentage/100)) as decimal(10,2))
	            WHEN fb.CalculationOn = 2 THEN cast((CashAmount*(fb.BonusPercentage/100)) as decimal(10,2))
	            ELSE 0
	            END,0) as CashAmount
                ,ISNULL(CASE 
                WHEN fb.CalculationOn = 1 THEN cast(((BankAmount * organization.BasicSalary/100)*(fb.BonusPercentage/100))+((CashAmount * organization.BasicSalary/100)*(fb.BonusPercentage/100)) as decimal(10,2))
	            WHEN fb.CalculationOn = 2 THEN cast((Salary*(fb.BonusPercentage/100)) as decimal(10,2))
	            ELSE 0
	            END,0) as TotalAmount
	            ,organization.Id as JobOrganizationId
	            ,branch.Id as JobBranchId
	            ,campus.Id as JobCampusId
	            ,designation.Id as JobDesignationId
	            ,cp.DepartmentId as JobDepartmentId
	            ,fb.Id as FestivalBonusSettingId
                ,fbs.Remarks
            from  [HR_TeamMember] AS hrm 
                Left Join ( ";

            if (viewModel.OrganizationType == OrganizationType.JobOrganization)
            {
                query += @"select 
								a. [CampusId]
								,a.[DesignationId]
								,a.[DepartmentId]
								,a.[TeamMemberId]
								,a.[EffectiveDate]
								,a.[EmploymentStatus]
								from (
									Select * from (
										Select [CampusId]
											,[DesignationId]
											,[DepartmentId]
											,[TeamMemberId]
											,[EffectiveDate]
											,[EmploymentStatus]
											,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC, Id DESC) AS R  
											from [HR_EmploymentHistory] 
											Where Status = 1
											and EffectiveDate <= '" + endDate.Date.ToString("yyyy-MM-dd 00:00:00.000") + @"'
									) as a where a.R=1  and a.EmploymentStatus != 6 
								) as a 
								inner join Campus as c on c.Id = a.CampusId  and c.Status = 1
								inner join Branch as b on b.Id = c.BranchId and b.Status = 1 and b.Id IN(" + string.Join(",", authBranchIdList) + @") 
								inner join Organization as o on o.Id = b.OrganizationId and o.Status = 1 and o.Id IN(" + string.Join(",", authOrganizationIdList) + @") 
								inner join [HR_Designation] as designation on designation.Id = a.DesignationId and designation.Status = 1 
                                inner join [HR_Department] as department on department.Id = a.DepartmentId and department.Status = 1
                                where b.Id in(" + string.Join(",", authBranchIdList) + @") and o.Id in(" + string.Join(",", authOrganizationIdList) + @")";

                    if (viewModel.CampusId != 0)
                    {
                        query += @" and c.Id =" + viewModel.CampusId + "";
                    }
                    if (viewModel.DepartmentId != 0)
                    {
                        query += @" and department.Id=" + viewModel.DepartmentId + "";
                    }
            }
            else
            {
                query += @" select sh.*
					--,eh.Id
					,eh.DesignationId as DesignationId
					,eh.EmploymentStatus as EmploymentStatus
					,eh.CampusId as CampusId
					,eh.DepartmentId as DepartmentId 
					  from (
							Select
							     sh.[Id]
							    ,sh.[SalaryOrganizationId]
							    ,sh.[SalaryCampusId]
								,sh.[SalaryDepartmentId]
								,sh.[SalaryDesignationId]
								,sh.[TeamMemberId]
								,sh.[EffectiveDate]
								--,[EmploymentStatus]
								,RANK() OVER(PARTITION BY sh.TeamMemberId ORDER BY sh.EffectiveDate DESC, sh.Id DESC) AS R  
								from [HR_SalaryHistory] sh
								Where sh.Status = 1
								and sh.EffectiveDate <= '" + endDate.Date.ToString("yyyy-MM-dd 00:00:00.000") + "'";
                query += @"
						) as sh 
						left join 
						(
							Select * from (
								Select
								     [Id] 
								    ,[CampusId]
									,[DesignationId]
									,[DepartmentId]
									,[TeamMemberId]
									,[EffectiveDate]
									,[EmploymentStatus]
									,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC, Id DESC) AS R  
									from [HR_EmploymentHistory] 
									Where Status = 1
									and EffectiveDate <= '" + endDate.Date.ToString("yyyy-MM-dd 00:00:00.000") + @"' ) as a 
									where a.R=1  and a.EmploymentStatus != 6  
						) as eh on eh.TeamMemberId = sh.TeamMemberId
						inner join Campus as c on c.Id = sh.SalaryCampusId  and c.Status = 1
						inner join Branch as b on b.Id = c.BranchId and b.Status = 1 
                        and b.Id IN(" + string.Join(",", authBranchIdList) + @")  
						where sh.R=1  
						and eh.Id is not null
						and sh.SalaryOrganizationId IN(" + string.Join(",", authOrganizationIdList) + ")";
                if (viewModel.CampusId != 0)
                {
                    query += @" and sh.SalaryCampusId =" + viewModel.CampusId + "";
                }
                if (viewModel.DepartmentId != 0)
                {
                    query += @" and sh.SalaryDepartmentId=" + viewModel.DepartmentId + "";
                }
					query+=@"--and sh.SalaryDepartmentId = 1
						--and sh.SalaryCampusId IN() 
                ";

            }
            
            query += @" ) as cp on cp.TeamMemberId = hrm.Id and hrm.Status = " + TeamMember.EntityStatus.Active + "";
            query += @" left join [Campus] as campus on campus.Id = cp.CampusId and campus.Status = " + Campus.EntityStatus.Active + "";
            query += @" left join [Branch] as branch on branch.Id = campus.BranchId and branch.Status = " + Branch.EntityStatus.Active + "";
            query += @" left join [Organization] as organization on organization.Id = branch.OrganizationId and organization.Status = " + Organization.EntityStatus.Active + "";
            query += @" left join [HR_Designation] as designation on designation.Id = cp.DesignationId and designation.Status = " + Designation.EntityStatus.Active + "";
            query += @" left join (
                select * from (
	                select fbs.Id,fbsc.BonusPercentage,fbsc.CalculationOn,fbsc.EmploymentStatus from HR_FestivalBonusSetting as fbs
		                inner join HR_FestivalBonusSettingCalculation as fbsc 
		                on fbs.Id = fbsc.FestivalBonusSettingId and fbsc.Status = " + FestivalBonusSetting.EntityStatus.Active + " ";
            query += @" where fbs.Id = " + viewModel.FestivalBonusSettingId + " and fbs.OrganizationId = " + viewModel.OrganizationId + " ";
            query += @" ) as a 
                ) as fb on fb.EmploymentStatus = cp.EmploymentStatus
                left Join (
		            select * from (
			            Select sh.[Id]
                            ,[salaryOrganizationId]
			                ,b.Id as SalaryBranchId
				            ,[SalaryCampusId]
				            ,[SalaryDepartmentId]
                            ,[SalaryDesignationId]
				            ,[TeamMemberId]
				            ,[EffectiveDate]
				            ,[Salary]
				            ,[BankAmount]
				            ,[CashAmount]
				            ,RANK() OVER(PARTITION BY sh.TeamMemberId ORDER BY EffectiveDate DESC, sh.Id DESC) AS R  
				            from [HR_SalaryHistory] as sh
				            inner join Campus as cam on cam.Id = sh.SalaryCampusId and cam.Status=" + Campus.EntityStatus.Active + "";
            query += @"     inner join Branch as b on b.Id = cam.BranchId and b.Status = " + Branch.EntityStatus.Active + "";

            query += @" Where sh.Status = " + SalaryHistory.EntityStatus.Active + "";

            query += @" and EffectiveDate <= '" + endDate.Date.ToString("yyyy-MM-dd 00:00:00.000") + "'";

            query += @" ) as a where a.R = 1
	            ) as sh on sh.TeamMemberId = hrm.Id and hrm.Status=" + TeamMember.EntityStatus.Active + "";

            query += @"  left join(
	                select [TeamMemberId]
	                       ,[Remarks] from [dbo].[HR_FestivalBonusSheet] 
                    where Status=" + FestivalBonusSheet.EntityStatus.Active + " and (StartDate>='" + startDate + "' and EndDate<='" + endDate + "') and FestivalBonusSettingId=" + viewModel.FestivalBonusSettingId + "";
            query += @"  ) as fbs on fbs.TeamMemberId=hrm.Id ";

            query += @" where cp.TeamMemberId is not null and hrm.Religion IN(" + string.Join(",", religionList) + ") and sh.Id is not null";
            query += @" order by cp.TeamMemberId ASC";
            var list = Session.CreateSQLQuery(query).SetResultTransformer(Transformers.AliasToBean<FestivalBonusSheetDto>());
            return list.List<FestivalBonusSheetDto>().ToList();
        }
        
        public List<TeamMemberInfoForLeaveSummaryDto> LoadTeamMemberForLeaveSummary(int start, int length, string orderBy, string orderDir, DateTime searchingEffectiveDate, List<long> branchIdList, List<long> campusIdList = null, List<long> departmntIdList = null, List<long> designationIdList = null, List<int> pinList = null)
        {
            string query = GetTeamMemberForLeaveSummaryQuery(searchingEffectiveDate, branchIdList, campusIdList, departmntIdList, designationIdList, pinList);
            string listQuery = "Select * from (" + query + " ) as aa ";
            if (!String.IsNullOrEmpty(orderBy))
                listQuery += " order by " + orderBy + " " + orderDir;

            else listQuery += " order by aa.Pin ASC ";
            if (length > 0)
            {
                listQuery += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }

            IQuery iQuery = Session.CreateSQLQuery(listQuery);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberInfoForLeaveSummaryDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<TeamMemberInfoForLeaveSummaryDto>().ToList();
            return list;
        }

        #endregion

        #region Others Function
        //public int CountTeamMemberDirectory(long? organizationId, long? branchId, long? campusId, long? departmentId, string name,int? pin)
        //public int CountTeamMemberDirectory(List<long> organizationIdList, List<long> branchIdList, List<long> campusIdList, long? departmentId, string name, int? pin)
        //{
        //    string query = GetTeamMemberDirectorySqlQuery(organizationIdList, branchIdList, campusIdList, departmentId, name, pin);
        //    IQuery iQuery = Session.CreateSQLQuery(query);
        //    iQuery.SetTimeout(2700);
        //    var count = iQuery.UniqueResult<int>();
        //    return count;
        //}
        public int CountTeamMemberDirectory(List<long> organizationIdList, List<long> branchIdList, List<long> campusIdList, long? departmentId, string name, int? pin, int? bloodGroup)
        {
            string query = GetTeamMemberDirectorySqlQuery(organizationIdList, branchIdList, campusIdList, departmentId, name, pin, bloodGroup);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public long CountTeamMemberListReport(List<long> organizationIdList, long? departmentId, List<long> branchIdList, List<long> campusIdList, int? memberTypes, int? memberStatus)
        {
            string query = GetTeamMemberListSqlQueryNew(organizationIdList, departmentId, branchIdList, campusIdList, memberTypes, memberStatus);
            //query += " order by b.Pin ASC ";
            query += " IF EXISTS(Select * from #aaa) drop table #aaa";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public long CountTeammemberSearchListReport(List<long> organizationIdList, List<long> branchIdList, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList)
        {
            string query = GetTeamMemberSearchListSqlQueryNew(organizationIdList, branchIdList, departmentId, memberTypes, memberStatus, keyword, informationViewList);
            query += " IF EXISTS(Select * from #aaa) drop table #aaa";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int GetCountSearchTeammemberList(List<long> authoBrnachIdList, List<long> departmentIdList, List<long> campusIdList, List<int> memberTypeList, List<int> memberStatuList, string keyword, List<string> informationViewList, List<int> employmentStatusIdList, List<int> genderIdList, List<int> religionIdList, List<int> bloodGroupIdList, List<int> maritalStatusIdList)
        {
            string query = GetSearchTeamMemberListQuery(authoBrnachIdList, departmentIdList, campusIdList, memberTypeList, memberStatuList, keyword, informationViewList, employmentStatusIdList, genderIdList, religionIdList, bloodGroupIdList, maritalStatusIdList);
            string countQuery = "Select count(*) From ( " + query + " ) as a ";
            IQuery iQuery = Session.CreateSQLQuery(countQuery);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public bool HasTinNumberAlready(string tinNo, long id = 0)
        {
            if (!String.IsNullOrEmpty(tinNo))
            {
                tinNo = tinNo.Trim();
            }
            int rowCount = Session.QueryOver<TeamMember>().Where(x => (x.TinNo != null || x.TinNo != "") && x.TinNo == tinNo && x.Id != id).RowCount();
            if (rowCount > 0)
                return true;

            return false;
        }

        public int GetTeamMemberForLeaveSummaryCount(DateTime searchingEffectiveDate, List<long> branchIdList, List<long> campusIdList = null, List<long> departmntIdList = null, List<long> designationIdList = null, List<int> pinList = null)
        {
            string query = GetTeamMemberForLeaveSummaryQuery(searchingEffectiveDate, branchIdList, campusIdList, departmntIdList, designationIdList, pinList);
            string countQuery = "Select count(*) From ( " + query + " ) as a ";
            IQuery iQuery = Session.CreateSQLQuery(countQuery);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function

        private string GetSearchTeamMemberListQuery(List<long> authoBrnachIdList, List<long> departmentIdList, List<long> campusIdList, List<int> memberTypeList, List<int> memberStatuList, string keyword, List<string> informationViewList, List<int> employmentStatusIdList, List<int> genderIdList, List<int> religionIdList, List<int> bloodGroupIdList, List<int> maritalStatusIdList)
        {
            string branchFilter = "";
            string departmentFilter = "";
            string campusFilter = "";
            string memberTypeFilter = "";
            string memberStatusFilter = "";
            string keyWordSearchquery = "";

            if (authoBrnachIdList != null && authoBrnachIdList.Any() && !authoBrnachIdList.Contains(SelectionType.SelelectAll))
            {
                branchFilter = @" AND brn.Id IN (" + string.Join(",", authoBrnachIdList) + ") ";
            }
            if (departmentIdList != null && departmentIdList.Any() && !departmentIdList.Contains(SelectionType.SelelectAll))
            {
                departmentFilter = @" AND de.Id IN (" + string.Join(",", departmentIdList) + ") ";
            }
            if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
            {
                campusFilter = @" AND cam.Id IN (" + string.Join(",", campusIdList) + ") ";
            }
            if (memberTypeList != null && memberTypeList.Any() && !memberTypeList.Contains(SelectionType.SelelectAll))
            {
                if (memberTypeList.Contains(1))
                {
                    memberTypeFilter = @" AND emh.EmploymentStatus != " + (int)MemberEmploymentStatus.Retired;
                }
                else if (memberTypeList.Contains(2))
                {
                    memberTypeFilter = @"AND emh.EmploymentStatus = " + (int)MemberEmploymentStatus.Retired;
                }
            }
            if (memberStatuList != null && memberStatuList.Any() && !memberStatuList.Contains(SelectionType.SelelectAll))
            {
                memberStatusFilter = @" AND emh.EmploymentStatus IN (" + string.Join(",", memberStatuList) + ") ";
            }

            //search by KeyWord 
            if (!String.IsNullOrEmpty(keyword))
            {
                if (informationViewList != null && informationViewList.Any())
                {
                    foreach (string information in informationViewList)
                    {
                        if (information == TeamMemberReportConstants.Pin)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " tm.Pin like '" + keyword + "'";
                        }
                        if (information == TeamMemberReportConstants.Organization)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " org.ShortName like '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.Branch)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " brn.Name like '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.Campus)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + "cam.Name like '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.FullName)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " tm.FullNameEng like '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.NickName)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " tm.Name like '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.Department)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " de.Name like '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.Designation)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " desg.Name like '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.EmploymentStatus)
                        {
                            if (employmentStatusIdList.Any())
                                keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " emh.EmploymentStatus IN (" + string.Join(",", employmentStatusIdList) + ") ";
                        }
                        if (information == TeamMemberReportConstants.JoiningDate)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " convert(varchar,jmh.EffectiveDate,121) LIKE '" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.PermanentDate)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " convert(varchar,pmh.EffectiveDate,121) LIKE '" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.DiscontinuationDate)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " convert(varchar,dismh.EffectiveDate,121) LIKE '" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.OfficialContact)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " mode.OfficialContact LIKE '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.PersonalContact)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " tm.PersonalContact LIKE '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.OfficialEmail)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + "  mode.OfficialEmail LIKE '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.PersonalEmail)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + "  tm.PersonalEmail LIKE '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.OfficeShift)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + "   swh.ShiftName LIKE '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.Gender)
                        {
                            if (genderIdList.Any())
                                keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " tm.Gender IN (" + string.Join(",", genderIdList) + ") ";
                        }
                        if (information == TeamMemberReportConstants.Religion)
                        {
                            if (religionIdList.Any())
                                keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " tm.Religion IN (" + string.Join(",", religionIdList) + ") ";
                        }
                        if (information == TeamMemberReportConstants.BloodGroup)
                        {
                            if (bloodGroupIdList.Any())
                                keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " tm.BloodGroup IN (" + string.Join(",", bloodGroupIdList) + ") ";
                        }
                        if (information == TeamMemberReportConstants.DateOfBirthCertificate)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " convert(varchar,tm.CertificateDOB,121) LIKE '" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.DateOfBirthActual)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " convert(varchar,tm.ActualDOB,121) LIKE '" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.HomeDistrict)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + "   dis.Name LIKE '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.Upozilla)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + "   tha.Name LIKE '%" + keyword + "%'";
                        }
                        if (information == TeamMemberReportConstants.MaritalStatus)
                        {
                            if (maritalStatusIdList.Any())
                                keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " mainfo.MaritalStatus IN (" + string.Join(",", maritalStatusIdList) + ") ";
                        }
                        if (information == TeamMemberReportConstants.MarriageDate)
                        {
                            keyWordSearchquery += (keyWordSearchquery == "" ? "" : " OR ") + " convert(varchar,mainfo.MarriageDate,121) LIKE '" + keyword + "%'";
                        }
                    }
                }
                if (!String.IsNullOrEmpty(keyWordSearchquery))
                {
                    keyWordSearchquery = " AND ( " + keyWordSearchquery + " ) ";
                }
            }


            string query = "";
            query = @"Select 
	                    tm.Id as Id
	                    , tm.Pin as Pin
	                    , org.ShortName as Organization
	                    , brn.Name as Branch
	                    , cam.Name as Campus
	                    , tm.FullNameEng as FullName
	                    , tm.Name as NickName
	                    , de.Name as Department
	                    , desg.Name as Designation
	                    , emh.EmploymentStatus as EmploymentStatus
	                    , mh.MentorName as MentorNamePin
	                    , jmh.EffectiveDate as JoiningDate
	                    , pmh.EffectiveDate as PermanentDate
	                    , dismh.EffectiveDate as DiscontinuationDate
	                    , mode.OfficialContact as OfficialContact
	                    , tm.PersonalContact as PersonalContact
	                    , mode.OfficialEmail as OfficialEmail
	                    , tm.PersonalEmail as PersonalEmail
	                    , swh.ShiftName as OfficeShift
	                    , tm.Gender as Gender
	                    , tm.Religion as Religion
	                    , tm.BloodGroup as BloodGroup
	                    , tm.CertificateDOB as DateOfBirthCertificate
	                    , tm.ActualDOB as DateOfBirthActual
	                    , dis.Name as HomeDistrict
	                    , tha.Name as Upozilla
	                    , mainfo.MaritalStatus as MaritalStatus
	                    , mainfo.MarriageDate as MarriageDate
                    from HR_TeamMember as tm
                    inner join (
	                    SELECT rank() OVER(PARTITION BY TeammemberId ORDER BY EffectiveDate DESC, Id DESC) AS remh
	                    , EffectiveDate 
	                    , CampusId
	                    , DepartmentId
	                    , DesignationId
	                    , TeamMemberId
	                    , EmploymentStatus
	                    FROM HR_EmploymentHistory
	                    where EffectiveDate <= getDate()
	                    and Status = " + EmploymentHistory.EntityStatus.Active + @"
                     ) as emh on emh.TeamMemberId = tm.Id and emh.remh = 1
                     inner join HR_Department as de on de.Id = emh.DepartmentId and de.Status = " + Department.EntityStatus.Active + @"
                     inner join Organization as org on org.Id = de.OrganizationId and org.Status = " + Organization.EntityStatus.Active + @"
                     inner join Campus as cam on cam.Id = emh.CampusId and cam.Status = " + Campus.EntityStatus.Active + @"
                     inner join Branch as brn on brn.Id = cam.BranchId and brn.Status = " + Branch.EntityStatus.Active + @"
                     inner join HR_Designation as desg on desg.Id = emh.DesignationId and desg.Status = " + Designation.EntityStatus.Active + @"
                     Left join (
	                    SELECT rank() OVER(PARTITION BY mh.TeammemberId ORDER BY mh.EffectiveDate DESC, mh.Id DESC) AS rmh
	                    , mh.EffectiveDate
	                    , mh.TeamMemberId
	                    , mh.Id
	                    , CASE when (mtm.FullNameEng is null or mtm.FullNameEng ='') then (mtm.Name + '('+ CONVERT(varchar(10), mtm.Pin) +')' ) ELSE (mtm.FullNameEng + '('+ CONVERT(varchar(10), mtm.Pin) +')' ) end as MentorName
	                    FROM HR_MentorHistory as mh
	                    inner join HR_TeamMember as mtm on mtm.Id = mh.MentorId and mtm.Status = " + TeamMember.EntityStatus.Active + @"
	                    where mh.EffectiveDate <= getDate()
	                    and mh.Status = " + MentorHistory.EntityStatus.Active + @"
                     ) as mh on mh.TeamMemberId = tm.Id and mh.rmh = 1
                      left join (
	                    SELECT rank() OVER(PARTITION BY TeammemberId ORDER BY EffectiveDate ASC, Id ASC) AS rjmh
	                    , EffectiveDate 
	                    , TeamMemberId
	                    FROM HR_EmploymentHistory
	                    where Status = " + EmploymentHistory.EntityStatus.Active + @"
                     ) as jmh on jmh.TeamMemberId = tm.Id and jmh.rjmh = 1
                     left join (
	                    SELECT rank() OVER(PARTITION BY TeammemberId ORDER BY EffectiveDate ASC, Id ASC) AS rpmh
	                    , EffectiveDate 
	                    , TeamMemberId
	                    FROM HR_EmploymentHistory
	                    where Status = " + EmploymentHistory.EntityStatus.Active + @"
	                    and EmploymentStatus = " + (int)MemberEmploymentStatus.Permanent + @"
                     ) as pmh on pmh.TeamMemberId = tm.Id and pmh.rpmh = 1
                     left join (
	                    SELECT rank() OVER(PARTITION BY TeammemberId ORDER BY EffectiveDate DESC, Id DESC) AS rdismh
	                    , EffectiveDate 
	                    , TeamMemberId
	                    FROM HR_EmploymentHistory
	                    where Status = " + EmploymentHistory.EntityStatus.Active + @"
	                    and EmploymentStatus = " + (int)MemberEmploymentStatus.Retired + @"
                     ) as dismh on dismh.TeamMemberId = tm.Id and dismh.rdismh = 1
                     left join (
	                    SELECT rank() OVER(PARTITION BY TeammemberId ORDER BY Id DESC) AS rmod
	                    , OfficialContact
	                    , OfficialEmail
	                    , TeamMemberId
	                    FROM HR_MemberOfficialDetail
	                    where Status = " + MemberOfficialDetail.EntityStatus.Active + @"
                     ) as mode on mode.TeamMemberId = tm.Id and mode.rmod = 1
                     left join (
	                    SELECT rank() OVER(PARTITION BY swh.TeammemberId ORDER BY swh.EffectiveDate DESC, swh.Id DESC) AS rswh
	                    , swh.EffectiveDate
	                    , s.Name as ShiftName
	                    , swh.TeamMemberId
	                    FROM HR_ShiftWeekendHistory as swh
	                    inner join HR_Shift as s on s.Id = swh.ShiftId and s.Status = " + Shift.EntityStatus.Active + @"
	                    where swh.EffectiveDate <= getDate()
	                    and swh.Status = " + ShiftWeekendHistory.EntityStatus.Active + @"
                     ) as swh on swh.TeamMemberId = tm.Id and swh.rswh = 1
                     left join Thana as tha on Tha.Id = tm.PermanentThanaId and tha.Status = " + Thana.EntityStatus.Active + @"
                     left join District as dis on dis.Id = tha.DistrictId and dis.Status = " + District.EntityStatus.Active + @"
                     left join (
	                    SELECT rank() OVER(PARTITION BY TeammemberId ORDER BY Id DESC) AS rmainfo
	                    , MaritalStatus
	                    , MarriageDate
	                    , TeamMemberId
	                    FROM HR_MaritalInfo
	                    where Status = " + MaritalInfo.EntityStatus.Active + @"
                     ) as mainfo on mainfo.TeamMemberId = tm.Id and mainfo.rmainfo = 1
                    Where 1=1 " + branchFilter + campusFilter + departmentFilter + memberTypeFilter + memberStatusFilter + keyWordSearchquery + @"
                     ";
            return query;
        }

        private string GetTeamMemberListSqlQueryNew(List<long> organizationIdList, long? departmentId, List<long> branchIdList, List<long> campusIdList, int? memberTypes, int? memberStatus, bool isCount = true)
        {
            string query = "";
            string filterQuery = "";
            string filterQuery1 = "";
            if (organizationIdList != null && organizationIdList.Any() && !organizationIdList.Contains(SelectionType.SelelectAll))
            {
                filterQuery += @" AND org.Id IN (" + string.Join(",", organizationIdList) + ") ";
            }
            if (branchIdList != null && branchIdList.Any() && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                filterQuery += @" AND br.Id IN (" + string.Join(",", branchIdList) + ") ";
            }
            if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
            {
                filterQuery += @" AND campus2_.Id IN (" + string.Join(",", campusIdList) + ") ";
            }

            if (departmentId != null && departmentId != SelectionType.SelelectAll)
            {
                filterQuery += @" AND department1_.Id =" + departmentId;
            }
            //if (memberTypes != null)
            //{
            //filterQuery1 += @" AND emHistory.EmploymentStatus=" + memberTypes;
            //}

            string selectQuery = isCount ? "Select count(*) from " : "Select b.* from ";
            query = @" 
                        select 
                                --emHistory.EmploymentStatus,
                               -- emHistory.EffectiveDate, 
a.* into #aaa 
						from (
	                         select * from(
								select TeamMemberId,CampusId,DesignationId,DepartmentId,EmploymentStatus,EffectiveDate	
								,RANK() OVER (PARTITION BY a.TeamMemberId ORDER BY a.EffectiveDate DESC) as r                        				
								  from  [dbo].[HR_EmploymentHistory] as a							
								left join [dbo].[Campus] as cam on a.CampusId=cam.Id and cam.Status=1
								left join [dbo].[HR_Department] as dept on a.DesignationId=dept.Id and dept.Status=1
								left join [dbo].[HR_Designation] as desg on desg.Id=a.DesignationId and desg.Status=1
								where a.Status=1
                                and a.effectiveDate<=getdate()
								) as a where a.r=1";
            if (memberTypes != null)
            {
                query += @" and a.EmploymentStatus=" + memberTypes;
            }
            if (memberStatus != null)
            {
                switch (memberStatus)
                {
                    case 2:
                        query += @" AND a.EmploymentStatus=" + (int)MemberEmploymentStatus.Retired;
                        break;
                    case 1:
                        query += @" AND a.EmploymentStatus<" + (int)MemberEmploymentStatus.Retired;
                        break;
                }
            }
            query += @" ) as a ";
            query += @" " + filterQuery1;
            query += selectQuery + @"                         
                        (
                        select 
                        teamTable.TeamMemberId as Id
                        ,tm.Pin as Pin
                        --,teamTable.JoiningDate as JoiningDate
						, (SELECT MIN(EffectiveDate) FROM HR_EmploymentHistory WHERE TeamMemberId = teamTable.TeamMemberId) AS JoiningDate
                       -- ,teamTable.PermanentDate as PermanentDate
					   , (SELECT MIN(EffectiveDate) FROM HR_EmploymentHistory WHERE TeamMemberId = teamTable.TeamMemberId AND EmploymentStatus = 2) AS PermanentDate
                        --,teamTable.Retired as Retired
						, (SELECT TOP 1 EffectiveDate FROM HR_EmploymentHistory WHERE TeamMemberId = teamTable.TeamMemberId AND EmploymentStatus = 6) AS Retired
                        ,tm.FullNameEng as FullName
                        ,tm.Name as NickName
                        ,tm.PersonalContact as PersonalContact
                        ,tm.PersonalEmail as PersonalEmail
                        ,tm.Gender as Gender
                        ,tm.Religion as Religion
                        ,tm.BloodGroup as BloodGroup
                        ,tm.ActualDOB as DateOfBirthActual 
                        ,tm.CertificateDOB as DateOfBirthCertificate
                        ,CONCAT(menhistory.MentorName,' (',menhistory.Pin,')') as MentorNamePin
                        ,swhistory.ShiftName as OfficeShift
                        ,mai.MaritalStatus as MaritalStatus
                        ,mai.MarriageDate as MarriageDate
                        ,campus2_.Name as Campus
                        ,org.ShortName as Organization
                        ,br.Name as Branch
                        ,department1_.Name as Department
                        ,desig.Name as Designation
                        ,teamTable.EmploymentStatus as EmploymentStatus
                        ,modetails.OfficialContact as OfficialContact
                        ,modetails.OfficialEmail as OfficialEmail
                        ,thana.Name as Upozilla
                        ,dis.Name as HomeDistrict
                        ,teamTable.EffectiveDate
						--,RANK() OVER (PARTITION BY teamTable.TeamMemberId ORDER BY teamTable.EffectiveDate,teamTable.emhId DESC) as r
                        from [HR_TeamMember] as tm 
                        inner join
                        (
                        select 
                            max(emh.Id) as emhId
	                        ,a.TeamMemberId
	                        --,max(a.JoiningDate) as JoiningDate
	                        --,max(a.PermanentDate) as PermanentDate
	                        --,max(a.Retired) as Retired 
	                        ,a.CampusId
	                        ,a.DepartmentId
	                        ,a.DesignationId
	                        ,a.EmploymentStatus
                            ,a.EffectiveDate
	                        from  #aaa as a 
                            left join [HR_EmploymentHistory] as emh on a.TeamMemberId=emh.TeamMemberId
	                        group by a.TeamMemberId
	                        ,a.CampusId,a.DepartmentId,a.DesignationId,a.EmploymentStatus,a.EffectiveDate
                        ) as teamTable on tm.Id=teamTable.TeamMemberId
                        inner join (
	                        --Mentor Name And Pin
		                        select * from(
			                        select 
                                    -- mh.MentorName
			                        tm.Id
			                        ,mh.Pin
			                        ,mh.EffectiveDate
		                            , Case when mentor.FullNameEng is null or mentor.FullNameEng !='' then mentor.Name else mentor.FullNameEng end AS MentorName
			                        ,RANK() OVER (PARTITION BY mh.TeamMemberId ORDER BY mh.EffectiveDate DESC) as r
				                        from HR_TeamMember as tm
			                        inner join HR_MentorHistory as mh on tm.Id=mh.TeamMemberId and mh.Status=1
		                            inner join HR_TeamMember as mentor on mentor.Id=mh.MentorId and mentor.Status=1
                            ";
            query += @" where mh.EffectiveDate<='" + DateTime.Now + "' and tm.Status=1";
            query += @"			                        
		                        ) as m where m.r=1
	                        ) as menhistory on menhistory.Id=tm.Id
                        inner join(
	                        -- Shift Name
		                        select * from(
			                        select 
			                        tm.Id as teamMemberId
			                        --,swh.EffectiveDate
			                        ,sh.Name as ShiftName
			                        ,RANK() OVER (PARTITION BY swh.TeamMemberId ORDER BY swh.EffectiveDate DESC) as r 
			                        from [HR_TeamMember] as tm
			                        inner join [HR_ShiftWeekendHistory] swh on tm.Id= swh.TeamMemberId and swh.Status=1
			                        inner join [HR_Shift] sh on sh.Id = swh.ShiftId and sh.Status=1  
			                        where tm.Status = 1                      
		                        ) as swhis where swhis.r=1 
	                        )  as swhistory on swhistory.teamMemberId = tm.Id
	                        -- Marital Status
                        inner join(
		                        select mi.MaritalStatus,mi.MarriageDate,tm.Id as teamMemberId from HR_TeamMember tm
		                        inner join [dbo].[HR_MaritalInfo] mi on tm.Id=mi.TeamMemberId and mi.Status=1
		                        where tm.Status = 1 
	                        ) as mai on tm.Id = mai.teamMemberId		
                          inner join [Campus] campus2_ on teamTable.CampusId=campus2_.Id and campus2_.Status=1
                          inner join [HR_Department] department1_ on teamTable.DepartmentId=department1_.Id and department1_.Status=1
                          inner join [Organization] org on org.Id = department1_.OrganizationId and org.Status=1
                          inner join [Branch] br on br.Id = campus2_.BranchId and br.Status=1
                          inner join [HR_Designation] desig on desig.Id = teamTable.DesignationId and desig.Status=1
                          left join  [HR_MemberOfficialDetail] modetails on tm.Id = modetails.TeamMemberId and modetails.Status=1
                          left join [Postoffice] as po on tm.PermanentPostofficeId = po.Id and po.Status=1
                          -- left join [Thana] as thana on po.ThanaId = thana.Id and thana.Status=1
                          left join [Thana] as thana on tm.PermanentThanaId = Thana.Id and po.Status=1
                          left join [District] as dis on dis.Id = thana.DistrictId and dis.Status=1
                          where tm.Status =1 " + filterQuery;
            query += @"
                          ) as b --where b.r=1                       
                    ";
            return query;
        }

        private string GetTeamMemberSearchListSqlQueryNew(List<long> organizationIdList, List<long> branchIdList, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList, bool isCount = true)
        {
            string query = "";
            string filterQuery = "";
            string filterQuery1 = "";
            if (organizationIdList != null && organizationIdList.Any() && !organizationIdList.Contains(SelectionType.SelelectAll))
            {
                filterQuery += @" AND org.Id IN (" + string.Join(",", organizationIdList) + ") ";
            }
            if (branchIdList != null && branchIdList.Any() && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                filterQuery += @" AND br.Id IN (" + string.Join(",", branchIdList) + ") ";
            }

            if (departmentId != null && departmentId != SelectionType.SelelectAll)
            {
                filterQuery += @" AND department1_.Id =" + departmentId;
            }
            //if (memberTypes != null)
            //{
            //    filterQuery1 += @" AND emHistory.EmploymentStatus=" + memberTypes;
            //}
            if (memberStatus != null)
            {
                switch (memberStatus)
                {
                    case 2:
                        filterQuery1 += @" AND emHistory.EmploymentStatus=" + (int)MemberEmploymentStatus.Retired;
                        break;
                    case 1:
                        filterQuery1 += @" AND emHistory.EmploymentStatus<" + (int)MemberEmploymentStatus.Retired;
                        break;
                }
            }
            if (!String.IsNullOrEmpty(keyword))
            {
                filterQuery += @" AND (tm.Pin like '%" + keyword + "%'";
                if (informationViewList != null && informationViewList.Length > 0)
                {
                    foreach (string informationView in informationViewList)
                    {
                        if (informationView == "Organization")
                        {
                            filterQuery += @" OR org.ShortName like '%" + keyword + "%'";
                        }
                        if (informationView == "Branch")
                        {
                            filterQuery += @" OR br.Name like '%" + keyword + "%'";
                        }
                        if (informationView == "Campus")
                        {
                            filterQuery += @" OR campus2_.Name like '%" + keyword + "%'";
                        }

                        if (informationView == "FullName")
                        {
                            filterQuery += @" OR tm.FullNameEng like '%" + keyword + "%'";
                        }
                        if (informationView == "NickName")
                        {
                            filterQuery += @" OR tm.Name like '%" + keyword + "%'";
                        }
                        if (informationView == "Department")
                        {
                            filterQuery += @" OR department1_.Name like '%" + keyword + "%'";
                        }
                        if (informationView == "Designation")
                        {
                            filterQuery += @" OR desig.Name like '%" + keyword + "%'";
                        }

                        if (informationView == "EmploymentStatus")
                        {
                            filterQuery += @" OR teamTable.EmploymentStatus like '%" + keyword + "%'";
                        }
                        if (informationView == "JoiningDate")
                        {
                            filterQuery += @" OR teamTable.EffectiveDate like '%" + keyword + "%'";
                        }
                        if (informationView == "PermanentDate")
                        {
                            filterQuery += @" OR teamTable.EffectiveDate like '%" + keyword + "%'";
                        }
                        if (informationView == "DiscontinuationDate")
                        {
                            filterQuery += @" OR teamTable.EffectiveDate like '%" + keyword + "%'";
                        }
                        if (informationView == "OfficialContact")
                        {
                            filterQuery += @" OR modetails.OfficialContact like '%" + keyword + "%'";
                        }
                        if (informationView == "PersonalContact")
                        {
                            filterQuery += @" OR tm.PersonalContact like '%" + keyword + "%'";
                        }
                        if (informationView == "OfficialEmail")
                        {
                            filterQuery += @" OR modetails.OfficialEmail like '%" + keyword + "%'";
                        }
                        if (informationView == "PersonalEmail")
                        {
                            filterQuery += @" OR tm.PersonalEmail like '%" + keyword + "%'";
                        }
                        if (informationView == "OfficeShift")
                        {
                            filterQuery += @" OR swhistory.ShiftName like '%" + keyword + "%'";
                        }
                        if (informationView == "Gender")
                        {
                            filterQuery += @" OR tm.Gender like '%" + keyword + "%'";
                        }
                        if (informationView == "Religion")
                        {
                            filterQuery += @" OR tm.Religion like '%" + keyword + "%'";
                        }
                        if (informationView == "BloodGroup")
                        {
                            filterQuery += @" OR tm.BloodGroup like '%" + keyword + "%'";
                        }
                        if (informationView == "DateOfBirth(certificate)")
                        {
                            filterQuery += @" OR tm.CertificateDOB like '%" + keyword + "%'";
                        }
                        if (informationView == "DateOfBirth(Actual)")
                        {
                            filterQuery += @" OR tm.ActualDOB like '%" + keyword + "%'";
                        }
                        if (informationView == "HomeDistrict")
                        {
                            filterQuery += @" OR dis.Name like '%" + keyword + "%'";
                        }
                        if (informationView == "Upozilla/Thana")
                        {
                            filterQuery += @" OR thana.Name like '%" + keyword + "%'";
                        }
                        if (informationView == "MaritalStatus")
                        {
                            filterQuery += @" OR mai.MaritalStatus like '%" + keyword + "%'";
                        }
                        if (informationView == "MarriageDate")
                        {
                            filterQuery += @" OR mai.MarriageDate like '%" + keyword + "%'";
                        }
                    }
                }
                filterQuery += ")";
            }
            string selectQuery = isCount ? "Select count(*) from " : "Select b.* from ";
            query = @" 
                        select emHistory.EffectiveDate,a.* into #aaa from (
	                        select * from(
								select TeamMemberId,CampusId,DesignationId,DepartmentId,EmploymentStatus	
								,RANK() OVER (PARTITION BY a.TeamMemberId ORDER BY a.EffectiveDate DESC) as r                        				
								  from  [dbo].[HR_EmploymentHistory] as a							
								left join [dbo].[Campus] as cam on a.CampusId=cam.Id and cam.Status=1
								left join [dbo].[HR_Department] as dept on a.DesignationId=dept.Id and dept.Status=1
								left join [dbo].[HR_Designation] as desg on desg.Id=a.DesignationId and desg.Status=1
								where a.Status=1
								) as a where a.r=1";
            if (memberTypes != null)
            {
                query += @" and a.EmploymentStatus=" + memberTypes;
            }
            query += @" ) as a
                        inner join [dbo].[HR_EmploymentHistory] as emHistory on a.TeamMemberId=emHistory.TeamMemberId";
            query += @" where emHistory.Status=1 " + filterQuery1;
            query += selectQuery + @"                         
                        (
                        select 
                        teamTable.TeamMemberId as Id
                        ,tm.Pin as Pin
                        --,teamTable.JoiningDate as JoiningDate
						, (SELECT MIN(EffectiveDate) FROM HR_EmploymentHistory WHERE TeamMemberId = teamTable.TeamMemberId) AS JoiningDate
                       -- ,teamTable.PermanentDate as PermanentDate
					   , (SELECT MIN(EffectiveDate) FROM HR_EmploymentHistory WHERE TeamMemberId = teamTable.TeamMemberId AND EmploymentStatus = 2) AS PermanentDate
                        --,teamTable.Retired as Retired
						, (SELECT TOP 1 EffectiveDate FROM HR_EmploymentHistory WHERE TeamMemberId = teamTable.TeamMemberId AND EmploymentStatus = 6) AS Retired
                        ,tm.FullNameEng as FullName
                        ,tm.Name as NickName
                        ,tm.PersonalContact as PersonalContact
                        ,tm.PersonalEmail as PersonalEmail
                        ,tm.Gender as Gender
                        ,tm.Religion as Religion
                        ,tm.BloodGroup as BloodGroup
                        ,tm.ActualDOB as DateOfBirthActual 
                        ,tm.CertificateDOB as DateOfBirthCertificate
                        ,CONCAT(menhistory.MentorName,' And ',menhistory.Pin) as MentorNamePin
                        ,swhistory.ShiftName as OfficeShift
                        ,mai.MaritalStatus as MaritalStatus
                        ,mai.MarriageDate as MarriageDate
                        ,campus2_.Name as Campus
                        ,org.Name as Organization
                        ,br.Name as Branch
                        ,department1_.Name as Department
                        ,desig.Name as Designation
                        ,teamTable.EmploymentStatus as EmploymentStatus
                        ,modetails.OfficialContact as OfficialContact
                        ,modetails.OfficialEmail as OfficialEmail
                        ,thana.Name as Upozilla
                        ,dis.Name as HomeDistrict
                        ,teamTable.EffectiveDate
						--,RANK() OVER (PARTITION BY teamTable.TeamMemberId ORDER BY teamTable.EffectiveDate,teamTable.Id DESC) as r
                        from [dbo].[HR_TeamMember] as tm 
                        inner join
                        (
                        select 
	                        max(emh.Id) as emhId
							,a.TeamMemberId
	                        --,max(a.JoiningDate) as JoiningDate
	                        --,max(a.PermanentDate) as PermanentDate
	                        --,max(a.Retired) as Retired 
	                        ,a.CampusId
	                        ,a.DepartmentId
	                        ,a.DesignationId
	                        ,a.EmploymentStatus
							,a.EffectiveDate
	                        from  #aaa as a 
                            left join [dbo].[HR_EmploymentHistory] as emh on a.TeamMemberId=emh.TeamMemberId
	                        group by a.TeamMemberId
	                        ,a.CampusId,a.DepartmentId,a.DesignationId,a.EmploymentStatus,a.EffectiveDate
                        ) as teamTable on tm.Id=teamTable.TeamMemberId
                        inner join (
	                        --Mentor Name And Pin
		                        select * from(
			                        select 
                                    --mh.MentorName
			                        tm.Id
			                        ,mh.Pin
			                        ,mh.EffectiveDate
		                            , Case when mentor.FullNameEng is null or mentor.FullNameEng !='' then mentor.Name else mentor.FullNameEng end AS MentorName
			                        ,RANK() OVER (PARTITION BY mh.TeamMemberId ORDER BY mh.EffectiveDate DESC) as r
				                        from HR_TeamMember as tm
			                        inner join HR_MentorHistory as mh on tm.Id=mh.TeamMemberId and mh.Status=1
		                            inner join HR_TeamMember as mentor on mentor.Id=mh.MentorId and mentor.Status=1";
            query += @" where mh.EffectiveDate<='" + DateTime.Now + "' and tm.Status=1";
            query += @"			                        
		                        ) as m where m.r=1
	                        ) as menhistory on menhistory.Id=tm.Id
                        inner join(
	                        -- Shift Name
		                        select * from(
			                        select 
			                        tm.Id as teamMemberId
			                        --,swh.EffectiveDate
			                        ,sh.Name as ShiftName
			                        ,RANK() OVER (PARTITION BY swh.TeamMemberId ORDER BY swh.EffectiveDate DESC) as r 
			                        from [dbo].[HR_TeamMember] as tm
			                        inner join [dbo].[HR_ShiftWeekendHistory] swh on tm.Id= swh.TeamMemberId and swh.Status=1
			                        inner join [dbo].[HR_Shift] sh on sh.Id = swh.ShiftId and sh.Status=1  
			                        where tm.Status = 1                      
		                        ) as swhis where swhis.r=1 
	                        )  as swhistory on swhistory.teamMemberId = tm.Id
	                        -- Marital Status
                        inner join(
		                        select mi.MaritalStatus,mi.MarriageDate,tm.Id as teamMemberId from HR_TeamMember tm
		                        inner join [dbo].[HR_MaritalInfo] mi on tm.Id=mi.TeamMemberId and mi.Status=1
		                        where tm.Status = 1 
	                        ) as mai on tm.Id = mai.teamMemberId		
                          inner join [dbo].[Campus] campus2_ on teamTable.CampusId=campus2_.Id and campus2_.Status=1
                          inner join [dbo].[HR_Department] department1_ on teamTable.DepartmentId=department1_.Id and department1_.Status=1
                          inner join [dbo].[Organization] org on org.Id = department1_.OrganizationId and org.Status=1
                          inner join [dbo].[Branch] br on br.Id = campus2_.BranchId and br.Status=1
                          inner join [dbo].[HR_Designation] desig on desig.Id = teamTable.DesignationId and desig.Status=1
                          left join  [dbo].[HR_MemberOfficialDetail] modetails on tm.Id = modetails.TeamMemberId and modetails.Status=1
                          left join [dbo].[Postoffice] as po on tm.PermanentPostofficeId = po.Id and po.Status=1
                          -- left join [dbo].[Thana] as thana on po.ThanaId = thana.Id and thana.Status=1
                          left join [Thana] as thana on tm.PermanentThanaId = Thana.Id and po.Status=1
                          left join [dbo].[District] as dis on dis.Id = thana.DistrictId and dis.Status=1
                          where tm.Status =1 " + filterQuery;
            query += @"
                          ) as b --where b.r=1                            
                    ";
            return query;
        }

        public bool IsDuplicatePersonalContact(string personalContact, long? id)
        {
            List<TeamMember> memberList;
            if (id != null)
            {
                memberList = Session.QueryOver<TeamMember>().Where(x => x.PersonalContact == personalContact && x.Id != id).List<TeamMember>() as List<TeamMember>;
            }
            else
            {
                memberList = Session.QueryOver<TeamMember>().Where(x => x.PersonalContact == personalContact).List<TeamMember>() as List<TeamMember>;
            }
            return memberList != null && memberList.Count > 0;
        }

        public void GetCurrentInfos(long hrMemberId, out object campus, out string department, out string designation, out int empStatus, out string joindate, out string todate, out string officeShift, out int weekend, out int mentorPin)
        {
            campus = null;
            department = designation = joindate = todate = officeShift = "";
            empStatus = weekend = mentorPin = 0;
            int i = 0;
            var employmentHistory = Session.QueryOver<EmploymentHistory>()
                .Where(x => x.TeamMember.Id == hrMemberId && x.Status == EmploymentHistory.EntityStatus.Active && x.EffectiveDate <= DateTime.Now.Date)
                .OrderBy(x => x.EffectiveDate).Desc.Take(1).SingleOrDefault();
            if (employmentHistory != null)
            {
                department = employmentHistory.Department.Name;
                designation = employmentHistory.Designation.Name;
                empStatus = Convert.ToInt32(employmentHistory.EmploymentStatus);
                //todate = employmentHistory.EffectiveDate.ToString("yyyy-MM-dd");
                campus = employmentHistory.Campus;
            }

            var employmentHistoryForToday = Session.QueryOver<EmploymentHistory>()
              .Where(x => x.TeamMember.Id == hrMemberId && x.Status == EmploymentHistory.EntityStatus.Active)
              .OrderBy(x => x.EffectiveDate).Desc.Take(1).SingleOrDefault();
            if (employmentHistory != null)
            {
                todate = employmentHistoryForToday.EffectiveDate.ToString("yyyy-MM-dd");

            }

            var employmentHistoryFirst = Session.QueryOver<EmploymentHistory>()
                .Where(x => x.TeamMember.Id == hrMemberId && x.Status == EmploymentHistory.EntityStatus.Active)
                .OrderBy(x => x.EffectiveDate).Asc.Take(1).SingleOrDefault();
            if (employmentHistoryFirst != null)
            {
                joindate = employmentHistoryFirst.EffectiveDate.ToString("yyyy-MM-dd");
            }


            //foreach (var hrEmploymentHistory in employmentHistory)
            //{
            //    if (i == 0)
            //    {
            //        campus = Session.QueryOver<Campus>().Where(x => x.Id == hrEmploymentHistory.Campus.Id).SingleOrDefault<Campus>();
            //        department = hrEmploymentHistory.Department.Name;
            //        designation = hrEmploymentHistory.Designation.Name;
            //        empStatus = Convert.ToInt32(hrEmploymentHistory.EmploymentStatus);
            //        joindate = hrEmploymentHistory.EffectiveDate.ToString("yyyy-MM-dd");
            //        todate = hrEmploymentHistory.EffectiveDate.ToString("yyyy-MM-dd");
            //    }
            //    else
            //    {
            //        campus = Session.QueryOver<Campus>().Where(x => x.Id == hrEmploymentHistory.Campus.Id).SingleOrDefault<Campus>();
            //        department = hrEmploymentHistory.Department.Name;
            //        designation = hrEmploymentHistory.Designation.Name;
            //        empStatus = Convert.ToInt32(hrEmploymentHistory.EmploymentStatus);
            //        todate = hrEmploymentHistory.EffectiveDate.ToString("yyyy-MM-dd");
            //    }
            //    i++;
            //    if (hrEmploymentHistory.EffectiveDate > DateTime.Now)
            //        break;
            //}

            var shiftWeekendHistory = Session.QueryOver<ShiftWeekendHistory>()
                    .Where(x => x.TeamMember.Id == hrMemberId && x.Status == ShiftWeekendHistory.EntityStatus.Active && x.EffectiveDate <= DateTime.Now.Date)
                    .OrderBy(x => x.EffectiveDate).Desc
                    .List<ShiftWeekendHistory>()
                    .FirstOrDefault();

            if (shiftWeekendHistory != null)
            {
                officeShift = shiftWeekendHistory.Shift.Name;
                weekend = Convert.ToInt32(shiftWeekendHistory.Weekend);

            }

            var mentorHistory = Session.QueryOver<MentorHistory>()
                    .Where(x => x.TeamMember.Id == hrMemberId && x.Status == MentorHistory.EntityStatus.Active && x.EffectiveDate <= DateTime.Now.Date)
                    .OrderBy(x => x.EffectiveDate).Desc
                    .List<MentorHistory>()
                    .FirstOrDefault();

            if (mentorHistory != null)
            {
                mentorPin = mentorHistory.Pin ?? Convert.ToInt32(mentorHistory.Pin);

            }
        }

        public int GetMemberWeekend(long hrMemberId)
        {
            int weekend = 0;
            var shiftWeekendHistory = Session.QueryOver<ShiftWeekendHistory>()
                    .Where(x => x.TeamMember.Id == hrMemberId && x.Status == ShiftWeekendHistory.EntityStatus.Active && x.EffectiveDate <= DateTime.Now.Date)
                    .OrderBy(x => x.EffectiveDate).Desc
                    .List<ShiftWeekendHistory>()
                    .FirstOrDefault();

            if (shiftWeekendHistory != null)
            {
                weekend = Convert.ToInt32(shiftWeekendHistory.Weekend);

            }
            return weekend;
        }

        public int GetNewPin()
        {
            var list = Session.QueryOver<TeamMember>()
                .Where(x => x.Status != TeamMember.EntityStatus.Delete).List<TeamMember>();
            if (list.Count == 0) return 1;
            return list.Select(z => z.Pin).Max() + 1;

            //.Select(Projections
            //.ProjectionList()
            //.Add(Projections.Max<TeamMember>(x => x.Pin)))
            //.List<int>().First() + 1;
        }

        public List<LeaveTypeDtoList> LoadMembersPendingLeave(long memId, bool isHr = false, int leaveYear = 0)
        {
            if (leaveYear == 0)
            {
                leaveYear = DateTime.Now.Year;
            }

            string IsPublicString = " ";
            if (isHr)
            {
                IsPublicString = " OR IsPublic = 0 ";
            }
            var query = @"SELECT 
                        L.Id as [LeaveId],
                        L.Name as [LaveName],
                        CASE WHEN MLS.AvailableBalance IS NULL THEN 0 ELSE MLS.AvailableBalance END as [Balance] ,
                        L.Name+' ('+CONVERT(varchar(10), ( CASE WHEN MLS.AvailableBalance is null THEN 0 ELSE MLS.AvailableBalance END))+') ' as [ShowBalance]      
                        FROM [dbo].[HR_Leave]  AS L 
                        INNER JOIN (
                            SELECT * FROM [dbo].[HR_MemberLeaveSummary] WHERE [TeamMemberId]=" + memId + @" AND [Year] = " + leaveYear + @" AND Status = " + MemberLeaveSummary.EntityStatus.Active + @" " +
                            " AND OrganizationId = (SELECT TOP 1 d.OrganizationId FROM HR_EmploymentHistory as e, HR_Designation as d WHERE e.status = 1 AND  e.DesignationId = d.Id AND TeamMemberId = " + memId + @" AND e.EffectiveDate <= getdate() ORDER BY e.EffectiveDate DESC)
                         ) AS MLS ON L.Id=MLS.LeaveId WHERE L.IsPublic = 1 AND L.PayType = 1";

            //if (isHr)
            //{
            query += @"UNION ALL
                            SELECT Id    as [LeaveId],
                                   Name  as [LaveName],  
                                365 as [Balance],
                                Name  + ' (-) ' as [ShowBalance]
                            FROM [HR_Leave] 
                            WHERE OrganizationId = (SELECT TOP 1 d.OrganizationId FROM HR_EmploymentHistory as e, HR_Designation as d WHERE e.Status = 1 AND  e.DesignationId = d.Id AND TeamMemberId = " + memId + @" AND e.EffectiveDate <= getdate() ORDER BY e.EffectiveDate DESC)
                             AND (PayType = 0 " + IsPublicString + @")";
            //}
            var list = Session.CreateSQLQuery(query).SetResultTransformer(Transformers.AliasToBean<LeaveTypeDtoList>());
            return list.List<LeaveTypeDtoList>().ToList();
        }

        public string GetTeamMemberDirectorySqlQuery(List<long> organizationIdList, List<long> branchIdList, List<long> campusIdList, long? departmentId, string name, int? pin, int? bloodGroup, bool isCount = true)
        {
            string query = "";
            string filterQuery = "";
            string filterQuery2 = "";
            string filterBloodGroup = "";

            if (name != "")
            {
                filterQuery2 += @" AND hrm.Name like '%" + name + "%'";
            }

            if (organizationIdList != null && organizationIdList.Any() && !organizationIdList.Contains(SelectionType.SelelectAll))
            {
                filterQuery2 += @" AND o.Id IN (" + string.Join(",", organizationIdList) + ") ";
            }
            if (branchIdList != null && branchIdList.Any() && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                filterQuery2 += @" AND br.Id IN (" + string.Join(",", branchIdList) + ") ";
            }
            if (pin != null)
            {
                filterQuery2 += @" AND hrm.Pin =" + pin;
            }
            if (bloodGroup != null)
            {
                filterBloodGroup += @" AND hrm.BloodGroup =" + bloodGroup;
            }
            //if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
            //{
            //    filterQuery += @" AND CampusId IN (" + string.Join(",", campusIdList) + ") ";
            //}

            if (departmentId != null)
            {
                filterQuery += @" AND DepartmentId =" + departmentId;
            }
            string selectQuery = isCount ? "Select count(*) from" : "Select x.* from";
            //string selectQueryOrderBy = isCount ? "" : " order by x.Pin ASC";
            query += selectQuery + @" (Select hrm.*,TeamMemberId, EffectiveDate from 
                    [HR_TeamMember] AS hrm 
                    Left Join (
	                    Select * from (
		                    Select [CampusId]
			                    ,[DesignationId]
			                    ,[DepartmentId]
			                    ,[TeamMemberId]
			                    ,[EffectiveDate]
			                    ,[EmploymentStatus]
			                    ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC, Id DESC) AS R  
			                    from [HR_EmploymentHistory] 
			                    Where Status = 1
                                and EffectiveDate <= getDate()
			                    -- " + filterQuery + @"
	                    ) AS A 
                    Where A.R = 1 AND A.[EmploymentStatus] != " + (int)MemberEmploymentStatus.Retired;
            if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND CampusId IN (" + string.Join(",", campusIdList) + ") ";
            }
            query += @"  ) AS currentPosition ON currentPosition.TeamMemberId  = hrm.Id and hrm.status=" + TeamMember.EntityStatus.Active +
                    @" Left Join [Campus] AS c ON c.Id = currentPosition.CampusId and c.Status = 1
                    Left Join [Branch] AS br ON br.Id = c.BranchId and br.Status =  1
                    Left Join [Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  1
                    Left Join [HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  1
                    Left Join [HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  1
                    Where 1 = 1
                    " + filterQuery + @"
                    " + filterQuery2 + @"
                    " + filterBloodGroup + @"
                    ) as x 
					--inner join 
					--(select TeamMemberId,MAX(EffectiveDate) as effectiveDate from  HR_EmploymentHistory where EffectiveDate<=GETDATE() group by TeamMemberId)y
					--on x.TeamMemberId=y.TeamMemberId and x.EffectiveDate=y.effectiveDate                    
                    ";
            return query;
        }

        private ICriteria GetTeamMemberSalarySearchingDateCriteria(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = Session.CreateCriteria<SalaryHistory>().Add(Restrictions.Eq("Status", EmploymentHistory.EntityStatus.Active));
            criteria.Add(Restrictions.Le("EffectiveDate", searchingDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59)));

            if (teamMemberPin != null)
            {
                criteria.CreateAlias("TeamMember", "tm").Add(Restrictions.Eq("tm.Status", TeamMember.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("tm.Pin", teamMemberPin));
            }
            if (teamMemberId != null)
            {
                criteria.Add(Restrictions.Eq("TeamMember.Id", teamMemberId));
            }
            criteria.AddOrder(Order.Desc("EffectiveDate"));
            criteria.AddOrder(Order.Desc("Id"));
            criteria.SetMaxResults(1);

            return criteria;
        }

        private ICriteria GetTeamMemberSearchingDateCriteria(DateTime searchingDate, long? teamMemberId, int? teamMemberPin)
        {
            ICriteria criteria = Session.CreateCriteria<EmploymentHistory>().Add(Restrictions.Eq("Status", EmploymentHistory.EntityStatus.Active));
            criteria.Add(Restrictions.Le("EffectiveDate", searchingDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59)));

            if (teamMemberPin != null)
            {
                criteria.CreateAlias("TeamMember", "tm").Add(Restrictions.Eq("tm.Status", TeamMember.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("tm.Pin", teamMemberPin));
            }
            if (teamMemberId != null)
            {
                criteria.Add(Restrictions.Eq("TeamMember.Id", teamMemberId));
            }
            criteria.AddOrder(Order.Desc("EffectiveDate"));
            criteria.AddOrder(Order.Desc("Id"));
            criteria.SetMaxResults(1);

            return criteria;
        }

        private string GetQueryForTeamMemberWithCurrentNextShiftInfor(List<long> branchIdList, List<long> campusIdList, List<int> employmentStatusList, List<long> departmntIdList, List<long> designationIdList, List<long> shiftIdList, DateTime? searchingEffectiveDate)
        {
            string branchFilter = "";
            string campusFilter = "";
            string employmentStatusFilter = "";
            string departmentFilter = "";
            string designationFilter = "";
            string shiftFilter = "";
            string shiftEffectiveDateFilter = "";
            string searchingEffectiveDateWiseFilter = " getdate() ";
            if (branchIdList != null && branchIdList.Any() && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                branchFilter += @" AND br.Id IN (" + string.Join(",", branchIdList) + ") ";
            }
            if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
            {
                campusFilter += @" AND cam.Id IN (" + string.Join(",", campusIdList) + ") ";
            }
            if (employmentStatusList != null && employmentStatusList.Any() && !employmentStatusList.Contains(SelectionType.SelelectAll))
            {
                employmentStatusFilter += @" AND caemh.EmployeeStatus IN (" + string.Join(",", employmentStatusList) + ") ";
            }
            if (departmntIdList != null && departmntIdList.Any() && !departmntIdList.Contains(SelectionType.SelelectAll))
            {
                departmentFilter += @" AND de.Id IN (" + string.Join(",", departmntIdList) + ") ";
            }
            if (designationIdList != null && designationIdList.Any() && !designationIdList.Contains(SelectionType.SelelectAll))
            {
                designationFilter += @" AND desg.Id IN (" + string.Join(",", designationIdList) + ") ";
            }
            if (shiftIdList != null && shiftIdList.Any() && !shiftIdList.Contains(SelectionType.SelelectAll))
            {
                shiftFilter += @" AND cswh.CurrentShiftId IN (" + string.Join(",", shiftIdList) + ") ";
            }
            if (searchingEffectiveDate != null)
            {
                shiftEffectiveDateFilter = @" AND cswh.CurrentShiftEffectiveDate <=  '" + searchingEffectiveDate.Value.Date.ToString("yyyy-MM-dd 00:00:00.000") + "'";
                searchingEffectiveDateWiseFilter = "'" + searchingEffectiveDate.Value.Date.ToString("yyyy-MM-dd 00:00:00.000") + "'";
            }
            string query = "";
            query = @"Select 
                        tm.Id as TeamMemberId
                        , CASE WHEN tm.FullNameEng IS NUll THEN tm.Name When tm.FullNameEng = '' then tm.Name Else tm.FullNameEng END as TeamMemberName
                        , tm.Pin as TeamMemberPin
                        , org.ShortName as OrganizationName
                        , de.Name as DepartmentName
                        , desg.Name as DesignationName
                        , br.Name as BranchName
                        , cam.Name as CampusName
                        , caemh.EmployeeStatus as EmployeeStatus
                        , caemh.EffectiveDate as EmployeeEffectiveDate
                        , cswh.CurrentShiftEffectiveDate
                        , cswh.CurrentShiftName
                        , cswh.CurrentShiftStartTime
                        , cswh.CurrentShiftEndTime
                        , fswh.NextShiftEffectiveDate
                        , fswh.NextShiftName
                        , fswh.NextShiftStartTime
                        , fswh.NextShiftEndTime
                        from HR_TeamMember as tm 
                        CROSS APPLY  (
	                        Select * from (
		                        Select 
		                         Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		                        , emh.EmploymentStatus as EmployeeStatus
		                        , emh.EffectiveDate As EffectiveDate
		                        , emh.CampusId as CampusId
		                        , emh.DepartmentId as DepartmentId
		                        , emh.DesignationId as DesignationId
		                        from HR_EmploymentHistory as emh
		                        where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		                        and  emh.EffectiveDate <=  " + searchingEffectiveDateWiseFilter + @"
		                        and emh.TeamMemberId = tm.Id
	                        ) as employmentHistory 
	                        where 1=1
	                        and employmentHistory.r = 1
                        ) as caemh
                        inner join HR_Department as de on de.Status = " + Department.EntityStatus.Active + @" and de.Id = caemh.DepartmentId
                        inner join HR_Designation as desg on desg.Status = " + Designation.EntityStatus.Active + @" and desg.Id = caemh.DesignationId
                        inner join Campus as cam on cam.Status = " + Campus.EntityStatus.Active + @" and cam.Id = caemh.CampusId
                        inner join Branch as br on br.Status = " + Branch.EntityStatus.Active + @" and br.Id = cam.BranchId
                        inner join Organization as org on org.Status = " + Organization.EntityStatus.Active + @" and de.OrganizationId = org.Id
                        Inner join (
	                        Select 
	                        Rank() Over(Partition by swh.TeamMemberId order by swh.EffectiveDate DESC, swh.Id DESC) AS rsh1
	                        , swh.EffectiveDate as CurrentShiftEffectiveDate
	                        , swh.ShiftId as CurrentShiftId
	                        , swh.Weekend as CurrentWeekend
	                        , swh.TeamMemberId as TeamMemberId
	                        , sf.Name as CurrentShiftName
	                        , sf.StartTime as CurrentShiftStartTime
	                        , sf.EndTime as CurrentShiftEndTime
	                        from HR_ShiftWeekendHistory swh
	                        inner Join HR_Shift as sf on sf.Status = " + Shift.EntityStatus.Active + @" and sf.Id = swh.ShiftId
	                        where swh.Status = " + ShiftWeekendHistory.EntityStatus.Active + @"
	                        and  swh.EffectiveDate <=  " + searchingEffectiveDateWiseFilter + @"
                        ) as cswh on cswh.TeamMemberId = tm.Id and cswh.rsh1 = 1
                        left join (
	                        Select 
	                        Rank() Over(Partition by swh.TeamMemberId order by swh.EffectiveDate ASC, swh.Id ASC) AS fsh1
	                        , swh.EffectiveDate as NextShiftEffectiveDate
	                        , swh.ShiftId as NextShiftId
	                        , swh.Weekend as NextWeekend
	                        , swh.TeamMemberId as TeamMemberId
	                        , sf.Name as NextShiftName
	                        , sf.StartTime as NextShiftStartTime
	                        , sf.EndTime as NextShiftEndTime
	                        from HR_ShiftWeekendHistory swh
	                        inner Join HR_Shift as sf on sf.Status = " + Shift.EntityStatus.Active + @" and sf.Id = swh.ShiftId
	                        where swh.Status = " + ShiftWeekendHistory.EntityStatus.Active + @"
	                        and  swh.EffectiveDate >  " + searchingEffectiveDateWiseFilter + @"
                        ) as fswh on fswh.TeamMemberId = tm.Id and fswh.fsh1 = 1

                        Where 1=1
                        and caemh.EmployeeStatus != " + (int)MemberEmploymentStatus.Retired + branchFilter + campusFilter + employmentStatusFilter + departmentFilter + designationFilter + shiftFilter + shiftEffectiveDateFilter + @"
                        Order By tm.Pin
                        ";

            return query;
        }

        private string GetQueryForTeamMemberWithLastNextSalaryHistoryInfor(List<long> branchIdList, List<long> campusIdList, List<int> employmentStatusList, List<long> departmntIdList, List<long> designationIdList, bool isSearchOnEmploymetHistory)
        {
            string branchFilter = "";
            string campusFilter = "";
            string employmentStatusFilter = "";
            string departmentFilter = "";
            string designationFilter = "";
            if (isSearchOnEmploymetHistory)
            {
                if (branchIdList != null && branchIdList.Any() && !branchIdList.Contains(SelectionType.SelelectAll))
                {
                    branchFilter += @" AND br.Id IN (" + string.Join(",", branchIdList) + ") ";
                }
                if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
                {
                    campusFilter += @" AND cam.Id IN (" + string.Join(",", campusIdList) + ") ";
                }
                if (employmentStatusList != null && employmentStatusList.Any() &&
                    !employmentStatusList.Contains(SelectionType.SelelectAll))
                {
                    employmentStatusFilter += @" AND caemh.EmployeeStatus IN (" + string.Join(",", employmentStatusList) +
                                              ") ";
                }
                if (departmntIdList != null && departmntIdList.Any() &&
                    !departmntIdList.Contains(SelectionType.SelelectAll))
                {
                    departmentFilter += @" AND de.Id IN (" + string.Join(",", departmntIdList) + ") ";
                }
                if (designationIdList != null && designationIdList.Any() &&
                    !designationIdList.Contains(SelectionType.SelelectAll))
                {
                    designationFilter += @" AND desg.Id IN (" + string.Join(",", designationIdList) + ") ";
                }
            }
            else
            {
                if (branchIdList != null && branchIdList.Any() && !branchIdList.Contains(SelectionType.SelelectAll))
                {
                    branchFilter += @" AND lash.LastSalaryHistoryBranchId IN (" + string.Join(",", branchIdList) + ") ";
                }
                if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
                {
                    campusFilter += @" AND lash.LastSalaryHistoryCampusId IN (" + string.Join(",", campusIdList) + ") ";
                }
                if (departmntIdList != null && departmntIdList.Any() && !departmntIdList.Contains(SelectionType.SelelectAll))
                {
                    departmentFilter += @" AND lash.LastSalaryHistoryDepartmentId IN (" + string.Join(",", departmntIdList) + ") ";
                }
            }
            string query = "";
            query = @"Select 
                        tm.Id as TeamMemberId
                        , CASE WHEN tm.FullNameEng IS NUll THEN tm.Name When tm.FullNameEng = '' then tm.Name Else tm.FullNameEng END as TeamMemberName
                        , tm.Pin as TeamMemberPin
                        , org.ShortName as OrganizationName
                        , org.Id as OrganizationId
                        , de.Name as DepartmentName
                        , desg.Name as DesignationName
                        , br.Name as BranchName
                        , cam.Name as CampusName
                        , caemh.EmployeeStatus as EmployeeStatus
                        , caemh.EffectiveDate as EmployeeEffectiveDate
                        , lash.LastSalaryHistoryId as LastSalaryHistoryId
                        , lash.LastSalaryHistoryOrganizationId as LastSalaryHistoryOrganizationId
                        , lash.LastSalaryHistoryOrganizationName as LastSalaryHistoryOrganizationName
                        , lash.LastSalaryHistoryBranchId as LastSalaryHistoryBranchId
                        , lash.LastSalaryHistoryBranchName as LastSalaryHistoryBranchName
                        , lash.LastSalaryHistoryCampusId as LastSalaryHistoryCampusId
                        , lash.LastSalaryHistoryCampusName as LastSalaryHistoryCampusName
                        , lash.LastSalaryHistoryDepartmentId as LastSalaryHistoryDepartmentId
                        , lash.LastSalaryHistoryDepartmentName as LastSalaryHistoryDepartmentName
						, lash.LastSalaryHistoryDesignationId as LastSalaryHistoryDesignationId --
						, lash.LastSalaryHistoryDesignationName as LastSalaryHistoryDesignationName --
                        , lash.LastSalaryHistorySalary as LastSalaryHistorySalary
                        , lash.LastSalaryHistoryBankAmount as LastSalaryHistoryBankAmount
                        , lash.LastSalaryHistoryCashAmount as LastSalaryHistoryCashAmount
                        , lash.LastSalaryHistorySalaryPurpose as LastSalaryHistorySalaryPurpose
                        , lash.LastSalaryHistoryEffectiveDate as LastSalaryHistoryEffectiveDate
                        , nxsh.NextSalaryHistoryId as NextSalaryHistoryId
                        , nxsh.NextSalaryHistoryOrganizationId as NextSalaryHistoryOrganizationId
                        , nxsh.NextSalaryHistoryOrganizationName as NextSalaryHistoryOrganizationName
                        , nxsh.NextSalaryHistoryBranchId as NextSalaryHistoryBranchId
                        , nxsh.NextSalaryHistoryBranchName as NextSalaryHistoryBranchName
                        , nxsh.NextSalaryHistoryCampusId as NextSalaryHistoryCampusId
                        , nxsh.NextSalaryHistoryCampusName as NextSalaryHistoryCampusName
                        , nxsh.NextSalaryHistoryDepartmentId as NextSalaryHistoryDepartmentId
                        , nxsh.NextSalaryHistoryDepartmentName as NextSalaryHistoryDepartmentName
                        , nxsh.NextSalaryHistorySalary as NextSalaryHistorySalary
                        , nxsh.NextSalaryHistoryBankAmount as NextSalaryHistoryBankAmount
                        , nxsh.NextSalaryHistoryCashAmount as NextSalaryHistoryCashAmount
                        , nxsh.NextSalaryHistorySalaryPurpose as NextSalaryHistorySalaryPurpose
                        , nxsh.NextSalaryHistoryEffectiveDate as NextSalaryHistoryEffectiveDate
                        from HR_TeamMember as tm 
                        CROSS APPLY  (
	                        Select * from (
		                        Select 
		                         Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		                        , emh.EmploymentStatus as EmployeeStatus
		                        , emh.EffectiveDate As EffectiveDate
		                        , emh.CampusId as CampusId
		                        , emh.DepartmentId as DepartmentId
		                        , emh.DesignationId as DesignationId
		                        from HR_EmploymentHistory as emh
		                        where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		                        and  emh.EffectiveDate <=  getdate()
		                        and emh.TeamMemberId = tm.Id
	                        ) as employmentHistory 
	                        where 1=1
	                        and employmentHistory.r = 1
                        ) as caemh
                        inner join HR_Department as de on de.Status = 1 and de.Id = caemh.DepartmentId
                        inner join HR_Designation as desg on desg.Status = 1 and desg.Id = caemh.DesignationId
                        inner join Campus as cam on cam.Status = 1 and cam.Id = caemh.CampusId
                        inner join Branch as br on br.Status = 1 and br.Id = cam.BranchId
                        inner join Organization as org on org.Status = 1 and de.OrganizationId = org.Id
                        left join (
	                        Select 
	                        Rank() Over(Partition by sh.TeamMemberId order by sh.EffectiveDate DESC, sh.Id DESC) AS rsh1
	                        , sh.EffectiveDate as LastSalaryHistoryEffectiveDate
	                        , sh.Id as LastSalaryHistoryId
	                        , sh.SalaryOrganizationId as LastSalaryHistoryOrganizationId
	                        , org.ShortName as LastSalaryHistoryOrganizationName
	                        , sh.SalaryDepartmentId as LastSalaryHistoryDepartmentId
	                        , de.Name as LastSalaryHistoryDepartmentName
							, sh.SalaryDesignationId as LastSalaryHistoryDesignationId --
							, desig.Name as LastSalaryHistoryDesignationName --
	                        , cam.BranchId as LastSalaryHistoryBranchId
	                        , br.Name as LastSalaryHistoryBranchName
	                        , sh.SalaryCampusId as LastSalaryHistoryCampusId
	                        , cam.Name as LastSalaryHistoryCampusName
	                        , sh.Salary as LastSalaryHistorySalary
	                        , sh.BankAmount as LastSalaryHistoryBankAmount
	                        , sh.CashAmount as LastSalaryHistoryCashAmount
	                        , sh.SalaryPurpose as LastSalaryHistorySalaryPurpose
	                        , sh.TeamMemberId as TeamMemberId
	                        from HR_SalaryHistory sh
	                        inner join HR_Department as de on de.Status = " + Department.EntityStatus.Active + @" and de.Id = sh.SalaryDepartmentId
                            inner join HR_Designation as desig on desig.Status = " + Designation.EntityStatus.Active + @" and desig.Id = sh.SalaryDesignationId --
	                        inner join Campus as cam on cam.Status = " + Campus.EntityStatus.Active + @" and cam.Id = sh.SalaryCampusId
	                        inner join Branch as br on br.Status = " + Branch.EntityStatus.Active + @" and br.Id = cam.BranchId
	                        inner join Organization as org on org.Status = " + Organization.EntityStatus.Active + @" and sh.SalaryOrganizationId = org.Id
	                        where sh.Status = " + SalaryHistory.EntityStatus.Active + @"
	                        and  sh.EffectiveDate <=  getdate()
                        ) as lash on lash.TeamMemberId = tm.Id and lash.rsh1 = 1
                        left join (
	                        Select 
	                        Rank() Over(Partition by sh.TeamMemberId order by sh.EffectiveDate DESC, sh.Id DESC) AS rsh1
	                        , sh.EffectiveDate as NextSalaryHistoryEffectiveDate
	                        , sh.Id as NextSalaryHistoryId
	                        , sh.SalaryOrganizationId as NextSalaryHistoryOrganizationId
	                        , org.ShortName as NextSalaryHistoryOrganizationName
	                        , sh.SalaryDepartmentId as NextSalaryHistoryDepartmentId
	                        , de.Name as NextSalaryHistoryDepartmentName
                            , sh.SalaryDesignationId as NextSalaryHistoryDesignationId --
							, desig.Name as NextSalaryHistoryDesignationName --
	                        , cam.BranchId as NextSalaryHistoryBranchId
	                        , br.Name as NextSalaryHistoryBranchName
	                        , sh.SalaryCampusId as NextSalaryHistoryCampusId
	                        , cam.Name as NextSalaryHistoryCampusName
	                        , sh.Salary as NextSalaryHistorySalary
	                        , sh.BankAmount as NextSalaryHistoryBankAmount
	                        , sh.CashAmount as NextSalaryHistoryCashAmount
	                        , sh.SalaryPurpose as NextSalaryHistorySalaryPurpose
	                        , sh.TeamMemberId as TeamMemberId
	                        from HR_SalaryHistory sh
	                        inner join HR_Department as de on de.Status = " + Department.EntityStatus.Active + @" and de.Id = sh.SalaryDepartmentId
                            inner join HR_Designation as desig on desig.Status = " + Designation.EntityStatus.Active + @" and desig.Id = sh.SalaryDesignationId --
	                        inner join Campus as cam on cam.Status = " + Campus.EntityStatus.Active + @" and cam.Id = sh.SalaryCampusId
	                        inner join Branch as br on br.Status = " + Branch.EntityStatus.Active + @" and br.Id = cam.BranchId
	                        inner join Organization as org on org.Status = " + Organization.EntityStatus.Active + @" and sh.SalaryOrganizationId = org.Id
	                        where sh.Status = " + SalaryHistory.EntityStatus.Active + @"
	                        and  sh.EffectiveDate >  getdate()
                        ) as nxsh on nxsh.TeamMemberId = tm.Id and nxsh.rsh1 = 1

                        Where 1=1
                        and caemh.EmployeeStatus != " + (int)MemberEmploymentStatus.Retired + branchFilter + campusFilter + employmentStatusFilter + departmentFilter + designationFilter + @"
                        Order By tm.Pin
                        ";

            return query;
        }

        private string GetTeamMemberForLeaveSummaryQuery(DateTime searchingEffectiveDate, List<long> branchIdList, List<long> campusIdList, List<long> departmntIdList, List<long> designationIdList, List<int> pinList)
        {
            string branchFilter = "";
            string campusFilter = "";
            string departmentFilter = "";
            string designationFilter = "";
            string searchingEffectiveFilter = @" AND emh.EffectiveDate <=  '" + searchingEffectiveDate.Date.ToString("yyyy-MM-dd 23:59:59") + "'";
            string pinFilter = "";
            if (branchIdList != null && branchIdList.Any() && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                branchFilter = @" AND br.Id IN (" + string.Join(",", branchIdList) + ") ";
            }
            if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
            {
                campusFilter = @" AND cam.Id IN (" + string.Join(",", campusIdList) + ") ";
            }
            if (departmntIdList != null && departmntIdList.Any() && !departmntIdList.Contains(SelectionType.SelelectAll))
            {
                departmentFilter = @" AND de.Id IN (" + string.Join(",", departmntIdList) + ") ";
            }
            if (designationIdList != null && designationIdList.Any() && !designationIdList.Contains(SelectionType.SelelectAll))
            {
                designationFilter = @" AND desg.Id IN (" + string.Join(",", designationIdList) + ") ";
            }
            if (pinList != null && pinList.Any())
            {
                pinFilter = @" AND tm.Pin IN (" + string.Join(",", pinList) + ") ";
            }
            string query = "";
            query = @"Select 
tm.Id as TeamMemberId
, tm.Name as NickName
, tm.Pin as Pin
from HR_TeamMember as tm 
CROSS APPLY  (
	Select * from (
		Select 
		    Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		, emh.EmploymentStatus as EmployeeStatus
		, emh.EffectiveDate As EffectiveDate
		, emh.CampusId as CampusId
		, emh.DepartmentId as DepartmentId
		, emh.DesignationId as DesignationId
		from HR_EmploymentHistory as emh
		where emh.Status = 1
		" + searchingEffectiveFilter + @"
		and emh.TeamMemberId = tm.Id
	) as employmentHistory 
	where 1=1
	and employmentHistory.r = 1
) as caemh
inner join HR_Department as de on de.Id = caemh.DepartmentId
inner join HR_Designation as desg on desg.Id = caemh.DesignationId
inner join Campus as cam on cam.Id = caemh.CampusId
inner join Branch as br on br.Id = cam.BranchId
inner join Organization as org on de.OrganizationId = org.Id
Where 1=1
and caemh.EmployeeStatus <> " + (int)MemberEmploymentStatus.Retired + @"
" + branchFilter + campusFilter + departmentFilter + designationFilter + pinFilter + @"
";

            return query;
        }

        #endregion
    }
}
