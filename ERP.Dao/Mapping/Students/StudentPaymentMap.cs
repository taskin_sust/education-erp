using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students {


    public partial class StudentPaymentMap : BaseClassMap<StudentPayment, long>
    {
        
        public StudentPaymentMap() {
			Table("StudentPayment");
			LazyLoad();

            Map(x => x.PayableAmount).Column("PayableAmount");
			Map(x => x.ReceivedAmount).Column("ReceivedAmount");
			Map(x => x.DiscountAmount).Column("DiscountAmount");
			Map(x => x.SpDiscountAmount).Column("SpDiscountAmount");
            Map(x => x.SpReferenceNote).Column("SpReferenceNote");
			Map(x => x.DueAmount).Column("DueAmount");
			Map(x => x.PaymentMethod).Column("PaymentMethod");
			Map(x => x.ReceivedDate).Column("ReceivedDate");
			Map(x => x.NextReceivedDate).Column("NextReceivedDate");
            Map(x => x.ReceiptNo).Column("ReceiptNo");
            Map(x => x.CashBackAmount).Column("CashBackAmount");
            Map(x => x.ConsiderationAmount).Column("ConsiderationAmount");
            Map(x => x.CourseFees).Column("CourseFees");
            Map(x => x.Remarks).Column("Remarks");
            Map(x => x.PaymentType).Column("PaymentType");

            Map(x => x.CampusReceiveDate).Column("CampusReceiveDate");
            Map(x => x.CampusReceiveBy).Column("CampusReceiveBy");
            References(x => x.CampusReceiveFrom).Column("CampusIdReceiveFrom").Nullable();


            References(x => x.StudentProgram).Column("StudentProgramId");
            References(x => x.Referrer).Column("SpReferenceId");

            References(x => x.Batch).Column("CurrentBatchId");
            HasMany(x => x.OnlineTransactionses).KeyColumn("StudentPaymentId").Cascade.AllDeleteOrphan().Inverse();

            HasManyToMany(x => x.CourseSubjectList).Table("StudentPaymentCourseSubject");
        }
    }
}
