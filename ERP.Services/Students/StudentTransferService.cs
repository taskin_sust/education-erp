﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IStudentTransferService : IBaseService
    {
        #region Operational Function
        bool Save(StudentTransfer studentTransfer);

        #endregion

        #region Single Instances Loading Function
        StudentTransfer LoadById(long id);
        #endregion

        #region List Loading Function

        IList<StudentTransfer> LoadStudentTransferByCriteria(int start, int length, long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string prnNo = "", string name = "", string mobile = "",DateTime? dateFrom=null,DateTime? dateTo=null);
        IList<StudentTransfer> LoadStudentTransferByCriteriaCount(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus,  long[] selectedToBranch, long[] selectedToCampus,
            string prnNo = "", string name = "", string mobile = "",DateTime? dateFrom=null,DateTime? dateTo=null);
        IList<StudentTransfer> LoadStudentTransferSettleByCriteria(int start, int length, long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string dateFrom, string dateTo, string prnNo, string name, string mobile);
        IList<StudentTransfer> LoadStudentTransferSettleByCriteriaCount(long programId, long sessionId,
            long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string dateFrom, string dateTo,
            string prnNo = "", string name = "", string mobile = "");
        #endregion

        #region Others Function

        #endregion

        void Update(long[] transferIds);


        decimal GetTotalPayable(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, string prnNo, string name, string mobile,DateTime dateFrom,DateTime dateTo);

        decimal GetTotalReceivable(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo);

        StudentTransfer GetPreviousTransferInfo(long studentProgramId);
        IList<StudentTransferReportDto> LoadStudentTransferReport(int start, int length, long programId, long sessionId, long[] fromBranchIds, long[] selectedFromCampus, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo);

        int LoadStudentTransferReportCount(long programId, long sessionId, long[] fromBranchIds, long[] p, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo);

        void GetOvetAllPayableReceivable(long programId, long sessionId, long[] fromBranchIds, long[] campusIds,string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo, out decimal overallPayable, out decimal overallReceivable);
    }

    public class StudentTransferService : BaseService, IStudentTransferService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentTransferDao _studentTransferDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IStudentBatchLogDao _studentBatchLogDao;
        public StudentTransferService(ISession session)
        {
            Session = session;
            _studentTransferDao = new StudentTransferDao() { Session = session };
            _studentProgramDao = new StudentProgramDao() { Session = session };
            _studentBatchLogDao = new StudentBatchLogDao { Session = session };
        }
        #endregion

        #region Operational Function
        public bool Save(StudentTransfer studentTransfer)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    _studentTransferDao.Save(studentTransfer);
                    StudentProgram studentProgram = _studentProgramDao.LoadById(studentTransfer.StudentProgram.Id);
                    studentProgram.Batch = studentTransfer.ToBatch;
                    _studentProgramDao.Update(studentProgram);

                    //StudentBatchLog
                    StudentBatchLog studentBatchLog = new StudentBatchLog();
                    studentBatchLog.FromBatch = studentTransfer.FromBatch;
                    studentBatchLog.ToBatch = studentTransfer.ToBatch;
                    studentBatchLog.StudentProgram = studentProgram;
                    studentBatchLog.TransferDate = DateTime.Now;
                    _studentBatchLogDao.Save(studentBatchLog);
                    //StudentBatchLog End

                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                {
                    transaction.Rollback();
                    _logger.Error(ex);
                    return false;
                }
            }
            return false;
        }
        
        public void Update(long[] transferIds)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var transferId in transferIds)
                    {
                        StudentTransfer studentTransfer = _studentTransferDao.LoadById(transferId);
                        studentTransfer.IsSettled = true;
                        _studentTransferDao.SaveOrUpdate(studentTransfer);
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
               
            }
        }

        public IList<StudentTransfer> LoadStudentTransferSettleByCriteria(int start, int length, long programId, long sessionId,
            long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus,
            string dateFrom, string dateTo, string prnNo, string name, string mobile)
        {
            try
            {
                return _studentTransferDao.LoadStudentTransferSettleByCriteria(start, length, programId, sessionId, selectedFromBranch, selectedFromCampus, selectedToBranch,
                    selectedToCampus, dateFrom, dateTo, prnNo, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<StudentTransfer> LoadStudentTransferSettleByCriteriaCount(long programId, long sessionId, long[] selectedFromBranch,
            long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string dateFrom, string dateTo,
            string prnNo = "", string name = "", string mobile = "")
        {
            try
            {
               return _studentTransferDao.LoadStudentTransferSettleByCriteriaCount( programId,sessionId, selectedFromBranch,
                selectedFromCampus,  selectedToBranch, selectedToCampus, dateFrom, dateTo,prnNo, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public StudentTransfer LoadById(long id)
        {
            try
            {
                return _studentTransferDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        #endregion

        #region List Loading Function
        public IList<StudentTransfer> LoadStudentTransferByCriteria(int start, int length, long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string prnNo = "", string name = "", string mobile = "",DateTime? dateFrom=null,DateTime? dateTo=null)
        {
            try
            {
                return _studentTransferDao.LoadStudentTransferByCriteria(start, length, programId, sessionId, selectedFromBranch, selectedFromCampus,selectedToBranch,selectedToCampus, prnNo, name, mobile,dateFrom,dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<StudentTransfer> LoadStudentTransferByCriteriaCount(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string prnNo = "", string name = "", string mobile = "", DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            try
            {
                return _studentTransferDao.LoadStudentTransferByCriteriaCount(programId, sessionId, selectedFromBranch, selectedFromCampus, selectedToBranch, selectedToCampus, prnNo, name, mobile,dateFrom,dateTo);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        
        #endregion

        #region Others Function

        public decimal GetTotalPayable(long programId, long sessionId, long[] selectedFromBranch,
            long[] selectedFromCampus, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo)
        {
            return _studentTransferDao.GetTotalPayable(programId, sessionId,
                        selectedFromBranch, selectedFromCampus, prnNo, name, mobile,dateFrom,dateTo);
        }

        public decimal GetTotalReceivable(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus,
            string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo)
        {
            return _studentTransferDao.GetTotalReceivable(programId, sessionId,
                        selectedFromBranch, selectedFromCampus, prnNo, name, mobile,dateFrom,dateTo);
        }
        #endregion

        public StudentTransfer GetPreviousTransferInfo(long studentProgramId)
        {
           return _studentTransferDao.GetPreviousTransferInfo(studentProgramId);
        }

        public IList<StudentTransferReportDto> LoadStudentTransferReport(int start, int length, long programId, long sessionId,
            long[] fromBranchIds, long[] selectedFromCampus, string prnNo, string name, string mobile,
            DateTime dateFrom, DateTime dateTo)
        {
            return _studentTransferDao.LoadStudentTransferReport(start, length, programId, sessionId,
                fromBranchIds, selectedFromCampus, prnNo, name, mobile,
                dateFrom, dateTo);
        }

        public int LoadStudentTransferReportCount(long programId, long sessionId, long[] fromBranchIds, long[] campusIds, string prnNo, string name, string mobile, DateTime dateFrom,
            DateTime dateTo)
        {
            return _studentTransferDao.LoadStudentTransferReportCount(programId, sessionId, fromBranchIds, campusIds, prnNo,  name,  mobile, dateFrom,
                dateTo);
        }

        public void GetOvetAllPayableReceivable(long programId, long sessionId, long[] fromBranchIds, long[] campusIds,string prnNo, string name, string mobile,
            DateTime dateFrom, DateTime dateTo, out decimal overallPayable, out decimal overallReceivable)
        {
            _studentTransferDao.GetOvetAllPayableReceivable(programId, sessionId, fromBranchIds, campusIds,prnNo,name,mobile,
                dateFrom, dateTo,out overallPayable,out overallReceivable);
        }

        #region Helper Function

        #endregion
    }
}
