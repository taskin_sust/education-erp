﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using FluentNHibernate.Utils;
using iTextSharp.text.pdf.qrcode;
using log4net;
using Microsoft.AspNet.Identity;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;
using System.Collections.Specialized;
using System.Globalization;
using UdvashERP.Services.Hr.AttendanceSummaryServices;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class AttendanceReportController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ICampusService _campusService;
        private readonly IBranchService _branchService;
        private readonly ITeamMemberService _hrMemberService;
        private readonly ILeaveApplicationService _hrLeaveApplicationService;
        private readonly IHolidaySettingService _hrHolidayService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IAttendanceSummaryService _attendanceSummaryService;
        private readonly AttendanceSummaryServiceHelper _attendanceSummaryServiceHelper;
        private readonly IDepartmentService _departmentService;
        private readonly IDesignationService _designationService;
        private readonly IMentorHistoryService _mentorHistoryService;
        private readonly IAttendanceDeviceRawDataService _attendanceDeviceRawDataService;
        private List<UserMenu> _userMenu;
        public AttendanceReportController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _campusService = new CampusService(session);
            _branchService = new BranchService(session);
            _userService = new UserService(session);
            _hrMemberService = new TeamMemberService(session);
            _hrHolidayService = new HolidaySettingService(session);
            _hrLeaveApplicationService = new LeaveApplicationService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _attendanceSummaryService = new AttendanceSummaryService(session);
            _attendanceSummaryServiceHelper = new AttendanceSummaryServiceHelper(session);
            _departmentService = new DepartmentService(session);
            _designationService = new DesignationService(session);
            _mentorHistoryService = new MentorHistoryService(session);
            _attendanceDeviceRawDataService = new AttendanceDeviceRawDataService(session);
        }
        #endregion

        #region Attendance Summery Report
        public ActionResult AttendanceSummeryReport()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;

            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.CampusList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.DepartmentList = new SelectList(string.Empty, "Value", "Text");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }

            return View();
        }
        [HttpPost]
        public ActionResult AttendanceSummeryReport(AttendanceSummeryReportViewModel attendanceSummeryReportViewModel)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<string> reportHeadInfo = new List<string>();
            try
            {


                if (attendanceSummeryReportViewModel != null)
                {
                    if (attendanceSummeryReportViewModel.InformationViewList == null)
                    {
                        var infoView = new List<string>(new[] { "Pin", "Name", "Department", "Designation", "Leave Count", "No. of Extra Day" });
                        attendanceSummeryReportViewModel.InformationViewList = infoView.ToArray();
                    }
                    //Organization Name 
                    if (!String.IsNullOrEmpty(attendanceSummeryReportViewModel.Organization))
                    {
                        var o = _organizationService.LoadById(Convert.ToInt64(attendanceSummeryReportViewModel.Organization));
                        reportHeadInfo.Add(o != null ? o.Name : "All");
                    }
                    else
                        reportHeadInfo.Add("All");

                    //Branch Name 
                    if (!String.IsNullOrEmpty(attendanceSummeryReportViewModel.Branch))
                    {
                        var b = _branchService.GetBranch(Convert.ToInt64(attendanceSummeryReportViewModel.Branch));
                        reportHeadInfo.Add(b != null ? b.Name : "All");
                    }
                    else
                        reportHeadInfo.Add("All");


                    //Department Name 
                    if (!String.IsNullOrEmpty(attendanceSummeryReportViewModel.Department))
                    {
                        var d = _departmentService.LoadById(Convert.ToInt64(attendanceSummeryReportViewModel.Department));
                        reportHeadInfo.Add(d != null ? d.Name : "All");
                    }
                    else
                        reportHeadInfo.Add("All");

                    //Campus Name 
                    if (!String.IsNullOrEmpty(attendanceSummeryReportViewModel.Campus))
                    {
                        var c = _campusService.GetCampus(Convert.ToInt64(attendanceSummeryReportViewModel.Campus));
                        reportHeadInfo.Add(c != null ? c.Name : "All");
                    }
                    else
                        reportHeadInfo.Add("All");

                    reportHeadInfo.Add(attendanceSummeryReportViewModel.DateFrom.ToString("yyyy-MM-dd"));
                    reportHeadInfo.Add(attendanceSummeryReportViewModel.DateTo.ToString("yyyy-MM-dd"));


                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            ViewBag.ReportHeadInfo = reportHeadInfo;
            return View("AttendanceSummeryReportTable", attendanceSummeryReportViewModel);
        }

        #region Render Data Table Student Info Search

        [HttpPost]
        public JsonResult AttendanceSummeryReportAjaxRequest(int draw, int start, int length, string organization, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    #region OrderBy and Direction

                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                    }

                    #endregion

                    #region Authorization

                    var _userMenu = (List<UserMenu>) ViewBag.UserMenu;

                    #endregion

                    int recordsTotal = _attendanceSummaryService.GetAttendanceSummeryReportCount(_userMenu, organization,
                        department, branch, informationViewList, campus, dateFrom, dateTo, pin);
                    long recordsFiltered = recordsTotal;

                    IList<AttendanceSummeryReportDto> attendanceSummeryReportDtoList =
                        _attendanceSummaryService.LoadAttendanceSummeryReport(_userMenu, organization, department,
                            branch, informationViewList, campus, dateFrom, dateTo, pin, start, length, orderBy,
                            orderDir.ToUpper()).ToList();

                    var data = new List<object>();
                    int sl = start + 1;

                    foreach (var c in attendanceSummeryReportDtoList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Department":
                                        str.Add(c.Department);
                                        break;
                                    case "Pin":
                                        str.Add(c.Pin.ToString());
                                        break;
                                    case "Name":
                                        str.Add(c.Name);
                                        break;
                                    case "Designation":
                                        str.Add(c.Designation);
                                        break;
                                        //case "Member Type":
                                        //    string memberType = ((MemberEmploymentStatus)c.MemberType).ToString();
                                        //    str.Add(memberType);
                                        //    break;
                                    case "Early Entry(hr)":
                                        str.Add(c.EarlyEntry.FormatSecondToHourMin());
                                        break;
                                    case "Late Leave(hr)":
                                        str.Add(c.LateLeave.FormatSecondToHourMin());
                                        break;
                                    case "Total Overtime(hr)":
                                        str.Add(c.TotalOvertime.FormatSecondToHourMin());
                                        break;
                                    case "Approved Overtime(hr)":
                                        str.Add(c.ApprovedOvertime.FormatSecondToHourMin());
                                        break;
                                    case "Total Late Entry(hr)":
                                        str.Add(c.TotalLateEntry.FormatSecondToHourMin());
                                        break;
                                    case "No. of Late Entry(Day)":
                                        str.Add(c.NoofLateEntry.ToString());
                                        break;
                                    case "Total Early Leave(hr)":
                                        str.Add(c.TotalEarlyLeave.FormatSecondToHourMin());
                                        break;
                                    case "No. of Early Leave(Day)":
                                        str.Add(c.NoofEarlyLeave.ToString());
                                        break;
                                    case "Zone":
                                        str.Add(c.AbsentCount < 1 ? c.Zone : "");
                                        break;
                                    case "ZoneNameWithCount":
                                        str.Add(c.ZoneNameWithCount ?? "");
                                        break;

                                    case "Absent Count":
                                        str.Add(c.AbsentCount.ToString());
                                        break;
                                    case "Leave Count":
                                        str.Add(c.LeaveCount.ToString());
                                        break;
                                    case "Leave Details":
                                        str.Add(c.LeaveDetails);
                                        break;

                                    case "No. of Working Day":
                                        str.Add(c.NoofWorkingDay.ToString());
                                        break;
                                    case "No. of Extra Day":
                                        str.Add(c.NoofExtraDay.ToString());
                                        break;
                                    case "Extra Day Details":
                                        str.Add(c.ExtraDayDetails);
                                        break;
                                    case "Holiday Count":
                                        str.Add(c.NoofCalendarLeave.ToString());
                                        break;
                                    case "Total Daily Allowance":
                                        str.Add(c.TotalDailyAllowance.ToString());
                                        break;
                                    case "Total Nightwork":
                                        str.Add(c.TotalNightwork.ToString());
                                        break;
                                    default:
                                        break;
                                }
                            }
                            //str.Add("<a target='_blank' href='" + Url.Action("InformationView", "Information") + "?PrnNo=" + c.PrnNo + "'  class='glyphicon glyphicon-th-list'></a>");
                        }
                        sl++;
                        data.Add(str);
                    }
                    return Json(
                        new
                        {
                            draw = draw,
                            recordsTotal = 10,
                            recordsFiltered = recordsFiltered,
                            start = start,
                            length = length,
                            data = data
                        });
                }
                catch (InvalidDataException ex)
                {
                    return Json(new Response(false, ex.Message));
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occured during display attendance report", ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }

            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }

        #endregion

        #region Export Functionality
        public ActionResult ExportAttendanceSummaryReport(string organization, string department,
            string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin)
        {

            try
            {
                #region Authorization

                var _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                #endregion

                bool columsAdded = false;
                var headerList = new List<string>();
                var columnList = new List<string>();
                var footerList = new List<string>();
                headerList.Add("Attendance Summary Report");

                //Organization Name 
                if (!String.IsNullOrEmpty(organization))
                {
                    var o = _organizationService.LoadById(Convert.ToInt64(organization));
                    headerList.Add("Organization: " + (o != null ? o.Name : "All"));
                }
                else
                    headerList.Add("Organization: All");

                //Branch Name 
                if (!String.IsNullOrEmpty(branch))
                {
                    var b = _branchService.GetBranch(Convert.ToInt64(branch));
                    headerList.Add("Branch: " + (b != null ? b.Name : "All"));
                }
                else
                    headerList.Add("Branch: All");


                ////Department Name 
                //if (!String.IsNullOrEmpty(attendanceSummeryReportViewModel.Department))
                //{
                //    var d = _departmentService.LoadById(Convert.ToInt64(attendanceSummeryReportViewModel.Department));
                //    reportHeadInfo.Add(d != null ? d.Name : "All");
                //}
                //else
                //    reportHeadInfo.Add("All");

                //Campus Name 
                if (!String.IsNullOrEmpty(campus))
                {
                    var c = _campusService.GetCampus(Convert.ToInt64(campus));
                    headerList.Add("Campus: " + (c != null ? c.Name : "All"));
                }
                else
                    headerList.Add("Campus: All");

                headerList.Add(DateTime.Parse(dateFrom).ToString("yyyy-MM-dd") + " to " + DateTime.Parse(dateTo).ToString("yyyy-MM-dd"));
                //  headerList.Add(dateTo);




                footerList.Add("");
                if (informationViewList != null)
                {
                    foreach (string informationView in informationViewList)
                    {
                        switch (informationView)
                        {
                            case "Department":
                                columnList.Add("Department");
                                break;
                            case "Pin":
                                columnList.Add("Pin");
                                break;
                            case "Name":
                                columnList.Add("Name");
                                break;
                            case "Designation":
                                columnList.Add("Designation");
                                break;
                            //case "Member Type":
                            //    string memberType = ((MemberEmploymentStatus)c.MemberType).ToString();
                            //    str.Add(memberType);
                            //    break;
                            case "Early Entry(hr)":
                                columnList.Add("Early Entry(hr)");
                                break;
                            case "Late Leave(hr)":
                                columnList.Add("Late Leave(hr)");
                                break;
                            case "Total Overtime(hr)":
                                columnList.Add("Total Overtime(hr)");
                                break;
                            case "Approved Overtime(hr)":
                                columnList.Add("Approved Overtime(hr)");
                                break;
                            case "Total Late Entry(hr)":
                                columnList.Add("Total Late Entry(hr)");
                                break;
                            case "No. of Late Entry(Day)":
                                columnList.Add("No. of Late Entry(Day)");
                                break;
                            case "Total Early Leave(hr)":
                                columnList.Add("Total Early Leave(hr)");
                                break;
                            case "No. of Early Leave(Day)":
                                columnList.Add("No. of Early Leave(Day)");
                                break;
                            case "Zone":
                                columnList.Add("Zone");
                                break;
                            case "ZoneNameWithCount":
                                columnList.Add("Zone With Count");
                                break;
                            case "Absent Count":
                                columnList.Add("Absent Count");
                                break;
                            case "Leave Count":
                                columnList.Add("Leave Count");
                                break;
                            case "Leave Details":
                                columnList.Add("Leave Details");
                                break;
                            case "No. of Working Day":
                                columnList.Add("No. of Working Day");
                                break;
                            case "No. of Extra Day":
                                columnList.Add("No. of Extra Day");
                                break;
                            case "Extra Day Details":
                                columnList.Add("Extra Day Details");
                                break;
                            case "Holiday Count":
                                columnList.Add("Holiday Count");
                                break;
                            case "Total Daily Allowance":
                                columnList.Add("Total Daily Allowance");
                                break;
                            case "Total Nightwork":
                                columnList.Add("Total Nightwork");
                                break;
                            default:
                                break;
                        }
                    }
                    //str.Add("<a target='_blank' href='" + Url.Action("InformationView", "Information") + "?PrnNo=" + c.PrnNo + "'  class='glyphicon glyphicon-th-list'></a>");
                }
                var data = new List<List<object>>();
                IList<AttendanceSummeryReportDto> attendanceSummeryReportDtoList =
                    _attendanceSummaryService.LoadAttendanceSummeryReport(_userMenu, organization, department,
                        branch, informationViewList, campus, dateFrom, dateTo, pin, 0, 0, "Date",
                        "ASC").ToList();

                foreach (var c in attendanceSummeryReportDtoList)
                {
                    var str = new List<object>();
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case "Department":
                                    str.Add(c.Department);
                                    break;
                                case "Pin":
                                    str.Add(c.Pin.ToString());
                                    break;
                                case "Name":
                                    str.Add(c.Name);
                                    break;
                                case "Designation":
                                    str.Add(c.Designation);
                                    break;
                                //case "Member Type":
                                //    string memberType = ((MemberEmploymentStatus)c.MemberType).ToString();
                                //    str.Add(memberType);
                                //    break;
                                case "Early Entry(hr)":
                                    str.Add(c.EarlyEntry.FormatSecondToHourMin());
                                    break;
                                case "Late Leave(hr)":
                                    str.Add(c.LateLeave.FormatSecondToHourMin());
                                    break;
                                case "Total Overtime(hr)":
                                    str.Add(c.TotalOvertime.FormatSecondToHourMin());
                                    break;
                                case "Approved Overtime(hr)":
                                    str.Add(c.ApprovedOvertime.FormatSecondToHourMin());
                                    break;
                                case "Total Late Entry(hr)":
                                    str.Add(c.TotalLateEntry.FormatSecondToHourMin());
                                    break;
                                case "No. of Late Entry(Day)":
                                    str.Add(c.NoofLateEntry.ToString());
                                    break;
                                case "Total Early Leave(hr)":
                                    str.Add(c.TotalEarlyLeave.FormatSecondToHourMin());
                                    break;
                                case "No. of Early Leave(Day)":
                                    str.Add(c.NoofEarlyLeave.ToString());
                                    break;
                                case "Zone":
                                    str.Add(c.AbsentCount < 1 ? c.Zone : "");
                                    break;
                                case "ZoneNameWithCount":
                                    str.Add(c.ZoneNameWithCount ?? "");
                                    break;
                                case "Absent Count":
                                    str.Add(c.AbsentCount.ToString());
                                    break;
                                case "Leave Count":
                                    str.Add(c.LeaveCount.ToString());
                                    break;
                                case "Leave Details":
                                    str.Add(c.LeaveDetails);
                                    break;
                                case "No. of Working Day":
                                    str.Add(c.NoofWorkingDay.ToString());
                                    break;
                                case "No. of Extra Day":
                                    str.Add(c.NoofExtraDay.ToString());
                                    break;
                                case "Extra Day Details":
                                    str.Add(c.ExtraDayDetails.ToString());
                                    break;
                                case "Holiday Count":
                                    str.Add(c.NoofCalendarLeave.ToString());
                                    break;
                                case "Total Daily Allowance":
                                    str.Add(c.TotalDailyAllowance.ToString());
                                    break;
                                case "Total Nightwork":
                                    str.Add(c.TotalNightwork.ToString());
                                    break;
                                default:
                                    break;
                            }
                        }
                        //str.Add("<a target='_blank' href='" + Url.Action("InformationView", "Information") + "?PrnNo=" + c.PrnNo + "'  class='glyphicon glyphicon-th-list'></a>");

                    }
                    data.Add(str);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, data, footerList, "attendance-summary-report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
            }
            catch (Exception ex)
            {
                _logger.Error("Error occured during display attendance report", ex);

            }
            return View("AutoClose");
        }
        #endregion

        #endregion

        #region Attendance Report
        [HttpGet]
        public ActionResult MyAttendance()
        {
            ViewBag.IsAssignedMember = false;
            ViewBag.HasData = false;
            try
            {
                var currentDay = DateTime.Today;
                var firstDayOfMonth = new DateTime(currentDay.Year, currentDay.Month, 1);
                ViewBag.StartDate = firstDayOfMonth;
                ViewBag.EndDate = currentDay;

                var teamMember = _hrMemberService.GetCurrentMember();
                if (teamMember == null)
                {
                    ViewBag.ErrorMessage = "Your team member account found!";
                    return View();
                }
                else
                {
                    ViewBag.IsAssignedMember = true;
                    ViewBag.TeamMemberId = teamMember.Id;
                    //IList<AttendanceSummary> attendanceDataList = _attendanceSummaryService.ListAttendanceSummary(teamMember, firstDayOfMonth, currentDay)
                    //                                                    .OrderByDescending(x => x.AttendanceDate).ToList();

                    //var totalDays = (int)((currentDay - firstDayOfMonth).TotalDays) + 1;
                    //var dateRange = GetDateRange(firstDayOfMonth, totalDays, 1);
                    AttendanceReportViewModel model = GenerateAttendanceReport(teamMember, firstDayOfMonth, currentDay, false);
                    ViewBag.DataModel = model;

                    if (model != null && model.DataList.Count > 0)
                    {
                        ViewBag.HasData = true;
                    }
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ValidForm = false;
                _logger.Error(ex);
                ViewBag.ErrorMessage = "No Attendance Record Found!";
                //ViewBag.ErrorMessage = ex.ToString();
                return View();
            }
        }

        [HttpPost]
        public ActionResult MyAttendance(DateTime startDate, DateTime endDate)
        {
            try
            {
                ViewBag.StartDate = startDate;
                ViewBag.EndDate = endDate;
                ViewBag.IsAssignedMember = true;
                ViewBag.HasData = false;
                var teamMember = _hrMemberService.GetCurrentMember();
                //var attendanceDataList = _attendanceSummaryService.ListAttendanceSummary(teamMember, startDate, endDate).OrderByDescending(x => x.AttendanceDate).ToList();

                //var totalDays = (int)((endDate - startDate).TotalDays) + 1;
                //var dateRange = GetDateRange(startDate, totalDays, 1);
                AttendanceReportViewModel model = GenerateAttendanceReport(teamMember, startDate, endDate, false);
                if (model != null && model.DataList.Count > 0)
                {
                    ViewBag.HasData = true;
                }
                ViewBag.DataModel = model;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.ToString();
                return View();
            }
        }

        [HttpGet]
        public ActionResult MentorDailyAttendanceReport(string pins = "", string startDate = "", string endDate = "", string employmentStatus = "")
        {
            ViewBag.EmploymentStatusList = new SelectList(_commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int> { (int)MemberEmploymentStatus.Retired }), "Key", "Value");
            DateTime sDate = DateTime.Today;
            DateTime eDate = DateTime.Today;
            try
            {

                try
                {
                    sDate = DateTime.ParseExact(startDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                    eDate = DateTime.ParseExact(endDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                    if (sDate > eDate)
                    {
                        DateTime temp = sDate;
                        sDate = eDate;
                        eDate = temp;
                    }
                }
                catch (Exception ex)
                {
                    sDate = DateTime.Today;
                    eDate = DateTime.Today;
                }

                if (sDate != eDate && pins.Trim() == "")
                {
                    throw new InvalidDataException(" Please Enter a PIN and Search.");
                }
                else if (sDate != eDate)
                {
                    pins = pins.Trim().Split(' ')[0].Trim();
                }

                List<int> TeamMemberPinList = new List<int>();
                List<int> employmentStatusList = new List<int>();
                int empStatus;
                if (!String.IsNullOrEmpty(employmentStatus) && int.TryParse(employmentStatus, out empStatus))
                {
                    employmentStatusList.Add(empStatus);
                }
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                TeamMember mentor = _hrMemberService.GetCurrentMember();
                List<long> mentorTeamMemberIdList = _mentorHistoryService.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), eDate, null, null, false, employmentStatusList);
                var pinList = pins.Trim().Split(' ');
                foreach (var p in pinList)
                {
                    try
                    {
                        if (p.Trim() == "")
                            continue;
                        TeamMemberPinList.Add(Convert.ToInt32(p.Trim()));
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidDataException(" Invalid pin(" + p + "). Please remove and request again.");
                    }
                }

                List<AttendanceReportViewModel> models = new List<AttendanceReportViewModel>();
                if (TeamMemberPinList.Count > 0)
                {
                    foreach (var m in TeamMemberPinList)
                    {
                        TeamMember member = _hrMemberService.GetByPin(m);
                        if (!mentorTeamMemberIdList.Contains(member.Id))
                        {
                            throw new InvalidDataException(" Your are not authorized to access this pin(" + member.Pin + "). Please remove and request again.");
                        }
                        models.Add(GenerateAttendanceReport(member, sDate, eDate, true, true));
                    }
                }
                else
                {
                    foreach (var m in mentorTeamMemberIdList)
                    {
                        TeamMember member = _hrMemberService.LoadById(m);
                        models.Add(GenerateAttendanceReport(member, sDate, eDate, true, true));
                    }
                }

                ViewBag.HasData = false;
                ViewBag.StartDate = sDate;
                ViewBag.EndDate = eDate;
                ViewBag.Pins = pins.Trim();

                if (models != null && models.Count > 0)
                {
                    ViewBag.HasData = true;
                }

                ViewBag.DataModel = models;
                return View();
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            ViewBag.HasData = false;
            ViewBag.StartDate = sDate;
            ViewBag.EndDate = eDate;
            //ViewBag.EmploymentStatusList = new SelectList(string.Empty, "Value", "Text");
            return View();
        }

        [HttpGet]
        public ActionResult HrDailyAttendanceReport(string pins = "", string startDate = "", string organizationId = "", string branchId = "", string campusId = "", string departmentId = "", string designationId = "")
        {
            ViewBag.HasData = false;
            DateTime sDate = DateTime.Today;
            List<long> organizationIdList = null;
            List<long> branchIdList = null;
            List<long> campusIdList = null;
            List<long> departmentIdList = null;
            List<long> designationIdList = null;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "ShortName");
            ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.DepartmentList = new SelectList(new List<Department>(), "Id", "Name");
            ViewBag.DesignationList = new SelectList(new List<Designation>(), "Id", "Name");
            long convertedOrganizationId = 0;
            long convertedBranchId = 0;
            long convertedCampusId = 0;
            long convertedDepartmentId = 0;
            long convertedDesignationId = 0;
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.Pins = pins.Trim();
            try
            {
                try
                {
                    sDate = DateTime.ParseExact(startDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                    sDate = DateTime.Today;
                }

                if (!String.IsNullOrEmpty(organizationId))
                {
                    convertedOrganizationId = Convert.ToInt64(organizationId);
                }

                if (!String.IsNullOrEmpty(branchId))
                {
                    convertedBranchId = Convert.ToInt64(branchId);
                }

                if (!String.IsNullOrEmpty(campusId))
                {
                    convertedCampusId = Convert.ToInt64(campusId);
                    campusIdList = new List<long>() { convertedCampusId };
                }

                if (!String.IsNullOrEmpty(departmentId))
                {
                    convertedDepartmentId = Convert.ToInt64(departmentId);
                    departmentIdList = new List<long>() { convertedDepartmentId };
                }

                if (!String.IsNullOrEmpty(designationId))
                {
                    convertedDesignationId = Convert.ToInt64(designationId);
                    designationIdList = new List<long>() { convertedDesignationId };
                }

                List<Organization> authoOrganizationList = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                //if (convertedOrganizationId == 0 && authoOrganizationList.Any())
                //{
                //    convertedOrganizationId = authoOrganizationList[0].Id;
                //}
                List<Branch> authoBranchList = _branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(convertedOrganizationId), null, null, false).ToList();
                //if (convertedBranchId == 0 && authoBranchList.Any())
                //{
                //    convertedBranchId = authoBranchList[0].Id;
                //}
                List<SelectListItem> branchSelectList = new SelectList(authoBranchList, "Id", "Name", convertedBranchId).ToList();
                List<SelectListItem> campusSelectList = new SelectList(_campusService.LoadAuthorizeCampus(_userMenu, _commonHelper.ConvertIdToList(convertedOrganizationId), null, _commonHelper.ConvertIdToList(convertedBranchId), null, false), "Id", "Name", convertedCampusId).ToList();
                campusSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Campus" });
                List<SelectListItem> departmentSelectList = new SelectList(_departmentService.LoadDepartment(_commonHelper.ConvertIdToList(convertedOrganizationId)), "Id", "Name", convertedDepartmentId).ToList();
                departmentSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Department" });
                List<SelectListItem> designationSelectList = new SelectList(_designationService.LoadDesignation(_commonHelper.ConvertIdToList(convertedOrganizationId)), "Id", "Name", convertedDesignationId).ToList();
                designationSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Designation" });

                ViewBag.OrganizationList = new SelectList(authoOrganizationList, "Id", "ShortName", convertedOrganizationId);
                ViewBag.BranchList = new SelectList(branchSelectList, "Value", "Text", convertedBranchId);
                ViewBag.CampusList = new SelectList(campusSelectList, "Value", "Text", convertedCampusId);
                ViewBag.DepartmentList = new SelectList(departmentSelectList, "Value", "Text", convertedDepartmentId);
                ViewBag.DesignationList = new SelectList(designationSelectList, "Value", "Text", convertedDesignationId);


                organizationIdList = new List<long>() { convertedOrganizationId };
                branchIdList = new List<long>() { convertedBranchId };
                if (startDate.Trim() != "")
                {
                    #region Addendence Data Generation
                    List<int> TeamMemberPinList = new List<int>();
                    var pinList = pins.Split(' ');
                    foreach (var p in pinList)
                    {
                        try
                        {
                            if (p.Trim() == "")
                                continue;
                            TeamMemberPinList.Add(Convert.ToInt32(p.Trim()));
                        }
                        catch (Exception ex)
                        {
                            throw new InvalidDataException(" Invalid pin(" + p + "). Please remove and request again.");
                        }
                    }
                    if (!TeamMemberPinList.Any())
                        TeamMemberPinList.Add((int)SelectionType.SelelectAll);

                    List<TeamMember> teamMemberList = _hrMemberService.LoadHrAuthorizedTeamMember(_userMenu, sDate, organizationIdList, branchIdList, campusIdList, departmentIdList, designationIdList, TeamMemberPinList, false).ToList();
                    List<AttendanceReportViewModel> models = new List<AttendanceReportViewModel>();
                    if (teamMemberList.Any())
                    {
                        foreach (var member in teamMemberList)
                        {
                            models.Add(GenerateAttendanceReport(member, sDate, sDate, true, true));
                        }
                    }

                    if (models != null && models.Count > 0)
                    {
                        ViewBag.HasData = true;
                    }

                    ViewBag.DataModel = models;
                    #endregion
                }
                ViewBag.StartDate = sDate;
                return View();
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            ViewBag.HasData = false;
            ViewBag.StartDate = sDate;
            return View();
        }

        [HttpGet]
        public ActionResult MemberAttendance()
        {
            try
            {
                var endDate = DateTime.Today;
                var startDate = new DateTime(endDate.Year, endDate.Month, 1);

                ViewBag.StartDate = startDate;
                ViewBag.EndDate = endDate;
                ViewBag.HasData = false;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.ToString();
                return View();
            }
        }

        [HttpPost]
        public ActionResult MemberAttendance(int pin, DateTime startDate, DateTime endDate)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var teamMember = _hrMemberService.GetByPin(pin);
                ViewBag.HasData = false;
                ViewBag.StartDate = startDate;
                ViewBag.EndDate = endDate;
                if (teamMember == null)
                {
                    throw new InvalidDataException("Team Member not found for following PIN " + pin.ToString(CultureInfo.CurrentCulture));
                }

                bool authorizedHrTeamMember = _hrMemberService.CheckHrAuthorizedTeamMember(_userMenu, teamMember.Pin);
                if (!authorizedHrTeamMember)
                {
                    throw new InvalidDataException("You are not authorized to access this TeamMember Information.");
                }
                //var dataList = _attendanceSummaryService.ListAttendanceSummary(teamMember, startDate, endDate);

                //var totalDays = (int)((endDate - startDate).TotalDays) + 1;
                //var dateRange = GetDateRange(startDate, totalDays, null);

                AttendanceReportViewModel model = GenerateAttendanceReport(teamMember, startDate, endDate, true, true);

                if (model != null && model.DataList.Count > 0)
                {
                    ViewBag.HasData = true;
                }
                ViewBag.DataModel = model;
                return View();
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                ViewBag.HasData = false;
                ViewBag.StartDate = startDate;
                ViewBag.EndDate = endDate;
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.HasData = false;
                ViewBag.StartDate = startDate;
                ViewBag.EndDate = endDate;
                ViewBag.ErrorMessage = ex.ToString();
                return View();
            }
        }

        #endregion

        #region Delete Attendance

        public ActionResult DeteleAttendance()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {

                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult DeteleAttendance(AttendanceDeleteViewModel attendanceDeleteViewModel)
        {
            ViewBag.InvalidPin = "";
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (ModelState.IsValid)
                {
                    var memberIdList = _hrMemberService.LoadHrAuthorizedTeamMemberId(_userMenu, DateTime.Now.Date,
                        pinList: new List<int>() { Convert.ToInt32(attendanceDeleteViewModel.Pin) });
                    if (memberIdList == null || memberIdList.Count == 0)
                    {
                        throw new DataNotFoundException("Member is not found/authorized.");
                    }
                    var teamMember = _hrMemberService.GetByPin(attendanceDeleteViewModel.Pin);
                    if (teamMember != null)
                    {
                        Organization org = _hrMemberService.GetTeamMemberOrganization(null, teamMember.Id);
                        ViewBag.Organization = org;
                        ViewBag.Name = teamMember.FullNameEng;
                        ViewBag.Designation = _hrMemberService.GetTeamMemberDesignation(null, teamMember.Id);

                        DateTime orgStartTime =
                            Convert.ToDateTime(attendanceDeleteViewModel.AttendanceDate.ToString("yyyy-MM-dd") + " " +
                                               org.AttendanceStartTime.ToString("HH:mm"));
                        DateTime orgEndTime =
                            Convert.ToDateTime(
                                attendanceDeleteViewModel.AttendanceDate.AddDays(1).ToString("yyyy-MM-dd") + " " +
                                org.AttendanceStartTime.AddMinutes(-1).ToString("HH:mm"));

                        var list = _attendanceDeviceRawDataService.AttendanceReportByPinDate(teamMember.Pin,
                            orgStartTime, orgEndTime, false);

                        var listViewModels = new List<UserAttendanceReportViewModel>();
                        foreach (var attendanceDeviceRawData in list)
                        {
                            var obj = new UserAttendanceReportViewModel()
                            {
                                Id = attendanceDeviceRawData.Id,
                                PunchTime = attendanceDeviceRawData.PunchTime,
                                DeviceName = attendanceDeviceRawData.AttendanceDevice.Name,
                                LastModifiedDate = attendanceDeviceRawData.ModificationDate,
                                LastModifiedBy =
                                    attendanceDeviceRawData.ModifyBy != 0
                                        ? _userService.GetUserNameByAspNetUserId(attendanceDeviceRawData.ModifyBy)
                                        : "",
                                Remarks = attendanceDeviceRawData.Remarks,
                                Status = attendanceDeviceRawData.Status
                            };
                            listViewModels.Add(obj);
                        }
                        ViewBag.ListRawData = listViewModels;
                    }
                    else
                    {
                        ViewBag.InvalidPin = "No View Data";
                    }
                    //return Json(new { isSuccess = true, returnMessage = partialView });
                }
            }
            catch (DataNotFoundException ex)
            {
                ViewBag.InvalidPin = ex.Message;
            }
           
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return PartialView("_partial/_DeleteAttendancePartialView", attendanceDeleteViewModel);
        }

        public ActionResult DeleteAttendanceRow(long id, string remarks, int status)
        {
            try
            {
                bool isDeleted = _attendanceDeviceRawDataService.IsDelete(id, remarks, status);
                if (isDeleted)
                {
                    var modifyBy = _userService.GetUserNameByAspNetUserId(Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId()));
                    var modificationDate = DateTime.Now.ToString("MMMM d, yyyy hh:mm tt");
                    return Json(new { IsSuccess = true, returnRemarks = remarks, returnModifyBy = modifyBy, returnModificationDate = modificationDate });
                }
                return Json(new Response(false, "Problem Occurred. Retry"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Helper
        private AttendanceReportViewModel GenerateAttendanceReport(TeamMember teamMember, DateTime startDate, DateTime endDate, bool isDateAsc = true, bool isRemarks = false)
        {
            AttendanceReportViewModel reportModel = new AttendanceReportViewModel();
            IList<AttendanceReportRowModel> dataModelList = new List<AttendanceReportRowModel>();
            DateTime teamMemberJoiningDate = teamMember.EmploymentHistory.Where(e => e.Status == EmploymentHistory.EntityStatus.Active).OrderBy(e => e.EffectiveDate).Select(e => e.EffectiveDate).FirstOrDefault();
            try
            {
                DateTime sDate = startDate;
                DateTime eDate = endDate;
                int dayIncrement = 1;
                if (isDateAsc == false)
                {
                    sDate = endDate;
                    eDate = startDate;
                    dayIncrement = -1;
                }

                int totalLateEntry = 0;
                int totalEarlyLeave = 0;
                int totalWorkingHour = 0;

                bool isAbsent = false;
                List<ZoneSetting> finalZoneList = new List<ZoneSetting>();

                for (DateTime calcDate = sDate; isDateAsc == true ? calcDate <= eDate : calcDate >= eDate; calcDate = calcDate.AddDays(dayIncrement))
                {
                    AttendanceSummary attenSummary = _attendanceSummaryServiceHelper.GetAttendenceByPinAndDateTime(teamMember.Pin, calcDate);
                    AttendanceReportRowModel dataModel = new AttendanceReportRowModel();

                    #region AttendanceSummery To AttendanceReportRow Conversion
                    dataModel.AttendanceDay = calcDate.ToString("dd MMM, yyyy");
                    if (attenSummary.InTime != null)
                        dataModel.InTime = attenSummary.InTime.Value.ToString("hh:mm tt");
                    else
                        dataModel.InTime = "-";

                    if (attenSummary.OutTime != null)
                        dataModel.OutTime = attenSummary.OutTime.Value.ToString("hh:mm tt");
                    else
                        dataModel.OutTime = "-";

                    if (attenSummary.LateEntry != null && attenSummary.IsWeekend == false && attenSummary.HolidaySetting == null && attenSummary.BusinessId.Trim() == "")
                    {
                        totalLateEntry += attenSummary.LateEntry.Value.Hour * 60 + attenSummary.LateEntry.Value.Minute;
                        dataModel.LateEntry = attenSummary.LateEntry.Value.ToString("HH:mm");
                    }
                    else
                        dataModel.LateEntry = "-";

                    if (attenSummary.EarlyLeave != null && attenSummary.IsWeekend == false && attenSummary.HolidaySetting == null && attenSummary.BusinessId.Trim() == "")
                    {
                        totalEarlyLeave += attenSummary.EarlyLeave.Value.Hour * 60 + attenSummary.EarlyLeave.Value.Minute;
                        dataModel.EarlyLeave = attenSummary.EarlyLeave.Value.ToString("HH:mm");
                    }
                    else
                        dataModel.EarlyLeave = "-";

                    if (attenSummary.InTime != null && attenSummary.OutTime != null && attenSummary.BusinessId.Contains("Leave:") == false)
                    {
                        int workingHour = (int)(attenSummary.OutTime.Value - attenSummary.InTime.Value).TotalMinutes;
                        totalWorkingHour += workingHour;
                        dataModel.WorkingHour = TimeSpan.FromMinutes(workingHour).ToString(@"hh\:mm");
                    }
                    else
                        dataModel.WorkingHour = "-";

                    //Weekend Calculation
                    dataModel.AbsentOrLeave = "";
                    if (attenSummary.IsWeekend == true && attenSummary.BusinessId.Contains("Leave:") == false)
                    {
                        dataModel.AbsentOrLeave = "Weekend";
                    }

                    //Holiday Calculation
                    if (attenSummary.HolidaySetting != null && attenSummary.BusinessId.Contains("Leave:") == false)
                    {
                        //if (dataModel.AbsentOrLeave.Trim() != "") dataModel.AbsentOrLeave += ", <br />";
                        //dataModel.AbsentOrLeave += attenSummary.HolidaySetting.Name;
                        dataModel.AbsentOrLeave = attenSummary.HolidaySetting.Name;
                    }

                    //Leave String
                    if (attenSummary.BusinessId != null && attenSummary.BusinessId.Trim() != "")
                    {
                        if (dataModel.AbsentOrLeave.Trim() != "") dataModel.AbsentOrLeave += ", <br />";
                        dataModel.AbsentOrLeave += attenSummary.BusinessId;
                        //dataModel.AbsentOrLeave += attenSummary.BusinessId;
                    }

                    //Absent Calculation
                    if (attenSummary.InTime == null && attenSummary.OutTime == null && attenSummary.IsWeekend == false && attenSummary.HolidaySetting == null && attenSummary.BusinessId.Trim() == "")
                    {
                        if (attenSummary.AttendanceDate.Value >= teamMemberJoiningDate && attenSummary.AttendanceDate.Value <= DateTime.Now)
                        {
                            dataModel.AbsentOrLeave = "Absent";
                            isAbsent = true;
                        }
                        else
                        {
                            dataModel.AbsentOrLeave = "-";
                        }
                    }

                    if (attenSummary.AttendanceDate.Value < teamMemberJoiningDate)
                    {
                        dataModel.AbsentOrLeave = "-";
                    }
                    else if (attenSummary.AttendanceDate.Value == teamMemberJoiningDate)
                    {
                        if (dataModel.AbsentOrLeave.Trim() != "") dataModel.AbsentOrLeave += ", <br />";
                        dataModel.AbsentOrLeave += "--Joining Date--";
                    }

                    if (attenSummary.ZoneSetting != null && attenSummary.BusinessId.Contains("Leave:") == false)
                    {
                        dataModel.Zone = attenSummary.ZoneSetting.Name;
                        finalZoneList.Add(attenSummary.ZoneSetting);
                    }
                    else
                    {
                        dataModel.Zone = "";
                    }

                    dataModel.Remarks = "";
                    if (isRemarks == true)
                    {
                        if (attenSummary.InTime != null)
                        {
                            if (attenSummary.InDevice != null)
                                dataModel.Remarks = "<strong>In:</strong> " + attenSummary.InDevice.Name;
                            else dataModel.Remarks = "<strong>In:</strong> Adjustment";
                        }
                        if (attenSummary.OutTime != null)
                        {
                            if (attenSummary.OutDevice != null)
                            {
                                if (dataModel.Remarks.Trim() == "")
                                    dataModel.Remarks = "<strong>Out:</strong> " + attenSummary.OutDevice.Name;
                                else
                                    dataModel.Remarks += "<br /><strong>Out:</strong> " + attenSummary.OutDevice.Name;
                            }
                            else
                            {
                                if (dataModel.Remarks.Trim() == "")
                                    dataModel.Remarks = "<strong>Out:</strong> Adjustment";
                                else
                                    dataModel.Remarks += "<br /><strong>Out:</strong> Adjustment";
                            }
                        }
                    }
                    #endregion

                    dataModelList.Add(dataModel);
                }

                reportModel.DataList = dataModelList;
                reportModel.FinalZone = "-";
                if (isAbsent == false && finalZoneList.Count > 0)
                {
                    reportModel.FinalZone = finalZoneList.OrderByDescending(z => z.ToleranceTime).FirstOrDefault().Name;
                }
                reportModel.ReportStartDate = startDate.ToString("dd MMM, yyyy");
                reportModel.ReportEndDate = endDate.ToString("dd MMM, yyyy");
                reportModel.TeamMemberName = teamMember.FullNameEng + "  (" + teamMember.Name + ")";
                reportModel.TeamMemberPin = teamMember.Pin.ToString();
                reportModel.TotalEarlyLeave = "-";
                if (totalEarlyLeave > 0) reportModel.TotalEarlyLeave = IntToHourMinute(totalEarlyLeave);

                reportModel.TotalLateEntry = "-";
                if (totalLateEntry > 0) reportModel.TotalLateEntry = IntToHourMinute(totalLateEntry);

                reportModel.TotalWorkingHour = "-";
                if (totalWorkingHour > 0) reportModel.TotalWorkingHour = IntToHourMinute(totalWorkingHour);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

            return reportModel;
        }


        //Convert Integer Minutes to Hour:Minute
        private string IntToHourMinute(int totalMinute)
        {
            int hour = totalMinute / 60;
            int minutes = totalMinute % 60;
            string ret = "";

            if (hour < 10) ret = "0" + hour.ToString();
            else ret = hour.ToString();

            if (minutes < 10) ret += ":0" + minutes.ToString();
            else ret += ":" + minutes.ToString();

            return ret;
        }

        //private AttendanceReportViewModel GenerateAttendanceReport(TeamMember teamMember, DateTime startDate, DateTime endDate, DateTime[] dateRange, IList<AttendanceSummary> dataList)
        //{
        //    try
        //    {
        //        double totalWorkingSeconds = 0, totalLateEntrySeconds = 0, totalEarlyLeaveSeconds = 0;
        //        int zoneTolerance = 0;
        //        string finalZone = "-", totalWorkingTimeText = "-", totalEarlyLeaveText = "-", totalLateEntryText = "-", zoneText = "-";
        //        bool isAbsentConst = false;
        //        IList<AttendanceReportRowModel> dataModelList = new List<AttendanceReportRowModel>();
        //        var empHistory = teamMember.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();
        //        var organization = empHistory != null ? empHistory.Campus.Branch.Organization:null;
        //        #region Empty Data Model
        //        if (dataList == null || dataList.Count <= 0)
        //        {
        //            dataModelList = GetEmptyDataModel(dateRange, teamMember);
        //            var reportModel = new AttendanceReportViewModel()
        //            {
        //                DataList = dataModelList,
        //                FinalZone = "-",
        //                ReportEndDate = endDate.ToShortDateString(),
        //                ReportStartDate = startDate.ToShortDateString(),
        //                TeamMemberName = teamMember.Name.ToString(CultureInfo.CurrentCulture),
        //                TeamMemberPin = teamMember.Pin.ToString(CultureInfo.CurrentCulture),
        //                TotalEarlyLeave = "-",
        //                TotalLateEntry = "-",
        //                TotalWorkingHour = "-"
        //            };
        //            return reportModel;
        //        }
        //        #endregion

        //        bool firstCall = true;
        //        bool isDataIndexComplete = false;
        //        string memberWeekEnd = ((WeekDay)GetMemberWeekend(teamMember.Id)).ToString();

        //        int index = 0;
        //        for (int i = 0; i < dateRange.Length; i++)
        //        {
        //            string inTimeText = "-",
        //                outTimeText = "-",
        //                lateEntryTimeText = "-",
        //                earlyLeaveTimeText = "-",
        //                workingHourText = "-",
        //                absentOrLeaveText = "-",
        //                remarksText = "-";

        //            DateTime dt = dateRange[i];
        //            string attendanceDayText = dt.ToShortDateString();
        //            LeaveApplication leaveApplication = _hrLeaveApplicationService.GetTeamMemberLeaveByDate(teamMember, dt);
        //            if (isDataIndexComplete)
        //            {
        //                bool finalAbsent = false;
        //                AttendanceReportRowModel rowModel = GetEmptyRowModel(dateRange[i], memberWeekEnd, leaveApplication, out finalAbsent, organization);
        //                if (finalAbsent && !isAbsentConst)
        //                {
        //                    isAbsentConst = finalAbsent;
        //                }
        //                dataModelList.Add(rowModel);
        //                continue;
        //            }
        //            for (int j = index; j < dataList.Count; )
        //            {
        //                var row = dataList[j];
        //                if (dt == row.AttendanceDate)
        //                {
        //                    bool finalAbsent = false;
        //                    if (row.ZoneSetting != null && leaveApplication == null && row.IsWeekend != true && row.HolidaySetting == null)
        //                    {
        //                        if (!firstCall)
        //                        {
        //                            int zoneToleranceTime = row.ZoneSetting.ToleranceTime;
        //                            if (zoneTolerance < zoneToleranceTime)
        //                            {
        //                                zoneTolerance = zoneToleranceTime;
        //                                zoneText = row.ZoneSetting.Name.ToString(CultureInfo.CurrentCulture);
        //                                finalZone = row.ZoneSetting.Name.ToString(CultureInfo.CurrentCulture);
        //                            }
        //                            else
        //                            {
        //                                zoneText = row.ZoneSetting.Name.ToString(CultureInfo.CurrentCulture);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            zoneTolerance = row.ZoneSetting.ToleranceTime;
        //                            zoneText = row.ZoneSetting.Name;
        //                            finalZone = row.ZoneSetting.Name.ToString(CultureInfo.CurrentCulture);
        //                            firstCall = false;
        //                        }
        //                    }
        //                    else if (row.ZoneSetting == null)
        //                    {
        //                        zoneText = "-";
        //                        finalZone = "-";
        //                    }
        //                    #region Get In Out and working hours. Leave, weekend, holiday or absence

        //                    if (row.HolidaySetting != null || leaveApplication != null || row.IsWeekend.HasValue)
        //                    {
        //                        string weekendText = "";
        //                        DayOfWeek dow = dt.DayOfWeek;
        //                        string dayText = dow.ToString();
        //                        GetLeaveWeekendOrHolidayText(true, leaveApplication, row.HolidaySetting, memberWeekEnd, dayText, out absentOrLeaveText, out remarksText, out finalAbsent);
        //                    }
        //                    if (row.InTime != null && row.OutTime != null)
        //                    {
        //                        double diff = 0;
        //                        GetInTimeOutTimeAndWorkingHour(row.InTime, row.OutTime, out inTimeText, out outTimeText, out workingHourText, out diff);
        //                        totalWorkingSeconds += diff;
        //                    }
        //                    else
        //                    {
        //                        if (row.InTime != null)
        //                        {
        //                            var time = (DateTime)row.InTime;
        //                            inTimeText = time.ToShortTimeString();
        //                        }
        //                        if (row.OutTime != null)
        //                        {
        //                            var time = (DateTime)row.OutTime;
        //                            outTimeText = time.ToShortTimeString();
        //                        }
        //                    }
        //                    #endregion
        //                    #region Get Late Entry and Early Leave
        //                    //Late Entry
        //                    if (row.LateEntry != null)
        //                    {
        //                        double durations = 0;
        //                        DateTime result = Convert.ToDateTime(row.LateEntry);
        //                        GetLateEntryOrEarlyLeaveText(result, out lateEntryTimeText, out durations);
        //                        totalLateEntrySeconds += durations;
        //                    }
        //                    //Early Leave
        //                    if (row.EarlyLeave != null)
        //                    {
        //                        double durations = 0;
        //                        DateTime result = Convert.ToDateTime(row.EarlyLeave);
        //                        GetLateEntryOrEarlyLeaveText(result, out earlyLeaveTimeText, out durations);
        //                        totalEarlyLeaveSeconds += durations;
        //                    }
        //                    #endregion
        //                    if (finalAbsent && !isAbsentConst)
        //                    {
        //                        isAbsentConst = finalAbsent;
        //                    }
        //                    var dataModel = new AttendanceReportRowModel()
        //                    {
        //                        AbsentOrLeave = absentOrLeaveText,
        //                        AttendanceDay = attendanceDayText,
        //                        EarlyLeave = earlyLeaveTimeText,
        //                        InTime = inTimeText,
        //                        LateEntry = lateEntryTimeText,
        //                        OutTime = outTimeText,
        //                        Remarks = remarksText,
        //                        WorkingHour = workingHourText,
        //                        Zone = zoneText
        //                    };
        //                    dataModelList.Add(dataModel);
        //                    index = j + 1;
        //                    if (index == dataList.Count)
        //                    {
        //                        isDataIndexComplete = true;
        //                    }
        //                    break;
        //                }
        //                else
        //                {
        //                    bool isAbsent = false;
        //                    AttendanceReportRowModel rowModel = GetEmptyRowModel(dateRange[i], memberWeekEnd, leaveApplication, out isAbsent,organization);
        //                    if (isAbsent && !isAbsentConst)
        //                    {
        //                        isAbsentConst = isAbsent;
        //                    }
        //                    dataModelList.Add(rowModel);
        //                    break;
        //                }
        //            }
        //        }

        //        if (isAbsentConst)
        //        {
        //            finalZone = "-";
        //        }
        //        #region Total W.Hour, LateEntry, EarlyLeave

        //        if (totalWorkingSeconds > 0)
        //        {
        //            totalWorkingTimeText = ReturnHourMinTextFromSeconds(totalWorkingSeconds);
        //        }
        //        if (totalEarlyLeaveSeconds > 0)
        //        {
        //            totalEarlyLeaveText = ReturnHourMinTextFromSeconds(totalEarlyLeaveSeconds);
        //        }
        //        if (totalLateEntrySeconds > 0)
        //        {
        //            totalLateEntryText = ReturnHourMinTextFromSeconds(totalLateEntrySeconds);
        //        }
        //        #endregion

        //        var reportViewModel = new AttendanceReportViewModel()
        //        {
        //            DataList = dataModelList,
        //            FinalZone = finalZone,
        //            ReportEndDate = dateRange.Last().ToShortDateString(),
        //            ReportStartDate = dateRange.First().ToShortDateString(),
        //            TeamMemberName = teamMember.Name.ToString(CultureInfo.CurrentCulture),
        //            TeamMemberPin = teamMember.Pin.ToString(CultureInfo.CurrentCulture),
        //            TotalEarlyLeave = totalEarlyLeaveText,
        //            TotalLateEntry = totalLateEntryText,
        //            TotalWorkingHour = totalWorkingTimeText
        //        };
        //        return reportViewModel;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}
        //private AttendanceReportRowModel GetEmptyRowModel(DateTime dateTime, string memberWeekEnd, LeaveApplication leaveApplication, out bool finalAbsent,Organization organization)
        //{
        //    string remarksText = "";
        //    string columnRowtext = "";
        //    bool isAbsent;

        //    var holiday = _hrHolidayService.GetHolidaySetting(dateTime,organization);
        //    DayOfWeek dow = dateTime.DayOfWeek;
        //    string dayText = dow.ToString();
        //    GetLeaveWeekendOrHolidayText(false, leaveApplication, holiday, memberWeekEnd, dayText, out columnRowtext, out remarksText, out isAbsent);
        //    finalAbsent = isAbsent;

        //    var dataModel = new AttendanceReportRowModel()
        //    {
        //        AbsentOrLeave = columnRowtext,
        //        AttendanceDay = dateTime.ToShortDateString(),
        //        EarlyLeave = "-",
        //        InTime = "-",
        //        LateEntry = "-",
        //        OutTime = "-",
        //        Remarks = remarksText,
        //        WorkingHour = "-",
        //        Zone = "-"
        //    };
        //    return dataModel;
        //}

        //private IList<AttendanceReportRowModel> GetEmptyDataModel(IEnumerable<DateTime> dateRange, TeamMember member)
        //{
        //    IList<AttendanceReportRowModel> dataModelList = new List<AttendanceReportRowModel>();
        //    try
        //    {
        //        var empHistory = member.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();
        //        var organization = empHistory != null ? empHistory.Campus.Branch.Organization : null;
        //        string weekEnd = ((WeekDay)GetMemberWeekend(member.Id)).ToString();
        //        foreach (var date in dateRange)
        //        {
        //            bool finalAbsent = false;
        //            LeaveApplication leaveApplication = _hrLeaveApplicationService.GetTeamMemberLeaveByDate(member, date);
        //            var model = GetEmptyRowModel(date, weekEnd, leaveApplication, out finalAbsent, organization);
        //            dataModelList.Add(model);
        //        }
        //        return dataModelList;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}

        //private static DateTime[] GetDateRange(DateTime startDate, int totalDays, int? dateOrder)
        //{
        //    var dateRange = new DateTime[totalDays];
        //    if (dateOrder != null && dateOrder == 1)
        //    {
        //        int j = 0;
        //        for (int i = totalDays - 1; i > -1; i--)
        //        {
        //            var dateNow = startDate.AddDays(i);
        //            dateRange[j] = dateNow;
        //            j++;
        //        }
        //    }
        //    else
        //    {
        //        for (int i = 0; i < totalDays; i++)
        //        {
        //            var dateNow = startDate.AddDays(i);
        //            dateRange[i] = dateNow;
        //        }
        //    }
        //    return dateRange;
        //}

        //private void GetLeaveWeekendOrHolidayText(bool hasAttendanceSummaryData, LeaveApplication leaveApplication, HolidaySetting holiday, string todaysName,
        //    string memberWeekEnd, out string columnRowtext, out string remarksText, out bool finalAbsent)
        //{
        //    remarksText = "-";
        //    columnRowtext = "";
        //    finalAbsent = false;
        //    string leaveText = "";
        //    if (leaveApplication != null)
        //    {
        //        leaveText = leaveApplication.Leave.Name.ToString(CultureInfo.CurrentCulture);
        //        if (leaveApplication.Remarks != null)
        //        {
        //            remarksText = leaveApplication.Remarks.ToString(CultureInfo.CurrentCulture);
        //        }
        //    }

        //    if (memberWeekEnd.Equals(todaysName) || holiday != null)
        //    {
        //        if (memberWeekEnd.Equals(todaysName) && holiday != null)
        //        {
        //            columnRowtext = "Weekend," + holiday.Name.ToString(CultureInfo.CurrentCulture);
        //        }
        //        else if (memberWeekEnd.Equals(todaysName) && holiday == null)
        //        {
        //            columnRowtext = "Weekend";
        //        }
        //        else if (!memberWeekEnd.Equals(todaysName) && holiday != null)
        //        {
        //            columnRowtext = holiday.Name.ToString(CultureInfo.CurrentCulture);
        //        }

        //    }
        //    if (!string.IsNullOrEmpty(leaveText))
        //    {
        //        if (string.IsNullOrEmpty(columnRowtext))
        //        {
        //            columnRowtext = leaveText;
        //        }
        //        else if (!string.IsNullOrEmpty(columnRowtext))
        //        {
        //            columnRowtext += "," + leaveText;
        //        }
        //    }
        //    if (string.IsNullOrEmpty(columnRowtext))
        //    {
        //        if (!hasAttendanceSummaryData)
        //        {
        //            columnRowtext = "Absent";
        //            finalAbsent = true;
        //        }
        //        else
        //        {
        //            columnRowtext = "-";
        //        }
        //    }
        //}

        //private void GetLateEntryOrEarlyLeaveText(DateTime result, out string timeText, out double durations)
        //{
        //    string time = result.ToString("HH:mm");
        //    if (time.Equals("00:00"))
        //    {
        //        timeText = "-";
        //        durations = 0;
        //    }
        //    else
        //    {
        //        timeText = time;
        //        time = time + ":00";
        //        durations = TimeSpan.Parse(time).TotalSeconds;
        //    }
        //}

        //private void GetInTimeOutTimeAndWorkingHour(DateTime? inTimeValue, DateTime? outTimeValue,
        //    out string inTimeText, out string outTimeText, out string workingText, out double diff)
        //{
        //    diff = 0;
        //    inTimeText = "-";
        //    outTimeText = "-";
        //    workingText = "-";
        //    var span = outTimeValue - inTimeValue;
        //    if (span != null)
        //    {
        //        var timeSpan = (TimeSpan)span;
        //        diff = (timeSpan.TotalSeconds);
        //        workingText = timeSpan.ToString(@"hh\:mm");
        //    }
        //    if (inTimeValue == null || outTimeValue == null) return;
        //    var inTime = (DateTime)inTimeValue;
        //    var outTime = (DateTime)outTimeValue;
        //    inTimeText = inTime.ToShortTimeString();
        //    outTimeText = outTime.ToShortTimeString();
        //}

        //private int GetMemberWeekend(long memberId)
        //{
        //    try
        //    {
        //        int weekEnd = _hrMemberService.GetMemberWeekEnd(memberId);
        //        return weekEnd;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}

        //private string ReturnHourMinTextFromSeconds(double seconds)
        //{
        //    string hoursString = "";
        //    string minString = "";
        //    int totalseconds = Convert.ToInt32(seconds);
        //    int hours = totalseconds / 3600;
        //    int minutes = (totalseconds % 3600) / 60;
        //    int sec = (totalseconds % 3600) % 60;
        //    hoursString = hours < 10 ? "0" + hours.ToString(CultureInfo.CurrentCulture) : hours.ToString(CultureInfo.CurrentCulture);
        //    minString = minutes < 10 ? "0" + minutes.ToString(CultureInfo.CurrentCulture) : minutes.ToString(CultureInfo.CurrentCulture);
        //    string text = hoursString + ":" + minString;
        //    return text;
        //}

        public JsonResult LoadDesignation(List<long> organizationIds, DateTime effectiveDate)
        {
            try
            {
                IList<Designation> designationList = _designationService.LoadDesignation(organizationIds, effectiveDate);
                var designationListSelectList = new SelectList(designationList.ToList(), "Id", "Name");
                return Json(new { returnDesignationList = designationListSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Designation";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        
        #endregion

    }
}