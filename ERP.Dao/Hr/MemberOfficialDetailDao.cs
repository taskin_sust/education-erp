﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IMemberOfficialDetailDao : IBaseDao<MemberOfficialDetail, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        //MemberOfficialDetail GetMemberOfficcalDetail(long? teamMemberId, int? pin);
        MemberOfficialDetail GetMemberByCardNumber(string cardNumber);
        #endregion

        #region List Loading Function
        IList<MemberOfficialDetail> GetTeamMemberLists(string synchronzerKey, bool isReset);
        #endregion

        #region Others Function
        bool IsDuplicateTeamMemberCardNo(TeamMember member, string cardNumber);
        #endregion

        #region Helper Function
        string GetNewCardNumber();

        #endregion


    }
    public class MemberOfficialDetailDaoDao : BaseDao<MemberOfficialDetail, long>, IMemberOfficialDetailDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public MemberOfficialDetail GetMemberByCardNumber(string cardNumber)
        {
            var row = Session.QueryOver<MemberOfficialDetail>()
                    .Where(x => x.CardNo == cardNumber && x.Status == MemberOfficialDetail.EntityStatus.Active)
                    .SingleOrDefault();
            return row;
        }
        #endregion

        #region List Loading Function
        public IList<MemberOfficialDetail> GetTeamMemberLists(string synchronzerKey, bool isReset)
        {
            ICriteria criteria = Session.CreateCriteria<MemberOfficialDetail>();
            DetachedCriteria lastMemberInfoUpdateTime = DetachedCriteria.For<AttendanceSynchronizer>().SetProjection(Projections.Property("LastMemberInfoUpdateTime"));
            lastMemberInfoUpdateTime.Add(Restrictions.Eq("SynchronizerKey", synchronzerKey));
            if (lastMemberInfoUpdateTime != null && isReset == false)
            {
                criteria.Add(Subqueries.PropertyGt("ModificationDate", lastMemberInfoUpdateTime));
                // criteria.Add(Restrictions.Ge("ModificationDate", lastmemberInfoUpdateTime));
            }
            else
            {
                criteria.Add(Restrictions.Eq("Status", MemberOfficialDetail.EntityStatus.Active));
            }
            criteria.CreateAlias("TeamMember", "tm").Add(Restrictions.Not(Restrictions.Eq("tm.Status", TeamMember.EntityStatus.Delete)));
            return criteria.List<MemberOfficialDetail>();
        }



        #endregion

        #region Others Function
        public bool IsDuplicateTeamMemberCardNo(TeamMember member, string cardNumber)
        {
            ICriteria criteria = Session.CreateCriteria<MemberOfficialDetail>()
                     .Add(Restrictions.Eq("Status", MemberOfficialDetail.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "tm").Add(Restrictions.Not(Restrictions.Eq("tm.Status", TeamMember.EntityStatus.Delete)));

            criteria.Add(Restrictions.Not(Restrictions.Eq("tm.Id", member.Id)));
            criteria.Add(Restrictions.Eq("CardNo", cardNumber));
            criteria.SetProjection(Projections.RowCount());
            var rowCount = Convert.ToInt32(criteria.UniqueResult());
            //List<MemberOfficialDetail> memberOfficialDetails = criteria.List<MemberOfficialDetail>().ToList();
            return rowCount > 0;
        }

        public string GetNewCardNumber()
        {
            var cardNo = Session.QueryOver<MemberOfficialDetail>()
                .Where(x => x.Status != MemberOfficialDetail.EntityStatus.Delete)
                .Select(Projections
                .ProjectionList()
                .Add(Projections.Max<MemberOfficialDetail>(x => x.CardNo)))
                .List<string>().First() + 1;
            return cardNo;
       
        }

        #endregion

        #region Helper Function

        #endregion


    }
}
