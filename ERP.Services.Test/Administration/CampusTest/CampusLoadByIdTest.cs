﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.CampusTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Campus")]
    public class CampusLoadByIdTest : CampusBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _campusService.GetCampus(-1);
            Assert.Null(obj);
            
        }

        [Fact]
        public void GetCampus_NotNull_Check()
        {
            var campusFactory =
               _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                })
                   .Persist();
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            campus = _campusService.GetCampus(campusFactory.Object.Id);
            Assert.NotNull(campus);
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
        }

        [Theory]
        [InlineData(49,"down")]
        [InlineData(51, "up")]
        public void GetCampusByRankNextOrPrevious_NotNull_Check(int rank,string action)
        {
            var campusFactory =
               _campusFactory.Create().WithBranch().WithNameAndRank(_name, 50).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                })
                   .Persist();
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusByRank = _campusService.GetCampusByRankNextOrPrevious(rank, action);
            Assert.NotNull(campusByRank);
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
        }
       
    }
}
