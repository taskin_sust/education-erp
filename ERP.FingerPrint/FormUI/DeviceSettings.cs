﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using UdvashERP.FingerPrint.BusinessModel;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using UdvashERP.FingerPrint.Service;

namespace UdvashERP.FingerPrint
{
    public partial class DeviceSettings : BaseForm
    {
        #region Propertise & Object Initialization

        string xmlFileName = "DeviceInfo.xml";
        Device _device;
        List<Device> _deviceList;
        FingerPrintScannerService _fingerPrintScannerService;

        public DeviceSettings()
        {
            InitializeComponent();
            _device = new Device();
            _deviceList = new List<Device>();
            _fingerPrintScannerService = new FingerPrintScannerService();

            deviceConnectionBindingSource.DataSource = _device;
        }

        private void DeviceSettings_Load(object sender, EventArgs e)
        {
            LoadDeviceInfoFromXml();
            // deviceConnectionBindingSource.DataSource = _deviceConnection;
        }


        #endregion

        #region Operational Function

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveDeviceCOnnectionToXml();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConnectionTest_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            Device device = (Device)lueDevice.GetSelectedDataRow();
            if (device == null)
            {
                return;
            }

            bool isConnected = _fingerPrintScannerService.ConnectionTest(device);

            if (isConnected)
            {
                MessageBox.Show("Successfully Connected");
            }
            else
            {
                MessageBox.Show("Not Connected");
            }
            Cursor = Cursors.Default;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            Device device = (Device)lueDevice.GetSelectedDataRow();
            if (device == null)
            {
                return;
            }

            bool isConnected = _fingerPrintScannerService.DeleteData(device);

            if (isConnected)
            {
                MessageBox.Show("Successfully Connected");
            }
            else
            {
                MessageBox.Show("Not Connected");
            }
            Cursor = Cursors.Default;
        }

        #endregion

        #region Others Function

        private void lueDevice_EditValueChanged(object sender, EventArgs e)
        {
            Device device = (Device)lueDevice.GetSelectedDataRow();
            if (device != null)
            {
                _device = device;
                deviceConnectionBindingSource.DataSource = device;
            }
        }

        #endregion

        #region Helper Function

        private bool SaveDeviceCOnnectionToXml()
        {
            Device device = (Device)lueDevice.GetSelectedDataRow();
            if (device == null)
            {
                return false;
            }

            foreach (var node in _deviceList)
            {
                if (node.Name == device.Name)
                {
                    node.IpAddress = _device.IpAddress;
                    node.CommunicationKey = _device.CommunicationKey;
                    node.MachineNo = _device.MachineNo;
                    node.Port = _device.Port;
                }
            }

            XmlSerializer xs = new XmlSerializer(typeof(List<Device>));
            // Create a new file stream to write the serialized object to a file
            TextWriter WriteFileStream = new StreamWriter(Application.StartupPath + "\\" + xmlFileName);
            xs.Serialize(WriteFileStream, _deviceList);
            // Cleanup
            WriteFileStream.Close();
            return true;
        }

        private bool LoadDeviceInfoFromXml()
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<Device>));
                FileStream fs = new FileStream(Application.StartupPath + "\\" + xmlFileName, FileMode.Open);
                _deviceList = (List<Device>)xs.Deserialize(fs);
                deviceListBindingSource.DataSource = _deviceList;
                fs.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
    }
}