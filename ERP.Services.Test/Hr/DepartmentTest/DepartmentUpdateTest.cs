﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DepartmentTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Department")]
    public class DepartmentUpdateTest : DepartmentBaseTest
    {       
        [Fact]
        public void Should_Return_Update_DuplicationCheck_IsFalse() //having data
        {
            var dFactory1 = _departmentFactory.Create().WithOrganization().WithNameAndRank(_name, 1).Persist();
            var org = dFactory1.Object.Organization;
            var dFactory2 = _departmentFactory.Create().WithOrganization(org).WithNameAndRank(_name, 1);
            Assert.Throws<DuplicateEntryException>(() => _departmentService.Update(dFactory2.Object));            
        }

        [Fact]
        public void Should_Return_Update_Null_Check()
        {
            Assert.Throws<EmptyFieldException>(()=>_departmentService.Update(null));
        }

        [Fact]
        public void Should_Return_NullObjectException_For_Organization()
        {
            var dFactory1 = _departmentFactory.Create().WithOrganization().Persist();
            var df2 = _departmentFactory.CreateWith(dFactory1.Object.Id, dFactory1.Object.Name).WithOrganization(null);
            Assert.Throws<NullObjectException>(() => _departmentService.Update(df2.Object));
        }

        [Fact]
        public void Should_Return_NullObjectException_For_InvalidId()
        {
            var dFactory1 = _departmentFactory.Create().WithOrganization().Persist();
            var df2 = _departmentFactory.CreateWith(dFactory1.Object.Id + 1, dFactory1.Object.Name)
                .WithOrganization(dFactory1.Object.Organization);
            Assert.Throws<NullObjectException>(() => _departmentService.Update(df2.Object));
        }

        [Fact]
        public void Should_Return_Successfully()
        {
            var df1 = _departmentFactory.Create().WithOrganization().Persist().Object;
            var df2 = _departmentFactory.CreateWith(df1.Id,Guid.NewGuid().ToString()).WithOrganization(df1.Organization);
            Assert.True(_departmentService.Update(df2.Object));
            Assert.Equal(df2.Object.Name,_departmentService.LoadById(df2.Object.Id).Name);
        }
    }
}
