﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.OrderedGoodsReceive
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReceive")]
    public class OrderedGoodsReceiveLoadTest : OrderedGoodsReceiveBaseTest
    {
        public void Should_Return_List_Of_GoodsReceive_Successfully()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(organizationFactory.Object, commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();
            //receive
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            List<OrderedGoodsRecViewModel> list = _goodsReceiveService.LoadGoodsReceived(0, 0, int.MaxValue, userMenus, organizationFactory.Object.Id, branchFactory.Object.Id,
                _itemFactory.Object.Name, _goodsReceiveFactory.Object.GoodsReceivedDate.ToString(), _directWorkOrderFactory.Object.WorkOrderNo, _directWorkOrderFactory.Object.WorkOrderDate.ToString(), "");
            Assert.True(list.Count > 0);
        }
    }
}
