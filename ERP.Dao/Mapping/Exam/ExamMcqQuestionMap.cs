﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Exam
{
    public class ExamMcqQuestionMap : BaseClassMap<ExamMcqQuestion, long>
    {
        public ExamMcqQuestionMap()
        {
            Table("ExamMcqQuestion");
            LazyLoad();

            Map(x => x.UniqueSet).Column("UniqueSet");
            Map(x => x.QuestionSerial).Column("QuestionSerial");
            Map(x => x.Version).Column("Version");
            Map(x => x.QuestionText).Column("QuestionText");
            Map(x => x.OptionA).Column("OptionA");
            Map(x => x.OptionB).Column("OptionB");
            Map(x => x.OptionC).Column("OptionC");
            Map(x => x.OptionD).Column("OptionD");
            Map(x => x.OptionE).Column("OptionE");
            Map(x => x.CorrectAnswer).Column("CorrectAnswer");
            Map(x => x.IsShuffle).Column("IsShuffle");
            References(x => x.Subject).Column("SubjectId");
            References(x => x.Exams).Column("ExamId");


        }
    }
}
