using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.Hr
{
public interface IMemberLoanCsrService : IBaseService
{
    #region Operational Function

    bool SaveOrUpdate(MemberLoanCsr prMemberLoanCsr);

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function

    #endregion

    #region Helper Function

    #endregion
}
public class MemberLoanCsrService : BaseService, IMemberLoanCsrService {

    #region Logger
    private readonly ILog _logger = LogManager.GetLogger("PayrollService");
    #endregion

    #region Propertise & Object Initialization
    private readonly CommonHelper _commonHelper;
    private readonly IMemberLoanCsrDao _prMemberLoanCsrDao;
    public MemberLoanCsrService(ISession session)
    {
        Session = session;
        _commonHelper = new CommonHelper();
        _prMemberLoanCsrDao=new MemberLoanCsrDao{Session = session};}
    #endregion

    #region Operational Functions

    public bool SaveOrUpdate(MemberLoanCsr prMemberLoanCsr)
    {
        return true;
    }

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function


    #endregion

    #region Helper function



    #endregion
}
}
                                    
