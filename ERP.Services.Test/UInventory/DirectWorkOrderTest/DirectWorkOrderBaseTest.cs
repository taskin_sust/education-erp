﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.DirectWorkOrderTest
{
    public class DirectWorkOrderBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal DirectWorkOrderFactory _directWorkOrderFactory;
        internal IWorkOrderService _workOrderService;
        internal TestBaseService<WorkOrder> _workOrderTestBaseService;
        internal TestBase _testBase;
        #endregion

        public DirectWorkOrderBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _directWorkOrderFactory = new DirectWorkOrderFactory(null, _session);
            _workOrderTestBaseService = new TestBaseService<WorkOrder>(_session);
            _workOrderService = new WorkOrderService(_session);
            _testBase=new TestBase();
        }
        public void CleanUpSpecificationCriteria(IList<SpecificationCriteria> specificationCriterias)
        {
            _workOrderTestBaseService.DeleteSpecificationCriteria(specificationCriterias);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            if (_directWorkOrderFactory.Object != null)
            {
                Session.CreateSQLQuery(
                       "Delete from uinv_WorkOrderCriteria where WorkOrderDetailId In(select id from uinv_WorkOrderDetails where workorderid=" +
                       _directWorkOrderFactory.Object.Id + ")")
                       .ExecuteUpdate();
                Session.CreateSQLQuery("Delete from uinv_WorkOrderDetails where workorderid=" + _directWorkOrderFactory.Object.Id)
                    .ExecuteUpdate();
            }
            if (_directWorkOrderFactory.ObjectList != null && _directWorkOrderFactory.ObjectList.Count > 0)
            {
                var workOrderIdList = _directWorkOrderFactory.ObjectList.Select(x => x.Id).ToList();
                var workOrderIdString = string.Join(", ", workOrderIdList);
                Session.CreateSQLQuery(
                       "Delete from uinv_WorkOrderCriteria where WorkOrderDetailId In(select id from uinv_WorkOrderDetails where workorderid in(" +
                       workOrderIdString + "))")
                       .ExecuteUpdate();
                Session.CreateSQLQuery("Delete from uinv_WorkOrderDetails where workorderid in(" + workOrderIdString+")")
                    .ExecuteUpdate();
            }
            _directWorkOrderFactory.CleanUp();
            base.Dispose();
        }
    }
}
