using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{


    public partial class SmsStudentRegistrationSettingMap : BaseClassMap<SmsStudentRegistrationSetting, long>
    {

        public SmsStudentRegistrationSettingMap()
        {
            Table("SmsStudentRegistrationSetting");
            LazyLoad();
            Map(x => x.StartDate);
            Map(x => x.EndDate);
            Map(x => x.SuccessMessage);
            Map(x => x.DuplicateMessage);


            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.Course).Column("CourseId");
            References(x => x.StudentExam).Column("StudentExamId");
            References(x => x.SmsMask).Column("SmsMaskId");

            HasMany(x => x.SmsStudentRegistrationReceiverList).KeyColumn("SmsStudentRegistrationSettingId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
