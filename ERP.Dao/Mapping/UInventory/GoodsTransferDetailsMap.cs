using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class GoodsTransferDetailsMap : BaseClassMap<GoodsTransferDetails, long>
    {

        public GoodsTransferDetailsMap()
        {
            Table("UINV_GoodsTransferDetails");
            LazyLoad();
            References(x => x.GoodsTransfer).Column("GoodsTransferId");
            References(x => x.Item).Column("ItemId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            Map(x => x.TransferQuantity).Column("TransferQuantity").Not.Nullable();
        }
    }
}
