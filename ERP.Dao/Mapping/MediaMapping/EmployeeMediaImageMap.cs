﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.MediaDb;

namespace UdvashERP.Dao.Mapping.MediaMapping
{
    public class EmployeeMediaImageMap : BaseClassMap<EmployeeMediaImage, long>
    {
        public EmployeeMediaImageMap()
        {
            Table("EmployeeMediaImage");
            LazyLoad();
            //Map(x => x.ImageIGuid);
            Map(x => x.Images).Column("Images").CustomSqlType("VARBINARY (MAX) FILESTREAM").Length(2147483647).LazyLoad();
            Map(x => x.ImageRef);
        }
    }
}
