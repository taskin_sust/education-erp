﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.EmployeeBenefitFundSettingTest
{
    public class EmployeeBenefitFundSettingBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initialization

        protected readonly ISession _session;
        protected readonly CommonHelper _commonHelper;
        protected readonly OrganizationFactory _organizationFactory;
        internal EmployeeBenefitsFundSettingFactory _employeeBenefitsFundSettingFactory;
        internal EmployeeBenefitsFundSettingEntitlementFactory _employeeBenefitsFundSettingEntitlementFactory;
        internal IEmployeeBenefitsFundSettingService _employeeBenefitsFundSettingService;

        #endregion

        #region Constructor

        public EmployeeBenefitFundSettingBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _organizationFactory = new OrganizationFactory(null, _session);
            _employeeBenefitsFundSettingFactory = new EmployeeBenefitsFundSettingFactory(null, _session);
            _employeeBenefitsFundSettingEntitlementFactory = new EmployeeBenefitsFundSettingEntitlementFactory(null, _session);
            _employeeBenefitsFundSettingService = new EmployeeBenefitsFundSettingService(_session);
        }

        #endregion

        #region Disposal

        public void Dispose()
        {
            _employeeBenefitsFundSettingEntitlementFactory.CleanupEmpBenefitSettingEntitlement(_employeeBenefitsFundSettingEntitlementFactory.SingleObjectList.Select(x => x.Id).ToList());
            _employeeBenefitsFundSettingFactory.CleanUp();
            branchFactory.Cleanup();
            _organizationFactory.Cleanup();
            base.Dispose();
        }

        #endregion
    }
}
