﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace DeviceSynchronizer.ApiReferences
{

    public class BaseApiRequest
    {
        #region Logger

        public static readonly ILog Log = LogManager.GetLogger("AttendanceApiRequestLogger");

        #endregion

        private string _baseUrl;
        public BaseApiRequest()
        {
            _baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
        }

        //Summation of strings to make a Url
        //return string
        public string MakeUrl(string controllerName, string functionName, string param)
        {
            return !String.IsNullOrEmpty(param) ? _baseUrl + controllerName + "//" + functionName + "?" + param : _baseUrl + controllerName + "//" + functionName;
        }

        //Accept a dictionary and add it's keypair value to generate a string
        //return string
        public string MakeParam(Dictionary<string, string> dataDictionary)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append("apiKey=" + Helper.Helper.GetApiKey());
                sb.Append("&syncKey=" + Helper.Helper.GetSyncKey());
                if (dataDictionary != null)
                {
                    foreach (var data in dataDictionary)
                    {
                        sb.Append("&" + data.Key + "=" + data.Value);
                    }
                }

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    }
}
