﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Hr
{
    public partial class DailySupportAllowanceBlockLogMap : ClassMap<DailySupportAllowanceBlockLog>
    {
        public DailySupportAllowanceBlockLogMap()
        {
            Table("HR_DailySupportAllowanceBlockLog");
            LazyLoad();

            References(x => x.BlockAllowance).Column("DailySupportAllowanceBlockId");
            Map(x => x.BlockAllowanceDate).Column("DailySupportAllowanceBlockDate");
            Map(x => x.IsBlocked).Column("IsBlocked");

            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
        }
    }
}
