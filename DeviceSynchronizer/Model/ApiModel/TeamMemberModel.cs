﻿namespace DeviceSynchronizer.Model.ApiModel
{
    public class TeamMemberModel
    {
        public string Name { get; set; }
        public string CardNumber { get; set; }
        public int Pin { get; set; }
        public int Status { get; set; }
        public int VersionNumber { get; set; }
        public virtual bool SaveToDeviceSuccessed { get; set; }
        public virtual int EnrollNo { get; set; }
        public bool IsAdmin { get; set; }
        public virtual string FingerIndex0 { get; set; }
        public virtual string FingerIndex1 { get; set; }
        public virtual string FingerIndex2 { get; set; }
        public virtual string FingerIndex3 { get; set; }
        public virtual string FingerIndex4 { get; set; }
        public virtual string FingerIndex5 { get; set; }
        public virtual string FingerIndex6 { get; set; }
        public virtual string FingerIndex7 { get; set; }
        public virtual string FingerIndex8 { get; set; }
        public virtual string FingerIndex9 { get; set; }

    }
}