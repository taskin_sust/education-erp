﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.AdminisTration.ActionTest
{
    public class ActionBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal ActionsService _actionsService;
        internal ActionsFactory _actionsFactory;
        internal AreaControllersService _areaControllersService;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        #endregion

        public ActionBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _actionsFactory = new ActionsFactory(null, _session);
            _actionsService = new ActionsService(_session);
            _areaControllersService=new AreaControllersService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _actionsFactory.Cleanup();
            base.Dispose();
            
        }
    }   
}
