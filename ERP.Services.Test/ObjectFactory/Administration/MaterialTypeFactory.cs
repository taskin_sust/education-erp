﻿using System;
using System.Collections.Generic;
using System.Globalization;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class MaterialTypeFactory : ObjectFactoryBase<MaterialType>
    {
        #region Object Initialization

        protected readonly IMaterialTypeService _materialTypeService;
        private OrganizationFactory _organizationFactory;

        public MaterialTypeFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _materialTypeService = new MaterialTypeService(_session);
            _organizationFactory = new OrganizationFactory(null, _session);
        }

        #endregion

        #region Create

        public MaterialTypeFactory WithName(string name) 
        {
            Object.Name = name;
            return this;
        }

        public MaterialTypeFactory WithShortName(string shortName) 
        {
            Object.ShortName = shortName;
            return this;
        } 

        public MaterialTypeFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }
        public MaterialTypeFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }
        public MaterialTypeFactory Create()
        {
            Object = new MaterialType()
            {
                ShortName = "M_MTC01" + "_" + Guid.NewGuid().ToString().Substring(0, 6),
                Rank = 1,
                Name = "TestMaterial" + "_" + Guid.NewGuid().ToString().Substring(0, 6),
                Code = "MTC01" + "_" + Guid.NewGuid().ToString().Substring(0, 6)
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MaterialTypeFactory CreateMore(int numOfCount)
        {
            ListStartIndex = ObjectList.Count;
            for (int i = 0; i < numOfCount; i++)
            {
                var obj = new MaterialType()
                {
                    ShortName = "MTSN_" + i.ToString(CultureInfo.CurrentCulture) + "_" + Guid.NewGuid().ToString().Substring(0, 6),
                    Rank = 1 + i,
                    Name = "TestMaterial_" + i.ToString(CultureInfo.CurrentCulture) + "_" + Guid.NewGuid().ToString().Substring(0, 6),
                    Code = "MTC" + i.ToString(CultureInfo.CurrentCulture) + "_" + Guid.NewGuid().ToString().Substring(0, 6)
                };
                ObjectList.Add(obj);
            }
            return this;
        }
        public MaterialTypeFactory CreateMore(int numOfCount, Organization orgObj)
        {
            ListStartIndex = ObjectList.Count;
            for (int i = 0; i < numOfCount; i++)
            {
                var obj = new MaterialType()
                {
                    ShortName = "MTSN_" + i.ToString(CultureInfo.CurrentCulture) + "_" + Guid.NewGuid().ToString().Substring(0, 6),
                    Rank = 1 + i,
                    Name = "TestMaterial_" + i.ToString(CultureInfo.CurrentCulture) + "_" + Guid.NewGuid().ToString().Substring(0, 6),
                    Code = "MTC" + i.ToString(CultureInfo.CurrentCulture) + "_" + Guid.NewGuid().ToString().Substring(0, 6),
                    Organization = orgObj,
                    OrganizationId = orgObj.Id
                };
                ObjectList.Add(obj);
            }
            return this;
        }
        public MaterialTypeFactory Persist()
        {
            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _materialTypeService.Save(item);
                    }
                }
            }
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _materialTypeService.Save(item);
                    }
                }
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void Cleanup()
        {
            DeleteAll();
            _organizationFactory.Cleanup();
        }

        #endregion

    }
}
