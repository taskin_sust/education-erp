using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Hr;
namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class NightWorkAllowanceSettingFactory : ObjectFactoryBase<NightWorkAllowanceSetting>
    {

        private readonly INightWorkAllowanceSettingService _prNightWorkAllowanceService;

        public NightWorkAllowanceSettingFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _prNightWorkAllowanceService = new NightWorkAllowanceSettingService(session);

        }

        #region Create

        public NightWorkAllowanceSettingFactory Create()
        {

            Object = new NightWorkAllowanceSetting()
            {
                Name = "Night Work Allowance " + Guid.NewGuid().ToString(),
                CalculationOn = (int)CalculationOn.Gross,
                InHouseMultiplyingFactor = (decimal)MultiplyingFactor.OneAndHalf,
                OutHouseMultiplyingFactor = (decimal)MultiplyingFactor.Two,
                EffectiveDate = DateTime.Now,
                IsContractual = true,
                IsIntern = true,
                IsPartTime = true,
                IsPermanent = true,
                IsProbation = true,
                MemberEmploymentStatuses = new List<MemberEmploymentStatus>() { MemberEmploymentStatus.Contractual, MemberEmploymentStatus.Intern,MemberEmploymentStatus.PartTime,
                MemberEmploymentStatus.Permanent,
                MemberEmploymentStatus.Probation,}
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public NightWorkAllowanceSettingFactory WithDatetime(DateTime effectiveDate, DateTime closingDate)
        {
            Object.EffectiveDate = effectiveDate;
            Object.ClosingDate = closingDate;
            return this;
        }

        public NightWorkAllowanceSettingFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public NightWorkAllowanceSettingFactory Persist(List<UserMenu> menus)
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _prNightWorkAllowanceService.SaveOrUpdate(menus, x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_prNightWorkAllowanceService.SaveOrUpdate(menus, x); });

            }
            return this;
        }

        public NightWorkAllowanceSettingFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new NightWorkAllowanceSetting() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other

        public NightWorkAllowanceSettingFactory WithOrganization(BusinessModel.Entity.Administration.Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        #endregion

        #region CleanUp

        public NightWorkAllowanceSettingFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion


    }
}
