﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MenuGroupTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "MenuGroup")]
    public class MenuGroupLoadByIdTest : MenuGroupBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _menuService.LoadById(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            MenuGroupFactory menugroupFactory =
                _menugroupFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithDisplayText(_displaytext)
                    .Persist();
            var obj = _menuService.LoadById(menugroupFactory.Object.Id);
            Assert.NotNull(obj);
            
        }

    }
}
