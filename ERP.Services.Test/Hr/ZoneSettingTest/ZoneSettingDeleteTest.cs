﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ZoneSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ZoneSetting")]
    public class ZoneSettingDeleteTest : ZoneSettingBaseTest
    {
        
        #region Basic Test
        
        [Fact]
        public void Should_Delete_Successfully()
        {
            var zoneFactory1 =
                _zoneSettingFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithToleranceTimeToleranceDayDeductionMultiplier(10, 2, .5m).Persist();
            ZoneSetting zoneSetting = _zoneSettingService.LoadById(zoneFactory1.Object.Id);
            var success = _zoneSettingService.Delete(zoneSetting.Id);
            Assert.True(success);
            zoneSetting = _zoneSettingService.LoadById(zoneFactory1.Object.Id);
            Assert.Null(zoneSetting);

        }

        [Fact]
        public void Delete_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _zoneSettingService.Delete(0));
        }
        
        [Fact]
        public void Delete_Holiday_Setting_Should_Not_Delete_Organization()
        {
            var zoneFactory1 =
                _zoneSettingFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithToleranceTimeToleranceDayDeductionMultiplier(10, 2, .5m).Persist();
            ZoneSetting zoneSetting = _zoneSettingService.LoadById(zoneFactory1.Object.Id);
            var organizationId = zoneSetting.Organization.Id;
            _zoneSettingService.Delete(zoneSetting.Id);
            var organization = _organizationService.LoadById(organizationId);
            Assert.NotNull(organization);
        }

        #endregion

    }
}
