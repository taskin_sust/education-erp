using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class DailySupportAllowanceBlockFactory : ObjectFactoryBase<DailySupportAllowanceBlock>
    {
        private readonly IDailyAllowanceBlockService _dailySupportAllowanceBlockService;
        private TestBaseService<DailySupportAllowanceBlockLog> _dailySupportAllowanceBlockLogService;
        public DailySupportAllowanceBlockFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _dailySupportAllowanceBlockService = new DailySupportAllowanceBlockService(session);
            _dailySupportAllowanceBlockLogService = new TestBaseService<DailySupportAllowanceBlockLog>(session);
        }

        #region Create

        public DailySupportAllowanceBlockFactory Create()
        {
            Object = new DailySupportAllowanceBlock()
            {
                Id = 0,
                IsBlocked = true,
                BlockingDate = DateTime.Now,
                Reason = "Testing",
                BusinessId = GetPcUserName()
            };
            SingleObjectList.Add(Object);
            return this;
        }

        /*public DailySupportAllowanceBlockFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x =>
                {
                    if (x.Id == 0) _dailySupportAllowanceBlockService.SaveOrUpdate();
                });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_dailySupportAllowanceBlockService.SaveOrUpdate(x); });

            }
            return this;
        }*/

        public DailySupportAllowanceBlockFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new DailySupportAllowanceBlock()
                {
                    Id = 0,
                    IsBlocked = true,
                    BlockingDate = DateTime.Now.AddDays(-1),
                    Reason = "Testing_" + i.ToString(CultureInfo.CurrentCulture),
                    BusinessId = GetPcUserName()
                };
                ObjectList.Add(obj);
            }
            return this;
        }
        public DailySupportAllowanceBlockFactory CreateMore(int num, IList<TeamMember> membersList)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new DailySupportAllowanceBlock()
                {
                    Id = 0,
                    IsBlocked = true,
                    BlockingDate = DateTime.Now.AddDays(-(1 + i)),
                    Reason = "Testing_" + i.ToString(CultureInfo.CurrentCulture),
                    BusinessId = GetPcUserName(),
                    TeamMember = membersList[i]
                };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public DailySupportAllowanceBlockFactory CleanUp()
        {
            var adIdList = _session.QueryOver<DailySupportAllowanceBlock>()
                            .Select(x => x.Id).Where(x => x.BusinessId == GetPcUserName().ToString(CultureInfo.InvariantCulture)).List<long>();
            var ids = new List<long>();
            foreach (long id in adIdList)
            {
                long id1 = id;
                DailySupportAllowanceBlockLog entity = _session.QueryOver<DailySupportAllowanceBlockLog>().Where(x => x.BlockAllowance.Id == id1).SingleOrDefault();
                if (entity != null) ids.Add(entity.Id);
            }
            if (adIdList.Count > 0)
                _dailySupportAllowanceBlockLogService.DeleteByIdList(ids.ToList());
            DeleteAll();
            return this;
        }

        #endregion

        #region With function

        public DailySupportAllowanceBlockFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }
        #endregion

    }
}
