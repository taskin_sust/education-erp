﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BranchTest
{
    interface IBranchLoadByIdTest
    {
        void LoadById_Null_Check();
        void LoadById_NotNull_Check();
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Branch")]
    public class BranchLoadByIdTest : BranchBaseTest, IBranchLoadByIdTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _branchService.GetBranch(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            BranchFactory branchFactory =
                _branchFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithCode(_branchFactory.GetRandomNumber())
                    .WithOrganization()
                    .Persist();
            var obj = _branchService.GetBranch(branchFactory.Object.Id);
            Assert.NotNull(obj);
            
        }

    }
}
