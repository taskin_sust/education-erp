﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ICourseProgressService : IBaseService
    {
        #region Operational Function
        bool Save(List<Lecture> lectures, List<Batch> batches,DateTime date);
        bool Delete(long id);
        bool SaveExamProgress(List<BusinessModel.Entity.Exam.Exams> list, List<Batch> batchList, DateTime dateTime);
        bool DeleteExamProgress(long id);
        #endregion

        #region Single Instances Loading Function
        CourseProgress GetCourseProgress(int id);
        ExamProgress GetExamProgress(int id);
        #endregion

        #region List Loading Function
        List<CourseProgress> LoadCourseProgress(int start,int length, string branch, string campus, string batch, string subject, string lecture,IList<Lecture> lectureList, List<Batch> batchList);
        List<ExamProgress> LoadExamProgress(int start, int length, string branch, string campus, string batch, string subject, string exam, IList<BusinessModel.Entity.Exam.Exams> examList, List<Batch> batchList);
        #endregion

        #region Others Function

        int GetCourseProgressCount(string branch, string campus, string batch,
            string subject, string lecture, IList<Lecture> lectureList, List<Batch> batchList);
        int GetExamProgressCount(string branch, string campus, string batch, string subject, string exam, IList<BusinessModel.Entity.Exam.Exams> examList, List<Batch> batchList);
        #endregion

        #region Helper Function

        #endregion

        
    }

    public class CourseProgressService : BaseService, ICourseProgressService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IBatchDao _batchDao;
        private readonly ICourseProgressDao _courseProgressDao;
        private readonly IExamProgressDao _examProgressDao;
        private readonly CommonHelper _commonHelper;
        public CourseProgressService(ISession session)
        {
            Session = session;
            _batchDao = new BatchDao() { Session = session };
            _courseProgressDao=new CourseProgressDao(){ Session=session };
            _examProgressDao=new ExamProgressDao(){ Session = session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public bool Save(List<Lecture> lectures, List<Batch> batches,DateTime date)
        {

            ITransaction trans = null;
            try
            {

                using (trans = Session.BeginTransaction())
                {
                    #region old code
                    //foreach (var batch in batches)
                    //{
                    //    var lecturesNotAssignedAlready = lectures.Except(batch.Lectures).ToList();
                    //    if (batch.Lectures.Count > 0)
                    //    {
                    //        batch.Lectures = batch.Lectures.Concat(lecturesNotAssignedAlready).ToList();
                    //    }
                    //    else
                    //    {
                    //        batch.Lectures = lecturesNotAssignedAlready.ToList();
                    //    }
                    //    Session.Update(batch);
                    //}
                    //foreach (var lecture in lectures)
                    //{
                    //    var batchesNotAssignedAlready = batches.Except(lecture.Batches).ToList();
                    //    if (lecture.Batches.Count > 0)
                    //    {
                    //        lecture.Batches = lecture.Batches.Concat(batchesNotAssignedAlready).ToList();
                    //    }
                    //    else
                    //    {
                    //        lecture.Batches = batchesNotAssignedAlready.ToList();
                    //    }
                    //    Session.Update(lecture);
                    //} 
                    #endregion
                    
                    foreach (var batch in batches)
                    {
                        foreach (var lecture in lectures)
                        {
                            var courseProgress=new CourseProgress();
                            courseProgress.Lecture = lecture;
                            courseProgress.Batch = batch;
                            courseProgress.HeldDate = date;
                            courseProgress.Course = lecture.LectureSettings.Course;
                            courseProgress.CourseSubject = lecture.LectureSettings.CourseSubject;
                            _courseProgressDao.Save(courseProgress);
                        }
                    }
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool Delete(long id)
        {
            ITransaction transaction = null;
            try
            {
                var tempCourseProgressObj = _courseProgressDao.LoadById(id);
               // CheckBeforeDelete(tempSessionObj);
                using (transaction = Session.BeginTransaction())
                {
                    tempCourseProgressObj.Status = BusinessModel.Entity.Administration.Session.EntityStatus.Delete;
                    _courseProgressDao.Update(tempCourseProgressObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool DeleteExamProgress(long id)
        {
            ITransaction transaction = null;
            try
            {
                var tempExamProgressObj = _examProgressDao.LoadById(id);
                // CheckBeforeDelete(tempSessionObj);
                using (transaction = Session.BeginTransaction())
                {
                    tempExamProgressObj.Status = BusinessModel.Entity.Administration.Session.EntityStatus.Delete;
                    _examProgressDao.Update(tempExamProgressObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool SaveExamProgress(List<BusinessModel.Entity.Exam.Exams> examList, List<Batch> batchList, DateTime heldDate)
        {
            ITransaction trans = null;
            try
            {

                using (trans = Session.BeginTransaction())
                {

                    foreach (var batch in batchList)
                    {
                        foreach (var exam in examList)
                        {
                            var examProgress = new ExamProgress();
                            examProgress.Exam = exam;
                            examProgress.Batch = batch;
                            examProgress.HeldDate = heldDate;
                            examProgress.Course = exam.Course;
                            _examProgressDao.Save(examProgress);
                        }
                    }
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function

        public CourseProgress GetCourseProgress(int id)
        {
            CourseProgress courseProgress=null;
            try
            {
                courseProgress= _courseProgressDao.LoadById(id);
            }
            catch (Exception ex)
            {
                
                _logger.Error(ex);
            }
            return courseProgress;
        }

        public ExamProgress GetExamProgress(int id)
        {
            ExamProgress examProgress = null;
            try
            {
                examProgress = _examProgressDao.LoadById(id);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return examProgress;
        }
        #endregion

        #region List Loading Function

        public List<CourseProgress> LoadCourseProgress(int start, int length, string branch, string campus, string batch, string subject, string lecture, IList<Lecture> lectureList, List<Batch> batchList)
        {
            var courseProgressList = new List<CourseProgress>();
            try
            {
                courseProgressList = _courseProgressDao.LoadCourseProgress(start, length, branch, campus, batch, subject, lecture, lectureList, batchList);
            }
            catch (Exception ex)
            {
                
                _logger.Error(ex);
            }
            return courseProgressList;
        }

        public List<ExamProgress> LoadExamProgress(int start, int length, string branch, string campus, string batch,
            string subject, string exam, IList<BusinessModel.Entity.Exam.Exams> examList, List<Batch> batchList)
        {
            var examProgressList = new List<ExamProgress>();
            try
            {
                examProgressList = _examProgressDao.LoadExamProgress(start, length, branch, campus, batch, subject, exam, examList, batchList);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return examProgressList;
        }
        #endregion

        #region Others Function

        public int GetCourseProgressCount(string branch, string campus, string batch,
            string subject, string lecture, IList<Lecture> lectureList, List<Batch> batchList)
        {
            int count = 0;
            try
            {
                count = _courseProgressDao.GetCourseProgressCount(branch, campus, batch,
                        subject, lecture, lectureList, batchList);
            }
            catch (Exception ex)
            {
                
                _logger.Error(ex);
            }
            return count;
        }

        public int GetExamProgressCount(string branch, string campus, string batch, string subject, string exam,
            IList<BusinessModel.Entity.Exam.Exams> examList, List<Batch> batchList)
        {
            int count = 0;
            try
            {
                count = _examProgressDao.GetExamProgressCount(branch, campus, batch,
                        subject, exam, examList, batchList);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return count;
        }
        #endregion

        #region Helper function



        #endregion
    }
}
