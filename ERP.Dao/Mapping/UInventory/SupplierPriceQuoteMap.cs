using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class SupplierPriceQuoteMap : BaseClassMap<SupplierPriceQuote, long>
    {
        public SupplierPriceQuoteMap()
        {
            Table("UINV_SupplierPriceQuote");
            LazyLoad();
            References(x => x.Supplier).Column("SupplierId");
            References(x => x.Quotation).Column("QuotationId");
            Map(x => x.Name).Column("Name");
            Map(x => x.Rank).Column("Rank");
            Map(x => x.PriceQuoteStatus).Column("PriceQuoteStatus").Not.Nullable();
            Map(x => x.QuotedUnitCost).Column("QuotedUnitCost").Not.Nullable();
            Map(x => x.TotalCost).Column("TotalCost").Not.Nullable();
        }
    }
}
