using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.BusinessRules.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class GoodsIssueFactory : ObjectFactoryBase<GoodsIssue>
   {
       private readonly IList<string> AssignedIssueNoList = new List<string>();
       
       private BranchFactory _branchFactory;
       private IGoodsIssueService _goodsIssueService;
       private GoodsIssueDetailsFactory _goodsIssueDetailsFactory;
       private ItemFactory _itemFactory;
       private ProgramFactory _programFactory;
       private SessionFactory _sessionFactory;



       public GoodsIssueFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
       {
          _branchFactory = new BranchFactory(this, _session);
          _goodsIssueService = new GoodsIssueService(session);
          _goodsIssueDetailsFactory = new GoodsIssueDetailsFactory(null, _session);
          _itemFactory = new ItemFactory(null, _session);
          _programFactory = new ProgramFactory(this, _session);
          _sessionFactory = new SessionFactory(this, _session);

       }

       #region Create 

       public GoodsIssueFactory Create()
       {
           Object = new GoodsIssue()
           {
               Id = 0,
               BusinessId = GetPcUserName(),
               GoodsIssueNo = "UDV12GI000001",
               Remarks = "Goods Issue",
               CreateBy = 1,
               ModifyBy = 1,
               Status = GoodsIssue.EntityStatus.Active
           };
           SingleObjectList.Add(Object);
           return this;
       }
       public GoodsIssueFactory Create(Branch branch)
       {
           Object = new GoodsIssue()
           {
               Id = 0,
               BusinessId = GetPcUserName(),
               GoodsIssueNo = GetGoodsIssueNo(branch),
               Remarks = "Goods Issue",
               CreateBy = 1,
               ModifyBy = 1,
               Status = GoodsIssue.EntityStatus.Active
           };
           SingleObjectList.Add(Object);
           return this;
       }

       public GoodsIssueFactory WithBranch()
       {
           var branch = _branchFactory.Create().WithOrganization().Persist().Object;
           branch.IsCorporate = true;
           Object.Branch = branch;
           return this;
       }


       public GoodsIssueFactory CreateMore(int numOfGoodsIssue)
       {
           CreateGoodsIssueList(numOfGoodsIssue);
           return this;
       }

       private GoodsIssueFactory CreateGoodsIssueList(int numOfGoodsIssue)
       {
           ListStartIndex = ObjectList.Count;
           var list = new List<GoodsIssue>();
           var program = _programFactory.Create().WithOrganization().Persist().Object;
           var branch = _branchFactory.Create().WithOrganization(program.Organization).Persist().Object;
           branch.IsCorporate = true;
           var goodsIssueDetails = _goodsIssueDetailsFactory.CreateMore(numOfGoodsIssue).Persist().GetLastCreatedObjectList();
           for (int i = 0; i < numOfGoodsIssue; i++)
           {
               var obj = new GoodsIssue()
               {
                   Id = 0,
                   BusinessId = GetPcUserName(),
                   Branch = branch,
                   GoodsIssueNo = GetGoodsIssueNo(branch),
                   Remarks = "Goods Issue xUnit Testing",
                   CreateBy = 1,
                   ModifyBy = 1,
                   Status = GoodsIssue.EntityStatus.Active
               };
               obj.GoodsIssueDetails = new List<GoodsIssueDetails>()
               {
                    goodsIssueDetails[i]   
               };
               list.Add(obj);
           }
           ObjectList.AddRange(list);
           return this;
       }


       public GoodsIssueFactory WithBranch(Branch branch)
       {
           Object.Branch = branch;
           return this;
       }

       public GoodsIssueFactory WithRequisition(Requisition requisition)
       {
           Object.Requisition = requisition;
           return this;
       }

       public GoodsIssueFactory WithGoodsIssueDetails(Organization organization)
       {
           var item = _itemFactory.Create().WithItemGroup().WithOrganization(organization).Persist().Object;
           var program = _programFactory.Create().WithOrganization(organization).Persist().Object;
           var session = _sessionFactory.Create().Persist().Object;
           var goodsIssueDetails = _goodsIssueDetailsFactory.Create(program, session, item, Object).Persist().Object;
           Object.GoodsIssueDetails = new List<GoodsIssueDetails>()
            {
                goodsIssueDetails
            };
           return this;
       }

       internal GoodsIssueFactory CreateMoreDetails(List<GoodsIssueDetails> list)
       {
           Object.GoodsIssueDetails = list;
           return this;
       }
       
       public GoodsIssueFactory Persist()       
       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x => { if (x.Id == 0) _goodsIssueService.SaveOrUpdate(x); });
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x => { if (x.Id == 0)_goodsIssueService.SaveOrUpdate(x); });

          }
          return this;
       }

       #endregion

       #region other

       private string GetGoodsIssueNo(Branch branchObj)
       {
           int min = 1;
           const int max = 999999;
           var organizationName = branchObj.Organization.ShortName.Substring(0, 3);
           var branchCode = branchObj.Code.ToString(CultureInfo.CurrentCulture);
           do
           {
               string name = "";
               var r = new Random();
               var x = r.Next(min, max);
               string newSl = x.ToString("000000");

               name = organizationName + branchCode + "GI" + newSl;
               if (!AssignedIssueNoList.Contains(name))
               {
                   AssignedIssueNoList.Add(name);
                   return name;
               }
               min++;

           } while (min <= max);
           return "Error Goods Issue Genaration";
       }

       #endregion
       
       #region CleanUp 

       public GoodsIssueFactory CleanUp()
       {
           _goodsIssueDetailsFactory.CleanUp();
             DeleteAll();
            _itemFactory.CleanUp();
            _sessionFactory.Cleanup();
            _branchFactory.CleanupWithoutOrganization();
            _programFactory.Cleanup();
           
            

          return this;
       }

       #endregion

    }
}
