﻿/***********************************************************************
 * File Name    : AppConfigHelper.cs
 * Class        : AppConfigHelper
 * Author       : S. M. Quamruzzaman Rahmani (Byron)
 * Copyright    : OnnoRokom Software Ltd.
 * Created      : 01/06/2012
 * Last Modify  : 12/11/2013
 * Version      : 1.2.0
 * Description  : Perform data load & save to App Config
***********************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Text;

namespace Synchronizer.Updater.Helper
{
    public class AppConfigHelper
    {
        public static bool SaveAppConfiguration(string key, string value)
        {
            Dictionary<string, string> settingsList = new Dictionary<string, string>();
            settingsList.Add(key, value);
            return SaveAppConfiguration(settingsList);
        }

        public static bool SaveAppConfiguration(Dictionary<string, string> settingsList)
        {
            try
            {
                //get object
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                foreach (KeyValuePair<string, string> obj in settingsList)
                {
                    if (config.AppSettings.Settings[obj.Key] != null)
                        config.AppSettings.Settings.Remove(obj.Key);
                    config.AppSettings.Settings.Add(obj.Key, obj.Value);
                }

                //save settings
                config.Save();
                ConfigurationManager.RefreshSection("appSettings");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool GetBoolean(string key)
        {
            return GetBoolean(key, false);
        }

        public static bool GetBoolean(string key, bool defaultValue)
        {
            bool result = defaultValue;
            if (bool.TryParse(GetString(key), out result) == false)
                result = defaultValue;
            return result;
        }


        public static int GetInt32(string key)
        {
            return GetInt32(key, 0);
        }

        public static int GetInt32(string key, int defaultValue)
        {
            int result = defaultValue;
            if (int.TryParse(GetString(key), out result) == false)
                result = defaultValue;
            return result;
        }


        public static decimal GetDecimal(string key)
        {
            return GetDecimal(key, 0);
        }

        public static decimal GetDecimal(string key, decimal defaultValue)
        {
            decimal result = defaultValue;
            if (decimal.TryParse(GetString(key), out result) == false)
                result = defaultValue;
            return result;
        }


        public static object GetEnum(string key, Type enumType, object defaultValue)
        {
            object result = null;
            try
            {
                result = Enum.Parse(enumType, GetString(key), true);
            }
            catch { }
            if (result == null)
                result = defaultValue;
            return result;
        }


        public static string GetString(string key)
        {
                return GetString(key, "");
        }

        public static string GetString(string key, string defaultValue)
        {
            if (ConfigurationManager.AppSettings[key] == null)
                return defaultValue;
            else
                return ConfigurationManager.AppSettings[key];
        }

        public static Color GetColorByArgb(string key, bool isCsv)
        {
            return GetColorByArgb(key, Color.Black.ToArgb().ToString(), isCsv);
        }
        public static Color GetColorByArgb(string key, string defaultValue, bool isCsv)
        {
            if (isCsv)
            {
                try
                {
                    var argb = GetString(key, defaultValue).Split(',');
                    return Color.FromArgb(int.Parse(argb[0]), int.Parse(argb[1]), int.Parse(argb[2]), int.Parse(argb[3]));
                }
                catch (Exception)
                {
                    return Color.FromArgb(int.Parse(defaultValue));
                }
            }
            else
            {
                int argb = GetInt32(key, int.Parse(defaultValue));
                return Color.FromArgb(argb);
            }
        }

        public static Color GetColorByName(string key)
        {
            return GetColorByName(key, Color.Black.ToString());
        }

        public static Color GetColorByName(string key, string defaultValue)
        {
            string colorName = GetString(key, defaultValue);
            try
            {
                return Color.FromName(colorName);
            }
            catch (Exception)
            {
                return Color.FromName(defaultValue);
            }
        }

        public static string ColorArgbToCsv(Color color)
        {
            var sb = new StringBuilder();
            sb.Append(color.A);
            sb.Append(",");
            sb.Append(color.R);
            sb.Append(",");
            sb.Append(color.G);
            sb.Append(",");
            sb.Append(color.B);
            return sb.ToString();
        }
    }
}
