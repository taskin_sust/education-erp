﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
   public class StudentExamAttendanceDetailsMap : BaseClassMap<StudentExamAttendanceDetails, long>
    {
       public StudentExamAttendanceDetailsMap()
       {
           Table("StudentExamAttendanceDetails");
           LazyLoad();
           References(x => x.StudentExamAttendance).Column("StudentExamAttendanceId");
           References(x => x.StudentProgram).Column("StudentProgramId");
           Map(x => x.StudentFeedback).Column("StudentFeedBack");
           Map(x => x.IsRegistered).Column("IsRegistered");
       }
    }
}
