﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Hr
{
    public interface IMemberFingerPrintService : IBaseService
    {
        #region List Loading
        IList<MemberInfoApi> GetMemberInfoForDeviceApiByPin(int pin);

        #endregion

        #region Operational Method

        void SaveOrUpdate(MemberInfoDeviceApi memberInfoApiObj);
        void Update(MemberFingerPrint memberFingerTemplate, int index);

        #endregion

        #region Single Object Load

        MemberFingerPrint GetMemberFingerPrint(long id);

        #endregion
    }
    public class MemberFingerPrintService : BaseService, IMemberFingerPrintService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Properties & Object Initialization

        private readonly IAttendanceDeviceTypeDao _attendanceDeviceTypeDao;
        private readonly IMemberFingerPrintLogDao _memberFingerPrintLogDao;
        private readonly IMemberFingerPrintDao _memberFingerPrintDao;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly IAttendanceDeviceMemberDao _attendanceDeviceMemberDao;

        public MemberFingerPrintService(ISession session)
        {
            Session = session;
            _attendanceDeviceTypeDao = new AttendanceDeviceTypeDao() { Session = session };
            _memberFingerPrintLogDao = new MemberFingerPrintLogDao() { Session = session };
            _memberFingerPrintDao = new MemberFingerPrintDao() { Session = session };
            _teamMemberDao = new TeamMemberDao() { Session = session };
            _attendanceDeviceMemberDao = new AttendanceDeviceMemberDao() { Session = session };
        }

        #endregion

        #region Operational Method

        public void SaveOrUpdate(MemberInfoDeviceApi memberInfoApiObj)
        {
            ITransaction transaction = null;
            MemberFingerPrint memberFingerPrint = null;
            try
            {
                if (memberInfoApiObj == null) throw new InvalidDataException("Empty Finger Object");
                if (memberInfoApiObj.Pin <= 0) throw new InvalidDataException("Empty Pin");
                TeamMember member = _teamMemberDao.GetMember(memberInfoApiObj.Pin);
                if (member == null) throw new InvalidDataException("Invalid Pin");
                foreach (
                    var attendanceDeviceApiModel in memberInfoApiObj.AttendanceDeviceApiModel.Where(x => x.IsFinger))
                {
                    using (transaction = Session.BeginTransaction())
                    {
                        if (String.IsNullOrEmpty(attendanceDeviceApiModel.ModelName))
                            throw new InvalidDataException("Invalid Model Name");
                        if (attendanceDeviceApiModel.ModelId <= 0) throw new InvalidDataException("Invalid Model Id");
                        var fingerPrintObj = _memberFingerPrintDao.GetByTeamMemberAndModelId(member.Id, attendanceDeviceApiModel.ModelId);
                        var attendanceDeviceType = _attendanceDeviceTypeDao.LoadById(attendanceDeviceApiModel.ModelId);
                        bool isDuplicate = CheckBeforeSaveOrUpdate(attendanceDeviceApiModel, fingerPrintObj);
                        if (isDuplicate)
                            throw new DuplicateEntryException("Duplicate Finger Print found in " + attendanceDeviceType.Name);
                        if (fingerPrintObj == null)
                        {
                            memberFingerPrint = new MemberFingerPrint();
                            memberFingerPrint.AttendanceDeviceType = attendanceDeviceType;
                            memberFingerPrint.TeamMember = member;
                            memberFingerPrint.Index0 = attendanceDeviceApiModel.Index0;
                            memberFingerPrint.Index1 = attendanceDeviceApiModel.Index1;
                            memberFingerPrint.Index2 = attendanceDeviceApiModel.Index2;
                            memberFingerPrint.Index3 = attendanceDeviceApiModel.Index3;
                            memberFingerPrint.Index4 = attendanceDeviceApiModel.Index4;
                            memberFingerPrint.Index5 = attendanceDeviceApiModel.Index5;
                            memberFingerPrint.Index6 = attendanceDeviceApiModel.Index6;
                            memberFingerPrint.Index7 = attendanceDeviceApiModel.Index7;
                            memberFingerPrint.Index8 = attendanceDeviceApiModel.Index8;
                            memberFingerPrint.Index9 = attendanceDeviceApiModel.Index9;
                            _memberFingerPrintDao.Save(memberFingerPrint);
                        }
                        else
                        {
                            fingerPrintObj.AttendanceDeviceType = attendanceDeviceType;
                            fingerPrintObj.TeamMember = member;
                            fingerPrintObj.Index0 = attendanceDeviceApiModel.Index0;
                            fingerPrintObj.Index1 = attendanceDeviceApiModel.Index1;
                            fingerPrintObj.Index2 = attendanceDeviceApiModel.Index2;
                            fingerPrintObj.Index3 = attendanceDeviceApiModel.Index3;
                            fingerPrintObj.Index4 = attendanceDeviceApiModel.Index4;
                            fingerPrintObj.Index5 = attendanceDeviceApiModel.Index5;
                            fingerPrintObj.Index6 = attendanceDeviceApiModel.Index6;
                            fingerPrintObj.Index7 = attendanceDeviceApiModel.Index7;
                            fingerPrintObj.Index8 = attendanceDeviceApiModel.Index8;
                            fingerPrintObj.Index9 = attendanceDeviceApiModel.Index9;
                            _memberFingerPrintDao.Update(fingerPrintObj);
                        }

                        foreach (var attendanceDevice in attendanceDeviceType.AttendanceDevices)
                        {
                            AttendanceDeviceMember attendanceDeviceMember =
                                _attendanceDeviceMemberDao.GetAttendanceDeviceMember(member.Id, attendanceDevice.Id);
                            if (attendanceDeviceMember != null)
                            {
                                attendanceDeviceMember.LastUpdateDateTime = DateTime.Now;
                                Session.Update(attendanceDeviceMember);
                            }
                        }
                        var fingerLog = new MemberFingerPrintLog
                        {
                            Index0 = attendanceDeviceApiModel.Index0,
                            Index1 = attendanceDeviceApiModel.Index1,
                            Index2 = attendanceDeviceApiModel.Index2,
                            Index3 = attendanceDeviceApiModel.Index3,
                            Index4 = attendanceDeviceApiModel.Index4,
                            Index5 = attendanceDeviceApiModel.Index5,
                            Index6 = attendanceDeviceApiModel.Index6,
                            Index7 = attendanceDeviceApiModel.Index7,
                            Index8 = attendanceDeviceApiModel.Index8,
                            Index9 = attendanceDeviceApiModel.Index9,
                            MemberFingerPrint =
                                memberFingerPrint != null && memberFingerPrint.TeamMember != null
                                    ? memberFingerPrint.Id
                                    : fingerPrintObj.Id
                        };
                        Session.Save(fingerLog);
                        transaction.Commit();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void Update(MemberFingerPrint memberFingerTemplate, int index)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    if (index == 0)
                        memberFingerTemplate.Index0 = null;
                    if (index == 1)
                        memberFingerTemplate.Index1 = null;
                    if (index == 2)
                        memberFingerTemplate.Index2 = null;
                    if (index == 3)
                        memberFingerTemplate.Index3 = null;
                    if (index == 4)
                        memberFingerTemplate.Index4 = null;
                    if (index == 5)
                        memberFingerTemplate.Index5 = null;
                    if (index == 6)
                        memberFingerTemplate.Index6 = null;
                    if (index == 7)
                        memberFingerTemplate.Index7 = null;
                    if (index == 8)
                        memberFingerTemplate.Index8 = null;
                    if (index == 9)
                        memberFingerTemplate.Index9 = null;

                    memberFingerTemplate.ModifyBy = GetCurrentUserId();
                    memberFingerTemplate.ModificationDate = DateTime.Now;
                    _memberFingerPrintDao.Update(memberFingerTemplate);

                    var fingerLog = new MemberFingerPrintLog
                    {
                        Index0 = memberFingerTemplate.Index0,
                        Index1 = memberFingerTemplate.Index1,
                        Index2 = memberFingerTemplate.Index2,
                        Index3 = memberFingerTemplate.Index3,
                        Index4 = memberFingerTemplate.Index4,
                        Index5 = memberFingerTemplate.Index5,
                        Index6 = memberFingerTemplate.Index6,
                        Index7 = memberFingerTemplate.Index7,
                        Index8 = memberFingerTemplate.Index8,
                        Index9 = memberFingerTemplate.Index9,
                        MemberFingerPrint = memberFingerTemplate != null && memberFingerTemplate.TeamMember != null ? memberFingerTemplate.Id : memberFingerTemplate.Id
                    };
                    Session.Save(fingerLog);

                    var deviceMembers = _attendanceDeviceMemberDao.LoadAttendanceDeviceMember(memberFingerTemplate.TeamMember.Id);
                    var lastUpdateDateTime = DateTime.Now;
                    foreach (var deviceMember in deviceMembers)
                    {
                        deviceMember.LastUpdateDateTime = lastUpdateDateTime;
                        Session.Update(deviceMember);
                    }
                    transaction.Commit();
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Object Load

        public MemberFingerPrint GetMemberFingerPrint(long id)
        {
            MemberFingerPrint memberFingerPrint = new MemberFingerPrint();
            try
            {
                memberFingerPrint = _memberFingerPrintDao.LoadById(id);
            }
            catch (Exception ex)
            {
                throw;
            }
            return memberFingerPrint;
        }

        #endregion

        #region List Load

        public IList<MemberInfoApi> GetMemberInfoForDeviceApiByPin(int pin)
        {
            try
            {
                if (pin <= 0) throw new InvalidDataException("Invalid Pin");
                return _memberFingerPrintDao.LoadMemberInfoForDeviceApiByPin(pin);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Helper

        private bool CheckBeforeSaveOrUpdate(AttendanceDeviceApiModel attendanceDeviceApiModel, MemberFingerPrint fingerPrint)
        {
            var fingerList = new List<string>();
            if (!String.IsNullOrEmpty(attendanceDeviceApiModel.Index0))
                fingerList.Add(attendanceDeviceApiModel.Index0);
            if (!String.IsNullOrEmpty(attendanceDeviceApiModel.Index1))
                fingerList.Add(attendanceDeviceApiModel.Index1);
            if (!String.IsNullOrEmpty(attendanceDeviceApiModel.Index2))
                fingerList.Add(attendanceDeviceApiModel.Index2);
            if (!String.IsNullOrEmpty(attendanceDeviceApiModel.Index3))
                fingerList.Add(attendanceDeviceApiModel.Index3);
            if (!String.IsNullOrEmpty(attendanceDeviceApiModel.Index4))
                fingerList.Add(attendanceDeviceApiModel.Index4);
            if (!String.IsNullOrEmpty(attendanceDeviceApiModel.Index5))
                fingerList.Add(attendanceDeviceApiModel.Index5);
            if (!String.IsNullOrEmpty(attendanceDeviceApiModel.Index6))
                fingerList.Add(attendanceDeviceApiModel.Index6);
            if (!String.IsNullOrEmpty(attendanceDeviceApiModel.Index7))
                fingerList.Add(attendanceDeviceApiModel.Index7);
            if (!String.IsNullOrEmpty(attendanceDeviceApiModel.Index8))
                fingerList.Add(attendanceDeviceApiModel.Index8);
            if (!String.IsNullOrEmpty(attendanceDeviceApiModel.Index9))
                fingerList.Add(attendanceDeviceApiModel.Index9);
            if (fingerList.Count > 0)
            {
                long id = 0;
                if (fingerPrint != null)
                    id = fingerPrint.Id;
                IList<MemberFingerPrint> list = _memberFingerPrintDao.LoadByFinderIndex(fingerList, id);
                if (fingerPrint == null)
                {
                    if (list != null && list.Count > 0)
                        return true;
                }
                else
                    if (list != null && list.Count > 1)
                        return true;
                //return true;
            }
            return false;
        }

        #endregion

    }
}
