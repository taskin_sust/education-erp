﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ICampusService : IBaseService
    {
        #region Operational Function
        bool Save(Campus campusObj);
        bool Update(long id, Campus campusObj);
        bool Delete(long id, Campus campus);
        bool UpdateRank(Campus campusObjOld, Campus campusObjUpdated);
        #endregion

        #region Single Instances Loading Function
        Campus GetCampus(long campusId);
        Campus GetCampusByRankNextOrPrevious(int newRank, string action);
        #endregion

        #region List Loading Function
        IList<Campus> LoadCampus(List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, bool isProgramBranSession = true);
        IList<Campus> LoadCampus(List<UserMenu> userMenu, int start, int length, string orderBy, string direction, string orgId, string bId, string cName, string contactNumber, string cRank, string cStatus);
        IList<Campus> LoadCampusByBranchIdsProgramSession(long[] branchId, long programId, long sessionId);
        IList<Campus> LoadAuthorizeCampus(List<UserMenu> userMenus, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null, bool isProgramBranSession = true);

        IList<Campus> LoadCampus(long[] campusId);

        #endregion

        #region Others Function
        int GetMaximumRank(Campus campus);
        int GetMinimumRank(Campus campus);
        int GetCampusCount(List<UserMenu> userMenu, string orgId, string bId, string cName, string contactNumber, string cRank, string cStatus);
        #endregion

        #region Public API
        IList<Campus> LoadPublicCampusByProgramSessionBranch(string orgBusinessId, long programId, long sessionId, long branchId);
        #endregion
    }
    public class CampusService : BaseService, ICampusService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ICampusDao _campusDao;
        private readonly ICampusRoomDao _campusRoomDao;
        private readonly IBatchDao _batchDao;
        private CommonHelper _commonHelper;
        public CampusService(ISession session)
        {
            Session = session;
            _campusDao = new CampusDao() { Session = session };
            _campusRoomDao = new CampusRoomDao() { Session = session };
            _batchDao = new BatchDao() { Session = session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function
        public bool Save(Campus campusObj)
        {
            ITransaction trans = null;
            try
            {
                if (campusObj == null)
                {
                    throw new NullObjectException("campus can not be null");
                }
                DoBeforeAdd(campusObj);
                using (trans = Session.BeginTransaction())
                {
                    int campusRoomRank = _campusRoomDao.GetMaximumRank(campusObj.CampusRooms[0]);
                    int campusRank = _campusDao.GetMaximumRank(campusObj);
                    campusObj.Rank = campusRank + 1;
                    campusObj.CampusRooms.ForEach(a =>
                    {
                        a.Campus = campusObj;
                        a.Status = CampusRoom.EntityStatus.Active;
                        a.CreationDate = a.ModificationDate = DateTime.Now;
                        a.Rank = campusRoomRank += 1;
                        //add createBy and ModifyBy
                        a.CreateBy = GetCurrentUserId();
                        a.ModifyBy = GetCurrentUserId();
                    });
                    _campusDao.Save(campusObj);
                    trans.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (ValueNotAssignedException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool Update(long id, Campus campusObj)
        {
            ITransaction trans = null;
            try
            {
                DoBeforeUdpate(campusObj);
                using (trans = Session.BeginTransaction())
                {
                    //update campus value
                    var dbCampus = _campusDao.LoadById(id);
                    dbCampus.Branch = campusObj.Branch;
                    dbCampus.Name = campusObj.Name;
                    dbCampus.Location = campusObj.Location;
                    dbCampus.ContactNumber = campusObj.ContactNumber;
                    dbCampus.Status = campusObj.Status;
                    //update campus room value 
                    int campusRoomRank = _campusRoomDao.GetMaximumRank(new CampusRoom());
                    campusObj.CampusRooms.Where(a => a.Id == 0).ForEach(a =>
                    {
                        a.Campus = dbCampus;
                        a.Status = CampusRoom.EntityStatus.Active;
                        a.CreationDate = a.ModificationDate = DateTime.Now;
                        a.Rank = campusRoomRank += 1;
                        a.CreateBy = GetCurrentUserId();
                            //Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                        //a.ModifyBy = a.ModifyBy != 0 ? a.ModifyBy : Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                    });
                    //campusObj.CampusRooms.ForEach(a =>
                    //{
                    //    a.ModifyBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                    //});
                    foreach (var a in campusObj.CampusRooms)
                    {
                        if (HttpContext.Current != null)
                            if (HttpContext.Current.User != null)
                                a.ModifyBy = GetCurrentUserId();//Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                    }

                    //update old campus rooms. which already saved with ZERO status
                    campusObj.CampusRooms.Where(a => a.Status == 0).ForEach(a =>
                    {
                        a.Status = CampusRoom.EntityStatus.Active;
                    });
                    //update old campus rooms. which already saved with ZERO Createby
                    campusObj.CampusRooms.Where(a => a.CreateBy == 0).ForEach(a =>
                    {
                        a.CreateBy = campusObj.CreateBy;
                    });
                    //update old campus rooms. which already saved with ZERO ModifyBy
                    campusObj.CampusRooms.Where(a => a.ModifyBy == 0).ForEach(a =>
                    {
                        a.ModifyBy = campusObj.ModifyBy;
                    });

                    dbCampus.CampusRooms.Clear();
                    campusObj.CampusRooms.ForEach(m => dbCampus.CampusRooms.Add(m));
                    _campusDao.SaveOrUpdate(dbCampus);

                    trans.Commit();
                    return true;
                }
            }
            catch (ValueNotAssignedException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }

        }

        public bool Delete(long id, Campus campusObj)
        {
            ITransaction trans = null;
            try
            {
                DoBeforeDelete(campusObj);
                using (trans = Session.BeginTransaction())
                {
                    Campus dbCampus = _campusDao.LoadById(id);
                    dbCampus.Status = Campus.EntityStatus.Delete;
                    _campusDao.Update(dbCampus);
                    trans.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool UpdateRank(Campus campusObjOld, Campus campusObjUpdated)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    _campusDao.Update(campusObjOld);
                    _campusDao.Update(campusObjUpdated);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public Campus GetCampus(long campusId)
        {
            try
            {
                return _campusDao.LoadById(campusId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Campus GetCampusByRankNextOrPrevious(int newRank, string action)
        {
            try
            {
                return _campusDao.GetCampusByRankNextOrPrevious(newRank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public IList<Campus> LoadCampus(List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, bool isProgramBranSession = true)
        {
            try
            {
                return _campusDao.LoadCampus(organizationIdList, programIdList, sessionIdList, branchIdList, campusIdList, isProgramBranSession);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Campus> LoadCampusByBranchIdsProgramSession(long[] branchId, long programId, long sessionId)
        {
            try
            {
                return _campusDao.LoadCampusByBranchIdsProgramSession(branchId, programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Campus> LoadAuthorizeCampus(List<UserMenu> userMenus, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null, bool isProgramBranSession = true)
        {
            try
            {

                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList), CommonHelper.ConvertSelectedAllIdList(programIdList));
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), authProgramIdList, CommonHelper.ConvertSelectedAllIdList(branchIdList));
                return _campusDao.LoadAuthorizeCampus(authProgramIdList, authBranchIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList), isProgramBranSession);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        public IList<Campus> LoadCampus(List<UserMenu> userMenu, int start, int length, string orderBy, string direction, string orgId, string bId, string cName, string contactNumber, string cRank, string cStatus)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                return _campusDao.LoadCampus(programIdList, branchIdList, start, length, orderBy, direction, orgId, bId, cName, contactNumber, cRank, cStatus);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Campus> LoadCampus(long[] campusId)
        {
            try
            {
                return _campusDao.LoadCampus(campusId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        public int GetCampusCount(List<UserMenu> userMenu, string orgId, string bId, string cName, string contactNumber, string cRank, string cStatus)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                return _campusDao.GetCampusCount(programIdList, branchIdList, orgId, bId, cName, contactNumber, cRank, cStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetMaximumRank(Campus campus)
        {
            try
            {
                return _campusDao.GetMaximumRank(campus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetMinimumRank(Campus campus)
        {
            try
            {
                return _campusDao.GetMinimumRank(campus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Helper Function
        private void DoBeforeAdd(Campus campusObj)
        {
            //campus assign check
            if (campusObj == null || String.IsNullOrEmpty(campusObj.Name) || campusObj.Branch == null || campusObj.Branch.Id < 1 || String.IsNullOrEmpty(campusObj.Location) || String.IsNullOrEmpty(campusObj.ContactNumber))
                throw new ValueNotAssignedException("Campus field value is not assigned");
            //valid contact no check
            const string contactNoPattern = "^(88)01[15-9]\\d{8}$";
            bool isValid = Regex.IsMatch(campusObj.ContactNumber ?? "", contactNoPattern);
            if (!isValid)
            {
                throw new InvalidDataException("Contact no is not valid.");
            }
            //campus room  assign check
            if (campusObj.CampusRooms == null || campusObj.CampusRooms.Count < 1)
                throw new ValueNotAssignedException("Campus room field value is not assigned");

            //campus room no value assing check
            int emptyRoomCount = (from x in campusObj.CampusRooms where x.RoomNo.Trim().Length < 1 select x).Count();
            if (emptyRoomCount > 0)
                throw new ValueNotAssignedException("All campus room no. field value is not assigned");

            //campus room class campacity value assing check
            int emptyClassCapacityCount = (from x in campusObj.CampusRooms where x.ClassCapacity == null || x.ClassCapacity < 1 select x).Count();
            if (emptyClassCapacityCount > 0)
                throw new ValueNotAssignedException("All campus room class seat capacity field value is not assigned");

            //campus room exam capacity value assing check
            int emptyExamCapacityCount = (from x in campusObj.CampusRooms where x.ExamCapacity == null || x.ExamCapacity < 1 select x).Count();
            if (emptyExamCapacityCount > 0)
                throw new ValueNotAssignedException("All campus room exam seat capacity field value is not assigned");

            bool duplicateFound = _campusDao.HasDupilcate(campusObj, false);
            if (duplicateFound)
                throw new DuplicateEntryException("Duplicate campus found");

            //duplicate campus room check 
            var a = campusObj.CampusRooms.GroupBy(b => b.RoomNo)
                .Select(c => new { Value = c.Key, Count = c.Count() })
                .OrderByDescending(b => b.Count);
            foreach (var x in a)
            {
                if (x.Count > 1)
                    throw new DuplicateEntryException("Duplicate campus room no. found");
                break;
            }
        }
        private void DoBeforeUdpate(Campus campusObj)
        {
            //campu assign check
            if (campusObj == null || campusObj.Branch == null || String.IsNullOrEmpty(campusObj.Name) || String.IsNullOrEmpty(campusObj.Location) || String.IsNullOrEmpty(campusObj.ContactNumber))
                throw new ValueNotAssignedException("Campus field value is not assigned");
            if (campusObj.Status == Campus.EntityStatus.Inactive)
            {

                //var campusHasActiveBatch = _batchDao.GetActiveBatchCount(campusObj);
                var dbCampus = _campusDao.LoadById(campusObj.Id);
                if (dbCampus.Batches != null)
                {
                    var campusHasActiveBatch = dbCampus.Batches.Count(x => x.Status == Batch.EntityStatus.Active);
                    if (campusHasActiveBatch > 0)
                        throw new DependencyException("This Campus Has " + campusHasActiveBatch + " Active Batch(s).");
                }
                if (dbCampus.EmploymentHistories != null && dbCampus.EmploymentHistories.Count > 0)
                {
                    var campusActiveEmploymentHistoryCount = dbCampus.EmploymentHistories.Count(x => x.Status == EmploymentHistory.EntityStatus.Active);
                    if (campusActiveEmploymentHistoryCount > 0)
                    {
                        throw new DependencyException("This Campus Has Employee(s).");
                    }

                }
            }


            bool duplicateFound = _campusDao.HasDupilcate(campusObj, true);
            if (duplicateFound)
                throw new DuplicateEntryException("Duplicate campus found");
        }
        private void DoBeforeDelete(Campus campusObj)
        {
            //campu assign check
            if (campusObj == null)
                throw new ValueNotAssignedException("Campus is not assigned");

            //bool dependencyFound = _campusDao.HasDependency(campusObj);
            //if (dependencyFound)
            //  throw new DependencyException("Dependency found");
            if (campusObj.Batches != null)
            {
                var campusHasActiveBatch = campusObj.Batches.Count(x => x.Status == Batch.EntityStatus.Active);
                if (campusHasActiveBatch > 0)
                    throw new DependencyException("This Campus Has " + campusHasActiveBatch + " Active Batch(s).");
            }
            if (campusObj.EmploymentHistories != null && campusObj.EmploymentHistories.Count > 0)
            {
                var campusActiveEmploymentHistoryCount = campusObj.EmploymentHistories.Count(x => x.Status == EmploymentHistory.EntityStatus.Active);
                if (campusActiveEmploymentHistoryCount > 0)
                {
                    throw new DependencyException("This Campus Has Employee(s).");
                }

            }

        }
        #endregion

        #region Public API
        public IList<Campus> LoadPublicCampusByProgramSessionBranch(string orgBusinessId, long programId, long sessionId, long branchId)
        {
            IList<Campus> campuses;
            try
            {
                campuses = _campusDao.LoadPublicCampusByProgramSessionBranch(orgBusinessId, programId, sessionId, branchId);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return campuses;
        }
        #endregion
    }
}