using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class BankMap : BaseClassMap<Bank, long>
    {
        public BankMap()
        {
            Table("Bank");
            LazyLoad();
            Map(x => x.Rank).Column("Rank").Not.Nullable();
            Map(x => x.Name).Column("Name").Not.Nullable();
            Map(x => x.ShortName).Column("ShortName").Not.Nullable();
            Map(x => x.Address).Column("Address");
            HasMany(x => x.BankBranch).KeyColumn("BankId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
