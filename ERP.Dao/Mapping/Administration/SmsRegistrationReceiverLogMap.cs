using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{


    public class SmsRegistrationReceiverLogMap : BaseClassMap<SmsRegistrationReceiverLog, long>
    {

        public SmsRegistrationReceiverLogMap()
        {
            Table("SmsRegistrationReceiverLog");
            LazyLoad();
            Map(x => x.SenderMobileNumber);
            Map(x => x.Message);
            Map(x => x.SmsKey);
            Map(x => x.RegistrationStatus);
            Map(x => x.SmsSenderId);
            Map(x => x.SmsSenderTime);

            References(x => x.StudentProgram).Column("StudentProgramId");
        }
    }
}
