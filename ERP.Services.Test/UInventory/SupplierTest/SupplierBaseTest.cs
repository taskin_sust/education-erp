﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.SupplierTest
{
    public class SupplierBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal SupplierService _supplierService;
        internal SupplierFactory _supplierFactory;
        internal SupplierBankDetailsFactory _supplierBankDetailsFactory;
        internal readonly string _name = "SupplierName_" + DateTime.Now.Second + "_" + DateTime.Now.Millisecond;

        #endregion
        public SupplierBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _supplierFactory = new SupplierFactory(null, _session);
            _supplierBankDetailsFactory = new SupplierBankDetailsFactory(null, _session);
            _supplierService = new SupplierService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }
        public void Dispose()
        {
            _supplierBankDetailsFactory.CleanUp();
            _supplierFactory.CleanUp();
            base.Dispose();
        
        }
    }
}
