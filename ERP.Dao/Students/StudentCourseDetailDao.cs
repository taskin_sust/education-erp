﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentCourseDetailDao : IBaseDao<StudentCourseDetail, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        StudentCourseDetail LoadStudentCourseDetailByCourseSubject(CourseSubject courseSubject);
        StudentCourseDetail LoadStudentCourseDetailByCourseSubjectId(long courseSubjectId);
        StudentCourseDetail LoadStudentCourseDetailByCourseSubjectAndStudentProgram(CourseSubject courseSubject, StudentProgram studentProgram);
        #endregion

        #region List Loading Function
        IList<StudentCourseDetail> LoadStudentCourseDetailsListByStudentProgramId(long stdProId);
        #endregion

        #region Others Function
        bool IsExistCheckStudentByProgramSessionAndSubject(long programId, long sessionId, long subjectId);
        #endregion                    
    
        StudentCourseDetail GetByCourseSubject(long courseSubId);
    }

    public class StudentCourseDetailDao : BaseDao<StudentCourseDetail, long>, IStudentCourseDetailDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public StudentCourseDetail LoadStudentCourseDetailByCourseSubject(CourseSubject courseSubject)
        {
            return Session.QueryOver<StudentCourseDetail>().Where(x => x.CourseSubject == courseSubject && x.Status == StudentCourseDetail.EntityStatus.Active).SingleOrDefault<StudentCourseDetail>();
        }
        public StudentCourseDetail LoadStudentCourseDetailByCourseSubjectId(long courseSubjectId)
        {
            return Session.QueryOver<StudentCourseDetail>().Where(x => x.CourseSubject.Id == courseSubjectId && x.Status == StudentCourseDetail.EntityStatus.Active).Take(1).SingleOrDefault<StudentCourseDetail>();
        }

        public StudentCourseDetail LoadStudentCourseDetailByCourseSubjectAndStudentProgram(CourseSubject courseSubject,
            StudentProgram studentProgram)
        {
            return
               Session.QueryOver<StudentCourseDetail>()
                   .Where(x => x.CourseSubject == courseSubject && x.StudentProgram == studentProgram && x.Status == StudentCourseDetail.EntityStatus.Active)
                   .SingleOrDefault<StudentCourseDetail>();
        }
        #endregion

        #region List Loading Function
        public IList<StudentCourseDetail> LoadStudentCourseDetailsListByStudentProgramId(long stdProId)
        {
            return
                Session.QueryOver<StudentCourseDetail>()
                    .Where(x => x.StudentProgram.Id == stdProId && x.Status == StudentCourseDetail.EntityStatus.Active)
                    .List<StudentCourseDetail>();
        }
        #endregion

        #region Others Function
        public bool IsExistCheckStudentByProgramSessionAndSubject(long programId, long sessionId, long subjectId)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(StudentCourseDetail)).Add(Restrictions.Eq("Status", StudentCourseDetail.EntityStatus.Active));
            criteria.CreateAlias("CourseSubject", "cs").Add(Restrictions.Eq("cs.Subject.Id", subjectId));
            criteria.CreateAlias("cs.Course", "c").Add(Restrictions.Eq("c.Program.Id", programId)).Add(Restrictions.Eq("c.RefSession.Id", sessionId));
            var data = (List<StudentCourseDetail>)criteria.List<StudentCourseDetail>();
            if (data.Count > 0)
                return true;
            return false;

        }

        public StudentCourseDetail GetByCourseSubject(long courseSubId)
        {
            return
                Session.QueryOver<StudentCourseDetail>()
                    .Where(x => x.CourseSubject.Id == courseSubId && x.Status==StudentCourseDetail.EntityStatus.Active)
                    .SingleOrDefault<StudentCourseDetail>();
        }

        #endregion

        #region Helper Function

        #endregion       
    }
}