﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Common;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Common
{
    public interface IAddressService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<District> LoadDistrictByQuery(string query);
        IList<Division> LoadDivision();
        IList<District> LoadDistrict(List<long> divisionIdList = null);
        IList<Thana> LoadThana(List<long> divisionIdList = null, List<long> districtIdList = null);
        //IList<Postoffice> LoadPostOffice(List<long> divisionIdList = null, List<long> districtIdList = null, List<long> thanaIdList = null);
        IList<Postoffice> LoadPostOffice(List<long> divisionIdList = null, List<long> districtIdList = null);
        #endregion

        #region Others Function

        #endregion  

    }
    public class AddressService : BaseService, IAddressService
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("CommonService");
        #endregion

        #region Propertise & Object Initialization
        private readonly DistrictDao _districtDao;
        private readonly DivisionDao _divisionDao;
        private readonly ThanaDao _thanaDao;
        private readonly PostOfficeDao _postOfficeDao;
        public AddressService(ISession session)
        {
            Session = session;
            _districtDao = new DistrictDao { Session = session };
            _divisionDao = new DivisionDao { Session = session };
            _thanaDao = new ThanaDao { Session = session };
            _postOfficeDao = new PostOfficeDao { Session = session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<District> LoadDistrictByQuery(string query)
        {
            try
            {
                return _districtDao.LoadDistrictByQuery(query);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public IList<Division> LoadDivision()
        {
            try
            {
                return _divisionDao.LoadDivision();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<District> LoadDistrict(List<long> divisionIdList = null)
        {
            try
            {
                return _districtDao.LoadDistrict(divisionIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<Thana> LoadThana(List<long> divisionIdList = null, List<long> districtIdList = null)
        {
            try
            {
                return _thanaDao.LoadThana(divisionIdList, districtIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        //public IList<Postoffice> LoadPostOffice(List<long> divisionIdList = null, List<long> districtIdList = null, List<long> thanaIdList = null)
        public IList<Postoffice> LoadPostOffice(List<long> divisionIdList = null, List<long> districtIdList = null)
        {
            try
            {
                //return _postOfficeDao.LoadPostOffice(divisionIdList, districtIdList, thanaIdList);
                return _postOfficeDao.LoadPostOffice(divisionIdList, districtIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function

        #endregion

    }
}
