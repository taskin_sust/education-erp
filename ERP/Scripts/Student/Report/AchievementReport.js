﻿$(document).ready(function () {
    $("#BranchId").hide();
    $(".branchLayer").hide();
    $("#ReportType").change(function () {
        var reportType = $(this).val();

        $("#OrganizationId").find('option:eq(0)').prop('selected', true);
        $('#ProgramId').empty();
        $('#ProgramId').append("<option value=''>Select a Program</option>");
        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select a Session</option>");
        $('#StudentAdmissionTarget').empty();
        $('#StudentAdmissionTarget').append("<option value=''>Select Deadline</option>");

        if (reportType != "") {
            if (reportType == 1) {
                $("#ProgramId").show();
                $(".programlayer").show();
                $("#SessionId").show();
                $(".sessionLayer").show();
                $("#BranchId").hide();
                $(".branchLayer").hide();
                $("#BranchId_err_div").remove();
            }
            else if (reportType == 2) {
                $("#ProgramId").hide();
                $(".programlayer").hide();
                $("#SessionId").hide();
                $(".sessionLayer").hide();
                $("#BranchId").show();
                $(".branchLayer").show();
                $("#ProgramId_err_div").remove();
                $("#SessionId_err_div").remove();

            }
        } else {

          //  $('#ReportType').append('<option value="">Select Report Type</option>');
        }
    });

    $("#OrganizationId").change(function () {
        var reportType = $("#ReportType").val();
        var organizatonId = $(this).val();
        $("#ProgramId option").remove();
        if (organizatonId != "" && reportType == 2) {
            $('#BranchId').empty();
            $('#StudentAdmissionTarget').empty();
            $('#StudentAdmissionTarget').append("<option value=''>Select Deadline</option>");
            $.when(
                $.ajax({
                    //url: 'GetProgramByOrganization',
                    url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                    type: "POST",
                    data: { organizationIds: organizatonId, programIds: null, sessionIds:null, isAuthorized: true },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                }), $.ajax({
                    url: 'LoadDeadline',
                    type: "POST",
                    data: { "organizationId": organizatonId },
                })).done(function (response, response2) {
                    console.log(response[0].returnBranchList);
                    $('#BranchId').append('<option value="">Select Branch</option>');
                    if (response[0].IsSuccess) {
                        $.each(response[0].returnBranchList, function (i, v) {
                            $('#BranchId').append($('<option>').text(v.Text).attr('value', v.Value));
                            });
                    }
                    if (response2[0].IsSuccess) {
                        $('#StudentAdmissionTarget').append('<option value="0">Last Deadline</option>');
                        //if (response2[0].deadline != null && response2[0].deadline.length > 0) {
                        //    $.each(response2[0].deadline, function (i, v) {
                        //        $('#StudentAdmissionTarget').append($('<option>').text(v.Text).attr('value', v.Value));
                        //    });
                        //    $("#StudentAdmissionTarget").find('option:eq(0)').prop('selected', true);
                        //}
                    }
                    $.unblockUI();
                });
            
        } else if (organizatonId != "" && reportType == 1) {
            $.ajax({
                type: "post",
                //url: "AjaxRequestForSession",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                cache: false,
                async: true,
                data: {  organizationIds: organizatonId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    $('#ProgramId').append('<option value="">Select Program</option>');
                    if (response.IsSuccess) {
                        $.each(response.returnProgramList, function (i, v) {
                            $('#ProgramId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        
                    }
                    console.log("Success -----");
                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    //console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    //console.log(result);
                }
            });
        }
        else {

            $('#ProgramId').append('<option value="">Select Program</option>');
        }
    });
    $(document).on('change', '#ProgramId', function () {
        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select a session</option>");
        $('#StudentAdmissionTarget').empty();
        $('#StudentAdmissionTarget').append("<option value=''>Select Deadline</option>");
        var programId = $("#ProgramId").val();

        if (programId != null && programId != "") {
            $.ajax({
                type: "post",
                //url: "AjaxRequestForSession",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                cache: false,
                async: true,
                data: { programIds: programId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnSessionList, function (i, v) {
                            $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        
                    }
                    console.log("Success -----");
                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    //console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    //console.log(result);
                }
            });
        }
    });
    //Session Change Functionalities
    $(document).on('change', '#SessionId', function () {

        $('#StudentAdmissionTarget').empty();
        $('#StudentAdmissionTarget').append("<option value=''>Select Deadline</option>");
        $('#StudentAdmissionTarget').empty();
        $('#StudentAdmissionTarget').append("<option value=''>Select Deadline</option>");
        $("#missingImageCount").html("...");
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var organizationId = $("#OrganizationId").val();
        $.ajax({
            type: "post",
            //url: "AjaxRequestForSession",
            url: $("body").attr("data-project-root") + "Student/StudentAdmissionTarget/LoadDeadline",
            cache: false,
            async: true,
            data: { organizationId: organizationId, programId: programId, sessionId: sessionId, },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                $.unblockUI();
                if (response.IsSuccess) {
                    if (response.deadline != null && response.deadline.length > 0) {
                        $.each(response.deadline, function (i, v) {
                            $('#StudentAdmissionTarget').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        $("#StudentAdmissionTarget").find('option:eq(0)').prop('selected', true);
                    }
                } else {

                }
                console.log("Success -----");
            }, complete: function () {
                $.unblockUI();
                console.log("Complete -----");
                //console.log(result);
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
                //console.log(result);
            }
        });
    });

    $(document).on('click', '#GenerateReport', function (e) {

        var isSuccess = true;
        var reportType = $("#ReportType").val();
        var organizationId = $("#OrganizationId").val();
        var deadLine = $("#StudentAdmissionTarget").val();
        console.log(deadLine);
        if (reportType != "") {
            if (reportType == 1) {
                console.log("Type 1");
                var programId = $("#ProgramId").val();
                var sessionId = $("#SessionId").val();
                if (programId == null || programId < 0 || programId == '') {
                    window.showErrorMessageBelowCtrl("ProgramId", "", false);
                    window.showErrorMessageBelowCtrl("ProgramId", "Please Select Program.", true);
                    isSuccess = false;
                }
                if (sessionId == null || sessionId < 0 || sessionId == '') {
                    window.showErrorMessageBelowCtrl("SessionId", "", false);
                    window.showErrorMessageBelowCtrl("SessionId", "Please Select Organization.", true);
                    isSuccess = false;
                }

            } else if (reportType == 2) {
                console.log("Type 2");
                var branchId = $("#BranchId").val();
                if (branchId == null || branchId < 0 || branchId == '') {
                    window.showErrorMessageBelowCtrl("BranchId", "", false);
                    window.showErrorMessageBelowCtrl("BranchId", "Please Select Branch.", true);
                    isSuccess = false;
                }
            }

            if (organizationId == null || organizationId <= 0 || organizationId == '') {
                window.showErrorMessageBelowCtrl("OrganizationId", "", false);
                window.showErrorMessageBelowCtrl("OrganizationId", "Please Select Organization.", true);
                isSuccess = false;
            }
            if (deadLine == null || deadLine < 0 || deadLine == '') {
                window.showErrorMessageBelowCtrl("StudentAdmissionTarget", "", false);
                window.showErrorMessageBelowCtrl("StudentAdmissionTarget", "Please Select DealLine.", true);
                isSuccess = false;
            }

        } else {
            window.showErrorMessageBelowCtrl("ReportType", "", false);
            window.showErrorMessageBelowCtrl("ReportType", "Please Select Report Type.", true);
            isSuccess = false;
        }

        if (isSuccess == true) {
            return true;
        } else {
            e.preventDefault();
            return false;
        }

    });
});