﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;

namespace UdvashERP.Api.Controllers
{
    public class SyncAttendanceReceiveApiController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AttendanceAPI");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly IAttendanceDeviceService _hrAttendanceDeviceService;
        private readonly IAttendanceDeviceLogDataService _attendanceDeviceLogDataService;
        private readonly IAttendanceDeviceMemberService _attendanceDeviceMemberService;
        private readonly IAttendanceSynchronizerService _attendanceSynchronizerService;
        public SyncAttendanceReceiveApiController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _hrAttendanceDeviceService = new AttendanceDeviceService(session);
                _attendanceDeviceLogDataService = new AttendanceDeviceLogDataService(session);
                _attendanceDeviceMemberService = new AttendanceDeviceMemberService(session);
                _attendanceSynchronizerService = new AttendanceSynchronizerService(session);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                //Console.WriteLine("{0}", e.Message);
            }
        }

        #endregion

        #region Private Method

        private int Authenticate(string key)
        {
            int error = 0;
            var apiKey = ConfigurationManager.AppSettings["ApiKey"];

            if (key != apiKey)
            {
                error = 1;
            }
            return error;
        }

        private IList<AttendanceDeviceLogData> BuildDeviceLogModelFromVm(IList<AttendanceLog> attendanceLogList)
        {
            IList<AttendanceDeviceLogData> resultList = new List<AttendanceDeviceLogData>();
            foreach (var attendanceDeviceLogDataViewModel in attendanceLogList)
            {
                var result = new AttendanceDeviceLogData();
                result.EnrollNo = attendanceDeviceLogDataViewModel.EnrollNumber;
                result.DeviceId = attendanceDeviceLogDataViewModel.DeviceId;
                result.PunchType = attendanceDeviceLogDataViewModel.PunchType;
                result.PunchTime = Convert.ToDateTime(attendanceDeviceLogDataViewModel.PunchTime);
                resultList.Add(result);
            }
            return resultList;
        }

        private IList<AttendanceDeviceRawData> BuildRowAttendanceFromLog(IList<AttendanceDeviceLogData> logList)
        {
            long aspNetUserIdL = Convert.ToInt64(ConfigurationManager.AppSettings["UserId"]);
            IList<AttendanceDeviceRawData> resultList = new List<AttendanceDeviceRawData>();
            foreach (var logData in logList)
            {
                var result = new AttendanceDeviceRawData();
                result.AttendanceDevice = _hrAttendanceDeviceService.GetAttendanceDeviceByMachineNo(Convert.ToInt64(logData.DeviceId));
                result.Pin = _attendanceDeviceMemberService.GetAttendanceDeviceMember(logData.EnrollNo, result.AttendanceDevice.Id).TeamMember.Pin;
                result.PunchTime = logData.PunchTime;
                result.PunchType = logData.PunchType;
                result.RequestIp = GetIpAddress();
                result.CreateBy = aspNetUserIdL;
                result.ModifyBy = aspNetUserIdL;
                resultList.Add(result);
            }
            return resultList;
        }

        private string GetIpAddress()
        {
            string VisitorsIPAddr = string.Empty;
            if (HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = HttpContext.Request.UserHostAddress;
            }
            return VisitorsIPAddr;
        }

        #endregion

        #region Api Methods

        [HttpPost]
        public JsonResult UploadAttendanceData(string apiKey, string syncKey, string attendanceString)
        {
            bool success = false;
            string message = "";
            try
            {
                var attendanceSynchronizer = _attendanceSynchronizerService.GetAttendanceSynchronizer(syncKey);
                if (attendanceSynchronizer == null || attendanceSynchronizer.AttendanceDevice.Any() == false)
                    message += "No Device Found";
                if (Authenticate(apiKey) > 0)
                    message += " Request not authenticated";
                if (attendanceString.Equals("[]") || String.IsNullOrEmpty(attendanceString))
                    message += " No Data Found";
                if (!String.IsNullOrEmpty(message))
                    return Json(new JsonResponse(success, message));

                var attendanceLogList = new JavaScriptSerializer().Deserialize<List<AttendanceLog>>(attendanceString);
                IList<AttendanceDeviceLogData> logList = BuildDeviceLogModelFromVm(attendanceLogList);
                IList<AttendanceDeviceRawData> rowList = BuildRowAttendanceFromLog(logList);
                _attendanceDeviceLogDataService.Save(logList, rowList);
                success = true;
                return Json(new JsonResponse(success, message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = "can't process at this time.please try again later";
                return Json(new JsonResponse(success, message));
            }
        }

        [HttpPost]
        public JsonResult UpdateTimeOfAttendanceDevice(string apiKey, string syncKey, string deviceId, DateTime updateTime, string enroList)
        {
            string message = "";
            try
            {
                if (Authenticate(apiKey) > 0)
                    message += " Request not authenticated";
                var attendanceSynchronizer = _attendanceSynchronizerService.GetAttendanceSynchronizer(syncKey);
                if (attendanceSynchronizer == null || attendanceSynchronizer.AttendanceDevice.Any() == false)
                    message += "No Device Found";
                if (!String.IsNullOrEmpty(message))
                    return Json(new JsonResponse(false, message));
                if (!String.IsNullOrEmpty(enroList))
                {
                    List<int> enrollList = new JavaScriptSerializer().Deserialize<List<int>>(enroList);
                    _attendanceDeviceMemberService.UpdateAttendanceMemberData(Convert.ToInt64(deviceId), updateTime, enrollList);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new JsonResponse(false, e.Message));
            }
            return Json(new JsonResponse(true, message));
        }

        #endregion
    }
}


