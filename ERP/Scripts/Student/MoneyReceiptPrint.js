﻿$(document).ready(function () {
    function PrintElem(elem) {
        Popup($(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'new div', 'height=400,width=600');
        mywindow.document.write('<html><head><title>my div</title>');
        /*optional stylesheet*/
        //mywindow.document.write('<link rel="stylesheet" href="/Content/bootstrap.css" type="text/css" media="print" />');
        //mywindow.document.write('<link rel="stylesheet" href="/Content/site.css" type="text/css" media="print" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();

        return true;
    }


    $(document).on("click", '#print', function() {
        PrintElem('#moneyReceipt');
    });
});