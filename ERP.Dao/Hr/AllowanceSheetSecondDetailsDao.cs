﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.SqlCommand;
using UdvashERP.BusinessModel.Entity.Hr;
using NHibernate;
using NHibernate.Transform;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface IAllowanceSheetSecondDetailsDao : IBaseDao<BusinessModel.Entity.Hr.AllowanceSheetSecondDetails, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        AllowanceSheetSecondDetails GetTeamMemberIsSubmittedChildrenAllowanceSheetDetails(long? childrenInfoId = null);  

        #endregion

        #region List Loading Function
        IList<AllowanceSheetSecondDetails> LoadByChildrenAllowanceId(long chldAllowanceId);
        IList<TeamMemberAllowanceSheetSecondDetailsDto> LoadAllowanceSheetSecondDetailsDto(long teamMemberId, long organizationId, int employmentStatus, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }

    public class AllowanceSheetSecondDetailsDao : BaseDao<BusinessModel.Entity.Hr.AllowanceSheetSecondDetails, long>, IAllowanceSheetSecondDetailsDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public AllowanceSheetSecondDetails GetTeamMemberIsSubmittedChildrenAllowanceSheetDetails(long? childrenInfoId = null) 
        {
            if (childrenInfoId == null || childrenInfoId == 0)
                return null;

            ICriteria criteria = Session.CreateCriteria<AllowanceSheetSecondDetails>();
            criteria.CreateAlias("AllowanceSheet", "a");
            criteria.Add(Restrictions.Not(Restrictions.Eq("a.Status", AllowanceSheet.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("a.IsFinalSubmit", true));
            criteria.Add(Restrictions.Eq("Id", childrenInfoId));
            criteria.AddOrder(Order.Desc("AllowanceDate"));

            return criteria.List<AllowanceSheetSecondDetails>().Take(1).FirstOrDefault();
        }
        #endregion

        #region List Loading Function

        public IList<AllowanceSheetSecondDetails> LoadByChildrenAllowanceId(long chldAllowanceId)
        {
            return
                Session.QueryOver<AllowanceSheetSecondDetails>()
                    .Where(x => x.ChildrenAllowanceSetting.Id == chldAllowanceId)
                    .List<AllowanceSheetSecondDetails>();
            //var criteria = Session.CreateCriteria<AllowanceSheetSecondDetails>();
            //criteria.Add(Restrictions.Eq("Id", chldAllowanceId));
            //criteria.Add(Restrictions.Eq("Status", AllowanceSheetSecondDetails.EntityStatus.Active));
            ////criteria.CreateCriteria("AllowanceSheet", "as", JoinType.InnerJoin);
            //return criteria.List<AllowanceSheetSecondDetails>();
        }

        public IList<TeamMemberAllowanceSheetSecondDetailsDto> LoadAllowanceSheetSecondDetailsDto(long teamMemberId, long organizationId, int employmentStatus, DateTime dateFrom, DateTime dateTo)
        {
            string query = GetAllowanceSheetSecondDetailsDtoQueryString(teamMemberId, organizationId, employmentStatus, dateFrom, dateTo);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberAllowanceSheetSecondDetailsDto>());
            iQuery.SetTimeout(2700);
            List<TeamMemberAllowanceSheetSecondDetailsDto> list = iQuery.List<TeamMemberAllowanceSheetSecondDetailsDto>().ToList();
            return list;
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        private string GetAllowanceSheetSecondDetailsDtoQueryString(long teamMemberId, long organizationId, int employmentStatus, DateTime dateFrom, DateTime dateTo)
        {
            string query = "";
            string startDate = dateFrom.ToString("yyyy-MM-dd");
            string endDate = dateTo.ToString("yyyy-MM-dd");


            query = @"
DECLARE @StartDate DATE = '" + startDate + @"';
DECLARE @EndDate DATE = '" + endDate + @"';
DECLARE @teamMemberId int = " + teamMemberId + @";
DECLARE @organizationId int = " + organizationId + @";
DECLARE @employmentStatus int = " + employmentStatus + @";

Select 
 case when (mlz.PaymentType=" + (int)BusinessRules.Hr.PaymentType.Once + @") then  mlz.EffectiveDate  
	  when  (mlz.PaymentType=" + (int)BusinessRules.Hr.PaymentType.Monthly + @") then  CAST(CAST(year( @StartDate ) AS varchar) + '-' + CAST(month( @StartDate ) AS varchar) + '-' + CAST(day( mlz.EffectiveDate ) AS varchar) AS DATETIME) 
	  else CAST(CAST(year( @StartDate ) AS varchar) + '-' + CAST(month( mlz.EffectiveDate ) AS varchar) + '-' + CAST(day( mlz.EffectiveDate ) AS varchar) AS DATETIME) 
	  end as Date
 , mla.TeamMemberId as TeamMemberId
 , mlz.Id as MemberLoanZakatId
 , mlz.PaymentType as ZakatPaymentType
 , mlz.Amount as ZakatAmount
 , mlz.EffectiveDate as ZakatEffectiveDate
 , mlz.ClosingDate as ZakatClosingDate
 , null as MemberLoanCsrId
 , null as CsrPaymentType
 , null as CsrAmount
 , null as CsrEffectiveDate
 , null as CsrClosingDate
 , null as SpecialAllowanceSettingId
 , null as SpecialAllowancePaymentType
 , null as SpecialAllowanceAmount
 , null as SpecialAllowanceEffectiveDate
 , null as SpecialAllowanceClosingDate
 , null as ChildrenAllowanceSettingId
 , null as ChildrenAllowancePaymentType
 , null as ChildrenAllowanceAmount
 , null as ChildrenAllowanceEffectiveDate
 , null as ChildrenAllowanceClosingDate
 , null as ChildrenAllowanceStartingAge
 , null as ChildrenAllowanceClosingAge
 , null as ChildrenAllowanceMaxNoOfChild
from HR_MemberLoanZakat as mlz
inner join HR_MemberLoanApplication as mla on mla.Id = mlz.MemberLoanApplicationId and mla.Status = " + MemberLoanApplication.EntityStatus.Active + @"
where mla.TeamMemberId = @teamMemberId
and mlz.Status = " + MemberLoanZakat.EntityStatus.Active + @"
and mlz.EffectiveDate <=  @StartDate
and mlz.ClosingDate >= @EndDate
and ( (mlz.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Once + @" and year(@StartDate) = year(mlz.EffectiveDate) and month(@StartDate) = month(mlz.EffectiveDate)) 
	or mlz.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Monthly + @" 
	or (mlz.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Yearly + @" and month(@StartDate) = month(mlz.EffectiveDate))) --- 1=Onece, 3=monthly, 4=Yearly

union all
Select 
 case when (mlc.PaymentType=" + (int)BusinessRules.Hr.PaymentType.Once + @") then  mlc.EffectiveDate  
	  when  (mlc.PaymentType=" + (int)BusinessRules.Hr.PaymentType.Monthly + @") then  CAST(CAST(year( @StartDate ) AS varchar) + '-' + CAST(month( @StartDate ) AS varchar) + '-' + CAST(day( mlc.EffectiveDate ) AS varchar) AS DATETIME) 
	  else CAST(CAST(year( @StartDate ) AS varchar) + '-' + CAST(month( mlc.EffectiveDate ) AS varchar) + '-' + CAST(day( mlc.EffectiveDate ) AS varchar) AS DATETIME) 
	  end as Date
 , mla.TeamMemberId as TeamMemberId
 , null as MemberLoanZakatId
 , null as ZakatPaymentType
 , null as ZakatAmount
 , null as ZakatEffectiveDate
 , null as ZakatClosingDate
 , mlc.Id as MemberLoanCsrId
 , mlc.PaymentType as CsrPaymentType
 , mlc.Amount as CsrAmount
 , mlc.EffectiveDate as CsrEffectiveDate
 , mlc.ClosingDate as CsrClosingDate
 , null as SpecialAllowanceSettingId
 , null as SpecialAllowancePaymentType
 , null as SpecialAllowanceAmount
 , null as SpecialAllowanceEffectiveDate
 , null as SpecialAllowanceClosingDate
 , null as ChildrenAllowanceSettingId
 , null as ChildrenAllowancePaymentType
 , null as ChildrenAllowanceAmount
 , null as ChildrenAllowanceEffectiveDate
 , null as ChildrenAllowanceClosingDate
 , null as ChildrenAllowanceStartingAge
 , null as ChildrenAllowanceClosingAge
 , null as ChildrenAllowanceMaxNoOfChild
from HR_MemberLoanCsr as mlc
inner join HR_MemberLoanApplication as mla on mla.Id = mlc.MemberLoanApplicationId and mla.Status = " + MemberLoanApplication.EntityStatus.Active + @" 
where mla.TeamMemberId = @teamMemberId
and mlc.Status = " + MemberLoanCsr.EntityStatus.Active + @"
and mlc.EffectiveDate <=  @StartDate
and mlc.ClosingDate >= @EndDate
and ( (mlc.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Once + @" and year(@StartDate) = year(mlc.EffectiveDate) and month(@StartDate) = month(mlc.EffectiveDate)) 
	or mlc.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Monthly + @"
	or (mlc.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Yearly + @" and month(@StartDate) = month(mlc.EffectiveDate))) --- 1=Onece, 3=monthly, 4=Yearly

union all
Select 
 case when (sas.PaymentType=" + (int)BusinessRules.Hr.PaymentType.Once + @") then  sas.EffectiveDate  
	  when  (sas.PaymentType=" + (int)BusinessRules.Hr.PaymentType.Monthly + @") then  CAST(CAST(year( @StartDate ) AS varchar) + '-' + CAST(month( @StartDate ) AS varchar) + '-' + CAST(day( sas.EffectiveDate ) AS varchar) AS DATETIME) 
	  else CAST(CAST(year( @StartDate ) AS varchar) + '-' + CAST(month( sas.EffectiveDate ) AS varchar) + '-' + CAST(day( sas.EffectiveDate ) AS varchar) AS DATETIME) 
	  end as Date
 , sas.TeamMemberId as TeamMemberId
 , null as MemberLoanZakatId
 , null as ZakatPaymentType
 , null as ZakatAmount
 , null as ZakatEffectiveDate
 , null as ZakatClosingDate
 , null as MemberLoanCsrId
 , null as CsrPaymentType
 , null as CsrAmount
 , null as CsrEffectiveDate
 , null as CsrClosingDate
 , sas.Id as SpecialAllowanceSettingId
 , sas.PaymentType as SpecialAllowancePaymentType
 , sas.Amount as SpecialAllowanceAmount
 , sas.EffectiveDate as SpecialAllowanceEffectiveDate
 , sas.ClosingDate as SpecialAllowanceClosingDate
 , null as ChildrenAllowanceSettingId
 , null as ChildrenAllowancePaymentType
 , null as ChildrenAllowanceAmount
 , null as ChildrenAllowanceEffectiveDate
 , null as ChildrenAllowanceClosingDate
 , null as ChildrenAllowanceStartingAge
 , null as ChildrenAllowanceClosingAge
 , null as ChildrenAllowanceMaxNoOfChild
from HR_SpecialAllowanceSetting as sas
where sas.TeamMemberId = @teamMemberId
and sas.Status = " + SpecialAllowanceSetting.EntityStatus.Active + @"
and sas.EffectiveDate <=  @StartDate
and (sas.ClosingDate is null or sas.ClosingDate >= @EndDate)
and ( (sas.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Once + @" and year(@StartDate) = year(sas.EffectiveDate) and month(@StartDate) = month(sas.EffectiveDate)) 
	or sas.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Daily + @" 
	or sas.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Monthly + @" 
	or (sas.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Yearly + @" and month(@StartDate) = month(sas.EffectiveDate))) --- 1= Onece, 2= Daily, 3= monthly, 4= Yearly

union all
Select 
 case when  (cas.PaymentType=" + (int)BusinessRules.Hr.PaymentType.Monthly + @") then  CAST(CAST(year( @StartDate ) AS varchar) + '-' + CAST(month( @StartDate ) AS varchar) + '-' + CAST(day( cas.EffectiveDate ) AS varchar) AS DATETIME) 
	  else CAST(CAST(year( @StartDate ) AS varchar) + '-' + CAST(month( cas.EffectiveDate ) AS varchar) + '-' + CAST(day( cas.EffectiveDate ) AS varchar) AS DATETIME) 
	  end as Date
 , @teamMemberId as TeamMemberId
 , null as MemberLoanZakatId
 , null as ZakatPaymentType
 , null as ZakatAmount
 , null as ZakatEffectiveDate
 , null as ZakatClosingDate
 , null as MemberLoanCsrId
 , null as CsrPaymentType
 , null as CsrAmount
 , null as CsrEffectiveDate
 , null as CsrClosingDate
 , null as SpecialAllowanceSettingId
 , null as SpecialAllowancePaymentType
 , null as SpecialAllowanceAmount
 , null as SpecialAllowanceEffectiveDate
 , null as SpecialAllowanceClosingDate
 , cas.Id as ChildrenAllowanceSettingId
 , cas.PaymentType as ChildrenAllowancePaymentType
 , cas.Amount as ChildrenAllowanceAmount
 , cas.EffectiveDate as ChildrenAllowanceEffectiveDate
 , cas.ClosingDate as ChildrenAllowanceClosingDate
 , cas.StartingAge as ChildrenAllowanceStartingAge
 , cas.ClosingAge as ChildrenAllowanceClosingAge
 , cas.MaxNoOfChild as ChildrenAllowanceMaxNoOfChild
from HR_ChildrenAllowanceSetting as cas
left join (
	select ChildrenAllowanceSettingId
	, count(ChildrenAllowanceSettingId) as c
	from HR_ChildrenAllowanceBlock 
		where Status = " + ChildrenAllowanceBlock.EntityStatus.Active + @"
		and EffectiveDate <=  @StartDate
		and (ClosingDate is null or ClosingDate >= @EndDate)
		and TeamMemberId = @teamMemberId
		group by ChildrenAllowanceSettingId
) as a on a.ChildrenAllowanceSettingId = cas.Id 
where cas.OrganizationId = @organizationId
and cas.Status = " + ChildrenAllowanceSetting.EntityStatus.Active + @"
and ( (cas.IsProbation = 1 and @employmentStatus = " + (int)MemberEmploymentStatus.Probation + @")
		or (cas.IsPermanent = 1 and @employmentStatus = " + (int)MemberEmploymentStatus.Permanent + @")
		or (cas.IsPartTime = 1 and @employmentStatus =" + (int)MemberEmploymentStatus.PartTime + @")
		or (cas.IsContractual = 1 and @employmentStatus =" + (int)MemberEmploymentStatus.Contractual + @")
		or (cas.IsIntern = 1 and @employmentStatus =" + (int)MemberEmploymentStatus.Intern + @")
	) 
and cas.EffectiveDate <=  @StartDate
and (cas.ClosingDate is null or cas.ClosingDate >= @EndDate)
and (cas.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Monthly + @" 
	or (cas.PaymentType = " + (int)BusinessRules.Hr.PaymentType.Yearly + @" and month(@StartDate) = month(cas.EffectiveDate))
	) --- 3= monthly, 4= Yearly
and isnull(a.c,0) = 0";

            return query;
        }

        #endregion

        
    }
}
