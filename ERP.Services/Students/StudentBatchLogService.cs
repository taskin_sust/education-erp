﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IStudentBatchLogService : IBaseService
    {
        #region Operational Function
        bool Save(StudentBatchLog studentBatchLog);

        #endregion

        #region Single Instances Loading Function
        StudentBatchLog LoadById(long id);
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function

        #endregion
    }

    public class StudentBatchLogService : BaseService, IStudentBatchLogService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentBatchLogDao _studentBatchLogDao;
        private readonly IStudentProgramDao _studentProgramDao;
        public StudentBatchLogService(ISession session)
        {
            Session = session;
            _studentBatchLogDao = new StudentBatchLogDao{ Session = session };
            _studentProgramDao = new StudentProgramDao { Session = session };
        }
        #endregion

        #region Operational Function
        public bool Save(StudentBatchLog studentBatchLog)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    _studentBatchLogDao.Save(studentBatchLog);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                {
                    transaction.Rollback();
                    _logger.Error(ex);
                    return false;
                }
            }
            return false;
        }

        #endregion

        #region Single Instances Loading Function
        public StudentBatchLog LoadById(long id)
        {
            try
            {
                return _studentBatchLogDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        #endregion

        #region List Loading Function
        
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
