﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IHolidayWorkDao : IBaseDao<HolidayWork, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        HolidayWork GetTeamMemberHolidayWork(long teamMemberId, DateTime? dayOffDate);
        HolidayWork GetTeamMemberHolidayWorkOnDate(long teamMemberId, DateTime date, bool checkApproveType = true);
        HolidayWork GetTeamMemberLastApprovedHolidayWork(long? teamMemberId = null, int? pin = null);
        HolidayWork GetTeamMemberLastIsPostHoliDayWork(long? teamMemberId, int? pin);
        HolidayWorkDto GetOrganizationsLastApprovedHolidayWork(long organizationId);

        #endregion

        #region List Loading Function

        IList<HolidayApprovalDto> LoadHolidayWorkApproval(DateTime holidayWorkDateFrom, DateTime holidayWorkDateTo, List<long> teamMemberIdList = null, List<int> teamMemberPinList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, bool isMentor = false);
        IList<HolidayApprovalDto> LoadHrHolidayWorkApprovalHistory(int start, int length, string orderBy, string orderDir, DateTime holidayWorkDateFrom, DateTime holidayWorkDateTo, List<long> teamMemberIdList = null, List<int> teamMemberPinList = null, List<long> authBranchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null);
        IList<HolidayWork> LoadHolidayWork(DateTime dateFrom, DateTime dateTo, long teamMemberId);

        #endregion

        #region Others Function

        int GetHrHolidayWorkApprovalHistoryRowCount(DateTime holidayWorkDateFrom, DateTime holidayWorkDateTo, List<long> teamMemberIdList = null, List<int> teamMemberPinList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null);
        bool CheckhasHolidayWorkOnADate(long teamMemberId, DateTime date);

        #endregion

        #region Helper Function

        #endregion

        #region no Need
        #endregion



    }
    public class HolidayWorkDao : BaseDao<HolidayWork, long>, IHolidayWorkDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public HolidayWork GetTeamMemberHolidayWork(long teamMemberId, DateTime? dayOffDate)
        {
            ICriteria criteria = Session.CreateCriteria<HolidayWork>();
            criteria.Add(Restrictions.Eq("Status", HolidayWork.EntityStatus.Active));
            criteria.Add(Restrictions.Gt("ApprovalType", 0));
            criteria.CreateAlias("TeamMember", "member")
                .Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
                .Add(Restrictions.Eq("member.Id", teamMemberId));
            if (dayOffDate != null)
                criteria.Add(Restrictions.Eq("HolidayWorkDate", dayOffDate.Value.Date));
            return criteria.List<HolidayWork>().FirstOrDefault();
        }

        public HolidayWork GetTeamMemberHolidayWorkOnDate(long teamMemberId, DateTime date, bool checkApproveType = true)
        {
            //DateTime dateTime = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            ICriteria criteria = Session.CreateCriteria<HolidayWork>().Add(Restrictions.Eq("Status", HolidayWork.EntityStatus.Active));
            if(checkApproveType)
                criteria.Add(Restrictions.Gt("ApprovalType", 0));
            criteria.CreateAlias("TeamMember", "member").Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active)).Add(Restrictions.Eq("member.Id", teamMemberId));
            criteria.Add(Restrictions.Eq("HolidayWorkDate", date.Date));
            return criteria.UniqueResult<HolidayWork>();
        }

        public HolidayWork GetTeamMemberLastApprovedHolidayWork(long? teamMemberId = null, int? pin = null)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<HolidayWork>();
            criteria.Add(Restrictions.Eq("Status", HolidayWork.EntityStatus.Active));
            criteria.Add(Restrictions.Not(Restrictions.IsNull("ApprovalType")));
            criteria.CreateAlias("TeamMember", "member");
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("HolidayWorkDate"));
            return criteria.List<HolidayWork>().Take(1).FirstOrDefault();
        }

        public HolidayWork GetTeamMemberLastIsPostHoliDayWork(long? teamMemberId, int? pin)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<HolidayWork>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", HolidayWork.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("IsPost", true));
            criteria.CreateAlias("TeamMember", "member");
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("HolidayWorkDate"));
            return criteria.List<HolidayWork>().Take(1).FirstOrDefault();
        }

        public HolidayWorkDto GetOrganizationsLastApprovedHolidayWork(long organizationId)
        {
            if (organizationId <= 0)
                return null;
            string query = @"
                            SELECT hw.*
                            FROM HR_HolidayWork AS hw
                            INNER JOIN 
                            (
	                            SELECT 
		                            ff.*
	                            FROM
	                            (
		                            SELECT * 
		                            FROM 
		                            ( 
			                            SELECT
				                            tm.Id,
				                            meh.DesignationId,
				                            RANK() OVER (PARTITION BY meh.TeamMemberId ORDER BY meh.EffectiveDate DESC, meh.Id DESC) AS rankNo
			                            FROM HR_TeamMember tm
			                            INNER JOIN HR_EmploymentHistory meh ON tm.Id = meh.TeamMemberId 
		                            ) AS ss WHERE ss.rankNo = 1
	                            ) AS ff 
	                            LEFT JOIN [dbo].[HR_Designation] desig ON desig.Id = ff.DesignationId
	                            LEFT JOIN [dbo].[Organization] org ON org.Id = desig.OrganizationId AND org.Id = " + organizationId + @"
                            ) AS rest ON rest.Id = hw.TeamMemberId
                            WHERE hw.IsPost = 1 
                            ORDER BY  hw.Id DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<HolidayWorkDto>());
            iQuery.SetTimeout(2700);
            var rows = iQuery.List<HolidayWorkDto>();
            return rows.Count > 0 ? rows[0] : null;
        }

        #endregion

        #region List Loading Function

        public IList<HolidayApprovalDto> LoadHolidayWorkApproval(DateTime holidayWorkDateFrom, DateTime holidayWorkDateTo, List<long> teamMemberIdList = null, List<int> teamMemberPinList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, bool isMentor = false)
        {
            string sqlQuery = GetHolidayWorkQuery(holidayWorkDateFrom, holidayWorkDateTo, teamMemberIdList, teamMemberPinList, branchIdList, campusIdList, departmentIdList, designationIdList, false, isMentor);
            IQuery iQuery = Session.CreateSQLQuery(sqlQuery);
            iQuery.SetResultTransformer(Transformers.AliasToBean<HolidayApprovalDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<HolidayApprovalDto>().ToList();
        }

        public IList<HolidayApprovalDto> LoadHrHolidayWorkApprovalHistory(int start, int length, string orderBy, string orderDir, DateTime holidayWorkDateFrom, DateTime holidayWorkDateTo, List<long> teamMemberIdList = null, List<int> teamMemberPinList = null, List<long> authBranchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null)
        {
            var filterPagination = "";
            if (length > 0)
            {
                filterPagination = " where pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            string sqlQuery = GetHolidayWorkQuery(holidayWorkDateFrom, holidayWorkDateTo, teamMemberIdList, teamMemberPinList, authBranchIdList, campusIdList, departmentIdList, null, true);
            string query = @"DECLARE @rowsperpage INT 
		                    DECLARE @start INT 
		                    SET @start = " + (start + 1) + @" 
		                    SET @rowsperpage = " + length + @" 
                Select * From (SELECT row_number() OVER (ORDER BY A." + orderBy + " " + orderDir + @") AS RowNum, A.Id,A.Pin,A.TeamMemberId,A.Name,A.DepartmentId,A.DesignationId,A.OrganizationId,A.BranchId,A.CampusId,A.OrganizationShortName,A.BranchName,A.CampusName,A.DepartmentName,A.DesignationName,A.HolidayWorkDate,A.InTime,A.OutTime,A.ApprovalType,A.HolidayWorkId,A.Reason,A.ModifyBy, A.LastModificationDate
	                            FROM(
		                            " + sqlQuery + @"
	                            ) A ) as pagination 
                            " + filterPagination;

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<HolidayApprovalDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<HolidayApprovalDto>().ToList();
        }

        public IList<HolidayWork> LoadHolidayWork(DateTime dateFrom, DateTime dateTo, long teamMemberId)
        {
            ICriteria criteria = Session.CreateCriteria<HolidayWork>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", HolidayWork.EntityStatus.Delete)));
            criteria.Add(Restrictions.Not(Restrictions.Eq("IsPost", true)));
            criteria.CreateAlias("TeamMember", "member");
            criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            criteria.Add(Restrictions.Ge("HolidayWorkDate", dateFrom.Date));
            criteria.Add(Restrictions.Le("HolidayWorkDate", dateTo.Date));
            return criteria.List<HolidayWork>();
        }

        #endregion

        #region Others Function

        public int GetHrHolidayWorkApprovalHistoryRowCount(DateTime holidayWorkDateFrom, DateTime holidayWorkDateTo, List<long> teamMemberIdList = null, List<int> teamMemberPinList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null)
        {
            string sqlQuery = GetHolidayWorkQuery(holidayWorkDateFrom, holidayWorkDateTo, teamMemberIdList, teamMemberPinList, branchIdList, campusIdList, departmentIdList, designationIdList, true);
            string query = "Select Count(*) as totalRow From (" + sqlQuery + ") AS A";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(5000);
            return iQuery.UniqueResult<int>();
        }

        public bool CheckhasHolidayWorkOnADate(long teamMemberId, DateTime date)
        {
            int holidayWorkCount = Session.QueryOver<HolidayWork>().Where(x => x.HolidayWorkDate.Value.Date == date.Date && (x.ApprovalType == (int)HolidayWorkApprovalStatus.Full || x.ApprovalType == (int)HolidayWorkApprovalStatus.Half) && x.Status == HolidayWork.EntityStatus.Active && x.TeamMember.Id == teamMemberId).RowCount();

            if (holidayWorkCount > 0)
                return true;
            return false;

        }

        #endregion

        #region Helper Function

        private string GetHolidayWorkQuery(DateTime holidayWorkDateFrom, DateTime holidayWorkDateTo, List<long> teamMemberIdList = null, List<int> teamMemberPinList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, bool isHistoryQuery = false, bool isMentor = false)
        {
            string teamMemberIdFilter = "";
            string teamMemberPinFilter = "";
            string branchIdFilter = "";
            string campusIdFilter = "";
            string departmentIdFilter = "";
            string designationIdFilter = "";
            string holidayWorkJoin = " Left Join ";

            string holidayWorkDateFilter = " AND hrhd.HolidayWorkDate  >= '" + holidayWorkDateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "'  AND hrhd.HolidayWorkDate  <= '" + holidayWorkDateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "'";
            string attendanceDateFilter = " AND AttendanceDate >= '" + holidayWorkDateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "' AND AttendanceDate <= '" + holidayWorkDateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "'";
            //string effectiveDateFilter = " AND EffectiveDate <= '" + holidayWorkDateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "'  ";


            if (isHistoryQuery)
            {
                holidayWorkJoin = " INNER Join ";
            }
            if (isMentor)
            {
                if (teamMemberIdList != null)
                {
                    teamMemberIdFilter += " AND Id IN( " + string.Join(",", teamMemberIdList) + ") ";
                }
                if (teamMemberPinList != null)
                {
                    teamMemberPinFilter += " AND Pin IN( " + string.Join(",", teamMemberPinList) + ") ";
                }
            }
            else
            {
                if (teamMemberIdList != null && !teamMemberIdList.Contains(SelectionType.SelelectAll))
                {
                    teamMemberIdFilter += " AND Id IN( " + string.Join(",", teamMemberIdList) + ") ";
                }
                if (teamMemberPinList != null && !teamMemberPinList.Contains(0))
                {
                    teamMemberPinFilter += " AND Pin IN( " + string.Join(",", teamMemberPinList) + ") ";
                }
            }

            if (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                branchIdFilter += " AND br.Id IN( " + string.Join(",", branchIdList) + ") ";
            }
            if (campusIdList != null && !campusIdList.Contains(SelectionType.SelelectAll))
            {
                campusIdFilter += " AND c.Id IN( " + string.Join(",", campusIdList) + ") ";
            }
            if (departmentIdList != null && !departmentIdList.Contains(SelectionType.SelelectAll))
            {
                departmentIdFilter += " AND dp.Id IN( " + string.Join(",", departmentIdList) + ") ";
            }
            if (designationIdList != null && !designationIdList.Contains(SelectionType.SelelectAll))
            {
                designationIdFilter += " AND dg.Id IN( " + string.Join(",", designationIdList) + ") ";
            }
            string query = "";
            query += @"SELECT hras.*
	                        ,hrhd.ApprovalType AS ApprovalType
                            ,hrhd.Id AS HolidayWorkId
                            ,hrhd.Reason AS Reason
	                        ,hrm.Name
	                        ,hrm.Pin
	                        ,o.Id AS OrganizationId
	                        ,br.Id AS BranchId
	                        ,c.Id AS CampusId
	                        ,dp.Id AS DepartmentId
	                        ,dg.Id AS DesignationId
	                        ,o.ShortName AS OrganizationShortName
	                        ,br.Name AS BranchName
	                        ,c.Name AS CampusName
	                        ,dp.Name AS DepartmentName
	                        ,dg.Name AS DesignationName
                            ,hrhd.ModifyBy as ModifyBy
                            ,hrhd.ModificationDate as LastModificationDate
	                        FROM (
		                        SELECT 
			                        Id
			                        , TeamMemberId AS TeamMemberId
			                        -- , AttendanceDate AS HolidayWorkDate
                                    , CONVERT(VARCHAR(10),AttendanceDate,121) AS HolidayWorkDate
			                        , [InTime] as InTime
			                        , [OutTime] as OutTime
		                        FROM [dbo].[HR_AttendanceSummary]
		                        WHERE 1=1
                                " + attendanceDateFilter + @"
		                        AND (IsWeekend = 1 OR HolidaySettingId IS NOT NULL) 
                                AND (InTime IS NOT NULL AND OutTime IS NOT NULL)
		                        AND [TeamMemberId] IN ( SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1 " + teamMemberIdFilter + teamMemberPinFilter + @") 
		                        AND [Status] = " + AttendanceSummary.EntityStatus.Active + @"
		                        ) AS hras
                        " + holidayWorkJoin + @" [dbo].[HR_HolidayWork] AS hrhd ON hrhd.TeamMemberId = hras.TeamMemberId AND hrhd.HolidayWorkDate = hras.HolidayWorkDate AND hrhd.Status =  " + HolidayWork.EntityStatus.Active + holidayWorkDateFilter + @"
                        Left Join [dbo].[HR_TeamMember] AS hrm ON hrm.Id = hras.TeamMemberId AND hrm.[Status] = " + TeamMember.EntityStatus.Active + @"
                        left Join [dbo].[HR_DayOffAdjustment] AS hrdayOff ON hrdayOff.TeamMemberId = hras.TeamMemberId AND hrdayOff.InsteadOfDate = hras.HolidayWorkDate AND hrdayOff.Status = " + DayOffAdjustment.EntityStatus.Active + @" AND hrdayOff.DayOffStatus = " + (int)DayOffAdjustmentStatus.Approved + @"
                        
                        CROSS APPLY  (
	                                Select * from (
		                                Select 
		                                emh.CampusId as CampusId
		                                , emh.DesignationId as DesignationId
		                                , emh.DepartmentId as DepartmentId
		                                , emh.TeamMemberId as TeamMemberId
		                                , emh.EffectiveDate As EffectiveDate
		                                , emh.EmploymentStatus as EmploymentStatus
		                                , Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		                                from HR_EmploymentHistory as emh
		                                where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		                                and  emh.EffectiveDate <=  hras.HolidayWorkDate 
		                                and emh.TeamMemberId = hras.TeamMemberId
	                                ) as employmentHistory 
	                                where 1=1
	                                and employmentHistory.r = 1
                                ) as currentPosition 

                        LEFT JOIN [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId AND c.Status = " + Campus.EntityStatus.Active + @"
                        LEFT JOIN [dbo].[Branch] AS br ON br.Id = c.BranchId AND br.Status =  " + Branch.EntityStatus.Active + @"
                        LEFT JOIN [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  " + Organization.EntityStatus.Active + @"
                        LEFT JOIN [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId AND dp.Status =  " + Department.EntityStatus.Active + @"
                        LEFT JOIN [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId AND dg.Status = " + Designation.EntityStatus.Active + @" 
                        Where 1=1 
                        AND hrdayOff.Id IS NULL
                        " + branchIdFilter + campusIdFilter + departmentIdFilter + designationIdFilter + @"
                        ";
            return query;
        }

        #endregion

        #region no Need
        #endregion

    }

}
