﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel.PeerResolvers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Axzkemkeeper;
using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model;
using DeviceSynchronizer.Model.ApiModel;
using DeviceSynchronizer.Model.BusinessModel;
using DeviceSynchronizer.Services;
using log4net;

namespace DeviceSynchronizer.DeviceHelper
{
    public abstract class AxAttendanceDeviceDriver : IAttendanceDeviceDriver
    {
        protected static readonly ILog Log = LogManager.GetLogger("DeviceHelperLogger");
        protected static AxCZKEM axDevice;
        protected AttendanceDeviceModel _attendanceDevice;
        protected MessagePass _throwMessage;

        #region Constructor

        public AxAttendanceDeviceDriver(AttendanceDeviceModel attendanceDevice, MessagePass throwMessage)
        {
            this._attendanceDevice = attendanceDevice;
            this._throwMessage = throwMessage;
            axDevice = new AxCZKEM();
            axDevice.CreateControl();
        }

        #endregion

        #region Properties

        public AxCZKEM AxDevice
        {
            get { return axDevice; }
            set { axDevice = value; }
        }

        #endregion

        #region General

        public bool OpenConnection(bool isDeviceLock = true)
        {
            bool isSuccess = false;
            try
            {
                //Connecting with device
                string connectWithMsg = _attendanceDevice.Name;
                AxDevice.SetCommPassword(Convert.ToInt32(_attendanceDevice.CommunicationKey));
                if (_attendanceDevice.CommunicationType == DeviceCommunicationType.Ethernet)
                {
                    isSuccess = AxDevice.Connect_Net(_attendanceDevice.IPAddress, Convert.ToInt32(_attendanceDevice.Port));
                    connectWithMsg += " [" + _attendanceDevice.IPAddress + "]";
                }
                else if (_attendanceDevice.CommunicationType == DeviceCommunicationType.Usb)
                {
                    isSuccess = AxDevice.Connect_USB(_attendanceDevice.MachineNo);
                    connectWithMsg += " [USB]";
                }

                //Locking Device
                if (isSuccess)
                {
                    OnThrowingMessage("Connected with: " + connectWithMsg, MessageType.Attention);
                    if (isDeviceLock)
                    {
                        isSuccess = LockDevice(isDeviceLock);
                        if (isSuccess)
                            OnThrowingMessage("Device Locked ", MessageType.Info);
                    }
                }
                else
                {
                    OnThrowingMessage("Failed to connect with: " + connectWithMsg, MessageType.Error);
                }

            }
            catch (Exception ex)
            {
                isSuccess = false;
                Log.Error(ex);
            }
            return isSuccess;
        }

        public void CloseConnection()
        {
            try
            {
                //unlock device
                bool isSuccess = LockDevice(false);
                if (isSuccess)
                    OnThrowingMessage("Device unlocked ", MessageType.Info);
                else
                    OnThrowingMessage("Unable to unlock device", MessageType.Error);

                //Disconnect Device
                AxDevice.Disconnect();
                string connectWithMsg = _attendanceDevice.Name;
                if (_attendanceDevice.CommunicationType == DeviceCommunicationType.Ethernet)
                    connectWithMsg += " [" + _attendanceDevice.IPAddress + "]";
                else if (_attendanceDevice.CommunicationType == DeviceCommunicationType.Usb)
                    connectWithMsg += " [USB]";
                OnThrowingMessage("Connection closed for: " + connectWithMsg, MessageType.Attention);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public void RefreshData()
        {
            try
            {
                AxDevice.RefreshData(_attendanceDevice.MachineNo);
                OnThrowingMessage("Refreshed device ", MessageType.Info);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        #endregion

        #region Device Implementation

        public bool SetTime(GetStandardTime getStandardTime)
        {
            bool isSuccess = false;
            try
            {
                OnThrowingMessage("Updating device time", MessageType.Info);
                var serverTime = getStandardTime();

                if (serverTime == null)
                {
                    throw new Exception("Unable to get server time");
                }
                else
                {
                    var deviceTime = GetTime();
                    var standardTime = serverTime.Value;
                    var stime = new DateTime(standardTime.Year, standardTime.Month, standardTime.Day, standardTime.Hour, standardTime.Minute, 0);
                    if (deviceTime != stime)
                    {
                        //Set Device Time
                        isSuccess = AxDevice.SetDeviceTime2(_attendanceDevice.MachineNo, standardTime.Year, standardTime.Month,
                                            standardTime.Day, standardTime.Hour, standardTime.Minute, standardTime.Second);
                        RefreshData();
                        if (isSuccess == false)
                            throw new Exception("Unable to set time on device");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                OnThrowingMessage(ex.Message, MessageType.Error);
            }
            return isSuccess;
        }

        public void Clean()
        {
            try
            {
                OnThrowingMessage("Clearing device data", MessageType.Info);

                //Clear Attendance Data
                bool isSuccess = AxDevice.ClearData(_attendanceDevice.MachineNo, 1);

                //Clear User Data
                if (isSuccess)
                    AxDevice.ClearData(_attendanceDevice.MachineNo, 5);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                OnThrowingMessage("Unable to clear device data", MessageType.Error);
            }
        }

        #endregion

        #region User Implementation

        public bool SaveToDevice(TeamMemberModel member, bool useInternalConnection = false, int machineNum = 0)
        {
            bool returnResult = false;
            //Open Connection if needed
            if (useInternalConnection)
            {
                bool isSuccess = OpenConnection();
                if (!isSuccess) return false;
            }

            //Business logic
            try
            {
                if (member == null) return false;
                string sPassword = "";
                int iPrivilege = 0;
                //bool isEnabled = (member.Status == MemberStatus.Active);
                if (member.IsAdmin)
                {
                    sPassword = _attendanceDevice.CommunicationKey.Substring(0, 4);
                    iPrivilege = 3;
                }
                returnResult = Register(member, sPassword, iPrivilege);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
            finally
            {
                if (useInternalConnection)
                {
                    RefreshData();
                    CloseConnection();
                }
            }
            return returnResult;
        }

        #endregion

        #region Attendance Implementation

        public bool IsAttendanceLogExits()
        {
            try
            {
                bool isSuc = AxDevice.ReadGeneralLogData(_attendanceDevice.MachineNo);
                return isSuc;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        public bool ClearLog()
        {
            try
            {
                bool isSuc = AxDevice.ClearGLog(_attendanceDevice.MachineNo);
                return isSuc;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        #endregion

        #region Protected methods

        protected bool LockDevice(bool isLock)
        {
            try
            {
                bool setEnable = (isLock == false);
                bool isSuccess = AxDevice.EnableDevice(_attendanceDevice.MachineNo, setEnable);
                return isSuccess;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        protected DateTime GetTime()
        {
            int idwYear = 0;
            int idwMonth = 0;
            int idwDay = 0;
            int idwHour = 0;
            int idwMinute = 0;
            int idwSecond = 0;
            try
            {
                AxDevice.GetDeviceTime(_attendanceDevice.MachineNo, ref idwYear,
                    ref idwMonth, ref idwDay, ref idwHour, ref idwMinute, ref idwSecond);
                if (idwYear == 0) throw new Exception("Unable to get device time");
                var deviceTime = new DateTime(idwYear, idwMonth, idwDay, idwHour, idwMinute, 0);
                return deviceTime;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        protected virtual void OnThrowingMessage(string message, MessageType messageType)
        {
            if (_throwMessage != null)
            {
                _throwMessage(message, messageType);
            }
        }

        #endregion

        #region Abstract methods

        //make abstract this interface method so that subclass can implement it
        public abstract List<AttendanceLog> LoadLogList();

        protected abstract bool Register(TeamMemberModel member, string sPassword, int iPrivilege);

        #endregion

    }
}
