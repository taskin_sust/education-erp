﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface ILectureSettingsDao : IBaseDao<LectureSettings, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Functions
        LectureSettings LoadByCourseSubjectPrefix(long courseId, string classNamePrefix, CourseSubject courseSubject = null); 
        #endregion
       
        #region List Loading Functions
        //IList<LectureSettings> GetLectureSettingsList(int start, int length, string orderBy, string orderDir, string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix);  
        IList<LectureSettings> GetLectureSettingsList(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string session, string course, string numberOfClasses, string classNamePrefix);  
        #endregion

        #region Other Functions
        //int LectureSettingsRowCount(string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix); 
        int LectureSettingsRowCount(List<long> organizationIdList, List<long> programIdList, string session, string course, string numberOfClasses, string classNamePrefix); 
        #endregion
    }

    public class LectureSettingsDao : BaseDao<LectureSettings, long>, ILectureSettingsDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Functions
        public LectureSettings LoadByCourseSubjectPrefix(long courseId, string classNamePrefix, CourseSubject courseSubject = null)
        {
            //ICriteria criteria = Session.CreateCriteria<LectureSettings>().Add(Restrictions.Not(Restrictions.Eq("Status", LectureSettings.EntityStatus.Delete)));
            //criteria.Add(Restrictions.Eq("CourseSubject.Id", courseSubjectId))
            //    .Add(Restrictions.Eq("ClassNamePrefix", classNamePrefix));
            if (courseSubject != null)
            {
                return
                    Session.QueryOver<LectureSettings>()
                        .Where(
                            x =>
                                x.CourseSubject.Id == courseSubject.Id && x.ClassNamePrefix == classNamePrefix &&
                                x.Status != LectureSettings.EntityStatus.Delete)
                        .SingleOrDefault<LectureSettings>();
            }
            else
            {
               return Session.QueryOver<LectureSettings>()
                        .Where(
                            x =>
                                x.Course.Id == courseId && x.ClassNamePrefix == classNamePrefix &&
                                x.Status != LectureSettings.EntityStatus.Delete)
                        .SingleOrDefault<LectureSettings>();
            }
            //LectureSettings data =  Session.QueryOver<LectureSettings>().Where(x =>x.CourseSubject.Id == courseSubjectId && x.ClassNamePrefix == classNamePrefix && x.Status != LectureSettings.EntityStatus.Delete);
        } 
        #endregion

        #region List Loading Functions
        //public IList<LectureSettings> GetLectureSettingsList(int start, int length, string orderBy, string orderDir, string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix)
        public IList<LectureSettings> GetLectureSettingsList(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string session, string course, string numberOfClasses, string classNamePrefix)
        {
            ICriteria criteria = Session.CreateCriteria<LectureSettings>().Add(Restrictions.Not(Restrictions.Eq("Status", LectureSettings.EntityStatus.Delete)));
            //criteria.CreateAlias("CourseSubject", "cs");
            criteria.CreateAlias("Course", "c");
            //criteria.CreateAlias("cs.Subject", "sub");
            criteria.CreateAlias("c.Program", "p");
            criteria.CreateAlias("p.Organization", "o");
            criteria.CreateAlias("c.RefSession", "s");
            //if (!String.IsNullOrEmpty(organization))
            //    criteria.Add(Restrictions.Eq("o.Id", Convert.ToInt64(organization)));
            //if (!String.IsNullOrEmpty(program))
            //    criteria.Add(Restrictions.Eq("p.Id", Convert.ToInt64(program)));
            if (organizationIdList!=null)
                criteria.Add(Restrictions.In("o.Id", organizationIdList));
            if (programIdList !=null)
                criteria.Add(Restrictions.In("p.Id", programIdList));
            if (!String.IsNullOrEmpty(session))
                criteria.Add(Restrictions.Eq("s.Id", Convert.ToInt64(session)));
            if (!String.IsNullOrEmpty(course))
                criteria.Add(Restrictions.Eq("c.Id", Convert.ToInt64(course)));
            if (!String.IsNullOrEmpty(numberOfClasses))
                criteria.Add(Restrictions.Eq("NumberOfClasses", Convert.ToInt32(numberOfClasses)));
            if (!String.IsNullOrEmpty(classNamePrefix))
                criteria.Add(Restrictions.Like("ClassNamePrefix", classNamePrefix, MatchMode.Anywhere));
            // criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            return (List<LectureSettings>)criteria.SetFirstResult(start).SetMaxResults(length).List<LectureSettings>();
        }
        #endregion

        #region Other Functions
        //public int LectureSettingsRowCount(string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix)
        public int LectureSettingsRowCount(List<long> organizationIdList, List<long> programIdList, string session, string course, string numberOfClasses, string classNamePrefix)
        {
            ICriteria criteria = Session.CreateCriteria<LectureSettings>().Add(Restrictions.Not(Restrictions.Eq("Status", LectureSettings.EntityStatus.Delete)));

            //criteria.CreateAlias("CourseSubject", "cs");
            criteria.CreateAlias("Course", "c");
            //criteria.CreateAlias("cs.Subject", "sub");
            criteria.CreateAlias("c.Program", "p");
            criteria.CreateAlias("p.Organization", "o");
            criteria.CreateAlias("c.RefSession", "s");

            //if (!String.IsNullOrEmpty(organization))
            //    criteria.Add(Restrictions.Eq("o.Id", Convert.ToInt64(organization)));
            //if (!String.IsNullOrEmpty(program))
            //    criteria.Add(Restrictions.Eq("p.Id", Convert.ToInt64(program)));
            if (organizationIdList != null)
                criteria.Add(Restrictions.In("o.Id", organizationIdList));
            if (programIdList != null)
                criteria.Add(Restrictions.In("p.Id", programIdList));
            if (!String.IsNullOrEmpty(session))
                criteria.Add(Restrictions.Eq("s.Id", Convert.ToInt64(session)));
            if (!String.IsNullOrEmpty(course))
                criteria.Add(Restrictions.Eq("c.Id", Convert.ToInt64(course)));

            if (!String.IsNullOrEmpty(numberOfClasses))
                criteria.Add(Restrictions.Eq("NumberOfClasses", Convert.ToInt32(numberOfClasses)));
            if (!String.IsNullOrEmpty(classNamePrefix))
                criteria.Add(Restrictions.Like("ClassNamePrefix", classNamePrefix, MatchMode.Anywhere));

            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
        
        #endregion

        #region Helper Function

        #endregion
    } 
}
