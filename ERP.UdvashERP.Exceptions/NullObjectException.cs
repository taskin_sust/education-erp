﻿using System;

namespace UdvashERP.MessageExceptions
{
    public class NullObjectException : Exception
    {
        public NullObjectException():base("Null Value")
        {

        }

        public NullObjectException(string message):base(message)
        {

        }
    }
}
