﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.LectureSettingTest
{
    public class LectureSettingBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal TestBaseService<ProgramSessionSubject> _TestBaseService;
        internal ILectureSettingService _lectureSettingService;
        internal ILectureService _lectureService;
        internal IProgramService _programService;
        internal ISessionService _sessionService;
        internal IBranchService _branchService;
        internal ICampusService _campusService;
        private ISession _session;
        internal LectureSettingFactory _lectureSettingFactory;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal DateTime _startTime = DateTime.Now.AddDays(1).AddHours(1);
        internal DateTime _endTime = DateTime.Now.AddDays(1).AddHours(10);
        #endregion

        public LectureSettingBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _lectureSettingFactory = new LectureSettingFactory(null, _session);
            _lectureSettingService = new LectureSettingService(_session);
            _programService = new ProgramService(_session);
            _sessionService = new SessionService(_session);
            _branchService = new BranchService(_session);
            _campusService = new CampusService(_session);
            _lectureService=new LectureService(_session);
            _TestBaseService = new TestBaseService<ProgramSessionSubject>(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _lectureSettingFactory.Cleanup();
            base.Dispose();
        }
    }
}
