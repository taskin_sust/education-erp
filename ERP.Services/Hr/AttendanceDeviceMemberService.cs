﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessModel.Dto.Hr;

namespace UdvashERP.Services.Hr
{
    public interface IAttendanceDeviceMemberService : IBaseService
    {
        #region Operational Function

        void UpdateAttendanceMemberData(long deviceId, DateTime updateTime, List<int> enrolList);

        #endregion

        #region Single Instances Loading Function

        AttendanceDeviceMember GetAttendanceDeviceMember(int enroll, long deviceId);

        #endregion

        #region List Loading Function

        IList<AttendanceDeviceMemberDto> LoadMemberInfos(AttendanceDevice attendanceDevice);

        #endregion


    }
    public class AttendanceDeviceMemberService : BaseService, IAttendanceDeviceMemberService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly IAttendanceDeviceMemberDao _attendanceDeviceMemberDao;
        public AttendanceDeviceMemberService(ISession session)
        {
            Session = session;
            _attendanceDeviceMemberDao = new AttendanceDeviceMemberDao() { Session = session };
        }

        #endregion

        #region Operational Function

        public void UpdateAttendanceMemberData(long deviceId, DateTime updateTime, List<int> enrolList)
        {
            ITransaction transaction = null;
            try
            {
                IList<AttendanceDeviceMember> deviceMembers = _attendanceDeviceMemberDao.LoadAttendanceMember(deviceId, enrolList);
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var attendanceDeviceMember in deviceMembers)
                    {
                        attendanceDeviceMember.LastSyncedDateTime = updateTime;
                        attendanceDeviceMember.IsSynced = 1;
                        Session.Update(attendanceDeviceMember);
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public AttendanceDeviceMember GetAttendanceDeviceMember(int enroll, long deviceId)
        {
            try
            {
                if (enroll <= 0 || deviceId <= 0) throw new InvalidDataException("invalid data");
                return _attendanceDeviceMemberDao.GetAttendanceDeviceMemberByEnrollDeviceId(enroll, deviceId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<AttendanceDeviceMemberDto> LoadMemberInfos(AttendanceDevice attendanceDevice)
        {
            try
            {
                return _attendanceDeviceMemberDao.LoadMemberInfos(attendanceDevice);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
