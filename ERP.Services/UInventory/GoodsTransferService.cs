using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.UInventory;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.UInventory
{
    public interface IGoodsTransferService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(GoodsTransfer goodsTransfer);

        #endregion

        #region Single Instances Loading Function

        List<GoodsTransferDto> GetGoodsTransfer(long id);

        #endregion

        #region List Loading Function

        IList<GoodsTransferDto> LoadGoodsTransfer(List<UserMenu> userMenus, int start, int length, string orderBy, string orderDir, long? organizationFromId, long? branchFromId, long? organizationToId, long? branchToId, string itemName, string requiredDate);

        #endregion

        #region Others Function

        int GetGoodsTransferRowCount(List<UserMenu> userMenus, long? organizationFromId, long? branchFromId, long? organizationToId, long? branchToId, string itemName, string requiredDate);
        int GetGoodsTransferByBranchRowCount(List<UserMenu> userMenus, long organizationId, long branchId, long organizationToId, long[] branchToId, long[] itemGroupId, long[] itemId, DateTime dateFrom, DateTime dateTo);
        IList<dynamic> LoadGoodsTransferByBranch(List<UserMenu> userMenus, int start, int length, long organizationId, long branchId, long organizationToId, long[] branchToId, long[] itemGroupId, long[] itemId, DateTime dateFrom, DateTime dateTo);
        IList<dynamic> LoadGoodsTransferByProgram(List<UserMenu> userMenus, int start, int length, long organizationId, long branchId, long organizationToId, long[] branchToId, long[] itemGroupId, long[] itemId, DateTime dateFrom, DateTime dateTo);
        int GetGoodsTransferByProgramRowCount(List<UserMenu> userMenus, long organizationId, long branchId, long organizationToId, long[] branchToId, long[] itemGroupId, long[] itemId, DateTime dateFrom, DateTime dateTo);


        #endregion

        #region Helper Function

        #endregion

    }
    public class GoodsTransferService : BaseService, IGoodsTransferService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IGoodsTransferDao _goodsTransferDao;
        private readonly ICurrentStockSummaryDao _currentStockSummaryDao;
        private readonly IBranchDao _branchDao;
        private IProgramDao _programDao;
        private IProgramBranchSessionDao _programBranchSessionDao;
        private ItemDao _itemDao;
        public GoodsTransferService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _goodsTransferDao = new GoodsTransferDao { Session = session };
            _currentStockSummaryDao = new CurrentStockSummaryDao { Session = session };
            _branchDao = new BranchDao { Session = session };
            _programDao = new ProgramDao { Session = session };
            _programBranchSessionDao = new ProgramBranchSessionDao { Session = session };
            _itemDao = new ItemDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public void SaveOrUpdate(GoodsTransfer goodsTransfer)
        {
            ITransaction trans = null;
            try
            {
                if (goodsTransfer.BranchFrom == null)
                    throw new NullObjectException("Branch from can't be null.");

                if (goodsTransfer.BranchTo == null)
                    throw new NullObjectException("Branch to can't be null.");

                if (goodsTransfer.GoodsTransferDetails == null || goodsTransfer.GoodsTransferDetails.Count == 0)
                {
                    throw new NullObjectException("Goods transfer details can not be null.");
                }

                var distincList = new HashSet<GoodsTransferDetails>(goodsTransfer.GoodsTransferDetails);
                if (goodsTransfer.GoodsTransferDetails.Count != distincList.Count)
                {
                    throw new InvalidDataException("Goods transfer details contain item details row with same property!");
                }

                IList<CurrentStockSummary> stockSummariesFromBranch = new List<CurrentStockSummary>();
                IList<CurrentStockSummary> stockSummariesToBranch = new List<CurrentStockSummary>();

                IList<GoodsTransferDetails> goodsTransferList = new List<GoodsTransferDetails>();

                foreach (var transferDetails in goodsTransfer.GoodsTransferDetails)
                {
                    //check for validation
                    if (transferDetails.Item == null)
                        throw new NullObjectException("Item can't be null.!");

                    if (goodsTransfer.BranchFrom.Organization != goodsTransfer.BranchTo.Organization)
                    {
                        if (goodsTransfer.BranchFrom.IsCorporate==false && goodsTransfer.BranchTo.IsCorporate == false )
                            throw new InvalidDataException("You can't transfer goods");
            
                        if (transferDetails.Item.ItemType != (int)ItemType.CommonItem)
                            throw new InvalidDataException("You can't transfer goods");

                    }
                    else
                    {

                        if (goodsTransfer.BranchFrom.IsCorporate)
                        {
                            if (goodsTransfer.BranchTo.IsCorporate == false)
                            {
                                throw new InvalidDataException("You can't transfer goods");
                            }
                        }
                        else
                        {
                            if (goodsTransfer.BranchTo.IsCorporate)
                            {
                                throw new InvalidDataException("You can't transfer goods");
                            }
                        }


                        if (goodsTransfer.BranchFrom == goodsTransfer.BranchTo)
                        {
                            throw new InvalidDataException("You can't transfer goods in the same branch");
                        }
                    }

                    if (transferDetails.TransferQuantity <= 0)
                        throw new InvalidDataException("Transfer quantity can't be zero or negative.!");

                    //assign data in the goods details

                    if (HttpContext.Current != null)
                    {
                        transferDetails.CreateBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                        transferDetails.ModifyBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                    }

                    transferDetails.Status = GoodsTransfer.EntityStatus.Active;
                    goodsTransferList.Add(transferDetails);

                    //get the current stock of this item using program, session and from branch
                    CurrentStockSummary currentStockSummaryFromBranch = new CurrentStockSummary
                    {
                        Item = transferDetails.Item,
                        Program = transferDetails.Program,
                        Session = transferDetails.Session,
                        Branch = transferDetails.GoodsTransfer.BranchFrom
                    };

                    CurrentStockSummary currentStockSummaryToBranch = new CurrentStockSummary
                    {
                        Item = transferDetails.Item,
                        Program = transferDetails.Program,
                        Session = transferDetails.Session,
                        Branch = transferDetails.GoodsTransfer.BranchTo
                    };

                    //var currentStockQuantityFromBranch = _currentStockSummaryDao.GetCurrentStockQuantity(transferDetails.Program != null ? _commonHelper.ConvertIdToList(transferDetails.Program.Id) : null, transferDetails.Session != null ? _commonHelper.ConvertIdToList(transferDetails.Session.Id) : null, _commonHelper.ConvertIdToList(transferDetails.GoodsTransfer.BranchFrom.Id), transferDetails.Item.Id);
                    //var currentStockQuantityToBranch = _currentStockSummaryDao.GetStockQuantity(transferDetails.Program != null ? _commonHelper.ConvertIdToList(transferDetails.Program.Id) : null, transferDetails.Session != null ? _commonHelper.ConvertIdToList(transferDetails.Session.Id) : null, _commonHelper.ConvertIdToList(transferDetails.GoodsTransfer.BranchTo.Id), transferDetails.Item.Id);
                    long? programId = null;
                    long? sessionId = null;
                    if (transferDetails.Program != null && transferDetails.Session != null)
                    {
                        programId = transferDetails.Program.Id;
                        sessionId = transferDetails.Session.Id;
                    }


                    var currentStockQuantityFromBranch = _currentStockSummaryDao.GetCurrentStockSummary(transferDetails.Item.Id, transferDetails.GoodsTransfer.BranchFrom.Id, programId, sessionId);
                    var currentStockQuantityToBranch = _currentStockSummaryDao.GetCurrentStockSummary(transferDetails.Item.Id, transferDetails.GoodsTransfer.BranchTo.Id, programId, sessionId);


                    if (currentStockQuantityFromBranch != null)
                    {
                        //validation
                        if (currentStockQuantityFromBranch.StockQuantity <= 0)
                            throw new InvalidDataException("Stock quantity can't be zero or negative.!");
                        if (transferDetails.TransferQuantity > currentStockQuantityFromBranch.StockQuantity)
                            throw new InvalidDataException("Transfer quantity can't be zero or negative.!");
                    }
                    else
                    {
                        throw new InvalidDataException("Stock quantity zero");
                    }

                    if (currentStockQuantityToBranch != null)
                    {
                        currentStockSummaryToBranch.StockQuantity = currentStockQuantityToBranch.StockQuantity + transferDetails.TransferQuantity;
                    }
                    else
                    {
                        currentStockSummaryToBranch.StockQuantity = transferDetails.TransferQuantity;
                    }

                    currentStockSummaryFromBranch.StockQuantity = currentStockQuantityFromBranch.StockQuantity - transferDetails.TransferQuantity;

                    stockSummariesFromBranch.Add(currentStockSummaryFromBranch);
                    stockSummariesToBranch.Add(currentStockSummaryToBranch);
                }

                using (trans = Session.BeginTransaction())
                {
                    goodsTransfer.TransferNo = GetCurrentTransferNo(goodsTransfer.BranchFrom);
                    goodsTransfer.GoodsTransferDetails = goodsTransferList;
                    _goodsTransferDao.Save(goodsTransfer);

                    //operation for from branch stock quantity
                    foreach (var updateStock in stockSummariesFromBranch)
                    {
                        var program = updateStock.Program;
                        var session = updateStock.Session;
                        long? programId = null;
                        long? sessionId = null;
                        if (program != null && session != null)
                        {
                            programId = program.Id;
                            sessionId = session.Id;
                        }
                        var currentItemStock = _currentStockSummaryDao.GetCurrentStockSummary(updateStock.Item.Id, updateStock.Branch.Id, programId, sessionId);
                        currentItemStock.StockQuantity = updateStock.StockQuantity;
                        _currentStockSummaryDao.Update(currentItemStock);
                    }
                    //operation for to branch stock quantity

                    foreach (var updateToBranchStock in stockSummariesToBranch)
                    {
                        var program = updateToBranchStock.Program;
                        var session = updateToBranchStock.Session;
                        long? programId = null;
                        long? sessionId = null;
                        if (program != null && session != null)
                        {
                            programId = program.Id;
                            sessionId = session.Id;
                        }
                        var currentItemStock = _currentStockSummaryDao.GetCurrentStockSummary(updateToBranchStock.Item.Id, updateToBranchStock.Branch.Id, programId, sessionId);
                        if (currentItemStock != null)
                        {
                            currentItemStock.StockQuantity = updateToBranchStock.StockQuantity;
                            _currentStockSummaryDao.Update(currentItemStock);
                        }
                        else
                        {
                            _currentStockSummaryDao.Save(updateToBranchStock);
                        }

                    }

                    trans.Commit();
                }
            }
            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }

        }

        #endregion

        #region Single Instances Loading Function

        public List<GoodsTransferDto> GetGoodsTransfer(long id)
        {
            List<GoodsTransferDto> dt = new List<GoodsTransferDto>();
            try
            {
                dt = _goodsTransferDao.GetGoodsTransfer(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        #endregion

        #region List Loading Function

        public IList<GoodsTransferDto> LoadGoodsTransfer(List<UserMenu> userMenus, int start, int length, string orderBy, string orderDir, long? organizationFromId, long? branchFromId, long? organizationToId, long? branchToId, string itemName, string requiredDate)
        {
            IList<GoodsTransferDto> goodsTransfer;
            try
            {

                List<long> authorizeOrganizationFrom = AuthHelper.LoadOrganizationIdList(userMenus, (organizationFromId == null) ? null : _commonHelper.ConvertIdToList((long)organizationFromId));
                List<long> authorizeOrganizationTo = AuthHelper.LoadOrganizationIdList(userMenus, (organizationToId == null) ? null : _commonHelper.ConvertIdToList((long)organizationToId));

                List<long> authorizeBranchFrom = AuthHelper.LoadBranchIdList(userMenus, authorizeOrganizationFrom, null, (branchFromId == null) ? null : _commonHelper.ConvertIdToList((long)branchFromId));
                List<long> authorizeBranchTo = AuthHelper.LoadBranchIdList(userMenus, authorizeOrganizationTo, null, (branchToId == null) ? null : _commonHelper.ConvertIdToList((long)branchToId));

                //List<long> authorizeOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenus, (organization == null) ? null : _commonHelper.ConvertIdToList((long)organization));
                //List<long> authorizeBranchIdList = AuthHelper.LoadBranchIdList(userMenus, authorizeOrganizationIdList, null, (branch == null) ? null : _commonHelper.ConvertIdToList((long)branch));
                List<long> authoOrganization = authorizeOrganizationFrom.Concat(authorizeOrganizationTo).ToList();
                List<long> authoBranch = authorizeBranchFrom.Concat(authorizeBranchTo).ToList();

                IList<long> authorizeProgramIds = AuthHelper.LoadProgramIdList(userMenus, authoOrganization, authoBranch);

                if (authoOrganization == null || !authoOrganization.Any())
                    throw new InvalidDataException("Invalid Organization");

                if (authoBranch == null || !authoBranch.Any())
                    throw new InvalidDataException("Invalid Branch");

                goodsTransfer = _goodsTransferDao.LoadGoodsTransfer(start, length, orderBy, orderDir, authorizeBranchFrom, authorizeBranchTo, authorizeProgramIds, itemName, requiredDate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return goodsTransfer;
        }



        #endregion

        #region Others Function

        public int GetGoodsTransferRowCount(List<UserMenu> userMenus, long? organizationFromId, long? branchFromId, long? organizationToId, long? branchToId, string itemName, string requiredDate)
        {
            int count;
            try
            {
                List<long> authorizeOrganizationFrom = AuthHelper.LoadOrganizationIdList(userMenus, (organizationFromId == null) ? null : _commonHelper.ConvertIdToList((long)organizationFromId));
                List<long> authorizeOrganizationTo = AuthHelper.LoadOrganizationIdList(userMenus, (organizationToId == null) ? null : _commonHelper.ConvertIdToList((long)organizationToId));

                List<long> authorizeBranchFrom = AuthHelper.LoadBranchIdList(userMenus, authorizeOrganizationFrom, null, (branchFromId == null) ? null : _commonHelper.ConvertIdToList((long)branchFromId));
                List<long> authorizeBranchTo = AuthHelper.LoadBranchIdList(userMenus, authorizeOrganizationTo, null, (branchToId == null) ? null : _commonHelper.ConvertIdToList((long)branchToId));

                List<long> authoOrganization = authorizeOrganizationFrom.Concat(authorizeOrganizationTo).ToList();
                List<long> authoBranch = authorizeBranchFrom.Concat(authorizeBranchTo).ToList();

                List<long> authorizeProgramIds = AuthHelper.LoadProgramIdList(userMenus, authoOrganization, authoBranch);

                count = _goodsTransferDao.GetGoodsTransferRowCount(authorizeBranchFrom, authorizeBranchTo, authorizeProgramIds, itemName, requiredDate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return count;
        }

        public int GetGoodsTransferByBranchRowCount(List<UserMenu> userMenus, long organizationId, long branchId, long organizationToId, long[] branchToId, long[] itemGroupId, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            int returnValue = 0;
            try
            {
                List<long> organizationIdsFrom;
                List<long> branchIdsFrom;
                List<long> organizationIdsTo;
                List<long> branchIdsTo;
                List<long> programIds;
                IList<string> branchNames;
               
                GetAuthorizeValues(userMenus,organizationId, out organizationIdsFrom, branchId, out branchIdsFrom, organizationToId, out organizationIdsTo, branchToId, out branchIdsTo, out programIds, out branchNames);

                #region OLD CODE

                //List<long> authOrganizationFrom = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                //List<long> authBranchFrom = AuthHelper.LoadBranchIdList(userMenus, authOrganizationFrom, null, _commonHelper.ConvertIdToList(branchId));
                //List<long> authOrganizationTo = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationToId));
                //List<long> authBranchTo = AuthHelper.LoadBranchIdList(userMenus, authOrganizationTo, null, (branchToId).ToList());

                //List<long> authOrganization = authOrganizationFrom.Concat(authOrganizationTo).ToList();

                //List<long> branchIds;
                //if (authBranchTo.Count > 0)
                //{
                //    List<long> authBranch = authBranchFrom.Concat(authBranchTo).ToList();
                //    //branchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganizationTo, null, authBranch);
                //    branchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganization, null, authBranch);
                //}
                //else
                //{
                //    //branchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganizationTo);
                //    branchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganization);
                //}

                //IList<string> branchNameList = _branchDao.LoadBranch(authOrganization, null, null, branchIds, null, false).Select(x => x.Name).ToList();

                //List<long> authProgramIds = AuthHelper.LoadProgramIdList(userMenus, authOrganization, branchIds);

                #endregion

                returnValue = _goodsTransferDao.GetGoodsTransferByBranchRowCount(organizationIdsFrom, branchIdsFrom, organizationIdsTo, branchIdsTo, programIds, branchNames, itemId, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return returnValue;
        }

        private void GetAuthorizeValues(List<UserMenu> userMenus, long organizationId, out List<long> organizationIdsFrom, long branchId, out List<long> branchIdsFrom, long organizationToId, out List<long> organizationIdsTo, long[] branchToId, out List<long> branchIdsTo, out List<long> programIds, out IList<string> branchNames)
        {
            List<long> authOrganizationFrom = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));//from organization
            List<long> authBranchFrom = AuthHelper.LoadBranchIdList(userMenus, authOrganizationFrom, null, _commonHelper.ConvertIdToList(branchId));//from branch

            List<long> authOrganizationTo = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationToId));//to organization
            List<long> authBranchTo = AuthHelper.LoadBranchIdList(userMenus, authOrganizationTo, null, !branchToId.Contains(0) ? branchToId.ToList() : null);

            IList<string> branchNameList = _branchDao.LoadBranch(authOrganizationTo, null, null, authBranchTo, null, null).Select(x => x.Name).ToList();

            List<long> authProgramIds = AuthHelper.LoadProgramIdList(userMenus, authOrganizationTo, authBranchTo, null);

            organizationIdsFrom = authOrganizationFrom;
            branchIdsFrom = authBranchFrom;
            organizationIdsTo = authOrganizationTo;
            branchIdsTo = authBranchTo;
            programIds = authProgramIds;
            branchNames = branchNameList;
        }

        //already modify this method
        public IList<dynamic> LoadGoodsTransferByBranch(List<UserMenu> userMenus, int start, int length, long organizationId, long branchId, long organizationToId, long[] branchToId, long[] itemGroupId, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            IList<dynamic> returnDynamicList;
            try
            {
                //List<long> authOrganizationFrom = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));//from organization
                //List<long> authBranchFrom = AuthHelper.LoadBranchIdList(userMenus, authOrganizationFrom, null, _commonHelper.ConvertIdToList(branchId));//from branch

                //List<long> authOrganizationTo = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationToId));//to organization
                //List<long> authBranchTo = AuthHelper.LoadBranchIdList(userMenus, authOrganizationTo, null, !branchToId.Contains(0) ? branchToId.ToList() : null);

                //IList<string> branchNameList = _branchDao.LoadBranch(authOrganizationTo, null, null, authBranchTo, null, null).Select(x => x.Name).ToList();

                //List<long> authProgramIds = AuthHelper.LoadProgramIdList(userMenus, authOrganizationTo, authBranchTo, null);
                List<long> organizationIdsFrom;
                List<long> branchIdsFrom;
                List<long> organizationIdsTo;
                List<long> branchIdsTo;
                List<long> programIds;
                IList<string> branchNames;

                GetAuthorizeValues(userMenus, organizationId, out organizationIdsFrom, branchId, out branchIdsFrom, organizationToId, out organizationIdsTo, branchToId, out branchIdsTo, out programIds, out branchNames);
                returnDynamicList = _goodsTransferDao.LoadGoodsTransferByBranch(start, length, organizationIdsFrom, branchIdsFrom, organizationIdsTo, branchIdsTo, programIds, branchNames, itemId, dateFrom, dateTo);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return returnDynamicList;
        }


        public int GetGoodsTransferByProgramRowCount(List<UserMenu> userMenus, long organizationId, long branchId, long organizationToId, long[] branchToId, long[] itemGroupId, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            int returnValue = 0;
            try
            {
                List<long> authOrganizationFrom = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                List<long> authBranchFrom = AuthHelper.LoadBranchIdList(userMenus, authOrganizationFrom, null, _commonHelper.ConvertIdToList(branchId));
                List<long> authOrganizationTo = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationToId));
                List<long> authBranchTo = AuthHelper.LoadBranchIdList(userMenus, authOrganizationTo, null, (branchToId).ToList());

                List<long> authOrganization = authOrganizationFrom.Concat(authOrganizationTo).ToList();

                List<long> programIds;
                List<long> branchIds;
                if (authBranchTo.Count > 0)
                {
                    List<long> authBranch = authBranchFrom.Concat(authBranchTo).ToList();
                    programIds = AuthHelper.LoadProgramIdList(userMenus, authOrganizationTo, authBranch);
                    //new line added
                    branchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganization, null, authBranch);

                }
                else
                {
                    //if no branch selecet passing  branch from list
                    programIds = AuthHelper.LoadProgramIdList(userMenus, authOrganizationTo, authBranchFrom);
                    //new line added
                    branchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganization);

                }

                List<long> authProgramIds = AuthHelper.LoadProgramIdList(userMenus, authOrganization, branchIds, programIds);

                IList<string> programNameList = _programDao.LoadAuthorizedProgram(programIds).Select(x => x.Name).ToList();

                returnValue = _goodsTransferDao.GetGoodsTransferByProgramRowCount(authOrganizationFrom, authBranchFrom, authOrganizationTo, authBranchTo, authProgramIds, programNameList, itemId, dateFrom, dateTo);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return returnValue;
        }

        public IList<dynamic> LoadGoodsTransferByProgram(List<UserMenu> userMenus, int start, int length, long organizationId, long branchId, long organizationToId, long[] branchToId, long[] itemGroupId, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            IList<dynamic> returnDynamicList = new List<dynamic>();
            try
            {
                List<long> authOrganizationFrom = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                List<long> authBranchFrom = AuthHelper.LoadBranchIdList(userMenus, authOrganizationFrom, null, _commonHelper.ConvertIdToList(branchId));
                List<long> authOrganizationTo = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationToId));
                
                //List<long> authBranchTo = AuthHelper.LoadBranchIdList(userMenus, authOrganizationTo, null, (branchToId).ToList());


                List<long> authBranchTo = AuthHelper.LoadBranchIdList(userMenus, authOrganizationTo, null, branchToId.Contains(0) ? null : branchToId.ToList());

                List<long> authProgram = AuthHelper.LoadProgramIdList(userMenus, authOrganizationTo, authBranchTo, null);

                //var programSessionListF = _programBranchSessionDao.LoadProgramSession(authOrganizationFrom, authBranchFrom, null, itemId.Contains(0) ? null : itemId.ToList());
                var programSessionListT = _programBranchSessionDao.LoadProgramSession(authOrganizationTo, authBranchTo, authProgram, itemId.Contains(0) ? null : itemId.ToList());
                IList<string> programNameList = new List<string>();
                List<long> authProgramIds = new List<long>();
                foreach (var programSession in programSessionListT)
                {
                    string programSessionConcate = programSession.ProgramShortName + " " + programSession.SessionName;
                    programNameList.Add(programSessionConcate);
                    authProgramIds.Add(programSession.ProgramId);
                }
                programNameList.Insert(0, "N/A");

                returnDynamicList = _goodsTransferDao.LoadGoodsTransferByProgram(start, length, authOrganizationFrom, authBranchFrom, authOrganizationTo, authBranchTo, authProgramIds, programNameList, itemId, dateFrom, dateTo);
                #region OLD CODE
                //List<long> authOrganization = authOrganizationFrom.Concat(authOrganizationTo).ToList();

                //List<long> programIds;
                //List<long> branchIds;
               
                //IList<ProgramSessionDto> programSessionList = new List<ProgramSessionDto>();
                //if (authBranchTo.Count > 0)
                //{
                //    List<long> authBranch = authBranchFrom.Concat(authBranchTo).ToList();
                //    branchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganization, null, authBranch);
                //    programIds = AuthHelper.LoadProgramIdList(userMenus, authOrganizationTo, authBranch);
                //    //  programNameList = _programDao.LoadAuthorizedProgram(programIds, CommonHelper.ConvertSelectedAllIdList(null), null, null, authBranch).Select(x => x.ShortName).ToList();
                //    programSessionList = _programBranchSessionDao.LoadProgramSession(authOrganization, branchIds, programIds, itemId.Contains(0) ? null : itemId.ToList());
                //    foreach (var programSession in programSessionList)
                //    {
                //        string programSessionConcate = programSession.ProgramShortName + " " + programSession.SessionName;
                //        programNameList.Add(programSessionConcate);
                //    }

                //}
                //else
                //{
                //    branchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganization);
                //    programIds = AuthHelper.LoadProgramIdList(userMenus, authOrganizationTo, authBranchFrom);
                //    //programNameList = _programDao.LoadAuthorizedProgram(programIds, CommonHelper.ConvertSelectedAllIdList(null), null, null, branchIds).Select(x => x.ShortName).ToList();
                //    programSessionList = _programBranchSessionDao.LoadProgramSession(authOrganization, branchIds, programIds, itemId.Contains(0) ? null : itemId.ToList());
                //    foreach (var programSession in programSessionList)
                //    {
                //        string programSessionConcate = programSession.ProgramShortName + " " + programSession.SessionName;
                //        programNameList.Add(programSessionConcate);
                //    }
                //}

                //List<long> authProgramIds = AuthHelper.LoadProgramIdList(userMenus, authOrganization, branchIds);

                #endregion
 
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return returnDynamicList;
        }


        #endregion

        #region Helper function

        public string GetCurrentTransferNo(Branch branch)
        {
            GoodsTransfer obj = Session.QueryOver<GoodsTransfer>().Where(x => x.BranchFrom.Id == branch.Id).List<GoodsTransfer>().OrderByDescending(x => x.Id).Take(1).SingleOrDefault();
            if (obj == null) return branch.Organization.ShortName.Substring(0, 3) + branch.Code + "GT" + "00000" + 1;
            return branch.Organization.ShortName.Substring(0, 3) + branch.Code + "GT" + (Convert.ToInt64(obj.TransferNo.Split(new string[] { "GT" }, StringSplitOptions.None)[1]) + 1).ToString("000000");
        }

        #endregion
    }
}

