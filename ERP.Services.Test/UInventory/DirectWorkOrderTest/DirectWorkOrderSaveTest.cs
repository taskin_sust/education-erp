﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.AdminisTration.OrganizationTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.DirectWorkOrderTest
{
    public interface IDirectWorkOrderSaveTest
    {
        #region Basic Test

        void Save_Should_Return_Null_Exception();
        void Should_Save_Successfully_For_Program_Item();
        void Should_Save_Successfully_For_Common_Item();

        #endregion

        #region Business Logic Test

        void Save_Should_Check_Invalid_Entry(int i);

        #endregion
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "WorkOrder")]
    public class DirectWorkOrderSaveTest : DirectWorkOrderBaseTest, IDirectWorkOrderSaveTest
    {
        #region Test Related Properties

        public static IEnumerable<object[]> WorkOrderIndex
        {
            get
            {
                var indexList = new List<object[]>();
                for (int i = 1; i < 7; i++)
                    indexList.Add(new object[] { i });
                return indexList;
            }
        }

        #endregion

        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _workOrderService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Save_Successfully_For_Program_Item()
        {
            var directworkOrderFactory = _directWorkOrderFactory.Create().WithBranch().WithSupplier();
            var organization = directworkOrderFactory.Object.Branch.Organization;
            directworkOrderFactory.WithWorkOrderDetails(organization).Persist();
            var workOrder = _workOrderService.GetWorkOrder(_directWorkOrderFactory.Object.Id);
            Assert.NotNull(workOrder);
            Assert.True(workOrder.Id > 0);
        }

        [Fact]
        public void Should_Save_Successfully_For_Common_Item()
        {
            var directworkOrderFactory = _directWorkOrderFactory.Create().WithBranch(true).WithSupplier();
            var organization = directworkOrderFactory.Object.Branch.Organization;
            directworkOrderFactory.WithWorkOrderDetails(organization,ItemType.CommonItem,null).Persist();
            var workOrder = _workOrderService.GetWorkOrder(_directWorkOrderFactory.Object.Id);
            Assert.NotNull(workOrder);
            Assert.True(workOrder.Id > 0);
        }

        #endregion

        #region Business Logic Test

        [Theory]
        [MemberData("WorkOrderIndex", MemberType = typeof(DirectWorkOrderSaveTest))]
        public void Save_Should_Check_Invalid_Entry(int i)
        {
            var workOrder = _directWorkOrderFactory.Create().WithBranch().WithSupplier().WithWorkOrderDetails().Object;
            if (i == 1)
            {
                workOrder.Supplier = null;
            }
            if (i == 2)
            {
                workOrder.DeliveryDate = null;
            }
            if (i == 3)
            {
                workOrder.WorkOrderDetails[0].Item = null;
            }
            if (i == 4)
            {
                workOrder.WorkOrderDetails[0].WorkOrderCriteriaList[0].Criteria = null;
            }
            if (i == 5)
            {
                workOrder.WorkOrderDetails[0].WorkOrderCriteriaList[0].CriteriaValue = null;
            }
            if (i == 6)
            {
                workOrder.WorkOrderDetails[0].WorkOrderCriteriaList = null;
            }
            if (i == 7)
            {
                workOrder.WorkOrderDetails = null;
            }
            Assert.Throws<InvalidDataException>(() => _workOrderService.SaveOrUpdate(workOrder));
        }

        [Fact]
        public void Purpose_Should_Not_Be_Null_For_Individual_Branch()
        {
            var directworkOrderFactory = _directWorkOrderFactory.Create().WithBranch().WithSupplier();
            var organization = directworkOrderFactory.Object.Branch.Organization;
            directworkOrderFactory.WithWorkOrderDetails(organization);
            directworkOrderFactory.Object.WorkOrderDetails[0].Purpose = null;
            Assert.Throws<InvalidDataException>(() => _workOrderService.SaveOrUpdate(directworkOrderFactory.Object));
        }

        [Fact]
        public void Program_Session_Should_Be_Null_For_Common_Item()
        {
            var directworkOrderFactory = _directWorkOrderFactory.Create().WithBranch().WithSupplier();
            var organization = directworkOrderFactory.Object.Branch.Organization;
            directworkOrderFactory.WithWorkOrderDetails(organization);
            directworkOrderFactory.Object.WorkOrderDetails[0].Item.ItemType = (int)ItemType.CommonItem;
            Assert.Throws<InvalidDataException>(() => _workOrderService.SaveOrUpdate(directworkOrderFactory.Object));
        }

        [Fact]
        public void Program_Session_Should_Not_Be_Null_For_Program_Item()
        {
            var directworkOrderFactory = _directWorkOrderFactory.Create().WithBranch().WithSupplier();
            var organization = directworkOrderFactory.Object.Branch.Organization;
            directworkOrderFactory.WithWorkOrderDetails(organization);
            directworkOrderFactory.Object.WorkOrderDetails[0].Program = null;
            directworkOrderFactory.Object.WorkOrderDetails[0].Session = null;
            directworkOrderFactory.Object.WorkOrderDetails[0].Item.ItemType = (int)ItemType.ProgramItem;
            Assert.Throws<InvalidDataException>(() => _workOrderService.SaveOrUpdate(directworkOrderFactory.Object));
        }

        [Fact(DisplayName = "Item is program item and purpose is N/A and purpose of program and session not null")]
        public void Combination_1()
        {
            var directworkOrderFactory = _directWorkOrderFactory.Create().WithBranch(true).WithSupplier();
            var organization = directworkOrderFactory.Object.Branch.Organization;
            directworkOrderFactory.WithWorkOrderDetails(organization,ItemType.ProgramItem,null);
            directworkOrderFactory.Object.WorkOrderDetails[0].PurposeProgram = directworkOrderFactory.Object.WorkOrderDetails[0].Program;
            directworkOrderFactory.Object.WorkOrderDetails[0].PurposeSession = directworkOrderFactory.Object.WorkOrderDetails[0].Session;
            Assert.Throws<InvalidDataException>(() => _workOrderService.SaveOrUpdate(directworkOrderFactory.Object));
        }

        [Fact(DisplayName = "Item is program item and purpose is any and purpose of program and session null")]
        public void Combination_2()
        {
            var directworkOrderFactory = _directWorkOrderFactory.Create().WithBranch(true).WithSupplier();
            var organization = directworkOrderFactory.Object.Branch.Organization;
            directworkOrderFactory.WithWorkOrderDetails(organization, ItemType.ProgramItem, (int)Purpose.BrandingPurpose);
            directworkOrderFactory.Object.WorkOrderDetails[0].PurposeProgram = null;
            directworkOrderFactory.Object.WorkOrderDetails[0].PurposeSession = null;
            Assert.Throws<InvalidDataException>(() => _workOrderService.SaveOrUpdate(directworkOrderFactory.Object));
        }

        [Fact(DisplayName = "Item is common item and purpose is first 3 and purpose of program and session null")]
        public void Combination_4()
        {
            var directworkOrderFactory = _directWorkOrderFactory.Create().WithBranch(true).WithSupplier();
            var organization = directworkOrderFactory.Object.Branch.Organization;
            directworkOrderFactory.WithWorkOrderDetails(organization, ItemType.CommonItem);
            directworkOrderFactory.Object.WorkOrderDetails[0].PurposeProgram = null;
            directworkOrderFactory.Object.WorkOrderDetails[0].PurposeSession = null;
            Assert.Throws<InvalidDataException>(() => _workOrderService.SaveOrUpdate(directworkOrderFactory.Object));
        }
        #endregion
    }
}
