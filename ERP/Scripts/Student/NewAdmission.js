﻿
var MESSAGE_SHOW_INTERVAL = 10000;
var NewAdmissionIsPaidProgram = true;

function StudentViewModel() {
    this.Id = 0;
    this.Name = 0;
    this.MobNumber = 0;
    this.Program = 0;
    this.Session = "";
    this.Branch = "";
    this.Campus = "";
    this.BatchDays = "";
    this.BatchTime = "";
    this.Batch = "";
    this.VersionOfStudy = 0,
    this.Gender = 0;
    this.Religion = 0;
    this.CourseViewModels = [];
    this.StudentPayment = {};
    this.StudentViewModel = function () { };
};

function CourseViewModel() {
    this.Id = 0;
    this.Name = 0;
    this.ProgramId = "";
    this.SessionId = "";
    this.IsTaken = false;
    this.maxSubject = 0;
    this.OfficeMinSub = 0;
    this.PublicMinSubject = 0;
    this.OfficeMinPayment = 0;
    this.PublicMinPayment = 0;
    this.IsOfficeCompulsary = false;
    this.IsPublicCompulsary = false;
    this.IsComplementaryCourse = false;
    this.SubjectViewModels = [];
    this.CourseViewModel = function () { };
};

function SubjectViewModel() {
    this.Id = 0;
    this.Name = 0;
    this.Payment = 0;
    this.IsTaken = false;
    this.SubjectViewModel = function () { };
};

function StudentPaymentModel() {

    this.OfferedDiscount = 0;
    this.NetReceivable = 0;
    this.CashBackAmount = 0;
    this.ConsiderationAmount = 0;
    this.CourseFees = 0;
    /*this.CourseSubjectList = [];*/
    this.DiscountAmount = 0;
    this.DueAmount = 0;
    this.NextReceivedDate = "";
    this.PayableAmount = 0;
    this.PaymentMethod = 0;
    this.PaymentType = 0;
    this.ReceiptNo = "";
    this.ReceivedAmount = 0;
    this.ReceivableAmount = 0;
    this.ReceivedDate = "";
    this.ReferrerId = 0;
    this.SpDiscountAmount = 0;
    this.Remarks = "";
    this.SpReferenceNote = "";
    this.StudentPaymentModel = function () { };
}

function CourseSubject() {
    this.Course = new Object();
    this.Subject = new Object();
}

function BoardInfo() {
    this.StudentExamId = 0;
    this.Year = 0;
    this.BoardId = "";
    this.BoardRoll = "";
    this.RegistrationNumber = "";
    this.StudentId = 0;
}

$(document).ready(function () {

    $(document).on("change", "#nextRecDate", function (event) {
        var val = $("#nextRecDate").val();

        console.log(val);
        if (val.length > 0) {
            showErrorMessageBelowCtrl("nextRecDate", "", false);
        }
    });

    $(document).on("input propertychange", "#MobNumber", function (event) {

        var mobile = $("#MobNumber").val().trim();
        mobile = mobileCheck(mobile);

        if (mobile == null) {
            showErrorMessageBelowCtrl("MobNumber", "", false);
            showErrorMessageBelowCtrl("MobNumber", "Please enter valid mobile number", true);
            return false;
        } else {
            $("#MobNumber").val(mobile);
            showErrorMessageBelowCtrl("MobNumber", "", false);
        }
    });

    validateTextField("Name", "Please enter valid name.");
    validateDropDownField("VersionOfStudy", "Please Select Version of Study.");
    validateDropDownField("Gender", "Please Select Gender.");
    validateDropDownField("Religion", "Please Select Religion.");
    validateDropDownField("Program", "Please Select Program.");
    validateDropDownField("Session", "Please Select Session.");
    validateDropDownField("Branch", "Please Select Branch.");
    validateDropDownField("Campus", "Please Select Campus.");
    validateDropDownField("BatchDays", "Please Select Batch Days.");
    validateDropDownField("BatchTime", "Please Select Batch Time.");
    validateDropDownField("Batch", "Please Select Batch.");
    validateTextField("referrerenceNote", "Please enter referance note.");
    validateDropDownField("RefererList", "Please Select Batch.");

    function resetExceptProgram() {
        $("#Session").html("<option selected='selected'>Select Session</option>");
        $("#Branch").html("<option selected='selected'>Select Branch</option>");
        $("#Campus").html("<option selected='selected'>Select Campus</option>");
        $("#Batch").html("<option selected='selected'>Select Batch Name</option>");
        $("#BatchDays").html("<option selected='selected'>Select Batch Days</option>");
        $("#BatchTime").html("<option selected='selected'>Select Batch Time</option>");
    }

    $(document).on('change', '#Program', function () {

        console.log("Program changed");
        var programId = $("#Program").val();
        var sessionId = $("#Session").val();

        $.ajax({
            url: "/Student/Admission/GetSessionByProgram",
            type: "POST",
            data: { programId: programId, sessionId: sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $.unblockUI();
                resetExceptProgram();
                $('.courseDetails').html(data.CourseView);
                $('#Session').html(data.SessionOptions);
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.unblockUI();
                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
            }
        });
    });

    $(document).on('change', '#Session', function () {
        console.log("Session changed");
        var programId = $("#Program").val();
        var sessionId = $("#Session").val();
        var studentId = $("#studentId").val();
        if (studentId == undefined) {
            studentId = 0;
        }

        $.ajax({
            url: "/Student/Admission/GetBranchByProgramSession",
            type: "POST",
            data: { programId: programId, sessionId: sessionId, studentId : studentId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $.unblockUI();
                
                $('.courseDetails').html(data.CourseView);
                $('#Branch').html(data.BrunchOptions);
                $('#studentExamInfoContainer').html(data.ExamBoardView);
                $("#Campus").html("<option selected='selected'>Select Campus</option>");
                $("#Batch").html("<option selected='selected'>Select Batch Name</option>");
                $("#BatchDays").html("<option selected='selected'>Select Batch Days</option>");
                $("#BatchTime").html("<option selected='selected'>Select Batch Time</option>");
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.unblockUI();
                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
            }
        });

    });

    $(document).on('change', '#VersionOfStudy', function () {

        //$("#Program").html("<option selected='selected'>Select Program</option>");

        /*$("#Session").html("<option selected='selected'>Select Session</option>");
        $('.courseDetails').empty();
        $("#Branch").html("<option selected='selected'>Select Branch</option>");
        */

        $("#Campus option:first-child").attr("selected", "selected");
        $("#Batch option:first-child").attr("selected", "selected");
        $("#BatchDays option:first-child").attr("selected", "selected");
        $("#BatchTime option:first-child").attr("selected", "selected");

    });

    $(document).on('change', '#Gender', function () {

        //$("#Program").html("<option selected='selected'>Select Program</option>");

        /*
        $("#Session").html("<option selected='selected'>Select Session</option>");
        $('.courseDetails').empty();
        $("#Branch").html("<option selected='selected'>Select Branch</option>");
        */

        $("#Campus option:first-child").attr("selected", "selected");
        $("#Batch option:first-child").attr("selected", "selected");
        $("#BatchDays option:first-child").attr("selected", "selected");
        $("#BatchTime option:first-child").attr("selected", "selected");

    });
    
    $(document).on('change', '#Branch', function () {

        console.log("Branch changed");

        var branchId = $("#Branch").val();
        var campusId = $("#Campus").val();

        $.ajax({
            url: "/Student/Admission/GetCampusByProgramSessionAndBranch",
            type: "POST",
            data: { branchId: branchId, campusId: campusId },

            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $.unblockUI();
                $('#Campus').html(data.CampusOptions);
                $("#Batch").html("<option selected='selected'>Select Batch Name</option>");
                $("#BatchDays").html("<option selected='selected'>Select Batch Days</option>");
                $("#BatchTime").html("<option selected='selected'>Select Batch Time</option>");
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.unblockUI();
                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
            }
        });

    });

    $(document).on('change', '#Campus', function () {

        console.log("Campus changed");

        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var branchId = $('#Branch').val();
        var campusId = $('#Campus').val();
        var versionStudy = $('#VersionOfStudy').val();
        var gender = $('#Gender').val();
        console.log(versionStudy);
        console.log(gender);
        /*var batchDay = $('#BatchDays').val();
        var batchTime = $('#BatchTime').val();
        var batchId = $('#Batch').val();*/

        if (branchId.length > 0) {
            $.ajax({
                url: "/Student/Admission/GetBatchDayByProgramSessionBranchAndCampus",
                type: "POST",
                data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId, versionStudy: versionStudy, gender: gender },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {

                    $.unblockUI();
                    $('#BatchDays').html(data.BatchDayOptioins);
                    $('#BatchTime').html("<option selected='selected'>Select Batch Time</option>");
                    $('#Batch').html("<option selected='selected'>Select Batch Name</option>");
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    $(document).on('change', '#BatchDays', function () {

        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var branchId = $('#Branch').val();
        var campusId = $('#Campus').val();
        var batchDay = $('#BatchDays').val();
        var versionStudy = $('#VersionOfStudy').val();
        var gender = $('#Gender').val();
        if (branchId.length > 0) {
            $.ajax({
                url: "/Student/Admission/GetBatchTimeByProgramSessionBranchCampusAndBatchDay",
                type: "POST",
                data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId, batchDay: batchDay, versionStudy: versionStudy, gender: gender },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {

                    $.unblockUI();
                    $('#BatchTime').html(data.BatchTimeOptions);
                    $('#Batch').html("<option selected='selected'>Select Batch Name</option>");

                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    $(document).on('change', '#BatchTime', function () {

        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var branchId = $('#Branch').val();
        var campusId = $('#Campus').val();
        var batchDay = $('#BatchDays').val();
        var batchTime = $('#BatchTime').val();
        var versionStudy = $('#VersionOfStudy').val();
        var gender = $('#Gender').val();
        console.log("Ver: " + versionStudy + "  Ge: " + gender);
        if (branchId.length > 0) {
            $.ajax({
                url: "/Student/Admission/GetBatchByProgramSessionBranchCampusAndBatchDayTime",
                type: "POST",
                data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId, batchDay: batchDay, batchTime: batchTime, versionStudy: versionStudy, gender: gender },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    $.unblockUI();
                    $('#Batch').html(data.BatchOptions);
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    function showErrorMessage(msg) {
        $("#messageContainer").append('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><strong>Error!</strong> ' + msg + '.</div>');
        scrollToElement("#messageContainer");
        setTimeout(function () {
            $("#messageContainer").html("");
        }, MESSAGE_SHOW_INTERVAL);
    }

    function disableNewAdmissionForm(flag) {
        $('#newAdmissionForm *').attr('disabled', flag);
        $('#courseDetailsCheckList *').attr('disabled', flag);

        /*complementary check here */

        $(".course-name-check").each(function (index, value) {
            if ($(this).is(':checked') && $(this).attr('disabled') != 'disabled') {
                var comIdstr = $(this).attr('data-complemetarycourses');
                var comIds = comIdstr.split(",");
                console.log(comIdstr);

                for (var i = 0; i < comIds.length; i++) { //complementary course
                    //console.log(comIds[i]);
                    $(".course-name-check").each(function (ind, val) { //total course list
                        var courseIdinner = $(this).attr("data-course-id");
                        if (courseIdinner == comIds[i]) { // check if this complemenary matches with any course if yes then disable this one.
                            //disable this course along it's subject
                            console.log("disable this course along it's subject");
                            $(this).prop('checked', false);
                            $(this).attr("disabled", true);

                            $('.course-' + courseIdinner + '-subjects').each(function (index, value) {
                                $(this).prop('checked', false);
                                $(this).attr("disabled", true);
                            });
                        }
                    });
                }
            }
        });


        $('.selectStudentRadio').attr('disabled', flag);
    }

    function disableBoardInfoForm(flag) {        
        $('#examInfo *').attr('disabled', flag);
    }

    function clearBoardInformation() {
        $("#verificationView").removeClass("alert alert-info");
        $("#verificationView").removeClass("alert alert-danger");
        $("#verificationView").html("");
    }

    function setPaymentFormData(data) {
        $("#totalCourseFee").val(data.TotalCourseFee);
        $("#offeredDiscount").val(data.OfferedDiscount);
        $("#receivableAmount").val(data.ReceivableAmount);
        $("#netReceivable").val(data.ReceivableAmount);
        $("#spDiscountAmount").val("");
        $("#receivedAmount").val("");
        $("#dueAmount").val(data.ReceivableAmount);
    }

    function clearPaymentForm() {
        $("#totalCourseFee").val(0);
        $("#receivableAmount").val(0);
        $("#netReceivable").val(0);
        $("#receivedAmount").val(0);
        $("#totalCourseFee").val(0);
        $("#dueAmount").val(0);
        $("#offeredDiscount").val(0);
        $("#spDiscountAmount").val(0);
    }

    function validateNewAdmissionPaymentForm() {

        var receivedAmountS = $('#receivedAmount').val();
        var receivedAmount = parseInt(receivedAmountS);
        var netReceivable = $("#netReceivable").val();

        if (isNaN(receivedAmount) || ( receivedAmount < 0 && NewAdmissionIsPaidProgram)) {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
            showErrorMessageBelowCtrl("receivedAmount", "Please enter valid amount", true);
            return false;
        }
        else if (receivedAmount > parseInt("0" + netReceivable)) {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
            showErrorMessageBelowCtrl("receivedAmount", "Received amount cannot be grater than Net Receiveable amount", true);
            return false;
        }
        else {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
        }

        var nextRecDate = $('#nextRecDate').val();
        var dueAmount = $('#dueAmount').val();

        if (nextRecDate.length <= 0 && parseInt("0" + dueAmount) > 0) {
            showErrorMessageBelowCtrl("nextRecDate", "", false);
            showErrorMessageBelowCtrl("nextRecDate", "Please select next payment date", true);
            return false;
        } else {
            showErrorMessageBelowCtrl("nextRecDate", "", false);
        }

        var spDiscount = $("#spDiscountAmount").val();
        var spDiscountI = parseInt("0" + spDiscount);

        if (spDiscountI > 0) {

            var referer = $("#RefererList").val();
            var refNote = $("#referrerenceNote").val();

            if (referer.length == 0) {
                showErrorMessageBelowCtrl("RefererList", "", false);
                showErrorMessageBelowCtrl("RefererList", "Please select referer", true);
                return false;
            } else {
                showErrorMessageBelowCtrl("RefererList", "", false);
            }

            if (refNote.length == 0) {
                showErrorMessageBelowCtrl("referrerenceNote", "", false);
                showErrorMessageBelowCtrl("referrerenceNote", "Please select referer note", true);
                return false;
            } else {
                showErrorMessageBelowCtrl("referrerenceNote", "", false);
            }
        }

        return true;
    }

    function validateNewRegistrationForm() {

        var studentName = $('#Name').val();
        var mobileNumber = $('#MobNumber').val();
        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var branchId = $('#Branch').val();
        var campusId = $('#Campus').val();
        var batchDay = $('#BatchDays').val();
        var batchTime = $('#BatchTime').val();
        var batchName = $('#Batch').val();

        var versionOfStudy = $('#VersionOfStudy option:selected').val();
        var gender = $('#Gender  option:selected').val();
        var religion = $('#Religion  option:selected').val();


        if (studentName.length <= 0) {
            showErrorMessageBelowCtrl("Name", "", false);
            showErrorMessageBelowCtrl("Name", "Please type Name", true);
            return false;
        } else {
            showErrorMessageBelowCtrl("Name", "", false);
        }

        if (mobileNumber.length <= 0) {
            showErrorMessageBelowCtrl("MobNumber", "", false);
            showErrorMessageBelowCtrl("MobNumber", "Please type Mobile number", true);
            return false;
        } else {
            showErrorMessageBelowCtrl("MobNumber", "", false);
        }

        var mn = mobileCheck(mobileNumber);
        if (mn == null) {
            showErrorMessageBelowCtrl("MobNumber", "", false);
            showErrorMessageBelowCtrl("MobNumber", "Please type valid Mobile Number", true);
            return false;
        } else {
            $("#MobNumber").val(mn);
            showErrorMessageBelowCtrl("MobNumber", "", false);
        }

        if (versionOfStudy.length <= 0) {
            showErrorMessageBelowCtrl('VersionOfStudy', "", false);
            showErrorMessageBelowCtrl('VersionOfStudy', "Please select Version of study", true);
            return false;
        } else {
            showErrorMessageBelowCtrl('VersionOfStudy', "", false);
        }

        if (gender.length <= 0) {
            showErrorMessageBelowCtrl('Gender', "", false);
            showErrorMessageBelowCtrl('Gender', "Please Select Gender", true);
            return false;
        } else {
            showErrorMessageBelowCtrl('Gender', "", false);
        }

        if (religion.length <= 0) {
            showErrorMessageBelowCtrl('Religion', "", false);
            showErrorMessageBelowCtrl('Religion', "Please Select Religion", true);
            return false;
        } else {
            showErrorMessageBelowCtrl('Religion', "", false);
        }

        if (programId.length <= 0) {
            showErrorMessageBelowCtrl('Program', "", false);
            showErrorMessageBelowCtrl('Program', "Please Select Program", true);
            return false;
        } else {
            showErrorMessageBelowCtrl('Program', "", false);
        }

        if (sessionId.length <= 0) {
            showErrorMessageBelowCtrl('Session', "", false);
            showErrorMessageBelowCtrl('Session', "Please Select Session", true);
            return false;
        } else {
            showErrorMessageBelowCtrl('Session', "", false);
        }

        if (branchId.length <= 0) {
            showErrorMessageBelowCtrl('Branch', "", false);
            showErrorMessageBelowCtrl('Branch', "Please Select Branch", true);
            return false;
        } else {
            showErrorMessageBelowCtrl('Branch', "", false);
        }

        if (campusId.length <= 0) {
            showErrorMessageBelowCtrl('Campus', "", false);
            showErrorMessageBelowCtrl('Campus', "Please Select Campus", true);
            return false;
        } else {
            showErrorMessageBelowCtrl('Campus', "", false);
        }

        if (batchDay.length <= 0) {
            showErrorMessageBelowCtrl('BatchDays', "", false);
            showErrorMessageBelowCtrl('BatchDays', "Please Select Batch Days", true);
            return false;
        } else {
            showErrorMessageBelowCtrl('BatchDays', "", false);
        }
        if (batchTime.length <= 0) {
            showErrorMessageBelowCtrl('BatchTime', "", false);
            showErrorMessageBelowCtrl('BatchTime', "Please Select Batch Time", true);
            return false;
        } else {
            showErrorMessageBelowCtrl('BatchTime', "", false);
        }

        if (batchName.length <= 0) {
            showErrorMessageBelowCtrl('Batch', "", false);
            showErrorMessageBelowCtrl('Batch', "Please Select Batch Name", true);
            return false;
        } else {
            showErrorMessageBelowCtrl('Batch', "", false);
        }

        return true;
    }

    function validateBoardInfo() {
        console.log("-----------Validating Board Info ---------------");
        var hasBoardInfo = $("#hasBoardInfo").val();
        var totalInfo = $("#totalInfo").val();
        console.log(hasBoardInfo);

        var success = false;
        if (hasBoardInfo != null && hasBoardInfo == "YES") {
            var totalInfoI = parseInt(totalInfo);
            console.log("Total: " + totalInfoI);            
            if (totalInfoI > 0) {
                for (var i = 0; i < totalInfoI; i++) {
                    var year = $("#examYear_" + i).val();
                    var board = $("#examBoard_" + i).val();
                    var roll = $("#boardRoll_" + i).val();
                    var registrationNumber = $("#registrationNumber_" + i).val();

                    console.log(year, board, roll, registrationNumber);

                    if (year != undefined && year > 1900 && board != undefined && board != "" && roll != undefined && roll != "" && registrationNumber != undefined && registrationNumber != "") {
                        success = true;
                    }
                }
                if (success == false) {                    
                    $("#boardInfoErrorMessage").html("<div class='alert alert-danger'>Please enter at least one board exam info.<a class='close' data-dismiss='alert'>×</a></div>");
                    scrollToElement('#boardInfoErrorMessage');
                }
                else {                    
                    $("#boardInfoErrorMessage").html("");                    
                }
                clearBoardInfoValidationMessage();
                return success;
            }
        }

        return true;
    }

    function clearBoardInfoValidationMessage() {
        setTimeout(function () {
            $("#boardInfoErrorMessage").html("");            
        }, 5000);
    }

    function validateCourseSelectionAccordingToMinMaxSubjectAndCompulsarySubject() {
        var isclear = true;

        $('.course-name-check').each(function (index, value) {
            var courseId = $(this).attr('data-course-id');
            var maximumSub = $(this).attr('data-maximumsubject');
            var officeMinSub = $(this).attr('data-officeminsub');
            var officeMinPayment = $(this).attr('data-officeminpayment');
            var isOfficecompulsary = $(this).attr('data-isofficecompulsary');
            //alert(isOfficecompulsary);
            var courseName = $(this).attr('data-course-name');
            console.log("courseId -->>" + courseId);
            console.log("maximumSub--->>" + maximumSub);
            console.log("officeMinSub --->>" + officeMinSub);
            console.log("officeMinPayment-->>" + officeMinPayment);
            //console.log("isOfficecompulsary -->>" + isOfficecompulsary);
            console.log("courseName-->>" + courseName);
            console.log("isOfficecompulsary-->>" + isOfficecompulsary);
            var takenSubCount = 0;
            if (isOfficecompulsary == "True") {
                //alert("Compulsary");
                if (courseId > 0) {
                    $('.course-' + courseId + '-subjects').each(function (index1, value1) {
                        if ($(this).is(':checked')) {
                            ++takenSubCount;
                        }
                    });
                    console.log("Taken--->>>" + takenSubCount);
                    if (takenSubCount == 0) {
                        showErrorMessage(courseName + " course is Compulsary." + " please take it ");
                        isclear = false;
                    }
                    if (takenSubCount < officeMinSub) {
                        showErrorMessage("Minium subject for " + courseName + " course is " + officeMinSub + ". please select atleast " + (officeMinSub - takenSubCount) + " subjects more");
                        isclear = false;
                    }
                    if (takenSubCount > maximumSub) {
                        showErrorMessage("Maximum subject for " + courseName + " course is " + maximumSub + ". please remove atleast " + (takenSubCount - maximumSub) + " subjects");
                        isclear = false;
                    }
                    //alert(isclear);
                    if (!isclear) {
                        //alert("Isclear");
                        return isclear;
                    }
                }
            } else {
                //alert("Not Compulsary");
                if ($(this).is(':checked')) {
                    $('.course-' + courseId + '-subjects').each(function (index1, value1) {
                        if ($(this).is(':checked')) {
                            ++takenSubCount;
                        }
                    });
                    console.log("Taken--->>>" + takenSubCount);
                    if (takenSubCount < officeMinSub) {
                        showErrorMessage("Minium subject for " + courseName + " course is " + officeMinSub + ". please select atleast " + (officeMinSub - takenSubCount) + " subjects more");
                        isclear = false;
                    }
                    if (takenSubCount > maximumSub) {
                        showErrorMessage("Maximum subject for " + courseName + " course is " + maximumSub + ". please remove atleast " + (takenSubCount - maximumSub) + " subjects");
                        isclear = false;
                    }
                    //alert(isclear);
                    if (!isclear) {
                        //alert("Isclear");
                        return isclear;
                    }
                }

            }

        });
        //alert(isclear);
        return isclear;
    }

    function validateCourseSubjectSelection() {

        var isAnySubjectTaken = false;

        $('.course-subject-check').each(function (incex, value) {
            var courseId = $(this).attr('data-course-id');
            if (courseId > 0) {
                if ($(this).is(':checked') && $(this).attr('disabled') != 'disabled') {
                    isAnySubjectTaken = true;
                }
            }
        });

        if (!isAnySubjectTaken) {
            showErrorMessage("Please select at least one course");
        }
        return isAnySubjectTaken;
    }

    function scrollToElement(selector) {
        $(document.body).scrollTop($(selector).offset().top - 120);
    }

    function getStudentInfoJson() {

        var student = new StudentViewModel();

        student.Name = $('#Name').val();
        student.MobNumber = $('#MobNumber').val();
        student.Program = $('#Program option:selected').val();
        student.Session = $('#Session option:selected').val();
        student.Branch = $('#Branch option:selected').val();
        student.Campus = $('#Campus option:selected').val();
        student.BatchDays = $('#BatchDays option:selected').val();
        student.BatchTime = $('#BatchTime option:selected').val();
        student.Batch = $('#Batch option:selected').val();

        student.VersionOfStudy = $('#VersionOfStudy option:selected').val();
        student.Gender = $('#Gender option:selected').val();
        student.Religion = $('#Religion option:selected').val();


        $('.course-name-check').each(function (index, value) {

            var courseId = $(this).attr('data-course-id');

            if ($(this).is(':checked')) {

                var courseDetail = new CourseViewModel();
                courseDetail.Name = $(this).attr("data-course-name");
                courseDetail.ProgramId = $(this).attr('data-program-id');
                courseDetail.SessionId = $(this).attr('data-session-id');
                courseDetail.Id = courseId;
                courseDetail.IsTaken = true;
                courseDetail.OfficeMinSub = $(this).attr('data-officeminsub');
                courseDetail.maxSubject = $(this).attr('data-maximumsubject');
                courseDetail.IsOfficeCompulsary = $(this).attr('data-isofficecompulsary');

                $('.course-' + courseId + '-subjects').each(function (index, value) {

                    /*if ($(this).is(':checked') && $(this).attr('disabled') != 'disabled') {*/
                    if ($(this).is(':checked')) {
                        var subject = new SubjectViewModel();
                        subject.Id = $(this).attr('data-course-subject-id');
                        subject.Name = $(this).attr('data-course-subject-name');
                        subject.Payment = $(this).attr('data-course-subject-payment');
                        subject.IsTaken = true;
                        courseDetail.SubjectViewModels.push(subject);
                    }
                });

                student.CourseViewModels.push(courseDetail);
            }

        });

        var studentPayment = new StudentPaymentModel();

        console.log($("#totalCourseFee").val());

        studentPayment.CourseFee = parseInt("0" + $("#totalCourseFee").val());
        studentPayment.OfferedDiscount = parseInt("0" + $("#offeredDiscount").val());
        studentPayment.ReceivableAmount = parseInt("0" + $("#receivableAmount").val());
        studentPayment.SpDiscountAmount = parseInt("0" + $("#spDiscountAmount").val());
        studentPayment.NetReceivable = parseInt("0" + $("#netReceivable").val());

        studentPayment.ReceivedAmount = parseInt("0" + $("#receivedAmount").val());
        studentPayment.DueAmount = parseInt("0" + $("#dueAmount").val());

        studentPayment.ReferrerId = parseInt("0" + $("#RefererList").val());
        studentPayment.Remarks = $("#referrerenceNote").val();
        studentPayment.SpReferenceNote = $("#referrerenceNote").val();
        studentPayment.PaymentMethod = parseInt("0" + $("#PaymentMethods").val());
        studentPayment.NextReceivedDate = $("#nextRecDate").val();

        console.log(studentPayment);
        student.StudentPayment = studentPayment;

        console.log(student);
        var studentObj = JSON.stringify(student);
        console.log(studentObj);
        return studentObj;
    }

    function getBoardInfosJson() {

        var hasBoardInfo = $("#hasBoardInfo").val();
        var totalInfo = $("#totalInfo").val();
        var boardInfoList = [];

        var success = false;
        if (hasBoardInfo != null && hasBoardInfo == "YES") {
            var totalInfoI = parseInt(totalInfo);
            console.log("Total: " + totalInfoI);
            if (totalInfoI > 0) {
                for (var i = 0; i < totalInfoI; i++) {
                    var boardInfo = new BoardInfo();                    
                    var boardId = $("#examBoard_" + i +" :selected").val();
                    boardInfo.StudentExamId = $("#examId_" + i).val();
                    boardInfo.Year = $("#examYear_" + i).val();
                    boardInfo.BoardId = boardId != null ? boardId : 0;
                    boardInfo.BoardRoll = $("#boardRoll_" + i).val();
                    boardInfo.RegistrationNumber = $("#registrationNumber_" + i).val();
                    boardInfoList.push(boardInfo);                    
                }                
            }
        }

        return JSON.stringify(boardInfoList);
    }
    function getAmount(receivable, spDiscount) {
        var val = parseFloat("0" + receivable) - parseFloat("0" + spDiscount);
        return val;
    }

    $(document).on("input propertychange", "#receivedAmount", function (event) {

        var receivableAmount = $('#receivableAmount').val();
        var netReceivable = $('#netReceivable').val();
        var receivedAmount = $('#receivedAmount').val();
        var received = $("#receivedAmount").val();
        var receivedI = parseInt("" + received);
        var netReceivableI = parseInt("" + netReceivable);

        if (receivedI >= 0) {
            $("#newAdmissionPaymentSubmitBtn").removeAttr("disabled");
            $("#admissionPaymentSubmitBtn").removeAttr("disabled");
        } else {
            $("#admissionPaymentSubmitBtn").attr("disabled", "disabled");
            $("#newAdmissionPaymentSubmitBtn").attr("disabled", "disabled");
        }

        if (receivedI >= netReceivable) {
            $('#spDiscountAmount').attr("disabled", "disabled");
        } else {
            $('#spDiscountAmount').removeAttr("disabled");
        }

        if (receivedI > netReceivableI) {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
            showErrorMessageBelowCtrl("receivedAmount", "Received amount can not be grater than Net Receiveable amount", true);
            return false;
        } else {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
        }

        if (receivableAmount.length > 0 && netReceivable.length > 0) {

            if (receivedAmount.length > 0) {
                var dAmount = getAmount(netReceivable, receivedAmount);
                $('#dueAmount').val(dAmount);
            } else {
                $('#dueAmount').val(netReceivable);
            }

            console.log("netReceivable: " + netReceivable);
        }
    });

    $(document).on("input propertychange", "#spDiscountAmount", function (event) {

        var receivableAmount = $('#receivableAmount').val();
        var netReceivable = $('#netReceivable').val();
        var receivedAmount = $('#receivedAmount').val();
        var spDiscountAmount = $('#spDiscountAmount').val();
        var dAmount = 0;

        if (receivableAmount.length > 0 && netReceivable.length > 0) {

            var sDis = parseFloat("0" + spDiscountAmount);
            var receiveableAmountF = parseFloat("0" + receivableAmount);
            var netReceivableI = parseInt("0" + netReceivable);

            if (sDis > 0) {

                $('#RefererList').removeAttr('disabled');
                $('#referrerenceNote').removeAttr('disabled');

                showErrorMessageBelowCtrl('spDiscountAmount', '', false);

                if (sDis >= 0 && sDis <= receiveableAmountF) {
                    netReceivable = getAmount(receivableAmount, spDiscountAmount);
                    $('#netReceivable').val(netReceivable);
                    dAmount = getAmount(netReceivable, receivedAmount);
                    if (dAmount < 0) {
                        showErrorMessageBelowCtrl("dueAmount", "", false);
                        showErrorMessageBelowCtrl("dueAmount", "Due amount can not be negative.", true);
                    } else {
                        showErrorMessageBelowCtrl("dueAmount", "", false);
                    }
                    $('#dueAmount').val(dAmount);
                } else {
                    showErrorMessageBelowCtrl('spDiscountAmount', 'Special discount can not be negative or higher than receiveable amount.', true);
                }

                console.log("SPD " + sDis + ", RECVA " + receiveableAmountF);

                if (sDis == receiveableAmountF) {
                    $("#newAdmissionPaymentSubmitBtn").removeAttr("disabled");
                    $("#admissionPaymentSubmitBtn").removeAttr("disabled");
                }

            } else {
                showErrorMessageBelowCtrl('spDiscountAmount', '', false);
                showErrorMessageBelowCtrl("dueAmount", "", false);
                $('#netReceivable').val(receivableAmount);
                receivedAmount = $('#receivedAmount').val();
                netReceivable = $('#netReceivable').val();
                var due = getAmount(netReceivable, receivedAmount);
                $('#dueAmount').val(due);

                $('#RefererList').prop('disabled', true);
                $('#referrerenceNote').prop('disabled', true);
            }
        }
    });

    $(document).on('click', '#newAdmissionNextBtn', function () {

        if ( validateNewRegistrationForm()
            && validateCourseSubjectSelection()
            && validateCourseSelectionAccordingToMinMaxSubjectAndCompulsarySubject()
            && validateBoardInfo()
            ) {
            var studentInfo = getStudentInfoJson();
            
            $.ajax({
                url: '/Student/Admission/CalculateCourseFee',
                type: 'POST',
                data: {
                    format: 'json',
                    studentViewModelJson: studentInfo,
                },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {

                    $.unblockUI();
                    if (data.IsSuccess == true) {

                        NewAdmissionIsPaidProgram = data.IsPaidProgram;

                        $("#paymentContainer").show();
                        $("#receivedAmount").focus();

                        setPaymentFormData(data);
                        disableNewAdmissionForm(true);
                        disableBoardInfoForm(true);
                        clearBoardInformation();
                        scrollToElement('#paymentContainer');
                        $("#nextBtn").hide();

                    } else {
                        for (var i = 0; i < data.Message.length; i++) {
                            showErrorMessage(data.Message[i].ErrorMessage);
                        }
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function () {
                    $.unblockUI();
                    showErrorMessage('Could not connect with server. Please check internet connection. Or try some time later.');
                }
            });
        }
    });

    $(document).on('click', '#newAdmissionPaymentEditBtn', function () {
        disableNewAdmissionForm(false);
        disableBoardInfoForm(false);
        clearPaymentForm();
        $("#paymentContainer").hide();
        $("#nextBtn").show();
    });

    $(document).on('click', '#newAdmissionPaymentSubmitBtn', function () {

        if (validateNewAdmissionPaymentForm()) {

            console.log("Submited");
            var student = getStudentInfoJson();
            console.log(student);
            var boardInfos = getBoardInfosJson();

            $.ajax({
                url: "/Student/Admission/NewStudentAdmission",
                type: "POST",
                data: { studentObj: student, boardInfos:boardInfos },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    console.log(data);
                    $.unblockUI();

                    if (data.IsSuccess) {
                        window.location = "/Student/Payment/GenerateMoneyReciept?id=" + data.PaymentId;
                    } else {
                        showErrorMessage(data.ErrorMessage);
                        
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    $(document).on('click', '#admissionPaymentSubmitBtn', function () {

        if (validateNewAdmissionPaymentForm()) {
            console.log("newProgramSubmited");

            $("#admissionPaymentSubmitBtn").attr("disabled", "disabled");

            var student = getStudentInfoJson();
            var boardInfos = getBoardInfosJson();
            var studentId = $("#studentId").val();
            var studentVisitedId = $("#studentVisitedId").val();

            $.ajax({
                url: "/Student/Admission/NewProgramRegistration",
                type: "POST",
                data: { studentJson: student, studentId: studentId, studentVisitedId: studentVisitedId, boardInfos:boardInfos },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    console.log(data);
                    $.unblockUI();

                    if (data.IsSuccess) {
                        window.location = "/Student/Payment/GenerateMoneyReciept?id=" + data.PaymentId;
                    } else {
                        showErrorMessage(data.Message);
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    $(document).on('click', '#saveStudent', function () {

        if (validateNewRegistrationForm()) {

            var student = getStudentInfoJson();
            var studentObj = JSON.stringify(student);
            console.log(studentObj);

            $.ajax({
                url: "/Student/Admission/StudentRegistration",
                type: "POST",
                data: { studentObj: studentObj },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    if (data.IsSuccess) {
                        var stdProgramId = data.Message;
                        $.ajax({
                            url: "/Student/Payment/DuePayment",
                            type: "POST",
                            data: { stdProgramId: stdProgramId },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data1) {
                                //alert(data);
                                $('.totalPage').html(data1);
                                $.unblockUI();
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (data1) {
                                bootbox.alert(data1.Message).css('margin-top', (($(window).height() / 4)));
                            }
                        });

                    } else {
                        bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                }
            });
            console.log(student);
        }
        else {
            $(".customMessage").html('<div class="alert alert-danger"> <a class="close" data-dismiss="alert"></a>You need to fillup required field</div>');
        }
    });

    $('.form_datetime').datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: false,
        showMeridian: true,
        initialDate: new Date(),
        startView: 2,
        minView: 2,
        maxView: 4
    });

});