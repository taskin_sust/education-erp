﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.TdsHisotryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "TdsHistory")]
    public class TdsHistorySaveTest : TdsHistoryBaseTest
    {
        [Fact]
        public void TdsHisotry_Save_Sucessfully()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            _tdsHistoryFactory.Create().WithTeamMember(teamMemberObj);
            bool saveTds = _tdsHistoryService.Save(_tdsHistoryFactory.Object);
            Assert.True(saveTds);
        }

        [Fact]
        public void TdsHitory_AddTdsHistory_Save_Sucessfully()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            _tdsHistoryFactory.Create().WithTeamMember(teamMemberObj);
            _tdsHistoryService.Save(_tdsHistoryFactory.Object);
           _tdsHistoryService.AddTdsHistory(teamMemberObj, _tdsHistoryFactory.Object.TdsAmount.ToString(), _tdsHistoryFactory.Object.EffectiveDate.ToString());
            Assert.True(true);
        }


        [Fact]
        public void InvalidDataExpection_TdsHistory_AddHistory_EffectiveDate_Null()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            var objFactory = _tdsHistoryFactory.Create().WithTeamMember(teamMemberObj);
            Assert.Throws<InvalidDataException>(() => _tdsHistoryService.AddTdsHistory(teamMemberObj, objFactory.Object.TdsAmount.ToString(), null));
           
        }

        [Fact]
        public void InvalidDataExpection_TdsHistory_AddHistory_Amount_Null()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            var objFactory = _tdsHistoryFactory.Create().WithTeamMember(teamMemberObj);
            Assert.Throws<InvalidDataException>(() => _tdsHistoryService.AddTdsHistory(teamMemberObj, null, objFactory.Object.EffectiveDate.ToString()));

        }


    }
}
