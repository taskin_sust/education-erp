﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Teachers;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class CourseProgressController : Controller
    {
        #region Logger

        private ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IProgramService _programService;
        private readonly IBranchService _branchService;
        private readonly IBatchService _batchService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ISubjectService _subjectService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IStudentClassAttendanceService _classAttendanceService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ICampusService _campusService;
        private readonly IExamsService _examsService;
        private readonly IExamsStudentMarksService _examsStudentMarksService;
        private readonly IExamsDetailsService _examsDetailsService;
        private List<UserMenu> _userMenu;
        private readonly IOrganizationService _organizationService;
        private readonly ITeacherService _teacherService;
        private readonly IStudentClassAttendanceService _studentClassAttendanceService;
        private readonly ILectureService _lectureService;
        private readonly IStudentExamAttendanceService _studentExamAttendanceService;
        private readonly ICourseSubjectService _courseSubjectService;
        private readonly ICourseProgressService _courseProgressService;
        private readonly CommonHelper _commonHelper;
        private readonly IUserService _userService;
        public CourseProgressController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _teacherService = new TeacherService(session);
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _batchService = new BatchService(session);
                _sessionService = new SessionService(session);
                _classAttendanceService = new StudentClassAttendanceService(session);
                _courseService = new CourseService(session);
                _subjectService = new SubjectService(session);
                _studentProgramService = new StudentProgramService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _campusService = new CampusService(session);
                _examsService = new ExamsService(session);
                _examsStudentMarksService = new ExamsStudentMarksService(session);
                _examsDetailsService = new ExamsDetailsService(session);
                _organizationService = new OrganizationService(session);
                _studentClassAttendanceService = new StudentClassAttendanceService(session);
                _studentExamAttendanceService = new StudentExamAttendanceService(session);
                _lectureService = new LectureService(session);
                _courseSubjectService = new CourseSubjectService(session);
                _courseProgressService = new CourseProgressService(session);
                _userService = new UserService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                if (ex.InnerException != null)
                {
                    _logger.Error("Course Progress Controller Constructor Can't be Initialize  due to " +
                                  ex.InnerException);
                }
                else
                {
                    _logger.Error("Course Progress Controller Constructor Can't be Initialize  due to " + ex.Message);
                }
                Console.WriteLine("{0}", ex.Message);
            }
        }

        #endregion

        #region Operational Function
       
        #region Save Operation
        public ActionResult EnterCourseProgress(string successMessage = "", string errorMessage = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(successMessage))
                {
                    ViewBag.SuccessMessage = successMessage;
                }
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    ViewBag.ErrorMessage = errorMessage;
                }
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                SelectList selectList = new SelectList(organizations, "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.orgList = selectList;
                ViewBag.SelectedLecture = new SelectList(new List<Subject>(), "Id", "Name");
                ViewBag.SelectedSubject = new SelectList(new List<Subject>(), "Id", "Name");
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult EnterCourseProgress(CourseProgressViewModel courseProgressObj,string date)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                SelectList selectList = new SelectList(organizations, "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.orgList = selectList;
                ViewBag.SelectedLecture = new SelectList(new List<Subject>(), "Id", "Name");
                ViewBag.SelectedSubject = new SelectList(new List<Subject>(), "Id", "Name");

                List<Batch> batchList = new List<Batch>();
                if (courseProgressObj.SelectedBatch.Contains(SelectionType.SelelectAll))
                {
                    batchList =
                        _batchService.LoadAuthorizeBatch(_userMenu,
                            _commonHelper.ConvertIdToList(courseProgressObj.OrganizationId),
                            _commonHelper.ConvertIdToList(courseProgressObj.ProgramId),
                            courseProgressObj.SelectedBranch.ToList(),
                            _commonHelper.ConvertIdToList(courseProgressObj.SessionId),
                            courseProgressObj.SelectedCampus.ToList(), courseProgressObj.SelectedBatchDays,
                            courseProgressObj.SelectedBatchTime).ToList();
                }
                else
                {
                    //batchList = _batchService.LoadBatch(courseProgressObj.SelectedBatch.ToList()).ToList(); //remove
                    batchList = _batchService.LoadBatch(null, null, null, null, null, courseProgressObj.SelectedBatch.ToList()).ToList();
                }
                IList<Lecture> lectureList = new List<Lecture>();
                if (courseProgressObj.SelectedLecture.Contains(SelectionType.SelelectAll))
                {
                    lectureList = _lectureService.LoadLecture(
                        _commonHelper.ConvertIdToList(courseProgressObj.ProgramId),
                        _commonHelper.ConvertIdToList(courseProgressObj.SessionId),
                        courseProgressObj.SelectedCourse.ToList(), courseProgressObj.SelectedSubject.ToList(), batchList,
                        false);

                }
                else
                {
                    lectureList = _lectureService.LoadLecture(courseProgressObj.SelectedLecture);
                }
                var success = _courseProgressService.Save(lectureList.ToList(), batchList,Convert.ToDateTime(date));
                if (success)
                {
                    return RedirectToAction("EnterCourseProgress", new{successMessage = "Course progress saved successfully."});
                }
                else
                {
                    return RedirectToAction("EnterCourseProgress", new { errorMessage = WebHelper.CommonErrorMessage });
                }
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                return RedirectToAction("EnterCourseProgress", new { errorMessage = WebHelper.CommonErrorMessage });
            }
           
        }
        #endregion
        
        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isSuccess = _courseProgressService.Delete(id);
                return Json(isSuccess ? new Response(true, "Course progress sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Course progress Delete Fail !"));
            }
        }
        #endregion

        #region Details Operation

        public ActionResult Details(int id)
        {
            var courseProgressObj = _courseProgressService.GetCourseProgress(id);
            if (courseProgressObj != null)
            {
                courseProgressObj.CreateByText = _userService.GetUserNameByAspNetUserId(courseProgressObj.CreateBy);
                courseProgressObj.ModifyByText = _userService.GetUserNameByAspNetUserId(courseProgressObj.ModifyBy);
            }
            return View(courseProgressObj);
        }
        #endregion
        #endregion

        #region Index/Manage
        public ActionResult ManageCourseProgress()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                SelectList selectList = new SelectList(organizations, "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.orgList = selectList;
                ViewBag.SelectedSubject = new SelectList(new List<Subject>(), "Id", "Name");
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult ManageCourseProgress(CourseProgressViewModel courseProgressObj)
        {
            try
            {
                var json = new JavaScriptSerializer().Serialize(courseProgressObj);
                ViewData["courseProgressObj"] = json;
                return View("GenerateCourseProgressReport");
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return View();
        }

        #region Render Data Table
        public JsonResult GenerateCourseProgressReport(int draw, int start, int length, string branch, string campus,
            string batch, string subject, string lecture, string courseProgressObjString)
        {
            var data = new List<object>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var courseProgressObj =
                    new JavaScriptSerializer().Deserialize<CourseProgressViewModel>(
                        courseProgressObjString);

                List<Batch> batchList = new List<Batch>();
                if (courseProgressObj.SelectedBatch.Contains(SelectionType.SelelectAll))
                {
                    batchList =
                        _batchService.LoadAuthorizeBatch(_userMenu,
                            _commonHelper.ConvertIdToList(courseProgressObj.OrganizationId),
                            _commonHelper.ConvertIdToList(courseProgressObj.ProgramId),
                            courseProgressObj.SelectedBranch.ToList(),
                            _commonHelper.ConvertIdToList(courseProgressObj.SessionId),
                            courseProgressObj.SelectedCampus.ToList(), courseProgressObj.SelectedBatchDays,
                            courseProgressObj.SelectedBatchTime).ToList();
                }
                else
                {
                   // batchList = _batchService.LoadBatch(courseProgressObj.SelectedBatch.ToList()).ToList();//remove
                    batchList = _batchService.LoadBatch(null, null, null, null, null, courseProgressObj.SelectedBatch.ToList()).ToList();
                }
                IList<Lecture> lectureList = new List<Lecture>();
                lectureList = _lectureService.LoadLecture(
                    _commonHelper.ConvertIdToList(courseProgressObj.ProgramId),
                    _commonHelper.ConvertIdToList(courseProgressObj.SessionId),
                    courseProgressObj.SelectedCourse.ToList(), courseProgressObj.SelectedSubject.ToList(), batchList,
                    true);
                int recordsTotal = _courseProgressService.GetCourseProgressCount(branch, campus, batch,
                    subject, lecture, lectureList, batchList);
                int recordsFiltered = recordsTotal;
                var courseProgressReportList = new List<CourseProgress>();
                courseProgressReportList = _courseProgressService.LoadCourseProgress(start, length, branch, campus,
                    batch,
                    subject, lecture, lectureList, batchList);
                int sl = start;
                foreach (var courseProgressReport in courseProgressReportList)
                {
                    sl++;
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    str.Add(courseProgressReport.Batch.Branch.Name);
                    str.Add(courseProgressReport.Batch.Campus.Name);
                    str.Add(courseProgressReport.Batch.Name);
                    if (courseProgressReport.Lecture.LectureSettings.CourseSubject != null)
                    {
                        str.Add(courseProgressReport.Lecture.LectureSettings.CourseSubject.Subject.Name);
                    }
                    else
                    {
                        str.Add("Combined");
                    }
                    str.Add(courseProgressReport.Lecture.LectureName);
                    if (courseProgressReport.HeldDate != null)
                    {
                        str.Add(courseProgressReport.HeldDate.ToString("dd-MM-yyyy"));
                    }
                    else
                    {
                        str.Add("");
                    }
                    var detailsLink = LinkGenerator.GetDetailsLink("Details", "CourseProgress", courseProgressReport.Id);
                    var deleteLink = LinkGenerator.GetDeleteLinkForModal(courseProgressReport.Id, courseProgressReport.Lecture.LectureName + " in batch: " + courseProgressReport.Batch.Name);
                    str.Add(detailsLink + deleteLink);
                    data.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data,
                    isSuccess = false
                });
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = data,
                isSuccess = false
            });
        }  
        #endregion
        #endregion
        
        #region Helper Function
        [HttpPost]
        public ActionResult GetLecture(List<long> organizationIds, List<long> programIds, List<long> branchIds,
            List<long> sessionIds, List<long> campusIds, string[] batchDays, string[] batchTimes, List<long> batchIds,
            List<long> courseIds, List<long> subjectIds)
        {
            try
            {
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                List<Batch> batchList = new List<Batch>();
                if (batchIds.Contains(SelectionType.SelelectAll))
                {
                    batchList =
                        _batchService.LoadAuthorizeBatch(_userMenu, organizationIds, programIds, branchIds, sessionIds,
                            campusIds, batchDays, batchTimes).ToList();
                }
                else
                {
                    //batchList = _batchService.LoadBatch(batchIds).ToList();//remove
                    batchList = _batchService.LoadBatch(null, null, null, null, null, batchIds).ToList();//remove
                }
                var lectureList = _lectureService.LoadLecture(programIds, sessionIds, courseIds, subjectIds, batchList,
                    false);

                var lectureSelectList = new SelectList(lectureList.DistinctBy(x => x.Id), "Id", "LectureName").ToList();
                //lectureSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Lecture" });
                return Json(new {lectureList = lectureSelectList, IsSuccess = true});
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult GetSubject(long programId, long sessionId, List<long> courseIdList)
        {
            try
            {
                var courseSubjectList = _courseSubjectService.LoadCourseSubject(programId, sessionId, courseIdList);
                var subjectSelectList = new List<SelectListItem>();
                //subjectSelectList.Add(new SelectListItem() { Text = "Select Subject", Value = "" });
                if (courseSubjectList != null && courseSubjectList.Count > 0)
                {
                    //subjectSelectList.Add(new SelectListItem() {Text = "Select subject", Value = ""});
                    foreach (var courseSubject in courseSubjectList)
                    {
                        if (courseSubject != null)
                        {
                            subjectSelectList.Add(new SelectListItem()
                            {
                                Text = courseSubject.Subject.Name,
                                Value = courseSubject.Subject.Id.ToString()
                            });
                        }

                    }
                    if (courseSubjectList.Contains(null))
                    {
                        subjectSelectList.Add(new SelectListItem() {Text = "Combined", Value = "-1"});
                    }
                    subjectSelectList = subjectSelectList.DistinctBy(x => x.Value).ToList();
                }
                return Json(new {subjectList = subjectSelectList, IsSuccess = true});
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion
    }
} ;