﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.LeaveApplicationTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "LeaveApplication")]
    public class LeaveApplicationLoadTest : LeaveApplicationBaseTest
    {
        #region List Test

        [Fact]
        public void Should_Return_OneOrMOre_LeaveApplication()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            Assert.True(
                _leaveApplicationService.LoadTeamMemberLeaveApplication(0, 10, DateTime.Now, teamMember.Id,
                    leaveList[0].Id.ToString(), _leaveApplicationFactory.Object.LeaveStatus.ToString()).Count > 0);
        }

        [Fact]
        public void Should_Return_Empty_MemberList()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);
            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.LoadMentorLeave(teamMember.Id, DateTime.Now,
                        (LeaveStatus?)_leaveApplicationFactory.Object.LeaveStatus, new List<int>(), 0, 10));
        }

        [Fact]
        public void Should_Return_OneOrMOre_MentorLeaveApplication()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            Assert.True(_leaveApplicationService.LoadMentorLeave(160, null,
                (LeaveStatus?)_leaveApplicationFactory.Object.LeaveStatus, new List<int>() { teamMember.Pin }, 0,
                10).Count > 0);
        }

        [Fact]
        public void Should_Return_Invalid_Branch()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            var usermenu = BuildUserMenu(new List<Organization>() { teamMember.EmploymentHistory[0].Campus.Branch.Organization }, null,
            new List<Branch>() { teamMember.EmploymentHistory[0].Campus.Branch });

            branchFactory.Create().WithOrganization(organizationFactory.Create().Persist().Object).Persist();

            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.LoadHrLeave(usermenu, "",
                        new List<long>() { organizationFactory.Object.Id }, new List<long>() { },
                        new List<long>() { branchFactory.SingleObjectList[0].Id }, null, new List<int>() { 160 }, null, 0, Int32.MaxValue));
        }

        [Fact]
        public void Should_Return_HrLeave_Successfully()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            var usermenu = BuildUserMenu(new List<Organization>() { teamMember.EmploymentHistory[0].Campus.Branch.Organization }, null, new List<Branch>() { teamMember.EmploymentHistory[0].Campus.Branch });
            Assert.True(_leaveApplicationService.LoadHrLeave(usermenu, "",
                 new List<long>() { teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id },
                 new List<long>() { teamMember.EmploymentHistory[0].Department.Id },
                 new List<long>() { teamMember.EmploymentHistory[0].Campus.Branch.Id },
                 new List<long>() { teamMember.EmploymentHistory[0].Campus.Id },
                 new List<int>() { teamMember.Pin }, (LeaveStatus?)_leaveApplicationFactory.Object.LeaveStatus, 0, 10).Count > 0);
        }

        #endregion

        #region Others Test

        [Fact]
        public void Should_Return_OneOrMOre_LeaveApplication_Others()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);
            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            Assert.True(
                _leaveApplicationService.GetTeamMemberLeaveApplicationCount(DateTime.Now, teamMember.Id,
                    leaveList[0].Id.ToString(), _leaveApplicationFactory.Object.LeaveStatus.ToString()) > 0);
        }

        [Fact]
        public void Should_Return_Empty_MemberList_Others()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);
            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.GetMentorLeaveCount(teamMember.Id, DateTime.Now,
                        (LeaveStatus?)_leaveApplicationFactory.Object.LeaveStatus, new List<int>()));
        }

        [Fact]
        public void Should_Return_OneOrMOre_MentorLeaveApplication_Others()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            Assert.True(_leaveApplicationService.GetMentorLeaveCount(160, null,
                (LeaveStatus?)_leaveApplicationFactory.Object.LeaveStatus, new List<int>() { teamMember.Pin }) > 0);
        }

        [Fact]
        public void Should_Return_Invalid_Branch_Others()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            var usermenu = BuildUserMenu(new List<Organization>() { teamMember.EmploymentHistory[0].Campus.Branch.Organization }, null,
            new List<Branch>() { teamMember.EmploymentHistory[0].Campus.Branch });

            branchFactory.Create().WithOrganization(organizationFactory.Create().Persist().Object).Persist();
            Assert.Throws<InvalidDataException>(() =>
                     _leaveApplicationService.GetHrLeaveCount(usermenu, "",
                         new List<long>() { organizationFactory.Object.Id }, new List<long>() { },
                         new List<long>() { branchFactory.SingleObjectList[0].Id }, null, new List<int>() { 160 }, null));

        }

        [Fact]
        public void Should_Return_HrLeave_Successfully_Others()
        {
            var teamMember = PersistTeamMember();
            //var teamMember = PersistTeamMember();
            var leaveList =
              _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                  teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                  teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            var usermenu = BuildUserMenu(new List<Organization>() { teamMember.EmploymentHistory[0].Campus.Branch.Organization }, null, new List<Branch>() { teamMember.EmploymentHistory[0].Campus.Branch });
            Assert.True(_leaveApplicationService.GetHrLeaveCount(usermenu, "",
                 new List<long>() { teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id }, new List<long>() { teamMember.EmploymentHistory[0].Department.Id },
                 new List<long>() { teamMember.EmploymentHistory[0].Campus.Branch.Id },
                  new List<long>() { teamMember.EmploymentHistory[0].Campus.Id }, new List<int>() { teamMember.Pin }, null) > 0);
        }

        #endregion

        #region Single Instance Loading

        [Fact]
        public void Should_Return_InvalidDate_Exception()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            Assert.Throws<InvalidDataException>(() => _leaveApplicationService.GetById(0));
        }

        [Fact]
        public void Should_Return_LeaveApplication_Successfully()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);
            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            Assert.True(_leaveApplicationService.GetById(_leaveApplicationFactory.Object.Id).Id > 0);
        }

        #endregion
    }
}
