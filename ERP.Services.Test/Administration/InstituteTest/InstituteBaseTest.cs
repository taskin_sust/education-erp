﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.InstituteTest
{
    public class InstituteBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal InstituteService _InstituteService;
        internal InstitutionCategoryService _institutionCategoryService;
        internal List<long> IdList = new List<long>();
        private ISession _session;
        internal InstituteFactory _InstituteFactory;
        internal const long OrgId = 1;
        internal DateTime _startTime = DateTime.Now.AddDays(1).AddHours(1);
        internal DateTime _endTime = DateTime.Now.AddDays(1).AddHours(10);
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        #endregion

        public InstituteBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _InstituteFactory = new InstituteFactory(null, _session);
            _InstituteService = new InstituteService(_session);
            _institutionCategoryService=new InstitutionCategoryService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _InstituteFactory.Cleanup();
            base.Dispose();
        }
    }   
}
