﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.BusinessModel.ViewModel.UInventory;

namespace UdvashERP.Areas.UInventory.Models
{
    public class JsonGoodsIssueModelBinder : IModelBinder
    {   
        public object BindModel(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext)
        {
            var model = new RequisitionItemList();

            foreach (string key in HttpContext.Current.Request.Form.AllKeys)
            {
                if (key.Contains("CurrentIssuedQuantity"))
                {
                    model.CurrentIssuedQuantity = string.IsNullOrWhiteSpace(HttpContext.Current.Request.Form[key]) ? "0" : HttpContext.Current.Request.Form[key];
                }
                else if (key.Contains("StockQuantityValue"))
                {
                    var abc = HttpContext.Current.Request.Form[key];
                    model.StockQuantityValue = Convert.ToInt32(HttpContext.Current.Request.Form[key]);
                }
                else if (key.Contains("RequiredQuantityValue"))
                {
                    model.RequiredQuantityValue = Convert.ToInt32(HttpContext.Current.Request.Form[key]);
                }
                else if (key.Contains("PreviousIssueQuantity"))
                {
                    model.PreviousIssueQuantityValue = Convert.ToInt32(HttpContext.Current.Request.Form[key]);
                }
            }
            return model;
        }
    }

    
}