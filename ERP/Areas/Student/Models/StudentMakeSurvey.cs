﻿using System.ComponentModel.DataAnnotations;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Areas.Student.Models
{
    public class StudentMakeSurvey 
    {

        [Required(ErrorMessage = "You need to select Organization")]
        public long OrganizationId { get; set; } 
        [Required(ErrorMessage = "You need to select program")]
        public long ProgramId { get; set; }
        [Required(ErrorMessage = "You need to select session")]
        public long SessionId { get; set; }
        public string[] InformationViewList { get; set; }
        public int SelectedSuveyStatus { get; set; }
        [Required(ErrorMessage = "You need to select branch")]
        public long[] SelectedBranch { get; set; }
        [Required(ErrorMessage = "You need to select campus")]
        public long[] SelectedCampus { get; set; }
        [Required(ErrorMessage = "You need to select batch day")]
        public string[] SelectedBatchDays { get; set; }
        [Required(ErrorMessage = "You need to select batch time")]
        public string[] SelectedBatchTime { get; set; }
        [Required(ErrorMessage = "You need to select batch")]
        public long[] SelectedBatch { get; set; }
        [Required(ErrorMessage = "You need to select course")]
        public long[] SelectedCourse { get; set; }
        public string ProgramRoll { get; set; }
        public string Name { get; set; }
        public int NumberOfRow { get; set; }
    }
}