using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class SalaryHistoryMap : BaseClassMap<SalaryHistory,long> {
        
        public SalaryHistoryMap() {
			Table("HR_SalaryHistory");
			LazyLoad();
			References(x => x.SalaryOrganization).Column("SalaryOrganizationId");
            References(x => x.SalaryDepartment).Column("SalaryDepartmentId");
			References(x => x.SalaryCampus).Column("SalaryCampusId");
			References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.SalaryDesignation).Column("SalaryDesignationId");
            Map(x => x.SalaryPurpose).Column("SalaryPurpose");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.Salary).Column("Salary");
			Map(x => x.BankAmount).Column("BankAmount");
			Map(x => x.CashAmount).Column("CashAmount");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
			//HasMany(x => x.SalaryHistoryLog).KeyColumn("SalaryHistoryId");
        }
    }
}
