﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using NHibernate.Type;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentDao : IBaseDao<Student, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        Student GetLastRegisteredStudent();
        Student GetStudentByRollNumber(string stdRollNumber);
        Student GetByMobileAndPrnNumber(string prnNo, string mobNumber);
        //Student GetByNickNameAndMobile(string nickName, string mobNumber, bool nickNameCaseSensitive = true);
        Student GetByNickNameAndMobile(string nickName, string mobNumber);
        #endregion

        #region List Loading Function

        IList<Student> LoadStudent(long organizationId, List<long> programIdList, long smsType, int? dayBefore = null, bool isRepeat = false, bool isIn = true);

        #endregion

        #region Others Function

        //bool HasDuplicateByNickNameAndMobileNumberAndProgramAndSession(string nickName, string mobileNum, long programId, long sessionId, bool nickNameCaseSensitive = true);
        bool HasDuplicateByNickNameAndMobileNumberAndProgramAndSession(string nickName, string mobileNum, long programId, long sessionId);
        int GetMobileCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus,
            string prnNo, string name, string mobile, int[] smsReciever);

        #endregion



    }

    public class StudentDao : BaseDao<Student, long>, IStudentDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public Student GetLastRegisteredStudent()
        {
            return Session.QueryOver<Student>().Where(x => x.Status == Student.EntityStatus.Active).OrderBy(x => x.Id).Desc.Take(1).SingleOrDefault<Student>();
        }
        public Student GetStudentByRollNumber(string stdRollNumber)
        {
            return Session.QueryOver<Student>().Where(x => x.RegistrationNo == stdRollNumber && x.Status != Student.EntityStatus.Delete).SingleOrDefault<Student>();
        }

        public Student GetByMobileAndPrnNumber(string prnNo, string mobNumber)
        {
            var criteria = Session.CreateCriteria<StudentProgram>();

            criteria.Add(Restrictions.Eq("PrnNo", prnNo));
            criteria.CreateAlias("Student", "std");

            var disjunction = new Disjunction();
            disjunction.Add(Restrictions.Like("std.Mobile", mobNumber, MatchMode.Anywhere));
            disjunction.Add(Restrictions.Like("std.GuardiansMobile1", mobNumber, MatchMode.Anywhere));
            disjunction.Add(Restrictions.Like("std.GuardiansMobile2", mobNumber, MatchMode.Anywhere));

            criteria.Add(disjunction);

            //criteria.Add(
            //    Restrictions.Or(
            //        Restrictions.Or(Restrictions.Eq("std.Mobile", mobNumber),
            //            Restrictions.Eq("std.GuardiansMobile1", mobNumber)),
            //        Restrictions.Or(Restrictions.Eq("std.Mobile", mobNumber),
            //            Restrictions.Eq("std.GuardiansMobile2", mobNumber))));

            //criteria.Add(Restrictions.Or(Restrictions.Eq("std.Mobile", mobNumber), Restrictions.Eq("std.GuardiansMobile1", mobNumber)));

            //criteria.Add(Restrictions.Or(Restrictions.Eq("std.Mobile", mobNumber), Restrictions.Eq("std.GuardiansMobile2", mobNumber)));
            //criteria.CreateAlias("Student", "std", JoinType.LeftOuterJoin).Add(Restrictions.Eq("std.PrnNo", prnNo));
            criteria.SetProjection(Projections.Property("Student"));
            var ss = criteria.UniqueResult<Student>();
            return ss;
        }

        //public Student GetByNickNameAndMobile(string nickName, string mobNumber, bool nickNameCaseSensitive = true)
        public Student GetByNickNameAndMobile(string nickName, string mobNumber)
        {
            var criteria = Session.CreateCriteria<Student>();
            //criteria.Add(nickNameCaseSensitive
            //        ? Restrictions.Eq("NickName", nickName)
            //        : Restrictions.Eq("NickName", nickName).IgnoreCase());
            criteria.Add(Restrictions.Eq("NickName", nickName).IgnoreCase());
            criteria.Add(Restrictions.Like("Mobile", mobNumber, MatchMode.Anywhere));
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Student.EntityStatus.Delete)));
            var ss = criteria.List<Student>().LastOrDefault();
            return ss;
        }

        #endregion

        #region List Loading Function

        public IList<Student> LoadStudent(long organizationId, List<long> programIdList, long smsType, int? dayBefore = null, bool isRepeat = false, bool isIn = true)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("p.Organization", "o").Add(Restrictions.Eq("o.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));

            criteria.Add(Restrictions.Eq("o.Id", organizationId));
            if (programIdList.Any())
            {
                criteria.Add(isIn
                    ? Restrictions.In("p.Id", programIdList)
                    : Restrictions.Not(Restrictions.In("p.Id", programIdList)));
            }


            if (smsType == 8)
            {

                var monthProjection = Projections.SqlFunction("month", NHibernateUtil.Int32, Projections.Property("st.DateOfBirth"));
                var dayProjection = Projections.SqlFunction("day", NHibernateUtil.Int32, Projections.Property("st.DateOfBirth"));
                DateTime today = DateTime.Today;
                if (isRepeat)
                {
                    var disjunction = Restrictions.Disjunction();
                    if (dayBefore != null)
                    {
                        for (int i = 0; i <= dayBefore; i++)
                        {
                            today = today.AddDays(1);
                            int month = today.Month;
                            int day = today.Day;
                            disjunction.Add(Expression.Eq(monthProjection, month) && Expression.Eq(dayProjection, day));
                        }
                        criteria.Add(disjunction);
                    }
                    else
                    {
                        int month = today.Month;
                        int day = today.Day;
                        disjunction.Add(Expression.Eq(monthProjection, month) && Expression.Eq(dayProjection, day));
                        criteria.Add(disjunction);
                    }
                }
                else
                {
                    today = today.AddDays((int)dayBefore);
                    int month = today.Month;
                    int day = today.Day;
                    criteria.Add(Expression.Eq(monthProjection, month));
                    criteria.Add(Expression.Eq(dayProjection, day));
                }
            }
            else if (smsType == 7)
            {
                criteria.Add(Restrictions.Gt("DueAmount", Convert.ToDecimal(0)));
                DateTime today = DateTime.Today;
                DateTime fromDate = DateTime.Today;
                DateTime toDate = today.AddDays(Convert.ToDouble(dayBefore));
                criteria.CreateCriteria("StudentPayments", "sp").Add(Restrictions.Gt("sp.DueAmount", Convert.ToDecimal(0)));
                criteria.Add(Restrictions.Ge("sp.NextReceivedDate", fromDate));
                criteria.Add(Restrictions.Le("sp.NextReceivedDate", toDate));
            }

            criteria.SetProjection(Projections.Distinct(Projections.Property("Student")));
            var students = criteria.List<Student>();
            return students;
        }

        #endregion

        #region Others Function

        //public bool HasDuplicateByNickNameAndMobileNumberAndProgramAndSession(string nickName, string mobileNum, long programId, long sessionId, bool nickNameCaseSensitive = true)
        public bool HasDuplicateByNickNameAndMobileNumberAndProgramAndSession(string nickName, string mobileNum, long programId, long sessionId)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Not(Restrictions.Eq("Status", StudentProgram.EntityStatus.Delete)));
            criteria.CreateCriteria("Batch", "b");
            criteria.CreateAlias("Student", "std");
            criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            //criteria.Add(nickNameCaseSensitive
            //        ? Restrictions.Eq("std.NickName", nickName)
            //        : Restrictions.Eq("std.NickName", nickName).IgnoreCase());
            criteria.Add(Restrictions.Eq("std.NickName", nickName).IgnoreCase());
            criteria.Add(Restrictions.Eq("std.Mobile", mobileNum));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));
            criteria.SetProjection(Projections.Property("Student"));
            var list = criteria.List<Student>();
            if (list.Count > 0)
                return true;
            return false;
        }

        public int GetMobileCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus, string prnNo, string name, string mobile, int[] smsReciever)
        {
            string query = "with MobileNoCte as ( SELECT   studentId, GuardiansMobile1,   GuardiansMobile2,   Mobile" +
                  " from StudentProgram sp" +
                  " inner join Program p on p.Id = sp.ProgramId" +
                  " inner join Student st on st.Id = sp.StudentId" +
                  " inner join Batch b on b.Id = sp.BatchId" +
                  " inner join Branch br on br.Id = b.BranchId" +
                  " inner join [Session] s on s.Id = b.SessionId" +
                  " inner join Campus c on c.Id = b.CampusId";
            string whereClause = " Where " +
                                 " st.Status = 1 and" +
                                 " p.Status = 1 and" +
                                 " b.Status = 1 and" +
                                 " br.Status = 1 and" +
                                 " s.Status = 1 and" +
                                 " c.Status = 1 ";
            switch (selectedStatus)
            {
                case 1:
                    whereClause += " and sp.Status=" + StudentProgram.EntityStatus.Active;
                    break;
                case 2:
                    whereClause += " and sp.Status=" + StudentProgram.EntityStatus.Inactive;
                    break;
            }


            if (!(Array.Exists(courseId, item => item == 0)))
            {
                whereClause += " and exists(" +
                          " select 1 from StudentCourseDetails scd inner join CourseSubject cs on scd.CourseSubjectId=cs.Id" +
                          " where cs.Status = 1 and scd.StudentProgramId=sp.id";
                whereClause += " and cs.courseId in (" + string.Join(",", courseId) + ") ";
                whereClause += ") ";
            }
            whereClause += " and s.Id = " + sessionId + " and p.Id = " + programId;
            if (authorizedProgramLists != null)
            {
                whereClause += " and p.Id in(" + string.Join(",", authorizedProgramLists.ToArray()) + ") ";
            }

            if (authorizedBranchLists != null)
            {
                whereClause += " and br.Id in(" + string.Join(",", authorizedBranchLists.ToArray()) + ") ";
            }

            if (!(Array.Exists(branchId, item => item == 0)))
            {
                whereClause += " and br.Id in(" + string.Join(",", branchId) + ") ";
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                whereClause += " and c.Id in(" + string.Join(",", campusId) + ") ";
            }

            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {

                whereClause += " and b.Days in ('" + string.Join("','", batchDays) + "')";
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                whereClause += " and (" + btQ + ")";
            }

            if (!(Array.Exists(batchId, item => item == 0)))
            {
                whereClause += " and b.Id in (" + string.Join(",", batchId) + ")";
            }
            if (!String.IsNullOrEmpty(startDate))
            {
                string df = startDate + " 00:00:00.000";
                DateTime dFrom = Convert.ToDateTime(df);
                whereClause += " and sp.CreationDate >= '" + dFrom + "'";
            }
            if (!String.IsNullOrEmpty(endDate))
            {
                DateTime dTo = Convert.ToDateTime(endDate + " 23:59:59.000");
                whereClause += " and sp.CreationDate <= '" + dTo + "'";
            }
            //if (startDate != null)
            //{
            //    whereClause += " and sp.CreationDate >= '" + startDate + "'";
            //}
            //if (endDate != null)
            //{
            //    whereClause += " and sp.CreationDate <= '" + endDate + "'";
            //}

            if (version != SelectionType.SelelectAll)
            {
                whereClause += " and sp.VersionOfStudy = " + version;
            }
            if (gender != SelectionType.SelelectAll)
            {
                whereClause += " and st.Gender = " + gender;
            }
            if (!(Array.Exists(religion, item => item == 0)))
            {
                whereClause += " and st.Religion in (" + string.Join(",", religion) + ")";
            }

            if (!String.IsNullOrEmpty(prnNo))
            {
                whereClause += " and sp.PrnNo like '%" + prnNo + "%'";
            }
            if (!String.IsNullOrEmpty(name))
            {
                whereClause += " and st.NickName like '%" + name + "%'";
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                whereClause += " and st.Mobile like '%" + mobile + "%'";
            }
            query += whereClause + " )";
            string outerQuery = "";
            string innerQuery = "";
            if (smsReciever.Contains(1))
            {
                innerQuery += "Mobile,";
            }
            if (smsReciever.Contains(2))
            {
                innerQuery += "GuardiansMobile1,";
            }
            if (smsReciever.Contains(3))
            {
                innerQuery += "GuardiansMobile2";
            }
            innerQuery = innerQuery.Trim(',', ' ');
            outerQuery += @"SELECT SUM(Mobilecount)
	                                FROM (
	                                SELECT COUNT(DISTINCT MobileNos) Mobilecount
	                                FROM MobileNoCte
	                                UNPIVOT(MobileNos FOR Receiver IN ( ";
            outerQuery += innerQuery;
            outerQuery += @")) unpiv
	                            GROUP BY studentId
	                            ) x";
            query += outerQuery;
            IQuery iQuery = Session.CreateSQLQuery(query);
            //iQuery.SetResultTransformer(Transformers.AliasToBean<MobileCountDto>());
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function

        #endregion

    }
}