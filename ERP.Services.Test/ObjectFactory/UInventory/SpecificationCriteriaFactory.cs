using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class SpecificationCriteriaFactory : ObjectFactoryBase<SpecificationCriteria>
   {

       private readonly ISpecificationCriteriaService _specificationCriteriaService;

       public SpecificationCriteriaFactory(IObjectFactoryBase caller, ISession session): base(caller, session){

       _specificationCriteriaService = new SpecificationCriteriaService(session);

       }

       #region Create 

       public SpecificationCriteriaFactory Create(){

       Object = null;
       SingleObjectList.Add(Object);
       return this;
       }

       public SpecificationCriteriaFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public SpecificationCriteriaFactory Persist()       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _specificationCriteriaService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x =>{if (x.Id == 0)_specificationCriteriaService.SaveOrUpdate(x);});

          }
          return this;
       }

       public SpecificationCriteriaFactory CreateMore(int num){

       for (int i = 0; i < num; i++){
       var obj = new SpecificationCriteria(){};
       ObjectList.Add(obj);}
       return this;}

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public SpecificationCriteriaFactory CleanUp()
       {

          DeleteAll();
          return this;
       }

       #endregion

    }
}
