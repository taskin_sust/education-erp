﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text.pdf.qrcode;
using log4net;
using NHibernate.Criterion;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class ManagementTimeController : Controller
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IUserService _userService;
        private readonly IManagementTimeService _managementTimeService;
        private readonly ITeamMemberService _teamMemberService;
        private List<UserMenu> _userMenu;
        public ManagementTimeController()
        {
            var nHSession = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(nHSession);
            _commonHelper = new CommonHelper();
            _userService = new UserService(nHSession);
            _managementTimeService = new ManagementTimeService(nHSession);
            _teamMemberService = new TeamMemberService(nHSession);
        }

        #endregion

        #region Index/Manage

        public ActionResult Index(string msg = "")
        {
            try
            {
                if (TempData["SuccessMessageMT"] != null && !String.IsNullOrEmpty(msg))
                {
                    ViewBag.SuccessMessage = TempData["SuccessMessageMT"];
                }
                ViewBag.PageSize = Constants.PageSize;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        #region Render Data Table
        public JsonResult ManagementTimeDataTable(int draw, int start, int length, string pinStringList, string workDateFrom, string workDateTo)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "WorkDate";
                            break;
                        case "1":
                            orderBy = "teamMember.Pin";
                            break;
                        case "3":
                            orderBy = "Time";
                            break;
                        default:
                            orderBy = "teamMember.Pin";
                            break;
                    }
                }

                #endregion

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime? workingDateFrom = null;
                DateTime? workingDateTo = null;
                DateTime date;
                if (!String.IsNullOrEmpty(workDateFrom) && DateTime.TryParse(workDateFrom, out date))
                    workingDateFrom = date;
                if (!String.IsNullOrEmpty(workDateTo) && DateTime.TryParse(workDateTo, out date))
                    workingDateTo = date;

                long currentUserId = _userService.GetCurrentUserId();

                recordsTotal = _managementTimeService.GetManagementTimeCount(null, orderBy, orderDir, pinStringList, workingDateFrom, workingDateTo, currentUserId);
                recordsFiltered = recordsTotal;
                IList<ManagementTime> managementTimeList = _managementTimeService.LoadManagementTimeList(null, start, length, orderBy, orderDir.ToUpper(), pinStringList, workingDateFrom, workingDateTo, currentUserId).ToList();
                decimal totalTime = 0;
                foreach (ManagementTime managementTime in managementTimeList)
                {
                    TeamMember teamMember = managementTime.TeamMember;
                    string name = "";
                    if (teamMember != null)
                    {
                        name = teamMember.Name + " (" + teamMember.Pin+")";
                    }
                    var str = new List<string> ();
                    str.Add(managementTime.WorkDate.ToString("yyyy-MM-dd"));
                    str.Add(name);
                    str.Add(managementTime.Time.ToString());
                    str.Add(managementTime.Remarks);
                    str.Add(LinkGenerator.GetEditLink("Edit", "ManagementTime", managementTime.Id) + LinkGenerator.GetDeleteLinkForModal(managementTime.Id, name));
                    getData.Add(str);
                    totalTime += managementTime.Time;
                }
                if (managementTimeList.Any())
                {
                    var summary = new List<string>();
                    summary.Add("<strong>Total</strong>");
                    summary.Add("");
                    summary.Add("<strong>" + String.Format("{0:0.##}", totalTime) + "<strong>");
                    summary.Add("");
                    summary.Add("");
                    getData.Add(summary);
                }
               
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = true
            });
        }

        #endregion

        public ActionResult AllManagementTime()
        {
            try
            {
                ViewBag.PageSize = Constants.PageSize;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        #region Render Data Table
        public JsonResult AllManagementTimeDataTable(int draw, int start, int length, string pinStringList, string workDateFrom, string workDateTo)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "WorkDate";
                            break;
                        case "1":
                            orderBy = "teamMember.Pin";
                            break;
                        case "3":
                            orderBy = "Time";
                            break;
                        default:
                            orderBy = "teamMember.Pin";
                            break;
                    }
                }

                #endregion
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime? workingDateFrom = null;
                DateTime? workingDateTo = null;
                DateTime date;
                if (!String.IsNullOrEmpty(workDateFrom) && DateTime.TryParse(workDateFrom, out date))
                    workingDateFrom = date;
                if (!String.IsNullOrEmpty(workDateTo) && DateTime.TryParse(workDateTo, out date))
                    workingDateTo = date;

                recordsTotal = _managementTimeService.GetManagementTimeCount(_userMenu, orderBy, orderDir, pinStringList, workingDateFrom, workingDateTo);
                recordsFiltered = recordsTotal;
                IList<ManagementTime> managementTimeList = _managementTimeService.LoadManagementTimeList(_userMenu, start, length, orderBy, orderDir.ToUpper(), pinStringList, workingDateFrom, workingDateTo).ToList();
                decimal totalTime = 0;
                foreach (ManagementTime managementTime in managementTimeList)
                {
                    TeamMember teamMember = managementTime.TeamMember;
                    string name = "";
                    string managerName = "";
                    TeamMember managerTeamMember = _teamMemberService.GetTeamMemberByAspNetUserId(managementTime.CreateBy);
                    if (teamMember != null)
                    {
                        name = teamMember.Name + " (" + teamMember.Pin + ")";
                    }
                    if (managerTeamMember != null)
                    {
                        managerName = managerTeamMember.Name + " (" + managerTeamMember.Pin + ")";
                    }
                    else
                    {
                        AspNetUser aspnetUser = _userService.LoadbyAspNetUserId(managementTime.CreateBy);
                        if (aspnetUser != null)
                            managerName = aspnetUser.Email;
                    }
                    var str = new List<string>();
                    str.Add(managementTime.WorkDate.ToString("yyyy-MM-dd"));
                    str.Add(name);
                    str.Add(managerName.ToString());
                    str.Add(managementTime.Time.ToString());
                    str.Add(managementTime.Remarks);
                    getData.Add(str);
                    totalTime += managementTime.Time;
                }
                if (managementTimeList.Any())
                {
                    var summary = new List<string>();
                    summary.Add("<strong>Total</strong>");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("<strong>" + String.Format("{0:0.##}", totalTime) + "<strong>");
                    summary.Add("");
                    getData.Add(summary);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = true
            });
        }

        #endregion

        #endregion

        #region Operational Function

        public ActionResult Create()
        {
            try
            {
         
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public JsonResult Create(ManagementTimeViewModel managementTimeViewModel)
        {
            string message = "";
            bool successState = false;
            try
            {
                TeamMember teamMember = _teamMemberService.GetByPin(managementTimeViewModel.TeamMemberPin);
                if(teamMember == null)
                    throw new InvalidDataException("No Team Member Found!");
                ManagementTime managementTime = new ManagementTime
                {
                    TeamMember = teamMember,
                    WorkDate = managementTimeViewModel.WorkDate,
                    Time = managementTimeViewModel.Time,
                    Remarks = managementTimeViewModel.Remarks
                };
                _managementTimeService.SaveManagementTime(managementTime);
                message = "Successfully Save";
                successState = true;
                TempData["SuccessMessageMT"] = message;
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        public ActionResult Edit(long id)
        {
            string departmentName = "";
            string designationName = "";
            string organizationShortName = "";
            string name = "";
            ManagementTimeViewModel mtviewModel = new ManagementTimeViewModel();
            try
            {
                long currentUserId = _userService.GetCurrentUserId();
                ManagementTime managementTime = _managementTimeService.GetManagementTime(id);
                
                if(managementTime == null)
                    throw new InvalidDataException("No Management Time entry found!");
                TeamMember teamMember = managementTime.TeamMember;
                if(teamMember == null)
                    throw new InvalidDataException("Invalid Management Time entry found!");
                if (managementTime.CreateBy != currentUserId)
                    throw new InvalidDataException("This is not your Management Time!");
                mtviewModel = new ManagementTimeViewModel
                {
                    TeamMemberPin =  managementTime.TeamMember.Pin,
                    WorkDate = managementTime.WorkDate,
                    Time = managementTime.Time,
                    Remarks = managementTime.Remarks
                };
                Department department = _teamMemberService.GetTeamMemberDepartment(managementTime.WorkDate.ToString("yyyy-MM-dd"),teamMember.Id);
                Designation designation = _teamMemberService.GetTeamMemberDesignation(managementTime.WorkDate.ToString("yyyy-MM-dd"), teamMember.Id);
               name = teamMember.Name;
               departmentName = (department != null) ? department.Name : "";
               designationName = (designation != null) ? designation.Name : "";
               organizationShortName = (department != null) ? department.Organization.ShortName : "";

            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            ViewBag.Name = name;
            ViewBag.DepartmentName = departmentName;
            ViewBag.DesignationName = designationName;
            ViewBag.OrganizationShortName = organizationShortName;
            return View(mtviewModel);
        }

        [HttpPost]
        public JsonResult Edit(ManagementTimeViewModel managementTimeViewModel)
        {
            string message = "";
            bool successState = false;
            try
            {
                TeamMember teamMember = _teamMemberService.GetByPin(managementTimeViewModel.TeamMemberPin);
                if (teamMember == null)
                    throw new InvalidDataException("No Team Member Found!");
                ManagementTime managementTime = new ManagementTime
                {
                    Id = managementTimeViewModel.Id,
                    TeamMember = teamMember,
                    WorkDate = managementTimeViewModel.WorkDate,
                    Time = managementTimeViewModel.Time,
                    Remarks = managementTimeViewModel.Remarks
                };
                _managementTimeService.UpdateManagementTime(managementTime);
                message = "Successfully Update";
                successState = true;
                TempData["SuccessMessageMT"] = message;
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            string message = "";
            bool successState = false;
            try
            {
                _managementTimeService.DeleteManagementTime(id);
                message = "Successfully Deleted";
                successState = true;
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        #endregion

        #region Others function
        #endregion

        #region Helper function
        #endregion

    }
}
