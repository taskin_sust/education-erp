﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherClassOpeningBalanceMap : BaseClassMap<TeacherClassOpeningBalance, long>
    {
        public TeacherClassOpeningBalanceMap()
        {
            Table("TeacherClassOpeningBalance");
            LazyLoad();
            References(x => x.Organization).Column("OrganizationId");
            References(x => x.Teacher).Column("TeacherId");
            Map(x => x.OpeningDate).Column("OpeningDate");
            Map(x => x.TotalClass).Column("TotalClass");
        }
    }
}
