﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class SmsMaskController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly IOrganizationService _organizationService;
        private readonly ISmsMaskService _smsMaskService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserService _userService;
        private List<UserMenu> _userMenu;
        public SmsMaskController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(nHsession);
            _smsMaskService = new SmsMaskService(nHsession);
            _userService = new UserService(nHsession);
        }
        #endregion

        #region Index/Manage Page

        public ActionResult Index()
        {
            return RedirectToAction("ManageSmsMask");
        }

        public ActionResult ManageSmsMask()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //ViewBag.Organization = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.StatusText = _commonHelper.GetStatus();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        #region Render Data Table
        public ActionResult SmsMaskList(int draw, int start, int length, string organization, string maskName, string status)
        {
            if (!Request.IsAjaxRequest()) return HttpNotFound();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                #region OrderBy and Direction
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "organization.Name";
                            break;
                        case "1":
                            orderBy = "Name";
                            break;
                        case "2":
                            orderBy = "Rank";
                            break;
                        case "3":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                #endregion
                var smsMask = new SmsMask();
                int recordsTotal = _smsMaskService.GetSmsMaskCount(orderBy, orderDir.ToUpper(), _userMenu, organization, maskName, status);
                long recordsFiltered = recordsTotal;
                List<SmsMask> smsMaskList = _smsMaskService.LoadSmsMask(start, length, orderBy, orderDir.ToUpper(), _userMenu, organization, maskName, status).ToList();
                var maxMinRank = new int[2];
                maxMinRank[0] = _smsMaskService.GetMinRank(smsMask);
                maxMinRank[1] = _smsMaskService.GetMaxRank(smsMask);

                var data = new List<object>();
                foreach (var smsMaskObj in smsMaskList)
                {
                    var str = new List<string>();
                    str.Add(smsMaskObj.Organization != null ? smsMaskObj.Organization.ShortName : "");
                    str.Add(smsMaskObj.Name);
                    string rankbtn = smsMaskObj.Rank.ToString();
                    if (smsMaskObj.Rank > maxMinRank[0])
                    {
                        rankbtn += "<a href='#'  id='" + smsMaskObj.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    }
                    if (smsMaskObj.Rank < maxMinRank[1])
                    {
                        rankbtn += "<a href='#'  id='" + smsMaskObj.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    }
                    str.Add(rankbtn);
                    str.Add(_commonHelper.GetSatus(smsMaskObj.Status.ToString()));
                    str.Add("<a href='" + Url.Action("Details", "SmsMask") + "?id=" + smsMaskObj.Id + "' data-id='" + smsMaskObj.Id + "' class='glyphicon glyphicon-th-list'></a>" +
                            "&nbsp;&nbsp; <a href='" + Url.Action("Edit", "SmsMask") + "?id=" + smsMaskObj.Id + "' data-id='" + smsMaskObj.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; " +
                            "<a href='#' id='" + smsMaskObj.Id + "' data-name = '" + smsMaskObj.Name + "' data-id='" + smsMaskObj.Id + "' class='glyphicon glyphicon-trash'></a>");
                    data.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });

            }
            catch (Exception ex)
            {
                var data = new List<object>();
                _logger.Error(ex);
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });
            }
        }
        #endregion

        #endregion

        #region Operational Function

        #region Save operation
        public ActionResult Create()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //var organizations = _organizationService.LoadOrganization(Organization.EntityStatus.Active);
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                ViewBag.organizationList = new SelectList(organizations.ToList(), "Id", "ShortName");
                if (TempData["SmsMaskAddSuccess"] != null)
                {
                    ViewBag.SuccessMessage = TempData["SmsMaskAddSuccess"];
                }
            }
            catch (Exception ex)
            {
                ViewBag.organizationList = new SelectList(new List<Organization>(), "Id", "ShortName");
                _logger.Error(ex);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Create(SmsMask smsMask)
        {
            ViewBag.organizationList = new SelectList(new List<Organization>(), "Id", "ShortName");
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //var organizations = _organizationService.LoadOrganization(Organization.EntityStatus.Active);
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                ViewBag.organizationList = new SelectList(organizations.ToList(), "Id", "ShortName");
                bool isSuccess = _smsMaskService.Save(smsMask);
                if (isSuccess)
                {
                    TempData["SmsMaskAddSuccess"] = "Mask Name Add Successfully";
                    return RedirectToAction("Create");
                }
                ViewBag.ErrorMessage = "Mask Name  Add Failed";
            }
            catch (DuplicateEntryException deex)
            {
                ViewBag.ErrorMessage = deex.Message;
                _logger.Error(deex);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }
        #endregion

        #region Eit operation
        [HttpGet]
        public ActionResult Edit(long id)
        {
            ViewBag.organizationList = new SelectList(new List<Organization>(), "Id", "ShortName");
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var smsMask = _smsMaskService.LoadById(id);
                //ViewBag.organizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(), "Id", "ShortName");
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName");
                ViewBag.Status = _commonHelper.GetStatus();
                ViewBag.SelectedOrganization = smsMask.Organization != null ? smsMask.Organization.Id.ToString() : "";
                return View(smsMask);
            }
            catch (Exception ex)
            {
                var smsMask = _smsMaskService.LoadById(id);
                var statusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                ViewBag.Status = statusList;
                _logger.Error(ex);
                return View(smsMask);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(long id, SmsMask smsMask, int status)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                bool success = _smsMaskService.Update(id, smsMask, status);
                if (success)
                {
                    ViewBag.SuccessMessage = "Mask Name Successfully Updated.";
                }
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error("Edit", ex);
            }

            var smsMaskObj = _smsMaskService.LoadById(id);
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName", smsMaskObj.Organization.Id.ToString());
            //ViewBag.organizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(), "Id", "ShortName", smsMaskObj.Organization.Id.ToString());
            ViewBag.Status = _commonHelper.GetStatus();
            return View(smsMaskObj);
        }
        #endregion

        #region Details operation
        public ActionResult Details(long id)
        {
            var smsMask = new SmsMask();
            try
            {
                smsMask = _smsMaskService.LoadById(id);
                if (smsMask != null)
                {
                    smsMask.CreateByText = _userService.GetUserNameByAspNetUserId(smsMask.CreateBy);
                    smsMask.ModifyByText = _userService.GetUserNameByAspNetUserId(smsMask.ModifyBy);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            if (smsMask == null)
            {
                return HttpNotFound();
            }
            return View(smsMask);
        }
        #endregion

        #region Delete operation
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                //var smsMaskobj = _smsMaskService.LoadById(id);
                _smsMaskService.Delete(id);
                return Json(new Response(true, "Mask Name Delete Successfully !"));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Mask Name Delete Fail !"));
            }
        }
        #endregion

        #region Rank operation
        public JsonResult RankChange(long id, int current, string action)
        {
            bool isSuccess = _smsMaskService.UpdateRank(id, current, action);
            if (isSuccess)
            {
                return Json("Rank Changes successfully!");
            }
            return Json("Rank Changes, Something Wrong Occured!!");
        }
        #endregion
        
        #endregion

        #region Helper Function
        #endregion

    }
}
