using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class SurveyQuestionAnswerMap : BaseClassMap<SurveyQuestionAnswer, long>
    {

        public SurveyQuestionAnswerMap()
        {
            Table("SurveyQuestionAnswer");
			LazyLoad();
            Map(x => x.Answer).Column("Answer");
            References(x => x.SurveyQuestion).Column("SurveyQuestionId");
            HasMany(x => x.StudentSurveyAnswers).KeyColumn("SurveyQuestionAnswerId").Cascade.AllDeleteOrphan().Inverse();            
        }
    }
}
