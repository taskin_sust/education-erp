using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class SupplierMap : BaseClassMap<Supplier, long>
    {
        public SupplierMap()
        {
            Table("UINV_Supplier");
            LazyLoad();
            Map(x => x.Name).Column("Name").Not.Nullable();
            Map(x => x.Rank).Column("Rank").Not.Nullable();
            Map(x => x.OwnerName).Column("OwnerName");
            Map(x => x.RegistrationAddress).Column("RegistrationAddress");
            Map(x => x.ContactPerson).Column("ContactPerson");
            Map(x => x.MobileNo).Column("MobileNo");
            Map(x => x.TelephoneNo).Column("TelephoneNo");
            Map(x => x.Email).Column("Email");
            Map(x => x.WebsiteAdress).Column("WebsiteAdress");
            Map(x => x.TlnInc).Column("TLNINC");
            Map(x => x.VatRegistrationNo).Column("VATRegistrationNo");
            Map(x => x.Tin).Column("TIN");
            Map(x => x.VatCircle).Column("VATCircle");
            Map(x => x.TaxCircle).Column("TaxCircle");
            Map(x => x.VatDivision).Column("VATDivision");
            Map(x => x.TaxZone).Column("TaxZone");
            HasMany(x => x.GoodsReceiveList).KeyColumn("SupplierId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SupplierBankDetails).KeyColumn("SupplierId").Cascade.AllDeleteOrphan().Inverse();
            //HasMany(x => x.SupplierItems).KeyColumn("SupplierId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SupplierPriceQuoteList).KeyColumn("SupplierId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.WorkOrderList).KeyColumn("SupplierId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.Items).KeyColumn("SupplierId").Cascade.AllDeleteOrphan().Inverse();
            //HasManyToMany(x => x.Items).Table("UINV_SupplierItem").ParentKeyColumn("SupplierId").ChildKeyColumn("ItemId").Cascade.SaveUpdate();
        }
    }
}
