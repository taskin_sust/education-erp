﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.CampusTest
{
    public interface ICampusSaveTest
    {
        #region Basic Test
        [Fact]
        void Save_Should_Return_Null_Exception();

        [Fact]
        void Should_Save_Successfully();

        [Fact]
        void Should_Not_Save_Empty_Name();

        [Fact]
        void Should_Not_Save_Without_Branch();

        [Fact]
        void Should_Not_Save_Without_Location();

        [Fact]
        void Should_Not_Save_Without_Contact_No();

        [Fact]
        void Should_Not_Save_Without_Campus_Room();

        [Fact]
        void Should_Not_Save_Invalid_Contact_No();

        [Fact]
        void Should_Not_Save_Empty_Room_No();

        [Fact]
        void Should_Not_Save_Empty_Room_Class_Capacity();

        [Fact]
        void Should_Not_Save_Empty_Exam_Capacity();

        #endregion

        #region Business Logic Test
        [Fact]
        void Should_Not_Save_Duplicate_Campus_Room();

        [Fact]
        void Save_Should_Check_Duplicate_Name_Entry();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Campus")]
    public class CampusSaveTest : CampusBaseTest, ICampusSaveTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _campusService.Save(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var campusFactory =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                })
                    .Persist();
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
            Assert.NotNull(campus);
            Assert.True(campus.Id > 0);
        }

        [Fact]
        public void Should_Not_Save_Empty_Name()
        {
            var campusFactory1 =
                _campusFactory.Create().WithBranch().WithNameAndRank("", 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                });
            Assert.Throws<ValueNotAssignedException>(() => campusFactory1.Persist());
        }

        [Fact]
        public void Should_Not_Save_Without_Branch()
        {
            var campusFactory1 =
                _campusFactory.Create().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                });
            Assert.Throws<ValueNotAssignedException>(() => campusFactory1.Persist());
        }

        [Fact]
        public void Should_Not_Save_Without_Location()
        {
            var campusFactory1 =
                _campusFactory.Create().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                });
            campusFactory1.Object.Location = "";
            Assert.Throws<ValueNotAssignedException>(() => campusFactory1.Persist());
        }

        [Fact]
        public void Should_Not_Save_Without_Contact_No()
        {
            var campusFactory1 =
                _campusFactory.Create().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                });
            campusFactory1.Object.ContactNumber = "";
            Assert.Throws<ValueNotAssignedException>(() => campusFactory1.Persist());
        }

        [Fact]
        public void Should_Not_Save_Invalid_Contact_No()
        {
            var campusFactory1 =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                });
            campusFactory1.Object.ContactNumber = "12345678910";
            Assert.Throws<InvalidDataException>(() => campusFactory1.Persist());
        }

        [Fact]
        public void Should_Not_Save_Without_Campus_Room()
        {
            var campusFactory1 =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1);
            Assert.Throws<ValueNotAssignedException>(() => campusFactory1.Persist());
        }

        [Fact]
        public void Should_Not_Save_Empty_Room_No()
        {
            var campusFactory1 =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                });
            Assert.Throws<ValueNotAssignedException>(() => campusFactory1.Persist());
        }

        [Fact]
        public void Should_Not_Save_Empty_Room_Class_Capacity()
        {
            var campusFactory1 =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ExamCapacity = 100
                    }
                });
            Assert.Throws<ValueNotAssignedException>(() => campusFactory1.Persist());
        }

        [Fact]
        public void Should_Not_Save_Empty_Exam_Capacity()
        {
            var campusFactory1 =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100
                    }
                });
            Assert.Throws<ValueNotAssignedException>(() => campusFactory1.Persist());
        }

        #endregion

        #region Business Logic Test
        [Fact]
        public void Should_Not_Save_Duplicate_Campus_Room()
        {
            var campusFactory1 =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "100",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    },
                    new CampusRoom()
                    {
                        RoomNo = "100",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                });
            Assert.Throws<DuplicateEntryException>(() => campusFactory1.Persist());
        }

        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            var campusFactory1 =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                })
                    .Persist();
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campusFactory1.Object.Id.ToString(CultureInfo.InvariantCulture)
            });
            Branch branch = campusFactory1.Object.Branch;
            var campusFactory2 =
                _campusFactory.Create().WithBranch(branch).WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                });
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
            Assert.Throws<DuplicateEntryException>(() => campusFactory2.Persist());
        }
        #endregion
    }
}
