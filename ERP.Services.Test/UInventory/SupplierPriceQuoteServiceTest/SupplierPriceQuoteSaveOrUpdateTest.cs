﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.SupplierPriceQuoteServiceTest
{
    interface ISupplierPriceQuoteSaveOrUpdateTest
    {
        void Should_Return_Successfully();
        void Should_Return_NullObjectException_For_MainObject();
        void Should_Return_NullObjectException_For_Supplier();
        void Should_Return_NullObjectException_For_Quotation();

    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "SupplierPriceQuote")]
    public class SupplierPriceQuoteSaveOrUpdateTest : SupplierPriceQuoteBaseTest, ISupplierPriceQuoteSaveOrUpdateTest
    {
        [Fact]
        public void Should_Return_Successfully()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create()
                .CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList)
                .Persist();

            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            Assert.Equal(_supplierPriceQuoteFactory.Object.TotalCost, _supplierPriceQuoteService.GetSupplierPriceQuote(_supplierPriceQuoteFactory.Object.Id).TotalCost);

        }
        [Fact]
        public void Should_Return_NullObjectException_For_MainObject()
        {
            Assert.Throws<NullObjectException>(() => _supplierPriceQuoteService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Return_NullObjectException_For_Supplier()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create()
                .CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList)
                .Persist();

            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();
            Assert.Throws<NullObjectException>(() => _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).Persist());
        }

        [Fact]
        public void Should_Return_NullObjectException_For_Quotation()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create()
                .CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList)
                .Persist();

            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();
            Assert.Throws<NullObjectException>(() => _supplierPriceQuoteFactory.Create().WithSupplier(_supplierFactory.Object).Persist());
        }
    }
}
