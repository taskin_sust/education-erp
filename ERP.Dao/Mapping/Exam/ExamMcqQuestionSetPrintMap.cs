﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Exam
{
    public class ExamMcqQuestionSetPrintMap : BaseClassMap<ExamMcqQuestionSetPrint, long>
    {
        public ExamMcqQuestionSetPrintMap()
        {
            Table("ExamMcqQuestionSetPrint");
            LazyLoad();

            Map(x => x.UniqueSet).Column("UniqueSet");
            Map(x => x.SetId).Column("SetId");
            Map(x => x.SetNo).Column("SetNo");
            Map(x => x.PrintStatus).Column("PrintStatus");
            Map(x => x.CodeNo).Column("CodeNo");
            Map(x => x.PrintBy).Column("PrintBy");
            Map(x => x.PrintDate).Column("PrintDate");
            References(x => x.Exam).Column("ExamId");
        }
    }
}
