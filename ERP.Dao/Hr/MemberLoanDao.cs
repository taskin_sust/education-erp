using System.Linq;
using NHibernate;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface IMemberLoanDao : IBaseDao<MemberLoan, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        MemberLoanDetailsDto GenerateLoanDetailByMemberPin(long id);
        #endregion

        #region List Loading Function

        decimal GetCurrentLoan(int pin);
        decimal GetCurrentMonthlyRefund(int pin);

        #endregion

        #region Others Function
        #endregion
    }

    public class MemberLoanDao : BaseDao<MemberLoan, long>, IMemberLoanDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public MemberLoanDetailsDto GenerateLoanDetailByMemberPin(long id)
        {
            string query = @"declare @mlaDate DATETIME
                            select @mlaDate= CreationDate from HR_MemberLoanApplication where id= " + id;

            query += @"SELECT
                              final.Pin,
                              final.FullNameEng Name,
                              fQuery.jobdept JobDepartment,
                              fQuery.jobBranch JobBranch,
                              fQuery.jobOrg JobOrganization,
                              fQuery.salarydept SalaryDepartment,
                              fQuery.salaryBranch SalaryBranch,
                              fQuery.salaryOrg SalaryOrganization,
                              final.RequestedAmount,
                              final.Remarks Reason,
                              final.zakatPaymentType PaymentTypeZakat,
                              final.zakatAmount AmountZakat,
                              final.EffectiveDate EffectiveFromZakat,
                              final.ClosingDate ClosingMonthZakat,
                              final.totalZakat TotalAmountZakat,
                              final.FundedBy FundedByZakat,
                              final.csrAmount AmountCsr,
                              final.csrApprovedDate ApprovedDateCsr,
                              final.csrClosingDate ClosingDateCsr,
                              final.csrEffectiveDate EffectiveFromCsr,
                              final.csrTotalAmount TotalAmountCsr,
                              final.paymentCsr PaymentTypeCsr,
                              final.LoanApprovedAmount LoanApproved,
                              final.MonthlyRefund MonthlyRefund,
                              final.RefundStart RefundStart,
                              final.approvedDate ApprovedDate,
                              final.FullName ApprovedBy
                            FROM (SELECT
                              tm.Id AS teamMemberId,
                              ma.Id AS memLoanAppId,
                              tm.Pin,
                              tm.FullNameEng,
                              ma.RequestedAmount,
                              ma.Remarks,
                              zakatPaymentType = (CASE mz.PaymentType
                                WHEN 1 THEN 'Once'
                                WHEN 3 THEN 'Monthly'
                                WHEN 4 THEN 'Yearly'
                                ELSE 'undefined'
                              END),
                              mz.Amount AS zakatAmount,
                              mz.EffectiveDate,
                              mz.ClosingDate,
                              mz.TotalAmount AS totalZakat,
                              FundedBy = (CASE mz.FundedBy
                                WHEN 1 THEN 'Mahmudul Hasan Sohag'
                                WHEN 2 THEN 'Muhammad Abul Hasan Liton'
                                ELSE 'undefined'
                              END),
                              paymentCsr = (CASE mls.PaymentType
                                WHEN 1 THEN 'once'
                                WHEN 3 THEN 'Monthly'
                                WHEN 4 THEN 'Yearly'
                                ELSE 'undefined'
                              END),
                              mls.Amount AS csrAmount,
                              mls.EffectiveDate AS csrEffectiveDate,
                              mls.ClosingDate AS csrClosingDate,
                              mls.TotalAmount AS csrTotalAmount,
                              mls.ApprovedDate AS csrApprovedDate,
                              ml.LoanApprovedAmount,
                              ml.MonthlyRefund,
                              ml.RefundStart,
                              aspNet.FullName,
                              ma.CreationDate AS approvedDate,
                              ma.Remarks AS Reason
                            FROM HR_MemberLoanApplication AS ma
                            LEFT JOIN HR_TeamMember AS tm
                              ON tm.id = ma.TeamMemberId and ma.id= " + id;
            query += @"LEFT JOIN [HR_MemberLoanZakat] AS mz
                              ON ma.id = mz.MemberLoanApplicationId
                            LEFT JOIN [HR_MemberLoanCsr] AS mls
                              ON ma.id = mls.MemberLoanApplicationId
                            LEFT JOIN [HR_MemberLoan] AS ml
                              ON ma.Id = ml.MemberLoanApplicationId
                            LEFT JOIN [dbo].[AspNetUsers] AS aspNet
                              ON aspNet.id = ma.CreateBy) AS final
                            INNER JOIN (SELECT
                              *
                            FROM (SELECT
                              dept.Name AS salarydept,
                              cam.Name AS salaryCampus,
                              br.Name AS salaryBranch,
                              org.Name AS salaryOrg,
                              sh.TeamMemberId AS salaryTemId
                            FROM (SELECT
                              Id,
                              TeamMemberId,
                              SalaryCampusId,
                              SalaryDepartmentId,
                              RANK() OVER (PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC) AS ranks,
                              EffectiveDate
                            FROM [dbo].HR_SalaryHistory) AS sh
                            LEFT JOIN Campus AS cam
                              ON sh.SalaryCampusId = cam.Id
                            LEFT JOIN HR_Department AS dept
                              ON sh.SalaryDepartmentId = dept.Id
                            LEFT JOIN Branch AS br
                              ON cam.BranchId = br.Id
                            LEFT JOIN Organization AS org
                              ON br.OrganizationId = org.Id
                            WHERE sh.ranks = 1
                            AND EffectiveDate <= @mlaDate) AS salaryInfo
                            INNER JOIN (SELECT
                              dept.Name AS jobdept,
                              cam.Name AS jobCampus,
                              br.Name AS jobBranch,
                              org.Name AS jobOrg,
                              eh.TeamMemberId AS jobTemId
                            FROM (SELECT
                              Id,
                              TeamMemberId,
                              CampusId,
                              DepartmentId,
                              RANK() OVER (PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC) AS ranks,
                              EffectiveDate
                            FROM [dbo].[HR_EmploymentHistory]) AS eh
                            LEFT JOIN Campus AS cam
                              ON eh.CampusId = cam.Id
                            LEFT JOIN HR_Department AS dept
                              ON eh.DepartmentId = dept.Id
                            LEFT JOIN Branch AS br
                              ON cam.BranchId = br.Id
                            LEFT JOIN Organization AS org
                              ON br.OrganizationId = org.Id
                            WHERE eh.ranks = 1
                            AND EffectiveDate <= @mlaDate) AS jobInfo
                              ON jobInfo.jobTemId = salaryInfo.salaryTemId) AS fQuery
                              ON final.teamMemberId = fQuery.jobTemId";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<MemberLoanDetailsDto>());
            iQuery.SetTimeout(2700);
            return iQuery.UniqueResult<MemberLoanDetailsDto>();
        }
        #endregion

        #region List Loading Function

        public decimal GetCurrentLoan(int pin)
        {
            var query =
                @"DECLARE @current_loan decimal = (select ISNULL(sum(ml.LoanApprovedAmount),0) from HR_MemberLoan ml 
                                                   inner join HR_MemberLoanApplication mla on ml.MemberLoanApplicationId=mla.Id
                                                   and mla.TeamMemberId={0})-(select ISNULL(sum(RefundAmount),0) from HR_MemberLoanRefund where TeamMemberId={0})
                                                   select @current_loan as 'current_loan'";
            query = string.Format(query, pin);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var currentLoan = iQuery.UniqueResult<decimal>();
            return currentLoan;
        }
        public decimal GetCurrentMonthlyRefund(int pin)
        {
            var query = @"select top 1 ISNULL(MonthlyRefund,0) from HR_MemberLoan ml inner join HR_MemberLoanApplication mla on ml.MemberLoanApplicationId=mla.Id
                         and mla.TeamMemberId={0} order by ml.ApprovedDate desc";
            query = string.Format(query, pin);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var currentMonthlyRefund = iQuery.UniqueResult<decimal>();
            return currentMonthlyRefund;
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion
    }
}
