using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface IItemDao : IBaseDao<Item, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        IList<Item> LoadItem(int start, int length, string orderBy, string orderDir, long? organization, int? itemType, long? itemGroup, string name, int? status);
        IList<Item> LoadAssignItemList(string[] programSessionIdList, int itemType, long itemGroupId, long organizationId);
        IList<Item> LoadItem(long? organizationId = null, long? itemGroupId = null, int? itemType = null, bool? hasSpecificationCriteria = null, bool withCommonItems = false);
        IList<Item> LoadItem(long fromOrganizationId, long toOrganizationId, long[] itemGroupId);
        IList<Item> LoadByIds(string[] itemIds);
        IList<Item> LoadItem(long[] itemGroupIds, long? organizationId);
        IList<Item> LoadItemList(long[] itemIds);
        IList<Item> LoadItemForOrganizationOrGroup(List<long> organizationIds,List<long>programIds ,long[] itemGroupId);
        //IList<Item> LoadItemForOrganizationOrGroup(long? organizationId, long[] itemGroupId = null);
        
        #endregion

        #region Others Function

        int HasManyItemObjects(long id);
        int LoadItemCount(long? organization, int? itemType, long? itemGroup, string name, int? status);
        bool IsDuplicateItem(out string fieldName, string name, long id);

        #endregion

    }

    public class ItemDao : BaseDao<Item, long>, IItemDao
    {

        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<Item> LoadItem(int start, int length, string orderBy, string orderDir, long? organization, int? itemType, long? itemGroup, string name, int? status)
        {
            ICriteria criteria = LoadManageItemCriteria(orderBy, orderDir, organization, itemType, itemGroup, name, status);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Item>();
        }

        public IList<Item> LoadAssignItemList(string[] programSessionIdList, int itemType, long itemGroupId, long organizationId)
        {
            string selectedProgramAndSession = " ";
            string join1 = " ";
            string join2 = " ";
            string join3 = " ";
            string join4 = " ";
            int count = programSessionIdList.Count();
            if (count > 0)
            {
                foreach (var programSessionId in programSessionIdList)
                {
                    var spltValue = programSessionId.Split(new string[] { "::" },
                        StringSplitOptions.RemoveEmptyEntries);
                    selectedProgramAndSession += @" (ProgramId = " + Convert.ToInt64(spltValue[0]) +
                                                 " AND SessionId = " + Convert.ToInt64(spltValue[1]) + ") " +
                                                 "OR ";
                }
                join2 += @" AND c = " + count + "";
            }
            else
            {
                selectedProgramAndSession += @" (ProgramId IS NULL AND SessionId IS NULL) " +
                                                "OR ";
            }
            if (organizationId == SelectionType.NotApplicable)
            {
                join3 = " AND item.OrganizationId is null ";
            }
            else
            {
                join3 = " AND item.OrganizationId =" + organizationId + " ";
                join4 = " INNER JOIN [Organization] as org ON item.OrganizationId = org.Id and org.Status=" + Organization.EntityStatus.Active;
            }

            join1 += @" INNER JOIN [UINV_ItemGroup] as itemGrp ON itemGrp.Id = item.ItemGroupId  and itemGrp.Status=" + ItemGroup.EntityStatus.Active + "  " +
                     join4 +
                     " WHERE item.ItemType=" + itemType + " AND itemGrp.Id = " + itemGroupId + join3;

            selectedProgramAndSession = selectedProgramAndSession.Remove(selectedProgramAndSession.Length - 3);
            string query = @"SELECT Item.* FROM (
                                SELECT ItemId, Count(*) As c
                                FROM [UINV_ProgramSessionItem]
                                WHERE 1=1 and
                                " + selectedProgramAndSession + " " +
                                "GROUP BY ItemId" +
                           ") as a " +
                           "INNER JOIN [UINV_Item] as item ON item.Id = a.ItemId   and item.Status = " + Item.EntityStatus.Active + " " +
                           join1 +
                           join2;

            IQuery iQuery = Session.CreateSQLQuery(query).AddEntity("Item", typeof(Item));

            iQuery.SetResultTransformer(Transformers.AliasToBean<Item.ItemQueryTransfer>());
            var res = iQuery.List<Item.ItemQueryTransfer>().ToList();
            return res.Select(itemDto => itemDto.Item).ToList();
        }

        public IList<Item> LoadItem(long? organizationId = null, long? itemGroupId = null, int? itemType = null, bool? hasSpecificationCriteria = null, bool withCommonItems = false)
        {
            ICriteria criteria = GetItemCriteria("", "", organizationId, itemType, itemGroupId, "", null, false, hasSpecificationCriteria, withCommonItems);
            return criteria.List<Item>();
        }

        public IList<Item> LoadItem(long fromOrganizationId, long toOrganizationId, long[] itemGroupId)
        {
            ICriteria criteria = Session.CreateCriteria<Item>().Add(Restrictions.Not(Restrictions.Eq("Status", Item.EntityStatus.Delete)));
            criteria.CreateAlias("ProgramSessionItems", "psi", JoinType.InnerJoin, Restrictions.Eq("psi.Status", ProgramSessionItem.EntityStatus.Active));
            var disjunctionFromOrg = Restrictions.Disjunction();
            disjunctionFromOrg.Add(Restrictions.Eq("Organization.Id", fromOrganizationId));
            disjunctionFromOrg.Add(Restrictions.IsNull("Organization"));

            var disjunctionToOrg = Restrictions.Disjunction();
            disjunctionToOrg.Add(Restrictions.Eq("Organization.Id", toOrganizationId));
            disjunctionToOrg.Add(Restrictions.IsNull("Organization"));

            var disjunctionItemGroup = Restrictions.Disjunction();
            disjunctionItemGroup.Add(Restrictions.In("ItemGroup.Id", itemGroupId));

            criteria.Add(disjunctionFromOrg);
            criteria.Add(disjunctionToOrg);
            criteria.Add(disjunctionItemGroup);
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);

            return criteria.List<Item>();
        }

        public IList<Item> LoadItem(long[] itemGroupIds, long? organizationId)
        {
            ICriteria criteria = Session.CreateCriteria<Item>().Add(Restrictions.Not(Restrictions.Eq("Status", Item.EntityStatus.Delete)));
            if (organizationId != null)
            {
                criteria.CreateAlias("Organization", "organization", JoinType.LeftOuterJoin, Restrictions.Eq("organization.Status", Organization.EntityStatus.Active));
                var disjunctionOrg = Restrictions.Disjunction();
                disjunctionOrg.Add(Restrictions.Eq("Organization.Id", organizationId));
                disjunctionOrg.Add(Restrictions.IsNull("Organization"));
                criteria.Add(disjunctionOrg);
            }
            if (itemGroupIds != null && itemGroupIds.Count()>0)
                criteria.Add(Restrictions.In("ItemGroup.Id", itemGroupIds));
            return criteria.List<Item>();
        }

        public IList<Item> LoadItemForOrganizationOrGroup(List<long> organizationIds,List<long> programIds, long[] itemGroupIds=null)
        {
            ICriteria criteria = Session.CreateCriteria<Item>().Add(Restrictions.Eq("Status", Item.EntityStatus.Active));
            if (programIds != null && !programIds.Contains(0))
            {
                criteria.CreateAlias("ProgramSessionItems", "psi", JoinType.InnerJoin, Restrictions.Eq("psi.Status", ProgramSessionItem.EntityStatus.Active));
                //criteria.Add(Restrictions.In("psi.Program.Id", programIds));
                var disjunctionPr = Restrictions.Disjunction();
                disjunctionPr.Add(Restrictions.In("psi.Program.Id", programIds));
                disjunctionPr.Add(Restrictions.IsNull("psi.Program"));
                criteria.Add(disjunctionPr);
            }
            
            if (organizationIds != null && !organizationIds.Contains(0))
            {
                criteria.CreateAlias("Organization", "organization", JoinType.LeftOuterJoin, Restrictions.Eq("organization.Status", Organization.EntityStatus.Active));
                var disjunctionOrg = Restrictions.Disjunction();
                disjunctionOrg.Add(Restrictions.In("Organization.Id", organizationIds));
                disjunctionOrg.Add(Restrictions.IsNull("Organization"));
                criteria.Add(disjunctionOrg);
            }
            if (itemGroupIds != null)
            {
                criteria.CreateAlias("ItemGroup", "itemGroup", JoinType.InnerJoin, Restrictions.Eq("itemGroup.Status", ItemGroup.EntityStatus.Active));
                criteria.Add(Restrictions.In("itemGroup.Id", itemGroupIds));
            }
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            return criteria.List<Item>();
        }

        //public IList<Item> LoadItemForOrganizationOrGroup(long? organizationId, long[] itemGroupId = null)
        //{
        //    ICriteria criteria = Session.CreateCriteria<Item>().Add(Restrictions.Eq("Status", Item.EntityStatus.Active));
        //    if (organizationId != null && organizationId != 0)
        //    {
        //        criteria.CreateAlias("Organization", "organization", JoinType.LeftOuterJoin, Restrictions.Eq("organization.Status", Organization.EntityStatus.Active));
        //        var disjunctionOrg = Restrictions.Disjunction();
        //        disjunctionOrg.Add(Restrictions.Eq("Organization.Id", organizationId));
        //        disjunctionOrg.Add(Restrictions.IsNull("Organization"));
        //        criteria.Add(disjunctionOrg);
        //    }
        //    if (itemGroupId != null)
        //    {
        //        criteria.CreateAlias("ItemGroup", "itemGroup", JoinType.InnerJoin, Restrictions.Eq("itemGroup.Status", ItemGroup.EntityStatus.Active));
        //        criteria.Add(Restrictions.In("itemGroup.Id", itemGroupId));
        //    }
        //    return criteria.List<Item>();
        //}

        public IList<Item> LoadItemList(long[] itemIds)
        {
            return Session.QueryOver<Item>().Where(x => x.Id.IsIn(itemIds)).List<Item>();
        }

        #endregion

        #region Others Function

        public int HasManyItemObjects(long itemGroupId)
        {
            var returnObj = Session.QueryOver<Item>().Where(x => x.ItemGroup.Id == itemGroupId && x.Status != Item.EntityStatus.Delete).List<Item>();
            return returnObj.Count;
        }

        public int LoadItemCount(long? organization, int? itemType, long? itemGroup, string name, int? status)
        {
            ICriteria criteria = LoadManageItemCriteria("", "", organization, itemType, itemGroup, name, status, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public bool IsDuplicateItem(out string fieldName, string name, long id)
        {
            fieldName = "";
            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<Item>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Item.EntityStatus.Delete)));

            if (id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (name != null)
            {
                criteria.Add(Restrictions.Eq("Name", name));
                fieldName = "Name";
            }
            var itemList = criteria.List<Item>();
            if (itemList != null && itemList.Count > 0)
            {
                checkValue = true;
            }
            return checkValue;
        }

        public IList<Item> LoadByIds(string[] itemIds)
        {
            ICriteria criteria = Session.CreateCriteria<Item>().Add(Restrictions.Eq("Status", Item.EntityStatus.Active));
            criteria.Add(Restrictions.In("Id", itemIds));
            return criteria.List<Item>();
        }

        #endregion

        #region Helper Function

        private ICriteria GetItemCriteria(string orderBy = "", string orderDir = "", long? organization = null, int? itemType = null, long? itemGroup = null
            , string name = "", int? status = null, bool countQuery = false, bool? hasSpecificationCriteria = null, bool withCommonItems = false)
        {
            ICriteria criteria = Session.CreateCriteria<Item>().Add(Restrictions.Not(Restrictions.Eq("Status", Item.EntityStatus.Delete)));
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }

            if (organization == SelectionType.NotApplicable)
            {
                //criteria.CreateAlias("Organization", "organization").Add(Restrictions.Not(Restrictions.Eq("organization.Status", Organization.EntityStatus.Delete)));
                criteria.Add(Restrictions.IsNull("Organization"));
            }
            else if (organization != null && organization != SelectionType.NotApplicable)
            {
                if (withCommonItems)
                {
                    criteria.CreateCriteria("Organization", "organization", NHibernate.SqlCommand.JoinType.LeftOuterJoin)
                    .Add(Restrictions.Or(
                        Restrictions.Eq("organization.Id", Convert.ToInt64(organization)),
                        Restrictions.IsNull("organization.Id")));
                }
                else
                {
                    criteria.CreateAlias("Organization", "organization").Add(Restrictions.Not(Restrictions.Eq("organization.Status", Organization.EntityStatus.Delete)));
                    criteria.Add(Restrictions.Eq("organization.Id", Convert.ToInt64(organization)));

                }

            }
            if (itemGroup != null && itemGroup > 0)
            {
                criteria.CreateAlias("ItemGroup", "itemGroup").Add(Restrictions.Not(Restrictions.Eq("itemGroup.Status", ItemGroup.EntityStatus.Delete)));
                criteria.Add(Restrictions.Eq("itemGroup.Id", Convert.ToInt64(itemGroup)));
            }
            if (itemType != null)
            {
                criteria.Add(Restrictions.Eq("ItemType", Convert.ToInt32(itemType)));
            }

            if (status != null)
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (hasSpecificationCriteria != null)
            {
                criteria.CreateAlias("SpecificationTemplate", "st").Add(Restrictions.Eq("st.Status", SpecificationTemplate.EntityStatus.Active));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }

        private ICriteria LoadManageItemCriteria(string orderBy = "", string orderDir = "", long? organization = null, int? itemType = null, long? itemGroup = null, string name = "",
            int? status = null, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<Item>().Add(Restrictions.Not(Restrictions.Eq("Status", Item.EntityStatus.Delete)));
            criteria.CreateAlias("ItemGroup", "itemGroup").Add(Restrictions.Not(Restrictions.Eq("itemGroup.Status", ItemGroup.EntityStatus.Delete)));

            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (organization == SelectionType.NotApplicable)
            {
                criteria.Add(Restrictions.IsNull("Organization"));
            }
            else if (organization != null)
            {
                criteria.CreateAlias("Organization", "organization").Add(Restrictions.Not(Restrictions.Eq("organization.Status", Organization.EntityStatus.Delete)));
                criteria.Add(Restrictions.Eq("organization.Id", Convert.ToInt64(organization)));
            }

            if (itemGroup != null)
            {
                criteria.Add(Restrictions.Eq("itemGroup.Id", Convert.ToInt64(itemGroup)));
            }
            if (itemType != null)
            {
                criteria.Add(Restrictions.Eq("ItemType", Convert.ToInt32(itemType)));
            }
            if (status != null)
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }

        #endregion

    }
}
