using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class EmploymentHistoryMap : BaseClassMap<EmploymentHistory,long> {
        
        public EmploymentHistoryMap() {
			Table("HR_EmploymentHistory");
			LazyLoad();
			References(x => x.Campus).Column("CampusId");
            References(x => x.Designation).Cascade.All().Column("DesignationId");//new
            References(x => x.Department).Cascade.All().Column("DepartmentId");//new
            //References(x => x.Designation).Cascade.All().Column("DesignationId");//old
            //References(x => x.Department).Cascade.All().Column("DepartmentId");//old
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.EmploymentStatus).Column("EmploymentStatus");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
            Map(x => x.LeaveDate).Column("LeaveDate");
			//HasMany(x => x.EmploymentHistoryLog).KeyColumn("EmploymentHistoryId");
        }
    }
}



