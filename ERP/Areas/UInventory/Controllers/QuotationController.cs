﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using iTextSharp.text.pdf;
using log4net;
using Microsoft.Ajax.Utilities;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Controllers;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class QuotationController : Controller
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly ItemService _itemService;
        private readonly ItemGroupService _itemGroupService;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private List<UserMenu> authorizeMenu;
        private IList<Quotation> _quotationList;
        private readonly IQuotationService _quotationService;
        private readonly IBranchService _branchService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IUserService _userService;
        private Quotation _quotation;
        private ISupplierService _supplierService;
        private ISupplierPriceQuoteService _supplierPriceQuoteService;
        private readonly IWorkOrderService _workOrderService;
        private List<UserMenu> _userMenu;

        public QuotationController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _itemService = new ItemService(session);
            _organizationService = new OrganizationService(session);
            _programBranchSessionService = new ProgramBranchSessionService(session);
            authorizeMenu = new List<UserMenu>();
            _quotationList = new List<Quotation>();
            _quotationService = new QuotationService(session);
            _itemGroupService = new ItemGroupService(session);
            _branchService = new BranchService(session);
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _userService = new UserService(session);
            _quotation = new Quotation();
            _supplierService = new SupplierService(session);
            _supplierPriceQuoteService = new SupplierPriceQuoteService(session);
            _workOrderService = new WorkOrderService(session);
        }

        #endregion

        #region Index/Manage

        public ActionResult Index()
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
                ViewBag.BranchList = new SelectList(string.Empty, "Id", "ShortName");
                ViewBag.QuotationStatusList = new SelectList(_commonHelper.LoadEmumToDictionary<QuotationStatus>(), "Key", "Value");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        public JsonResult QuotationList(int draw, int start, int length, string organization, string branch, string itemName, string deadLine, string quotationStatus)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            int recordsFiltered = 0;
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "org.ShortName";
                            break;
                        case "1":
                            orderBy = "branch.Name";
                            break;
                        case "2":
                            orderBy = "item.Name";
                            break;
                        case "3":
                            orderBy = "DeliveryDate";
                            break;
                        case "4":
                            orderBy = "QuotationNo";
                            break;
                        case "5":
                            orderBy = "SubmissionDeadLine";
                            break;
                        case "6":
                            orderBy = "QuotationStatus";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                QuotationStatus? qs = null;

                if (!String.IsNullOrEmpty(quotationStatus))
                {
                    qs = (QuotationStatus)Enum.Parse(typeof(QuotationStatus), quotationStatus, true);
                }

                recordsTotal = _quotationService.LoadQuotationCount(orderBy, orderDir.ToUpper(), _userMenu, organization, branch, itemName, deadLine, qs);
                recordsFiltered = recordsTotal;
                _quotationList = _quotationService.LoadQuotation(start, length, orderBy, orderDir.ToUpper(), _userMenu, organization, branch, itemName, deadLine, qs).ToList();

                foreach (var value in _quotationList)
                {
                    var str = new List<string>();
                    str.Add(value.Branch.Organization != null ? value.Branch.Organization.ShortName : "");
                    str.Add(value.Branch != null ? value.Branch.Name : "");
                    str.Add(value.Item.Name);
                    str.Add(value.PublishedDate == null ? "Not Published" : value.PublishedDate.Value.ToString("MMMM d, yyyy"));
                    str.Add(value.QuotationNo);
                    str.Add(value.SubmissionDeadLine.ToString("MMMM d, yyyy hh:mm tt"));

                    string quotStatus = LinkGenerator.GetDetailsLink("Details", "Quotation", value.Id);

                    if (value.QuotationStatus == QuotationStatus.NotPublished)
                    {
                        str.Add(_commonHelper.GetEmumIdToValue<QuotationStatus>((int)value.QuotationStatus));
                        quotStatus += LinkGenerator.GetDynamicLink("#", "Quotation", value.Id, "glyphicon glyphicon-remove cancelQuotation");
                    }
                    else if (value.QuotationStatus == QuotationStatus.Running)
                    {
                        DateTime currentDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        DateTime submissionDeadLine = DateTime.ParseExact(value.SubmissionDeadLine.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

                        if (submissionDeadLine <= currentDate)
                        {
                            str.Add(_commonHelper.GetEmumIdToValue<QuotationStatus>((int)QuotationStatus.SubmissionClosed));
                            quotStatus += LinkGenerator.GetDynamicLink("QuotationReview", "Quotation", value.Id, "glyphicon-new-window");
                        }
                        else
                        {
                            str.Add(_commonHelper.GetEmumIdToValue<QuotationStatus>((int)value.QuotationStatus));
                        }
                        quotStatus += LinkGenerator.GetDynamicLink("#", "Quotation", value.Id, "glyphicon glyphicon-remove cancelQuotation");
                    }
                    else if (value.QuotationStatus == QuotationStatus.Issued ||
                             value.QuotationStatus == QuotationStatus.PertialIssued)
                    {
                        str.Add(_commonHelper.GetEmumIdToValue<QuotationStatus>((int)value.QuotationStatus));
                        quotStatus += LinkGenerator.GetDynamicLink("QuotationReview", "Quotation", value.Id,
                            "glyphicon-new-window");
                    }
                    else
                    {
                        str.Add(_commonHelper.GetEmumIdToValue<QuotationStatus>((int)value.QuotationStatus));
                    }

                    str.Add(quotStatus);

                    getData.Add(str);
                }
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData
            });
        }

        #endregion

        #region Quotation Review

        public ActionResult QuotationReview(long id)
        {
            try
            {
                Quotation quotation = _quotationService.GetQuotation(id);
                DateTime currentDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                DateTime submissionDeadLine = DateTime.ParseExact(quotation.SubmissionDeadLine.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

                if (submissionDeadLine > currentDate) { return HttpNotFound(); }
                return View(quotation);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return HttpNotFound();
        }

        #endregion

        #region For Supplier

        public ActionResult ManageQuotationForSupplier()
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
                ViewBag.BranchList = new SelectList(string.Empty, "Id", "ShortName");
                ViewBag.ItemList = new SelectList(string.Empty, "Id", "Name");
                ViewBag.SupplierList = new SelectList(_supplierService.LoadAll(), "Id", "Name");
                var priceQuotStatus = _commonHelper.LoadEmumToDictionary<PriceQouteStatus>().ToList();
                priceQuotStatus.Add(new KeyValuePair<int, string>(0, "Running"));
                ViewBag.PriceQuotStatusList = new SelectList(priceQuotStatus.OrderBy(x => x.Key), "Key", "Value");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        public JsonResult QuotationListForSupplier(int draw, int start, int length, string supplierId, string organization, string branch, string item, string quotationStatus)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            int recordsFiltered = 0;
            try
            {

                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "org.Name";
                            break;
                        case "1":
                            orderBy = "branch.Name";
                            break;
                        case "2":
                            orderBy = "item.Name";
                            break;
                        case "3":
                            orderBy = "DeliveryDate";
                            break;
                        case "4":
                            orderBy = "SubmissionDeadLine";
                            break;
                        case "5":
                            orderBy = "QuotationStatus";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                recordsTotal = _quotationService.LoadSupplierManageQuotationCount(supplierId, organization, branch, item, quotationStatus);
                recordsFiltered = recordsTotal;

                var list = _quotationService.LoadSupplierManageQuotation(start, length, supplierId, organization, branch, item, quotationStatus).ToList();

                foreach (var value in list)
                {
                    var str = new List<string>();
                    string quotStatus = LinkGenerator.GetDetailsLink("SupplierQuotationDetails", "Quotation", value.Id);
                    str.Add(_organizationService.LoadById(value.OrganizationId).ShortName);
                    str.Add(_branchService.GetBranch(value.BranchId).Name);
                    str.Add(_itemService.LoadById(value.ItemId).Name);
                    str.Add(value.PublishedDate == null ? "N/A" : value.PublishedDate.Value.ToString("MMMM d, yyyy"));
                    str.Add(value.SubmissionDate.ToString("MMMM d, yyyy hh:mm tt"));

                    if (value.TableName == "1Quotation")
                    {
                        DateTime currentDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        DateTime submissionDeadLine = DateTime.ParseExact(value.SubmissionDate.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

                        if (submissionDeadLine <= currentDate)
                        {
                            str.Add(_commonHelper.GetEmumIdToValue<QuotationStatus>((int)QuotationStatus.SubmissionClosed));
                        }
                        else
                        {
                            quotStatus += LinkGenerator.GetDynamicLink("SubmitQuotation", "Quotation", value.Id, "glyphicon-new-window");
                            str.Add(_commonHelper.GetEmumIdToValue<QuotationStatus>((int)value.Status));
                        }
                    }

                    else if (value.TableName == "2PriceQuot")
                    {
                        if (value.Status == (int)PriceQouteStatus.Submitted)
                        {
                            quotStatus += LinkGenerator.GetDynamicLink("#", "Quotation", value.PriceQuotationId, "glyphicon glyphicon-remove cancelPriceQuotation");
                        }
                        str.Add(_commonHelper.GetEmumIdToValue<PriceQouteStatus>((int)value.Status));
                    }

                    str.Add(quotStatus);
                    getData.Add(str);
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData
            });
        }

        public ActionResult SubmitQuotation(long id)
        {
            try
            {
                Quotation quotation = _quotationService.GetQuotation(id);
                if (quotation != null)
                {
                    var a = _supplierService.LoadSupplierByItemIds(new string[] { quotation.Item.Id.ToString() });
                    ViewBag.SupplierList = new SelectList(a, "Id", "Name");
                    return View("SubmitQuotationBySupplier", quotation);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult SubmitQuotationBySupplier(float unitCost, long quotationId, long supplierId)
        {
            string errormessage = "";
            try
            {

                Supplier supplier = _supplierService.LoadById(supplierId);

                Quotation quotation = _quotationService.GetQuotation(quotationId);
                if (quotation == null)
                    throw new NullObjectException("Quotation can't empty!!");

                bool check = _supplierPriceQuoteService.CheckSupplierPriceQuotation(supplier.Id, quotation.Id);
                if (check)
                    return Json(new { isSuccess = false, Message = "Quotation already submitted." });

                var supplierPriceQuote = new SupplierPriceQuote
                {
                    Supplier = supplier,
                    Quotation = quotation,
                    PriceQuoteStatus = (int)PriceQouteStatus.Submitted,
                    QuotedUnitCost = Convert.ToDecimal(String.Format("{0:0.00}", unitCost)),
                    TotalCost = Math.Round((decimal)(unitCost * quotation.QuotationQuantity)),
                    Status = SupplierPriceQuote.EntityStatus.Active
                };

                _supplierPriceQuoteService.SaveOrUpdate(supplierPriceQuote);

                return Json(new { isSuccess = true, Message = "Quotation submitted successfully !!" });
            }
            catch (MessageException ex)
            {
                errormessage = ex.Message;
            }

            catch (EmptyFieldException ex)
            {
                errormessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errormessage = ex.Message;
            }
            catch (NullObjectException ex)
            {
                errormessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                errormessage = WebHelper.SetExceptionMessage(ex);

            }
            return Json(new { isSuccess = false, Message = errormessage });
        }

        public ActionResult SupplierQuotationDetails(long id)
        {
            try
            {
                var quotation = _quotationService.GetQuotation(id);
                if (quotation == null)
                {
                    ViewBag.ErrorMessage = "Invalid Quotation.";
                    return RedirectToAction("ManageQuotationForSupplier");
                }
                return View(quotation);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return HttpNotFound();
            }
        }

        public ActionResult PriceQuotCancel(long id)
        {
            try
            {
                bool isCancel = _supplierPriceQuoteService.IsCancel(id);
                if (isCancel)
                {
                    return Json(new { IsSuccess = true, returnMessage = "Price Quotation sucessfully cancel." });
                }
                return Json(new Response(true, "Problem Occurred. Retry"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public ActionResult CheckSupplierPriceQuotation(long supplierId, long quotationId)
        {
            bool check = _supplierPriceQuoteService.CheckSupplierPriceQuotation(supplierId, quotationId);
            if (check)
            {
                return Json(new { IsSuccess = true });
            }
            else
            {
                return Json(new { IsSuccess = false });
            }
        }

        #endregion

        #region Details Operation

        public ActionResult Details(long id)
        {
            try
            {
                _quotation = _quotationService.GetQuotation(id);
                if (_quotation == null) ViewBag.ErrorMessage = "Quotation can't be empty";
                else
                {
                    _quotation.CreateByText = _userService.GetUserNameByAspNetUserId(_quotation.CreateBy);
                    _quotation.ModifyByText = _userService.GetUserNameByAspNetUserId(_quotation.ModifyBy);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View(_quotation);
        }

        [HttpPost]
        public ActionResult QuotationPublishedFromDetails(long quotationId)
        {
            string errorMessage = "";
            try
            {
                Quotation quotation = _quotationService.GetQuotation(quotationId);
                if (quotation == null)
                    return Json(new Response(false, "Quotation can't empty !!"));

                quotation.QuotationStatus = QuotationStatus.Running;
                quotation.PublishedDate = DateTime.Now;
                bool isSave = _quotationService.SaveOrUpdate(quotation);
                return isSave ? Json(new { isSuccess = true, returnMessage = "Quotation published successfully." }) : Json(new Response(false, "Quotation published failed."));
            }
            catch (MessageException ex)
            {
                errorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                errorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                errorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                errorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new Response(false, errorMessage));
        }

        #endregion

        #region Create Operation

        public ActionResult Create()
        {
            try
            {
                InitializeView();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(QuotationViewModel quotationViewModel)
        {
            string errorMessage = "";
            try
            {
                InitializeView();
                quotationViewModel.Id = 0;
                if (!ModelState.IsValid)
                {
                    string validationErrors = string.Join(",", this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors).GroupBy(x => x.ErrorMessage).Select(x => x.Key));
                    return Json(new Response(false, validationErrors));
                }
                else
                {
                    int quotationStatus = (int)QuotationStatus.NotPublished;
                    Quotation quotation = GetQuotation(quotationViewModel, quotationStatus);
                    bool isSave = _quotationService.SaveOrUpdate(quotation);
                    return isSave ? Json(new { isSuccess = true, returnMessage = "Quotation submit successfully." }) : Json(new Response(false, "Quotation submit failed."));
                }
            }
            catch (NullObjectException ex)
            {
                errorMessage = ex.Message;
            }
            catch (MessageException ex)
            {
                errorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                errorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                errorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
            return Json(new Response(false, errorMessage));
        }

        [HttpPost]
        public ActionResult QuotationPublished(QuotationViewModel quotationViewModel)
        {
            string errorMessage = "";
            try
            {
                InitializeView();
                if (!ModelState.IsValid)
                {
                    string validationErrors = string.Join(",",
                        ModelState.Values.Where(E => E.Errors.Count > 0).SelectMany(E => E.Errors)
                            .Select(E => E.ErrorMessage).ToArray());
                    return Json(new Response(false, validationErrors));
                }
                else
                {
                    int quotationStatus = (int)QuotationStatus.Running;
                    Quotation quotation = GetQuotation(quotationViewModel, quotationStatus);
                    quotation.PublishedDate = DateTime.Now;
                    bool isSave = _quotationService.SaveOrUpdate(quotation);
                    return isSave ? Json(new { isSuccess = true, returnMessage = "Quotation submit successfully." }) : Json(new Response(false, "Quotation submit failed."));

                }
            }
            catch (MessageException ex)
            {
                errorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                errorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                errorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
            return Json(new Response(false, errorMessage));
        }

        #endregion

        #region Edit Operation

        public ActionResult Edit(long id)
        {
            Quotation quotation;
            var quotationViewModel = new QuotationViewModel();
            var listCriteria = new List<QuatationSpecificationListViewModel>();
            try
            {
                quotation = _quotationService.GetQuotation(id);
                
                if (quotation == null)
                {
                    ViewBag.ErrorMessage = "Quotation can't empty";
                    return RedirectToAction("Index");
                }
                //var a = quotation.Item.SpecificationTemplate;
                //var b = quotation.Item.Organization;
                InitializeUpdateView(quotation);
                foreach (var quotationCriteria in quotation.QuotationCriteriaList)
                {
                    var quoCriViewModel = new QuatationSpecificationListViewModel
                    {
                        Criteria = quotationCriteria.Criteria,
                        CriteriaValue = quotationCriteria.CriteriaValue
                    };
                    listCriteria.Add(quoCriViewModel);
                }
                quotationViewModel = new QuotationViewModel()
                {
                    Id = quotation.Id,
                    OrganizationId = quotation.Branch.Organization.Id,
                    ItemGroupId = quotation.Item.ItemGroup.Id,
                    ItemId = quotation.Item.Id,
                    ProgramSessionId = (quotation.Program == null || quotation.Session == null) ? SelectionType.NotApplicable.ToString() : quotation.Program.Id + "::" + quotation.Session.Id,
                    BranchId = quotation.Branch.Id,
                    Purpose = quotation.Purpose,
                    QuotationQuantity = quotation.QuotationQuantity,
                    SubmissionDeadline = quotation.SubmissionDeadLine,
                    DeliveryDate = quotation.DeliveryDate,
                    Remarks = quotation.Remarks,
                    SpecificationListViewModels = listCriteria
                };
                ViewBag.QuotationObj = quotation;
                return View(quotationViewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View(quotationViewModel);
        }

        [HttpPost]
        public ActionResult Edit(QuotationViewModel quotationViewModel)
        {
            string errorMessage = "";
            try
            {
                var obj = _quotationService.GetQuotation(quotationViewModel.Id);
                if (obj == null)
                    return Json(new Response(false, "Invalid Quotation"));

                if (!ModelState.IsValid)
                {
                    string validationErrors = string.Join(",",
                        ModelState.Values.Where(E => E.Errors.Count > 0).SelectMany(E => E.Errors)
                            .Select(E => E.ErrorMessage).ToArray());
                    return Json(new Response(false, validationErrors));
                }
                else
                {
                    int quotationStatus = (int)QuotationStatus.NotPublished;
                    Quotation quotation = GetQuotation(quotationViewModel, quotationStatus);
                    bool isSave = _quotationService.SaveOrUpdate(quotation);
                    return isSave ? Json(new { isSuccess = true, returnMessage = "Quotation update successfully." }) : Json(new Response(false, "Quotation update failed."));
                }
            }
            catch (MessageException ex)
            {
                errorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                errorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                errorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
            return Json(new Response(false, errorMessage));
        }

        [HttpPost]
        public ActionResult EditQuotationPublished(QuotationViewModel quotationViewModel)
        {
            string errorMessage = "";
            try
            {
                if (!ModelState.IsValid)
                {
                    string validationErrors = string.Join(",",
                        ModelState.Values.Where(E => E.Errors.Count > 0).SelectMany(E => E.Errors)
                            .Select(E => E.ErrorMessage).ToArray());
                    return Json(new Response(false, validationErrors));
                }
                else
                {
                    int quotationStatus = (int)QuotationStatus.Running;
                    Quotation quotation = GetQuotation(quotationViewModel, quotationStatus);
                    quotation.PublishedDate = DateTime.Now;
                    bool isSave = _quotationService.SaveOrUpdate(quotation);
                    return isSave ? Json(new { isSuccess = true, returnMessage = "Quotation submit successfully." }) : Json(new Response(false, "Quotation submit failed."));
                }
            }
            catch (MessageException ex)
            {
                errorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                errorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                errorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
            return Json(new Response(false, errorMessage));
        }

        #endregion

        #region Delete Operation

        public ActionResult Delete(long id)
        {
            try
            {
                bool isDeleted = _quotationService.IsDelete(id);
                if (isDeleted)
                {
                    return Json(new Response(true, "Quotation sucessfully deleted."));
                }
                return Json(new Response(true, "Problem Occurred. Retry"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Report

        public ActionResult QuotationReport()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                var authorgList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");

                ViewBag.OrganizationList = authorgList;
                ViewBag.BranchIdList = new SelectList(string.Empty, "Value", "Text");

                var itemGroupList = _itemGroupService.LoadItemGroup();
                itemGroupList.Insert(0, new ItemGroup() { Id = 0, Name = "All selected" });
                ViewBag.ItemGroupList = new SelectList(itemGroupList, "Id", "Name");

                ViewBag.ItemList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.ProgramSessionList = new SelectList(string.Empty, "Value", "Text");

                var purposeList = _commonHelper.LoadEmumToDictionary<Purpose>();
                purposeList = new Dictionary<int, string> { { 0, "All selected" }, { -1, "N/A" } }.Concat(purposeList).ToDictionary(k => k.Key, v => v.Value);
                ViewBag.PurposeList = new SelectList(purposeList, "Key", "Value");

                var quotationStatus = _commonHelper.LoadEmumToDictionary<QuotationStatus>();
                quotationStatus = (new Dictionary<int, string> { { 0, "Select All" } }).Concat(quotationStatus).ToDictionary(k => k.Key, v => v.Value);

                ViewBag.QuotationStatusList = new SelectList(quotationStatus, "Key", "Value");
            }
            catch (Exception)
            {
                return HttpNotFound();
            }
            return View();
        }

        [HttpPost]
        public ActionResult QuotationReport(QuotationReportFormViewModel quotationReport)
        {
            if (ModelState.IsValid)
            {
                var programSession = "";
                var purpose = "";

                ViewBag.OrganizationName = _organizationService.LoadById(quotationReport.OrganizationId).Name;
                ViewBag.BranchName = quotationReport.BranchId != null && !quotationReport.BranchId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _branchService.LoadBranch(quotationReport.BranchId).Select(x => x.Name).ToList()) : "All Branch";
                ViewBag.ItemGroup = quotationReport.ItemGroupId != null && !quotationReport.ItemGroupId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _itemGroupService.LoadItemGroup(quotationReport.ItemGroupId.ToList()).Select(x => x.Name).ToList()) : "All Item Groups";
                ViewBag.Item = quotationReport.ItemId != null && !quotationReport.ItemId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _itemService.LoadItemList(quotationReport.ItemId).Select(x => x.Name).ToList()) : "All Items";
                ViewBag.QuotationStatus = (int)quotationReport.QuotationStatus != SelectionType.SelelectAll ? _commonHelper.GetEmumIdToValue<QuotationStatus>((int)quotationReport.QuotationStatus) : "All Status";
                ViewBag.DateFrom = quotationReport.DateFrom.ToString("MMMM d, yyyy");
                ViewBag.DateTo = quotationReport.DateTo.ToString("MMMM d, yyyy");

                if (quotationReport.ProgramSession == null || quotationReport.ProgramSession.Contains(SelectionType.SelelectAll.ToString()))
                {
                    programSession = "All Program & Session";
                }
                else if (quotationReport.ProgramSession != null)
                {
                    foreach (var programsession in quotationReport.ProgramSession.Where(x => x != "-1"))
                    {
                        string[] programSessionArray = programsession.Split(new[] { "::" }, StringSplitOptions.None);
                        Program program = _programService.GetProgram(Convert.ToInt64(programSessionArray[0]));
                        Session session = _sessionService.LoadById(Convert.ToInt64(programSessionArray[1]));
                        string programName = program.Name;
                        string sessionName = session.Name;
                        string programSessionName = programName + " & " + sessionName + ", ";
                        programSession += programSessionName;
                    }
                    if (quotationReport.ProgramSession.Contains("-1"))
                    {
                        programSession += " N/A";
                    }
                    programSession = programSession.Trim(',', ' ', ',');
                }

                if (quotationReport.Purpose == null || quotationReport.Purpose.Where(x => (int)x == SelectionType.SelelectAll).Any())
                {
                    purpose = "All Purposes";
                }
                else if (quotationReport.Purpose != null)
                {
                    foreach (var purposeValue in quotationReport.Purpose.Where(x => (int)x != -1))
                    {
                        purpose += _commonHelper.GetEmumIdToValue<Purpose>((int)purposeValue) + ", ";
                    }
                    if (quotationReport.Purpose.Where(x => (int)x == -1).Any())
                    {
                        purpose += " N/A";
                    }
                    purpose = purpose.Trim(' ', ',');
                }

                ViewBag.ViewProgramSession = programSession;
                ViewBag.Purpose = purpose;
                return PartialView("Partial/_QuotationReportPartialView", quotationReport);
            }
            return View();
        }

        public ActionResult QuotationReportAjaxRequest(int draw, int start, int length, string organizationId, long[] branchIdList, int[] purposeList,
            long[] itemGroupIdList, long[] itemIdList, string[] programSessionIds, int quotationStatus, string dateFrom, string dateTo)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            int recordsFiltered = 0;
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "BranchName";
                            break;
                        case "1":
                            orderBy = "ItemGroupName";
                            break;
                        case "2":
                            orderBy = "ItemName";
                            break;
                        case "3":
                            orderBy = "ProgramSessionName";
                            break;
                        case "4":
                            orderBy = "PublishedDate";
                            break;
                        case "5":
                            orderBy = "SubmissionDeadLine";
                            break;
                        case "6":
                            orderBy = "QuotationNo";
                            break;
                        case "7":
                            orderBy = "QuotationStatus";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                recordsTotal = _quotationService.LoadQuotationReportCount(_userMenu, organizationId, branchIdList, purposeList,
                    itemGroupIdList, itemIdList, programSessionIds, quotationStatus, dateFrom, dateTo);
                recordsFiltered = recordsTotal;
                List<QuotationReportDto> quotationList = _quotationService.LoadQuotationReport(start, length, orderBy, orderDir.ToUpper(), _userMenu, organizationId, branchIdList,
                    purposeList, itemGroupIdList, itemIdList, programSessionIds, quotationStatus, dateFrom, dateTo).ToList();
                int srl = start+1;
                foreach (var value in quotationList)
                {
                    var str = new List<string>();
                    str.Add(srl.ToString());
                    str.Add(value.OrgName);
                    str.Add(value.BranchName);
                    str.Add(value.ItemGroupName);
                    str.Add(value.ItemName);
                    str.Add(value.ProgramSessionName!=" "?value.ProgramSessionName:"N/A");
                    str.Add(value.PublishedDate.ToString("MMMM d, yyyy"));
                    str.Add(value.SubmissionDeadLine.ToString("MMMM d, yyyy"));
                    str.Add(value.QuotationNo);
                    str.Add(value.QuotationStatus);
                    str.Add(LinkGenerator.GetDetailsLink("Details", "Quotation", value.Id));
                    getData.Add(str);
                    srl++;
                }
            }
            catch (InvalidDataContractException ex)
            {
                _logger.Error(ex);
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData
            });
        }

        #endregion

        #region Export

        public ActionResult ExportQuotationReport(string organizationId, long[] branchIdList, int[] purposeList, long[] itemGroupIdList, long[] itemIdList,
            string[] programSessionIds, int quotationStatus, string dateFrom, string dateTo)
        {
            var excelList = new List<List<object>>();
            try
            {
                var organization = _organizationService.LoadById(Convert.ToInt64(organizationId));
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<string> headerList = new List<string>();
                if (organization!=null)
                    headerList.Add(organization.Name);
                headerList.Add("Quotation Report");
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                columnList.Add("Organization");
                columnList.Add("Branch");
                columnList.Add("Item Group");
                columnList.Add("Item Name");
                columnList.Add("Program & Session");
                columnList.Add("Published Date");
                columnList.Add("Submission DeadLine");
                columnList.Add("Quotation Ref No");
                columnList.Add("Status");
                int recordsTotal = _quotationService.LoadQuotationReportCount(userMenu, organizationId, branchIdList, purposeList,
                    itemGroupIdList, itemIdList, programSessionIds, quotationStatus, dateFrom, dateTo);

                List<QuotationReportDto> quotationList = _quotationService.LoadQuotationReport(0, recordsTotal, "", "", userMenu, organizationId, branchIdList,
                    purposeList, itemGroupIdList, itemIdList, programSessionIds, quotationStatus, dateFrom, dateTo).ToList();
                foreach (var q in quotationList)
                {
                    var xlsRow = new List<object>();
                    xlsRow.Add(q.OrgName);
                    xlsRow.Add(q.BranchName);
                    xlsRow.Add(q.ItemGroupName);
                    xlsRow.Add(q.ItemName);
                    xlsRow.Add(q.ProgramSessionName);
                    xlsRow.Add(q.PublishedDate.ToString("MMMM d, yyyy"));
                    xlsRow.Add(q.SubmissionDeadLine.ToString("MMMM d, yyyy"));
                    xlsRow.Add(q.QuotationNo);
                    xlsRow.Add(q.QuotationStatus);
                    excelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, excelList, footerList, "Quotation_Report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }
            catch (Exception ex)
            {
                return View("AutoClose");
            }
        }

        #endregion

        #region Ajax Operation

        public JsonResult LoadItemForAjax(string organizationId, string itemGroupId)
        {
            try
            {
                if (organizationId == "") organizationId = null;
                if (itemGroupId == "") itemGroupId = null;
                var itemList = _itemService.LoadItem(Convert.ToInt64(organizationId), Convert.ToInt64(itemGroupId));
                var itemSelectList = new SelectList(itemList.ToList(), "Id", "Name");
                return Json(new { returnItemList = itemSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public ActionResult LoadItemSpecificationForAjax(long itemId, long quotationId)
        {
            try
            {
                var itemObj = _itemService.LoadById(Convert.ToInt64(itemId));
                if (itemObj == null || itemObj.SpecificationTemplate == null)
                    return Json(new Response(false, "Specification of item are not assigned yet!!"));

                var specCriteriaList = itemObj.SpecificationTemplate.SpecificationCriteriaList;
                Quotation selectQuot = null;
                if (quotationId > 0)
                {
                    selectQuot = _quotationService.GetQuotation(quotationId, itemId);
                }
                ViewBag.SelectQuot = selectQuot;

                if (specCriteriaList != null)
                {
                    return PartialView("Partial/_specificationList", specCriteriaList);
                }
                else
                {
                    return Json(new Response(false, "Specification of item are not assigned yet!!"));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public JsonResult CheckIsCorporateBranch(long branchId)
        {
            try
            {
                var branch = _branchService.GetBranch(branchId);
                bool isCorporate = branch!=null && branch.IsCorporate;
                return Json(new { IsCorporate = isCorporate, isSuccess = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper Operation

        private void InitializeView()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.programSessionList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            ViewBag.branchList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.itemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");

            var purposeList = _commonHelper.LoadEmumToDictionary<Purpose>().ToList();
            purposeList.Add(new KeyValuePair<int, string>(0, "N/A"));
            ViewBag.purposeList = new SelectList(purposeList.OrderBy(x => x.Key), "Key", "Value");

            ViewBag.ProgramSessionIdist = new SelectList(string.Empty, "Value", "Text");
            ViewBag.itemList = new SelectList(string.Empty, "Key", "Value");
        }

        private void InitializeUpdateView(Quotation quotation)
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;

            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName", quotation.Branch.Organization.Id);

            List<Branch> brList = quotation.Branch.Organization != null ? _branchService.LoadAuthorizedBranch(authorizeMenu, new List<long>() { quotation.Branch.Organization.Id }, null, null, false).ToList()
                : _branchService.LoadAuthorizedBranch(authorizeMenu).ToList();

            ViewBag.branchList = new SelectList(brList, "Id", "Name", quotation.Branch.Id);

            ViewBag.itemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name", quotation.Item.ItemGroup.Id);

            var purposeList = _commonHelper.LoadEmumToDictionary<Purpose>().ToList();
            purposeList.Add(new KeyValuePair<int, string>(0, "N/A"));
            ViewBag.purposeList = new SelectList(purposeList.OrderBy(x => x.Key), "Key", "Value", quotation.Purpose == null ? SelectionType.NotApplicable : quotation.Purpose);

            var itemGroups = new[] { quotation.Item.ItemGroup.Id };

            var itemList = _itemService.LoadItemForOrganizationOrGroup(authorizeMenu,quotation.Branch.Organization.Id, itemGroups).ToList();
            ViewBag.itemList = new SelectList(itemList, "Id", "Name", quotation.Item.Id);

            //var itemProgramSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, quotation.Branch.Organization.Id,CommonHelper.ConvertToList(quotation.Branch.Id)).ToList();
            var itemProgramSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, quotation.Branch.Organization.Id, CommonHelper.ConvertToList(quotation.Branch.Id), CommonHelper.ConvertToList(quotation.Item.Id)).ToList();

            var psList = new SelectList(itemProgramSessionList, "ProgramAndSessionId", "ProgramAndSessionName").ToList();
            if (quotation.Purpose == null || quotation.Purpose>3)
                psList.Insert(0, new SelectListItem() { Value = SelectionType.NotApplicable.ToString(), Text = "N/A" });

            if (quotation.Program == null || quotation.Session == null)
            {
                ViewBag.ProgramSessionIdist = new SelectList(psList, "Value", "Text", SelectionType.NotApplicable);
            }
            else
            {
                ViewBag.ProgramSessionIdist = new SelectList(psList, "Value", "Text", quotation.Program.Id + "::" + quotation.Session.Id);
            }
        }

        private Quotation GetQuotation(QuotationViewModel quotationViewModel, int quotationStatus)
        {
            var org = _organizationService.LoadById(quotationViewModel.OrganizationId);
            var br = _branchService.GetBranch(quotationViewModel.BranchId);
            var item = _itemService.LoadById((int) quotationViewModel.ItemId);
            Program program = null;
            Session session = null;
            string quotationNo = "";
            var checkQuotation = _quotationService.GetQuotation(quotationViewModel.Id);
            if (checkQuotation != null)
            {
                quotationNo = checkQuotation.QuotationNo;
            }
            else
            {
                if (org != null && br != null)
                {
                    int quotationLast6Digit = 000000;
                    var lastQuotationNo = _quotationService.GetLastQuotationNo(br.Id);
                    quotationLast6Digit = lastQuotationNo != ""
                        ? Convert.ToInt32(lastQuotationNo.Substring(lastQuotationNo.Length - 6, 6)) + 1
                        : 000001;
                    quotationNo = org.ShortName.Substring(0, 3) + "" + br.Code + "QN";
                    quotationNo = quotationNo + quotationLast6Digit.ToString("000000");
                }
                else throw new InvalidDataException("Organization or Branch can't empty");
            }

            if (quotationViewModel.ProgramSessionId != Convert.ToString(SelectionType.NotApplicable))
            {
                var spltValue = quotationViewModel.ProgramSessionId.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                program = _programService.GetProgram(Convert.ToInt64(spltValue[0]));
                session = _sessionService.LoadById(Convert.ToInt64(spltValue[1]));
            }

            var quotation = new Quotation()
            {
                Id = quotationViewModel.Id,
                //Program = program,
               // Session = session,
                QuotationNo = quotationNo,
                Item = item,
                Branch = _branchService.GetBranch(quotationViewModel.BranchId),
                Purpose = quotationViewModel.Purpose == 0 ? null : quotationViewModel.Purpose,
                QuotationQuantity = quotationViewModel.QuotationQuantity,
                SubmissionDeadLine = Convert.ToDateTime(quotationViewModel.SubmissionDeadline),
                DeliveryDate = Convert.ToDateTime(quotationViewModel.DeliveryDate),
                QuotationStatus = (QuotationStatus)quotationStatus,
                Remarks = quotationViewModel.Remarks
            };

            if (item.ItemType == (int) ItemType.CommonItem)
            {
                quotation.Program = null;
                quotation.Session = null;
            }
            else
            {
                quotation.Program = program;
                quotation.Session = session;
            }

            if (item.ItemType == (int)ItemType.ProgramItem && quotation.Purpose == null)
            {
                quotation.PurposeProgram = null;
                quotation.PurposeSession = null;
            }
            else if (item.ItemType == (int)ItemType.ProgramItem && quotation.Purpose != null)
            {
                quotation.PurposeProgram = program;
                quotation.PurposeSession = session;
            }
            else if (item.ItemType == (int)ItemType.CommonItem && quotation.Purpose == null)
            {
                quotation.PurposeProgram = null;
                quotation.PurposeSession = null;
            }
            else if (item.ItemType == (int) ItemType.CommonItem && (quotation.Purpose > 0 && quotation.Purpose <= 3))
            {
                quotation.PurposeProgram = program;
                quotation.PurposeSession = session;
            }
            else
            {
                quotation.PurposeProgram = program;
                quotation.PurposeSession = session;
            }


            var quotationCriteriaList = new List<QuotationCriteria>();
            foreach (var obj in quotationViewModel.SpecificationListViewModels)
            {
                var quotationCriteria = new QuotationCriteria
                {
                    Quotation = quotation,
                    Criteria = obj.Criteria,
                    CriteriaValue = obj.CriteriaValue,
                    Status = QuotationCriteria.EntityStatus.Active
                };
                quotationCriteriaList.Add(quotationCriteria);
            }

            quotation.QuotationCriteriaList = quotationCriteriaList;

            return quotation;
        }

        #endregion

    }
}