﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Areas.Student.Models
{
    public class StudentListSms 
    {
        [Required(ErrorMessage = "You need to select organization")]
        public long OrganizationId { get; set; }       
        [Required(ErrorMessage = "You need to select program")]
        public long ProgramId { get; set; }
        [Required(ErrorMessage = "You need to select session")]
        public long SessionId { get; set; }
        public int SelectedVersion { get; set; }
        public int SelectedGender { get; set; }
        public int[] SelectedReligion { get; set; }    
        [Required(ErrorMessage = "You need to select branch")]
        public long[] SelectedBranch { get; set; }
        [Required(ErrorMessage = "You need to select campus")]
        public long[] SelectedCampus { get; set; }
        [Required(ErrorMessage = "You need to select batch day")]
        public string[] SelectedBatchDays { get; set; }
        [Required(ErrorMessage = "You need to select batch time")]
        public string[] SelectedBatchTime { get; set; }
        [Required(ErrorMessage = "You need to select batch")]
        public long[] SelectedBatch { get; set; }
        [Required(ErrorMessage = "You need to select course")]
        public long[] SelectedCourse { get; set; }
        [Required(ErrorMessage = "You need to select Sms Receiver")]
        //public List<int> SelectedVersions { get; set; }
        //[Required(ErrorMessage = "You need to select gender")]
        //public List<int> SelectedGenders { get; set; }
        //[Required(ErrorMessage = "You need to select Sms Receiver")]
        public long[] SelectedSmsReceiver { get; set; }       
        public string[] SelectedSmsOptions { get; set; }
        [Required(ErrorMessage = "You need to enter Sms template")]
        public virtual string Template { get; set; }
        public virtual string MaskName { get; set; } 

    }
}