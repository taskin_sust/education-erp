﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class DesignationFactory : ObjectFactoryBase<Designation>
    {
        #region Object Initialization

        public IDesignationService _designationService;
        private OrganizationFactory _organizationFactory;

        public DesignationFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _organizationFactory = new OrganizationFactory(this, _session);
            _designationService = new DesignationService(session);
        }

        #endregion

        #region Create

        public DesignationFactory Create()
        {
            Object = new Designation()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = "Test_Designation_" + "_" + DateTime.Now.Second.ToString(CultureInfo.CurrentCulture)
                            + "_" + DateTime.Now.Millisecond.ToString(CultureInfo.CurrentCulture),
                ShortName = "TDSN_" + DateTime.Now.Millisecond.ToString(CultureInfo.CurrentCulture),
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Designation.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public DesignationFactory CreateMore(int numberOfDesignation)
        {
            var orgNamePrefix = Guid.NewGuid().ToString();
            _organizationFactory.Create().WithNameAndRank(orgNamePrefix + "_" + DateTime.Now.Second.ToString(CultureInfo.CurrentCulture), 1).Persist();

            for (int i = 0; i < numberOfDesignation; i++)
            {
                var designation = new Designation()
                {
                    Id = 0,
                    Name = "TestDesignation_" + i,
                    ShortName = "TD_" + i,
                    Rank = i+1,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Branch.EntityStatus.Active
                };
                designation.Organization = _organizationFactory.Object;
                ObjectList.Add(designation);
            }
            return this;
        }

        public DesignationFactory WithNameAndRank(string name, string shourtName, int? rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            Object.ShortName = shourtName;
            return this;
        }
        public DesignationFactory WithRank(int rank)
        {
            Object.Rank = rank;
            return this;
        }

        public DesignationFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        public DesignationFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public DesignationFactory Persist()
        {
            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _designationService.DesignationSaveUpdate(item);
                    }
                }
            }
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _designationService.DesignationSaveUpdate(item);
                    }
                }
            }
            return this;
        }
        #endregion

        #region Others
        #endregion

        #region Cleanup

        public void Cleanup()
        {
            DeleteAll();
            _organizationFactory.Cleanup();
        }

        public void OnlyDesignationCleanUp()
        {
            DeleteAll();
        }

        #endregion

    }
}
