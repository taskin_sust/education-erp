﻿using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.OrganizationTest
{
    public interface IOrganizationUpdateTest
    {
        #region Basic Test
        [Fact]
        void Update_Should_Return_Null_Exception();

        [Fact]
        void Should_Update_Successfully();

        [Theory]
        [MemberData("OrganizationIndex", MemberType = typeof(IOrganizationUpdateTest))]
        void Update_Should_Check_Invalid_Entry(int i);
        #endregion

        #region Business Logic Test
        [Fact]
        void Update_Should_Check_Duplicate_Entry();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Organization")]
    public class OrganizationUpdateTest : OrganizationBaseTest, IOrganizationUpdateTest
    {
        #region Test Related Properties

        public static readonly List<Organization> TestCaseOrganizationList = OrganizationList();

        public static IEnumerable<object[]> OrganizationIndex
        {
            get
            {
                var indexList = new List<object[]>();
                for (int i = 0; i < TestCaseOrganizationList.Count; i++)
                    indexList.Add(new object[] { i });
                return indexList;
            }
        }
        #endregion

        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var organizationFactory =
                _organizationFactory.Create().WithNameAndRank(_name, 1).WithRequiredProperties(_name, _shortName, _website, _email, _contactNo, _logoImageUrl, _reportImageUrl, _idFooterImageUrl)
                    .Persist();
            Assert.Throws<NullObjectException>(() => _organizationService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var organizationFactory =
                _organizationFactory.Create().WithNameAndRank(_name, 1).WithRequiredProperties(_name, _shortName, _website, _email, _contactNo, _logoImageUrl, _reportImageUrl, _idFooterImageUrl)
                    .Persist();
            Organization organization = _organizationService.LoadById(organizationFactory.Object.Id);
            organization.Name = organization.Name.ToLower();
            organization.ShortName = organization.ShortName.ToLower();
            organization.Website = organization.Website.ToLower();
            organization.Email = organization.Email.ToLower();
            organization.ContactNo = organization.ContactNo.ToLower();
            organization.LogoImageUrl = organization.LogoImageUrl.ToLower();
            organization.ReportImageUrl = organization.ReportImageUrl.ToLower();
            organization.IdFooterImageUrl = organization.IdFooterImageUrl.ToLower();
            organization.Status = Organization.EntityStatus.Inactive;
            var success = _organizationService.SaveOrUpdate(organization);
            Assert.True(success);
        }

        [Theory]
        [InlineData("up")]
        public void Should_Update_Rank_Successfully(string action)
        {
            var organizationFactory = _organizationFactory.Create().Persist();
            var organization = organizationFactory.Object;
            var success = _organizationService.UpdateRank(organization.Id, 1, action);
            Assert.Equal(true, success);
        }

        [Theory]
        [MemberData("OrganizationIndex", MemberType = typeof(OrganizationUpdateTest))]
        public void Update_Should_Check_Invalid_Entry(int i)
        {
            var organizationFactory =
                _organizationFactory.Create().WithNameAndRank(_name, 1).WithRequiredProperties(_name, _shortName, _website, _email,
                _contactNo, _logoImageUrl, _reportImageUrl, _idFooterImageUrl)
                    .Persist();
            var organization = OrganizationSaveTest.TestCaseOrganizationList[i];
            organization.Id = organizationFactory.Object.Id;
            Assert.Throws<InvalidDataException>(() => _organizationService.SaveOrUpdate(organization));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Update_Should_Check_Duplicate_Entry()
        {
            var organizationFactory = _organizationFactory.CreateMore(2).Persist();
            organizationFactory.GetLastCreatedObjectList()[1].ShortName = organizationFactory.GetLastCreatedObjectList()[0].ShortName;
            Assert.Throws<DuplicateEntryException>(() => _organizationService.SaveOrUpdate(organizationFactory.GetLastCreatedObjectList()[1]));
        }

        #endregion

        #region Helper Functions
        public static List<Organization> OrganizationList()
        {
            var organizationList = new List<Organization>();
            for (var i = 0; i < 22; i++)
            {
                organizationList.Add(new Organization()
                {
                    Name = i == 0 ? null : Guid.NewGuid() + "_name",
                    ShortName = i == 1 ? null : Guid.NewGuid() + "_shortName",
                    Website = i == 2 ? null : Guid.NewGuid() + ".com",
                    Email = i == 3 ? null : i == 8 ? "test email" : Guid.NewGuid() + "@yahoo.com",
                    ContactNo = i == 4 ? null : i == 9 ? "12345678910" : "8801714963254",
                    LogoImageUrl = i == 5 ? null : Guid.NewGuid() + "_logoImageUrl",
                    ReportImageUrl = i == 6 ? null : Guid.NewGuid() + "_reportImageUrl",
                    IdFooterImageUrl = i == 7 ? null : Guid.NewGuid() + "_idFooterImageUrl",
                    DeductionPerAbsent = i == 10 ? -1.5M : 1.5M,
                    HouseRent = i == 11 ? -10.5M : 10.5M,
                    MedicalAllowance = i == 12 ? -10.5M : 10.5M,
                    MonthlyWorkingDay = i == 13 ? -1 : 1,
                    Convenyance = i == 14 ? -10.5M : 10.5M,
                    BasicSalary = i == 15 ? -10.5M : 10.5M
                });
            }
            organizationList[16].DeductionPerAbsent = null;
            organizationList[17].HouseRent = null;
            organizationList[18].MedicalAllowance = null;
            organizationList[19].MonthlyWorkingDay = null;
            organizationList[20].Convenyance = null;
            organizationList[21].BasicSalary = null;
            return organizationList;
        }
        #endregion
    }
}
