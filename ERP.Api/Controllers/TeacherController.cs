﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common.EntitySql;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Teachers;

namespace UdvashERP.Api.Controllers
{
    public class TeacherController : Controller
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeacherAPI");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ITeacherActivityService _teacherActivityService;
        private readonly ISubjectService _subjectService;
        private readonly ITeacherPublicRegistrationService _teacherPublicRegistrationService;
        private readonly IOrganizationService _organizationService;
        private readonly ICommonHelper _commonHelper;

        public TeacherController()
        {
            try
            {
                ISession session = NHibernateSessionFactory.OpenSession();
                _teacherActivityService = new TeacherActivityService(session);
                _subjectService = new SubjectService(session);
                _teacherPublicRegistrationService = new TeacherPublicRegistrationService(session);
                _organizationService = new OrganizationService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.Message);
            }
        }
        #endregion

        #region Private functions

        private int Authenticate(string key)
        {
            int error = 0;
            var apiKey = ConfigurationManager.AppSettings["ApiKey"];

            if (key != apiKey)
            {
                error = 1;
            }
            return error;
        }

        #endregion

        #region Select list 

        [HttpGet]
        public JsonResult GetReligion(string key)
        {
            bool successState = false;
            string message = "";
            List<CommonObjectApi> religionList = new List<CommonObjectApi>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                religionList = _commonHelper.LoadEmumToDictionary<Religion>()
                                .Select(x => new  CommonObjectApi{ Id = x.Key, Name = x.Value })
                                .ToList();
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message, Data = religionList }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult GetGender(string key)
        {
            bool successState = false;
            string message = "";
            List<CommonObjectApi> genderList = new List<CommonObjectApi>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                genderList = _commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined })
                                .Select(x => new CommonObjectApi { Id = x.Key, Name = x.Value })
                                .ToList();
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message, Data = genderList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetTeacherActivity(string key)
        {
            bool successState = false;
            string message = "";
            List<CommonObjectApi> teacherActivityList = new List<CommonObjectApi>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                teacherActivityList = _teacherActivityService.LoadTeacherActivity().Select(x => new CommonObjectApi() { Id = x.Id, Name = x.Name }).ToList();
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message, Data = teacherActivityList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSubject(string key)
        {
            bool successState = false;
            string message = "";
            List<CommonObjectApi> subjectList = new List<CommonObjectApi>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                subjectList = _subjectService.LoadSubject().Select(x => new CommonObjectApi() { Id = x.Id, Name = x.Name }).ToList();
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message, Data = subjectList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetVersionPriority(string key)
        {
            bool successState = false;
            string message = "";
            List<CommonObjectApi> versionPriorityList = new List<CommonObjectApi>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                versionPriorityList = _commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int> { (int)VersionOfStudy.Combined })
                                .Select(x => new CommonObjectApi { Id = x.Key, Name = x.Value })
                                .ToList();
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message, Data = versionPriorityList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetOrganizationByBusinessId(string key, string businessId)
        {
            bool successState = false;
            string message = "";
            List<CommonObjectApi> organizationIdNameList = new List<CommonObjectApi>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                Organization organization = _organizationService.GetByBusinessId(businessId);
                if (organization != null)
                {
                    CommonObjectApi tempCommonObjectApi = new CommonObjectApi
                    {
                        Id = organization.Id,
                        Name = organization.ShortName,
                    };
                    organizationIdNameList.Add(tempCommonObjectApi);
                    successState = true;
                }
                message = "Invalid Organization";
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message, Data = organizationIdNameList }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Registration Process
        [HttpPost]
        public JsonResult Registration(RegistrationJsonData registrationJsonData)
        {
            bool successState = false;
            string message = "";
            try
            {
                if (registrationJsonData == null)
                {
                    throw new InvalidDataException("Invalid Request");
                }
                if (Authenticate(registrationJsonData.Key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                if (String.IsNullOrEmpty(registrationJsonData.TeacherRegistrationViewModelString))
                {
                    throw new InvalidDataException("Invalid Request");
                }
                TeacherRegistrationViewModel teacherRegistrationViewModel = new JavaScriptSerializer().Deserialize<TeacherRegistrationViewModel>(registrationJsonData.TeacherRegistrationViewModelString);
               
                #region fill teacher public register 

                int HscPassingYear,religion,gender, versionPriority1, versionPriority2;
                long teacherActivity1, teacherActivity2, teacherActivity3, subject1, subject2, subject3, organizationId;

                DateTime dateTime = DateTime.Now;
                long createdBy = Convert.ToInt64(ConfigurationManager.AppSettings["UserId"]);
                long modifiedBy = createdBy;

                TeacherPublicRegistration teacherPublicRegistration = new TeacherPublicRegistration();

                teacherPublicRegistration.FullName = (!String.IsNullOrEmpty(teacherRegistrationViewModel.FullName)) ? teacherRegistrationViewModel.FullName.Trim() : teacherRegistrationViewModel.FullName;
                teacherPublicRegistration.NickName = (!String.IsNullOrEmpty(teacherRegistrationViewModel.NickName)) ? teacherRegistrationViewModel.NickName.Trim() : teacherRegistrationViewModel.NickName;
                teacherPublicRegistration.MobileNumber1 = (!String.IsNullOrEmpty(teacherRegistrationViewModel.MobileNumber1)) ? teacherRegistrationViewModel.MobileNumber1.Trim() : teacherRegistrationViewModel.MobileNumber1;
                teacherPublicRegistration.MobileNumber2 = (!String.IsNullOrEmpty(teacherRegistrationViewModel.MobileNumber2)) ? teacherRegistrationViewModel.MobileNumber2.Trim() : teacherRegistrationViewModel.MobileNumber2;
                teacherPublicRegistration.Email = (!String.IsNullOrEmpty(teacherRegistrationViewModel.EmailAddress)) ? teacherRegistrationViewModel.EmailAddress.Trim() : teacherRegistrationViewModel.EmailAddress;
                teacherPublicRegistration.FacebookId = (!String.IsNullOrEmpty(teacherRegistrationViewModel.FacebookId))?teacherRegistrationViewModel.FacebookId.Trim():teacherRegistrationViewModel.FacebookId;
                teacherPublicRegistration.Institute = (!String.IsNullOrEmpty(teacherRegistrationViewModel.Institute))?teacherRegistrationViewModel.Institute.Trim():teacherRegistrationViewModel.Institute;
                teacherPublicRegistration.Department = (!String.IsNullOrEmpty(teacherRegistrationViewModel.Department)) ? teacherRegistrationViewModel.Department.Trim() : teacherRegistrationViewModel.Department;
                if (int.TryParse((!String.IsNullOrEmpty(teacherRegistrationViewModel.HscPassingYear)) ? teacherRegistrationViewModel.HscPassingYear.Trim() : teacherRegistrationViewModel.HscPassingYear, out HscPassingYear))
                {
                    teacherPublicRegistration.HscPassingYear = HscPassingYear;
                }
                if (int.TryParse(teacherRegistrationViewModel.Religion, out religion))
                {
                    teacherPublicRegistration.Religion = religion;
                }
                if (int.TryParse(teacherRegistrationViewModel.Gender, out gender))
                {
                    teacherPublicRegistration.Gender = gender;
                }

                if (Int64.TryParse(teacherRegistrationViewModel.TeacherActivity1, out teacherActivity1))
                {
                    teacherPublicRegistration.TeacherActivity1 = _teacherActivityService.GetTeacherActivity(teacherActivity1);
                }
                if (Int64.TryParse(teacherRegistrationViewModel.TeacherActivity2, out teacherActivity2))
                {
                    teacherPublicRegistration.TeacherActivity2 = _teacherActivityService.GetTeacherActivity(teacherActivity2);
                }
                if (Int64.TryParse(teacherRegistrationViewModel.TeacherActivity3, out teacherActivity3))
                {
                    teacherPublicRegistration.TeacherActivity3 = _teacherActivityService.GetTeacherActivity(teacherActivity3);
                }

                if (Int64.TryParse(teacherRegistrationViewModel.Subject1, out subject1))
                {
                    teacherPublicRegistration.Subject1 = _subjectService.GetSubject(subject1);
                }
                if (Int64.TryParse(teacherRegistrationViewModel.Subject2, out subject2))
                {
                    teacherPublicRegistration.Subject2 = _subjectService.GetSubject(subject2);
                }
                if (Int64.TryParse(teacherRegistrationViewModel.Subject3, out subject3))
                {
                    teacherPublicRegistration.Subject3 = _subjectService.GetSubject(subject3);
                }

                if (int.TryParse(teacherRegistrationViewModel.VersionPriority1, out versionPriority1))
                {
                    teacherPublicRegistration.VersionOfStudy1 = versionPriority1;
                }
                if (int.TryParse(teacherRegistrationViewModel.VersionPriority2, out versionPriority2))
                {
                    teacherPublicRegistration.VersionOfStudy2 = versionPriority2;
                }

                teacherPublicRegistration.IsUdvashStudent = teacherRegistrationViewModel.IsUdvashStudent;
                teacherPublicRegistration.AppointmentRemarks = teacherRegistrationViewModel.AppointmentRemarks;

                if (Int64.TryParse(teacherRegistrationViewModel.OrganizationId, out organizationId))
                {
                    teacherPublicRegistration.Organization = _organizationService.LoadById(organizationId);
                }

                teacherPublicRegistration.CreateBy = createdBy;
                teacherPublicRegistration.ModifyBy = modifiedBy;
                teacherPublicRegistration.CreationDate = dateTime;
                teacherPublicRegistration.ModificationDate = dateTime;
                teacherPublicRegistration.Status = TeacherPublicRegistration.EntityStatus.Active;

                #endregion

                _teacherPublicRegistrationService.SaveTeacherRegistration(teacherPublicRegistration);
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message }, JsonRequestBehavior.AllowGet); 
        }

        #endregion
    }

    public class RegistrationJsonData
    {
        public virtual string Key { get; set; }
        public virtual string TeacherRegistrationViewModelString { get; set; }
    }
}