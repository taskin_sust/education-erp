﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BatchTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Batch")]
    public class BatchUpdateTest : BatchBaseTest
    {

        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var batchFactory = _batchFactory.Create().WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            Assert.Throws<NullObjectException>(() => _batchService.Update(batchFactory.Object.Id, null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var batchFactory = _batchFactory.Create().WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            var batch = batchFactory.Object;
            batch.Name = batch.Name + " gfghfghfhg";
            batch.Days = "Sun, Tue, Thu, Fri";
            batch.StartTime = _startTime.AddDays(1);
            batch.EndTime = _endTime.AddDays(1);
            var success = _batchService.Update(batchFactory.Object.Id, batch);
            Assert.True(success);
        }

        [Fact]
        public void Update_Should_Check_Empty_Day()
        {
            var batchFactory = _batchFactory.Create().WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.Days = "";
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Update_Should_Check_Empty_StartTime()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.StartTime = null;
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Update_Should_Check_Empty_EndTime()
        {
            var batchFactory = _batchFactory.Create()
                     .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.EndTime = null;
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Update_Should_Check_Empty_Name()
        {
            var batchFactory = _batchFactory.Create()
                     .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.Name = "";
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Update_Should_Check_Empty_Capacity()
        {
            var batchFactory = _batchFactory.Create()
                     .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.Capacity = null;
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Update_Should_Check_Empty_VersionOfStudy()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.VersionOfStudy = 0;
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Update_Should_Check_Empty_Gender()
        {
            var batchFactory =
                _batchFactory.Create().WithNameAndRank(_name, 1)
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.Gender = 0;
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Update_Should_Check_Empty_Program()
        {
            var batchFactory =
                 _batchFactory.Create().WithNameAndRank(_name, 1)
                     .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.Program = null;
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Update_Should_Check_Empty_Branch()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.Branch = null;
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Update_Should_Check_Empty_Session()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.Session = null;
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Update_Should_Check_Empty_Campus()
        {
            var batchFactory = _batchFactory.Create()
                     .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            batchFactory.Object.Campus = null;
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        [Fact]
        public void Should_check_datetime_properly()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, DateTime.Now.Add(new TimeSpan(10, 10, 10)), DateTime.Now, 100, 1, (int)Gender.Male);
            Assert.Throws<MessageException>(() => _batchService.Update(batchFactory.Object.Id, batchFactory.Object));
        }

        #endregion
    }
}
