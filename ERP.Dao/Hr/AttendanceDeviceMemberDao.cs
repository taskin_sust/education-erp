﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface IAttendanceDeviceMemberDao : IBaseDao<AttendanceDeviceMember, long>
    {
        #region Single Instances Loading Function

        AttendanceDeviceMember GetAttendanceDeviceMemberByEnrollDeviceId(int enroll, long deviceId);
        AttendanceDeviceMember GetAttendanceDeviceMember(long memberId = 0, long deviceTypeId = 0);
        #endregion

        #region List Loading Function

        IList<AttendanceDeviceMember> LoadAttendanceDeviceMember(long teamMemberId);
        IList<AttendanceDeviceMemberDto> LoadMemberInfos(AttendanceDevice attendanceDevice);
        IList<AttendanceDeviceMember> LoadDeviceMember(long deviceId);
        IList<AttendanceDeviceMember> LoadAttendanceMember(long deviceId, List<int> enrolList);
        #endregion

        #region Others Function

        int GetMaxEnrollNo(long deviceId);

        #endregion

        bool HasDeviceMember(long deviceId);

    }
    public class AttendanceDeviceMemberDao : BaseDao<AttendanceDeviceMember, long>, IAttendanceDeviceMemberDao
    {
        #region Single Instances Loading Function

        public AttendanceDeviceMember GetAttendanceDeviceMemberByEnrollDeviceId(int enroll, long deviceId)
        {
            var criteria = Session.CreateCriteria<AttendanceDeviceMember>();
            // criteria.Add(Restrictions.Not(Restrictions.Eq("Status", AttendanceDevice.EntityStatus.Delete)));

            if (enroll > 0)
                criteria.Add(Restrictions.Eq("EnrollNo", enroll));
            if (deviceId > 0)
            {
                criteria.CreateAlias("AttendanceDevice", "ad");
                criteria.Add(Restrictions.Eq("ad.Id", deviceId));
            }
            return criteria.UniqueResult<AttendanceDeviceMember>();
        }

        public AttendanceDeviceMember GetAttendanceDeviceMember(long memberId = 0, long deviceId = 0)
        {
            var criteria = Session.CreateCriteria<AttendanceDeviceMember>();          
            if (memberId > 0)
            {
                criteria.CreateAlias("TeamMember", "tm");
                criteria.Add(Restrictions.Eq("tm.Id", memberId));
            }
            if (deviceId > 0)
            {
                criteria.CreateAlias("AttendanceDevice", "ad");
                criteria.Add(Restrictions.Eq("ad.Id", deviceId));
            }
            return criteria.UniqueResult<AttendanceDeviceMember>();
        }

        #endregion

        #region List Loading Function

        public IList<AttendanceDeviceMember> LoadAttendanceDeviceMember(long teamMemberId)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(AttendanceDeviceMember));
            criteria.CreateCriteria("TeamMember", "teamMember").Add(Restrictions.Eq("teamMember.Status", TeamMember.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("teamMember.Id", teamMemberId));
            return criteria.List<AttendanceDeviceMember>();
        }


        public IList<AttendanceDeviceMember> LoadDeviceMember(long deviceId)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(AttendanceDeviceMember));
            criteria.CreateCriteria("AttendanceDevice", "attendanceDevice").Add(Restrictions.Eq("attendanceDevice.Status", AttendanceDevice.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("attendanceDevice.Id", deviceId));
            return criteria.List<AttendanceDeviceMember>();
        }

        public IList<AttendanceDeviceMemberDto> LoadMemberInfos(AttendanceDevice attendanceDevice)
        {
            string condition = "";
            if (attendanceDevice.IsReset == false)
            {
                condition += string.Format(@" AND (dm.LastSyncedDateTime IS NULL or dm.LastSyncedDateTime <= dm.LastUpdateDateTime)");
            }

            string query = string.Format(@"SELECT   dm.AttendanceDeviceId ,
                                                    dm.EnrollNo ,
                                                    dm.LastUpdateDateTime ,
                                                    dm.IsSynced ,
                                                    dm.LastSyncedDateTime ,
                                                    tm.Id AS TeamMemberId ,
                                                    tm.Name ,
                                                    tm.FullNameEng AS FullName ,
                                                    tm.Pin ,
                                                    od.CardNo ,
                                                    od.Status ,
                                                    fp.Index0 ,
                                                    fp.Index1 ,
                                                    fp.Index2 ,
                                                    fp.Index3 ,
                                                    fp.Index4 ,
                                                    fp.Index5 ,
                                                    fp.Index6 ,
                                                    fp.Index7 ,
                                                    fp.Index8 ,
                                                    fp.Index9
                                            FROM    HR_AttendanceDeviceMember dm
                                                    INNER JOIN HR_TeamMember tm ON tm.Id = dm.TeamMemberId
                                                    LEFT JOIN HR_MemberOfficialDetail od ON od.TeamMemberId = tm.Id
                                                    LEFT JOIN HR_MemberFingerPrint fp ON fp.TeamMemberId = tm.Id
                                            WHERE   1 = 1

                                            AND AttendanceDeviceId ={0} "
                                            , attendanceDevice.Id);


            if (!String.IsNullOrEmpty(condition))
            {
                query += condition;
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<AttendanceDeviceMemberDto>());
            return iQuery.List<AttendanceDeviceMemberDto>();
        }

        public IList<AttendanceDeviceMember> LoadAttendanceMember(long deviceId, List<int> enrolList)
        {
            var critera = Session.CreateCriteria<AttendanceDeviceMember>();
            critera.CreateAlias("AttendanceDevice", "ad").Add(Restrictions.Eq("ad.Status", AttendanceDevice.EntityStatus.Active));
            critera.Add(Restrictions.Eq("ad.Id", deviceId));
            critera.Add(Restrictions.In("EnrollNo", enrolList));
            return critera.List<AttendanceDeviceMember>();
        }
        #endregion

        #region Others Function

        public int GetMaxEnrollNo(long deviceId)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(AttendanceDeviceMember));
            criteria.CreateAlias("AttendanceDevice", "attendanceDevice").Add(Restrictions.Eq("attendanceDevice.Status", AttendanceDevice.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("attendanceDevice.Id", deviceId));
            criteria.SetProjection(Projections.Max("EnrollNo"));
            if (criteria.UniqueResult() != null)
            {
                var returnValue = (int)criteria.UniqueResult();
                return returnValue + 1;
            }
            else
            {
                return 1;
            }
        }

        #endregion

        public bool HasDeviceMember(long deviceId)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceDeviceMember>();
            criteria.CreateAlias("AttendanceDevice", "attendanceDevice").Add(Restrictions.Eq("attendanceDevice.Status", AttendanceDevice.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("attendanceDevice.Id", deviceId));
            criteria.SetTimeout(3000);
            criteria.SetProjection(Projections.RowCount());
            bool returnValue = false;
            int count = Convert.ToInt32(criteria.UniqueResult());
            return returnValue = count > 0 ? true : false;
        }
    }
}
