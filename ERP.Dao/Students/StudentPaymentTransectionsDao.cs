﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Students
{
    public interface IStudentPaymentTransectionsDao : IBaseDao<StudentPaymentTransections, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        bool TransactionsSettlementFinalPost(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, List<long> studentPaymentTransectionsIdList, long userId);
        #endregion

        #region List Loading Function
        IList<StudentPaymentTransections> GetTransactionReportVat(long[] sessionId, long[] programId, long[] branchId, long[] campusId, long[] authorizedPrograms, long[] authorizedSessions, List<long> authorizedBranches, long[] authorizedCampuses, DateTime datef, DateTime datet, int paymentMethod, int start, int length, int orderType);
        IList<StudentPaymentTransections> LoadTransactionsSettlementCalculate(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, decimal totalAmount);
        List<dynamic> LoadBranchWiseSummery(long branchId, DateTime dateFrom, DateTime dateTo);
        #endregion

        #region Others Function
        int GetTransactionReportCountVat(long[] sessionId, long[] programId, long[] branchId, long[] campusId, long[] authorizedPrograms, long[] authorizedSessions, List<long> authorizedBranches, long[] authorizedCampuses, DateTime datef, DateTime datet, int paymentMethod, int orderType);

        DateTime GetTransactionsSettlementStartDate(long sessionId, long programId, long branchId, long campusId);

        decimal GetTotalCollectionAmountByReferenceId(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, long referenceId);
        decimal GetTotalCollectionAmountByPaymentMethod(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, int paymentMethod);
        decimal GetPreSettledAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo);
        #endregion

    }

    public class StudentPaymentTransectionsDao : BaseDao<StudentPaymentTransections, long>, IStudentPaymentTransectionsDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function
        public bool TransactionsSettlementFinalPost(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, List<long> studentPaymentTransectionsIdList, long userId)
        {
            bool status = true;
            string idList = "0";
            foreach (long i in studentPaymentTransectionsIdList)
            {
                if (idList == "0")
                    idList = i.ToString();
                else
                    idList += "," + i.ToString();
            }
            string query = @"
                            UPDATE [dbo].[StudentPaymentTransections]
                            SET [IsSubmitted] = 1, [VersionNumber] = [VersionNumber] + 1, [ModificationDate] = getdate(), [ModifyBy] = " + userId.ToString() + @"
                            WHERE [IsSubmitted] = 0  
                                AND Id IN (" + idList + @")
                                AND [SessionId] = " + sessionId + @"
                                AND [ProgramId] = " + programId + @"
                                AND [BranchId] = " + branchId + @"
                                AND [CampusId] = " + campusId + @"
                                AND [ReceivedDate] BETWEEN '" + dateFrom.ToString("yyyy-MM-dd HH:mm:dd") + @"' AND '" + dateTo.ToString("yyyy-MM-dd HH:mm:dd") + @"';

                            UPDATE [dbo].[StudentPaymentTransections]
                            SET [Status] = -404, [VersionNumber] = [VersionNumber] + 1, [ModificationDate] = getdate(), [ModifyBy] = " + userId.ToString() + @"
                            WHERE [IsSubmitted] = 0  
                                AND Id NOT IN (" + idList + @")
                                AND [SessionId] = " + sessionId + @"
                                AND [ProgramId] = " + programId + @"
                                AND [BranchId] = " + branchId + @"
                                AND [CampusId] = " + campusId + @"
                                AND [ReceivedDate] BETWEEN '" + dateFrom.ToString("yyyy-MM-dd HH:mm:dd") + @"' AND '" + dateTo.ToString("yyyy-MM-dd HH:mm:dd") + @"';

                            UPDATE [dbo].[StudentPaymentTransections]
                            SET [StudentPaymentTransections].IsExistingStudent = (
	                            SELECT CAST(IIF ( COUNT(*) > 0, 1, 0 ) AS BIT) 
	                            FROM [StudentPaymentTransections] as t 
	                            WHERE t.Id<[StudentPaymentTransections].Id 
		                            AND t.StudentProgramId = [StudentPaymentTransections].StudentProgramId
		                            AND t.Status = 1 
		                            AND t.IsSubmitted = 1
                            )
                            WHERE [Status] = 1 AND [IsSubmitted] = 1 
                                AND Id IN (" + idList + @")
                                AND [SessionId] = " + sessionId + @"
                                AND [ProgramId] = " + programId + @"
                                AND [BranchId] = " + branchId + @"
                                AND [CampusId] = " + campusId + @"
                                AND [ReceivedDate] BETWEEN '" + dateFrom.ToString("yyyy-MM-dd HH:mm:dd") + @"' AND '" + dateTo.ToString("yyyy-MM-dd HH:mm:dd") + @"';
                ";
            using (var txn = Session.BeginTransaction())
            {
                try
                {
                    IQuery iQuery = Session.CreateSQLQuery(query);
                    iQuery.ExecuteUpdate();
                    txn.Commit();
                }
                catch (Exception ex)
                {
                    txn.Rollback();
                    status = false;
                }
            }
            //iQuery.SetTimeout(5000);
            //return iQuery.List<StudentPaymentTransections>().ToList();
            return status;
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<StudentPaymentTransections> GetTransactionReportVat(long[] sessionId, long[] programId, long[] branchId, long[] campusId, long[] authorizedPrograms, long[] authorizedSessions, List<long> authorizedBranches, long[] authorizedCampuses, DateTime datef, DateTime datet, int paymentMethod, int start, int length, int orderType)
        {
            var criteria = GetTransactionReportCriteriaVat(sessionId, programId, branchId, campusId, authorizedPrograms, authorizedSessions, authorizedBranches, authorizedCampuses, datef, datet, paymentMethod);
            if (orderType == 1)
            {
                criteria = criteria.AddOrder(Order.Asc("ReceivedDate")).AddOrder(Order.Asc("Serial1"));
            }
            else
            {
                criteria = criteria
                    //.AddOrder(Order.Asc("AdmissionDate"))  
                    //.AddOrder(Order.Asc("ReceivedDate"))
                            .AddOrder(Order.Asc("Serial1"))
                    //.SetProjection(Projections.Distinct(Projections.ProjectionList()
                    //    .Add(Projections.Property("Serial1"), "Serial1")))
                    //.SetResultTransformer(Transformers.AliasToBean())
                    //.SetProjection(Projections.Distinct(Projections.Property("Serial1")))
                            ;
                //ICriteria criteria = session.CreateCriteria(typeof(Person));
                criteria.SetProjection(
                    Projections.Distinct(Projections.ProjectionList()
                        .Add(Projections.Alias(Projections.Property("StudentFullName"), "StudentFullName"))
                        .Add(Projections.Alias(Projections.Property("StudentNickName"), "StudentNickName"))
                        .Add(Projections.Alias(Projections.Property("Serial1"), "Serial1"))));

                //IList<StudentPaymentTransections> studentPaymentsList1 = criteria.List<StudentPaymentTransections>();
                //return
            }
            if (length > 0)
            {
                criteria = criteria.SetFirstResult(start).SetMaxResults(length);
            }
            if (orderType == 3)
            {
                criteria.SetResultTransformer(
                    new NHibernate.Transform.AliasToBeanResultTransformer(typeof(StudentPaymentTransections)));
            }
            IList<StudentPaymentTransections> studentPaymentsList = criteria.List<StudentPaymentTransections>();
            return studentPaymentsList;
        }

        public IList<StudentPaymentTransections> LoadTransactionsSettlementCalculate(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, decimal totalAmount)
        {
            string query = @"
                            SELECT DISTINCT * INTO #StudentTransections 
                            FROM (
	                            SELECT ROW_NUMBER() over (ORDER BY [Priority], [Rank], [ReceivedDate]) AS tId
		                            , Cast( 0 as decimal(38,2)) AS BAmount
		                            , [Id], [VersionNumber], [BusinessId], [CreationDate], [ModificationDate], [Status], [CreateBy], [ModifyBy]
                                    , [ProgramId], [SessionId], [BranchId], [CampusId], [Serial1], [Serial2], [StudentProgramId]
                                    , [StudentFullName], [StudentNickName], [AdmissionDate], [ReceivedDate], [ReceivedAmount]
                                    , [PaymentMethod], [PaymentType], [Remarks], [SpReferenceId], [SpReferenceNote], [Priority], [IsSubmitted], [StudentPaymentId]
									, (SELECT TOP 1 CAST(IIF ( COUNT(*) > 0, 1, 0 ) AS BIT) FROM StudentPaymentTransections t WHERE t.StudentProgramId=a.[StudentProgramId] AND t.Status = 1 AND t.id<a.Id) AS IsExistingStudent 
	                            FROM (
		                            SELECT DENSE_RANK() OVER(PARTITION BY CONVERT(VARCHAR,[ReceivedDate],112) ORDER BY [Priority] ASC, CONVERT(VARCHAR,[ReceivedDate],112) ASC, NEWID() ASC) AS [Rank]
			                            , *
		                            FROM (
			                            SELECT *		
			                            FROM [dbo].[StudentPaymentTransections]
			                            WHERE [Status] = 1
				                            AND IsSubmitted = 0
				                            AND ReceivedAmount > 0
                                            AND SessionId = " + sessionId.ToString() + @"
                                            AND ProgramId = " + programId.ToString() + @"
                                            AND BranchId = " + branchId.ToString() + @"
                                            AND CampusId = " + campusId.ToString() + @"
				                            AND [ReceivedDate]>='" + dateFrom.ToString("yyyy-MM-dd HH:mm:ss") + @"' AND [ReceivedDate]<='" + dateTo.ToString("yyyy-MM-dd HH:mm:ss") + @"' 
		                            ) as a
	                            ) AS a
                            ) as a;

                            --TEMP TABLE BAmount Generation
                            UPDATE #StudentTransections
                            SET #StudentTransections.BAmount = (SELECT Cast( SUM(ReceivedAmount) as decimal(38,2)) FROM #StudentTransections WHERE tId <= t1.tId)
                            FROM #StudentTransections t1;

                            SELECT * 
                            FROM #StudentTransections
                            WHERE BAmount <= " + totalAmount.ToString() + @"
                            ORDER BY tId ASC;

                            --TEMP TABLE DELETE
                            IF EXISTS(SELECT * FROM #StudentTransections) DROP TABLE #StudentTransections;
                ";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentPaymentTransections>());
            //iQuery.SetTimeout(5000);
            return iQuery.List<StudentPaymentTransections>().ToList();
        }

        public List<dynamic> LoadBranchWiseSummery(long branchId, DateTime dateFrom, DateTime dateTo)
        {
            string query = @"
                            SELECT 
	                            a.BranchId, a.CampusId, a.ProgramId, a.SessionId
	                            , MAX(c.Name) Campus
	                            , MAX(p.ShortName) + ' - ' +MAX(s.Name) ProgramSession
	                            , SUM(a.PreviousReceivedOfThisMonth) AS AlreadySettledAmount
	                            , SUM(a.NetCollection) AS NetCollection
	                            , SUM(a.NotSettledAmount) AS NotSettledAmount
	                            , SUM(a.VatTaxBkash) AS VatTaxBkash
	                            , SUM(a.TotalVatTaxBkash) AS TotalVatTaxBkash
	                            , CASE WHEN SUM(a.NotSettledAmount) > 0 THEN CEILING(SUM(a.VatTaxBkash)/SUM(a.NotSettledAmount)*100) ELSE 0 END AS MinPercent
	                            , CASE WHEN SUM(a.NotSettledAmount) > 0 THEN SUM(a.VatTaxBkash)/SUM(a.NotSettledAmount)*100 ELSE 0 END AS OriginalMinPercent
								, SUBSTRING(CONVERT(varchar, MAX(AlreadySettledDate), 113),1,11) AlreadySettledDate
                            FROM (
	                            SELECT BranchId, CampusId, ProgramId,SessionId, IsSubmitted
		                            , CASE WHEN IsSubmitted = 1 AND Status = 1 THEN SUM(ReceivedAmount) ELSE 0 END AS PreviousReceivedOfThisMonth
		                            , CASE WHEN IsSubmitted = 0 AND Status = 1 THEN SUM(ReceivedAmount) ELSE 0 END AS NotSettledAmount
									, CASE WHEN IsSubmitted = 0 AND Status = 1 AND (SpReferenceId IN (39,40) OR PaymentMethod = 2) THEN SUM(ReceivedAmount) ELSE 0 END AS VatTaxBkash
									, CASE WHEN (SpReferenceId IN (39,40) OR PaymentMethod = 2) THEN SUM(ReceivedAmount) ELSE 0 END AS TotalVatTaxBkash
		                            , SUM(ReceivedAmount) NetCollection
		                            , CASE WHEN IsSubmitted = 1 OR Status = -404 THEN MAX(ReceivedDate) ELSE GETDATE()-1000 END AS AlreadySettledDate
	                            FROM [dbo].[StudentPaymentTransections]
	                            WHERE BranchId IN (" + branchId + @")
		                            AND ReceivedDate >= '" + dateFrom.ToString("yyyy-MM-dd HH:mm:ss") + @"' -- FIRST Day  
		                            AND ReceivedDate <= '" + dateTo.ToString("yyyy-MM-dd HH:mm:ss") + @"' -- LAST Day 
	                            GROUP BY BranchId, CampusId, ProgramId,SessionId, IsSubmitted, SpReferenceId, PaymentMethod, Status
                            ) AS a
                            LEFT JOIN Campus AS c ON c.id = a.CampusId
                            LEFT JOIN Program AS p ON p.id = a.ProgramId
                            LEFT JOIN [Session] AS s ON s.id = a.SessionId
                            GROUP BY a.BranchId, a.CampusId, a.ProgramId, a.SessionId
                ";
            IQuery iQuery = Session.CreateSQLQuery(query);

            return iQuery.List<dynamic>().ToList();
        }
        #endregion

        #region Others Function
        public int GetTransactionReportCountVat(long[] sessionId, long[] programId, long[] branchId, long[] campusId, long[] authorizedPrograms, long[] authorizedSessions, List<long> authorizedBranches, long[] authorizedCampuses, DateTime datef, DateTime datet, int paymentMethod, int orderType)
        {
            var transactionReportCriteria = GetTransactionReportCriteriaVat(sessionId, programId, branchId, campusId, authorizedPrograms, authorizedSessions, authorizedBranches, authorizedCampuses, datef, datet, paymentMethod);
            var rowCount = 0;
            if (orderType == 1)
            {
                rowCount = transactionReportCriteria.SetProjection(Projections.RowCount()).UniqueResult<int>();
            }
            else
            {
                //rowCount = transactionReportCriteria
                //    .SetProjection(Projections.Distinct(Projections.ProjectionList()
                //        .Add(Projections.Property("Serial1"), "Serial1")))
                //    .SetProjection(Projections.RowCount()).UniqueResult<int>();
                transactionReportCriteria = transactionReportCriteria.SetProjection(
                    Projections.Distinct(Projections.ProjectionList()
                        .Add(Projections.Alias(Projections.Property("StudentFullName"), "StudentFullName"))
                        .Add(Projections.Alias(Projections.Property("StudentNickName"), "StudentNickName"))
                        .Add(Projections.Alias(Projections.Property("Serial1"), "Serial1"))));
                //transactionReportCriteria.SetProjection(Projections.RowCount());

                transactionReportCriteria.SetResultTransformer(
                    new NHibernate.Transform.AliasToBeanResultTransformer(typeof(StudentPaymentTransections)));
                rowCount = transactionReportCriteria.List().Count;
                //.SetProjection(Projections.Distinct(Projections.Property("Serial1")))
                //.SetProjection(Projections.RowCount()).UniqueResult<int>();


            }
            return rowCount;
        }

        public DateTime GetTransactionsSettlementStartDate(long sessionId, long programId, long branchId, long campusId)
        {
            ICriteria criteria = Session.CreateCriteria<StudentPaymentTransections>()
                                .Add(Restrictions.Eq("Status", StudentPaymentTransections.EntityStatus.Active))
                                .Add(Restrictions.Eq("IsSubmitted", false))
                                .Add(Restrictions.Eq("ProgramId", programId))
                                .Add(Restrictions.Eq("SessionId", sessionId))
                                .Add(Restrictions.Eq("BranchId", branchId))
                                .Add(Restrictions.Eq("CampusId", campusId));
            //var s = criteria.List();
            var spt = criteria.SetProjection(Projections.Min("ReceivedDate")).UniqueResult<DateTime>();
            return spt;
        }

        public decimal GetTotalCollectionAmountByReferenceId(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, long referenceId)
        {
            ICriteria criteria = Session.CreateCriteria<StudentPaymentTransections>()
                                .Add(Restrictions.Eq("Status", StudentPaymentTransections.EntityStatus.Active))
                                .Add(Restrictions.Eq("IsSubmitted", false))
                                .Add(Restrictions.Eq("SpReferenceId", referenceId))
                                .Add(Restrictions.IsNotNull("ReceivedAmount"))
                                .Add(Restrictions.Eq("ProgramId", programId))
                                .Add(Restrictions.Eq("SessionId", sessionId))
                                .Add(Restrictions.Eq("BranchId", branchId))
                                .Add(Restrictions.Eq("CampusId", campusId))
                                .Add(Restrictions.Ge("ReceivedDate", dateFrom))
                                .Add(Restrictions.Le("ReceivedDate", dateTo));

            return criteria.SetProjection(Projections.Sum("ReceivedAmount")).UniqueResult<decimal>();
        }

        public decimal GetTotalCollectionAmountByPaymentMethod(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, int paymentMethod)
        {
            ICriteria criteria = Session.CreateCriteria<StudentPaymentTransections>()
                                .Add(Restrictions.Eq("Status", StudentPaymentTransections.EntityStatus.Active))
                                .Add(Restrictions.Eq("IsSubmitted", false))
                                .Add(Restrictions.Eq("PaymentMethod", paymentMethod))
                                .Add(Restrictions.IsNotNull("ReceivedAmount"))
                                .Add(Restrictions.Eq("ProgramId", programId))
                                .Add(Restrictions.Eq("SessionId", sessionId))
                                .Add(Restrictions.Eq("BranchId", branchId))
                                .Add(Restrictions.Eq("CampusId", campusId))
                                .Add(Restrictions.Ge("ReceivedDate", dateFrom))
                                .Add(Restrictions.Le("ReceivedDate", dateTo));

            return criteria.SetProjection(Projections.Sum("ReceivedAmount")).UniqueResult<decimal>();
        }

        public decimal GetPreSettledAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo)
        {
            ICriteria criteria = Session.CreateCriteria<StudentPaymentTransections>()
                                .Add(Restrictions.Eq("Status", StudentPaymentTransections.EntityStatus.Active))
                                .Add(Restrictions.Eq("IsSubmitted", true))
                                .Add(Restrictions.IsNotNull("ReceivedAmount"))
                                .Add(Restrictions.Eq("ProgramId", programId))
                                .Add(Restrictions.Eq("SessionId", sessionId))
                                .Add(Restrictions.Eq("BranchId", branchId))
                                .Add(Restrictions.Eq("CampusId", campusId))
                                .Add(Restrictions.Ge("ReceivedDate", dateFrom))
                                .Add(Restrictions.Le("ReceivedDate", dateTo));

            return criteria.SetProjection(Projections.Sum("ReceivedAmount")).UniqueResult<decimal>();
        }
        #endregion

        #region Helper Function
        private ICriteria GetTransactionReportCriteriaVat(long[] sessionId, long[] programId, long[] branchId, long[] campusId, long[] authorizedPrograms, long[] authorizedSessions, List<long> authorizedBranches, long[] authorizedCampuses, DateTime datef, DateTime datet, int paymentMethod)
        {
            ICriteria criteria = Session.CreateCriteria<StudentPaymentTransections>().Add(Restrictions.Eq("Status", StudentPayment.EntityStatus.Active));

            if (!programId.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("ProgramId", programId));
            }
            else
            {
                criteria.Add(Restrictions.In("ProgramId", authorizedPrograms));
            }
            if (!sessionId.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("SessionId", sessionId));
            }
            else
            {
                criteria.Add(Restrictions.In("SessionId", authorizedSessions));
            }

            if (!branchId.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("BranchId", branchId));
            }
            else
            {
                criteria.Add(Restrictions.In("BranchId", authorizedBranches));
            }

            if (!campusId.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("CampusId", campusId));
            }
            else
            {
                criteria.Add(Restrictions.In("CampusId", authorizedCampuses));
            }

            criteria.Add(Restrictions.Ge("ReceivedDate", datef));
            criteria.Add(Restrictions.Le("ReceivedDate", datet));

            if (paymentMethod > 0)
            {
                criteria.Add(Restrictions.Eq("PaymentMethod", paymentMethod));
            }

            return criteria;
        }

        #endregion
    }
}