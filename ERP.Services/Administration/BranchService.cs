﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using log4net;
using NHibernate;
using Remotion.Linq.Parsing;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface IBranchService : IBaseService
    {
        #region Operational Function
        bool IsSave(Branch branchObj);
        bool IsUpdate(long branchId, Branch branchObj);
        bool IsDelete(long branchId);
        bool UpdateRank(Branch branch, string action);
        #endregion

        #region Single Instances Loading Function

        Branch GetBranch(long branchId);

        Branch GetBranchByRankNextOrPrevious(int rank, string action);

        #endregion

        #region List Loading Function
        IList<Branch> LoadBranch(List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> branchIdList = null, List<int?> admissionType = null, bool? isCorporate = null);
        IList<Branch> LoadBranch(int start, int length, string orderBy, string orderDir, string organization, string name, string code, string status);
        IList<Branch> LoadAuthorizedBranch(List<UserMenu> userMenu, List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, bool isProgramBranSession = true, bool? isCorporate = null);
        IList<Branch> LoadBranchByProgramSessionForTransaction(long programId, long sessionId, long[] authorizedProgramIds, long[] authorizedSessionIds, List<long> allAuthorizedBranches);
        IList<Branch> LoadBranch(long[] branchIds);
        IList<Branch> LoadBranchByOrganization(long organizationId, bool isCorporate);
        IList<string> LoadAuthorizedBranchNameList(List<UserMenu> userMenu, List<long> organizationIdList, List<long> branchIdList);

        #endregion

        #region Others Function
        int GetMinimumRank(Branch obj);
        int GetMaximumRank(Branch obj);
        int GetBranchCount(string orderBy, string orderDir, string organization, string name, string code, string status);
        List<string> LoadAllCode();
        bool IsCorporateBranch(long branchId);

        #endregion

        #region Public API
        IList<Branch> LoadPublicBranchByProgramSession(string orgBusinessId, long programId, long sessionId);
        #endregion

    }
    public class BranchService : BaseService, IBranchService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Properties & Object Initialization
        private readonly IBranchDao _branchDao;
        private readonly ICampusDao _campusDao;
        private readonly IProgramBranchSessionDao _programBranchSessionDao;
        private readonly IOrganizationDao _organizationDao;
        private CommonHelper _commonHelper;
        public BranchService(ISession session)
        {
            Session = session;
            _branchDao = new BranchDao() { Session = Session };
            _campusDao = new CampusDao() { Session = Session };
            _programBranchSessionDao = new ProgramBranchSessionDao() { Session = Session };
            _organizationDao = new OrganizationDao() { Session = Session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function
        public bool IsSave(Branch branchObj)
        {
            ITransaction transaction = null;
            if (branchObj == null)
            {
                throw new NullObjectException("object can not be null");
            }
            try
            {
                //TypeDescriptor.AddProviderTransparent(
                //new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Branch), typeof(Branch.BranchhMetaData)), typeof(Branch));

                //var validationContext = new ValidationContext(branchObj, null, null);
                //var validationResults = new List<ValidationResult>();
                //var isValid = Validator.TryValidateObject(branchObj, validationContext, validationResults, true);
                //if (!isValid)
                //{
                //    string errorMessage = "";
                //    validationResults.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                //    throw new EmptyFieldException(errorMessage);
                //}
                CheckBeforeSave(branchObj);
                using (transaction = Session.BeginTransaction())
                {
                    if (branchObj.Organization.OrganizationReference == 0)
                    {
                        branchObj.Organization.OrganizationReference = branchObj.Organization.Id;
                    }
                    branchObj.Rank = _branchDao.GetMaximumRank(branchObj) + 1;
                    //TODO: This should not happen;
                    branchObj.Organization = _organizationDao.LoadById(branchObj.Organization.OrganizationReference);
                    _branchDao.Save(branchObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool IsUpdate(long branchId, Branch branchObj)
        {
            ITransaction transaction = null;
            try
            {
                if (branchObj == null)
                    throw new NullObjectException("Branch can't Empty");

                //check before save
                CheckBeforeUpdate(branchObj);
                using (transaction = Session.BeginTransaction())
                {
                    var tempBranchObj = _branchDao.LoadById(branchId);
                    tempBranchObj.Name = branchObj.Name;
                    tempBranchObj.ShortName = branchObj.ShortName;
                    tempBranchObj.Code = branchObj.Code;
                    tempBranchObj.Status = branchObj.Status;
                    tempBranchObj.Organization = _organizationDao.LoadById(branchObj.Organization.OrganizationReference);
                    _branchDao.Update(tempBranchObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (EmptyFieldException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }


        }
        public bool IsDelete(long branchId)
        {
            ITransaction trans = null;
            try
            {
                if (branchId <= 0)
                {
                    throw new NullObjectException("branch id is not valid");
                }
                var erpBranch = _branchDao.LoadById(branchId);
                CheckBeforeDelete(erpBranch);
                using (trans = Session.BeginTransaction())
                {
                    var tempBranchObj = _branchDao.LoadById(branchId);
                    tempBranchObj.Status = Branch.EntityStatus.Delete;
                    _branchDao.Update(tempBranchObj);
                    trans.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool UpdateRank(Branch branch, string action)
        {
            ITransaction trans = null;

            #region old code

            //if (branchOldObj == null || branchUpdateObj == null)
            //{
            //    throw new NullObjectException("Branch can not be empty");
            //}
            //try
            //{
            //    using (trans = Session.BeginTransaction())
            //    {
            //        _branchDao.Update(branchOldObj);
            //        _branchDao.Update(branchUpdateObj);
            //        trans.Commit();
            //        return true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (trans != null && trans.IsActive)
            //        trans.Rollback();
            //    _logger.Error(ex);
            //    throw;
            //}
            #endregion

            try
            {
                if (branch == null)
                {
                    throw new NullObjectException("Branch can not be empty");
                }
                int newRank;
                if (action == "up")
                    newRank = branch.Rank - 1;
                else
                    newRank = branch.Rank + 1;
                var branchOldObj = GetBranchByRankNextOrPrevious(newRank, action);
                if (branchOldObj == null)
                    throw new NullObjectException("Branch can not be empty");
                newRank = branchOldObj.Rank;
                branchOldObj.Rank = branch.Rank;
                branch.Rank = newRank;
                using (trans = Session.BeginTransaction())
                {
                    _branchDao.Update(branchOldObj);
                    _branchDao.Update(branch);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {

                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public Branch GetBranch(long branchId)
        {
            Branch branch;
            try
            {
                branch = _branchDao.LoadById(branchId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return branch;
        }
        public Branch GetBranchByRankNextOrPrevious(int rank, string action)
        {
            Branch branch;
            try
            {
                branch = _branchDao.LoadByRankDirection(rank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return branch;
        }
        #endregion

        #region List Loading Function

        public IList<Branch> LoadBranch(List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> branchIdList = null, List<int?> admissionType = null, bool? isCorporate = null)
        {
            try
            {
                return _branchDao.LoadBranch(organizationIdList, programIdList, sessionIdList, branchIdList, admissionType, isCorporate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Branch> LoadAuthorizedBranch(List<UserMenu> userMenu, List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, bool isProgramBranSession = true, bool? isCorporate = null)
        {
            IList<Branch> branches;
            try
            {
                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationIdList != null && !organizationIdList.Contains(SelectionType.SelelectAll)) ? organizationIdList : null);
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu, authoOrganizationIdList, null, CommonHelper.ConvertSelectedAllIdList(programIdList));
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationIdList, authProgramIdList);
                branches = _branchDao.LoadAuthorizedBranch(authBranchIdList, CommonHelper.ConvertSelectedAllIdList(authProgramIdList), CommonHelper.ConvertSelectedAllIdList(sessionIdList), isProgramBranSession, isCorporate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return branches;
        }

        public IList<Branch> LoadBranch(int start, int length, string orderBy, string orderDir, string organization, string name, string code, string status)
        {
            IList<Branch> branches;
            try
            {
                branches = _branchDao.LoadBranch(start, length, orderBy, orderDir, organization, name, code, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return branches;
        }

        public IList<Branch> LoadBranchByProgramSessionForTransaction(long programId, long sessionId, long[] authorizedProgramIds, long[] authorizedSessionIds, List<long> allAuthorizedBranches)
        {
            IList<Branch> branches;
            try
            {
                branches = _branchDao.LoadBranchByProgramSessionForTransaction(programId, sessionId, authorizedProgramIds, authorizedSessionIds, allAuthorizedBranches);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return branches;
        }

        public IList<Branch> LoadBranch(long[] branchIds)
        {
            try
            {
                return _branchDao.LoadBranch(branchIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Branch> LoadBranchByOrganization(long organizationId, bool withOutCorporate)
        {
            try
            {
                return _branchDao.LoadBranchByOrganization(organizationId, withOutCorporate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<string> LoadAuthorizedBranchNameList(List<UserMenu> userMenu, List<long> organizationIdList, List<long> branchIdList)
        {
            List<long> branchIds = new List<long>();
            try
            {
                if (branchIdList.Count == 1 && branchIdList[0] == 0) branchIds = null;
                else if (branchIdList.Count > 0)
                    branchIds.AddRange(branchIdList);
                else
                    branchIds = null;
                branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchIds);
                var branchNameList = _branchDao.LoadBranchNameList(branchIdList);
                return branchNameList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        public int GetMinimumRank(Branch obj)
        {
            try
            {
                if (obj == null)
                    throw new NullObjectException("Branch Can not Null");
                return _branchDao.GetMinimumRank(obj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public int GetMaximumRank(Branch obj)
        {
            try
            {
                if (obj == null)
                    throw new NullObjectException("Branch Can not Null");
                return _branchDao.GetMaximumRank(obj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public int GetBranchCount(string orderBy, string orderDir, string organization, string name, string code, string status)
        {
            int branchCount;
            try
            {
                branchCount = _branchDao.GetBranchCount(orderBy, orderDir, organization, name, code, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return branchCount;
        }

        public List<string> LoadAllCode()
        {
            try
            {
                return _branchDao.LoadAllCode();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Helper Function
        private void CheckBeforeSave(Branch branchObj)
        {
            string fieldName;
            CustomModelValidationCheck.DataAnnotationCheck(branchObj, new Branch.BranchhMetaData());
            var checkDuplicateName = _branchDao.IsDuplicateBranch(out fieldName, branchObj.Organization.OrganizationReference, branchObj.Name, null, null, branchObj.Id);
            if (checkDuplicateName)
            {
                if (fieldName == "Name")
                    throw new DuplicateEntryException("Duplicate branch found");
            }
            var checkDuplicateShortName = _branchDao.IsDuplicateBranch(out fieldName, branchObj.Organization.OrganizationReference, null, branchObj.ShortName, null, branchObj.Id);
            if (checkDuplicateShortName)
            {
                if (fieldName == "ShortName")
                    throw new DuplicateEntryException("Duplicate short name found");
            }
            var checkDuplicateCode = _branchDao.IsDuplicateBranch(out fieldName, branchObj.Organization.OrganizationReference, null, null, branchObj.Code, branchObj.Id);
            if (checkDuplicateCode)
            {
                if (fieldName == "Code")
                    throw new DuplicateEntryException("Duplicate code found");
            }

        }
        private void CheckBeforeUpdate(Branch branchObj)
        {
            string fieldName;
            CustomModelValidationCheck.DataAnnotationCheck(branchObj, new Branch.BranchhMetaData());
            var checkDuplicateName = _branchDao.IsDuplicateBranch(out fieldName, branchObj.Organization.OrganizationReference, branchObj.Name, null, null, branchObj.Id);
            if (checkDuplicateName)
            {
                if (fieldName == "Name")
                    throw new DuplicateEntryException("Duplicate branch found");
            }
            var checkDuplicateShortName = _branchDao.IsDuplicateBranch(out fieldName, branchObj.Organization.OrganizationReference, null, branchObj.ShortName, null, branchObj.Id);
            if (checkDuplicateShortName)
            {
                if (fieldName == "ShortName")
                    throw new DuplicateEntryException("Duplicate short name found");
            }
            var checkDuplicateCode = _branchDao.IsDuplicateBranch(out fieldName, branchObj.Organization.OrganizationReference, null, null, branchObj.Code, branchObj.Id);
            if (checkDuplicateCode)
            {
                if (fieldName == "Code")
                    throw new DuplicateEntryException("Duplicate code found");
            }
            if (branchObj.Status == Branch.EntityStatus.Inactive)
            {
                var branchHasActiveCampus = _campusDao.GetActiveBranchCampuCount(branchObj);
                if (branchHasActiveCampus > 0)
                    throw new DependencyException("This Branch Has " + branchHasActiveCampus + " Active Campus");
            }

        }
        private void CheckBeforeDelete(Branch erpBranch)
        {

            long count = _programBranchSessionDao.HasManyBranchObject(erpBranch.Id);
            if (count > 0)
                throw new DependencyException();
            long campusCount = _campusDao.HasManyCampusObject(erpBranch.Id);
            if (campusCount > 0)
                throw new DependencyException();
        }

        public bool IsCorporateBranch(long branchId)
        {
            try
            {
                if (branchId <= 0) throw new InvalidDataException("Invalid branch Id");
                var branch = _branchDao.LoadById(branchId);
                if (branch == null) throw new InvalidDataException("Invalid branch Id");
                return branch.IsCorporate;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Public API
        public IList<Branch> LoadPublicBranchByProgramSession(string orgBusinessId, long programId, long sessionId)
        {
            IList<Branch> branches;
            try
            {
                branches = _branchDao.LoadPublicBranchByProgramSession(orgBusinessId, programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return branches;
        }
        #endregion
    }
}
