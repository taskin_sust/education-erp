﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class DirectWorkOrderController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IItemGroupService _itemGroupService;
        private readonly IItemService _itemService;
        private readonly ISupplierService _supplierService;
        private readonly IUserService _userService;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly IProgramSessionItemService _programSessionItemService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ISessionService _sessionService;
        private readonly IBranchService _branchService;
        private readonly IWorkOrderService _workOrderService;
        private List<UserMenu> _userMenu;

        public DirectWorkOrderController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _itemGroupService = new ItemGroupService(session);
            _userService = new UserService(session);
            _itemService = new ItemService(session);
            _organizationService = new OrganizationService(session);
            _programService = new ProgramService(session);
            _programSessionItemService = new ProgramSessionItemService(session);
            _sessionService = new SessionService(session);
            _programBranchSessionService = new ProgramBranchSessionService(session);
            _supplierService = new SupplierService(session);
            _branchService = new BranchService(session);
            _workOrderService = new WorkOrderService(session);
        }
        #endregion

        #region Operational Functions

        #region Save

        public ActionResult SaveWorkOrder()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.PurposeList = new SelectList(_commonHelper.LoadEmumToDictionary<Purpose>(), "Key", "Value");
                ViewBag.itemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
                ViewBag.SupplierList = new SelectList(_supplierService.LoadAll(), "Id", "Name");
                ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        [HttpPost]
        public ActionResult SaveWorkOrder(DirectWorkOrderViewModel obj)
        {
            try
            {
                var workOrder = new WorkOrder();
                var supplier = _supplierService.LoadById(obj.SupplierId);
                var branch = _branchService.GetBranch(obj.BranchId);
                if (obj.ProgramSessionId.Equals("0"))
                {
                    obj.ProgramSessionId = null;
                }
                var item = _itemService.LoadById(obj.ItemId);
                var programSessionArray = !string.IsNullOrEmpty(obj.ProgramSessionId)
                    ? obj.ProgramSessionId.Split(new[] { "::" }, StringSplitOptions.None)
                    : null;
                var program = programSessionArray != null ? _programService.GetProgram(Convert.ToInt64(programSessionArray[0])) : null;
                var session = programSessionArray != null ? _sessionService.LoadById(Convert.ToInt64(programSessionArray[1])) : null;
                workOrder.Supplier = supplier;
                workOrder.WorkOrderStatus = WorkOrderStatus.Issued;
                workOrder.DeliveryDate = obj.DeliveryDate;
                workOrder.OrderedQuantity = Convert.ToInt32(obj.OrderQuantity);
                workOrder.TotalCost = obj.TotalCost;
                workOrder.Remarks = obj.Remarks;
                workOrder.Branch = branch;
                
                var workOrderDetails = new WorkOrderDetails()
                {
                    Item = item,
                    //Program = programId > 0 ? _programService.GetProgram(programId) : null,
                    //Session = sessionId > 0 ? _sessionService.LoadById(sessionId) : null,
                    WorkOrder = workOrder,
                    Purpose = obj.PurposeId != null ? Convert.ToInt32(obj.PurposeId) : (int?)null,
                    OrderQuantity = Convert.ToInt32(obj.OrderQuantity),
                    UnitCost = obj.UnitCost,
                    TotalCost = obj.TotalCost
                };

                if (item.ItemType == (int)ItemType.CommonItem)
                {
                    workOrderDetails.Program = null;
                    workOrderDetails.Session = null;
                }
                else
                {
                    workOrderDetails.Program = program;
                    workOrderDetails.Session = session;
                }
                if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose == null)
                {
                    workOrderDetails.PurposeProgram = null;
                    workOrderDetails.PurposeSession = null;
                }
                else if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose != null)
                {
                    workOrderDetails.PurposeProgram = program;
                    workOrderDetails.PurposeSession = session;
                }
                else if (item.ItemType == (int)ItemType.CommonItem && workOrderDetails.Purpose == null)
                {
                    workOrderDetails.PurposeProgram = null;
                    workOrderDetails.PurposeSession = null;
                }
                else if (item.ItemType == (int)ItemType.CommonItem && (workOrderDetails.Purpose > 0 && workOrderDetails.Purpose <= 3))
                {
                    workOrderDetails.PurposeProgram = program;
                    workOrderDetails.PurposeSession = session;
                }
                else
                {
                    workOrderDetails.PurposeProgram = program;
                    workOrderDetails.PurposeSession = session;
                }
                
                var workOrderCriteriaList = obj.CriteriaList.Select(criteria => new WorkOrderCriteria()
                {
                    Criteria = criteria.Criteria,
                    CriteriaValue = criteria.CriteriaValue,
                    WorkOrderDetail = workOrderDetails
                }).ToList();
                workOrderDetails.WorkOrderCriteriaList = workOrderCriteriaList;
                workOrder.WorkOrderDetails = new List<WorkOrderDetails>()
                {
                    workOrderDetails
                };
                var success = _workOrderService.SaveOrUpdate(workOrder);
                if (success)
                {
                    return Json(new { id = workOrder.Id, isSuccess = true });
                }
            }
            catch (InvalidDataException ex)
            {
                return Json(new { message = ex.Message, isSuccess = false });
            }

            catch (NullObjectException ex)
            {
                return Json(new { message = ex.Message, isSuccess = false });
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { message = WebHelper.CommonErrorMessage, isSuccess = false });
            }
            
            return Json(new { message = WebHelper.CommonErrorMessage, isSuccess = false });
        }

        #endregion

        #region Details

        public ActionResult Details(long id)
        {
            var workOrder = _workOrderService.GetWorkOrder(id);
            var workOrderDetails = workOrder.WorkOrderDetails[0];
            var purpose = workOrderDetails.Purpose;
            ViewBag.Purpose = purpose != null ? _commonHelper.GetEmumIdToValue<Purpose>((int)purpose) : "N/A";
            if (workOrderDetails.Program == null && workOrderDetails.Session == null)
            {
                ViewBag.ProgramSession = "N/A";
            }
            else
            {
                if (workOrderDetails.Program != null)
                    ViewBag.ProgramSession = workOrderDetails.Program.Name + " " + workOrderDetails.Session.Name;
            }
            return View(workOrder);
        }

        #endregion

        #region Cancel

        [HttpPost]
        public ActionResult CancelWorkOrder(long id)
        {

            try
            {
                bool isSuccess = _workOrderService.CancelWorkOrder(id);
                return Json(isSuccess ? new Response(true, "Work Order successfully cancelled.") : new Response(false, "Work order cancellation failed."));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #endregion

        #region Index/Manage functionalities

        public ActionResult Index()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                GetStatusText();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                GetStatusText();
                return View();
            }
        }

        #region Render Data Table

        public JsonResult WorkOrderList(int draw, int start, int length, long? organizationId = null, long? branchId = null, string item = "", int? status = null)
        {
            var getData = new List<object>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                #region OrderBy and Direction

                //NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                //if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                //{
                //    orderBy = nvc["order[0][column]"];
                //    orderDir = nvc["order[0][dir]"];
                //    switch (orderBy)
                //    {
                //        case "0":
                //            orderBy = "Name";
                //            break;
                //        case "1":
                //            orderBy = "Code";
                //            break;
                //        case "2":
                //            orderBy = "Rank";
                //            break;
                //        case "3":
                //            orderBy = "Status";
                //            break;
                //        default:
                //            orderBy = "";
                //            break;
                //    }
                //}

                #endregion

                int recordsTotal = _workOrderService.WorkOrderCount(_userMenu, organizationId, branchId, item, status);
                long recordsFiltered = recordsTotal;
                List<WorkOrderDto> workOrderList = _workOrderService.LoadWorkOrder(start, length, orderBy, orderDir.ToUpper(), _userMenu, organizationId, branchId, item, status).ToList();
                var requestContext = System.Web.HttpContext.Current.Request.RequestContext;
                foreach (var workOrder in workOrderList)
                {
                    string cancelLink = "";
                    var workOrderStatus = _commonHelper.GetEmumIdToValue<WorkOrderStatus>(workOrder.Status);
                    if (workOrder.Status == (int)WorkOrderStatus.Issued)
                    {
                        cancelLink = "<a href='" +
                        new UrlHelper(requestContext).Action("CancelWorkOrder", "DirectWorkOrder")
                        + "?id=" + workOrder.WorkOrderId + "' data-id='" + workOrder.WorkOrderId +
                        "' class='glyphicon glyphicon-remove'> </a> ";
                    }
                    getData.Add(new List<string>()
                    {
                        workOrder.Organization,
                        workOrder.Branch,
                        workOrder.ItemName,
                        workOrder.WorkOrderRefNo,
                        workOrder.OrderedQty.ToString(CultureInfo.InvariantCulture),
                        workOrderStatus,
                        LinkGenerator.GetDetailsLink("Details", "DirectWorkOrder", workOrder.WorkOrderId)+cancelLink
                    });
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData,
                    isSuccess = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }

        #endregion

        #endregion

        #region Helper Functions

        #region Ajax Functions

        [HttpPost]
        public JsonResult LoadItemList(long organizationId, long itemGroupId)
        {
            try
            {
                IList<Item> itemList = _itemService.LoadItem(organizationId, itemGroupId, null, true);
                var itemSelectList = new SelectList(itemList.DistinctBy(x => x.Id), "Id", "Name").ToList();
                return Json(new { itemList = itemSelectList, isSuccess = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        [HttpPost]
        public JsonResult LoadSupplierList(string[] itemIds)
        {
            try
            {
                IList<Supplier> supplierList = _supplierService.LoadSupplierByItemIds(itemIds);
                var supplierSelectList = new SelectList(supplierList.DistinctBy(x => x.Id), "Id", "Name").ToList();
                return Json(new { supplierList = supplierSelectList, isSuccess = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        [HttpPost]
        public JsonResult CheckCorporateBranch(long branchId)
        {
            try
            {
                var branch = _branchService.GetBranch(branchId);
                bool isCorporate = branch.IsCorporate;
                return Json(new { IsCorporate = isCorporate, isSuccess = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        [HttpPost]
        public JsonResult LoadSpecificationCriteriaList(long itemId)
        {
            try
            {
                var item = _itemService.LoadById(itemId);
                if (item.SpecificationTemplate == null)
                {
                    throw new DataNotFoundException("Specification Template not assigned to this item.");
                }
                var specificationCriteriaList = item.SpecificationTemplate.SpecificationCriteriaList;
                //IList<Supplier> specificationCriteriaList = _supplierService.LoadSupplierByItemIds(itemIds);
                //var supplierSelectList = new SelectList(specificationCriteriaList.DistinctBy(x => x.Id), "Id", "Name").ToList();
                IList<SpecificationCriteriaViewModel> specificationCriteriaVmList = new List<SpecificationCriteriaViewModel>();
                SpecificationCriteriaOptionViewModel specificationCriteriaOption = new SpecificationCriteriaOptionViewModel();
                foreach (var criteria in specificationCriteriaList)
                {
                    var specificationviewmodel = new SpecificationCriteriaViewModel
                    {
                        Name = criteria.Name,
                        Controltype = criteria.Controltype
                    };
                    specificationviewmodel.SpecificationCriteriaOptions = new List<SpecificationCriteriaOptionViewModel>();
                    foreach (var criteriaOption in criteria.SpecificationCriteriaOptions)
                    {
                        specificationviewmodel.SpecificationCriteriaOptions.Add(new SpecificationCriteriaOptionViewModel()
                        {
                            Name = criteriaOption.Name
                        });
                    }
                    specificationCriteriaVmList.Add(specificationviewmodel);
                }
                //var json = new JavaScriptSerializer().Serialize(specificationCriteriaVmList);
                return Json(new { specificationCriteriaList = specificationCriteriaVmList, isSuccess = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DataNotFoundException ex)
            {
                return Json(new { message = ex.Message, isSuccess = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { message = WebHelper.CommonErrorMessage, isSuccess = true }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.LoadEmumToDictionary<WorkOrderStatus>();
        }

        #endregion

    }
}