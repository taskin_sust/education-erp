﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IOvertimeDao : IBaseDao<Overtime, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        Overtime GetOverTime(int teamMemberPin, DateTime overTimeDate);
        AttendanceSummary GetAttendance(int pin, DateTime overTimeDate);
        Overtime GetTeamMemberLastIsPostOverTime(long? teamMemberId = null, int? pin = null);
        Overtime GetTeamMemberLastApprovedOvertime(long? teamMemberId = null, int? pin = null);

        #endregion

        #region List Loading Function

        IList<OverTimeDto> LoadOvertime(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdList, long? campusId, long? departmentId, int start, int length, string orderBy, string orderDir, bool isHistory);
        IList<Overtime> LoadOvertime(DateTime dateFrom, DateTime dateTo, long teamMemberId);


        #endregion

        #region Others Function

        int LoadOvertimeCount(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdList, long? campusId, long? departmentId, bool isHistory);
        bool CheckHasOverTimeOnADate(long teamMemberId, DateTime dateTime);

        #endregion

        #region Helper Function

        #endregion

    }
    public class OvertimeDao : BaseDao<Overtime, long>, IOvertimeDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public Overtime GetOverTime(int teamMemberPin, DateTime overTimeDate)
        {
            var criteria = Session.CreateCriteria<Overtime>().Add(Restrictions.Eq("Status", Overtime.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "hm");
            criteria.Add(Restrictions.Eq("hm.Pin", teamMemberPin));
            criteria.Add(Restrictions.Eq("OverTimeDate", overTimeDate.Date));
            //criteria.SetMaxResults(1);
            return criteria.UniqueResult<Overtime>();
        }

        public AttendanceSummary GetAttendance(int pin, DateTime overTimeDate)
        {
            var criteria = Session.CreateCriteria<AttendanceSummary>();
            criteria.CreateAlias("TeamMember", "hm");
            criteria.Add(Restrictions.Eq("hm.Pin", pin));
            criteria.Add(Restrictions.Eq("AttendanceDate", overTimeDate));
            criteria.SetMaxResults(1);
            return criteria.UniqueResult<AttendanceSummary>();
        }

        public Overtime GetTeamMemberLastIsPostOverTime(long? teamMemberId = null, int? pin = null)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<Overtime>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Overtime.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("IsPost", true));
            criteria.CreateAlias("TeamMember", "member");
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("OverTimeDate"));
            return criteria.List<Overtime>().Take(1).FirstOrDefault();
        }

        public Overtime GetTeamMemberLastApprovedOvertime(long? teamMemberId = null, int? pin = null)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            var overtimeQuery = Session.QueryOver<Overtime>()
                .Where(x => x.Status == Overtime.EntityStatus.Active && x.ApprovedTime != null && (x.ApprovedTime.Value.Hour > 0 || x.ApprovedTime.Value.Minute > 0))
                ;
            if (teamMemberId != null)
            {
                overtimeQuery.And(x => x.TeamMember.Id == teamMemberId);
            }
            if (pin != null)
            {
                overtimeQuery.And(x => x.TeamMember.Pin == pin);
            }
            Overtime overtime = overtimeQuery.OrderBy(x => x.OverTimeDate).Desc.Take(1).SingleOrDefault<Overtime>();
            return overtime;
            //.SingleOrDefault<Overtime>();

            //ICriteria criteria = Session.CreateCriteria<Overtime>();
            //criteria.Add(Restrictions.Eq("Status", Overtime.EntityStatus.Active));
            //criteria.Add(Restrictions.Eq("IsBlocked", true));


            ////var disjunction = Restrictions.Disjunction(); // for OR statement 
            ////disjunction.Add(Restrictions.Ge())
            ////if (programBranchPairList != null && programBranchPairList.Count > 0)
            ////{
            ////    foreach (var programBranch in programBranchPairList)
            ////    {
            ////        disjunction.Add(Restrictions.Eq("Program.Id", programBranch.ProgramId) && Restrictions.Eq("Branch.Id", programBranch.BranchId));
            ////    }
            ////    criteria.Add(disjunction);
            ////}

            ////criteria.Add(Restrictions.Eq("IsBlocked", true));
            //criteria.CreateAlias("TeamMember", "member");
            //if (teamMemberId != null)
            //    criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            //if (pin != null)
            //    criteria.Add(Restrictions.Eq("member.Pin", pin));
            //criteria.AddOrder(Order.Desc("BlockingDate"));
            //return criteria.List<DailySupportAllowanceBlock>().Take(1).FirstOrDefault();
        }

        #endregion

        #region List Loading Function

        public IList<OverTimeDto> LoadOvertime(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdList, long? campusId, long? departmentId, int start, int length, string orderBy, string orderDir, bool isHistory)
        {
            var filterPagination = "";
            if (length > 0)
            {
                filterPagination = " where pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            string sqlQuery = GetOvertimeQuery(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdList, campusId, departmentId, isHistory);
            string query = @"DECLARE @rowsperpage INT 
		                    DECLARE @start INT 
		                    SET @start = " + (start + 1) + @" 
		                    SET @rowsperpage = " + length + @" 
                Select * From (SELECT row_number() OVER (ORDER BY A.Id asc) AS RowNum, A.Id,A.Pin,A.TeamMemberId,A.Name,A.Department,A.Designation,A.Organization,A.Branch,A.Campus,A.OverTimeDate,A.TimeFrom,A.TimeTo,A.ApprovedTime
                                ,A.[EarlyEntry],A.[LateLeave],A.[TotalOvertime], A.[ModifiedBy], A.[LastModificationDate] 
	                            FROM(
		                            " + sqlQuery + @"
	                            ) A ) as pagination 
                            " + filterPagination;

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<OverTimeDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<OverTimeDto>().ToList();
        }

        public IList<Overtime> LoadOvertime(DateTime dateFrom, DateTime dateTo, long teamMemberId)
        {
            ICriteria criteria = Session.CreateCriteria<Overtime>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Overtime.EntityStatus.Delete)));
            criteria.Add(Restrictions.Not(Restrictions.Eq("IsPost", true)));
            criteria.CreateAlias("TeamMember", "member");
            criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            criteria.Add(Restrictions.Ge("OverTimeDate", dateFrom.Date));
            criteria.Add(Restrictions.Le("OverTimeDate", dateTo.Date));
            return criteria.List<Overtime>();
        }

        #endregion

        #region Others Function

        public int LoadOvertimeCount(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdList, long? campusId, long? departmentId, bool isHistory)
        {
            string sqlQuery = GetOvertimeQuery(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdList, campusId, departmentId, isHistory);
            string query = "Select Count(*) as totalRow From (" + sqlQuery + ") AS A";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(5000);
            return iQuery.UniqueResult<int>();
        }

        public bool CheckHasOverTimeOnADate(long teamMemberId, DateTime dateTime)
        {
            int overTimeCount = Session.QueryOver<Overtime>().Where(x => x.ApprovedTime != null && (x.ApprovedTime.Value.Hour > 0 || x.ApprovedTime.Value.Minute > 0) && x.Status == Overtime.EntityStatus.Active && x.TeamMember.Id == teamMemberId && x.OverTimeDate.Value.Date == dateTime.Date).RowCount();

            if (overTimeCount > 0)
                return true;
            return false;
        }

        #endregion

        #region Helper Function

        private string GetOvertimeQuery(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdList, long? campusId, long? departmentId, bool isHistory)
        {
            string query;
            string pinFiltering = "";
            string filtering = "";
            string mentorTeamMemberFiltering = "";
            string attendanceDateFilter = " AttendanceDate >= '" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "' AND AttendanceDate <= '" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "'";
            string overTimeDateAttendanceDateFilter = " AND hra.OverTimeDate  >= '" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "' AND hra.OverTimeDate  <= '" + dateTo.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "'";

            if (!string.IsNullOrEmpty(pinList))
            {
                pinFiltering = " and [Pin] IN (" + pinList + ")";
            }
            if (mentorTeamMemberIdList.Any())
            {
                mentorTeamMemberFiltering += " and Id IN (" + String.Join(", ", mentorTeamMemberIdList) + ")";
            }
            if (organizationIdList != null && organizationIdList.Any())
            {
                filtering += " and o.Id IN (" + String.Join(", ", organizationIdList) + ")";
            }
            if (branchIdList != null && branchIdList.Any())
            {
                filtering += " and br.Id IN (" + String.Join(", ", branchIdList) + ")";
            }
            if (campusId != null && campusId > 0)
            {
                filtering += " and c.Id = " + campusId;
            }
            if (departmentId != null && departmentId > 0)
            {
                filtering += " and dp.Id = " + departmentId;
            }

            if (isHistory)
            {
                query = @"Select hras.*
                                , hra.Id
	                            , ApprovedTime
	                            , hrm.Name
	                            , hrm.Pin
	                            , c.Name AS Campus
	                            , br.Name AS Branch
	                            , o.ShortName AS Organization
	                            , dp.Name AS Department
	                            , dg.Name AS Designation
                                , hra.ModifyBy AS ModifiedBy
                                , hra.ModificationDate AS LastModificationDate
                            from [dbo].[HR_Overtime] AS hra	 
                            INNER Join (
	                            Select TeamMemberId AS TeamMemberId,AttendanceDate AS OverTimeDate, [InTime] as TimeFrom,[OutTime] as TimeTo 
                                ,ISNULL([EarlyEntry],DATEADD(s, 0, '20000101')) AS [EarlyEntry]
								,ISNULL([LateLeave],DATEADD(s, 0, '20000101')) AS [LateLeave]
								,ISNULL([TotalOvertime],DATEADD(s, 0, '20000101')) AS [TotalOvertime]	
	                            from [dbo].[HR_AttendanceSummary]
	                            Where " + attendanceDateFilter + @" AND [TeamMemberId] IN (
		                            SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1  " + mentorTeamMemberFiltering + pinFiltering + @" 
	                            ) AND [Status] = " + AttendanceSummary.EntityStatus.Active
                          + @") AS hras ON hra.TeamMemberId = hras.TeamMemberId  and hras.OverTimeDate = hra.OverTimeDate" + overTimeDateAttendanceDateFilter + @"                          
                            Left Join [dbo].[HR_TeamMember] AS hrm ON hrm.Id = hras.TeamMemberId AND hrm.[Status] = " +
                          TeamMember.EntityStatus.Active + @"
                            CROSS APPLY  (
	                                Select * from (
		                                Select 
		                                 Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		                                , emh.CampusId as [CampusId]
		                                , emh.DesignationId as [DesignationId]
		                                , emh.DepartmentId as [DepartmentId]
		                                , emh.TeamMemberId as [TeamMemberId]
		                                , emh.EffectiveDate as [EffectiveDate]
		                                , emh.EmploymentStatus as [EmploymentStatus]
		                                from HR_EmploymentHistory as emh
		                                where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		                                and TeamMemberId IN (SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1 " + pinFiltering + @")
		                                and  emh.EffectiveDate <=  hras.OverTimeDate
		                                and emh.TeamMemberId = hras.TeamMemberId
	                                ) as employmentHistory 
	                                where 1=1
	                                and employmentHistory.r = 1
                              ) as currentPosition 
                            Left Join [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status =  " + Campus.EntityStatus.Active + @"
                            Left Join [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status =  " + Branch.EntityStatus.Active + @"
                            Left Join [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  " + Organization.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  " + Department.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  " + Designation.EntityStatus.Active + @"
                            Where 1=1 
                            AND hra.Status = " + Overtime.EntityStatus.Active + @"
                            AND hras.[TotalOvertime]>'2000-01-01 00:00:00.000' " + filtering;
            }
            else
            {
                query = @"Select hras.*
                                ,hra.Id
	                            ,ApprovedTime
	                            ,hrm.Name
	                            ,hrm.Pin
	                            ,c.Name AS Campus
	                            ,br.Name AS Branch
	                            ,o.ShortName AS Organization
	                            ,dp.Name AS Department
	                            ,dg.Name AS Designation
                                , hra.ModifyBy AS ModifiedBy
                                , hra.ModificationDate AS LastModificationDate
                            from (
	                            Select TeamMemberId AS TeamMemberId,AttendanceDate AS OverTimeDate, [InTime] as TimeFrom,[OutTime] as TimeTo 
                                ,ISNULL([EarlyEntry],DATEADD(s, 0, '20000101')) AS [EarlyEntry]
								,ISNULL([LateLeave],DATEADD(s, 0, '20000101')) AS [LateLeave]
								,ISNULL([TotalOvertime],DATEADD(s, 0, '20000101')) AS [TotalOvertime]	
	                            from [dbo].[HR_AttendanceSummary]
	                            Where " + attendanceDateFilter + @" AND [TeamMemberId] IN (
		                            SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1  " + mentorTeamMemberFiltering + pinFiltering + @" 
	                            ) AND [Status] = " + AttendanceSummary.EntityStatus.Active
                           + @") AS hras
                            Left Join [dbo].[HR_Overtime] AS hra ON hra.TeamMemberId = hras.TeamMemberId AND hra.Status = " + Overtime.EntityStatus.Active + overTimeDateAttendanceDateFilter + @"
                            Left Join [dbo].[HR_TeamMember] AS hrm ON hrm.Id = hras.TeamMemberId AND hrm.[Status] = " +
                           TeamMember.EntityStatus.Active + @"
                            CROSS APPLY  (
	                                Select * from (
		                                Select 
		                                 Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		                                , emh.CampusId as [CampusId]
		                                , emh.DesignationId as [DesignationId]
		                                , emh.DepartmentId as [DepartmentId]
		                                , emh.TeamMemberId as [TeamMemberId]
		                                , emh.EffectiveDate as [EffectiveDate]
		                                , emh.EmploymentStatus as [EmploymentStatus]
		                                from HR_EmploymentHistory as emh
		                                where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		                                and TeamMemberId IN (SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1 " + pinFiltering + @")
		                                and  emh.EffectiveDate <=  hras.OverTimeDate
		                                and emh.TeamMemberId = hras.TeamMemberId
	                                ) as employmentHistory 
	                                where 1=1
	                                and employmentHistory.r = 1
                              ) as currentPosition
                            Left Join [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status =  " + Campus.EntityStatus.Active + @"
                            Left Join [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status =  " + Branch.EntityStatus.Active + @"
                            Left Join [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  " + Organization.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  " + Department.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  " + Designation.EntityStatus.Active + " Where 1=1 AND hras.[TotalOvertime]>'2000-01-01 00:00:00.000' " + filtering;
            }

            return query;
        }

        #endregion


    }

}
