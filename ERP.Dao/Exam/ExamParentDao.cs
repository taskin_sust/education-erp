﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Exam
{
    public interface IExamParentDao : IBaseDao<ExamParent, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        ExamParent GetExamParent(long programId, long sessionId, long courseId, long branchId, long batchId, string examNamePrefix, long id ); 
        #endregion

        #region List Loading Function
        IList<ExamParent> LoadExamParentList(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, int start, int length, string orderBy, string orderDirection, string organization, string program, string session, string course, string examNamePrefix);
        #endregion
        
        #region Others Function
        int GetExamParentRowCount(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, string organization, string program, string session, string course, string examNamePrefix);
        int GetAnswerScriptCount(long id);
        int GetStudentMarksCount(long id);
        #endregion
    }
    public class ExamParentDao : BaseDao<ExamParent, long>, IExamParentDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        public ExamParent GetExamParent(long programId, long sessionId, long courseId, long branchId, long batchId,
            string examNamePrefix, long id )
        {
            ICriteria criteria = Session.CreateCriteria<ExamParent>().Add(Restrictions.Not(Restrictions.Eq("Status", ExamParent.EntityStatus.Delete)));
            criteria.CreateAlias("Course", "c");
            criteria.Add(Restrictions.Eq("c.Id", courseId));
            criteria.Add(Restrictions.Eq("c.Program.Id", programId));
            criteria.Add(Restrictions.Eq("c.RefSession.Id", sessionId));
            if (id>0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            criteria.Add(branchId > 0 ? Restrictions.Eq("Branch.Id", branchId) : Restrictions.IsNull("Branch"));
            criteria.Add(batchId > 0 ? Restrictions.Eq("Batch.Id", batchId) : Restrictions.IsNull("Batch"));
            criteria.Add(Restrictions.Eq("ExamNamePrefix", examNamePrefix));
            return criteria.SetMaxResults(1).UniqueResult<ExamParent>();
        }

        public IList<ExamParent> LoadExamParentList(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, int start, int length, string orderBy, string orderDirection, string organization,
            string program, string session, string course, string examNamePrefix)
        {
            ICriteria criteria = GetExamParentCriteria(authorizeOrgIds, authorizeProgramIds, authorizeSessionIds, organization, program, session, course, examNamePrefix);
            if (length>0)
            {
                return criteria.SetFirstResult(start).SetMaxResults(length).List<ExamParent>(); 
            }
            return criteria.List<ExamParent>(); 
        }

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function
        public int GetExamParentRowCount(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, string organization, string program, string session, string course, string examNamePrefix)
        {
            ICriteria criteria = GetExamParentCriteria(authorizeOrgIds, authorizeProgramIds, authorizeSessionIds, organization, program, session, course, examNamePrefix);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public int GetAnswerScriptCount(long id)
        {
            ICriteria criteria = Session.CreateCriteria<ExamsStudentAnswerScript>();
            criteria.CreateAlias("ExamsDetails", "ed").Add(Restrictions.Eq("ed.Status", ExamsDetails.EntityStatus.Active));
            criteria.CreateAlias("ed.Exams", "e").Add(Restrictions.Eq("e.Status", Exams.EntityStatus.Active));
            criteria.CreateAlias("e.ExamParent", "ep").Add(Restrictions.Eq("ep.Status", ExamParent.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("ep.Id", id));
            criteria.Add(Restrictions.Eq("e.IsMcq", Convert.ToBoolean(ExamType.Mcq)));
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public int GetStudentMarksCount(long id)
        {
            ICriteria criteria = Session.CreateCriteria<ExamsStudentMarks>();
            criteria.CreateAlias("ExamsDetails", "ed").Add(Restrictions.Eq("ed.Status", ExamsDetails.EntityStatus.Active));
            criteria.CreateAlias("ed.Exams", "e").Add(Restrictions.Eq("e.Status", Exams.EntityStatus.Active));
            criteria.CreateAlias("e.ExamParent", "ep").Add(Restrictions.Eq("ep.Status", ExamParent.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("ep.Id", id));
           // criteria.Add(Restrictions.Eq("e.IsMcq", ExamType.Mcq));
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function
        private ICriteria GetExamParentCriteria(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, string organization, string program, string session, string course, string examNamePrefix)
        {
           
            ICriteria criteria = Session.CreateCriteria<ExamParent>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", ExamParent.EntityStatus.Delete)));
            criteria.CreateAlias("Course", "c").Add(Restrictions.Eq("c.Status", Course.EntityStatus.Active));
            criteria.CreateAlias("c.Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("c.RefSession", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            
            criteria.CreateAlias("p.Organization", "o").Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active));

            criteria.Add(Restrictions.In("o.Id", authorizeOrgIds));
            criteria.Add(Restrictions.In("p.Id", authorizeProgramIds));
            criteria.Add(Restrictions.In("s.Id", authorizeSessionIds));

            if (!String.IsNullOrEmpty(course))
            {
                criteria.Add(Restrictions.Eq("c.Id", Convert.ToInt64(course)));
            }
            if (!String.IsNullOrEmpty(organization))
            {
               
                criteria.Add(Restrictions.Eq("o.Id", Convert.ToInt64(organization)));
            }
            if (!String.IsNullOrEmpty(program))
            {
                criteria.Add(Restrictions.Eq("p.Id", Convert.ToInt64(program)));
            }
            if (!String.IsNullOrEmpty(session))
            {
                criteria.Add(Restrictions.Eq("s.Id", Convert.ToInt64(session)));
            }

            if (!String.IsNullOrEmpty(examNamePrefix))
            {
                criteria.Add(Restrictions.Like("ExamNamePrefix", examNamePrefix, MatchMode.Anywhere));
            }
            return criteria;
           
        }
        #endregion

        
    }
}
