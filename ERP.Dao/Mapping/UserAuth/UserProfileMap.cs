using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Mapping.UserAuth {


    public partial class UserProfileMap : BaseClassMap<UserProfile, long>
    {
        public UserProfileMap() {

            Table("UserProfile");
            LazyLoad();
            //Not.LazyLoad();
            Map(x => x.NickName).Column("NickName");
            References(x => x.Branch).Column("BranchId").LazyLoad();
            References(x => x.Campus).Column("CampusId").LazyLoad();
            References(x => x.AspNetUser).Column("AspNetUserId").LazyLoad();
            HasMany(x => x.UserMenus).KeyColumn("UserProfileId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            HasMany(x => x.UserImages).KeyColumn("User_id").Cascade.AllDeleteOrphan().Inverse();
           // Map(x => x.Image).Column("Image").CustomSqlType("VARBINARY (MAX) FILESTREAM").Length(2147483647).LazyLoad();
            // References(x => x.Images).Column("ImageId");
        }
    }
}
