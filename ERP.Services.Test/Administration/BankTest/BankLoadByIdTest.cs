﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BankTest
{
    public interface IBankLoadByIdTest
    {
        void LoadById_Null_Check();
        void LoadById_NotNull_Check();
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Bank")]
    public class BankLoadByIdTest : BankBaseTest, IBankLoadByIdTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _bankService.GetBank(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            BankFactory bankFactory =
                _bankFactory.Create()
                    .Persist();
            var obj = _bankService.GetBank(bankFactory.Object.Id);
            Assert.NotNull(obj);

        }
    }
}
