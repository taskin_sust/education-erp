﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentBatchLogDao : IBaseDao<StudentBatchLog, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
       
    
        #endregion

        #region Others Function

        #endregion    
    }

    public class StudentBatchLogDao : BaseDao<StudentBatchLog, long>, IStudentBatchLogDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
}