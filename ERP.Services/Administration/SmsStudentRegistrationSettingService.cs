﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Util;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ISmsStudentRegistrationSettingService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(SmsStudentRegistrationSetting srs);

        bool Delete(long id);

        #endregion

        #region Single Instances Loading Function

        SmsStudentRegistrationSetting GetSmsStudentRegistrationSetting(long id);

        #endregion

        #region List Loading Function

        IList<SmsStudentRegistrationSetting> LoadSmsStudentRegistrationSetting(List<UserMenu> authorizeMenu, int start, int length, List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> courseIdList = null);

        #endregion

        #region Others Function

        long GetSmsStudentRegistrationSettingCount(List<UserMenu> authorizeMenu, List<long> convertedOrganizationIdList, List<long> convertedProgramIdList, List<long> convertedSessionIdList, List<long> convertedCourseIdList, string smsSettingStatus);

        bool IsStudentRegistered(long programId, long courseId);

        #endregion

        #region Helper Function

        #endregion

    }
    public class SmsStudentRegistrationSettingService : BaseService, ISmsStudentRegistrationSettingService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");

        #endregion

        #region Propertise & Object Initialization

        private readonly ISmsStudentRegistrationSettingDao _srsDao;
        private readonly ISmsStudentRegistrationReceiverDao _srrDao;
        private readonly IProgramDao _programDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly ISessionDao _sessionDao;
        private readonly ICommonHelper _commonHelper;
        
        public SmsStudentRegistrationSettingService(ISession session)
        {
            Session = session;
            _srsDao = new SmsStudentRegistrationSettingDao { Session = Session };
            _srrDao = new SmsStudentRegistrationReceiverDao { Session = Session };
            _programDao = new ProgramDao { Session = Session };
            _sessionDao = new SessionDao { Session = Session };
            _organizationDao = new OrganizationDao { Session = Session };
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Function

        public void SaveOrUpdate(SmsStudentRegistrationSetting srs)
        {
            ITransaction trans = null;
            try
            {
                CheckBeforeSaveOrUpdate(srs);
                using (trans = Session.BeginTransaction())
                {
                    if (srs.Id > 0)
                    {
                        var existingSrs = _srsDao.LoadById(srs.Id);
                        //check registration already done under this setting
                        if ((existingSrs.Course.Id != srs.Course.Id || existingSrs.StartDate.Date != srs.StartDate.Date) && IsStudentRegistered(existingSrs.Program.Id, existingSrs.Course.Id))
                            throw new DependencyException("Student registration found under this setting.");
                        existingSrs.BusinessId = srs.BusinessId;
                        existingSrs.Program = srs.Program;
                        existingSrs.Session = srs.Session;
                        existingSrs.Course = srs.Course;
                        existingSrs.StudentExam = srs.StudentExam;
                        existingSrs.SmsMask = srs.SmsMask;
                        existingSrs.StartDate = srs.StartDate;
                        existingSrs.EndDate = srs.EndDate;
                        existingSrs.Status = srs.Status;
                        existingSrs.SuccessMessage = srs.SuccessMessage;
                        existingSrs.DuplicateMessage = srs.DuplicateMessage;
                        _srsDao.Update(existingSrs);
                    }
                    else
                    {
                        _srsDao.Save(srs);
                    }
                    trans.Commit();
                }
            }
            catch (DependencyException ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw;
            }
            catch (NullObjectException ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw;
            }
            catch (InvalidDataException ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw;
            }

            catch (DuplicateEntryException ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool IsStudentRegistered(long programId, long courseId)
        {
            try
            {
                return _srsDao.IsStudentRegistered(programId, courseId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(long id)
        {
            ITransaction trans = null;
            try
            {
                var srs = _srsDao.LoadById(id);
                if (srs == null)
                {
                    throw new NullObjectException("SMS registration setting not found");
                }
                //check registration already done under this setting
                if (IsStudentRegistered(srs.Program.Id, srs.Course.Id))
                    throw new DependencyException("Student registration found under this setting.");
                using (trans = Session.BeginTransaction())
                {
                    srs.Status = Program.EntityStatus.Delete;
                    _srsDao.Update(srs);
                    trans.Commit();
                }
            }
            catch (NullObjectException ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw;
            }
            catch (DependencyException ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
            return true;
        }

        #endregion

        #region Single Instances Loading Function

        public SmsStudentRegistrationSetting GetSmsStudentRegistrationSetting(long id)
        {
            try
            {
                return _srsDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<SmsStudentRegistrationSetting> LoadSmsStudentRegistrationSetting(List<UserMenu> authorizeMenu, int start, int length, List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> courseIdList = null)
        {
            try
            {
                List<long> authorizeOrganizationIdList = AuthHelper.LoadOrganizationIdList(authorizeMenu, organizationIdList);
                List<long> authorizeProgramIdList = AuthHelper.LoadProgramIdList(authorizeMenu, authorizeOrganizationIdList);
                IList<SmsStudentRegistrationSetting> srsList = _srsDao.LoadSmsStudentRegistrationSetting(start, length, authorizeProgramIdList, sessionIdList, courseIdList);
                return srsList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        
        #endregion

        #region Others Function

        public long GetSmsStudentRegistrationSettingCount(List<UserMenu> authorizeMenu, List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> courseIdList = null, string smsSettingStatus = null)
        {
            try
            {
                List<long> authorizeOrganizationIdList = AuthHelper.LoadOrganizationIdList(authorizeMenu, organizationIdList);
                List<long> authorizeProgramIdList = AuthHelper.LoadProgramIdList(authorizeMenu, authorizeOrganizationIdList);
                long srsCount = _srsDao.GetSmsStudentRegistrationSettingCount(authorizeProgramIdList, sessionIdList, courseIdList, smsSettingStatus);
                return srsCount;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        
        #endregion

        #region Helper Function
        
        private void CheckBeforeSaveOrUpdate(SmsStudentRegistrationSetting srs)
        {
            if (srs == null)
            {
                throw new NullObjectException("Student Registration SMS Setting can not be null");
            }
            if (srs.Id == 0 && srs.StartDate.Date < DateTime.Now.Date)
            {
                throw new InvalidDataException("Start date cannot be less than today's date");
            }
            if (srs.StartDate.Date > srs.EndDate.Date)
            {
                throw new InvalidDataException("Start date cannot be greater than end date");
            }
            //validation check with meta data
            string errorMessage = "";
            var validationResultSrs = ValidationHelper.ValidateEntity<SmsStudentRegistrationSetting, SmsStudentRegistrationSetting>(srs);
            if (validationResultSrs.HasError)
            {
                validationResultSrs.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
            }
            foreach (var srr in srs.SmsStudentRegistrationReceiverList)
            {
                var validationResultSrr = ValidationHelper.ValidateEntity<SmsStudentRegistrationReceiver, SmsStudentRegistrationReceiver>(srr);
                if (validationResultSrr.HasError)
                {
                    validationResultSrr.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                }
            }
            if (!string.IsNullOrEmpty(errorMessage))
            {
                throw new InvalidDataException(errorMessage);
            }

            var isDuplicate = _srsDao.IsDuplicate(srs);

            if (isDuplicate)
            {
                throw new DuplicateEntryException("SMS registration setting already found for this course.");
            }
            //check date for settings under this program
            var program = srs.Program;
            var otherSettings = program.SmsStudentRegistrationSettingList.Where(x => x.Course.Id != srs.Course.Id && x.Status != SmsStudentRegistrationSetting.EntityStatus.Delete);
            foreach (var setting in otherSettings)
            {
                if (setting.StartDate <= srs.EndDate && srs.StartDate <= setting.EndDate)
                {
                    throw new InvalidDataException("Overlapping date range under same program");
                }
            }
        }

        #endregion
    }
}
