﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface ISurveyQuestionAnswerDao : IBaseDao<SurveyQuestionAnswer, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        SurveyQuestionAnswer LoadSurveyQuestionAnswer(long answerId, long programId, long sessionId);
        #endregion

        #region List Loading Function
        IList<SurveyQuestionAnswer> LoadSurveyQuestionAnswer(long programId, long sessionId, long[] questionId);
        #endregion

        #region Others Function

        #endregion

    }

    public class SurveyQuestionAnswerDao : BaseDao<SurveyQuestionAnswer, long>, ISurveyQuestionAnswerDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public SurveyQuestionAnswer LoadSurveyQuestionAnswer(long answerId, long programId, long sessionId)
        {
            //answerId = 12;
            ICriteria criteria = Session.CreateCriteria(typeof(SurveyQuestionAnswer));
            criteria.Add(Restrictions.Eq("Id", answerId));
            criteria.CreateAlias("SurveyQuestion", "sq");
            criteria.CreateAlias("sq.ProgramSessionSurveyQuestions", "pssq");

            criteria.Add(Restrictions.Eq("pssq.Program.Id", programId));
            criteria.Add(Restrictions.Eq("pssq.Session.Id", sessionId));
            criteria.Add(Restrictions.Eq("sq.Status", SurveyQuestion.EntityStatus.Active));

            var data = (List<SurveyQuestionAnswer>)criteria.List<SurveyQuestionAnswer>();
            if (data.Any())
                return data.FirstOrDefault();
            // var data = criteria.;
            return new SurveyQuestionAnswer();
        }
        #endregion

        #region List Loading Function
        public IList<SurveyQuestionAnswer> LoadSurveyQuestionAnswer(long programId, long sessionId, long[] questionId)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(SurveyQuestionAnswer));

            criteria.CreateAlias("SurveyQuestion", "sq");
            criteria.CreateAlias("sq.ProgramSessionSurveyQuestions", "pssq");
            criteria.Add(Restrictions.Eq("pssq.Program.Id", programId));
            criteria.Add(Restrictions.Eq("pssq.Session.Id", sessionId));
            if (questionId != null)
            {
                if (!questionId.Contains(SelectionType.SelelectAll))
                    criteria.Add(Restrictions.In("sq.Id", questionId));
            }
            criteria.Add(Restrictions.Eq("sq.Status", SurveyQuestion.EntityStatus.Active));
            var data = (List<SurveyQuestionAnswer>)criteria.List<SurveyQuestionAnswer>();
            return data;
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion


    }
}
