﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MemberBenefitFundOpening
{
    public interface IMemberBenefitFundOpeningFunctionTest
    {
        void Employee_Benefit_Fund_Opening_Count_Function();
        void Employee_Benefit_Fund_Opening_Invalid_Organization_Function();
        void Employee_Benefit_Fund_Opening_Save_Successfull_Function();
        void Employee_Benefit_Fund_Opening_Null_UserMenu();
        void Employee_Benefit_Fund_Opening_Null_Object();
    }

    [Trait("Area", "Hr")]
    [Trait("Service", "MemberEbfBalanceService")]
    public class MemberBenefitFundOpeningFunctionTest : MemberBenefitFundOpeningBaseTest, IMemberBenefitFundOpeningFunctionTest
    {
        [Fact]
        public void Employee_Benefit_Fund_Opening_Count_Function()
        {

            var teammember = PersistTeamMember();
            var org = teammember.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = teammember.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = teammember.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = teammember.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = teammember.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();
            var userMenu = BuildUserMenu(org, (Program)null, branch);

            var avaiableMember = _employeeBenefitsFundSettingService.LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(
                userMenu, org.Id, commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(department.Id), commonHelper.ConvertIdToList(designation.Id),
                  (int)MemberEmploymentStatus.Permanent, DateTime.Now);

            Assert.NotEmpty(avaiableMember);
        }

        [Fact]
        public void Employee_Benefit_Fund_Opening_Save_Successfull_Function()
        {


            var teammember = PersistTeamMember();
            var organization = teammember.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = teammember.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = teammember.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = teammember.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = teammember.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();

            var userMenu = BuildUserMenu(organization, (Program)null, branch);

            var avaiableMember = _employeeBenefitsFundSettingService.LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(
              userMenu, organization.Id, commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(department.Id), commonHelper.ConvertIdToList(designation.Id),
                (int)MemberEmploymentStatus.Permanent, DateTime.Now);

            EmployeeBenefitFundSettingOpeningView employeeBenefitFundSettingOpeningView = new EmployeeBenefitFundSettingOpeningView();
            employeeBenefitFundSettingOpeningView.Organization = organization.Id;
            employeeBenefitFundSettingOpeningView.Branch = new List<long>();
            employeeBenefitFundSettingOpeningView.Campus = new List<long>();
            employeeBenefitFundSettingOpeningView.Department = new List<long>();
            employeeBenefitFundSettingOpeningView.Designation = new List<long>();
            employeeBenefitFundSettingOpeningView.Branch.Add(branch.Id);
            employeeBenefitFundSettingOpeningView.Campus.Add(campus.Id);
            employeeBenefitFundSettingOpeningView.Department.Add(department.Id);
            employeeBenefitFundSettingOpeningView.Designation.Add(designation.Id);
            var depositDateTime = DateTime.Now;
            employeeBenefitFundSettingOpeningView.DepositDate = depositDateTime;
            employeeBenefitFundSettingOpeningView.EmploymentStatus = avaiableMember[0].MemberType;
            foreach (var forOpeningFund in avaiableMember)
            {
                employeeBenefitFundSettingOpeningView.EmployeeBenefitFundBalanceViewList = new List<EmployeeBenefitFundBalanceView>();
                forOpeningFund.DepositDate = depositDateTime;
                employeeBenefitFundSettingOpeningView.EmployeeBenefitFundBalanceViewList.Add(forOpeningFund);

            }

            Assert.True(_memberEbfBalanceService.SaveOrUpdate(userMenu, employeeBenefitFundSettingOpeningView));

        }

        [Fact]
        public void Employee_Benefit_Fund_Opening_Invalid_Organization_Function()
        {

            var teammember = PersistTeamMember();
            var organization = teammember.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = teammember.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = teammember.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = teammember.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = teammember.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();

            var userMenu = BuildUserMenu(organization, (Program)null, branch);

            var avaiableMember = _employeeBenefitsFundSettingService.LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(userMenu, organization.Id, commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(department.Id), commonHelper.ConvertIdToList(designation.Id),
            (int)MemberEmploymentStatus.Permanent, DateTime.Now);


            EmployeeBenefitFundSettingOpeningView employeeBenefitFundSettingOpeningView = new EmployeeBenefitFundSettingOpeningView();
            //employeeBenefitFundSettingOpeningView.Organization = organization.Id;
            employeeBenefitFundSettingOpeningView.Branch = new List<long>();
            employeeBenefitFundSettingOpeningView.Campus = new List<long>();
            employeeBenefitFundSettingOpeningView.Department = new List<long>();
            employeeBenefitFundSettingOpeningView.Designation = new List<long>();
            employeeBenefitFundSettingOpeningView.Branch.Add(branch.Id);
            employeeBenefitFundSettingOpeningView.Campus.Add(campus.Id);
            employeeBenefitFundSettingOpeningView.Department.Add(department.Id);
            employeeBenefitFundSettingOpeningView.Designation.Add(designation.Id);
            var depositDateTime = DateTime.Now;
            employeeBenefitFundSettingOpeningView.DepositDate = depositDateTime;
            employeeBenefitFundSettingOpeningView.EmploymentStatus = avaiableMember[0].MemberType;
            foreach (var forOpeningFund in avaiableMember)
            {
                employeeBenefitFundSettingOpeningView.EmployeeBenefitFundBalanceViewList = new List<EmployeeBenefitFundBalanceView>();
                forOpeningFund.DepositDate = depositDateTime;
                employeeBenefitFundSettingOpeningView.EmployeeBenefitFundBalanceViewList.Add(forOpeningFund);

            }



            Assert.Throws<NullObjectException>(() => _memberEbfBalanceService.SaveOrUpdate(userMenu, employeeBenefitFundSettingOpeningView));
        }


        [Fact]
        public void Employee_Benefit_Fund_Opening_Null_UserMenu()
        {


            var teammember = PersistTeamMember();
            var organization = teammember.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = teammember.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = teammember.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = teammember.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = teammember.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();

            var userMenu = BuildUserMenu(organization, (Program)null, branch);

            var avaiableMember = _employeeBenefitsFundSettingService.LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(
              userMenu, organization.Id, commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(department.Id), commonHelper.ConvertIdToList(designation.Id),
                (int)MemberEmploymentStatus.Permanent, DateTime.Now);

            EmployeeBenefitFundSettingOpeningView employeeBenefitFundSettingOpeningView = new EmployeeBenefitFundSettingOpeningView();
            employeeBenefitFundSettingOpeningView.Organization = organization.Id;
            employeeBenefitFundSettingOpeningView.Branch = new List<long>();
            employeeBenefitFundSettingOpeningView.Campus = new List<long>();
            employeeBenefitFundSettingOpeningView.Department = new List<long>();
            employeeBenefitFundSettingOpeningView.Designation = new List<long>();
            employeeBenefitFundSettingOpeningView.Branch.Add(branch.Id);
            employeeBenefitFundSettingOpeningView.Campus.Add(campus.Id);
            employeeBenefitFundSettingOpeningView.Department.Add(department.Id);
            employeeBenefitFundSettingOpeningView.Designation.Add(designation.Id);
            var depositDateTime = DateTime.Now;
            employeeBenefitFundSettingOpeningView.DepositDate = depositDateTime;
            employeeBenefitFundSettingOpeningView.EmploymentStatus = avaiableMember[0].MemberType;
            foreach (var forOpeningFund in avaiableMember)
            {
                employeeBenefitFundSettingOpeningView.EmployeeBenefitFundBalanceViewList = new List<EmployeeBenefitFundBalanceView>();
                forOpeningFund.DepositDate = depositDateTime;
                employeeBenefitFundSettingOpeningView.EmployeeBenefitFundBalanceViewList.Add(forOpeningFund);

            }

            Assert.Throws<NullObjectException>(() => _memberEbfBalanceService.SaveOrUpdate(null, employeeBenefitFundSettingOpeningView));

        }



        [Fact]
        public void Employee_Benefit_Fund_Opening_Null_Object()
        {


            var teammember = PersistTeamMember();
            var organization = teammember.EmploymentHistory.Select(x => x.Department.Organization).FirstOrDefault();
            var branch = teammember.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = teammember.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = teammember.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            var designation = teammember.EmploymentHistory.Select(x => x.Designation).FirstOrDefault();

            var userMenu = BuildUserMenu(organization, (Program)null, branch);

            var avaiableMember = _employeeBenefitsFundSettingService.LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(
              userMenu, organization.Id, commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(department.Id), commonHelper.ConvertIdToList(designation.Id),
                (int)MemberEmploymentStatus.Permanent, DateTime.Now);

            EmployeeBenefitFundSettingOpeningView employeeBenefitFundSettingOpeningView = new EmployeeBenefitFundSettingOpeningView();
            employeeBenefitFundSettingOpeningView.Organization = organization.Id;
            employeeBenefitFundSettingOpeningView.Branch = new List<long>();
            employeeBenefitFundSettingOpeningView.Campus = new List<long>();
            employeeBenefitFundSettingOpeningView.Department = new List<long>();
            employeeBenefitFundSettingOpeningView.Designation = new List<long>();
            employeeBenefitFundSettingOpeningView.Branch.Add(branch.Id);
            employeeBenefitFundSettingOpeningView.Campus.Add(campus.Id);
            employeeBenefitFundSettingOpeningView.Department.Add(department.Id);
            employeeBenefitFundSettingOpeningView.Designation.Add(designation.Id);
            var depositDateTime = DateTime.Now;
            employeeBenefitFundSettingOpeningView.DepositDate = depositDateTime;
            employeeBenefitFundSettingOpeningView.EmploymentStatus = avaiableMember[0].MemberType;
            foreach (var forOpeningFund in avaiableMember)
            {
                employeeBenefitFundSettingOpeningView.EmployeeBenefitFundBalanceViewList = new List<EmployeeBenefitFundBalanceView>();
                forOpeningFund.DepositDate = depositDateTime;
                employeeBenefitFundSettingOpeningView.EmployeeBenefitFundBalanceViewList.Add(forOpeningFund);

            }
            employeeBenefitFundSettingOpeningView = new EmployeeBenefitFundSettingOpeningView();
            Assert.Throws<NullObjectException>(() => _memberEbfBalanceService.SaveOrUpdate(userMenu, employeeBenefitFundSettingOpeningView));

        }
    }
}
