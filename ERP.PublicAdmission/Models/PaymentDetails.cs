﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UdvashERP.PublicAdmission.Models
{
    public class PaymentDetails
    {
        public PaymentDetails()
        {
            PaymentTransactionViewModels = new List<PaymentTransactionViewModel>();
        }
        public virtual decimal OfferedDiscount { get; set; }

        [Display(Name = "Net Receivable")]
        public virtual decimal NetReceivable { get; set; }

        public virtual decimal CashBackAmount { get; set; }
        [Display(Name = "Receivable Amount")]
        public virtual decimal ReceivableAmount { get; set; }

        [Display(Name = "Consideration Amount")]
        public virtual decimal ConsiderationAmount { get; set; }

        [Display(Name = "Payable Amount")]
        public virtual decimal PayableAmount { get; set; }

        [Display(Name = "Discount Amount")]
        public virtual decimal DiscountAmount { get; set; }
        
        public virtual decimal CourseFee { get; set; }

        [Display(Name = "Received Amount")]
        public virtual decimal ReceivedAmount { get; set; }
        [Display(Name = "Due Amount")]
        public virtual decimal DueAmount { get; set; }
        [Display(Name = "Payment Method")]
        public virtual int PaymentMethod { get; set; }

        [Display(Name = "Special Discount")]
        public virtual decimal SpDiscountAmount { get; set; }
       
        public virtual long ReferrerId { get; set; }

        [Display(Name = "Reference Note")]
        public virtual string Remarks { get; set; }

        public virtual string NextReceivedDate { get; set; }

        [Display(Name = "Next Receive Note")]
        public virtual DateTime NextRecDate { get; set; }
 
        public IList<PaymentTransactionViewModel> PaymentTransactionViewModels { get; set; }  

    }
}