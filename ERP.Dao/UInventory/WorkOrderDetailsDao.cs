using System;
using System.Collections.Generic;
using System.Linq;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface IWorkOrderDetailDao : IBaseDao<WorkOrderDetails, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function

        bool HasManyItemObjects(Item obj);

        #endregion
        
    }

    public class WorkOrderDetailsDao : BaseDao<WorkOrderDetails, long>, IWorkOrderDetailDao
    {

        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function

        public bool HasManyItemObjects(Item obj)
        {
            IList<ProgramSessionItem> programSessionList =
                Session.QueryOver<ProgramSessionItem>()
                    .Where(x => x.Item.Id == obj.Id && x.Status != ProgramSessionItem.EntityStatus.Delete)
                    .List<ProgramSessionItem>();
            if (programSessionList.Count > 0)
                return false;

            IList<Quotation> quotationList =
                Session.QueryOver<Quotation>()
                    .Where(x => x.Item.Id == obj.Id && x.Status != Quotation.EntityStatus.Delete)
                    .List<Quotation>();
            if (quotationList.Count > 0) return false;

            IList<WorkOrderDetails> workOrderList =
                Session.QueryOver<WorkOrderDetails>()
                    .Where(x => x.Item.Id == obj.Id && x.Status != WorkOrderDetails.EntityStatus.Delete)
                    .List<WorkOrderDetails>();
            if (workOrderList.Count > 0)
                return false;

            IList<GoodsIssueDetails> goodsIssueList =
                Session.QueryOver<GoodsIssueDetails>()
                    .Where(x => x.Item.Id == obj.Id && x.Status != GoodsIssueDetails.EntityStatus.Delete)
                    .List<GoodsIssueDetails>();
            if (goodsIssueList.Count > 0)
                return false;

            IList<GoodsReturnDetails> goodsReturnList =
                Session.QueryOver<GoodsReturnDetails>()
                    .Where(x => x.Item.Id == obj.Id && x.Status != GoodsReturnDetails.EntityStatus.Delete)
                    .List<GoodsReturnDetails>();
            if (goodsReturnList.Count > 0)
                return false;

            IList<RequisitionDetails> requisitionList =
                Session.QueryOver<RequisitionDetails>()
                    .Where(x => x.Item.Id == obj.Id && x.Status != RequisitionDetails.EntityStatus.Delete)
                    .List<RequisitionDetails>();
            if (requisitionList.Count > 0)
                return false;

            return true;

        }

        #endregion

        #region Helper Function
        #endregion
        
    }
}
