﻿
function calculateTotal() {
    var studyMaterialsTotal = 0;
    var tutionFeeTotal = 0;
    var paymentTotal = 0;
    $(".guide").each(function () {
        if ($(this).val() != null && $(this).val() != '') {
            studyMaterialsTotal += parseFloat($(this).val());
        }
    });
    $(".tutionfee").each(function () {
        if ($(this).val() != null && $(this).val() != '') {
            tutionFeeTotal += parseFloat($(this).val());
        }
    });
    $(".totalpay").each(function () {
        if ($(this).val() != null && $(this).val() != '') {
            paymentTotal += parseFloat($(this).val());
        }
    });
    $("#StudyMaterialsTotal").html('<strong>' + studyMaterialsTotal.toFixed(2) + '</strong>');
    $("#TutionFeeTotal").html('<strong>' + tutionFeeTotal.toFixed(2) + '</strong>');
    $("#PaymentTotal").html('<strong>' + paymentTotal.toFixed(2) + '</strong>');
}

$(document).ready(function () {

    calculateTotal();
    $(document).on('change', 'input:checkbox, .guide, .tutionfee, .totalpay', function (event) {
        console.log("ok");
        calculateTotal();
    });

    var today = new Date();
    var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();
    var startDate = initialDate;
   //$('.form-date').datetimepicker({
   //     format: "yyyy-mm-dd",
   //     autoclose: true,
   //     todayBtn: false,
   //     showMeridian: true,
   //     initialDate: initialDate,
   //     startDate: startDate,
   //     //endDate: endDate,
   //     startView: 2,
   //     minView: 2,
   //     maxView: 4
    // });
    $('.form-date').datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: false,
        showMeridian: true,
        initialDate: new Date(),
        startView: 2,
        minView: 2,
        maxView: 4
    });
   var paymentErrorMessage = '<p class="errorMsg" style="color: #F00"> Field Value Invalid </p>';
    var successMess = "Course Add Successfully";
    var errorMess = "Course Add Failed";
    var updateMess = "Course Update Successfully";
    var ajaxError = "Slow Internet Connection";

    /*Get program Against Organization*/
    $(document).on('change', '#Organization', function (event) {

        $('#Program').empty();
        $('#Program').append("<option value=''>Select program</option>");

        $('#Session').empty();
        $('#Session').append("<option value=''>Select session</option>");

        var organizationId = $('#Organization').val();
        if (organizationId.length > 0) {
            $.ajax({
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                //url: "/Administration/Course/GetProgram",
                type: "POST",
                data: { organizationIds: organizationId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    //$('.programList').html(data);
                    //$("#Session").html("<option value=''>Select a Session</option>");
                    //$.unblockUI();
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnProgramList, function (i, v) {
                            $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    });

    /*Get Session Against Program*/
    $(document).on('change', '#Program', function (event) {

        $('#Session').empty();
        $('#Session').append("<option value=''>Select session</option>");
        var organizationId = $('#Organization').val();
        var program = $('#Program').val();

        if (program.length > 0) {
            $('#Program').removeClass('highlight');
            $.ajax({
                //url: "/Administration/Course/GetSession",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                type: "POST",
                data: { organizationIds: organizationId, programIds: program, isAuthorized: true },
                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    //$('.sessionListGenerate').html(data);
                    //$.unblockUI();
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnSessionList, function (i, v) {
                            $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function(data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        } else {
            $('#Program').addClass('highlight');
        }
    });

    /*Get Subjects According to program and session*/
    $(document).on('change', '#Session', function (data) {
        var program = $('#Program').val();
        var session = $('#Session').val();
        if (program.length > 0 && session.length > 0) {
            $('#Session').removeClass('highlight');
            $.ajax({
                url: "/Administration/Course/GetAssignedSubject",
                type: "GET",
                data: { program: program, session: session },
                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function(data1) {
                    $('.subList').html(data1);
                    $.unblockUI();
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function(data1) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                    //$('.customMessage').append(errorMessage);
                }
            });
        } else {
            $('#Session').addClass('highlight');
            $('.subList').html("");
        }
    });


    $(document).on('click', '.checkAllSubject', function() {
        $('#courseFees td input[type=checkbox]').prop('checked', this.checked);
        if (this.checked) {
            $('#courseFees td input[type=number]').removeAttr('disabled');
            $('#courseFees td input[type=number]').keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                return true;
            });
        } else {
            $('#courseFees td input[type=number]').val('');
            $('#courseFees td input[type=number]').attr('disabled', 'disabled');

        }
    });

    $(document).on('click', 'input:checkbox', function (data) {
        var $tblRow = $(this).parent().parent();
        if ($(this).is(':checked')) {
            $(this).prop('checked', true);
            $tblRow.find('input[type=number]').removeAttr('disabled');
            $tblRow.find('input[type=number]').keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                return true;
            });
        } else {
            $(this).prop('checked', false);
            var textValue = $tblRow.find('input[type=number]').val();
            if (textValue != null) {
                if (textValue.length > 0) {
                    $tblRow.find('input[type=number]').val('');
                }
            }
            $tblRow.find('input[type=number]').attr('disabled', 'disabled');
        }
    });

    //$(document).on('click', 'input:checkbox', function (data) {
        
    //    var chkBoxId = $(this).attr('id');
    //    var firstTdWhichContainsCheckbox = $(this).parent();
    //    var secondTdWhichContainsTextbox = firstTdWhichContainsCheckbox.next();
    //    var textboxId = secondTdWhichContainsTextbox.find('input[type=number]').attr('id');
    //    /*Check is Checkbox is checked */
    //    if ($(this).is(':checked')) {
    //        $(this).prop('checked', true);
    //        /* Compare checkbox Id and TextBox Id if same then enable checkbox */
    //        if (chkBoxId === textboxId) {
    //            secondTdWhichContainsTextbox.find('input[type=number]').removeAttr('disabled');
    //        }
    //        secondTdWhichContainsTextbox.find('input[type=number]').keypress(function (e) {
    //            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    //                return false;
    //            }
    //            return true;
    //        });
    //    }
    //    else
    //    {
    //        /* Checkbox disable */
    //        $(this).prop('checked', false);
    //        /*If textbox is not empty and disable the checkbox then clear textbox value and disable text box */
    //        var textValue = secondTdWhichContainsTextbox.find('input[type=number]').val();
    //        if (textValue != null) {
    //            if (textValue.length > 0) {
    //                secondTdWhichContainsTextbox.find('input[type=number]').val('');
    //            }
    //        }
    //        secondTdWhichContainsTextbox.find('input[type=number]').attr('disabled', 'disabled');
    //    }
    //});

    function checkValidateForCourseAdd() {

        var isValid = true;
        $('#Name').removeClass('highlight');
        $('#StartDate').removeClass('highlight');
        $('#EndDate').removeClass('highlight');
        $('#MaxSubject').removeClass('highlight');
        $('#OfficeMinSubject').removeClass('highlight');
        $('#PublicMinSubject').removeClass('highlight');
        $('#OfficeMinPayment').removeClass('highlight');
        $('#PublicMinPayment').removeClass('highlight');
        $('#Program').removeClass('highlight');
        $('#Session').removeClass('highlight');
        $('input').removeClass('highlight');
        $('.errorMsg').remove();
        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var courseName = $('#Name').val();
        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();
        var maxSubject = $('#MaxSubject').val();
        var offMinSub = $('#OfficeMinSubject').val();
        var pubMinSub = $('#PublicMinSubject').val();
        var officeMinPayment = $('#OfficeMinPayment').val();
        var publicMinPayment = $('#PublicMinPayment').val();
        if (programId.length <= 0) { $('#Program').addClass('highlight'); isValid = false; }
        if (sessionId.length <= 0) { $('#Session').addClass('highlight'); isValid = false; }
        if (courseName.length <= 0) { $('#Name').addClass('highlight'); isValid = false; }
        if (startDate.length <= 0) { $('#StartDate').addClass('highlight'); isValid = false; }
        if (endDate.length <= 0) { $('#EndDate').addClass('highlight'); isValid = false; }
        if (maxSubject.length <= 0 || parseInt(maxSubject) <= 0) { $('#MaxSubject').addClass('highlight'); isValid = false; }
        if (offMinSub.length <= 0 || parseInt(offMinSub) <= 0) { $('#OfficeMinSubject').addClass('highlight'); isValid = false; }
        if (pubMinSub.length <= 0 || parseInt(pubMinSub) <= 0) { $('#PublicMinSubject').addClass('highlight'); isValid = false; }
        if (officeMinPayment.length <= 0 || parseInt(officeMinPayment) < 0 || parseInt(officeMinPayment) > 100) { $('#OfficeMinPayment').addClass('highlight'); isValid = false; }
        if (publicMinPayment.length <= 0 || parseInt(publicMinPayment) < 0 || parseInt(publicMinPayment) > 100) { $('#PublicMinPayment').addClass('highlight'); isValid = false; }
        var totalChecked = $("#courseFees td input[type=checkbox]:checked").length;
        if (maxSubject > totalChecked) { $('#MaxSubject').addClass('highlight'); isValid = false; }
        if (offMinSub > maxSubject) { $('#OfficeMinSubject').addClass('highlight'); isValid = false; }
        if (pubMinSub > maxSubject) { $('#PublicMinSubject').addClass('highlight'); isValid = false; }
        return isValid;
    }

    function checkValidateForCourseUpdate() {

        var isValid = true;
        $('#Name').removeClass('highlight');
        $('#MaxSubject').removeClass('highlight');
        $('#StartDate').removeClass('highlight');
        $('#EndDate').removeClass('highlight');
        $('#OfficeMinSubject').removeClass('highlight');
        $('#PublicMinSubject').removeClass('highlight');
        $('#OfficeMinPayment').removeClass('highlight');
        $('#PublicMinPayment').removeClass('highlight');
        $('input').removeClass('highlight');
        $('.errorMsg').remove();
        $('.messageFieldForCustomValidation').remove();
        var programId = $('#Program_Id').val();
        var sessionId = $('#RefSession_Id').val();
        var courseName = $('#Name').val();
        var maxSubject = $('#MaxSubject').val();
        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();
        var offMinSub = $('#OfficeMinSubject').val();
        var pubMinSub = $('#PublicMinSubject').val();
        var officeMinPayment = $('#OfficeMinPayment').val();
        var publicMinPayment = $('#PublicMinPayment').val();
        if (programId.length <= 0) { $('#Program_Id').addClass('highlight'); isValid = false; }
        if (sessionId.length <= 0) { $('#RefSession_Id').addClass('highlight'); isValid = false; }
        if (courseName.length <= 0) { $('#Name').addClass('highlight'); isValid = false; }
        if (maxSubject.length <= 0 || parseInt(maxSubject) <= 0) { $('#MaxSubject').addClass('highlight'); isValid = false; }
        if (startDate.length <= 0) { $('#StartDate').addClass('highlight'); isValid = false; }
        if (endDate.length <= 0) { $('#EndDate').addClass('highlight'); isValid = false; }
        if (offMinSub.length <= 0 || parseInt(offMinSub) <= 0) {$('#OfficeMinSubject').addClass('highlight'); isValid = false;}
        if (pubMinSub.length <= 0 || parseInt(pubMinSub) <= 0) { $('#PublicMinSubject').addClass('highlight'); isValid = false; }
        if (officeMinPayment.length <= 0 || parseInt(officeMinPayment) < 0 || parseInt(officeMinPayment) > 100) { $('#OfficeMinPayment').addClass('highlight'); isValid = false; }
        if (publicMinPayment.length <= 0 || parseInt(publicMinPayment) < 0 || parseInt(publicMinPayment) > 100) { $('#PublicMinPayment').addClass('highlight'); isValid = false; }
        var totalChecked = $("#courseFees td input[type=checkbox]:checked").length;
        if (maxSubject > totalChecked) { $('#MaxSubject').addClass('highlight'); isValid = false; }
        if (parseInt(offMinSub) > parseInt(maxSubject)) {$('#OfficeMinSubject').addClass('highlight'); isValid = false;}
        if (parseInt(pubMinSub) > parseInt(maxSubject)) { $('#PublicMinSubject').addClass('highlight'); isValid = false; }
        if (new Date(startDate) > new Date(endDate)) {$('#EndDate').addClass('highlight'); isValid = false;}
        return isValid;
    }

    /*validation check */
    $(document).on('click', '#updateCourse', function (data) {
        var programId = $('#Program_Id').val();
        var sessionId = $('#RefSession_Id').val();
        var courseName = $('#Name').val();
        var maxSubject = $('#MaxSubject').val();
        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();
        var offMinSub = $('#OfficeMinSubject').val();
        var pubMinSub = $('#PublicMinSubject').val();

        var offMinPay = $('#OfficeMinPayment').val();
        var pubMinPay = $('#PublicMinPayment').val();
        var offComp;
        var pubComp;

        var checkTrue = checkValidateForCourseUpdate();
        var isReqSendToServer = true;
        
        var courseId = $('#Id').val();
        var status = $('#status').val();
        var courseFeesObj = [];
        var isPaymentFieldNotEmpty = false;
        if (checkTrue) {
           
            if ($("#OfficeCompulsory").is(":checked")) offComp = true;
            else offComp = false;

            if ($("#PublicCompulsory").is(":checked")) pubComp = true;
            else pubComp = false;

            //alert(offMinSub + "//" + pubMinSub + "//" + offMinPay + "//" + pubMinPay + "//" + offComp + "//" + pubComp);

            if (courseName.length > 0) {
                /*Get SubjectId and Subject corresponding payment */
                //$('#courseFees tr').each(function (index, value) {
                //    var courseFees = {};
                //    if ($(this).find('input[type=checkbox]').is(':checked')) {
                //        //if ($(this).find('.subPay').is(':checked')) {
                //        /*Empty textbox check */
                //        if ($(this).find('input[type=number]').val().length >= 0) {
                //            isPaymentFieldNotEmpty = true;
                //            var subPayVal = $(this).find('input[type=number]').val();
                //            subPayVal = Number(subPayVal);
                //            var subPayment = parseFloat(subPayVal);
                //            if (subPayment >= 0 && subPayment < 100000) {
                //                courseFees[$(this).find('input[type=checkbox]').attr('id')] = $(this).find('input[type=number]').val();
                //                courseFees[$(this).find('input[type=checkbox]').attr('data-courseId')] = $(this).find('input[type=checkbox]').attr('data-courseId');
                //                courseFeesObj.push(JSON.stringify(courseFees));
                //            } else {
                //                isReqSendToServer = false;
                //                $(this).find('input[type=number]').addClass('highlight');
                //            }
                //        } else {
                //            $(this).find('input[type=number]').addClass('highlight');
                //            $(this).find('input[type=number]').after(paymentErrorMessage);
                //            isPaymentFieldNotEmpty = false;
                //            return false;
                //        }
                //    }
                //});

                $('#courseFees tr').each(function (index, value) {

                    var courseFees = {};
                    //if ($(this).find('input[type=checkbox]').is(':checked')) {
                    if ($(this).find('input[type=checkbox]').is(':checked')) {

                        var $parent = $(this);
                        var guide = $parent.find(".guide").val();
                        var tutionfee = $parent.find(".tutionfee").val();
                        var totalpay = $parent.find(".totalpay").val();
                        if (guide == "") {
                            guide = 0;
                        }
                        if (tutionfee == "") {
                            tutionfee = 0;
                        }
                        
                       // var totalPay = guide + tutionfee;

                        var id = $(this).find('input[type=checkbox]').attr('id');
                        var courseid = $(this).find('input[type=checkbox]').attr('data-courseId');
                        /* Empty textbox check */
                        if (totalpay >= 0) {
                            guide = parseInt(guide);
                            tutionfee = parseInt(tutionfee);
                            totalpay = parseInt(totalpay);
                            isPaymentFieldNotEmpty = true;
                            if ((totalpay >= 0 && totalpay < 100000)) {
                                if (totalpay >= 0) {
                                    courseFees["id"] = id;
                                    courseFees["courseid"] = courseid;
                                    courseFees["guide"] = guide;
                                    courseFees["tutionfee"] = tutionfee;
                                    courseFees["totalPay"] = totalpay;
                                    courseFeesObj.push(JSON.stringify(courseFees));
                                } else {
                                    $(this).find('input[type=number]').after(paymentErrorMessage);
                                }
                            } else {
                                isReqSendToServer = false;
                                $(this).find('input[type=number]').addClass('highlight');
                            }

                        } else {
                            $(this).find('input[type=number]').addClass('highlight');
                            $(this).find('input[type=number]').after(paymentErrorMessage);
                            isPaymentFieldNotEmpty = false;
                            return false;
                        }
                    }
                });
                console.log(courseFeesObj);
                if (courseFeesObj.length > 0) {
                    console.log("ENTERED ");
                    if (isPaymentFieldNotEmpty == true && isReqSendToServer) {
                        console.log("ENTERED ");
                        $.ajax({
                            url: "/Administration/Course/Edit",
                            type: "POST",
                            data: {
                                courseFeesObjArray: courseFeesObj, programId: programId, sessionId: sessionId, courseName: courseName,  maxSubject: maxSubject, courseId: courseId, status: status
                                , offMinSub: offMinSub, pubMinSub: pubMinSub, offMinPay: offMinPay, pubMinPay: pubMinPay,
                                offComp: offComp, pubComp: pubComp, startDate: startDate, endDate: endDate
                            },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data) {
                                $.unblockUI();
                                if (data.IsSuccess) {
                                    $.fn.customMessage({
                                        displayMessage: updateMess,
                                        displayMessageType: "s",
                                    });
                                } else {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "error",
                                    });
                                }
                                $('html, body').animate({ scrollTop: 0 }, 0);
                            },
                            error: function () {
                                $.unblockUI();
                                $.fn.customMessage({
                                    displayMessage: ajaxError,
                                    displayMessageType: "error",
                                    displayTime: 5000
                                });
                                $('html, body').animate({ scrollTop: 0 }, 0);
                            }
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: "Select at least one subject with valid payment",
                            displayMessageType: "w",
                        });
                    }
                } else {
                    $.fn.customMessage({
                        displayMessage: "Select Atleast One Subject With Valid Payment",
                        displayMessageType: "w",
                    });
                }
            } else {
                $.fn.customMessage({
                    displayMessage: "Invalid Course Name",
                    displayMessageType: "info",
                    displayTime: 5000
                });
                $('#Name').addClass('highlight');
                $('#Name').after("<p class='messageFieldForCustomValidation' style ='color:red'>Invalid Name Found</p>");
            }
        } else {
            
            $.fn.customMessage({
                displayMessage: "Please fill your input field that is highlighted",
                displayMessageType: "e",
            });
        }
    });

    /*COURSE ADD SCOPE*/
    $(document).on('click', '#courseAdd', function (data) {
        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var courseName = $('#Name').val();
        var maxSubject = $('#MaxSubject').val();
        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();
        var offMinSub = $('#OfficeMinSubject').val();
        var pubMinSub = $('#PublicMinSubject').val();
        var offMinPay = $('#OfficeMinPayment').val();
        var pubMinPay = $('#PublicMinPayment').val();
        var paidStatus = $('#PaidStatus').val();
        var offComp ;
        var pubComp ;
        //alert(offComp+"//"+pubComp);
        var checkTrue = checkValidateForCourseAdd();
        //alert(check);
        var isReqSendToServer = true;
        var courseFeesObj = [];
        var isPaymentFieldNotEmpty = false;
        if (checkTrue) {
            //alert(checkTrue);
            if ($("#OfficeCompulsory").is(":checked")) offComp = true;
            else  offComp = false;

            if ($("#PublicCompulsory").is(":checked")) pubComp = true;
            else pubComp = false;

            
            /*Get SubjectId and Subject corresponding payment */
            $('#courseFees tr').each(function (index, value) {
                
                var courseFees = {};
                //if ($(this).find('input[type=checkbox]').is(':checked')) {
                if ($(this).find('.subPay').is(':checked')) {

                    var $parent = $(this);
                    var guide = $parent.find(".guide").val();
                    var tutionfee = $parent.find(".tutionfee").val();
                    var totalpay = $parent.find(".totalpay").val();
                    
                    if (guide == "") {
                        guide = 0;
                    }
                    if (tutionfee == "") {
                        tutionfee = 0;
                    }
                    
                    //var totalPay = guide + tutionfee;
                    console.log(totalpay);
                    var id = $(this).find('.subPay').attr('id');
                    /* Empty textbox check */
                    if (totalpay.length > 0) {
                        guide = parseInt(guide);
                        tutionfee = parseInt(tutionfee);
                        totalpay = parseInt(totalpay);
                        isPaymentFieldNotEmpty = true;
                        if ((totalpay >= 0 && totalpay < 100000)) {
                            if (totalpay >= 0) {
                                courseFees["id"] = id;
                                courseFees["guide"] = guide;
                                courseFees["tutionfee"] = tutionfee;
                                courseFees["totalPay"] = totalpay;
                                    courseFeesObj.push(JSON.stringify(courseFees));
                                } else {
                                    $(this).find('input[type=number]').after(paymentErrorMessage);
                                }
                        } else {
                            isReqSendToServer = false;
                            $(this).find('input[type=number]').addClass('highlight');
                        }

                    } else {
                        $(this).find('input[type=number]').addClass('highlight');
                        $(this).find('input[type=number]').after(paymentErrorMessage);
                        isPaymentFieldNotEmpty = false;
                        return false;
                    }
                }
            });
            console.log(courseFeesObj);
            if (courseFeesObj.length > 0) {
                if (isPaymentFieldNotEmpty == true && isReqSendToServer) {
                    console.log("isPayment: " + courseName);
                    console.log("isReqSendToServer: " + isReqSendToServer);
                    $.ajax({
                        url: "/Administration/Course/AddCourse",
                        type: "POST",
                        data: {
                            courseFeesObjArray: courseFeesObj, programId: programId, sessionId: sessionId, courseName: courseName,
                            startDate:startDate, endDate:endDate,
                            maxSubject: maxSubject, offMinSub: offMinSub, pubMinSub: pubMinSub, offMinPay: offMinPay, pubMinPay: pubMinPay,
                            offComp: offComp, pubComp: pubComp
                        },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (data) {
                            $.unblockUI();
                            if (data.IsSuccess) {
                                $.fn.customMessage({
                                    displayMessage: successMess,
                                    displayMessageType: "s",
                                    //displayTime: 5000
                                });

                            } else {
                                $.fn.customMessage({
                                    //displayMessage: ERROR_MESS,
                                    displayMessage: data.Message,
                                    displayMessageType: "e",
                                    //displayTime: 5000
                                });
                            }
                        },
                        error: function () {
                            $.unblockUI();
                            $.fn.customMessage({
                                displayMessage: ajaxError,
                                displayMessageType: "error",
                                displayTime: 5000
                            });
                        }
                    });
                } else {
                    $.fn.customMessage({
                        displayMessage: "Missing Payment",
                        displayMessageType: "w",
                        //displayTime: 5000
                    });
                }

            } else {
                $.fn.customMessage({
                    displayMessage: "Select at least one subject with valid payment",
                    displayMessageType: "w",
                    //displayTime: 5000
                });
            }
        } else {
            $.fn.customMessage({
                displayMessage: errorMess,
                displayMessageType: "error",
            });
        }
        console.log(courseFeesObj);
    });
   
    /*COURSE UPDATE SCOPE*/   

    window.validateTextField("Name", "");
    $(document).on("change", "#StartDate", function (event) {
        var val = $("#StartDate").val();
        if (val.length > 0) {
            window.showErrorMessageBelowCtrl("StartDate", "", false);
        }
    });

    $(document).on("change", "#EndDate", function (event) {
        var val = $("#EndDate").val();
        if (val.length > 0) {
            window.showErrorMessageBelowCtrl("EndDate", "", false);
        }
    });

    $("#MaxSubject").keyup(function () {
        var courseFeesRowCount = $('#courseFees tr').length;
        var maxSubject = parseInt($("#MaxSubject").val());
        if (maxSubject != "" && maxSubject > 0 && maxSubject < courseFeesRowCount) $('#MaxSubject').removeClass('highlight');
        else $('#MaxSubject').addClass('highlight');
    });
    $("#OfficeMinSubject").keyup(function () {
        var officeMinSubject = parseInt($("#OfficeMinSubject").val());
        var maxSubject = parseInt($("#MaxSubject").val());
        if (officeMinSubject != "" && officeMinSubject > 0 && officeMinSubject <= maxSubject) {
            $('#OfficeMinSubject').removeClass('highlight');
        } else {
            $('#OfficeMinSubject').addClass('highlight');
        }
    });
    $("#PublicMinSubject").keyup(function () {
        var publicMinSubject = parseInt($("#PublicMinSubject").val());
        var maxSubject = parseInt($("#MaxSubject").val());
        if (publicMinSubject != "" && publicMinSubject > 0 && publicMinSubject <= maxSubject){ $('#PublicMinSubject').removeClass('highlight');}
        else {$('#PublicMinSubject').addClass('highlight');}
    });
    $("#OfficeMinPayment").keyup(function () {
        var officeMinPayment = parseInt($("#OfficeMinPayment").val());
        if ($("#OfficeMinPayment").val() != "" && officeMinPayment >= 0 && officeMinPayment <= 100) { $('#OfficeMinPayment').removeClass('highlight'); }
        else {$('#OfficeMinPayment').addClass('highlight');}
    });
    $("#PublicMinPayment").keyup(function () {
        var publicMinPayment = parseInt($("#PublicMinPayment").val());
        if ($("#PublicMinPayment").val() != "" && publicMinPayment >= 0 && publicMinPayment <= 100) $('#PublicMinPayment').removeClass('highlight');
        else $('#PublicMinPayment').addClass('highlight');
    });
    $("#MaxSubject,#OfficeMinSubject,#PublicMinSubject,#OfficeMinPayment,#PublicMinPayment").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
        return true;
    });
    $('#courseFees td input[type=number]').keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
        return true;
    });

    
    $(document).on("input propertychange", ".guide,.tutionfee", function (event) {
        var $parent = $(this).parent().parent();
        var guide = $parent.find(".guide").val();
        if (guide=="") {
            guide = 0;
        }
        var tutionfee = $parent.find(".tutionfee").val();
        if (tutionfee == "") {
            tutionfee = 0;
        }
        var total = parseInt(tutionfee) + parseInt(guide);

        console.log(guide);
        console.log(tutionfee);

        $parent.find(".totalpay").val(total);
    });
});