﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Mapping.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public class SmsHistoryMap : BaseClassMap<SmsHistory, long>
    {
        public SmsHistoryMap()
        {
            Table("SmsHistory");
            LazyLoad();
            Map(x => x.Rank);
            Map(x => x.Sms);
            Map(x => x.ReceiverNumber);
            Map(x => x.ResponseCode);
            Map(x => x.ErrorCode);
            Map(x => x.Mask);
            Map(x => x.CampaignName);
            Map(x => x.Type);
            Map(x => x.SendingTryCount);
            Map(x => x.Priority);
            References(x => x.Organization).Column("OrganizationId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Batch).Column("BatchId");
            References(x => x.SmsReceiver).Column("SmsReceiverId");
            References(x => x.SmsSettings).Column("SmsSettingsId");
            References(x => x.Student).Column("StudentId");
        }
    }
}
