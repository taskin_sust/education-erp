﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Common
{
    public interface IPostOfficeDao : IBaseDao<Postoffice, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        //IList<Postoffice> LoadPostOffice(List<long> divisionIdList = null, List<long> districtIdList = null, List<long> thanaIdList = null);
        IList<Postoffice> LoadPostOffice(List<long> divisionIdList = null, List<long> districtIdList = null);
        #endregion

        #region Others Function

        #endregion
    }
    public class PostOfficeDao : BaseDao<Postoffice, long>, IPostOfficeDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        //public IList<Postoffice> LoadPostOffice(List<long> divisionIdList = null, List<long> districtIdList = null, List<long> thanaIdList = null)
        public IList<Postoffice> LoadPostOffice(List<long> divisionIdList = null, List<long> districtIdList = null)
        {
            ICriteria criteria = Session.CreateCriteria<Postoffice>().Add(Restrictions.Eq("Status", Postoffice.EntityStatus.Active));
            criteria.CreateAlias("District", "district").Add(Restrictions.Eq("district.Status", District.EntityStatus.Active));

            if (divisionIdList != null && !divisionIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.CreateAlias("district.Division", "division").Add(Restrictions.Eq("division.Status", Division.EntityStatus.Active));
                criteria.Add(Restrictions.In("division.Id", divisionIdList));
            }
            if (districtIdList != null && !districtIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("district.Id", districtIdList));
            }
            criteria.AddOrder(Order.Asc("Name"));
            return criteria.List<Postoffice>();
        }
        #endregion

        #region Others Function

        #endregion
    }
}