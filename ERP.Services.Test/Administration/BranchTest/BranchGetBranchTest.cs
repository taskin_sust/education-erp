﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Test.AdminisTration.BranchTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BranchTest
{
    interface IBranchGetBranchTest
    {
        void Should_Return_GetBranch_Null_Check();
        void Should_Return_GetBranch_NotNull_Check();
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Branch")]
    public class BranchGetBranchTest : BranchBaseTest, IBranchGetBranchTest
    {
        [Fact]
        public void Should_Return_GetBranch_Null_Check()
        {
            var obj = _branchService.GetBranch(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void Should_Return_GetBranch_NotNull_Check()
        {
            BranchFactory branchFactory =_branchFactory.Create().WithOrganization().Persist();
            var obj = _branchService.GetBranch(branchFactory.Object.Id);
            Assert.NotNull(obj);

        }
    }

}
