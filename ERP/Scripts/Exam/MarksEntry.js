function autoCompleteFunc() {
    $('.autoComplete').each(function () {
        var $this = $(this);
            $this.typeahead({
                source: function (query, process) {
                    var programId = $("#Program").val();
                    var sessionId = $("#Session").val();
                    var courseId = $("#Course").val();
                    var data = { query: query, programId: programId, sessionId: sessionId, courseId: courseId, isMcq:true }
                    $.ajax({
                        type: "POST",
                        url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForExamCoad",
                        cache: false,
                        async: true,
                        data: data,

                        success: function(result) {
                            if (result.IsSuccess) {
                                process(result.returnList);
                            }
                        },
                        complete: function() {
                        },
                        error: function(result) {
                        }
                    });
                },
                updater: function(item) {
                    $("#ExamId").val(item.Value);
                    $("#ExamType option").remove();
                    $("#ExamCode").val("");
                    $("#ExamName").val("");
                    $.ajax({
                        type: "post",
                        url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForExamInformation",
                        cache: false,
                        async: true,
                        data: { "id": item.Value },
                        beforeSend: function() {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function(result) {
                            $.unblockUI();
                            if (result.IsSuccess) {
                                console.log(result.returnList.Name);
                                if (result.returnList.hasOwnProperty("Name")) {
                                    $("#ExamName").val(result.returnList.Name);
                                    showErrorMessageBelowCtrl("ExamCode", "", false);
                                }
                                if (result.returnList.IsMcq && result.returnList.IsWritten) {
                                    showErrorMessageBelowCtrl("ExamType", "", false);
                                    $('#ExamType').append("<option value='" + result.returnList.IsMcqValue + "'>MCQ</option>");
                                    //$('#ExamType').append("<option value='" + result.returnList.IsWrittenValue + "'>Written</option>");
                                }
                                else if (result.returnList.IsMcq && !result.returnList.IsWritten) {
                                     showErrorMessageBelowCtrl("ExamType", "", false);
                                    $('#ExamType').append("<option value='" + result.returnList.IsMcqValue + "'>MCQ</option>");
                                }
                                
                                else {
                                    showErrorMessageBelowCtrl("ExamType", "", false);
                                    showErrorMessageBelowCtrl("ExamType", "The Exam Type field is required.", true);
                                }
                                //console.log(result.returnList.IsTakeAnswerSheetWhenUploadMarks);
                                //alert(result.returnList.IsTakeAnswerSheetWhenUploadMarks);
                                $("#IsTakeAnswerSheetWhenUploadMarks").val(result.returnList.IsTakeAnswerSheetWhenUploadMarks);
                                if (result.returnList.TotalQuestion > 0 && result.returnList.IsTakeAnswerSheetWhenUploadMarks == 'True') {
                                    $(".marksEntryUp").hide();
                                    $(".marksEntryNewSubmit").show();
                                    $("#marksEntryByXml").show();
                                    var tquestion = result.returnList.TotalQuestion;
                                    $("#marksEntryByXml tbody").empty();
                                    $("#tq").val(tquestion);
                                    var inputField = "";
                                    for (var i = 1; i <= tquestion; i++) {
                                        inputField += "<div class='col-md-2'><input style='margin-bottom:3px;' disabled='disabled' maxlength='4' type='text' value='' placeholder='" + i + "' name='' id='' class='ansField form-control text-box single-line'></div>";
                                    }
                                    var html = "<tr id='rowId_1'>" +
                                        "<td style='text-align: center; font-weight: bold; ' ><input onchange='toggleCheckbox(this)' class='chk' type='checkbox' value='1' id='1' class='rowId1'>&nbsp;&nbsp;A</td>" +
                                        "<td>" + inputField + "</td>" +
                                        "</tr>";
                                    $("#marksEntryByXml tbody").append(html);
                                } else {
                                    $(".marksEntryUp").show();
                                    $(".marksEntryNewSubmit").hide();
                                    $("#marksEntryByXml").hide();
                                }
                            } else {
                                $.fn.customMessage({
                                    displayMessage: result.Message,
                                    displayMessageType: "error"
                                });
                            }
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        error: function(result) {
                            $.unblockUI();
                            console.log("Failed -----");
                        }
                    });
                    return item.Text;
                },
                matcher: function(item) {
                    if (item.Text.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                        return true;
                    }
                    return false;
                },
                sorter: function(items) {
                    return items.sort();
                },
                highlighter: function(item) {
                    var regex = new RegExp('(' + this.query + ')', 'gi');
                    return item.Text.replace(regex, "<strong>$1</strong>");
                },
                onselect: function(obj) {
                    alert('Selected ' + obj);
                }
            });
        //}
        //else {
        //    $("#Course").parent().find("span").removeClass(".field-validation-valid").addClass("field-validation-error").text("The Course field is required.");
        //}
        
    });
}

$(document).on("input propertychange", ".ansField ", function (event) {
    if (!(/^[A-Da-d]+$/.test($(this).val()))) {
        $(this).val($(this).val().slice(0, -1));
    }
    else {
        console.log($(this).val());
        if ((/^[A-Da-d]+$/.test($(this).val()))) {
            $(this).val($(this).val().toUpperCase());
        }
    }
});

$(document).ready(function () {
    $(".marksEntryNewSubmit").hide();
    $("#marksEntryByXml").hide();
    $("#AddSetCode").click(function () {
        var lastCodeId = parseInt($('#marksEntryByXml tbody input[type=checkbox]:last').attr('id'));
        var code;
        var setCodeId = lastCodeId + 1;
        if (setCodeId <= 10) {
            switch (setCodeId) {
                case 1:
                    code = "A";
                    break;
                case 2:
                    code = "B";
                    break;
                case 3:
                    code = "C";
                    break;
                case 4:
                    code = "D";
                    break;
                case 5:
                    code = "E";
                    break;
                case 6:
                    code = "F";
                    break;
                case 7:
                    code = "G";
                    break;
                case 8:
                    code = "H";
                    break;
                case 9:
                    code = "I";
                    break;
                case 10:
                    code = "J";
                    break;
            }
            var totalQuestion = parseInt($("#tq").val());
            var inputField = "";
            if (totalQuestion > 0) {
                for (var i = 1; i <= totalQuestion; i++) {
                    inputField += "<div class='col-md-2'><input style='margin-bottom:3px;' disabled='disabled' maxlength='4' type='text' value='' placeholder='" + i + "' name='' id='' class='ansField form-control text-box single-line'></div>";
                }
                var html = "<tr id='rowId_" + setCodeId + "'>" +
                            "<td style='text-align: center; font-weight: bold; '><input onchange='toggleCheckbox(this)' class='chk' type='checkbox' value='" + setCodeId + "' id='" + setCodeId + "'>&nbsp;&nbsp;" + code + "</td>" +
                            "<td>" + inputField + "</td>" +
                            "</tr>";
                $("#marksEntryByXml tbody").append(html);
            }
        }
    });
});

function toggleCheckbox(element) {
    //console.log($(element).parent().next("td"));
    var nextTd = $(element).parent().next("td");
   // console.log(nextTd);
    if (element.checked) {
        nextTd.find("input").each(function() {
            $(this).removeAttr("disabled");
        });
    } else {

        nextTd.find("input").each(function () {
            $(this).attr("disabled", true);
            $(this).val("");
        });
    }
}
$(document).on("input propertychange", ".autoComplete", function (event) {
    $("#marksEntryByXml tbody").empty();
    autoCompleteFunc();
    if ($(this).val() == "") {
        $("#ExamCode").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").text("The Exam Code field is required.");
    }
});
//get file size
function GetFileSize(fileid) {
    try {
        var fileSize = 0;
        //for IE
        if ($.browser.msie) {
            //before making an object of ActiveXObject,
            //please make sure ActiveX is enabled in your IE browser
            var objFSO = new ActiveXObject("Scripting.FileSystemObject"); var filePath = $("#" + fileid)[0].value;
            var objFile = objFSO.getFile(filePath);
            var fileSize = objFile.size; //size in kb
            fileSize = fileSize / 1048576; //size in mb
        }
            //for FF, Safari, Opeara and Others
        else {
            fileSize = $("#" + fileid)[0].files[0].size //size in kb
            fileSize = fileSize / 1048576; //size in mb
        }
        return fileSize;
    }
    catch (e) {
        alert("Error is :" + e);
    }
}

//get file path from client system
function getNameFromPath(strFilepath) {
    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);
    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }
}

function checkFileField() {
    
    var file = getNameFromPath($("#FileField").val());
   
    var flag = false;
    if (file != null) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'xml':
                flag = true;
                break;
            default:
                flag = false;
        }
    }
    
    if (flag == false) {
        $("#FileField").parent().find("span").removeClass(".field-validation-valid").addClass("field-validation-error").text("You can upload only xml extension file");
        return false;
    }
    else {
        //var size = GetFileSize('FileField');
        //if (size > 3) {
        //    $(".lifile > span").text("You can upload file up to 3 MB");
        //    $(this).parent().find("span").removeClass(".field-validation-valid").addClass("field-validation-error").text("You can upload only xml extension file");
        //}
        //else {
        // $(".lifile > span").text("");
        // }
        
        $("#FileField").parent().find("span").removeClass(".field-validation-error").addClass("field-validation-valid").text("");
        return true;
    }
}

function responseReturn() {

    var check = true;
    var isTakeAnswerSheetWhenUploadMarks = $("#IsTakeAnswerSheetWhenUploadMarks").val();
    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var course = $("#Course").val();
    var examCode = $("#ExamCode").val();
    var examType = $("#ExamType").val();
    if (organizationId == "" || organizationId == null) {
        showErrorMessageBelowCtrl("Organization", "", false);
        showErrorMessageBelowCtrl("Organization", "The Organization field is required.", true);
        check = false;
    }
   
    if (programId == "" || programId == null) {
        showErrorMessageBelowCtrl("Program", "", false);
        showErrorMessageBelowCtrl("Program", "The Program field is required.", true);
        check = false;
    }
    
    if (sessionId == "" || sessionId == null) {
        showErrorMessageBelowCtrl("Session", "", false);
        showErrorMessageBelowCtrl("Session", "The Session field is required.", true);
        check = false;
    }
    
    if (course == "" || course == null) {
        showErrorMessageBelowCtrl("Course", "", false);
        showErrorMessageBelowCtrl("Course", "The Course field is required.", true);
        check = false;
    }

    
    if (examCode == "" || examCode == null) {
        showErrorMessageBelowCtrl("ExamCode", "", false);
        showErrorMessageBelowCtrl("ExamCode", "The Exam Code field is required.", true);
        check = false;
    }
    
    if (examType == "" || examType == null) {
        showErrorMessageBelowCtrl("ExamType", "", false);
        showErrorMessageBelowCtrl("ExamType", "The Exam Type field is required.", true);
        check = false;
    }
    if (!checkFileField()) {       
        check = false;
    }

    if (check) {
        if (isTakeAnswerSheetWhenUploadMarks == "True") {
            var favorite = [];
            var checkSetAnswerSheet;
            var allAnswerSheet = [];
            //var marksEntryModel = {};
            $.each($("input[class='chk']:checked"), function() {
                favorite.push($(this).val());
                var nextTd = $(this).parent().next("td");
                nextTd.find("input").each(function() {
                    favorite.push($(this).val());
                });
                //console.log("favorite :" + favorite);
                checkSetAnswerSheet = favorite.join(", ");
                //console.log("checkSetAnswerSheet :" + checkSetAnswerSheet);
                allAnswerSheet.push(checkSetAnswerSheet + "/");
                //console.log("allAnswerSheet :" + allAnswerSheet);
                favorite = [];
            });
            //alert("5555");
            var formDate = new window.FormData();
            formDate.append('Organization', organizationId);
            formDate.append('Program', programId);
            formDate.append('Session', sessionId);
            formDate.append('Course', course);
            formDate.append('ExamType', examType);
            formDate.append('ExamCode', examCode);
            formDate.append('ExamId', $("#ExamId").val());
            formDate.append('FileField', $('#FileField')[0].files[0]);
            formDate.append('allAnswerSheet', allAnswerSheet);

            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Exam/MarksEntry/MarksEntryNewByXml",
                processData: false,
                contentType: false,
                //dataType: "json",
                //contentType: "application/json",
                data: formDate,
                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    console.log(response);
                   // alert(response.inValidRolls);
                   // { isSuccess: true, returnMessageSuccess: 0, returnMessageSuccessUpdate: 1, successPrnListUpdate: "30150300196", returnMessageFail: Array[3], inValidRolls: "24280600001, ", inValidRegNo: "", invalidExamCode: "", invalidSession: "10160200003, 10160200002, ", rmfMessase: "" }

                    if (response.isSuccess) {

                        if (response.returnMessageSuccess != "")
                        {
                            $.fn.customMessage({
                                displayMessage: "Total " + response.returnMessageSuccess + " Item Insert",
                                displayMessageType: "s"
                            });
                        }
                        if (response.returnMessageSuccessUpdate != "") {
                            $.fn.customMessage({
                                displayMessage: "Total " + response.returnMessageSuccessUpdate + " Item Update(" + response.successPrnListUpdate + ")",
                                displayMessageType: "w"
                            });
                        }
                        if (response.returnMessageFail.length >0) {
                            if (response.rmfMessase != "") {
                                $.fn.customMessage({
                                    displayMessage: response.rmfMessase,
                                    displayMessageType: "error"
                                });
                            } else {

                                $.fn.customMessage({
                                    displayMessage: response.invalidAllMsg,
                                    displayMessageType: "error"
                                });
                            }
                        }

                    } else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error"
                        });
                    }
                    $.unblockUI();
                    $("#ExamName").val("");
                    $("#Organization").val("0");
                    $("#FileField").val("");
                    window.setTimeout(function () { location.reload(); }, 5000);
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function() {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        } else {
            //alert("ok2");
            $("#uploadEmlForm").hide();
            $.blockUI({
                timeout: 0,
                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
            });
            $("#marksEntryForm").get(0).submit();
        }

    }

}

$(document).ready(function () {
    
    $("#marksEntryForm").submit(function () {
        return false;
    });
    
    $('body').keydown(function (e) {
        if (e.keyCode == 13) {
            responseReturn();
        }
    });
    
    $("#submitForm").click(function () {
        responseReturn();
    });
    $("#uploadEmlForm").click(function () {
        responseReturn();
    });
    $("#FileField").change(function () {
        checkFileField();
    });

    $("#Organization").change(function () {
        $("#Program option").remove();
        $('#Program').append("<option value=''>--Select Program--</option>");
        $("#Session option").remove();
        $('#Session').append("<option value=''>--Select Session--</option>");
        $("#Course option").remove();
        $('#Course').append("<option value=''>--Select Course--</option>");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");
        if ($(this).val() != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",

                cache: false,
                async: true,
                data: { "organizationIds": $(this).val(), isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnProgramList, function (i, v) {
                            $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function () {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
    });


    $("#Program").change(function () {
        $("#Session option").remove();
        $('#Session').append("<option value=''>--Select Session--</option>");
        $("#Course option").remove();
        $('#Course').append("<option value=''>--Select Course--</option>");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");
        var organizationId = $("#Organization").val();
        var programId = $("#Program").val();
        if (organizationId != "" && programId!="") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",

                cache: false,
                async: true,
                data: { organizationIds: organizationId, programIds: programId, isAuthorized: true },

                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnSessionList, function (i, v) {
                            $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }       
    });

    $("#Session").change(function () {
        $("#Course option").remove();
        $('#Course').append("<option value=''>--Select Course--</option>");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");

        var organizationId = $("#Organization").val();
        var programId = $("#Program").val();
        var sessionId = $("#Session").val();

        if (organizationId != "" && organizationId != null && programId != "" && programId != null && sessionId != "" && sessionId != null) {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
                cache: false,
                async: true,
                data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId },

                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {

                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnCourse, function (i, v) {
                            $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function() {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
        
    });
    $("#Course").change(function() {
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
    });
    


});