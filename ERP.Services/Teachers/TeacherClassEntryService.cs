﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherClassEntryService : IBaseService
    {
        #region Operational Function

        void Save(TeacherClassEntry teacherClassEntry);

        #endregion

        #region Single Instances Loading Function

        TeacherClassType GetTeacherClassType(long Id);

        #endregion

        #region List Loading Function

        IList<TeacherClassType> LoadTeacherClassType();
        IList<TeacherCampusRoutineDto> ListTeacherCampusRoutine(int start, int length, string orderBy, string orderDir, long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string tpin);
        List<TeacherClassPaymentReport> LoadClassPaymentReportList(List<UserMenu> userMenu, int start, string orderBy, string orderDir, TeacherClassPaymentFormViewModel teacherClassPayment);
        List<TeacherSeniorshipCalculationReport> LoadSeniorshipCalculationReportList(List<UserMenu> userMenu, int start, string orderBy, string orderDir, TeacherSeniorshipCalculationReportFormViewModel teacherSeniorshipCalculation);

        #endregion

        #region Others Function

        int CountTeacherCampusRoutine(long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string tpin);
        int GetClassPaymentReportListCount(List<UserMenu> userMenu, TeacherClassPaymentFormViewModel teacherClassPayment);
        int GetSeniorshipCalculationReportListCount(List<UserMenu> userMenu, TeacherSeniorshipCalculationReportFormViewModel teacherSeniorshipCalculation);

        #endregion

    }

    public class TeacherClassEntryService : BaseService, ITeacherClassEntryService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("TeacherService");

        #endregion

        #region Propertise & Object Initialization

        private readonly ITeacherClassTypeDao _teacherClassTypeDao;
        private readonly ITeacherClassEntryDao _teacherClassEntryDao;
        private readonly ITeacherClassEntryDetailsDao _teacherClassEntryDetailsDao;
        private readonly ICommonHelper _commonHelper;

        public TeacherClassEntryService(ISession session)
        {
            Session = session;
            _teacherClassTypeDao = new TeacherClassTypeDao() { Session = session };
            _teacherClassEntryDao = new TeacherClassEntryDao() { Session = session };
            _teacherClassEntryDetailsDao = new TeacherClassEntryDetailsDao() { Session = session };
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Function

        public void Save(TeacherClassEntry teacherClassEntry)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    IList<TeacherClassEntryDetails> teacherClassEntryDetails = teacherClassEntry.TeacherClassEntryDetails;
                    teacherClassEntry.TeacherClassEntryDetails = null;
                    //throw new DuplicateEntryException("This Lecture is already exist for subject : " + oldLectureSetting.CourseSubject.Subject.Name);                                
                    teacherClassEntry.SerialNumber = GetSerialNumber();
                    teacherClassEntry.Status = TeacherClassEntry.EntityStatus.Active;
                    _teacherClassEntryDao.Save(teacherClassEntry);
                    foreach (var teacherClassEntryDetail in teacherClassEntryDetails)
                    {
                        var t = new TeacherClassEntryDetails();
                        t.Teacher = teacherClassEntryDetail.Teacher;
                        t.Quantity = teacherClassEntryDetail.Quantity;
                        t.Duration = teacherClassEntryDetail.Duration;
                        t.Description = teacherClassEntryDetail.Description;
                        t.Amount = teacherClassEntryDetail.Amount;
                        t.TeacherClassEntry = teacherClassEntry;
                        t.Status = TeacherClassEntryDetails.EntityStatus.Active;
                        //t.PaymentStatus = 0;
                        t.PaymentStatus = teacherClassEntryDetail.PaymentStatus;
                        _teacherClassEntryDetailsDao.Save(t);
                    }
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public TeacherClassType GetTeacherClassType(long Id)
        {
            try
            {
                return _teacherClassTypeDao.LoadById(Id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<TeacherClassType> LoadTeacherClassType()
        {
            try
            {
                return _teacherClassTypeDao.LoadAll(TeacherClassType.EntityStatus.Active);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<TeacherCampusRoutineDto> ListTeacherCampusRoutine(int start, int length, string orderBy, string orderDir, long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string tpin)
        {
            try
            {
                return _teacherClassEntryDao.ListTeacherCampusRoutine(start, length, orderBy, orderDir, organizationId, programId, sessionId, branchIds, campusIds, heldDate, fullName, nickName, mobile, tpin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public List<TeacherClassPaymentReport> LoadClassPaymentReportList(List<UserMenu> userMenu, int start, string orderBy, string orderDir, TeacherClassPaymentFormViewModel teacherClassPayment)
        {
            try
            {
                List<long> givenOrganizationIdList = null;
                List<long> givenProgramIdList = null;
                List<long> givenBranchIdList = null;
                if (teacherClassPayment.OrganizationId != SelectionType.SelelectAll)
                {
                    givenOrganizationIdList = _commonHelper.ConvertIdToList(teacherClassPayment.OrganizationId);
                }
                if (teacherClassPayment.ProgramId != SelectionType.SelelectAll)
                {
                    givenProgramIdList = _commonHelper.ConvertIdToList(teacherClassPayment.ProgramId);
                }
                if (teacherClassPayment.BranchId != SelectionType.SelelectAll)
                {
                    givenBranchIdList = _commonHelper.ConvertIdToList(teacherClassPayment.BranchId);
                }

                if (userMenu == null)
                    throw new InvalidDataException("No User Menu Found.");

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, givenOrganizationIdList);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList, null, givenBranchIdList);
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu, authOrganizationIdList, authBranchIdList, givenProgramIdList);

                List<TeacherClassPaymentReport> teacherClassPaymentReportList = _teacherClassEntryDao.LoadClassPaymentReportList(authBranchIdList, authProgramIdList, _commonHelper.ConvertIdToList(teacherClassPayment.SessionId), _commonHelper.ConvertIdToList(teacherClassPayment.CampusId), teacherClassPayment.DateFrom, teacherClassPayment.DateTo, teacherClassPayment.ReportType, teacherClassPayment.ReportTypeGroupBy,  teacherClassPayment.PaymentStatus, teacherClassPayment.TpinList, start, orderBy, orderDir, teacherClassPayment.NoOfData);

                return teacherClassPaymentReportList;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<TeacherSeniorshipCalculationReport> LoadSeniorshipCalculationReportList(List<UserMenu> userMenu, int start, string orderBy, string orderDir,
            TeacherSeniorshipCalculationReportFormViewModel teacherSeniorshipCalculation)
        {
            List<long> givenOrganizationIdList = null;
            if (teacherSeniorshipCalculation.OrganizationId != SelectionType.SelelectAll)
            {
                givenOrganizationIdList = _commonHelper.ConvertIdToList(teacherSeniorshipCalculation.OrganizationId);
            }
            if (userMenu == null)
                throw new InvalidDataException("No User Menu Found.");
            List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, givenOrganizationIdList);
            List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList);
            List<TeacherSeniorshipCalculationReport> teacherClassPaymentReportList = _teacherClassEntryDao.LoadSeniorshipCalculationReportList(authOrganizationIdList,authBranchIdList, teacherSeniorshipCalculation.TillDate, teacherSeniorshipCalculation.TpinList, start, orderBy, orderDir, teacherSeniorshipCalculation.NoOfData);

            return teacherClassPaymentReportList;
        }

        #endregion

        #region Others Function

        public int? GetSerialNumber()
        {
            try
            {
                int? serial = 100001;
                var classEntry = _teacherClassEntryDao.GetTeacherClassEntry();
                if (classEntry != null)
                {
                    serial = classEntry.SerialNumber + 1;
                }
                return serial;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int CountTeacherCampusRoutine(long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string tpin)
        {
            try
            {
                return _teacherClassEntryDao.CountTeachercampusRoutine(organizationId, programId, sessionId, branchIds, campusIds, heldDate, fullName, nickName, mobile, tpin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public int GetClassPaymentReportListCount(List<UserMenu> userMenu, TeacherClassPaymentFormViewModel teacherClassPayment)
        {
            try
            {
                List<long> givenOrganizationIdList = null;
                List<long> givenProgramIdList = null;
                List<long> givenBranchIdList = null;
                if (teacherClassPayment.OrganizationId != SelectionType.SelelectAll)
                {
                    givenOrganizationIdList = _commonHelper.ConvertIdToList(teacherClassPayment.OrganizationId);
                }
                if (teacherClassPayment.ProgramId != SelectionType.SelelectAll)
                {
                    givenProgramIdList = _commonHelper.ConvertIdToList(teacherClassPayment.ProgramId);
                }
                if (teacherClassPayment.BranchId != SelectionType.SelelectAll)
                {
                    givenBranchIdList = _commonHelper.ConvertIdToList(teacherClassPayment.BranchId);
                }

                if (userMenu == null)
                    throw new InvalidDataException("No User Menu Found.");

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, givenOrganizationIdList);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList, null, givenBranchIdList);
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu, authOrganizationIdList, authBranchIdList, givenProgramIdList);

                int teacherClassPaymentReportListCount = _teacherClassEntryDao.GetClassPaymentReportListCount(authBranchIdList, authProgramIdList, _commonHelper.ConvertIdToList(teacherClassPayment.SessionId), _commonHelper.ConvertIdToList(teacherClassPayment.CampusId), teacherClassPayment.DateFrom, teacherClassPayment.DateTo, teacherClassPayment.ReportType, teacherClassPayment.ReportTypeGroupBy, teacherClassPayment.PaymentStatus, teacherClassPayment.TpinList);

                return teacherClassPaymentReportListCount;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetSeniorshipCalculationReportListCount(List<UserMenu> userMenu,
            TeacherSeniorshipCalculationReportFormViewModel teacherSeniorshipCalculation)
        {
            List<long> givenOrganizationIdList = null;
            if (teacherSeniorshipCalculation.OrganizationId != SelectionType.SelelectAll)
            {
                givenOrganizationIdList = _commonHelper.ConvertIdToList(teacherSeniorshipCalculation.OrganizationId);
            }

            if (userMenu == null)
                throw new InvalidDataException("No User Menu Found.");

            List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, givenOrganizationIdList);
            List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList);
            int teacherClassPaymentReportListCount = _teacherClassEntryDao.GetSeniorshipCalculationReportListCount(authOrganizationIdList,authBranchIdList, teacherSeniorshipCalculation.TillDate, teacherSeniorshipCalculation.TpinList);
            return teacherClassPaymentReportListCount;
        }

        #endregion
    }
}
