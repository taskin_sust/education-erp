using UdvashERP.BusinessRules;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{    
    public partial class AttendanceDeviceMap : BaseClassMap<AttendanceDevice,long> {
        
        public AttendanceDeviceMap() {
			Table("HR_AttendanceDevice");
			LazyLoad();
			References(x => x.Campus).Column("CampusId");
			References(x => x.AttendanceSynchronizer).Column("AttendanceSynchronizerId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
            Map(x => x.DeviceModelNo).Column("DeviceModelNo");
			Map(x => x.CommunicationType).Column("CommunicationType");
			Map(x => x.IpAddress).Column("IPAddress");
			Map(x => x.Port).Column("Port");
            Map(x => x.MachineNo).Column("MachineNo");
            Map(x => x.CommunicationKey).Column("CommunicationKey");
            Map(x => x.IsReset).Column("IsReset");
            Map(x => x.LastMemberInfoUpdateTime).Column("LastMemberInfoUpdateTime");
            Map(x => x.LastRequestIp).Column("LastRequestIp");
            Map(x => x.DisableCard).Column("DisableCard");

            References(x => x.AttendanceDeviceType).Column("AttendanceDeviceTypeId");
			HasMany(x => x.AttendanceDeviceActivationLog).KeyColumn("AttendanceDeviceId");
            HasMany(x => x.AttendanceDeviceRawDatas).KeyColumn("AttendanceDeviceId").Where(ss => ss.Status != AttendanceDevice.EntityStatus.Delete).LazyLoad(); 
        }
    }
}
