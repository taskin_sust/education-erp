﻿namespace DeviceSynchronizer
{
    partial class Synchronizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Synchronizer));
            this.label18 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Main_Synchronizer = new System.Windows.Forms.TabPage();
            this.stopBtn = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.startBtn = new System.Windows.Forms.Button();
            this.currentStatedataGridView = new System.Windows.Forms.DataGridView();
            this.colTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stateinformation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.axCZKEM1 = new Axzkemkeeper.AxCZKEM();
            this.Attendance_Device_Crud = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataType = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lvLogs = new System.Windows.Forms.ListView();
            this.lvLogsch1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClearGLog = new System.Windows.Forms.Button();
            this.btnGetGeneralExtLogData = new System.Windows.Forms.Button();
            this.btnGetGeneralLogDataStr = new System.Windows.Forms.Button();
            this.btnGetDeviceStatus = new System.Windows.Forms.Button();
            this.btnGetGeneralLogData = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Connect_t2_Btn = new System.Windows.Forms.Button();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Attendance_Device_Retrive = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.deleteUser_Btn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.delPin = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lvCard = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnGetStrCardNumber = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.chbEnabled = new System.Windows.Forms.CheckBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnSetStrCardNumber = new System.Windows.Forms.Button();
            this.label89 = new System.Windows.Forms.Label();
            this.cbPrivilege = new System.Windows.Forms.ComboBox();
            this.txtCardnumber = new System.Windows.Forms.TextBox();
            this.Privilege = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPass2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Connect_t3_btn = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.axCZKEM3 = new Axzkemkeeper.AxCZKEM();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.tabControl1.SuspendLayout();
            this.Main_Synchronizer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.currentStatedataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axCZKEM1)).BeginInit();
            this.Attendance_Device_Crud.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Attendance_Device_Retrive.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axCZKEM3)).BeginInit();
            this.SuspendLayout();
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(445, 559);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(0, 13);
            this.label18.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.Main_Synchronizer);
            this.tabControl1.Controls.Add(this.Attendance_Device_Crud);
            this.tabControl1.Controls.Add(this.Attendance_Device_Retrive);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(945, 736);
            this.tabControl1.TabIndex = 0;
            // 
            // Main_Synchronizer
            // 
            this.Main_Synchronizer.BackColor = System.Drawing.Color.PowderBlue;
            this.Main_Synchronizer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Main_Synchronizer.Controls.Add(this.stopBtn);
            this.Main_Synchronizer.Controls.Add(this.btnExit);
            this.Main_Synchronizer.Controls.Add(this.startBtn);
            this.Main_Synchronizer.Controls.Add(this.currentStatedataGridView);
            this.Main_Synchronizer.Controls.Add(this.axCZKEM1);
            this.Main_Synchronizer.Location = new System.Drawing.Point(4, 22);
            this.Main_Synchronizer.Name = "Main_Synchronizer";
            this.Main_Synchronizer.Padding = new System.Windows.Forms.Padding(3);
            this.Main_Synchronizer.Size = new System.Drawing.Size(937, 710);
            this.Main_Synchronizer.TabIndex = 0;
            this.Main_Synchronizer.Text = "Main Synchronizer";
            // 
            // stopBtn
            // 
            this.stopBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.stopBtn.BackColor = System.Drawing.Color.Transparent;
            this.stopBtn.Enabled = false;
            this.stopBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopBtn.Location = new System.Drawing.Point(739, 656);
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(192, 48);
            this.stopBtn.TabIndex = 1;
            this.stopBtn.Text = "Stop";
            this.stopBtn.UseVisualStyleBackColor = false;
            this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(426, 656);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(109, 48);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit Application";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // startBtn
            // 
            this.startBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.startBtn.BackColor = System.Drawing.Color.Transparent;
            this.startBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startBtn.Location = new System.Drawing.Point(541, 656);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(192, 48);
            this.startBtn.TabIndex = 0;
            this.startBtn.Text = "Start";
            this.startBtn.UseVisualStyleBackColor = false;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // currentStatedataGridView
            // 
            this.currentStatedataGridView.AccessibleDescription = "service surrent state information";
            this.currentStatedataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.currentStatedataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.currentStatedataGridView.BackgroundColor = System.Drawing.Color.PowderBlue;
            this.currentStatedataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.currentStatedataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.currentStatedataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTime,
            this.stateinformation});
            this.currentStatedataGridView.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.currentStatedataGridView.Location = new System.Drawing.Point(6, 6);
            this.currentStatedataGridView.Name = "currentStatedataGridView";
            this.currentStatedataGridView.RowHeadersWidth = 20;
            this.currentStatedataGridView.Size = new System.Drawing.Size(925, 644);
            this.currentStatedataGridView.TabIndex = 5;
            // 
            // colTime
            // 
            this.colTime.HeaderText = "Time";
            this.colTime.MinimumWidth = 140;
            this.colTime.Name = "colTime";
            this.colTime.ReadOnly = true;
            this.colTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.colTime.Width = 140;
            // 
            // stateinformation
            // 
            this.stateinformation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.stateinformation.HeaderText = "Information Log";
            this.stateinformation.MinimumWidth = 600;
            this.stateinformation.Name = "stateinformation";
            this.stateinformation.ReadOnly = true;
            this.stateinformation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // axCZKEM1
            // 
            this.axCZKEM1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.axCZKEM1.Enabled = true;
            this.axCZKEM1.Location = new System.Drawing.Point(6, 656);
            this.axCZKEM1.Name = "axCZKEM1";
            this.axCZKEM1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axCZKEM1.OcxState")));
            this.axCZKEM1.Size = new System.Drawing.Size(192, 192);
            this.axCZKEM1.TabIndex = 3;
            // 
            // Attendance_Device_Crud
            // 
            this.Attendance_Device_Crud.BackColor = System.Drawing.Color.PowderBlue;
            this.Attendance_Device_Crud.Controls.Add(this.groupBox2);
            this.Attendance_Device_Crud.Controls.Add(this.groupBox1);
            this.Attendance_Device_Crud.Location = new System.Drawing.Point(4, 22);
            this.Attendance_Device_Crud.Name = "Attendance_Device_Crud";
            this.Attendance_Device_Crud.Padding = new System.Windows.Forms.Padding(3);
            this.Attendance_Device_Crud.Size = new System.Drawing.Size(937, 710);
            this.Attendance_Device_Crud.TabIndex = 1;
            this.Attendance_Device_Crud.Text = "Crud ";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.LightBlue;
            this.groupBox2.Controls.Add(this.dataType);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.lvLogs);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.btnClearGLog);
            this.groupBox2.Controls.Add(this.btnGetGeneralExtLogData);
            this.groupBox2.Controls.Add(this.btnGetGeneralLogDataStr);
            this.groupBox2.Controls.Add(this.btnGetDeviceStatus);
            this.groupBox2.Controls.Add(this.btnGetGeneralLogData);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.ForeColor = System.Drawing.Color.DarkBlue;
            this.groupBox2.Location = new System.Drawing.Point(243, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(592, 438);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Download or Clear Attendance Records";
            // 
            // dataType
            // 
            this.dataType.FormattingEnabled = true;
            this.dataType.Items.AddRange(new object[] {
            "Attendance record",
            "Fingerprint template data",
            "None",
            "Operation record",
            "User information"});
            this.dataType.Location = new System.Drawing.Point(13, 400);
            this.dataType.Name = "dataType";
            this.dataType.Size = new System.Drawing.Size(136, 21);
            this.dataType.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(296, 403);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Clear all Data.";
            // 
            // lvLogs
            // 
            this.lvLogs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvLogsch1,
            this.lvLogsch2,
            this.lvLogsch3,
            this.lvLogsch4,
            this.lvLogsch5,
            this.lvLogsch6,
            this.lvLogsch7});
            this.lvLogs.GridLines = true;
            this.lvLogs.Location = new System.Drawing.Point(13, 19);
            this.lvLogs.Name = "lvLogs";
            this.lvLogs.Size = new System.Drawing.Size(561, 234);
            this.lvLogs.TabIndex = 0;
            this.lvLogs.UseCompatibleStateImageBehavior = false;
            this.lvLogs.View = System.Windows.Forms.View.Details;
            // 
            // lvLogsch1
            // 
            this.lvLogsch1.Text = "Count";
            this.lvLogsch1.Width = 45;
            // 
            // lvLogsch2
            // 
            this.lvLogsch2.Text = "EnrollNumber";
            // 
            // lvLogsch3
            // 
            this.lvLogsch3.Text = "VerifyMode";
            this.lvLogsch3.Width = 76;
            // 
            // lvLogsch4
            // 
            this.lvLogsch4.Text = "InOutMode";
            this.lvLogsch4.Width = 71;
            // 
            // lvLogsch5
            // 
            this.lvLogsch5.Text = "Date";
            // 
            // lvLogsch6
            // 
            this.lvLogsch6.Text = "WorkCode";
            this.lvLogsch6.Width = 67;
            // 
            // lvLogsch7
            // 
            this.lvLogsch7.Text = "Reserved";
            this.lvLogsch7.Width = 81;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(154, 398);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "ClearData";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(154, 368);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Clear all attendance logs.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(154, 342);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Get the total of logs.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(155, 316);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Get extended attendance logs.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(155, 263);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Get attendance logs.";
            // 
            // btnClearGLog
            // 
            this.btnClearGLog.Location = new System.Drawing.Point(12, 362);
            this.btnClearGLog.Name = "btnClearGLog";
            this.btnClearGLog.Size = new System.Drawing.Size(136, 23);
            this.btnClearGLog.TabIndex = 6;
            this.btnClearGLog.Text = "ClearGLog";
            this.btnClearGLog.UseVisualStyleBackColor = true;
            this.btnClearGLog.Click += new System.EventHandler(this.btnClearGLog_Click);
            // 
            // btnGetGeneralExtLogData
            // 
            this.btnGetGeneralExtLogData.Location = new System.Drawing.Point(12, 310);
            this.btnGetGeneralExtLogData.Name = "btnGetGeneralExtLogData";
            this.btnGetGeneralExtLogData.Size = new System.Drawing.Size(137, 23);
            this.btnGetGeneralExtLogData.TabIndex = 5;
            this.btnGetGeneralExtLogData.Text = "GetGeneralExtLogData";
            this.btnGetGeneralExtLogData.UseVisualStyleBackColor = true;
            this.btnGetGeneralExtLogData.Click += new System.EventHandler(this.btnGetGeneralExtLogData_Click);
            // 
            // btnGetGeneralLogDataStr
            // 
            this.btnGetGeneralLogDataStr.Location = new System.Drawing.Point(12, 284);
            this.btnGetGeneralLogDataStr.Name = "btnGetGeneralLogDataStr";
            this.btnGetGeneralLogDataStr.Size = new System.Drawing.Size(137, 23);
            this.btnGetGeneralLogDataStr.TabIndex = 4;
            this.btnGetGeneralLogDataStr.Text = "GetGeneralLogDataStr";
            this.btnGetGeneralLogDataStr.UseVisualStyleBackColor = true;
            this.btnGetGeneralLogDataStr.Click += new System.EventHandler(this.btnGetGeneralLogDataStr_Click);
            // 
            // btnGetDeviceStatus
            // 
            this.btnGetDeviceStatus.Location = new System.Drawing.Point(12, 336);
            this.btnGetDeviceStatus.Name = "btnGetDeviceStatus";
            this.btnGetDeviceStatus.Size = new System.Drawing.Size(136, 23);
            this.btnGetDeviceStatus.TabIndex = 3;
            this.btnGetDeviceStatus.Text = "GetRecordCount";
            this.btnGetDeviceStatus.UseVisualStyleBackColor = true;
            this.btnGetDeviceStatus.Click += new System.EventHandler(this.btnGetDeviceStatus_Click);
            // 
            // btnGetGeneralLogData
            // 
            this.btnGetGeneralLogData.Location = new System.Drawing.Point(12, 258);
            this.btnGetGeneralLogData.Name = "btnGetGeneralLogData";
            this.btnGetGeneralLogData.Size = new System.Drawing.Size(137, 23);
            this.btnGetGeneralLogData.TabIndex = 1;
            this.btnGetGeneralLogData.Text = "GetGeneralLogData";
            this.btnGetGeneralLogData.UseVisualStyleBackColor = true;
            this.btnGetGeneralLogData.Click += new System.EventHandler(this.btnGetGeneralLogData_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(154, 290);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Get attendance logs by strings.";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LightBlue;
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtPass);
            this.groupBox1.Controls.Add(this.lblState);
            this.groupBox1.Controls.Add(this.txtIP);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Connect_t2_Btn);
            this.groupBox1.Controls.Add(this.txtPort);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(220, 438);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Communication With Device";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 98);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "Pass";
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(61, 91);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(137, 20);
            this.txtPass.TabIndex = 16;
            this.txtPass.Text = "419555";
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.ForeColor = System.Drawing.Color.Crimson;
            this.lblState.Location = new System.Drawing.Point(58, 177);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(138, 13);
            this.lblState.TabIndex = 15;
            this.lblState.Text = "Current State:Disconnected";
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(61, 39);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(137, 20);
            this.txtIP.TabIndex = 11;
            this.txtIP.Text = "192.168.0.28";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Port";
            // 
            // Connect_t2_Btn
            // 
            this.Connect_t2_Btn.Location = new System.Drawing.Point(103, 117);
            this.Connect_t2_Btn.Name = "Connect_t2_Btn";
            this.Connect_t2_Btn.Size = new System.Drawing.Size(95, 35);
            this.Connect_t2_Btn.TabIndex = 10;
            this.Connect_t2_Btn.Text = "Connect";
            this.Connect_t2_Btn.UseVisualStyleBackColor = true;
            this.Connect_t2_Btn.Click += new System.EventHandler(this.Connect_t2_Btn_Click);
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(61, 65);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(137, 20);
            this.txtPort.TabIndex = 12;
            this.txtPort.Text = "4370";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "IP";
            // 
            // Attendance_Device_Retrive
            // 
            this.Attendance_Device_Retrive.BackColor = System.Drawing.Color.PowderBlue;
            this.Attendance_Device_Retrive.Controls.Add(this.groupBox8);
            this.Attendance_Device_Retrive.Controls.Add(this.groupBox4);
            this.Attendance_Device_Retrive.Controls.Add(this.groupBox10);
            this.Attendance_Device_Retrive.Controls.Add(this.axCZKEM3);
            this.Attendance_Device_Retrive.Location = new System.Drawing.Point(4, 22);
            this.Attendance_Device_Retrive.Name = "Attendance_Device_Retrive";
            this.Attendance_Device_Retrive.Padding = new System.Windows.Forms.Padding(3);
            this.Attendance_Device_Retrive.Size = new System.Drawing.Size(937, 710);
            this.Attendance_Device_Retrive.TabIndex = 2;
            this.Attendance_Device_Retrive.Text = "Retrive";
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.LightBlue;
            this.groupBox8.Controls.Add(this.deleteUser_Btn);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Controls.Add(this.delPin);
            this.groupBox8.Location = new System.Drawing.Point(6, 213);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(343, 111);
            this.groupBox8.TabIndex = 54;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Delete Card Number";
            // 
            // deleteUser_Btn
            // 
            this.deleteUser_Btn.Location = new System.Drawing.Point(58, 69);
            this.deleteUser_Btn.Name = "deleteUser_Btn";
            this.deleteUser_Btn.Size = new System.Drawing.Size(117, 23);
            this.deleteUser_Btn.TabIndex = 65;
            this.deleteUser_Btn.Text = "Delete User Info";
            this.deleteUser_Btn.UseVisualStyleBackColor = true;
            this.deleteUser_Btn.Click += new System.EventHandler(this.deleteUser_Btn_Click);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(14, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 18);
            this.label6.TabIndex = 64;
            this.label6.Text = "User ID";
            // 
            // delPin
            // 
            this.delPin.Location = new System.Drawing.Point(82, 38);
            this.delPin.Name = "delPin";
            this.delPin.Size = new System.Drawing.Size(69, 20);
            this.delPin.TabIndex = 57;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.LightBlue;
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Location = new System.Drawing.Point(365, 15);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(499, 455);
            this.groupBox4.TabIndex = 52;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Download or Upload Card Number";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Crimson;
            this.label13.Location = new System.Drawing.Point(12, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(305, 13);
            this.label13.TabIndex = 46;
            this.label13.Text = "Please make sure your device has an optional ID card module. ";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lvCard);
            this.groupBox5.Controls.Add(this.btnGetStrCardNumber);
            this.groupBox5.Location = new System.Drawing.Point(6, 52);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(479, 257);
            this.groupBox5.TabIndex = 43;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Download the Card Number(A property of user information)";
            // 
            // lvCard
            // 
            this.lvCard.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lvCard.GridLines = true;
            this.lvCard.Location = new System.Drawing.Point(6, 16);
            this.lvCard.Name = "lvCard";
            this.lvCard.Size = new System.Drawing.Size(467, 194);
            this.lvCard.TabIndex = 45;
            this.lvCard.UseCompatibleStateImageBehavior = false;
            this.lvCard.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "UserID";
            this.columnHeader1.Width = 54;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 41;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Cardnumber";
            this.columnHeader3.Width = 78;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Privilege";
            this.columnHeader4.Width = 92;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Password";
            this.columnHeader5.Width = 76;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Enabled";
            this.columnHeader6.Width = 84;
            // 
            // btnGetStrCardNumber
            // 
            this.btnGetStrCardNumber.Location = new System.Drawing.Point(183, 228);
            this.btnGetStrCardNumber.Name = "btnGetStrCardNumber";
            this.btnGetStrCardNumber.Size = new System.Drawing.Size(117, 23);
            this.btnGetStrCardNumber.TabIndex = 1;
            this.btnGetStrCardNumber.Text = "GetStrCardNumber";
            this.btnGetStrCardNumber.UseVisualStyleBackColor = true;
            this.btnGetStrCardNumber.Click += new System.EventHandler(this.btnGetStrCardNumber_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtUserID);
            this.groupBox6.Controls.Add(this.txtName);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.chbEnabled);
            this.groupBox6.Controls.Add(this.txtPassword);
            this.groupBox6.Controls.Add(this.btnSetStrCardNumber);
            this.groupBox6.Controls.Add(this.label89);
            this.groupBox6.Controls.Add(this.cbPrivilege);
            this.groupBox6.Controls.Add(this.txtCardnumber);
            this.groupBox6.Controls.Add(this.Privilege);
            this.groupBox6.Controls.Add(this.label55);
            this.groupBox6.Controls.Add(this.label90);
            this.groupBox6.Location = new System.Drawing.Point(12, 315);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(479, 111);
            this.groupBox6.TabIndex = 44;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Upload the Card Number(part of users information)";
            // 
            // txtUserID
            // 
            this.txtUserID.Location = new System.Drawing.Point(90, 15);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(69, 20);
            this.txtUserID.TabIndex = 56;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(238, 15);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(76, 20);
            this.txtName.TabIndex = 57;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(30, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 18);
            this.label16.TabIndex = 63;
            this.label16.Text = "User ID";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(174, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 17);
            this.label15.TabIndex = 64;
            this.label15.Text = "Name";
            // 
            // chbEnabled
            // 
            this.chbEnabled.AutoSize = true;
            this.chbEnabled.Location = new System.Drawing.Point(383, 50);
            this.chbEnabled.Name = "chbEnabled";
            this.chbEnabled.Size = new System.Drawing.Size(15, 14);
            this.chbEnabled.TabIndex = 69;
            this.chbEnabled.UseVisualStyleBackColor = true;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(383, 15);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(67, 20);
            this.txtPassword.TabIndex = 58;
            // 
            // btnSetStrCardNumber
            // 
            this.btnSetStrCardNumber.Location = new System.Drawing.Point(181, 80);
            this.btnSetStrCardNumber.Name = "btnSetStrCardNumber";
            this.btnSetStrCardNumber.Size = new System.Drawing.Size(117, 23);
            this.btnSetStrCardNumber.TabIndex = 0;
            this.btnSetStrCardNumber.Text = "SetStrCardNumber";
            this.btnSetStrCardNumber.UseVisualStyleBackColor = true;
            this.btnSetStrCardNumber.Click += new System.EventHandler(this.btnSetStrCardNumber_Click);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(328, 51);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(52, 13);
            this.label89.TabIndex = 67;
            this.label89.Text = "Enabled  ";
            // 
            // cbPrivilege
            // 
            this.cbPrivilege.FormattingEnabled = true;
            this.cbPrivilege.Location = new System.Drawing.Point(90, 47);
            this.cbPrivilege.Name = "cbPrivilege";
            this.cbPrivilege.Size = new System.Drawing.Size(69, 21);
            this.cbPrivilege.TabIndex = 59;
            // 
            // txtCardnumber
            // 
            this.txtCardnumber.Location = new System.Drawing.Point(238, 47);
            this.txtCardnumber.Name = "txtCardnumber";
            this.txtCardnumber.Size = new System.Drawing.Size(76, 20);
            this.txtCardnumber.TabIndex = 61;
            // 
            // Privilege
            // 
            this.Privilege.Location = new System.Drawing.Point(30, 50);
            this.Privilege.Name = "Privilege";
            this.Privilege.Size = new System.Drawing.Size(61, 19);
            this.Privilege.TabIndex = 65;
            this.Privilege.Text = "Privilege";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(174, 51);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(66, 13);
            this.label55.TabIndex = 66;
            this.label55.Text = "CardNumber";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(327, 20);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(53, 13);
            this.label90.TabIndex = 68;
            this.label90.Text = "Password";
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.LightBlue;
            this.groupBox10.Controls.Add(this.label17);
            this.groupBox10.Controls.Add(this.txtPass2);
            this.groupBox10.Controls.Add(this.label7);
            this.groupBox10.Controls.Add(this.textBox1);
            this.groupBox10.Controls.Add(this.label11);
            this.groupBox10.Controls.Add(this.Connect_t3_btn);
            this.groupBox10.Controls.Add(this.textBox2);
            this.groupBox10.Controls.Add(this.label12);
            this.groupBox10.Location = new System.Drawing.Point(6, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(343, 191);
            this.groupBox10.TabIndex = 48;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Communication With Device";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 95);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 13);
            this.label17.TabIndex = 19;
            this.label17.Text = "Pass";
            // 
            // txtPass2
            // 
            this.txtPass2.Location = new System.Drawing.Point(55, 88);
            this.txtPass2.Name = "txtPass2";
            this.txtPass2.Size = new System.Drawing.Size(137, 20);
            this.txtPass2.TabIndex = 18;
            this.txtPass2.Text = "419555";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Crimson;
            this.label7.Location = new System.Drawing.Point(42, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Current State:Disconnected";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(55, 36);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(137, 20);
            this.textBox1.TabIndex = 11;
            this.textBox1.Text = "192.168.0.28";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 66);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Port";
            // 
            // Connect_t3_btn
            // 
            this.Connect_t3_btn.Cursor = System.Windows.Forms.Cursors.Default;
            this.Connect_t3_btn.Location = new System.Drawing.Point(97, 126);
            this.Connect_t3_btn.Name = "Connect_t3_btn";
            this.Connect_t3_btn.Size = new System.Drawing.Size(95, 30);
            this.Connect_t3_btn.TabIndex = 10;
            this.Connect_t3_btn.Text = "Connect";
            this.Connect_t3_btn.UseVisualStyleBackColor = true;
            this.Connect_t3_btn.Click += new System.EventHandler(this.Connect_t3_btn_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(55, 62);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(137, 20);
            this.textBox2.TabIndex = 12;
            this.textBox2.Text = "4370";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(14, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "IP";
            // 
            // axCZKEM3
            // 
            this.axCZKEM3.Enabled = true;
            this.axCZKEM3.Location = new System.Drawing.Point(6, 330);
            this.axCZKEM3.Name = "axCZKEM3";
            this.axCZKEM3.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axCZKEM3.OcxState")));
            this.axCZKEM3.Size = new System.Drawing.Size(192, 192);
            this.axCZKEM3.TabIndex = 51;
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // Synchronizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 760);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Synchronizer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Synchronizer_FormClosing);
            this.Load += new System.EventHandler(this.Synchronizer_Load);
            this.Resize += new System.EventHandler(this.Synchronizer_Resize);
            this.tabControl1.ResumeLayout(false);
            this.Main_Synchronizer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.currentStatedataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axCZKEM1)).EndInit();
            this.Attendance_Device_Crud.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Attendance_Device_Retrive.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axCZKEM3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Axzkemkeeper.AxCZKEM axCZKEM2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Main_Synchronizer;
        private System.Windows.Forms.Button stopBtn;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.DataGridView currentStatedataGridView;
        private Axzkemkeeper.AxCZKEM axCZKEM1;
        private System.Windows.Forms.TabPage Attendance_Device_Crud;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox dataType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListView lvLogs;
        private System.Windows.Forms.ColumnHeader lvLogsch1;
        private System.Windows.Forms.ColumnHeader lvLogsch2;
        private System.Windows.Forms.ColumnHeader lvLogsch3;
        private System.Windows.Forms.ColumnHeader lvLogsch4;
        private System.Windows.Forms.ColumnHeader lvLogsch5;
        private System.Windows.Forms.ColumnHeader lvLogsch6;
        private System.Windows.Forms.ColumnHeader lvLogsch7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnClearGLog;
        private System.Windows.Forms.Button btnGetGeneralExtLogData;
        private System.Windows.Forms.Button btnGetGeneralLogDataStr;
        private System.Windows.Forms.Button btnGetDeviceStatus;
        private System.Windows.Forms.Button btnGetGeneralLogData;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Connect_t2_Btn;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage Attendance_Device_Retrive;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button deleteUser_Btn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox delPin;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ListView lvCard;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button btnGetStrCardNumber;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox chbEnabled;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnSetStrCardNumber;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.ComboBox cbPrivilege;
        private System.Windows.Forms.TextBox txtCardnumber;
        private System.Windows.Forms.Label Privilege;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPass2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button Connect_t3_btn;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label12;
        private Axzkemkeeper.AxCZKEM axCZKEM3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn stateinformation;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Button btnExit;
    }
}