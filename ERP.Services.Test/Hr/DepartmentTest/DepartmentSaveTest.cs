﻿using System;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.DepartmentTest
{
    public interface IDepartmentSaveTest
    {
        void Save_Should_Return_Null_Exception();
        void Save_Should_Check_Property();
        void Save_Should_Check_Duplicate();
        void Should_Save_Successfully();

    }

    [Trait("Area", "Hr")]
    [Trait("Service", "Department")]
    public class DepartmentSaveTest : DepartmentBaseTest, IDepartmentSaveTest
    {
         
        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _departmentService.Save(null));
        }

        [Fact]
        public void Save_Should_Check_Property()
        {
            var dFactory1 = _departmentFactory.Create().WithOrganization().WithNameAndRank("", 1);
            var exception = Assert.Throws<ServiceException>(() => _departmentService.Save(dFactory1.Object));
            Assert.Contains("empty", exception.Message);
        }

        [Fact]
        public void Save_Should_Check_Duplicate()
        {
            var dFactory1 = _departmentFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            var dFactory2 = _departmentFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            _departmentService.Save(dFactory1.Object);
            Assert.Throws<DuplicateEntryException>(() => _departmentService.Save(dFactory2.Object));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var dFactory = _departmentFactory.Create().WithOrganization();
            _departmentService.Save(dFactory.Object);
            Assert.NotNull(dFactory.Object);
            Assert.NotEqual(dFactory.Object.Id, 0);
        }

        //TODO: Check failed test for department save without organization.
        //TODO: Write failed test for same organization and same department name.

    }
}
