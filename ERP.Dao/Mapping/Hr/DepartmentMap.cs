using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{
    public partial class DepartmentMap : BaseClassMap<Department,long> {
        
        public DepartmentMap() {
			Table("HR_Department");
			LazyLoad();
			References(x => x.Organization).Column("OrganizationId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			HasMany(x => x.EmploymentHistory).KeyColumn("DepartmentId");
			HasMany(x => x.EmploymentHistoryLog).KeyColumn("DepartmentId");
			HasMany(x => x.MentorHistory).KeyColumn("MentorDepartmentId");
			HasMany(x => x.MentorHistoryLog).KeyColumn("MentorDepartmentId");
            HasMany(x => x.FestivalBonusSheetsSalary).KeyColumn("SalaryDepartmentId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.FestivalBonusSheetsJob).KeyColumn("JobDepartmentId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
