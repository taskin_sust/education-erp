﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common.EntitySql;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Dto.Exam;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Exam;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Teachers;
using InvalidDataException = System.IO.InvalidDataException;

namespace UdvashERP.Api.Controllers
{
    public class ExamQuestionSetPrintController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamAPI");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly IExamsService _examsService;
        private readonly IExamMcqQuestionSetService _examMcqQuestionSetService;
        private readonly IExamMcqQuestionSetPrintService _examMcqQuestionSetPrintService;
        private readonly ICommonHelper _commonHelper;

        public ExamQuestionSetPrintController()
        {
            try
            {
                ISession session = NHibernateSessionFactory.OpenSession();
                _organizationService = new OrganizationService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _courseService = new CourseService(session);
                _examsService = new ExamsService(session);
                _examMcqQuestionSetService = new ExamMcqQuestionSetService(session);
                _examMcqQuestionSetPrintService = new ExamMcqQuestionSetPrintService(session);

                _commonHelper = new CommonHelper();
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }
        #endregion

        #region Private functions

        private int Authenticate(string key)
        {
            int error = 0;
            var apiKey = ConfigurationManager.AppSettings["ApiKey"];

            if (key != apiKey)
            {
                error = 1;
            }
            return error;
        }

        #endregion

        #region Select list 

        [HttpGet]
        public JsonResult LoadOrganization(string key)
        {
            bool successState = false;
            string message = "";
            List<ExamQuestionSetPrintOrganizationApiObj> organizationList = new List<ExamQuestionSetPrintOrganizationApiObj>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                List<Organization> orgList = _organizationService.LoadOrganization().ToList();
                if (orgList.Any())
                {
                    organizationList =
                        orgList.Select(
                            x =>
                                new ExamQuestionSetPrintOrganizationApiObj
                                {
                                    Id = x.Id,
                                    ShortName = x.ShortName,
                                    FullName = x.Name
                                }).ToList();
                }
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            //dynamic data;
            //if (successState == false)
            //    data = message;
            //else
            //{
            //    data = organizationList;
            //}
            //return Json(new JsonResponse(successState, data), JsonRequestBehavior.AllowGet);
            return Json(new { IsSuccess = successState, Message = message, Data = organizationList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult LoadProgram(string key, string organizationId)
        {
            bool successState = false;
            string message = "";
            List<ExamQuestionSetPrintProgramApiObj> programList = new List<ExamQuestionSetPrintProgramApiObj>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                long orgId = 0;
                if(String.IsNullOrEmpty(organizationId) || !Int64.TryParse(organizationId, out orgId) || orgId == 0)
                {
                    throw new InvalidDataException("Invalid Organization");
                }
                List<Program> proList = _programService.LoadProgram(_commonHelper.ConvertIdToList(orgId)).ToList();
                if (proList.Any())
                {
                    programList = proList.Select(x => new ExamQuestionSetPrintProgramApiObj { Id = x.Id, ShortName = x.ShortName, FullName = x.Name }).ToList();
                }
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message, Data = programList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult LoadSession(string key, string organizationId, string programId)
        {
            bool successState = false;
            string message = "";
            List<ExamQuestionSetPrintSessionApiObj> sessionList = new List<ExamQuestionSetPrintSessionApiObj>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                long orgId = 0;
                if (String.IsNullOrEmpty(organizationId) || !Int64.TryParse(organizationId, out orgId) || orgId == 0)
                {
                    throw new InvalidDataException("Invalid Organization");
                }
                long proId = 0;
                if (String.IsNullOrEmpty(programId) || !Int64.TryParse(programId, out proId) || proId == 0)
                {
                    throw new InvalidDataException("Invalid Program");
                }
                List<Session> sesList = _sessionService.LoadSession(_commonHelper.ConvertIdToList(orgId), _commonHelper.ConvertIdToList(proId)).ToList();
                if (sesList.Any())
                {
                    sessionList = sesList.Select(x => new ExamQuestionSetPrintSessionApiObj { Id = x.Id, Name = x.Name }).ToList();
                }
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message, Data = sessionList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult LoadCourse(string key, string organizationId, string programId, string sessionId)
        {
            bool successState = false;
            string message = "";
            List<ExamQuestionSetPrintCourseApiObj> courseList = new List<ExamQuestionSetPrintCourseApiObj>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                long orgId = 0;
                if (String.IsNullOrEmpty(organizationId) || !Int64.TryParse(organizationId, out orgId) || orgId == 0)
                {
                    throw new InvalidDataException("Invalid Organization");
                }
                long proId = 0;
                if (String.IsNullOrEmpty(programId) || !Int64.TryParse(programId, out proId) || proId == 0)
                {
                    throw new InvalidDataException("Invalid Program");
                }
                long sesId = 0;
                if (String.IsNullOrEmpty(sessionId) || !Int64.TryParse(sessionId, out sesId) || sesId == 0)
                {
                    throw new InvalidDataException("Invalid Session");
                }
                List<Course> couList = _courseService.LoadCourse(_commonHelper.ConvertIdToList(orgId), _commonHelper.ConvertIdToList(proId), _commonHelper.ConvertIdToList(sesId)).ToList();
                if (couList.Any())
                {
                    courseList = couList.Select(x => new ExamQuestionSetPrintCourseApiObj { Id = x.Id, Name = x.Name }).ToList();
                }
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message, Data = courseList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult LoadExam(string key, string organizationId, string programId, string sessionId, string courseId)
        {
            bool successState = false;
            string message = "";
            List<ExamQuestionSetPrintExamApiObj> examList = new List<ExamQuestionSetPrintExamApiObj>();
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }
                long orgId = 0;
                if (String.IsNullOrEmpty(organizationId) || !Int64.TryParse(organizationId, out orgId) || orgId == 0)
                {
                    throw new InvalidDataException("Invalid Organization");
                }
                long proId = 0;
                if (String.IsNullOrEmpty(programId) || !Int64.TryParse(programId, out proId) || proId == 0)
                {
                    throw new InvalidDataException("Invalid Program");
                }
                long sesId = 0;
                if (String.IsNullOrEmpty(sessionId) || !Int64.TryParse(sessionId, out sesId) || sesId == 0)
                {
                    throw new InvalidDataException("Invalid Session");
                }
                long couId = 0;
                if (String.IsNullOrEmpty(courseId) || !Int64.TryParse(courseId, out couId) || couId == 0)
                {
                    throw new InvalidDataException("Invalid Course");
                }
                List<Exams> exList = _examsService.GetExamByProgramSessionAndCourse(proId, sesId, couId).ToList();
                if (exList.Any())
                {
                    examList = exList.Select(x => new ExamQuestionSetPrintExamApiObj
                    {
                        Id = x.Id
                        , Name = x.Name
                        , Code = x.Code
                        , UniqueSet = x.TotalUniqueSet
                        , CourseId = x.Course.Id
                        , IsBanglaVersion = x.IsBanglaVersion
                        , IsEnglishVersion = x.IsEnglishversion
                    }).ToList();
                }
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = successState, Message = message, Data = examList }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Send Question

        public JsonResult LoadQuestion(string key, string examId, string version, string printCount, string uniqueSet)  
        {

            bool successState = false;
            string message = "";
             var examQuestionSetPrintApiDto = new ExamQuestionSetPrintApiDto();
            if (System.Web.HttpContext.Current.Application["RunningSetPrintShcedule"] == null)
            {
                System.Web.HttpContext.Current.Application["RunningSetPrintShcedule"] = "SetPrintShcedule(" + DateTime.Now + ")";
                try
                {
                    if (Authenticate(key) > 0)
                    {
                        throw new InvalidDataException("Invalid Key");
                    }

                    long eId = 0;
                    if (String.IsNullOrEmpty(examId) || !Int64.TryParse(examId, out eId) || eId == 0)
                    {
                        throw new InvalidDataException("Invalid Exam");
                    }

                    int examVersion = 0;
                    if (String.IsNullOrEmpty(examId) || !Int32.TryParse(version, out examVersion) ||
                        !(examVersion == (int) ExamVersion.Bangla || examVersion == (int) ExamVersion.English))
                    {
                        throw new InvalidDataException("Invalid Version");
                    }

                    int totalPrint = 0;
                    if (String.IsNullOrEmpty(examId) || !Int32.TryParse(printCount, out totalPrint) || totalPrint == 0)
                    {
                        throw new InvalidDataException("Invalid Print Count");
                    }



                    if (String.IsNullOrEmpty(uniqueSet) )
                    {
                        throw new InvalidDataException("Invalid Unique Set");
                    }

                    var rgx = new Regex(@"^([0-9]+,)*[0-9]+$");
                    if (!rgx.IsMatch(uniqueSet))
                    {
                         throw new InvalidDataException("Invalid Unique Sets");
                    }

                   

                    var server = ConfigurationManager.AppSettings["MainServer"];
                    var printBy = ConfigurationManager.AppSettings["UserId"];

                    examQuestionSetPrintApiDto = _examMcqQuestionSetPrintService.GetExamQuestionSetPrintApiDto(printBy,
                        server, eId, examVersion, totalPrint, uniqueSet);

                    successState = true;
                    message = "Success !!!";

                   // System.Web.HttpContext.Current.Application.Remove("RunningSetPrintShcedule");
                }
                catch (MessageException ex)
                {
                    message = ex.Message;
                }
                catch (NullObjectException ex)
                {
                    message = ex.Message;
                }
                catch (InvalidDataException ex)
                {
                    message = ex.Message;
                }
                catch (Exception ex)
                {
                    message = "Problem Occur. Please try again.";
                    _logger.Error(ex);
                   
                }
                finally
                {
                    System.Web.HttpContext.Current.Application.Remove("RunningSetPrintShcedule");
                }
            }
            else
            {
                message = "Another Set Print Schedule is running.";
                _logger.Error(message);
            }

            
            return Json(new { IsSuccess = successState, Message = message, Data = examQuestionSetPrintApiDto }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdatePrintStatus(string key, string id, string printStatus)  
        {
            bool successState = false;
            string message = "";
            try
            {
                if (Authenticate(key) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }

                long examMcqQuestionSetPrintId = 0;
                if (String.IsNullOrEmpty(id) || !Int64.TryParse(id, out examMcqQuestionSetPrintId))
                {
                    throw new InvalidDataException("Invalid Exam Mcq Question Set Print Id");
                }

                int ps = 0;
                if (String.IsNullOrEmpty(printStatus) || !Int32.TryParse(printStatus, out ps) ||
                    !(ps == (int)PrintStatus.NotPrinted || ps == (int)PrintStatus.Printed))
                {
                    throw new InvalidDataException("Invalid Print Status");
                }

                _examMcqQuestionSetPrintService.Update(examMcqQuestionSetPrintId, ps);    


                successState = true;
                message = "Print Status Updated Successfully.";
            }
            catch (MessageException ex)
            {
                message = ex.Message;
            }
            catch (NullObjectException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occur. Please try again.";
                _logger.Error(ex);
            }

            return Json(new { IsSuccess = successState, Message = message, Data = "" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}