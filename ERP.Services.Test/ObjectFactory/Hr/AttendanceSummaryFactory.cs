﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class AttendanceSummaryFactory : ObjectFactoryBase<AttendanceSummary>
    {
        public AttendanceSummaryFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

        }

        #region Create

        public AttendanceSummaryFactory Create()
        {
            Object = new AttendanceSummary()
            {
                
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public AttendanceSummaryFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public AttendanceSummaryFactory WithDate(DateTime currentDate)
        {
            Object.EarlyEntry = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, 8, 0, 0);
            Object.LateLeave = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, 20, 0, 0);
            Object.AttendanceDate = currentDate;
            return this;
        }


        #endregion

        #region  others
        #endregion

        #region Clean up

        public AttendanceSummaryFactory CleanUp()
        {
            DeleteAll();
            return this;
        }

        #endregion
    }
}