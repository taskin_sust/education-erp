﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.EmploymentHistoryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "EmploymentHistory")]
    public class EmploymentHistorySaveTest : EmploymentHistoryBaseTest
    {
        #region Basic Test

        [Fact]
        public void EmploymentHistory_Should_Save_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryService.Save(_employmentHistoryFactory.Object);
            Assert.True(true);
        }

        [Fact]
        public void EmploymentHistory_AddEmploymentHistory_Should_Save_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , campus.Id, dept.Id, designation.Id
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString());
            Assert.True(true);
        }

        #endregion

        #region Business Logic Test


        [Fact]
        public void InvalidDataException_EmploymentHistory_Save_For_Null_Campus()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            _employmentHistoryFactory.Create().WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            Assert.Throws<NullReferenceException>(() => _employmentHistoryService.Save(_employmentHistoryFactory.Object));
        }

        [Fact]
        public void InvalidDataException_EmploymentHistory_Save_For_Null_Department()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            Assert.Throws<NullReferenceException>(() => _employmentHistoryService.Save(_employmentHistoryFactory.Object));
        }

        [Fact]
        public void NullReferenceException_EmploymentHistory_Save_For_Null_Designation()
        {
            var teamMember = PersistTeamMember(MentorId);
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithTeamMember(teamMemberFactory.Object);
            Assert.Throws<NullReferenceException>(() => _employmentHistoryService.Save(_employmentHistoryFactory.Object));
        }
        [Fact]
        public void NullReferenceException_EmploymentHistory_Save_For_Null_TeamMember()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus).WithDepartment(dept).WithDesignation(designation);
            Assert.Throws<NullReferenceException>(() => _employmentHistoryService.Save(_employmentHistoryFactory.Object));
        }

        [Fact]
        public void NullReferenceException_EmploymentHistory_Save_For_Invalid_EmploymentStatus()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus).WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMember);
            _employmentHistoryFactory.Object.EmploymentStatus = 0;
            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.Save(_employmentHistoryFactory.Object));
        }

        [Fact]
        public void NullReferenceException_EmploymentHistory_AddEmploymentHistory_For_Duplicate_Date()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , campus.Id, dept.Id, designation.Id
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.ToString(CultureInfo.InvariantCulture)));
        }
        [Fact]
        public void NullReferenceException_EmploymentHistory_AddEmploymentHistory_For_Invalid_EmploymentStatus()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , campus.Id, dept.Id, designation.Id
                                                            , 0
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture)));
        }

        [Fact]
        public void NullReferenceException_EmploymentHistory_AddEmploymentHistory_For_Campus_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , 0, dept.Id, designation.Id
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture)));
        }

        [Fact]
        public void NullReferenceException_EmploymentHistory_AddEmploymentHistory_For_Department_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , campus.Id, 0, designation.Id
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture)));
        }

        [Fact]
        public void NullReferenceException_EmploymentHistory_AddEmploymentHistory_For_Designation_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithTeamMember(teamMemberFactory.Object);
            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.AddEmploymentHistory(teamMember, dept.Organization.Id, campus.Branch.Id
                                                            , campus.Id, dept.Id, 0
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture)));
        }

        #endregion
    }
}
