﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UdvashERP.Areas.Student.Models
{

  
    public class MaterialReports
    {
        public string BranchName { get; set; }
        public string CampusName { get; set; }
        public string BatchDays { get; set; }
        public string BatchName { get; set; }

       public List<StudentProgramDetails> SudentProgramDetailsList { get; set; } 

    }

    public class MaterialReportList
    {
        public IList<MaterialReports> MaterialReportses { get; set; }
        public IList<MaterialSearchInfo> MaterialSearchInfos { get; set; }
    }

    public class StudentProgramDetails
    {
        public string[] StudentInfo { get; set; }


       public List<StudentMaterial> StudentMaterial { get; set; }

    }


    public class StudentMaterial
    {
        public long ItemId { get; set; }

        public long ItemTypeId { get; set; }
        public string Item { get; set; }
        public bool IsDistributed = false;
        public int TotalMaterialCount { get; set; }


    }
    
}