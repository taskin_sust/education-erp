using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class CampusMap : BaseClassMap<Campus, long>
    {
        public CampusMap() {
			Table("Campus");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");

            Map(x => x.Name);
			Map(x => x.Location).Column("Location");
			Map(x => x.ContactNumber).Column("ContactNumber");
            Map(x => x.Rank);
            References(x => x.Branch).Column("BranchId");

            HasMany(x => x.Batches).KeyColumn("CampusId").Cascade.AllDeleteOrphan();
            HasMany(x => x.CampusRooms).KeyColumn("CampusId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.EmploymentHistories).KeyColumn("CampusId").Cascade.AllDeleteOrphan();
            HasMany(x => x.FestivalBonusSheetsSalary).KeyColumn("SalaryCampusId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.FestivalBonusSheetsJob).KeyColumn("JobCampusId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
