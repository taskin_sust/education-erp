$(document).ready(function () {
    function dataTableRender() {
        var pageSize = 10;
        if ($('#pageSize').val() != "" && /^\d+$/.test($('#pageSize').val())) {
            pageSize = parseInt($('#pageSize').val());
        }
        else {
            $('#pageSize').val(pageSize);
        }
        $('#DataGrid').dataTable({
            destroy: true,
            "processing": true,
            searching: false,
            serverSide: true,
            "scrollX": true,
            "bLengthChange": false,
            "iDisplayLength": pageSize,
            //stateSave: true,
            order: [[0, "desc"]],
            ajax: {
                url: $("body").attr("data-project-root") + "Administration/Survey/SurveyQuestionList",
                type: 'POST',
                data: function (d) {                                       
                    d.question = $('#Question').val();
                    d.status = $('#status').val();                   
                }
            }
        });
    }
        
    dataTableRender();

    $(document).on("click", "#search", function () {
        dataTableRender();
    });

    $(document).on("click", ".glyphicon-arrow-up, .glyphicon-arrow-down", function () {
        var id = $(this).attr("id");
        var current = $(this).parent().text().trim();
        var action = $(this).attr("data-action");
        //glyphicon glyphicon-arrow-up
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/Survey/RankChange",
            cache: false,
            async: true,
            data: { "id": id, "current": current, "action": action },
            success: function (result) {
                if (result.IsSuccess) {
                    dataTableRender();
                }
                else {                        
                    bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                }
            },
            error: function (result) {
                bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
            }
        });
    });

    //$(document).on("click", ".glyphicon-th-list", function () {
    //    console.log("Hi");
    //    var id = $(this).attr("id");
    //    if (id != "") {
    //        $.ajax({
    //            type: "post",
    //            url: $("body").attr("data-project-root") + "Administration/Survey/Details",
    //            cache: false,
    //            async: true,
    //            data: { "id": id },
    //            beforeSend: function () {
    //                $.blockUI({
    //                    timeout: 0,
    //                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //                });
    //            },
    //            success: function (result) {
    //                $.unblockUI();
    //                if (result.hasOwnProperty('IsSuccess')) {
    //                    bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
    //                }
    //                else {
    //                    bootbox.alert(result).css('margin-top', (($(window).height() / 4)));
    //                    $(".bootbox-close-button").addClass("glyphicon").addClass("glyphicon-remove").text("");
    //                    $(".modal-footer").css("margin-top", "0px").css("padding", "10px").css("padding-right", "20px");
    //                }
    //            },
    //            complete: function () {
    //                $.unblockUI();
    //            },
    //            error: function (result) {
    //                $.unblockUI();
    //                bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
    //            }
    //        });
    //    }
    //    return false;
    //});

    $(document).on("click", ".glyphicon-trash", function () {
        var id = $(this).attr("id");
        var name = $(this).attr("data-name");
        console.log(id);
        if (id != "") {
            bootbox.deleteConfirm("<h3>Are you sure you want to delete this <span class='confirm-message'>" + name + "</span> Survey Question?</h3>", function (result) {
                if (result) {
                    $.ajax({
                        type: "post",
                        url: $("body").attr("data-project-root") + "Administration/Survey/Delete",
                        cache: false,
                        async: true,
                        data: { "id": id },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (result) {
                            $.unblockUI();

                            if (result.IsSuccess) {                                   
                                $.fn.customMessage({
                                    displayMessage: "Survey Question Delete Successfully",
                                    displayMessageType: "success",
                                });
                                dataTableRender();
                                //customMessage
                            }
                            else {
                                $.fn.customMessage({
                                    displayMessage: result.Message,
                                    displayMessageType: "e",
                                });                                    
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (result) {
                            $.fn.customMessage({
                                displayMessage: "Survey Question Delete Failed",
                                displayMessageType: "e",
                            });
                        }
                    });

                }
            }).css({ 'margin-top': (($(window).height() / 4)) });
        }
        return false;
    });
});

