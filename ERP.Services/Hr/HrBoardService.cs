﻿using System;
using System.Collections;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{
    public interface IHrBoardService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<BusinessModel.Entity.Hr.Board> LoadBoard();
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class HrBoardService : BaseService, IHrBoardService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IAcademicExamDao _hrExamDao;
        private readonly IHrBoardDao _hrBoardDao;

        public HrBoardService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrExamDao = new AcademicExamDao(){Session = session};
            _hrBoardDao = new HrBoardDao(){ Session = session };
        }
        #endregion

        #region Operational Functions

        public bool Save()
        {

            return true;
        }

        #endregion

        #region Single Instances Loading Function

        public Board LoadBoardById(long id)
        {
            try
            {
                Board entity = _hrBoardDao.LoadBoardById(id);
                return entity ?? null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public IList<Board> LoadBoard()
        {
            try
            {
                var list = _hrBoardDao.BoardList();
                if (list != null && list.Count > 0)
                    return list;
                return null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}
