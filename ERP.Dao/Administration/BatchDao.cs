﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Administration
{
    public interface IBatchDao : IBaseDao<Batch, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        Batch GetBatchAuthorize(long batchId, List<long> branchIdList, List<long> programIdList);
        #endregion

        #region List Loading Function
        IList<Batch> LoadBatch(List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, string[] batchDaysList, string[] batchTimeList, List<int> genderList);
        IList<Batch> LoadBatch(List<ProgramBranchPair> programBranchPair, int start, int length, string orderBy, string direction, long? organizationId, string sessionName, string campusName, string batchName, string batchStatus);
        IList<Batch> LoadAuthorizeBatch(List<long> authProgramIdList, List<long> authBranchIdList, List<long> sessionIdList = null, List<long> campusIdList = null, string[] batchDaysList = null, string[] batchTimeList = null, List<int> versionOfStudyList = null, List<int> genderList = null);
        IList<Batch> LoadBatch(long[] batchIds);
        #endregion

        #region Others Function
        int GetBatchCount(List<ProgramBranchPair> programBranchPairList, string orderBy, string direction, long? organizationId, string sessionName, string campusName, string batchName, string batchStatus);
        int GetAuthorizedBatchCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDaysList, string[] batchTimeList, List<int> versionList, List<int> genderList, long[] batchIds);
        bool CheckDuplicateBatch(Batch batchObj, bool isUpdate);
        bool HasDependency(Batch batchObj);
        int GetActiveBatchCount(Campus campusObj);

        #endregion

        #region Helper Function
        //IList<BatchWiseAdmissionReportDto> LoadAuthorizedBatchAdmissionReport(List<long> authOrganizationIdList, List<long> authorizedProgramLists, List<long> authorizedBranchLists, List<long> programIdList, List<long> sessionIdList, List<long> branchIdList, List<long> campusIdList, string[] batchDaysList, string[] batchTimeList, List<int> versionList, List<int> genderList, long[] batchIds, int start, int length);
        IList<BatchWiseAdmissionReportDto> LoadAuthorizedBatchAdmissionReport( List<long> authorizedProgramLists, List<long> authorizedBranchLists,  List<long> sessionIdList, List<long> campusIdList, string[] batchDaysList, string[] batchTimeList, List<int> versionList, List<int> genderList, long[] batchIds, int start, int length);

        #endregion

        #region Public API
        IList<Batch> LoadPublicBatch(string orgBusinessId, long programId, long sessionId, long branchId, long campusId, long versionOfStudy = 0, long gender = 0);
        #endregion

    }
    public class BatchDao : BaseDao<Batch, long>, IBatchDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public Batch GetBatchAuthorize(long batchId, List<long> branchIdList, List<long> programIdList)
        {
            ICriteria criteria = Session.CreateCriteria<Batch>().Add(Restrictions.Not(Restrictions.Eq("Status", Batch.EntityStatus.Delete)));
            criteria.CreateAlias("Program", "p");
            criteria.CreateAlias("Branch", "b");
            criteria.Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("b.Status", Branch.EntityStatus.Active));
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("Program.Id", programIdList));
            }
            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("Branch.Id", branchIdList));
            }
            criteria.Add(Restrictions.Eq("Id", Convert.ToInt64(batchId)));
            return criteria.UniqueResult<Batch>();
        }

        #endregion

        #region List Loading Function

        public IList<Batch> LoadAuthorizeBatch(List<long> authProgramIdList, List<long> authBranchIdList, List<long> sessionIdList = null, List<long> campusIdList = null, string[] batchDaysList = null, string[] batchTimeList = null, List<int> versionOfStudyList = null, List<int> genderList = null)
        {
            ICriteria criteria = Session.CreateCriteria<Batch>().Add(Restrictions.Eq("Status", Batch.EntityStatus.Active));

            criteria.CreateAlias("Campus", "ca").Add(Restrictions.Eq("ca.Status", Campus.EntityStatus.Active));
            DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id"))).CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            programSessionBranch.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
            programSessionBranch.CreateAlias("Session", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            
            if (authProgramIdList != null)
            {
                criteria.Add(Restrictions.In("Program.Id", authProgramIdList));
            }
            if (authBranchIdList != null)
            {
                criteria.Add(Restrictions.In("Branch.Id", authBranchIdList));
            }
            if (sessionIdList != null)
            {
                criteria.Add(Restrictions.In("Session.Id", sessionIdList));
            }
            if (campusIdList != null)
            {
                    criteria.Add(Restrictions.In("Campus.Id", campusIdList));
            }
            if (versionOfStudyList != null && versionOfStudyList.Count > 0)
            {
                if (!versionOfStudyList.Contains((int)SelectionType.SelelectAll))
                {
                    criteria.Add(Restrictions.In("VersionOfStudy", versionOfStudyList));
                }
            }
            if (genderList != null && genderList.Count > 0)
            {
                if (!genderList.Contains((int)SelectionType.SelelectAll))
                {
                    criteria.Add(Restrictions.In("Gender", genderList));
                }
            }
            criteria.Add(Subqueries.PropertyIn("Branch.Id", programSessionBranch));
            return criteria.List<Batch>().OrderBy(x => x.Rank).ToList();
        }
        
        public IList<Batch> LoadBatch(List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, string[] batchDaysList, string[] batchTimeList, List<int> genderList)
        {
            ICriteria criteria = Session.CreateCriteria<Batch>().Add(Restrictions.Eq("Status", Batch.EntityStatus.Active));

            if (organizationIdList != null || programIdList != null)
            {
                criteria.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
                criteria.CreateAlias("pr.Organization", "org").Add(Restrictions.Eq("org.Status", Organization.EntityStatus.Active));
                if (organizationIdList != null)
                {
                    if(!organizationIdList.Contains(SelectionType.SelelectAll))
                        criteria.Add(Restrictions.In("org.Id", organizationIdList));
                }
                if (programIdList != null)
                {
                    if (!programIdList.Contains(SelectionType.SelelectAll))
                        criteria.Add(Restrictions.In("pr.Id", programIdList));
                }
            }

            if (sessionIdList != null)
            {
                criteria.CreateAlias("Session", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
                if (!sessionIdList.Contains(SelectionType.SelelectAll))
                    criteria.Add(Restrictions.In("s.Id", sessionIdList));
            }
            if (branchIdList != null)
            {
                criteria.CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
                if (!branchIdList.Contains(SelectionType.SelelectAll))
                    criteria.Add(Restrictions.In("br.Id", branchIdList));
            }
            if (campusIdList != null)
            {
                criteria.CreateAlias("Campus", "cam").Add(Restrictions.Eq("cam.Status", Campus.EntityStatus.Active));
                if (!campusIdList.Contains(SelectionType.SelelectAll))
                    criteria.Add(Restrictions.In("cam.Id", campusIdList));
            }
            if (batchIdList != null)
            {
                if (!batchIdList.Contains(SelectionType.SelelectAll))
                    criteria.Add(Restrictions.In("Id", batchIdList));
            }
            if (batchDaysList != null && !(Array.Exists(batchDaysList, item => item == SelectionType.SelelectAll.ToString())) && !(Array.Exists(batchDaysList, item => item == "")))
            {
                criteria.Add(Restrictions.In("Days", batchDaysList));
            }

            if (batchTimeList != null && !(Array.Exists(batchTimeList, item => item == SelectionType.SelelectAll.ToString())))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string batchTime in batchTimeList)
                {
                    var batchArrays = batchTime.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("StartTime", startTime) && Restrictions.Le("EndTime", endTime));

                }
                criteria.Add(disjunction);
            }
            if (genderList != null && !genderList.Contains(0) && genderList.Count>0)
                criteria.Add(Restrictions.In("Gender", genderList));
            return criteria.List<Batch>().OrderBy(x => x.Rank).ToList();
        }

        public IList<Batch> LoadBatch(List<ProgramBranchPair> programBranchPairList, int start, int length, string orderBy, string direction, long? organizationId, string sessionName, string campusName, string batchName, string batchStatus)
        {
            ICriteria criteria = GetBatchCriteria(programBranchPairList, orderBy, direction, organizationId, sessionName, campusName, batchName, batchStatus);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Batch>();
        }

        public IList<Batch> LoadBatch(long[] batchIds)
        {
            ICriteria criteria = Session.CreateCriteria<Batch>().Add(Restrictions.Eq("Status", Batch.EntityStatus.Active));
            if (batchIds.Any())
            {
                criteria.Add(Restrictions.In("Id", batchIds));
            }
            return criteria.List<Batch>();
        }
        #endregion

        #region Others Function

        public int GetBatchCount(List<ProgramBranchPair> programBranchPairList, string orderBy, string direction, long? organizationId, string sessionName, string campusName, string batchName, string batchStatus)
        {
            ICriteria criteria = GetBatchCriteria(programBranchPairList, orderBy, direction, organizationId, sessionName, campusName, batchName, batchStatus, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public int GetAuthorizedBatchCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId,long[] branchIds, long[] campusIds, string[] batchDaysList, string[] batchTimeList,List<int> versionList, List<int> genderList, long[] batchIds)
        {

            ICriteria criteria = GetAuthorizedBatchCriteria(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchIds, campusIds, batchDaysList, batchTimeList, versionList, genderList, batchIds);
            criteria.SetProjection(Projections.RowCount());
            return criteria.UniqueResult<int>();
        }

        public bool HasDependency(Batch batchObj)
        {
            int dependBatchCount = Session.QueryOver<StudentProgram>().Where(
                x => x.Batch.Id == batchObj.Id
                     && x.Status != StudentProgram.EntityStatus.Delete
                ).RowCount();

            if (dependBatchCount > 0)
                return true;
            return false;
        }

        public int GetActiveBatchCount(Campus campusObj)
        {
            ICriteria criteria = Session.CreateCriteria<Batch>().Add(Restrictions.Eq("Status",Batch.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Campus.Id", campusObj.Id));
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public bool CheckDuplicateBatch(Batch batchObj, bool isUpdate)
        {
            if (isUpdate)
            {
                var result = Session.QueryOver<Batch>().Where(x => x.Program == batchObj.Program && x.Session == batchObj.Session && x.Branch == batchObj.Branch
                    && x.Campus == batchObj.Campus && x.Name == batchObj.Name && x.Status != Batch.EntityStatus.Delete && x.Id != batchObj.Id).SingleOrDefault<Batch>();

                if (result == null || result.Id < 1)
                    return false;
                return true;
            }
            var result2 = Session.QueryOver<Batch>().Where(x => x.Program == batchObj.Program && x.Session == batchObj.Session && x.Branch == batchObj.Branch
                     && x.Campus == batchObj.Campus && x.Name == batchObj.Name && x.Status != Batch.EntityStatus.Delete).SingleOrDefault<Batch>();

            if (result2 == null || result2.Id < 1)
                return false;
            return true;
        }

        #endregion

        #region Helper Function
        private ICriteria GetBatchCriteria(List<ProgramBranchPair> programBranchPairList, string orderBy, string direction, long? organizationId, string sessionName, string campusName, string batchName, string batchStatus, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<Batch>().Add(Restrictions.Not(Restrictions.Eq("Status", Batch.EntityStatus.Delete)));
            criteria.CreateAlias("Program", "Program");
            criteria.CreateAlias("Session", "Session");
            criteria.CreateAlias("Branch", "Branch");
            criteria.CreateAlias("Campus", "Campus");
            criteria.CreateAlias("Program.Organization", "Organization");
            criteria.Add(Restrictions.Eq("Organization.Status", Organization.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Program.Status", Program.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Session.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Branch.Status", Branch.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Campus.Status", Campus.EntityStatus.Active));
            var disjunction = Restrictions.Disjunction(); // for OR statement 
            if (programBranchPairList!=null && programBranchPairList.Count>0)
            {
                foreach (var programBranch in programBranchPairList)
                {
                    disjunction.Add(Restrictions.Eq("Program.Id", programBranch.ProgramId) && Restrictions.Eq("Branch.Id", programBranch.BranchId));
                }
                criteria.Add(disjunction); 
            }
            if (organizationId != null && organizationId > 0)
            {
                criteria.Add(Restrictions.Eq("Organization.Id", organizationId));
            }
            if (!String.IsNullOrEmpty(sessionName))
            {
                criteria.Add(Restrictions.Like("Session.Name", sessionName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(campusName))
            {
                criteria.Add(Restrictions.Like("Campus.Name", campusName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(batchName))
            {
                criteria.Add(Restrictions.InsensitiveLike("Name", batchName.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(batchStatus))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(batchStatus)));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(direction == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }
        
        private ICriteria GetBatchCriteria(long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDaysList=null, string[] batchTimeList=null)
        {
            ICriteria criteria = Session.CreateCriteria<Batch>().Add(Restrictions.Eq("Status", Batch.EntityStatus.Active));
            criteria.CreateAlias("Program", "program").Add(Restrictions.Eq("Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Session", "session").Add(Restrictions.Eq("Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("Branch", "branch").Add(Restrictions.Eq("Status", Branch.EntityStatus.Active));
            criteria.CreateAlias("Campus", "campus").Add(Restrictions.Eq("Status", Campus.EntityStatus.Active));

            criteria.Add(Restrictions.Eq("program.Id", programId));
            criteria.Add(Restrictions.Eq("session.Id", sessionId));

            if (!(Array.Exists(branchIds, item => item == SelectionType.SelelectAll)))
            {
                criteria.Add(Restrictions.In("branch.Id", branchIds));
            }
            if (!(Array.Exists(campusIds, item => item == SelectionType.SelelectAll)))
            {
                criteria.Add(Restrictions.In("campus.Id", campusIds));
            }

            if (batchDaysList != null && !(Array.Exists(batchDaysList, item => item == SelectionType.SelelectAll.ToString())) && !(Array.Exists(batchDaysList, item => item == "")))
            {
                criteria.Add(Restrictions.In("Days", batchDaysList));
            }

            if (batchTimeList != null && !(Array.Exists(batchTimeList, item => item == SelectionType.SelelectAll.ToString())))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string batchTime in batchTimeList)
                {
                    var batchArrays = batchTime.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("StartTime", startTime) && Restrictions.Le("EndTime", endTime));

                }
                criteria.Add(disjunction);
            }
            return criteria;
        }

        private ICriteria GetAuthorizedBatchCriteria(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId,long[] branchIds, long[] campusIds, string[] batchDaysList, string[] batchTimeList,List<int> versionList, List<int> genderList, long[] batchIds) 
        {
            ICriteria criteria = GetBatchCriteria(programId, sessionId, branchIds, campusIds, batchDaysList, batchTimeList);
            if (authorizedProgramLists != null)
            {
                criteria.Add(Restrictions.In("Program.Id", authorizedProgramLists));
            }
            if (authorizedBranchLists != null)
            {
                criteria.Add(Restrictions.In("Branch.Id", authorizedBranchLists));
            }

            if (!versionList.Contains((int)SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("VersionOfStudy", versionList));
            }
            if (!genderList.Contains((int)SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("Gender", genderList));
            }
            if (!batchIds.Contains((int)SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("Id", batchIds));
            }
            return criteria;
        }

        public IList<BatchWiseAdmissionReportDto> LoadAuthorizedBatchAdmissionReport(List<long> authorizedProgramLists, List<long> authorizedBranchLists,  List<long> sessionIdList,  List<long> campusIdList, string[] batchDaysList, string[] batchTimeList, List<int> versionList, List<int> genderList, long[] batchIds, int start, int length)
        {
            string query = "DECLARE @rowsperpage INT DECLARE @start INT SET @start = " + (start + 1) + " SET @rowsperpage = " + length + " select * from (select row_number() OVER (ORDER BY b.Id) AS RowNum, br.Name as Branch, b.Days as BatchDays, b.StartTime as BatchStartTime, b.EndTime as BatchEndTime, c.Name as Campus,b.VersionOfStudy as Version,b.Gender as Gender,b.Name as Batch, b.Capacity as TotalCapacity,b.[Rank] as BatchRank,ISNULL(a.StudentAdmit,0) as StudentAdmit, b.Capacity-ISNULL(a.StudentAdmit,0) as AvailableCapacity"
                        + " from [dbo].[Batch] as b left join (SELECT count(Id) as StudentAdmit, BatchId FROM [dbo].[StudentProgram] WHERE Status =" + StudentProgram.EntityStatus.Active.ToString() + " group by BatchId) as a on a.BatchId = b.Id"
                        + " left join [dbo].[Branch] as br on b.BranchId = br.Id left join [dbo].[Campus] as c on b.CampusId = c.Id where b.ProgramId IN( ";
            query += string.Join(",", authorizedProgramLists) + ") and b.SessionId IN( " + string.Join(",", sessionIdList) + ")";

            if (authorizedProgramLists != null)
            {
                query += " and b.ProgramId IN (" + string.Join(",", authorizedProgramLists) + ")";
            }
            if (authorizedBranchLists != null)
            {
                query += " and b.BranchId IN (" + string.Join(",", authorizedBranchLists) + ")";
            }
            if (campusIdList != null)
            {
                query += " and b.CampusId IN (" + string.Join(",", campusIdList) + ")";
            }


            if (!batchDaysList.Contains(SelectionType.SelelectAll.ToString()))
            {
                query += " and b.Days IN ('" + string.Join("','", batchDaysList) + "')";
            }

            if (batchTimeList != null &&
                !(Array.Exists(batchTimeList, item => item == SelectionType.SelelectAll.ToString())))
            {
                string batchTimeQuery = "";
                int i = 0;
                foreach (string batchTime in batchTimeList)
                {
                    //var batchArray = batchTime.Split(',');
                    var batchArrays = batchTime.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (i == 0)
                    {
                        batchTimeQuery += " (b.StartTime>='" + startTime + "' and b.EndTime<='" + endTime + "')";
                    }
                    else
                    {
                        batchTimeQuery += " OR (b.StartTime>='" + startTime + "' and b.EndTime<='" + endTime + "')";
                    }
                    i++;

                }
                if (batchTimeQuery != "")
                {
                    query += " and (" + batchTimeQuery + ")";
                }
            }
            if (!versionList.Contains((int)SelectionType.SelelectAll))
            {
                query += " and b.VersionOfStudy IN(" + string.Join(",", versionList) + ")";
            }
            if (!genderList.Contains((int)SelectionType.SelelectAll))
            {
                query += " and b.Gender IN(" + string.Join(",", genderList) + ")";
            }
            if (!batchIds.Contains((int)SelectionType.SelelectAll))
            {
                query += " and b.Id IN(" + string.Join(",", batchIds) + ")";
            }
            else
            {
                var batchIdList = LoadAuthorizeBatch(authorizedProgramLists, authorizedBranchLists, sessionIdList, campusIdList, batchDaysList, batchTimeList, versionList, genderList).Select(x => x.Id).ToList();
                query += " and b.Id IN(" + string.Join(",", batchIdList) + ")";
            }
            query += ") as pagination";

            if (length > 0)
            {
                query += " where  pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<BatchWiseAdmissionReportDto>());
            return iQuery.List<BatchWiseAdmissionReportDto>().ToList();
        }

        #endregion

        #region Public API
        public IList<Batch> LoadPublicBatch(string orgBusinessId, long programId, long sessionId, long branchId, long campusId, long versionOfStudy = 0, long gender = 0)
        {
            ICriteria criteria = Session.CreateCriteria<Batch>().Add(Restrictions.Eq("Status", Batch.EntityStatus.Active));

            if (versionOfStudy == 1)
            {
                criteria.Add(Restrictions.Or(Restrictions.Eq("VersionOfStudy", 1), Restrictions.Eq("VersionOfStudy", 3)));
            }
            else if (versionOfStudy == 2)
            {
                criteria.Add(Restrictions.Or(Restrictions.Eq("VersionOfStudy", 2), Restrictions.Eq("VersionOfStudy", 3)));
            }
            else if (versionOfStudy == 3)
            {
                criteria.Add(Restrictions.Eq("VersionOfStudy", 3));
            }

            if (gender == 1)
            {
                criteria.Add(Restrictions.Or(Restrictions.Eq("Gender", 1), Restrictions.Eq("Gender", 3)));
            }
            else if (gender == 2)
            {
                criteria.Add(Restrictions.Or(Restrictions.Eq("Gender", 2), Restrictions.Eq("Gender", 3)));
            }
            else if (gender == 3)
            {
                criteria.Add(Restrictions.Eq("Gender", 3));
            }

            criteria.CreateAlias("Campus", "ca").Add(Restrictions.Eq("ca.Status", Campus.EntityStatus.Active));

            DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id"))).CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));

            programSessionBranch.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));

            programSessionBranch.CreateAlias("Session", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));

            if (programId != (SelectionType.SelelectAll))
                criteria.Add(Restrictions.Eq("Program.Id", programId));

            if (sessionId != (SelectionType.SelelectAll))
                criteria.Add(Restrictions.Eq("Session.Id", sessionId));

            if (branchId != (SelectionType.SelelectAll))
                criteria.Add(Restrictions.Eq("Branch.Id", branchId));

            if (campusId != (SelectionType.SelelectAll))
                criteria.Add(Restrictions.Eq("Campus.Id", campusId));

            criteria.Add(Subqueries.PropertyIn("Branch.Id", programSessionBranch));
            return criteria.List<Batch>().OrderBy(x => x.Rank).ToList();
        }
        #endregion
    }
}