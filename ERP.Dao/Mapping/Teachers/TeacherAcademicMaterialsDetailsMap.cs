﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.Inspections;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherAcademicMaterialsDetailsMap : BaseClassMap<TeacherAcademicMaterialsDetails, long>
    {
        public TeacherAcademicMaterialsDetailsMap()
        {
            Table("TeacherAcMaterialsDetails");
            LazyLoad();
            References(x => x.TeacherAcademicMaterials).Column("TeacherAcMaterialsId");
            References(x => x.Teacher).Column("TeacherId");
            References(x => x.ForBranch).Column("ForBranchId");
            References(x => x.PaidFromBranch).Column("PaidFromBranchId");
            Map(x => x.Description).Column("Description");
            Map(x => x.Amount).Column("Amount");
            Map(x => x.PaymentStatus).Column("PaymentStatus");
            Map(x => x.PaymentDate).Column("PaymentDate");
            Map(x => x.PaidBy).Column("PaidBy");

        }
    }
}
