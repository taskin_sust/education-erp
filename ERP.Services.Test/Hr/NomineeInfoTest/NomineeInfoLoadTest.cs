﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.NomineeInfoTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "NomineeInfo")]
    public class NomineeInfoLoadTest : NomineeInfoBaseTest
    {
        [Fact]
        public void LoadById_Not_Null_Check()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            var objFactory = NomineeInfoFactory.Create();
            NomineeInfoService.UpdateNomineeInformation(teamMemberObj, objFactory.Object);
            var jobExperiencesObj = NomineeInfoService.GetTeamMemberNomineeInfo(teamMemberObj.Id);
            Assert.NotNull(jobExperiencesObj);
        }

        [Fact]
        public void LoadById_Null_Check()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            var jobExperiencesObj = NomineeInfoService.GetTeamMemberNomineeInfo(teamMemberObj.Id);
            Assert.Null(jobExperiencesObj);
        }
        
    }
}
