﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface ISalarySheetDao : IBaseDao<SalarySheet, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        SalarySheet GetTeamMemberLastSalarySheet(long? teamMemberId, int? pin);
        SalarySheet GetOrganizationLastSalarySheet(long organizationId);
        SalarySheet GetTeamMemberSalarySheet(TeamMember tmMember, DateTime dateFrom, DateTime dateTo, SalaryHistory salaryHistory);
        SalarySheetDto GetSalarySheetForFirstMonthTeamMember(List<long> authOrganizationIdList, List<long> authBranchIdList, int totalDaysOfMonth, DateTime startDate,
            DateTime endDate, EmploymentHistory employmentHistory, SalarySheetFormViewModel viewModel);

        #endregion

        #region List Loading Function

        List<SalarySheetDto> LoadTeamMemberForSalarySheet(List<long> authOrganizationIdList, List<long> authBranchIdList, int totalDaysOfMonth,
            DateTime startDate, DateTime endDate, SalarySheetFormViewModel viewModel);

        List<SalarySheet> LoadSalarySheetList(List<long> authOrganizationIdList, List<long> authBranchIdList, int totalDaysOfMonth,
            DateTime startDate, DateTime endDate, SalarySheetFormViewModel viewModel);

        #endregion

        #region Others Function

        void GetHaveToUpdateTableInformation(out List<AttendanceSummary> attenadnceSummarylist, out List<AttendanceAdjustment> attendanceAdjustmentList,
            out List<DayOffAdjustment> dayOffList, out List<LeaveApplicationDetails> leaveApplicationDetailList, TeamMember teamMember, DateTime startDate,
            DateTime endDate);

        #endregion

        #region Helper Function
        #endregion
        
    }
    public class SalarySheetDao : BaseDao<SalarySheet, long>, ISalarySheetDao
    {
        #region Propertise & Object Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public SalarySheet GetTeamMemberLastSalarySheet(long? teamMemberId, int? pin)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<SalarySheet>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", SalarySheet.EntityStatus.Delete))); //Add(Restrictions.Eq("Status", SalarySheet.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("IsSubmit", true));
            criteria.CreateAlias("TeamMember", "member");//.Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("EndDate"));
            return criteria.List<SalarySheet>().Take(1).FirstOrDefault();
        }

        public SalarySheet GetOrganizationLastSalarySheet(long organizationId)
        {
            ICriteria criteria = Session.CreateCriteria<SalarySheet>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", SalarySheet.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("IsSubmit", true));
            criteria.Add(Restrictions.Eq("IsSubmit", true));
            criteria.CreateAlias("JobOrganization", "organization");
            criteria.Add(Restrictions.Eq("organization.Id", organizationId));
            criteria.AddOrder(Order.Desc("EndDate"));
            return criteria.List<SalarySheet>().Take(1).SingleOrDefault();
        }

        public SalarySheet GetTeamMemberSalarySheet(TeamMember tmMember, DateTime dateFrom, DateTime dateTo, SalaryHistory salaryHistory)
        {
            ICriteria criteria = Session.CreateCriteria<SalarySheet>();
            criteria.Add(Restrictions.Eq("Status", SalarySheet.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "member");
            criteria.CreateAlias("SalaryHistory", "sh");
            criteria.Add(Restrictions.Eq("member.Id", tmMember.Id));
            criteria.Add(Restrictions.Eq("sh.Id", salaryHistory.Id));
            criteria.Add(Restrictions.Ge("StartDate", dateFrom));
            criteria.Add(Restrictions.Le("EndDate", dateTo));
            return criteria.List<SalarySheet>().Take(1).FirstOrDefault();
        }

        public SalarySheetDto GetSalarySheetForFirstMonthTeamMember(List<long> authOrganizationIdList, List<long> authBranchIdList,
            int totalDaysOfMonth, DateTime startDate, DateTime endDate, EmploymentHistory employmentHistory, SalarySheetFormViewModel viewModel)
        {
            string query = @" 
                        DECLARE @Year AS INT,@TotalWeekendOfMonth AS INT,@TotalDaysOFMonth AS INT,
                        @FirstDateOfYear DATETIME,
                        @LastDateOfYear DATETIME,
						@Month AS INT,@TotaldaysOfMonthFromJoinDate AS INT
                        SELECT @year = " + viewModel.SalaryYear + @"
                        SELECT @FirstDateOfYear = DATEADD(yyyy, @Year - 1900, 0)
                        SELECT @LastDateOfYear = DATEADD(yyyy, @Year - 1900 + 1, 0)
                        SELECT @TotalDaysOFMonth = " + totalDaysOfMonth + @"
						SELECT @Month = " + startDate.Month + @"
						SELECT @TotaldaysOfMonthFromJoinDate = DATEDIFF(DAY, '" + employmentHistory.EffectiveDate + @"', '" + endDate + @"')+1
                        --wEEKND
                        ;WITH cte AS (
	                        SELECT 1 AS DayID,
	                        @FirstDateOfYear AS FromDate,
	                        DATENAME(dw, @FirstDateOfYear) AS Dayname
	                        UNION ALL
	                        SELECT cte.DayID + 1 AS DayID,
	                        DATEADD(d, 1 ,cte.FromDate),
	                        DATENAME(dw, DATEADD(d, 1 ,cte.FromDate)) AS Dayname
	                        FROM cte
	                        WHERE DATEADD(d,1,cte.FromDate) < @LastDateOfYear
                        ),
                        memberWeekend AS(
                          SELECT * FROM(
	                        SELECT DayName,count(DayName) AS TotalWeek FROM CTE WHERE FromDate>='" + employmentHistory.EffectiveDate +
                           @"' and FromDate<='" + endDate + @"' 
	                        group by DayName
	                        ) as a
                        ),           

                        -- EBF Settings
                        ebf AS(
	                        select * from (
								select ebfs.Id as EbfSettingsId,ebfs.EmploymentStatus,ebfs.CalculationOn,ebfs.EmployeeContribution,ebfs.EmployerContribution,ebfs.EffectiveDate,ebfs.ClosingDate 
								from (
									select ebfs.Id ,ebfs.OrganizationId,ebfs.EffectiveDate,ebfs.ClosingDate
									,ebfs.EmploymentStatus,ebfs.CalculationOn,ebfs.EmployeeContribution,ebfs.EmployerContribution 
									,RANK() OVER(PARTITION BY ebfs.EmploymentStatus ORDER BY ebfs.EffectiveDate DESC,ebfs.Id DESC) AS R  
									from HR_EmployeeBenefitsFundSetting as ebfs
									where ebfs.OrganizationId = " + viewModel.OrganizationId + @" and 
									ebfs.EffectiveDate<='" + startDate + @"' and (ebfs.ClosingDate is null  or ebfs.ClosingDate>='" + endDate + @"') 
									and ebfs.Status = 1
								) as ebfs where ebfs.R = 1
								) as a --order by a.EffectiveDate desc
                        )

                        -- Main Query
                        Select 
                        f.TeamMemberId as TeamMemberId
                        ,f.SalaryHistoryId
                        ,f.SalaryOrganizationId
                        ,f.SalaryBranchId
                        ,f.SalaryCampusId
                        ,f.SalaryDepartmentId
                        ,f.SalaryDesignationId
                        ,f.OrganizationId
                        ,f.BranchId
                        ,f.CampusId
                        ,f.DepartmentId
                        ,f.DesignationId
                        ,f.Name
                        ,f.Pin                       
                        ,f.designation as Designation
                        ,f.BasicSalary
                        ,f.HouseRent
                        ,f.Conveyance
                        ,f.Medical
                        ,f.GrossSalary
                        ,f.Increament
                        ,f.Arrear
                        ,f.EmploymentStatus
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.ZoneAmount
						   ELSE 0.00
						  END AS ZoneAmount               
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.ZoneAmount
						   ELSE 0.00
						  END AS FinalZoneAmount
                        ,ISNULL(                            
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN CONVERT(DECIMAL(18,2),f.AbsentAmount)
						   ELSE 0.00
						  END,0
                         ) AS AbsentAmount
                        ,ISNULL(                            
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN CONVERT(DECIMAL(18,2),f.AbsentAmount)
						   ELSE 0.00
						  END,0
                         ) AS FinalAbsentAmount
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.LeaveWithoutPayAmount
						   ELSE 0.00
						  END AS LeaveWithoutPayAmount
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.Loan
						   ELSE 0.00
						  END AS Loan
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.TotalRefundAmount
						   ELSE 0.00
						  END AS FinalLoanRefund
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.TdsAmount
						   ELSE 0.00
						  END AS TdsAmount
                         ,ISNULL(
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN CONVERT(DECIMAL(18,2),f.Ebf)
						   ELSE 0.00
						  END,0) AS Ebf
                         ,ISNULL(
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN CONVERT(DECIMAL(18,2),f.EbfForEmplyer)
						   ELSE 0.00
						  END,0) AS EbfForEmplyer
                        ,f.Bank
                        ,f.Cash
                        --,f.ZoneSettingsId
                        --,ISNULL(f.ZoneCount,0) AS ZoneCount
                        --,ISNULL(CONVERT(DECIMAL(18,2),f.ZoneDeductionMultiplier),0) AS ZoneDeductionMultiplier
                        --,ISNULL(CONVERT(DECIMAL(18,2),f.ZoneUnitAmount),0) AS ZoneUnitAmount
                        ,(f.Bank + f.Cash) as TotalAmount
                        ,f.DeductionPerAbsent
                        ,f.TotalAbsentDay
                        ,f.AbsentMultiplierFactor
                        ,f.EbfSettingsId
                        ,ISNULL(CONVERT(DECIMAL(18,2),f.CalculationOn),0) AS CalculationOn
                        ,ISNULL(CONVERT(DECIMAL(18,2),f.EmployeeContribution),0) AS EmployeeContribution
                        ,ISNULL(CONVERT(DECIMAL(18,2),f.EmployerContribution),0) AS EmployerContribution
                        ,ISNULL(CONVERT(DECIMAL(18,2),f.TotalSalaryWithoutDeduction),0) AS TotalSalaryWithoutDeduction
                        ,'' as Remarks
                        from (
	                        Select e.*,
	                        ISNULL(
		                        CASE 
			                        WHEN (e.CashAmount-e.deduction)>=0 THEN (e.CashAmount-e.deduction)		
			                        ELSE 0
		                        END
	                        ,0) as Cash
	                        ,ISNULL(
		                        CASE 
			                        WHEN (e.CashAmount-e.deduction)<=0 THEN (e.BankAmount-(e.deduction-e.CashAmount))

			                        WHEN (e.CashAmount-e.deduction)>=0 THEN e.BankAmount		
			                        ELSE 0
		                        END
	                        ,0) as Bank  
	                          FROM (
		                        Select d.*, (d.ZoneAmount + d.AbsentAmount + d.LeaveWithoutPayAmount + d.Loan + d.TdsAmount + d.Ebf) as deduction
		                         from (
                                        Select 
											d.TeamMemberId,d.Name,d.Pin,d.IsAutoDeduction,d.designation,d.SalaryHistoryId,d.BankAmount,d.CashAmount,d.OrganizationId,d.BranchId,d.CampusId
											,d.DepartmentId,d.DesignationId,d.SalaryOrganizationId,d.SalaryBranchId,d.SalaryCampusId,d.SalaryDepartmentId,d.SalaryDesignationId,d.DeductionPerAbsent,d.TotalAbsentDay
											,d.TotalSalaryWithoutDeduction,d.BasicSalary,d.HouseRent,d.Conveyance,d.Medical,d.GrossSalary,d.Increament,d.Arrear,d.AbsentAmount,d.AbsentMultiplierFactor
											,SUM(d.LeaveWithoutPayAmount) as LeaveWithoutPayAmount,d.Loan,d.TotalRefundAmount,d.TdsAmount,d.Ebf,d.EbfForEmplyer,SUM(d.ZoneAmount) as ZoneAmount
                                            ,d.EbfSettingsId,d.CalculationOn,d.EmployeeContribution,d.EmployerContribution,d.EmploymentStatus
									   from (
			                                Select hrm.Id as TeamMemberId,hrm.FullNameEng as Name,hrm.Pin as Pin,hrm.IsAutoDeduction,designation.Name as designation--, cp.EmploymentStatus as EmploymentStatusInt
			                                ,sh.SalaryHistoryId
                                            ,sh.BankAmount
			                                ,sh.CashAmount
			                                ,cp.OrganizationId
			                                ,cp.BranchId
			                                ,cp.CampusId
			                                ,cp.DepartmentId
			                                ,cp.DesignationId
			                                ,cp.EbfSettingsId
			                                ,cp.CalculationOn
			                                ,cp.EmployeeContribution
			                                ,cp.EmployerContribution
			                                ,sh.SalaryOrganizationId
			                                ,sh.SalaryBranchId
			                                ,sh.SalaryCampusId
			                                ,sh.SalaryDepartmentId
			                                ,sh.SalaryDesignationId
                                            ,cp.EmploymentStatus
			                                ,zonededuction.ZoneSettingsId
			                                ,zonededuction.ZoneCount
			                                ,zonededuction.ZoneDeductionMultiplier
			                                ,CONVERT(DECIMAL(18,2),(sh.Salary/organization.MonthlyWorkingDay)) as ZoneUnitAmount
			                                ,organization.DeductionPerAbsent
			                                ,ISNULL(absentDays.Absent,0) as TotalAbsentDay
                                            ,sh.Salary as TotalSalaryWithoutDeduction
			                        
			                                ,ISNULL(
			                                CASE
			                                  WHEN cp.EmploymentStatus = 2 THEN CONVERT(DECIMAL(18,2), (organization.BasicSalary*sh.Salary)/100)
			                                  ELSE 0
			                                END,0) as BasicSalary

			                                ,ISNULL(
			                                CASE
			                                  WHEN cp.EmploymentStatus = 2 THEN CONVERT(DECIMAL(18,2), (organization.HouseRent*sh.Salary)/100)
			                                  ELSE 0
			                                END,0) as HouseRent
			                                ,ISNULL(
			                                CASE
			                                  WHEN cp.EmploymentStatus = 2 THEN CONVERT(DECIMAL(18,2), (organization.Convenyance*sh.Salary)/100)
			                                  ELSE 0
			                                END,0) as Conveyance
			                                ,ISNULL(
			                                CASE
			                                  WHEN cp.EmploymentStatus = 2 THEN CONVERT(DECIMAL(18,2), (organization.MedicalAllowance*sh.Salary)/100)
			                                  ELSE 0
			                                END,0) as Medical
			                                ,ISNULL( sh.Salary,0) as GrossSalary
			                                ,ISNULL(CONVERT(DECIMAL(18,2),0),0) as Increament
			                                ,ISNULL(CONVERT(DECIMAL(18,2),0),0) as Arrear
			                                ,ISNULL( 
			                                 CASE 
			                                   WHEN cp.EmploymentStatus =  1 or cp.EmploymentStatus =  3 or cp.EmploymentStatus =  4 or cp.EmploymentStatus =  5 THEN CONVERT(DECIMAL(18,2),(sh.Salary/organization.MonthlyWorkingDay)*zonededuction.ZoneDeductionMultiplier)

                                               WHEN cp.EmploymentStatus =  2 and Month(jd.EffectiveDate)=@Month and YEAR(jd.EffectiveDate)=@Year   THEN  CONVERT(DECIMAL(18,2),(sh.Salary/organization.MonthlyWorkingDay)*zonededuction.ZoneDeductionMultiplier) --First Month calculation

			                                   --WHEN cp.EmploymentStatus =  2 THEN  CONVERT(DECIMAL(18,2),(((organization.BasicSalary*sh.Salary)/100)/organization.MonthlyWorkingDay)*zonededuction.ZoneDeductionMultiplier)
			                           
                                                else 0
			                                   END,0) as ZoneAmount

			                                ,ISNULL(
				                                 CASE 
				                                 WHEN cp.EmploymentStatus =  1 or cp.EmploymentStatus =  3 or cp.EmploymentStatus =  4 or cp.EmploymentStatus =  5 THEN CONVERT(DECIMAL(18,2),((sh.Salary/organization.MonthlyWorkingDay)*absentDays.Absent)*organization.DeductionPerAbsent)
												        --+CONVERT(DECIMAL(18,2),((sh.Salary/organization.MonthlyWorkingDay)*absentDays.AbsentDaysCountBeforeJoiningDate))

                                                 WHEN cp.EmploymentStatus =  2 and Month(jd.EffectiveDate)=@Month and YEAR(jd.EffectiveDate)=@Year  THEN CONVERT(DECIMAL(18,2),((sh.Salary/organization.MonthlyWorkingDay))*absentDays.Absent*organization.DeductionPerAbsent)
												        --+CONVERT(DECIMAL(18,2),((sh.Salary*organization.MonthlyWorkingDay))*absentDays.AbsentDaysCountBeforeJoiningDate) --First Month calculation

				                                -- WHEN cp.EmploymentStatus =  2 THEN CONVERT(DECIMAL(18,2),((((organization.BasicSalary*sh.Salary)/100)/organization.MonthlyWorkingDay)*organization.DeductionPerAbsent)*absentDays.Absent)
										        --		+CONVERT(DECIMAL(18,2),((((organization.BasicSalary*sh.Salary)/100)/organization.MonthlyWorkingDay)*absentDays.AbsentDaysCountBeforeJoiningDate))
				                                 ELSE 0
			                                  END
			                                ,0) as AbsentAmount
			                                ,ISNULL(
				                                 CASE 
				                                 WHEN cp.EmploymentStatus =  1 or cp.EmploymentStatus =  3 or cp.EmploymentStatus =  4 or cp.EmploymentStatus =  5 THEN CONVERT(DECIMAL(18,2),((sh.Salary/organization.MonthlyWorkingDay)*organization.DeductionPerAbsent))

                                                 WHEN cp.EmploymentStatus =  2 and Month(jd.EffectiveDate)=@Month and YEAR(jd.EffectiveDate)=@Year   THEN CONVERT(DECIMAL(18,2),((sh.Salary/organization.MonthlyWorkingDay)*organization.DeductionPerAbsent)) --First Month calculation

				                                 --WHEN cp.EmploymentStatus =  2 THEN CONVERT(DECIMAL(18,2),((((organization.BasicSalary*sh.Salary)/100)/organization.MonthlyWorkingDay)*organization.DeductionPerAbsent))
				                                 ELSE 0
			                                  END
			                                ,0) as AbsentMultiplierFactor
			                                ,ISNULL(
			                                 CASE

                                              WHEN cp.EmploymentStatus = 2 and Month(jd.EffectiveDate)=@Month and YEAR(jd.EffectiveDate)=@Year  THEN CONVERT(DECIMAL(18,2), (sh.Salary/organization.MonthlyWorkingDay*lwp.TotalLeaveDay)) --First Month calculation

			                                  --WHEN cp.EmploymentStatus = 2 THEN CONVERT(DECIMAL(18,2), ((organization.BasicSalary*sh.Salary)/100)/organization.MonthlyWorkingDay*lwp.TotalLeaveDay)

			                                  ELSE CONVERT(DECIMAL(18,2),(sh.Salary/organization.MonthlyWorkingDay*lwp.TotalLeaveDay))
			                                END,0) as LeaveWithoutPayAmount
			                                ,ISNULL(Loan.Loan,0) as Loan
									        ,ISNULL(Loan.RefundAmount,0) As TotalRefundAmount
			                                ,ISNULL(tds.TdsAmount,0) as TdsAmount
			                                ,ISNULL(
			                                CASE 
				                                 WHEN (cp.EmploymentStatus =  1 or cp.EmploymentStatus =  3 or cp.EmploymentStatus = 4 or cp.EmploymentStatus = 5 )  and cp.CalculationOn = 1 THEN CONVERT(DECIMAL(18,2), (((organization.BasicSalary*sh.Salary)/100)*cp.EmployeeContribution)/100)
				                                 WHEN (cp.EmploymentStatus =  1 or cp.EmploymentStatus =  3 or cp.EmploymentStatus = 4 or cp.EmploymentStatus = 5 )  and cp.CalculationOn = 2 THEN CONVERT(DECIMAL(18,2), (sh.Salary*cp.EmployeeContribution)/100)

                                                 WHEN cp.EmploymentStatus =  2 and cp.CalculationOn = 1 and Month(jd.EffectiveDate)=@Month and YEAR(jd.EffectiveDate)=@Year THEN CONVERT(DECIMAL(18,2), (sh.Salary*cp.EmployeeContribution)/100) --First Month calculation

				                                 --WHEN cp.EmploymentStatus =  2 and cp.CalculationOn = 1 THEN CONVERT(DECIMAL(18,2), (((organization.BasicSalary*sh.Salary)/100)*cp.EmployeeContribution)/100)

				                                 WHEN cp.EmploymentStatus =  2 and cp.CalculationOn = 2 THEN CONVERT(DECIMAL(18,2), (sh.Salary*cp.EmployeeContribution)/100)
				                        
				                                 ELSE 0
			                                  END
			                                ,0) as Ebf
			                                ,ISNULL(
			                                CASE 
				                                 WHEN (cp.EmploymentStatus =  1 or cp.EmploymentStatus =  3 or cp.EmploymentStatus = 4 or cp.EmploymentStatus = 5 )  and cp.CalculationOn = 1 THEN CONVERT(DECIMAL(18,2), (((organization.BasicSalary*sh.Salary)/100)*cp.EmployerContribution)/100)
				                                 WHEN (cp.EmploymentStatus =  1 or cp.EmploymentStatus =  3 or cp.EmploymentStatus = 4 or cp.EmploymentStatus = 5 )  and cp.CalculationOn = 2 THEN CONVERT(DECIMAL(18,2), (sh.Salary*cp.EmployerContribution)/100)

                                                 WHEN cp.EmploymentStatus =  2 and Month(jd.EffectiveDate)=@Month and YEAR(jd.EffectiveDate)=@Year and cp.CalculationOn = 1 THEN CONVERT(DECIMAL(18,2),  (sh.Salary*cp.EmployerContribution)/100) --First Month calculation

				                                 --WHEN cp.EmploymentStatus =  2  and cp.CalculationOn = 1 THEN CONVERT(DECIMAL(18,2), (((organization.BasicSalary*sh.Salary)/100)*cp.EmployerContribution)/100)

				                                 WHEN cp.EmploymentStatus =  2  and cp.CalculationOn = 2 THEN CONVERT(DECIMAL(18,2), (sh.Salary*cp.EmployerContribution)/100)
				                         
				                                 ELSE 0
			                                  END
			                                ,0) as EbfForEmplyer
			                                from  [HR_TeamMember] AS hrm 
			                                  Left Join (";
                    if (viewModel.OrganizationType == OrganizationType.JobOrganization)
                    {
                        query += @"
									                    ------ Job Organization
											            select c.* from (
						                                select a.*,b.CalculationOn,b.EbfSettingsId,b.EmployeeContribution,b.EmployerContribution from (
							                                Select * from (
									                                Select 
										                                o.[Id] as OrganizationId
										                                ,b.[Id] as BranchId
										                                ,eh.[CampusId]
										                                ,eh.[DesignationId]
										                                ,eh.[DepartmentId]
										                                ,eh.[TeamMemberId]
										                                ,eh.[EffectiveDate]
										                                ,eh.[EmploymentStatus]
										                                ,RANK() OVER(PARTITION BY eh.TeamMemberId ORDER BY eh.EffectiveDate DESC, eh.Id DESC) AS R  
										                                from [HR_EmploymentHistory] as eh
										                                inner join Campus as c on c.Id = eh.CampusId  and c.Status = 1
										                                inner join Branch as b on b.Id = c.BranchId and b.Status = 1
										                                inner join Organization as o on o.Id = b.OrganizationId and o.Status = 1
										                                Where eh.Status = 1
										                                and eh.EffectiveDate <= '" + endDate + @"'
							                                ) as a where a.R=1  and a.EmploymentStatus != 6
                                                            and a.OrganizationId = " + viewModel.OrganizationId;
                        if (authBranchIdList != null)
                        {
                            query += @" and a.BranchId IN(" + string.Join(",", authBranchIdList) + ")";
                        }
                        if (viewModel.CampusId != 0)
                        {
                            query += @" and a.CampusId =" + viewModel.CampusId + "";
                        }
                        if (viewModel.DepartmentId != 0)
                        {
                            query += @" and a.DepartmentId =" + viewModel.DepartmentId + "";
                        }
                        query += @"  ) as a 
						                                left join (select * from ebf)  as b on a.EmploymentStatus = b.EmploymentStatus
					                                ) as c	";

                    }
                    else
                    {
                        query += @"
											        ----- Salary Organization
					                                select c.* from (
						                                select a.*,b.CalculationOn,b.EbfSettingsId,b.EmployeeContribution,b.EmployerContribution from (
                                                                select 
									                                a.TeamMemberId
									                                ,a.SalaryOrganizationId as OrganizationId
									                                ,a.BranchId as BranchId
									                                ,a.SalaryCampusId as CampusId
									                                ,a.SalaryDepartmentId as DepartmentId
									                                ,a.SalaryDesignationId as DesignationId
									                                ,a.EffectiveDate
									                                ,b.EmploymentStatus 
								                                from (
								                                Select 
								                                    sh.[SalaryOrganizationId]
								                                    ,b.[Id] as BranchId
								                                    ,sh.[SalaryCampusId]
								                                    ,sh.[SalaryDepartmentId]
								                                    ,sh.[SalaryDesignationId]
								                                    ,sh.[TeamMemberId]
								                                    ,sh.[EffectiveDate]
								                                    ,RANK() OVER(PARTITION BY sh.TeamMemberId ORDER BY sh.EffectiveDate DESC, sh.Id DESC) AS R
								                                from HR_SalaryHistory   as sh
								                                inner join Campus as c on c.Id = sh.SalaryCampusId  and c.Status = 1
								                                inner join Branch as b on b.Id = c.BranchId and b.Status = 1
								                                where sh.Status = 1 and sh.EffectiveDate<= '" + endDate + @"' 
								                                ) as a 
								                                left join(
								                                Select * from (
										                                Select 
											                                o.[Id] as OrganizationId
											                                ,b.[Id] as BranchId			
											                                ,eh.[TeamMemberId]			
											                                ,eh.[EmploymentStatus]
											                                ,RANK() OVER(PARTITION BY eh.TeamMemberId ORDER BY eh.EffectiveDate DESC, eh.Id DESC) AS R  
											                                from [HR_EmploymentHistory] as eh
											                                inner join Campus as c on c.Id = eh.CampusId  and c.Status = 1
											                                inner join Branch as b on b.Id = c.BranchId and b.Status = 1
											                                inner join Organization as o on o.Id = b.OrganizationId and o.Status = 1
											                                Where eh.Status = 1
											                                and eh.EffectiveDate <= '" + endDate + @"'
								                                ) as a where a.R=1  and a.EmploymentStatus != 6 ";
                        query+=@"
                                                        
								                                ) as b on b.TeamMemberId = a.TeamMemberId
								                                where a.R=1  and a.SalaryOrganizationId="+viewModel.OrganizationId;
                        if (authBranchIdList != null)
                        {
                            query += @" and a.BranchId IN(" + string.Join(",", authBranchIdList) + ")";
                        }
                        if (viewModel.CampusId != 0)
                        {
                            query += @" and a.SalaryCampusId =" + viewModel.CampusId + "";
                        }
                        if (viewModel.DepartmentId != 0)
                        {
                            query += @" and a.SalaryDepartmentId =" + viewModel.DepartmentId + "";
                        }
                        query+=@"                                   
						                                ) as a 
						                                left join (select * from ebf)  as b on a.EmploymentStatus = b.EmploymentStatus
					                                ) as c		";
                    }
		                        query+=@"
			                                   ) as cp on cp.TeamMemberId = hrm.Id and hrm.Status = 1  and hrm.IsSalarySheet = 1
			                                   left join [Campus] as campus on campus.Id = cp.CampusId and campus.Status = 1
			                                   left join [Branch] as branch on branch.Id = campus.BranchId and branch.Status = 1 --and branch.Id = 68
			                                   left join [Organization] as organization on organization.Id = branch.OrganizationId and organization.Status = 1 and organization.Id="+viewModel.OrganizationId+@"
			                                   left join [HR_Designation] as designation on designation.Id = cp.DesignationId and designation.Status = 1
			                                   left Join ( -- Salary History
					                                select * from (
						                                Select  sh.[Id] as SalaryHistoryId 
                                                            ,sh.[salaryOrganizationId]
						                                    ,b.[Id] as SalaryBranchId						     
							                                ,sh.[SalaryCampusId]
							                                ,sh.[SalaryDepartmentId]
							                                ,sh.[SalaryDesignationId]
							                                ,sh.[TeamMemberId]
							                                ,sh.[EffectiveDate]
							                                ,sh.[Salary]
							                                ,sh.[BankAmount]
							                                ,sh.[CashAmount]
							                                ,RANK() OVER(PARTITION BY sh.TeamMemberId ORDER BY sh.EffectiveDate DESC, sh.Id DESC) AS R  
							                                from [HR_SalaryHistory] as sh
							                                inner join Campus as c on c.Id = sh.SalaryCampusId and c.Status = 1
							                                inner join Branch as b on b.Id = c.BranchId  and b.Status = 1
							                                Where sh.Status = 1
							                                and sh.EffectiveDate <= '" + endDate + @"' 
					                                ) as a where a.R = 1
				                                ) as sh on sh.TeamMemberId = hrm.Id and hrm.Status=1 and hrm.IsSalarySheet = 1 
				                                left join( -- Zone Count
				                                  select a.zoneSettingsId
				                                   ,a.TeamMemberId	   	   
				                                   ,ISNULL(
						                                CASE 
						                                  WHEN (a.TotalToleranceDay - a.ToleranceDay)>=0 THEN (a.TotalToleranceDay - a.ToleranceDay)
						                                  ELSE 0
						                                  END
					                                 ,0) as ZoneCount	    
					                                ,ISNULL(CASE 
						                                WHEN a.TotalToleranceDay>a.ToleranceDay THEN CONVERT(DECIMAL(18,2),(a.TotalToleranceDay-a.ToleranceDay)*a.DeductionMultiplier)
						                                else 0
						                                END
					                                ,0)
					                                 as ZoneDeductionMultiplier
											         from (
													        select count(ISNULL(a.ToleranceDay,0)) as TotalToleranceDay
														        ,ISNULL(a.ToleranceDay,0) as ToleranceDay
														        ,CONVERT(DECIMAL(18,2),a.DeductionMultiplier) as DeductionMultiplier
														        ,a.TeamMemberId,a.Id as ZoneSettingsId
													        from(
														        select zs.Id,zs.ToleranceDay,zs.ToleranceTime, zs.DeductionMultiplier,ats.TeamMemberId
														        from HR_ZoneSetting as zs
														        inner join HR_AttendanceSummary as ats on ats.ZoneId=zs.Id
														        where zs.OrganizationId="+viewModel.OrganizationId+@" and 
														        (ats.AttendanceDate>='" + employmentHistory.EffectiveDate + @"' and ats.AttendanceDate<='" + endDate + @"')
														        and zs.DeductionMultiplier is not null
														        and zs.Status = 1
														        and ats.Status = 1   
														        and ats.TeamMemberId = " + employmentHistory.TeamMember.Id + @"
													        ) as a group by a.TeamMemberId, a.Id,a.DeductionMultiplier,a.ToleranceDay
													        --order by a.TeamMemberId
											        ) as a
				                                ) as zonededuction on zonededuction.TeamMemberId = hrm.Id
				                        
				                                left join( -- leave Without Pay
					                                select la.TeamMemberId,la.TotalLeaveDay from HR_LeaveApplication as la
					                                inner join HR_Leave as l on la.LeaveId = l.Id
					                                where l.PayType = 0 and l.OrganizationId = "+viewModel.OrganizationId+@" and la.LeaveStatus = 2 and
												        (la.DateFrom>='" + employmentHistory.EffectiveDate + @"' and la.DateTo<='" + endDate + @"')
												        and la.Status = 1
												        and l.Status = 1
				                                ) as lwp on lwp.TeamMemberId = hrm.Id
				                                left join(-- Loan Amount
					                                select Loan.TeamMemberId --,Loan.LoanApprovedAmount,Loan.MonthlyRefund,Loan.RefundAmount 
					                                ,CASE 
						                                WHEN (Loan.LoanApprovedAmount-Loan.RefundAmount)>=Loan.MonthlyRefund THEN Loan.MonthlyRefund
						                                else Loan.LoanApprovedAmount-Loan.RefundAmount
					                                 END AS Loan
											         ,ISNULL(Loan.RefundAmount,0) AS RefundAmount
					                                from (
						                                select ml.TeamMemberId,ml.LoanApprovedAmount,ml.MonthlyRefund,ISNULL(tmlr.RefundAmount,0) as RefundAmount from (
							                                select Loan.TeamMemberId,Loan.MonthlyRefund,mLoan.LoanApprovedAmount from (
								                                select mLoan.TeamMemberId,SUM(mLoan.LoanApprovedAmount) as LoanApprovedAmount
									                                from (
										                                select ml.LoanApprovedAmount,ml.MonthlyRefund,mla.TeamMemberId 
										                                from HR_MemberLoan as ml
										                                inner join HR_MemberLoanApplication as mla on ml.MemberLoanApplicationId = mla.Id
										                                where mla.TeamMemberId = " + employmentHistory.TeamMember.Id + @" and
																        ml.RefundStart<='" + endDate + @"'		
										                                and ml.Status = 1
										                                and mla.Status = 1
									                                ) as mLoan 
									                                group by mLoan.TeamMemberId
								                                ) as mLoan
								                                inner join(
								                                select mLRefund.MonthlyRefund,mLRefund.TeamMemberId from (
										                                select ml.MonthlyRefund,mla.TeamMemberId
											                                ,RANK() OVER(PARTITION BY mla.TeamMemberId ORDER BY ml.RefundStart DESC,ml.Id DESC) AS R 
											                                from HR_MemberLoan as ml
											                                inner join HR_MemberLoanApplication as mla on ml.MemberLoanApplicationId = mla.Id
											                                where ml.RefundStart<='" + endDate + @"' 
											                                and	  ml.Status = 1
											                                and   mla.Status = 1
									                                ) as mLRefund where mLRefund.R = 1
								                                ) as Loan on mLoan.TeamMemberId = Loan.TeamMemberId
								                                inner join(
									                                select tm.TmMemberId,tm.CampusId,tm.DesignationId,tm.DepartmentId from (
										                                select eh.CampusId,eh.DesignationId,eh.DepartmentId,tm.Id as TmMemberId
										                                ,RANK() OVER(PARTITION BY eh.TeamMemberId ORDER BY eh.EffectiveDate DESC,eh.Id DESC) AS R  
										                                from HR_EmploymentHistory as eh
										                                inner join HR_TeamMember as tm on tm.Id = eh.TeamMemberId
										                                inner join HR_Department as dprt on dprt.Id = eh.DepartmentId
										                                where dprt.OrganizationId = "+viewModel.OrganizationId+@" 
                                                                      --and dprt.Id = 117 
                                                                      --and eh.CampusId = 117
										                                and eh.Status = 1
										                                and tm.Status = 1
										                                and dprt.Status = 1
									                                ) as tm where tm.R = 1
								                                ) as tm on tm.TmMemberId = Loan.TeamMemberId
						                                ) as ml
						                                left join(--- Loan Refund Amount
							                                select * from (
							                                select mlr.TeamMemberId,sum(mlr.RefundAmount) as RefundAmount from HR_MemberLoanRefund as mlr 
								                                where mlr.RefundDate<='" + endDate + @"' 
									                                --and mlr.SalaryOrganizationId = 5
									                                --and mlr.SalaryBranchId = 107 
									                                --and mlr.SalaryCampusId = 124
								                                group by mlr.TeamMemberId
							                                ) as a
						                                ) as tmlr on ml.TeamMemberId =  tmlr.TeamMemberId
					                                ) as Loan
	
				                                ) as Loan on Loan.TeamMemberId = hrm.Id
				                                left join(-- TDS Amount
					                                select tds.TeamMemberId,tds.TdsAmount from(
					                                select tds.TeamMemberId,tds.TdsAmount ,RANK() OVER(PARTITION BY tds.TeamMemberId ORDER BY tds.EffectiveDate DESC, tds.Id DESC) AS R  
										                                from HR_TdsHistory as tds
										                                Where tds.Status = 1
										                                and tds.EffectiveDate <= '" + endDate + @"'
					                                ) as tds where tds.R = 1	
				                                ) as tds on tds.TeamMemberId = hrm.Id	
				                                left join(	-- Absent Count
						                                Select absentOFDay.TeamMemberId
												        ,ISNULL( 
													        CASE 
													        WHEN @TotaldaysOfMonthFromJoinDate >= absentOFDay.attendant THEN (@TotaldaysOfMonthFromJoinDate - absentOFDay.attendant)
													        else 0
													        END,0) as Absent 
													        ,(@TotalDaysOFMonth - @TotaldaysOfMonthFromJoinDate) as AbsentDaysCountBeforeJoiningDate 
												        FROM (
													        select totalhWL.TeamMemberId,SUM(ISNULL(totalhWL.TotalHWL,0)+ISNULL(ra.RegularAttendance,0)+ISNULL(dyAdjustment.totalDayOfAdjustment,0)) as attendant from (
															        select a.TeamMemberId,count(*) as TotalHWL from (
																        -- Weekend
																	        select a.TeamMemberId,DateOfMonth.FromDate from ( 
																		        select * from(
																			        select swh.TeamMemberId,
																				        CASE 
																					        WHEN swh.Weekend = 1 THEN 'Saturday' 
																					        WHEN swh.Weekend = 2 THEN 'Sunday'
																					        WHEN swh.Weekend = 3 THEN 'Monday'
																					        WHEN swh.Weekend = 4 THEN 'Tuesday'
																					        WHEN swh.Weekend = 5 THEN 'Wednesday'
																					        WHEN swh.Weekend = 6 THEN 'Thursday'
																					        WHEN swh.Weekend = 7 THEN 'Friday'
																					        ELSE ''
																				        END AS Weekend 
																				        from (
																					        select *,
																					        RANK() OVER(PARTITION BY swh.TeamMemberId ORDER BY swh.EffectiveDate DESC, swh.Id DESC) AS R  
																					        from HR_ShiftWeekendHistory as swh
																									        where swh.EffectiveDate<='" + endDate + @"'
																										        and swh.Status = 1 	
																										        and swh.OrganizationId = "+viewModel.OrganizationId+@"
																										        and swh.TeamMemberId = "+employmentHistory.TeamMember.Id+@"
																				        ) as swh where swh.R = 1
																		        ) as a group by a.TeamMemberId,a.Weekend
																	        ) as a
																	        INNER JOIN(select * from cte WHERE FromDate>='" + employmentHistory.EffectiveDate + @"' and FromDate<='" + endDate + @"') as DateOfMonth
																	        on DateOfMonth.Dayname = a.Weekend
																        --OPTION (MaxRecursion 370)
															        Union  ---- Holiday Date
																        select a.TeamMemberId,b.Dates as FromDate from (
																	        Select swh.OrganizationId,swh.TeamMemberId from HR_ShiftWeekendHistory as swh
																	        where swh.EffectiveDate<='" + endDate + @"'
																		        and swh.Status = 1 	
																		        and swh.OrganizationId = "+viewModel.OrganizationId+@"
																		        and swh.TeamMemberId = "+employmentHistory.TeamMember.Id+@"
																		        group by swh.OrganizationId,swh.TeamMemberId
																	        ) as a
																	        inner join(
																	        select --row_number() over(order by D.Dates) as SN,
																		        D.Dates,t.OrganizationId
																	        from (
																	        select hs.DateFrom  ,hs.DateTo,hs.OrganizationId 
																	        from HR_HolidaySetting hs 
																		        where (hs.DateFrom>='" + employmentHistory.EffectiveDate + @"' and hs.DateTo<='" + endDate + @"') and hs.Status = 1
																			        and hs.OrganizationId = " + viewModel.OrganizationId + @"
																	        ) as T
																		        inner join master..spt_values as N
																		        on N.number between 0 and datediff(day, T.DateFrom, T.DateTo)
																		        cross apply (select dateadd(day, N.number, T.DateFrom)) as D(Dates)
																	        where N.type ='P'
																	        ) as b on a.OrganizationId = b.OrganizationId

															        UNION -- Leave
																        select la.TeamMemberId,D.Dates as FromDate from (
																	        select 
																	        l.OrganizationId
																	        ,la.TeamMemberId 
																	        ,lad.DateFrom
																	        ,lad.DateTo
																	        from HR_LeaveApplication as la
																	        inner join HR_LeaveApplicationDetails as lad on la.Id = lad.LeaveApplicationId
																	        inner join HR_Leave as l on l.Id = la.LeaveId 
																	        where (lad.DateFrom>='" + employmentHistory.EffectiveDate + @"' and lad.DateTo<='" + endDate + @"') 
																	        and la.Status = 1 
																	        and l.OrganizationId = "+viewModel.OrganizationId+@"
																	        and la.TeamMemberId ="+employmentHistory.TeamMember.Id+ @"
																	        and la.LeaveStatus = 2
																        ) as la 
																        inner join master..spt_values as N
																	        on N.number between 0 and datediff(day, la.DateFrom, la.DateTo)
																	        cross apply (select dateadd(day, N.number, la.DateFrom)) as D(Dates)
																        where N.type ='P'
															        ) as a group by a.TeamMemberId
														        ) as totalhWL
														        Left join( ---Regular Attendance
																        select b.Weekend,b.TeamMemberId,count(b.RegularAttendance) as RegularAttendance,b.OrganizationId from (
																	        select * from (
																		        select a.Weekend,a.TeamMemberId,  --a.HolidayWorkDate,a.AttendanceDate,
																			        CASE 
																			        WHEN CAST(a.HolidayWorkDate AS DATE) IS NULL THEN a.AttendanceDate
																			        WHEN CAST(a.AttendanceDate as DATE) <> CAST(a.HolidayWorkDate AS DATE) THEN a.AttendanceDate   
																			        ELSE a.HolidayWorkDate
																			        END AS RegularAttendance
																			        ,a.OrganizationId
																			        from (
																				        select h.Weekend,h.TeamMemberId,h.HolidayWorkDate,atds.AttendanceDate,h.OrganizationId from 
																				        (
																					        select swh.Weekend,swh.TeamMemberId,hw.HolidayWorkDate,swh.OrganizationId from (
			                                                                                    select * from (
				                                                                                    select swh.Weekend,swh.TeamMemberId
				                                                                                    ,swh.OrganizationId,RANK() OVER(PARTITION BY swh.TeamMemberId ORDER BY swh.EffectiveDate DESC, swh.Id DESC) AS R   
				                                                                                    from HR_ShiftWeekendHistory as swh
			                                                                                    ) as swh where swh.R = 1
		                                                                                    ) as swh
		                                                                                    left join HR_HolidayWork as hw 
		                                                                                    on hw.TeamMemberId = swh.TeamMemberId and hw.HolidayWorkDate>='"+employmentHistory.EffectiveDate+@"' and hw.HolidayWorkDate<='"+endDate+@"'
		                                                                                    where swh.OrganizationId="+viewModel.OrganizationId+@" and swh.TeamMemberId = "+employmentHistory.TeamMember.Id+@"
																				        ) as h
																				        inner join (
																					        select ats.TeamMemberId,ats.AttendanceDate 
																					        from  HR_AttendanceSummary as ats 
																					        where (ats.AttendanceDate>='" + employmentHistory.EffectiveDate + @"' and ats.AttendanceDate<='"+endDate+@"') 
																						        and ats.Status = 1 and ats.IsWeekend = 0 and ats.HolidaySettingId is null
																				        ) as atds on atds.TeamMemberId = h.TeamMemberId
																				        where atds.TeamMemberId = "+employmentHistory.TeamMember.Id+@" --order by atds.AttendanceDate ASC 
																			        ) as a --where a.TeamMemberId = 728
																	        ) as c group by c.RegularAttendance,c.OrganizationId,c.TeamMemberId,c.Weekend
																        ) as b
																        group by b.TeamMemberId,b.Weekend,b.OrganizationId
														        ) as ra on ra.TeamMemberId = totalhWL.TeamMemberId
														        Left join( -- Dayoff Adjustment
															        select doa.TeamMemberId, count(*) as totalDayOfAdjustment from HR_DayOffAdjustment as doa
															        where doa.DayOffDate>='" + employmentHistory.EffectiveDate + @"' and doa.DayOffDate<='" + endDate + @"'
															        and  TeamMemberId = "+employmentHistory.TeamMember.Id+@"
															        group by doa.TeamMemberId
														        ) as dyAdjustment on dyAdjustment.TeamMemberId = totalhWL.TeamMemberId
														        group by totalhWL.TeamMemberId
												        ) as absentOFDay	                                            
				                                ) as absentDays on absentDays.TeamMemberId = hrm.Id
                                                left join( -- Joining date
											        -- Get first effective date of all organization(said that, nazmul vi)
											        Select a.TeamMemberId,a.EffectiveDate from (
												        Select 
													        o.[Id] as OrganizationId
													        ,b.[Id] as BranchId
													        ,eh.[CampusId]
													        ,eh.[DesignationId]
													        ,eh.[DepartmentId]
													        ,eh.[TeamMemberId]
													        ,eh.[EffectiveDate]
													        ,eh.[EmploymentStatus]
													        ,RANK() OVER(PARTITION BY eh.TeamMemberId ORDER BY eh.EffectiveDate ASC, eh.Id ASC) AS R  
													        from [HR_EmploymentHistory] as eh
													        inner join Campus as c on c.Id = eh.CampusId  and c.Status = 1
													        inner join Branch as b on b.Id = c.BranchId and b.Status = 1
													        inner join Organization as o on o.Id = b.OrganizationId and o.Status = 1 -- and o.Id = 5
													        Where eh.Status = 1
											        ) as a where a.R=1  
											        and a.EmploymentStatus != 6  and a.OrganizationId IN (" + viewModel.OrganizationId + @") 
											        and a.EffectiveDate >= '" + startDate + @"' and a.EffectiveDate <= '" + endDate + @"'
											        and a.TeamMemberId = "+employmentHistory.TeamMember.Id+@"
							                   ) as jd on jd.TeamMemberId = hrm.Id
			                                   where cp.TeamMemberId is not null and sh.SalaryHistoryId is not null and cp.TeamMemberId="+employmentHistory.TeamMember.Id+ @"
                                        ) as d 
										    group by d.TeamMemberId,d.Name,d.Pin,d.IsAutoDeduction,d.designation,d.SalaryHistoryId,d.BankAmount,d.CashAmount,d.OrganizationId,d.BranchId,d.CampusId
										    ,d.DepartmentId,d.DesignationId,d.SalaryOrganizationId,d.SalaryBranchId,d.SalaryCampusId,d.SalaryDepartmentId,d.SalaryDesignationId,d.DeductionPerAbsent
										    ,d.TotalAbsentDay,d.TotalSalaryWithoutDeduction,d.BasicSalary,d.HouseRent,d.Conveyance,d.Medical,d.GrossSalary,d.Increament,d.Arrear,d.AbsentAmount
										    ,d.AbsentMultiplierFactor,d.Loan,d.TotalRefundAmount,d.TdsAmount,d.Ebf,d.EbfForEmplyer
                                            ,d.EbfSettingsId,d.CalculationOn,d.EmployeeContribution,d.EmployerContribution,d.EmploymentStatus
		                        ) as d   
	                        ) as e
                        ) as f
                        OPTION (MaxRecursion 370)  ";

            var list = Session.CreateSQLQuery(query).SetResultTransformer(Transformers.AliasToBean<SalarySheetDto>());
            return list.List<SalarySheetDto>().ToList().FirstOrDefault();
        }

        #endregion

        #region List Loading Function

        public List<SalarySheetDto> LoadTeamMemberForSalarySheet(List<long> authOrganizationIdList, List<long> authBranchIdList, int totalDaysOfMonth,
            DateTime startDate, DateTime endDate, SalarySheetFormViewModel viewModel)
        {
            string query = @" 
                        DECLARE @Year AS INT,@TotalWeekendOfMonth AS INT,@TotalDays AS INT,
                        @FirstDateOfYear DATETIME,
                        @LastDateOfYear DATETIME,
                        @Month AS INT
                        SELECT @year = " + startDate.Year + @"
                        SELECT @FirstDateOfYear = DATEADD(yyyy, @Year - 1900, 0)
                        SELECT @LastDateOfYear = DATEADD(yyyy, @Year - 1900 + 1, 0)
                        SELECT @TotalDays = " + totalDaysOfMonth + @"
                        Select @Month = " + startDate.Month + @"
                        --wEEKND
                        ;WITH cte AS (
	                        SELECT 1 AS DayID,
	                        @FirstDateOfYear AS FromDate,
	                        DATENAME(dw, @FirstDateOfYear) AS Dayname
	                        UNION ALL
	                        SELECT cte.DayID + 1 AS DayID,
	                        DATEADD(d, 1 ,cte.FromDate),
	                        DATENAME(dw, DATEADD(d, 1 ,cte.FromDate)) AS Dayname
	                        FROM cte
	                        WHERE DATEADD(d,1,cte.FromDate) < @LastDateOfYear
                        ),
                        memberWeekend AS(
                          SELECT * FROM(
	                        SELECT DayName,count(DayName) AS TotalWeek FROM CTE WHERE FromDate>='" + startDate +
                           @"' and FromDate<='" + endDate + @"' 
	                        group by DayName
	                        ) as a
                        ),           

                        -- EBF Settings
                        ebf AS(
	                        select * from (
								select ebfs.Id as EbfSettingsId,ebfs.EmploymentStatus,ebfs.CalculationOn,ebfs.EmployeeContribution,ebfs.EmployerContribution,ebfs.EffectiveDate,ebfs.ClosingDate 
								from (
									select ebfs.Id ,ebfs.OrganizationId,ebfs.EffectiveDate,ebfs.ClosingDate
									,ebfs.EmploymentStatus,ebfs.CalculationOn,ebfs.EmployeeContribution,ebfs.EmployerContribution 
									,RANK() OVER(PARTITION BY ebfs.EmploymentStatus ORDER BY ebfs.EffectiveDate DESC,ebfs.Id DESC) AS R  
									from HR_EmployeeBenefitsFundSetting as ebfs
									where ebfs.OrganizationId = " + viewModel.OrganizationId+@" and 
									ebfs.EffectiveDate<='"+startDate+@"' and (ebfs.ClosingDate is null  or ebfs.ClosingDate>='"+endDate+ @"') 
									and ebfs.Status = 1
								) as ebfs where ebfs.R = 1
								) as a --order by a.EffectiveDate desc
                        )

                        -- Main Query
                        Select 
                        f.TeamMemberId as TeamMemberId
                        ,f.SalaryHistoryId
                        ,f.SalaryOrganizationId
                        ,f.SalaryBranchId
                        ,f.SalaryCampusId
                        ,f.SalaryDepartmentId
                        ,f.SalaryDesignationId
                        ,f.OrganizationId
                        ,f.BranchId
                        ,f.CampusId
                        ,f.DepartmentId
                        ,f.DesignationId
                        ,f.Name
                        ,f.Pin                       
                        ,f.designation as Designation
                        ,f.BasicSalary
                        ,f.HouseRent
                        ,f.Conveyance
                        ,f.Medical
                        ,f.GrossSalary
                        ,f.Increament
                        ,f.Arrear
                        ,f.EmploymentStatus
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.ZoneAmount
						   ELSE 0.00
						  END AS ZoneAmount                
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.ZoneAmount
						   ELSE 0.00
						  END AS FinalZoneAmount
                        ,ISNULL(                            
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN CONVERT(DECIMAL(18,2),f.AbsentAmount)
						   ELSE 0.00
						  END,0
                         ) AS AbsentAmount
                        
                        ,ISNULL(                            
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN CONVERT(DECIMAL(18,2),f.AbsentAmount)
						   ELSE 0.00
						  END,0
                         ) AS FinalAbsentAmount                        
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.LeaveWithoutPayAmount
						   ELSE 0.00
						  END AS LeaveWithoutPayAmount
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.Loan
						   ELSE 0.00
						  END AS Loan
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.TotalRefundAmount
						   ELSE 0.00
						  END AS FinalLoanRefund
                        ,
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN f.TdsAmount
						   ELSE 0.00
						  END AS TdsAmount
                         ,ISNULL(
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN CONVERT(DECIMAL(18,2),f.Ebf)
						   ELSE 0.00
						  END,0) AS Ebf
                         ,ISNULL(
						  CASE 
						   WHEN f.IsAutoDeduction=1 THEN CONVERT(DECIMAL(18,2),f.EbfForEmplyer)
						   ELSE 0.00
						  END,0) AS EbfForEmplyer
                        ,f.Bank
                        ,f.Cash
                        --,f.ZoneSettingsId
                        --,ISNULL(f.ZoneCount,0) AS ZoneCount
                        --,ISNULL(CONVERT(DECIMAL(18,2),f.ZoneDeductionMultiplier),0) AS ZoneDeductionMultiplier
                        --,ISNULL(CONVERT(DECIMAL(18,2),f.ZoneUnitAmount),0) AS ZoneUnitAmount
                        ,(f.Bank + f.Cash) as TotalAmount
                        ,f.DeductionPerAbsent
                        ,f.TotalAbsentDay
                        ,f.AbsentMultiplierFactor
                        ,f.EbfSettingsId
                        ,ISNULL(CONVERT(DECIMAL(18,2),f.CalculationOn),0) AS CalculationOn
                        ,ISNULL(CONVERT(DECIMAL(18,2),f.EmployeeContribution),0) AS EmployeeContribution
                        ,ISNULL(CONVERT(DECIMAL(18,2),f.EmployerContribution),0) AS EmployerContribution
                        ,ISNULL(CONVERT(DECIMAL(18,2),f.TotalSalaryWithoutDeduction),0) AS TotalSalaryWithoutDeduction
                        ,'' as Remarks
                        from (
	                        Select e.*,
	                        ISNULL(
		                        CASE 
			                        WHEN (e.CashAmount-e.deduction)>=0 THEN (e.CashAmount-e.deduction)		
			                        ELSE 0
		                        END
	                        ,0) as Cash
	                        ,ISNULL(
		                        CASE 
			                        WHEN (e.CashAmount-e.deduction)<=0 THEN (e.BankAmount-(e.deduction-e.CashAmount))

			                        WHEN (e.CashAmount-e.deduction)>=0 THEN e.BankAmount		
			                        ELSE 0
		                        END
	                        ,0) as Bank  
	                          FROM (
		                        Select d.*, (d.ZoneAmount + d.AbsentAmount + d.LeaveWithoutPayAmount + d.Loan + d.TdsAmount + d.Ebf) as deduction
		                         from (
                                      Select 
											d.TeamMemberId,d.Name,d.Pin,d.IsAutoDeduction,d.designation,d.SalaryHistoryId,d.BankAmount,d.CashAmount,d.OrganizationId,d.BranchId,d.CampusId
											,d.DepartmentId,d.DesignationId,d.SalaryOrganizationId,d.SalaryBranchId,d.SalaryCampusId,d.SalaryDepartmentId,d.SalaryDesignationId,d.DeductionPerAbsent,d.TotalAbsentDay
											,d.TotalSalaryWithoutDeduction,d.BasicSalary,d.HouseRent,d.Conveyance,d.Medical,d.GrossSalary,d.Increament,d.Arrear,d.AbsentAmount,d.AbsentMultiplierFactor
											,SUM(d.LeaveWithoutPayAmount) as LeaveWithoutPayAmount,d.Loan,d.TotalRefundAmount,d.TdsAmount,d.Ebf,d.EbfForEmplyer,SUM(d.ZoneAmount) as ZoneAmount
                                            ,d.EbfSettingsId,d.CalculationOn,d.EmployeeContribution,d.EmployerContribution,d.EmploymentStatus
									 from (
			                                Select hrm.Id as TeamMemberId,hrm.FullNameEng as Name,hrm.Pin as Pin,hrm.IsAutoDeduction,designation.Name as designation--, cp.EmploymentStatus as EmploymentStatusInt
			                                ,sh.SalaryHistoryId
                                            ,sh.BankAmount
			                                ,sh.CashAmount
			                                ,cp.OrganizationId
			                                ,cp.BranchId
			                                ,cp.CampusId
			                                ,cp.DepartmentId
			                                ,cp.DesignationId
			                                ,cp.EbfSettingsId
			                                ,cp.CalculationOn
			                                ,cp.EmployeeContribution
			                                ,cp.EmployerContribution
			                                ,sh.SalaryOrganizationId
			                                ,sh.SalaryBranchId
			                                ,sh.SalaryCampusId
			                                ,sh.SalaryDepartmentId
			                                ,sh.SalaryDesignationId
                                            ,cp.EmploymentStatus
			                                --,zonededuction.ZoneSettingsId
			                                --,zonededuction.ZoneCount
			                                --,zonededuction.ZoneDeductionMultiplier
			                                --,CONVERT(DECIMAL(18,2),(sh.Salary/organization.MonthlyWorkingDay)) as ZoneUnitAmount
			                                ,organization.DeductionPerAbsent
			                                ,ISNULL(absentDays.Absent,0) as TotalAbsentDay
                                            ,sh.Salary as TotalSalaryWithoutDeduction			                        
			                                ,ISNULL(
			                                CASE
			                                  WHEN cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Permanent +
                                   @" THEN CONVERT(DECIMAL(18,2), (organization.BasicSalary*sh.Salary)/100)
			                                  ELSE 0
			                                END,0) as BasicSalary

			                                ,ISNULL(
			                                CASE
			                                  WHEN cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Permanent +
                                   @" THEN CONVERT(DECIMAL(18,2), (organization.HouseRent*sh.Salary)/100)
			                                  ELSE 0
			                                END,0) as HouseRent
			                                ,ISNULL(
			                                CASE
			                                  WHEN cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Permanent +
                                   @" THEN CONVERT(DECIMAL(18,2), (organization.Convenyance*sh.Salary)/100)
			                                  ELSE 0
			                                END,0) as Conveyance
			                                ,ISNULL(
			                                CASE
			                                  WHEN cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Permanent +
                                   @" THEN CONVERT(DECIMAL(18,2), (organization.MedicalAllowance*sh.Salary)/100)
			                                  ELSE 0
			                                END,0) as Medical
			                                ,ISNULL( sh.Salary,0) as GrossSalary
			                                ,ISNULL(CONVERT(DECIMAL(18,2),0),0) as Increament
			                                ,ISNULL(CONVERT(DECIMAL(18,2),0),0) as Arrear
			                                ,ISNULL( 
			                                 CASE 
			                                   WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Probation +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.PartTime +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Contractual +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Intern +
                                   @" THEN CONVERT(DECIMAL(18,2),(sh.Salary/organization.MonthlyWorkingDay)*zonededuction.ZoneDeductionMultiplier)

                                               --WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent + " and jd.EffectiveDate='" + startDate.Date.ToString("yyyy-MM-dd 00:00:00.000") + "' " +
                                   @" THEN  CONVERT(DECIMAL(18,2),(sh.Salary/organization.MonthlyWorkingDay)*zonededuction.ZoneDeductionMultiplier) --First Month calculation

			                                   WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent +
                                   @" THEN  CONVERT(DECIMAL(18,2),(((organization.BasicSalary*sh.Salary)/100)/organization.MonthlyWorkingDay)*zonededuction.ZoneDeductionMultiplier)
			                           
                                                else 0
			                                   END,0) as ZoneAmount

			                                ,ISNULL(
				                                 CASE 
				                                 WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Probation +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.PartTime +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Contractual +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Intern +
                                   @" THEN CONVERT(DECIMAL(18,2),((sh.Salary/organization.MonthlyWorkingDay)*absentDays.Absent)*organization.DeductionPerAbsent)

                                                 --WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent + " and jd.EffectiveDate='" + startDate.Date.ToString("yyyy-MM-dd 00:00:00.000") + "' " +
                                   @" THEN CONVERT(DECIMAL(18,2),((sh.Salary*organization.DeductionPerAbsent)/100)*absentDays.Absent) --First Month calculation

				                                 WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent +
                                   @" THEN CONVERT(DECIMAL(18,2),((((organization.BasicSalary*sh.Salary)/100)/organization.MonthlyWorkingDay)*organization.DeductionPerAbsent)*absentDays.Absent)
				                                 ELSE 0
			                                  END
			                                ,0) as AbsentAmount
			                                ,ISNULL(
				                                 CASE 
				                                 WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Probation +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.PartTime +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Contractual +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Intern +
                                   @" THEN CONVERT(DECIMAL(18,2),((sh.Salary/organization.MonthlyWorkingDay)*organization.DeductionPerAbsent))

                                                 --WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent + " and jd.EffectiveDate='" + startDate.Date.ToString("yyyy-MM-dd 00:00:00.000") + "' " +
                                   @" THEN CONVERT(DECIMAL(18,2),(sh.Salary/organization.DeductionPerAbsent)) --First Month calculation

				                                 WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent +
                                   @" THEN CONVERT(DECIMAL(18,2),((((organization.BasicSalary*sh.Salary)/100)/organization.MonthlyWorkingDay)*organization.DeductionPerAbsent))
				                                 ELSE 0
			                                  END
			                                ,0) as AbsentMultiplierFactor
			                                ,ISNULL(
			                                 CASE

                                              --WHEN cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Permanent + " and jd.EffectiveDate='" + startDate.Date.ToString("yyyy-MM-dd 00:00:00.000") + "' " +
                                   @" THEN CONVERT(DECIMAL(18,2), (sh.Salary/organization.MonthlyWorkingDay*lwp.TotalLeaveDay)) --First Month calculation

			                                  WHEN cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Permanent +
                                   @" THEN CONVERT(DECIMAL(18,2), ((organization.BasicSalary*sh.Salary)/100)/organization.MonthlyWorkingDay*lwp.TotalLeaveDay)

			                                  ELSE CONVERT(DECIMAL(18,2),(sh.Salary/organization.MonthlyWorkingDay*lwp.TotalLeaveDay))
			                                END,0) as LeaveWithoutPayAmount
			                                ,ISNULL(Loan.Loan,0) as Loan
                                            ,ISNULL(Loan.RefundAmount,0) As TotalRefundAmount
			                                ,ISNULL(tds.TdsAmount,0) as TdsAmount
			                                ,ISNULL(
			                                CASE 
				                                 WHEN (cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Probation +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.PartTime +
                                   @" or cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Contractual +
                                   @" or cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Intern +
                                   @" )  and cp.CalculationOn = " + (int)CalculationOnTypeEnum.Basic +
                                   @" THEN CONVERT(DECIMAL(18,2), (((organization.BasicSalary*sh.Salary)/100)*cp.EmployeeContribution)/100)
				                                 WHEN (cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Probation +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.PartTime +
                                   @" or cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Contractual +
                                   @" or cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Intern +
                                   @" )  and cp.CalculationOn = " + (int)CalculationOnTypeEnum.Gross +
                                   @" THEN CONVERT(DECIMAL(18,2), (sh.Salary*cp.EmployeeContribution)/100)

                                                 --WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent +
                                   @" and cp.CalculationOn = " + (int)CalculationOnTypeEnum.Basic + " and jd.EffectiveDate='" + startDate.Date.ToString("yyyy-MM-dd 00:00:00.000") + "' " +
                                   @" THEN CONVERT(DECIMAL(18,2), (sh.Salary*cp.EmployeeContribution)/100) --First Month calculation

				                                 WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent +
                                   @" and cp.CalculationOn = " + (int)CalculationOnTypeEnum.Basic +
                                   @" THEN CONVERT(DECIMAL(18,2), (((organization.BasicSalary*sh.Salary)/100)*cp.EmployeeContribution)/100)

				                                 WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent +
                                   @" and cp.CalculationOn = " + (int)CalculationOnTypeEnum.Gross +
                                   @" THEN CONVERT(DECIMAL(18,2), (sh.Salary*cp.EmployeeContribution)/100)
				                        
				                                 ELSE 0
			                                  END
			                                ,0) as Ebf
			                                ,ISNULL(
			                                CASE 
				                                 WHEN (cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Probation +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.PartTime +
                                   @" or cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Contractual +
                                   @" or cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Intern +
                                   @" )  and cp.CalculationOn = 1 THEN CONVERT(DECIMAL(18,2), (((organization.BasicSalary*sh.Salary)/100)*cp.EmployerContribution)/100)
				                                 WHEN (cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Probation +
                                   @" or cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.PartTime +
                                   @" or cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Contractual +
                                   @" or cp.EmploymentStatus = " + (int)MemberEmploymentStatus.Intern +
                                   @" )  and cp.CalculationOn = 2 THEN CONVERT(DECIMAL(18,2), (sh.Salary*cp.EmployeeContribution)/100)

                                                 --WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent + " and jd.EffectiveDate='" + startDate.Date.ToString("yyyy-MM-dd 00:00:00.000") + "' " +
                                   @"  and cp.CalculationOn = " + (int)CalculationOnTypeEnum.Basic +
                                   @" THEN CONVERT(DECIMAL(18,2),  (sh.Salary*cp.EmployeeContribution)/100) --First Month calculation

				                                 WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent +
                                   @"  and cp.CalculationOn = " + (int)CalculationOnTypeEnum.Basic +
                                   @" THEN CONVERT(DECIMAL(18,2), (((organization.BasicSalary*sh.Salary)/100)*cp.EmployerContribution)/100)

				                                 WHEN cp.EmploymentStatus =  " + (int)MemberEmploymentStatus.Permanent +
                                   @"  and cp.CalculationOn = " + (int)CalculationOnTypeEnum.Gross +
                                   @" THEN CONVERT(DECIMAL(18,2), (sh.Salary*cp.EmployeeContribution)/100)
				                         
				                                 ELSE 0
			                                  END
			                                ,0) as EbfForEmplyer
			                                from  [HR_TeamMember] AS hrm 
			                                  Left Join (";
                    if (viewModel.OrganizationType == OrganizationType.JobOrganization)
                    {
                        query += @"
									                            ------ Job Organization
											                    select c.* from (
						                                        select a.*,b.CalculationOn,b.EbfSettingsId,b.EmployeeContribution,b.EmployerContribution from (
							                                        Select * from (
									                                        Select 
										                                        o.[Id] as OrganizationId
										                                        ,b.[Id] as BranchId
										                                        ,eh.[CampusId]
										                                        ,eh.[DesignationId]
										                                        ,eh.[DepartmentId]
										                                        ,eh.[TeamMemberId]
										                                        ,eh.[EffectiveDate]
										                                        ,eh.[EmploymentStatus]
										                                        ,RANK() OVER(PARTITION BY eh.TeamMemberId ORDER BY eh.EffectiveDate DESC, eh.Id DESC) AS R  
										                                        from [HR_EmploymentHistory] as eh
										                                        inner join Campus as c on c.Id = eh.CampusId  and c.Status = 1
										                                        inner join Branch as b on b.Id = c.BranchId and b.Status = 1
										                                        inner join Organization as o on o.Id = b.OrganizationId and o.Status = 1
										                                        Where eh.Status = 1
										                                        and eh.EffectiveDate <= '" + endDate + @"'
							                                        ) as a where a.R=1  and a.EmploymentStatus != 6
                                                                    and a.OrganizationId = " + viewModel.OrganizationId;
                        if (authBranchIdList != null)
                        {
                            query += @" and a.BranchId IN(" + string.Join(",", authBranchIdList) + ")";
                        }
                        if (viewModel.CampusId != 0)
                        {
                            query += @" and a.CampusId =" + viewModel.CampusId + "";
                        }
                        if (viewModel.DepartmentId != 0)
                        {
                            query += @" and a.DepartmentId =" + viewModel.DepartmentId + "";
                        }
                        query += @"  ) as a 
						                                        left join (select * from ebf)  as b on a.EmploymentStatus = b.EmploymentStatus
					                                        ) as c	";

                    }
                    else
                    {
                        query += @"
											                ----- Salary Organization
					                                        select c.* from (
						                                        select a.*,b.CalculationOn,b.EbfSettingsId,b.EmployeeContribution,b.EmployerContribution from (
                                                                        select 
									                                        a.TeamMemberId
									                                        ,a.SalaryOrganizationId as OrganizationId
									                                        ,a.BranchId as BranchId
									                                        ,a.SalaryCampusId as CampusId
									                                        ,a.SalaryDepartmentId as DepartmentId
									                                        ,a.SalaryDesignationId as DesignationId
									                                        ,a.EffectiveDate
									                                        ,b.EmploymentStatus 
								                                        from (
								                                        Select 
								                                            sh.[SalaryOrganizationId]
								                                            ,b.[Id] as BranchId
								                                            ,sh.[SalaryCampusId]
								                                            ,sh.[SalaryDepartmentId]
								                                            ,sh.[SalaryDesignationId]
								                                            ,sh.[TeamMemberId]
								                                            ,sh.[EffectiveDate]
								                                            ,RANK() OVER(PARTITION BY sh.TeamMemberId ORDER BY sh.EffectiveDate DESC, sh.Id DESC) AS R
								                                        from HR_SalaryHistory   as sh
								                                        inner join Campus as c on c.Id = sh.SalaryCampusId  and c.Status = 1
								                                        inner join Branch as b on b.Id = c.BranchId and b.Status = 1
								                                        where sh.Status = 1 and sh.EffectiveDate<= '" + endDate + @"' 
								                                        ) as a 
								                                        left join(
								                                        Select * from (
										                                        Select 
											                                        o.[Id] as OrganizationId
											                                        ,b.[Id] as BranchId			
											                                        ,eh.[TeamMemberId]			
											                                        ,eh.[EmploymentStatus]
											                                        ,RANK() OVER(PARTITION BY eh.TeamMemberId ORDER BY eh.EffectiveDate DESC, eh.Id DESC) AS R  
											                                        from [HR_EmploymentHistory] as eh
											                                        inner join Campus as c on c.Id = eh.CampusId  and c.Status = 1
											                                        inner join Branch as b on b.Id = c.BranchId and b.Status = 1
											                                        inner join Organization as o on o.Id = b.OrganizationId and o.Status = 1
											                                        Where eh.Status = 1
											                                        and eh.EffectiveDate <= '" + endDate + @"'
								                                        ) as a where a.R=1  and a.EmploymentStatus != 6 ";
                        query += @"
                                                        
								                                        ) as b on b.TeamMemberId = a.TeamMemberId
								                                        where a.R=1  and a.SalaryOrganizationId=" + viewModel.OrganizationId;
                        if (authBranchIdList != null)
                        {
                            query += @" and a.BranchId IN(" + string.Join(",", authBranchIdList) + ")";
                        }
                        if (viewModel.CampusId != 0)
                        {
                            query += @" and a.SalaryCampusId =" + viewModel.CampusId + "";
                        }
                        if (viewModel.DepartmentId != 0)
                        {
                            query += @" and a.SalaryDepartmentId =" + viewModel.DepartmentId + "";
                        }
                        query += @"                                   
						                                        ) as a 
						                                        left join (select * from ebf)  as b on a.EmploymentStatus = b.EmploymentStatus
					                                        ) as c		";
                    }

                    
		            query +=@"
			                                   ) as cp on cp.TeamMemberId = hrm.Id and hrm.Status = " + TeamMember.EntityStatus.Active +
                             @"  and hrm.IsSalarySheet = 1
			                                   left join [Campus] as campus on campus.Id = cp.CampusId and campus.Status = " +
                             Campus.EntityStatus.Active + @"
			                                   left join [Branch] as branch on branch.Id = campus.BranchId and branch.Status = " +
                             Branch.EntityStatus.Active + @" --and branch.Id = 68
			                                   left join [Organization] as organization on organization.Id = branch.OrganizationId and organization.Status = " +
                             Organization.EntityStatus.Active + @" and organization.Id=" + viewModel.OrganizationId + @"
			                                   left join [HR_Designation] as designation on designation.Id = cp.DesignationId and designation.Status = " +
                             Designation.EntityStatus.Active + @"
			                                   left Join (--Salary History
					                                select * from (
						                                Select  sh.[Id] as SalaryHistoryId 
                                                            ,sh.[salaryOrganizationId]
						                                    ,b.[Id] as SalaryBranchId						     
							                                ,sh.[SalaryCampusId]
							                                ,sh.[SalaryDepartmentId]
							                                ,sh.[SalaryDesignationId]
							                                ,sh.[TeamMemberId]
							                                ,sh.[EffectiveDate]
							                                ,sh.[Salary]
							                                ,sh.[BankAmount]
							                                ,sh.[CashAmount]
							                                ,RANK() OVER(PARTITION BY sh.TeamMemberId ORDER BY sh.EffectiveDate DESC, sh.Id DESC) AS R  
							                                from [HR_SalaryHistory] as sh
							                                inner join Campus as c on c.Id = sh.SalaryCampusId and c.Status = " +
                             Campus.EntityStatus.Active + @"
							                                inner join Branch as b on b.Id = c.BranchId  and b.Status = " +
                             Branch.EntityStatus.Active + @"
							                                Where sh.Status = " + SalaryHistory.EntityStatus.Active + @"
							                                and sh.EffectiveDate <= '" + endDate + @"' 
					                                ) as a where a.R = 1
				                                ) as sh on sh.TeamMemberId = hrm.Id and hrm.Status=" + TeamMember.EntityStatus.Active +
                             @" and hrm.IsSalarySheet = 1 
				                                left join(--Zone Count
				                                  select a.zoneSettingsId
				                                   ,a.TeamMemberId	   	   
				                                   ,ISNULL(
						                                CASE 
						                                  WHEN (a.TotalToleranceDay - a.ToleranceDay)>=0 THEN (a.TotalToleranceDay - a.ToleranceDay)
						                                  ELSE 0
						                                  END
					                                 ,0) as ZoneCount	    
					                                ,ISNULL(CASE 
						                                WHEN a.TotalToleranceDay>a.ToleranceDay THEN CONVERT(DECIMAL(18,2),(a.TotalToleranceDay-a.ToleranceDay)*a.DeductionMultiplier)
						                                else 0
						                                END
					                                ,0)
					                                 as ZoneDeductionMultiplier
				                                from (
						                                select count(ISNULL(a.ToleranceDay,0)) as TotalToleranceDay
							                                ,ISNULL(a.ToleranceDay,0) as ToleranceDay
							                                ,CONVERT(DECIMAL(18,2),a.DeductionMultiplier) as DeductionMultiplier
							                                ,a.TeamMemberId,a.Id as ZoneSettingsId
						                                from(
							                                select zs.Id,zs.ToleranceDay,zs.ToleranceTime, zs.DeductionMultiplier,ats.TeamMemberId
							                                from HR_ZoneSetting as zs
							                                inner join HR_AttendanceSummary as ats on ats.ZoneId=zs.Id
							                                where zs.OrganizationId=" + viewModel.OrganizationId + @" and 
							                                (ats.AttendanceDate>='" + startDate + "' and ats.AttendanceDate<='" + endDate + @"')
							                                and zs.DeductionMultiplier is not null
							                                and zs.Status = " + ZoneSetting.EntityStatus.Active + @"
							                                and ats.Status = " + AttendanceSummary.EntityStatus.Active + @"
   
						                                ) as a group by a.TeamMemberId, a.Id,a.DeductionMultiplier,a.ToleranceDay
						                                --order by a.TeamMemberId
					                                ) as a
				                                ) as zonededuction on zonededuction.TeamMemberId = hrm.Id				                        
				                                left join( -- leave Without Pay
					                                select la.TeamMemberId,la.TotalLeaveDay from HR_LeaveApplication as la
					                                inner join HR_Leave as l on la.LeaveId = l.Id
					                                where l.PayType = " + (int)PayType.WithoutPay + @" and l.OrganizationId = " +
                             viewModel.OrganizationId + @" and la.LeaveStatus = " + (int)LeaveStatus.Approved + @" and
					                                (la.DateFrom>='" + startDate + @"' and la.DateTo<='" + endDate + @"')
					                                and la.Status = " + LeaveApplication.EntityStatus.Active + @"
					                                and l.Status = " + Leave.EntityStatus.Active + @"
				                                ) as lwp on lwp.TeamMemberId = hrm.Id
				                                left join(-- Loan Amount
					                                select Loan.TeamMemberId --,Loan.LoanApprovedAmount,Loan.MonthlyRefund,Loan.RefundAmount 
					                                ,CASE 
						                                WHEN (Loan.LoanApprovedAmount-Loan.RefundAmount)>=Loan.MonthlyRefund THEN Loan.MonthlyRefund
						                                else Loan.LoanApprovedAmount-Loan.RefundAmount
					                                 END AS Loan
                                                    ,ISNULL(Loan.RefundAmount,0) AS RefundAmount
					                                from (
						                                select ml.TeamMemberId,ml.LoanApprovedAmount,ml.MonthlyRefund,ISNULL(tmlr.RefundAmount,0) as RefundAmount from (
							                                select Loan.TeamMemberId,Loan.MonthlyRefund,mLoan.LoanApprovedAmount from (
								                                select mLoan.TeamMemberId,SUM(mLoan.LoanApprovedAmount) as LoanApprovedAmount
									                                from (
										                                select ml.LoanApprovedAmount,ml.MonthlyRefund,mla.TeamMemberId 
										                                from HR_MemberLoan as ml
										                                inner join HR_MemberLoanApplication as mla on ml.MemberLoanApplicationId = mla.Id
										                                where ml.RefundStart<='" + endDate + @"'		
										                                and ml.Status = " + MemberLoan.EntityStatus.Active + @"
										                                and mla.Status = " + MemberLoanApplication.EntityStatus.Active + @"
									                                ) as mLoan 
									                                group by mLoan.TeamMemberId
								                                ) as mLoan
								                                inner join(
								                                select mLRefund.MonthlyRefund,mLRefund.TeamMemberId from (
										                                select ml.MonthlyRefund,mla.TeamMemberId
											                                ,RANK() OVER(PARTITION BY mla.TeamMemberId ORDER BY ml.RefundStart DESC,ml.Id DESC) AS R 
											                                from HR_MemberLoan as ml
											                                inner join HR_MemberLoanApplication as mla on ml.MemberLoanApplicationId = mla.Id
											                                where ml.RefundStart<='" + endDate + @"' 
											                                and	  ml.Status = " + MemberLoan.EntityStatus.Active + @"
											                                and   mla.Status = " + MemberLoanApplication.EntityStatus.Active + @"
									                                ) as mLRefund where mLRefund.R = 1
								                                ) as Loan on mLoan.TeamMemberId = Loan.TeamMemberId
								                                inner join(
									                                select tm.TmMemberId,tm.CampusId,tm.DesignationId,tm.DepartmentId from (
										                                select eh.CampusId,eh.DesignationId,eh.DepartmentId,tm.Id as TmMemberId
										                                ,RANK() OVER(PARTITION BY eh.TeamMemberId ORDER BY eh.EffectiveDate DESC,eh.Id DESC) AS R  
										                                from HR_EmploymentHistory as eh
										                                inner join HR_TeamMember as tm on tm.Id = eh.TeamMemberId
										                                inner join HR_Department as dprt on dprt.Id = eh.DepartmentId
										                                where dprt.OrganizationId = " + viewModel.OrganizationId + @" 
                                                                      --and dprt.Id = 117 
                                                                      --and eh.CampusId = 117
										                                and eh.Status = " + EmploymentHistory.EntityStatus.Active + @"
										                                and tm.Status = " + TeamMember.EntityStatus.Active + @"
										                                and dprt.Status = " + Department.EntityStatus.Active + @"
									                                ) as tm where tm.R = 1
								                                ) as tm on tm.TmMemberId = Loan.TeamMemberId
						                                ) as ml
						                                left join( --- Loan Refund Amount
							                                select * from (
							                                select mlr.TeamMemberId,sum(mlr.RefundAmount) as RefundAmount from HR_MemberLoanRefund as mlr 
								                                where mlr.RefundDate<='" + endDate + @"' 
									                                --and mlr.SalaryOrganizationId = 3
									                                --and mlr.SalaryBranchId = 107 
									                                --and mlr.SalaryCampusId = 124
								                                group by mlr.TeamMemberId
							                                ) as a
						                                ) as tmlr on ml.TeamMemberId =  tmlr.TeamMemberId
					                                ) as Loan	
				                                ) as Loan on Loan.TeamMemberId = hrm.Id
				                                left join(-- TDS Amount
					                                select tds.TeamMemberId,tds.TdsAmount from(
					                                select tds.TeamMemberId,tds.TdsAmount ,RANK() OVER(PARTITION BY tds.TeamMemberId ORDER BY tds.EffectiveDate DESC, tds.Id DESC) AS R  
										                                from HR_TdsHistory as tds
										                                Where tds.Status = " + TdsHistory.EntityStatus.Active + @"
										                                and tds.EffectiveDate <= '" + startDate + @"'
					                                ) as tds where tds.R = 1	
				                                ) as tds on tds.TeamMemberId = hrm.Id	
				                                left join(	-- Absent Count
						                                Select absentOFDay.TeamMemberId
						                                ,ISNULL( 
						                                 CASE 
						                                   WHEN @TotalDays >= absentOFDay.TotalWorkingDays THEN (@TotalDays - absentOFDay.TotalWorkingDays)
						                                   else 0
						                                 END,0) as Absent 
						                                FROM (
							                                select totalhWL.TeamMemberId,SUM(ISNULL(totalhWL.TotalHWL,0)+ISNULL(ra.RegularAttendance,0)+ISNULL(dyAdjustment.totalDayOfAdjustment,0)) as TotalWorkingDays from (
														        select a.TeamMemberId,count(*) as TotalHWL from (
															        select a.TeamMemberId,DateOfMonth.FromDate from (
																        select * from(
																	        select swh.TeamMemberId,
																		        CASE 
																			        WHEN swh.Weekend = 1 THEN 'Saturday' 
																			        WHEN swh.Weekend = 2 THEN 'Sunday'
																			        WHEN swh.Weekend = 3 THEN 'Monday'
																			        WHEN swh.Weekend = 4 THEN 'Tuesday'
																			        WHEN swh.Weekend = 5 THEN 'Wednesday'
																			        WHEN swh.Weekend = 6 THEN 'Thursday'
																			        WHEN swh.Weekend = 7 THEN 'Friday'
																			        ELSE ''
																		        END AS Weekend 
																		        from (
																		        select *,
																		        RANK() OVER(PARTITION BY swh.TeamMemberId ORDER BY swh.EffectiveDate DESC, swh.Id DESC) AS R  
																		        from HR_ShiftWeekendHistory as swh
																						        where swh.EffectiveDate<='" + endDate + @"'
																							        and swh.Status = 1 	
																							        and swh.OrganizationId = " + viewModel.OrganizationId + @"
																							        --and swh.TeamMemberId = 3
																		        ) as swh where swh.R = 1
																        ) as a group by a.TeamMemberId,a.Weekend
															        ) as a
															        INNER JOIN(select * from cte WHERE FromDate>='" + startDate + @"' and FromDate<='" + endDate + @"') as DateOfMonth
															        on DateOfMonth.Dayname = a.Weekend	
															        --OPTION (MaxRecursion 370)
														        Union ---- Holiday Date
														        select a.TeamMemberId,b.Dates as FromDate from (
															        Select swh.OrganizationId,swh.TeamMemberId from HR_ShiftWeekendHistory as swh
															        where swh.EffectiveDate<='" + endDate + @"'
																        and swh.Status = 1 	
																        and swh.OrganizationId = " + viewModel.OrganizationId + @"
																        --and swh.TeamMemberId = 6
																        group by swh.OrganizationId,swh.TeamMemberId
															        ) as a
															        inner join(
															        select --row_number() over(order by D.Dates) as SN,
																        D.Dates,t.OrganizationId
															        from (
															        select hs.DateFrom  ,hs.DateTo,hs.OrganizationId 
															        from HR_HolidaySetting hs 
																        where (hs.DateFrom>='" + startDate + @"' and hs.DateTo<='" + endDate + @"') and hs.Status = 1
																	        and hs.OrganizationId = " + viewModel.OrganizationId + @"
															        ) as T
																        inner join master..spt_values as N
																        on N.number between 0 and datediff(day, T.DateFrom, T.DateTo)
																        cross apply (select dateadd(day, N.number, T.DateFrom)) as D(Dates)
															        where N.type ='P'
															        ) as b on a.OrganizationId = b.OrganizationId

														        UNION -- Leave
														        select la.TeamMemberId,D.Dates as FromDate from (
															          select 
																        l.OrganizationId
																        ,la.TeamMemberId 
																        ,lad.DateFrom
																        ,lad.DateTo
																        from HR_LeaveApplication as la
																        inner join HR_LeaveApplicationDetails as lad on la.Id = lad.LeaveApplicationId
																        inner join HR_Leave as l on l.Id = la.LeaveId 
																        where (lad.DateFrom>='" + startDate + @"' and lad.DateTo<='" + endDate + @"') 
																        and la.Status = 1 and lad.Status=1 and l.Status=1
																        and l.OrganizationId = " + viewModel.OrganizationId + @"
																        --and la.TeamMemberId =368
																        and la.LeaveStatus = 2
															        ) as la 
															        inner join master..spt_values as N
																        on N.number between 0 and datediff(day, la.DateFrom, la.DateTo)
																        cross apply (select dateadd(day, N.number, la.DateFrom)) as D(Dates)
															        where N.type ='P'
														        ) as a group by a.TeamMemberId
														        --order by a.TeamMemberId ASC
													        ) as totalhWL
													        left join(---Regular Attendance
														        select b.Weekend,b.TeamMemberId,count(b.RegularAttendance) as RegularAttendance,b.OrganizationId from (
															        select * from (
																        select a.Weekend,a.TeamMemberId,  --a.HolidayWorkDate,a.AttendanceDate,
																	        CASE 
																	        WHEN CAST(a.HolidayWorkDate AS DATE) IS NULL THEN a.AttendanceDate
																	        WHEN CAST(a.AttendanceDate as DATE) <> CAST(a.HolidayWorkDate AS DATE) THEN a.AttendanceDate   
																	        ELSE a.HolidayWorkDate
																	        END AS RegularAttendance
																	        ,a.OrganizationId
																	        from (
																		        select h.Weekend,h.TeamMemberId,h.HolidayWorkDate,atds.AttendanceDate,h.OrganizationId from 
																		        (
																			        select swh.Weekend,swh.TeamMemberId,hw.HolidayWorkDate,swh.OrganizationId from (
								                                                        select * from (
									                                                        select swh.Weekend,swh.TeamMemberId
									                                                        ,swh.OrganizationId,RANK() OVER(PARTITION BY swh.TeamMemberId ORDER BY swh.EffectiveDate DESC, swh.Id DESC) AS R   
									                                                        from HR_ShiftWeekendHistory as swh
								                                                        ) as swh where swh.R = 1
							                                                        ) as swh
							                                                        left join HR_HolidayWork as hw 
							                                                        on hw.TeamMemberId = swh.TeamMemberId and hw.HolidayWorkDate>='"+startDate+@"' and hw.HolidayWorkDate<='"+endDate+@"'
							                                                        where swh.OrganizationId="+viewModel.OrganizationId+@"
																		        ) as h
																		        inner join (
																			        select ats.TeamMemberId,ats.AttendanceDate 
																			        from  HR_AttendanceSummary as ats 
																			        where (ats.AttendanceDate>='" + startDate + @"' and ats.AttendanceDate<='" + endDate + @"') 
																				        and ats.Status = 1 and ats.IsWeekend = 0 and ats.HolidaySettingId is null
																		        ) as atds on atds.TeamMemberId = h.TeamMemberId
																		        --where atds.TeamMemberId = 8 order by atds.AttendanceDate ASC 
																	        ) as a --where a.TeamMemberId = 512
															        ) as c group by c.RegularAttendance,c.OrganizationId,c.TeamMemberId,c.Weekend
														        ) as b
														        group by b.TeamMemberId,b.Weekend,b.OrganizationId
														        --order by b.TeamMemberId ASC
													        ) as ra on ra.TeamMemberId = totalhWL.TeamMemberId
													        Left join(--DayOffAdjustment
														        select doa.TeamMemberId, count(*) as totalDayOfAdjustment from HR_DayOffAdjustment as doa
														        where doa.DayOffDate>='" + startDate + @"' and doa.DayOffDate<='" + endDate + @"'
														        --and  TeamMemberId = 512
														        group by doa.TeamMemberId
													        ) as dyAdjustment on dyAdjustment.TeamMemberId = totalhWL.TeamMemberId
													        group by totalhWL.TeamMemberId
						                                ) as absentOFDay	                                            
				                                ) as absentDays on absentDays.TeamMemberId = hrm.Id
                                                left join( -- Joining date
											        Select a.TeamMemberId,a.EffectiveDate from (
													        Select 
														        o.[Id] as OrganizationId
														        ,b.[Id] as BranchId
														        ,eh.[CampusId]
														        ,eh.[DesignationId]
														        ,eh.[DepartmentId]
														        ,eh.[TeamMemberId]
														        ,eh.[EffectiveDate]
														        ,eh.[EmploymentStatus]
														        ,RANK() OVER(PARTITION BY eh.TeamMemberId ORDER BY eh.EffectiveDate ASC, eh.Id ASC) AS R  
														        from [HR_EmploymentHistory] as eh
														        inner join Campus as c on c.Id = eh.CampusId  and c.Status = 1
														        inner join Branch as b on b.Id = c.BranchId and b.Status = 1
														        inner join Organization as o on o.Id = b.OrganizationId and o.Status = 1
														        Where eh.Status = 1
														        and Month(eh.EffectiveDate) <= " + viewModel.SalaryMonth + @" and Year(eh.EffectiveDate) <= " + viewModel.SalaryYear + @"
											        ) as a where a.R=1  and a.EmploymentStatus != 6 ";

                    if (authOrganizationIdList != null)
                    {
                        query += @" and a.OrganizationId IN (" + string.Join(",", authOrganizationIdList) + ")";
                    }
                    if (authBranchIdList != null)
                    {
                        query += @" and a.BranchId IN(" + string.Join(",", authBranchIdList) + ")";
                    }
                    query += @"
                                                    --and a.OrganizationId = 3 
                                                    --and a.BranchId IN(107)
							                   ) as jd on jd.TeamMemberId = hrm.Id
			                                   where cp.TeamMemberId is not null and sh.SalaryHistoryId is not null
                                            ) as d 
										    group by d.TeamMemberId,d.Name,d.Pin,d.IsAutoDeduction,d.designation,d.SalaryHistoryId,d.BankAmount,d.CashAmount,d.OrganizationId,d.BranchId,d.CampusId
										    ,d.DepartmentId,d.DesignationId,d.SalaryOrganizationId,d.SalaryBranchId,d.SalaryCampusId,d.SalaryDepartmentId,d.SalaryDesignationId,d.DeductionPerAbsent
										    ,d.TotalAbsentDay,d.TotalSalaryWithoutDeduction,d.BasicSalary,d.HouseRent,d.Conveyance,d.Medical,d.GrossSalary,d.Increament,d.Arrear,d.AbsentAmount
										    ,d.AbsentMultiplierFactor,d.Loan,d.TotalRefundAmount,d.TdsAmount,d.Ebf,d.EbfForEmplyer
                                            ,d.EbfSettingsId,d.CalculationOn,d.EmployeeContribution,d.EmployerContribution,d.EmploymentStatus
		                                ) as d   
	                        ) as e
                        ) as f order by f.Pin ASC
                        OPTION (MaxRecursion 370)
  ";

            var list = Session.CreateSQLQuery(query).SetResultTransformer(Transformers.AliasToBean<SalarySheetDto>());
            return list.List<SalarySheetDto>().ToList();

        }
        
        public List<SalarySheet> LoadSalarySheetList(List<long> authOrganizationIdList, List<long> authBranchIdList, int totalDaysOfMonth, DateTime startDate,
            DateTime endDate, SalarySheetFormViewModel viewModel)
        {
            string query = "";
            if (viewModel.OrganizationType == OrganizationType.JobOrganization)
            {
                query = "select Ssheet.* from HR_SalarySheet as Ssheet where Ssheet.JobOrganizationId IN (" +
                               string.Join(",", authOrganizationIdList) + @") 
                                and Ssheet.JobBranchId IN (" + string.Join(",", authBranchIdList) + ") " +
                               "and Ssheet.StartDate>='" + startDate + "' and Ssheet.EndDate<='" + endDate + "'";

                if (viewModel.CampusId != 0)
                {
                    query += @" and Ssheet.JobCampusId =" + viewModel.CampusId + @"";
                }
                if (viewModel.DepartmentId != 0)
                {
                    query += @" and Ssheet.JobDepartmentId =" + viewModel.DepartmentId + "";
                }
            }
            else
            {
                query = "select Ssheet.* from HR_SalarySheet as Ssheet where Ssheet.SalaryOrganizationId IN (" +
                               string.Join(",", authOrganizationIdList) + @") 
                                and Ssheet.SalaryBranchId IN (" + string.Join(",", authBranchIdList) + ") " +
                               @" and Ssheet.StartDate>='" + startDate + "' and Ssheet.EndDate<='" + endDate + "'";

                if (viewModel.CampusId != 0)
                {
                    query += @" and Ssheet.SalaryCampusId =" + viewModel.CampusId + @"";
                }
                if (viewModel.DepartmentId != 0)
                {
                    query += @" and Ssheet.SalaryDepartmentId =" + viewModel.DepartmentId + "";
                }
            }
            //var list = Session.CreateSQLQuery(query).SetResultTransformer(Transformers.AliasToBean<SalarySheet>());
            //return list.List<SalarySheet>().ToList();

            IQuery iQuery = Session.CreateSQLQuery(query).AddEntity("Ssheet", typeof(SalarySheet));

            iQuery.SetResultTransformer(Transformers.AliasToBean<SalarySheetQueryTransfer>());
            var res = iQuery.List<SalarySheetQueryTransfer>().ToList();
            return res.Select(salarySheetDto => salarySheetDto.Ssheet).ToList();
        }

        
        #endregion

        #region Others Function


        public void GetHaveToUpdateTableInformation(out List<AttendanceSummary> attenadnceSummarylist, out List<AttendanceAdjustment> attendanceAdjustmentList,
            out List<DayOffAdjustment> dayOffList, out List<LeaveApplicationDetails> leaveApplicationDetailList, TeamMember teamMember, DateTime startDate,
            DateTime endDate)
        {
            //attenadnceSummarylist = new List<AttendanceSummary>();
            //attendanceAdjustmentList = new List<AttendanceAdjustment>();
            //dayOffList = new List<DayOffAdjustment>();
            //leaveApplicationDetailList = new List<LeaveApplicationDetails>();

            ICriteria criteria1 = Session.CreateCriteria<AttendanceSummary>();
            criteria1.Add(Restrictions.Not(Restrictions.Eq("Status", SalarySheet.EntityStatus.Delete)));
            criteria1.CreateAlias("TeamMember", "member");
            criteria1.Add(Restrictions.Eq("member.Id", teamMember.Id));
            criteria1.Add(Restrictions.Ge("AttendanceDate", startDate)).Add(Restrictions.Le("AttendanceDate", endDate));
            attenadnceSummarylist = criteria1.List<AttendanceSummary>().ToList();

            ICriteria criteria2 = Session.CreateCriteria<AttendanceAdjustment>();
            criteria2.Add(Restrictions.Not(Restrictions.Eq("Status", AttendanceAdjustment.EntityStatus.Delete)));
            criteria2.CreateAlias("TeamMember", "member");
            criteria2.Add(Restrictions.Eq("member.Id", teamMember.Id));
            criteria2.Add(Restrictions.Ge("Date", startDate)).Add(Restrictions.Le("Date", endDate));
            attendanceAdjustmentList = criteria2.List<AttendanceAdjustment>().ToList();

            ICriteria criteria3 = Session.CreateCriteria<DayOffAdjustment>();
            criteria3.Add(Restrictions.Not(Restrictions.Eq("Status", DayOffAdjustment.EntityStatus.Delete)));
            criteria3.CreateAlias("TeamMember", "member");
            criteria3.Add(Restrictions.Eq("member.Id", teamMember.Id));
            criteria3.Add(Restrictions.Ge("DayOffDate", startDate)).Add(Restrictions.Le("DayOffDate", endDate));
            dayOffList = criteria3.List<DayOffAdjustment>().ToList();

            ICriteria criteria4 = Session.CreateCriteria<LeaveApplicationDetails>();
            criteria4.Add(Restrictions.Not(Restrictions.Eq("Status", LeaveApplicationDetails.EntityStatus.Delete)));
            criteria4.CreateAlias("LeaveApplication", "la");
            criteria4.CreateAlias("la.TeamMember", "member");
            criteria4.Add(Restrictions.Eq("member.Id", teamMember.Id));
            criteria4.Add(Restrictions.Ge("DateFrom", startDate)).Add(Restrictions.Le("DateTo", endDate));
            leaveApplicationDetailList = criteria4.List<LeaveApplicationDetails>().ToList();

        }

        #endregion

        #region Helper Function
        #endregion

    }
}