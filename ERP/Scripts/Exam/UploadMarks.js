﻿function autoCompleteFunc() {
    $('.autoComplete').each(function () {
        var $this = $(this);
        $this.typeahead({
            source: function (query, process) {
                var programId = $("#Program").val();
                var sessionId = $("#Session").val();
                var courseId = $("#Course").val();
                var data = { query: query, programId: programId, sessionId: sessionId, courseId: courseId }
                $.ajax({
                    type: "POST",
                    url: $("body").attr("data-project-root") + "Exam/MarksUpload/AjaxRequestForExamCode",
                    cache: false,
                    async: true,
                    data: data,

                    success: function (result) {
                        if (result.IsSuccess) {
                            process(result.returnList);
                        }
                    },
                    complete: function () {
                    },
                    error: function (result) {
                    }
                });
            },
            updater: function (item) {
                $("#ExamId").val(item.Value);
                $("#ExamCode").val("");
                $("#ExamName").val("");

                $.ajax({
                    type: "post",
                    url: $("body").attr("data-project-root") + "Exam/MarksUpload/AjaxRequestForExamInformation",
                    cache: false,
                    async: true,
                    data: { "id": item.Value },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (result) {
                        $.unblockUI();
                        if (result.IsSuccess) {
                            console.log(result.returnList.Name);
                            if (result.returnList.hasOwnProperty("Name")) {
                                $("#ExamName").val(result.returnList.Name);
                                showErrorMessageBelowCtrl("ExamCode", "", false);
                            }
                           
                        } else {
                            $.fn.customMessage({
                                displayMessage: result.Message,
                                displayMessageType: "error"
                            });
                        }
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (result) {
                        $.unblockUI();
                        console.log("Failed -----");
                    }
                });
                return item.Text;
            },
            matcher: function (item) {
                if (item.Text.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                    return true;
                }
                return false;
            },
            sorter: function (items) {
                return items.sort();
            },
            highlighter: function (item) {
                var regex = new RegExp('(' + this.query + ')', 'gi');
                return item.Text.replace(regex, "<strong>$1</strong>");
            },
            onselect: function (obj) {
                alert('Selected ' + obj);
            }
        });

    });
}

$(document).on("input propertychange", ".autoComplete", function (event) {
    $("#marksEntryByXml tbody").empty();
    autoCompleteFunc();
    if ($(this).val() == "") {
        $("#ExamCode").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").text("The Exam Code field is required.");
    }
});

function toggleCheckbox(element) {
    //console.log($(element).parent().next("td"));
    var nextTd = $(element).parent().next("td");
    // console.log(nextTd);
    if (element.checked) {
        nextTd.find("input").each(function () {
            $(this).removeAttr("disabled");
        });
    } else {

        nextTd.find("input").each(function () {
            $(this).attr("disabled", true);
            $(this).val("");
        });
    }
}

function GetFileSize(fileid) {
    try {
        var fileSize = 0;
        //for IE
        if ($.browser.msie) {
            var objFSO = new ActiveXObject("Scripting.FileSystemObject"); var filePath = $("#" + fileid)[0].value;
            var objFile = objFSO.getFile(filePath);
            var fileSize = objFile.size; //size in kb
            fileSize = fileSize / 1048576; //size in mb
        }
        else {
            fileSize = $("#" + fileid)[0].files[0].size //size in kb
            fileSize = fileSize / 1048576; //size in mb
        }
        return fileSize;
    }
    catch (e) {
        alert("Error is :" + e);
    }
}

function getNameFromPath(strFilepath) {
    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);
    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }
}

function checkFileField() {

    var file = getNameFromPath($("#FileField").val());

    var flag = false;
    if (file != null) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'xml':
                flag = true;
                break;
            default:
                flag = false;
        }
    }

    if (flag == false) {
        $("#FileField").parent().find("span").removeClass(".field-validation-valid").addClass("field-validation-error").text("You can upload only xml extension file");
        return false;
    }
    else {
        $("#FileField").parent().find("span").removeClass(".field-validation-error").addClass("field-validation-valid").text("");
        return true;
    }
}

function responseReturn() {

    var check = true;
    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var course = $("#Course").val();
    var examCode = $("#ExamCode").val();
    var boardExam = $("#BoardExam").val(); 

    if (organizationId == "" || organizationId == null) {
        showErrorMessageBelowCtrl("Organization", "", false);
        showErrorMessageBelowCtrl("Organization", "The Organization field is required.", true);
        check = false;
    }

    if (programId == "" || programId == null) {
        showErrorMessageBelowCtrl("Program", "", false);
        showErrorMessageBelowCtrl("Program", "The Program field is required.", true);
        check = false;
    }

    if (sessionId == "" || sessionId == null) {
        showErrorMessageBelowCtrl("Session", "", false);
        showErrorMessageBelowCtrl("Session", "The Session field is required.", true);
        check = false;
    }

    if (course == "" || course == null) {
        showErrorMessageBelowCtrl("Course", "", false);
        showErrorMessageBelowCtrl("Course", "The Course field is required.", true);
        check = false;
    }


    if (examCode == "" || examCode == null) {
        showErrorMessageBelowCtrl("ExamCode", "", false);
        showErrorMessageBelowCtrl("ExamCode", "The Exam Code field is required.", true);
        check = false;
    }

    if (boardExam == "" || boardExam == null) {
        showErrorMessageBelowCtrl("BoardExam", "", false);
        showErrorMessageBelowCtrl("BoardExam", "The Board Exam field is required.", true);
        check = false;
    }

    if (!checkFileField()) {
        check = false;
    }

    if (check) {
       
        //alert("5555");
        var formData = new window.FormData();
        formData.append('Organization', organizationId);
        formData.append('Program', programId);
        formData.append('Session', sessionId);
        formData.append('Course', course);
        formData.append('BoardExam', boardExam);
        formData.append('ExamCode', examCode);
        formData.append('ExamId', $("#ExamId").val());
        formData.append('FileField', $('#FileField')[0].files[0]);

        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Exam/MarksUpload/UploadXml",
            processData: false,
            contentType: false,
            data: formData,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                $.unblockUI();
                if (response.isSuccess) {
                    returnMessageFail: Array
                    if (response.returnMessageSuccess != "") {
                        $.fn.customMessage({
                            displayMessage: "Total " + response.returnMessageSuccess + " Item Insert",
                            displayMessageType: "s"
                        });
                    }
                    if (response.returnMessageSuccessUpdate != "") {
                        $.fn.customMessage({
                            displayMessage: "Total " + response.returnMessageSuccessUpdate + " Item Update(" + response.successPrnListUpdate + ")",
                            displayMessageType: "w"
                        });
                    }
                    if (response.returnMessageFail.length > 0) {
                        $.each(response.returnMessageFail, function (key, value) {
                            var msq = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><strong>Error! </strong>' + value.message + '</div>';
                            $(".customMessageMarksUpload").append(msq);

                            //console.log(value.message);
                            //$.fn.customMessage({
                            //    displayMessage: value.message,
                            //    displayMessageType: "error"
                            //});
                        });

                        //window.setTimeout(function() {
                        //    $(".customMessageMarksUpload").empty();
                        //}, 10000);
                    }

                } else {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "error"
                    });
                }
               
                //$("#ExamName").val("");
               // $("#Organization").val("0");
                $("#FileField").val("");
               // window.setTimeout(function () { location.reload(); }, 5000);
            },
            complete: function () {
                $.unblockUI();
            },
            error: function () {
                $.unblockUI();
                console.log("Failed -----");
            }
        });

            //alert("ok2");
           // $("#uploadXmlForm").hide();

            //$.blockUI({
            //    timeout: 0,
            //    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
            //});

            //$("#marksEntryForm").get(0).submit();
        }
}

$(document).ready(function () {

    $("#marksEntryForm").submit(function () {
        return false;
    });

    $('body').keydown(function (e) {
        if (e.keyCode == 13) {
            responseReturn();
        }
    });
    
});

$(document).on("change", "#Organization", function (event) {

    $("#Program option").remove();
    $('#Program').append("<option value=''>--Select Program--</option>");

    $("#Session option").remove();
    $('#Session').append("<option value=''>--Select Session--</option>");

    $("#Course option").remove();
    $('#Course').append("<option value=''>--Select Course--</option>");

    $("#ExamId").val("");
    $("#ExamCode").val("");
    $("#ExamName").val("");

    if ($(this).val() != "") {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
            cache: false,
            async: true,
            data: { "organizationIds": $(this).val(), isAuthorized: true },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                $.unblockUI();
                if (response.IsSuccess) {
                    $.each(response.returnProgramList, function (i, v) {
                        $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});

$(document).on("change", "#Program", function (event) {

    $("#Session option").remove();
    $('#Session').append("<option value=''>--Select Session--</option>");

    $("#Course option").remove();
    $('#Course').append("<option value=''>--Select Course--</option>");

    $("#ExamId").val("");
    $("#ExamCode").val("");
    $("#ExamName").val("");


    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();

    if (organizationId != "" && programId != "") {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
            cache: false,
            async: true,
            data: { organizationIds: organizationId, programIds: programId, isAuthorized: true },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                $.unblockUI();
                if (response.IsSuccess) {
                    $.each(response.returnSessionList, function (i, v) {
                        $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
            }
        });
    }
});

$(document).on("change", "#Session", function (event) {

    $("#Course option").remove();
    $('#Course').append("<option value=''>--Select Course--</option>");

    $("#ExamId").val("");
    $("#ExamCode").val("");
    $("#ExamName").val("");


    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    if (organizationId != "" && organizationId != null && programId != "" && programId != null && sessionId != "" && sessionId != null) {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
            cache: false,
            async: true,
            data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {

                $.unblockUI();
                if (response.IsSuccess) {
                    $.each(response.returnCourse, function (i, v) {
                        $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }

});

$(document).on("change", "#Course", function (event) {
    $("#ExamId").val("");
    $("#ExamCode").val("");
    $("#ExamName").val("");

});

$(document).on("change", "#FileField", function (event) {
    checkFileField();
});

$(document).on("click", "#uploadXmlForm", function (event) {
    responseReturn();
});
