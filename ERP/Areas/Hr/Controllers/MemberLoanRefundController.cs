﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using System.Collections.Specialized;
using Microsoft.AspNet.Identity;
using UdvashERP.Services.UserAuth;


namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class MemberLoanRefundController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private List<UserMenu> _userMenus;
        private IMemberLoanRefundService _memberLoanRefundService;
        private ITeamMemberService _teamMemberService;
        private IOrganizationService _organizationService;
        private IBranchService _branchService;
        private ICampusService _campusService;
        private ICommonHelper _commonHelper;
        IUserService _userService;
        public MemberLoanRefundController()
        {
            var currentSession = NHibernateSessionFactory.OpenSession();
            _memberLoanRefundService = new MemberLoanRefundService(currentSession);
            _teamMemberService = new TeamMemberService(currentSession);
            _organizationService = new OrganizationService(currentSession);
            _branchService = new BranchService(currentSession);
            _campusService = new CampusService(currentSession);
            _commonHelper = new CommonHelper();
            _userService = new UserService(currentSession);
        }

        #endregion

        #region Operational Function

        #region Render Data Table

        [HttpPost]
        public JsonResult LoanRefundList(int draw, int start, int length, int? pin, string name,string receiptNo)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            bool isSuccess = false;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        //case "0":
                        //    orderBy = "Organization";
                        //    break;
                        //case "1":
                        //    orderBy = "Name";
                        //    break;
                        //case "2":
                        //    orderBy = "EmploymentStatus";
                        //    break;
                        //case "3":
                        //    orderBy = "CalculationOn";
                        //    break;
                        //case "4":
                        //    orderBy = "EmployeeContribution";
                        //    break;
                        //case "5":
                        //    orderBy = "EmployerContribution";
                        //    break;

                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                _userMenus = (List<UserMenu>)ViewBag.UserMenu;

                recordsTotal = _memberLoanRefundService.LoadMemberLoanRefundCount(_userMenus, start,length,orderBy, orderDir, pin, name, receiptNo);
                recordsFiltered = recordsTotal;

                IList<MemberLoanRefund> memberLoadRefundList = _memberLoanRefundService.LoadMemberLoanRefund(_userMenus,start, length, orderBy, orderDir.ToUpper(), pin, name, receiptNo).ToList();

                foreach (var memberloan in memberLoadRefundList)
                {
                    if (memberloan.Remarks == null)
                    {
                        memberloan.Remarks = "";
                    }
                    
                    var str = new List<string>
                    {
                        memberloan.TeamMember.Pin.ToString(),
                        memberloan.TeamMember.Name,
                        memberloan.ReceiptNo.ToString(),
                        memberloan.RefundDate.ToString("MMM dd, yyyy"),
                        memberloan.LoanAmount.ToString(),
                        memberloan.RefundAmount.ToString(),
                        memberloan.DueAmount.ToString(),
                        memberloan.Remarks.ToString(),
                       // LinkGenerator.GetGeneratedLink("SpecialAllowanceDetail", "SpecialAllowanceUpdate", "Delete", "SpecialAllowance", specialAllowance.Id, "PIN: "+specialAllowance.TeamMember.Pin+" Name: "+specialAllowance.TeamMember.Name)
                        LinkGenerator.GetDetailsLink("Receipt", "MemberLoanRefund", memberloan.Id)
                    };
                    getData.Add(str);
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        #endregion


        [HttpGet]
        public ActionResult MemberSearch()
        {
            return View();
        }

        [HttpPost]
        public ActionResult MemberSearch(MemberLoanRefundViewModel memberLoanRefund)
        {
            try
            {
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;

                List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(_userMenus);
                List<long> authBranchIds = AuthHelper.LoadBranchIdList(_userMenus, authOrganizationIds);

                var authorizeTeamMember = _teamMemberService.LoadHrAuthorizedTeamMember(_userMenus, null, authOrganizationIds, authBranchIds, null, null, null, _commonHelper.ConvertIdToList(memberLoanRefund.MemberPin), false);

                if (!authorizeTeamMember.Any())
                {
                    throw new NullObjectException("Invalid Team Member");
                }

                #region salary info

                var organization = _teamMemberService.GetTeamMemberSalaryOrganization(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);
                var branch = _teamMemberService.GetTeamMemberSalaryBranch(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);
                var campus = _teamMemberService.GetTeamMemberSalaryCampus(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);
                var department = _teamMemberService.GetTeamMemberSalaryDepartment(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);

                #endregion

                memberLoanRefund.TeamMemberId = authorizeTeamMember[0].Id;
                memberLoanRefund.MemberPin = authorizeTeamMember[0].Pin;
                memberLoanRefund.MemberName = authorizeTeamMember[0].FullNameEng + "(" + authorizeTeamMember[0].Name + ")";
                memberLoanRefund.Organization = organization;
                memberLoanRefund.Branch = branch;
                memberLoanRefund.Campus = campus;
                memberLoanRefund.Department = department;

                var previousLoan = _memberLoanRefundService.GetLoanBalance(authorizeTeamMember[0].Id);
                if (previousLoan == null)
                {
                    return Json(new Response(false, "No loan fund for this team member"));
                }
                else
                {
                    memberLoanRefund.LoanBalance = previousLoan.LoanBalance;
                    memberLoanRefund.CurrentBalance = previousLoan.LoanBalance;
                }

                ViewBag.LoanBalance = memberLoanRefund.LoanBalance;

                var rsp = this.RenderRazorViewToString("Partial/_MemberLoanRefund", memberLoanRefund);
                return Json(new Response(true, rsp));
            }
            catch (NullObjectException ex)
            {
                //ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, ex.Message));

            }
            catch (Exception ex)
            {
                //ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public ActionResult MemberRefundOperation(MemberLoanRefundViewModel memberLoanRefund)
        {
            try
            {
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;

                MemberLoanRefund obj = new MemberLoanRefund();
                var teamMember = _teamMemberService.LoadById(memberLoanRefund.TeamMemberId);
                obj.TeamMember = teamMember;
                obj.MemberName = teamMember.Name;
                obj.MemberPin = teamMember.Pin;
                obj.ReceivedBy = User.Identity.Name;
                obj.ReceivedById = Convert.ToInt64(User.Identity.GetUserId());
                obj.SalaryOrganization = _organizationService.LoadById(memberLoanRefund.Organization.Id);
                obj.SalaryBranch = _branchService.GetBranch(memberLoanRefund.Branch.Id);
                obj.SalaryCampus = _campusService.GetCampus(memberLoanRefund.Campus.Id);
                obj.RefundAmount = memberLoanRefund.RefundAmount;
                obj.Remarks = memberLoanRefund.Remarks;
                _memberLoanRefundService.SaveOrUpdate(_userMenus, obj);

                // memberLoanRefund=new MemberLoanRefundViewModel();
                var loanRefund = _memberLoanRefundService.GetMemberLoanRefund(obj.Id);
                var receiverInfo = _userService.LoadAspNetUserById(loanRefund.ReceivedById);
                ViewBag.ReceiverName = receiverInfo.FullName;
                return PartialView("Partial/_LoanRefundReceipt", loanRefund);
                //var rsp = this.RenderRazorViewToString("_partial/_MemberLoanRefund", memberLoanRefund);
                //return Json(new Response(true, rsp));
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }


        [HttpGet]
        public ActionResult Receipt(long id)
        {
            try
            {
                // memberLoanRefund=new MemberLoanRefundViewModel();
                var loanRefund = _memberLoanRefundService.GetMemberLoanRefund(id);
                var receiverInfo = _userService.LoadAspNetUserById(loanRefund.ReceivedById);
                ViewBag.ReceiverName = receiverInfo.FullName;
                //return PartialView("_partial/_LoanRefundReceipt", memberLoanRefund);
                return View("LoanRefundReceipt", loanRefund);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                throw;
            }
        }

        #endregion

        #region Ajax Request

        public ActionResult Receipt2(long id)
        {
            try
            {
                // memberLoanRefund=new MemberLoanRefundViewModel();
                var loanRefund = _memberLoanRefundService.GetMemberLoanRefund(id);
                var receiverInfo = _userService.LoadAspNetUserById(loanRefund.ReceivedById);
                ViewBag.ReceiverName = receiverInfo.FullName;
                //return PartialView("_partial/_LoanRefundReceipt", memberLoanRefund);
                return View("LoanRefundReceipt", loanRefund);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                throw;
            }
        }

        #endregion

        #region Helper

        #endregion
    }
}