﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.UserAuth
{
    public interface IUserMenuDao : IBaseDao<UserMenu, long>
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        UserMenu LoadDataByUserBranchProgram(long userId, long? orId, long? brId, long? prId, long? menuId);
        #endregion

        #region List Loading Function
        IList<UserMenu> LoadDataByUserBranchProgramOrganization(long[] listUser, long[] organizationIds, long[] program, long[] branch);
        List<UserMenu> LoadUserMenuByUserAndMenu(UserProfile userProfile, IList<Menu> menu);
        List<UserMenu> LoadUsrMenuByUsrAndMenuNullBP(UserProfile userProfile, Menu menu, long id);
        List<UserMenu> LoadByProfile(long userProfileId);
        List<UserMenu> LoadByProfile(UserProfile userProfile);
        List<UserMenu> LoadUserMenuByUserProfileIdAndMenuId(long userProfileId, long menuId, List<long> organizationIdList = null);
        List<UserMenu> LoadDetailsObservePermission(long userProfileId, long menuId);
        //List<UserMenu> LoadUserMenuForPermission(long userProfileId, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null);

        #endregion

        #region Others Function
        bool CheckMenuByUserBranchProgram(long userId, long? orId, long? brId, long? prId, long? menuId);
        #endregion

    }

    public class UserMenuDao : BaseDao<UserMenu, long>, IUserMenuDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public UserMenu LoadDataByUserBranchProgram(long userId, long? orId, long? brId, long? prId, long? menuId)
        {
            return Session.QueryOver<UserMenu>().Where(x => x.User.Id == userId && x.Menu.Id == menuId && x.Branch.Id == brId && 
                x.Program.Id == prId && x.Organization.Id == orId && x.Status == UserMenu.EntityStatus.Active).SingleOrDefault<UserMenu>();
        }

        #endregion

        #region List Loading Function

        public IList<UserMenu> LoadDataByUserBranchProgramOrganization(long[] listUser, long[] organizationIds,
            long[] program, long[] branch)
        {

            var query = "Select {um.*} From UserMenu as um " +
                        "Where um.MenuId In(Select MenuId From(Select MenuId,Count(*) as c From ( Select distinct MenuId,UserProfileId From UserMenu Where ";


            var query1 = "UserProfileId IN (" + string.Join(",", listUser) + ") And ";
            var query2 = "";
            var query3 = "";
            var queryOrg = "";
            var queryProgram = "";
            var queryBranch = "";
            if (organizationIds != null)
            {
                query2 += "OrganizationId IN (" + string.Join(",", organizationIds) + ") ";
                query3 += "um.OrganizationId IN (" + string.Join(",", organizationIds) + ") ";
                queryOrg += " WHERE oc>=" + organizationIds.Count();
            }
            else
            {
                query2 += "OrganizationId Is NULL ";
                query3 += "um.OrganizationId Is NULL ";
            }
            if (branch != null)
            {
                query2 += " And BranchId IN(" + string.Join(",", branch) + ") ";
                query3 += " And um.BranchId IN(" + string.Join(",", branch) + ") ";
                queryBranch += " WHERE bc>=" + branch.Count();
            }
            else
            {
                query2 += " And BranchId Is NULL ";
                query3 += " And um.BranchId Is NULL ";
            }
            if (program != null)
            {
                query2 += " And ProgramId IN(" + string.Join(",", program) + ") ";
                query3 += " And um.ProgramId IN(" + string.Join(",", program) + ") ";
                queryProgram += " WHERE pc>=" + program.Count();
            }
            else
            {
                query2 += " And programId Is NULL ";
                query3 += " And um.programId Is NULL ";
            }
            query += query2;
            query += " ) as a GROUP BY a.MenuId  ) AS a  WHERE c>=" + listUser.Count() + ") and um.UserProfileId IN (" +
                     string.Join(",", listUser) + ") And " + query3;

            //query = "Select {um.*} " +
            //        "From UserMenu as um " +
            //        "Where um.MenuId In( " +
            //        "Select MenuId From( " +
            //        "select a.MenuId, a.u, b.b, c.p from (	" +
            //        "Select MenuId, Count(*) as u From UserMenu Where " +
            //        query1 +
            //        query2 +
            //        "GROUP BY MenuId  " +
            //        ") as a " +
            //        "LEFT JOIN (	" +
            //        "Select MenuId, Count(*) as b From UserMenu Where " +
            //        query1 +
            //        query2 +
            //        "GROUP BY BranchId  " +
            //        ") as b on a.MenuId = b.MenuId " +
            //        "LEFT JOIN (	" +
            //        "Select MenuId, Count(*) as p From UserMenu Where " +
            //        query1 +
            //        query2 +
            //        "GROUP BY ProgramId " +
            //        ") as c on a.MenuId = c.MenuId " +
            //        "GROUP BY a.MenuId " +
            //        ") AS a " +
            //        "WHERE u>=" + listUser.Count() + " " +
            //        query4 +
            //        ") " +
            //        " and um.UserProfileId IN (" + string.Join(",", listUser) + ") " +
            //        query3;

            query = " Select {um.*} From UserMenu as um " +
                    " Where um.MenuId In( " +
                    " Select a.MenuId From( " +
                    " SELECT MenuId, count(*) as bc FROM ( " +
                    " SELECT MenuId, BranchId, count(*) as pc FROM ( " +
                    " SELECT MenuId, BranchId, ProgramId, count(*) as oc FROM ( " +
                    " SELECT MenuId, BranchId, ProgramId, OrganizationId, count(*) as uc " +
                    " FROM UserMenu " +
                    " Where UserProfileId IN (" + string.Join(",", listUser) + ") And " + query2 +
                    " GROUP BY MenuId, BranchId, ProgramId, OrganizationId " +
                    " ) as a " +
                    " WHERE uc>=" + listUser.Count() +
                    " GROUP BY MenuId, BranchId, ProgramId "+
                    " ) AS a " +
                    queryOrg +
                    " GROUP BY MenuId, BranchId " +
                    " ) as a " +
                    queryProgram +
                    " GROUP BY MenuId " +
                    " ) AS a " + queryBranch +
                    " ) " +
                    " and um.UserProfileId IN (" + string.Join(",", listUser) + ") AND " + query3;
            ISQLQuery iQuery = Session.CreateSQLQuery(query).AddEntity("um", typeof (UserMenu));


            var aa = iQuery.List<UserMenu>();
            return aa;

            #region old code

            //ICriteria criteria = Session.CreateCriteria(typeof(UserMenu));
            //criteria.Add(Restrictions.Eq("Status", UserMenu.EntityStatus.Active));
            //if (organizationIds != null)
            //{
            //    criteria.Add(Restrictions.In("Organization.Id", organizationIds));
            //}
            //else
            //{
            //    criteria.Add(Restrictions.IsNull("Organization"));
            //}
            //if (branch != null)
            //{
            //    criteria.Add(Restrictions.In("Branch.Id", branch));
            //}
            //else
            //{
            //    criteria.Add(Restrictions.IsNull("Branch"));
            //}
            //if (program != null)
            //{
            //    criteria.Add(Restrictions.In("Program.Id", program));
            //}
            //else
            //{
            //    criteria.Add(Restrictions.IsNull("Program"));
            //}
            //criteria.Add(Restrictions.In("User.Id", listUser));
            //var users = criteria.List<UserMenu>();

            //return aa;

            #endregion

        }

        public List<UserMenu> LoadUserMenuByUserAndMenu(UserProfile userProfile, IList<Menu> menu)
        {
            var criteria = Session.CreateCriteria<UserMenu>().Add(Restrictions.Eq("Status", UserMenu.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("User", userProfile)).Add(Restrictions.In("Menu", menu.ToArray()));
            criteria.CreateAlias("Menu", "m");
            criteria.AddOrder(Order.Asc("m.Rank"));
            var userMenuList = criteria.List<UserMenu>().ToList();
            return userMenuList;
        }
        public List<UserMenu> LoadUsrMenuByUsrAndMenuNullBP(UserProfile userProfile, Menu menu, long id)
        {
            var criteria =
                Session.CreateCriteria<UserMenu>().Add(Restrictions.Eq("Status", UserMenu.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("User", userProfile)).Add(Restrictions.Eq("Menu", menu));
            criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            var allSelectedMenu = criteria.List<UserMenu>().ToList();
            return allSelectedMenu;
        }

        public List<UserMenu> LoadByProfile(long userProfileId)
        {
            //Session.Clear();                                    //first level cache
            //Session.SessionFactory.Evict(typeof(UserMenu));     //second level cache

            var criteria = Session.CreateCriteria<UserMenu>();
            criteria.Add(Restrictions.Eq("User.Id", userProfileId));
            criteria.Add(Expression.Eq("Status", UserMenu.EntityStatus.Active));

            var dataList = criteria.List<UserMenu>().ToList();
            return dataList;
        }

        public List<UserMenu> LoadByProfile(UserProfile userProfile)
        {
            Session.Clear();                                    //first level cache
            Session.SessionFactory.Evict(typeof(UserMenu));     //second level cache

            var criteria = Session.CreateCriteria<UserMenu>();
            criteria.Add(Restrictions.Eq("User", userProfile));
            criteria.Add(Expression.Eq("Status", UserMenu.EntityStatus.Active));

            var dataList = criteria.List<UserMenu>().ToList();
            return dataList;
        }

        public List<UserMenu> LoadUserMenuByUserProfileIdAndMenuId(long userProfileId, long menuId, List<long> organizationIdList = null)
        {
            //return Session.QueryOver<UserMenu>().Where(x => x.User.Id == userProfileId && x.Menu.Id == menuId && x.Status == UserMenu.EntityStatus.Active && x.Branch == null && x.Program == null && x.Organization == null).SingleOrDefault();

            var criteria = Session.CreateCriteria<UserMenu>().Add(Restrictions.Eq("Status", UserMenu.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("User.Id", userProfileId));
            criteria.Add(Restrictions.Eq("Menu.Id", menuId));
            if (organizationIdList != null && !organizationIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("Organization.Id", organizationIdList));
            }
            var userMenuList = criteria.List<UserMenu>().ToList();
            return userMenuList;
        }

        public List<UserMenu> LoadDetailsObservePermission(long userProfileId, long menuId)
        {
            var criteria = Session.CreateCriteria<UserMenu>().Add(Restrictions.Eq("Status", UserMenu.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("User.Id", userProfileId));
            criteria.Add(Restrictions.Eq("Menu.Id", menuId));
            var userMenuList = criteria.List<UserMenu>().ToList();
            return userMenuList;
        }

        //public List<UserMenu> LoadUserMenuForPermission(long userProfileId, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null)
//        public List<UserMenu> LoadMenuForUserPermission(long userProfileId, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null)
//        {
//            int count = 0;
//            string organizationFilterQuery = "";
//            string programFilterQuery = "";
//            string branchFilterQuery = "";
//            if (organizationIdList != null)
//            {
//                if (organizationIdList.Contains(SelectionType.SelelectAll))
//                {
//                    count = 1;
//                }
//                else
//                {
//                    organizationFilterQuery = " or OrganizationId IN (" + string.Join(",", organizationIdList) + ") ";
//                    count = organizationIdList.Count;
//                }
//            }
//            if (programIdList != null)
//            {
//                if (programIdList.Contains(SelectionType.SelelectAll))
//                {
//                    count *= 1;
//                }
//                else
//                {
//                    programFilterQuery = " or ProgramId IN (" + string.Join(",", programIdList) + ") ";
//                    count *= programIdList.Count;
//                }
//            }
//            if (branchIdList != null)
//            {
//                if (branchIdList.Contains(SelectionType.SelelectAll))
//                {
//                    count *= 1;
//                }
//                else
//                {
//                    branchFilterQuery = " or BranchId IN (" + string.Join(",", branchIdList) + ") ";
//                    count *= branchIdList.Count;
//                }
//            }
//            string query2 = @"select MenuId from UserMenu 
//                            where UserProfileId = " + userProfileId + @" 
//                             and (OrganizationId is null )
//                             and (BranchId is null )
//                             and (ProgramId is null )
//                            UNION ALL
//                            SELECT MenuId
//                            FROM (
//                             select MenuId, count(*) c from UserMenu 
//                             where UserProfileId = " + userProfileId + @" 
//                              and (OrganizationId is null " + organizationFilterQuery + @")
//                              and (BranchId is null " + branchFilterQuery + @")
//                              and (ProgramId is null  " + programFilterQuery + @" )
//                             GROUP BY MenuId
//                            ) As a
//                            WHERE a.c >= 4
//                            UNION ALL
//                            SELECT ParentId as MenuId
//                            FROM Menu
//                            WHERE ID IN (
//                             select MenuId from UserMenu 
//                             where UserProfileId = " + userProfileId + @" 
//                              and (OrganizationId is null )
//                              and (BranchId is null )
//                              and (ProgramId is null )
//                             UNION ALL
//                             SELECT MenuId
//                             FROM (
//                              select MenuId, count(*) c from UserMenu 
//                              where UserProfileId = " + userProfileId + @" 
//                              and (OrganizationId is null " + organizationFilterQuery + @")
//                              and (BranchId is null " + branchFilterQuery + @")
//                              and (ProgramId is null  " + programFilterQuery + @" )
//                              GROUP BY MenuId
//                             ) As a
//                             WHERE a.c >= 4
//                            )";

//            string query = @" Select {m.*} from Menu as m 
//                            inner join ( " + query2 + @" ) as a on a.MenuId = m.Id
//                            where m.Status = " + Menu.EntityStatus.Active + @"
//                            ";
//            ISQLQuery iQuery = Session.CreateSQLQuery(query).AddEntity("m", typeof(Menu));
//            var aa = iQuery.List<Menu>();

//            //var criteria = Session.CreateCriteria<UserMenu>().Add(Restrictions.Eq("Status", UserMenu.EntityStatus.Active));
//            //criteria.Add(Restrictions.Eq("User.Id", userProfileId));

//            //var disjunctionorganization = Restrictions.Disjunction(); // for OR statement 
//            //disjunctionorganization.Add(Restrictions.IsNull("Organization"));
//            //if (organizationIdList != null && !organizationIdList.Contains(SelectionType.SelelectAll))
//            //{
//            //    disjunctionorganization.Add(Restrictions.In("Organization.Id", organizationIdList));
//            //}
//            //criteria.Add(disjunctionorganization);

//            //var disjunctionProgram = Restrictions.Disjunction(); // for OR statement 
//            //disjunctionProgram.Add(Restrictions.IsNull("Program"));
//            //if (programIdList != null && !programIdList.Contains(SelectionType.SelelectAll))
//            //{
//            //    disjunctionProgram.Add(Restrictions.In("Program.Id", programIdList));
//            //}
//            //criteria.Add(disjunctionProgram);

//            //var disjunctionBranch = Restrictions.Disjunction(); // for OR statement 
//            //disjunctionBranch.Add(Restrictions.IsNull("Branch"));
//            //if (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll))
//            //{
//            //    disjunctionBranch.Add(Restrictions.In("Branch.Id", branchIdList));
//            //}
//            //criteria.Add(disjunctionBranch);

//            //var userMenuList = criteria.List<UserMenu>().ToList();
//            //return userMenuList;
//        }




        #endregion

        #region Others Function
        public bool CheckMenuByUserBranchProgram(long userId, long? orId, long? brId, long? prId, long? menuId)
        {
            var rowList = Session.QueryOver<UserMenu>().Where(x => x.User.Id == userId && x.Menu.Id == menuId && x.Branch.Id == brId
                    && x.Program.Id == prId && x.Organization.Id == orId && x.Status == UserMenu.EntityStatus.Active).RowCount();
            if (rowList < 1)
                return false;
            return true;
        }
        #endregion
    }
}