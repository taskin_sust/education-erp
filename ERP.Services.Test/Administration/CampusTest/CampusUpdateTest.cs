﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.CampusTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Campus")]
    public class CampusUpdateTest : CampusBaseTest
    {
        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var campusFactory =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                })
                    .Persist();
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
            Assert.Throws<ValueNotAssignedException>(() => _campusService.Update(campusFactory.Object.Id, null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var campusFactory =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                })
                    .Persist();
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            campus.Name = campus.Name + " DEF GHI";
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            var success = _campusService.Update(campus.Id, campus);
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
            Assert.True(success);
        }

        [Fact]
        public void Update_Should_Check_Duplicate_Entry()
        {
            var campusFactory1 =
                _campusFactory.CreateMore(2).Persist();
            var campus1 = campusFactory1.GetLastCreatedObjectList()[0];
            var campus2 = campusFactory1.GetLastCreatedObjectList()[1];
            campus1.Name = campus2.Name;
            Assert.Throws<DuplicateEntryException>(() => _campusService.Update(campus1.Id, campus1));
            _campusTestService.DeleteCampusRoom(campus1.CampusRooms.Select(x => x.Id).ToList());
            _campusTestService.DeleteCampusRoom(campus2.CampusRooms.Select(x => x.Id).ToList());
        }
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Not_Update_Empty_Name()
        {
            var campusFactory =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                }).Persist();
            campusFactory.Object.Name = "";
            Assert.Throws<ValueNotAssignedException>(() => _campusService.Update(campusFactory.Object.Id, campusFactory.Object));
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
        }

        [Fact]
        public void Should_Not_Update_Without_Branch()
        {
            var campusFactory =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                }).Persist();
            campusFactory.Object.Branch = null;
            Assert.Throws<ValueNotAssignedException>(() => _campusService.Update(campusFactory.Object.Id, campusFactory.Object));
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
        }
        [Fact]
        public void Should_Not_Update_Empty_Location()
        {
            var campusFactory =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                }).Persist();
            campusFactory.Object.Location = "";
            Assert.Throws<ValueNotAssignedException>(() => _campusService.Update(campusFactory.Object.Id, campusFactory.Object));
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
        }
        [Fact]
        public void Should_Not_Update_Empty_Contact_No()
        {
            var campusFactory =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                }).Persist();
            campusFactory.Object.ContactNumber = "";
            Assert.Throws<ValueNotAssignedException>(() => _campusService.Update(campusFactory.Object.Id, campusFactory.Object));
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
        }

        [Fact]
        public void Should_Not_Deactive_If_Active_Batch_Exist()
        {
            var campusFactory =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                }).Persist();
            campusFactory.Object.Batches.Add(new Batch()
            {
                Status = Batch.EntityStatus.Active
            });
            campusFactory.Object.Status = Campus.EntityStatus.Inactive;
            Assert.Throws<DependencyException>(() => _campusService.Update(campusFactory.Object.Id, campusFactory.Object));
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var batchIdList = campus.Batches.Select(x => x.Id).ToList();
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            _campusFactory.CleanUpBatch(batchIdList);
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
        }


        #endregion
    }
}
