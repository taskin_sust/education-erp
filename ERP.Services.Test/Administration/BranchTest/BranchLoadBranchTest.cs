﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Test.AdminisTration.BranchTest;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BranchTest
{
    interface IBranchLoadBranchTest
    {
        void Should_Return_Successfully_Load_Branch();
        void Should_Return_Successfully_Load_All_Branch();
        void Should_Return_Successfully_Load_Branch_Without_organization();
        void Should_Return_Successfully_Load_Branch_With_BranchIdList();
        void Should_Return_Successfully_Load_Branch_With_Program_And_Session();

    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Branch")]
    public class BranchLoadBranchTest : BranchBaseTest, IBranchLoadBranchTest
    {
        [Fact]
        public void Should_Return_Successfully_Load_Branch()
        {
            Organization organization = organizationFactory.Create().Persist().Object;

            //Create New Program
            Program program = _programFactory.Create().WithOrganization(organization).Persist().Object;

            //Create New Session
            Session sessionObj = _SessionFactory.Create().Persist().Object;

            //Create New Branches and generate branchArray
            IList<Organization> organizations = new List<Organization>();
            organizations.Add(program.Organization);
            var branchFactory = _branchFactory.CreateMore(5, organizations).Persist();

            IList<Branch> branchList = branchFactory.GetLastCreatedObjectList();
            var branchIdArray = branchList.Select(x => x.Id).ToArray();

            //Generate selected Student Exam List
            var studentExamList = _studentExamService.LoadBoardExam(true);
            int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y => (int?)Convert.ToInt32(y.Id)).ToArray();

            //generate selected Admission Type List
            var selectedAdmissionTypeList = new int?[] { 1, 2 };

            List<long> nonDeletableBranchIds;
            _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id,
                branchIdArray, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);

            var orgIdList =  new List<long> {organization.Id};
            var programIdList = new List<long>() {program.Id};
            var sessionIdList = new List<long>() {sessionObj.Id};
            var branchIdList = branchList.Select(x => x.Id).ToList();

            IList<Branch> branchLists = _branchService.LoadBranch(orgIdList, programIdList, sessionIdList, branchIdList,selectedAdmissionTypeList.ToList());

            Assert.Equal(branchLists.Count,5);

        }
        [Fact]
        public void Should_Return_Successfully_Load_All_Branch()
        {
            Organization organization = organizationFactory.Create().Persist().Object;

            //Create New Program
            Program program = _programFactory.Create().WithOrganization(organization).Persist().Object;

            //Create New Session
            Session sessionObj = _SessionFactory.Create().Persist().Object;

            //Create New Branches and generate branchArray
            IList<Organization> organizations = new List<Organization>();
            organizations.Add(program.Organization);
            var branchFactory = _branchFactory.CreateMore(5, organizations).Persist();

            IList<Branch> branchList = branchFactory.GetLastCreatedObjectList();
            var branchIdArray = branchList.Select(x => x.Id).ToArray();

            //Generate selected Student Exam List
            var studentExamList = _studentExamService.LoadBoardExam(true);
            int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y => (int?)Convert.ToInt32(y.Id)).ToArray();

            //generate selected Admission Type List
            var selectedAdmissionTypeList = new int?[] { 1, 2 };

            List<long> nonDeletableBranchIds;
            _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id,
                branchIdArray, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);
            
            IList<Branch> branchLists = _branchService.LoadBranch();
            Assert.NotEmpty(branchLists);
        }
        [Fact]
        public void Should_Return_Successfully_Load_Branch_Without_organization()
        {
            Organization organization = organizationFactory.Create().Persist().Object;

            //Create New Program
            Program program = _programFactory.Create().WithOrganization(organization).Persist().Object;

            //Create New Session
            Session sessionObj = _SessionFactory.Create().Persist().Object;

            //Create New Branches and generate branchArray
            IList<Organization> organizations = new List<Organization>();
            organizations.Add(program.Organization);
            var branchFactory = _branchFactory.CreateMore(5, organizations).Persist();

            IList<Branch> branchList = branchFactory.GetLastCreatedObjectList();
            var branchIdArray = branchList.Select(x => x.Id).ToArray();

            //Generate selected Student Exam List
            var studentExamList = _studentExamService.LoadBoardExam(true);
            int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y => (int?)Convert.ToInt32(y.Id)).ToArray();

            //generate selected Admission Type List
            var selectedAdmissionTypeList = new int?[] { 1, 2 };

            List<long> nonDeletableBranchIds;
            _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id,
                branchIdArray, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);

            //var orgIdList = new List<long> { organization.Id };
            var programIdList = new List<long>() { program.Id };
            var sessionIdList = new List<long>() { sessionObj.Id };
            var branchIdList = branchList.Select(x => x.Id).ToList();

            IList<Branch> branchLists = _branchService.LoadBranch(null, programIdList, sessionIdList, branchIdList, selectedAdmissionTypeList.ToList());

            Assert.Equal(branchLists.Count, 5);
        }
        [Fact]
        public void Should_Return_Successfully_Load_Branch_With_BranchIdList()
        {
            Organization organization = organizationFactory.Create().Persist().Object;

            //Create New Program
            Program program = _programFactory.Create().WithOrganization(organization).Persist().Object;

            //Create New Session
            Session sessionObj = _SessionFactory.Create().Persist().Object;

            //Create New Branches and generate branchArray
            IList<Organization> organizations = new List<Organization>();
            organizations.Add(program.Organization);
            var branchFactory = _branchFactory.CreateMore(5, organizations).Persist();

            IList<Branch> branchList = branchFactory.GetLastCreatedObjectList();
            var branchIdArray = branchList.Select(x => x.Id).ToArray();

            //Generate selected Student Exam List
            var studentExamList = _studentExamService.LoadBoardExam(true);
            int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y => (int?)Convert.ToInt32(y.Id)).ToArray();

            //generate selected Admission Type List
            var selectedAdmissionTypeList = new int?[] { 1, 2 };

            List<long> nonDeletableBranchIds;
            _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id,
                branchIdArray, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);

            var branchIdList = branchList.Select(x => x.Id).ToList();

            IList<Branch> branchLists = _branchService.LoadBranch(null, null, null, branchIdList);

            Assert.Equal(branchLists.Count, 5);
        }
        [Fact]
        public void Should_Return_Successfully_Load_Branch_With_Program_And_Session()
        {
            Organization organization = organizationFactory.Create().Persist().Object;

            //Create New Program
            Program program = _programFactory.Create().WithOrganization(organization).Persist().Object;

            //Create New Session
            Session sessionObj = _SessionFactory.Create().Persist().Object;

            //Create New Branches and generate branchArray
            IList<Organization> organizations = new List<Organization>();
            organizations.Add(program.Organization);
            var branchFactory = _branchFactory.CreateMore(5, organizations).Persist();

            IList<Branch> branchList = branchFactory.GetLastCreatedObjectList();
            var branchIdArray = branchList.Select(x => x.Id).ToArray();

            //Generate selected Student Exam List
            var studentExamList = _studentExamService.LoadBoardExam(true);
            int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y => (int?)Convert.ToInt32(y.Id)).ToArray();

            //generate selected Admission Type List
            var selectedAdmissionTypeList = new int?[] { 1, 2 };

            List<long> nonDeletableBranchIds;
            _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id,
                branchIdArray, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);
            
            var programIdList = new List<long>() { program.Id };
            var sessionIdList = new List<long>() { sessionObj.Id };
            
            IList<Branch> branchLists = _branchService.LoadBranch(null, programIdList, sessionIdList, null);

            Assert.Equal(branchLists.Count, 5);
        }
    }
}
