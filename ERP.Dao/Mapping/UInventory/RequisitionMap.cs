using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class RequisitionMap : BaseClassMap<Requisition, long>
    {
        public RequisitionMap()
        {
            Table("UINV_Requisition");
            LazyLoad();
            References(x => x.Branch).Column("BranchId");
            Map(x => x.RequisitionNo).Column("RequisitionNo").Not.Nullable();
            Map(x => x.RequiredDate).Column("RequiredDate").Not.Nullable();
            Map(x => x.Remarks).Column("Remarks");
            Map(x => x.RequisionStatus).Column("RequisionStatus").Not.Nullable();
            HasMany(x => x.GoodsIssueList).KeyColumn("RequisitionId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.RequisitionDetails).KeyColumn("RequisitionId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
