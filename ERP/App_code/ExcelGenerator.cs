﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Conventions;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.RefAndLookup;
using OfficeOpenXml.Style;

namespace UdvashERP.App_code
{
    public class ExcelGenerator
    {
        public static void GenerateExcel(List<string> headerList, List<string> columnTitleList, List<List<object>> dataList, List<string> footerList, string fileName = "", bool addSerial = true, bool addBorder = true)
        {
            //set file name
            if (fileName.IsEmpty())
                fileName = DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".xlsx";
            else if (fileName.EndsWith(".xlsx", StringComparison.OrdinalIgnoreCase) == false)
                fileName += ".xlsx";

            //if serial added
            if (addSerial)
                columnTitleList.Insert(0, "SL.");

            using (var package = new ExcelPackage())
            {
                int rowNo = 0, rowNoColumnTitle = 0, rowNoDataEnd = 0;
                int columnTitleCount = columnTitleList.Count;
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");

                //set headers
                foreach (var title in headerList)
                {
                    rowNo++;
                    int columnNo = 1;
                    worksheet.Cells[rowNo, columnNo, rowNo, columnTitleCount].Merge = true;
                    worksheet.Cells[rowNo, columnNo].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[rowNo, columnNo].Style.Font.Size += 2;
                    //worksheet.Cells[rowNo, columnNo].Style.Font.Bold = true;
                    worksheet.Cells[rowNo, columnNo].Value = title;
                    //worksheet.Cells[rowNo, columnNo].Style.WrapText = true;
                }

                //set columnTitle
                rowNo++;
                rowNoColumnTitle = rowNo;
                for (int i = 0; i < columnTitleCount; i++)
                {
                    int columnNo = i + 1;
                    worksheet.Cells[rowNo, columnNo].Value = columnTitleList[i];
                    worksheet.Cells[rowNo, columnNo].Style.Font.Bold = true;
                    //worksheet.Cells[rowNo, columnNo].Style.WrapText = true;
                    worksheet.Cells[rowNo, columnNo].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[rowNo, columnNo].AutoFitColumns();
                }

                //set data
                int serialNo = 0;
                foreach (var data in dataList)
                {
                    rowNo++;
                    int colNoEx = 1;

                    if (addSerial)
                    {
                        worksheet.Cells[rowNo, colNoEx].Value = ++serialNo;
                        worksheet.Cells[rowNo, colNoEx].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        colNoEx = 2;
                    }

                    int dataColumnCount = data.Count;
                    for (int i = 0; i < dataColumnCount; i++)
                    {
                        worksheet.Cells[rowNo, i + colNoEx].Value = data[i];

                        //set datetime format
                        if (data[i] is DateTime)
                        {
                            worksheet.Cells[rowNo, i + colNoEx].Style.Numberformat.Format = "dd-MM-yyyy";
                            worksheet.Cells[rowNo, i + colNoEx].AutoFitColumns();
                        }

                    }
                }

                //set border around data
                rowNoDataEnd = rowNo;
                if (addBorder)
                {
                    worksheet.Cells[rowNoColumnTitle, 1, rowNoDataEnd, columnTitleCount].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowNoColumnTitle, 1, rowNoDataEnd, columnTitleCount].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowNoColumnTitle, 1, rowNoDataEnd, columnTitleCount].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowNoColumnTitle, 1, rowNoDataEnd, columnTitleCount].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                }

                //set footer
                foreach (var footer in footerList)
                {
                    rowNo++;
                    int columnNo = 1;
                    worksheet.Cells[rowNo, columnNo, rowNo, columnTitleCount].Merge = true;
                    worksheet.Cells[rowNo, columnNo].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowNo, columnNo].Value = footer;
                }

                //write to output
                HttpContext.Current.Response.BinaryWrite(package.GetAsByteArray());
            }

            //response
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.End();
        }
        public static void GenerateExcel(List<string> headerList, List<List<object>> headerColumnList, List<string> columnTitleList, List<List<object>> dataList, List<string> footerList, string fileName = ""
                                                                    , bool addSerial = true, bool addBorder = true)
        {
            //set file name
            if (fileName.IsEmpty())
                fileName = DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".xlsx";
            else if (fileName.EndsWith(".xlsx", StringComparison.OrdinalIgnoreCase) == false)
                fileName += ".xlsx";

            //if serial added
            if (addSerial)
                columnTitleList.Insert(0, "SL.");

            using (var package = new ExcelPackage())
            {
                int rowNo = 0, rowNoColumnTitle = 0, rowNoDataEnd = 0;
                int columnTitleCount = columnTitleList.Count;
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");

                //set headers
                foreach (var title in headerList)
                {
                    rowNo++;
                    int columnNo = 1;
                    worksheet.Cells[rowNo, columnNo, rowNo, columnTitleCount].Merge = true;
                    worksheet.Cells[rowNo, columnNo].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[rowNo, columnNo].Style.Font.Size += 2;
                    worksheet.Cells[rowNo, columnNo].Value = title;
                }

                #region Set Header Column

                if (headerColumnList.Count > 0 && headerColumnList[0].Count > 0)
                {
                    int rowIndex = 0;
                    int titleCount = headerColumnList[0].Count;
                    int rowNoHeaderColumnTitle = rowNo + 1;
                    foreach (var headerColData in headerColumnList)
                    {
                        rowNo++;
                        int colNoEx = 1;
                        if (addSerial)
                        {
                            worksheet.Cells[rowNo, colNoEx].Value = "";
                            worksheet.Cells[rowNo, colNoEx].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            colNoEx = 2;
                        }

                        int dataColumnCount = headerColData.Count;
                        for (int i = 0; i < dataColumnCount; i++)
                        {
                            worksheet.Cells[rowNo, i + colNoEx].Value = headerColData[i];
                            worksheet.Cells[rowNo, i + colNoEx].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            if (rowIndex == 0)
                            {
                                worksheet.Cells[rowNo, i + colNoEx].Style.Font.Bold = true;
                            }
                            worksheet.Cells[rowNo, i + colNoEx].AutoFitColumns();
                        }
                        rowIndex++;
                    }
                    int rowNoHeaderColEnd = rowNo;
                    //Set Border
                    int borderStartCell = addSerial ? 2 : 1;
                    titleCount = addSerial ? titleCount + 1 : titleCount;

                    worksheet.Cells[rowNoHeaderColumnTitle, borderStartCell, rowNoHeaderColEnd, titleCount].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowNoHeaderColumnTitle, borderStartCell, rowNoHeaderColEnd, titleCount].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowNoHeaderColumnTitle, borderStartCell, rowNoHeaderColEnd, titleCount].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowNoHeaderColumnTitle, borderStartCell, rowNoHeaderColEnd, titleCount].Style.Border.Left.Style = ExcelBorderStyle.Thin;

                    //Add an empty row
                    rowNo++;
                    worksheet.Cells[rowNo, 1, rowNo, columnTitleCount].Merge = true;
                    worksheet.Cells[rowNo, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[rowNo, 1].Style.Font.Size += 2;
                    worksheet.Cells[rowNo, 1].Value = "";
                }

                #endregion

                //set columnTitle
                rowNo++;
                rowNoColumnTitle = rowNo;
                for (int i = 0; i < columnTitleCount; i++)
                {
                    int columnNo = i + 1;
                    worksheet.Cells[rowNo, columnNo].Value = columnTitleList[i];
                    worksheet.Cells[rowNo, columnNo].Style.Font.Bold = true;
                    worksheet.Cells[rowNo, columnNo].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[rowNo, columnNo].AutoFitColumns();
                }

                #region set data

                int serialNo = 0;
                foreach (var data in dataList)
                {
                    rowNo++;
                    int colNoEx = 1;

                    if (addSerial)
                    {
                        worksheet.Cells[rowNo, colNoEx].Value = ++serialNo;
                        worksheet.Cells[rowNo, colNoEx].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        colNoEx = 2;
                    }

                    int dataColumnCount = data.Count;
                    for (int i = 0; i < dataColumnCount; i++)
                    {
                        worksheet.Cells[rowNo, i + colNoEx].Value = data[i];

                        //set datetime format
                        if (data[i] is DateTime)
                        {
                            worksheet.Cells[rowNo, i + colNoEx].Style.Numberformat.Format = "dd-MM-yyyy";
                            worksheet.Cells[rowNo, i + colNoEx].AutoFitColumns();
                        }

                    }
                }

                //set border around data
                rowNoDataEnd = rowNo;
                if (addBorder)
                {
                    worksheet.Cells[rowNoColumnTitle, 1, rowNoDataEnd, columnTitleCount].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowNoColumnTitle, 1, rowNoDataEnd, columnTitleCount].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowNoColumnTitle, 1, rowNoDataEnd, columnTitleCount].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowNoColumnTitle, 1, rowNoDataEnd, columnTitleCount].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                }

                #endregion
                //set footer
                foreach (var footer in footerList)
                {
                    rowNo++;
                    int columnNo = 1;
                    worksheet.Cells[rowNo, columnNo, rowNo, columnTitleCount].Merge = true;
                    worksheet.Cells[rowNo, columnNo].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowNo, columnNo].Value = footer;
                }

                //write to output
                HttpContext.Current.Response.BinaryWrite(package.GetAsByteArray());
            }

            //response
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.End();
        }
    }
}