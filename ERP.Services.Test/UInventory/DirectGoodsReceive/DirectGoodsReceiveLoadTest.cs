﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.DirectGoodsReceive
{

    public interface IDirectGoodsReceiveLoadActiveTest
    {
        #region List Load Test
        void Should_Return_List_of_GoodsReceive_By_WorkOrderId_Succesfully();
        void Should_Return_Invalid_WorkorderId();
        void Should_Return_List_Of_ItemOpeningBalanceDetailsViewModel_WithAuth_Successfully();
        void Should_Return_List_Of_ItemOpeningBalanceDetailsViewModel_WithoutAuth_Successfully();
        void Should_Return_List_Of_GoodsReceive_Successfully();
        void Should_Return_Invalid_User_Menu();

        #endregion

        #region Single Object Load Test

        void Should_Return_GoodsReceiveObj_Successfully();
        void Should_Return_Invalid_id_Exception();
        void Should_Return_GoodsReceiveObj_by_ReceiveNo_Successfully();
        void Should_Return_Invalid_ReceiveNo_Exception();
        void Should_Return_Invalid_ReceiveNoLength_Exception();

        #endregion

        #region Others Test

        void Should_Return_CurrentGoodsReceivedNo_Successfully();

        #endregion

        #region  Count Test

        void Should_Return_Count_More_than_1_DirectGoodsReceive_SuccessFully();
        void Should_Return_Invalid_User_Menu_FromCount();

        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReceive")]
    public class DirectGoodsReceiveLoadTest : DirectGoodsReceiveBaseTest, IDirectGoodsReceiveLoadActiveTest
    {
        #region List Test

        [Fact]
        public void Should_Return_List_of_GoodsReceive_By_WorkOrderId_Succesfully()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();

            // receive against workorder number
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            IList<GoodsReceive> list = _goodsReceiveService.LoadGoodsReceiveByWorkOrder(_directWorkOrderFactory.Object.Id);
            Assert.True(list.Count > 0);
        }

        [Fact]
        public void Should_Return_Invalid_WorkorderId()
        {
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.LoadGoodsReceiveByWorkOrder(-1));
        }

        [Fact(Skip = "Shakil vai will write this one after completing service")]
        public void Should_Return_List_Of_ItemOpeningBalanceDetailsViewModel_WithAuth_Successfully()
        {
            throw new NotImplementedException();
        }

        [Fact(Skip = "Shakil vai will write this one after completing service")]
        public void Should_Return_List_Of_ItemOpeningBalanceDetailsViewModel_WithoutAuth_Successfully()
        {
            throw new NotImplementedException();
        }

        [Fact]
        public void Should_Return_List_Of_GoodsReceive_Successfully()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(organizationFactory.Object, commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();
            //receive
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            List<OrderedGoodsRecViewModel> list = _goodsReceiveService.LoadGoodsReceived(0, 0, int.MaxValue, userMenus, organizationFactory.Object.Id, branchFactory.Object.Id,
                _itemFactory.Object.Name, _goodsReceiveFactory.Object.GoodsReceivedDate.ToString(), _directWorkOrderFactory.Object.WorkOrderNo, _directWorkOrderFactory.Object.WorkOrderDate.ToString(), "");
            Assert.True(list.Count > 0);
        }

        [Fact]
        public void Should_Return_Invalid_User_Menu()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();

            //receive
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.LoadGoodsReceived(0, 0, int.MaxValue, null, organizationFactory.Object.Id, branchFactory.Object.Id, "", DateTime.Now.ToString(), "", "", ""));
        }

        #endregion

        #region Single Object Load Test

        [Fact]
        public void Should_Return_GoodsReceiveObj_Successfully()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create()
                .WithPurpose()
                .WithProgram()
                .WithSession()
                .WithItem()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);
            Assert.Equal(_goodsReceiveFactory.Object.Id, _goodsReceiveService.GetGoodsReceive(_goodsReceiveFactory.Object.Id).Id);
        }

        [Fact]
        public void Should_Return_Invalid_id_Exception()
        {
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.GetGoodsReceive(-1));
        }

        [Fact]
        public void Should_Return_GoodsReceiveObj_by_ReceiveNo_Successfully()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create()
                .WithPurpose()
                .WithProgram()
                .WithSession()
                .WithItem()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);
            Assert.Equal(_goodsReceiveFactory.Object.Id, _goodsReceiveService.GetGoodsReceive(_goodsReceiveFactory.Object.GoodsReceivedNo).Id);
        }

        [Fact]
        public void Should_Return_Invalid_ReceiveNo_Exception()
        {
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.GetGoodsReceive(""));
        }

        [Fact]
        public void Should_Return_Invalid_ReceiveNoLength_Exception()
        {
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.GetGoodsReceive("1547856"));
        }

        #endregion

        #region Others Test

        [Fact]
        public void Should_Return_CurrentGoodsReceivedNo_Successfully()
        {
            branchFactory.Create().WithOrganization().WithCode(new Random().Next(99).ToString());
            var recCode = _goodsReceiveService.GetCurrentGoodsReceivedNoByBranchAndOrg(branchFactory.Object);
            Assert.StartsWith(branchFactory.Object.Organization.ShortName.Substring(0, 3) + branchFactory.Object.Code + "GR" + "00000" + 1, recCode);
        }

        #endregion

        #region Count Test

        [Fact]
        public void Should_Return_Count_More_than_1_DirectGoodsReceive_SuccessFully()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(organizationFactory.Object, commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();
            //receive
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            int count = _goodsReceiveService.CountGoodsReceive(userMenus, organizationFactory.Object.Id, branchFactory.Object.Id, _itemFactory.Object.Name, _goodsReceiveFactory.Object.GoodsReceivedDate.ToString(),
                _directWorkOrderFactory.Object.WorkOrderNo, _directWorkOrderFactory.Object.WorkOrderDate.ToString(), "");
            Assert.True(count > 0);
        }

        [Fact]
        public void Should_Return_Invalid_User_Menu_FromCount()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            //var userMenus = BuildUserMenu(_organizationFactory.Object, _commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), _branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();

            //receive
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.CountGoodsReceive(null, organizationFactory.Object.Id, branchFactory.Object.Id, "", DateTime.Now.ToString(), "", "", ""));
        }

        #endregion
    }
}
