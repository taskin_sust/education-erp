using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface IWorkOrderDao : IBaseDao<WorkOrder, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        WorkOrder GetByWorkOrderNo(string workOrderNo);

        #endregion

        #region List Loading Function

        IList<WorkOrderDto> LoadWorkOrder(int start, int length, string orderBy, string orderDir, List<long> branchIdList = null, string item = "", int? status = null);

        IList<WorkOrderReportDto> LoadWorkOrderReport(int start, int length, List<long> authBranchIdList, int workOrderType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, int workOrderstatus, DateTime dateFrom, DateTime dateTo);

        IList<ProgramSessionDto> LoadProgramSessionList(List<long> authBranchIdList, List<long> itemIdList);

        #endregion

        #region Others Function

        string GenerateSeqeuntialNo(long branchId);
        int WorkOrderCount(List<long> branchIdList = null, string item = "", int? status = null);
        int GetWorkOrderReportCount(List<long> authBranchIdList, int workOrderType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int workOrderstatus, DateTime dateFrom, DateTime dateTo);
        bool CheckActiveQuotation(long quotationId);

        #endregion

    }

    public class WorkOrderDao : BaseDao<WorkOrder, long>, IWorkOrderDao
    {

        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        
        public WorkOrder GetByWorkOrderNo(string workOrderNo)
        {
            return
                Session.QueryOver<WorkOrder>()
                    .Where(x => x.WorkOrderNo == workOrderNo.Trim() && x.Status == WorkOrder.EntityStatus.Active && x.WorkOrderStatus != WorkOrderStatus.Cancelled)
                    .SingleOrDefault<WorkOrder>();
        }

        #endregion

        #region List Loading Function

        public IList<WorkOrderDto> LoadWorkOrder(int start, int length, string orderBy, string orderDir, List<long> branchIdList = null, string item = "", int? workOrderStatus = null)
        {
            string query = CommonQueryString(orderBy, orderDir, branchIdList, item, workOrderStatus);
            IQuery iQuery = Session.CreateSQLQuery(query);
            if (length > 0)
            {
                iQuery.SetFirstResult(start).SetMaxResults(length);
            }
            iQuery.SetResultTransformer(Transformers.AliasToBean<WorkOrderDto>());
            var list = iQuery.List<WorkOrderDto>().ToList();
            return list;
        }

        public IList<WorkOrderReportDto> LoadWorkOrderReport(int start, int length, List<long> authBranchIdList,
            int workOrderType, List<long> purposeList,
            List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, int workOrderstatus,
            DateTime dateFrom, DateTime dateTo)
        {
            string query = GetWorkOrderReportCommonQueryString(authBranchIdList, workOrderType, purposeList,
                itemGroupIdList, itemIdList, programIdList, sessionIdList, workOrderstatus, dateFrom,
                dateTo);
            IQuery iQuery = Session.CreateSQLQuery(query);
            if (length > 0)
            {
                iQuery.SetFirstResult(start).SetMaxResults(length);
            }
            iQuery.SetResultTransformer(Transformers.AliasToBean<WorkOrderReportDto>());
            var list = iQuery.List<WorkOrderReportDto>().ToList();
            return list;
        }

        private string GetWorkOrderReportCommonQueryString(List<long> authBranchIdList, int workOrderType, List<long> purposeList, List<long> itemGroupIdList,
            List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int workOrderstatus, DateTime dateFrom, DateTime dateTo)
        {
            string whereClause = "";
            if (authBranchIdList != null && authBranchIdList.Count > 0)
            {
                whereClause += " and b.id in( " + string.Join(", ", authBranchIdList) + ")";
            }
            if (workOrderType == (int)WorkOrderType.QuotationBasedWorkOrder)
            {
                whereClause += " and wo.QuotationId is not null";
            }
            else if (workOrderType == (int)WorkOrderType.DirectWorkOrder)
            {
                whereClause += " and wo.QuotationId is null";
            }
            if (purposeList != null && purposeList.Count > 0 && !purposeList.Contains(SelectionType.SelelectAll))
            {
                if (purposeList.Count == 1 && purposeList.Contains(-1))
                {
                    whereClause += " and wod.Purpose is null";
                }
                else if (purposeList.Count > 1 && purposeList.Contains(-1))
                {
                    whereClause += " and (wod.Purpose in( " + string.Join(", ", purposeList) + ") or wod.Purpose is null )";
                }
                else
                {
                    whereClause += " and (wod.Purpose in( " + string.Join(", ", purposeList) + "))";
                }
            }
            if (itemGroupIdList != null && itemGroupIdList.Count > 0 && !itemGroupIdList.Contains(SelectionType.SelelectAll))
            {
                whereClause += " and ig.id in( " + string.Join(", ", itemGroupIdList) + ")";
            }
            if (itemIdList != null && itemIdList.Count > 0 && !itemIdList.Contains(SelectionType.SelelectAll))
            {
                whereClause += " and i.id in( " + string.Join(", ", itemIdList) + ")";
            }
            if (programIdList != null && programIdList.Count > 0 && !programIdList.Contains(SelectionType.SelelectAll))
            {
                if (programIdList.Count == 1 && programIdList.Contains(-1))
                {
                    whereClause += " and p.id is null";
                }
                else if (programIdList.Count > 1 && programIdList.Contains(-1))
                {
                    whereClause += " and (p.id in( " + string.Join(", ", programIdList) + ") or p.id is null )";
                }
                else
                {
                    whereClause += " and (p.id in( " + string.Join(", ", programIdList) + "))";
                }
            }
            if (sessionIdList != null && sessionIdList.Count > 0 && !sessionIdList.Contains(SelectionType.SelelectAll))
            {
                if (sessionIdList.Count == 1 && sessionIdList.Contains(-1))
                {
                    whereClause += " and s.id is null";
                }
                else if (sessionIdList.Count > 1 && sessionIdList.Contains(-1))
                {
                    whereClause += " and (s.id in( " + string.Join(", ", sessionIdList) + ") or s.id is null )";
                }
                else
                {
                    whereClause += " and (s.id in( " + string.Join(", ", sessionIdList) + "))";
                }
            }
            if (workOrderstatus != SelectionType.SelelectAll)
            {
                whereClause += " and wo.WorkOrderStatus=" + workOrderstatus;
            }
            whereClause += " and wo.CreationDate>='" + dateFrom.Date + "'";
            whereClause += " and wo.CreationDate < '" + dateTo.AddDays(1).Date + "'";
            string query = @"select wo.Id 'WorkOrderId'                           
                            ,b.name 'Branch'
                            ,case when wo.QuotationId is null then 'Direct Work Order'
                            when wo.QuotationId is not null then 'Quotation Based Work Order' 
                            end 'WorkOrderType'
                            , ig.Name 'ItemGroup'
                            ,i.Name 'ItemName'
                            ,isnull(p.Name+' '+s.Name,'N/A') 'ProgramSession'
                            ,wo.CreationDate 'WorkOrderDate'
                            ,wo.WorkOrderNo 'WorkOrderRefNo'
                            ,case when wo.WorkOrderStatus =1 then 'Issued'
                             when wo.WorkOrderStatus =2 then 'Partially Received'
                             when wo.WorkOrderStatus =3 then 'Received'
                             when wo.WorkOrderStatus =4 then 'Cancelled' 
                             end
                            'WorkOrderStatus' 
                            from UINV_WorkOrderDetails wod 
                            inner join UINV_WorkOrder wo on wo.Id=wod.WorkOrderId
                            inner join UINV_Item i on i.Id=wod.ItemId
                            left join Program p on p.Id=wod.ProgramId
                            left join Session s on s.Id=wod.SessionId
                            inner join Branch b on wo.BranchId=b.Id
                            inner join UINV_ItemGroup ig on ig.Id=i.ItemGroupId
                            where wo.Status=1 and i.status=1 
                            --and p.status=1 and s.status=1 
                            and b.status =1 and ig.status=1 ";
            return query + whereClause;
        }

        public IList<ProgramSessionDto> LoadProgramSessionList(List<long> authBranchIdList, List<long> itemIdList)
        {
            string query = @"select distinct wod.ProgramId , wod.SessionId , p.Name AS ProgramName , p.ShortName as ProgramShortName, s.Name AS SessionName
                            from UINV_WorkOrder wo 
                            inner join UINV_WorkOrderDetails wod on wo.Id=wod.WorkOrderId
                            inner join Program p on wod.ProgramId=p.Id
                            inner join Session s on wod.SessionId=s.Id
                            where wo.status=1 and wod.status=1 ";
            if (authBranchIdList != null && authBranchIdList.Any() &&
                !authBranchIdList.Contains(SelectionType.SelelectAll))
            {
                query += " and wo.branchId in (" +string.Join(", ",authBranchIdList)+ ")";
            }
            if (itemIdList != null && itemIdList.Any() &&
                !itemIdList.Contains(SelectionType.SelelectAll))
            {
                query += " and wod.itemid in (" +string.Join(", ",itemIdList)+ ")";

            }
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ProgramSessionDto>());
            var list = iQuery.List<ProgramSessionDto>().ToList();
            return list;
        }

        #endregion

        #region Others Function

        public int WorkOrderCount(List<long> branchIdList = null, string item = "", int? workOrderStatus = null)
        {
            string query = "select count(*) from (" + CommonQueryString("", "", branchIdList, item, workOrderStatus) + ")x";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int GetWorkOrderReportCount(List<long> authBranchIdList, int workOrderType, List<long> purposeList, List<long> itemGroupIdList,
            List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int workOrderstatus, DateTime dateFrom, DateTime dateTo)
        {
            string query = "select count(*) from (" + GetWorkOrderReportCommonQueryString(authBranchIdList, workOrderType, purposeList,
                itemGroupIdList, itemIdList, programIdList, sessionIdList, workOrderstatus, dateFrom,
                dateTo) + ")x";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }
        
        public bool CheckActiveQuotation(long quotationId)
        {
            IList<WorkOrder> list = Session.QueryOver<WorkOrder>()
                    .Where(x => x.Quotation.Id == quotationId && x.Status == WorkOrder.EntityStatus.Active)
                    .List<WorkOrder>();
            return list.Count <= 0;
        }
        
        #endregion

        #region Helper Function

        public string GenerateSeqeuntialNo(long branchId)
        {
            string workOrderNo = "";
            int count = Session.QueryOver<WorkOrder>().Where(x => x.Branch.Id == branchId).RowCount();
            if (count == 0)
            {
                workOrderNo = "000001";
            }
            else
            {
                var workOrder = Session.QueryOver<WorkOrder>().Where(x => x.Branch.Id == branchId).OrderBy(x => x.Id).Desc.Take(1).SingleOrDefault<WorkOrder>();
                var lastWorkOrderNo = Convert.ToInt32(workOrder.WorkOrderNo.Substring(workOrder.WorkOrderNo.Length - 6));
                workOrderNo = (lastWorkOrderNo + 1).ToString();
            }

            return workOrderNo;
        }

        public string CommonQueryString(string orderBy = "", string orderDir = "", List<long> branchIdList = null, string item = "", int? workOrderStatus = null)
        {
            string whereClause = "";
            if (branchIdList != null && branchIdList.Count > 0) whereClause += " and b.Id in(" + string.Join(", ", branchIdList) + ")";
            if (!string.IsNullOrEmpty(item))
            {
                whereClause += " and i.Name Like '%" + item + "%'";
            }
            if (workOrderStatus != null)
            {
                whereClause += " and wo.WorkOrderStatus=" + workOrderStatus;
            }
            string query = @"select wo.Id WorkOrderId,o.ShortName Organization,b.Name Branch,i.Name ItemName 
                            ,wo.WorkOrderNo WorkOrderRefNo,wo.OrderedQuantity OrderedQty,wo.WorkOrderStatus 'Status' from UINV_WorkOrder wo 
                            inner join UINV_WorkOrderDetails wd on wd.WorkOrderId=wo.Id
                            inner join Branch b on b.Id=wo.BranchId
                            inner join Organization o on b.OrganizationId=o.Id
                            inner join UINV_Item i on wd.ItemId=i.Id
                            where wo.[status]=1 and wd.status=1 and b.status=1 and o.status=1 and i.status=1 " + whereClause;

            return query;
        }

        #endregion

    }
}
