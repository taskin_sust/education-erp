﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UdvashERP.FingerPrint.BusinessRule
{
    public class PrintConstant
    {
        public static readonly string ConfigXmlFileName = "Config.xml";
        public static readonly string BasePath = Application.StartupPath;
        public static readonly string QuestionPathDir = "QuestionPdf";
        public static readonly string NewQuestionPathDir = "NewQuestionPdf";
        public static readonly string MargeQuestionPathDir = "MargeQuestionPdf";
        public static readonly string QuestionPath = BasePath + "\\" + QuestionPathDir;
        public static readonly string NewQuestionPath = BasePath + "\\" + NewQuestionPathDir;
        public static readonly string MargeQuestionPath = BasePath + "\\" + MargeQuestionPathDir;
        public static readonly string PdfFileExtension = ".pdf";

        public static readonly string OrganizationUrl = "LoadOrganization";
        public static readonly string ProgramUrl = "LoadProgram";
        public static readonly string SessionUrl = "LoadSession";
        public static readonly string CourseUrl = "LoadCourse";
        public static readonly string ExamUrl = "LoadExam";
        public static readonly string QuestionUrl = "LoadQuestion";
        public static readonly string QuestionAcknowledgementUrl = "UpdatePrintStatus";

    }
}
