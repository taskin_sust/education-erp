﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Administration
{
    public interface ISmsStudentRegistrationSettingDao : IBaseDao<SmsStudentRegistrationSetting, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<SmsStudentRegistrationSetting> LoadSmsStudentRegistrationSetting(int start, int length, List<long> authorizeProgramIdList, List<long> sessionIdList, List<long> courseIdList);

        #endregion

        #region Others Function

        long GetSmsStudentRegistrationSettingCount(List<long> authorizeProgramIdList, List<long> sessionIdList, List<long> courseIdList, string smsSettingStatus);

        SmsStudentRegistrationSetting GetRecentSmsStudentRegistrationSetting(long programId, long sessonId);

        bool IsDuplicate(SmsStudentRegistrationSetting srs);

        bool IsStudentRegistered(long programId, long courseId);

        #endregion

    }
    public class SmsStudentRegistrationSettingDao : BaseDao<SmsStudentRegistrationSetting, long>, ISmsStudentRegistrationSettingDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function


        #endregion

        #region List Loading Function

        public IList<SmsStudentRegistrationSetting> LoadSmsStudentRegistrationSetting(int start, int length, List<long> authorizeProgramIdList, List<long> sessionIdList,
            List<long> courseIdList)
        {
            ICriteria criteria = Session.CreateCriteria<SmsStudentRegistrationSetting>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", SmsStudentRegistrationSetting.EntityStatus.Delete)));
            criteria.CreateAlias("Program", "p")
                .Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Session", "s")
                .Add(Restrictions.Eq("s.Status", UdvashERP.BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("Course", "c")
                .Add(Restrictions.Eq("c.Status", Course.EntityStatus.Active));
            if (authorizeProgramIdList != null)
            {
                criteria.Add(Restrictions.In("p.Id", authorizeProgramIdList));
            }
            if (sessionIdList != null)
            {
                criteria.Add(Restrictions.In("s.Id", sessionIdList));
            }
            if (courseIdList != null)
            {
                criteria.Add(Restrictions.In("c.Id", courseIdList));
            }
            criteria.AddOrder(Order.Asc("StartDate"));
            criteria.SetFirstResult(start).SetMaxResults(length);
            return criteria.List<SmsStudentRegistrationSetting>().ToList();
        }

        public long GetSmsStudentRegistrationSettingCount(List<long> authorizeProgramIdList, List<long> sessionIdList, List<long> courseIdList,
            string smsSettingStatus)
        {
            ICriteria criteria = Session.CreateCriteria<SmsStudentRegistrationSetting>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", SmsStudentRegistrationSetting.EntityStatus.Delete)));
            criteria.CreateAlias("Program", "p")
                .Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Session", "s")
                .Add(Restrictions.Eq("s.Status", UdvashERP.BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("Course", "c")
                .Add(Restrictions.Eq("c.Status", Course.EntityStatus.Active));
            if (authorizeProgramIdList != null)
            {
                criteria.Add(Restrictions.In("p.Id", authorizeProgramIdList));
            }
            if (sessionIdList != null)
            {
                criteria.Add(Restrictions.In("s.Id", sessionIdList));
            }
            if (courseIdList != null)
            {
                criteria.Add(Restrictions.In("c.Id", courseIdList));
            }
            if (!string.IsNullOrEmpty(smsSettingStatus))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(smsSettingStatus)));
            }
            return Convert.ToInt64(criteria.SetProjection(Projections.RowCountInt64()).UniqueResult());
        }

        public SmsStudentRegistrationSetting GetRecentSmsStudentRegistrationSetting(long programId, long sessonId)
        {
            ICriteria criteria = Session.CreateCriteria<SmsStudentRegistrationSetting>();
            criteria.CreateAlias("Program", "p")
                .Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Session", "s")
                .Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("p.Id", programId));
            criteria.Add(Restrictions.Eq("s.Id", sessonId));
            criteria.Add(Restrictions.Le("StartDate", DateTime.Now.Date));
            criteria.Add(Restrictions.Ge("EndDate", DateTime.Now.Date));
            criteria.Add(Restrictions.Eq("Status", SmsStudentRegistrationSetting.EntityStatus.Active));
            return criteria.UniqueResult<SmsStudentRegistrationSetting>();
        }

        #endregion

        #region Others Function

        public bool IsDuplicate(SmsStudentRegistrationSetting srs)
        {
            IList<SmsStudentRegistrationSetting> srsDbList;
            if (srs.Id > 0)
            {
                srsDbList = Session.QueryOver<SmsStudentRegistrationSetting>().Where(x => x.Course.Id == srs.Course.Id && x.Id != srs.Id && x.Status != SmsStudentRegistrationSetting.EntityStatus.Delete).List<SmsStudentRegistrationSetting>();
                return srsDbList.Count > 0;
            }
            srsDbList = Session.QueryOver<SmsStudentRegistrationSetting>().Where(x => x.Course.Id == srs.Course.Id && x.Status != SmsStudentRegistrationSetting.EntityStatus.Delete).List<SmsStudentRegistrationSetting>();
            return srsDbList.Count > 0;
        }

        public bool IsStudentRegistered(long programId, long courseId)
        {
            string sql = @"select count(*) from StudentProgram sp inner join StudentCourseDetails scd on scd.StudentProgramId=sp.Id
                           inner join SmsStudentRegistrationSetting ssrs on ssrs.ProgramId=sp.ProgramId and ssrs.CourseId=scd.CourseId
                           inner join SmsRegistrationReceiverLog srsl on srsl.StudentProgramId=sp.Id
                           and ssrs.ProgramId=" + programId + " and ssrs.CourseId=" + courseId;
            IQuery iQuery = Session.CreateSQLQuery(sql);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count > 0;
        }

        #endregion

        #region Helper Function

        #endregion
    }
}