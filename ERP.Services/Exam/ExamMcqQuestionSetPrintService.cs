﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Event;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.Exam;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessRules.Exam;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Exam;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using ExamQuestionSetPrintApiObj = UdvashERP.BusinessModel.Dto.Exam.ExamQuestionSetPrintApiObj;
using InvalidDataException = System.IO.InvalidDataException;

namespace UdvashERP.Services.Exam
{
    public interface IExamMcqQuestionSetPrintService : IBaseService
    {
        #region Operational Function

        void Save(List<ExamMcqQuestionSetPrint> examMcqQuestionSetPrintList);
        void Update(long examMcqQuestionSetPrintId,int printStatus);
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<ExamMcqQuestionSetPrint> GetExamMcqQuestionSetPrintReportList(int start, int length, List<UserMenu> _userMenu, List<long> convertedOrganizationIdList, List<long> convertedProgramIdList, List<long> convertedSessionIdList, List<long> convertedCourseIdList, string examOrCode, int? uniqueRes, string codeNo, int? printStat);
        IList<ExamMcqQuestionSetPrint> LoadExamMcqQuestionSetPrintList(long examId); 

        #endregion

        #region Other Functions

        int GetExamMcqQuestionSetPrintReportCount(List<UserMenu> authorizeMenu, List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, string examOrCode, int? uniqueResult, string codeNo, int? printStatus);
        ExamQuestionSetPrintApiDto GetExamQuestionSetPrintApiDto(string printBy, string server, long examId, int examVersion, int totalPrint, string uniqueSets);  

        #endregion

        #region Helper Function


        #endregion

        
    }
    public class ExamMcqQuestionSetPrintService : BaseService, IExamMcqQuestionSetPrintService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("ExamService");

        #endregion

        #region Propertise & Object Initialization

        private List<long> _authorizeOrganizationIdList, _authorizeProgramIdList, _authSessionIdList, _authCourseIdList;
        private readonly ISessionDao _sessionDao;
        private readonly ICourseDao _courseDao;
        private readonly IExamMcqQuestionSetPrintDao _examMcqQuestionSetPrintDao;
        private readonly IExamMcqQuestionSetDao _examMcqQuestionSetDao;
        private readonly IExamsDao _examsDao;

        public ExamMcqQuestionSetPrintService(ISession session)
        {
            Session = session;
            _sessionDao = new SessionDao() { Session = session };
            _courseDao = new CourseDao() { Session = session };
            _examMcqQuestionSetPrintDao = new ExamMcqQuestionSetPrintDao() { Session = session };
            _examMcqQuestionSetDao = new ExamMcqQuestionSetDao() { Session = session };
            _examsDao = new ExamsDao() { Session = session };
        }

        #endregion

        #region Operational Function
        public void Save(List<ExamMcqQuestionSetPrint> examMcqQuestionSetPrintList)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSave(examMcqQuestionSetPrintList); 

                using (transaction = Session.BeginTransaction())
                {
                    foreach (var examMcqQuestionSetPrint in examMcqQuestionSetPrintList) 
                    {
                        ExamMcqQuestionSetPrint obj = examMcqQuestionSetPrint;
                        obj.Status = ExamMcqQuestionSetPrint.EntityStatus.Active;
                        _examMcqQuestionSetPrintDao.Save(obj);
                    }
                    transaction.Commit();
                }

            }
            catch (NullObjectException)
            {
                throw;
            }           
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void Update(long examMcqQuestionSetPrintId, int printStatus)
        {
            try
            {
                var examMcqQuestionSetPrint = _examMcqQuestionSetPrintDao.LoadById(examMcqQuestionSetPrintId);
                if (examMcqQuestionSetPrint==null)
                {
                    throw new InvalidDataException("Exam Mcq Question Set Print Not found");
                }
                examMcqQuestionSetPrint.PrintStatus = printStatus;
                var examMcqQuestionSetPrintList = new List<ExamMcqQuestionSetPrint> {examMcqQuestionSetPrint};
                Save(examMcqQuestionSetPrintList);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<ExamMcqQuestionSetPrint> GetExamMcqQuestionSetPrintReportList(int start, int length, List<UserMenu> authorizeMenu, List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, string examOrCode, int? uniqueResult, string codeNo, int? printStatus)
        {
            try
            {
                LoadAuthorizedList(authorizeMenu, organizationIdList, programIdList, sessionIdList, courseIdList);
                var examMcqQuestionSetPrintReportList = _examMcqQuestionSetPrintDao.GetExamMcqQuestionSetPrintReportList(start, length, _authorizeProgramIdList, _authSessionIdList, _authCourseIdList,examOrCode, uniqueResult, codeNo, printStatus);
                return examMcqQuestionSetPrintReportList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ExamMcqQuestionSetPrint> LoadExamMcqQuestionSetPrintList(long examId)
        {
            try
            {
                return _examMcqQuestionSetPrintDao.LoadExamMcqQuestionSetPrintList(examId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Other Functions

        public int GetExamMcqQuestionSetPrintReportCount(List<UserMenu> authorizeMenu, List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, string examOrCode, int? uniqueResult, string codeNo, int? printStatus)
        {
            try
            {
                LoadAuthorizedList(authorizeMenu, organizationIdList, programIdList, sessionIdList, courseIdList);
                return _examMcqQuestionSetPrintDao.GetExamMcqQuestionSetPrintReportCount(_authorizeProgramIdList, _authSessionIdList, _authCourseIdList, examOrCode, uniqueResult, codeNo, printStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public ExamQuestionSetPrintApiDto GetExamQuestionSetPrintApiDto(string printBy, string server, long examId, int examVersion, int totalPrint, string uniqueSets)
        {
            try
            {
                var examQuestionSetPrintApiDtos = new ExamQuestionSetPrintApiDto();
                var examMcqQuestionSetPrintList = new List<ExamMcqQuestionSetPrint>();  
                
                var examMcqQuestionSets = _examMcqQuestionSetDao.LoadExamMcqQuestionSet(examId, examVersion, uniqueSets);
                var exam = _examsDao.LoadById(examId);

                if(exam==null) 
                    throw new MessageException("Exam Not found");
                if (!examMcqQuestionSets.Any())
                    throw new MessageException("Question Set Not found");

                string link = server + "Question/" + examId + "/" + examVersion + "/Question_bn.zip";
                if (examVersion == (int)ExamVersion.English)
                {
                    link = server + "Question/" + examId + "/" + examVersion + "/Question_en.zip";
                }

                examQuestionSetPrintApiDtos.QuestionSetZipPath = link;

                var maxCodeNo = 100000000001;
                
               
                var objWithMaxCodeNo = _examMcqQuestionSetPrintDao.GetExamMcqQuestionSetPrint();

                if (objWithMaxCodeNo != null)
                {
                    maxCodeNo = Convert.ToInt64(objWithMaxCodeNo.CodeNo) + 1;
                    //initialSetId = examMcqQuestionSets.Count== objWithMaxCodeNo.SetId ? 1 : objWithMaxCodeNo.SetId + 1;
                }

                

                var numbers = uniqueSets.Split(new char[]{','},StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToArray();
               
                var objWithMaxSetId = _examMcqQuestionSetPrintDao.GetExamMcqQuestionSetPrint(numbers);
                var firstQuestionSet = examMcqQuestionSets.FirstOrDefault();
                var lastQuestionSet = examMcqQuestionSets.LastOrDefault();
                var initialSetId = firstQuestionSet.SetId; 
                if (objWithMaxSetId != null)
                {
                    initialSetId = lastQuestionSet.SetId == objWithMaxSetId.SetId ? firstQuestionSet.SetId : objWithMaxSetId.SetId + 1;
                }
                var j = firstQuestionSet.SetId;


                for (int i = 0; i < totalPrint;)
                {

                    foreach (var emqs in examMcqQuestionSets)
                    {
                        j = emqs.SetId;
                        if (i == 0 && j < initialSetId)
                        {
                            continue; // Continue for last generated setId
                        }

                        var examMcqQuestionSetPrint = new ExamMcqQuestionSetPrint();
                        var codeNo = maxCodeNo++;

                        examMcqQuestionSetPrint.CodeNo = codeNo;                  
                        examMcqQuestionSetPrint.UniqueSet = emqs.UniqueSet;
                        examMcqQuestionSetPrint.SetId = emqs.SetId;
                        examMcqQuestionSetPrint.PrintDate = DateTime.Now;
                        examMcqQuestionSetPrint.PrintStatus = (int)PrintStatus.NotPrinted;
                        examMcqQuestionSetPrint.Exam = exam;
                        examMcqQuestionSetPrint.SetNo = emqs.SetNo;
                        examMcqQuestionSetPrint.PrintBy = Convert.ToInt64(printBy);
                        examMcqQuestionSetPrintList.Add(examMcqQuestionSetPrint);
                       
                        i = i + 1;
                        if (i == totalPrint)
                        {
                            break;
                        }
                    }
                }

                Save(examMcqQuestionSetPrintList);


                var  examQuestionSetPrintApiObjects = examMcqQuestionSetPrintList.Select(emqspl => new ExamQuestionSetPrintApiObj
                {
                    Id = emqspl.Id, 
                    CodeNo = emqspl.CodeNo, 
                    UniqueSet = emqspl.UniqueSet, 
                    SetId = emqspl.SetId, 
                    PrintDate = emqspl.PrintDate, 
                    PrintStatus = emqspl.PrintStatus, 
                    ExamId = emqspl.Exam.Id
                }).ToList();



                examQuestionSetPrintApiDtos.ExamQuestionSetPrintApiObjects = examQuestionSetPrintApiObjects;
                return examQuestionSetPrintApiDtos;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        

        #endregion

        #region Helper Function

        private void LoadAuthorizedList(List<UserMenu> authorizeMenu, List<long> organizationIdList, List<long> programIdList,
            List<long> sessionIdList, List<long> courseIdList)
        {
            _authorizeOrganizationIdList = AuthHelper.LoadOrganizationIdList(authorizeMenu, organizationIdList);
            _authorizeProgramIdList = AuthHelper.LoadProgramIdList(authorizeMenu, _authorizeOrganizationIdList, null, programIdList);
            //load session
            _authSessionIdList = new List<long>();
            IList<Session> authSessionList = _sessionDao.LoadAuthorizedSession(_authorizeProgramIdList, null, null, null);
            if (sessionIdList != null && sessionIdList.Count > 0)
            {
                authSessionList =
                    authSessionList.Where(x => sessionIdList.Contains(x.Id)).ToList();
            }
            _authSessionIdList = authSessionList.Select(x => x.Id).ToList();
            //load course
            IList<Course> courseList = new List<Course>();
            courseList = _courseDao.LoadCourse(organizationIdList, programIdList, sessionIdList);
            _authCourseIdList = new List<long>();
            if (courseIdList != null && courseIdList.Count > 0)
            {
                courseList =
                    courseList.Where(x => courseIdList.Contains(x.Id)).ToList();
            }
            _authCourseIdList =
                    courseList.Select(x => x.Id).ToList();
        }

        private void CheckBeforeSave(List<ExamMcqQuestionSetPrint> examMcqQuestionSetPrintList)
        {
            if (examMcqQuestionSetPrintList == null)
                throw new NullObjectException("Question Set Print List is empty");
            if (!examMcqQuestionSetPrintList.Any())
                throw new NullObjectException("Question Set Print  List is empty");
           

            foreach (var emqsp in examMcqQuestionSetPrintList)  
            {
                if (!(emqsp.PrintStatus == (int)PrintStatus.NotPrinted || emqsp.PrintStatus == (int)PrintStatus.Printed))
                {
                    throw new InvalidDataException("Invalid Print Status");
                }
                CheckValidation(emqsp);
            }
        }

        private void CheckValidation(ExamMcqQuestionSetPrint examMcqQuestionSetPrint)
        {
            var validationResult = ValidationHelper.ValidateEntity<ExamMcqQuestionSetPrint, ExamMcqQuestionSetPrint>(examMcqQuestionSetPrint);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        #endregion
    }
}



