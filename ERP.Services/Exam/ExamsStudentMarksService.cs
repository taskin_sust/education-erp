﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Exam;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Exam
{
    public interface IExamsStudentMarksService : IBaseService
    {
        #region Operational Function

        bool Update(ExamsStudentMarks smd);
        bool Save(ExamsStudentMarks smd);
        bool deleteExamMarksByStudent(IList<ExamsStudentMarks> studentmarksList);
        bool Delete(ExamsStudentMarks iae);
        bool IsMeritListDeleteSuccess(List<UserMenu> _userMenu, long[] selectedExam, string studentProgramRoll);
        bool ClearMarksStudentList(List<UserMenu> userMenu, long programId, long sessionId, string gender, long[] branchIds, long[] campusIds,
            string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long examId, long examType);

        void SaveMarksUploadRawData(ExamsStudentMarksUploadRawData examsStudentMarksUploadRawData);
      
        #endregion

        #region Single Instances Loading Function
        ExamsStudentMarks LoadExamsStudentMarksByExamsDetailsStudentProgram(long examsDetailsId, long studentProgramId);

        #endregion

        #region List Loading Function

        int GetExamResultListCount(List<UserMenu> userMenu, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn = "");
        int GetExamResultListCountForClearMarks(List<UserMenu> userMenu, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn = "");
        IList<ExamMeritListDto> GetExamResultList(List<UserMenu> userMenu, long[] selectedExam, int start, int length, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn = "", int totalRow= -1);
        IList<ExamsStudentMarks> LoadExamsStudentMarksByExamStudentProgram(long examId, long studentProgramId, int examType);
        IList<ExamsStudentMarks> GetExamMarksByProgramId(long examId, long studentProgramId, int examType);
        IList<ExamStudentMarksDto> GetExamTotalResultList(List<UserMenu> userMenu, long programId, long sessionId, long selectedExamType, long examId);
        List<ExamsStudentMarksUploadRawData> LoadExamsSudentMarksUploadData(int draw, int start, int length, string orderBy, string p, string markdUploadLogText);

        #endregion

        #region Others Function

        bool isAlreadyExistMarks(long examId, long studentProgramId, int examType);
        bool IsMeritListDeleteSuccess(List<UserMenu> _userMenu, long[] selectedExam);
        int CountClearMarksStudent(List<UserMenu> userMenu, long programId, long sessionId, string gender, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long examId, long examType);
        int CountStudentForMeritListSendSms(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, long[] courseIds, long[] branchIds, 
            long[] campusIds,string[]batchDays, string[] batchTimes, long[] batchIds, long examId, int examType, int gender);
        int GetMobileCount(List<UserMenu> userMenu, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, int[] smsReciever, List<ServiceBlock> serviceBlocks);
        bool ClearExamMarksOfStudents(List<UserMenu> userMenu, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn = "");
        int CountExamsSudentMarksUploadData(string markdUploadLogText);
        
        #endregion
        
    }
    public class ExamsStudentMarksService : BaseService, IExamsStudentMarksService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamService");
        #endregion

        #region Properties & Object & Initialization
        private readonly IExamsStudentMarksDao _examsStudentMarksDao;
        private readonly IComplementaryCourseDao _complementaryCourseDao;

        private CommonHelper _commonHelper;
        public ExamsStudentMarksService(ISession session)
        {
            Session = session;
            _examsStudentMarksDao = new ExamsStudentMarksDao() { Session = Session };
            _commonHelper = new CommonHelper();
            _complementaryCourseDao = new ComplementaryCourseDao() { Session = Session };
        }

        #endregion

        #region Operational Function
        public bool Update(ExamsStudentMarks smd)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        _examsStudentMarksDao.Update(smd);
                        trans.Commit();
                        return true;
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool Save(ExamsStudentMarks smd)
        {
            try
            {
                ExamsStudentMarks isAlreadyExistMarks = _examsStudentMarksDao.LoadExamsStudentMarksByExamsDetailsStudentProgram(smd.ExamsDetails.Id, smd.StudentProgram.Id);
                if (isAlreadyExistMarks == null)
                {
                    _examsStudentMarksDao.Save(smd);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public bool Delete(ExamsStudentMarks iae)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        _examsStudentMarksDao.Delete(iae);
                        trans.Commit();
                        return true;
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                    }
                }
                return false;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public bool IsMeritListDeleteSuccess(List<UserMenu> _userMenu, long[] selectedExam)
        {
            IList<ExamsStudentMarks> examsStudentMarks = _examsStudentMarksDao.GetExamResultList(selectedExam);
            using (var trans = Session.BeginTransaction())
            {
                try
                {
                    foreach (var examStdMarks in examsStudentMarks)
                    {
                        _examsStudentMarksDao.Delete(examStdMarks);
                    }
                    trans.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    trans.Rollback();
                    throw ex;
                }
            }
        }
        public bool IsMeritListDeleteSuccess(List<UserMenu> _userMenu, long[] selectedExam, string studentProgramRoll)
        {
            IList<ExamsStudentMarks> examsStudentMarks = _examsStudentMarksDao.GetExamResultList(selectedExam, studentProgramRoll);
            using (var trans = Session.BeginTransaction())
            {
                try
                {
                    foreach (var examStdMarks in examsStudentMarks)
                    {
                        _examsStudentMarksDao.Delete(examStdMarks);
                    }
                    trans.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    trans.Rollback();
                    throw ex;
                }
            }
        }
        public bool ClearMarksStudentList(List<UserMenu> userMenu, long programId, long sessionId, string gender, long[] branchIds,
            long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long examId,
            long examType)
        {
            List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
            List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
            IList<ExamsStudentMarks> esm = _examsStudentMarksDao.ClearMarksStudentList(programIdList, branchIdList, programId, sessionId, gender, branchIds, campusIds, batchDays, batchTimes, batchIds, courseIds, examId, examType);
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        foreach (var sml in esm)
                        {
                            _examsStudentMarksDao.Delete(sml);
                        }
                        trans.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        trans.Rollback();
                        throw ex;
                    }
                }
                return false;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public void SaveMarksUploadRawData(ExamsStudentMarksUploadRawData examsStudentMarksUploadRawData)
        {
            try
            {
                examsStudentMarksUploadRawData.CreationDate = DateTime.Now;
                examsStudentMarksUploadRawData.CreateBy = GetCurrentUserId();
                Session.Save(examsStudentMarksUploadRawData);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }
        }


        public bool deleteExamMarksByStudent(IList<ExamsStudentMarks> studentmarksList)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        foreach (var sml in studentmarksList)
                        {
                            _examsStudentMarksDao.Delete(sml);
                        }
                        trans.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        trans.Rollback();
                        throw ex;
                    }
                }
                return false;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public ExamsStudentMarks LoadExamsStudentMarksByExamsDetailsStudentProgram(long examsDetailsId, long studentProgramId)
        {
            try
            {
                return _examsStudentMarksDao.LoadExamsStudentMarksByExamsDetailsStudentProgram(examsDetailsId, studentProgramId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function

        public IList<ExamsStudentMarks> LoadExamsStudentMarksByExamStudentProgram(long examId, long studentProgramId, int examType)
        {
            try
            {
                return _examsStudentMarksDao.LoadExamsStudentMarksByExamStudentProgram(examId, studentProgramId, examType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }

        }
        public IList<ExamsStudentMarks> GetExamMarksByProgramId(long examId, long studentProgramId, int examType)
        {
            try
            {
                return _examsStudentMarksDao.GetExamMarksByProgramId(examId, studentProgramId, examType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<ExamStudentMarksDto> GetExamTotalResultList(List<UserMenu> userMenu, long programId, long sessionId, long selectedExamType, long examId)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                return _examsStudentMarksDao.GetExamTotalResultList(branchIdList, programIdList,programId, sessionId, selectedExamType, examId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<ExamsStudentMarksUploadRawData> LoadExamsSudentMarksUploadData(int draw, int start, int length, string orderBy, string orderDir, string markdUploadLogText)
        {
            try
            {
                return _examsStudentMarksDao.LoadExamsSudentMarksUploadData(draw, start, length, orderBy, orderDir, markdUploadLogText);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ExamMeritListDto> GetExamResultList(List<UserMenu> userMenu, long[] selectedExam, int start, int length, string selectedGenerateBy,
            string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds,
            string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn = "", int totalRow = -1)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<long> mainCourseList = _complementaryCourseDao.LoadMainCourse(courseIds);

                //var courseIdList = courseIds.ToList().AddRange(mainCourseList);
                var courseIdList = new List<long>(courseIds.Count() + mainCourseList.Count);
                courseIdList.AddRange(courseIds.ToList());
                courseIdList.AddRange(mainCourseList);
                 

                return _examsStudentMarksDao.GetExamResultList(programIdList, branchIdList, start, length, selectedExam, selectedGenerateBy, selectedGender,
                    selectedOrderBy, programId, sessionId, courseIdList.ToArray(), branchIds, campusIds, selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, serviceBlocks, prn, totalRow);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }


        }
        
        #endregion

        #region Others Function

        public int CountClearMarksStudent(List<UserMenu> userMenu, long programId, long sessionId, string gender, long[] branchIds,
            long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long examId,
            long examType)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<ServiceBlock> serviceBlocks = new List<ServiceBlock>();
                List<Batch> batches = new List<Batch>();
                return _examsStudentMarksDao.GetExamResultListCount(programIdList, branchIdList,_commonHelper.ConvertIdToList(examId).ToArray(), null,gender, null,
                    programId, sessionId, courseIds, branchIds, campusIds, examType.ToString(), "0", "1", batches,serviceBlocks, null);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int CountStudentForMeritListSendSms(List<UserMenu> userMenu,long organizationId, long programId, long sessionId, long[] courseIds,
            long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long examId, int examType, int gender)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                return _examsStudentMarksDao.CountStudentForMeritListSendSms(programIdList, branchIdList, organizationId, programId, sessionId, courseIds, branchIds,
                    campusIds,batchDays, batchTimes, batchIds, examId, examType, gender);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool isAlreadyExistMarks(long examId, long studentProgramId, int examType)
        {
            try
            {
                return _examsStudentMarksDao.isAlreadyExistMarks(examId, studentProgramId, examType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetMobileCount(List<UserMenu> userMenu, long[] selectedExam, string selectedGenerateBy,
            string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds,
            long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType,
            string selectedMeritBy, List<Batch> batchList, int[] smsReciever, List<ServiceBlock> serviceBlocks)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                var mobileCount= _examsStudentMarksDao.GetMobileCount(programIdList, branchIdList, selectedExam, selectedGenerateBy, selectedGender, selectedOrderBy,
                    programId, sessionId, courseIds, branchIds, campusIds, selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, smsReciever, serviceBlocks);

                return mobileCount;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool ClearExamMarksOfStudents(List<UserMenu> userMenu, long[] selectedExam, string selectedGenerateBy, string selectedGender,
            string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds,
            string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks,
            string prn = "")
        {
            IList<ExamStudentClearMarksDto> esm = _examsStudentMarksDao.ClearExamMarksOfStudents(userMenu, selectedExam, selectedGenerateBy, selectedGender,
             selectedOrderBy, programId, sessionId, courseIds, branchIds, campusIds,selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, serviceBlocks,prn);
            try
            {
                if (esm.Count > 0)
                {

                    var esMarksIds = esm.Select(x => x.Id).ToArray();
                    var ids = string.Join(",", esMarksIds);
                    _examsStudentMarksDao.DeleteClearMarks(ids);
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }


        public int GetExamResultListCount(List<UserMenu> userMenu, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn = "")
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<long> mainCourseList = _complementaryCourseDao.LoadMainCourse(courseIds);

                //var courseIdList = courseIds.ToList().AddRange(mainCourseList);
                //List<long> courseIdList = new List<long>(courseIds.Count() + mainCourseList.Count);
                List<long> courseIdList = new List<long>();
                courseIdList.AddRange(courseIds.ToList());
                courseIdList.AddRange(mainCourseList);
                courseIdList = courseIdList.Distinct().ToList();
                return _examsStudentMarksDao.GetExamResultListCount(programIdList, branchIdList, selectedExam, selectedGenerateBy, selectedGender, selectedOrderBy,
                    programId, sessionId, courseIdList.ToArray(), branchIds, campusIds, selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, serviceBlocks, prn);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetExamResultListCountForClearMarks(List<UserMenu> userMenu, long[] selectedExam, string selectedGenerateBy, string selectedGender, string selectedOrderBy, long programId, long sessionId, long[] courseIds, long[] branchIds, long[] campusIds, string selectedExamType, string selectedAttendanceType, string selectedMeritBy, List<Batch> batchList, List<ServiceBlock> serviceBlocks, string prn = "")
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<long> mainCourseList = _complementaryCourseDao.LoadMainCourse(courseIds);

                //var courseIdList = courseIds.ToList().AddRange(mainCourseList);
                var courseIdList = new List<long>(courseIds.Count() + mainCourseList.Count);
                courseIdList.AddRange(courseIds.ToList());
                courseIdList.AddRange(mainCourseList);
                return _examsStudentMarksDao.GetExamResultListCountForClearMarks(programIdList, branchIdList, selectedExam, selectedGenerateBy, selectedGender, selectedOrderBy,
                    programId, sessionId, courseIdList.ToArray(), branchIds, campusIds, selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, serviceBlocks, prn);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }


        public int CountExamsSudentMarksUploadData(string markdUploadLogText)
        {
            try
            {
                return _examsStudentMarksDao.CountExamsSudentMarksUploadData(markdUploadLogText);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion
    }
}
