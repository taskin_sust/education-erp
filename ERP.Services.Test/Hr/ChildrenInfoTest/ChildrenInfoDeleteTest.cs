﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ChildrenInfoTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ChildrenInfo")]
    public class ChildrenInfoDeleteTest : ChildrenInfoBaseTest
    {
        [Fact]
        public void Delete_Should_Return_True()
        {
            var teamMemberObj = PersistTeamMember();
            var objFactory = ChildrenInfoFactory.Create();
            var entity = new TeammemberChildrenArray
            {
                Name = objFactory.Object.Name,
                Gender = objFactory.Object.Gender.ToString(),
                Studying = "1",
                Dob = objFactory.Object.Dob.ToString(),
                Status = objFactory.Object.Status.ToString(CultureInfo.CurrentCulture),
                Remarks = objFactory.Object.Remarks
            };
            var arrayList = new List<TeammemberChildrenArray> { entity };
            var ids = ChildrenInfoService.UpdateChildrenInfo(teamMemberObj, arrayList);
            objFactory.Object.Id = ids[0];
            Assert.True(ChildrenInfoService.DeleteChildrenInfo(objFactory.Object.Id));
        }
    }
}
