﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;

namespace UdvashERP.Services.Students
{
    public interface IStudentImageService : IBaseService
    {
        #region Operational Function
        void Delete(string programRoll, List<UserMenu> userMenu);
        #endregion

        #region Single Instances Loading Function
        StudentProgramImage GetStudentProgramImage(long studentProgramImageId);
        #endregion

        #region List Loading Function
        IList<MissingExistingImageDto> LoadImageReports(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string[] batchDays, string[] batchTime, bool isMissingImage);
        #endregion

        #region Others Function
        string FileUpload(HttpPostedFileBase upl, bool overwrite, List<UserMenu> userMenuList);
        string FileUpload(string programRoll, HttpPostedFileBase upl, bool overwrite, List<UserMenu> userMenuList);
        string CheckFileSettingsForTypeAndSize(string name, string contentType, int size);       
        int ImageCount(List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList, bool isMissingImage);
        int LoadImageReportsRowCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList, bool isMissingImage);
        #endregion

        #region Clean
        //int ImageMissingCount(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList);

       // List<StudentImageMissing> LoadMissingImageReports(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string[] batchDays, string[] batchId);
       // List<StudentImageMissing> LoadExistingImageReports(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string[] batchDays, string[] batchId);
       // int ImageExistingCount(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList);
        
        #endregion
    }

    public class StudentImageService : BaseService, IStudentImageService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IStudentImageDao _studentImageDao;
        private readonly IStudentDao _studentDao;
        private readonly IBranchService _branchService;
        private readonly IProgramService _programService;
        private readonly ICommonHelper _commonHelper;
        private readonly IStudentImagesMediaService _studentImagesMediaService;
        private ISession _sessionMedia;
        public StudentImageService(ISession session)
        {
            Session = session;
            _sessionMedia = NHibernateMediaSessionFactory.OpenSession();
            _studentProgramDao = new StudentProgramDao() { Session = Session };
            _studentImageDao = new StudentImageDao() { Session = Session };
            _studentDao = new StudentDao() { Session = session };
            _branchService = new BranchService(session) { Session = session };
            _programService = new ProgramService(session) { Session = session };
            _commonHelper = new CommonHelper();
            _studentImagesMediaService = new StudentImagesMediaService();

        }

        public StudentImageService()
        {

        }
        #endregion

        #region Operational Function
        public void Delete(string programRoll, List<UserMenu> userMenu)
        {
            try
            {
                StudentProgram studentProgram = _studentProgramDao.GetStudentProgram(programRoll);
                IList<Branch> authorizedBranchList = _branchService.LoadAuthorizedBranch(userMenu);
                if (authorizedBranchList == null || authorizedBranchList.Count < 1)
                    throw new MessageException("You are not authorized to access this student info");


                long branchId = (from x in authorizedBranchList where x.Id == studentProgram.Batch.Branch.Id select x.Id).FirstOrDefault();
                if (branchId < 1)
                    throw new MessageException("You are not authorized to access this student info");


                IList<Program> authorizedProgramList = _programService.LoadAuthorizedProgram(userMenu);

                if (authorizedProgramList == null || authorizedProgramList.Count < 1)
                    throw new MessageException("You are not authorized to access this student info");

                long programId =
                    (from x in authorizedProgramList where x.Id == studentProgram.Program.Id select x.Id)
                        .FirstOrDefault();
                if (programId < 1)
                    throw new MessageException("You are not authorized to access this student info");

                //StudentImage si = _studentImageDao.GetStudentImage(programRoll); 
                using (var tranacition = Session.BeginTransaction())
                {
                    try
                    {
                        _studentImagesMediaService.Delete(studentProgram);

                        studentProgram.IsImage = false;
                        studentProgram.StudentImage.Clear();
                        _studentProgramDao.Update(studentProgram);
                        tranacition.Commit();
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        tranacition.Rollback();
                    }
                }
                //studentProgram.StudentImage.Clear();
                //_studentProgramDao.Update(studentProgram);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        #endregion

        #region Single Instances Loading Function


        public StudentProgramImage GetStudentProgramImage(long studentProgramImageId)
        {
            try
            {
                return _studentImageDao.LoadById(studentProgramImageId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function



        public IList<MissingExistingImageDto> LoadImageReports(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList,
           string[] batchDaysList, string[] batchTimeList, bool isMissingImage)
        {

            IList<MissingExistingImageDto> imageDataList = new List<MissingExistingImageDto>();
            try
            {

                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                imageDataList = _studentProgramDao.LoadImageReports(start, length, branchIdList, programIdList, programId, sessionId, branchId, campusId, batchList, batchDaysList, batchTimeList, isMissingImage);
                return imageDataList;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return imageDataList;
        }
        #endregion

        #region Others Function
        public string FileUpload(HttpPostedFileBase upl, bool overwrite, List<UserMenu> userMenuList)
        {
            return FileUpload(null, upl, overwrite, userMenuList);
        }
        public string FileUpload(string programRoll, HttpPostedFileBase upl, bool overwrite, List<UserMenu> userMenuList)
        {
            try
            {
                if (upl != null)
                {
                    string sizeCheckerMessage = CheckFileSettingsForTypeAndSize(upl.FileName, upl.ContentType, upl.ContentLength);
                    if (sizeCheckerMessage != null && sizeCheckerMessage.Length > 1)
                        return sizeCheckerMessage;

                    var contentType = upl.ContentType;
                    var imageFile = ConvertFileInByteArray(upl.InputStream, upl.ContentLength);
                    var currentImageHash = ComputeHash(imageFile);
                    StudentProgramImage duplicateImage = null;

                    //Check contentType for image
                    if (contentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                    {
                        string proNo = "";

                        if (programRoll != null && programRoll.Length == 11)
                            proNo = programRoll;
                        else
                            proNo = upl.FileName.Substring(0, upl.FileName.LastIndexOf(".", StringComparison.Ordinal));

                        //Check if this student is under authorizied branch to this AppUser  
                        //load student by ProgramRollNo
                        StudentProgram studentProgram = _studentProgramDao.GetStudentProgram(proNo);
                        if (studentProgram == null || studentProgram.Id <= 0)
                            return "Program roll no. is not matching";


                        IList<Branch> authorizedBranchList = _branchService.LoadAuthorizedBranch(userMenuList);
                        if (authorizedBranchList == null || authorizedBranchList.Count < 1)
                            return "You are not authorized to access this student info";


                        long branchId = (from x in authorizedBranchList where x.Id == studentProgram.Batch.Branch.Id select x.Id).FirstOrDefault();
                        if (branchId < 1)
                            return "You are not authorized to access this student info";


                        IList<Program> authorizedProgramList = _programService.LoadAuthorizedProgram(userMenuList);

                        if (authorizedProgramList == null || authorizedProgramList.Count < 1)
                            return "You are not authorized to access this student info";

                        long programId =
                            (from x in authorizedProgramList where x.Id == studentProgram.Program.Id select x.Id)
                                .FirstOrDefault();
                        if (programId < 1)
                            return "You are not authorized to access this student info";

                        //Check is it Different Student Image. Load existing image by its Hash
                        IList<StudentProgramImage> tmpAnynomousStudentImageList = _studentImageDao.ImageAlreadyExists(currentImageHash);
                        StudentProgramImage studentImage = null;
                        //Don't Allow when it is DifferentStudent image
                        foreach (var tmpDbImage in tmpAnynomousStudentImageList)
                        {
                            if (tmpDbImage.StudentProgram.Student.Id != studentProgram.Student.Id)
                                return "This is other student's image. Which already exists. [ Student Program Roll is " + tmpDbImage.StudentProgram.PrnNo + " ]";
                        }
                        //--------------------RULE------------------------
                        //Allow when it is NewImage
                        //Allow when SameStudent's SameImage to DifferentProgram
                        //Dont't Allow when SameStudent's SameImage at SameProgram
                        //Don't Allow when it is DifferentStudent image

                        //Allow when it is NewImage- which not exists in database. 
                        bool isDuplicateImage = tmpAnynomousStudentImageList.Count > 0;

                        if (isDuplicateImage)
                        {
                            //Dont't Allow when SameStudent's SameImage at SameProgram
                            if (studentProgram.StudentImage == null || studentProgram.StudentImage.Count < 1)
                            {
                                isDuplicateImage = false;
                            }
                            else
                            {
                                foreach (var oldImage in studentProgram.StudentImage)
                                {
                                    if (currentImageHash == oldImage.Hash && studentProgram.Id == oldImage.StudentProgram.Id)
                                    {
                                        isDuplicateImage = true;
                                        duplicateImage = oldImage;
                                        break;
                                    }
                                    //Allow when SameStudent's SameImage to DifferentProgram
                                    else
                                    {
                                        isDuplicateImage = false;
                                    }
                                }
                            }
                        }


                        if (!isDuplicateImage || overwrite)
                        {
                            using (var tranacition = Session.BeginTransaction())
                            {
                                try
                                {
                                    if (!isDuplicateImage)
                                    {
                                        if (studentProgram.StudentImage != null) studentProgram.StudentImage.Clear();
                                        studentImage = new StudentProgramImage()
                                       {
                                           //ImageIGuid = Guid.NewGuid(),
                                           //Images = imageFile,
                                           Hash = currentImageHash,
                                           StudentProgram = studentProgram,
                                       };
                                        //studentProgram.StudentImage.Add(studentImage);
                                        studentProgram.IsImage = true;
                                        _studentProgramDao.Update(studentProgram);
                                        _studentImageDao.Save(studentImage);
                                        using (var transicationMedia = _sessionMedia.BeginTransaction())
                                        {
                                            StudentMediaImage mediaStudentImages = new StudentMediaImage()
                                            {
                                                CreationDate = DateTime.Now,
                                                ModificationDate = DateTime.Now,
                                                Status = StudentProgram.EntityStatus.Active,
                                                CreateBy = studentProgram.CreateBy,
                                                ModifyBy = studentProgram.ModifyBy,
                                                ImageGuid = Guid.NewGuid(),
                                                Images = imageFile,
                                                ImageRef = studentImage.Id
                                            };
                                            _sessionMedia.Save(mediaStudentImages);
                                            transicationMedia.Commit();
                                            
                                        }
                                        tranacition.Commit();
                                        Session.Clear();
                                        return "Success";
                                    }
                                    if (overwrite)
                                    {
                                        using (var tranacition2 = Session.BeginTransaction())
                                        {
                                            if (duplicateImage != null && duplicateImage.Id > 0)
                                                //0studentProgram.StudentImage.Add(duplicateImage);
                                                _studentImageDao.Update(duplicateImage);
                                            tranacition2.Commit();
                                        }
                                        return "Updated";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var delObjSecondDb = _sessionMedia.QueryOver<StudentMediaImage>()
                                        .Where(x => x.ImageRef == studentImage.Id)
                                        .SingleOrDefault<StudentMediaImage>();
                                    {
                                        var trans = _sessionMedia.BeginTransaction();
                                        _sessionMedia.Delete(delObjSecondDb);
                                        trans.Commit();
                                    }
                                    throw;
                                }

                            }
                        }
                        else
                        {
                            return "Duplicate";
                        }
                    }
                    return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
                }
                return "File is missing";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "Failed to upload";
            }
        }

        public string CheckFileSettingsForTypeAndSize(string name, string contentType, int size)
        {
            try
            {
                if (name == null || contentType == null || size < 1)
                    return "This is not valid file. Check the file extension.";

                if (contentType.ToUpper() == "JPG" || contentType.ToUpper() == "JPEG" || contentType.ToUpper() == "IMAGE/JPEG")
                {
                    string message = "";
                    var imageSizeKB = StudentImageSize.CalculateSize(Convert.ToDouble(size));

                    if (imageSizeKB < StudentImageSize.MinimumSizeKB)
                        return "The image size is too small.";

                    if (imageSizeKB > StudentImageSize.MaximumSizeKB)
                        return "The image size is too large. Max valid size is up to " + StudentImageSize.MaximumSizeKB + "KB";

                    return message;
                }

                return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
            }
        }


        private byte[] ConvertFileInByteArray(Stream inputStream, int contentLength)
        {
            try
            {
                byte[] file = null;

                using (var binaryReader = new BinaryReader(inputStream))
                {
                    file = binaryReader.ReadBytes(contentLength);
                }

                return file;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        private string ComputeHash(byte[] file)
        {
            MD5 md5 = MD5.Create();

            byte[] hashAraay = md5.ComputeHash(file);

            var builder = new StringBuilder();

            foreach (byte b in hashAraay)
            {
                builder.AppendFormat("{0:x2}", b);
            }

            return builder.ToString();
        }       

        public int ImageCount(List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList,
            string[] batchDaysList, string[] batchTimeList, bool isMissingImage)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(usermenu);
                return _studentProgramDao.ImageCount(branchIdList, programIdList, programId, sessionId, branchId, campusId, batchList, batchDaysList, batchTimeList, isMissingImage);
                // return new int[2]{0,5};
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int LoadImageReportsRowCount(List<UserMenu> userMenu, long programId, long sessionId,
            long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList,
            bool isMissingImage)
        {
            List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
            List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
            return _studentProgramDao.LoadImageReportsRowCount(branchIdList, programIdList, programId, sessionId,
                branchId, campusId, batchList, batchDaysList, batchTimeList, isMissingImage);
        }

        #endregion

        #region Helper Function

        #endregion

        #region Clean
        //public int ImageMissingCount(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList)
        //{

        //    int missingImages = 0;
        //    try
        //    {
        //        List<StudentProgram> data = new List<StudentProgram>();
        //        data = _studentProgramDao.LoadMissingImageStudent(programId, sessionId, branchId, campusId, batchList, batchDaysList, batchTimeList);
        //        if (data == null)
        //            missingImages = 0;
        //        else
        //            missingImages = data.Count;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //    }
        //    return missingImages;
        //}

        //public int ImageExistingCount(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList)
        //{
        //    int existImages = 0;
        //    try
        //    {
        //        List<StudentProgram> data = new List<StudentProgram>();
        //        data = _studentProgramDao.LoadExistingImageStudent(programId, sessionId, branchId, campusId, batchList, batchDaysList, batchTimeList);
        //        if (data == null)
        //            existImages = 0;
        //        else
        //            existImages = data.Count;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //    }
        //    return existImages;
        //}
        //public List<StudentImageMissing> LoadMissingImageReports(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchId, string[] batchDays, string[] batchTime)
        //{
        //    List<StudentImageMissing> result = new List<StudentImageMissing>();
        //    try
        //    {
        //        List<StudentProgram> data = _studentProgramDao.LoadMissingImageStudent(programId, sessionId, branchId,
        //            campusId, batchId, batchDays, batchTime);

        //        if (data == null || data.Count < 1)
        //            return null;

        //        data = (from x in data select x).OrderBy(a => a.PrnNo).ToList();

        //        IList<StudentImageMissingFlat> flatDataList = new List<StudentImageMissingFlat>();

        //        foreach (StudentProgram item in data)
        //        {
        //            StudentImageMissingFlat smF = new StudentImageMissingFlat();
        //            smF.ProgramId = item.Program.Id;
        //            smF.ProgramName = item.Program.Name;
        //            smF.SessionId = item.Batch.Session.Id;
        //            smF.SessionName = item.Batch.Session.Name;
        //            smF.BranchId = item.Batch.Branch.Id;
        //            smF.BranchName = item.Batch.Branch.Name;
        //            smF.CampusId = item.Batch.Campus.Id;
        //            smF.CampusName = item.Batch.Campus.Name;
        //            smF.BatchId = item.Batch.Id;
        //            smF.BatchName = item.Batch.Name;
        //            smF.BatchDays = item.Batch.Days;
        //            smF.BatchTime = Convert.ToDateTime(item.Batch.StartTime.ToString()).ToString("hh:mm tt") + " to " +
        //                            Convert.ToDateTime(item.Batch.EndTime.ToString()).ToString("hh:mm tt");
        //            smF.BatchRank = item.Batch.Rank;
        //            smF.ProgramRoll = item.PrnNo;

        //            flatDataList.Add(smF);
        //        }

        //        flatDataList =
        //            flatDataList.ToList()
        //                .OrderBy(a => a.BatchDays)
        //                .ThenBy(b => b.BatchId)
        //                .ThenBy(c => c.BatchRank)
        //                .ToList();
        //        if (flatDataList == null || flatDataList.Count < 1)
        //            return null;



        //        foreach (StudentImageMissingFlat item in flatDataList)
        //        {
        //            if (
        //                result.Any(
        //                    a =>
        //                        a.BatchDays == item.BatchDays && a.BranchId == item.BranchId &&
        //                        a.CampusId == item.CampusId) == false)
        //            {
        //                StudentImageMissing simv = new StudentImageMissing();
        //                simv.ProgramId = item.ProgramId;
        //                simv.ProgramName = item.ProgramName;
        //                simv.SessionId = item.SessionId;
        //                simv.SessionName = item.SessionName;
        //                simv.BranchId = item.BranchId;
        //                simv.BranchName = item.BranchName;
        //                simv.CampusId = item.CampusId;
        //                simv.CampusName = item.CampusName;
        //                simv.BatchDays = item.BatchDays;
        //                result.Add(simv);

        //            }
        //            StudentImageMissing curttentHeader =
        //                (from x in result where x.BranchId == item.BranchId && x.CampusId == item.CampusId select x)
        //                    .FirstOrDefault();
        //            if (curttentHeader != null &&
        //                result.Any(a => a.BatchTimeProgramRoll.Any(b => b.BatchId == item.BatchId)) == false)
        //            {
        //                BatchTimeProgramRoll recordInfo = new BatchTimeProgramRoll();
        //                recordInfo.BatchId = item.BatchId;
        //                recordInfo.BatchName = item.BatchName;
        //                recordInfo.BatchTime = item.BatchTime;
        //                recordInfo.ProgramRoll = item.ProgramRoll;
        //                curttentHeader.BatchTimeProgramRoll.Add(recordInfo);
        //            }
        //            else if (curttentHeader != null)
        //            {
        //                BatchTimeProgramRoll recordInfo =
        //                    (from x in curttentHeader.BatchTimeProgramRoll where x.BatchId == item.BatchId select x)
        //                        .FirstOrDefault();
        //                if (recordInfo != null)
        //                {
        //                    recordInfo.ProgramRoll += ", " + item.ProgramRoll;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //    }

        //    return result;
        //}

        //public List<StudentImageMissing> LoadExistingImageReports(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchId, string[] batchDays, string[] batchTime)
        //{
        //    List<StudentImageMissing> result = new List<StudentImageMissing>();

        //    try
        //    {
        //        List<StudentProgram> data = _studentProgramDao.LoadExistingImageStudent(programId, sessionId, branchId, campusId, batchId, batchDays, batchTime);

        //        if (data == null || data.Count < 1)
        //            return null;

        //        data = (from x in data select x).OrderBy(a => a.PrnNo).ToList();

        //        IList<StudentImageMissingFlat> flatDataList = new List<StudentImageMissingFlat>();

        //        foreach (StudentProgram item in data)
        //        {
        //            StudentImageMissingFlat smF = new StudentImageMissingFlat();
        //            smF.ProgramId = item.Program.Id;
        //            smF.ProgramName = item.Program.Name;
        //            smF.SessionId = item.Batch.Session.Id;
        //            smF.SessionName = item.Batch.Session.Name;
        //            smF.BranchId = item.Batch.Branch.Id;
        //            smF.BranchName = item.Batch.Branch.Name;
        //            smF.CampusId = item.Batch.Campus.Id;
        //            smF.CampusName = item.Batch.Campus.Name;
        //            smF.BatchId = item.Batch.Id;
        //            smF.BatchName = item.Batch.Name;
        //            smF.BatchDays = item.Batch.Days;
        //            smF.BatchTime = Convert.ToDateTime(item.Batch.StartTime.ToString()).ToString("hh:mm tt") + " to " + Convert.ToDateTime(item.Batch.EndTime.ToString()).ToString("hh:mm tt");
        //            smF.BatchRank = item.Batch.Rank;
        //            smF.ProgramRoll = item.PrnNo;

        //            flatDataList.Add(smF);
        //        }

        //        flatDataList = flatDataList.ToList().OrderBy(a => a.BatchDays).ThenBy(b => b.BatchId).ThenBy(c => c.BatchRank).ToList();
        //        if (flatDataList == null || flatDataList.Count < 1)
        //            return null;



        //        foreach (StudentImageMissingFlat item in flatDataList)
        //        {
        //            //Header info add
        //            if (result.Any(a => a.BatchDays == item.BatchDays && a.BranchId == item.BranchId && a.CampusId == item.CampusId) == false)
        //            {
        //                StudentImageMissing simv = new StudentImageMissing();
        //                simv.ProgramId = item.ProgramId;
        //                simv.ProgramName = item.ProgramName;
        //                simv.SessionId = item.SessionId;
        //                simv.SessionName = item.SessionName;
        //                simv.BranchId = item.BranchId;
        //                simv.BranchName = item.BranchName;
        //                simv.CampusId = item.CampusId;
        //                simv.CampusName = item.CampusName;
        //                simv.BatchDays = item.BatchDays;
        //                result.Add(simv);
        //            }

        //            StudentImageMissing curttentHeader = (from x in result where x.BranchId == item.BranchId && x.CampusId == item.CampusId select x).FirstOrDefault();

        //            //report detail add
        //            if (curttentHeader != null && result.Any(a => a.BatchTimeProgramRoll.Any(b => b.BatchId == item.BatchId)) == false)
        //            {
        //                BatchTimeProgramRoll recordInfo = new BatchTimeProgramRoll();
        //                recordInfo.BatchId = item.BatchId;
        //                recordInfo.BatchName = item.BatchName;
        //                recordInfo.BatchTime = item.BatchTime;
        //                recordInfo.ProgramRoll = item.ProgramRoll;
        //                curttentHeader.BatchTimeProgramRoll.Add(recordInfo);
        //            }
        //            else if (curttentHeader != null)
        //            {
        //                BatchTimeProgramRoll recordInfo = (from x in curttentHeader.BatchTimeProgramRoll where x.BatchId == item.BatchId select x).FirstOrDefault();
        //                if (recordInfo != null)
        //                {
        //                    recordInfo.ProgramRoll += ", " + item.ProgramRoll;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //    }
        //    return result;
        //}


        #endregion
    }
}