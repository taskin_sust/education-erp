﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.CampusTest
{
    interface ICampusLoadActiveTest
    {
        #region Basic Test
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Campus")]
    public class CampusLoadActiveTest : CampusBaseTest, ICampusLoadActiveTest
    {
        #region Basic Test
        [Fact]
        public void LoadWithUserMenu_should_return_list()
        {
            _campusFactory.CreateMore(100).Persist();
            var firstCampus = _campusFactory.GetLastCreatedObjectList()[0];
            var branch = firstCampus.Branch;
            var organization = branch.Organization;
            var usermenu = BuildUserMenu(org: organization, programList: null, branch: branch);
            IList<Campus> campusList = _campusService.LoadCampus(usermenu, 0, 100, "", "", _campusFactory.GetLastCreatedObjectList()[0].Branch.Organization.Id.ToString(), "", "", "", "", "1");
            Assert.Equal(100, campusList.Count);
            var campusroomIdlist = (from campus in _campusFactory.ObjectList from campusRoom in campus.CampusRooms select campusRoom.Id).ToList();
            _campusTestService.DeleteCampusRoom(campusroomIdlist);
        }

        [Fact]
        public void LoadCampus_should_return_list()
        {
            _campusFactory.CreateMore(100).Persist();
            var branch = _campusFactory.GetLastCreatedObjectList()[0].Branch;
            var organizatinList = commonHelper.ConvertIdToList(_campusFactory.GetLastCreatedObjectList()[0].Branch.Organization.Id);
            var branchIdList = commonHelper.ConvertIdToList(_campusFactory.GetLastCreatedObjectList()[0].Branch.Id);
            var campusIdList = _campusFactory.GetLastCreatedObjectList().Select(x => x.Id).ToList();
            IList<Campus> campusList = _campusService.LoadCampus(organizatinList, branchIdList: branchIdList, campusIdList: campusIdList, isProgramBranSession: false);
            var campusroomIdlist = (from campus in _campusFactory.GetLastCreatedObjectList() from campusRoom in campus.CampusRooms select campusRoom.Id).ToList();
            _campusTestService.DeleteCampusRoom(campusroomIdlist);
            Assert.Equal(100, campusList.Count);
        }

        [Fact]
        public void LoadCampusByIdList_should_return_list()
        {
            _campusFactory.CreateMore(100).Persist();
            var campusIdList = _campusFactory.GetLastCreatedObjectList().Select(x => x.Id).ToList();
            IList<Campus> campusList = _campusService.LoadCampus(campusIdList.ToArray());
            var campusroomIdlist = (from campus in _campusFactory.GetLastCreatedObjectList() from campusRoom in campus.CampusRooms select campusRoom.Id).ToList();
            _campusTestService.DeleteCampusRoom(campusroomIdlist);
            Assert.Equal(100, campusList.Count);
        }

        [Fact]
        public void LoadActive_should_Not_return_list()
        {
            var firstCampus = _campusFactory.GetLastCreatedObjectList()[0];
            var branch = firstCampus.Branch;
            var organization = branch.Organization;
            var usermenu = BuildUserMenu(org: organization, programList: null, branch: branch);
            IList<Campus> campusList = _campusService.LoadCampus(usermenu, 0, 1000, "", "", "-1", "", "", "", "", "1");
            Assert.Equal(0, campusList.Count);
        }

        [Fact]
        public void LoadCampusByBranchIdsProgramSession_should_Not_return_list()
        {
            Organization organization = organizationFactory.Create().Persist().Object;
            //Create New Program
            _code = _programFactory.GetRandomNumber();
            Program program =
                _programFactory.Create().WithOrganization(organization).WithNameAndRank(_programName, 1).WithProperties(_programName, _code, _paidProgram)
                    .Persist().Object;

            //Create New Session
            _sessionFactory.Create().Persist();
            Session sessionObj = _sessionFactory.Object;

            //Create New Branches and generate branchArray
            IList<Organization> organizations = new List<Organization>();
            organizations.Add(program.Organization);
            var branchFactory = _branchFactory.CreateMore(1, organizations).Persist();
            IList<Branch> branchList = branchFactory.GetLastCreatedObjectList();
            var branchIdArray = branchList.Select(x => x.Id).ToArray();
            //Generate selected Student Exam List
            var studentExamList = _studentExamService.LoadBoardExam(true);
            int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y => (int?)Convert.ToInt32(y.Id)).ToArray();

            //generate selected Admission Type List
            var selectedAdmissionTypeList = new int?[] { 1, 2 };
            List<long> nonDeletableBranchIds;
            _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id,
                branchIdArray, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);
            var campusRooms = new List<CampusRoom>()
            {
                new CampusRoom()
                {
                    RoomNo = (101).ToString(CultureInfo.InvariantCulture),
                    ClassCapacity = 100,
                    ExamCapacity = 100
                }
            };
            _campusFactory.CreateMore(100, branchList, campusRooms).Persist();
            var campusObjectList = _campusFactory.GetLastCreatedObjectList();
            var campusList = _campusService.LoadCampusByBranchIdsProgramSession(branchList.Select(x => x.Id).ToArray(), program.Id, sessionObj.Id);
            Assert.True(campusList.Count > 0);
            var campusRoomIdList = (from campus in _campusFactory.GetLastCreatedObjectList() from campusRoom in campus.CampusRooms select campusRoom.Id).ToList();
            CleanUpProgramStudentExamSession(program.Id, sessionObj.Id);
            CleanUpProgramBranchSession(program.Id, sessionObj.Id, branchIdArray);
            _campusTestService.DeleteCampusRoom(campusRoomIdList);
        }

        #endregion
    }
}
