﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.MediaDao
{
    public interface ITeamMemberImageMediaDao : IBaseDao<TeamMemberMediaImage, long>
    {

        TeamMemberMediaImage GetTeamMemberImageMediaByRefId(long id);
        TeamMemberNomineeMediaImage GetTeamMemberNomineeImageMediaByRefId(long id);

        void DeleteTeamMemberMediaImage(long refId);
    }
    public class TeamMemberImageMediaDao : BaseDao<TeamMemberMediaImage, long>, ITeamMemberImageMediaDao
    {
        public TeamMemberNomineeMediaImage GetTeamMemberNomineeImageMediaByRefId(long id)
        {
            return
                Session.QueryOver<TeamMemberNomineeMediaImage>()
                    .Where(x => x.ImageRef == id)
                    .Take(1)
                    .SingleOrDefault<TeamMemberNomineeMediaImage>();
        }

        public void DeleteTeamMemberMediaImage(long refId)
        {
            var teamMemberMediaImage =
                Session.QueryOver<TeamMemberMediaImage>().Where(x => x.ImageRef == refId).SingleOrDefault<TeamMemberMediaImage>();
            Session.Delete(teamMemberMediaImage);
        }
        public TeamMemberMediaImage GetTeamMemberImageMediaByRefId(long id)
        {
            return Session.QueryOver<TeamMemberMediaImage>().Where(x => x.ImageRef == id).Take(1).SingleOrDefault<TeamMemberMediaImage>();
        }
    }
}
