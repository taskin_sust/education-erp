﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class LeaveApplicationDetailFactory : ObjectFactoryBase<LeaveApplicationDetails>
    {
        public LeaveApplicationDetailFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

        }

        public LeaveApplicationDetailFactory Create()
        {
            Object = new LeaveApplicationDetails()
            {

            };
            return this;
        }

        public LeaveApplicationDetailFactory WithLeaveApplication(LeaveApplication leaveApplication)
        {
            Object.LeaveApplication = leaveApplication;
            return this;
        }
    }
}
