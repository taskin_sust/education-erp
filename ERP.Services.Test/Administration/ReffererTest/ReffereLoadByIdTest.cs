﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ReffererTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Refferer")]
    public class ReffererLoadByIdTest : ReffererBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _referenceService.LoadById(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            ReferrerFactory referrerFactory =
                _referrerFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithCode(_code)
                    .Persist();
            var obj = _referenceService.LoadById(referrerFactory.Object.Id);
            Assert.NotNull(obj);
            
        }

    }
}
