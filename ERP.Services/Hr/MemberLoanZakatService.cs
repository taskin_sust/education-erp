using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.Hr
{
public interface IMemberLoanZakatService : IBaseService
{
    #region Operational Function

    bool SaveOrUpdate(MemberLoanZakat prMemberLoanZakat);

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function

    #endregion

    #region Helper Function

    #endregion
}
public class MemberLoanZakatService : BaseService, IMemberLoanZakatService {
    #region Logger
    private readonly ILog _logger = LogManager.GetLogger("PayrollService");
    #endregion

    #region Propertise & Object Initialization
    private readonly CommonHelper _commonHelper;
    private readonly IMemberLoanZakatDao _prMemberLoanZakatDao;
    public MemberLoanZakatService(ISession session)
    {
        Session = session;
        _commonHelper = new CommonHelper();
        _prMemberLoanZakatDao=new MemberLoanZakatDao{Session = session};}
    #endregion

    #region Operational Functions

    public bool SaveOrUpdate(MemberLoanZakat prMemberLoanZakat)
    {

        return true;
    }

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function


    #endregion

    #region Helper function



    #endregion
}
}
                                    
