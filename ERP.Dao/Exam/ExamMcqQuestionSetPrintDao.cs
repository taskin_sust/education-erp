﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using NHibernate.Loader.Criteria;
using NHibernate.Persister.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Exam
{
    public interface IExamMcqQuestionSetPrintDao : IBaseDao<ExamMcqQuestionSetPrint, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        ExamMcqQuestionSetPrint GetExamMcqQuestionSetPrint(int[] uniqueSets = null);  
        #endregion

        #region List Loading Function

        IList<ExamMcqQuestionSetPrint> GetExamMcqQuestionSetPrintReportList(int start, int length, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, string examOrCode = "", int? uniqueResult = null, string codeNo = "", int? printStatus = null);
        IList<ExamMcqQuestionSetPrint> LoadExamMcqQuestionSetPrintList(long examId);
        #endregion

        #region Others Function
        int GetExamMcqQuestionSetPrintReportCount(List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, string examOrCode = "", int? uniqueResult = null, string codeNo = "", int? printStatus = null);
       
        #endregion
        
        #region Helper Functions
        #endregion
        
    }
    public class ExamMcqQuestionSetPrintDao : BaseDao<ExamMcqQuestionSetPrint, long>, IExamMcqQuestionSetPrintDao
    {
        #region Properties & Object & Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public ExamMcqQuestionSetPrint GetExamMcqQuestionSetPrint(int[] uniqueSets = null)
        {
            ICriteria criteria = Session.CreateCriteria<ExamMcqQuestionSetPrint>()
                .Add(Restrictions.Not(Restrictions.Eq("Status", ExamMcqQuestion.EntityStatus.Delete)));
            if (uniqueSets!=null && uniqueSets.Any())
            {
                criteria.Add(Restrictions.In("UniqueSet", uniqueSets));
            }
            criteria.AddOrder(Order.Desc("CodeNo"));
            criteria.SetFirstResult(0);
            criteria.SetMaxResults(1);
            var result = criteria.UniqueResult<ExamMcqQuestionSetPrint>();

            return result;            
        }

        #endregion

        #region List Loading Function

        public IList<ExamMcqQuestionSetPrint> GetExamMcqQuestionSetPrintReportList(int start, int length, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, string examOrCode = "", int? uniqueResult = null, string codeNo = "", int? printStatus = null)
        {
            var examMcqQuestionSetPrintReportCriteria = GetExamMcqQuestionSetPrintReportCriteria(programIdList, sessionIdList, courseIdList, examOrCode, uniqueResult, codeNo, printStatus);
            return examMcqQuestionSetPrintReportCriteria.SetFirstResult(start).SetMaxResults(length).List<ExamMcqQuestionSetPrint>();
        }

        public IList<ExamMcqQuestionSetPrint> LoadExamMcqQuestionSetPrintList(long examId)
        {
            ICriteria criteria = Session.CreateCriteria<ExamMcqQuestionSetPrint>().Add(
               Restrictions.Not(Restrictions.Eq("Status", ExamMcqQuestion.EntityStatus.Delete)));

            criteria.CreateAlias("Exam", "e").Add(Restrictions.Eq("e.Status", Exams.EntityStatus.Active)).
                Add(Restrictions.Eq("e.Id", examId));
            return criteria.List<ExamMcqQuestionSetPrint>();
        }

        
        #endregion

        #region Others Function

        public int GetExamMcqQuestionSetPrintReportCount(List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, string examOrCode = "", int? uniqueResult = null, string codeNo = "", int? printStatus = null)
        {
            var examMcqQuestionSetPrintReportCriteria = GetExamMcqQuestionSetPrintReportCriteria(programIdList, sessionIdList, courseIdList, examOrCode, uniqueResult, codeNo, printStatus);
            var rowCount = examMcqQuestionSetPrintReportCriteria.SetProjection(Projections.RowCount()).UniqueResult<int>();
            return rowCount;
        }

        #endregion

        #region Helper Function

        private ICriteria GetExamMcqQuestionSetPrintReportCriteria(List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, string examOrCode = "", int? uniqueResult = null, string codeNo = "", int? printStatus = null)
        {
            ICriteria criteria = Session.CreateCriteria<ExamMcqQuestionSetPrint>().Add(Restrictions.Not(Restrictions.Eq("Status", ExamMcqQuestionSetPrint.EntityStatus.Delete)));
            criteria.CreateAlias("Exam", "ex");
            criteria.CreateAlias("ex.Program", "p");
            criteria.CreateAlias("ex.Session", "s");
            criteria.CreateAlias("ex.Course", "c");
            if (programIdList != null && programIdList.Any())
            {
                criteria.Add(Restrictions.In("p.Id", programIdList));
            }
            if (sessionIdList != null && sessionIdList.Any())
            {
                criteria.Add(Restrictions.In("s.Id", sessionIdList));
            }
            if (courseIdList != null && courseIdList.Any())
            {
                criteria.Add(Restrictions.In("c.Id", courseIdList));
            }
            if (uniqueResult != null)
                criteria.Add(Restrictions.Eq("UniqueSet", uniqueResult));
            if (!string.IsNullOrEmpty(examOrCode))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                disjunction.Add(Restrictions.Like("ex.Name", examOrCode, MatchMode.Anywhere).IgnoreCase());
                disjunction.Add(Restrictions.Like("ex.ShortName", examOrCode, MatchMode.Anywhere).IgnoreCase());
                disjunction.Add(Restrictions.Like("ex.Code", examOrCode, MatchMode.Anywhere).IgnoreCase());
            }
            if (!string.IsNullOrEmpty(codeNo))
                criteria.Add(Restrictions.Eq("CodeNo", Convert.ToInt64(codeNo)));

            if (printStatus != null)
                criteria.Add(Restrictions.Eq("PrintStatus", printStatus));
            return criteria;
        }

        #endregion

    }
}
