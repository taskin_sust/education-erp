﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Hr
{
    public class FestivalBonusSheetMap:BaseClassMap<FestivalBonusSheet,long>
    {
        public FestivalBonusSheetMap()
        {
            Table("HR_FestivalBonusSheet");
            LazyLoad();
            Map(x => x.BankAccount).Column("BankAmount");
            Map(x => x.CashAccount).Column("CashAmout");
            Map(x => x.Remarks).Column("Remarks");
            Map(x => x.IsSubmit).Column("IsSubmit");
            Map(x => x.StartDate).Column("StartDate");
            Map(x => x.EndDate).Column("EndDate");
            Map(x => x.Year).Column("Year");
            Map(x => x.Month).Column("Month");
            References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.FestivalBonusSetting).Column("FestivalBonusSettingId");
            References(x => x.SalaryOrganization).Column("SalaryOrganizationId");
            References(x => x.SalaryBranch).Column("SalaryBranchId");
            References(x => x.SalaryCampus).Column("SalaryCampusId");
            References(x => x.SalaryDepartment).Column("SalaryDepartmentId");
            References(x => x.SalaryDesignation).Column("SalaryDesignationId");
            References(x => x.JobOrganization).Column("JobOrganizationId");
            References(x => x.JobBranch).Column("JobBranchId");
            References(x => x.JobCampus).Column("JobCampusId");
            References(x => x.JobDepartment).Column("JobDepartmentId");
            References(x => x.JobDesignation).Column("JobDesignationId");
        }
    }
}
