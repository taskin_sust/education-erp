﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UdvashERP.Areas.Student.Models
{
    public class StudentInfoTransfer
    {
        public long OrganizationId { get; set; }
        [Required(ErrorMessage = "You need to select program")]
        [Display(Name = "Program")]
        public long ProgramId { get; set; }

        [Required(ErrorMessage = "You need to select session")]
        [Display(Name = "Session")]
        public long SessionId { get; set; }

        [Required(ErrorMessage = "You need to select from branch")]
        [Display(Name = "From Branch")]
        public long[] SelectedFromBranch { get; set; }

        [Required(ErrorMessage = "You need to select to branch")]
        [Display(Name = "To Branch")]
        public long[] SelectedToBranch { get; set; }


        [Required(ErrorMessage = "You need to select from campus")]
        [Display(Name = "From Campus")]
        public long[] SelectedFromCampus { get; set; }

        [Required(ErrorMessage = "You need to select to campus")]
        [Display(Name = "To Campus")]
        public long[] SelectedToCampus { get; set; }

        [Display(Name = "Total Transferred")]
        public long NumberOfTransferred { get; set; }

        [Display(Name = "Date From")]
        public DateTime DateFrom { get; set; }
        
        [Display(Name = "Date To")]
        public DateTime DateTo { get; set; }
    }
}