using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class ProgramSessionItemMap : BaseClassMap<ProgramSessionItem, long>
    {

        public ProgramSessionItemMap()
        {
            Table("UINV_ProgramSessionItem");
            LazyLoad();
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.Item).Column("ItemId");
        }
    }
}
