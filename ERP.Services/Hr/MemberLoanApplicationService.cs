using System;
using System.Collections.Generic;
using System.Security.Authentication;
using FluentNHibernate;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.Hr
{
    public interface IMemberLoanApplicationService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(MemberLoanApplication memberLoanApplication);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<MemberLoanApplicationHistoryDto> LoadMemberLoanApplication(int start, int length, List<UserMenu> userMenu, int? pin = null, long? organizationId = null,
            long? departmentId = null, long? branchId = null, int? status = null);
        IList<MemberLoanDetailsDto> GetLoadDetails(int pin);
        #endregion

        #region Others Function

        int GetMemberLoanApplicationCount(List<UserMenu> userMenu, int? pin = null, long? organizationId = null, long? departmentId = null, long? branchId = null, int? status = null);

        #endregion

        #region Helper Function

        #endregion


    }
    public class MemberLoanApplicationService : BaseService, IMemberLoanApplicationService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("PayrollService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IMemberLoanApplicationDao _memberLoanApplicationDao;
        private readonly IMemberLoanDao _memberLoanDao;
        public MemberLoanApplicationService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _memberLoanApplicationDao = new MemberLoanApplicationDao { Session = session };
            _memberLoanDao = new MemberLoanDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(MemberLoanApplication memberLoanApplication)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    ModelValidationCheck(memberLoanApplication);
                    CheckBeforeSave(memberLoanApplication);
                    if (memberLoanApplication.Id == 0)
                    {
                        _memberLoanApplicationDao.Save(memberLoanApplication);
                    }
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
            return true;
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<MemberLoanApplicationHistoryDto> LoadMemberLoanApplication(int start, int length, List<UserMenu> userMenu, int? pin = null, long? organizationId = null,
            long? departmentId = null, long? branchId = null, int? status = null)
        {
            try
            {
                if (userMenu == null) throw new AuthenticationException("Invalid user permission");
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);
                return _memberLoanApplicationDao.LoadMemberLoanApplication(start, length, organizationIdList, branchIdList, departmentId, pin, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<MemberLoanDetailsDto> GetLoadDetails(int pin)
        {
            try
            {
                if (pin <= 0) throw new InvalidDataException("Invalid Pin found");
                return _memberLoanApplicationDao.GetLoadDetails(pin);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Others Function

        public int GetMemberLoanApplicationCount(List<UserMenu> userMenu, int? pin = null, long? organizationId = null, long? departmentId = null, long? branchId = null,
            int? status = null)
        {
            try
            {
                if (userMenu == null) throw new AuthenticationException("Invalid User Permission");
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);
                return _memberLoanApplicationDao.GetMemberLoanApplicationCount(organizationIdList, branchIdList, departmentId, pin, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        public void ModelValidationCheck(MemberLoanApplication memberLoanApplication)
        {
            string errorMessage = "";
            var memberLoanApplicationValidationResult =
                ValidationHelper.ValidateEntity<MemberLoanApplication, MemberLoanApplication>(memberLoanApplication);
            if (memberLoanApplicationValidationResult.HasError)
            {
                memberLoanApplicationValidationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
            }
            if (memberLoanApplication.MemberLoanZakat != null && memberLoanApplication.MemberLoanZakat.Any())
            {
                var memberLoanZakatValidationResult =
                        ValidationHelper.ValidateEntity<MemberLoanZakat, MemberLoanZakat>(memberLoanApplication.MemberLoanZakat[0]);
                if (memberLoanZakatValidationResult.HasError)
                {
                    memberLoanZakatValidationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                }
            }
            if (memberLoanApplication.MemberLoanCsr != null && memberLoanApplication.MemberLoanCsr.Any())
            {
                var memberLoanCsrValidationResult =
                        ValidationHelper.ValidateEntity<MemberLoanCsr, MemberLoanCsr>(memberLoanApplication.MemberLoanCsr[0]);
                if (memberLoanCsrValidationResult.HasError)
                {
                    memberLoanCsrValidationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                }
            }
            if (memberLoanApplication.MemberLoan != null && memberLoanApplication.MemberLoan.Any())
            {
                var memberLoanValidationResult =
                        ValidationHelper.ValidateEntity<MemberLoan, MemberLoan>(memberLoanApplication.MemberLoan[0]);
                if (memberLoanValidationResult.HasError)
                {
                    memberLoanValidationResult.Errors.ForEach(
                        r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                }
            }
            if (!string.IsNullOrEmpty(errorMessage))
            {
                throw new InvalidDataException(errorMessage);
            }
        }

        private void CheckBeforeSave(MemberLoanApplication memberLoanApplication)
        {
            var currentYear = DateTime.Now.Year;
            var currentMonth = DateTime.Now.Month;
            var paymentTypes = _commonHelper.ConvertEnumToIdList<PaymentType>();
            decimal zakatTotalAmount = 0;
            decimal csrTotalAmount = 0;
            decimal loanApprovedAmount = 0;

            //check zakat payment type
            if (memberLoanApplication.MemberLoanZakat != null && memberLoanApplication.MemberLoanZakat.Any())
            {
                var zakat = memberLoanApplication.MemberLoanZakat[0];
                zakatTotalAmount = zakat.TotalAmount;
                //check valid zakat payment type
                if (!paymentTypes.Contains(zakat.PaymentType))
                {
                    throw new InvalidDataException("Invalid payment type for zakat");
                }
                //check valid zakat efective and closing month
                var zakatEffectiveDate = zakat.EffectiveDate;
                var zakatClosingDate = zakat.ClosingDate;

                if (zakatEffectiveDate > zakatClosingDate)
                {
                    throw new InvalidDataException("Zakat closing month must be greater than Effective month");
                }
                if ((zakatEffectiveDate.Year == currentYear && zakatEffectiveDate.Month < currentMonth)
                    || zakatEffectiveDate.Year < currentYear)
                {
                    throw new InvalidDataException("Zakat effective month must be greater than now");
                }
                if ((zakatClosingDate.Year == currentYear && zakatClosingDate.Month < currentMonth)
                    || zakatClosingDate.Year < currentYear)
                {
                    throw new InvalidDataException("Zakat closing month must be greater than now");
                }
                //check zakat Funded by
                var fundedByList = _commonHelper.ConvertEnumToIdList<FundedBy>();
                if (!fundedByList.Contains(zakat.FundedBy))
                {
                    throw new InvalidDataException("Invalid payment type for zakat");
                }
            }
            if (memberLoanApplication.MemberLoanCsr != null && memberLoanApplication.MemberLoanCsr.Any())
            {
                var csr = memberLoanApplication.MemberLoanCsr[0];
                csrTotalAmount = csr.TotalAmount;
                //check valid zakat payment type
                if (!paymentTypes.Contains(csr.PaymentType))
                {
                    throw new InvalidDataException("Invalid payment type for csr");
                }
                //check valid zakat efective and closing month
                var csrEffectiveDate = csr.EffectiveDate;
                var csrClosingDate = csr.ClosingDate;

                if (csrEffectiveDate > csrClosingDate)
                {
                    throw new InvalidDataException("Csr closing month must be greater than Effective month");
                }
                if ((csrEffectiveDate.Year == currentYear && csrEffectiveDate.Month < currentMonth)
                    || csrEffectiveDate.Year < currentYear)
                {
                    throw new InvalidDataException("Csr effective month must be greater than now");
                }
                if ((csrClosingDate.Year == currentYear && csrClosingDate.Month < currentMonth)
                    || csrClosingDate.Year < currentYear)
                {
                    throw new InvalidDataException("Csr closing month must be greater than now");
                }
            }
            if (memberLoanApplication.MemberLoan != null && memberLoanApplication.MemberLoan.Any())
            {
                var currentLoan = _memberLoanDao.GetCurrentLoan(memberLoanApplication.TeamMember.Pin);
                var loan = memberLoanApplication.MemberLoan[0];
                loanApprovedAmount = loan.LoanApprovedAmount;
                //check new loan approved and monthly refund
                if (loan.MonthlyRefund > (currentLoan + loanApprovedAmount))
                {
                    throw new InvalidDataException("Monthly refund can not be greater than approved loan amount");
                }
                var loanRefundStarts = loan.RefundStart;
                if ((loanRefundStarts.Year == currentYear && loanRefundStarts.Month < currentMonth)
                    || loanRefundStarts.Year < currentYear)
                {
                    throw new InvalidDataException("Refund start month must be greater than now");
                }
            }
            //if (zakatTotalAmount + csrTotalAmount + loanApprovedAmount > memberLoanApplication.RequestedAmount)
            //{
            //    throw new InvalidDataException("Approved amount can not be greater than the requested amount");
            //}
        }


        #endregion
    }
}

