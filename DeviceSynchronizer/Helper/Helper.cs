﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Management;
using System.Windows.Forms;
using System.Xml.Serialization;
using DeviceSynchronizer.Model;
using DeviceSynchronizer.Model.ApiModel;
using log4net;

namespace DeviceSynchronizer.Helper
{
    public static class Helper
    {
        #region Objects/Properties/Services/Dao & Initialization

        private static readonly ILog Log = LogManager.GetLogger("HelperLogger");

        private const string ConfigFile = "Config.xml";
        private static SynchronizerConfig _synchronizerConfig;

        #endregion


        #region Configuration 
        
        //this function is responsible for get the sync key from config file to perform sync operation
        public static string GetSyncKey()
        {
            var config = GetConfigFile();
            return config != null ? config.SyncKey : "";
        }

        public static string GetApiKey()
        {
            var config = GetConfigFile();
            return config != null ? config.ApiKey : "";
        }

        private static SynchronizerConfig GetConfigFile()
        {
            try
            {
                if (_synchronizerConfig == null)
                {
                    XmlSerializer xs = new XmlSerializer(typeof(SynchronizerConfig));
                    FileStream fs = new FileStream(Application.StartupPath + "\\" + ConfigFile, FileMode.Open);
                    _synchronizerConfig = new SynchronizerConfig();
                    _synchronizerConfig = (SynchronizerConfig)xs.Deserialize(fs);
                    fs.Close();
                }
                return _synchronizerConfig;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<string> GetHddSerials()
        {
            var returnList = new List<string>();
            var searcher = new ManagementObjectSearcher(@"SELECT * FROM Win32_DiskDrive");
            var collection = searcher.Get();
            foreach (ManagementObject hdd in collection)
            {
                //GET ONLY FIRST IDE OR SSD OR SCSI SERIAL NUMBER
                string storageType = hdd["InterfaceType"].ToString();
                if (storageType.ToUpper().Trim() == "IDE" || storageType.ToUpper().Trim() == "SSD" || storageType.ToUpper().Trim() == "SCSI")
                {
                    if (hdd["serialNumber"] != null)
                        returnList.Add(hdd["serialNumber"].ToString().Trim());
                }
            }

            return returnList;
        }

        #endregion
       
        #region FingerTemplate Data Check

        public static int FingerPrintAvailable(TeamMemberModel member)
        {
            int index = 0;
            if (!String.IsNullOrEmpty(member.FingerIndex0)) index++;
            if (!String.IsNullOrEmpty(member.FingerIndex1)) index++;
            if (!String.IsNullOrEmpty(member.FingerIndex2)) index++;
            if (!String.IsNullOrEmpty(member.FingerIndex3)) index++;
            if (!String.IsNullOrEmpty(member.FingerIndex4)) index++;
            if (!String.IsNullOrEmpty(member.FingerIndex5)) index++;
            if (!String.IsNullOrEmpty(member.FingerIndex6)) index++;
            if (!String.IsNullOrEmpty(member.FingerIndex7)) index++;
            if (!String.IsNullOrEmpty(member.FingerIndex8)) index++;
            if (!String.IsNullOrEmpty(member.FingerIndex9)) index++;

            return index;
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length).Where(x => x % 2 == 0).Select(x => Convert.ToByte(hex.Substring(x, 2), 16)).ToArray();
        }

        public static void RegisterFingerTemplate(int index, TeamMemberModel member, int machineNum, out byte[] fingerData)
        {
            fingerData = new byte[2000];
            if (!String.IsNullOrEmpty(member.FingerIndex0) && index == 0)
            {
                fingerData = StringToByteArray(member.FingerIndex0);
            }
            if (!String.IsNullOrEmpty(member.FingerIndex1) && index == 1)
            {
                fingerData = StringToByteArray(member.FingerIndex1);
            }
            if (!String.IsNullOrEmpty(member.FingerIndex2) && index == 2)
            {
                fingerData = StringToByteArray(member.FingerIndex2);
            }
            if (!String.IsNullOrEmpty(member.FingerIndex3) && index == 3)
            {
                fingerData = StringToByteArray(member.FingerIndex3);
            }
            if (!String.IsNullOrEmpty(member.FingerIndex4) && index == 4)
            {
                fingerData = StringToByteArray(member.FingerIndex4);
            }
            if (!String.IsNullOrEmpty(member.FingerIndex5) && index == 5)
            {
                fingerData = StringToByteArray(member.FingerIndex5);
            }
            if (!String.IsNullOrEmpty(member.FingerIndex6) && index == 6)
            {
                fingerData = StringToByteArray(member.FingerIndex6);
            }
            if (!String.IsNullOrEmpty(member.FingerIndex7) && index == 7)
            {
                fingerData = StringToByteArray(member.FingerIndex7);
            }
            if (!String.IsNullOrEmpty(member.FingerIndex8) && index == 8)
            {
                fingerData = StringToByteArray(member.FingerIndex8);
            }
            if (!String.IsNullOrEmpty(member.FingerIndex9) && index == 9)
            {
                fingerData = StringToByteArray(member.FingerIndex9);
            }
        }

        #endregion

    }
}
