﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.MaritalInfoTest
{
    public class MaritalInfoBaseTest : TestBase, IDisposable
    {
        public const int MentorId = 2;
        public const string SpouseName = "Monica Bellucci";
        public const string SpouseContacts = "01910212121";
        protected readonly ISession _session;
        protected readonly IMaritalInfoService _maritalInfoService;

        protected readonly MaritalInfoFactory _maritalInformationFactory;
        public MaritalInfoBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _maritalInfoService = new MaritalInfoService(_session);
            _maritalInformationFactory = new MaritalInfoFactory(null, _session);
        }

        public void Dispose()
        {
            _maritalInformationFactory.CleanUp();
            base.Dispose();
        }
    }
}
