﻿/*Written BY Taskin*/
$(document).ready(function () {
    /* CHECKBOX SELECTION */
    $(document).on('click', '#allchecked', function () {

        if ($('#allchecked').is(':checked')) {

            $('#checkbox .checkbox-inline').each(function (index, value) {
                $(this).prop('checked', true);
            });

        } else {
            $('#checkbox .checkbox-inline').each(function (index, value) {
                $(this).prop('checked', false);

            });
        }

    });
    /* IF YOU CHANGE BRANCH OR SESSION VALUE FROM DROPDOWN THEN FIND THE ASSOCIATED PROGRAM BY THIS PIECE OF CODE*/
    $(document).on('change', '#Branch,#Session', function () {
        //alert();
        var branchId = $('#Branch').val();
        var sessionId = $('#Session').val();
        if (branchId.length > 0 && sessionId.length > 0) {
            $.ajax({
                url: "/Administration/Program/AssignProgram",
                type: "GET",
                data: { branchId: branchId, sessionId: sessionId },
                success: function (data) {
                    $('#checkbox').html(data);
                },
                error: function (data) {
                    $('.customMessage').append(data);
                }
            });
        }
    });

    /* NEW PROGRAM WILL BE ASSIGNED FROM THIS PIECE OF CODE */
    $(document).on('click', '#programAssign', function () {
        // alert();
        var ii = 0;
        var successMessage = '<div style="text-align: center;"class="alert alert-success"><a class="close" data-dismiss="alert">×</a><strong>Success!</strong> Progam Assign Successfully</div>';
        var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Progam Assign Failed</div>';
        var programList = {};
        $('#checkbox .checkbox-inline').each(function (index, value) {
            if ($(this).is(':checked')) {
                programList[ii++] = $(this).attr('Id');
            }
        });

        //if (programList.size() > 0) {
        var branchId = $('#Branch').val();
        var sessionId = $('#Session').val();
        //  alert(branchId + "---->>" + sessionId);
        var program = JSON.stringify(programList);
        $.ajax({
            url: "/Administration/Program/AssignProgramPost",
            type: "POST",
            data: { programList: program, branchId: branchId, sessionId: sessionId },
            success: function (data) {
                //alert("TRUE");
                if ($.trim(data) === "Success") {

                    $('.customMessage').append(successMessage);
                } else {

                    $('.customMessage').append(errorMessage);
                }
            },
            error: function (data) {
                $('.customMessage').append(data);
            }
        });
    });
});