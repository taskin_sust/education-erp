﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherAcademicMaterialDetailsService : IBaseService
    {

        #region Operation Function

        bool Delete(int id);
        bool Update(TeacherAcademicMaterialsDetails teacherAcademicMaterilDetails);

        #endregion

        #region List Loading Function

        IList<TeacherAcademicMaterialsDetails> TeacherAcademicMaterialsList(int start, int length, List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string paymentStatus, string date, string teacherName, long? forBranchId, string trackingId, string tpin);

        #endregion

        #region Single Instance loading Function

        TeacherAcademicMaterialsDetails GetTeacherAcademicMaterialsDetailById(long id);

        #endregion

        #region Others function

        long CountTeacherAcademicMaterials(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string paymentStatus, string date, string teacherName, long? forBranchId, string trackingId, string tpin);

        #endregion

    }

    public class TeacherAcademicMaterialDetailsService : BaseService, ITeacherAcademicMaterialDetailsService
    {
        #region logger

        private readonly ILog _logger = LogManager.GetLogger("TeacherAcademicMaterials");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        public readonly ITeacherAcademicMaterialDetailsDao _teacherAcademicMaterialDetailsDao;

        public TeacherAcademicMaterialDetailsService(ISession session)
        {
            Session = session;
            _teacherAcademicMaterialDetailsDao = new TeacherAcademicMaterialDetailsDao() { Session = session };
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region operation Function

        public bool Delete(int id)
        {
            ITransaction transaction = null;
            try
            {
                var tempObj = _teacherAcademicMaterialDetailsDao.LoadById(id);
                if (tempObj.PaymentStatus == 1)
                    throw new DependencyException("You can't delete this Academic Materials, payment is already paid.");

                using (transaction = Session.BeginTransaction())
                {
                    tempObj.Status = TeacherAcademicMaterialsDetails.EntityStatus.Delete;
                    _teacherAcademicMaterialDetailsDao.Update(tempObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool Update(TeacherAcademicMaterialsDetails teacherAcademicMaterilDetails)
        {
            ITransaction transaction = null;
            try
            {
                if (teacherAcademicMaterilDetails.PaymentStatus == 1)
                    throw new DependencyException("You can't update this academic material, payment is already paid.");

                using (transaction = Session.BeginTransaction())
                {
                    _teacherAcademicMaterialDetailsDao.Update(teacherAcademicMaterilDetails);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<TeacherAcademicMaterialsDetails> TeacherAcademicMaterialsList(int start, int length, List<UserMenu> userMenu, long organizationId, long programId,
            long sessionId, string paymentStatus, string date, string teacherName, long? forBranchId, string trackingId, string tpin)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, _commonHelper.ConvertIdToList(organizationId), null, _commonHelper.ConvertIdToList(programId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId));
                return _teacherAcademicMaterialDetailsDao.TeacherAcademicMaterialsList(start, length, organizationId, programIdList, branchIdList, sessionId, paymentStatus, date, teacherName, forBranchId, trackingId, tpin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instance loading function

        public TeacherAcademicMaterialsDetails GetTeacherAcademicMaterialsDetailById(long id)
        {
            try
            {
                return _teacherAcademicMaterialDetailsDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others function

        public long CountTeacherAcademicMaterials(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string paymentStatus,
            string date, string teacherName, long? forBranchId, string trackingId, string tpin)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, _commonHelper.ConvertIdToList(organizationId), null, _commonHelper.ConvertIdToList(programId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId));
                return _teacherAcademicMaterialDetailsDao.CountTeacherAcademicMaterials(organizationId, programIdList, branchIdList, sessionId, paymentStatus, date, teacherName, forBranchId, trackingId, tpin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

    }
}
