using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class NomineeInfoMap : BaseClassMap<NomineeInfo,long> {
        
        public NomineeInfoMap() {
			Table("HR_NomineeInfo");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Name).Column("Name");
			Map(x => x.MobileNo).Column("MobileNo");
			Map(x => x.Relation).Column("Relation");
			HasMany(x => x.Images).KeyColumn("NomineeInfoId");
        }
    }
}
