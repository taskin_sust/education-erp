﻿$(document).ready(function() {
});

$(document).on('click', '#lecturePlanUpdate', function (event) {
    var classNamePrefix = $("#classNamePrefix").val();
    var numberOfClasses = $("#numberOfClasses").val();
    var lectureSettingsId = $("#lectureSettingsId").val();
    
    var isSuccess = 0;
    if (lectureSettingsId == "" || !(/^\d+$/.test(lectureSettingsId))) {
        isSuccess = isSuccess + 1;
    }
    
    

    if (numberOfClasses == "") {
        $("#numberOfClasses").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Number Of Classes field is required.");
        $("#numberOfClasses").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(numberOfClasses))) {
        $("#numberOfClasses").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Number Of Classes field is Invalid.");
        $("#numberOfClasses").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#numberOfClasses").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#numberOfClasses").removeClass("highlight");
    }

    if (classNamePrefix == "") {
        $("#classNamePrefix").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Lecture Name Prefix field is required.");
        $("#classNamePrefix").addClass("highlight");
        isSuccess = isSuccess + 1;
    }   
    else {
        $("#classNamePrefix").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#classNamePrefix").removeClass("highlight");
    }
    
    
    console.log(classNamePrefix);
    console.log(numberOfClasses);
    console.log(lectureSettingsId);

    if (isSuccess == 0) {       
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/Lecture/Edit",
            cache: false,
            async: true,
            data: { "id": lectureSettingsId, "classNamePrefix": classNamePrefix, "numberOfClasses": numberOfClasses },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "s",
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "error",
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }

});