﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.BusinessModel.Entity;

namespace UdvashERP.Areas.Student.Models
{
    public class MaterialSearchInfo
    {
        #region Paasing Data View To Controller

        [Required(ErrorMessage = "You need to select organization")]
        public long OrganizationId { get; set; }

        [Required(ErrorMessage = "You need to select program")]
        public long ProgramId { get; set; }

        [Required(ErrorMessage = "You need to select session")]
        public long SessionId { get; set; }


        [Required(ErrorMessage = "You need to select branch")]
        public long[] SelectedBranch { get; set; }

        [Required(ErrorMessage = "You need to select campus")]
        public long[] SelectedCampus { get; set; }


        [Required(ErrorMessage = "You need to select batch")]
        public long[] SelectedBatch { get; set; }

        [Required(ErrorMessage = "You need to select batch days")]
        public string[] SelectedBatchDays { get; set; }

        [Required(ErrorMessage = "You need to select batch time")]
        public string[] SelectedBatchTime { get; set; }

        [Required(ErrorMessage = "You need to select material type")]
        public long[] SelectedMaterialType { get; set; }

        [Required(ErrorMessage = "You need to select material")]
        public long[] SelectedMaterial { get; set; }


        public IEnumerable<SelectListItem> PaymentStatusList { get; set; }

        public long PaymentStatus { get; set; } 


        public IEnumerable<SelectListItem> MaterialStatusList { get; set; }

        public long MaterialStatus { get; set; } 

        //public IEnumerable<string> SelectedPaymentStatus { get; set; }
        //public IEnumerable<string> SelectedMaterialStatus { get; set; }


        public string[] InformationViewList { get; set; }
        #endregion



        #region NO NEED


        //[Required]
        //public IList<Branch> Branch { get; set; }

        //[Required]
        //public IList<Campus> Campus { get; set; }

        //[Required]
        //public IList<Batch> Batch { get; set; }

        //public Batch Batches { get; set; }



        //[Required]
        //public IEnumerable<SelectListItem> BatchDays { get; set; }

        //public IEnumerable<SelectListItem> SelectedBatchDays { get; set; }

        //public IEnumerable<SelectListItem> BatchTime { get; set; }

        //public IEnumerable<SelectListItem> SelectedBatchTime { get; set; }



        //public IList<MaterialType> MaterialType { get; set; }


        //public IList<Material> Material { get; set; }


        //public IEnumerable<SelectListItem> PaymentStatus { get; set; }



        //public IEnumerable<SelectListItem> MaterialStatus { get; set; }





        //public IList<StudentProgram> StudentProgram { get; set; }






        #endregion



    }
}