﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Engine;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Common;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.MediaDao;
using UdvashERP.Dao.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.BusinessRules.Teachers;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Services.Hr
{

    public interface ITeamMemberService : IBaseService
    {
        #region Operational Function

        TeamMember SaveNew(TeamMember hrMember, string joiningDate, string dateTo);
        bool UpdateBasicInformation(TeamMember member);
        bool UpdatePersonalInformation(TeamMember member, long? presentThana, long? permanentThana, long? presentPostOffice, long? permanentPostOffice);
        bool UpdateParentalInformation(TeamMember member);
        bool UpdateSpecialization(TeamMember member);
        void UpdateTaxInformation(List<UserMenu> userMenus, TeamMember teamMember);
        void TranferTeamMember(List<UserMenu> userMenus, TeamMember transferingTeamMember, DateTime transferingDateTime);

        #endregion

        #region Single Instances Loading Function

        TeamMember GetByPin(int outerNum, DateTime? joiningDate = null);
        TeamMember LoadById(long id);
        TeamMember LoadByUserProfile(UserProfile user);
        TeamMember GetTeamMemberByAspNetUserId(long aspNetUserId);
        TeamMember GetMember(int mentorPin);
        TeamMember GetCurrentMember();
        Organization GetTeamMemberSalaryOrganization(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null);
        Organization GetTeamMemberOrganization(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null);
        Branch GetTeamMemberSalaryBranch(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null);
        Branch GetTeamMemberBranch(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null);
        Campus GetTeamMemberSalaryCampus(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null);
        Campus GetTeamMemberCampus(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null);
        Department GetTeamMemberDepartment(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null);
        Department GetTeamMemberSalaryDepartment(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null);
        Designation GetTeamMemberDesignation(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null);
        Designation GetTeamMemberSalaryDesignation(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null);
        TeamMemberTransferViewModel GetTeamMemberTransferInfo(long teamMemberId);

        #endregion

        #region List Loading Function

        List<long> LoadHrAuthorizedTeamMemberId(List<UserMenu> userMenus, DateTime? searchingDate = null, List<long> organizationIdList = null, List<long> branchIdList = null,
            List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, List<int> pinList = null);
        IList<TeamMember> LoadHrAuthorizedTeamMember(List<UserMenu> userMenus, DateTime? searchingDate = null, List<long> organizationIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, List<int> pinList = null, bool withRetired = true);
        List<LeaveTypeDtoList> LoadMembersPendingLeave(long memId, bool isHr = false);
        IList<TeamMember> LoadTeamMemberDirectory(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long? organizationId, long? branchId, long? campusId, long? departmentId, string name, int? pin, int? bloodGroup);
        IList<MemberSearchListDto> LoadTeamMemberListReport(int draw, int start, string orderBy, string orderDir, int length, List<UserMenu> userMenu, long organizationId, long? departmentId, long[] branchIds, long[] campusIds, int? memberTypes, int? memberStatus);
        IList<MemberSearchListDto> LoadTeamMemberSearchListReport(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long organizationId, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList);
        IList<MemberSearchListDto> LoadSearchTeammemberList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long organizationId, List<long> departmentIdList, List<long> branchIdList, List<long> campusIdList, List<int> memberTypeList, List<int> memberStatuList, string keyword, List<string> informationViewList);
        IList<TeamMemberBatchShiftDto> LoadTeamMemberBatchShiftDto(List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<long> campusIdList, List<int> employmentStatusList, List<long> departmntIdList, List<long> designationIdList, List<long> shiftIdList, DateTime? searchingEffectiveDate);
        IList<TeamMemberBatchSalaryHistoryDto> LoadTeamMemberBatchSalaryHistoryDto(List<UserMenu> userMenu, long organizationId, List<long> branchIds, List<long> campusIds, List<int> employmentStatus, List<long> departmentIds, List<long> designationIds, bool isSearchOnEmploymetHistory);
        List<FestivalBonusSheetDto> LoadTeamMemberListForFbSheet(List<UserMenu> userMenu, DateTime startDate, DateTime endDate, List<int> religionList, FestivalBonusSheetFormViewModel viewModel);
        List<TeamMemberInfoForLeaveSummaryDto> LoadTeamMemberForLeaveSummary(int start, int length, string orderBy, string orderDir,List<UserMenu> userMenus, DateTime searchingEffectiveDate, long organizationId, long branchId, long campusId , long departmentId, long designationId, string pinListString);
        #endregion

        #region Others Function

        int GetMemberWeekEnd(long memberId);
        bool AssignTeamMemberToUser(long teamMemberId, long userId);
        void GetCurrentTeamMemberNomineeInfo(long p, out string nomineeName, out string nomineeNameRelation, out string nomineeContactNumber);
        //int CountTeamMemberDirectory(List<UserMenu> userMenu, long? organizationId, long? branchId, long? campusId, long? departmentId, string name, int? pin);
        int CountTeamMemberDirectory(List<UserMenu> userMenu, long? organizationId, long? branchId, long? campusId, long? departmentId, string name, int? pin, int? bloodGroup);
        long CountTeamMemberListReport(List<UserMenu> userMenu, long organizationId, long? departmentId, long[] branchIds, long[] campusIds, int? memberTypes, int? memberStatus);
        long CountTeammemberSearchListReport(List<UserMenu> userMenu, long organizationId, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList);
        int GetCountSearchTeammemberList(List<UserMenu> userMenu, long organizationId, List<long> departmentIdList, List<long> branchIdList, List<long> campusIdList, List<int> memberTypeList, List<int> memberStatuList, string keyword, List<string> informationViewList);
        bool CheckHrAuthorizedTeamMember(List<UserMenu> userMenus, int pinList, DateTime? searchingDate = null, List<long> organizationIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null);
        int GetTeamMemberForLeaveSummaryCount(List<UserMenu> userMenus, DateTime searchingEffectiveDate, long organizationId, long branchId, long campusId, long departmentId, long designationId, string pinListString);
        int GetNewPin();

        #endregion

        #region Helper Function
        void GetCurrentInfos(long hrMemberId, out object campus, out string department, out string designation, out int empStatus,
            out string joindate, out string todate, out string officeShift, out int weekend, out int mentorPin);

        #endregion
        
    }

    public class TeamMemberService : BaseService, ITeamMemberService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly ITeamMemberDao _hrMemberDao;
        private readonly ITeamMemberNomineeIfnoDao _hrMemberNomineeIfnoDao;
        private readonly IEmploymentHistoryDao _hrEmploymentHistoryDao;
        private readonly IShiftWeekendHistoryDao _hrShiftWeekendHistoryDao;
        private readonly IMentorHistoryDao _hrMentorHistoryDao;
        private readonly IUserProfileDao _userProfileDao;
        private readonly IMaritalInfoDao _hrMaritalInfoDao;
        private readonly ILeaveDao _hrLeaveDao;
        private readonly IMembersLeaveSummaryDao _hrMembersLeaveSummaryDao;
        public readonly IUserProfileDao _UserProfileDao;
        public readonly IThanaDao _ThanaDao;
        public readonly IPostOfficeDao _postOfficeDao;
        private ISession _sessionMedia;
        private readonly IMemberOfficialDetailDao _memberOfficialDetailDao;
        private readonly ISalarySheetDao _salarySheetDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;
        private readonly IAttendanceDeviceDao _attendanceDeviceDao;
        private readonly IAttendanceDeviceMemberDao _attendanceDeviceMemberDao;

        public TeamMemberService(ISession session)
        {
            Session = session;

            _hrMemberDao = new TeamMemberDao()
            {
                Session = session
            };
            _hrEmploymentHistoryDao = new EmploymentHistoryDao()
            {
                Session = session
            };
            _hrShiftWeekendHistoryDao = new ShiftWeekendHistoryDao()
            {
                Session = session
            };
            _memberOfficialDetailDao = new MemberOfficialDetailDaoDao { Session = session };
            _hrMentorHistoryDao = new MentorHistoryDao() { Session = session };
            _userProfileDao = new UserProfileDao() { Session = session };
            _hrMaritalInfoDao = new MaritalInfoDao() { Session = session };
            _hrLeaveDao = new LeaveDao() { Session = session };
            _hrMembersLeaveSummaryDao = new MembersLeaveSummaryDao() { Session = session };
            _UserProfileDao = new UserProfileDao() { Session = session };
            _commonHelper = new CommonHelper();
            _postOfficeDao = new PostOfficeDao { Session = session };
            _ThanaDao = new ThanaDao { Session = session };
            _sessionMedia = NHibernateMediaSessionFactory.OpenSession();
            _hrMemberNomineeIfnoDao = new TeamMemberNomineeIfnoDao() { Session = session };
            _salarySheetDao = new SalarySheetDao { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao { Session = session };
            _attendanceDeviceDao = new AttendanceDeviceDao { Session = session };
            _attendanceDeviceMemberDao = new AttendanceDeviceMemberDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public TeamMember SaveNew(TeamMember hrMember, string joiningDate, string dateTo)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    DateTime currentDateTime = DateTime.Now;

                    #region save member

                    if (hrMember == null)
                        throw new NullObjectException("Team member is not found");

                    if (joiningDate == "" || dateTo == "")
                        throw new DataNotFoundException("Must be required joining date and dateTo");

                    if (Convert.ToDateTime(joiningDate) > Convert.ToDateTime(dateTo))
                        throw new DataNotFoundException("Joing date can not exceed dateTo");

                    var orgObj =
                        hrMember.MentorHistory.OrderByDescending(x => x.EffectiveDate)
                            .Select(x => x.Organization)
                            .FirstOrDefault();
                    if (orgObj == null)
                        throw new DataNotFoundException("Organization is not found");
                    
                    var employmentStatus =
                        hrMember.EmploymentHistory.OrderByDescending(x => x.EffectiveDate)
                            .Select(x => x.EmploymentStatus)
                            .FirstOrDefault();
                    var maritalStatus = hrMember.MaritalInfo
                        .OrderByDescending(x => x.MarriageDate)
                        .FirstOrDefault();

                    //get mentor dept,desi,org
                    var mntr = hrMember.MentorHistory.OrderByDescending(x => x.EffectiveDate).Select(x => x.Mentor).FirstOrDefault();
                    if (mntr == null)
                        throw new DataNotFoundException("Mentor is not found");
                    var emp = Session.QueryOver<EmploymentHistory>()
                        .Where(x => x.TeamMember.Id == mntr.Id && x.EffectiveDate <= DateTime.Now.Date)
                        .OrderBy(x => x.EffectiveDate)
                        .Desc.List<EmploymentHistory>().FirstOrDefault();

                    DoBeforeSave(hrMember);
                    hrMember.Pin = _hrMemberDao.GetNewPin();
                    _hrMemberDao.Save(hrMember);
                    hrMember = _hrMemberDao.LoadById(hrMember.Id);
                    #endregion

                    #region Attendacne Device Member
                    //This code is comment due to error
                    if (hrMember != null)
                    {
                        DateTime lastUpdateTime = DateTime.Now;
                        IList<AttendanceDevice> deviceList = _attendanceDeviceDao.LoadAllOk();

                        foreach (AttendanceDevice attendanceDevice in deviceList.Where(x => x.Status == 1))
                        {
                            AttendanceDeviceMember newDeviceMember = new AttendanceDeviceMember();
                            newDeviceMember.AttendanceDevice = attendanceDevice;
                            newDeviceMember.TeamMember = hrMember;
                            newDeviceMember.EnrollNo = _attendanceDeviceMemberDao.GetMaxEnrollNo(attendanceDevice.Id);
                            newDeviceMember.LastUpdateDateTime = lastUpdateTime;
                            newDeviceMember.IsSynced = 0;
                            newDeviceMember.LastSyncedDateTime = null;
                            Session.Save(newDeviceMember);
                        }
                    }

                    #endregion

                    #region save hrEmploymentHistory and hrEmploymentHistoryLog

                    //var empHistory = hrMember.EmploymentHistory.FirstOrDefault(x => x.EmploymentStatus == (int)MemberEmploymentStatus.Permanent);
                    var empHistory = hrMember.EmploymentHistory.FirstOrDefault();
                    if (empHistory != null && empHistory.EmploymentStatus == (int)MemberEmploymentStatus.Permanent)
                    {
                        #region Permanent
                        if (Convert.ToDateTime(joiningDate) == Convert.ToDateTime(dateTo))
                        {
                            foreach (var employmentHistory in hrMember.EmploymentHistory)
                            {
                                employmentHistory.TeamMember = hrMember;
                                employmentHistory.EmploymentStatus = employmentHistory.EmploymentStatus;
                                employmentHistory.EffectiveDate = Convert.ToDateTime(joiningDate);
                                _hrEmploymentHistoryDao.Save(employmentHistory);

                                #region log

                                var hrEmploymentHistoryLog = new EmploymentHistoryLog
                                {
                                    //BusinessId = WindowsIdentity.GetCurrent().Name,
                                    EmploymentStatus = employmentHistory.EmploymentStatus,
                                    EffectiveDate = employmentHistory.EffectiveDate,
                                    CampusId = employmentHistory.Campus.Id,
                                    DesignationId = employmentHistory.Designation.Id,
                                    DepartmentId = employmentHistory.Department.Id,
                                    TeamMemberId = hrMember.Id,
                                    EmploymentHistoryId = employmentHistory.Id,
                                    CreationDate = currentDateTime,
                                    CreateBy = GetCurrentUserId()
                                };
                                Session.Save(hrEmploymentHistoryLog);
                                //_hrEmploymentHistoryLogDao.Save(hrEmploymentHistoryLog);
                                //employmentHistory.EmploymentHistoryLog.Add(hrEmploymentHistoryLog);

                                #endregion
                            }
                        }
                        else
                        {
                            //var i = 1;
                            foreach (var employmentHistory in hrMember.EmploymentHistory)
                            {
                                var empStatus = employmentHistory.EmploymentStatus;
                                employmentHistory.TeamMember = hrMember;
                                employmentHistory.EmploymentStatus = (int)MemberEmploymentStatus.Probation;
                                employmentHistory.EffectiveDate = Convert.ToDateTime(joiningDate);
                                _hrEmploymentHistoryDao.Save(employmentHistory);
                                //_hrEmploymentHistoryDao.SaveOrUpdate(employmentHistory);

                                #region log

                                EmploymentHistoryLog hrEmploymentHistoryLog = new EmploymentHistoryLog
                                {
                                    //BusinessId = WindowsIdentity.GetCurrent().Name,
                                    EmploymentStatus = employmentHistory.EmploymentStatus,
                                    EffectiveDate = employmentHistory.EffectiveDate,
                                    CampusId = employmentHistory.Campus.Id,
                                    DesignationId = employmentHistory.Designation.Id,
                                    DepartmentId = employmentHistory.Department.Id,
                                    TeamMemberId = hrMember.Id,
                                    EmploymentHistoryId = employmentHistory.Id,
                                    CreationDate = currentDateTime,
                                    CreateBy = GetCurrentUserId()
                                };
                                //_hrEmploymentHistoryLogDao.Save(hrEmploymentHistoryLog);
                                //employmentHistory.EmploymentHistoryLog.Add(hrEmploymentHistoryLog);
                                Session.Save(hrEmploymentHistoryLog);

                                #endregion

                                var hrEmploymentHistory1 = new EmploymentHistory
                                {
                                    //BusinessId = WindowsIdentity.GetCurrent().Name,
                                    EmploymentStatus = empStatus,
                                    EffectiveDate = Convert.ToDateTime(dateTo),
                                    Campus = employmentHistory.Campus,
                                    Designation = employmentHistory.Designation,
                                    Department = employmentHistory.Department,
                                    TeamMember = hrMember,
                                    Rank = 10
                                };
                                _hrEmploymentHistoryDao.Save(hrEmploymentHistory1);

                                #region log

                                var hrEmploymentHistoryLog1 = new EmploymentHistoryLog();
                                hrEmploymentHistoryLog1.EmploymentStatus = hrEmploymentHistory1.EmploymentStatus;
                                hrEmploymentHistoryLog1.EffectiveDate = hrEmploymentHistory1.EffectiveDate;
                                hrEmploymentHistoryLog1.CampusId = hrEmploymentHistory1.Campus.Id;
                                hrEmploymentHistoryLog1.DesignationId = hrEmploymentHistory1.Designation.Id;
                                hrEmploymentHistoryLog1.DepartmentId = hrEmploymentHistory1.Department.Id;
                                hrEmploymentHistoryLog1.TeamMemberId = hrMember.Id;
                                hrEmploymentHistoryLog1.EmploymentHistoryId = hrEmploymentHistory1.Id;
                                hrEmploymentHistoryLog1.CreationDate = currentDateTime;
                                hrEmploymentHistoryLog1.CreateBy = GetCurrentUserId();
                                //hrEmploymentHistoryLog1.BusinessId = WindowsIdentity.GetCurrent().Name;
                                //_hrEmploymentHistoryLogDao.Save(hrEmploymentHistoryLog1);
                                Session.Save(hrEmploymentHistoryLog1);
                                //hrEmploymentHistory1.EmploymentHistoryLog.Add(hrEmploymentHistoryLog1);
                                //employmentHistories.Add(hrEmploymentHistory1);
                                //hrMember.EmploymentHistory.Insert(i, hrEmploymentHistory1);
                                break;
                                //i++;

                                #endregion
                            }
                        }
                        #endregion
                    }
                    else if (empHistory != null && empHistory.EmploymentStatus == (int)MemberEmploymentStatus.Probation)
                    {
                        #region Probation
                        foreach (var employmentHistory in hrMember.EmploymentHistory)
                        {
                            var empStatus = employmentHistory.EmploymentStatus;
                            employmentHistory.TeamMember = hrMember;
                            employmentHistory.EmploymentStatus = empStatus;
                            employmentHistory.EffectiveDate = Convert.ToDateTime(joiningDate);
                            _hrEmploymentHistoryDao.Save(employmentHistory);

                            #region log

                            EmploymentHistoryLog hrEmploymentHistoryLog = new EmploymentHistoryLog();
                            hrEmploymentHistoryLog.EmploymentStatus = employmentHistory.EmploymentStatus;
                            hrEmploymentHistoryLog.EffectiveDate = employmentHistory.EffectiveDate;
                            hrEmploymentHistoryLog.CampusId = employmentHistory.Campus.Id;
                            hrEmploymentHistoryLog.DesignationId = employmentHistory.Designation.Id;
                            hrEmploymentHistoryLog.DepartmentId = employmentHistory.Department.Id;
                            hrEmploymentHistoryLog.TeamMemberId = hrMember.Id;
                            hrEmploymentHistoryLog.EmploymentHistoryId = employmentHistory.Id;
                            hrEmploymentHistoryLog.CreationDate = currentDateTime;
                            hrEmploymentHistoryLog.CreateBy = GetCurrentUserId();
                            //hrEmploymentHistoryLog.BusinessId = WindowsIdentity.GetCurrent().Name;
                            //_hrEmploymentHistoryLogDao.Save(hrEmploymentHistoryLog);
                            Session.Save(hrEmploymentHistoryLog);
                            //employmentHistory.EmploymentHistoryLog.Add(hrEmploymentHistoryLog);

                            #endregion

                            var hrEmploymentHistory1 = new EmploymentHistory();
                            hrEmploymentHistory1.EmploymentStatus = (int)MemberEmploymentStatus.Retired;
                            hrEmploymentHistory1.EffectiveDate = Convert.ToDateTime(dateTo);
                            hrEmploymentHistory1.Campus = employmentHistory.Campus;
                            hrEmploymentHistory1.Designation = employmentHistory.Designation;
                            hrEmploymentHistory1.Department = employmentHistory.Department;
                            hrEmploymentHistory1.TeamMember = hrMember;
                            hrEmploymentHistory1.Rank = 10;
                            //hrEmploymentHistory1.BusinessId = WindowsIdentity.GetCurrent().Name;
                            _hrEmploymentHistoryDao.Save(hrEmploymentHistory1);

                            #region log

                            var hrEmploymentHistoryLog1 = new EmploymentHistoryLog();
                            hrEmploymentHistoryLog1.EmploymentStatus = hrEmploymentHistory1.EmploymentStatus;
                            hrEmploymentHistoryLog1.EffectiveDate = hrEmploymentHistory1.EffectiveDate;
                            hrEmploymentHistoryLog1.CampusId = hrEmploymentHistory1.Campus.Id;
                            hrEmploymentHistoryLog1.DesignationId = hrEmploymentHistory1.Designation.Id;
                            hrEmploymentHistoryLog1.DepartmentId = hrEmploymentHistory1.Department.Id;
                            hrEmploymentHistoryLog1.TeamMemberId = hrMember.Id;
                            hrEmploymentHistoryLog1.CreationDate = currentDateTime;
                            hrEmploymentHistoryLog1.CreateBy = GetCurrentUserId();
                            hrEmploymentHistoryLog1.EmploymentHistoryId = hrEmploymentHistory1.Id;
                            Session.Save(hrEmploymentHistoryLog1);
                            break;

                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        #region Other Status
                        foreach (var employmentHistory in hrMember.EmploymentHistory)
                        {
                            var empStatus = employmentHistory.EmploymentStatus;
                            employmentHistory.TeamMember = hrMember;
                            employmentHistory.EmploymentStatus = empStatus;
                            employmentHistory.EffectiveDate = Convert.ToDateTime(joiningDate);
                            _hrEmploymentHistoryDao.Save(employmentHistory);

                            #region log

                            EmploymentHistoryLog hrEmploymentHistoryLog = new EmploymentHistoryLog();
                            hrEmploymentHistoryLog.EmploymentStatus = employmentHistory.EmploymentStatus;
                            hrEmploymentHistoryLog.EffectiveDate = employmentHistory.EffectiveDate;
                            hrEmploymentHistoryLog.CampusId = employmentHistory.Campus.Id;
                            hrEmploymentHistoryLog.DesignationId = employmentHistory.Designation.Id;
                            hrEmploymentHistoryLog.DepartmentId = employmentHistory.Department.Id;
                            hrEmploymentHistoryLog.TeamMemberId = hrMember.Id;
                            hrEmploymentHistoryLog.EmploymentHistoryId = employmentHistory.Id;
                            hrEmploymentHistoryLog.CreationDate = currentDateTime;
                            hrEmploymentHistoryLog.CreateBy = GetCurrentUserId();
                            //hrEmploymentHistoryLog.BusinessId = WindowsIdentity.GetCurrent().Name;
                            //_hrEmploymentHistoryLogDao.Save(hrEmploymentHistoryLog);
                            Session.Save(hrEmploymentHistoryLog);
                            //employmentHistory.EmploymentHistoryLog.Add(hrEmploymentHistoryLog);

                            #endregion

                            var hrEmploymentHistory1 = new EmploymentHistory();
                            hrEmploymentHistory1.EmploymentStatus = (int)MemberEmploymentStatus.Retired;
                            hrEmploymentHistory1.EffectiveDate = Convert.ToDateTime(dateTo);
                            hrEmploymentHistory1.Campus = employmentHistory.Campus;
                            hrEmploymentHistory1.Designation = employmentHistory.Designation;
                            hrEmploymentHistory1.Department = employmentHistory.Department;
                            hrEmploymentHistory1.TeamMember = hrMember;
                            hrEmploymentHistory1.Rank = 10;
                            //hrEmploymentHistory1.BusinessId = WindowsIdentity.GetCurrent().Name;
                            _hrEmploymentHistoryDao.Save(hrEmploymentHistory1);

                            #region log

                            var hrEmploymentHistoryLog1 = new EmploymentHistoryLog();
                            hrEmploymentHistoryLog1.EmploymentStatus = hrEmploymentHistory1.EmploymentStatus;
                            hrEmploymentHistoryLog1.EffectiveDate = hrEmploymentHistory1.EffectiveDate;
                            hrEmploymentHistoryLog1.CampusId = hrEmploymentHistory1.Campus.Id;
                            hrEmploymentHistoryLog1.DesignationId = hrEmploymentHistory1.Designation.Id;
                            hrEmploymentHistoryLog1.DepartmentId = hrEmploymentHistory1.Department.Id;
                            hrEmploymentHistoryLog1.TeamMemberId = hrMember.Id;
                            hrEmploymentHistoryLog1.CreationDate = currentDateTime;
                            hrEmploymentHistoryLog1.CreateBy = GetCurrentUserId();
                            hrEmploymentHistoryLog1.EmploymentHistoryId = hrEmploymentHistory1.Id;
                            //hrEmploymentHistoryLog1.BusinessId = WindowsIdentity.GetCurrent().Name;
                            Session.Save(hrEmploymentHistoryLog1);
                            //_hrEmploymentHistoryLogDao.Save(hrEmploymentHistoryLog1);
                            //hrEmploymentHistory1.EmploymentHistoryLog.Add(hrEmploymentHistoryLog1);
                            //hrMember.EmploymentHistory.Add(hrEmploymentHistory1);
                            break;
                            //i++;

                            #endregion
                        }
                        #endregion
                    }

                    #endregion

                    #region save hrShiftWeekendHistory

                    foreach (var shiftWeekendHistory in hrMember.ShiftWeekendHistory)
                    {
                        shiftWeekendHistory.EffectiveDate = Convert.ToDateTime(joiningDate);
                        shiftWeekendHistory.Rank = 10;
                        shiftWeekendHistory.TeamMember = hrMember;
                        _hrShiftWeekendHistoryDao.Save(shiftWeekendHistory);

                        #region save hrShiftWeekendHistoryLog

                        var hrShiftWeekendHistoryLog = new ShiftWeekendHistoryLog();
                        hrShiftWeekendHistoryLog.Weekend = shiftWeekendHistory.Weekend;
                        hrShiftWeekendHistoryLog.EffectiveDate = Convert.ToDateTime(joiningDate);
                        hrShiftWeekendHistoryLog.OrganizationId = shiftWeekendHistory.Organization.Id;
                        hrShiftWeekendHistoryLog.ShiftId = shiftWeekendHistory.Shift.Id;
                        hrShiftWeekendHistoryLog.TeamMemberId = hrMember.Id;
                        hrShiftWeekendHistoryLog.ShiftWeekendHistoryId = shiftWeekendHistory.Id;
                        hrShiftWeekendHistoryLog.CreationDate = currentDateTime;
                        hrShiftWeekendHistoryLog.CreateBy = GetCurrentUserId();
                        //hrShiftWeekendHistoryLog.BusinessId = WindowsIdentity.GetCurrent().Name;
                        Session.Save(hrShiftWeekendHistoryLog);
                        //_hrShiftWeekendHistoryLogDao.Save(hrShiftWeekendHistoryLog);
                        //shiftWeekendHistory.ShiftWeekendHistoryLog.Add(hrShiftWeekendHistoryLog);

                        #endregion

                    }

                    #endregion

                    #region save hrMentorHistory and hrMentorHistoryLog

                    foreach (var mentorHistory in hrMember.MentorHistory)
                    {
                        mentorHistory.TeamMember = hrMember;
                        var checkMentorObj = _hrMemberDao.GetMember(mentorHistory.Pin);
                        if (checkMentorObj == null) continue;
                        mentorHistory.EffectiveDate = Convert.ToDateTime(joiningDate);
                        mentorHistory.Mentor = checkMentorObj;
                        //object des, dep, org;
                        //_hrEmploymentHistoryDao.GetCurrentInformations(checkMentorObj.Id, out des, out dep, out org);
                        //if (des == null || dep == null || org == null) continue;

                        //var emp = hrMember.EmploymentHistory.Where(
                        //    x => x.TeamMember == checkMentorObj && x.EffectiveDate <= DateTime.Now.Date)
                        //    .OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

                        if (emp != null)
                        {
                            mentorHistory.MentorDesignation = emp.Designation;
                            mentorHistory.MentorDepartment = emp.Department;
                            mentorHistory.MentorOrganization = emp.Campus.Branch.Organization;
                        }

                        //mentorHistory.MentorDesignation = (Designation) des;
                        //mentorHistory.MentorDepartment = (Department) dep;
                        //mentorHistory.MentorOrganization = (Organization) org;
                        mentorHistory.Rank = 10;
                        mentorHistory.TeamMember = hrMember;
                        _hrMentorHistoryDao.Save(mentorHistory);

                        #region log

                        var hrMentorHistoryLog = new MentorHistoryLog();
                        //  hrMentorHistoryLog.MentorName = mentorHistory.MentorName;
                        hrMentorHistoryLog.EffectiveDate = Convert.ToDateTime(joiningDate);
                        hrMentorHistoryLog.MemberDesignationId = mentorHistory.Designation.Id;
                        hrMentorHistoryLog.MemberDepartmentId = mentorHistory.Department.Id;
                        hrMentorHistoryLog.MemberOrganizationId = mentorHistory.Organization.Id;
                        hrMentorHistoryLog.TeamMemberId = hrMember.Id;
                        hrMentorHistoryLog.Pin = mentorHistory.Pin;
                        hrMentorHistoryLog.MentorId = checkMentorObj.Id;
                        hrMentorHistoryLog.MentorDesignationId = mentorHistory.MentorDesignation.Id;
                        hrMentorHistoryLog.MentorDepartmentId = mentorHistory.MentorDepartment.Id;
                        hrMentorHistoryLog.MentorOrganizationId = mentorHistory.MentorOrganization.Id;
                        hrMentorHistoryLog.MentorHistoryId = mentorHistory.Id;
                        hrMentorHistoryLog.CreationDate = currentDateTime;
                        hrMentorHistoryLog.CreateBy = GetCurrentUserId();
                        //hrMentorHistoryLog.BusinessId = WindowsIdentity.GetCurrent().Name;
                        Session.Save(hrMentorHistoryLog);
                        // _hrMentorHistoryLogDao.Save(hrMentorHistoryLog);

                        #endregion
                    }

                    #endregion

                    #region save hrMaritalInfo

                    foreach (var maritalInfo in hrMember.MaritalInfo)
                    {
                        maritalInfo.TeamMember = hrMember;
                        _hrMaritalInfoDao.Save(maritalInfo);
                    }

                    #endregion

                    #region hrMemberLeaveSummary

                    if (maritalStatus == null)
                        throw new DataNotFoundException("maritalStatus is not found");
                    IList<Leave> leave = _hrLeaveDao.LoadLeave(orgObj.Id, hrMember.Gender, employmentStatus, (int)maritalStatus.MaritalStatus);
                    DateTime yearEndingDate = new DateTime(DateTime.Today.Year, 12, 31);
                    DateTime calculationEndingDate = yearEndingDate;
                    if (employmentStatus != 2)
                    {
                        if (yearEndingDate > Convert.ToDateTime(dateTo))
                        {
                            calculationEndingDate = Convert.ToDateTime(dateTo);
                        }
                    }

                    #region Leave
                    foreach (var hrleave in leave)
                    {
                        var hrMemberLeaveSummary = new MemberLeaveSummary();
                        hrMemberLeaveSummary.Rank = 10;
                        hrMemberLeaveSummary.ApprovedLeave = 0;
                        hrMemberLeaveSummary.PendingLeave = 0;
                        hrMemberLeaveSummary.CarryForwardLeave = 0;
                        hrMemberLeaveSummary.EncashLeave = 0;
                        hrMemberLeaveSummary.VoidLeave = 0;
                        hrMemberLeaveSummary.Year = DateTime.Now.Year;
                        hrMemberLeaveSummary.TeamMember = hrMember;
                        hrMemberLeaveSummary.Leave = hrleave;
                        hrMemberLeaveSummary.Organization = orgObj; //_organizationDao.LoadById(organizationId);
                        //hrMemberLeaveSummary.BusinessId = WindowsIdentity.GetCurrent().Name;

                        if (hrleave.StartFrom == 1) // Start from joining date
                        {
                            double noOfDays = (double)hrleave.NoOfDays;
                            noOfDays = (noOfDays * ((calculationEndingDate - Convert.ToDateTime(joiningDate)).TotalDays)) /
                                       365.00;
                            int totalNoOfDays = (int)Math.Ceiling(noOfDays);
                            //Profile Leave Setting add condition
                            if (hrleave.IsCarryforward == true && totalNoOfDays > hrleave.MaxCarryDays)
                            {
                                totalNoOfDays = (hrleave.MaxCarryDays != null)
                                    ? Convert.ToInt32(hrleave.MaxCarryDays)
                                    : 0;
                            }
                            else if (hrleave.IsCarryforward == false && totalNoOfDays > hrleave.NoOfDays)
                            {
                                totalNoOfDays = (hrleave.NoOfDays != null) ? Convert.ToInt32(hrleave.NoOfDays) : 0;
                            }
                            //Profile Leave Setting add condition end
                            hrMemberLeaveSummary.AvailableBalance = totalNoOfDays;
                            hrMemberLeaveSummary.CurrentYearLeave = totalNoOfDays;
                            hrMemberLeaveSummary.TotalLeaveBalance = totalNoOfDays;
                            
                            _hrMembersLeaveSummaryDao.Save(hrMemberLeaveSummary);
                        }
                        else if (hrleave.StartFrom == 2 && employmentStatus == (int)MemberEmploymentStatus.Permanent)
                        // Start from Permanent date
                        {
                            double noOfDays = (double)hrleave.NoOfDays;
                            noOfDays = (noOfDays * ((calculationEndingDate - Convert.ToDateTime(dateTo)).TotalDays)) /
                                       365.00;
                            int totalNoOfDays = (int)Math.Ceiling(noOfDays);
                            //Profile Leave Setting add condition
                            if (hrleave.IsCarryforward == true && totalNoOfDays > hrleave.MaxCarryDays)
                            {
                                totalNoOfDays = (hrleave.MaxCarryDays != null)
                                    ? Convert.ToInt32(hrleave.MaxCarryDays)
                                    : 0;
                            }
                            else if (hrleave.IsCarryforward == false && totalNoOfDays > hrleave.NoOfDays)
                            {
                                totalNoOfDays = (hrleave.NoOfDays != null) ? Convert.ToInt32(hrleave.NoOfDays) : 0;
                            }
                            //Profile Leave Setting add condition end
                            hrMemberLeaveSummary.AvailableBalance = totalNoOfDays;
                            hrMemberLeaveSummary.CurrentYearLeave = totalNoOfDays;
                            hrMemberLeaveSummary.TotalLeaveBalance = totalNoOfDays;
                            hrMemberLeaveSummary.TeamMember = hrMember;
                            _hrMembersLeaveSummaryDao.Save(hrMemberLeaveSummary);
                        }
                        else if (hrleave.StartFrom == 3 && employmentStatus == (int)MemberEmploymentStatus.Permanent)
                        // Start from After one year of permanent
                        {
                            DateTime afterOneYearOfPermanentDate = Convert.ToDateTime(dateTo).AddYears(1);
                            if (afterOneYearOfPermanentDate < yearEndingDate)
                            {
                                double noOfDays = (double)hrleave.NoOfDays;
                                noOfDays = (noOfDays * ((yearEndingDate - afterOneYearOfPermanentDate).TotalDays)) /
                                           365.00;
                                int totalNoOfDays = (int)Math.Ceiling(noOfDays);
                                //Profile Leave Setting add condition
                                if (hrleave.IsCarryforward == true && totalNoOfDays > hrleave.MaxCarryDays)
                                {
                                    totalNoOfDays = (hrleave.MaxCarryDays != null)
                                        ? Convert.ToInt32(hrleave.MaxCarryDays)
                                        : 0;
                                }
                                else if (hrleave.IsCarryforward == false && totalNoOfDays > hrleave.NoOfDays)
                                {
                                    totalNoOfDays = (hrleave.NoOfDays != null)
                                        ? Convert.ToInt32(hrleave.NoOfDays)
                                        : 0;
                                }
                                //Profile Leave Setting add condition end
                                hrMemberLeaveSummary.AvailableBalance = totalNoOfDays;
                                hrMemberLeaveSummary.CurrentYearLeave = totalNoOfDays;
                                hrMemberLeaveSummary.TotalLeaveBalance = totalNoOfDays;
                                hrMemberLeaveSummary.TeamMember = hrMember;
                                _hrMembersLeaveSummaryDao.Save(hrMemberLeaveSummary);
                            }
                        }
                    }
                    #endregion

                    #endregion

                    //Session.Flush();
                    //Session.FlushMode = NHibernate.FlushMode.Auto;
                    trans.Commit();
                    return hrMember;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool Update(long id, TeamMember hrMember)
        {

            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    var dbHrMember = _hrMemberDao.LoadById(id);
                    dbHrMember.Name = hrMember.Name;
                    dbHrMember.PersonalContact = hrMember.PersonalContact;
                    _hrMemberDao.Update(dbHrMember);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool UpdateBasicInformation(TeamMember member)
        {
            ITransaction trans = null;
            try
            {
                if (member == null)
                    throw new NullObjectException("Invalid Team member");
                CheckBeforeSavingUpdateBasicInformation(member);
                using (trans = Session.BeginTransaction())
                {
                    var date = DateTime.Now;
                    //member.Name = nickName;
                    //member.PersonalContact = personalContact;
                    //var officalDetail = member.MemberOfficialDetails.FirstOrDefault();

                    #region old code
                    //if (member.MemberOfficialDetails.Any())
                    //{

                    //    if (member.MemberOfficialDetails[0].CardNo != cardNumber || 
                    //        member.MemberOfficialDetails[0].OfficialContact != officialContact || 
                    //        member.MemberOfficialDetails[0].OfficialEmail != officialEmail)
                    //    {
                    //        member.MemberOfficialDetails[0].CardNo = cardNumber;
                    //        member.MemberOfficialDetails[0].OfficialContact = officialContact;
                    //        member.MemberOfficialDetails[0].OfficialEmail = officialEmail;
                    //        member.MemberOfficialDetails[0].ModificationDate = date;
                    //        member.MemberOfficialDetails[0].ModifyBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                    //        //log
                    //        SaveMemberOfficialDetailLog(member.MemberOfficialDetails[0], cardNumber, officialContact, officialEmail, date);
                    //    }
                    //}
                    //else
                    //{
                    //    MemberOfficialDetail officalDetail = new MemberOfficialDetail
                    //    {
                    //        TeamMember = member,
                    //        Rank = 10,
                    //        CardNo = cardNumber,
                    //        OfficialContact = officialContact,
                    //        OfficialEmail = officialEmail,
                    //        CreateBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId()),
                    //        ModifyBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId()),
                    //        CreationDate = date,
                    //        ModificationDate = date,
                    //        Status = 1
                    //    };
                    //    member.MemberOfficialDetails.Add(officalDetail);

                    //    //log
                    //    SaveMemberOfficialDetailLog(officalDetail, cardNumber, officialContact, officialEmail, date);
                    //}
                    #endregion

                    _hrMemberDao.SaveOrUpdate(member);

                    #region Attendance Device Member

                    DateTime lastUpdateTime = DateTime.Now;
                    IList<AttendanceDeviceMember> attendanceDeviceMembers = _attendanceDeviceMemberDao.LoadAttendanceDeviceMember(member.Id);
                    foreach (AttendanceDeviceMember deviceMember in attendanceDeviceMembers)
                    {
                        deviceMember.LastUpdateDateTime = lastUpdateTime;
                        Session.Update(deviceMember);
                    }

                    #endregion
                
                    var officalDetail = Session.QueryOver<MemberOfficialDetail>()
                        .Where(x => x.TeamMember.Id == member.Id).List<MemberOfficialDetail>().FirstOrDefault();
                    SaveMemberOfficialDetailLog(officalDetail, date);

                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool UpdatePersonalInformation(TeamMember member, long? presentThana, long? permanentThana, long? presentPostOffice, long? permanentPostOffice)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    CheckBeforeSavingPersonalInformation(member);

                    #region postoffice & Thana Check
                    Thana presentThanaObj = null;
                    Thana permanentThanaObj = null;
                    Postoffice presentPostofficeObj = null;
                    Postoffice permanentPostofficeObj = null;
                    if (presentThana != null)
                    {
                        presentThanaObj = _ThanaDao.LoadById((long)presentThana);
                    }
                    if (permanentThana != null)
                    {
                        permanentThanaObj = _ThanaDao.LoadById((long)permanentThana);
                    }
                    if (presentPostOffice != null)
                    {
                        presentPostofficeObj = _postOfficeDao.LoadById((long)presentPostOffice);
                    }
                    if (permanentPostOffice != null)
                    {
                        permanentPostofficeObj = _postOfficeDao.LoadById((long)permanentPostOffice);
                    }
                    if ((presentThanaObj != null && presentPostofficeObj == null) || (presentThanaObj == null && presentPostofficeObj != null))
                    {
                        throw new InvalidDataException("You have to select both Post office and Thana for Present Address!");
                    }
                    if ((permanentThanaObj != null && permanentPostofficeObj == null) || (permanentThanaObj == null && permanentPostofficeObj != null))
                    {
                        throw new InvalidDataException("You have to select both Post office and Thana for Permanent Address!");
                    }
                    #endregion

                    //if (member.BloodGroup == 0)
                    //    throw new InvalidDataException("Invalid Blood group");

                    member.PresentThana = presentThanaObj;
                    member.PermanentThana = permanentThanaObj;
                    member.PresentPostOffice = presentPostofficeObj;
                    member.PermanentPostOffice = permanentPostofficeObj;

                    _hrMemberDao.Update(member);

                    #region Attendance Device Member

                    DateTime lastUpdateTime = DateTime.Now;
                    IList<AttendanceDeviceMember> attendanceDeviceMembers = _attendanceDeviceMemberDao.LoadAttendanceDeviceMember(member.Id);
                    foreach (AttendanceDeviceMember deviceMember in attendanceDeviceMembers)
                    {
                        deviceMember.LastUpdateDateTime = lastUpdateTime;
                        Session.Update(deviceMember);
                    }

                    #endregion

                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool UpdateParentalInformation(TeamMember member)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    if (member == null)
                        throw new NullObjectException("Team member is not empty");

                    CheckBeforeSavingParentalInformation(member.FatherContactNo, member.MotherContactNo);

                    if (String.IsNullOrEmpty(member.FatherNameEng) || String.IsNullOrWhiteSpace(member.FatherNameEng))
                        throw new InvalidDataException("Father name english is not empty");
                    if (String.IsNullOrEmpty(member.FatherNameBang) || String.IsNullOrWhiteSpace(member.FatherNameBang))
                        throw new InvalidDataException("Father name bangla is not empty");
                    if (String.IsNullOrEmpty(member.MotherNameEng) || String.IsNullOrWhiteSpace(member.MotherNameEng))
                        throw new InvalidDataException("Mother name english not empty");
                    if (String.IsNullOrEmpty(member.MotherNameBang) || String.IsNullOrWhiteSpace(member.MotherNameBang))
                        throw new InvalidDataException("Mother name bangla not empty");
                    if (!String.IsNullOrEmpty(member.FatherContactNo) && !String.IsNullOrWhiteSpace(member.FatherContactNo))
                    {
                        if (member.FatherContactNo.Length == 11)
                            member.FatherContactNo = "88" + member.FatherContactNo;
                        else if (member.FatherContactNo.Length == 10)
                            member.FatherContactNo = "880" + member.FatherContactNo;
                        //member.FatherContactNo = fatherContactNumber;
                    }
                    if (!String.IsNullOrEmpty(member.MotherContactNo) && !String.IsNullOrWhiteSpace(member.MotherContactNo))
                    {
                        if (member.MotherContactNo.Length == 11)
                            member.MotherContactNo = "88" + member.MotherContactNo;
                        else if (member.MotherContactNo.Length == 10)
                            member.MotherContactNo = "880" + member.MotherContactNo;
                        // member.MotherContactNo = motherContactNumber;
                    }

                    _hrMemberDao.Update(member);

                    #region Attendance Device Member

                    DateTime lastUpdateTime = DateTime.Now;
                    IList<AttendanceDeviceMember> attendanceDeviceMembers = _attendanceDeviceMemberDao.LoadAttendanceDeviceMember(member.Id);
                    foreach (AttendanceDeviceMember deviceMember in attendanceDeviceMembers)
                    {
                        deviceMember.LastUpdateDateTime = lastUpdateTime;
                        Session.Update(deviceMember);
                    }

                    #endregion

                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool UpdateSpecialization(TeamMember member)
        {
            ITransaction trans = null;
            try
            {
                if (member == null)
                    throw new NullObjectException("Invalid team member!");
                using (trans = Session.BeginTransaction())
                {
                    member.Specialization = !string.IsNullOrEmpty(member.Specialization) ? member.Specialization.ToString(CultureInfo.CurrentCulture) : "";
                    member.ExtracurricularActivities = !string.IsNullOrEmpty(member.ExtracurricularActivities)
                        ? member.ExtracurricularActivities.ToString(CultureInfo.CurrentCulture) : "";
                    _hrMemberDao.Update(member);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }

        public void UpdateTaxInformation(List<UserMenu> userMenus, TeamMember teamMember)
        {
            ITransaction trans = null;
            try
            {
                if (teamMember == null)
                    throw new InvalidDataException("Invalid team member.");

                if (String.IsNullOrEmpty(teamMember.TinNo) && !string.IsNullOrEmpty(teamMember.TaxesCircle) &&
                    !String.IsNullOrEmpty(teamMember.TaxesZone))
                {
                    throw new InvalidDataException("Please enter TIN.");
                }

                if (userMenus == null)
                    throw new InvalidDataException("Invalid user menu.");

                var memberIdList = LoadHrAuthorizedTeamMemberId(userMenus, DateTime.Now.Date, pinList: new List<int>() { teamMember.Pin });
                if (memberIdList == null || memberIdList.Count == 0)
                {
                    throw new InvalidDataException("Member is not found/authorized.");
                }

                if (_hrMemberDao.HasTinNumberAlready(teamMember.TinNo, teamMember.Id))
                {
                    throw new InvalidDataException(
                        "This TIN alreay assign for another TeamMember. Please contact to HR.");
                }

                using (trans = Session.BeginTransaction())
                {
                    _hrMemberDao.SaveOrUpdate(teamMember);
                    trans.Commit();
                }
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        private void FillCreatedModifiedInfo<T>(T objectToFill, DateTime? dateTime = null, bool createdInfo = true, bool modifiedInfo = true)
        {
            if (dateTime == null)
                dateTime = DateTime.Now;
            IBaseEntity<long> entity = (IBaseEntity<long>)objectToFill;
            if (createdInfo)
            {
                //entity.CreateBy = (entity.CreateBy != 0) ? Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId()): entity.CreateBy;
                entity.CreateBy = GetCurrentUserId();
                entity.CreationDate = dateTime.Value;
            }
            if (modifiedInfo)
            {
                //entity.ModifyBy = (entity.ModifyBy != 0) ? Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId()) : entity.ModifyBy;
                entity.ModifyBy = GetCurrentUserId();
                entity.ModificationDate = dateTime.Value;

            }

        }

        public void TranferTeamMember(List<UserMenu> userMenus, TeamMember transferingTeamMember, DateTime transferingDateTime)
        {
            ITransaction trans = null;
            try
            {
                DateTime dateTime = DateTime.Now;
                List<EmploymentHistory> validEmploymentHistoryList = new List<EmploymentHistory>();
                List<ShiftWeekendHistory> validShiftWeekendHistoryList = new List<ShiftWeekendHistory>();
                List<MentorHistory> validMentorHistoryList = new List<MentorHistory>();
                List<MemberOfficialDetail> validMemberOfficialDetailList = new List<MemberOfficialDetail>();

                #region basic validation

                if (userMenus == null)
                {
                    throw new InvalidDataException("Invalid menu");
                }

                if (transferingTeamMember == null)
                {
                    throw new InvalidDataException("No information to transfer team member");
                }

                if (transferingTeamMember.Id <= 0)
                {
                    throw new InvalidDataException("Invalid Team Member");
                }
                TeamMember savingTeamMember = _hrMemberDao.LoadById(transferingTeamMember.Id);
                if (savingTeamMember == null)
                {
                    throw new InvalidDataException("Invalid Team Member");
                }
                var count = savingTeamMember.LeaveApplication.Count;
                #endregion

                #region block user for transfer

                EmploymentHistory currentLastActiveEmployementHistory = savingTeamMember.EmploymentHistory.Where(x => x.Status != EmploymentHistory.EntityStatus.Delete)
                        .OrderByDescending(x => x.EffectiveDate)
                        .ThenByDescending(x => x.Id)
                        .Take(1)
                        .SingleOrDefault();
                int currentOrganizationEmploymentHistoryCount = transferingTeamMember.EmploymentHistory
                        .Where(x => x.Designation.Organization.Id == currentLastActiveEmployementHistory.Designation.Organization.Id && x.Status != EmploymentHistory.EntityStatus.Delete)
                        .ToList()
                        .Count();

                if (currentOrganizationEmploymentHistoryCount > 0)
                {
                    throw new InvalidDataException("You can't transfer Team Member to Same Organization. Use Add new employment history.");
                }

                int futureEmploymentHistoryCount =
                    savingTeamMember.EmploymentHistory.Where(
                        x => x.EffectiveDate >= transferingDateTime && x.Status != EmploymentHistory.EntityStatus.Delete)
                        .ToList()
                        .Count();
                if (futureEmploymentHistoryCount > 0)
                {
                    throw new InvalidDataException("This Team Member has total " +
                                                   futureEmploymentHistoryCount.ToString() +
                                                   " future Employment history. Please remove those before transfering this team member.");
                }

                int sameDayEmploymentHistoryCount =
                    savingTeamMember.EmploymentHistory.Where(
                        x =>
                            x.EffectiveDate.Date >= transferingDateTime.Date &&
                            x.Status != EmploymentHistory.EntityStatus.Delete).ToList().Count();
                if (sameDayEmploymentHistoryCount > 0)
                {
                    throw new InvalidDataException("This Team Member has total " +
                                                   sameDayEmploymentHistoryCount.ToString() +
                                                   " Employment history for to " +
                                                   transferingDateTime.ToString("dd/mm/yyyy") +
                                                   ". you can not transfer this team member this day again.");
                }

                int futureShiftWeekendHistoryCount =
                    savingTeamMember.ShiftWeekendHistory.Where(
                        x => x.EffectiveDate >= transferingDateTime && x.Status != EmploymentHistory.EntityStatus.Delete)
                        .ToList()
                        .Count();
                if (futureShiftWeekendHistoryCount > 0)
                {
                    throw new InvalidDataException("This Team Member has total " +
                                                   futureShiftWeekendHistoryCount.ToString() +
                                                   " future Shift Weekend history. Please remove those before transfering this team member.");
                }
                int futureLeaveApplicationCount =
                    savingTeamMember.LeaveApplication.Where(
                        x =>
                            x.DateTo >= transferingDateTime &&
                            (x.LeaveStatus == (int)LeaveStatus.Pending || x.LeaveStatus == (int)LeaveStatus.Approved))
                        .ToList()
                        .Count();
                if (futureLeaveApplicationCount > 0)
                {
                    throw new InvalidDataException("This Team Member has total " +
                                                   futureLeaveApplicationCount.ToString() +
                                                   " future leave application . Please remove those before transfering this team member.");
                }



                #endregion
                
                #region authorization check

                //Organization teamMemberCurrentOrganization = _hrMemberDao.GetTeamMemberOrganization(DateTime.Now,
                //    savingTeamMember.Id, null);
                //Branch teamMemberCurrentBranch = _hrMemberDao.GetTeamMemberBranch(DateTime.Now, savingTeamMember.Id,
                //    null);

                //List<long> orgnizationIdList = AuthHelper.LoadOrganizationIdList(userMenus,
                //    _commonHelper.ConvertIdToList(teamMemberCurrentOrganization.Id));
                //List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenus, orgnizationIdList, null,
                //    _commonHelper.ConvertIdToList(teamMemberCurrentBranch.Id));
                //if (!branchIdList.Any())
                //{
                //    throw new InvalidDataException("Your are not authorized to access this team member information");
                //}
                List<long> memberIdList = LoadHrAuthorizedTeamMemberId(userMenus, DateTime.Now.Date,
                    pinList: new List<int>() { savingTeamMember.Pin });
                if (!memberIdList.Any())
                {
                    throw new InvalidDataException("Your are not authorized to access this team member information");
                }

                #endregion

                #region object null check

                if (!transferingTeamMember.EmploymentHistory.Any() || !transferingTeamMember.ShiftWeekendHistory.Any())
                {
                    throw new InvalidDataException(
                        "Eithe Employment history nor Shift weekend history information is missing");
                }

                #endregion

                #region form check

                #region EmploymentHistory

                if (transferingTeamMember.EmploymentHistory.Any())
                {
                    foreach (var employmentHistory in transferingTeamMember.EmploymentHistory)
                    {
                        if (employmentHistory.Department == null)
                        {
                            throw new InvalidDataException("Invalid Department For Employment History!");
                        }
                        if (employmentHistory.Designation == null)
                        {
                            throw new InvalidDataException("Invalid Designation For Employment History!");
                        }
                        if (employmentHistory.Campus == null)
                        {
                            throw new InvalidDataException("Invalid Campus For Employment History!");
                        }
                        employmentHistory.TeamMember = savingTeamMember;
                        employmentHistory.Status = EmploymentHistory.EntityStatus.Active;
                        employmentHistory.Rank = 10;
                        FillCreatedModifiedInfo(employmentHistory, dateTime);
                        validEmploymentHistoryList.Add(employmentHistory);
                    }
                }

                #endregion

                #region ShiftWeekendHistory

                if (transferingTeamMember.ShiftWeekendHistory.Any())
                {
                    foreach (var shiftWeekendHistory in transferingTeamMember.ShiftWeekendHistory)
                    {
                        if (shiftWeekendHistory.Organization == null)
                        {
                            throw new InvalidDataException("Invalid Oganization For ShiftWeekend History!");
                        }
                        if (shiftWeekendHistory.Shift == null)
                        {
                            throw new InvalidDataException("Invalid Shift For ShiftWeekend History!");
                        }
                        if (shiftWeekendHistory.Weekend == null)
                        {
                            throw new InvalidDataException("Invalid Weekend For ShiftWeekend History!");
                        }

                        shiftWeekendHistory.TeamMember = savingTeamMember;
                        shiftWeekendHistory.Status = EmploymentHistory.EntityStatus.Active;
                        shiftWeekendHistory.Rank = 10;
                        FillCreatedModifiedInfo(shiftWeekendHistory, dateTime);
                        validShiftWeekendHistoryList.Add(shiftWeekendHistory);
                    }
                }

                #endregion

                #region Mentor History

                if (transferingTeamMember.MentorHistory.Any())
                {
                    foreach (var mentorHistory in transferingTeamMember.MentorHistory)
                    {
                        //MentorHistory currentMentorHistory = savingTeamMember.MentorHistory.Where(x => x.EffectiveDate.Value <= dateTime).OrderByDescending(x => x.EffectiveDate).ThenByDescending(x => x.Id).SingleOrDefault();
                        //if()
                        if (mentorHistory.Mentor.Pin > 0)
                        {
                            TeamMember mentor = _hrMemberDao.GetMember(mentorHistory.Mentor.Pin);
                            if (mentor == null)
                            {
                                throw new InvalidDataException("Invalid Mentor Information!");
                            }

                            Organization mentorOrganization = _hrMemberDao.GetTeamMemberOrganization(dateTime, mentor.Id,
                                null);
                            Department mentorDepartment = _hrMemberDao.GetTeamMemberDepartment(dateTime, mentor.Id, null);
                            Designation mentorDesignation = _hrMemberDao.GetTeamMemberDesignation(dateTime, mentor.Id,
                                null);
                            if (mentorOrganization == null || mentorDepartment == null || mentorDesignation == null ||
                                mentorHistory.Organization == null || mentorHistory.Department == null ||
                                mentorHistory.Designation == null || mentorHistory.EffectiveDate == null)
                            {
                                throw new InvalidDataException("Invalid Mentory Hsitory Info !");
                            }

                            mentorHistory.MentorDepartment = mentorDepartment;
                            mentorHistory.MentorDesignation = mentorDesignation;
                            mentorHistory.MentorOrganization = mentorOrganization;
                            mentorHistory.TeamMember = savingTeamMember;
                            mentorHistory.Pin = mentor.Pin;
                            FillCreatedModifiedInfo(mentorHistory, dateTime);
                            validMentorHistoryList.Add(mentorHistory);
                        }
                    }
                }

                #endregion

                #region Member Offical Information

                if (transferingTeamMember.MemberOfficialDetails.Any())
                {
                    foreach (var memberOfficialDetails in transferingTeamMember.MemberOfficialDetails)
                    {
                        //if (memberOfficialDetails != null)
                        //{
                        if (!String.IsNullOrEmpty(memberOfficialDetails.CardNo.Trim()) &&
                            (memberOfficialDetails.Status != MemberOfficialDetail.EntityStatus.Active &&
                             memberOfficialDetails.Status != MemberOfficialDetail.EntityStatus.Inactive))
                        {
                            throw new InvalidDataException("Invalid Offical Details Information!");
                        }

                        MemberOfficialDetail officialDetail = new MemberOfficialDetail();
                        if (memberOfficialDetails.Id > 0)
                        {
                            officialDetail =
                                savingTeamMember.MemberOfficialDetails.SingleOrDefault(
                                    x => x.Id == memberOfficialDetails.Id);
                            if (officialDetail == null)
                            {
                                throw new InvalidDataException("Invalid Offical Details Information!");
                            }
                        }
                        else
                        {
                            officialDetail.CreationDate = dateTime;
                            officialDetail.CreateBy = GetCurrentUserId();
                            //(memberOfficialDetails.CreateBy != 0)
                            //? Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId())
                            //: memberOfficialDetails.CreateBy;
                        }
                        officialDetail.Status = memberOfficialDetails.Status;
                        officialDetail.CardNo = memberOfficialDetails.CardNo.Trim();
                        officialDetail.OfficialContact = memberOfficialDetails.OfficialContact;
                        officialDetail.OfficialEmail = memberOfficialDetails.OfficialEmail;
                        officialDetail.ModificationDate = dateTime;
                        officialDetail.ModifyBy = GetCurrentUserId();
                        //(memberOfficialDetails.ModifyBy != 0)
                        //? Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId())
                        //: memberOfficialDetails.ModifyBy;
                        if (memberOfficialDetails.Id <= 0)
                        {
                            validMemberOfficialDetailList.Add(officialDetail);
                        }
                        //}
                    }
                }

                #endregion

                #endregion

                #region Same Organization Check

                if ((transferingTeamMember.EmploymentHistory.Any() && transferingTeamMember.ShiftWeekendHistory.Any()))
                {
                    long baseOrganizationId = transferingTeamMember.EmploymentHistory[0].Department.Organization.Id;
                    if (
                        transferingTeamMember.EmploymentHistory.Any(
                            employmentHistory =>
                                (employmentHistory.Department.Organization.Id != baseOrganizationId) ||
                                (employmentHistory.Campus.Branch.Organization.Id != baseOrganizationId) ||
                                (employmentHistory.Designation.Organization.Id != baseOrganizationId)))
                    {
                        throw new InvalidDataException(
                            "Department, Designation and Campus are not from same Organization");
                    }
                    if (
                        transferingTeamMember.ShiftWeekendHistory.Any(
                            shiftHistory =>
                                (shiftHistory.Organization.Id != baseOrganizationId) ||
                                (shiftHistory.Shift.Organization.Id != baseOrganizationId)))
                    {
                        throw new InvalidDataException(
                            "Shift, Department, Designation and Campus are not from same Organization");
                    }
                }

                #endregion
                
                #region Is Post Check

                SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(transferingTeamMember.Id, null);
                AllowanceSheet allowanceSheet = _allowanceSheetDao.GetTeamMemberLastIsSubmittedAllowanceSheet(transferingTeamMember.Id, null);
                if (salarySheet != null)
                {
                    if (validEmploymentHistoryList.Any() && validEmploymentHistoryList.Any(employmentHistory => salarySheet.EndDate.Date >= employmentHistory.EffectiveDate.Date))
                        throw new InvalidDataException("This Team member has already salary sheet. Please transfer after Date(" + salarySheet.EndDate.Date.ToString("d MMM,yyyy") + ")");
                    if (validShiftWeekendHistoryList.Any() && validShiftWeekendHistoryList.Any(shiftWeekendHistory => salarySheet.EndDate.Date >= shiftWeekendHistory.EffectiveDate.Value.Date))
                        throw new InvalidDataException("This Team member has already salary sheet. Please transfer after Date(" + salarySheet.EndDate.Date.ToString("d MMM,yyyy") + ")");
                    if (validMentorHistoryList.Any() && validMentorHistoryList.Any(mentorHistory => salarySheet.EndDate.Date >= mentorHistory.EffectiveDate.Value.Date))
                        throw new InvalidDataException("This Team member has already salary sheet. Please transfer after Date(" + salarySheet.EndDate.Date.ToString("d MMM,yyyy") + ")");
                }
                if (allowanceSheet != null)
                {
                    if (validEmploymentHistoryList.Any() && validEmploymentHistoryList.Any(employmentHistory => allowanceSheet.DateTo.Date >= employmentHistory.EffectiveDate.Date))
                        throw new InvalidDataException("This Team member has already salary sheet. Please transfer after Date(" + salarySheet.EndDate.Date.ToString("d MMM,yyyy") + ")");
                    if (validShiftWeekendHistoryList.Any() && validShiftWeekendHistoryList.Any(shiftWeekendHistory => allowanceSheet.DateTo.Date >= shiftWeekendHistory.EffectiveDate.Value.Date))
                        throw new InvalidDataException("This Team member has already salary sheet. Please transfer after Date(" + salarySheet.EndDate.Date.ToString("d MMM,yyyy") + ")");
                    if (validMentorHistoryList.Any() && validMentorHistoryList.Any(mentorHistory => allowanceSheet.DateTo.Date >= mentorHistory.EffectiveDate.Value.Date))
                        throw new InvalidDataException("This Team member has already salary sheet. Please transfer after Date(" + salarySheet.EndDate.Date.ToString("d MMM,yyyy") + ")");
                }

                #endregion

                using (trans = Session.BeginTransaction())
                {
                    #region save teamMember

                    #region member offical information

                    if (validMemberOfficialDetailList.Any())
                    {
                        savingTeamMember.MemberOfficialDetails = validMemberOfficialDetailList;
                    }
                    _hrMemberDao.Save(savingTeamMember);

                    #endregion

                    #region saving Offical Details log

                    if (
                        savingTeamMember.MemberOfficialDetails.Where(x => x.ModificationDate >= transferingDateTime)
                            .ToList()
                            .Any())
                    {
                        foreach (
                            var officalDetail in
                                savingTeamMember.MemberOfficialDetails.Where(
                                    x => x.ModificationDate >= transferingDateTime).ToList())
                        {
                            SaveMemberOfficialDetailLog(officalDetail, dateTime);
                        }

                    }

                    #endregion

                    #region save TeamMember Employment History

                    if (validEmploymentHistoryList.Any())
                    {
                        foreach (var employmentHistory in validEmploymentHistoryList)
                        {
                            _hrEmploymentHistoryDao.Save(employmentHistory);

                            #region log

                            var hrEmploymentHistoryLog = new EmploymentHistoryLog
                            {
                                EmploymentStatus = employmentHistory.EmploymentStatus,
                                EffectiveDate = employmentHistory.EffectiveDate,
                                CampusId = employmentHistory.Campus.Id,
                                DesignationId = employmentHistory.Designation.Id,
                                DepartmentId = employmentHistory.Department.Id,
                                TeamMemberId = employmentHistory.TeamMember.Id,
                                EmploymentHistoryId = employmentHistory.Id,
                                CreationDate = dateTime,
                                CreateBy = GetCurrentUserId()
                            };
                            Session.Save(hrEmploymentHistoryLog);

                            #endregion
                        }
                    }

                    #endregion

                    #region save TeamMember Shift Weekend History

                    if (validShiftWeekendHistoryList.Any())
                    {
                        foreach (var shiftWeekendHistory in validShiftWeekendHistoryList)
                        {
                            _hrShiftWeekendHistoryDao.Save(shiftWeekendHistory);

                            #region save hrShiftWeekendHistoryLog

                            var hrShiftWeekendHistoryLog = new ShiftWeekendHistoryLog
                            {
                                Weekend = shiftWeekendHistory.Weekend,
                                EffectiveDate = shiftWeekendHistory.EffectiveDate,
                                OrganizationId = shiftWeekendHistory.Organization.Id,
                                ShiftId = shiftWeekendHistory.Shift.Id,
                                TeamMemberId = shiftWeekendHistory.TeamMember.Id,
                                ShiftWeekendHistoryId = shiftWeekendHistory.Id,
                                CreationDate = dateTime,
                                CreateBy = GetCurrentUserId()
                            };
                            Session.Save(hrShiftWeekendHistoryLog);

                            #endregion
                        }
                    }

                    #endregion

                    #region TeamMember Mentor History

                    if (validMentorHistoryList.Any())
                    {
                        foreach (var mentorHistory in validMentorHistoryList)
                        {
                            _hrMentorHistoryDao.Save(mentorHistory);

                            #region log

                            var hrMentorHistoryLog = new MentorHistoryLog
                            {
                                EffectiveDate = mentorHistory.EffectiveDate,
                                MemberDesignationId = mentorHistory.Designation.Id,
                                MemberDepartmentId = mentorHistory.Department.Id,
                                MemberOrganizationId = mentorHistory.Organization.Id,
                                TeamMemberId = mentorHistory.TeamMember.Id,
                                Pin = mentorHistory.Pin,
                                MentorId = mentorHistory.Mentor.Id,
                                MentorDesignationId = mentorHistory.MentorDesignation.Id,
                                MentorDepartmentId = mentorHistory.MentorDepartment.Id,
                                MentorOrganizationId = mentorHistory.MentorOrganization.Id,
                                MentorHistoryId = mentorHistory.Id,
                                CreationDate = dateTime,
                                CreateBy = GetCurrentUserId()
                            };
                            Session.Save(hrMentorHistoryLog);

                            #endregion
                        }
                    }

                    #endregion

                    trans.Commit();

                    #endregion
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public TeamMember GetCurrentMember()
        {
            long userId = GetCurrentUserId();//Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
            UserProfile user = _UserProfileDao.GetByAspNetUser(userId);
            TeamMember member = LoadByUserProfile(user);
            return member;
        }

        public int GetMemberWeekEnd(long memberId)
        {
            try
            {
                var weekEnd = _hrMemberDao.GetMemberWeekend(memberId);
                return weekEnd;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public TeamMember LoadById(long id)
        {
            try
            {
                return _hrMemberDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public TeamMember GetByPin(int outerNum, DateTime? joiningDate = null)
        {
            try
            {
                return _hrMemberDao.GetMember(outerNum, joiningDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TeamMember LoadByUserProfile(UserProfile user)
        {
            try
            {
                if (user == null)
                    throw new NullObjectException("Invalid User !");
                var result = _hrMemberDao.LoadByUserProfile(user);
                return result;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public TeamMember GetTeamMemberByAspNetUserId(long aspNetUserId)
        {
            try
            {
                UserProfile user = _UserProfileDao.GetByAspNetUser(aspNetUserId);
                TeamMember result = _hrMemberDao.LoadByUserProfile(user);
                return result;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public TeamMember GetMember(int mentorPin)
        {
            try
            {
                return _hrMemberDao.GetMember(mentorPin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public Organization GetTeamMemberSalaryOrganization(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId == null && teamMemberPin == null)
                    throw new InvalidDataException("Both teamMember id and pin can't be empty");
                Organization organization;
                DateTime searchingConvertedDate = DateTime.Today;
                if (!String.IsNullOrEmpty(searchingDate))
                    searchingConvertedDate = Convert.ToDateTime(searchingDate);
                organization = _hrMemberDao.GetTeamMemberSalaryOrganization(searchingConvertedDate, teamMemberId, teamMemberPin);
                return organization;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw;
            }
        }

        public Organization GetTeamMemberOrganization(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId == null && teamMemberPin == null)
                    throw new InvalidDataException("Both teamMember id and pin can't be empty");
                Organization organization;
                DateTime searchingConvertedDate = DateTime.Today;
                if (!String.IsNullOrEmpty(searchingDate))
                    searchingConvertedDate = Convert.ToDateTime(searchingDate);
                organization = _hrMemberDao.GetTeamMemberOrganization(searchingConvertedDate, teamMemberId, teamMemberPin);
                return organization;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw;
            }
        }

        public Branch GetTeamMemberSalaryBranch(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId == null && teamMemberPin == null)
                    throw new InvalidDataException("Both teamMember id and pin can't be empty");
                Branch branch;
                DateTime searchingConvertedDate = DateTime.Today;
                if (!String.IsNullOrEmpty(searchingDate))
                    searchingConvertedDate = Convert.ToDateTime(searchingDate);

                branch = _hrMemberDao.GetTeamMemberSalaryBranch(searchingConvertedDate, teamMemberId, teamMemberPin);
                return branch;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw;
            }
        }

        public Branch GetTeamMemberBranch(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId == null && teamMemberPin == null)
                    throw new InvalidDataException("Both teamMember id and pin can't be empty");
                Branch branch;
                DateTime searchingConvertedDate = DateTime.Today;
                if (!String.IsNullOrEmpty(searchingDate))
                    searchingConvertedDate = Convert.ToDateTime(searchingDate);

                branch = _hrMemberDao.GetTeamMemberBranch(searchingConvertedDate, teamMemberId, teamMemberPin);
                return branch;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw;
            }
        }

        public Campus GetTeamMemberSalaryCampus(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId == null && teamMemberPin == null)
                    throw new InvalidDataException("Both teamMember id and pin can't be empty");
                Campus campus;
                DateTime searchingConvertedDate = DateTime.Today;
                if (!String.IsNullOrEmpty(searchingDate))
                    searchingConvertedDate = Convert.ToDateTime(searchingDate);

                campus = _hrMemberDao.GetTeamMemberSalaryCampus(searchingConvertedDate, teamMemberId, teamMemberPin);
                return campus;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Campus GetTeamMemberCampus(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId == null && teamMemberPin == null)
                    throw new InvalidDataException("Both teamMember id and pin can't be empty");
                Campus campus;
                DateTime searchingConvertedDate = DateTime.Today;
                if (!String.IsNullOrEmpty(searchingDate))
                    searchingConvertedDate = Convert.ToDateTime(searchingDate);

                campus = _hrMemberDao.GetTeamMemberCampus(searchingConvertedDate, teamMemberId, teamMemberPin);
                return campus;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Department GetTeamMemberDepartment(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId == null && teamMemberPin == null)
                    throw new InvalidDataException("Both teamMember id and pin can't be empty");

                Department department;
                DateTime searchingConvertedDate = DateTime.Today;
                if (!String.IsNullOrEmpty(searchingDate))
                    searchingConvertedDate = Convert.ToDateTime(searchingDate);

                department = _hrMemberDao.GetTeamMemberDepartment(searchingConvertedDate, teamMemberId, teamMemberPin);
                return department;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw;
            }
        }

        public Department GetTeamMemberSalaryDepartment(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId == null && teamMemberPin == null)
                    throw new InvalidDataException("Both teamMember id and pin can't be empty");

                Department department;
                DateTime searchingConvertedDate = DateTime.Today;
                if (!String.IsNullOrEmpty(searchingDate))
                    searchingConvertedDate = Convert.ToDateTime(searchingDate);

                department = _hrMemberDao.GetTeamMemberSalaryDepartment(searchingConvertedDate, teamMemberId, teamMemberPin);
                return department;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw;
            }
        }

        public Designation GetTeamMemberDesignation(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId == null && teamMemberPin == null)
                    throw new InvalidDataException("Both teamMember id and pin can't be empty");

                Designation designation;
                DateTime searchingConvertedDate = DateTime.Today;
                if (!String.IsNullOrEmpty(searchingDate))
                    searchingConvertedDate = Convert.ToDateTime(searchingDate);

                designation = _hrMemberDao.GetTeamMemberDesignation(searchingConvertedDate, teamMemberId, teamMemberPin);
                return designation;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw;
            }
        }

        public Designation GetTeamMemberSalaryDesignation(string searchingDate = null, long? teamMemberId = null, int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId == null && teamMemberPin == null)
                    throw new InvalidDataException("Both teamMember id and pin can't be empty");

                Designation designation;
                DateTime searchingConvertedDate = DateTime.Today;
                if (!String.IsNullOrEmpty(searchingDate))
                    searchingConvertedDate = Convert.ToDateTime(searchingDate);

                designation = _hrMemberDao.GetTeamMemberDesignation(searchingConvertedDate, teamMemberId, teamMemberPin);
                return designation;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public TeamMemberTransferViewModel GetTeamMemberTransferInfo(long teamMemberId)
        {
            try
            {
                return _hrMemberDao.GetTeamMemberTransferInfo(teamMemberId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

       
        #endregion

        #region List Loading Function

        public IList<TeamMember> LoadHrAuthorizedTeamMember(List<UserMenu> userMenus, DateTime? searchingDate = null, List<long> organizationIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, List<int> pinList = null, bool withRetired = true)
        {
            try
            {
                if (userMenus == null)
                    throw new InvalidDataException("Invalid UserMenu.");
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), null, CommonHelper.ConvertSelectedAllIdList(branchIdList));
                if (searchingDate != null)
                {
                    searchingDate = Convert.ToDateTime(searchingDate).Date;
                }
                return _hrMemberDao.LoadHrAuthorizedTeamMember(searchingDate, authBranchIdList, campusIdList, departmentIdList, designationIdList, pinList, withRetired);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<long> LoadHrAuthorizedTeamMemberId(List<UserMenu> userMenus, DateTime? searchingDate = null, List<long> organizationIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null, List<int> pinList = null)
        {
            try
            {
                if (userMenus == null)
                    throw new InvalidDataException("userMenu can't empty");

                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), null, CommonHelper.ConvertSelectedAllIdList(branchIdList));
                if (searchingDate != null)
                {
                    searchingDate = Convert.ToDateTime(searchingDate).Date;
                }
                return _hrMemberDao.LoadHrAuthorizedTeamMemberId(searchingDate, authBranchIdList, campusIdList, departmentIdList, designationIdList, pinList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<TeamMember> LoadTeamMemberDirectory(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long? organizationId, long? branchId, long? campusId, long? departmentId, string name, int? pin, int? bloodGroup)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);
                List<long> campusIdList = (campusId != null) ? _commonHelper.ConvertIdToList((long)campusId) : null;

                return _hrMemberDao.LoadTeamMemberDirectory(start, length, orderBy, orderDir, organizationIdList, branchIdList, campusIdList, departmentId, name, pin, bloodGroup);
                //return _hrMemberDao.LoadTeamMemberDirectory(start, length, orderBy, orderDir, organizationId, branchId, campusId, departmentId, name, pin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<MemberSearchListDto> LoadTeamMemberListReport(int draw, int start, string orderBy, string orderDir, int length, List<UserMenu> userMenu, long organizationId, long? departmentId, long[] branchIds, long[] campusIds, int? memberTypes, int? memberStatus)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                List<long> branIdList = null;
                if (!branchIds.Contains(0))
                    branIdList = branchIds.ToList();
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branIdList);

                return _hrMemberDao.LoadTeamMemberListReport(draw, start, orderBy, orderDir, length, organizationIdList, departmentId, branchIdList, campusIds.ToList(), memberTypes, memberStatus);
                //return _hrMemberDao.LoadTeamMemberListReport(draw, start, orderBy, orderDir, length, organizationId, departmentId, branchIds, campusIds, memberTypes, memberStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<MemberSearchListDto> LoadTeamMemberSearchListReport(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long organizationId, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList);
                return _hrMemberDao.LoadTeamMemberSearchListReport(start, length, orderBy, orderDir, organizationIdList, branchIdList, departmentId, memberTypes, memberStatus, keyword, informationViewList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<MemberSearchListDto> LoadSearchTeammemberList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long organizationId, List<long> departmentIdList, List<long> branchIdList, List<long> campusIdList, List<int> memberTypeList, List<int> memberStatuList, string keyword, List<string> informationViewList)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                List<long> autoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> authoBrnachIdList = AuthHelper.LoadBranchIdList(userMenu, autoOrganizationIdList, null, (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll)) ? branchIdList : null);


                List<int> employmentStatusIdList = new List<int>();
                List<int> genderIdList = new List<int>();
                List<int> religionIdList = new List<int>();
                List<int> bloodGroupIdList = new List<int>();
                List<int> maritalStatusIdList = new List<int>();

                //search keyword 
                if (informationViewList != null && informationViewList.Any())
                {
                    foreach (string information in informationViewList)
                    {
                        switch (information)
                        {
                            case TeamMemberReportConstants.EmploymentStatus:
                                employmentStatusIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<MemberEmploymentStatus>(keyword);
                                break;
                            case TeamMemberReportConstants.Gender:
                                genderIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Gender>(keyword);
                                break;
                            case TeamMemberReportConstants.Religion:
                                religionIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Religion>(keyword);
                                break;
                            case TeamMemberReportConstants.BloodGroup:
                                bloodGroupIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<BloodGroup>(keyword);
                                break;
                            case TeamMemberReportConstants.MaritalStatus:
                                maritalStatusIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<MaritalType>(keyword);
                                break;
                        }
                    }
                }

                return _hrMemberDao.LoadSearchTeammemberList(start, length, orderBy, orderDir, authoBrnachIdList, departmentIdList, campusIdList, memberTypeList, memberStatuList, keyword, informationViewList, employmentStatusIdList, genderIdList, religionIdList, bloodGroupIdList, maritalStatusIdList);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<TeamMemberBatchShiftDto> LoadTeamMemberBatchShiftDto(List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<long> campusIdList, List<int> employmentStatusList, List<long> departmntIdList, List<long> designationIdList, List<long> shiftIdList, DateTime? searchingEffectiveDate)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> authbranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll)) ? branchIdList : null);
                return _hrMemberDao.LoadTeamMemberBatchShiftDto(authbranchIdList, campusIdList, employmentStatusList, departmntIdList, designationIdList, shiftIdList, searchingEffectiveDate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<TeamMemberBatchSalaryHistoryDto> LoadTeamMemberBatchSalaryHistoryDto(List<UserMenu> userMenu, long organizationId, List<long> branchIds, List<long> campusIds, List<int> employmentStatusList, List<long> departmentIds, List<long> designationIds, bool isSearchOnEmploymetHistory)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> authbranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchIds != null && !branchIds.Contains(SelectionType.SelelectAll)) ? branchIds : null);
                return _hrMemberDao.LoadTeamMemberBatchSalaryHistoryDto(authbranchIdList, campusIds, employmentStatusList, departmentIds, designationIds, isSearchOnEmploymetHistory);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<FestivalBonusSheetDto> LoadTeamMemberListForFbSheet(List<UserMenu> userMenu, DateTime startDate, DateTime endDate, List<int> religionList, FestivalBonusSheetFormViewModel viewModel)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");

                if (viewModel == null)
                    throw new InvalidDataException("Invalid Object");

                if(religionList==null || religionList.Count()<=0)
                    throw new InvalidDataException("Invalid religion");

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu,
                    _commonHelper.ConvertIdToList(viewModel.OrganizationId));
                List<long> authBranchIdList = new List<long>();
                if (viewModel.BranchId == 0)
                    authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList);
                else
                    authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList, null,
                        _commonHelper.ConvertIdToList(viewModel.BranchId));

                return _hrMemberDao.LoadTeamMemberListForFbSheet(startDate, endDate, religionList, viewModel,
                    authOrganizationIdList, authBranchIdList);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return new List<FestivalBonusSheetDto>();
        }

        public List<TeamMemberInfoForLeaveSummaryDto> LoadTeamMemberForLeaveSummary(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus, DateTime searchingEffectiveDate, long organizationId, long branchId, long campusId, long departmentId, long designationId, string pinListString)
        {
            try
            {
                if (userMenus == null)
                    throw new InvalidDataException("Invalid menu");

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                List<long> authBranchIdList = new List<long>();
                authBranchIdList = branchId == 0 ? AuthHelper.LoadBranchIdList(userMenus, authOrganizationIdList) : AuthHelper.LoadBranchIdList(userMenus, authOrganizationIdList, null, _commonHelper.ConvertIdToList(branchId));
                List<int> pinList = new List<int>();
                pinListString = (!String.IsNullOrEmpty(pinListString)) ? pinListString.Trim() : "";
                Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                pinListString = regex.Replace(pinListString, @",");
                if (!String.IsNullOrEmpty(pinListString))
                {
                    pinList = pinListString.Split(',').Select(Int32.Parse).ToList();
                }

                return _hrMemberDao.LoadTeamMemberForLeaveSummary(start, length, orderBy, orderDir, searchingEffectiveDate, authBranchIdList, (campusId != SelectionType.SelelectAll) ? CommonHelper.ConvertToList(campusId) : null, (departmentId != SelectionType.SelelectAll) ? CommonHelper.ConvertToList(departmentId) : null, (designationId != SelectionType.SelelectAll) ? CommonHelper.ConvertToList(designationId) : null,pinList);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        private void CheckBeforeSavingPersonalInformation(TeamMember member)
        {
            DateTime presentDay = DateTime.Now;

            if (member == null)
                throw new NullObjectException("Invalid TeamMember");

            if (String.IsNullOrEmpty(member.FullNameEng) || String.IsNullOrWhiteSpace(member.FullNameEng))
                throw new InvalidDataException("Invalid Full Name(English)");

            if (String.IsNullOrEmpty(member.FullNameBang) || String.IsNullOrWhiteSpace(member.FullNameBang))
                throw new InvalidDataException("Invalid Full Name(Bangla)");

            var genderValue = new List<int>(Enum.GetValues(typeof(Gender)).Cast<int>());

            if (!genderValue.Contains(Convert.ToInt32(member.Gender)))
            {
                throw new InvalidDataException("Invalid Gender");
            }
            var religionValue = new List<int>(Enum.GetValues(typeof(Religion)).Cast<int>());
            if (!religionValue.Contains(Convert.ToInt32(member.Religion)))
            {
                throw new InvalidDataException("Invalid Religion");
            }
            if (member.BloodGroup != null)
            {
                var bloodGroupValue = new List<int>(Enum.GetValues(typeof(BloodGroup)).Cast<int>());
                if (!bloodGroupValue.Contains(Convert.ToInt32(member.BloodGroup)))
                {
                    throw new InvalidDataException("Invalid Blood Group");
                }
            }
            if (member.Nationality != 1)
            {
                throw new InvalidDataException("Invalid Nationality");
            }

            if (member.CertificateDOB != null)
            {
                if (presentDay.Year - member.CertificateDOB.Value.Year <= 10)
                {
                    throw new InvalidDataException("Invalid date of Birth (Certificate). Team Member must be grater then 10 Years old.");
                }
            }
            if (member.ActualDOB != null)
            {
                if (presentDay.Year - member.ActualDOB.Value.Year <= 10)
                {
                    throw new InvalidDataException("Invalid date of Birth (Actual). Team Member must be grater then 10 Years old.");
                }
            }
        }
        private void CheckBeforeSavingParentalInformation(string fatherContactNumber, string motherContactNumber)
        {
            var reg = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
            if (!String.IsNullOrEmpty(motherContactNumber) && !String.IsNullOrWhiteSpace(motherContactNumber))
            {
                if (!reg.IsMatch(motherContactNumber))
                {
                    throw new InvalidDataException("Invalid Mother Mobile Number!");
                }
            }
            if (!String.IsNullOrEmpty(fatherContactNumber) && !String.IsNullOrWhiteSpace(fatherContactNumber))
            {
                if (!reg.IsMatch(fatherContactNumber))
                {
                    throw new InvalidDataException("Invalid Father Mobile Number!");
                }
            }
        }

        public long CountTeamMemberListReport(List<UserMenu> userMenu, long organizationId, long? departmentId, long[] branchIds, long[] campusIds, int? memberTypes, int? memberStatus)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                List<long> branIdList = null;
                if (!branchIds.Contains(0))
                    branIdList = branchIds.ToList();
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branIdList);

                return _hrMemberDao.CountTeamMemberListReport(organizationIdList, departmentId, branchIdList, campusIds.ToList(), memberTypes, memberStatus);
                //return _hrMemberDao.CountTeamMemberListReport(organizationId, departmentId, branchIds, campusIds, memberTypes, memberStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public long CountTeammemberSearchListReport(List<UserMenu> userMenu, long organizationId, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList);

                return _hrMemberDao.CountTeammemberSearchListReport(organizationIdList, branchIdList, departmentId, memberTypes, memberStatus, keyword, informationViewList);
                //return _hrMemberDao.CountTeammemberSearchListReport(organizationId, departmentId, memberTypes, memberStatus, keyword, informationViewList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetCountSearchTeammemberList(List<UserMenu> userMenu, long organizationId, List<long> departmentIdList, List<long> branchIdList, List<long> campusIdList, List<int> memberTypeList, List<int> memberStatuList, string keyword, List<string> informationViewList)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                List<long> autoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> authoBrnachIdList = AuthHelper.LoadBranchIdList(userMenu, autoOrganizationIdList, null, (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll)) ? branchIdList : null);

                List<int> employmentStatusIdList = new List<int>();
                List<int> genderIdList = new List<int>();
                List<int> religionIdList = new List<int>();
                List<int> bloodGroupIdList = new List<int>();
                List<int> maritalStatusIdList = new List<int>();

                //search keyword 
                if (informationViewList != null && informationViewList.Any())
                {
                    foreach (string information in informationViewList)
                    {
                        switch (information)
                        {
                            case TeamMemberReportConstants.EmploymentStatus:
                                employmentStatusIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<MemberEmploymentStatus>(keyword);
                                break;
                            case TeamMemberReportConstants.Gender:
                                genderIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Gender>(keyword);
                                break;
                            case TeamMemberReportConstants.Religion:
                                religionIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Religion>(keyword);
                                break;
                            case TeamMemberReportConstants.BloodGroup:
                                bloodGroupIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<BloodGroup>(keyword);
                                break;
                            case TeamMemberReportConstants.MaritalStatus:
                                maritalStatusIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<MaritalType>(keyword);
                                break;
                        }
                    }
                }
                return _hrMemberDao.GetCountSearchTeammemberList(authoBrnachIdList, departmentIdList, campusIdList, memberTypeList, memberStatuList, keyword, informationViewList, employmentStatusIdList, genderIdList, religionIdList, bloodGroupIdList, maritalStatusIdList);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool AssignTeamMemberToUser(long teamMemberId, long userId)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    TeamMember teamMember = _hrMemberDao.LoadById(teamMemberId);
                    UserProfile userProfile = null;
                    if (userId != 0)
                    {
                        userProfile = _userProfileDao.GetByAspNetUser(userId);
                        if (userProfile == null)
                            throw new InvalidDataException("Invalid User");

                        var teamMemberByUserProfile = _hrMemberDao.GetTeamMemberByProfileId(userProfile.Id, teamMemberId);
                        if (teamMemberByUserProfile != null)
                            throw new InvalidDataException("This User Profile Already Assigned to " +
                                                           teamMemberByUserProfile.Name);
                    }
                    if (teamMember != null)
                    {

                        teamMember.UserProfile = userProfile;
                        _hrMemberDao.Save(teamMember);
                    }
                    trans.Commit();
                    return true;
                }

            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool CheckHrAuthorizedTeamMember(List<UserMenu> userMenus, int pinList, DateTime? searchingDate = null, List<long> organizationIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null)
        {
            try
            {
                if (userMenus == null)
                    throw new InvalidDataException("Invalid menu");
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), null, CommonHelper.ConvertSelectedAllIdList(branchIdList));
                if (searchingDate != null)
                {
                    searchingDate = Convert.ToDateTime(searchingDate).Date;
                }
                List<TeamMember> teammemberList = _hrMemberDao.LoadHrAuthorizedTeamMember(searchingDate, authBranchIdList, campusIdList, departmentIdList, designationIdList, _commonHelper.ConvertIdToList(pinList)).ToList();
                if (teammemberList.Any())
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetTeamMemberForLeaveSummaryCount(List<UserMenu> userMenus, DateTime searchingEffectiveDate, long organizationId, long branchId, long campusId, long departmentId, long designationId, string pinListString)
        {
            try
            {
                if (userMenus == null)
                    throw new InvalidDataException("Invalid menu");

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                List<long> authBranchIdList = new List<long>();
                authBranchIdList = branchId == 0 ? AuthHelper.LoadBranchIdList(userMenus, authOrganizationIdList) : AuthHelper.LoadBranchIdList(userMenus, authOrganizationIdList, null, _commonHelper.ConvertIdToList(branchId));
                List<int> pinList = new List<int>();
                pinListString = (!String.IsNullOrEmpty(pinListString)) ? pinListString.Trim() : "";
                Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                pinListString = regex.Replace(pinListString, @",");
                if (!String.IsNullOrEmpty(pinListString))
                {
                    pinList = pinListString.Split(',').Select(Int32.Parse).ToList();
                }

                return _hrMemberDao.GetTeamMemberForLeaveSummaryCount(searchingEffectiveDate, authBranchIdList, (campusId != SelectionType.SelelectAll) ? CommonHelper.ConvertToList(campusId) : null, (departmentId != SelectionType.SelelectAll) ? CommonHelper.ConvertToList(departmentId) : null, (designationId != SelectionType.SelelectAll) ? CommonHelper.ConvertToList(designationId) : null, pinList);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetNewPin()
        {
            try
            {
                return _hrMemberDao.GetNewPin();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        public void SaveMemberOfficialDetailLog(MemberOfficialDetail memberOfficialDetail, DateTime date)
        {
            MemberOfficialDetailLog memberOfficialDetailLog = new MemberOfficialDetailLog();
            memberOfficialDetailLog.MemberOfficialDetailId = memberOfficialDetail.Id;
            memberOfficialDetailLog.OfficialContact = memberOfficialDetail.OfficialContact;
            memberOfficialDetailLog.CardNo = memberOfficialDetail.CardNo;
            memberOfficialDetailLog.OfficialEmail = memberOfficialDetail.OfficialEmail;
            memberOfficialDetailLog.CreationDate = date;
            memberOfficialDetailLog.CreateBy = GetCurrentUserId();
            Session.Save(memberOfficialDetailLog);
        }

        private void DoBeforeSave(TeamMember teamMember)
        {
            var orgObj = teamMember.ShiftWeekendHistory.Select(x => x.Organization).FirstOrDefault();

            var checkList = teamMember.EmploymentHistory.Where(x => x.Department.Organization.Id != orgObj.Id
                                                                    || x.Designation.Organization.Id != orgObj.Id ||
                                                                    x.Campus.Branch.Organization.Id != orgObj.Id).ToList();
            if (checkList.Count > 0)
                throw new InvalidDataException("Invalid data");

            var isDuplicatePersonalContact = _hrMemberDao.IsDuplicatePersonalContact(teamMember.PersonalContact.Trim(), null);
            if (isDuplicatePersonalContact)
                throw new DuplicateEntryException("Personal contact can not be duplicate.");
        }

        private void CheckBeforeSavingUpdateBasicInformation(TeamMember member)
        {

            var isDuplicateCardNo = _memberOfficialDetailDao.IsDuplicateTeamMemberCardNo(member, member.MemberOfficialDetails.Select(x => x.CardNo).FirstOrDefault());
            if (isDuplicateCardNo)
                throw new DuplicateEntryException("Please enter the valid card No.");


            var isDuplicatePersonalContact = _hrMemberDao.IsDuplicatePersonalContact(member.PersonalContact.Trim(), member.Id);
            if (isDuplicatePersonalContact)
                throw new DuplicateEntryException("Personal contact can not be duplicate.");

        }

        public int CountTeamMemberDirectory(List<UserMenu> userMenu, long? organizationId, long? branchId, long? campusId, long? departmentId, string name, int? pin, int? bloodGroup)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid usermenu");
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);
                List<long> campusIdList = (campusId != null) ? _commonHelper.ConvertIdToList((long)campusId) : null;

                return _hrMemberDao.CountTeamMemberDirectory(organizationIdList, branchIdList, campusIdList, departmentId, name, pin, bloodGroup);
                //return _hrMemberDao.CountTeamMemberDirectory(organizationId, branchId, campusId, departmentId, name, pin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        public void GetCurrentInfos(long hrMemberId, out object campus, out string department, out string designation, out int empStatus,
            out string joindate, out string todate, out string officeShift, out int weekend, out int mentorPin)
        {
            try
            {
                _hrMemberDao.GetCurrentInfos(hrMemberId, out campus, out department, out designation, out empStatus, out joindate, out todate, out officeShift, out weekend, out mentorPin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<LeaveTypeDtoList> LoadMembersPendingLeave(long memId, bool isHr = false)
        {
            try
            {
                return _hrMemberDao.LoadMembersPendingLeave(memId, isHr);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }

        }

        public void GetCurrentTeamMemberNomineeInfo(long hrMemberId, out string nomineeName, out string nomineeNameRelation,
            out string nomineeContactNumber)
        {
            try
            {
                _hrMemberNomineeIfnoDao.GetCurrentTeamMemberNomineeInfo(hrMemberId, out nomineeName, out nomineeNameRelation, out nomineeContactNumber);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

    }
}
