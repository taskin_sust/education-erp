﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Testing.Values;
using NHibernate;
using Remotion.Linq.Clauses;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DailySupportAllowanceBlockTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "DailySupportAllowanceBlock")]
    public class DailySupportAllowanceBlockLoadTest : DailySupportAllowanceBlockBaseTest
    {
        //TODO: All load test fails on my pc
        #region Basic Test

        [Fact]
        public void AllowanceBlock_Load_For_Mentor()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";
            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();

            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }, null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason)
                                        .WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });
            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu, new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, mentor, true);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>().Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId).List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;

            var list = _dailyAllowanceBlockService.LoadDailyAllowance(date, userMenu, teamMemberFactory.Object.Pin.ToString(CultureInfo.CurrentCulture), true);

            Assert.StrictEqual(1, list.Count);
            Assert.StrictEqual(list[0].Organization, organizationFactory.Object.ShortName);
            Assert.StrictEqual(list[0].Branch, employment.Campus.Branch.Name);
            Assert.StrictEqual(list[0].Campus, employment.Campus.Name);
            Assert.StrictEqual(list[0].Pin, teamMemberFactory.Object.Pin);

            //TODO: verify that it loads only requested org,branch,campus user's allownce
        }

        [Fact]
        public void AllowanceBlock_Load_For_Hr()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";

            var teamMember = PersistTeamMember(MentorId);
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }, null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu, new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, teamMember);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>().Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId).List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;

            var list = _dailyAllowanceBlockService.LoadDailyAllowance(date, userMenu, teamMemberFactory.Object.Pin.ToString(CultureInfo.CurrentCulture));

            Assert.StrictEqual(1, list.Count);
            Assert.StrictEqual(list[0].Organization, organizationFactory.Object.ShortName);
            Assert.StrictEqual(list[0].Branch, employment.Campus.Branch.Name);
            Assert.StrictEqual(list[0].Campus, employment.Campus.Name);
            Assert.StrictEqual(list[0].Pin, teamMemberFactory.Object.Pin);

            //TODO: verify that it loads only requested org, branch, campus user's allownce
        }

        [Fact]
        public void DailyAllowanceBlock_List_Load_For_Mentor()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";
            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();

            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }, null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });
            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu, new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, mentor, true);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>().Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId).List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;

            var list = _dailyAllowanceBlockService.LoadDailyAllowance(0, 10, "Pin", "DESC", userMenu, organizationFactory.Object.Id,
                                                                        employment.Campus.Branch.Id,
                                                                        employment.Department.Id,
                                                                        employment.Campus.Id, -1, "",
                                                                        date.AddDays(-1), date.AddDays(1), true, mentor);
            Assert.StrictEqual(1, list.Count);
            Assert.StrictEqual(list[0].Organization, organizationFactory.Object.ShortName);
            Assert.StrictEqual(list[0].Branch, employment.Campus.Branch.Name);
            Assert.StrictEqual(list[0].Campus, employment.Campus.Name);
            Assert.StrictEqual(list[0].Pin, teamMemberFactory.Object.Pin);
        }

        [Fact]
        public void DailyAllowanceBlock_List_Load_For_Hr()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";

            var teamMember = PersistTeamMember(MentorId);
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }, null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu, new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, teamMember);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>().Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId).List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;

            var list = _dailyAllowanceBlockService.LoadDailyAllowance(0, 10, "Pin", "DESC", userMenu, organizationFactory.Object.Id,
                                                                        employment.Campus.Branch.Id,
                                                                        employment.Department.Id,
                                                                        employment.Campus.Id, -1, "",
                                                                        date.AddDays(-1), date.AddDays(1), false);
            Assert.StrictEqual(1, list.Count);
            Assert.StrictEqual(list[0].Organization, organizationFactory.Object.ShortName);
            Assert.StrictEqual(list[0].Branch, employment.Campus.Branch.Name);
            Assert.StrictEqual(list[0].Campus, employment.Campus.Name);
            Assert.StrictEqual(list[0].Pin, teamMemberFactory.Object.Pin);
        }

        #endregion

        #region Business logic

        #region Allowance Block Load

        [Fact]
        public void InvalidDataException_AllowanceBlockLoad_For_UserMenu_Null()
        {
            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.LoadDailyAllowance(new DateTime(), null, string.Empty));
        }

        [Fact]
        public void InvalidDataException_AllowanceBlockLoad_For_UserMenu_Empty()
        {
            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.LoadDailyAllowance(new DateTime(), new List<UserMenu>(), string.Empty));
        }

        [Fact]
        public void MentorDailyAllowanceBlockLoad_Return_Null_For_Branchs_Null()
        {
            organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(organizationFactory.ObjectList);
            var list = _dailyAllowanceBlockService.LoadDailyAllowance(new DateTime(), userMenu, string.Empty);
            Assert.Null(list);
        }

        [Fact]
        public void InvalidDataException_For_Mentor_DailyAllowanceBlockLoad_Should_Return_Empty_List()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";

            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }, null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu, new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, teamMember);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>().Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId).List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;
            using (ITransaction trans = Session.BeginTransaction())
            {
                for (int i = 0; i < teamMemberFactory.Object.MentorHistory.Count; i++)
                {
                    teamMemberFactory.Object.MentorHistory.RemoveAt(i);
                }
                trans.Commit();
            }
            var list = _dailyAllowanceBlockService.LoadDailyAllowance(date, userMenu, string.Empty, true, mentor);
            Assert.Equal(0, list.Count);
        }

        [Fact]
        public void InvalidDataException_For_Mentor_DailyAllowanceBlockLoad_For_Authorized_Pin()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";

            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }
                , null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu
                , new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, teamMember);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>()
                .Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId)
                .List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;

            using (ITransaction trans = Session.BeginTransaction())
            {
                for (int i = 0; i < teamMemberFactory.Object.MentorHistory.Count; i++)
                {
                    teamMemberFactory.Object.MentorHistory.RemoveAt(i);
                }
                trans.Commit();
            }
            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.LoadDailyAllowance(date, userMenu
                , teamMemberFactory.Object.Pin.ToString(CultureInfo.CurrentCulture), true, mentor));
        }

        [Fact]
        public void HrDailyAllowanceBlockLoad_Should_Return_Empty_List()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";

            var teamMember = PersistTeamMember(MentorId);
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }, null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu, new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, teamMember);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>().Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId)
                                .List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;

            branchFactory.Create().WithOrganization().Persist();
            var userMenu2 = BuildUserMenu(new List<Organization>() { branchFactory.Object.Organization }, null, new List<Branch>() { branchFactory.Object });
            var list = _dailyAllowanceBlockService.LoadDailyAllowance(date, userMenu2, teamMemberFactory.Object.Pin.ToString(CultureInfo.CurrentCulture));
            Assert.Equal(list.Count, 0);
        }

        #endregion

        #region Alowance Block List Load

        [Fact]
        public void InvalidDataException_For_DailyAllowanceBlock_List_Load_For_UserMenu_Null()
        {
            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.LoadDailyAllowance(0, 10, "Pin", "DESC"
                , null, 0, 0, 0, 0, -1, "", DateTime.Now, DateTime.Now, false));
        }

        [Fact]
        public void InvalidDataException_For_DailyAllowanceBlock_List_Load_For_UserMenu_Empty()
        {
            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.LoadDailyAllowance(0, 10, "Pin", "DESC"
                , new List<UserMenu>(), 0, 0, 0, 0, -1, "", DateTime.Now, DateTime.Now, false));
        }

        [Fact]
        public void HrDailyAllowanceBlock_List_Load_Should_Return_Null()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";


            var teamMember = PersistTeamMember(MentorId);
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }, null, new List<Branch>() { employment.Campus.Branch });


            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason)
                .WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });
            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu, new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, teamMember);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>().Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId)
                                .List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;

            branchFactory.Create().WithOrganization().Persist();
            var userMenu2 = BuildUserMenu(new List<Organization>() { branchFactory.Object.Organization }, null, new List<Branch>() { branchFactory.Object });
            var list = _dailyAllowanceBlockService.LoadDailyAllowance(0, 10, "Pin", "DESC", userMenu2,
                                                                        branchFactory.Object.Organization.Id,
                                                                        branchFactory.Object.Id,
                                                                        0, 0, -1, "",
                                                                        date, date, false);
            Assert.Equal(0, list.Count);
        }

        [Fact]
        public void InvalidDataException_DailySupportAllowanceBlock_Save_Failed_For_Invalid_Mentor()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";

            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }, null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason)
                .WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu, new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, teamMember);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>().Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId)
                                .List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;
            using (ITransaction trans = Session.BeginTransaction())
            {
                for (int i = 0; i < teamMemberFactory.Object.MentorHistory.Count; i++)
                {
                    teamMemberFactory.Object.MentorHistory.RemoveAt(i);
                }
                trans.Commit();
            }

            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.LoadDailyAllowance(0, 10, "Pin", "DESC", userMenu, organizationFactory.Object.Id,
                                                                        employment.Campus.Branch.Id,
                                                                        employment.Department.Id,
                                                                        employment.Campus.Id, -1,
                                                                        teamMemberFactory.Object.Pin.ToString(CultureInfo.CurrentCulture),
                                                                        date, date, true, mentor));
        }

        [Fact]
        public void DailySupportAllowanceBlock_List_Load_Should_Return_Empty_For_Unauthorized_Pin()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";

            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }, null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason)
                .WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu, new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, teamMember);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>().Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId)
                                .List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;
            using (ITransaction trans = Session.BeginTransaction())
            {
                for (int i = 0; i < teamMemberFactory.Object.MentorHistory.Count; i++)
                {
                    teamMemberFactory.Object.MentorHistory.RemoveAt(i);
                }
                trans.Commit();
            }

            var list = _dailyAllowanceBlockService.LoadDailyAllowance(0, 10, "Pin", "DESC", userMenu, organizationFactory.Object.Id,
                                                                       employment.Campus.Branch.Id,
                                                                       employment.Department.Id,
                                                                       employment.Campus.Id, -1,
                                                                       string.Empty,
                                                                       date, date, true, mentor);
            Assert.Equal(0, list.Count);
        }

        #endregion

        #endregion

        //TODO: Write more test
        //1. Load allowance list for user who has not permission
    }
}
