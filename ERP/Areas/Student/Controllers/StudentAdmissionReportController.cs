﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate.Exceptions;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class StudentAdmissionReportController : Controller
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IBranchService _branchService;
        private readonly IProgramService _programService;
        private readonly IStudentProgramService _studentProgramService;
        private List<UserMenu> _userMenu;


        public StudentAdmissionReportController()
        {
            //open nHibernate session
            var nHSession = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(nHSession);
            _branchService = new BranchService(nHSession);
            _programService = new ProgramService(nHSession);
            _studentProgramService = new StudentProgramService(nHSession);
        }
        #endregion

        // GET: Student/StudentAdmissionReport
        public ActionResult Index()
        {
            return RedirectToAction("StudentAdmissionSummaryReport");
        }

        public ActionResult StudentAdmissionSummaryReport()
        {

            ViewBag.IsPost = false;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.lastSession = "False";
            try
            {
                //ViewBag.DateFrom = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName"); ;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return View();

        }
        [HttpPost]
        public ActionResult StudentAdmissionSummaryReport(string organizationId, string dateFrom, string dateTo, string lastSession)
        {

            ViewBag.IsPost = true;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateFrom = "";
            ViewBag.DateTo = "";
            ViewBag.lastSession = "";
            try
            {
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu),
                    "Id", "ShortName");
                ;
                ViewBag.DateFrom = dateFrom;
                ViewBag.DateTo = dateTo;
                ViewBag.OrganizationId = organizationId;
                ViewBag.lastSession = lastSession;
                var organization = _organizationService.LoadById(Convert.ToInt64(organizationId));
                ViewBag.OrganizationFullName = (organization != null) ? organization.Name : "";

                if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo) && !Convert.ToBoolean(lastSession))
                {
                    dateFrom = Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd") + " 00:00:00.000";
                    dateTo = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd") + " 23:59:59.000";
                    if(Convert.ToDateTime(dateFrom) >= Convert.ToDateTime(dateTo))
                    {
                        throw new DuplicateEntryException("Date from must be less then Date to.");
                    }
                }

                var dynamicTh = new List<string> {"Branch/Program"};
                List<Program> authoProgramLists = _programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId))).ToList();
                if (authoProgramLists.Any())
                {
                    foreach (var authoProgram in authoProgramLists)
                    {
                        var sessionName = "";
                        if (Convert.ToBoolean(lastSession))
                        {
                            var session = authoProgram.ProgramBranchSessions.Select(x => x.Session).OrderBy(x => x.Rank).Take(1).SingleOrDefault();
                            if (session != null)
                                sessionName = " "+session.Name;
                        }
                        dynamicTh.Add(authoProgram.ShortName + sessionName);
                    }
                }
                dynamicTh.Add("Total");
                ViewBag.informationViewList = dynamicTh.ToArray();
               // return View("LoadStudentAdmissionSummaryReport");
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        public JsonResult LoadStudentAdmissionSummaryReport(string organizationId, string dateFrom, string dateTo, string lastSession)
        {
            var boolLastSession = false;
            if (!String.IsNullOrEmpty(dateFrom))
            {
                dateFrom = Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd") + " 00:00:00.000";
            }
            if (!String.IsNullOrEmpty(dateTo))
            {
                dateTo = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd") + " 23:59:59.000";
            }
            if (!String.IsNullOrEmpty(lastSession))
            {
                boolLastSession = Convert.ToBoolean(lastSession);
            }
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    List<Branch> authorizeBranches = _branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId))).ToList();
                    List<Program> authoProgramLists = _programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId))).ToList();
                    var data = new List<object>();
                    var footerTotal = new List<int>();
                    var listProgramStudentCount = new Dictionary<long, int>();
                    listProgramStudentCount.Add(Convert.ToInt64(-1), 0);
                    if (authorizeBranches.Any())
                    {
                        List<StudentAdmissionSummeryReport> summeryReportList = _studentProgramService.LoadBranchProgramWiseStudentAdmssion(_userMenu, authorizeBranches.Select(x=>x.Id).ToList(), dateFrom, dateTo, boolLastSession);
                        foreach (var authorizeBranch in authorizeBranches)
                        {
                            int totalStudentForBranch = 0;
                            List<StudentAdmissionSummeryReport> branchSummeryReportList = new List<StudentAdmissionSummeryReport>();
                            if (summeryReportList.Any())
                                branchSummeryReportList = summeryReportList.Where(x => x.BranchId == authorizeBranch.Id).ToList();

                            var str = new List<string>();
                            str.Add(authorizeBranch.Name.ToString());
                            foreach (var program in authoProgramLists)
                            {
                                if (!listProgramStudentCount.Keys.Contains(program.Id))
                                {
                                    listProgramStudentCount.Add(program.Id, 0);
                                }
                                StudentAdmissionSummeryReport branchProgramSummeryReportList = new StudentAdmissionSummeryReport();
                                if (branchSummeryReportList.Any())
                                    branchProgramSummeryReportList = branchSummeryReportList.SingleOrDefault(x => x.ProgramId == program.Id);
                                if (branchProgramSummeryReportList != null && branchProgramSummeryReportList.ProgramId == program.Id)
                                {
                                    totalStudentForBranch += branchProgramSummeryReportList.StudentCount;
                                    if (listProgramStudentCount.Keys.Contains(program.Id))
                                    {
                                        listProgramStudentCount[program.Id] += branchProgramSummeryReportList.StudentCount;
                                    }
                                    str.Add(branchProgramSummeryReportList.StudentCount.ToString());
                                }
                                else
                                {
                                    str.Add("-");
                                }
                            }
                            listProgramStudentCount[-1] += totalStudentForBranch;
                            str.Add("<span class='total'>" + totalStudentForBranch.ToString() + "</span>");
                            data.Add(str);
                        }
                    }

                    var strFooter = new List<string>();
                    strFooter.Add("<span class='total'>Total</span>");
                    strFooter.AddRange(authoProgramLists.Select(program => "<span class='total'>" + listProgramStudentCount[program.Id].ToString() + "</span>"));
                    strFooter.Add("<span class='total'>" + listProgramStudentCount[-1].ToString() + "</span>");
                    data.Add(strFooter);

                    int recordsTotal = authorizeBranches.Count;
                    long recordsFiltered = recordsTotal;
                    return Json(new
                    {
                        //draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = 0,
                        length = recordsTotal,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        //draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        //start = start,
                        //length = length,
                        data = data
                    });
                    //return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return Json(HttpNotFound()); }

        }

        #region Export Student Admission Summary Report List To Excel

        [HttpGet]
        public ActionResult LoadStudentAdmissionSummaryReportToExcel(string organizationId, string dateFrom, string dateTo, string lastSession)
        {
            try
            {
                var boolLastSession = false;
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                if (!String.IsNullOrEmpty(dateFrom))
                {
                    dateFrom = Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd") + " 00:00:00.000";
                }
                if (!String.IsNullOrEmpty(dateTo))
                {
                    dateTo = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd") + " 23:59:59.000";
                }
                if (!String.IsNullOrEmpty(lastSession))
                {
                    boolLastSession = Convert.ToBoolean(lastSession);
                }

                List<string> headerList = new List<string>();
                List<string> columnList = new List<string>();
                List<string> footerList = new List<string>();
                var organization = _organizationService.LoadById(Convert.ToInt64(organizationId));

                headerList.Add(organization.Name);
                headerList.Add("Student Admission Summary Report");
                if (boolLastSession)
                    headerList.Add(" Till " + Convert.ToDateTime(dateTo).ToString("d MMM, yyyy"));
                else
                    headerList.Add(Convert.ToDateTime(dateFrom).ToString("d MMM, yyyy") + " to " + Convert.ToDateTime(dateTo).ToString("d MMM, yyyy"));

                List<Branch> authorizeBranches = _branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId))).ToList();
                List<Program> authoProgramLists = _programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId))).ToList();
                var xlsList = new List<List<object>>();
                var listProgramStudentCount = new Dictionary<long, int>();
                listProgramStudentCount.Add(Convert.ToInt64(-1), 0);
                columnList.Add("Branch/Program");

                if (authorizeBranches.Any())
                {
                    columnList.AddRange(authoProgramLists.Select(authoProgram => authoProgram.ShortName));
                    columnList.Add("Total");
                    List<StudentAdmissionSummeryReport> summeryReportList = _studentProgramService.LoadBranchProgramWiseStudentAdmssion(_userMenu, authorizeBranches.Select(x => x.Id).ToList(), dateFrom, dateTo, boolLastSession);

                    foreach (var authorizeBranch in authorizeBranches)
                    {
                        int totalStudentForBranch = 0;
                        List<StudentAdmissionSummeryReport> branchSummeryReportList = new List<StudentAdmissionSummeryReport>();
                        if (summeryReportList.Any())
                            branchSummeryReportList = summeryReportList.Where(x => x.BranchId == authorizeBranch.Id).ToList();

                        var xlsRow = new List<object> {authorizeBranch.Name};
                        foreach (var authoProgram in authoProgramLists)
                        {
                            if (!listProgramStudentCount.Keys.Contains(authoProgram.Id))
                                listProgramStudentCount.Add(authoProgram.Id, 0);

                            StudentAdmissionSummeryReport branchProgramSummeryReportList = new StudentAdmissionSummeryReport();
                            if (branchSummeryReportList.Any())
                                branchProgramSummeryReportList = branchSummeryReportList.SingleOrDefault(x => x.ProgramId == authoProgram.Id);
                            if (branchProgramSummeryReportList != null && branchProgramSummeryReportList.ProgramId == authoProgram.Id)
                            {
                                totalStudentForBranch += branchProgramSummeryReportList.StudentCount;
                                if (listProgramStudentCount.Keys.Contains(authoProgram.Id))
                                {
                                    listProgramStudentCount[authoProgram.Id] += branchProgramSummeryReportList.StudentCount;
                                }
                                xlsRow.Add(branchProgramSummeryReportList.StudentCount.ToString());
                            }
                            else
                            {
                                xlsRow.Add("-");
                            }
                        }
                        listProgramStudentCount[-1] += totalStudentForBranch;
                        xlsRow.Add(totalStudentForBranch.ToString());
                        xlsList.Add(xlsRow);
                    }

                }
                var strFooter = new List<object> { "Total" };
                strFooter.AddRange(authoProgramLists.Select(program => listProgramStudentCount[program.Id].ToString()).Cast<object>());
                strFooter.Add(listProgramStudentCount[-1].ToString());
                xlsList.Add(strFooter);
                ExcelGenerator.GenerateExcel(headerList, columnList, xlsList, footerList, "student_Admission_summery-list__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion
    }
}