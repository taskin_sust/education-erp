using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class ShiftWeekendHistoryLogMap : ClassMap<ShiftWeekendHistoryLog> {
        
        public ShiftWeekendHistoryLogMap() {
			Table("HR_ShiftWeekendHistoryLog");
			LazyLoad();
			//References(x => x.Organization).Cascade.All().Column("OrganizationId");old
			//References(x => x.Shift).Column("ShiftId");old
			//References(x => x.TeamMember).Column("TeamMemberId");old
			//References(x => x.ShiftWeekendHistory).Column("ShiftWeekendHistoryId");old
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.OrganizationId);//new
            Map(x => x.ShiftId);//new
            Map(x => x.TeamMemberId);//new
            Map(x => x.ShiftWeekendHistoryId);//new
            Map(x => x.CreateBy);
            Map(x => x.Weekend).Column("Weekend");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
        }
    }
}
