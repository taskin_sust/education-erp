﻿using System;
using System.IO;
using log4net.Config;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UdvashERP.Startup))]
namespace UdvashERP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //XmlConfigurator.Configure(new FileInfo("Log4net.config"));
        }
    }
}
