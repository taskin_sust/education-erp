using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class GoodsTransferMap : BaseClassMap<GoodsTransfer, long>
    {
        public GoodsTransferMap()
        {
            Table("UINV_GoodsTransfer");
            LazyLoad();
            References(x => x.BranchFrom).Column("BranchFromId");
            References(x => x.BranchTo).Column("BranchToId");
            Map(x => x.Remarks).Column("Remarks");
            Map(x => x.TransferNo).Column("TransferNo").Not.Nullable();
            HasMany(x => x.GoodsTransferDetails).KeyColumn("GoodsTransferId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
