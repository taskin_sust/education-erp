﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Cache;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.NomineeInfoTest
{
    public class NomineeInfoBaseTest:TestBase, IDisposable
    {
        #region Propertise & Object Initialization

        public const int MentorId = 2;
        protected readonly CommonHelper CommonHelper;
        protected readonly INomineeInfoService NomineeInfoService;
        protected readonly ITeamMemberService TeamMemberService;
        protected List<long> IdList = new List<long>();
        internal NomineeInfoFactory NomineeInfoFactory;
        internal TeamMemberFactory TeamMemberFactory;
        internal readonly OrganizationFactory OrganizationFactory;
        internal readonly DepartmentFactory DepartmentFactory;
        internal readonly EmploymentHistoryFactory EmploymentHistoryFactory;
        internal readonly MaritalInfoFactory MaritalInfoFactory;
        internal readonly ShiftWeekendHistoryFactory ShiftWeekendHistoryFactory;
        internal readonly MentorHistoryFactory MentorHistoryFactory;


        private ISession _session;

        #endregion

        public NomineeInfoBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            NomineeInfoService = new NomineeInfoService(_session);
            TeamMemberService = new TeamMemberService(_session);
            NomineeInfoFactory = new NomineeInfoFactory(null, _session);
            TeamMemberFactory = new TeamMemberFactory(null, _session);
            OrganizationFactory = new OrganizationFactory(null, _session);
            DepartmentFactory = new DepartmentFactory(null, _session);
            EmploymentHistoryFactory = new EmploymentHistoryFactory(null, _session);
            MaritalInfoFactory = new MaritalInfoFactory(null, _session);
            ShiftWeekendHistoryFactory = new ShiftWeekendHistoryFactory(null, _session);
            MentorHistoryFactory = new MentorHistoryFactory(null, _session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        #region Custom Error Text

        public const string MemberEntityNotFound = "MemberEntityNotFound";
        public const string NullObjectError = "NullObject";

        #endregion


        public void Dispose()
        {
            NomineeInfoFactory.CleanUp();

            if (TeamMemberFactory.Object != null)
            {
                ShiftWeekendHistoryFactory.LogCleanUp(TeamMemberFactory.Object);
                MentorHistoryFactory.LogCleanUp(TeamMemberFactory.Object);
                EmploymentHistoryFactory.LogCleanUp(employmentHistoryFactory);
            }

            TeamMemberFactory.CleanUpWithLeaveSummary();
            TeamMemberFactory.TeamMemberChieldCleanup();
            TeamMemberFactory.Cleanup();
            ShiftWeekendHistoryFactory.ShiftCleanUp();
            EmploymentHistoryFactory.CleanUpParentFactory();
            base.Dispose();
        }

    }
}
