﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UdvashERP.Services.Test.Hr.OverTimeAllowanceSettingTest
{
    public class OverTimeAllowanceSettingDeleteTest : OverTimeAllowanceSettingBaseTest
    {
        [Fact]
        public void Should_Be_Delete_Successfully()
        {
            _organizationFactory.Create().Persist();
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            //_overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object);
            _overtimeAllowanceSettingService.Delete(_overtimeAllowanceSettingFactory.Object);
           var entity= _overtimeAllowanceSettingService.GetOvertimeAllowanceSetting(_overtimeAllowanceSettingFactory.Object.Id);
           Assert.Null(entity);
        }
    }
}
