using System;
using System.Collections.Generic;
using FluentNHibernate.Conventions;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
using UdvashERP.Dao.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Services.Hr
{
    public interface ISpecialAllowanceSettingService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(List<UserMenu> userMenus, SpecialAllowanceSetting specialAllowanceSetting, bool isUpdate = false);
        bool Save(List<UserMenu> userMenus, SpecialAllowanceSetting specialAllowanceSetting);
        bool Update(List<UserMenu> userMenus, SpecialAllowanceSetting specialAllowanceSetting);
        bool Delete(List<UserMenu> userMenus, long id);

        #endregion

        #region Single Instances Loading Function

        SpecialAllowanceSetting GetSpecialAllowanceSettings(long id);

        #endregion

        #region List Loading Function

        IList<SpecialAllowanceSetting> LoadSpecialAllowanceSettings(List<UserMenu> userMenus, int start, int length, string orderBy, string orderDir, int? pin, string name, long? paymentType);
        IList<SpecialAllowanceSetting> LoadIndividualTeamMemberSpecialAllowanceSetting(int pin, DateTime effectiveDate);

        #endregion

        #region Others Function

        int GetSpecialAllowanceSettingCount(List<UserMenu> userMenus, string orderBy, string orderDir, int? pin, string name, long? paymentType);

        #endregion

        #region Helper Function

        string GetUserNameByAspNetUserId(long id);

        #endregion

    }
    public class SpecialAllowanceSettingService : BaseService, ISpecialAllowanceSettingService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("PayrollService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly ISpecialAllowanceSettingDao _specialAllowanceSettingDao;
        private ITeamMemberDao _teamMemberDao;
        private IAspNetUserDao _aspNetUserDao;
        IAllowanceSheetDao _allowanceSheetDao;
        public SpecialAllowanceSettingService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _specialAllowanceSettingDao = new SpecialAllowanceSettingDao { Session = session };
            _teamMemberDao = new TeamMemberDao() { Session = session };
            _aspNetUserDao = new AspNetUserDao() { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool Save(List<UserMenu> userMenus, SpecialAllowanceSetting specialAllowanceSetting)
        {
            bool returnValue = false;
            ITransaction trans = null;
            try
            {
                DoBeforeSave(userMenus, specialAllowanceSetting);
                using (trans = Session.BeginTransaction())
                {
                    _specialAllowanceSettingDao.Save(specialAllowanceSetting);
                    trans.Commit();
                }
            }
            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (MessageException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                _logger.Error(ex);
                throw;
            }
            return returnValue;
        }

        public bool Update(List<UserMenu> userMenus, SpecialAllowanceSetting specialAllowanceSetting)
        {
            bool returnValue = false;
            ITransaction trans = null;
            try
            {
                DoBeforeUpdate(userMenus, specialAllowanceSetting);
                using (trans = Session.BeginTransaction())
                {
                    var tempSpecialAllowanceSettings = _specialAllowanceSettingDao.LoadById(specialAllowanceSetting.Id);
                    tempSpecialAllowanceSettings.ModifyBy = GetCurrentUserId();
                    tempSpecialAllowanceSettings.PaymentType = specialAllowanceSetting.PaymentType;
                    tempSpecialAllowanceSettings.Amount = specialAllowanceSetting.Amount;
                    tempSpecialAllowanceSettings.EffectiveDate = specialAllowanceSetting.EffectiveDate;
                    tempSpecialAllowanceSettings.ClosingDate = specialAllowanceSetting.ClosingDate;
                    tempSpecialAllowanceSettings.Remarks = specialAllowanceSetting.Remarks;
                    _specialAllowanceSettingDao.Update(tempSpecialAllowanceSettings);
                    trans.Commit();
                }
            }
            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (MessageException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                _logger.Error(ex);
                throw;
            }
            return returnValue;
        }

        public bool Delete(List<UserMenu> userMenus, long id)
        {
            bool returnValue = false;
            ITransaction trans = null;
            try
            {
                if (userMenus == null || userMenus.Count <= 0)
                {
                    throw new InvalidDataException("user menu not found!");
                }
                List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(userMenus);
                List<long> authBranchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganizationIds);

                SpecialAllowanceSetting specialAllowanceSetting = new SpecialAllowanceSetting();
                specialAllowanceSetting = _specialAllowanceSettingDao.LoadById(id);

                DoBeforeDelete(specialAllowanceSetting);


                //if (specialAllowanceSetting.ResponsibleMemberPin == 0)
                //{
                //    throw new NullObjectException("Pin number not found for this operation");
                //}

                //if (specialAllowanceSetting.Amount <= 0)
                //{
                //    throw new InvalidDataException("Invalid data for the amount");
                //}

                var authorizeTeamMember = _teamMemberDao.LoadHrAuthorizedTeamMember(null, authBranchIds, null, null, null, _commonHelper.ConvertIdToList(specialAllowanceSetting.TeamMember.Pin), false);
                if (authorizeTeamMember == null)
                {
                    throw new InvalidDataException("Valid team member not found");
                }
                specialAllowanceSetting.TeamMember = _teamMemberDao.LoadById(authorizeTeamMember[0].Id);

                // CheckDateDiff(specialAllowanceSetting.EffectiveDate, specialAllowanceSetting.ClosingDate);
                // ValidationCheck(specialAllowanceSetting);
                using (trans = Session.BeginTransaction())
                {

                    specialAllowanceSetting.ModifyBy = GetCurrentUserId();
                    specialAllowanceSetting.Status = SpecialAllowanceSetting.EntityStatus.Delete;
                    specialAllowanceSetting.ClosingDate = specialAllowanceSetting.ClosingDate;
                    _specialAllowanceSettingDao.Update(specialAllowanceSetting);
                    trans.Commit();
                    return true;
                }

            }
            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (MessageException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                _logger.Error(ex);
                throw;
            }
            return returnValue;
        }

        public bool SaveOrUpdate(List<UserMenu> userMenus, SpecialAllowanceSetting entity, bool isUpdate = false)
        {
            const bool returnValue = false;
            ITransaction trans = null;
            try
            {
                DoBeforeSave(userMenus, entity);
                //DoBeforeSaveOrUpdate(userMenus, entity, isUpdate);
                using (trans = Session.BeginTransaction())
                {
                    _specialAllowanceSettingDao.Save(entity);
                    trans.Commit();
                }
            }
            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (MessageException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                _logger.Error(ex);
                throw;
            }
            return returnValue;
        }


        #endregion

        #region Single Instances Loading Function

        public SpecialAllowanceSetting GetSpecialAllowanceSettings(long id)
        {
            try
            {
                return _specialAllowanceSettingDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<SpecialAllowanceSetting> LoadSpecialAllowanceSettings(List<UserMenu> userMenus, int start, int length, string orderBy, string orderDir, int? pin, string name, long? paymentType)
        {
            try
            {
                if (userMenus == null || userMenus.Count <= 0)
                {
                    throw new InvalidDataException("User menu not found!");
                }
                List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(userMenus);
                List<long> authBranchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganizationIds);

                if (authBranchIds == null || !authBranchIds.Any())
                {
                    throw new InvalidDataException("Permission denied.");
                }
                //var authorizeTeamMember = _teamMemberDao.LoadHrAuthorizedTeamMember(null, authBranchIds, null, null,null, null, false);
                var teamMemberIds = _teamMemberDao.LoadHrAuthorizedTeamMemberId(null, authBranchIds, null, null, null, null, true);
                if (!teamMemberIds.Any())
                    throw new InvalidDataException("Valid team member not found");

                return _specialAllowanceSettingDao.LoadSpecialAllowanceSettings(start, length, orderBy, orderDir, teamMemberIds, pin, name, paymentType);
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<SpecialAllowanceSetting> LoadIndividualTeamMemberSpecialAllowanceSetting(int pin, DateTime effectiveDate)
        {
            try
            {
                var authorizeTeamMember = _teamMemberDao.LoadHrAuthorizedTeamMember(null, null, null, null, null, _commonHelper.ConvertIdToList(pin), false);
                if (authorizeTeamMember.Count <= 0)
                {
                    throw new InvalidDataException("Team Member pin is invalid!");
                }
                return _specialAllowanceSettingDao.LoadIndividualTeamMemberSpecialAllowanceSetting(authorizeTeamMember[0].Id, effectiveDate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int GetSpecialAllowanceSettingCount(List<UserMenu> userMenus, string orderBy, string orderDir, int? pin, string name, long? paymentType)
        {
            int returnValue = 0;
            try
            {
                if (userMenus == null || !userMenus.Any())
                {
                    throw new InvalidDataException("User menu not found!");
                }
                List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(userMenus);
                List<long> authBranchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganizationIds);
                if (authBranchIds == null || !authBranchIds.Any())
                {
                    throw new InvalidDataException("Permission denied.");
                }
                //var authorizeTeamMember = _teamMemberDao.LoadHrAuthorizedTeamMember(null, authBranchIds, null, null,null, null, false);
                var teamMemberIds = _teamMemberDao.LoadHrAuthorizedTeamMemberId(null, authBranchIds, null, null, null, null, true);
                if (!teamMemberIds.Any())
                    throw new InvalidDataException("Valid team member not found");
                returnValue = _specialAllowanceSettingDao.GetSpecialAllowanceSettingCount(orderBy, orderDir, teamMemberIds, pin, name, paymentType);

            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return returnValue;
        }

        private AllowanceSheet GetAllowanceSheet(List<UserMenu> userMenus, SpecialAllowanceSetting entity)
        {
            AllowanceSheet allowanceSheet = new AllowanceSheet();
            #region Team Member & Team Member Organization

            List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(userMenus);
            List<long> authBranchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganizationIds);

            var authorizeTeamMember = _teamMemberDao.LoadHrAuthorizedTeamMember(null, authBranchIds, null, null, null, _commonHelper.ConvertIdToList(entity.ResponsibleMemberPin), false);
            if (authorizeTeamMember == null || authorizeTeamMember.Count == 0)
            {
                throw new InvalidDataException("Valid team member not found");
            }
            entity.TeamMember = _teamMemberDao.LoadById(authorizeTeamMember[0].Id);
            Organization organization = _teamMemberDao.GetTeamMemberOrganization(entity.EffectiveDate, entity.TeamMember.Id, entity.TeamMember.Pin);

            #endregion

             allowanceSheet = _allowanceSheetDao.GetEligibleAllowanceSheetByOrganizationId(organization.Id);

            return allowanceSheet;

        }

        #endregion

        #region Helper function

        private void DoBeforeUpdate(List<UserMenu> userMenus, SpecialAllowanceSetting entity)
        {
            #region Basic Check

            if (userMenus == null || userMenus.Count <= 0)
            {
                throw new InvalidDataException("user menu not found!");
            }

            ObjectValidationChecker(entity);

            if (entity.ClosingDate != null)
            {
                if (DateTime.Compare(entity.EffectiveDate, entity.ClosingDate.Value.Date) >= 0)
                {
                    throw new InvalidDataException("Effective date can't be less than or equal closing date!");
                }
            }

            if (entity.ResponsibleMemberPin == 0)
            {
                throw new NullObjectException("Pin number not found for this operation");
            }

            if (entity.Amount <= 0)
            {
                throw new InvalidDataException("Invalid data for the amount");
            }

            #endregion

            #region Is Post

            AllowanceSheet IsPostAllowanceSheet = GetAllowanceSheet(userMenus, entity);

            if (IsPostAllowanceSheet != null)
            {
                if (entity.EffectiveDate <= IsPostAllowanceSheet.DateTo)
                {
                    var oldSpecialSettings = _specialAllowanceSettingDao.LoadById(entity.Id);
                    if (entity.Amount != oldSpecialSettings.Amount)
                    {
                        throw new MessageException("You can't change the special allowance amount for PIN: " + entity.TeamMember.Pin + " , Name: " + entity.TeamMember.Name);
                    }

                    if (entity.PaymentType != oldSpecialSettings.PaymentType)
                    {
                        throw new MessageException("You can't change the payment type for PIN: " + entity.TeamMember.Pin + " , Name: " + entity.TeamMember.Name);
                    }

                    if (entity.EffectiveDate != oldSpecialSettings.EffectiveDate)
                    {
                        throw new MessageException("You can't change the Effective date for PIN: " + entity.TeamMember.Pin + " , Name: " + entity.TeamMember.Name);
                    }

                    if (IsPostAllowanceSheet.DateTo <= entity.EffectiveDate)
                    {
                        throw new MessageException("You can't change the effective date in this date range");
                    }
                }

                if (entity.ClosingDate != null)
                {
                    //if (IsPostAllowanceSheet.DateTo < entity.ClosingDate)
                    //{
                    //    throw new MessageException("You can't change the closing date in this date range");
                    //}
                    if (IsPostAllowanceSheet.DateTo >= entity.ClosingDate)
                    {
                        throw new MessageException("You can't change the closing date in this date range");
                    }
                }

            }

            #endregion
        }

        private void DoBeforeSave(List<UserMenu> userMenus, SpecialAllowanceSetting entity)
        {
            #region Basic Check

            if (userMenus == null || userMenus.Count <= 0)
            {
                throw new InvalidDataException("user menu not found!");
            }

            ObjectValidationChecker(entity);

            if (entity.ClosingDate != null)
            {
                if (DateTime.Compare(entity.EffectiveDate, entity.ClosingDate.Value.Date) >= 0)
                {
                    throw new InvalidDataException("Effective date can't be less than or equal closing date!");
                }
            }

            if (entity.ResponsibleMemberPin == 0)
            {
                throw new NullObjectException("Pin number not found for this operation");
            }

            if (entity.Amount <= 0)
            {
                throw new InvalidDataException("Invalid data for the amount");
            }

            #endregion

            #region Is Post

            AllowanceSheet IsPostAllowanceSheet = GetAllowanceSheet(userMenus, entity);

            if (IsPostAllowanceSheet != null)
            {
                if (entity.EffectiveDate <= IsPostAllowanceSheet.DateTo)
                {
                    throw new MessageException("You can't create this special allowance");
                }
            }

            #endregion
        }

        private void DoBeforeSaveOrUpdate(List<UserMenu> userMenus, SpecialAllowanceSetting entity, bool isUpdate)
        {
            #region Basic Check

            if (userMenus == null || userMenus.Count <= 0)
            {
                throw new InvalidDataException("user menu not found!");
            }

            ObjectValidationChecker(entity);

            if (entity.ClosingDate != null)
            {
                if (DateTime.Compare(entity.EffectiveDate, entity.ClosingDate.Value.Date) >= 0)
                {
                    throw new InvalidDataException("Effective date can't be less than or equal closing date!");
                }
            }

            if (entity.ResponsibleMemberPin == 0)
            {
                throw new NullObjectException("Pin number not found for this operation");
            }

            if (entity.Amount <= 0)
            {
                throw new InvalidDataException("Invalid data for the amount");
            }

            #endregion

            #region Team Member & Team Member Organization

            List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(userMenus);
            List<long> authBranchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganizationIds);

            var authorizeTeamMember = _teamMemberDao.LoadHrAuthorizedTeamMember(null, authBranchIds, null, null, null, _commonHelper.ConvertIdToList(entity.ResponsibleMemberPin), false);
            if (authorizeTeamMember == null || authorizeTeamMember.Count == 0)
            {
                throw new InvalidDataException("Valid team member not found");
            }
            entity.TeamMember = _teamMemberDao.LoadById(authorizeTeamMember[0].Id);
            Organization organization = _teamMemberDao.GetTeamMemberOrganization(entity.EffectiveDate, entity.TeamMember.Id, entity.TeamMember.Pin);

            #endregion

            #region Is Post

            AllowanceSheet allowanceSheet = _allowanceSheetDao.GetEligibleAllowanceSheetByOrganizationId(organization.Id);

            if (allowanceSheet != null)
            {
                if (isUpdate)
                {

                }
                else
                {
                    if (entity.EffectiveDate <= allowanceSheet.DateTo)
                    {
                        throw new MessageException("You can't create this special allowance");
                    }
                }
                //if (isUpdate == false)
                //{
                //    if (entity.EffectiveDate <= allowanceSheet.DateTo)
                //    {
                //        throw new MessageException("You can't create this special allowance");
                //    }
                //}
                //else
                //{
                //    if (entity.ClosingDate != null)
                //    {
                //        if (allowanceSheet.DateTo >= entity.EffectiveDate && allowanceSheet.DateTo < entity.ClosingDate)
                //        {
                //            throw new MessageException("You can't update this special allowance");
                //        }
                //    }
                //    else
                //    {
                //        if (allowanceSheet.DateTo >= entity.EffectiveDate)
                //        {
                //            throw new MessageException("You can't update this special allowance");
                //        }
                //    }

                //}
            }

            #endregion
        }

        //private void DoBeforeUpdate(SpecialAllowanceSetting entity)
        //{
        //    var specialAllowanceSetting = _specialAllowanceSettingDao.LoadById(entity.Id);
        //    if (_specialAllowanceSettingDao.HasSpecialSettings(specialAllowanceSetting))
        //        throw new DependencyException("You can't delete this special allowance settings");
        //}

        private void DoBeforeDelete(SpecialAllowanceSetting specialAllowanceSetting)
        {
            if (_specialAllowanceSettingDao.HasSpecialSettings(specialAllowanceSetting))
                throw new DependencyException("You can't delete this special allowance settings");
        }

        private static void ObjectValidationChecker(SpecialAllowanceSetting entity)
        {
            var validationResult = ValidationHelper.ValidateEntity<SpecialAllowanceSetting, SpecialAllowanceSettingMetaData>(entity);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private static void CheckDateDiff(DateTime effectiveDate, DateTime? closingDate)
        {
            if (closingDate != null)
            {
                if (DateTime.Compare(effectiveDate.Date, closingDate.Value.Date) > 0)
                {
                    throw new InvalidDataException("Effective date can't be less then closing date!");
                }
            }
        }

        public string GetUserNameByAspNetUserId(long id)
        {
            try
            {
                var aspNetUser = _aspNetUserDao.LoadById(id);
                return aspNetUser != null ? aspNetUser.UserName : "";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion
    }
}

