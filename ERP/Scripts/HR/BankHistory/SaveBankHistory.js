﻿
$(document).ready(function () {
    window.validateTextField("AccountTitle", "Please Enter A/C Title.");
    window.validateTextField("AccountNumber", "Please Enter A/C Number.");
    window.validateDropDownField("BankId2", "Please Select Bank.");
    window.validateDropDownField("BankBranchId2", "Please Select Bank Branch.");
    $(document).on("change", "#EffectiveDate", function (event) {
        var val = $("#EffectiveDate").val();
        if (val.length > 0) {
            window.showErrorMessageBelowCtrl("EffectiveDate", "", false);
        }
    });

    $('.form_datetime1').datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: false,
        showMeridian: true,
        initialDate: new Date(),
        startView: 2,
        minView: 2,
        maxView: 4
    });

    $(document).on('change', "#BankId2", function () {
        $('#BankBranchId2').empty();
        $('#BankBranchId2').append("<option value=''>Select a Bank Branch</option>");
        var bankId = $("#BankId2").val();
        if (bankId != '') {
            $.ajax({
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBankBranch",
                type: "POST",
                data: { bankIdList: bankId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.returnBankBranchList, function (i, v) {
                            $('#BankBranchId2').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function () {
                    $.unblockUI();
                }
            });
        }
    });

    $(document).on("click", "#Back", function (event) {
        var teamMemberPin = $("#TeamMemberPin").val();
        var url = $("body").attr("data-project-root") + "HR/TeamMember/SearchOfficialDetails?memberPin=" + teamMemberPin;
        window.location.href = url;
    });

    $(document).on("click", "#SaveBankHistory", function (event) {
        var canAddBankHistory = true;
        var teamMemberId = $("#TeamMemberId").val();
        var teamMemberPin = $("#TeamMemberPin").val();
        var id = $("#Id").val();
        var bankId = $("#BankId2").val();
        var bankBranchId = $("#BankBranchId2").val();
        var accountTitle = $("#AccountTitle").val();
        var accountNumber = $("#AccountNumber").val();
        var effectiveDate = $("#EffectiveDate").val();

        if (accountTitle == '' || accountTitle.length <= 0) {
            window.showErrorMessageBelowCtrl("AccountTitle", "", false);
            window.showErrorMessageBelowCtrl("AccountTitle", "Please Enter A/C Title.", true);
            canAddBankHistory = false;
        }
        if (accountNumber == '' || accountNumber.length <= 0) {
            window.showErrorMessageBelowCtrl("AccountNumber", "", false);
            window.showErrorMessageBelowCtrl("AccountNumber", "Please Enter A/C No..", true);
            canAddBankHistory = false;
        }
        if (bankId == '' || bankId.length <= 0) {
            window.showErrorMessageBelowCtrl("BankId2", "", false);
            window.showErrorMessageBelowCtrl("BankId2", "Please Select Bank.", true);
            canAddBankHistory = false;
        }
        if (bankBranchId == '' || bankBranchId.length <= 0) {
            window.showErrorMessageBelowCtrl("BankBranchId2", "", false);
            window.showErrorMessageBelowCtrl("BankBranchId2", "Please Select Bank Branch.", true);
            canAddBankHistory = false;
        }
        if (effectiveDate == '' || effectiveDate.length <= 0) {
            window.showErrorMessageBelowCtrl("EffectiveDate", "", false);
            window.showErrorMessageBelowCtrl("EffectiveDate", "Please Select Effective Date.", true);
            canAddBankHistory = false;
        }
        if (canAddBankHistory == true) {

            var teamMemberBankHistoryViewModelObj = new TeamMemberBankHistoryViewModel();
            teamMemberBankHistoryViewModelObj.Id = id;
            teamMemberBankHistoryViewModelObj.TeamMemberId = teamMemberId;
            teamMemberBankHistoryViewModelObj.TeamMemberPin = teamMemberPin;
            teamMemberBankHistoryViewModelObj.BankBranchId = bankBranchId;
            teamMemberBankHistoryViewModelObj.AccountNumber = accountNumber;
            teamMemberBankHistoryViewModelObj.AccountTitle = accountTitle;
            teamMemberBankHistoryViewModelObj.EffectiveDate = effectiveDate;
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "HR/TeamMember/SaveBankHistory",
                cache: false,
                async: true,
                data: { bankHistoryViewModel: teamMemberBankHistoryViewModelObj },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        var url = $("body").attr("data-project-root") + "HR/TeamMember/SearchOfficialDetails?memberPin=" + teamMemberPin;
                        window.location.href = url;
                    } else {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "error",
                        });
                    }

                },
                complete: function () {
                    $.unblockUI();

                },
                error: function (result) {
                    $.unblockUI();
                }
            });
        }
    });
});
function TeamMemberBankHistoryViewModel() {
    this.TeamMemberId = 0;
    this.TeamMemberPin = 0;
    this.Id = 0;
    this.BankId = 0;
    this.BankBranchId = 0;
    this.AccountNumber = "";
    this.AccountTitle = "";
    this.EffectiveDate = "";
    this.TeamMemberBankHistoryViewModel = function () { };
}