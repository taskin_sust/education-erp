﻿$(document).ready(function () {
    var organizationId = "";
    var program = "";
    var session = "";
    var course = "";
    var examCode = "";
    var examId = "";
    var setCode = "";
    validateDropDownField("OrganizationId", "Please Select Organization.");
    validateDropDownField("Program", "Please Select Program");
    validateDropDownField("Session", "Please Select Session");
    validateDropDownField("Course", "Please Select Course");
    validateDropDownField("Exam", "ExamHidden", "Please Select Exam");
    validateDropDownField("SetCode", "Please Select SetCode");
    //validateDropDownField("ExamHidden", "Please Select Exam Code.");
    //var jqXHRData;

    $(document).on("change", "#OrganizationId", function () {
        var organizationId = $("#OrganizationId").val();

        $('#Program').empty();
        $('#Program').append("<option value=''>Select a Program</option>");
        //$('#SessionId').empty();
        //$('#SessionId').append("<option value=''>Select a session</option>");

        if (organizationId != null && organizationId != "") {
            $.ajax({
                //url: "/Exam/UploadAnswerSheet/GetProgram",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                type: "POST",
                data: { organizationIds: organizationId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    //console.log(data.ProgramOptions);
                    //$("#Program").html(data.ProgramOptions);
                    $.each(data.returnProgramList, function (i, v) {
                        $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));

                    });
                    $("#Session").html("<option selected='selected'>Select Session</option>");
                    $("#Course").html("<option selected='selected'>Select Course</option>");
                    $("#Exam").html("<option selected='selected'>Select Exam</option>");
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: AJAX_ERROR,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }

    });

    /*Get Session*/
    $(document).on("change", "#Program", function () {

        //$('#Program').empty();
        //$('#Program').append("<option value=''>Select a Program</option>");
        $('#Session').empty();
        $('#Session').append("<option value=''>Select a session</option>");

        var programId = $("#Program").val();
        if (programId != null && programId != "") {
            $.ajax({
                //url: "/Exam/UploadAnswerSheet/GetSession",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                type: "POST",
                data: { programIds: programId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    //$('#discountDetail').html(data);
                    $.each(data.returnSessionList, function (i, v) {
                        $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));

                    });
                    //$("#Session").html(data.SessionOptions);
                    $("#Course").html("<option selected='selected'>Select Course</option>");
                    $("#Exam").html("<option selected='selected'>Select Exam</option>");
                    //$("#SetCode").html("<option selected='selected'>Select Campus</option>");
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: AJAX_ERROR,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });

        }


    });

    /*Get Course*/
    $(document).on("change", "#Session", function () {
        var programId = $("#Program").val();
        var sessionId = $("#Session").val();
        $("#Course option:not(:first)").remove();
        $.ajax({
            //url: "/Exam/UploadAnswerSheet/GetCourse",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
            type: "POST",
            data: { programIds: programId, sessionIds: sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                if (response.IsSuccess) {
                    $.each(response.returnCourse, function (i, v) {
                        $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                //$("#Course").html(data.CourseOptions);
                $("#Exam").html("<option selected='selected'>Select Exam</option>");
                //$("#SetCode").html("<option selected='selected'>Select Campus</option>");
                $.unblockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.fn.customMessage({
                    displayMessage: AJAX_ERROR,
                    displayMessageType: "error",
                    displayTime: 5000
                });
            }
        });
    });


    $(document).on("change", "#SetCode", function () {

        valueCalculation();

        if (program.length <= 0) { $('#Program').addClass('highlight'); }
        if (session.length <= 0) { $('#Session').addClass('highlight'); }
        if (course.length <= 0) { $('#Course').addClass('highlight'); }
        if (examCode.length <= 0) { $('#Exam').addClass('highlight'); }
        if (setCode.length <= 0) { $('#SetCode').addClass('highlight'); }
        if (program.length > 0 && session.length > 0 && course.length > 0 && examCode.length > 0 && setCode.length > 0) {
            $.ajax({
                url: "/Exam/UploadAnswerSheet/GetEmptyExamSheetForm",
                type: "POST",
                data: { program: program, session: session, course: course, examCode: $("#Exam").val(), setCode: setCode, examId: $("#ExamHidden").val() },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    if (data.IsSuccess) {
                        $('#answerSheet').html(data.Message);
                        $("#answerSubmitBtn").show();
                        //disableAllDropDown();
                        $.unblockUI();
                    } else {
                        $("#answerSheetUpdate").empty();
                        $.fn.customMessage({
                            displayMessage: data.Message,
                            displayMessageType: "error"
                        });

                    }
                },
                complete: function () {
                    $.unblockUI();

                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: AJAX_ERROR,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }

    });

    function disableAllDropDown(parameters) {
        $("#Program").prop('disabled', 'disabled');
        $("#Session").prop('disabled', 'disabled');
        $("#Course").prop('disabled', 'disabled');
        $("#Exam").prop('disabled', 'disabled');
        $("#SetCode").prop('disabled', 'disabled');
    }

    function valueCalculation(parameters) {
        program = $("#Program").val();
        session = $("#Session").val();
        course = $("#Course").val();
        examCode = $("#ExamHidden").val();
        setCode = $("#SetCode").val();
    }

    function validValueCalculationForUpdate(parameters) {
        program = $("#Program").val();
        session = $("#Session").val();
        course = $("#Course").val();
        examCode = $("#ExamHidden").val();
        setCode = $("#SetCodeUpdate").val();
    }

    $(document).on("input propertychange", "#Exam", function (event) {
        var $this = $(this);

        //console.log("---------------->>>>>>" + courseId);
        //if (programId.length > 0 && sessionId.length > 0 && courseId.length > 0) {
        $this.typeahead({
            source: function (query, process) {

                var programId = $("#Program").val();
                var sessionId = $("#Session").val();
                var courseId = $("#Course").val();

                var data = { query: query, programId: programId, sessionId: sessionId, courseId: courseId, isMcq: true };
                $.ajax({
                    url: "/Exam/UploadAnswerSheet/GetExam",
                    type: "POST",
                    //cache: false,
                    //async: true,
                    data: data,
                    success: function (result) {
                        if (result.IsSuccess) {
                            process(result.returnList);
                        }
                    },
                    complete: function () {
                    },
                    error: function (result) {
                    }
                });
            },
            updater: function (item) {
                $("#ExamHidden").val(item.Value);
                $("#Exam").val("");
                $("#ExamName").val("");
                $.ajax({
                    type: "post",
                    url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForExamInformation",
                    cache: false,
                    async: true,
                    data: { "id": item.Value, "isAnswerUpload":"Y" },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (result) {
                        $.unblockUI();
                        if (result.IsSuccess) {
                            console.log(result.returnList);
                            if (result.returnList.hasOwnProperty("Name")) {
                                $("#ExamName").val(result.returnList.Name);
                                //$("#ExamCode").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").text("");
                            }
                        } else {
                            $("#answerSheetUpdate").empty();
                            $.fn.customMessage({
                                displayMessage: result.Message,
                                displayMessageType: "error"
                            });
                        }
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (result) {
                        $.unblockUI();
                        console.log("Failed -----");
                    }
                });
                return item.Text;
            },
            matcher: function (item) {
                if (item.Text.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                    return true;
                }
                //return false;
            },
            sorter: function (items) {
                return items.sort();
            },
            highlighter: function (item) {
                var regex = new RegExp('(' + this.query + ')', 'gi');
                return item.Text.replace(regex, "<strong>$1</strong>");
            },
            onselect: function (obj) {
                alert('Selected ' + obj);
            }
        });
        //}

    });

    function fileUploadProgress(prefix, e) {
        if ($("#" + prefix + "FileProgress").hasClass('hidden') == true) {
            $("#" + prefix + "FileProgress").fadeIn('slow').removeClass('hidden');
        }
        //$('progress').attr({ value: e.loaded, max: e.total });
        var persent = Math.floor((e.loaded * 100) / e.total);
        $("#" + prefix + "FileProgressPersent").html(persent);//(e.loaded / 1024 / 1024).toFixed(2) + " MB of " + (e.total / 1024 / 1024).toFixed(2) + " MB. " + 
        $("#" + prefix + "FileProgressBar").css("width", persent + '%').attr('aria-valuenow', persent);
        if (persent >= 100) {
            var fileSize = (e.total / 1024).toFixed(2);
            if (fileSize < 1024) fileSize = fileSize + " KB";
            else fileSize = (e.total / 1024 / 1024).toFixed(2) + " MB";
            $("#" + prefix + "FileSend").prop('disabled', true);
            $("#" + prefix + "FileProgress").fadeOut('slow').addClass('hidden');
            $("#" + prefix + "FileMessage").html(fileSize + " File Upload Complete.");
            $("#" + prefix + "LoadingBlock").fadeIn().removeClass('hidden');
        }
    }

    function pmProgressHandlingFunction(e) {
        if (e.lengthComputable) {
            fileUploadProgress("pm", e);
        }
    }

    $("#pmFileSend").on("click", function () {

        valueCalculation();

        if (program.length <= 0) { $('#Program').addClass('highlight'); }
        if (session.length <= 0) { $('#Session').addClass('highlight'); }
        if (course.length <= 0) { $('#Course').addClass('highlight'); }
        if (examCode.length <= 0) { $('#Exam').addClass('highlight'); }
        if (setCode.length <= 0) { $('#SetCode').addClass('highlight'); }

        if ($('#pmFile')[0].files[0] == undefined) {
            //window.showTopMessage("i", "Please select a file");
            return;
        }
        console.log(program + "-->" + session + "-->" + course + "-->" + examCode + "-->" + setCode);
        //var istrue = ValidatationCheck(program, session, course, examCode, setCode);
        if (program.length > 0 && session.length > 0 && course.length > 0 && examCode.length > 0 && setCode.length > 0) {
            $(".progress").show();
            $("#pmFileSend").prop('disabled', true);
            var fileInfo = new window.FormData();
            fileInfo.append('pmFile', $('#pmFile')[0].files[0]);
            fileInfo.append('Program', $("#Program").val());
            fileInfo.append('Session', $("#Session").val());
            fileInfo.append('Course', $("#Course").val());
            fileInfo.append('Exam', $("#ExamHidden").val());
            fileInfo.append('SetCode', $("#SetCode").val());
            $.ajax({
                url: "/Exam/UploadAnswerSheet/UploadFile",
                type: "POST",
                data: fileInfo,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                xhr: function () { // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) { // Check if upload property exists
                        myXhr.upload.addEventListener('progress', pmProgressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                cache: false,
                success: function (data1) {
                    $("#answerSubmitBtn").hide();
                    if (data1.IsSuccess) {
                        /*Finally Save*/
                        $.ajax({
                            url: "/Exam/UploadAnswerSheet/UploadFilePost",
                            type: "POST",
                            data: fileInfo,
                            processData: false,
                            contentType: false,
                            xhr: function () { // Custom XMLHttpRequest
                                var myXhr = $.ajaxSettings.xhr();
                                if (myXhr.upload) { // Check if upload property exists
                                    myXhr.upload.addEventListener('progress', pmProgressHandlingFunction, false); // For handling the progress of the upload
                                }
                                return myXhr;
                            },
                            cache: false,
                            success: function (data2) {
                                if (data2.IsSuccess) {
                                    $('#answerSheet').html(data2.Message);
                                    $("#answerSubmit").show();
                                    $("#answerEdit").show();
                                } else {
                                    $.fn.customMessage({
                                        displayMessage: data2.Message,
                                        displayMessageType: "error",
                                    });
                                }
                            },
                            complete: function () {
                            },
                        });
                        /*End*/

                    } else {
                        bootbox.confirm("<h3>This Answer sheet have already uploaded. Are you sure you want to Overwrite <span class='confirm-message'>" + name + "</span> Answer Sheet</h3>", function (result) {
                            if (result) {
                                //fileInfo.append('overwrite', "true");
                                $.ajax({
                                    url: "/Exam/UploadAnswerSheet/UploadFilePost",
                                    type: "POST",
                                    data: fileInfo,
                                    processData: false,
                                    contentType: false,
                                    xhr: function () { // Custom XMLHttpRequest
                                        var myXhr = $.ajaxSettings.xhr();
                                        if (myXhr.upload) { // Check if upload property exists
                                            myXhr.upload.addEventListener('progress', pmProgressHandlingFunction, false); // For handling the progress of the upload
                                        }
                                        return myXhr;
                                    },
                                    cache: false,
                                    success: function (data2) {
                                        console.log("data2 message:" + data2);
                                        if (data2.IsSuccess == false) {
                                            $.fn.customMessage({
                                                displayMessage: data2.Message,
                                                displayMessageType: "error"
                                            });
                                        } else {
                                            $('#answerSheet').html(data2.Message);
                                            //$.fn.customMessage({
                                            //    displayMessage: data2.Message,
                                            //    displayMessageType: "s"
                                            //});

                                        }
                                        $("#answerSubmit").show();
                                        $("#answerEdit").show();
                                    },
                                    complete: function () {
                                    },
                                });
                            }
                        }).css({ 'margin-top': (($(window).height() / 4)) });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
            });
        }
    });

    function validationCheck() {
        $('#SetCode').removeClass('highlight');
        $('#XmlSetCode').removeClass('highlight');
        $('#Exam').removeClass('highlight');
        var answers = "";
        $('.answerScript').each(function (index, val) {
            var value = "";
            $(this).find('input[type=checkbox]').each(function () {
                if ($(this).is(':checked')) {
                    value = value + $(this).val();
                    //console.log(value);
                }
            });
            answers = answers + value + ",";

        });
        console.log("-------------->>" + answers);
        return answers;
    }

    $("#answerSubmitBtn").on("click", function () {
        var answers = validationCheck();
        valueCalculation();
        var isReqSendToServer = true;
        console.log(answers);
        var examId = $("#ExamHidden").val();
        var overwrite = $("#overwrite").val();
        if (examId.length <= 0) { $('#Exam').addClass('highlight'); isReqSendToServer = false; }
        if (examCode.length <= 0) { $('#Exam').addClass('highlight'); isReqSendToServer = false; }
        if (setCode.length <= 0) { $('#SetCode').addClass('highlight'); isReqSendToServer = false; }
        if (isReqSendToServer) {
            if (overwrite == "overwrite") {
                $.ajax({
                    url: "/Exam/UploadAnswerSheet/SaveAnswerManually",
                    type: "POST",
                    data: { answers: answers, setCode: setCode, examId: examId, 'overwrite': overwrite },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    cache: false,
                    success: function (data2) {
                        $.unblockUI();
                        $("#pmFileSend").prop('disabled', false);
                        if (data2.IsSuccess) {
                            disableAllDropDown();
                            $.fn.customMessage({
                                displayMessage: data2.Message,
                                displayMessageType: "s"
                            });
                        } else {
                            $.fn.customMessage({
                                displayMessage: data2.Message,
                                displayMessageType: "error"
                            });
                        }
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                });
            } else {
                $.ajax({
                    url: "/Exam/UploadAnswerSheet/SaveAnswerManually",
                    type: "POST",
                    data: { answers: answers, setCode: setCode, examId: examId },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    cache: false,
                    success: function (data2) {
                        $.unblockUI();
                        $("#pmFileSend").prop('disabled', false);
                        if (data2.IsSuccess) {
                            disableAllDropDown();
                            $.fn.customMessage({
                                displayMessage: data2.Message,
                                displayMessageType: "s"
                            });
                        } else {
                            $.fn.customMessage({
                                displayMessage: data2.Message,
                                displayMessageType: "error"
                            });
                        }
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                });
            }

        }
    });

    $("#answerSubmit").on("click", function () {

        var answers = validationCheck();
        valueCalculation();
        var isReqSendToServer = true;

        console.log(answers);
        var examId = $("#ExamHidden").val();
        var xmlSetcode = $("#XmlSetCode").val();
        if (setCode.length <= 0) { $('#SetCode').addClass('highlight'); isReqSendToServer = false; }
        if (xmlSetcode.length <= 0) { $('#xmlSetcode').addClass('highlight'); isReqSendToServer = false; }
        if (examId.length <= 0) { $('#Exam').addClass('highlight'); isReqSendToServer = false; }

        var fileInfo1 = new window.FormData();
        fileInfo1.append('pmFile', $('#pmFile')[0].files[0]);
        fileInfo1.append('Program', $("#Program").val());
        fileInfo1.append('Session', $("#Session").val());
        fileInfo1.append('Course', $("#Course").val());
        fileInfo1.append('Exam', $("#ExamHidden").val());
        fileInfo1.append('SetCode', $("#SetCode").val());

        if (isReqSendToServer == true) {
            $.ajax({
                url: "/Exam/UploadAnswerSheet/UploadFile",
                type: "POST",
                data: fileInfo1,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                xhr: function () { // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) { // Check if upload property exists
                        myXhr.upload.addEventListener('progress', pmProgressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                cache: false,
                success: function (data) {
                    $.unblockUI();
                    if (data.IsSuccess) {
                        $.ajax({
                            url: "/Exam/UploadAnswerSheet/SaveAnswer",
                            type: "POST",
                            data: { answers: answers, setCode: setCode, examId: examId, xmlSetcode: xmlSetcode },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            cache: false,
                            success: function (data2) {
                                $.unblockUI();
                                $("#pmFileSend").prop('disabled', false);
                                if (data2.IsSuccess) {
                                    disableAllDropDown();
                                    $.fn.customMessage({
                                        displayMessage: data2.Message,
                                        displayMessageType: "s"
                                    });
                                } else {
                                    $.fn.customMessage({
                                        displayMessage: data2.Message,
                                        displayMessageType: "error"
                                    });
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                        });

                    } else {
                        bootbox.confirm("<h3>Are you sure you want to Overwrite <span class='confirm-message'>" + name + "</span> Answer Sheet</h3>", function (result) {
                            if (result) {
                                $.ajax({
                                    url: "/Exam/UploadAnswerSheet/SaveAnswer",
                                    type: "POST",
                                    data: { answers: answers, setCode: setCode, examId: examId, xmlSetcode: xmlSetcode, 'overwrite': 'true' },
                                    beforeSend: function () {
                                        $.blockUI({
                                            timeout: 0,
                                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                        });
                                    },
                                    cache: false,
                                    success: function (data2) {
                                        $.unblockUI();
                                        $("#pmFileSend").prop('disabled', false);
                                        if (data2.IsSuccess) {
                                            $.fn.customMessage({
                                                displayMessage: data2.Message,
                                                displayMessageType: "s"
                                            });
                                        } else {
                                            $.fn.customMessage({
                                                displayMessage: data2.Message,
                                                displayMessageType: "error"
                                            });
                                        }
                                    },
                                    complete: function () {
                                        $.unblockUI();
                                    },
                                });
                            }
                        }).css({ 'margin-top': (($(window).height() / 4)) });
                    }
                },
                complete: function () {
                    $.unblockUI();
                    $("#pmFileSend").prop('disabled', false);
                },
                error: function (data) {
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    $("#answerEdit").on("click", function () {
        $("#answerSheet").empty();
        $("#answerSubmit").hide();
        $("#answerEdit").hide();
        $("#pmFileMessage").hide();
        $(".progress").hide();
        $("#pmFileSend").removeAttr('disabled');

    });

    $("#SetCodeUpdate").on("change", function () {
        validValueCalculationForUpdate();
        console.log("--------->>>>>>" + examCode);
        examId = $("#ExamHidden").val();
        if (program.length <= 0) { $('#Program').addClass('highlight'); }
        if (session.length <= 0) { $('#Session').addClass('highlight'); }
        if (course.length <= 0) { $('#Course').addClass('highlight'); }
        if (examId.length <= 0) { $('#Exam').addClass('highlight'); }
        if (setCode.length <= 0) { $('#SetCode').addClass('highlight'); }

        console.log("--------->>>>>>" + examId);
        if (program.length > 0 && session.length > 0 && course.length > 0 && examId.length > 0 && setCode.length > 0) {
            $.ajax({
                url: "/Exam/UploadAnswerSheet/GetExamAnswerSheet",
                type: "POST",
                data: { program: program, session: session, course: course, examId: examId, setCode: setCode },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    if (data.IsSuccess) {
                        $("#answerSheetUpdate").html(data.Message);
                        disableAllDropDown();
                        $.unblockUI();
                    } else {
                        $("#answerSheetUpdate").empty();
                        $.fn.customMessage({
                            displayMessage: data.Message,
                            displayMessageType: "error"
                        });

                    }
                },
                complete: function () {
                    $.unblockUI();

                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: AJAX_ERROR,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }

    });

    $("#answerUpdate").on("click", function () {
        var answers = validationCheck();
        console.log(answers);
        validValueCalculationForUpdate();
        var isReqSendToServer = true;

        console.log(answers);
        var examId = $("#ExamHidden").val();
        var xmlSetcode = $("#XmlSetCode").val();
        if (setCode.length <= 0) { $('#SetCode').addClass('highlight'); isReqSendToServer = false; }
        if (xmlSetcode.length <= 0) { $('#xmlSetcode').addClass('highlight'); isReqSendToServer = false; }
        if (examId.length <= 0) { $('#Exam').addClass('highlight'); isReqSendToServer = false; }
        if (isReqSendToServer) {
            bootbox.confirm("<h3>Are you sure you want to Overwrite <span class='confirm-message'>" + name + "</span> Answer Sheet</h3>", function (result) {
                if (result) {
                    $.ajax({
                        url: "/Exam/UploadAnswerSheet/SaveAnswer",
                        type: "POST",
                        data: { answers: answers, setCode: setCode, examId: examId, xmlSetcode: xmlSetcode, 'overwrite': 'true' },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        cache: false,
                        success: function (data2) {
                            $.unblockUI();

                            if (data2.IsSuccess) {
                                $.fn.customMessage({
                                    displayMessage: data2.Message,
                                    displayMessageType: "s"
                                });
                            } else {
                                $.fn.customMessage({
                                    displayMessage: data2.Message,
                                    displayMessageType: "error"
                                });
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                    });
                }
            }).css({ 'margin-top': (($(window).height() / 4)) });
        }

    });

    $("#answerDelete").on("click", function () {
        var answers = validationCheck();
        console.log(answers);
        validValueCalculationForUpdate();
        var isReqSendToServer = true;

        console.log(answers);
        var examId = $("#ExamHidden").val();
        var xmlSetcode = $("#XmlSetCode").val();
        if (setCode.length <= 0) { $('#SetCode').addClass('highlight'); isReqSendToServer = false; }
        if (xmlSetcode.length <= 0) { $('#xmlSetcode').addClass('highlight'); isReqSendToServer = false; }
        if (examId.length <= 0) { $('#Exam').addClass('highlight'); isReqSendToServer = false; }
        if (isReqSendToServer) {
            bootbox.confirm("<h3>Are you sure you want to Delete <span class='confirm-message'>" + name + "</span> Answer Sheet</h3>", function (result) {
                if (result) {
                    $.ajax({
                        url: "/Exam/UploadAnswerSheet/DeleteAnswer",
                        type: "POST",
                        data: { answers: answers, setCode: setCode, examId: examId, xmlSetcode: xmlSetcode },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        cache: false,
                        success: function (data2) {
                            $.unblockUI();

                            if (data2.IsSuccess) {
                                $("#answerSheetUpdate").empty();
                                $.fn.customMessage({
                                    displayMessage: data2.Message,
                                    displayMessageType: "s"
                                });
                            } else {
                                $("#answerSheetUpdate").empty();
                                $.fn.customMessage({
                                    displayMessage: data2.Message,
                                    displayMessageType: "error"
                                });
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                    });
                }
            }).css({ 'margin-top': (($(window).height() / 4)) });
        }

    });

    /*Get Exams*/
    //$(document).on("change", "#Course", function () {
    //    var programId = $("#Program").val();
    //    var sessionId = $("#Session").val();
    //    var courseId = $("#Course").val();
    //    $.ajax({
    //        url: "/Exam/UploadAnswerSheet/GetExam",
    //        type: "POST",
    //        data: { programId: programId, sessionId: sessionId, courseId: courseId },
    //        beforeSend: function () {
    //            $.blockUI({
    //                timeout: 0,
    //                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //            });
    //        },
    //        success: function (data) {
    //            $("#Exam").html(data.ExamsOptions);
    //            $.unblockUI();
    //        },
    //        complete: function () {
    //            $.unblockUI();
    //        },
    //        error: function (data) {
    //            $.fn.customMessage({
    //                displayMessage: AJAX_ERROR,
    //                displayMessageType: "error",
    //                displayTime: 5000
    //            });
    //        }
    //    });
    //});
    //Initialization of fileupload
    //Handler for "Start upload" button 
    //$("#start-upload").on('click', function () {
    //    'use strict';
    //    console.log("GET THIS ");
    //    $('#fileUpload').fileupload({
    //        url: "/Exam/UploadAnswerSheet/UploadFile",
    //        type: "POST",
    //        dataType: 'json',
    //        add: function (e, data) {
    //            jqXHRData = data;
    //            console.log(jqXHRData);
    //        },
    //        done: function (event, data) {
    //            if (data.result.isUploaded) {
    //            }
    //            else {
    //            }
    //            alert(data.result.message);
    //        },
    //        fail: function (event, data) {
    //            if (data.files[0].error) {
    //                alert(data.files[0].error);
    //            }
    //        }
    //    });
    //    if (jqXHRData) {
    //        console.log(jqXHRData);
    //        jqXHRData.submit();
    //    }
    //    return false;
    //});

});