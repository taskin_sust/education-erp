﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.AreaControllersTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "AreaControllers")]
    public class AreaControllersUpdateTest : AreaControllersBaseTest
    {
        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var areaControllersFactory =
                 _areaControllersFactory.Create().WithNameAndRank(_name, 1).WithArea(_areaName)
                     .Persist();
            Assert.Throws<ArgumentNullException>(() => _areaControllersService.Update(areaControllersFactory.Object.Id, null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var areaControllersFactory =
                _areaControllersFactory.Create().WithNameAndRank(_name, 1).WithArea(_areaName)
                    .Persist();
            AreaControllers areaObj = _areaControllersService.LoadById(areaControllersFactory.Object.Id);
            areaObj.Name = areaObj.Name + " DEF GHI";
            areaObj.Area = areaObj.Area + " KHT GHR";
            var success=_areaControllersService.Update(areaObj.Id, areaObj);
            Assert.True(success);
        }

        [Fact]
        public void Update_Should_Check_Duplicate_Entry()
        {
            AreaControllersFactory areaControllersFactory =
                _areaControllersFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithArea(_areaName).Persist();
            var area2 = new AreaControllers();
            area2.Name = areaControllersFactory.Object.Name;
            area2.Area = areaControllersFactory.Object.Area;
            area2.BusinessId = areaControllersFactory.Object.BusinessId;
            area2.Id = 0;
            area2.Name = areaControllersFactory.Object.Name + " HJKHJKJK";
            area2.Area = areaControllersFactory.Object.Area + " HGHGHGH";
            _areaControllersService.Save(area2);
            area2.Name = areaControllersFactory.Object.Name;
            Assert.Throws<DuplicateEntryException>(() => _areaControllersService.Update(area2.Id, area2));
            _areaControllersFactory.CleanupAreaController(new List<long>() { area2.Id });
        }
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Not_Update_Empty_Name()
        {
            var areaControllersFactory =
                _areaControllersFactory.Create().WithNameAndRank(_name, 1).WithArea(_areaName)
                    .Persist();
            areaControllersFactory.Object.Name = "";
            Assert.Throws<EmptyFieldException>(() => _areaControllersService.Update(areaControllersFactory.Object.Id, areaControllersFactory.Object));
        }
        [Fact]
        public void Should_Not_Update_Empty_areaName()
        {
            var areaControllersFactory =
                 _areaControllersFactory.Create().WithNameAndRank(_name, 1).WithArea(_areaName)
                     .Persist();
            areaControllersFactory.Object.Area = "";
            Assert.Throws<EmptyFieldException>(() => _areaControllersService.Update(areaControllersFactory.Object.Id, areaControllersFactory.Object));
        }

        #endregion
    }
}
