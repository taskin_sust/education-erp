﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Conventions;
using FluentNHibernate.Utils;
using log4net;
using NHibernate;
using UdvashERP.Dao.Exam;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Exam
{
    public interface IIndividualPerformanceService : IBaseService
    {
        #region List Loading Functions
        List<List<string>> LoadIndividualReport(string studentProgramRoll, out decimal totalObtainedMarks, out decimal totalFullMarks, out decimal finalMeritPosition,long? examId=null);
        IList<IndividualMeritListNewDto> LoadIndividualMeritListReport(StudentProgram studentProgram); 
        #endregion
       
        #region Other Functions
        decimal CountIndividualMeritPosition(List<List<ExamsStudentMarks>> mpList, string studentProgramRoll); 
        #endregion
    }

    public class IndividualPerformanceService : BaseService, IIndividualPerformanceService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IExamsStudentMarksDao _examsStudentMarksDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IExamsDao _examsDao;
        private readonly  IStudentCourseDetailDao _courseDetailDao;
        public IndividualPerformanceService(ISession session)
        {
            Session = session;
            _examsStudentMarksDao = new ExamsStudentMarksDao() { Session = session };
            _studentProgramDao = new StudentProgramDao() { Session = session };
            _examsDao = new ExamsDao(){Session = session};
            _courseDetailDao=new StudentCourseDetailDao(){Session = session};
        } 
        #endregion
       
        #region List Loading Functions
        public List<List<string>> LoadIndividualReport(string studentProgramRoll, out decimal totalObtainedMarks, out decimal totalFullMarks, out decimal finalMeritPosition, long? selectedExamId=null )
        {
            try
            {
                totalObtainedMarks = 0;
                totalFullMarks = 0;
                finalMeritPosition = 0;
                var studentProgram = _studentProgramDao.GetStudentProgram(studentProgramRoll);
                int sl = 0;
                var resultList = new List<List<string>>();
                
                if (selectedExamId != null)
                {
                    var examName = _examsDao.GetExamName(Convert.ToInt64(selectedExamId));
                    var individualResult= _examsStudentMarksDao.GetIndividualReport(studentProgram.Program.Id, studentProgram.Batch.Session.Id, Convert.ToInt64(selectedExamId), studentProgram.Id, studentProgram.Batch.Branch.Id);
                    var writtenMarks = "";
                    var mcqMarks = "";
                    if (individualResult != null)
                    {
                        if (individualResult.IsMcq)
                        {
                            if (individualResult.McqMarks != null)
                            {
                                mcqMarks = individualResult.McqMarks.ToString();
                            }
                            else
                            {
                                mcqMarks = "A";
                            }
                        }
                        else
                        {
                            mcqMarks = "N/A";
                        }
                        if (individualResult.IsWritten)
                        {
                            if (individualResult.WrittenMarks != null)
                            {
                                writtenMarks = individualResult.WrittenMarks.ToString();
                            }
                            else
                            {
                                writtenMarks = "A";
                            }
                        }
                        else
                        {
                            writtenMarks = "N/A";
                        }
                        var resultByExam = new List<string>()
                        {
                            examName,
                            mcqMarks,
                            writtenMarks,
                            individualResult.TotalMarks.ToString(),
                            individualResult.HighestMark.ToString(),
                            individualResult.FullMarks.ToString(),
                            individualResult.Bmp.ToString(),
                            individualResult.Cmp.ToString()
                        };
                        resultList.Add(resultByExam);
                        if (individualResult.TotalMarks != null)
                        {
                            totalObtainedMarks += Convert.ToDecimal(individualResult.TotalMarks);
                        }
                        totalFullMarks += Convert.ToDecimal(individualResult.FullMarks);
                        finalMeritPosition = 0;
                    }
                }
                else
                {
                    var complementaryCourseIdList = new List<long>();
                    
                    var courses = studentProgram.StudentCourseDetails.Select(
                            x => x.CourseSubject.Course).ToList();
                    foreach (var course in courses)
                    {
                        complementaryCourseIdList.AddRange(course.ComplementaryCourses
                            .Where(x => x.Program.Id == studentProgram.Program.Id && x.Session.Id == studentProgram.Batch.Session.Id)
                            .Select(x => x.CompCourse.Id).Distinct());
                    }
                    
                    IList<long> compExamList = _examsDao.LoadCompExamIds(complementaryCourseIdList);
                    IList<long> examIdList = _examsDao.LoadExamIds(studentProgramRoll);
                    examIdList = examIdList.Union(compExamList).ToList();
                    var registeredCourseIds = _courseDetailDao.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id).Select(x=>x.CourseSubject.Course.Id).Distinct().ToList();
                    var registeredCourseSubjectIds = _courseDetailDao.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id).Select(x => x.CourseSubject.Subject.Id).Distinct().ToList();
                    foreach (var examId in examIdList)
                    {
                        var exam = _examsDao.LoadById(examId);
                        var examCourseId = exam.Course.Id;
                        var examNullSubject = exam.ExamDetails.Where(x => x.Subject == null && x.Exams.Id == examId).ToList();
                        bool check ;
                        if (examNullSubject.IsNotEmpty())
                        {
                            check = true;
                        }
                        else 
                        {
                            var examSubject = exam.ExamDetails.Where(x => x.Subject != null).Select(x => x.Subject).ToList();
                            var checkSubjectIds = examSubject.Where(x => x.Id.In(registeredCourseSubjectIds.ToArray()));
                            check = checkSubjectIds.IsNotEmpty();
                        }
                        if ((registeredCourseIds.Contains(examCourseId) && check) || complementaryCourseIdList.Contains(examCourseId))
                        {
                            var examName = _examsDao.GetExamName(examId);
                            var individualResult = _examsStudentMarksDao.GetIndividualReport(studentProgram.Program.Id, studentProgram.Batch.Session.Id, examId, studentProgram.Id, studentProgram.Batch.Branch.Id);
                            var writtenMarks = "";
                            var mcqMarks = "";
                            if (individualResult != null)
                            {
                                if (individualResult.IsMcq)
                                {
                                    if (individualResult.McqMarks != null)
                                    {
                                        mcqMarks = individualResult.McqMarks.ToString();
                                    }
                                    else
                                    {
                                        mcqMarks = "A";
                                    }
                                }
                                else
                                {
                                    mcqMarks = "N/A";
                                }
                                if (individualResult.IsWritten)
                                {
                                    if (individualResult.WrittenMarks != null)
                                    {
                                        writtenMarks = individualResult.WrittenMarks.ToString();
                                    }
                                    else
                                    {
                                        writtenMarks = "A";
                                    }
                                }
                                else
                                {
                                    writtenMarks = "N/A";
                                }
                                var resultByExam = new List<string>() { examName, mcqMarks, writtenMarks, individualResult.TotalMarks.ToString(), individualResult.HighestMark.ToString(), individualResult.FullMarks.ToString(), individualResult.Bmp.ToString(), individualResult.Cmp.ToString() };
                                resultList.Add(resultByExam);
                                if (individualResult.TotalMarks != null)
                                {
                                    totalObtainedMarks += Convert.ToDecimal(individualResult.TotalMarks);
                                }
                                totalFullMarks += Convert.ToDecimal(individualResult.FullMarks);  
                            }
                            else
                            {
                                mcqMarks = "A";
                                writtenMarks = "A";
                                const string totalMarks = "A";
                                var fullMarks = (exam.McqFullMarks+exam.WrittenFullMark).ToString();
                                const string bmp = "A";
                                const string cmp = "A";
                                var examMarks = _examsStudentMarksDao.GetExamMarksByExamId(examId);
                                decimal highestMarks = 0;
                                if (examMarks != null && examMarks.Count != 0)
                                {
                                    highestMarks = examMarks.GroupBy(x => new { x.ExamsDetails, x.StudentProgram }).Select(y => y.Select(z => z.ObtainMark).Sum()).Max();
                                }
                                
                                var resultByExam = new List<string>() { examName, mcqMarks, writtenMarks, totalMarks, highestMarks.ToString(), fullMarks, bmp, cmp };
                                resultList.Add(resultByExam);
                            }
                            
                        }
                        
                       
                    }
                    finalMeritPosition = Convert.ToDecimal(_examsStudentMarksDao.GetFinalMeritPosition(studentProgram.Program.Id, studentProgram.Batch.Session.Id, studentProgram.Id, registeredCourseIds));
                }
                
                return resultList;
                
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<IndividualMeritListNewDto> LoadIndividualMeritListReport(StudentProgram studentProgram)
        {
            try
            {
                return  _examsStudentMarksDao.LoadIndividualMeritListReport(studentProgram);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        #endregion
      
        #region Other Functions
        public decimal CountIndividualMeritPosition(List<List<ExamsStudentMarks>> mpList, string studentProgramRoll)
        {
            try
            {
                long position = 0;
                for (var i = 0; i < mpList.Count; i++)
                {
                    if (i == 0)
                    {
                        position = 1;
                    }
                    else
                    {
                        if (mpList[i].Sum(x => x.ObtainMark) < mpList[i - 1].Sum(x => x.ObtainMark))
                        {
                            position = i + 1;
                        }
                    }
                    if (mpList[i].Select(x => x.StudentProgram.PrnNo).FirstOrDefault() == studentProgramRoll)
                    {
                        break;
                    }
                }
                return position;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        } 
        #endregion
         
      
    }
}
