﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public partial class TeacherPublicRegistrationMap : BaseClassMap<TeacherPublicRegistration, long>
    {
        public TeacherPublicRegistrationMap()
        {
            Table("TeacherPublicRegistration");
            LazyLoad();
            Map(x => x.Name);
            Map(x => x.FullName);
            Map(x => x.NickName);
            Map(x => x.MobileNumber1);
            Map(x => x.MobileNumber2);
            Map(x => x.Email);
            Map(x => x.FacebookId);
            Map(x => x.Institute);
            Map(x => x.Department);
            Map(x => x.HscPassingYear);
            Map(x => x.Religion);
            Map(x => x.Gender);
            Map(x => x.VersionOfStudy1);
            Map(x => x.VersionOfStudy2);
            Map(x => x.IsUdvashStudent);
            Map(x => x.AppointmentRemarks);
            Map(x => x.Tpin);
            
            References(x => x.Organization).Column("OrganizationId");
            References(x => x.TeacherActivity1).Column("TeacherActivity1Id");
            References(x => x.TeacherActivity2).Column("TeacherActivity2Id");
            References(x => x.TeacherActivity3).Column("TeacherActivity3Id");
            References(x => x.Subject1).Column("Subject1Id");
            References(x => x.Subject2).Column("Subject2Id");
            References(x => x.Subject3).Column("Subject3Id");
        }
    }
}
