﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Microsoft.AspNet.Identity;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;
using Constants = UdvashERP.BusinessRules.Constants;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class BankBranchController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private BankBranch _bankBranchObj;
        private IList<BankBranch> _bankBranchList;
        private readonly IBankBranchService _bankBranchService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserService _userService;
        private readonly IBankService _bankService;

        public BankBranchController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _bankBranchObj = new BankBranch();
            _bankBranchList = new List<BankBranch>();
            _bankBranchService = new BankBranchService(session);
            _commonHelper = new CommonHelper();
            _userService = new UserService(session);
            _bankService = new BankService(session);
        }

        #endregion

        #region Index/Manage Page

        public ActionResult Index()
        {
            try
            {
                GetStatusText();
                var banks = _bankService.LoadBankList();
                ViewBag.bankList = new SelectList(banks.OrderBy(x => x.Name).ToList(), "Id", "Name");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                throw;
            }
            return View();
        }
        public ActionResult ManageBankBanch()
        {
            try
            {
                ViewBag.bankBranchList = new SelectList(_bankService.LoadBankList(), "Id", "Name");
                GetStatusText();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                GetStatusText();
            }
            return View();
        }

        #region Render Data Table
        public JsonResult BankBranchList(int draw, int start, int length, string bankId, string name, string shortName, string contactPerson, string contactPersonMobile, string routingNo, string swifeCode, string rank, string status)
        {
            var getData = new List<object>();
            long recordsTotal = 0;
            long recordsFiltered = 0;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        //case "0":
                        //    orderBy = "Bank";
                        //    break;
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "ShortName";
                            break;
                        case "2":
                            orderBy = "ContactPerson";
                            break;
                        case "3":
                            orderBy = "ContactPersonMobile";
                            break;
                        case "4":
                            orderBy = "RoutingNo";
                            break;
                        case "5":
                            orderBy = "SwifeCode";
                            break;
                        case "6":
                            orderBy = "Rank";
                            break;
                        case "7":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                recordsTotal = _bankBranchService.GetBankBranchCount(orderBy, orderDir, bankId, name, shortName, contactPerson, contactPersonMobile, routingNo, swifeCode, rank, status);
                recordsFiltered = recordsTotal;
                _bankBranchList = _bankBranchService.LoadBankBranch(start, length, orderBy, orderDir.ToUpper(), bankId, name, shortName, contactPerson, contactPersonMobile, routingNo, swifeCode, rank, status).ToList();
                var bankBranchObj = new BankBranch();
                var maxMinRank = new int[2];
                maxMinRank[0] = _bankBranchService.GetMinimumRank(bankBranchObj);
                maxMinRank[1] = _bankBranchService.GetMaximumRank(bankBranchObj);
                int srn = 1;
                foreach (var bankBranch in _bankBranchList)
                {
                    var str = new List<string>();
                    str.Add(srn.ToString());
                    str.Add(bankBranch.Bank != null ? bankBranch.Bank.Name : "");
                    str.Add(bankBranch.Name);
                    str.Add(bankBranch.ShortName);
                    str.Add(bankBranch.ContactPerson);
                    str.Add(bankBranch.ContactPersonMobile);
                    str.Add(bankBranch.RoutingNo);
                    str.Add(bankBranch.SwiftCode);
                    string rankbtn = bankBranch.Rank.ToString();
                    if (bankBranch.Rank > maxMinRank[0])
                    {
                        rankbtn += "<a href='#'  id='" + bankBranch.Id +
                                   "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    }
                    if (bankBranch.Rank < maxMinRank[1])
                    {
                        rankbtn += "<a href='#'  id='" + bankBranch.Id +
                                   "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    }
                    str.Add(rankbtn);
                    str.Add(StatusTypeText.GetStatusText(bankBranch.Status));
                    str.Add(
                        "<a href='" + Url.Action("Details", "BankBranch") + "?Id=" + bankBranch.Id + "' data-id='" + bankBranch.Id +
                        "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;&nbsp;"
                        +
                        "<a href='" + Url.Action("Edit", "BankBranch") + "?Id=" + bankBranch.Id + "' data-id='" + bankBranch.Id +
                        "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; "
                        +
                        "<a  id='" + bankBranch.Id.ToString() + "'    href='#' data-name='" + bankBranch.Name.ToString() +
                        "' class='glyphicon glyphicon-trash'></a> "
                        );
                    srn++;
                    getData.Add(str);
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }

        #endregion

        #endregion
        
        #region Operational Funcation

        public ActionResult Details(long id)
        {
            try
            {
                _bankBranchObj = _bankBranchService.GetBankBranch(id);
                if (_bankBranchObj != null)
                {
                    _bankBranchObj.CreateByText = _userService.GetUserNameByAspNetUserId(_bankBranchObj.CreateBy);
                    _bankBranchObj.ModifyByText = _userService.GetUserNameByAspNetUserId(_bankBranchObj.ModifyBy);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View(_bankBranchObj);
        }

        public ActionResult Create(string message)
        {
            try
            {
                var banks = _bankService.LoadBankList();
                ViewBag.bankList = new SelectList(banks.OrderBy(x => x.Name).ToList(), "Id", "Name");
                if (!string.IsNullOrEmpty(message))
                {
                    ViewBag.SuccessMessage = message; 
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(BankBranch bankBranchObj)
        {
            try
            {
                var banks = _bankService.LoadBankList();
                ViewBag.bankList = new SelectList(banks.ToList(), "Id", "Name");
                var bank = _bankService.GetBank(bankBranchObj.BankId);
                bankBranchObj.Bank = bank;
                bool isSaved = _bankBranchService.IsSave(bankBranchObj);
                if (isSaved)
                {
                    return RedirectToAction("Create", new {message = "Bank Branch successfully saved."});
                }
            }
            catch (NullObjectException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
            }
            return View();
        }

        public ActionResult Edit(long id)
        {
            var bankBranch=new BankBranch();
            try
            {
                GetStatusText();
                bankBranch = _bankBranchService.GetBankBranch(id);
                var bankId = bankBranch.Bank.Id;
                bankBranch.BankId = bankId; 
                var banks = _bankService.LoadBankList();
                ViewBag.bankList = new SelectList(banks.ToList(), "Id", "Name", bankId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
            }
            GetStatusText();
            return View(bankBranch);
        }

        [HttpPost]
        public ActionResult Edit(long id, BankBranch bankBranchObj)
        {
           
            try
            {
                GetStatusText();
                var bankBranch = _bankBranchService.GetBankBranch(id);
                var bankId = bankBranch.BankId;
                var banks = _bankService.LoadBankList();
                ViewBag.bankList = new SelectList(banks.ToList(), "Id", "Name", bankId);
                if (ModelState.IsValid)
                {
                    bool isSuccess = _bankBranchService.IsUpdate(id, bankBranchObj);
                    if (isSuccess)
                        ViewBag.SuccessMessage = "Bank Branch successfully updated.";
                    else
                        ViewBag.ErrorMessage = "Problem occurred, during session update.";
                    return View(bankBranchObj);
                }
                return View(bankBranchObj);
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem occurred, during Bank Branch update.";
            }
            return View(bankBranchObj);
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isDeleted = _bankBranchService.IsDelete(id);
                if (isDeleted)
                {
                    return Json(new Response(true, "Bank Branch sucessfully deleted."));
                }
                return Json(new Response(true, "Problem Occurred. Retry"));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, "You can't delete this Bank Branch, Bank Branch is already assigned to Supplier Bank Details."));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Bank Branch Delete Fail!"));
            }
        } 
        #endregion

        #region Other Functions

        //Remote Duplicate Swift Code Check
        public JsonResult RouteNoUniqueCheck(string routingNo, long? id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool d = _bankBranchService.IsRouteNoUnique(routingNo.Trim(), id);
                    return Json(d);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            return Json(false);
        }

        //Remote Duplicate Swift Code Check
        public JsonResult SwiftCodeUniqueCheck(string swiftCode, long? id)
            {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool d = _bankBranchService.IsSwiftCodeUnique(swiftCode.Trim(), id);
                    return Json(d);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            return Json(false);
        }

        //Rank Change
        public JsonResult RankChange(long id, int current, string action)
        {
            try
            {
                var isSuccess = _bankBranchService.UpdateRank(id, current, action);
                return Json(isSuccess ? "Rank Changes" : "Something Wrong Occured!!");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Rank Changes, Something Wrong Occured!!"));
            }

        }
        
        #endregion

        #region Helper Function

        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }

        #endregion
    }
}
