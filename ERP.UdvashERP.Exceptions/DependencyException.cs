﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.MessageExceptions
{
    public class DependencyException : Exception
    {

        public DependencyException()
            : base("Dependency Found")
        {

        }

        public DependencyException(string message)
            : base(message)
        {

        }
    }
}
