﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class NightWorkFactory : ObjectFactoryBase<NightWork>
    {
        private TestBaseService<NightWorkLog> _nightWorkLogService;
        private readonly INightWorkService _nightWorkService;
        private TeamMemberFactory _teamMemberFactory;

        public NightWorkFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _nightWorkLogService = new TestBaseService<NightWorkLog>(session);
            _nightWorkService = new NightWorkService(session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
        }

        #region Create

        public NightWorkFactory Create()
        {
            Object = new NightWork()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                NightWorkDate = DateTime.Now,
                InHouseWork = false,
                ApprovalType = 1
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public NightWorkFactory CreateWith(DateTime nightWorkDate, int approvalType)
        {
            Object = new NightWork()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                NightWorkDate = nightWorkDate,
                InHouseWork = false,
                ApprovalType = approvalType
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public NightWorkFactory WithTeamMember()
        {
            Object.TeamMember = _teamMemberFactory.Create().Persist().Object;
            return this;
        }
        public NightWorkFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }
        public NightWorkFactory Persist()
        {
            //if (Object != null)
            //{
            //    _nightWorkService.Save(Object.NightWorkDate.ToString(), Object.TeamMember.Pin.ToString(), Object.TeamMember.Pin.ToString(), "", "", "", "");
            //}

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _nightWorkService.Save(item.NightWorkDate.ToString(), item.TeamMember.Pin.ToString(), item.TeamMember.Pin.ToString(), "", "", "", "");
                    }
                }
            }

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _nightWorkService.Save(item.NightWorkDate.ToString(), item.TeamMember.Pin.ToString(), item.TeamMember.Pin.ToString(), "", "", "", "");
                    }
                }
            }

            return this;
        }

        public NightWorkFactory CreateMore(int count, IList<DateTime> nightWorkDate, IList<int> approvalTypes, IList<TeamMember> teamMembers)
        {
            for (int i = count; i < count; i++)
            {
                var obj = new NightWork()
                {
                    NightWorkDate = nightWorkDate.Count <= count ? nightWorkDate[i] : nightWorkDate.Last(),
                    ApprovalType = approvalTypes.Count <= count ? approvalTypes[i] : approvalTypes.Last(),
                    TeamMember = teamMembers.Count <= count ? teamMembers[i] : teamMembers.Last(),
                    Rank = 10,
                    InHouseWork = false,
                    BusinessId = GetPcUserName()
                };
                ObjectList.Add(obj);
            }
            return this;
        }


        #endregion

        #region Others
        #endregion

        #region CleanUp

        public void LogCleanUp(NightWorkFactory nightWorkFactory)
        {
            if (nightWorkFactory.ObjectList.Any())
            {
                DeleteLog(nightWorkFactory.ObjectList);
            }
            if (nightWorkFactory.SingleObjectList.Any())
            {
                DeleteLog(nightWorkFactory.SingleObjectList);
            }
        }

        private void DeleteLog(IEnumerable<NightWork> list)
        {
            foreach (var nightWork in list)
            {
                var tableName = GetEntityTableName(typeof(NightWorkLog));
                string swData = "DELETE FROM " + tableName + " WHERE [NightWorkId] = " + nightWork.Id + ";";
                _session.CreateSQLQuery(swData).ExecuteUpdate();
            }
        }

        public NightWorkFactory CleanUp(NightWorkFactory nightWorkFactory)
        {
            if (nightWorkFactory != null && nightWorkFactory.Object != null)
            {
                var list = _nightWorkService.LoadNightWork((DateTime)nightWorkFactory.Object.NightWorkDate,
                (DateTime)nightWorkFactory.Object.NightWorkDate, nightWorkFactory.Object.TeamMember.Id);
                if (list != null && list.Any())
                {
                    foreach (var nightWork in list)
                    {
                        var tableName = GetEntityTableName(typeof(NightWorkLog));
                        string swData = "DELETE FROM " + tableName + " WHERE [NightWorkId] = " + nightWork.Id + ";";
                        _session.CreateSQLQuery(swData).ExecuteUpdate();
                    }

                    foreach (var nightWork in list)
                    {
                        var tableName = GetEntityTableName(typeof(NightWork));
                        string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + nightWork.Id + ";";
                        _session.CreateSQLQuery(swData).ExecuteUpdate();
                    }
                }
            }
            return this;
        }

        internal NightWorkFactory CleanUpByTeamMember(long teamMemberId)
        {
            string queryFinal = "delete from HR_NightWork where TeamMemberId in ( " + string.Join(",", teamMemberId) + ")";
            _session.CreateSQLQuery(queryFinal).ExecuteUpdate();
            return this;
        }

        internal NightWorkFactory LogCleanUp()
        {
            string queryFinal = "delete from HR_NightWorkLog ";
            _session.CreateSQLQuery(queryFinal).ExecuteUpdate();
            return this;
        }

        public NightWorkFactory CleanUp()
        {
            DeleteAll();
            _teamMemberFactory.Cleanup();
            return this;
        }

        #endregion



    }
}
