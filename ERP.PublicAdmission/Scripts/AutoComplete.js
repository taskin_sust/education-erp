﻿
$.fn.myAutoComplete = function(options) {
    var settings = $.extend({
        url: "",//Ajax Url
        holder: "",//Id of text field
        hiddenField: ""       //Id of hidden field
    }, options);
    
    if (settings.url != undefined && settings.url != "" && settings.holder != undefined && settings.holder != "" && settings.hiddenField != undefined && settings.hiddenField != "") {
        console.log(settings);
        $(settings.holder).typeahead({
            source: function (query, process) {
                $.ajax({
                    type: "POST",
                    url: settings.url,
                    cache: false,
                    async: true,
                    data: { query: query },
                    //beforeSend: function () {
                    //            $.blockUI({
                    //                timeout: 0,
                    //                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    //            });
                    //},
                    success: function (result) {
                             //   $.unblockUI();
                                if (result.IsSuccess) {
                                    process(result.instituteList);
                                }
                    },
                    complete: function () {
                            //    $.unblockUI();
                    },
                    error: function (result) {
                            //    $.unblockUI();
                    }
                });
            },
            updater: function (item) {
                $(settings.holder).parent().find(settings.hiddenField).val(item.Value);
                //$(settings.hiddenField).val(item.Value);
                return item.Text;
            },
            matcher: function (item) {
                if (item.Text.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                    return true;
                }
                return false;
            },
            sorter: function (items) {
                    return items.sort();
            },
            highlighter: function (item) {
                    var regex = new RegExp('(' + this.query + ')', 'gi');
                    return item.Text.replace(regex, "<strong>$1</strong>");
            },
        });
    }
}


$(document).ready(function () {
   
});