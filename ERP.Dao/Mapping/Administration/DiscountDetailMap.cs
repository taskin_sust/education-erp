using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class DiscountDetailMap : BaseClassMap<DiscountDetail, long>
    {
        
        public DiscountDetailMap() {
			Table("DiscountDetails");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
			
            Map(x => x.PhaseName).Column("PhaseName");
			Map(x => x.MinSubject).Column("MinSubject");

            References(x => x.Discount).Column("DiscountId");
            References(x => x.Course).Column("CourseId");
        }
    }
}
