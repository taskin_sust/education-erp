﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AcademicInfoTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AcademicInfo")]
    public class AcademicInfoSaveTest : AcademicInfoBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Successfully_Save()
        {
            var tm = PersistTeamMember();
            var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            var board = _hrBoardService.LoadBoard().FirstOrDefault();
            _InstituteFactory.Create().WithInstituteCategory().Persist();
            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = examLabel.Id.ToString(),
                Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            Assert.True(_academicInfoService.UpdateAcademicInfo(tm, academicInfoArray).Any());
        }

        [Fact]
        public void Should_Return_FormateException_For_Exam()
        {
            var tm = PersistTeamMember();
            //var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            var board = _hrBoardService.LoadBoard().FirstOrDefault();
            _InstituteFactory.Create().WithInstituteCategory().Persist();

            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = "test",
                Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            Assert.Throws<FormatException>(() => _academicInfoService.UpdateAcademicInfo(tm, academicInfoArray));
        }

        [Fact]
        public void Should_Return_FormatException_For_Board()
        {
            var tm = PersistTeamMember();
            var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            //var board = _hrBoardService.LoadBoard().FirstOrDefault();
            _InstituteFactory.Create().WithInstituteCategory().Persist();

            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = examLabel.Id.ToString(),
                Board = "test",
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            Assert.Throws<FormatException>(() => _academicInfoService.UpdateAcademicInfo(tm, academicInfoArray));
        }

        [Fact]
        public void Should_Return_FormatException_For_INstitute()
        {
            var tm = PersistTeamMember();
            var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            var board = _hrBoardService.LoadBoard().FirstOrDefault();

            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = examLabel.Id.ToString(),
                Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = "test"
            };

            academicInfoArray.Add(acInfo);
            Assert.Throws<FormatException>(() => _academicInfoService.UpdateAcademicInfo(tm, academicInfoArray));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_InvalidException_For_TeamMember()
        {
            //var tm = PersistTeamMember();
            var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            var board = _hrBoardService.LoadBoard().FirstOrDefault();
            _InstituteFactory.Create().WithInstituteCategory().Persist();

            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = examLabel.Id.ToString(),
                Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            Assert.Throws<InvalidDataException>(() => _academicInfoService.UpdateAcademicInfo(null, academicInfoArray));
        }

        [Fact]
        public void Should_Return_InvalidException_For_Exam()
        {
            var tm = PersistTeamMember();
            //var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            var board = _hrBoardService.LoadBoard().FirstOrDefault();
            _InstituteFactory.Create().WithInstituteCategory().Persist();

            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                //ExamName = "test",
                Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            Assert.Throws<InvalidDataException>(() => _academicInfoService.UpdateAcademicInfo(tm, academicInfoArray));
        }
        
        [Fact]
        public void Should_Return_InvalidException_For_Board()
        {
            var tm = PersistTeamMember();
            var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            //var board = _hrBoardService.LoadBoard().FirstOrDefault();
            _InstituteFactory.Create().WithInstituteCategory().Persist();

            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = examLabel.Id.ToString(),
                //Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            Assert.Throws<InvalidDataException>(() => _academicInfoService.UpdateAcademicInfo(tm, academicInfoArray));
        }
        
        [Fact]
        public void Should_Return_InvalidException_For_INstitute()
        {
            var tm = PersistTeamMember();
            var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            var board = _hrBoardService.LoadBoard().FirstOrDefault();

            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = examLabel.Id.ToString(),
                Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                // Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            Assert.Throws<InvalidDataException>(() => _academicInfoService.UpdateAcademicInfo(tm, academicInfoArray));
        }
        
        #endregion
    }
}
