using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class GoodsReturnDetailsMap : BaseClassMap<GoodsReturnDetails, long>
    {

        public GoodsReturnDetailsMap()
        {
            Table("UINV_GoodsReturnDetails");
            LazyLoad();
            References(x => x.Item).Column("ItemId");
            References(x => x.GoodsReturn).Column("GoodsReturnId");
            Map(x => x.ReturnedQuantity).Column("ReturnedQuantity").Not.Nullable();
            Map(x => x.ReturnStatus).Column("ReturnStatus").Not.Nullable();
        }
    }
}
