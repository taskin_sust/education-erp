﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.Services.Cache;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class AdministrationHomeController : Controller
    {
        #region CacheReset
        [AuthorizeRequired]
        public ActionResult Index()
        {
            return View();
        }

        [AuthorizeAccess]
        public ActionResult CashReset()
        {
            return View();
        }
        [HttpPost]
        public JsonResult CacheResetAjax()
        {
            try
            {
                AuthorizationCache.ResetCache(true, true, true);
                return Json(new Response(true, "Menu Cache Reset Completed."));
            }
            catch (Exception ex) { }
            return Json(new Response(false, "Menu Cache Reset FAILED."));
        }
        [HttpPost]
        public JsonResult OrganizationCacheResetAjax()
        {
            try
            {
                AuthorizationCache.ResetOrganicationCache();
                return Json(new Response(true, "Organization Cache Reset Completed."));
            }
            catch (Exception ex) { }
            return Json(new Response(false, "Organization Cache Reset FAILED."));
        }
        [HttpPost]
        public JsonResult ProgramCacheResetAjax()
        {
            try
            {
                AuthorizationCache.ResetProgramCache();
                return Json(new Response(true, "Program Cache Reset Completed."));
            }
            catch (Exception ex) { }
            return Json(new Response(false, "Program Cache Reset FAILED."));
        }
        [HttpPost]
        public JsonResult BranchCacheResetAjax()
        {
            try
            {
                AuthorizationCache.ResetBranchCache();
                return Json(new Response(true, "Branch Cache Reset Completed."));
            }
            catch (Exception ex) { }
            return Json(new Response(false, "Branch Cache Reset FAILED."));
        }
        [HttpPost]
        public JsonResult SelectedCacheResetAjax(string[] selectedValues = null)
        {
            try
            {
                if (selectedValues != null && selectedValues.Any())
                {
                    var cacheName = "";
                    foreach (var selectedValue in selectedValues)
                    {
                        if (selectedValue == "menu")
                        {
                            cacheName += " Menu";
                            AuthorizationCache.ResetCache(true, true, true);
                        }
                        else if (selectedValue == "organization")
                        {
                            cacheName += ", Organization";
                            AuthorizationCache.ResetOrganicationCache();
                        }
                        else if (selectedValue == "program")
                        {
                            cacheName += ", Program";
                            AuthorizationCache.ResetProgramCache();
                        }
                        else if (selectedValue == "branch")
                        {
                            cacheName += ", Branch";
                            AuthorizationCache.ResetBranchCache();
                        }
                    }
                    return Json(new Response(true, cacheName.TrimEnd(',').TrimStart(',')+" Cache Reset Completed."));
                }
                
                //AuthorizationCache.ResetBranchCache();
                return Json(new Response(true, "No Selected value found to reset Cache."));
            }
            catch (Exception ex) { }
            return Json(new Response(false, "Branch Cache Reset FAILED."));
        }
        #endregion

        #region Display log file
        public ActionResult ReadLogFile()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }
            DirectoryInfo directory = new DirectoryInfo(Server.MapPath(@"~\__Logs"));
            var allFiles = directory.GetFiles().ToList();
            ViewBag.Files = allFiles;
            return View();
        }
        

        #endregion

        #region Download log file
        public ActionResult DownloadLogFile(string fileName)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                if (String.IsNullOrEmpty(fileName))
                    throw new InvalidDataException("No File Name Found!");
                string filePath = Server.MapPath(@"~\__Logs\"+fileName);

                if(!System.IO.File.Exists(filePath))
                    throw new InvalidDataException("No File Found!");

                string zipFileName = "" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_" + fileName + ".zip";
                MemoryStream outputMemStream = new MemoryStream();
                ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);
                zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

                var newEntry = new ZipEntry(fileName);
                newEntry.DateTime = DateTime.Now;
                zipStream.PutNextEntry(newEntry);
                var bytes = System.IO.File.ReadAllBytes(filePath);
                if (bytes.Count() <= 0)
                {
                    throw new InvalidDataException("Nothing to download!");
                }
                MemoryStream inStream = new MemoryStream(bytes);
                StreamUtils.Copy(inStream, zipStream, new byte[bytes.Length]);
                inStream.Close();
                zipStream.CloseEntry();
                zipStream.IsStreamOwner = false; // False stops the Close also Closing the underlying stream.
                zipStream.Close(); // Must finish the ZipOutputStream before using outputMemStream.
                
                outputMemStream.Position = 0;
                return File(outputMemStream.ToArray(), "application/octet-stream", zipFileName);
            }
            catch (InvalidDataException ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                //ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = WebHelper.SetExceptionMessage(ex);
                //ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                //throw;
            }
            return RedirectToAction("ReadLogFile");
            //return View();
        }


        #endregion

    }
}