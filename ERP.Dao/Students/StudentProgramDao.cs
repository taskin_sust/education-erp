﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using Remotion.Linq.Parsing.Structure.IntermediateModel;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessRules.Student;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;

namespace UdvashERP.Dao.Students
{
    public class ExpandoObjectResultSetTransformers : IResultTransformer
    {
        public object TransformTuple(object[] tuple, string[] aliases)
        {
            var expando = new ExpandoObject();
            var dictionary = (IDictionary<string, object>)expando;
            for (int i = 0; i < tuple.Length; i++)
            {
                string alias = aliases[i];
                if (alias != null)
                {
                    dictionary[alias] = tuple[i];
                }
            }
            return expando;
        }
        public IList TransformList(IList collection)
        {
            return collection;
        }
    }

    public interface IStudentProgramDao : IBaseDao<StudentProgram, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        StudentProgram GetStudentProgram(string prnNo, bool withInactive = false, bool allStatus = false);
        StudentProgram GetStudentProgram(long programId, long sessionId, long studentId);
        StudentProgram GetStudentProgram(string nickName, string mobileNum, long programId, long sessionId);
        StudentProgram GetStudentProgram(long studentExamId, long boardId, string year, string boradRoll, string registrationNumber, long programId, long sessionId);
        StudentProgram GetLastStudentProgram(string prefixPrnNo);
        StudentProgram GetStudentProgram(long programId, long sessionId, string board, long boardExam, string studentBoardRoll, string studentBoardRegNo);
        #endregion

        #region List Loading Function

        IList<StudentProgramDto> LoadStudentProgramListByCriteriaForPreviousStudentReport(int start, int length, List<long> authorizedProgramLists, List<long> authorizedBranchListsTo, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo, int studentType);
        /*---------------------------------------------------*/
        IList<StudentProgram> LoadStudentProgram(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int surveyStatus, string orderBy, string orderDir, int start, int length);
        IList<StudentProgram> LoadStudentProgram(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, long[] questionId, string[] answerName, string programRoll, string nickName, string mobile, int start, int length, string orderBy = null, string orderDir = null);
        //IList<StudentListDto> LoadAuthorizedStudents(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId,
        //    long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string orderBy,
        //    string orderDir, int start, int length, string startDate, string endDate, int selectedStatus, bool isDisplayAcademicInfo, string prnNo, string name, string mobile);
        IList<StudentListDto> LoadAuthorizedStudents(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, List<long> branchIdList, List<long> campusIdList, List<string> batchDays, List<string> batchTime, List<long> batchIdList, List<long> courseIdList, int version, int gender, List<int> religionList, string orderBy, string orderDir, int start, int length, string startDate, string endDate, int selectedStatus, bool isDisplayAcademicInfo, string prnNo, string name, string mobile);
        IList<StudentListDto> LoadAuthorizedStudents(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, string searchKey, string[] informationViewList, int start, int length, string orderBy, string orderDir, string prnNo, string name, string mobile, List<int> versionOfStudyList = null, List<int> genderIdList = null, List<int> religionIdList = null);
        /*---------------------------------------------------*/
        IList<StudentProgram> LoadStudentCompareWithBoard(int start, int length, List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long studentExamId, string year, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchNames, int[] religions, int gender, int version, string prnNo, string fullName, string fatherName, string institute);
        /*---------------------------------------------------*/
        IList<StudentProgram> LoadStudentProgram(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long studentExamId, string[] informationViewList);
        List<MissingBoardRollListDto> GetMissingBoardRollStudent(List<long> programIdList, List<long> branchIdList, int start, int length, long sessionId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null);
        List<StudentProgram> GetBoardResultStudent(List<long> programIdList, List<long> branchIdList, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null);
        List<StudentProgram> LoadStudentProgramForBoardResult(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId);
        List<StudentProgram> LoadStudentProgramForUpdateStudentInfo(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] selectedBranch, long[] selectedCampus, string[] selectedBatchDays, string[] selectedBatchTime, long[] selectedBatch, long studentExamId, int? selectStatus);
        /*---------------------------------------------------*/
        IList<StudentProgram> LoadStudentProgram(string[] prnNo);
        IList<StudentProgram> LoadStudentProgram(string mobileOrPrnNo);
        IList<StudentProgram> LoadStudentProgram(long studentId);
        List<StudentProgram> LoadAllStudentProgramList(long[] studentProgramIdList);
        /*---------------------------------------------------*/
        IList<StudentProgram> LoadStudentProgramByCriteriaForClassEvaluation(int start, int length, long lectureId, long teacherId, long[] batchIdList, DateTime dateFrom, DateTime dateTo);
        IList<StudentProgram> LoadStudentProgram(long organizationId, List<long> programIdList, SmsSettings smsSettings, bool isIn = true);
        //service block for list
        List<StudentIdCardListDto> LoadStudentProgram(long programId, long sessionId, long[] branchId, List<long> authorizedProgramLists, List<long> branchIdList, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] course, int printingStatus, int start, int length, string orderBy, string orderDir, int? distributionStatus, bool generateIdCard = false, List<ServiceBlock> serviceBlocks = null);
        //Student Admission Summery Report
        List<StudentAdmissionSummeryReport> LoadBranchProgramWiseStudentAdmssion(List<long> authBranchIdList, List<long> authProgramIdList, string dateFrom, string dateTo, bool lastSession);
        IList<long> LoadStudentProgramWithImage(List<long> authorizedProgramIdList, List<long> authorizedBranchIdList, long[] programIds, long[] sessionIds, long[] branchId, long[] campusId, long[] batchName, string[] batchDays, string[] batchTime);
        List<CourseAPIDto> LoadCourses(long id);
        IList<MissingExistingImageDto> LoadImageReports(int start, int length, List<long> branchIdList, List<long> programIdList, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList, bool isMissingImage);
        IList<dynamic> LoadStudentProgramListByCriteriaForPreviousStudentSummaryReport(int start, int length, List<long> programIdList, List<long> branchIdList, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo, int studentType);
        IList<dynamic> GenerateStudentImageList(List<long> authorizedProgramIdList, List<long> authorizedBranchIdList, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, string programRoll = "", string nickName = "", string mobileNumber = "");

        IList<ProgramAttendedSummary> LoadProgramSummaryData(string stdRollOrStdProRoll); //Viwe model included according to masum

        
        #endregion

        #region Others Function

        int GetBatchStudentCount(Batch batchObj);
        bool IsAssignedStudentToThisBranch(long programId, long branchId, long sessionId);
        long GetMissingBoardRollStudentCount(List<long> authoProgramIdList, List<long> authoBranchIdList, long sessionId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null);
        long GetBoardResultStudentCount(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null);
        long CountStudentCompareWithBoard(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long studentExamId, string year, long[] branch, long[] campus, string[] batchDays, string[] batchTimes, long[] batch, int[] religion, int? gender, int? versionOfStudy);
        long GetBoardResultStatus(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, int? status);
        //int GetAuthorizedStudentsCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus, string prnNo, string name, string mobile);
        int GetAuthorizedStudentsCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, List<long> campusIdList, List<string> batchDays, List<string> batchTime, List<long> batchIdList, List<long> courseIdList, int version, int gender, List<int> religionList, string startDate, string endDate, int selectedStatus, string prnNo, string name, string mobile);
        int GetAuthorizedStudentsCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, string searchKey, string[] informationViewList, string prnNo, string name, string mobile, List<int> versionOfStudyList = null, List<int> genderIdList = null, List<int> religionIdList = null);
        int GetStudentsCountForPreviousStudentReport(List<long> authorizedProgramLists, List<long> authorizedBranchListsTo, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo, int studentType);
        int GetStudentProgramCountByCriteriaForAttendance(List<long> authorizedProgramLists, List<long> authorizedBranchListsFrom, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus);
        int LoadStudentProgramCountForClassEvaluation(long lectureId, long teacherId, long[] batchIdList, DateTime dateFrom, DateTime dateTo);
        int GetAdmittedStudentCountByBranchProgramSession(long branchId, long programId, long sessionId);
        int GetStudentProgramCount(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId,
            long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus,
            long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds, long[] courseIds, string lectureIds);
        //for service block
        int GetAuthorizedStudentProgramCount(long programId, long sessionId, long[] branchId, List<long> authorizedProgramLists, List<long> branchIdList, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] course, int printingStatus, int? distributionStatus, List<ServiceBlock> serviceBlocks = null);
        int ImageCount(List<long> branchIdList, List<long> programIdList, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList, bool isMissingImage);
        int LoadImageReportsRowCount(List<long> branchIdList, List<long> programIdList, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList, bool isMissingImage);
        bool HasStudentProgram(long studentExamId, long boardId, string year, string boradRoll, string registrationNumber, long programId, long sessionId);
        int CountStudentImageList(List<long> authorizedProgramIdList, List<long> authorizedBranchIdList, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, string programRoll = "", string nickName = "", string mobileNumber = "");

        #endregion
                
    }

    public class StudentProgramDao : BaseDao<StudentProgram, long>, IStudentProgramDao
    {
        #region Propertise & Object Initialization
        public static IResultTransformer ExpandoObject;
        private const int MaximumParameter = 2000;
        public StudentProgramDao()
        {
            ExpandoObject = new ExpandoObjectResultSetTransformers();
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public StudentProgram GetStudentProgram(string prnNo, bool withInactive = false, bool allStatus = false)
        {

            StudentProgram obj;
            if (withInactive)
            {
                obj = Session.QueryOver<StudentProgram>().Where(x => x.PrnNo == prnNo && x.Status != StudentProgram.EntityStatus.Delete).SingleOrDefault();
            }
            else if (allStatus)
            {
                obj = Session.QueryOver<StudentProgram>().Where(x => x.PrnNo == prnNo).SingleOrDefault();
            }
            else
            {
                obj = Session.QueryOver<StudentProgram>().Where(x => x.PrnNo == prnNo && x.Status == StudentProgram.EntityStatus.Active).SingleOrDefault();
            }
            return obj;
        }

        public StudentProgram GetLastStudentProgram(string prefixPrnNo)
        {
            return Session.CreateCriteria<StudentProgram>()
                //.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active))
                    .Add(Restrictions.Not(Restrictions.Eq("Status", StudentProgram.EntityStatus.Delete)))
                    .Add(Restrictions.Like("PrnNo", prefixPrnNo, MatchMode.Start))
                    .AddOrder(Order.Desc("Id")).SetMaxResults(1).UniqueResult<StudentProgram>();

        }

        

        public StudentProgram GetStudentProgram(long programId, long sessionId, long studentId)
        {
            StudentProgram studentProgram = null;
            var criteria = Session.CreateCriteria<StudentProgram>();
            criteria.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.CreateAlias("Batch", "bh");
            criteria.Add(Restrictions.Eq("bh.Session.Id", sessionId));
            criteria.Add(Restrictions.Eq("Student.Id", studentId));
            criteria.SetMaxResults(1);
            studentProgram = criteria.UniqueResult<StudentProgram>();
            return studentProgram;
        }

        public StudentProgram GetStudentProgram(string nickName, string mobileNum, long programId, long sessionId)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Not(Restrictions.Eq("Status", StudentProgram.EntityStatus.Delete)));
            criteria.CreateCriteria("Batch", "b");
            criteria.CreateAlias("Student", "std");
            criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("std.NickName", nickName).IgnoreCase());
            criteria.Add(Restrictions.Like("std.Mobile", mobileNum, MatchMode.Anywhere));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));
            return criteria.List<StudentProgram>().LastOrDefault();
        }

        public StudentProgram GetStudentProgram(long studentExamId, long boardId, string year, string boradRoll, string registrationNumber, long programId, long sessionId)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Not(Restrictions.Eq("Status", StudentProgram.EntityStatus.Delete)));
            criteria.CreateCriteria("Batch", "b");
            criteria.CreateAlias("Student", "std");
            criteria.CreateAlias("std.StudentAcademicInfos", "sai");
            criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("sai.StudentExam.Id", studentExamId));
            criteria.Add(Restrictions.Eq("sai.StudentBoard.Id", boardId));
            criteria.Add(Restrictions.Eq("sai.Year", year));
            criteria.Add(Restrictions.Eq("sai.BoradRoll", boradRoll));
            criteria.Add(Restrictions.Eq("sai.RegistrationNumber", registrationNumber));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));
            criteria.SetTimeout(300);
            return criteria.List<StudentProgram>().LastOrDefault();
        }

        public StudentProgram GetStudentProgram(long programId, long sessionId, string board, long boardExam, string studentBoardRoll,
            string studentBoardRegNo)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.CreateCriteria("Batch", "b");
            criteria.CreateAlias("Student", "std");
            criteria.CreateAlias("std.StudentAcademicInfos", "sai").Add(Restrictions.Eq("Status", StudentAcademicInfo.EntityStatus.Active)); ;
            criteria.CreateAlias("sai.StudentBoard", "sb");
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));
           // criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("sai.StudentExam.Id", boardExam));
            criteria.Add(Restrictions.Eq("sb.Name", board));
          //  criteria.Add(Restrictions.Eq("sai.Year", year));
            criteria.Add(Restrictions.Eq("sai.BoradRoll", studentBoardRoll));
            criteria.Add(Restrictions.Eq("sai.RegistrationNumber", studentBoardRegNo));
            
            return criteria.List<StudentProgram>().LastOrDefault();
        }
        #endregion

        #region List Loading Function

        public IList<StudentProgram> LoadStudentProgram(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int surveyStatus, string orderBy, string orderDir, int start, int length)
        {
            var criteria = GetAuthorizedStudentsReportResultQuery(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, surveyStatus);
            criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            return criteria.SetFirstResult(start).SetMaxResults(length).List<StudentProgram>();
        }

        public IList<StudentProgram> LoadStudentProgram(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, long[] questionId, string[] answerName, string programRoll, string nickName, string mobile, int start, int length, string orderBy = null, string orderDir = null)
        {
            var criteria = GetAuthorizedStudentWiseSurveyReportQuery(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, questionId, answerName, programRoll, nickName, mobile);
            criteria.SetProjection(Projections.Distinct(Projections.Property("Id")));
            var clonedDetachedCriteria = new ConvertedDetachedCriteria(CriteriaTransformer.Clone(criteria));
            var newCriteria = Session.CreateCriteria<StudentProgram>().Add(Subqueries.PropertyIn("Id", clonedDetachedCriteria)); // Mod.ID in (select
            IList<StudentProgram> r = new List<StudentProgram>();
            r = length > 0 ? newCriteria.SetFirstResult(start).SetMaxResults(length).List<StudentProgram>() : newCriteria.List<StudentProgram>();
            return r;
        }

        public IList<StudentProgram> LoadStudentProgram(string[] prnNo)
        {
            return
                Session.CreateCriteria<StudentProgram>()
                    .Add(Restrictions.In("PrnNo", prnNo)).Add(Restrictions.Eq("Status", Institute.EntityStatus.Active)).List<StudentProgram>();
        }


        public IList<StudentProgram> LoadStudentProgram(string mobileOrPrnNo)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>();
            //criteria.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", StudentProgram.EntityStatus.Delete)));
            criteria.CreateAlias("Student", "s");
            criteria.Add(Restrictions.Or(Restrictions.Eq("PrnNo", mobileOrPrnNo), Restrictions.InsensitiveLike("s.Mobile", mobileOrPrnNo, MatchMode.End)));
            var data = (List<StudentProgram>)criteria.List<StudentProgram>();
            return data;
        }

        public IList<StudentProgramDto> LoadStudentProgramListByCriteriaForPreviousStudentReport(int start, int length, List<long> authorizedProgramLists,
            List<long> authorizedBranchListsTo, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo, int studentType)
        {
            string queryFrom = "";
            string queryTo = "";
            string query = "";

            ICriteria studentPrograms = Session.CreateCriteria<StudentProgram>();
            if (studentType == 1)
            {
                queryFrom += " where exists(select  1 from studentprogram spo inner join" +
                            " batch b on spo.batchid=b.id inner join " +
                            "branch br on br.id=b.branchid inner join " +
                            "session s on b.sessionid=s.id and spo.status=1 and b.status=1 and br.status=1 and s.status=1 and spo.studentid=sp.studentid and spo.programId=" + programIdFrom + " and s.id=" + sessionIdFrom;
                if (!branchIdFrom.Contains(SelectionType.SelelectAll))
                {
                    queryFrom += " and br.id in(" + string.Join(",", branchIdFrom) + ")";
                }
                queryTo += "     select  sp.Id,sp.prnNo from studentprogram sp inner join " +
                                "batch b on sp.batchid=b.id inner join " +
                                "branch br on br.id=b.branchid inner join " +
                                "session s on b.sessionid=s.id and sp.status=1 and b.status=1 and br.status=1 and s.status=1 and s.status=1  and sp.programId=" + programIdTo + " and s.id=" + sessionIdTo;
                if (authorizedProgramLists != null)
                {
                    queryTo += " and sp.programId in(" + string.Join(",", authorizedProgramLists) + ")";
                }
                if (authorizedBranchListsTo != null)
                {
                    queryTo += " and br.id in(" + string.Join(",", authorizedBranchListsTo) + ")";
                }
                if (!branchIdTo.Contains(SelectionType.SelelectAll))
                {
                    queryTo += " and br.id in(" + string.Join(",", branchIdTo) + ")";
                }
                queryFrom += " ) order by sp.prnNo";
                query = queryTo + queryFrom;
            }
            else
            {
                queryFrom += "select  spo.Id,spo.prnNo from studentprogram spo inner join" +
                " batch b on spo.batchid=b.id inner join " +
                "branch br on br.id=b.branchid inner join " +
                "session s on b.sessionid=s.id and spo.status=1 and b.status=1 and br.status=1 and s.status=1 and spo.programId=" + programIdFrom + " and s.id=" + sessionIdFrom;
                if (!branchIdFrom.Contains(SelectionType.SelelectAll))
                {
                    queryFrom += " and br.id in(" + string.Join(",", branchIdFrom) + ")";
                }
                queryTo += " where not exists(select  1 from studentprogram sp inner join " +
                                "batch b on sp.batchid=b.id inner join " +
                                "branch br on br.id=b.branchid inner join " +
                                "session s on b.sessionid=s.id and sp.status=1 and b.status=1 and br.status=1 and s.status=1 and s.status=1 and spo.studentid=sp.studentId and  sp.programId=" + programIdTo + " and s.id=" + sessionIdTo;
                if (authorizedProgramLists != null)
                {
                    queryTo += " and sp.programId in(" + string.Join(",", authorizedProgramLists) + ")";
                }
                if (authorizedBranchListsTo != null)
                {
                    queryTo += " and br.id in(" + string.Join(",", authorizedBranchListsTo) + ")";
                }
                if (!branchIdTo.Contains(SelectionType.SelelectAll))
                {
                    queryTo += " and br.id in(" + string.Join(",", branchIdTo) + ")";
                }
                queryTo += " ) order by spo.prnNo";
                query = queryFrom + queryTo;
            }
            query += "";
            if (length > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentProgramDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<StudentProgramDto>().ToList();
            return list;
        }

        public List<StudentProgram> LoadAllStudentProgramList(long[] studentProgramIdList)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>();
            criteria.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.Add(Restrictions.In("Id", studentProgramIdList));
            var data = (List<StudentProgram>)criteria.List<StudentProgram>();
            return data;
        }

        public IList<StudentProgram> LoadStudentProgram(long studentId)
        {
            return Session.QueryOver<StudentProgram>().Where(x => x.Student.Id == studentId && x.Status != StudentProgram.EntityStatus.Delete).List<StudentProgram>();
        }

        public IList<MissingExistingImageDto> LoadImageReports(int start, int length, List<long> branchIdList, List<long> programIdList, long programId, long sessionId, long[] branchId,
            long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList, bool isMissingImage)
        {
            string innerQuery = GetInnerQueryStudentMissingImage(branchIdList, programIdList, programId, sessionId, branchId, campusId, batchDaysList, batchTimeList, batchList, isMissingImage);
            string query = innerQuery + " SELECT BatchId, PrnNo = STUFF((SELECT ', ' + PrnNo FROM missingImageCte b WHERE b.batchid = a.batchid FOR XML PATH('')), 1, 2, '') FROM missingImageCte a GROUP BY batchid,batchrank order by batchrank";
            if (length > 0)
            {
                query += " offset " + start + " rows fetch next " + length + " rows only";
            }
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<MissingExistingImageDto>());
            return iQuery.List<MissingExistingImageDto>().ToList();
        }

        public IList<dynamic> LoadStudentProgramListByCriteriaForPreviousStudentSummaryReport(int start, int length, List<long> authorizedProgramLists,
            List<long> authorizedBranchListsTo, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo,
            long[] branchIdTo, int studentType)
        {
            string query = @"Select C.*,B.Total,B.BranchName from (
                  Select A.BranchId,A.BranchName,Count(A.Id) AS Total From (
	                   Select sp.Id,sp.StudentId,br.Id as branchId,br.Name AS BranchName FROM [dbo].[StudentProgram] AS sp
							                INNER JOIN [dbo].[Program] AS p ON sp.[ProgramId]=p.Id
							                INNER JOIN [dbo].[Batch] AS b ON sp.[BatchId]= b.Id
							                INNER JOIN [dbo].[Session] AS s ON b.[SessionId]=s.Id
							                INNER JOIN [dbo].[Branch] AS br ON b.[BranchId]= br.Id
							                AND sp.status=1
							                AND b.status=1
							                AND br.status=1
							                AND s.status=1 ";

            query += " AND p.Id= " + programIdFrom;
            query += " AND s.Id= " + sessionIdFrom;
            query += " ) AS A Group By A.BranchId,A.BranchName ";
            query += " ) AS B ";
            query += " Left join ";
            query += " ( ";

            query +=
                          "select  fq.fbrIds, COUNT(fq.[prn number]) as [Admitted] from ( " +
                          "select * from ( " +
                          "select std.NickName as [NAME] ,pp.PrnNo as [prnnumber],pp.StudentId as [studentId], pp.Id as [stdprogramId], pp.bfName, pp.fbrIds from [dbo].[Student] as std " +
                          "inner join ( " +
                          "select sp.Id,sp.PrnNo,sp.StudentId, br.Name as bfName,br.id as [fbrIds] from [dbo].[StudentProgram] as sp " +
                          "inner join [dbo].[Program] as p on sp.[ProgramId]=p.Id " +
                          "inner join [dbo].[Batch] as b on sp.[BatchId]= b.Id " +
                          "inner join [dbo].[Session] as s on b.[SessionId]=s.Id " +
                          "inner join [dbo].[Branch] as br on b.[BranchId]= br.Id " +
                          "and sp.status=1 and b.status=1 and br.status=1 and s.status=1 " +
                          "and p.Id='" + programIdFrom + "' " +
                          "and s.Id='" + sessionIdFrom + "' ";
            if (authorizedProgramLists != null)
            {
                query += " and sp.programId in(" + string.Join(",", authorizedProgramLists) + ")";
            }
            if (authorizedBranchListsTo != null)
            {
                query += " and br.id in(" + string.Join(",", authorizedBranchListsTo) + ")";
            }
            query += ") pp on pp.StudentId =std.Id and std.status =1) as fromquery " +
                     "inner join( " +

                     "select std.NickName ,pp.PrnNo as [prn number],pp.StudentId as [student Id], pp.Id as [std program Id], pp.btName from [dbo].[Student] as std inner join ( " +
                     "select sp.Id,sp.PrnNo,sp.StudentId, br.Name as btName from [dbo].[StudentProgram] as sp " +
                     "inner join [dbo].[Program] as p on sp.[ProgramId]=p.Id " +
                     "inner join [dbo].[Batch] as b on sp.[BatchId]= b.Id " +
                     "inner join [dbo].[Session] as s on b.[SessionId]=s.Id " +
                     "inner join [dbo].[Branch] as br on b.[BranchId]= br.Id " +
                     "and sp.status=1 and b.status=1 and br.status=1 and s.status=1 " +
                     "and p.Id='" + programIdTo + "' " +
                     "and s.Id='" + sessionIdTo + "' ";
            if (authorizedProgramLists != null)
            {
                query += " and sp.programId in(" + string.Join(",", authorizedProgramLists) + ")";
            }
            if (authorizedBranchListsTo != null)
            {
                query += " and br.id in(" + string.Join(",", authorizedBranchListsTo) + ")";
            }
            if (!branchIdTo.Contains(SelectionType.SelelectAll))
            {
                query += " and br.id in(" + string.Join(",", branchIdTo) + ")";
            }

            query +=
                ") as pp on pp.StudentId =std.Id and std.status=1 ) as toquery on fromquery.[studentId]= toquery.[student Id] ) as fq " +
                "group by fq.fbrIds " +
                ") AS C On C.fbrIds = B.branchId ";
            if (branchIdFrom.Length == 1 && branchIdTo[0] == 0)
            {

            }
            else
            {
                query += "where C.fbrIds in ({0}) ";
            }
            query = String.Format(query, String.Join(",", branchIdFrom));
            IQuery fQuery = Session.CreateSQLQuery(query);
            IList<dynamic> result = DynamicList(fQuery);
            return result;
        }


        public IList<dynamic> GenerateStudentImageList(List<long> authorizedProgramLists, List<long> authorizedBranchListsTo, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, string programRoll = "", string nickName = "", string mobileNumber = "")
        {
            string query1 = GetStudentImageListQuery(authorizedProgramLists, authorizedBranchListsTo, programId, sessionId, branchId, campusId, batchName, statusVal, batchDays, batchTime, programRoll, nickName, mobileNumber);
            string query = query1;
            if (length > 0)
            {
                query += " order by ins.Id OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }
            IQuery fQuery = Session.CreateSQLQuery(query);
            IList<dynamic> result = DynamicList(fQuery);
            return result;
        }

        public int CountStudentImageList(List<long> authorizedProgramLists, List<long> authorizedBranchListsTo, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, string programRoll = "", string nickName = "", string mobileNumber = "")
        {
            string query1 = GetStudentImageListQuery(authorizedProgramLists, authorizedBranchListsTo, programId, sessionId, branchId, campusId, batchName, statusVal, batchDays, batchTime, programRoll, nickName, mobileNumber);
            string query = "SELECT COUNT(fqC.[PROGRAM ROLL]) from ( " + query1 + "  ) as fqC";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;

        }

        public IList<StudentProgram> LoadStudentProgram(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long studentExamId, string[] informationViewList)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>();
            criteria.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.CreateCriteria("Batch", "b").Add(Restrictions.Eq("b.Session.Id", sessionId)).Add(Restrictions.Eq("b.Program.Id", programId));
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("b.Program.Id", programIdList));
            }
            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
            }
            if (!(Array.Exists(branchId, item => item == 0)))
            {
                criteria.CreateAlias("b.Branch", "br").Add(Restrictions.In("br.Id", branchId));
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Campus.Id", campusId));
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {
                criteria.Add(Restrictions.In("b.Days", batchDays));
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }
            if (!(Array.Exists(batch, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Id", batch));
            }
            var returnObj = (List<StudentProgram>)criteria.List<StudentProgram>();
            return returnObj;
        }

        public List<StudentProgram> LoadStudentProgramForUpdateStudentInfo(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, int? statusSync)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Status", Batch.EntityStatus.Active));
            criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            criteria.CreateAlias("b.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("b.Campus", "ca").Add(Restrictions.Eq("ca.Status", Campus.EntityStatus.Active));
            criteria.CreateCriteria("st.StudentAcademicInfos", "sai", JoinType.InnerJoin)
                .Add(Restrictions.Eq("sai.Status", StudentAcademicInfo.EntityStatus.Active));
            criteria.CreateCriteria("sai.StudentExam", "studentExm")
                .Add(Restrictions.Eq("studentExm.Status", StudentExam.EntityStatus.Active));
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("Program.Id", programIdList));
            }
            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
            }
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));
            if (!(Array.Exists(branchIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIds));
            }
            if (!(Array.Exists(campusIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Campus.Id", campusIds));
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")))
            {
                criteria.Add(Restrictions.In("b.Days", batchDays));
            }
            if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTimes)
                {
                    var batchArray = bt.Split(',');
                    var startTime = Convert.ToDateTime(batchArray[0]);
                    var endTime = Convert.ToDateTime(batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }
            if ((!Array.Exists(batchIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("Batch.Id", batchIds));
            }
            criteria.Add(Restrictions.Eq("studentExm.Id", studentExamId));
            if (statusSync != null)
            {
                criteria.Add(statusSync == 1 ? Restrictions.Eq("IsSync", true) : Restrictions.Eq("IsSync", false));
            }
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            List<StudentProgram> resultList = criteria.List<StudentProgram>().ToList();
            return resultList;
        }

        //        public IList<StudentListDto> LoadAuthorizedStudents(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string orderBy, string orderDir, int start, int length, string startDate, string endDate, int selectedStatus, bool isDisplayAcademicInfo, string prnNo, string name, string mobile)
        //        {
        //            string innerQuery = GetInnerQueryStudentList(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, prnNo, name, mobile);
        //            string query = "DECLARE @rowsperpage INT DECLARE @start INT SET @start = " + (start + 1) + " SET @rowsperpage = " + length +
        //                          " select * INTO #StudentSummary from ( SELECT row_number() OVER (ORDER BY " + orderBy + " " + orderDir + ") AS RowNum,  A.*, I.Name as Institute FROM(" + innerQuery + ") AS A" +
        //                         " left join Institute I on I.Id = A.InstituteId) as pagination";
        //            if (length > 0)
        //            {
        //                query += " where pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
        //            }
        //            if (isDisplayAcademicInfo)
        //            {
        //                query += @"
        //                Select * INTO #AcademicSummary
        //                FROM (
        //	                Select S.StudentId,
        //                    SE.Name as StudentExam,
        //	               CONCAT('Year: ',S.[Year],', Board Roll: ',S.BoradRoll,', Board: ', SB.Name) AS AcaddemicInfo
        //	                --S.[Year] AS ExamYear,S.BoradRoll,SE.Name AS ExamName, SB.Name AS BoardName
        //	                 from [dbo].[StudentAcademicInfo] as S
        //	                inner join  #StudentSummary AS SS ON SS.StudentId = S.StudentId
        //	                left join [dbo].[StudentExam] AS SE ON SE.Id = S.StudentExamId
        //	                left join [dbo].[StudentBoard] AS SB ON SB.Id = S.BoardId
        //                    Where S.Status =" + StudentAcademicInfo.EntityStatus.Active + @"	
        //                ) AS A
        //                select A.*
        //                    ,SAc.PSC,SAc.JSC,SAc.SSC,SAc.HSC,SAc.OLevel,SAc.ALevel
        //                from #StudentSummary AS A
        //                left join (
        //		               Select StudentId, MAX(PSC) AS PSC,MAX(JSC) AS JSC,MAX(SSC) AS SSC,MAX(HSC) AS HSC,MAX(OLevel) AS OLevel,MAX(ALevel) AS ALevel FROM (
        //			                Select StudentId,
        //			                case when StudentExam = 'PSC' then AcaddemicInfo else '' end as PSC,
        //			                case when StudentExam = 'JSC' then AcaddemicInfo else '' end as JSC,
        //			                case when StudentExam = 'SSC' then AcaddemicInfo else '' end as SSC,
        //			                case when StudentExam = 'HSC' then AcaddemicInfo else '' end as HSC,
        //			                case when StudentExam = 'O Level' then AcaddemicInfo else '' end as OLevel,
        //			                case when StudentExam = 'A Level' then AcaddemicInfo else '' end as ALevel
        //			                from #AcademicSummary AS A 
        //		                ) as A GROUP BY StudentId
        //	                ) AS SAc ON SAc.StudentId = A.StudentId
        //
        //                IF EXISTS(SELECT * FROM #StudentSummary) DROP TABLE #StudentSummary;
        //                IF EXISTS(SELECT * FROM #AcademicSummary) DROP TABLE #AcademicSummary;";
        //            }
        //            else
        //            {
        //                query += @"               
        //                select A.*
        //                    ,'' AS PSC
        //                    ,'' AS JSC
        //                    ,'' AS SSC
        //                    ,'' AS HSC
        //                    ,'' AS OLevel
        //                    ,'' AS ALevel 
        //                from #StudentSummary AS A
        //                IF EXISTS(SELECT * FROM #StudentSummary) DROP TABLE #StudentSummary;";
        //            }


        //            IQuery iQuery = Session.CreateSQLQuery(query);
        //            iQuery.SetTimeout(2700);
        //            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentListDto>());
        //            return iQuery.List<StudentListDto>().ToList();
        //        }
        public IList<StudentListDto> LoadAuthorizedStudents(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, List<long> branchIdList, List<long> campusIdList, List<string> batchDays, List<string> batchTime, List<long> batchIdList, List<long> courseIdList, int version, int gender, List<int> religionList, string orderBy, string orderDir, int start, int length, string startDate, string endDate, int selectedStatus, bool isDisplayAcademicInfo, string prnNo, string name, string mobile)
        {
            //string innerQuery = GetInnerQueryStudentList(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, prnNo, name, mobile);
            string innerQuery = GetAuthorizedStudentsListInnerQuery(authorizedProgramLists, authorizedBranchLists, sessionId, campusIdList, batchDays, batchTime, batchIdList, courseIdList, version, gender, religionList, startDate, endDate, selectedStatus, prnNo, name, mobile);
            string query = "DECLARE @rowsperpage INT DECLARE @start INT SET @start = " + (start + 1) + " SET @rowsperpage = " + length +
                          " select * INTO #StudentSummary from ( SELECT row_number() OVER (ORDER BY " + orderBy + " " + orderDir + ") AS RowNum,  A.*, I.Name as Institute FROM(" + innerQuery + ") AS A" +
                         " left join Institute I on I.Id = A.InstituteId) as pagination";
            if (length > 0)
            {
                query += " where pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            if (isDisplayAcademicInfo)
            {
                query += @"
                Select * INTO #AcademicSummary
                FROM (
	                Select S.StudentId,
                    SE.Name as StudentExam,
	               CONCAT('Year: ',S.[Year],', Board Roll: ',S.BoradRoll,', Board: ', SB.Name) AS AcaddemicInfo
	                --S.[Year] AS ExamYear,S.BoradRoll,SE.Name AS ExamName, SB.Name AS BoardName
	                 from [dbo].[StudentAcademicInfo] as S
	                inner join  #StudentSummary AS SS ON SS.StudentId = S.StudentId
	                left join [dbo].[StudentExam] AS SE ON SE.Id = S.StudentExamId
	                left join [dbo].[StudentBoard] AS SB ON SB.Id = S.BoardId
                    Where S.Status =" + StudentAcademicInfo.EntityStatus.Active + @"	
                ) AS A
                select A.*
                    ,SAc.PSC,SAc.JSC,SAc.SSC,SAc.HSC,SAc.OLevel,SAc.ALevel
                from #StudentSummary AS A
                left join (
		               Select StudentId, MAX(PSC) AS PSC,MAX(JSC) AS JSC,MAX(SSC) AS SSC,MAX(HSC) AS HSC,MAX(OLevel) AS OLevel,MAX(ALevel) AS ALevel FROM (
			                Select StudentId,
			                case when StudentExam = 'PSC' then AcaddemicInfo else '' end as PSC,
			                case when StudentExam = 'JSC' then AcaddemicInfo else '' end as JSC,
			                case when StudentExam = 'SSC' then AcaddemicInfo else '' end as SSC,
			                case when StudentExam = 'HSC' then AcaddemicInfo else '' end as HSC,
			                case when StudentExam = 'O Level' then AcaddemicInfo else '' end as OLevel,
			                case when StudentExam = 'A Level' then AcaddemicInfo else '' end as ALevel
			                from #AcademicSummary AS A 
		                ) as A GROUP BY StudentId
	                ) AS SAc ON SAc.StudentId = A.StudentId

                IF EXISTS(SELECT * FROM #StudentSummary) DROP TABLE #StudentSummary;
                IF EXISTS(SELECT * FROM #AcademicSummary) DROP TABLE #AcademicSummary;";
            }
            else
            {
                query += @"               
                select A.*
                    ,'' AS PSC
                    ,'' AS JSC
                    ,'' AS SSC
                    ,'' AS HSC
                    ,'' AS OLevel
                    ,'' AS ALevel 
                from #StudentSummary AS A
                IF EXISTS(SELECT * FROM #StudentSummary) DROP TABLE #StudentSummary;";
            }


            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentListDto>());
            return iQuery.List<StudentListDto>().ToList();
        }

        public IList<StudentListDto> LoadAuthorizedStudents(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, string searchKey, string[] informationViewList, int start, int length, string orderBy, string orderDir, string prnNo, string name, string mobile, List<int> versionOfStudyList = null, List<int> genderIdList = null, List<int> religionIdList = null)
        {
            var criteria = GetAuthorizedStudentsSearchResultQuery(authorizedProgramLists, authorizedBranchLists, programId, sessionId, searchKey, informationViewList, prnNo, name, mobile, versionOfStudyList, genderIdList, religionIdList);
            criteria.AddOrder(Order.Asc("PrnNo"));
            if (length > 0)
            {
                criteria.SetFirstResult(start);
                criteria.SetMaxResults(length);
            }
            criteria.SetProjection(Projections.ProjectionList()
                                     .Add(Projections.Property("PrnNo"), "PrnNo")
                                     .Add(Projections.Property("Id"), "Id")
                                     .Add(Projections.Property("Student.Id"), "StudentId")
                                     .Add(Projections.Property("b.Name"), "Batch")
                                     .Add(Projections.Property("br.Name"), "Branch")
                                     .Add(Projections.Property("inst.Name"), "Institute")
                                     .Add(Projections.Property("VersionOfStudy"), "VersionOfStudy")
                                     .Add(Projections.Property("s.NickName"), "NickName")
                                     .Add(Projections.Property("s.FullName"), "FullName")
                                     .Add(Projections.Property("s.Mobile"), "Mobile")
                                     .Add(Projections.Property("s.GuardiansMobile1"), "GuardiansMobile1")
                                     .Add(Projections.Property("s.GuardiansMobile2"), "GuardiansMobile2")
                                     .Add(Projections.Property("s.Email"), "Email")
                                     .Add(Projections.Property("s.RegistrationNo"), "RegistrationNo")
                                     .Add(Projections.Property("s.FatherName"), "FatherName")
                                     .Add(Projections.Property("s.Religion"), "Religion")
                                     .Add(Projections.Property("s.Gender"), "Gender")
                                     .Add(Projections.Property("Id"), "RowNum")
                                     .Add(Projections.Property("inst.Id"), "InstituteId")
                                     .Add(Projections.Property("dist.Name"), "District")
                                     );
            criteria.SetResultTransformer(Transformers.AliasToBean(typeof(StudentListDto)));
            return criteria.List<StudentListDto>().ToList();
        }

        public IList<StudentProgram> LoadStudentCompareWithBoard(int start, int length, List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long studentExamId, string year, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, int[] religions, int gender, int versionOfStudy, string prnNo, string fullName, string fatherName, string institute)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Status", Batch.EntityStatus.Active));
            criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            criteria.CreateAlias("b.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("b.Campus", "ca").Add(Restrictions.Eq("ca.Status", Campus.EntityStatus.Active));
            criteria.CreateCriteria("st.StudentAcademicInfos", "sai", JoinType.InnerJoin)
                .Add(Restrictions.Eq("sai.Status", StudentAcademicInfo.EntityStatus.Active));
            criteria.CreateCriteria("sai.StudentExam", "studentExm")
                .Add(Restrictions.Eq("studentExm.Status", StudentExam.EntityStatus.Active));
            criteria.CreateCriteria("sai.StudentAcademicInfoBaseData", "saiB", JoinType.InnerJoin);
            if (!String.IsNullOrEmpty(prnNo))
            {
                criteria.Add(Restrictions.Like("PrnNo", prnNo, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(fullName))
            {
                criteria.Add(Restrictions.Like("st.FullName", fullName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(fatherName))
            {
                criteria.Add(Restrictions.Like("st.FatherName", fatherName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(institute))
            {
                criteria.CreateCriteria("st.StudentPrograms", "sp");
                criteria.CreateAlias("sp.Institute", "spI");
                criteria.Add(Restrictions.Like("spI.Name", institute, MatchMode.Anywhere));
            }
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("Program.Id", programIdList));
            }
            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
            }
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));

            if (!(Array.Exists(branchIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIds));
            }

            if (!(Array.Exists(campusIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Campus.Id", campusIds));
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")))
            {
                criteria.Add(Restrictions.In("b.Days", batchDays));
            }
            if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTimes)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }
            if ((!Array.Exists(batchIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("Batch.Id", batchIds));
            }
            if (versionOfStudy != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("VersionOfStudy", versionOfStudy));
            }
            if (gender != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("st.Gender", gender));
            }
            if ((!Array.Exists(religions, item => item == 0)))
            {
                criteria.Add(Restrictions.In("st.Religion", religions));
            }
            criteria.Add(Restrictions.Eq("sai.Year", year));
            criteria.Add(Restrictions.Eq("studentExm.Id", studentExamId));
            criteria.AddOrder(Order.Asc("PrnNo"));
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            criteria.SetFirstResult(start).SetMaxResults(length);
            return criteria.List<StudentProgram>().ToList();
        }

        public IList<StudentProgram> LoadStudentProgramByCriteriaForClassEvaluation(int start, int length, long lectureId, long teacherId, long[] batchIdList, DateTime dateFrom, DateTime dateTo)
        {
            dateFrom = dateFrom.AddDays(1);
            dateTo = dateTo.AddDays(1);
            StudentClassAttendence scaAlias = null;
            StudentProgram spAlias = null;
            var classAttendanceDetailsList = Session.QueryOver<StudentClassAttendanceDetails>().JoinAlias(r => r.StudentClassAttendence, () => scaAlias).JoinAlias(r => r.StudentProgram, () => spAlias)
                  .Where(x => scaAlias.Lecture.Id == lectureId && scaAlias.Teacher.Id == teacherId);
            classAttendanceDetailsList =
                classAttendanceDetailsList.Where(
                    x => scaAlias.HeldDate >= dateFrom.Date && scaAlias.HeldDate <= dateTo.Date);
            classAttendanceDetailsList = classAttendanceDetailsList.Where(x => spAlias.Batch.Id.IsIn(batchIdList));
            var lectures = Session.QueryOver<CourseProgress>().Where(x => x.Batch.Id.IsIn(batchIdList)).Select(x => x.Lecture.Id).List<long>();
            classAttendanceDetailsList
                .Where(x => scaAlias.Lecture.Id.IsIn(lectures.Distinct().ToArray()));
            var studentPrograms = classAttendanceDetailsList.Where(x => x.StudentFeedback.IsIn(new[] { 1, 2, 3 })).Select(x => x.StudentProgram).List<StudentProgram>().Distinct().ToList();
            if (length != 0)
            {
                return studentPrograms.Skip(start).Take(length).ToList();
            }
            return studentPrograms;
        }

        public IList<StudentProgram> LoadStudentProgram(long organizationId, List<long> programIdList, SmsSettings smsSettings, bool isIn = true)
        {
            List<StudentProgram> returnResList = new List<StudentProgram>();
            var students = new List<StudentProgram>();
            if (smsSettings.SmsType.Name == "Birthday Reminder")
            {
                ICriteria criteria = Session.CreateCriteria<StudentProgram>();
                criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
                criteria.CreateAlias("p.Organization", "o").Add(Restrictions.Eq("o.Status", Program.EntityStatus.Active));
                criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("o.Id", organizationId));
                if (programIdList.Any())
                {
                    criteria.Add(isIn
                        ? Restrictions.In("p.Id", programIdList)
                        : Restrictions.Not(Restrictions.In("p.Id", programIdList)));
                }
                var monthProjection = Projections.SqlFunction("month", NHibernateUtil.Int32, Projections.Property("st.DateOfBirth"));
                var dayProjection = Projections.SqlFunction("day", NHibernateUtil.Int32, Projections.Property("st.DateOfBirth"));
                DateTime today = DateTime.Today;
                if (smsSettings.IsRepeat)
                {
                    var disjunction = Restrictions.Disjunction();
                    if (smsSettings.DayBefore != null)
                    {
                        for (int i = 0; i <= smsSettings.DayBefore; i++)
                        {
                            today = today.AddDays(1);
                            int month = today.Month;
                            int day = today.Day;
                            disjunction.Add(Expression.Eq(monthProjection, month) && Expression.Eq(dayProjection, day));
                        }
                        criteria.Add(disjunction);
                    }
                    else
                    {
                        int month = today.Month;
                        int day = today.Day;
                        disjunction.Add(Expression.Eq(monthProjection, month) && Expression.Eq(dayProjection, day));
                        criteria.Add(disjunction);
                    }
                }
                else
                {
                    today = today.AddDays((int)smsSettings.DayBefore);
                    int month = today.Month;
                    int day = today.Day;
                    criteria.Add(Expression.Eq(monthProjection, month));
                    criteria.Add(Expression.Eq(dayProjection, day));
                }
                students = criteria.List<StudentProgram>().ToList();
            }
            else if (smsSettings.SmsType.Name == "Due Reminder")
            {

                DateTime today = DateTime.Today;
                DateTime fromDate = today.AddDays(Convert.ToDouble(-smsSettings.DayAfter));
                DateTime toDate = today.AddDays(Convert.ToDouble(smsSettings.DayBefore));

                ICriteria criteria = Session.CreateCriteria<StudentProgram>();
                criteria.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
                criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
                criteria.CreateAlias("p.Organization", "o").Add(Restrictions.Eq("o.Status", Program.EntityStatus.Active));
                criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("o.Id", organizationId));
                if (programIdList.Any())
                {
                    criteria.Add(isIn
                        ? Restrictions.In("p.Id", programIdList)
                        : Restrictions.Not(Restrictions.In("p.Id", programIdList)));
                }
                string dateFilter = "AND a.NextReceivedDate>='" + fromDate + "' AND a.NextReceivedDate<='" + toDate + "'";
                string query = @"select a.StudentProgramId from (
                SELECT Id, StudentProgramId, NextReceivedDate,DueAmount, RANK() OVER(PARTITION BY StudentProgramId ORDER BY ReceivedDate DESC) as R 
                FROM StudentPayment) as a 
                where a.R=1 " + dateFilter + @"
                AND a.DueAmount>0";
                var studentProgramIds = new List<long>();
                IQuery iQuery = Session.CreateSQLQuery(query);
                studentProgramIds = iQuery.List<long>().ToList();
                ICriteria cri = Session.CreateCriteria<StudentProgram>();
                cri = (ICriteria)criteria.Clone();

                for (int index = 0; index < (studentProgramIds.Count / MaximumParameter) + 1; index++)
                {
                    criteria = (ICriteria)cri.Clone();
                    var count = studentProgramIds.ToList().Skip(index * MaximumParameter).Take(MaximumParameter);
                    IEnumerable<long> enumerable = count as IList<long> ?? count.ToList();
                    //criteria.Add(Restrictions.In("Id", new object[]{"a,s","df"}));
                    criteria.Add(Restrictions.In("Id", enumerable.ToArray()));
                    students = criteria.List<StudentProgram>().ToList();
                    returnResList.AddRange(students);
                }
                students = returnResList;
                //if (studentProgramIds.Any())
                //{
                //    criteria.Add(Restrictions.In("Id", studentProgramIds.ToArray()));
                //    students = criteria.List<StudentProgram>().ToList();
                //}
                //else
                //{
                //    students = new List<StudentProgram>();
                //}
            }
            return students;
        }

        //service block for list
        public List<StudentIdCardListDto> LoadStudentProgram(long programId, long sessionId, long[] branchId, List<long> authorizedProgramLists, List<long> branchIdList,
            long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] course, int printingStatus,
            int start, int length, string orderBy, string orderDir, int? distributionStatus, bool generateIdCard = false, List<ServiceBlock> serviceBlocks = null)
        {
            string innerQuery = GetInnerQueryStudentList(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, course, printingStatus, distributionStatus, generateIdCard, serviceBlocks);
            string query = "DECLARE @rowsperpage INT DECLARE @start INT SET @start = " + (start + 1) + " SET @rowsperpage = " + length +
                          " select * from ( SELECT row_number() OVER (ORDER BY " + orderBy + " " + orderDir + ") AS RowNum,  A.* FROM(" + innerQuery + ") AS A" +
                         " ) as pagination";
            if (length > 0)
            {
                query += " where pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2000);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentIdCardListDto>());
            return iQuery.List<StudentIdCardListDto>().ToList();
        }

        public List<StudentAdmissionSummeryReport> LoadBranchProgramWiseStudentAdmssion(List<long> authBranchIdList, List<long> authProgramIdList, string dateFrom, string dateTo, bool lastSession)
        {
            string queryString = "";
            if (lastSession)
            {
                queryString = @"SELECT   
                                p.ProgramId  AS ProgramId
                                , COUNT(sp.Id) AS StudentCount
                                , br.Id as BranchId  
                                FROM [dbo].[StudentProgram] AS sp
                                    LEFT JOIN (
                                        SELECT ProgramId, SessionId, Rank FROM (
                                        SELECT DISTINCT ProgramId, SessionId, RANK() OVER(Partition by programId ORDER BY s.[Rank] ASC ) as r, p.Rank 
                                        FROM ProgramBranchSession as pbs
                                        INNER JOIN [session] as s ON s.Id = pbs.SessionId
                                        INNER JOIN Program as p ON p.Id = pbs.ProgramId
                                        WHERE p.Status = 1 AND s.Status = 1
                                        ) as ps
                                   WHERE ps.r=1
                                   ) AS p ON p.ProgramId = sp.ProgramId              
                                INNER JOIN [dbo].[Batch] AS ba ON ba.Id = sp.BatchId AND ba.Status = 1
                                INNER JOIN [dbo].[Branch] AS br ON br.Id = ba.BranchId AND br.Status = 1
                                WHERE sp.Status = 1
                                AND ba.SessionId = p.SessionId 
                                AND br.Id IN (" + string.Join(",", authBranchIdList.ToArray()) + @")
                                AND p.ProgramId IN (" + string.Join(",", authProgramIdList.ToArray()) + @")
                                --AND sp.CreationDate >= '2016-01-01 00:00:00.000'
                                AND sp.CreationDate <= '" + dateTo + @"'
                                Group BY p.ProgramId, p.Rank, br.Id
                                Order by p.Rank ASC";
            }
            else
            {
                queryString = @"SELECT 
p.Id AS ProgramId 
, COUNT(sp.Id) AS StudentCount
, br.Id as BranchId  
FROM [dbo].[StudentProgram] AS sp   
INNER JOIN [dbo].[Program] AS p ON p.Id = sp.ProgramId AND p.Status = 1 
INNER JOIN [dbo].[Batch] AS ba ON ba.Id = sp.BatchId AND ba.Status = 1  
INNER JOIN [dbo].[Branch] AS br ON br.Id = ba.BranchId AND br.Status = 1 
WHERE sp.Status = 1  AND br.Id IN (" + string.Join(",", authBranchIdList.ToArray()) + @")  
AND p.Id IN (" + string.Join(",", authProgramIdList.ToArray()) + @")
AND sp.CreationDate >= '" + dateFrom + @"'  
AND sp.CreationDate <= '" + dateTo + @"'  
Group BY p.Id, p.Rank, br.Id  Order by p.Rank ASC ";
            }

            IQuery query = Session.CreateSQLQuery(queryString);
            query.SetResultTransformer(Transformers.AliasToBean<StudentAdmissionSummeryReport>());
            return query.List<StudentAdmissionSummeryReport>().ToList();
        }

        public IList<dynamic> DynamicList(IQuery query)
        {
            var result = query.SetResultTransformer(ExpandoObject)
                        .List<dynamic>();
            return result;
        }

        public IList<long> LoadStudentProgramWithImage(List<long> authorizedProgramIdList, List<long> authorizedBranchIdList, long[] programIds, long[] sessionIds, long[] branchIds, long[] campusIds, long[] batchNames, string[] batchDays, string[] batchTimes)
        {
            string query = GetStudentProgramWithImageListQuery(authorizedProgramIdList, authorizedBranchIdList,
                programIds, sessionIds, branchIds, campusIds, batchNames, batchDays, batchTimes);
            IQuery iQuery = Session.CreateSQLQuery(query);
            //iQuery.SetResultTransformer(Transformers.AliasToBean<MissingExistingImageDto>());
            return iQuery.List<long>().ToList();
        }

        public List<CourseAPIDto> LoadCourses(long id)
        {

            string query = @"Select distinct a.CourseId 
            INTO #StudentCourseDetails 
            From (
	            select sp.id, sc.SubjectId, sc.courseId
	            from studentProgram as sp
	            inner join studentCourseDetails as scd on sp.id = scd.studentProgramId
	            inner join courseSubject as sc on scd.courseSubjectId = sc.id
	            where scd.status!=-404 AND sp.Id in (" + id + @")  
            ) as a
            Select c.Id, c.Name from (
	            Select * from #StudentCourseDetails 
	            union all 
	            Select cc.CompCourseId from [dbo].[ComplementaryCourse] as cc		
	            Where cc.CourseId IN ( Select * from #StudentCourseDetails )
            ) as a
            inner Join [dbo].[Course] c on c.Id = a.CourseId 
            Where c.Status = 1
            IF EXISTS(SELECT * FROM #StudentCourseDetails) DROP TABLE #StudentCourseDetails;";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<CourseAPIDto>());
            var list = iQuery.List<CourseAPIDto>().ToList();
            return list;
        }

        public IList<ProgramAttendedSummary> LoadProgramSummaryData(string stdRollOrStdProRoll)
        {

            string query = @"DECLARE @prnOrMob as varchar(20);
                            SET @prnOrMob = '" + stdRollOrStdProRoll.Trim() + @"';
                            -- SET @prnOrMob = '1070166';
                            --SET @prnOrMob = '8801811999422';

                            SELECT s.NickName
                                , p.ShortName AS ProgramName
                                , sessn.Name AS SessionName
                                , br.Name AS BranchName
                                , s.RegistrationNo 
                                , sp.PrnNo AS PrNo
                                , s.Mobile AS MobileNumber
                                , o.ShortName AS Organization
                                , CASE WHEN sp.DueAmount > 0 THEN 'Payment Due'
	                               ELSE 'Payment Clear'
                                END AS PaymentStatus
                                , CAST(0 AS bit)  AS IsSelected
                                , s.Id AS StudentId
                                , 0 AS StudentVisitedId
                                --, sp.Id
                            FROM (
                                SELECT * 
                                FROM StudentProgram 
                                WHERE Id IN (	   
	                               SELECT id FROM StudentProgram WHERE PrnNo like @prnOrMob
	                               UNION ALL
	                               SELECT sp.Id FROM Student s
	                               LEFT JOIN StudentProgram sp ON s.Id = sp.StudentId  
	                               WHERE (s.Mobile like '%' + @prnOrMob OR s.RegistrationNo like @prnOrMob)
                                )
                            ) AS sp
                            INNER JOIN Student AS s ON s.Id = sp.StudentId
                            INNER JOIN Batch AS b ON b.Id = sp.BatchId
                            INNER JOIN Program AS p ON p.Id = b.ProgramId
                            INNER JOIN Session AS sessn ON sessn.Id = b.SessionId
                            INNER JOIN Branch AS br ON br.Id = b.BranchId
                            INNER JOIN Organization AS o ON o.Id = p.OrganizationId

                            UNION ALL

                            SELECT sv.NickName
                                , sv.Class AS ProgramName
                                , sv.Year AS SessionName
                                , 'NA' AS BranchName
                                , 'NA' AS RegistrationNo
                                , '' AS PrNo
                                , sv.Mobile AS MobileNumber
                                , CASE WHEN sv.AdmittedStudentProgramId > 0 THEN 
	                               (
		                              SELECT Organization.ShortName
		                              FROM StudentProgram, Program, Organization 
		                              WHERE StudentProgram.ProgramId = Program.Id 
			                             AND Program.OrganizationId = Organization.Id
			                             AND StudentProgram.id = sv.AdmittedStudentProgramId
	                               )
	                               ELSE '-'
                                END AS Organization
                                , CASE WHEN IsAdmitted = 1 THEN 'Visited'
	                               ELSE 'Admitted'
                                END AS PaymentStatus
                                , CAST(0 AS bit)  AS IsSelected
                                , 0 AS StudentId
                                , sv.Id AS StudentVisitedId
                                --, sv.* 
                            FROM StudentVisited AS sv
                            WHERE Mobile like '%' + @prnOrMob;";
            IQuery iQuery = Session.CreateSQLQuery(query);
            //iQuery.SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ProgramAttendedSummary>());
            var list = iQuery.List<ProgramAttendedSummary>().ToList();
            return list;
        }
        #endregion

        #region Others Function

        public int GetBatchStudentCount(Batch batchObj)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Not(Restrictions.Eq("Status", StudentProgram.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("Batch.Id", batchObj.Id));
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public bool IsAssignedStudentToThisBranch(long programId, long branchId, long sessionId)
        {
            bool studentExist = false;
            ICriteria criteria = Session.CreateCriteria<StudentProgram>();
            criteria.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.CreateAlias("Batch", "b");
            criteria.CreateAlias("b.Session", "s");
            criteria.CreateAlias("b.Branch", "br");
            criteria.Add(Restrictions.Eq("s.Id", sessionId));
            criteria.Add(Restrictions.Eq("br.Id", branchId));
            criteria.SetProjection(Projections.RowCount());
            var studentProgramCount = Convert.ToInt32(criteria.UniqueResult());
            if (studentProgramCount > 0)
            {
                studentExist = true;
            }
            return studentExist;
        }

        public long GetBoardResultStatus(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchIds
                 , long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, int? statusSync)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Status", Batch.EntityStatus.Active));
            criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            criteria.CreateAlias("b.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("b.Campus", "ca").Add(Restrictions.Eq("ca.Status", Campus.EntityStatus.Active));
            criteria.CreateCriteria("st.StudentAcademicInfos", "sai", JoinType.InnerJoin)
                .Add(Restrictions.Eq("sai.Status", StudentAcademicInfo.EntityStatus.Active));
            criteria.CreateCriteria("sai.StudentExam", "studentExm")
                .Add(Restrictions.Eq("studentExm.Status", StudentExam.EntityStatus.Active));

            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("Program.Id", programIdList));
            }
            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
            }
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));
            if (!(Array.Exists(branchIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIds));
            }
            if (!(Array.Exists(campusIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Campus.Id", campusIds));
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")))
            {
                criteria.Add(Restrictions.In("b.Days", batchDays));
            }
            if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTimes)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }
            if ((!Array.Exists(batchIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("Batch.Id", batchIds));
            }
            criteria.Add(Restrictions.Eq("studentExm.Id", studentExamId));
            if (statusSync != null)
            {
                criteria.Add(statusSync == 1 ? Restrictions.Eq("IsSync", true) : Restrictions.Eq("IsSync", false));
            }
            criteria.SetResultTransformer(Transformers.AliasToBean(typeof(Student)));
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt64(criteria.UniqueResult());
        }

        //public int GetAuthorizedStudentsCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus, string prnNo, string name, string mobile)
        //{
        //    string innerQuery = GetInnerQueryStudentList(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, prnNo, name, mobile);
        //    string query = "SELECT count(A.Id) as total FROM(" + innerQuery + ") as A";
        //    IQuery iQuery = Session.CreateSQLQuery(query);
        //    iQuery.SetTimeout(2000);
        //    return Convert.ToInt32(iQuery.UniqueResult());
        //}

        public int GetAuthorizedStudentsCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, List<long> campusIdList, List<string> batchDays, List<string> batchTime, List<long> batchIdList, List<long> courseIdList, int version, int gender, List<int> religionList, string startDate, string endDate, int selectedStatus, string prnNo, string name, string mobile)
        {
            //string innerQuery = GetInnerQueryStudentList(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, prnNo, name, mobile);
            string innerQuery = GetAuthorizedStudentsListInnerQuery(authorizedProgramLists, authorizedBranchLists, sessionId, campusIdList, batchDays, batchTime, batchIdList, courseIdList, version, gender, religionList, startDate, endDate, selectedStatus, prnNo, name, mobile);
            string query = "SELECT count(A.Id) as total FROM(" + innerQuery + ") as A";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2000);
            return Convert.ToInt32(iQuery.UniqueResult());
        }

        public int GetAuthorizedStudentsCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, string searchKey, string[] informationViewList, string prnNo, string name, string mobile, List<int> versionOfStudyList = null, List<int> genderIdList = null, List<int> religionIdList = null)
        {
            var criteria = GetAuthorizedStudentsSearchResultQuery(authorizedProgramLists, authorizedBranchLists, programId, sessionId, searchKey, informationViewList, prnNo, name, mobile, versionOfStudyList, genderIdList, religionIdList);
            criteria.SetProjection(Projections.CountDistinct("Id"));
            return (int)criteria.UniqueResult();
        }

        public int GetStudentsCountForPreviousStudentReport(List<long> authorizedProgramLists, List<long> authorizedBranchListsTo, long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo, int studentType)
        {
            string queryFrom = "";
            string queryTo = "";
            string query = "";

            if (studentType == 1)
            {
                queryFrom += " where exists(select  1 from studentprogram spo inner join" +
                            " batch b on spo.batchid=b.id inner join " +
                            "branch br on br.id=b.branchid inner join " +
                            "session s on b.sessionid=s.id and spo.status=1 and b.status=1 and br.status=1 and s.status=1 and spo.studentid=sp.studentid and spo.programId=" + programIdFrom + " and s.id=" + sessionIdFrom;
                if (!branchIdFrom.Contains(SelectionType.SelelectAll))
                {
                    queryFrom += " and br.id in(" + string.Join(",", branchIdFrom) + ")";
                }
                queryTo += "     select  count(*) from studentprogram sp inner join " +
                                "batch b on sp.batchid=b.id inner join " +
                                "branch br on br.id=b.branchid inner join " +
                                "session s on b.sessionid=s.id and sp.status=1 and b.status=1 and br.status=1 and s.status=1 and s.status=1  and sp.programId=" + programIdTo + " and s.id=" + sessionIdTo;
                if (authorizedProgramLists != null)
                {
                    queryTo += " and sp.programId in(" + string.Join(",", authorizedProgramLists) + ")";
                }
                if (authorizedBranchListsTo != null)
                {
                    queryTo += " and br.id in(" + string.Join(",", authorizedBranchListsTo) + ")";
                }
                if (!branchIdTo.Contains(SelectionType.SelelectAll))
                {
                    queryTo += " and br.id in(" + string.Join(",", branchIdTo) + ")";
                }
                queryFrom += " )";
                query = queryTo + queryFrom;
            }
            else
            {
                queryFrom += "select  count(*) from studentprogram spo inner join" +
                " batch b on spo.batchid=b.id inner join " +
                "branch br on br.id=b.branchid inner join " +
                "session s on b.sessionid=s.id and spo.status=1 and b.status=1 and br.status=1 and s.status=1 and spo.programId=" + programIdFrom + " and s.id=" + sessionIdFrom;
                if (!branchIdFrom.Contains(SelectionType.SelelectAll))
                {
                    queryFrom += " and br.id in(" + string.Join(",", branchIdFrom) + ")";
                }
                queryTo += " where not exists(select  1 from studentprogram sp inner join " +
                                "batch b on sp.batchid=b.id inner join " +
                                "branch br on br.id=b.branchid inner join " +
                                "session s on b.sessionid=s.id and sp.status=1 and b.status=1 and br.status=1 and s.status=1 and s.status=1 and spo.studentid=sp.studentId and  sp.programId=" + programIdTo + " and s.id=" + sessionIdTo;
                if (authorizedProgramLists != null)
                {
                    queryTo += " and sp.programId in(" + string.Join(",", authorizedProgramLists) + ")";
                }
                if (authorizedBranchListsTo != null)
                {
                    queryTo += " and br.id in(" + string.Join(",", authorizedBranchListsTo) + ")";
                }
                if (!branchIdTo.Contains(SelectionType.SelelectAll))
                {
                    queryTo += " and br.id in(" + string.Join(",", branchIdTo) + ")";
                }
                queryTo += " )";
                query = queryFrom + queryTo;
            }
            query += "";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int GetStudentProgramCountByCriteriaForAttendance(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>();

            criteria.CreateAlias("Batch", "b", JoinType.LeftOuterJoin);
            criteria.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            if (authorizedProgramLists != null)
            {
                criteria.Add(Restrictions.In("Program.Id", authorizedProgramLists));
            }
            if (authorizedBranchLists != null)
            {
                criteria.Add(Restrictions.In("b.Branch.Id", authorizedBranchLists));
            }
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));
            if (!(Array.Exists(branchId, item => item == 0)))
            {
                criteria.CreateAlias("b.Branch", "br").Add(Restrictions.In("br.Id", branchId));
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Campus.Id", campusId));
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {
                criteria.Add(Restrictions.In("b.Days", batchDays));
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }
            if (!(Array.Exists(batchList, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Id", batchList));
            }
            if (!(Array.Exists(paymentStatus, item => item == 0)))
            {
                if (!paymentStatus.Contains(3))
                {
                    if (paymentStatus.Contains(1) && !paymentStatus.Contains(2))
                    {
                        criteria.Add(Restrictions.Eq("DueAmount", Convert.ToDecimal(0)));
                    }
                    if (paymentStatus.Contains(2) && !paymentStatus.Contains(1))
                    {
                        criteria.Add(Restrictions.Gt("DueAmount", Convert.ToDecimal(0)));
                    }
                    if (paymentStatus.Contains(1) && paymentStatus.Contains(2))
                    {
                        criteria.Add(Restrictions.Disjunction()
                            .Add(Restrictions.Eq("DueAmount", Convert.ToDecimal(0)))
                            .Add(Restrictions.Gt("DueAmount", Convert.ToDecimal(0))));
                    }
                }
            }
            criteria.SetProjection(Projections.RowCount());
            var studentProgramCount = Convert.ToInt32(criteria.UniqueResult());
            return studentProgramCount;
        }

        public int LoadStudentProgramCountForClassEvaluation(long lectureId, long teacherId, long[] batchIdList, DateTime dateFrom, DateTime dateTo)
        {
            dateFrom = dateFrom.AddDays(1);
            dateTo = dateTo.AddDays(1);
            StudentClassAttendence scaAlias = null;
            StudentProgram spAlias = null;
            var classAttendanceDetailsList = Session.QueryOver<StudentClassAttendanceDetails>().JoinAlias(r => r.StudentClassAttendence, () => scaAlias).JoinAlias(r => r.StudentProgram, () => spAlias)
                  .Where(x => scaAlias.Lecture.Id == lectureId && scaAlias.Teacher.Id == teacherId);
            classAttendanceDetailsList =
                classAttendanceDetailsList.Where(
                    x => scaAlias.HeldDate >= dateFrom.Date && scaAlias.HeldDate <= dateTo.Date);
            classAttendanceDetailsList = classAttendanceDetailsList.Where(x => spAlias.Batch.Id.IsIn(batchIdList));
            var lectures = Session.QueryOver<CourseProgress>().Where(x => x.Batch.Id.IsIn(batchIdList)).Select(x => x.Lecture.Id).List<long>();
            classAttendanceDetailsList
                .Where(x => scaAlias.Lecture.Id.IsIn(lectures.Distinct().ToArray()));
            var studentPrograms = classAttendanceDetailsList.Where(x => x.StudentFeedback.IsIn(new[] { 1, 2, 3 })).Select(x => x.StudentProgram).List<StudentProgram>().Distinct().ToList();

            return studentPrograms.Count;
        }

        public int GetAdmittedStudentCountByBranchProgramSession(long branchId, long programId, long sessionId)
        {
            return Session.Query<StudentProgram>().Count(x => x.Status == StudentProgram.EntityStatus.Active && x.Batch.Branch.Id == branchId && x.Program.Id == programId && x.Batch.Session.Id == sessionId);
        }

        public int GetStudentProgramCount(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds, long[] courseIds, string lectureIds)
        {
            var query = @"with studentProgramCTEE as ( select
                           distinct Id,
                           prnNo,
                           batchid 
                        from
                           studentprogram 
                        where
                           id in(select sp.id from studentprogram sp inner join  batch b on sp.batchid=b.id where

                        sp.programid={0} and sp.programid in({1}) and b.sessionid={2} and b.branchid in({3}) and b.Id in ({6})";
            if (!paymentStatus.Contains(0))
            {
                if (paymentStatus.Contains(1))
                {
                    query += " and dueamount=0";
                }
                if (paymentStatus.Contains(2))
                {
                    query += " and dueamount>0";
                }
            }

            query += @" )) select
                       count(x.id)
                    from
                       (select
                          swcp.prnNo,
                          swcp.id,
                          count(distinct cp.lectureid) as RegisteredAbsent 
                       from
                          studentProgramCTEE swcp 
                       left join
                          courseprogress cp 
                             on cp.batchid =swcp.batchid
                    --and cp.batchid  =(select batchid from studentprogram where id=swcp.BatchId)  
                             and cp.heldDate>='{4}' 
                             and cp.heldDate<'{5}' 
                             and cp.lectureid not in (
                                select
                                   sca.lectureid 
                             from
                                [dbo].[StudentClassAttendanceDetails] scad 
                             inner join
                                [dbo].[StudentClassAttendence] sca 
                                   on scad.[StudentClassAttendanceId]=sca.id 
                                   and scad.studentprogramid=swcp.id 
                             where
                                sca.heldDate>='{4}'
                                and sca.heldDate<'{5}'
                          ) 
                          and cp.coursesubjectid in (
                    select courseSubjectid from studentcoursedetails scd where scd.studentprogramid=swcp.id  and status=1        
                    ) ";
            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and cp.lectureId in(" + lectureIds + ")";
            }
            query += @" group by
                              swcp.id,
                              swcp.prnNo) x   
                        left join
                                    (
                                       select
                                 swcp.prnNo,
                                 swcp.id,
                                 count(distinct cp.lectureid) as RegisteredAbsent2      
                              from
                                 studentProgramCTEE swcp      
                              left join
                                 courseprogress cp            
                                    on cp.batchid =swcp.batchid
                    
                                    and cp.heldDate>='{4}'           
                                    and cp.heldDate<'{5}'         
                                    and cp.lectureid not in (
                                       select
                                          sca.lectureid            
                                    from
                                       [dbo].[StudentClassAttendanceDetails] scad            
                                    inner join
                                       [dbo].[StudentClassAttendence] sca                  
                                          on scad.[StudentClassAttendanceId]=sca.id                  
                                          and scad.studentprogramid=swcp.id            
                                    where
                                       sca.heldDate>='{4}'              
                                       and sca.heldDate<'{5}'        
                                 )         
                                 and cp.courseSubjectid is null";

            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and cp.lectureId in(" + lectureIds + ")";
            }
            query += @" group by
                            swcp.id,
                            swcp.prnNo
			                   )zyx            
                                  on x.id=zyx.id
                 --complementary absent

				                  left join
                            (
                               select
                         swcp.prnNo,
                         swcp.id,
                         count(distinct cp.lectureid) as complementaryAbsent   
                      from
                         studentProgramCTEE swcp      
                      left join
                         courseprogress cp            
                            on cp.batchid = swcp.batchid            
                            and cp.heldDate>='{4}'            
                            and cp.heldDate<'{5}'            
                            and cp.lectureid not in (
                               select
                                  sca.lectureid            
                            from
                               [dbo].[StudentClassAttendanceDetails] scad            
                            inner join
                               [dbo].[StudentClassAttendence] sca                  
                                  on scad.[StudentClassAttendanceId]=sca.id                  
                                  and scad.studentprogramid=swcp.id            
                            where
                               sca.heldDate>='{4}'              
                               and sca.heldDate<'{5}'        
                         )         
                         and cp.courseid in (
                            select compc.CompCourseId from studentcoursedetails scd inner join CourseSubject cs on scd.CourseSubjectId=cs.Id inner join Course c on cs.CourseId=c.Id 
			                left join ComplementaryCourse compc on c.Id=compc.CourseId
			                where scd.studentprogramid=swcp.id  and scd.status=1 and cs.Status=1 and c.Status=1 and compc.Status=1
			                 ) ";

            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += "and cp.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and cp.lectureId in(" + lectureIds + ")";
            }
            query += @" group by
            swcp.id,
            swcp.prnNo
			   )zyxx            
                   on x.id=zyxx.id
                    --complementary absent
                    left join
                          (
                             select
                                scad.studentprogramid,
                                count(*) as RegisteredPresent 
                             from
                                [dbo].[StudentClassAttendanceDetails] scad 
                             inner join
                                [dbo].[StudentClassAttendence] sca 
                                   on scad.[StudentClassAttendanceId]=sca.id ";
            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += " inner join lecture l on l.id=sca.lectureid inner join lecturesettings ls on l.lecturesettingsid=ls.id and ls.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            query += @" where
                                scad.isregistered=1 
                                and sca.heldDate>='{4}'
                                and sca.heldDate<'{5}' 
                                and exists(select 1 from CourseProgress where LectureId=sca.LectureId";
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and lectureId in(" + lectureIds + ")";
            }
            query += @") 
                             group by
                                scad.studentprogramid
                          )y 
                             on x.id=y.studentprogramid 
                       left join
                          (
                             select
                                scad.studentprogramid,
                                count(*) as UnregisteredPresent 
                             from
                                [dbo].[StudentClassAttendanceDetails] scad 
                             inner join
                                [dbo].[StudentClassAttendence] sca 
                                   on scad.[StudentClassAttendanceId]=sca.id ";
            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += " inner join lecture l on l.id=sca.lectureid inner join lecturesettings ls on l.lecturesettingsid=ls.id and ls.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            query += @" where
                                scad.isregistered=0 
                                and sca.heldDate>='{4}'
                                and sca.heldDate<'{5}'
                                and exists(select 1 from CourseProgress where LectureId=sca.LectureId";
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and lectureId in(" + lectureIds + ")";
            }
            query += @") group by
                                scad.studentprogramid
                          )z 
                             on x.id=z.studentprogramid 
                       left join
                          (
                             select
                                sp.id spid,
                                COALESCE(classHeld,
                                0) as LectureHeld 
                             from
                                studentProgramCTEE sp 
                             left join
                                (
                                   select
                                      batchid,
                                      count(*) classHeld 
                                   from
                                      courseprogress cp  ";
            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += " inner join lecture l on l.id=cp.lectureid inner join lecturesettings ls on l.lecturesettingsid=ls.id and ls.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            query += @" where
                                      cp.heldDate>='{4}' 
                                      and cp.heldDate<'{5}' 
                    ";
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and lectureId in(" + lectureIds + ")";
            }
            query += @"
               group by
                  batchid
            )x 
               on sp.batchid=x.batchid 
            )p 
               on p.spid=x.id where 1=1";

            if (!paymentStatus.Contains(0))
            {
                if (paymentStatus.Contains(3))
                {
                    query += " and unregisteredpresent>0";
                }

            }
            if (attendanceStatus != 0)
            {
                if (attendanceStatus == 1)
                {
                    query += " and registeredpresent>0";
                }
                if (attendanceStatus == 2)
                {
                    query += " and registeredabsent>0";
                }
                if (attendanceStatus == 3)
                {
                    query += " and unregisteredpresent>0";
                }
            }
            string endQuery = String.Format(query, programId, string.Join(", ", programIdList), sessionId, string.Join(", ", branchIdList), startDate, endDate.AddDays(1), batchIds);
            IQuery iQuery = Session.CreateSQLQuery(endQuery);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();

            return count;
        }

        //for service block
        public int GetAuthorizedStudentProgramCount(long programId, long sessionId, long[] branchId, List<long> authorizedProgramLists, List<long> branchIdList, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] course, int printingStatus, int? distributionStatus, List<ServiceBlock> serviceBlocks = null)
        {
            string innerQuery = GetInnerQueryStudentList(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, course, printingStatus, distributionStatus, false, serviceBlocks);
            string query = "SELECT count(A.Id) as total FROM(" + innerQuery + ") as A";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2000);
            return Convert.ToInt32(iQuery.UniqueResult());
        }

        public int ImageCount(List<long> branchIdList, List<long> programIdList, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList, bool isMissingImage)
        {
            string innerQuery = GetInnerQueryStudentImageCount(branchIdList, programIdList, programId, sessionId, branchId, campusId, batchDaysList, batchTimeList, batchList, isMissingImage);
            string query = "SELECT count(A.Id) as total FROM(" + innerQuery + ") as A";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2000);
            return Convert.ToInt32(iQuery.UniqueResult());
        }

        public int LoadImageReportsRowCount(List<long> branchIdList, List<long> programIdList, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchList, string[] batchDaysList, string[] batchTimeList, bool isMissingImage)
        {
            string innerQuery = GetInnerQueryStudentMissingImage(branchIdList, programIdList, programId, sessionId, branchId, campusId, batchDaysList, batchTimeList, batchList, isMissingImage);
            string query = innerQuery + " select count(distinct batchid) from missingImageCte";
            IQuery iQuery = Session.CreateSQLQuery(query);
            int count = iQuery.UniqueResult<int>();
            return count;
        }

        public bool HasStudentProgram(long studentExamId, long boardId, string year, string boradRoll, string registrationNumber, long programId, long sessionId)
        {

            var subCriteria = DetachedCriteria.For<StudentProgram>();
            subCriteria.CreateAlias("Batch","b");
            subCriteria.SetProjection(Projections.Property("Student.Id"));
            subCriteria.Add(Restrictions.Eq("Status",StudentProgram.EntityStatus.Active));
            subCriteria.Add(Restrictions.Eq("Program.Id", programId));
            subCriteria.Add(Restrictions.Eq("b.Session.Id", sessionId));

            ICriteria criteria = Session.CreateCriteria<StudentAcademicInfo>().Add(Restrictions.Not(Restrictions.Eq("Status", StudentAcademicInfo.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("StudentExam.Id", studentExamId));
            criteria.Add(Restrictions.Eq("StudentBoard.Id", boardId));
            criteria.Add(Restrictions.Eq("Year", year));
            criteria.Add(Restrictions.Eq("BoradRoll", boradRoll));
            criteria.Add(Restrictions.Eq("RegistrationNumber", registrationNumber));
            
            criteria.Add(Subqueries.PropertyIn("Student.Id", subCriteria));

            criteria.SetProjection(Projections.RowCount());
            var count = Convert.ToInt32(criteria.UniqueResult());

            if(count>0)
                return true;

            return false;            
        }

        #endregion

        #region Helper Function

        private string GetStudentImageListQuery(List<long> authorizedProgramLists, List<long> authorizedBranchListsTo, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays, string[] batchTime, string programRoll = "", string nickName = "", string mobileNumber = "")
        {
            string querySec = "";
            string filterQuery = "";
            string innerJoinQuery = "";
            string innerJoinSelectQuery = ", null AS Image, null AS [Upload By], null AS [Updated By]";
            if (!String.IsNullOrEmpty(programRoll))
            {
                filterQuery += "AND sp.[PrnNo]='" + programRoll + "' ";
            }
            if (authorizedProgramLists != null)
            {
                filterQuery += " and sp.programId in(" + string.Join(",", authorizedProgramLists) + ")";
            }
            if (authorizedBranchListsTo != null)
            {
                filterQuery += " and br.id in(" + string.Join(",", authorizedBranchListsTo) + ")";
            }
            if (!(Array.Exists(branchId, item => item == 0)))
            {
                filterQuery += " and b.BranchId in(" + string.Join(",", branchId) + ") ";
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                filterQuery += " and b.CampusId in(" + string.Join(",", campusId) + ") ";
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {
                filterQuery += " and b.Days in ('" + string.Join("','", batchDays) + "')";
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                filterQuery += " and (" + btQ + ")";
            }
            if (!(Array.Exists(batchName, item => item == 0)))
            {
                filterQuery += " and b.Id in (" + string.Join(",", batchName) + ")";
            }
            if (!String.IsNullOrEmpty(nickName))
            {
                querySec += " AND std.[NickName] like '" + nickName + "%' ";
            }
            if (!String.IsNullOrEmpty(mobileNumber))
            {
                querySec += " AND std.[Mobile] like '%" + mobileNumber + "' ";
            }
            if (Convert.ToInt32(statusVal) == 1)//Active
            {
                innerJoinQuery = " INNER JOIN StudentProgramImage as spimg on fq.SpId = spimg.StudentProgramId and spimg.Status = 1 ";
                innerJoinSelectQuery = ", spimg.Id AS Image, spimg.CreateBy AS [Upload By], spimg.ModifyBy AS [Updated By]";
            }

            string query = @"SELECT 
                                fq.*
                                , ins.Name AS [INSTITUTE]
                                " + innerJoinSelectQuery + @"
                              FROM (
                                SELECT inQ.PrnNo as[PROGRAM ROLL]
                                , std.NickName as [NICK NAME]
                                , std.FullName as [FULL NAME]
                                , std.Mobile as [MOBILE NUMBER(PERSONAL)]
                                , std.GuardiansMobile1 as [MOBILE NUMBER(FATHER)]
                                , std.GuardiansMobile2 as [MOBILE NUMBER(MOTHER)]
                                , inQ.bfName as [BRANCH]
                                , inQ.[CAMPUS NAME]
                                , inQ.BatchId AS BatchId
                                , inQ.[BATCH DAYS]
                                , inQ.[BATCH START TIME]
                                , inQ.[BATCH NAME]
                                , inQ.StudentId 
                                , inQ.InstituteId
                                , inQ.[STDPROGRAM ID] AS SpId 
                                , inQ.BatchTime AS BatchTime 
                                from ( SELECT sp.Id as [STDPROGRAM ID]
                                    , sp.PrnNo
                                    , sp.StudentId
                                    , br.Name AS bfName
                                    , br.id AS [fbrIds]
                                    , cam.Name as [CAMPUS NAME]
                                    , b.Days as [BATCH DAYS]
                                    , b.StartTime as [BATCH START TIME]
                                    , b.Name as [BATCH NAME]
                                    , sp.InstituteId  
                                    , b.Id AS BatchId
                                    , Concat(
	                                    Left(RIGHT(CONVERT(VARCHAR, b.StartTime, 100), 8),6)
	                                    ,' '
	                                    ,RIGHT(CONVERT(VARCHAR, b.StartTime, 100), 2)
	                                    ,' To' 
	                                    ,Left(RIGHT(CONVERT(VARCHAR, b.EndTime, 100), 8),6)
	                                    ,' '
	                                    ,RIGHT(CONVERT(VARCHAR, b.StartTime, 100), 2)
	                                    ) AS BatchTime
                                    FROM [StudentProgram] AS sp 
                                    INNER JOIN [Program] AS p ON sp.[ProgramId]=p.Id 
                                    INNER JOIN [Batch] AS b ON sp.[BatchId]= b.Id 
                                    INNER JOIN [Session] AS s ON b.[SessionId]=s.Id 
                                    INNER JOIN [Branch] AS br ON b.[BranchId]= br.Id 
                                    INNER JOIN [Campus] AS cam ON cam.Id = b.CampusId AND sp.status=1 AND b.status=1 AND br.status=1 AND s.status=1 AND sp.[IsImage]=" + Convert.ToInt32(statusVal) + " AND cam.status=1  " + " AND p.Id=" + programId + " AND s.Id=" + sessionId + @" 
                                        " + filterQuery + @"
                                    ) as inQ 
                                    LEFT JOIN [Student] as std on std.Id= inQ.StudentId 
                                    where 1=1 
                                    " + querySec + @" 
                                ) as fq 
                                LEFT JOIN [Institute] as ins on fq.InstituteId = ins.Id 
                                " + innerJoinQuery + @"
                                ";
            return query;
        }

        private string GetAuthorizedStudentsListInnerQuery(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, List<long> campusIdList, List<string> batchDays, List<string> batchTime, List<long> batchIdList, List<long> courseIdList, int version, int gender, List<int> religionList, string startDate, string endDate, int selectedStatus, string prnNo, string name, string mobile)
        {

            string programFilter = "";
            string branchFilter = "";
            string sessionFilter = "";
            string campusFilter = "";
            string batchDayFilter = "";
            string batchTimeFilter = "";
            string batchFilter = "";
            string courseFilter = "";
            string courseInnerJoinQueryFilter = "";
            string courseWhereFilter = "";
            string versionFilter = "";
            string genderFilter = "";
            string religionFilter = "";
            string startDateFilter = "";
            string endDateFilter = "";
            string statusFilter = "";
            string prnNoFilter = "";
            string nameFilter = "";
            string mobileFilter = "";

            #region Filter Part

            if (authorizedProgramLists != null && authorizedProgramLists.Any() && !authorizedProgramLists.Contains(SelectionType.SelelectAll))
                programFilter = @" AND sp.ProgramId IN (" + string.Join(",", authorizedProgramLists) + @")  ";

            if (authorizedBranchLists != null && authorizedBranchLists.Any() && !authorizedBranchLists.Contains(SelectionType.SelelectAll))
                branchFilter = @" AND br.Id IN (" + string.Join(",", authorizedBranchLists) + @")  ";

            if (sessionId != SelectionType.SelelectAll)
                sessionFilter = @" AND b.SessionId = " + sessionId + @"  ";

            if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
                campusFilter = @" AND b.CampusId IN (" + string.Join(",", campusIdList) + @")  ";

            if (batchDays != null && !batchDays.Contains(SelectionType.SelelectAll.ToString()) && !batchDays.Contains(""))
                batchDayFilter += " AND b.Days IN ('" + string.Join("','", batchDays) + @"') ";

            if (batchTime != null && !batchTime.Contains(SelectionType.SelelectAll.ToString()) && !batchTime.Contains(""))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' AND b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " OR (b.StartTime >= '" + startTime + "' AND b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                batchTimeFilter = " AND (" + btQ + ")";
            }

            if (batchIdList != null && batchIdList.Any() && !batchIdList.Contains(SelectionType.SelelectAll))
                batchFilter = @" AND b.Id IN (" + string.Join(",", batchIdList) + @")  ";

            if (version != SelectionType.SelelectAll)
                versionFilter = " AND sp.VersionOfStudy = " + version + @" ";

            if (gender != SelectionType.SelelectAll)
                genderFilter = " AND st.Gender = " + gender + @" ";

            if (religionList != null && !religionList.Contains(SelectionType.SelelectAll))
                religionFilter = " and st.Religion in (" + string.Join(",", religionList) + ")";

            if (!String.IsNullOrEmpty(startDate))
            {
                DateTime dFrom = Convert.ToDateTime(startDate + " 00:00:00.000");
                startDateFilter = " AND sp.CreationDate >= '" + dFrom + @"' ";
            }

            if (!String.IsNullOrEmpty(endDate))
            {
                DateTime dTo = Convert.ToDateTime(endDate + " 23:59:59.000");
                endDateFilter = " AND sp.CreationDate <= '" + dTo + @"' ";
            }

            switch (selectedStatus)
            {
                case 1:
                    statusFilter = " AND sp.Status = " + StudentProgram.EntityStatus.Active + @" ";
                    break;
                case 2:
                    statusFilter = " AND sp.Status = " + StudentProgram.EntityStatus.Inactive + @" ";
                    break;
                default:
                    statusFilter = " AND sp.Status != " + StudentProgram.EntityStatus.Delete + @" ";
                    break;
            }

            if (!String.IsNullOrEmpty(prnNo))
                prnNoFilter = " AND sp.PrnNo LIKE '%" + prnNo + @"%' ";

            if (!String.IsNullOrEmpty(name))
                nameFilter = " AND st.NickName LIKE '%" + name + @"%' ";

            if (!String.IsNullOrEmpty(mobile))
                mobileFilter = " AND st.Mobile LIKE '%" + mobile + @"%' ";

            if (courseIdList != null && courseIdList.Any() && !courseIdList.Contains(SelectionType.SelelectAll))
            {
                courseFilter = " AND cs.courseId IN (" + string.Join(",", courseIdList) + @")  ";
                courseInnerJoinQueryFilter = @" INNER JOIN ( 
	SELECT scd.StudentProgramId, count(scd.Id) AS c  FROM StudentCourseDetails scd 
	INNER JOIN CourseSubject cs ON scd.CourseSubjectId = cs.Id 
	WHERE scd.status = " + StudentCourseDetail.EntityStatus.Active + @"
	AND cs.Status = " + CourseSubject.EntityStatus.Active + @"
	" + courseFilter + @"
	GROUP BY scd.StudentProgramId
) as scd ON scd.StudentProgramId = sp.Id";
                courseWhereFilter = @" AND isnull(scd.c,0) > 0 ";
            }
            #endregion

            string query = @"SELECT 
sp.Id
, sp.PrnNo
, sp.VersionOfStudy
, sp.StudentId
, sp.InstituteId
, b.Name AS Batch
, b.Days AS BatchDays
, CONCAT(FORMAT(b.StartTime, 'hh:mm tt '),'To', FORMAT(b.EndTime, ' hh:mm tt')) AS BatchTime
, br.Name AS Branch
, c.Name AS Campus
, st.NickName
, st.FullName
, st.Mobile
, st.GuardiansMobile1
, st.GuardiansMobile2,st.Gender
, st.Religion,st.Email
, st.RegistrationNo
, st.FatherName 
from StudentProgram sp 
INNER JOIN Student st on st.Id = sp.StudentId 
INNER JOIN Batch b on b.Id = sp.BatchId 
INNER JOIN Branch br on br.Id = b.BranchId 
INNER JOIN [Session] s on s.Id = b.SessionId 
LEFT JOIN (
	SELECT Id
	,Name 
	FROM Campus
	WHERE Status = 1
) AS c ON c.Id = b.CampusId 
" + courseInnerJoinQueryFilter + @"
WHERE
 st.Status = " + Student.EntityStatus.Active + @"
 AND b.Status = " + Batch.EntityStatus.Active + @"
 AND br.Status = " + Branch.EntityStatus.Active + @"
" + courseWhereFilter + programFilter + branchFilter + sessionFilter + campusFilter + batchDayFilter + batchTimeFilter + batchFilter + versionFilter + genderFilter + religionFilter + startDateFilter + endDateFilter + statusFilter + prnNoFilter + nameFilter + mobileFilter + @"
";

            return query;
        }

        private string GetInnerQueryStudentList(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus, string prnNo, string name, string mobile)
        {

            string query = "SELECT sp.Id, sp.PrnNo,sp.VersionOfStudy,sp.StudentId, sp.InstituteId, b.Name as Batch, b.Days as BatchDays, CONCAT(FORMAT(b.StartTime, 'hh:mm tt '),'To', FORMAT(b.EndTime, ' hh:mm tt')) as BatchTime, br.Name as Branch, c.Name as Campus, " +
                   " st.NickName,st.FullName,st.Mobile,st.GuardiansMobile1,st.GuardiansMobile2,st.Gender,st.Religion,st.Email,st.RegistrationNo,st.FatherName" +
                   " from StudentProgram sp" +
                   " inner join Program p on p.Id = sp.ProgramId" +
                   " inner join Student st on st.Id = sp.StudentId" +
                   " inner join Batch b on b.Id = sp.BatchId" +
                   " inner join Program p on p.Id = b.ProgramId" +
                   " inner join Branch br on br.Id = b.BranchId" +
                   " inner join [Session] s on s.Id = b.SessionId" +
                   " inner join Campus c on c.Id = b.CampusId";
            string whereClause = " Where " +
                                 " st.Status = 1 and" +
                                 " p.Status = 1 and" +
                                 " b.Status = 1 and" +
                                 " br.Status = 1 and" +
                                 " s.Status = 1 and" +
                                 " c.Status = 1 ";
            switch (selectedStatus)
            {
                case 1:
                    whereClause += " and sp.Status=" + StudentProgram.EntityStatus.Active;
                    break;
                case 2:
                    whereClause += " and sp.Status=" + StudentProgram.EntityStatus.Inactive;
                    break;
                default:
                    whereClause += " and sp.Status!=" + StudentProgram.EntityStatus.Delete;
                    break;
            }


            if (!(Array.Exists(courseId, item => item == 0)))
            {
                whereClause += " and exists(" +
                          " select 1 from StudentCourseDetails scd inner join CourseSubject cs on scd.CourseSubjectId=cs.Id" +
                          " where scd.status = 1 AND cs.Status = 1 and scd.StudentProgramId=sp.id";
                whereClause += " and cs.courseId in (" + string.Join(",", courseId) + ") ";
                whereClause += ") ";
            }
            whereClause += " and s.Id = " + sessionId + " and p.Id = " + programId;
            if (authorizedProgramLists != null)
            {
                whereClause += " and p.Id in(" + string.Join(",", authorizedProgramLists.ToArray()) + ") ";
            }

            if (authorizedBranchLists != null)
            {
                whereClause += " and br.Id in(" + string.Join(",", authorizedBranchLists.ToArray()) + ") ";
            }

            if (!(Array.Exists(branchId, item => item == 0)))
            {
                whereClause += " and br.Id in(" + string.Join(",", branchId) + ") ";
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                whereClause += " and c.Id in(" + string.Join(",", campusId) + ") ";
            }

            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {

                whereClause += " and b.Days in ('" + string.Join("','", batchDays) + "')";
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                whereClause += " and (" + btQ + ")";
            }

            if (!(Array.Exists(batchId, item => item == 0)))
            {
                whereClause += " and b.Id in (" + string.Join(",", batchId) + ")";
            }

            if (!String.IsNullOrEmpty(startDate))
            {
                string df = startDate + " 00:00:00.000";
                DateTime dFrom = Convert.ToDateTime(df);
                whereClause += " and sp.CreationDate >= '" + dFrom + "'";
            }
            if (!String.IsNullOrEmpty(endDate))
            {
                DateTime dTo = Convert.ToDateTime(endDate + " 23:59:59.000");
                whereClause += " and sp.CreationDate <= '" + dTo + "'";
            }

            if (version != SelectionType.SelelectAll)
            {
                whereClause += " and sp.VersionOfStudy = " + version;
            }
            if (gender != SelectionType.SelelectAll)
            {
                whereClause += " and st.Gender = " + gender;
            }
            if (!(Array.Exists(religion, item => item == 0)))
            {
                whereClause += " and st.Religion in (" + string.Join(",", religion) + ")";
            }

            if (!String.IsNullOrEmpty(prnNo))
            {
                whereClause += " and sp.PrnNo like '%" + prnNo + "%'";
            }
            if (!String.IsNullOrEmpty(name))
            {
                whereClause += " and st.NickName like '%" + name + "%'";
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                whereClause += " and st.Mobile like '%" + mobile + "%'";
            }
            query += whereClause;
            return query;
        }

        //service block
        private string GetInnerQueryStudentList(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] courseId, int printingStatus, int? distributionStatus, bool generateIdCard = false, List<ServiceBlock> serviceBlocks = null)
        {
            var query = "SELECT idcard.Id,idcard.PrnNo,idcard.Batch,idcard.Branch,idcard.NickName,idcard.FullName,idcard.Mobile,idcard.GuardiansMobile1,idcard.FatherName,idcard.PrintingCount from (" +
                        " SELECT sp.Id, sp.PrnNo,b.Name as Batch, br.Name as Branch," +
                        " st.NickName,st.FullName,st.Mobile,st.GuardiansMobile1,st.FatherName,";
            var idCardinnerQuery = "";
            var idCardOuterWhereClause = "";

            switch (distributionStatus)
            {
                case null:
                    idCardinnerQuery =
                           "(select PrintCount from StudentIdCard sic where sic.StudentProgramId=sp.id) AS PrintingCount";
                    switch (printingStatus)
                    {
                        case 2:
                            idCardOuterWhereClause = " Where idcard.PrintingCount IS NOT NULL";
                            break;
                        case 3:
                            idCardOuterWhereClause = " Where idcard.PrintingCount IS NULL";
                            break;
                    }
                    break;
                case 1:
                    idCardinnerQuery = "(select PrintCount from StudentIdCard sic where sic.StudentProgramId=sp.id) AS PrintingCount";
                    idCardOuterWhereClause = " Where idcard.PrintingCount IS NOT NULL";
                    break;
                case 2:
                    idCardinnerQuery = "(select PrintCount from StudentIdCard sic where sic.StudentProgramId=sp.id and sic.IsDistributed = 1) AS PrintingCount";
                    idCardOuterWhereClause = " Where idcard.PrintingCount IS NOT NULL";
                    break;
                case 3:
                    idCardinnerQuery = "(select PrintCount from StudentIdCard sic where sic.StudentProgramId=sp.id and sic.IsDistributed = 0) AS PrintingCount";
                    idCardOuterWhereClause = " Where idcard.PrintingCount IS NOT NULL";
                    break;
            }

            query += idCardinnerQuery;
            query += " from StudentProgram sp" +
                     " inner join Program p on p.Id = sp.ProgramId" +
                     " inner join Student st on st.Id = sp.StudentId" +
                     " inner join Batch b on b.Id = sp.BatchId" +
                     " inner join Branch br on br.Id = b.BranchId" +
                     " inner join [Session] s on s.Id = b.SessionId" +
                     " inner join Campus c on c.Id = b.CampusId";
            string whereClause = " Where sp.Status = 1 and" +
                                " st.Status = 1 and" +
                                " p.Status = 1 and" +
                                " b.Status = 1 and" +
                                " br.Status = 1 and" +
                                " s.Status = 1 and" +
                                " c.Status = 1 and";
            #region Service Block Content

            string serviceBlockedContent = "";

            if (serviceBlocks != null && serviceBlocks.Count > 0)
            {
                foreach (var blockContent in serviceBlocks)
                {
                    if (blockContent.ConditionType == (int)ConditionType.PaymentDue)
                    {
                        serviceBlockedContent += "  sp.DueAmount = 0 AND ";
                    }

                    if (blockContent.ConditionType == (int)ConditionType.ImageDue)
                    {
                        serviceBlockedContent += "  sp.IsImage= 1 AND ";
                    }

                    if (blockContent.ConditionType == (int)ConditionType.PoliticalReference)
                    {
                        serviceBlockedContent += "  sp.IsPolitical = 0 AND ";
                    }

                    if (blockContent.ConditionType == (int)ConditionType.Jsc)
                    {
                        serviceBlockedContent += "  st.IsJsc = 1 AND ";
                    }

                    if (blockContent.ConditionType == (int)ConditionType.Ssc)
                    {
                        serviceBlockedContent += "  st.IsSsc = 1 AND ";
                    }

                    if (blockContent.ConditionType == (int)ConditionType.Hsc)
                    {
                        serviceBlockedContent += "  st.IsHsc = 1 AND ";
                    }
                }


                whereClause += serviceBlockedContent;
            }


            #endregion

            if (!(Array.Exists(courseId, item => item == 0)))
            {
                whereClause += " exists(" +
                           " select 1 from StudentCourseDetails scd inner join CourseSubject cs on scd.CourseSubjectId=cs.Id" +
                           " where scd.Status = 1 and cs.Status = 1 and scd.StudentProgramId=sp.id";
                whereClause += " and cs.courseId in (" + string.Join(",", courseId) + ") ) and";
            }
            whereClause += " s.Id = " + sessionId + " and p.Id = " + programId;
            if (authorizedProgramLists != null)
            {
                whereClause += " and p.Id in(" + string.Join(",", authorizedProgramLists.ToArray()) + ") ";

            }
            if (authorizedBranchLists != null)
            {
                whereClause += " and br.Id in(" + string.Join(",", authorizedBranchLists.ToArray()) + ") ";
            }

            if (!(Array.Exists(branchId, item => item == 0)))
            {
                whereClause += " and br.Id in(" + string.Join(",", branchId) + ") ";
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                whereClause += " and c.Id in(" + string.Join(",", campusId) + ") ";
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {
                whereClause += " and b.Days in ('" + string.Join("','", batchDays) + "')";
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                whereClause += " and (" + btQ + ")";
            }

            if (!(Array.Exists(batchName, item => item == 0)))
            {
                whereClause += " and b.Id in (" + string.Join(",", batchName) + ")";
            }
            query += whereClause + ") as idcard " + idCardOuterWhereClause;

            return query;

        }

        private string GetInnerQueryStudentMissingImage(List<long> branchIdList, List<long> programIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDaysList, string[] batchTimeList, long[] batchList, bool isMissingImage)
        {

            var query = "with missingImageCte as(Select missingImage.* from( select sp.Id,sp.PrnNo,b.Name as Batch,b.id as batchId,b.[Rank] as batchRank, " +
                        " (select count(*) from StudentProgramImage si where sp.Id = si.StudentProgramId) as ImageCount" +
                        " from StudentProgram sp " +
                        " inner join Batch b on b.Id = sp.BatchId";
            string whereClause = " Where sp.Status = 1 and b.Status = 1 and b.SessionId = " + sessionId +
                                 " and sp.ProgramId = " + programId;
            if (programIdList != null)
            {
                whereClause += " and b.ProgramId in(" + string.Join(",", programIdList.ToArray()) + ") ";
            }

            if (branchIdList != null)
            {
                whereClause += " and b.BranchId in(" + string.Join(",", branchIdList.ToArray()) + ") ";
            }

            if (!(Array.Exists(branchId, item => item == 0)))
            {
                whereClause += " and b.BranchId in(" + string.Join(",", branchId) + ") ";
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                whereClause += " and b.CampusId in(" + string.Join(",", campusId) + ") ";
            }

            if (batchDaysList != null && !(Array.Exists(batchDaysList, item => item == "0")) && !(Array.Exists(batchDaysList, item => item == "")))
            {
                whereClause += " and b.Days in ('" + string.Join("','", batchDaysList) + "')";
            }
            if (batchTimeList != null && !(Array.Exists(batchTimeList, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTimeList)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                whereClause += " and (" + btQ + ")";
            }

            if (!(Array.Exists(batchList, item => item == 0)))
            {
                whereClause += " and b.Id in (" + string.Join(",", batchList) + ")";
            }
            query += whereClause + ") AS missingImage ";
            if (isMissingImage)
            {
                query += " Where missingImage.ImageCount=0)";
            }
            else
            {
                query += " Where missingImage.ImageCount>0)";
            }
            return query;
        }

        private string GetInnerQueryStudentImageCount(List<long> branchIdList, List<long> programIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDaysList, string[] batchTimeList, long[] batchList, bool isMissingImage)
        {

            var query = "Select missingImage.* from( select sp.Id,sp.PrnNo,b.Name as Batch," +
                        " (select count(*) from StudentProgramImage si where sp.Id = si.StudentProgramId) as ImageCount" +
                        " from StudentProgram sp " +
                        " inner join Batch b on b.Id = sp.BatchId";
            string whereClause = " Where sp.Status = 1 and b.Status = 1 and b.SessionId = " + sessionId +
                                 " and sp.ProgramId = " + programId;
            if (programIdList != null)
            {
                whereClause += " and b.ProgramId in(" + string.Join(",", programIdList.ToArray()) + ") ";
            }

            if (branchIdList != null)
            {
                whereClause += " and b.BranchId in(" + string.Join(",", branchIdList.ToArray()) + ") ";
            }

            if (!(Array.Exists(branchId, item => item == 0)))
            {
                whereClause += " and b.BranchId in(" + string.Join(",", branchId) + ") ";
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                whereClause += " and b.CampusId in(" + string.Join(",", campusId) + ") ";
            }

            if (batchDaysList != null && !(Array.Exists(batchDaysList, item => item == "0")) && !(Array.Exists(batchDaysList, item => item == "")))
            {
                whereClause += " and b.Days in ('" + string.Join("','", batchDaysList) + "')";
            }
            if (batchTimeList != null && !(Array.Exists(batchTimeList, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTimeList)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                whereClause += " and (" + btQ + ")";
            }

            if (!(Array.Exists(batchList, item => item == 0)))
            {
                whereClause += " and b.Id in (" + string.Join(",", batchList) + ")";
            }
            query += whereClause + ") AS missingImage ";
            if (isMissingImage)
            {
                query += " Where missingImage.ImageCount=0";
            }
            else
            {
                query += " Where missingImage.ImageCount>0";
            }
            return query;
        }

        private string GetInnerQueryStudentImage(List<long> branchIdList, List<long> programIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDaysList, string[] batchTimeList, long[] batchList, bool isMissingImage)
        {

            var query = "Select missingImage.* from(select sp.Id as StudentProgramId, sp.PrnNo AS ProgramRoll,p.Id AS ProgramId,p.Name AS ProgramName," +
                        "s.Id AS SessionId, s.Name AS SessionName,br.Id AS BranchId, br.Name AS BranchName, c.Id AS CampusId, c.Name AS CampusName,b.Days AS BatchDays," +
                        "RIGHT(CONVERT(VARCHAR(20), StartTime, 100), 7) + ' To ' + RIGHT(CONVERT(VARCHAR(20), EndTime, 100), 7) AS BatchTime," +
                        " b.Id AS BatchId,b.Rank AS BatchRank, b.Name as BatchName," +
                        " CASE WHEN EXISTS (select 1 from StudentProgramImage si where sp.Id = si.StudentProgramId) THEN '1' ELSE '0' END AS ImageCount " +
                        " from StudentProgram sp " +
                        " inner join Batch b on b.Id = sp.BatchId" +
                        " inner join Program p on p.Id = b.ProgramId" +
                        " inner join Branch br on br.Id = b.BranchId" +
                        " inner join Session s on s.Id = b.SessionId" +
                        " inner join Campus c on c.Id = b.CampusId";
            string whereClause = " Where sp.Status = 1 and b.Status = 1 and b.SessionId = " + sessionId +
                                 " and sp.ProgramId = " + programId + " " +
                //" and exists(select 1 from   StudentCourseDetails scd where  scd.Status = 1 " +
                                 " and scd.StudentProgramId = sp.id)";
            if (programIdList != null)
            {
                whereClause += " and b.ProgramId in(" + string.Join(",", programIdList.ToArray()) + ") ";
            }

            if (branchIdList != null)
            {
                whereClause += " and b.BranchId in(" + string.Join(",", branchIdList.ToArray()) + ") ";
            }

            if (!(Array.Exists(branchId, item => item == 0)))
            {
                whereClause += " and b.BranchId in(" + string.Join(",", branchId) + ") ";
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                whereClause += " and b.CampusId in(" + string.Join(",", campusId) + ") ";
            }

            if (batchDaysList != null && !(Array.Exists(batchDaysList, item => item == "0")) && !(Array.Exists(batchDaysList, item => item == "")))
            {
                whereClause += " and b.Days in ('" + string.Join("','", batchDaysList) + "')";
            }
            if (batchTimeList != null && !(Array.Exists(batchTimeList, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTimeList)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                whereClause += " and (" + btQ + ")";
            }

            if (!(Array.Exists(batchList, item => item == 0)))
            {
                whereClause += " and b.Id in (" + string.Join(",", batchList) + ")";
            }
            query += whereClause + ") AS missingImage ";
            if (isMissingImage)
            {
                query += " Where missingImage.ImageCount='0'";
            }
            else
            {
                query += " Where missingImage.ImageCount='1'";
            }
            return query;
        }

        private ICriteria GetAuthorizedStudentsSearchResultQuery(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, string searchKey, string[] informationViewList, string prnNo, string name, string mobile, List<int> versionOfStudyList = null, List<int> genderIdList = null, List<int> religionIdList = null)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>();
            criteria.CreateAlias("Program", "p", JoinType.LeftOuterJoin);
            criteria.CreateAlias("Student", "s", JoinType.LeftOuterJoin);
            criteria.CreateAlias("Batch", "b", JoinType.LeftOuterJoin);
            criteria.CreateAlias("b.Branch", "br", JoinType.LeftOuterJoin);
            criteria.CreateAlias("Institute", "inst", JoinType.LeftOuterJoin);
            criteria.CreateAlias("s.District", "dist", JoinType.LeftOuterJoin);
            if (authorizedProgramLists != null)
            {
                criteria.Add(Restrictions.In("p.Id", authorizedProgramLists));
            }
            if (authorizedBranchLists != null)
            {
                criteria.Add(Restrictions.In("br.Id", authorizedBranchLists));
            }
            if (!String.IsNullOrEmpty(searchKey))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                if (informationViewList.Length > 0)
                {
                    foreach (string informationView in informationViewList)
                    {
                        if (informationView == StudentListConstant.ProgramRoll)
                        {
                            disjunction.Add(Restrictions.Like("PrnNo", searchKey, MatchMode.Anywhere));
                        }
                        if (informationView == StudentListConstant.RegistrationNumber)
                        {
                            disjunction.Add(Restrictions.Like("s.RegistrationNo", searchKey, MatchMode.Anywhere));
                        }
                        if (informationView == StudentListConstant.StudentsName)
                        {
                            disjunction.Add(Restrictions.Like("s.FullName", searchKey, MatchMode.Anywhere));
                        }
                        if (informationView == StudentListConstant.NickName)
                        {
                            disjunction.Add(Restrictions.Like("s.NickName", searchKey, MatchMode.Anywhere));
                        }
                        if (informationView == StudentListConstant.MobileNumberStudent)
                        {
                            disjunction.Add(Restrictions.Like("s.Mobile", searchKey, MatchMode.Anywhere));
                        }

                        if (informationView == StudentListConstant.MobileNumberFather)
                        {
                            disjunction.Add(Restrictions.Like("s.GuardiansMobile1", searchKey, MatchMode.Anywhere));
                        }

                        if (informationView == StudentListConstant.MobileNumberMother)
                        {
                            disjunction.Add(Restrictions.Like("s.GuardiansMobile2", searchKey, MatchMode.Anywhere));
                        }

                        if (informationView == StudentListConstant.FathersName)
                        {
                            disjunction.Add(Restrictions.Like("s.FatherName", searchKey, MatchMode.Anywhere));
                        }

                        if (informationView == StudentListConstant.InstituteName)
                        {
                            disjunction.Add(Restrictions.Like("inst.Name", searchKey, MatchMode.Anywhere));
                        }

                        if (informationView == StudentListConstant.Email)
                        {
                            disjunction.Add(Restrictions.Like("s.Email", searchKey, MatchMode.Anywhere));
                        }
                        if (informationView == StudentListConstant.Branch)
                        {
                            disjunction.Add(Restrictions.Like("br.Name", searchKey, MatchMode.Anywhere));
                        }
                        if (informationView == StudentListConstant.Batch)
                        {
                            disjunction.Add(Restrictions.Like("b.Name", searchKey, MatchMode.Anywhere));
                        }
                        if (informationView == StudentListConstant.District)
                        {
                            disjunction.Add(Restrictions.Like("dist.Name", searchKey, MatchMode.Anywhere));
                        }
                        if (versionOfStudyList != null && versionOfStudyList.Any())
                        {
                            disjunction.Add(Restrictions.In("VersionOfStudy", versionOfStudyList));
                        }
                        if (genderIdList != null && genderIdList.Any())
                        {
                            disjunction.Add(Restrictions.In("s.Gender", genderIdList));
                        }
                        if (religionIdList != null && religionIdList.Any())
                        {
                            disjunction.Add(Restrictions.In("s.Religion", religionIdList));
                        }
                    }
                    criteria.Add(disjunction);
                }
            }

            if (!String.IsNullOrEmpty(prnNo))
            {
                criteria.Add(Restrictions.Like("PrnNo", prnNo, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("s.NickName", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                criteria.Add(Restrictions.Like("s.Mobile", mobile, MatchMode.Anywhere));
            }
            criteria.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            if (programId != SelectionType.SelelectAll)
                criteria.Add(Restrictions.Eq("b.Program.Id", programId));
            if (sessionId != SelectionType.SelelectAll)
                criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));
            return criteria;
        }

        private ICriteria GetAuthorizedStudentsReportResultQuery(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int surveyStatus)
        {
            ICriteria criteria = GetAuthorizedStudentCriteria(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId);
            if (surveyStatus != SelectionType.SelelectAll)
            {
                if (surveyStatus == SelectionType.Complete)
                {
                    criteria.Add(Restrictions.IsNotEmpty("StudentSurveyAnswers"));
                }
                else
                {
                    criteria.Add(Restrictions.IsEmpty("StudentSurveyAnswers"));
                }
            }
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            return criteria;
        }

        private ICriteria GetAuthorizedStudentCriteria(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));

            criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            criteria.CreateAlias("Batch", "b").Add(Restrictions.Eq("b.Status", Batch.EntityStatus.Active));
            criteria.CreateAlias("b.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("b.Campus", "ca").Add(Restrictions.Eq("ca.Status", Campus.EntityStatus.Active));
            if (authorizedProgramLists != null)
            {
                criteria.Add(Restrictions.In("Program.Id", authorizedProgramLists));
            }
            if (authorizedBranchLists != null)
            {
                criteria.Add(Restrictions.In("br.Id", authorizedBranchLists));
            }
            criteria.Add(Restrictions.Eq("p.Id", programId));
            criteria.Add(Restrictions.Eq("se.Id", sessionId));
            if (!(Array.Exists(branchId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("br.Id", branchId));
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("ca.Id", campusId));
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {
                criteria.Add(Restrictions.In("b.Days", batchDays));
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }

            if (!(Array.Exists(batchId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Id", batchId));
            }

            DetachedCriteria courseDetachedCriteria = DetachedCriteria.For<StudentCourseDetail>().SetProjection(Projections.Distinct(Projections.Property("StudentProgram.Id")));
            courseDetachedCriteria.CreateAlias("StudentProgram", "sp").CreateAlias("sp.Program", "Program").Add(Restrictions.Eq("Program.Id", programId));
            courseDetachedCriteria.CreateAlias("CourseSubject", "cs").Add(Restrictions.Eq("cs.Status", CourseSubject.EntityStatus.Active));
            courseDetachedCriteria.CreateAlias("cs.Course", "co").Add(Restrictions.Eq("co.Status", Course.EntityStatus.Active));//.Add(Restrictions.In("co.Id", courseId));

            if (!(Array.Exists(courseId, item => item == 0)))
            {
                courseDetachedCriteria.Add(Restrictions.In("co.Id", courseId));
            }
            criteria.Add(Subqueries.PropertyIn("Id", courseDetachedCriteria));

            return criteria;
        }

        private ICriteria GetAuthorizedStudentWiseSurveyReportQuery(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, long[] questionId, string[] answerName, string programRoll, string nickName, string mobile)
        {
            ICriteria criteria = GetAuthorizedStudentCriteria(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId);

            criteria.CreateAlias("StudentSurveyAnswers", "ssa");
            criteria.CreateCriteria("ssa.SurveyQuestion", "sq").Add(Restrictions.Eq("sq.Status", SurveyQuestion.EntityStatus.Active));
            criteria.CreateCriteria("ssa.SurveyQuestionAnswer", "sqa").Add(Restrictions.Eq("sqa.Status", SurveyQuestionAnswer.EntityStatus.Active));
            if (!(Array.Exists(questionId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("sq.Id", questionId));
            }
            if (!(Array.Exists(answerName, item => item == "0")))
            {
                criteria.Add(Restrictions.In("sqa.Answer", answerName));
            }
            if (!String.IsNullOrEmpty(programRoll))
            {
                criteria.Add(Restrictions.Like("PrnNo", programRoll, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(nickName))
            {
                criteria.Add(Restrictions.Like("st.NickName", nickName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
            }
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            return criteria;
        }

        //common query for student program with images
        private string GetStudentProgramWithImageListQuery(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long[] programIds, long[] sessionIds, long[] branchIds, long[] campusIds, long[] batchNames, string[] batchDays, string[] batchTimes)
        {
            string filterQuery = "";
            if (authorizedProgramLists != null)
            {
                filterQuery += " and sp.programId in(" + string.Join(",", authorizedProgramLists) + ")";
            }
            if (authorizedBranchLists != null)
            {
                filterQuery += " and br.id in(" + string.Join(",", authorizedBranchLists) + ")";
            }
            if (!(Array.Exists(branchIds, item => item == 0)))
            {
                filterQuery += " and b.BranchId in(" + string.Join(",", branchIds) + ") ";
            }
            if (!(Array.Exists(campusIds, item => item == 0)))
            {
                filterQuery += " and b.CampusId in(" + string.Join(",", campusIds) + ") ";
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {
                filterQuery += " and b.Days in ('" + string.Join("','", batchDays) + "')";
            }
            if (batchTimes != null && !Array.Exists(batchTimes, item => item == "0"))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTimes)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                filterQuery += " and (" + btQ + ")";
            }
            if (!(Array.Exists(batchNames, item => item == 0)))
            {
                filterQuery += " and b.Id in (" + string.Join(",", batchNames) + ")";
            }
            string query = @"SELECT sp.Id as studentProgramId
                                    FROM [StudentProgram] AS sp 
                                    INNER JOIN [Program] AS p ON sp.[ProgramId]=p.Id 
                                    INNER JOIN [Batch] AS b ON sp.[BatchId]= b.Id 
                                    INNER JOIN [Session] AS s ON b.[SessionId]=s.Id 
                                    INNER JOIN [Branch] AS br ON b.[BranchId]= br.Id 
                                    INNER JOIN [Campus] AS cam ON cam.Id = b.CampusId 
                                    INNER JOIN StudentProgramImage as spimg on sp.Id = spimg.StudentProgramId and spimg.Status = 1
                                    AND b.sessionId in(" + string.Join(", ", sessionIds) + ") and sp.status=1 AND b.status=1 AND br.status=1 AND s.status=1 AND sp.[IsImage]= " +
                           (int)StudentImageStatus.Active + " AND cam.status=1  " + " and sp.programId in (" + string.Join(", ", programIds) + ")" +
                            filterQuery + @"";
            return query;
        }

        #endregion

        #region Responsible For Missing Board Roll Student Information

        private string GetMissingBoardRollStudentQuery(List<long> authoProgramIdList, List<long> authoBranchIdList, long sessionId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null)
        {
            string programFilter = "";
            string branchFilter = "";
            string sessionFilter = " AND ses.Id = " + sessionId + " ";
            string campusFilter = "";
            string batchDayFilter = "";
            string batchTimesFilter = "";
            string batchFilter = "";
            string examFilter = " AND sai.StudentExamId =  " + studentExamId + " ";
            string prnNoFilter = "";
            string nameFilter = "";
            string mobileFilter = "";



            if (authoProgramIdList.Any() && !authoProgramIdList.Contains(SelectionType.SelelectAll))
            {
                programFilter = " AND sp.ProgramId IN (" + string.Join(",", authoProgramIdList.ToArray()) + ") ";
            }
            if (authoBranchIdList.Any() && !authoBranchIdList.Contains(SelectionType.SelelectAll))
            {
                branchFilter = " AND b.BranchId IN (" + string.Join(",", authoBranchIdList.ToArray()) + ") ";
            }
            if (campusId.ToList().Any() && !campusId.ToList().Contains(SelectionType.SelelectAll))
            {
                campusFilter = " AND b.CampusId IN (" + string.Join(",", campusId) + ") ";
            }
            if (batchDays.ToList().Any() && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {
                batchDayFilter = " and b.Days in ('" + string.Join("','", batchDays) + "')";
            }
            if (batchTimes.ToList().Any() && !(Array.Exists(batchTimes, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTimes)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                batchTimesFilter += " and (" + btQ + ")";
            }
            if (batchIds.ToList().Any() && !batchIds.ToList().Contains(SelectionType.SelelectAll))
            {
                batchFilter = " and b.Id in ('" + string.Join("','", batchIds) + "')";
            }
            if (!String.IsNullOrEmpty(prnNo))
            {
                prnNoFilter = " AND sp.PrnNo = '" + prnNo + "' ";
            }
            if (!String.IsNullOrEmpty(name))
            {
                nameFilter = " AND s.NickName like '%" + name + "%' ";
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                mobileFilter = " AND s.Mobile like '%" + mobile + "%' ";
            }

            string query = @"select 
                            sp.Id as StudentProgramId
                            , s.Id as StudentId
                            , sp.PrnNo as ProgramRoll
                            , s.NickName as NickName
                            , s.Mobile as MobileNumber
                            , s.GuardiansMobile1 as MobileNumberFather
                            , s.GuardiansMobile2 as MobileNumberMother
                            , sai.BoardId as BoardId
                            , sai.StudentExamId as StudentExamId
                            , sai.ExamYear as Year
                            , sai.BoradRoll as BoradRoll
                            , sai.RegistrationNumber as RegistrationNumber
                            , sai.BoardName as BoardName
                            from Student as s
                            inner join StudentProgram as sp on sp.StudentId = s.Id and sp.Status = " + StudentProgram.EntityStatus.Active + " " + programFilter + @"
                            inner join Batch as b on b.Id = sp.BatchId and b.Status = " + Batch.EntityStatus.Active + @"
                            inner join Session as ses on ses.Id = b.SessionId and ses.Status = " + BusinessModel.Entity.Administration.Session.EntityStatus.Active + @"
                            left join ( 
	                            select 
	                            sai.Id as StudentAcademicInfoId
	                            , sai.BoradRoll as BoradRoll
	                            , sai.BoardId as BoardId
	                            , sai.RegistrationNumber as RegistrationNumber
	                            , sai.Year as ExamYear
	                            , sai.StudentId as StudentId
	                            , sai.StudentExamId as  StudentExamId
                                , sb.Name as BoardName
	                            ,RANK() OVER(PARTITION BY sai.StudentId ORDER BY sai.Id DESC) AS R  
	                            from StudentAcademicInfo as sai
                                inner join StudentBoard as sb on sb.Id = sai.BoardId
	                            where sai.Status = " + StudentAcademicInfo.EntityStatus.Active + examFilter + @"
                            ) as sai on sai.StudentId = s.Id and sai.R = 1
                            where s.Status = " + Student.EntityStatus.Active + sessionFilter + branchFilter + campusFilter + batchDayFilter + batchTimesFilter + batchFilter + prnNoFilter + nameFilter + mobileFilter + @"
                            and (sai.BoradRoll is null or sai.BoradRoll = '' or sai.RegistrationNumber is null or sai.RegistrationNumber = '')
                            ";

            return query;

            //ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            //criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));

            //DetachedCriteria dc = DetachedCriteria.For<StudentAcademicInfo>().SetProjection(Projections.Distinct(Projections.Property("Student.Id"))).Add(Restrictions.Eq("StudentExam.Id", studentExamId));
            //dc.Add(Restrictions.Eq("Status", StudentAcademicInfo.EntityStatus.Active));
            ////dc.Add(Restrictions.Eq("Status", StudentAcademicInfo.EntityStatus.Active));

            //criteria.Add(Subqueries.PropertyNotIn("st.Id", dc));
            //criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Session.Id", sessionId));

            //if (authoProgramIdList != null)
            //{
            //    criteria.Add(Restrictions.In("b.Program.Id", authoProgramIdList));
            //}
            //if (authoBranchIdList != null)
            //{
            //    criteria.Add(Restrictions.In("b.Branch.Id", authoBranchIdList));
            //}
            //if (!(Array.Exists(campusId, item => item == 0)))
            //    criteria.Add(Restrictions.In("b.Campus.Id", campusId));

            //if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            //    criteria.Add(Restrictions.In("b.Days", batchDays));
            //if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            //{
            //    var disjunction = Restrictions.Disjunction(); // for OR statement 
            //    foreach (string bt in batchTimes)
            //    {
            //        var batchArrays = bt.Replace("To", ",");
            //        var batchArray = batchArrays.Split(',');
            //        var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
            //        var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
            //        disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
            //    }
            //    criteria.Add(disjunction);
            //}
            //if (!(Array.Exists(batchIds, item => item == 0)))
            //    criteria.Add(Restrictions.In("b.Id", batchIds));
            //if (!String.IsNullOrEmpty(prnNo))
            //{
            //    criteria.Add(Restrictions.Like("PrnNo", prnNo, MatchMode.Anywhere));
            //}
            //if (!String.IsNullOrEmpty(name))
            //{
            //    criteria.Add(Restrictions.Like("st.NickName", name, MatchMode.Anywhere));
            //}
            //if (!String.IsNullOrEmpty(mobile))
            //{
            //    criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
            //}

            //return criteria;
        }

        public long GetMissingBoardRollStudentCount(List<long> authoProgramIdList, List<long> authoBranchIdList, long sessionId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null)
        {
            string criteriaQuery = GetMissingBoardRollStudentQuery(authoProgramIdList, authoBranchIdList, sessionId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);

            string query = "SELECT count(A.StudentId) as total FROM(" + criteriaQuery + ") as A";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2000);
            return Convert.ToInt32(iQuery.UniqueResult());

            //criteria.SetTimeout(2700).SetProjection(Projections.RowCount());
            //return Convert.ToInt64(criteria.UniqueResult());
        }
        //public long GetMissingBoardRollStudentCount(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null)
        //{
        //    ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
        //    criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));

        //    DetachedCriteria dc = DetachedCriteria.For<StudentAcademicInfo>().SetProjection(Projections.Distinct(Projections.Property("Student.Id"))).Add(Restrictions.Eq("StudentExam.Id", studentExamId));
        //    dc.Add(Restrictions.Eq("Status", StudentAcademicInfo.EntityStatus.Active));
        //    criteria.Add(Subqueries.PropertyNotIn("st.Id", dc));
        //    criteria.Add(Restrictions.Eq("Program.Id", programId));
        //    criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Session.Id", sessionId)).Add(Restrictions.Eq("b.Program.Id", programId));

        //    if (programIdList != null)
        //    {
        //        criteria.Add(Restrictions.In("b.Program.Id", programIdList));
        //    }
        //    if (branchIdList != null)
        //    {
        //        criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
        //    }
        //    if (!(Array.Exists(branchId, item => item == 0)))
        //        criteria.Add(Restrictions.In("b.Branch.Id", branchId));
        //    else
        //    {
        //        DetachedCriteria branchByProgramSession = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
        //            .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
        //        criteria.Add(Subqueries.PropertyIn("b.Branch.Id", branchByProgramSession));
        //    }
        //    if (!(Array.Exists(campusId, item => item == 0)))
        //        criteria.Add(Restrictions.In("b.Campus.Id", campusId));
        //    else
        //    {
        //        DetachedCriteria branchByProgramSession2 = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
        //            .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
        //        criteria.Add(Subqueries.PropertyIn("b.Branch.Id", branchByProgramSession2));
        //        DetachedCriteria campusFind = DetachedCriteria.For<Campus>().SetProjection(Projections.Distinct(Projections.Property("Id")));
        //        campusFind.Add(Subqueries.PropertyIn("Branch.Id", branchByProgramSession2));
        //        criteria.Add(Subqueries.PropertyIn("b.Campus.Id", campusFind));
        //    }

        //    if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
        //        criteria.Add(Restrictions.In("b.Days", batchDays));
        //    if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
        //    {
        //        var disjunction = Restrictions.Disjunction(); // for OR statement 
        //        foreach (string bt in batchTimes)
        //        {
        //            var batchArrays = bt.Replace("To", ",");
        //            var batchArray = batchArrays.Split(',');
        //            var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
        //            var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
        //            disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
        //        }
        //        criteria.Add(disjunction);
        //    }
        //    if (!(Array.Exists(batchIds, item => item == 0)))
        //        criteria.Add(Restrictions.In("b.Id", batchIds));
        //    if (!String.IsNullOrEmpty(prnNo))
        //    {
        //        criteria.Add(Restrictions.Like("PrnNo", prnNo, MatchMode.Anywhere));
        //    }
        //    if (!String.IsNullOrEmpty(name))
        //    {
        //        criteria.Add(Restrictions.Like("st.NickName", name, MatchMode.Anywhere));
        //    }
        //    if (!String.IsNullOrEmpty(mobile))
        //    {
        //        criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
        //    }
        //    criteria.SetTimeout(2700).SetProjection(Projections.RowCount());
        //    return Convert.ToInt64(criteria.UniqueResult());
        //}

        //public List<StudentProgram> GetMissingBoardRollStudent(List<long> programIdList, List<long> branchIdList, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null)
        public List<MissingBoardRollListDto> GetMissingBoardRollStudent(List<long> authoProgramIdList, List<long> authoBranchIdList, int start, int length, long sessionId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null)
        {
            //ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            //criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            //DetachedCriteria dc = DetachedCriteria.For<StudentAcademicInfo>().SetProjection(Projections.Distinct(Projections.Property("Student.Id"))).Add(Restrictions.Eq("StudentExam.Id", studentExamId));
            //dc.Add(Restrictions.Eq("Status", StudentAcademicInfo.EntityStatus.Active));
            //criteria.Add(Subqueries.PropertyNotIn("st.Id", dc));
            //criteria.Add(Restrictions.Eq("Program.Id", programId));
            //criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Session.Id", sessionId)).Add(Restrictions.Eq("b.Program.Id", programId));
            //if (programIdList != null)
            //{
            //    criteria.Add(Restrictions.In("b.Program.Id", programIdList));
            //}
            //if (branchIdList != null)
            //{
            //    criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
            //}
            //if (!(Array.Exists(branchId, item => item == 0)))
            //    criteria.Add(Restrictions.In("b.Branch.Id", branchId));
            //else
            //{
            //    DetachedCriteria branchByProgramSession = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
            //        .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
            //    criteria.Add(Subqueries.PropertyIn("b.Branch.Id", branchByProgramSession));
            //}
            //if (!(Array.Exists(campusId, item => item == 0)))
            //    criteria.Add(Restrictions.In("b.Campus.Id", campusId));
            //else
            //{
            //    DetachedCriteria branchByProgramSession2 = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
            //        .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
            //    criteria.Add(Subqueries.PropertyIn("b.Branch.Id", branchByProgramSession2));
            //    DetachedCriteria campusFind = DetachedCriteria.For<Campus>().SetProjection(Projections.Distinct(Projections.Property("Id")));
            //    campusFind.Add(Subqueries.PropertyIn("Branch.Id", branchByProgramSession2));
            //    criteria.Add(Subqueries.PropertyIn("b.Campus.Id", campusFind));
            //}

            //if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            //    criteria.Add(Restrictions.In("b.Days", batchDays));
            //if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            //{
            //    var disjunction = Restrictions.Disjunction(); // for OR statement 
            //    foreach (string bt in batchTimes)
            //    {
            //        var batchArrays = bt.Replace("To", ",");
            //        var batchArray = batchArrays.Split(',');
            //        var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
            //        var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
            //        disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
            //    }
            //    criteria.Add(disjunction);
            //}

            //if (!(Array.Exists(batchIds, item => item == 0)))
            //    criteria.Add(Restrictions.In("b.Id", batchIds));


            //if (!String.IsNullOrEmpty(prnNo))
            //{
            //    criteria.Add(Restrictions.Like("PrnNo", prnNo, MatchMode.Anywhere));
            //}
            //if (!String.IsNullOrEmpty(name))
            //{
            //    criteria.Add(Restrictions.Like("st.NickName", name, MatchMode.Anywhere));
            //}
            //if (!String.IsNullOrEmpty(mobile))
            //{
            //    criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
            //}
            //criteria.AddOrder(Order.Asc("PrnNo"));
            string rqwQuery = GetMissingBoardRollStudentQuery(authoProgramIdList, authoBranchIdList, sessionId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);

            if (length > 0)
            {
                rqwQuery += " order by sp.PrnNo asc OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }

            IQuery iQuery = Session.CreateSQLQuery(rqwQuery);
            iQuery.SetResultTransformer(Transformers.AliasToBean<MissingBoardRollListDto>());
            List<MissingBoardRollListDto> list = iQuery.List<MissingBoardRollListDto>().ToList();
            return list;



            //IQuery iQuery = Session.CreateSQLQuery(query);
            //iQuery.SetTimeout(2000);
            //iQuery.SetResultTransformer(Transformers.AliasToBean<MissingBoardRollListDto>());
            //return iQuery.List<MissingBoardRollListDto>().ToList();
            //criteria.SetTimeout(2700).SetResultTransformer(Transformers.DistinctRootEntity);
            //if (length > 0)
            //{
            //    return
            //        (List<StudentProgram>)criteria.SetFirstResult(start).SetMaxResults(length).List<StudentProgram>();
            //}
            //else
            //{
            //    return
            //        (List<StudentProgram>)criteria.List<StudentProgram>();
            //}
        }
        #endregion

        #region Responsible For View Board Result Information

        public long GetBoardResultStudentCount(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            criteria.CreateAlias("st.StudentAcademicInfos", "sai", JoinType.InnerJoin).Add(Restrictions.EqProperty("sai.Student.Id", "st.Id"));
            criteria.CreateAlias("sai.StudentAcademicInfoBaseData", "bd", JoinType.InnerJoin).Add(Restrictions.EqProperty("bd.StudentAcademicInfo.Id", "sai.Id"));
            criteria.Add(Restrictions.Eq("sai.StudentExam.Id", studentExamId));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Session.Id", sessionId)).Add(Restrictions.Eq("b.Program.Id", programId));

            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("b.Program.Id", programIdList));
            }
            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
            }
            if (!(Array.Exists(branchId, item => item == 0)))
                criteria.Add(Restrictions.In("b.Branch.Id", branchId));
            else
            {
                DetachedCriteria branchByProgramSession = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
                    .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
                criteria.Add(Subqueries.PropertyIn("b.Branch.Id", branchByProgramSession));
            }
            if (!(Array.Exists(campusId, item => item == 0)))
                criteria.Add(Restrictions.In("b.Campus.Id", campusId));
            else
            {
                DetachedCriteria branchByProgramSession2 = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
                    .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
                criteria.Add(Subqueries.PropertyIn("b.Branch.Id", branchByProgramSession2));
                DetachedCriteria campusFind = DetachedCriteria.For<Campus>().SetProjection(Projections.Distinct(Projections.Property("Id")));
                campusFind.Add(Subqueries.PropertyIn("Branch.Id", branchByProgramSession2));
                criteria.Add(Subqueries.PropertyIn("b.Campus.Id", campusFind));
            }

            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
                criteria.Add(Restrictions.In("b.Days", batchDays));
            if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTimes)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }

            if (!(Array.Exists(batchIds, item => item == 0)))
                criteria.Add(Restrictions.In("b.Id", batchIds));
            if (!String.IsNullOrEmpty(prnNo))
            {
                criteria.Add(Restrictions.Like("PrnNo", prnNo, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("st.NickName", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
            }
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt64(criteria.UniqueResult());
        }

        public List<StudentProgram> GetBoardResultStudent(List<long> programIdList, List<long> branchIdList, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string prnNo = null, string name = null, string mobile = null)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            criteria.CreateAlias("st.StudentAcademicInfos", "sai", JoinType.InnerJoin).Add(Restrictions.EqProperty("sai.Student.Id", "st.Id"));
            criteria.CreateAlias("sai.StudentAcademicInfoBaseData", "bd", JoinType.InnerJoin).Add(Restrictions.EqProperty("bd.StudentAcademicInfo.Id", "sai.Id"));
            criteria.Add(Restrictions.Eq("sai.StudentExam.Id", studentExamId));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Session.Id", sessionId)).Add(Restrictions.Eq("b.Program.Id", programId));
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("b.Program.Id", programIdList));
            }
            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
            }
            if (!(Array.Exists(branchId, item => item == 0)))
                criteria.Add(Restrictions.In("b.Branch.Id", branchId));
            else
            {
                DetachedCriteria branchByProgramSession = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
                    .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
                criteria.Add(Subqueries.PropertyIn("b.Branch.Id", branchByProgramSession));
            }
            if (!(Array.Exists(campusId, item => item == 0)))
                criteria.Add(Restrictions.In("b.Campus.Id", campusId));
            else
            {
                DetachedCriteria branchByProgramSession2 = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
                    .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
                criteria.Add(Subqueries.PropertyIn("b.Branch.Id", branchByProgramSession2));
                DetachedCriteria campusFind = DetachedCriteria.For<Campus>().SetProjection(Projections.Distinct(Projections.Property("Id")));
                campusFind.Add(Subqueries.PropertyIn("Branch.Id", branchByProgramSession2));
                criteria.Add(Subqueries.PropertyIn("b.Campus.Id", campusFind));
            }

            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
                criteria.Add(Restrictions.In("b.Days", batchDays));
            if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTimes)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }
            if (!(Array.Exists(batchIds, item => item == 0)))
                criteria.Add(Restrictions.In("b.Id", batchIds));
            if (!String.IsNullOrEmpty(prnNo))
            {
                criteria.Add(Restrictions.Like("PrnNo", prnNo, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("st.NickName", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
            }
            criteria.AddOrder(Order.Asc("PrnNo"));
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            return (List<StudentProgram>)criteria.SetFirstResult(start).SetMaxResults(length).List<StudentProgram>();
        }

        public List<StudentProgram> LoadStudentProgramForBoardResult(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            DetachedCriteria dc = DetachedCriteria.For<StudentAcademicInfo>()
                .SetProjection(Projections.Distinct(Projections.Property("Student.Id"))).Add(Restrictions.Eq("StudentExam.Id", studentExamId));
            criteria.Add(Subqueries.PropertyIn("st.Id", dc));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Session.Id", sessionId)).Add(Restrictions.Eq("b.Program.Id", programId));
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("b.Program.Id", programIdList));
            }
            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
            }
            if (!(Array.Exists(branchId, item => item == 0)))
                criteria.Add(Restrictions.In("b.Branch.Id", branchId));
            else
            {
                DetachedCriteria branchByProgramSession = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
                    .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
                criteria.Add(Subqueries.PropertyIn("b.Branch.Id", branchByProgramSession));
            }
            if (!(Array.Exists(campusId, item => item == 0)))
                criteria.Add(Restrictions.In("b.Campus.Id", campusId));
            else
            {
                DetachedCriteria branchByProgramSession2 = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
                    .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
                criteria.Add(Subqueries.PropertyIn("b.Branch.Id", branchByProgramSession2));
                DetachedCriteria campusFind = DetachedCriteria.For<Campus>().SetProjection(Projections.Distinct(Projections.Property("Id")));
                campusFind.Add(Subqueries.PropertyIn("Branch.Id", branchByProgramSession2));
                criteria.Add(Subqueries.PropertyIn("b.Campus.Id", campusFind));
            }

            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
                criteria.Add(Restrictions.In("b.Days", batchDays));
            if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTimes)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }
            if (!(Array.Exists(batchIds, item => item == 0)))
                criteria.Add(Restrictions.In("b.Id", batchIds));
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            List<StudentProgram> resultList = criteria.List<StudentProgram>().ToList();
            return resultList;
        }

        #endregion

        public long CountStudentCompareWithBoard(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long studentExamId, string year, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, int[] religion, int? gender, int? versionOfStudy)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));

            criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Status", Batch.EntityStatus.Active));
            criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            criteria.CreateAlias("b.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("b.Campus", "ca").Add(Restrictions.Eq("ca.Status", Campus.EntityStatus.Active));
            criteria.CreateCriteria("st.StudentAcademicInfos", "sai", JoinType.InnerJoin)
                .Add(Restrictions.Eq("sai.Status", StudentAcademicInfo.EntityStatus.Active));
            criteria.CreateCriteria("sai.StudentExam", "studentExm")
                .Add(Restrictions.Eq("studentExm.Status", StudentExam.EntityStatus.Active));
            criteria.CreateCriteria("sai.StudentAcademicInfoBaseData", "saiB", JoinType.InnerJoin);
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("Program.Id", programIdList));
            }
            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
            }
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));

            if (!(Array.Exists(branchIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIds));
            }
            if (!(Array.Exists(campusIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Campus.Id", campusIds));
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")))
            {
                criteria.Add(Restrictions.In("b.Days", batchDays));
            }
            if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTimes)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }
            if ((!Array.Exists(batchIds, item => item == 0)))
            {
                criteria.Add(Restrictions.In("Batch.Id", batchIds));
            }
            if (versionOfStudy != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("VersionOfStudy", versionOfStudy));
            }
            if (gender != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("st.Gender", gender));
            }
            if ((!Array.Exists(religion, item => item == 0)))
            {
                criteria.Add(Restrictions.In("st.Religion", religion));
            }
            criteria.Add(Restrictions.Eq("sai.Year", year));
            criteria.Add(Restrictions.Eq("studentExm.Id", studentExamId));
            criteria.SetResultTransformer(Transformers.AliasToBean(typeof(Student)));
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt64(criteria.UniqueResult());
        }

    }
}