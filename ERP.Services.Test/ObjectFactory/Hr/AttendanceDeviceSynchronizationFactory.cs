﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class AttendanceDeviceSynchronizationFactory : ObjectFactoryBase<AttendanceDeviceRawData>
    {
        private readonly AttendanceDeviceFactory _attendanceDeviceFactory; //AttendanceDeviceSynchronizationService

        private readonly AttendanceDeviceRawDataService _attendanceDeviceSynchronizationService;    
       public AttendanceDeviceSynchronizationFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _attendanceDeviceFactory = new AttendanceDeviceFactory(this, session);
            _attendanceDeviceSynchronizationService = new AttendanceDeviceRawDataService(session);
        }
       public AttendanceDeviceSynchronizationFactory Create()
       {
           Object = CreateObject(
               "_Attendance_Synchronization_" + DateTime.Now + "_" + Guid.NewGuid().ToString(), 
               GenerateNumaricGuid().ToString(),
               1,
               true,
               DateTime.Now,
               "672384",
               10
              );
           SingleObjectList.Add(Object);
           return this;
       }
       private AttendanceDeviceRawData CreateObject(string name, string deviceModelNo, int pin, bool isIn,
            DateTime punchTime, string cardNumber, int deviceCallingInterval)
       {
           ListStartIndex = ObjectList.Count;
           var obj = new AttendanceDeviceRawData()
           {
               Id = 0,
               Name = name,
               //Din = deviceModelNo,
               Pin = pin,
               IsIn = isIn,
               PunchTime = punchTime,
               //SynchronizerKey = machineNumber,
               //CardNumber = cardNumber,
               //DeviceCallingInterval = deviceCallingInterval,
               BusinessId = GetPcUserName()
           };
           return obj;
       }

       public AttendanceDeviceSynchronizationFactory WithAttendanceDevice(AttendanceDevice attendanceDevice)  
       {
           Object.AttendanceDevice = attendanceDevice;
           return this;
       }
       public AttendanceDeviceSynchronizationFactory Persist()
       {
           SingleObjectList.ForEach(item =>
           {
               if (item.Id == 0)
               {
                   _attendanceDeviceSynchronizationService.Save(item);
               }
           });
           ObjectList.ForEach(item =>
           {
               if (item.Id == 0)
               {
                   _attendanceDeviceSynchronizationService.Save(item);
               }
           });
           return this;
       }
       public AttendanceDeviceSynchronizationFactory CleanUp()
       {
           DeleteAll();
           _attendanceDeviceFactory.CleanUp();
           return this;
       }

       private static long GenerateNumaricGuid()
       {
           byte[] buffer = Guid.NewGuid().ToByteArray();
           return BitConverter.ToInt64(buffer, 0);
       }
    }
}
