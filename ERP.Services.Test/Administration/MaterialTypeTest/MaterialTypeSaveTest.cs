﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.MaterialTypeTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "MaterialType")]
    public class MaterialTypeSaveTest : MaterialTypeBaseTest
    {
        #region Basic Test
        [Fact]
        public void MaterialType_Save_Should_Object_Null()
        {
            Assert.Throws<NullObjectException>(() => _materialTypeService.Save(null));
        }
        [Fact]
        public void MaterialType_Save_Should_Return_True()
        {
            _materialTypeFactory.Create().WithOrganization();
            _materialTypeService.Save(_materialTypeFactory.Object);
            Assert.True(_materialTypeFactory.Object.Id > 0);
        }

        [Fact]
        public void MaterialType_Save_Should_Return_Name_Null()
        {
            var obj = _materialTypeFactory.Create().WithOrganization().Object;
            obj.Name = null;
            Assert.Throws<InvalidDataException>(() => _materialTypeService.Save(_materialTypeFactory.Object));
        }
        [Fact]
        public void MaterialType_Save_Should_Return_ShortName_Null()
        {
            var obj = _materialTypeFactory.Create().WithOrganization().Object;
            obj.ShortName = null;
            Assert.Throws<InvalidDataException>(() => _materialTypeService.Save(_materialTypeFactory.Object));
        }
        [Fact]
        public void MaterialType_Save_Should_Return_Name_Empty()
        {
            var obj = _materialTypeFactory.Create().WithOrganization().Object;
            obj.Name = string.Empty;
            Assert.Throws<InvalidDataException>(() => _materialTypeService.Save(_materialTypeFactory.Object));
        }
        [Fact]
        public void MaterialType_Save_Should_Return_ShortName_Empty()
        {
            var obj = _materialTypeFactory.Create().WithOrganization().Object;
            obj.ShortName = string.Empty;
            Assert.Throws<InvalidDataException>(() => _materialTypeService.Save(_materialTypeFactory.Object));
        }
        [Fact]
        public void MaterialType_Save_Should_Return_Orgnazition_Null()
        {
            var obj = _materialTypeFactory.Create().Object;
            Assert.Throws<InvalidDataException>(() => _materialTypeService.Save(_materialTypeFactory.Object));
        }

        #endregion

        #region Business Logic Test
        [Fact]
        public void Save_Should_Check_Duplicate_Name()
        {
            organizationFactory.Create().Persist();
            _materialTypeFactory.CreateMore(2, organizationFactory.Object);
            _materialTypeService.Save(_materialTypeFactory.ObjectList[0]);

            _materialTypeFactory.ObjectList[1].Name = _materialTypeFactory.ObjectList[0].Name;
            Assert.Throws<DuplicateEntryException>(() => _materialTypeService.Save(_materialTypeFactory.ObjectList[1]));
        }
        [Fact]
        public void Save_Should_Check_Duplicate_ShortName()
        {
            organizationFactory.Create().Persist();
            _materialTypeFactory.CreateMore(2, organizationFactory.Object);
            _materialTypeService.Save(_materialTypeFactory.ObjectList[0]);

            _materialTypeFactory.ObjectList[1].ShortName = _materialTypeFactory.ObjectList[0].ShortName;
            Assert.Throws<DuplicateEntryException>(() => _materialTypeService.Save(_materialTypeFactory.ObjectList[1]));
        }
        #endregion
    }
}
