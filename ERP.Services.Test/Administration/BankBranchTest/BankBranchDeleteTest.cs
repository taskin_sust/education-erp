﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BankBranchTest
{
    
    public interface IBankBranchDeleteTest
    {
        #region Basic Test
        [Fact]
        void Should_Delete_Successfully();

        [Fact]
        void Delete_Should_Return_Null_Exception();
        #endregion

        #region Business Logic Test
        [Fact]
        void Delete_Should_Throw_Dependency_Exception();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "BankBranch")]
    public class BankBranchDeleteTest : BankBranchBaseTest, IBankBranchDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            BankBranchFactory bankBranchFactory = _bankBranchFactory.Create().WithBank().Persist();
            BankBranch bankBranch = _bankBranchService.GetBankBranch(bankBranchFactory.Object.Id);
            var success = _bankBranchService.IsDelete(bankBranch.Id);
            Assert.True(success);
            bankBranch = _bankBranchService.GetBankBranch(bankBranchFactory.Object.Id);
            Assert.Null(bankBranch);
        }

        [Fact]
        public void Delete_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _bankBranchService.IsDelete(0));
        }
        #endregion

        #region Business Logic
        [Fact]
        public void Delete_Should_Throw_Dependency_Exception()
        {
            BankBranchFactory bankBranchFactory =
                    _bankBranchFactory.Create()
                    .WithBank()
                    .Persist();
            BankBranch bankBranch = _bankBranchService.GetBankBranch(bankBranchFactory.Object.Id);
            bankBranch.SupplierBankDetails = new List<SupplierBankDetails>() { new SupplierBankDetails() };
            Assert.Throws<DependencyException>(() => _bankBranchService.IsDelete(bankBranch.Id));
        } 
        #endregion
    }
}
