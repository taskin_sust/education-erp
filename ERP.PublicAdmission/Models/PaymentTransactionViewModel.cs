﻿using System;

namespace UdvashERP.PublicAdmission.Models
{
    public class PaymentTransactionViewModel
    {
        public virtual string Referance { get; set; }
        public virtual string TransactionId { get; set; }
        public virtual string SenderMobile { get; set; }
        public virtual DateTime? TransactionTime { get; set; }
        public virtual decimal? Amount { get; set; } 
    }
}