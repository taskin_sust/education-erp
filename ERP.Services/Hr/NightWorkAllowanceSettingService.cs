using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.Hr
{
    public interface INightWorkAllowanceSettingService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(List<UserMenu> userMenu, NightWorkAllowanceSetting prNightWorkAllowance);
        bool Delete(NightWorkAllowanceSetting entity);

        #endregion

        #region Single Instances Loading Function

        NightWorkAllowanceSetting GetById(long id);
        NightWorkAllowanceSetting GetTeamMemberNightWorkAllowance(long memberId, DateTime dateTime);

        #endregion

        #region List Loading Function

        IList<NightWorkAllowanceSetting> LoadAll(int draw, int start, int length, List<UserMenu> authorizeMenu, long? organizationId, int? empStatus);

        #endregion

        #region Others Function

        int AllowanceCount(List<UserMenu> authorizeMenu, long? organizationId, int? empStatus);

        #endregion

        #region Helper Function

        #endregion
    }
    public class NightWorkAllowanceSettingService : BaseService, INightWorkAllowanceSettingService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("PayrollService");

        #endregion

        #region Properties & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly INightWorkAllowanceSettingDao _prNightWorkAllowanceDao;
        private readonly IEmploymentHistoryDao _employmentHistoryDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;
        private readonly IAllowanceSheetFirstDetailsDao _allowanceSheetFirstDetailsDao;
        public NightWorkAllowanceSettingService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _prNightWorkAllowanceDao = new NightWorkAllowanceSettingDao { Session = session };
            _employmentHistoryDao = new EmploymentHistoryDao() { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao() { Session = session };
            _allowanceSheetFirstDetailsDao = new AllowanceSheetFirstDetailsDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public void SaveOrUpdate(List<UserMenu> authorizeMenu, NightWorkAllowanceSetting prNightWorkAllowance)
        {
            ITransaction transaction = null;
            try
            {
                if (prNightWorkAllowance == null) throw new NullObjectException("Empty Object Exception");
                if (prNightWorkAllowance.MemberEmploymentStatuses == null)
                    throw new InvalidDataException("Invalid Employee Status");
                prNightWorkAllowance = FillUpData(prNightWorkAllowance);
                CheckBeforeSave(authorizeMenu, prNightWorkAllowance);
                CheckValidation(prNightWorkAllowance);
                if (prNightWorkAllowance.Id == 0)
                {
                    EligibleForSave(prNightWorkAllowance);
                    _prNightWorkAllowanceDao.Save(prNightWorkAllowance);
                }
                else
                {
                    prNightWorkAllowance = EligibleForUpdate(prNightWorkAllowance);
                    using (transaction = Session.BeginTransaction())
                    {
                        _prNightWorkAllowanceDao.Update(prNightWorkAllowance);
                        transaction.Commit();
                    }
                }
            }
            catch (AuthenticationException ae)
            {
                throw ae;
            }
            catch (DuplicateEntryException dex)
            {
                throw dex;
            }
            catch (InvalidDataException ide)
            {
                throw ide;
            }
            catch (MessageException me)
            {
                throw me;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public bool Delete(NightWorkAllowanceSetting entity)
        {
            ITransaction transaction = null;
            try
            {
                entity = EligibleForUpdate(entity);
                CheckBeforeDelete(entity);
                using (transaction = Session.BeginTransaction())
                {
                    entity.Status = NightWorkAllowanceSetting.EntityStatus.Delete;
                    _prNightWorkAllowanceDao.Update(entity);
                    transaction.Commit();
                    return true;
                }
            }
            catch (AuthenticationException ae)
            {
                throw ae;
            }
            catch (DuplicateEntryException dex)
            {
                throw dex;
            }
            catch (InvalidDataException ide)
            {
                throw ide;
            }
            catch (MessageException me)
            {
                throw me;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public NightWorkAllowanceSetting GetById(long id)
        {
            try
            {
                if (id <= 0) throw new InvalidDataException("Invalid Id");
                return _prNightWorkAllowanceDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public NightWorkAllowanceSetting GetTeamMemberNightWorkAllowance(long memberId, DateTime dateTime)
        {
            try
            {
                object responsiblePersonDes, responsiblePersonDep, responsiblePersonOrg;
                int empStatus;
                if (memberId <= 0) throw new InvalidDataException("invalid memberid");
                if (dateTime <= DateTime.MinValue || dateTime > DateTime.MaxValue) throw new InvalidDataException("Invalid datetime");
                _employmentHistoryDao.GetCurrentInformations(memberId, out empStatus, out responsiblePersonDes, out responsiblePersonDep, out responsiblePersonOrg, dateTime);
                if (responsiblePersonDes == null) throw new InvalidDataException("Invalid Designation");
                if (responsiblePersonDep == null) throw new InvalidDataException("Invalid Department");
                if (responsiblePersonOrg == null) throw new InvalidDataException("Invalid Organization");
                if (empStatus == 0) throw new InvalidDataException("Invalid Emp Status");
                var org = (Organization)responsiblePersonOrg;
                return _prNightWorkAllowanceDao.GetTeamMemberNightWorkAllowance(org.Id, (int)empStatus, dateTime);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function

        public IList<NightWorkAllowanceSetting> LoadAll(int draw, int start, int length, List<UserMenu> authorizeMenu, long? organizationId, int? empStatus)
        {
            try
            {
                if (authorizeMenu == null) throw new InvalidDataException("invalid user permission");
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authorizeMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                if (authOrgList.Count <= 0) throw new UnauthorizeDataException("Unauthorized data exception");
                return _prNightWorkAllowanceDao.LoadAll(draw, start, length, authOrgList, empStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Others Function

        public int AllowanceCount(List<UserMenu> authorizeMenu, long? organizationId, int? empStatus)
        {
            try
            {
                if (authorizeMenu == null) throw new InvalidDataException("invalid user permission");
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authorizeMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                if (authOrgList.Count <= 0) throw new UnauthorizeDataException("Unauthorized data exception");
                return _prNightWorkAllowanceDao.CountAllowance(authOrgList, empStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Helper function

        private static void CheckValidation(NightWorkAllowanceSetting prNightWorkAllowance)
        {
            var validationResult = ValidationHelper.ValidateEntity<NightWorkAllowanceSetting, NightWorkAllowanceSetting>(prNightWorkAllowance);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private void CheckBeforeSave(List<UserMenu> authorizeMenu, NightWorkAllowanceSetting prNightWorkAllowance)
        {
            if (authorizeMenu == null) throw new AuthenticationException("unauthorized Organization");
            if (prNightWorkAllowance.ClosingDate.ToString().Length > 0)
                if (prNightWorkAllowance.EffectiveDate > prNightWorkAllowance.ClosingDate)
                    throw new InvalidDataException("Closing date must be bigger than effective date");
        }

        private void EligibleForSave(NightWorkAllowanceSetting prNightWorkAllowance)
        {
            var orgId = prNightWorkAllowance.Organization.Id;
            var effectiveDateTime = prNightWorkAllowance.EffectiveDate;
            var allowanceObj = _allowanceSheetDao.GetEligibleAllowanceSheetByOrganizationId(orgId);
            DateTime nightWorkDate = _prNightWorkAllowanceDao.GetLastpostedDateFromNightWork(orgId);
            DateTime maxDate = DateTime.MinValue;
            if (allowanceObj != null && nightWorkDate != DateTime.MinValue)
                maxDate = GetMaxDate(allowanceObj.DateTo, nightWorkDate);

            if (maxDate != DateTime.MinValue && maxDate >= effectiveDateTime)
                throw new MessageException("You can't save this allowance from this effective date because allowance sheet has already been generated for this month!! try another effective date after " + allowanceObj.DateTo.ToString("yyyy MMMM dd"));
            if (allowanceObj != null)
                if (allowanceObj.DateTo >= effectiveDateTime)
                    throw new MessageException("You can't save this allowance from this effective date because allowance sheet has already been generated for this month!! try another effective date after " + allowanceObj.DateTo.ToString("yyyy MMMM dd"));
            if (nightWorkDate != DateTime.MinValue)
                if (nightWorkDate >= effectiveDateTime)
                    throw new MessageException("You can't save this allowance from this effective date because allowance sheet has already been generated for this month!! try another effective date after " + allowanceObj.DateTo.ToString("yyyy MMMM dd"));
            CheckDuplicateEntryWhileSavingAllowance(prNightWorkAllowance);

        }

        private void CheckDuplicateEntryWhileSavingAllowance(NightWorkAllowanceSetting prNightWorkAllowance)
        {
            IList<NightWorkAllowanceSetting> list =
                _prNightWorkAllowanceDao.LoadByOrgAndNameAndEmpStatusAndCalculationOnAndMulFactorAndDate(
                    _commonHelper.ConvertIdToList(prNightWorkAllowance.Organization.Id), prNightWorkAllowance.Name,
                    prNightWorkAllowance.MemberEmploymentStatuses, prNightWorkAllowance.CalculationOn,
                    prNightWorkAllowance.InHouseMultiplyingFactor, prNightWorkAllowance.OutHouseMultiplyingFactor, prNightWorkAllowance.EffectiveDate,
                    null);
            if (list != null && list.Any())
            {
                //others necessary check goes here
                NightWorkAllowanceSetting supportAllowanceSetting = list.OrderByDescending(x => x.EffectiveDate).Take(1).SingleOrDefault();
                if (supportAllowanceSetting.ClosingDate == null) throw new InvalidDataException("You can't update this allowance because an allowance has already been declared from " + supportAllowanceSetting.EffectiveDate.ToString("yyyy MMMM dd"));
                if (supportAllowanceSetting.ClosingDate >= prNightWorkAllowance.EffectiveDate) throw new InvalidDataException("You can't update this allowance because an allowance has already been declared from " + supportAllowanceSetting.EffectiveDate.ToString("yyyy MMMM dd") + " to " + supportAllowanceSetting.ClosingDate.Value.ToString("yyyy MMMM dd") + " Try another date which will be greater than " + supportAllowanceSetting.ClosingDate.Value.ToString("yyyy MMMM dd"));
            }
        }

        private void CheckBeforeDelete(NightWorkAllowanceSetting entity)
        {
            IList<AllowanceSheetFirstDetails> list = _allowanceSheetFirstDetailsDao.LoadByNightWorkAllowanceId(entity.Id);
            if (list.Count > 0) throw new InvalidDataException("Can't change this allowance setting because Allowance sheet has a already been prepared ");
        }

        private DateTime GetMaxDate(DateTime dateTo, DateTime? nightWorkDate)
        {
            if (dateTo > nightWorkDate) return dateTo;
            return (DateTime)nightWorkDate;
        }

        private NightWorkAllowanceSetting EligibleForUpdate(NightWorkAllowanceSetting prNightWorkAllowance)
        {
            var orgId = prNightWorkAllowance.Organization.Id;
            var nightworkAllowanceId = prNightWorkAllowance.Id;
            var closingdateTime = prNightWorkAllowance.ClosingDate;
            prNightWorkAllowance = CheckDuplicateEntryWhileUpdatingAllowance(prNightWorkAllowance);
            AllowanceSheet obj = _allowanceSheetDao.GetEligibleAllowanceSheetByOrganizationId(orgId);
            if (obj != null && obj.DateTo != DateTime.MinValue)
                if (obj.DateTo > closingdateTime) throw new InvalidDataException("closing date must be greater than " + obj.DateTo.ToString("yyyy MMMM dd"));
            return prNightWorkAllowance;
        }

        private NightWorkAllowanceSetting FillUpData(NightWorkAllowanceSetting prNightWorkAllowance)
        {
            foreach (var empStatus in prNightWorkAllowance.MemberEmploymentStatuses)
            {
                switch (empStatus)
                {
                    case MemberEmploymentStatus.Contractual:
                        prNightWorkAllowance.IsContractual = true;
                        break;
                    case MemberEmploymentStatus.PartTime:
                        prNightWorkAllowance.IsPartTime = true;
                        break;
                    case MemberEmploymentStatus.Probation:
                        prNightWorkAllowance.IsProbation = true;
                        break;
                    case MemberEmploymentStatus.Permanent:
                        prNightWorkAllowance.IsPermanent = true;
                        break;
                    case MemberEmploymentStatus.Intern:
                        prNightWorkAllowance.IsIntern = true;
                        break;
                }
            }
            return prNightWorkAllowance;
        }

        private NightWorkAllowanceSetting CheckDuplicateEntryWhileUpdatingAllowance(NightWorkAllowanceSetting prNightWorkAllowance)
        {
            Session.Clear();
            var proxyObj = _prNightWorkAllowanceDao.LoadById(prNightWorkAllowance.Id);
            IList<NightWorkAllowanceSetting> list = _prNightWorkAllowanceDao.LoadByOrgAndNameAndEmpStatusAndCalculationOnAndMulFactorAndDate(
                   _commonHelper.ConvertIdToList(prNightWorkAllowance.Organization.Id), prNightWorkAllowance.Name,
                   prNightWorkAllowance.MemberEmploymentStatuses, prNightWorkAllowance.CalculationOn, prNightWorkAllowance.InHouseMultiplyingFactor,
                   prNightWorkAllowance.OutHouseMultiplyingFactor, prNightWorkAllowance.EffectiveDate, null, prNightWorkAllowance.Id);
            NightWorkAllowanceSetting prevObj = list.Where(x => x.ClosingDate < proxyObj.EffectiveDate).OrderByDescending(x => x.EffectiveDate).Take(1).SingleOrDefault<NightWorkAllowanceSetting>();
            NightWorkAllowanceSetting nextObj = list.Where(x => x.EffectiveDate > proxyObj.ClosingDate).OrderBy(x => x.EffectiveDate).Take(1).SingleOrDefault<NightWorkAllowanceSetting>();
            if (prevObj != null)
            {
                if (prevObj.ClosingDate == null)
                    throw new InvalidDataException("You can't update this allowance because an allowance has already been declared from " + prevObj.EffectiveDate.ToString("yyyy MMMM dd"));
                if (prevObj.ClosingDate >= prNightWorkAllowance.EffectiveDate || prevObj.EffectiveDate > prNightWorkAllowance.ClosingDate)
                    throw new InvalidDataException("You can't update this allowance because an allowance has already been declared from " + prevObj.EffectiveDate.ToString("yyyy MMMM dd") + " to " + prevObj.ClosingDate.Value.ToString("yyyy MMMM dd") + " Try another date which will be greater than " + prevObj.ClosingDate.Value.ToString("yyyy MMMM dd"));
            }
            if (nextObj != null)
            {
                if (nextObj.EffectiveDate <= prNightWorkAllowance.ClosingDate || nextObj.EffectiveDate <= prNightWorkAllowance.EffectiveDate)
                    throw new InvalidDataException("You can't update this allowance because an allowance has already been declared from " + nextObj.EffectiveDate.ToString("yyyy MMMM dd") + " to " + nextObj.ClosingDate.Value.ToString("yyyy MMMM dd") + " Try another date which will be greater than " + nextObj.ClosingDate.Value.ToString("yyyy MMMM dd"));
            }
            proxyObj.MemberEmploymentStatuses = prNightWorkAllowance.MemberEmploymentStatuses;
            proxyObj.Name = prNightWorkAllowance.Name;
            proxyObj.CalculationOn = prNightWorkAllowance.CalculationOn;
            proxyObj.InHouseMultiplyingFactor = Convert.ToDecimal(prNightWorkAllowance.InHouseMultiplyingFactor);
            proxyObj.OutHouseMultiplyingFactor = Convert.ToDecimal(prNightWorkAllowance.OutHouseMultiplyingFactor);
            proxyObj.EffectiveDate = prNightWorkAllowance.EffectiveDate;
            proxyObj.ClosingDate = prNightWorkAllowance.ClosingDate;
            proxyObj = FillUpData(proxyObj);
            return proxyObj;
        }

        #endregion
    }
}

