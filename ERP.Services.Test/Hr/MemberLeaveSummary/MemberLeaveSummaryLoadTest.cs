﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MemberLeaveSummary
{
    [Trait("Area", "Hr")]
    [Trait("Service", "MemberLeaveSummary")]
    public class MemberLeaveSummaryLoadTest:MemberLeaveSummaryBaseTest
    {

        #region GetLeaveSummary

        [Fact]
        public void Should_Return_GetLeaveSummary_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.NotNull(_membersLeaveSummaryService.GetLeaveSummary(org.Id, tm.Id, leaveList[0].Id));

        }

        [Fact]
        public void Should_Return_GetLeaveSummaryList_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.NotEmpty(_membersLeaveSummaryService.GetLeaveSummary(org.Id,tm.Id,lvStartDate.Year));

        }

        [Fact]
        public void Should_Return_GetLeaveSummary_Null_Object()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.Null(_membersLeaveSummaryService.GetLeaveSummary(org.Id + 1, tm.Id + 1, leaveList[0].Id));
        }

        [Fact]
        public void Should_Return_GetLeaveSummaryList_Null_Object()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.Empty(_membersLeaveSummaryService.GetLeaveSummary(org.Id+1,tm.Id+1,lvStartDate.Year));
        }


        #endregion

        #region LoadByMemberAndYear

        [Fact]
        public void Should_Return_LoadByMemberAndYear_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.NotNull(_membersLeaveSummaryService.LoadByMemberAndYear(tm.Id, lvStartDate.Year));

        }

        [Fact]
        public void Should_Return_LoadByMemberAndYear_Empty()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.Empty(_membersLeaveSummaryService.LoadByMemberAndYear(tm.Id+1, lvStartDate.Year+1));

        }

        #endregion

        #region LoadMemberLeaveSumarryForTransfer
        
        [Fact]
        public void Should_Return_LoadMemberLeaveSumarryForTransfer_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.NotEmpty(_membersLeaveSummaryService.LoadMemberLeaveSumarryForTransfer(tm.Id, lvStartDate.Year));

        }

        [Fact]
        public void Should_Return_LoadMemberLeaveSumarryForTransfer_Empty()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.Empty(_membersLeaveSummaryService.LoadMemberLeaveSumarryForTransfer(tm.Id+1, lvStartDate.Year+1));

        }
        
        #endregion

        #region LoadMemberLeaveSummaryByOrgLeaveMemberYear

        [Fact]
        public void Should_Return_LoadMemberLeaveSummaryByOrgLeaveMemberYear_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.NotEmpty(_membersLeaveSummaryService.LoadMemberLeaveSummaryByOrgLeaveMemberYear(lvStartDate.Year, org.Id, new List<long>() { tm.Id }, new List<long>() { leaveList[0] .Id}));

        }

        [Fact]
        public void Should_Return_LoadMemberLeaveSummaryByOrgLeaveMemberYear_Empty()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.Empty(_membersLeaveSummaryService.LoadMemberLeaveSummaryByOrgLeaveMemberYear(lvStartDate.Year+1, org.Id+1, new List<long>() { tm.Id+1 }, new List<long>() { leaveList[0].Id }));

        }

        #endregion

        #region GetLeaveSummary OverLoad

        [Fact]
        public void Should_Return_GetLeaveSummaryOverLoad_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.NotNull(_membersLeaveSummaryService.GetLeaveSummary(org.Id, tm.Id, leaveList[0].Id, lvStartDate.Year));

        }

        [Fact]
        public void Should_Return_GetLeaveSummaryOverLoad_Empty()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();
            Assert.Null(_membersLeaveSummaryService.GetLeaveSummary(org.Id + 1, tm.Id + 1, leaveList[0].Id + 1, lvStartDate.Year + 1));

        }

        #endregion

    }
}