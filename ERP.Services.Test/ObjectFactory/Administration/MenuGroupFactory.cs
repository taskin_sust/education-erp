﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class MenuGroupFactory : ObjectFactoryBase<MenuGroup>
    {
        #region Object Initialization

        protected readonly IMenuService _menuService;

        public MenuGroupFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _menuService = new MenuService(_session);
        }

        #endregion

        #region Create

        public MenuGroupFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }
        public MenuGroupFactory WithDisplayText(string displaytext)
        {
            Object.DisplayText = displaytext;
            return this;
        }
        public MenuGroupFactory Create()
        {
            Object = new MenuGroup()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                DisplayText = Guid.NewGuid().ToString().Substring(0, 5),
                Status = MenuGroup.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }


        public MenuGroupFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _menuService.Save(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _menuService.Save(item);
                    }
                }
            }
            return this;
        }

        public MenuGroupFactory CreateMore(int numberOfmenugroup)
        {
            CreateMenuGroupList(numberOfmenugroup);
            return this;
        }

        private List<MenuGroup> CreateMenuGroupList(int numberOfmenugroup)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<MenuGroup>();
            for (int i = 0; i < numberOfmenugroup; i++)
            {
                var menugroup = new MenuGroup()
                {
                    Id = 0,
                    Name = "_" + i,
                    DisplayText = Guid.NewGuid() + "displaytext_" + i,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = MenuGroup.EntityStatus.Active
                };
                list.Add(menugroup);
            }
            ObjectList.AddRange(list);
            return list;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void CleanupMenuGroup(List<long> menugroupIdList)
        {
            _session.CreateSQLQuery("Delete from MenuGroup where id in(" + string.Join(", ", menugroupIdList) + ")").ExecuteUpdate();
        }

        public void Cleanup()
        {
            DeleteAll();
        }

        #endregion

    }
}
