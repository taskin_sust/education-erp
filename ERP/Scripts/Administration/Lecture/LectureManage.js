$(document).ready(function () {

        function dataTableRender() {
            var pageSize = 10;
            if ($('#pageSize').val() != "" && /^\d+$/.test($('#pageSize').val())) {
                pageSize = parseInt($('#pageSize').val());
            }
            else {
                $('#pageSize').val(pageSize);
            }
            $('#DataGrid').dataTable({
                destroy: true,
                "processing": true,
                searching: false,
                serverSide: true,
                "scrollX": true,
                "bLengthChange": false,
                "iDisplayLength": pageSize,
                //stateSave: true,
                order: [[0, "desc"]],
                ajax: {
                    url: "LectureListResult",
                    type: 'POST',
                    data: function (d) {
                        d.organization = $('#Organization').val();
                        d.program = $('#Program').val();
                        d.session = $('#Session').val();
                        d.course = $('#Course').val();                        
                        d.numberOfClasses = $('#NumberOfClasses').val();
                        d.classNamePrefix = $('#ClassNamePrefix').val();
                    }
                }
            });
        }
        
        dataTableRender();

        $(document).on("click", "#search", function () {
            dataTableRender();
        });
    //$("body").attr("data-project-root") + "Administration/Lecture/LectureListResult",
        //$(document).on("click", ".glyphicon-arrow-up, .glyphicon-arrow-down", function () {
        //    var id = $(this).attr("id");
        //    var current = $(this).parent().text().trim();
        //    var action = $(this).attr("data-action");
        //    //glyphicon glyphicon-arrow-up
        //    $.ajax({
        //        type: "post",
        //        url: $("body").attr("data-project-root") + "Administration/MenuGroup/RankChange",
        //        cache: false,
        //        async: true,
        //        data: { "id": id, "current": current, "action": action },
        //        success: function (result) {
        //            if (result.IsSuccess) {
        //                dataTableRender();
        //            }
        //            else {                        
        //                bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
        //            }
        //        },
        //        error: function (result) {
        //            bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
        //        }
        //    });

        //});


        $(document).on("click", ".glyphicon-trash", function () {
            var id = $(this).attr("id");
            console.log(id);
            if (id != "") {
                bootbox.deleteConfirm("<h3>Are you sure you want to delete this <span class='confirm-message'></span> Lecture Settings?</h3>", function (result) {
                    if (result) {
                        $.ajax({
                            type: "post",
                            url: $("body").attr("data-project-root") + "Administration/Lecture/Delete",
                            cache: false,
                            async: true,
                            data: { "id": id },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (result) {
                                $.unblockUI();

                                if (result.IsSuccess) {                                   
                                    $.fn.customMessage({
                                        displayMessage: "Lecture Delete Successfully",
                                        displayMessageType: "success",
                                    });
                                    dataTableRender();
                                    //customMessage
                                }
                                else {
                                    $.fn.customMessage({
                                        displayMessage: result.Message,
                                        displayMessageType: "e",
                                    });                                    
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (result) {
                                $.fn.customMessage({
                                    displayMessage: "Lecture Delete Failed",
                                    displayMessageType: "e",
                                });
                            }
                        });

                    }
                }).css({ 'margin-top': (($(window).height() / 4)) });
            }
            return false;
        });
});


$(document).on('change', '#Organization', function (e) {
    $("#Program option").remove();
    $('#Program').append("<option value=''>All</option>");
    $("#Session option").remove();
    $('#Session').append("<option value=''>All</option>");
    $("#Course option").remove();
    $('#Course').append("<option value=''>All</option>");
    //$("#Subject option").remove();
    //$('#Subject').append("<option value=''>All</option>");
    var orgId = $("#Organization").val();
    if (orgId != "") {        
        $.ajax({
            type: "post",
            //url: $("body").attr("data-project-root") + "Administration/Lecture/GetProgramByOrg",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
            cache: false,
            async: true,
            data: { organizationIds: orgId, isAuthorized: true },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                //$.unblockUI();
                //if (result.IsSuccess) {
                //    $.each(result.returnList, function (i, v) {
                //        $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                //    });
                //} else {
                //    $.fn.customMessage({
                //        displayMessage: result.Message,
                //        displayMessageType: "error",
                //    });
                //}

                $.unblockUI();
                if (response.IsSuccess) {
                    $.each(response.returnProgramList, function (i, v) {
                        $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "error",
                    });
                }

            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }

});

$(document).on('change', '#Program', function (e) {
    $("#Session option").remove();
    $('#Session').append("<option value=''>All</option>");
    $("#Course option").remove();
    $('#Course').append("<option value=''>All</option>");
    //$("#Subject option").remove();
    //$('#Subject').append("<option value=''>All</option>");
    var organizationIds = $("#Organization").val();
    var prId = $("#Program").val();
    if (prId != "") {
       
        $.ajax({
            type: "post",
            //url: $("body").attr("data-project-root") + "Administration/Lecture/GetSessionByProgram",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
            cache: false,
            async: true,
            data: { organizationIds: organizationIds, programIds: prId, isAuthorized: true },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                //$.unblockUI();
                //if (result.IsSuccess) {
                //    $.each(result.returnList, function (i, v) {
                //        $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                //    });
                //} else {
                //    $.fn.customMessage({
                //        displayMessage: result.Message,
                //        displayMessageType: "error",
                //    });
                //}

                if (response.IsSuccess) {
                    $.each(response.returnSessionList, function (i, v) {
                        $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "error",
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (response) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});
$(document).on('change', '#Session', function (e) {
    $("#Course option").remove();
    $('#Course').append("<option value=''>All</option>");
    //$("#Subject option").remove();
    //$('#Subject').append("<option value=''>All</option>");
    var organizationIds = $("#Organization").val();
    var sId = $("#Session").val();
    var prId = $("#Program").val();
    if (sId != "" && prId != "") {       
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
            cache: false,
            async: true,
            data: { organizationIds: organizationIds, programIds: prId, sessionIds: sId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                //$.unblockUI();
                //if (result.IsSuccess) {
                //    $.each(result.returnList, function (i, v) {
                //        $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                //    });
                //}
                //else {
                //    $.fn.customMessage({
                //        displayMessage: result.Message,
                //        displayMessageType: "error",
                //    });
                //}
                $.unblockUI();
                if (response.IsSuccess) {
                    $.each(response.returnCourse, function (i, v) {
                        $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "error",
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});

//$(document).on('change', '#Course', function (e) {
//    $("#Subject option").remove();
//    $('#Subject').append("<option value=''>All</option>");
//    var sId = $("#Session").val();
//    var prId = $("#Program").val();
//    var cId = $("#Course").val();

//    if (sId != "" && prId != "" && cId != "") {
//        $("#Course").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
//        $("#Course").removeClass("highlight");
//        $.ajax({
//            type: "post",
//            url: $("body").attr("data-project-root") + "Administration/Lecture/GetSubjectByCourseSessionProgram",
//            cache: false,
//            async: true,
//            data: { "programId": prId, "sessionId": sId, "courseId": cId,"action":"forDropDownList" },
//            beforeSend: function () {
//                $.blockUI({
//                    timeout: 0,
//                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
//                });
//            },
//            success: function (result) {
//                $.unblockUI();
//                if (result.IsSuccess) {
//                    $.each(result.returnList, function (i, v) {
//                        $('#Subject').append($('<option>').text(v.Text).attr('value', v.Value));
//                    });
//                }
//                else {
//                    $.fn.customMessage({
//                        displayMessage: result.Message,
//                        displayMessageType: "error",
//                    });
//                }
//            },
//            complete: function () {
//                $.unblockUI();
//            },
//            error: function (result) {
//                $.unblockUI();
//                console.log("Failed -----");
//            }
//        });
//    }
//});