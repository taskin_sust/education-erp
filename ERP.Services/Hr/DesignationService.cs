﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
namespace UdvashERP.Services.Hr
{
    public interface IDesignationService : IBaseService
    {
        #region Operational Function
        //bool Save(Designation hrDesignation);
        //bool Update(Designation hrDepartment);
        bool Delete(Designation hrDepartment);
        bool DesignationSaveUpdate(Designation hrDepartment);
        #endregion

        #region Single Instances Loading Function
        Designation LoadById(long id);

        #endregion

        #region List Loading Function

        Designation GetDesignationByStatus(int status);
        IList<Designation> LoadActive(List<UserMenu> userMenu, int start, int length, string orderBy, string orderDir, string organizationId, string status);
        IList<Designation> LoadDesignation(List<long> organizationIds);
        IList<Designation> LoadDesignation(List<long> organizationIds, DateTime effectiveDate);

        #endregion

        #region Others Function
        int TotalRowCountOfDepartmentSettings(List<UserMenu> userMenu, string organizationId, string status);
        bool HasDuplicateByRank(int rank, long organizationId, long? id);
        #endregion

        #region Helper Function
        bool HasDuplicateByDesignationName(string name, long organizationId, long? id);
        bool HasDuplicateByDesignationShortName(string shortName, long organizationId, long? id);
        #endregion

    }
    public class DesignationService : BaseService, IDesignationService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IDesignationDao _hrDesignationDao;
        public DesignationService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrDesignationDao = new DesignationDao() { Session = session };
        }
        #endregion

        #region Operational Functions
        /*

        public bool DesignationSaveUpdate(List<UserMenu> userMenu, Designation entity)
        {
            if (userMenu == null)
            {
                throw new InvalidDataException("Permission related information missing!");
            }
            if (entity == null)
            {
                throw new InvalidDataException("Designation not found!");
            }
            DoBeforeSaveOrUpdate(entity);
            return DesignationSaveUpdate(entity);
        }
        */

        public bool DesignationSaveUpdate(Designation hrDesignation)
        {
            ITransaction transaction = null;
            bool isSuccess = false;
            try
            {
                DoBeforeSaveOrUpdate(hrDesignation);
                var designationEntity = new Designation();
                if (hrDesignation.Id > 0)
                {
                    designationEntity = _hrDesignationDao.LoadById(hrDesignation.Id);
                }
                else if (hrDesignation.Id <= 0)
                {
                    hrDesignation.Status = Designation.EntityStatus.Active;
                }
                designationEntity.Organization = hrDesignation.Organization;
                designationEntity.Name = hrDesignation.Name;
                designationEntity.Rank = hrDesignation.Rank;
                designationEntity.ShortName = hrDesignation.ShortName;
                designationEntity.Status = hrDesignation.Status;
                if (!string.IsNullOrEmpty(hrDesignation.BusinessId))
                {
                    designationEntity.BusinessId = hrDesignation.BusinessId;
                }

                using (transaction = Session.BeginTransaction())
                {
                    _hrDesignationDao.SaveOrUpdate(designationEntity);
                    transaction.Commit();
                    isSuccess = true;
                    hrDesignation.Id = designationEntity.Id;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                {
                    transaction.Rollback();
                }
            }
            return isSuccess;
        }
        public bool Delete(Designation hrDesignation)
        {
            ITransaction transaction = null;
            bool isSuccess = false;
            try
            {
                DoBeforeDelete(hrDesignation);
                using (transaction = Session.BeginTransaction())
                {

                    hrDesignation.Status = Department.EntityStatus.Delete;
                    _hrDesignationDao.Update(hrDesignation);
                    transaction.Commit();
                    isSuccess = true;
                }
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                isSuccess = false;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
            return isSuccess;
        }
        #endregion

        #region Single Instances Loading Function
        public Designation LoadById(long id)
        {
            try
            {
                DoBeforeLoadById(id);
                return _hrDesignationDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Designation GetDesignationByStatus(int status)
        {
            try
            {
                return _hrDesignationDao.GetDesignationByStatus(status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function

        public IList<Designation> LoadActive(List<UserMenu> userMenu, int start, int length, string orderBy, string orderDir, string organizationId, string status)
        {
            try
            {
                long? convertedOrganizationId = organizationId == "" ? (long?)null : Convert.ToInt32(organizationId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                return _hrDesignationDao.LoadActive(start, length, orderBy, orderDir, organizationIdList, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Designation> LoadDesignation(List<long> organizationIds)
        {
            try
            {
                var designationList = _hrDesignationDao.LoadHrDesignationList(organizationIds);
                return designationList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Designation> LoadDesignation(List<long> organizationIds, DateTime effectiveDate)
        {
            try
            {
                var designationList = _hrDesignationDao.LoadHrDesignationList(organizationIds, effectiveDate);
                return designationList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        public int TotalRowCountOfDepartmentSettings(List<UserMenu> userMenu, string organizationId, string status)
        {
            try
            {
                long? convertedOrganizationId = organizationId == "" ? (long?)null : Convert.ToInt32(organizationId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                return _hrDesignationDao.TotalRowCountOfDepartmentSettings(organizationIdList, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public bool HasDuplicateByRank(int rank, long organizationId, long? id)
        {
            try
            {
                return _hrDesignationDao.HasDuplicateByRank(rank, organizationId, id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }



        public bool HasDuplicateByDesignationName(string name, long organizationId, long? id)
        {
            try
            {
                return _hrDesignationDao.HasDuplicateByDesignationName(name, organizationId, id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool HasDuplicateByDesignationShortName(string shortName, long organizationId, long? id)
        {
            try
            {
                return _hrDesignationDao.HasDuplicateByDesignationShortName(shortName, organizationId, id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function
        private void DoBeforeSaveOrUpdate(Designation hrDesignation)
        {
            if (hrDesignation == null)
            {
                throw new NullObjectException();
            }
            var validationResult = ValidationHelper.ValidateEntity<Designation, DesignationMetaData>(hrDesignation);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
            if (hrDesignation.Rank <= 0)
            {
                throw new InvalidDataException("Invalid designation rank");
            }
            var isDesignationExistByName = _hrDesignationDao.HasDuplicateByDesignationName(hrDesignation.Name, hrDesignation.Organization.Id, hrDesignation.Id);
            if (!isDesignationExistByName)
            {
                throw new DuplicateEntryException("Duplicate designation name found");
            }
            var isOrganizationRankExists = _hrDesignationDao.HasDuplicateByRank((int)hrDesignation.Rank, hrDesignation.Organization.Id, hrDesignation.Id);
            if (!isOrganizationRankExists)
            {
                throw new DuplicateEntryException("Duplicate rank found");
            }
        }
        private void DoBeforeDelete(Designation hrDesignation)
        {
            bool dependencyFound = _hrDesignationDao.HasDependency(hrDesignation);
            if (dependencyFound)
                throw new DependencyException("HasDependency");
        }
        private void DoBeforeLoadById(long id)
        {
            if (id < 1)
            {
                throw new NullObjectException("InvalidDesignationSearch");
            }
        }
        #endregion

    }
}
