using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class TdsHistoryMap : BaseClassMap<TdsHistory,long> {

        public TdsHistoryMap()
        {
			Table("HR_TdsHistory");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
            Map(x => x.TdsAmount).Column("TdsAmount");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
			HasMany(x => x.TdsHistoryLog).KeyColumn("TdsHistoryId");
        }
    }
}
