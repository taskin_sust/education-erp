﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class SupplierItemMap : BaseClassMap<SupplierItem, long>
    {
        public SupplierItemMap()
        {
            Table("UINV_SupplierItem");
            LazyLoad();
            References(x => x.Item).Column("ItemId");
            References(x => x.Supplier).Column("SupplierId");
        }

    }
}
