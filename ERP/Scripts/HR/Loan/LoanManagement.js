﻿//zakat
var approvedRefundsuccess = true;
var zakatEdCdSuccess;
var csrEdCdSuccess;
//parse string to date
function ParseDateString(dateString) {
    console.log(dateString);
    var dateArray = dateString.split(" ");
    var month = dateArray[0];
    var year = dateArray[1];
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var monthNumber = $.inArray(month, months);
    var date = new Date(year, monthNumber, "01");
    console.log(date);
    date.setHours(0, 0, 0, 0);
    return date;
}

//find date difference by month
function FindMonthDifferenceOfDates(dateFrom, dateTo) {
    console.log("hello =>>> " + (dateTo.getMonth() - dateFrom.getMonth() + 1)
        + (12 * (dateTo.getFullYear() - dateFrom.getFullYear())));

    return (dateTo.getMonth() - dateFrom.getMonth() + 1)+ (12 * (dateTo.getFullYear() - dateFrom.getFullYear()));

}

function ParseFormValidation() {
    $("#createLoanApplicationForm").data("validator", null);
    $.validator.unobtrusive.parse($("#createLoanApplicationForm"));
}
//disable closing date field
function DisableClosingDateField(dateField) {
    dateField.prop('disabled', true);
}

//enable closing date field
function EnableClosingDateField(dateField) {
    dateField.prop('disabled', false);
}

$(document).ready(function () {
    //parse date to string
    function ParseDateToString(date) {
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var monthNo = date.getMonth();
        var monthName = months[monthNo];
        var year = date.getFullYear();
        var dateString = monthName + " " + year;
        return dateString;
    }

    //Calculate approximate closing month
    Date.prototype.addMonths = function (m) {
        var d = new Date(this);
        var years = Math.floor(m / 12);
        var months = m - (years * 12);
        if (years) d.setFullYear(d.getFullYear() + years);
        if (months) d.setMonth(d.getMonth() + months);
        return d;
    }
    function CalculateApproximateClosingMonth() {
        var totalLoan = $('#loanPanelBody').find('#TotalLoanBalance');
        var totalLoanAmount = 0;
        if (totalLoan.val() !== "" && totalLoan.val() !== null && !isNaN(totalLoan.val())) {
            totalLoanAmount = parseFloat(totalLoan.val());
        }
        var newMonthlyRefund = $('#loanPanelBody').find('#NewMonthlyRefundAmount');
        var newMonthlyRefundAmount = 0;
        if (newMonthlyRefund.val() !== "" && newMonthlyRefund.val() !== null && !isNaN(newMonthlyRefund.val())) {
            newMonthlyRefundAmount = parseFloat(newMonthlyRefund.val());
        }
        var newRefundStartDateField = $('#loanPanelBody').find('#NewRefundStartDate');
        var newRefundStartDateValue = "";
        if (newRefundStartDateField.val() !== "" && newRefundStartDateField.val() !== null) {
            newRefundStartDateValue = newRefundStartDateField.val();
            console.log(newRefundStartDateValue);
        } else {
            console.log("else condition "+newRefundStartDateValue);
            return;
        }
        var approximateClosingDateField = $('#loanPanelBody').find('#ApproximateClosingDate');
        var approximateClosingDate = new Date();
        var approximateNoOfMonth = parseFloat(totalLoanAmount / newMonthlyRefundAmount);
        approximateNoOfMonth = Math.ceil(approximateNoOfMonth);
        var newRefundStartDate = ParseDateString(newRefundStartDateValue);
        console.log(newRefundStartDate);
        approximateClosingDateField.val("");
        if (newMonthlyRefundAmount == totalLoanAmount) {
            //console.log(newMonthlyRefundAmount);
            //console.log(totalLoanAmount);
            approximateClosingDateField.val(newRefundStartDateValue);
        }
        else if (newMonthlyRefundAmount > 0) {
            tempApproximateClosingDate = newRefundStartDate.addMonths(approximateNoOfMonth - 1);
            approximateClosingDate.setMonth(tempApproximateClosingDate.getMonth());
            approximateClosingDate.setFullYear(tempApproximateClosingDate.getFullYear());
            var approximateCloasinDateString = ParseDateToString(approximateClosingDate);
            approximateClosingDateField.val(approximateCloasinDateString);
        }

    }

    function CalculateTotalLoan() {
        var currentLoan = $('#loanPanelBody').find('#CurrentLoanBalance');
        var newLoan = $('#loanPanelBody').find('#NewLoanApprovedAmount');
        var totalLoan = $('#loanPanelBody').find('#TotalLoanBalance');
        var totalLoanAmount = 0;
        var currentLoanAmount = 0;
        var newLoanAmount = 0;

        if (currentLoan.val() !== "" && currentLoan.val() !== null && !isNaN(currentLoan.val())) {
            currentLoanAmount = parseFloat(currentLoan.val());
        }
        if (newLoan.val() !== "" && newLoan.val() !== null && !isNaN(newLoan.val())) {
            newLoanAmount = parseFloat(newLoan.val());
        }
        totalLoanAmount = parseFloat(currentLoanAmount + newLoanAmount);
        totalLoan.val(totalLoanAmount);
        CalculateApproximateClosingMonth();
    }

  
    //loan
    $('#loanCheckbox').on("click", function () {
        var ss = $('#loanPanelBody').find('#NewRefundStartDate').val();
        console.log(ss);
        var chkbox = $(this);
        if (chkbox.is(':checked')) {
            var loanDivHtml = $("#loanDiv").html();
            $("#loanPanelBody").html(loanDivHtml);

            //bind datepicker
            $('#loanPanelBody').on('focus', '#NewRefundStartDate', function () {
                var $this = $(this);
                $this.datetimepicker({
                    startDate: new Date(),
                    format: "MM yyyy",
                    startView: "year",
                    minView: "year",
                    autoclose: true
                }).on('changeDate', function (ev) {
                    CalculateApproximateClosingMonth();
                });
            });
            $("#loanPanelBody").show();
        } else {
            $("#loanPanelBody").html("");
            $("#loanPanelBody").hide();
        }
        ParseFormValidation();
        $('#loanPanelBody').find("#CurrentLoanBalance").val($('#currentLoan').val());
        $('#loanPanelBody').find("#CurrentMonthlyRefund").val($('#currentRefund').val());
    });

    //current refund amount on change
    $('#loanPanelBody').on('input propertychange', '#NewLoanApprovedAmount', function () {
        CalculateTotalLoan();
    });

    //new refund amount on change
    $('#loanPanelBody').on('input propertychange', '#NewMonthlyRefundAmount', function () {
        CalculateApproximateClosingMonth();
    });

    //calculate total loan


    $(document).on("blur", "#Pin", function () {
        var pin = $(this).val();
        if (pin !== null && pin !== "") {
            $.ajax({
                type: "POST",
                url: $("body").attr("data-project-root") + "Hr/CommonAjaxHr/GetTeamMemberInformation",
                cache: false,
                async: true,
                data: { pin: pin, isOrganization: true, isBranch: true, isDepartment: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $("#Name").val(response.teamMemberName);
                        $("#Department").val(response.departmentName);
                        $("#Organization").val(response.organizationName);
                        $("#Branch").val(response.branchName);

                        //get current loan and monthly refund
                        $.ajax({
                            type: "POST",
                            url: $("body").attr("data-project-root") + "Hr/LoanManagement/GetCurrentLoan",
                            cache: false,
                            async: true,
                            data: { pin: pin },
                            success: function (response) {
                                if (response.IsSuccess) {
                                    $("#loanPanelBody").find("#CurrentLoanBalance").val(response.CurrentLoanBalance);
                                    $("#loanPanelBody").find("#CurrentMonthlyRefund").val(response.CurrentMonthlyRefund);
                                    $("#currentLoan").val(response.CurrentLoanBalance);
                                    $("#currentRefund").val(response.CurrentMonthlyRefund);
                                    //CalculateTotalLoan();

                                    /*call back function for asking member salary info*/
                                    $.ajax({
                                        type: "POST",
                                        url: $("body").attr("data-project-root") + "Hr/CommonAjaxHr/GetMemberSalaryInformation",
                                        cache: false,
                                        async: true,
                                        data: { pin: pin, isOrganization: true, isBranch: true, isCampus: true },
                                        success: function (response) {
                                            if (response.IsSuccess) {
                                                $("#OrganizationSalary").val(response.organizationName);
                                                $("#BranchSalary").val(response.branchName);
                                                $("#CampusSalary").val(response.campusName);
                                            } else {
                                                $("#Pin").val("");
                                                $("#Name").val("");
                                                $("#Department").val("");
                                                $("#Organization").val("");
                                                $("#Branch").val("");
                                                $("#OrganizationSalary").val("");
                                                $("#BranchSalary").val("");
                                                $("#CampusSalary").val("");
                                                $("#currentLoan").val(0);
                                                $("#currentRefund").val(0);
                                                $.fn.customMessage({
                                                    //displayMessage: response.Message,
                                                    displayMessage: "Salary information not found for this team member",
                                                    displayMessageType: "error",
                                                });
                                            }
                                        },
                                        error: function (response) {
                                        }
                                    });

                                } else {
                                    $("#loanPanelBody").find("#CurrentLoanBalance").val("");
                                    $("#loanPanelBody").find("#CurrentMonthlyRefund").val("");
                                    $("#currentLoan").val(0);
                                    $("#currentRefund").val(0);
                                    $.fn.customMessage({
                                        displayMessage: response.Message,
                                        displayMessageType: "error",
                                    });
                                }
                            },
                            error: function (response) {
                            }
                        });

                        //

                    } else {
                        $("#Pin").val("");
                        $("#Name").val("");
                        $("#Organization").val("");
                        $("#Department").val("");
                        $("#Branch").val("");
                        $("#currentLoan").val(0);
                        $("#currentRefund").val(0);
                        $.fn.customMessage({
                            displayMessage: "Enter valid pin",
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function (response) {
                    $.unblockUI();

                },
                error: function (response) {
                    $.unblockUI();

                }
            });
        } else {
            $("#Pin").val("");
            $("#Name").val("");
            $("#Organization").val("");
            $("#Department").val("");
            $("#Branch").val("");
            $("#currentLoan").val(0);
            $("#currentRefund").val(0);
        }
    });

    //approved amount and monthly refund on change
    //$("#loanPanelBody").find("#NewLoanApprovedAmount").
    $(document).on("input propertychange", "#NewLoanApprovedAmount,#NewMonthlyRefundAmount", function () {
        var currentField = $(this);
        var approved = $("#loanPanelBody").find("#TotalLoanBalance");
        var refund = $("#loanPanelBody").find("#NewMonthlyRefundAmount");
        console.log(approved.val());
        console.log(refund.val());
        var approvedAmount = 0;
        var refundAmount = 0;
        if (approved.val() !== null && approved.val() !== "" && !isNaN(approved.val())) {
            approvedAmount = parseFloat(approved.val());
        }
        if (refund.val() !== null && refund.val() !== "" && !isNaN(approved.val())) {
            refundAmount = refund.val();
        }
        if (approvedAmount < refundAmount) {
            console.log(approvedAmount);
            console.log(refundAmount);
            $("#loanPanelBody").find("#approveRefundErrDiv").remove();
            currentField.parent().after("<div id='approveRefundErrDiv' style='color:red;'>Refund amount can not exceed Total Loan amount.</div>");
            approvedRefundsuccess = false;
        } else {
            $("#loanPanelBody").find("#approveRefundErrDiv").remove();
            approvedRefundsuccess = true;
        }
    });

    //submit
    $("#createLoanBtn").click(function () {
        var form = $('#createLoanApplicationForm');
        //|| !approvedRefundsuccess
        if (!$('#createLoanApplicationForm').data('unobtrusiveValidation').validate() || !approvedRefundsuccess) {
            console.log("Inalid return problem");
            return false;
        } else {
            var token = $('input[name="__RequestVerificationToken"]', form).val();
            var hasZakat = $('#zakatCheckbox').is(':checked');
            var hasCsr = $('#csrCheckbox').is(':checked');
            var hasLoan = $('#loanCheckbox').is(':checked');
            if (hasZakat == false && hasCsr == false && hasLoan == false) {
                $.fn.customMessage({
                    displayMessage: "select atleast one criteria",
                    displayMessageType: "error",
                });
                return false;
            }
            var loanManagement = {};
            loanManagement.Pin = $("#Pin").val();
            loanManagement.RequestedAmount = $("#RequestedAmount").val();
            loanManagement.Reason = $("#Reason").val();
            loanManagement.ZakatPaymentType = $("#zakatPanelBody").find("#ZakatPaymentType").val();
            loanManagement.ZakatAmount = $("#zakatPanelBody").find("#ZakatAmount").val();
            loanManagement.ZakatEffectiveDate = $("#zakatPanelBody").find("#ZakatEffectiveDate").val();
            loanManagement.ZakatClosingDate = $("#zakatPanelBody").find("#ZakatClosingDate").val();
            loanManagement.ZakatFundedBy = $("#zakatPanelBody").find("#ZakatFundedBy").val();
            loanManagement.CsrPaymentType = $("#csrPanelBody").find("#CsrPaymentType").val();
            loanManagement.CsrAmount = $("#csrPanelBody").find("#CsrAmount").val();
            loanManagement.CsrEffectiveDate = $("#csrPanelBody").find("#CsrEffectiveDate").val();
            loanManagement.CsrClosingDate = $("#csrPanelBody").find("#CsrClosingDate").val();
            loanManagement.NewLoanApprovedAmount = $("#loanPanelBody").find("#NewLoanApprovedAmount").val();
            loanManagement.NewMonthlyRefundAmount = $("#loanPanelBody").find("#NewMonthlyRefundAmount").val();
            loanManagement.NewRefundStartDate = $("#loanPanelBody").find("#NewRefundStartDate").val();
            $.ajax({
                type: "POST",
                url: $("body").attr("data-project-root") + "Hr/LoanManagement/Create",
                cache: false,
                async: true,
                data: { __RequestVerificationToken: token, loanManagement: loanManagement, hasZakat: hasZakat, hasCsr: hasCsr, hasLoan: hasLoan },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$("#createLoanBtn").prop('disabled', true);
                        $('#createLoanApplicationForm')[0].reset();
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "success",
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                        // $("html, body").animate({ scrollTop: 0 }, "slow");
                    }
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                },
                complete: function (response) {
                    $.unblockUI();

                },
                error: function (response) {
                    $.unblockUI();

                }
            });
        }
    });
});