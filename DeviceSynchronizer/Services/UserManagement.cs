﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceSynchronizer.Dao;
using DeviceSynchronizer.DeviceHelper;
using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model;
using DeviceSynchronizer.Model.ApiModel;
using DeviceSynchronizer.Model.BusinessModel;
using log4net;
using DeviceSynchronizer.ApiReferences;

namespace DeviceSynchronizer.Services
{
    public class UserManagement
    {
        #region Declaration

        private static readonly ILog Log = LogManager.GetLogger("ServiceLogger");
        private readonly AttendanceDeviceModel _attendanceDevice;
        public readonly ServiceStatus _serviceStatus;
        private MessagePass ThrowMessage;
        private IAttendanceDeviceDriver _attendanceDeviceDriver;

        #endregion

        #region Constructor

        public UserManagement(AttendanceDeviceModel attendanceDevice, IAttendanceDeviceDriver attendanceDeviceDriver, ServiceStatus serviceStatus, MessagePass throwMessage)
        {
            this._attendanceDevice = attendanceDevice;
            this._attendanceDeviceDriver = attendanceDeviceDriver;
            this._serviceStatus = serviceStatus;
            this.ThrowMessage = throwMessage;
        }

        #endregion

        #region Helper

        protected virtual void OnThrowingMessage(string message, MessageType messageType)
        {
            if (ThrowMessage != null)
            {
                ThrowMessage(message, messageType);
            }
        }

        #endregion

        #region Operation

        //Register user to device and save to database 
        internal bool RegisterUser()
        {
            try
            {
                var errorPins = new List<AttendanceErrorInfoModel>();

                //User add
                foreach (var memberViewModel in _attendanceDevice.TeamMembers)
                {
                    if (_serviceStatus.IsRunning == false)
                        return false;

                    var isSuccess = _attendanceDeviceDriver.SaveToDevice(memberViewModel, false, _attendanceDevice.MachineNo);
                    if (isSuccess)
                    {
                        memberViewModel.SaveToDeviceSuccessed = true;
                        OnThrowingMessage("Member " + memberViewModel.Name + " operation completed ", MessageType.Success);
                    }
                    else
                    {
                        AttendanceErrorInfoModel errorInfo = new AttendanceErrorInfoModel();
                        errorInfo.DeviceId = _attendanceDevice.Id;
                        errorInfo.Pin = memberViewModel.Pin;
                        errorInfo.EnrollNo = memberViewModel.EnrollNo;
                        errorInfo.LogTime =Convert.ToDateTime( _attendanceDevice.LastUpdateTime);
                        errorPins.Add(errorInfo);
                    }
                   
                }

                //Refresh data at device
                _attendanceDeviceDriver.RefreshData();

                //notify online for error pin that cant save in device (if any)
                if (errorPins.Count > 0)
                {
                    OnThrowingMessage("Uploading error pins to server ", MessageType.Info);
                    var apiRequest = new SyncTeamMemberInfoApiRequest();
                    bool isDone = apiRequest.UploadAttendanceErrorPins(errorPins);
                    if (isDone == false)
                        OnThrowingMessage("Unable to save error pins to server ", MessageType.Error);
                }

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        #endregion
    }
}
