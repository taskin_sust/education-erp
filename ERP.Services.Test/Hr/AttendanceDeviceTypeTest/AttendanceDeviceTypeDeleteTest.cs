﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceDeviceTypeTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceDeviceType")]
    public class AttendanceDeviceTypeDeleteTest : AttendanceDeviceTypeBaseTest
    {
        #region Basic

        [Fact]
        public void Should_Delete_Successfully()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].Status = -404;
            _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]);
            var lObj = _attendanceDeviceTypeService.GetAttendanceDeviceType(AttendanceDeviceTypeFactory.SingleObjectList[0].Id);
            Assert.Equal(lObj, null);
        }

        #endregion

        #region Validation Check Test

        [Fact]
        public void Should_Return_Empty_Object()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].Status = -404;
            Assert.Throws<NullReferenceException>(() => _attendanceDeviceTypeService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Return_Dependency_With_MemberFingerPrint()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            _memberFingerPrintService.SaveOrUpdate(new MemberInfoDeviceApi() { Pin = 225, AttendanceDeviceApiModel = new List<AttendanceDeviceApiModel>() { new AttendanceDeviceApiModel() { ModelId = AttendanceDeviceTypeFactory.Object.Id, ModelName = AttendanceDeviceTypeFactory.Object.Name, Index0 = "645fghfg4hf4gh6f4h3f1gh6f4h3f1hf64hfh1f6h4f6h1f3h4f3h13f24h6fh413f4h5hf3h46f4hf64h6f4hf3h1" } } });
            Assert.Throws<DependencyException>(() => _attendanceDeviceTypeService.Delete(AttendanceDeviceTypeFactory.SingleObjectList[0]));
        }


        #endregion

        #region Invalid Input Check Test
        #endregion
    }
}
