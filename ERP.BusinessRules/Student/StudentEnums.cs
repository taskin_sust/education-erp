﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.BusinessRules.Student
{
    public enum StudentImageStatus
    {
        [Description("Existing Image")]
        Active = 1,
        [Description("Missing Image")]
        Missing = 0
    }

    public enum PaymentMethod
    {
        [Description("Cash")]
        Cash = 1,
        [Description("BKash")]
        BKash = 2,
        [Description("Cheque")]
        Cheque = 3
    }

}
