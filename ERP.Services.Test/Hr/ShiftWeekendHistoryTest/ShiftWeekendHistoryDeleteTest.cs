﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ShiftWeekendHistoryTest
{
    public interface IShiftWeekendHistoryDeleteTest
    {
        void Should_Return_Delete_ShiftWeekendHistory_Successfully();
        void Should_Return_Dependency_Exception();

    }

    [Trait("Area", "Hr")]
    [Trait("Service", "ShiftWeekendHistoryService")]
    public class ShiftWeekendHistoryDeleteTest : ShiftWeekendHistoryBaseTest, IShiftWeekendHistoryDeleteTest
    {
        [Fact]
        public void Should_Return_Delete_ShiftWeekendHistory_Successfully()
        {
            var teamMember = PersistTeamMemberForHistory();
            var deletedObj = teamMember.ShiftWeekendHistory[1];
            _shiftWeekendHistoryService.DeleteShiftWeekendHistory(deletedObj);
            Assert.True(deletedObj.Status == ShiftWeekendHistory.EntityStatus.Delete);
        }

        [Fact]
        public void Should_Return_Dependency_Exception()
        {
            var teamMember = PersistTeamMember();
            var deletedObj = teamMember.ShiftWeekendHistory[0];
            Assert.Throws<DependencyException>(() => _shiftWeekendHistoryService.DeleteShiftWeekendHistory(deletedObj));
        }
    }
}
