﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.GoodReceiveReportTest
{
    public class GoodReceiveReportBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper _commonHelper;
        private ISession _session;

        public OrganizationFactory _organizationFactory;
        public BranchFactory _branchFactory;
        public ItemGroupFactory _itemGroupFactory;
        public ItemFactory _itemFactory;
        public ProgramFactory _programFactory;
        public SessionFactory _sessionFactory;
        public SupplierFactory _supplierFactory;
        public BankBranchFactory _bankBranchFactory;
        public GoodsReceiveFactory _goodsReceiveFactory;
        public GoodsReceiveDetailsFactory _goodsReceiveDetailsFactory;

        public GoodsReceiveService _goodsReceiveService;
        public GoodsReceiveDetailsService _goodsReceiveDetailsService;
        public BranchService _branchService;

        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        #endregion
        public GoodReceiveReportBaseTest()
        {
            _commonHelper = new CommonHelper();
            _session = NHibernateSessionFactory.OpenSession();

            _organizationFactory = new OrganizationFactory(null, _session);
            _branchFactory = new BranchFactory(null, _session);
            _itemGroupFactory = new ItemGroupFactory(null, _session);
            _itemFactory = new ItemFactory(null, _session);
            _programFactory = new ProgramFactory(null, _session);
            _sessionFactory = new SessionFactory(null, _session);
            _supplierFactory = new SupplierFactory(null, _session);
            _bankBranchFactory = new BankBranchFactory(null, _session);
            _goodsReceiveFactory = new GoodsReceiveFactory(null, _session);
            _goodsReceiveDetailsFactory = new GoodsReceiveDetailsFactory(null, _session);

            _goodsReceiveService = new GoodsReceiveService(_session);
            _goodsReceiveDetailsService = new GoodsReceiveDetailsService(_session);
            _branchService = new BranchService(_session);
        }

        public List<string> GenerateProgramSessionMix(Organization organization, Program program, Session session)
        {
            return new List<string>() { program.Id + "::" + session.Id };
        }

        public IList<string> OnGettingAuthorizeBranchNameList(List<UserMenu> userMenu, List<long> organizationIdList, List<long> branchIds)
        {
            var bName = !branchIds.Contains(SelectionType.SelelectAll) ? string.Join(", ",
               _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds.Skip(0).Take(branchIds.Count).ToList())
               ) : string.Join(", ", _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds).ToList());
            return bName.Split(',');
        }

        public void Dispose()
        {
            if (_goodsReceiveFactory.Object != null)
                foreach (var recDetail in _goodsReceiveFactory.Object.GoodsReceiveDetails)
                {
                    if (recDetail.GoodsReceive.Branch != null && recDetail.Item != null)
                    {
                        string swData = "DELETE FROM [UINV_CurrentStockSummary] WHERE [BranchId] = " + recDetail.GoodsReceive.Branch.Id + " and [ItemId] =" + recDetail.Item.Id + " ; ";
                        Session.CreateSQLQuery(swData).ExecuteUpdate();
                    }
                }
            _goodsReceiveDetailsFactory.CleanUp();
            _goodsReceiveFactory.CleanUp();
            _supplierFactory.CleanUp();
            _itemFactory.CleanUp();
            _branchFactory.Cleanup();
            _programFactory.Cleanup();
            base.Dispose();
        }
    }
}
