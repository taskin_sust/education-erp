using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class EmployeeBenefitsFundSettingEntitlementFactory : ObjectFactoryBase<EmployeeBenefitsFundSettingEntitlement>
    {

        private readonly IEmployeeBenefitsFundSettingEntitlementService _employeeBenefitsFundEntitlementService;

        public EmployeeBenefitsFundSettingEntitlementFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

           _employeeBenefitsFundEntitlementService = new EmployeeBenefitsFundSettingEntitlementService(session);

        }

        #region Create

        public EmployeeBenefitsFundSettingEntitlementFactory Create()
        {

            Object = new EmployeeBenefitsFundSettingEntitlement()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = EmployeeBenefitsFundSettingEntitlement.EntityStatus.Active,
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingEntitlementFactory WithServicePeriod(int servicePeriod)
        {
            Object.ServicePeriod = servicePeriod;     
            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingEntitlementFactory WithEmployerContribution(decimal employerContribution)
        {
            Object.EmployerContribution = employerContribution;       
            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingEntitlementFactory CreateWith(long id)
        {
            Object = null;
            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingEntitlementFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _employeeBenefitsFundEntitlementService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_employeeBenefitsFundEntitlementService.SaveOrUpdate(x); });

            }
            return this;
        }

        public EmployeeBenefitsFundSettingEntitlementFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new EmployeeBenefitsFundSettingEntitlement() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public void CleanupEmpBenefitSettingEntitlement(List<long> empBenefitSettingEntitlementIds)
        {
            if (empBenefitSettingEntitlementIds.Count > 0)
            {
                var tableName = GetEntityTableName(typeof(EmployeeBenefitsFundSettingEntitlement));
                _session.CreateSQLQuery("Delete from " + tableName + " where id in(" + string.Join(", ", empBenefitSettingEntitlementIds) + ")").ExecuteUpdate();
            }
        }


        public EmployeeBenefitsFundSettingEntitlementFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion

    }
}
