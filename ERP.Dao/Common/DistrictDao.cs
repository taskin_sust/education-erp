﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Common
{
    public interface IDistrictDao : IBaseDao<District, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<District> LoadDistrictByQuery(string query);
        IList<District> LoadDistrict(List<long> divisionIdList = null);
        #endregion

        #region Others Function

        #endregion
    }
    public class DistrictDao : BaseDao<District, long>, IDistrictDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<District> LoadDistrictByQuery(string query)
        {
            ICriteria criteria =Session.CreateCriteria<District>().Add(Restrictions.Eq("Status", District.EntityStatus.Active));
            if (!String.IsNullOrEmpty(query))
            {
                criteria.Add(Restrictions.Like("Name", query, MatchMode.Start));
            }
            criteria.SetFirstResult(0).SetMaxResults(10);
            return criteria.List<District>();
        }
        public IList<District> LoadDistrict(List<long> divisionIdList = null)
        {
            ICriteria criteria = Session.CreateCriteria<District>().Add(Restrictions.Eq("Status", District.EntityStatus.Active));
            criteria.CreateAlias("Division", "division").Add(Restrictions.Eq("division.Status", Division.EntityStatus.Active));
            if (divisionIdList != null && !divisionIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("division.Id", divisionIdList));
            }
            criteria.AddOrder(Order.Asc("Name"));
            return criteria.List<District>();
        }
        #endregion

        #region Others Function

        #endregion
    }
}