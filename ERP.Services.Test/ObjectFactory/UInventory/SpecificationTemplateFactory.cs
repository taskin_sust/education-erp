using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class SpecificationTemplateFactory : ObjectFactoryBase<SpecificationTemplate>
    {

        private readonly ISpecificationTemplateService _specificationTemplateService;

        public SpecificationTemplateFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _specificationTemplateService = new SpecificationTemplateService(session);

        }

        #region Create

        public SpecificationTemplateFactory Create()
        {

            Object = new SpecificationTemplate()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = SpecificationTemplate.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }
        
        public SpecificationTemplateFactory WithSpecificationCriteria()
        {
            var criteria1 = new SpecificationCriteria()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString().Substring(0, 5),
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                CreationDate = DateTime.Now,
                ModificationDate = DateTime.Now,
                Status = SpecificationCriteria.EntityStatus.Active,
                Controltype = FieldType.Dropdown,
                SpecificationTemplate = Object
            };
            var criteriaOption1 = new SpecificationCriteriaOption()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString().Substring(0, 5),
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                CreationDate = DateTime.Now,
                ModificationDate = DateTime.Now,
                Status = SpecificationCriteriaOption.EntityStatus.Active,
                SpecificationCriteria = criteria1
            };
            var criteriaOption2 = new SpecificationCriteriaOption()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString().Substring(0, 5),
                Rank = 2,
                CreateBy = 1,
                ModifyBy = 1,
                CreationDate = DateTime.Now,
                ModificationDate = DateTime.Now,
                Status = SpecificationCriteriaOption.EntityStatus.Active,
                SpecificationCriteria= criteria1
            };
            criteria1.SpecificationCriteriaOptions.Add(criteriaOption1);
            criteria1.SpecificationCriteriaOptions.Add(criteriaOption2);
            var criteria2 = new SpecificationCriteria()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString().Substring(0, 5),
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                CreationDate = DateTime.Now,
                ModificationDate= DateTime.Now,
                Status = SpecificationCriteria.EntityStatus.Active,
                Controltype = FieldType.Text,
                SpecificationTemplate = Object
            };
            Object.SpecificationCriteriaList.Add(criteria1);
            Object.SpecificationCriteriaList.Add(criteria2);
            return this;
        }

        public SpecificationTemplateFactory WithSpecificationCriteria(IList<SpecificationCriteria> specificatonCriteria )
        {
            foreach (var specificatonCriterion in specificatonCriteria)
            {
                specificatonCriterion.SpecificationTemplate = Object;
                Object.SpecificationCriteriaList.Add(specificatonCriterion);
            }
            return this;
        }
        
        public SpecificationTemplateFactory WithSpecificationCriteria(IList<SpecificationCriteria> specificatonCriteria,IList<SpecificationCriteriaOption> specificatonCriteriaOption )
        {
            foreach (var specificatonCriterion in specificatonCriteria)
            {
                specificatonCriterion.Controltype=FieldType.Dropdown;
                specificatonCriterion.SpecificationTemplate = Object;
                foreach (var specificationCriteriaOption in specificatonCriteriaOption)
                {
                    specificationCriteriaOption.SpecificationCriteria = specificatonCriterion;
                    specificatonCriterion.SpecificationCriteriaOptions.Add(specificationCriteriaOption);
                }
                Object.SpecificationCriteriaList.Add(specificatonCriterion);
            }
            return this;
        }

        public SpecificationTemplateFactory WithName(string name)
        {

            Object.Name = name;
            SingleObjectList.Add(Object);
            return this;

        }

        public SpecificationTemplateFactory CreateMore(int num)
        {

            CreateSpecificationTemplateList(num);
            return this;
        }

        private SpecificationTemplateFactory CreateSpecificationTemplateList(int num)
        {
            ListStartIndex = ObjectList.Count;
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            var list = new List<SpecificationTemplate>();
            for (int i = 0; i < num; i++)
            {
                var criteriaOption1 = new SpecificationCriteriaOption()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = name+"_i",
                    Rank = 1,
                    CreateBy = 1,
                    ModifyBy = 1,
                    CreationDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    Status = SpecificationCriteriaOption.EntityStatus.Active
                };
                var criteriaOption2 = new SpecificationCriteriaOption()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = Guid.NewGuid().ToString().Substring(0, 5),
                    Rank = 2,
                    CreateBy = 1,
                    ModifyBy = 1,
                    CreationDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    Status = SpecificationCriteriaOption.EntityStatus.Active
                };
                var criteria1 = new SpecificationCriteria()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = Guid.NewGuid().ToString().Substring(0, 5),
                    Rank = 1,
                    CreateBy = 1,
                    ModifyBy = 1,
                    CreationDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    Status = SpecificationCriteria.EntityStatus.Active,
                    Controltype = FieldType.Dropdown
                };
                criteriaOption1.SpecificationCriteria = criteria1;
                criteriaOption2.SpecificationCriteria = criteria1;
                criteria1.SpecificationCriteriaOptions.Add(criteriaOption1);
                criteria1.SpecificationCriteriaOptions.Add(criteriaOption2);
                var criteria2 = new SpecificationCriteria()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = Guid.NewGuid().ToString().Substring(0, 5),
                    Rank = 1,
                    CreateBy = 1,
                    ModifyBy = 1,
                    CreationDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    Status = SpecificationCriteria.EntityStatus.Active,
                    Controltype = FieldType.Text
                };
                var specificationTemplate = new SpecificationTemplate()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = Guid.NewGuid().ToString().Substring(0,5)+"_" + i,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    CreationDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    Status = Campus.EntityStatus.Active
                };
                criteria1.SpecificationTemplate = specificationTemplate;
                criteria2.SpecificationTemplate = specificationTemplate;
                specificationTemplate.SpecificationCriteriaList.Add(criteria1);
                specificationTemplate.SpecificationCriteriaList.Add(criteria2);
                list.Add(specificationTemplate);
            }
            ObjectList.AddRange(list);
            return this;
        }
        
        public SpecificationTemplateFactory Persist()
        {

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _specificationTemplateService.SaveOrUpdate(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _specificationTemplateService.SaveOrUpdate(item);
                    }
                }
            }
            return this;
        }
        
        #endregion

        #region other
        #endregion

        #region CleanUp

        public SpecificationTemplateFactory CleanUp()
        {
            DeleteAll();
            return this;
        }

        #endregion

    }
}
