using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public partial class BranchMap : BaseClassMap<Branch, long>
    {
        public BranchMap()
        {
            Table("Branch");
            LazyLoad();
           // Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name);
            Map(x => x.ShortName);
            Map(x => x.Code).Column("Code");
            Map(x => x.Rank);
            Map(x => x.IsCorporate);
            References(x => x.Organization).Column("OrganizationId");
            HasMany(x => x.Batches).KeyColumn("BranchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.Campus).KeyColumn("BranchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.Users).KeyColumn("BranchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.UserMenus).KeyColumn("BranchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ProgramBranchSessions).KeyColumn("BranchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ExamList).KeyColumn("BranchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.FestivalBonusSheetsSalary).KeyColumn("SalaryBranchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.FestivalBonusSheetsJob).KeyColumn("JobBranchId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
