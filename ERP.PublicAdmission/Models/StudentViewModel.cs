﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UdvashERP.PublicAdmission.Models 
{
    public class StudentViewModel
    {
       
        public StudentViewModel()
        {
            CourseViewModels = new List<CourseViewModel>();
            PaymentTransactionViewModel = new List<PaymentTransactionViewModel>();
            //StudentPayment = new PaymentDetails();
        }
        public virtual long Id { get; set; }
        [Required]
        [Display(Name = "Nick Name :")]
        public virtual string Name { get; set; }

        [Required]
        [RegularExpression(@"(\d*)?\d{13}", ErrorMessage = "Invalid mobile number. Please enter valid number.<br/> Ex: 88018XXXXXXXX")]
        [StringLength(13)]
        [Display(Name = "Mobile Number :")]
        public virtual string MobNumber { get; set; }

        [Required]
        [Display(Name = "Program Name :")]
        public virtual string Program { get; set; }

        [Required]
        [Display(Name = "Session :")]
        public virtual string Session { get; set; }

        [Required]
        [Display(Name = "Branch Name :")]
        public virtual string Branch { get; set; }

        [Required]
        [Display(Name = "Campus Name :")]
        public virtual string Campus { get; set; }

        [Required]
        [Display(Name = "Batch Days :")]
        public virtual string BatchDays { get; set; }

        [Required]
        [Display(Name = "Batch Time  :")]
        public virtual string BatchTime { get; set; }

        [Display(Name = "Gender :")]
        [Required]
        public virtual int? Gender { get; set; }

        [Display(Name = "Religion :")]
        [Required]
        public virtual int? Religion { get; set; }

        [Display(Name = "Study Version :")]
        [Required]
        public virtual int? VersionOfStudy { get; set; }


        [Required]
        [Display(Name = "Batch Name :")]
        public virtual string Batch { get; set; }

        public virtual IList<CourseViewModel> CourseViewModels { get; set; }
        public virtual IList<PaymentTransactionViewModel> PaymentTransactionViewModel { get; set; }         
       // public virtual PaymentDetails StudentPayment { get; set; }
    }
}