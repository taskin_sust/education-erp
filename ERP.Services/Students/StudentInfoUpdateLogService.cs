﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IStudentInfoUpdateLogService : IBaseService
    {
        #region Operational Function
        void SaveLog(StudentInfoUpdateLog obj);
        #endregion
    }

    public class StudentInfoUpdateLogService:BaseService,IStudentInfoUpdateLogService
    {
        #region Propertise & Object Initialization
        private readonly IStudentInfoUpdateLogDao _studentInfoUpdateLogDao;
        public StudentInfoUpdateLogService(ISession session)
        {
            Session = Session;
            _studentInfoUpdateLogDao = new StudentInfoUpdateLogDao() { Session = session };
        }
        #endregion
        
        #region Operational Function
        public void SaveLog(StudentInfoUpdateLog obj)
        {
            _studentInfoUpdateLogDao.SaveLog(obj);
        }
        #endregion
    }
}
