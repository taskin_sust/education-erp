using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class DayOffAdjustmentMap : BaseClassMap<DayOffAdjustment,long> {
        
        public DayOffAdjustmentMap() {
			Table("HR_DayOffAdjustment");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.DayOffDate).Column("DayOffDate");
			Map(x => x.InsteadOfDate).Column("InsteadOfDate");
			Map(x => x.Reason).Column("Reason");
			Map(x => x.DayOffStatus).Column("DayOffStatus");
            Map(x => x.IsPost).Column("IsPost");
			HasMany(x => x.DayOffAdjustmentLog).KeyColumn("DayOffAdjustmentId");
        }
    }
}
