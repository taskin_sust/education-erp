﻿using System.Collections.Generic;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Teachers
{

    public interface ITeacherActivityDao : IBaseDao<TeacherActivity, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion
    }

    public class TeacherActivityDao : BaseDao<TeacherActivity, long>, ITeacherActivityDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion
    }
}
