﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;
using NHibernate;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class AttendanceDeviceTypeController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private List<UserMenu> authorizeMenu;
        private ISession _session;

        private readonly IAttendanceDeviceTypeService _attendanceDeviceTypeService;

        #endregion

        #region Constructor

        public AttendanceDeviceTypeController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _attendanceDeviceTypeService = new AttendanceDeviceTypeService(_session);
        }

        public ActionResult Index()
        {
            return RedirectToAction("ManageAttendanceDeviceType");
        }

        #endregion

        #region Index

        [HttpGet]
        public ActionResult ManageAttendanceDeviceType()
        {
            try
            {
                var fingerList = new List<SelectListModel>();
                fingerList.Add(new SelectListModel() { Text = "Finger", Value = "1" });
                fingerList.Add(new SelectListModel() { Text = "Non Finger", Value = "2" });
                ViewBag.fingerPrintList = fingerList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        public ActionResult ManageAttendanceDeviceType(int draw, int start, int length, string name = "", string code = "", string finger = "")
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<AttendanceDeviceType> attendanceDeviceTypes = _attendanceDeviceTypeService.LoadAttendanceDeviceType(draw, start, length, name, code, finger);
                int count = _attendanceDeviceTypeService.AttendanceDevicetypeCount(name, code, finger);
                var data = new List<object>();
                foreach (var attendanceDeviceType in attendanceDeviceTypes)
                {
                    var str = new List<string>();
                    str.Add(attendanceDeviceType.Name);
                    str.Add(attendanceDeviceType.Code);
                    str.Add(attendanceDeviceType.IsFinger ? "Finger Print" : "No Finger Print");
                    str.Add(attendanceDeviceType.FirmwareVersion.ToString());
                    str.Add(attendanceDeviceType.AlgorithmVersion.ToString());
                    str.Add(attendanceDeviceType.AttendanceLogCapacity.ToString());
                    str.Add(attendanceDeviceType.UserCapacity.ToString());
                    str.Add(attendanceDeviceType.TemplateCapacity.ToString());
                    str.Add(LinkGenerator.GetGeneratedDetailsEditLink("Details", "Edit", "AttendanceDeviceType", attendanceDeviceType.Id)
                            + LinkGenerator.GetDeleteLink("Delete", "AttendanceDeviceType", attendanceDeviceType.Id));
                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = count,
                    recordsFiltered = count,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                var data = new List<object>();
                _logger.Error(ex);
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });
            }
        }

        #endregion

        #region Operational Function

        #region Save

        [HttpGet]
        public ActionResult Create(string message = "", int type = 0)
        {
            try
            {
                if (!String.IsNullOrEmpty(message))
                {
                    switch (type)
                    {
                        case 1://success
                            ViewBag.SuccessMessage = message;
                            break;
                        case 2://failed
                            ViewBag.ErrorMessage = message;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(AttendanceDeviceType deviceType)
        {
            try
            {
                _attendanceDeviceTypeService.SaveOrUpdate(deviceType);
                return RedirectToAction("Create", "AttendanceDeviceType", new { message = "Added Successfully", type = 1 });
            }
            catch (InvalidDataException ide)
            {
                return RedirectToAction("Create", "AttendanceDeviceType", new { message = ide.Message, type = 2 });
            }
            catch (DuplicateEntryException dee)
            {
                return RedirectToAction("Create", "AttendanceDeviceType", new { message = dee.Message, type = 2 });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("Create", "AttendanceDeviceType", new { message = "Failed Operation", type = 2 });
            }
        }

        #endregion

        #region Edit

        [HttpGet]
        public ActionResult Edit(long id, string message = "", int? type = 0)
        {
            AttendanceDeviceType deviceType = null;
            try
            {
                deviceType = _attendanceDeviceTypeService.GetAttendanceDeviceType(id);
                switch (type)
                {
                    case 1:
                        ViewBag.SuccessMessage = message;
                        break;
                    case 2:
                        ViewBag.ErrorMessage = message;
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View(deviceType);
        }

        [HttpPost]
        public ActionResult Edit(AttendanceDeviceType deviceType)
        {
            var proxyObj = new AttendanceDeviceType();
            var messsage = "";
            try
            {
                if (!ModelState.IsValid)
                {
                    IEnumerable<ModelError> modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
                    messsage = modelStateErrors.Aggregate(messsage, (current, modelStateError) => current + (modelStateError.ErrorMessage + " "));
                    return RedirectToAction("Edit", new { message = messsage, type = 2 /*failed*/ });
                }
                proxyObj = _attendanceDeviceTypeService.GetAttendanceDeviceType(deviceType.Id);
                proxyObj.Name = deviceType.Name;
                proxyObj.Code = deviceType.Code;
                proxyObj.IsFinger = deviceType.IsFinger;
                proxyObj.FirmwareVersion = deviceType.FirmwareVersion;
                proxyObj.AlgorithmVersion = deviceType.AlgorithmVersion;
                proxyObj.AttendanceLogCapacity = deviceType.AttendanceLogCapacity;
                proxyObj.UserCapacity = deviceType.UserCapacity;
                proxyObj.TemplateCapacity = deviceType.TemplateCapacity;
                _attendanceDeviceTypeService.SaveOrUpdate(proxyObj);
                messsage = "Operation completed successfully";
                return RedirectToAction("Edit", new { id = proxyObj.Id, message = messsage, type = 1 /*success*/ });
            }
            catch (NullObjectException nullObjectException)
            {
                messsage = nullObjectException.Message;
            }
            catch (InvalidDataException invalidDataException)
            {
                messsage = invalidDataException.Message;
            }
            catch (DuplicateEntryException duplicateEntryException)
            {
                messsage = duplicateEntryException.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                messsage = WebHelper.CommonErrorMessage;
            }
            return RedirectToAction("Edit", new { id = proxyObj.Id, message = messsage, type = 2 /*fail*/});
        }

        #endregion

        #region Delete

        public ActionResult Delete(long id)
        {
            try
            {
                var entity = _attendanceDeviceTypeService.GetAttendanceDeviceType(id);
                _attendanceDeviceTypeService.Delete(entity);
                return Json(new { isSuccess = true, Text = "Device Type Successfully Deleted!" });
            }

            catch (NullObjectException nullObjectException)
            {
                return Json(new { isSuccess = false, Text = nullObjectException.Message });
            }
            catch (InvalidDataException invalidDataException)
            {
                return Json(new { isSuccess = false, Text = invalidDataException.Message });
            }
            catch (DependencyException de)
            {
                return Json(new { isSuccess = false, Text = de.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { isSuccess = false, Text = "Settings Delete Failed!" });
            }
        }
        #endregion

        #region Details

        [HttpGet]
        public ActionResult Details(long id)
        {
            try
            {
                var entity = _attendanceDeviceTypeService.GetAttendanceDeviceType(id);
                return View(entity);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("ManageAttendanceDeviceType");
            }
        }

        #endregion

        #endregion

        #region Helper


        #endregion

    }
}