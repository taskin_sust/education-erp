﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class GoodReceiveReportController : Controller
    {
        #region logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private List<UserMenu> authorizeMenu;

        private readonly ICommonHelper _commonHelper;
        private readonly IGoodsReceiveDetailService _goodsReceiveDetailService;
        private readonly IOrganizationService _organizationService;
        private readonly IItemService _itemService;
        private readonly IItemGroupService _itemGroupService;
        private readonly ISupplierService _supplierService;
        private readonly IBranchService _branchService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IProgramBranchSessionService _programBranchSessionService;

        private AuthorizeBranchNameDelegate branchNameDelegate;
        public GoodReceiveReportController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _goodsReceiveDetailService = new GoodsReceiveDetailsService(session);
            _organizationService = new OrganizationService(session);
            _itemService = new ItemService(session);
            _itemGroupService = new ItemGroupService(session);
            _supplierService = new SupplierService(session);
            _branchService = new BranchService(session);
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _programBranchSessionService = new ProgramBranchSessionService(session);

            branchNameDelegate = new AuthorizeBranchNameDelegate(OnGettingAuthorizeBranchNameList);
        }

        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region Report

        public ActionResult Report()
        {
            try
            {
                Initialize();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Report(GoodsReceiveViewModel goodReceiveViewModel)
        {
            if (ModelState.IsValid)
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (goodReceiveViewModel.BranchId == null) goodReceiveViewModel.BranchId = new long[] { 0 };
                if (goodReceiveViewModel.Purpose == null) goodReceiveViewModel.Purpose = new Purpose[] { 0 };
                if (goodReceiveViewModel.ItemGroupId == null) goodReceiveViewModel.ItemGroupId = new long[] { 0 };
                if (goodReceiveViewModel.ItemId == null) goodReceiveViewModel.ItemId = new long[] { 0 };
                if (goodReceiveViewModel.Supplier == null) goodReceiveViewModel.Supplier = new long[] { 0 };
                if (goodReceiveViewModel.ProgramSession == null) goodReceiveViewModel.ProgramSession = new string[] { SelectionType.SelelectAll.ToString() };
                List<long> branchIds = new List<long>(goodReceiveViewModel.BranchId.ToArray());

                ViewBag.organizationName = _organizationService.LoadById(goodReceiveViewModel.OrganizationId).Name;
                ViewBag.organizationShortName = _organizationService.LoadById(goodReceiveViewModel.OrganizationId).ShortName;
                var branchName = !goodReceiveViewModel.BranchId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _branchService.LoadAuthorizedBranchNameList(
                    userMenu, _commonHelper.ConvertIdToList(goodReceiveViewModel.OrganizationId), branchIds).ToList()) : "All Udvash Branch";
                var organizationIdList = _commonHelper.ConvertIdToList(goodReceiveViewModel.OrganizationId);

                var bName = !goodReceiveViewModel.BranchId.Contains(SelectionType.SelelectAll) ? string.Join(", ",
                    _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, goodReceiveViewModel.BranchId.Skip(0).Take(goodReceiveViewModel.BranchId.Length).ToList())
                    ) : string.Join(", ", _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds).ToList());
                ViewBag.branchAry = bName.Split(',');

                ViewBag.branchName = branchName;
                ViewBag.receiveType = goodReceiveViewModel.ReceiveType != SelectionType.SelelectAll ? _commonHelper.GetEmumIdToValue<ReceiveType>(goodReceiveViewModel.ReceiveType) : "All Type Received";
                ViewBag.itemGroup = !goodReceiveViewModel.ItemGroupId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _itemGroupService.LoadItemGroup(goodReceiveViewModel.ItemGroupId.ToList()).Select(x => x.Name).ToList()) : "All Item Groups";
                ViewBag.item = !goodReceiveViewModel.ItemId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _itemService.LoadItemList(goodReceiveViewModel.ItemId).Select(x => x.Name).ToList()) : "All Items";
                ViewBag.dateFrom = goodReceiveViewModel.DateFrom.ToString("MMM dd, yyyy");
                ViewBag.dateTo = goodReceiveViewModel.DateTo.ToString("MMM dd, yyyy");
                var supplier = !goodReceiveViewModel.Supplier.Contains(SelectionType.SelelectAll) ? string.Join(", ", _supplierService.LoadSupplierByIds(goodReceiveViewModel.Supplier).Select(x => x.Name).ToList()) : "All Supplier";
                ViewBag.SupplierIds = supplier;
                string programSession = "";
                int countshouldhave = CountProgramAndSession(goodReceiveViewModel.OrganizationId, branchIds, goodReceiveViewModel.ItemId.ToList());
                int counthave = goodReceiveViewModel.ProgramSession.Count();
                if ((counthave == 1 && goodReceiveViewModel.ProgramSession[0] == SelectionType.SelelectAll.ToString()) ||
                    ((counthave - 1) == countshouldhave))
                {
                    programSession = "All Program & Session";
                    var programSessionColName = GetProgramSessionName(goodReceiveViewModel.ProgramSession, goodReceiveViewModel.OrganizationId, goodReceiveViewModel.BranchId, goodReceiveViewModel.ItemId);
                    ViewBag.programSessionAry = programSessionColName.Split(',');
                }
                else
                {
                    programSession = GetProgramSessionName(goodReceiveViewModel.ProgramSession, goodReceiveViewModel.OrganizationId, goodReceiveViewModel.BranchId, goodReceiveViewModel.ItemId);
                    ViewBag.programSessionAry = programSession.Split(',');
                }
                string purpose = GetPurpose(goodReceiveViewModel.Purpose);
                ViewBag.programSession = programSession;
                ViewBag.purpose = purpose;
                return PartialView("Partial/_GoodReceiveReport", goodReceiveViewModel);
            }
            return View();
        }

        public int CountProgramAndSession(long organizationId, List<long> branchIds, List<long> itemIdList)
        {
            IList<ProgramSessionDto> list = new List<ProgramSessionDto>();
            try
            {
                if (branchIds.Count == 1 && branchIds.Contains(0)) branchIds = null;
                if (itemIdList.Count == 1 && itemIdList.Contains(0)) itemIdList = null;
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                list = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIds, itemIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return list.Count();
        }


        #endregion

        #region data table render

        [HttpPost]
        public JsonResult GoodReceiveReportAjaxRequest(int draw, int start, int length, long organizationId, List<long> branchIdList, List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, int receiveType, int reportType, string dateFrom, string dateTo, List<string> programsession)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (reportType == (int)ReceiveReportType.BranchWise)
                    return GoodReceiveBranchReport(draw, start, length, userMenu, organizationId, branchIdList, purposeList, itemGroupIdList, itemIdList, programIdList,
                    sessionIdList, supplierIdList, receiveType, reportType, dateFrom, dateTo, programsession);
                return GoodReceiveProgramReport(draw, start, length, userMenu, organizationId, branchIdList, purposeList, itemGroupIdList, itemIdList, programIdList,
                sessionIdList, supplierIdList, receiveType, reportType, dateFrom, dateTo, programsession);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                throw ex;
            }
        }

        #endregion

        #region Export

        public ActionResult ExportReport(long organizationId, List<long> branchIdList, List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, int receiveType, int reportType
           , string dateFrom, string dateTo, List<string> programsession)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                int start = 0;
                int length = int.MaxValue;
                IList<dynamic> gooodRecList = new List<dynamic>();
                string organizationName = _organizationService.LoadById(organizationId).Name;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);

                List<string> headerList = new List<string>();
                var bName = !branchIdList.Contains(SelectionType.SelelectAll) ? string.Join(", ",
                     _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIdList.Skip(0).Take(branchIdList.Count).ToList())
                     ) : string.Join(", ", _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIdList).ToList());
                var branchAry = bName.Split(',');

                bool isAddNa = false;
                bool isOnlyNa = false;
                string[] psAry = programsession.ToArray();
                if (psAry.Length > 0)
                    psAry = psAry.Distinct().ToArray();

                if (psAry.Contains(SelectionType.SelectNone.ToString()))
                {
                    isAddNa = true;
                    psAry = psAry.Skip(1).Take(psAry.Length - 1).ToArray();
                    if (psAry.Length <= 0) isOnlyNa = true;
                }
                if (psAry.Length == 1 && psAry.Contains(SelectionType.SelelectAll.ToString()))
                    isAddNa = true;

                string programSessionColName = "";
                int countshouldhave = CountProgramAndSession(organizationId, branchIdList, itemIdList);
                int counthave = programsession.Count();
                if ((counthave == 1 && programsession[0] == SelectionType.SelelectAll.ToString()) || ((counthave - 1) == countshouldhave))
                    programSessionColName = GetProgramSessionName(programsession.ToArray(), organizationId, branchIdList.ToArray(), itemIdList.ToArray());
                else
                    programSessionColName = GetProgramSessionName(programsession.ToArray(), organizationId, branchIdList.ToArray(), itemIdList.ToArray());

                var programSessionAry = programSessionColName.Split(',');

                string[] programSessionAuthArrayList = _goodsReceiveDetailService.BuildAuthProgramSession(userMenu, organizationId, branchIdList.ToArray(), itemIdList.ToArray(), psAry);
                var proSessionList = programSessionAuthArrayList.ToList();

                if (isOnlyNa) { proSessionList = new List<string>(); }
                if (isAddNa) { proSessionList.Insert(0, SelectionType.SelectNone.ToString()); }

                headerList.Add(organizationName);
                headerList.Add("Goods Receive Report");
                headerList.Add(Convert.ToDateTime(dateFrom).ToString("MMM dd, yyyy") + " to " + Convert.ToDateTime(dateTo).ToString("MMM dd, yyyy"));
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                if (reportType == (int)ReceiveReportType.BranchWise)
                {
                    columnList.Add("Item Group");
                    columnList.Add("Item Name");
                    columnList.Add("Program & Session");
                    columnList.AddRange(branchAry);
                    gooodRecList = _goodsReceiveDetailService.LoadGoodReceiveBranchWise(branchNameDelegate, start, length, userMenu, organizationId, branchIdList, purposeList, itemGroupIdList, itemIdList, programIdList,
                    sessionIdList, supplierIdList, receiveType, reportType, dateFrom, dateTo, proSessionList, isOnlyNa);
                }
                else
                {
                    columnList.Add("Item Group");
                    columnList.Add("Item Name");
                    columnList.AddRange(programSessionAry);
                    gooodRecList = _goodsReceiveDetailService.LoadGoodReceiveProgramWise(start, length, userMenu, organizationId, branchIdList, purposeList, itemGroupIdList,
                     itemIdList, programIdList, sessionIdList, supplierIdList, receiveType, reportType, dateFrom, dateTo, proSessionList, isOnlyNa);
                }
                columnList.Add("Total");
                var transactionXlsList = new List<List<object>>();
                foreach (var goodRecObj in gooodRecList)
                {
                    var str = new List<object>();
                    IDictionary<string, object> dic;
                    str.AddRange(from entry in dic = goodRecObj select entry.Value);
                    transactionXlsList.Add(str);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "GRReport__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("Autoclose");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion

        #region Helper
        private void Initialize()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var authorgList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");

            ViewBag.organizationList = authorgList;
            ViewBag.branchIdList = new SelectList(string.Empty, "Value", "Text");

            var list = _itemGroupService.LoadItemGroup(0, int.MaxValue, "", "", "", ItemGroup.EntityStatus.Active.ToString());
            list.Insert(0, new ItemGroup() { Id = 0, Name = "All Selected" });
            ViewBag.itemGroupList = new SelectList(list, "Id", "Name");

            ViewBag.itemList = new SelectList(string.Empty, "Value", "Text");

            var psList = new List<ProgramSessionDto>();
            psList.Insert(0, new ProgramSessionDto() { ProgramAndSessionId = SelectionType.SelelectAll.ToString(), ProgramAndSessionName = "All Selected" });
            ViewBag.programSessionList = new SelectList(psList, "ProgramAndSessionId", "ProgramAndSessionName");

            var dic = _commonHelper.LoadEmumToDictionary<Purpose>();
            dic.Add(SelectionType.SelectNone, "N/A");
            dic.Add(0, "All Selected");

            ViewBag.perposeList = new SelectList(dic, "Key", "Value");

            var slist = _supplierService.LoadSupplierList(0, int.MaxValue, "", "", "", Supplier.EntityStatus.Active.ToString());
            slist.Insert(0, new Supplier() { Id = 0, Name = "All Selected" });
            ViewBag.supplierList = new SelectList(slist, "Id", "Name");

            var receiveTypeEnum = _commonHelper.LoadEmumToDictionary<ReceiveType>();
            ViewBag.receiveTypeList = new SelectList(receiveTypeEnum, "key", "value");

            var reportType = _commonHelper.LoadEmumToDictionary<ReceiveReportType>();
            ViewBag.reportType = new SelectList(reportType, "key", "value");
        }

        private JsonResult GoodReceiveProgramReport(int draw, int start, int length, List<UserMenu> userMenu, long organizationId, List<long> branchIdList,
            List<int> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, int receiveType,
            int reportType, string dateFrom, string dateTo, List<string> programSessionArrayList)
        {
            bool isAddNa = false;
            bool isOnlyNa = false;
            string[] psAry = programSessionArrayList.ToArray();
            if (psAry.Length > 0)
                psAry = psAry.Distinct().ToArray();

            if (psAry.Contains(SelectionType.SelectNone.ToString()))
            {
                isAddNa = true;
                psAry = psAry.Skip(1).Take(psAry.Length - 1).ToArray();
                if (psAry.Length <= 0) isOnlyNa = true;
            }
            if (psAry.Length == 1 && psAry.Contains(SelectionType.SelelectAll.ToString()))
                isAddNa = true;

            string[] programSessionAuthArrayList = _goodsReceiveDetailService.BuildAuthProgramSession(userMenu, organizationId, branchIdList.ToArray(), itemIdList.ToArray(), psAry);
            var proSessionList = programSessionAuthArrayList.ToList();
            if (isOnlyNa) { proSessionList = new List<string>(); }
            if (isAddNa) { proSessionList.Insert(0, SelectionType.SelectNone.ToString()); }
            IList<dynamic> gooodRecList = _goodsReceiveDetailService.LoadGoodReceiveProgramWise(start, length, userMenu, organizationId, branchIdList, purposeList, itemGroupIdList,
                itemIdList, programIdList, sessionIdList, supplierIdList, receiveType, reportType, dateFrom, dateTo, proSessionList, isOnlyNa);

            int count = _goodsReceiveDetailService.CountGoodReceiveProgramWise(userMenu, organizationId, branchIdList, purposeList, itemGroupIdList, itemIdList, programIdList,
                sessionIdList, supplierIdList, receiveType, reportType, dateFrom, dateTo, proSessionList, isOnlyNa);
            var data = new List<object>();
            int sl = start + 1;
            foreach (var goodRecObj in gooodRecList)
            {
                var str = new List<object>();
                str.Add(sl.ToString());
                IDictionary<string, object> dic;
                str.AddRange(from entry in dic = goodRecObj select entry.Value);
                sl++;
                data.Add(str);
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = count,
                recordsFiltered = count,
                start = start,
                length = length,
                data = data,

            });
        }

        private JsonResult GoodReceiveBranchReport(int draw, int start, int length, List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<int> purposeList,
            List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, int receiveType, int reportType,
            string dateFrom, string dateTo, List<string> programsession)
        {
            bool isAddNa = false;
            bool isOnlyNa = false;
            string[] psAry = programsession.ToArray();
            if (psAry.Length > 0)
                psAry = psAry.Distinct().ToArray();

            // selectNone means N/A
            if (psAry.Contains(SelectionType.SelectNone.ToString()))
            {
                isAddNa = true;
                psAry = psAry.Skip(1).Take(psAry.Length - 1).ToArray();
                if (psAry.Length <= 0) isOnlyNa = true;
            }
            if (psAry.Length == 1 && psAry.Contains(SelectionType.SelelectAll.ToString()))
                isAddNa = true;

            string[] programSessionAuthArrayList = _goodsReceiveDetailService.BuildAuthProgramSession(userMenu, organizationId, branchIdList.ToArray(), itemIdList.ToArray(), psAry);
            var proSessionList = programSessionAuthArrayList.ToList();
            if (isOnlyNa) { proSessionList = new List<string>(); }
            if (isAddNa) { proSessionList.Insert(0, SelectionType.SelectNone.ToString()); }

            IList<dynamic> goodrecList = _goodsReceiveDetailService.LoadGoodReceiveBranchWise(branchNameDelegate, start, length, userMenu, organizationId, branchIdList, purposeList, itemGroupIdList, itemIdList, programIdList,
                    sessionIdList, supplierIdList, receiveType, reportType, dateFrom, dateTo, proSessionList, isOnlyNa);
            int count = _goodsReceiveDetailService.CountGoodReceiveBranchWise(branchNameDelegate, userMenu, organizationId, branchIdList, purposeList, itemGroupIdList, itemIdList, programIdList,
                 sessionIdList, supplierIdList, receiveType, reportType, dateFrom, dateTo, proSessionList, isOnlyNa);
            int sl = start + 1;
            var data = new List<object>();
            foreach (var goodRecObj in goodrecList)
            {
                var str = new List<object>();
                str.Add(sl.ToString());
                IDictionary<string, object> dic;
                str.AddRange(from entry in dic = goodRecObj select entry.Value);
                sl++;
                data.Add(str);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = count,
                recordsFiltered = count,
                start = start,
                length = length,
                data = data,

            });
        }

        private IList<string> OnGettingAuthorizeBranchNameList(List<UserMenu> userMenu, List<long> organizationIdList, List<long> branchIds)
        {
            try
            {
                var bName = !branchIds.Contains(SelectionType.SelelectAll) ? string.Join(", ",
                   _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds.Skip(0).Take(branchIds.Count).ToList())
                   ) : string.Join(", ", _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds).ToList());
                return bName.Split(',');
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        private string GetPurpose(Purpose[] purposes)
        {
            string purpose = "";
            if (!purposes.Where(x => (int)x == SelectionType.SelelectAll).Any())
            {
                foreach (var purposeValue in purposes)
                {
                    if (purposeValue == (Purpose)SelectionType.SelectNone)
                        purpose += "N/A, ";
                    purpose += _commonHelper.GetEmumIdToValue<Purpose>((int)purposeValue) + ", ";
                }
                purpose = purpose.Trim(' ', ',');
            }
            else
                purpose = "All Purposes";
            return purpose;
        }

        private string GetProgramSessionName(string[] programSessionAryStrings, long organizationId, long[] branchId, long[] itemId)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<long> programIdList = new List<long>();
            IList<long> sessionIdList = new List<long>();

            var programSession = "";
            string[] programSessionArrayList;
            if (programSessionAryStrings.Contains(SelectionType.SelectNone.ToString()))
            {
                programSession = "N/A,";
                programSessionArrayList = programSessionAryStrings.Skip(1).Take(programSessionAryStrings.Length - 1).ToArray();
            }
            else
                programSessionArrayList = programSessionAryStrings;
            if (programSessionAryStrings.Contains(SelectionType.SelelectAll.ToString()))
            {
                programSession = "N/A,";
                programSessionArrayList = _goodsReceiveDetailService.BuildAuthProgramSession(userMenu, organizationId, branchId, itemId);
            }
            foreach (var programsession in programSessionArrayList)
            {
                string[] programSessionArray = programsession.Split(new string[] { "::" }, StringSplitOptions.None);
                Program program = _programService.GetProgram(Convert.ToInt64(programSessionArray[0]));
                Session session = _sessionService.LoadById(Convert.ToInt64(programSessionArray[1]));
                if (program != null && session != null)
                {
                    programIdList.Add(program.Id);
                    sessionIdList.Add(session.Id);
                    string programName = program.ShortName;
                    string sessionName = session.Name;
                    string programSessionName = programName + " & " + sessionName + ", ";
                    programSession += programSessionName;
                }
            }
            programSession = programSession.Trim(' ', ',');
            ViewBag.ProgramIdList = programIdList;
            ViewBag.SessionIdList = sessionIdList;
            return programSession;
        }


        #endregion

    }
}