﻿using System;

namespace UdvashERP.MessageExceptions
{
    public class ServiceException : Exception
    {
        public ServiceException():base("Service Exception")
        {

        }

        public ServiceException(string message):base(message)
        {

        }

    }
}
