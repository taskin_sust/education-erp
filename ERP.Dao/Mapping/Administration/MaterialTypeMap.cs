using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class MaterialTypeMap : BaseClassMap<MaterialType, long>
    {
        
        public MaterialTypeMap() {
			Table("MaterialType");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name);
            Map(x => x.ShortName);
            Map(x => x.Rank);
			Map(x => x.Code).Column("Code");

            References(x => x.Organization).Column("OrganizationId");

            HasMany(x => x.Materials).KeyColumn("MaterialTypeId").Cascade.AllDeleteOrphan().Inverse().Where(x=>x.Status==Material.EntityStatus.Active);
        }
    }
}
