﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSummaryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSummary")]
    public class AttendanceSummaryLoadTest:AttendanceSummaryBaseTest
    {
        #region GetAttendanceSummeryReportCount

        [Fact]
        public void Should_Return_GetAttendanceSummeryReportCount_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            attendanceAdjustmentFactory.CreateWith("Test", DateTime.Now,
                new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 10, 10),
                new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Approved, 0).WithTeamMember(tm);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(org), null,commonHelper.ConvertIdToList(branch));

            int cnt = _attendanceSummaryService.GetAttendanceSummeryReportCount(menu, org.Id.ToString(), "", "", null, "", "", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0).ToString(), "");

            Assert.NotEqual(0,cnt);
        }

        [Fact]
        public void Should_Return_GetAttendanceSummeryReportCount_Invalid_DateTo()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            Assert.Throws<InvalidDataException>(() => _attendanceSummaryService.GetAttendanceSummeryReportCount(menu, org.Id.ToString(), "", "", null, "", "","", ""));
        }

        [Fact]
        public void Should_Return_GetAttendanceSummeryReportCount_Invalid_UserMenu()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            Assert.Throws<InvalidDataException>(() => _attendanceSummaryService.GetAttendanceSummeryReportCount(null, org.Id.ToString(), "", "", null, "", "", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0).ToString(), ""));
        }

        #endregion

        #region LoadAttendanceSummeryReport

        [Fact]
        public void Should_Return_LoadAttendanceSummeryReport_Successfully_LoadData()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);

            //const long orgId = 3;

            //var org = _organizationService.LoadById(orgId);
            //var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", _commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);


            var tm = PersistTeamMember(joiningDate, dateTo); 

            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var dept = employmentHistory.Department;

            attendanceAdjustmentFactory.CreateMore(new DateTime(2016,12,1), tm);

            DateTime holyDayDate = new DateTime(2016,12,16);

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //    .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            nightWorkFactory.CreateWith(new DateTime(2016,12,15),(int)HolidayWorkApprovalStatus.Half)
                .WithTeamMember(tm).Persist();

            _overTimeFactory.CreateWith(new DateTime(2016,12, 10), new DateTime(2000,01,01)).WithTeamMember(tm).Persist();
            
            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, menu);
            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016,12, 19), new DateTime(2016,12, 18), "test", 1).WithTeamMember(tm).Persist();

            var a = _attendanceSummaryService.LoadAttendanceSummeryReport(menu, org.Id.ToString(), dept.Id.ToString(),
                branch.Id.ToString(), null, "", startDate.ToString(),endDate.ToString(),"",0,100,"","");
            Assert.NotEqual(0,a.Count());
        }
        
        [Fact]
        public void Should_Return_LoadAttendanceSummeryReport_Exception_For_UserMenu()
        {
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, 31);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var dept = employmentHistory.Department;

            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);

            DateTime holyDayDate = new DateTime(2016, 12, 2);
            
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            nightWorkFactory.CreateWith(new DateTime(2016,12, 7), (int)HolidayWorkApprovalStatus.Half)
                .WithTeamMember(tm).Persist();

            _overTimeFactory.CreateWith(new DateTime(2016,12,10),
                new DateTime(2000, 01, 01)).WithTeamMember(tm).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, menu);
            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016,12,19), 
                new DateTime(2016,12,18), "test", 1).WithTeamMember(tm).Persist();

            Assert.Throws<InvalidDataException>(() => _attendanceSummaryService.LoadAttendanceSummeryReport(null, org.Id.ToString(), dept.Id.ToString(),
                branch.Id.ToString(), null, "", startDate.ToString(), endDate.ToString(), "", 0, 100, "", ""));
        }

        [Fact]
        public void Should_Return_LoadAttendanceSummeryReport_Exception_For_DateTo()
        {
            var startDate = new DateTime(2016, 12, 1);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var dept = employmentHistory.Department;
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);

            DateTime holyDayDate = new DateTime(2016, 12, 2);
            
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            nightWorkFactory.CreateWith(new DateTime(2016,12, 7), (int)HolidayWorkApprovalStatus.Half)
                .WithTeamMember(tm).Persist();

            _overTimeFactory.CreateWith(new DateTime(2016,12, 10),
                new DateTime(2000, 01, 01)).WithTeamMember(tm).Persist();
            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, menu);
            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016,12, 19), new DateTime(2016,12,18), "test", 1).WithTeamMember(tm).Persist();

            Assert.Throws<InvalidDataException>(() => _attendanceSummaryService.LoadAttendanceSummeryReport(menu, org.Id.ToString(), dept.Id.ToString(),
                branch.Id.ToString(), null, "", startDate.ToString(), "", "", 0, 100, "", ""));
        }

        #endregion

    }
}