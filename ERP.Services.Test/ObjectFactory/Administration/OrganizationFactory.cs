﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class OrganizationFactory : ObjectFactoryBase<Organization>
    {
        #region Object Initialization

        IOrganizationService _organizationService;
        public OrganizationFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _organizationService = new OrganizationService(_session);
        }

        #endregion

        #region Create

        public OrganizationFactory Create()
        {
            Object = CreateOrganization(Name + "_" + Guid.NewGuid(), 1, "Org_" + Guid.NewGuid().ToString().Substring(0, 5), "8801811000000", 60, 3.5m, 3.1m, 2.5m, 27,
                0.7m);
            SingleObjectList.Add(Object);
            return this;
        }

        public OrganizationFactory CreateWith(string orgName,
            int rank,
            string shortName,
            string contact,
            decimal basicSalary,
            decimal houseRent,
            decimal medicalAllowance,
            decimal convenyance,
            int monthlyWorkingDay,
            decimal deductionPerAbsent,decimal workingHour = 8)
        {
            Object = CreateOrganization(orgName, rank, shortName, contact, basicSalary, houseRent, medicalAllowance,
                convenyance, monthlyWorkingDay, deductionPerAbsent, null,null,null,null,null,workingHour);
            SingleObjectList.Add(Object);
            return this;
        }

        public OrganizationFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        public OrganizationFactory WithShortName(string shortName)
        {
            Object.ShortName = shortName;
            return this;
        }

        public OrganizationFactory WithRequiredProperties()
        {
            Object.Name = Guid.NewGuid() + "test organization name";
            Object.Rank = 1;
            Object.ShortName = Guid.NewGuid() + "test organization short name";
            Object.Website = Guid.NewGuid() + "test organization website";
            Object.Email = "test_organization_email@test.com";
            Object.ContactNo = "8801965698345";
            Object.LogoImageUrl = Guid.NewGuid() + "test organization Logo Image Url";
            Object.ReportImageUrl = Guid.NewGuid() + "test organization Report Image Url";
            Object.IdFooterImageUrl = Guid.NewGuid() + "test organization Id Footer Image Url";
            Object.WorkingHour = 8;
            return this;
        }

        public OrganizationFactory WithRequiredProperties(string name, string shortname, string website, string email, string contactno, string logoimgurl,
            string reportimgurl, string idfooterimgurl, decimal? basicSalary = 60M, decimal? houseRent = 20M, decimal? medicalAllowance = 10M,
            decimal? convenyance = 10M, int? monthlyWorkingDay = 24, decimal? deductionPerAbsent = 2.5M,decimal workingHour = 8)
        {
            Object.Name = name;
            Object.ShortName = shortname;
            Object.Website = website;
            Object.Email = email;
            Object.ContactNo = contactno;
            Object.LogoImageUrl = logoimgurl;
            Object.ReportImageUrl = reportimgurl;
            Object.IdFooterImageUrl = idfooterimgurl;
            Object.BasicSalary = basicSalary;
            Object.HouseRent = houseRent;
            Object.MedicalAllowance = medicalAllowance;
            Object.Convenyance = convenyance;
            Object.MonthlyWorkingDay = monthlyWorkingDay;
            Object.DeductionPerAbsent = deductionPerAbsent;
            Object.WorkingHour = workingHour;
            return this;
        }

        public OrganizationFactory CreateMore(int numberOfOrganization, int? status = null)
        {
            CreateOrganizationList(numberOfOrganization, status);
            return this;
        }

        private List<Organization> CreateOrganizationList(int numberOfOrganization, int? status = null)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Organization>();
            for (int i = 0; i < numberOfOrganization; i++)
            {
                var name = Guid.NewGuid().ToString().Substring(0, 6) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                var org = CreateOrganization(name, i, Guid.NewGuid().ToString().Substring(0, 6), "8801811410000", 60M, 20M, 20M,
                    20M, 24, 2.5M);
                org.Status = status ?? Organization.EntityStatus.Active;
                list.Add(org);
            }
            ObjectList.AddRange(list);
            return list;
        }

        private Organization CreateOrganization(string orgName, int rank, string shortName, string contact, decimal basicSalary, decimal houseRent, decimal medicalAllowance, decimal convenyance,
            int monthlyWorkingDay,
            decimal deductionPerAbsent,
            string website = null, string email = null, string logoImageUrl = null, string reportImqageUrl = null, string idFooterImageUrl = null,
            decimal workingHour = 8)
        {
            var org = new Organization()
            {
                BusinessId = GetPcUserName(),
                BasicSalary = basicSalary,
                ContactNo = contact,
                Convenyance = convenyance,
                CreateBy = 1,
                ModifyBy = 1,
                DeductionPerAbsent = deductionPerAbsent,
                Email = email ?? "hr@onnorokom.com",
                HouseRent = houseRent,
                MedicalAllowance = medicalAllowance,
                MonthlyWorkingDay = monthlyWorkingDay,
                Name = orgName,
                Rank = rank,
                ShortName = shortName,
                Status = Organization.EntityStatus.Active,
                Website = website ?? "http://onnorokomsoftware.com",
                LogoImageUrl = logoImageUrl ?? "http://onnorokomsoftware.com/images/logo.png",
                ReportImageUrl = reportImqageUrl ?? "http://onnorokomsoftware.com/images/logo.png",
                IdFooterImageUrl = idFooterImageUrl ?? "http://onnorokomsoftware.com/images/logo.png",
                AttendanceStartTime = new DateTime(2010, DateTime.Now.Month, DateTime.Now.Day, 06, 00, 00),
                WorkingHour = workingHour
            };

            return org;
        }

        public OrganizationFactory Persist()
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x =>
                {
                    if (x.Id == 0)
                        _organizationService.SaveOrUpdate(x);
                });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x =>
                {
                    if (x.Id == 0)
                        _organizationService.SaveOrUpdate(x);
                });
            }

            return this;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup

        public void Cleanup()
        {
            DeleteAll();
        }

        #endregion

    }
}
