﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.BusinessModel.Entity;

namespace UdvashERP.Areas.Student.Models
{
    public class AttendanceSearchInfo
    {
        #region Paasing Data View To Controller

        [Required(ErrorMessage = "You need to select Organization")]
        public long OrganizationId { get; set; }
        [Required(ErrorMessage = "You need to select organization")]
        public long ProgramId { get; set; }

        [Required(ErrorMessage = "You need to select session")]
        public long SessionId { get; set; }

       // [Required(ErrorMessage = "You need to select date range [From]")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",ApplyFormatInEditMode = true)] 
        public DateTime StartDate { get; set; }


        //[Required(ErrorMessage = "You need to select date range [To]")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)] 
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "You need to select branch")]
        public long[] SelectedBranch { get; set; }

        [Required(ErrorMessage = "You need to select campus")]
        public long[] SelectedCampus { get; set; }

        [Required(ErrorMessage = "You need to select batch")]
        public long[] SelectedBatch { get; set; }

        [Required(ErrorMessage = "You need to select batch days")]
        public string[] SelectedBatchDays { get; set; }

        [Required(ErrorMessage = "You need to select batch time")]
        public string[] SelectedBatchTime { get; set; }

        [Required(ErrorMessage = "You need to select course")]
        public long[] SelectedCourse { get; set; }

        [Required(ErrorMessage = "You need to select payment status")]
        public long[] SelectedPaymentStatus { get; set; }

        public IEnumerable<SelectListItem> AttendanceTypeList { get; set; }
        public long AttendanceType { get; set; }

        public IEnumerable<SelectListItem> AttendenseStatusList { get; set; }
        public long AttendanceStatus { get; set; }

        public string[] InformationViewList { get; set; }


        [Required(ErrorMessage = "You need to select Exam")]
        public long[] SelectedExam { get; set; }
        [Required(ErrorMessage = "You need to select Lecture")]
        public long[] SelectedLecture{ get; set; }
        #endregion

        public AttendanceSearchInfo()
        {
            //StartDate = DateTime.Now;
            //EndDate = DateTime.Now;
        }
    }
    
}
public static class PaymentStatus
{
    public static int FullPaid { get { return 1; } }
    public static int PaymentDue { get { return 2; } }
    public static int NotEnrolled { get { return 3; } }
}
public static class AttendanceType
{
    public static int ClassAttendance { get { return 1; } }
    public static int ExamAttendance { get { return 2; } }
}
public static class AttendanceStatus
{
    public static int Present { get { return 1; } }
    public static int Absent { get { return 2; } }
    public static int NotEnrolled { get { return 3; } }
}
