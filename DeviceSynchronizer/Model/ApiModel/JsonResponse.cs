﻿namespace DeviceSynchronizer.Model.ApiModel
{
    public class JsonResponse
    {
        public bool IsSuccess { get; set; }
        public dynamic Data { get; set; }

        public JsonResponse()
        {
        }

        public JsonResponse(bool isSuccess, dynamic data)
        {
            this.IsSuccess = isSuccess;
            this.Data = data;
        }
    }
}
