﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate.Criterion;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.UserAuth;
using UdvashERP.Services.Hr.AttendanceSummaryServices;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class TeamMemberController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly IBatchService _batchService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly ICourseService _courseService;
        private readonly IExamsService _examsService;
        private readonly IOrganizationService _organizationService;
        private readonly IInstitutionService _institutionService;
        private readonly ITeamMemberService _hrMemberService;
        private readonly IExamService _hrExamService;
        private readonly IHrBoardService _hrBoardService;
        private readonly IDepartmentService _hrDepartmentService;
        private readonly IDesignationService _hrDesignationService;
        private readonly IShiftService _shiftService;
        private readonly IEmploymentHistoryService _employmentHistoryService;
        private readonly IEmploymentHistoryLogService _employmentHistoryLogService;
        private readonly ISalaryHistoryService _salaryHistoryService;
        private readonly ISalaryHistoryLogService _salaryHistoryLogService;
        private readonly IMentorHistoryService _hrMentorHistoryService;
        private readonly IShiftWeekendHistoryService _hrShiftWeekendHistoryService;
        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _userMenu;
        private readonly IUserService _userService;
        private readonly IAddressService _addressService;
        private readonly IMaritalInfoService _maritalInfoService;
        private readonly IChildrenInfoService _childrenInfoService;
        private readonly IAcademicInfoService _academicInfoService;
        private readonly INomineeInfoService _nomineeInfoService;
        private readonly ITrainingInfoService _trainingInfoService;
        private readonly IJobExperienceService _hrJobExpService;
        private readonly ITeamMemberImagesMediaService _teamMemberImagesMediaService;
        private readonly ITeamMemberNomineeMediaService _teamMemberNomineeImagesMediaService;
        private readonly ITeamMemberCertificateMediaService _teamMemberCertificateImagesMediaService;
        private readonly ITdsHistoryService _tdsHistoryService;
        private readonly IAttendanceSummaryService _attendanceSummaryService;
        private readonly ILeaveService _leaveService;
        private readonly IBankHistoryService _bankHistoryService;
        private readonly IBankService _bankService;
        private readonly IBankBranchService _bankBranchService;
        IMemberFingerPrintService _memberFingerPrintService;

        public TeamMemberController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                var sessionMedia = NHibernateMediaSessionFactory.OpenSession();
                _sessionService = new SessionService(session);
                _userService = new UserService(session);
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _batchService = new BatchService(session);
                _courseService = new CourseService(session);
                _examsService = new ExamsService(session);
                _institutionService = new InstituteService(session);
                _organizationService = new OrganizationService(session);
                _hrDepartmentService = new DepartmentService(session);
                _hrDesignationService = new DesignationService(session);
                _hrMemberService = new TeamMemberService(session);
                _hrExamService = new ExamService(session);
                _hrBoardService = new HrBoardService(session);

                _shiftService = new ShiftService(session);
                _employmentHistoryLogService = new EmploymentHistoryLogService(session);
                _employmentHistoryService = new EmploymentHistoryService(session);
                _salaryHistoryService = new SalaryHistoryService(session);
                _salaryHistoryLogService = new SalaryHistoryLogService(session);
                _hrMentorHistoryService = new MentorHistoryService(session);
                _hrShiftWeekendHistoryService = new ShiftWeekendHistoryService(session);
                _addressService = new AddressService(session);
                _maritalInfoService = new MaritalInfoService(session);
                _childrenInfoService = new ChildrenInfoService(session);
                _nomineeInfoService = new NomineeInfoService(session);
                _academicInfoService = new AcademicInfoService(session);
                _trainingInfoService = new TrainingInfoService(session);
                _hrJobExpService = new JobExperienceService(session);
                _commonHelper = new CommonHelper();
                _teamMemberImagesMediaService = new TeamMemberImagesMediaService(sessionMedia);
                _teamMemberNomineeImagesMediaService = new TeamMemberNomineeMediaService(sessionMedia);
                _teamMemberCertificateImagesMediaService = new TeamMemberCertificateMediaService(sessionMedia);

                _leaveService = new LeaveService(session);

                _tdsHistoryService = new TdsHistoryService(session);
                _attendanceSummaryService = new AttendanceSummaryService(session);
                _bankHistoryService = new BankHistoryService(session);
                _bankService = new BankService(session);
                _bankBranchService = new BankBranchService(session);
                _memberFingerPrintService = new MemberFingerPrintService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Index/Manage Page

        public ActionResult Index(string hrMemberInfo)
        {
            var memberInfo = new JavaScriptSerializer().Deserialize<MemberViewModel>(hrMemberInfo);
            ViewBag.SuccessMessage = "Team member added successfully";
            return View("MemberInfo", memberInfo);
        }
        #endregion

        #region Operational Function

        #region Save Operation

        public ActionResult Create()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName");
            var empStatus = from MemberEmploymentStatus mt in Enum.GetValues(typeof(MemberEmploymentStatus))
                            where (int)mt != 6
                            select new { Id = (int)mt, Name = mt.ToString() };
            ViewBag.EmploymentStatus = new SelectList(empStatus, "Id", "Name");
            var weekDays = from WeekDay ed in Enum.GetValues(typeof(WeekDay))
                           select new { Id = (int)ed, Name = ed.ToString() };
            ViewBag.WeekDays = new SelectList(weekDays, "Id", "Name");
            var genders = from Gender ed in Enum.GetValues(typeof(Gender))
                          where (int)ed != 3
                          select new { Id = (int)ed, Name = ed.ToString() };
            var maritalStatus = from MaritalType ed in Enum.GetValues(typeof(MaritalType))
                                select new { Id = (int)ed, Name = ed.ToString() };
            ViewBag.Gender = new SelectList(genders, "Id", "Name");
            ViewBag.MaritalStatus = new SelectList(maritalStatus, "Id", "Name");
            return View();
        }

        public ActionResult CreateMember(string nickName, string personalContact, int gender, int maritalStatus, long organizationId, long branchId, long campusId, long departmentId, long designationId, int employmentStatus, string joiningDate, string dateTo, long officeShiftId, int weekend, int mentorPin, string mentorName)
        {
            try
            {
                //Add validation check
                var orgObj = _organizationService.LoadById(organizationId);
                //var brObj = _branchService.GetBranch(branchId);
                var camObj = _campusService.GetCampus(campusId);
                var deptObj = _hrDepartmentService.LoadById(departmentId);
                var desigObj = _hrDesignationService.LoadById(designationId);
                var shiftObj = _shiftService.LoadById(officeShiftId);

                var tmObj = new TeamMember();
                tmObj.Name = nickName.Trim();
                tmObj.PersonalContact = personalContact.Trim();
                tmObj.Gender = gender;
                tmObj.Rank = 10;
                tmObj.IsSalarySheet = true;
                tmObj.IsAutoDeduction = true;

                var empHistory = new EmploymentHistory()
                {
                    EmploymentStatus = employmentStatus,
                    Campus = camObj,
                    Designation = desigObj,
                    Department = deptObj,
                    TeamMember = tmObj,
                    Rank = 10,
                    EffectiveDate = Convert.ToDateTime(joiningDate)
                };
                var hrShiftWeekendHistory = new ShiftWeekendHistory()
                {
                    Weekend = weekend,
                    Shift = shiftObj,
                    Organization = orgObj,
                    TeamMember = tmObj,
                    EffectiveDate = Convert.ToDateTime(joiningDate)
                };
                var hrMentorHistory = new MentorHistory()
                {
                    Mentor = _hrMemberService.GetMember(mentorPin),
                    //   MentorName = mentorName,
                    Pin = mentorPin,
                    Designation = desigObj,
                    Department = deptObj,
                    Organization = orgObj,
                    TeamMember = tmObj,
                    EffectiveDate = Convert.ToDateTime(joiningDate),
                };
                var hrMaritalInfo = new MaritalInfo()
                {
                    MaritalStatus = maritalStatus,
                    TeamMember = tmObj
                };
                tmObj.EmploymentHistory.Add(empHistory);
                tmObj.ShiftWeekendHistory.Add(hrShiftWeekendHistory);
                tmObj.MentorHistory.Add(hrMentorHistory);
                tmObj.MaritalInfo.Add(hrMaritalInfo);

                TeamMember savedMember = _hrMemberService.SaveNew(tmObj, joiningDate, dateTo);

                //var hrMemberLeaveSummary = new MemberLeaveSummary();
                //var memberleaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(organizationId, tmObj.Gender, 
                //    employmentStatus, maritalStatus);

                //TeamMember savedMember = _hrMemberService.Save(tmObj, maritalStatus, organizationId, branchId, campusId, departmentId,
                //    designationId, employmentStatus, joiningDate, dateTo, officeShiftId, weekend, mentorPin, mentorName);

                var hrMember = new MemberViewModel();
                hrMember.Id = savedMember.Id;
                hrMember.Pin = savedMember.Pin;
                hrMember.NickName = nickName;
                hrMember.PersonalContact = personalContact;
                hrMember.Organization = _organizationService.LoadById(organizationId).ShortName;
                hrMember.Branch = _branchService.GetBranch(branchId).Name;
                hrMember.Campus = _campusService.GetCampus(campusId).Name;
                hrMember.Department = _hrDepartmentService.LoadById(departmentId).Name;
                hrMember.Designation = _hrDesignationService.LoadById(designationId).Name;
                hrMember.EmploymentStatus = ((MemberEmploymentStatus)employmentStatus).ToString();
                hrMember.JoiningDate = Convert.ToDateTime(joiningDate).ToString("yyyy-MM-dd");
                hrMember.DateTo = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd");
                hrMember.OfficeShift = _shiftService.LoadById(officeShiftId).Name;
                hrMember.Weekend = weekend;
                hrMember.WeekendName = ((WeekDay)weekend).ToString();
                hrMember.MentorPin = mentorPin.ToString();
                hrMember.MentorName = mentorName;
                hrMember.Gender = ((Gender)gender).ToString();
                hrMember.MaritalStatus = ((MaritalType)maritalStatus).ToString();

                var memberInfo = new JavaScriptSerializer().Serialize(hrMember);
                return
                    Json(new { IsSuccess = true, Message = "Team member added successfully", HrMemberInfo = memberInfo });
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (DataNotFoundException ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, Message = WebHelper.CommonErrorMessage });
            }
        }
        #endregion

        #region Assign TeamMember To User

        public ActionResult AssignTeamMemberToUser()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName");
                ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.TeamMemberList = new SelectList(new List<TeamMember>(), "Id", "Name");
                ViewBag.UserList = new SelectList(new List<AspNetUser>(), "Id", "AspNetUser.Email");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public JsonResult AssignTeamMemberToUser(long teamMemberId, long userId)
        {
            try
            {
                if (_hrMemberService.AssignTeamMemberToUser(teamMemberId, userId))
                {
                    if (userId == 0)
                    {
                        return Json(new Response(true, "User Unassign Successfully Done"));
                    }
                    else
                    {
                        return Json(new Response(true, "User Assign Successfully Done"));
                    }
                }
                else
                {
                    return Json(new Response(false, "User Assign Fail... Please Try Again.."));
                }
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region offical Details
        public ActionResult SearchOfficialDetails(string memberPin = "")
        {
            ViewBag.MemberHasInfo = null;
            try
            {

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (TempData["AddEmploymentHistory"] != null)
                {
                    ViewBag.SuccessMessage = TempData["AddEmploymentHistory"];
                }
                if (TempData["SaveOrUpdateBankHistory"] != null)
                {
                    ViewBag.SuccessMessage = TempData["SaveOrUpdateBankHistory"];
                }
                if (TempData["SaveOrUpdateSalaryHistory"] != null)
                {
                    ViewBag.SuccessMessage = TempData["SaveOrUpdateSalaryHistory"];
                }

                ViewBag.MemberPin = memberPin;
                if (memberPin != "")
                {
                    var memberIdList = _hrMemberService.LoadHrAuthorizedTeamMemberId(_userMenu, DateTime.Now.Date,
                        pinList: new List<int>() { Convert.ToInt32(memberPin) });
                    if (memberIdList == null || memberIdList.Count == 0)
                    {
                        throw new DataNotFoundException("Member is not found/authorized.");
                    }
                    int memPin = Convert.ToInt32(memberPin);
                    ViewBag.MemberHasInfo = true;
                    TeamMember member = _hrMemberService.GetByPin(memPin);
                    ViewBag.TeamMember = member;

                    #region Member Basic Inforamtion

                    object campus = new Campus();
                    if (member == null)
                        throw new InvalidDataException(
                            "There is no information found with this PIN number. Plase try again!");
                    string department, designation, joindate, todate, officeShift;
                    int empStatus, weekend, mentorPin;
                    _hrMemberService.GetCurrentInfos(member.Id, out campus, out department, out designation,
                        out empStatus, out joindate, out todate, out officeShift, out weekend, out mentorPin);
                    var campusValue = (Campus)campus;
                    ViewBag.Organization = (campusValue != null) ? campusValue.Branch.Organization.ShortName : "N/A";
                    ViewBag.Branch = (campusValue != null) ? campusValue.Branch.Name : "N/A";
                    ViewBag.Campus = (campusValue != null) ? campusValue.Name : "N/A";
                    ViewBag.department = department;
                    ViewBag.empStatus = (empStatus != null) ? ((MemberEmploymentStatus)empStatus).ToString() : "N/A";
                    ViewBag.joindate = joindate;
                    ViewBag.todate = todate;
                    ViewBag.officeShift = officeShift;
                    ViewBag.weekend = ((WeekDay)weekend).ToString();
                    TeamMember mentorInfo = _hrMemberService.GetByPin(mentorPin);
                    ViewBag.mentorPin = (mentorInfo != null) ? mentorPin.ToString() : "N/A";
                    ViewBag.mentorName = (mentorInfo != null) ? mentorInfo.Name : "N/A";
                    ViewBag.designation = designation;
                    ViewBag.Gender = ((Gender)Convert.ToInt32(member.Gender)).ToString();
                    ViewBag.MaritalStatus =
                        ((MaritalType)
                            Convert.ToInt32(
                                member.MaritalInfo.OrderByDescending(x => x.Id).FirstOrDefault().MaritalStatus))
                            .ToString();

                    ViewBag.CardNumber = "";
                    ViewBag.OfficialContact = "";
                    ViewBag.OfficialEmail = "";
                    ViewBag.OfficalDetailsId = "";
                    ViewBag.OfficalDetailsStatus = "";

                    if (member.MemberOfficialDetails.Any())
                    {
                        MemberOfficialDetail memberOfficialDetail =
                            member.MemberOfficialDetails.FirstOrDefault(
                                x =>
                                    x.TeamMember.Id == member.Id && x.Status != MemberOfficialDetail.EntityStatus.Delete);
                        if (memberOfficialDetail != null)
                        {
                            ViewBag.CardNumber = memberOfficialDetail.CardNo;
                            ViewBag.OfficialContact = memberOfficialDetail.OfficialContact;
                            ViewBag.OfficialEmail = memberOfficialDetail.OfficialEmail;
                            ViewBag.OfficalDetailsId = memberOfficialDetail.Id;
                            ViewBag.OfficalDetailsStatus = memberOfficialDetail.Status;
                        }
                    }

                    #endregion Member Basic Information

                    #region Finger Print Information
   
                    ViewBag.FingerPrintInformations = member.MemberFingerPrints;// memberFingerTemplateList;

                    #endregion


                    #region Employment History

                    ViewBag.TeamMemberEmploymentHistories = _employmentHistoryService.LoadEmploymentHistories(member);

                    #endregion

                    #region Employment Salary History

                    ViewBag.TeamMemberSalaryHistories = _salaryHistoryService.LoadSalaryHistories(member);

                    #endregion

                    #region Mentor History

                    ViewBag.TeamMemberMentorHistories = _hrMentorHistoryService.LoadMentorHistory(member.Pin);

                    #endregion

                    #region Shift Weekend History

                    ViewBag.TeamMemberShiftWeekendHistories =
                        _hrShiftWeekendHistoryService.LoadShiftWeekendHistory(member.Id);

                    #endregion

                    #region Employment Tds History

                    ViewBag.TeamMemberTdsHistories = _tdsHistoryService.LoadTdsHistories(member);

                    #endregion

                    #region Tax Information

                    TeamMemberTaxInformationViewModel teamMemberTaxInformationViewModel = new TeamMemberTaxInformationViewModel
                    {
                        Id = member.Id,
                        Pin = member.Pin,
                        TaxesCircle = member.TaxesCircle,
                        TaxesZone = member.TaxesZone,
                        TinNo = member.TinNo
                    };
                    ViewBag.TeamMemberTaxInformationViewModel = teamMemberTaxInformationViewModel;

                    #endregion

                    #region Bank Branch History

                    List<BankHistory> teamMemberBankHistoryList =
                        _bankHistoryService.LoadTeamMemberBankHistory(member.Id, DateTime.Now).ToList();
                    ViewBag.TeamMemberBankHistoryList = teamMemberBankHistoryList;

                    #endregion
                }
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMEssage = ex.Message;
            }
            catch (DataNotFoundException ex)
            {
                // ViewBag.MemberHasInfo = false;
                ViewBag.ErrorMEssage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.MemberHasInfo = false;
                _logger.Error(ex);
            }

            return View();
        }
        [HttpPost]
        public JsonResult UpdateTeamMemberBasicInformation(int memberPin, string nickName, string personalContact, string cardNumber, string officialContact,
            string officialEmail, string officalDetailsId, string officalDetailsStatus)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                var member = _hrMemberService.GetByPin(memberPin);
                if (member == null) return Json(new Response(false, "Invalid Team member"));
                int status = MemberOfficialDetail.EntityStatus.Active;
                if (!String.IsNullOrEmpty(officalDetailsStatus.Trim()))
                {
                    if (Convert.ToInt32(officalDetailsStatus) == MemberOfficialDetail.EntityStatus.Inactive)
                    {
                        status = MemberOfficialDetail.EntityStatus.Inactive;
                    }
                }

                member.Name = nickName;
                member.PersonalContact = personalContact;

                if (!String.IsNullOrEmpty(officalDetailsId.Trim()))
                {
                    MemberOfficialDetail memberOfficialDetail = member.MemberOfficialDetails.FirstOrDefault(x => x.Status != MemberOfficialDetail.EntityStatus.Delete && x.Id == Convert.ToInt64(officalDetailsId));
                    if (memberOfficialDetail != null)
                    {
                        memberOfficialDetail.CardNo = cardNumber;
                        memberOfficialDetail.OfficialContact = officialContact;
                        memberOfficialDetail.OfficialEmail = officialEmail;
                        memberOfficialDetail.Status = status;
                        memberOfficialDetail.ModifyBy = Convert.ToInt64(HttpContext.User.Identity.GetUserId());
                        memberOfficialDetail.ModificationDate = dateTime;
                    }
                }
                else
                {
                    var modDetail = new MemberOfficialDetail();
                    modDetail.CardNo = cardNumber;
                    modDetail.OfficialContact = officialContact;
                    modDetail.OfficialEmail = officialEmail;
                    modDetail.Status = status;
                    modDetail.CreateBy = Convert.ToInt64(HttpContext.User.Identity.GetUserId());//Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId())
                    modDetail.ModifyBy = Convert.ToInt64(HttpContext.User.Identity.GetUserId());
                    modDetail.CreationDate = dateTime;
                    modDetail.ModificationDate = dateTime;
                    member.MemberOfficialDetails.Clear();
                    member.MemberOfficialDetails.Add(modDetail);
                }

                //var modDetail = new MemberOfficialDetail();
                //modDetail.CardNo = cardNumber;
                //modDetail.OfficialContact = officialContact;
                //modDetail.OfficialEmail = officialEmail;
                //member.MemberOfficialDetails.Clear();
                //member.MemberOfficialDetails.Add(modDetail);
                if (_hrMemberService.UpdateBasicInformation(member))
                {
                    return Json(new Response(true, "Member Basic information update successfully"));
                }
                else
                    return Json(new Response(false, "Member Basic information update fail!"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #region Finger Template

        public   ActionResult EditFingerPrintTemplate(long id)
        {
            MemberFingerPrint memberFingerTemplate = new MemberFingerPrint();
            try
            {
                memberFingerTemplate = _memberFingerPrintService.GetMemberFingerPrint(id);
                if (memberFingerTemplate == null)
                {
                    RedirectToAction("SearchOfficialDetails");
                }
                   
            }
            catch(Exception ex)
            {
                _logger.Error(ex.Message);
            }

            return View(memberFingerTemplate);
        }

        [HttpPost]
        public ActionResult EditFingerPrintTemplate(long id, int index)
        {
            try
            {
                var memberFingerTemplate=_memberFingerPrintService.GetMemberFingerPrint(id);
                if(memberFingerTemplate==null)
                    throw new MessageException("Invalid Finger Template");
                _memberFingerPrintService.Update(memberFingerTemplate, index);

            }
            catch(Exception ex)
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

            return Json(new Response(true, "Clear Data"));
        }


        #endregion

        #region Employment History
        public ActionResult AddEmploymentHistory(long memberId)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                TeamMember member = _hrMemberService.LoadById(memberId);
                Organization memberCurrentOrganization = _hrMemberService.GetTeamMemberOrganization(null, member.Id);
                long memberOrgId = memberCurrentOrganization.Id;
                List<Organization> organizationList = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                if (organizationList.Any())
                {
                    organizationList = organizationList.Where(x => x.Id == memberOrgId).ToList();
                }

                ViewBag.Organization = new SelectList(organizationList, "Id", "ShortName", memberOrgId);
                ViewBag.Branch = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(memberOrgId), null, null, false), "Id", "Name");
                ViewBag.Campus = Enumerable.Empty<SelectListItem>();
                ViewBag.Department = new SelectList(_hrDepartmentService.LoadDepartment(_commonHelper.ConvertIdToList(memberOrgId)), "Id", "Name");
                ViewBag.Designation = new SelectList(_hrDesignationService.LoadDesignation(_commonHelper.ConvertIdToList(memberOrgId)), "Id", "Name");

                var empStatus = from MemberEmploymentStatus mt in Enum.GetValues(typeof(MemberEmploymentStatus))
                                select new { Id = (int)mt, Name = mt.ToString() };
                ViewBag.MemberType = new SelectList(empStatus, "Id", "Name");
                ViewBag.MemberId = member.Id;
                ViewBag.MemberPin = member.Pin;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

            return View();
        }
        [HttpPost]
        public ActionResult AddEmploymentHistory(long memberId, long organizationId, long branchId, long campusId, long departmentId, long designationId, int memberType, string effectiveDate)
        {
            string message = "";
            bool steate = false;
            try
            {
                TeamMember member = _hrMemberService.LoadById(memberId);
                if (_employmentHistoryService.AddEmploymentHistory(member, organizationId, branchId, campusId, departmentId, designationId, memberType, effectiveDate))
                {
                    message = "Member Employment Add successfully";
                    TempData["AddEmploymentHistory"] = message;
                    steate = true;
                }
                else
                {
                    message = "Member Basic information update fail!";
                }

            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
            }

            return Json(new { IsSuccess = steate, Message = message });
        }
        public ActionResult EditEmploymentHistory(long id)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var employmentHistory = _employmentHistoryService.GetEmploymentHistory(id);
                var memberOrgId = employmentHistory.Campus.Branch.Organization.Id;
                var memberBranchId = employmentHistory.Campus.Branch.Id;
                var memberCampusId = employmentHistory.Campus.Id;

                List<Organization> organizationList = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                if (organizationList.Any())
                {
                    organizationList = organizationList.Where(x => x.Id == memberOrgId).ToList();
                }

                ViewBag.Organization = new SelectList(organizationList, "Id", "ShortName", memberOrgId);
                ViewBag.Branch = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(memberOrgId), null, null, false), "Id", "Name", memberBranchId);
                ViewBag.Campus = new SelectList(_campusService.LoadAuthorizeCampus(_userMenu, _commonHelper.ConvertIdToList(memberOrgId), null, _commonHelper.ConvertIdToList(memberBranchId), null, false), "Id", "Name", memberCampusId);
                ViewBag.Department = new SelectList(_hrDepartmentService.LoadDepartment(_commonHelper.ConvertIdToList(memberOrgId)), "Id", "Name", employmentHistory.Department.Id);
                ViewBag.Designation = new SelectList(_hrDesignationService.LoadDesignation(_commonHelper.ConvertIdToList(memberOrgId)), "Id", "Name", employmentHistory.Designation.Id);

                var empStatus = from MemberEmploymentStatus mt in Enum.GetValues(typeof(MemberEmploymentStatus))
                                select new { Id = (int)mt, Name = mt.ToString() };
                ViewBag.MemberType = new SelectList(empStatus, "Id", "Name", Convert.ToInt32(employmentHistory.EmploymentStatus));
                ViewBag.MemberId = employmentHistory.TeamMember.Id;
                ViewBag.MemberPin = employmentHistory.TeamMember.Pin;
                ViewBag.EmploymentHistory = employmentHistory;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }
        [HttpPost]
        public ActionResult EditEmploymentHistory(long id, long organizationId, long branchId, long campusId, long departmentId, long designationId, int memberType, string effectiveDate)
        {
            try
            {
                var employmentHistory = _employmentHistoryService.GetEmploymentHistory(id);
                if (_employmentHistoryService.UpdateEmploymentHistory(employmentHistory, campusId, departmentId, designationId, memberType, effectiveDate))
                {
                    TempData["AddEmploymentHistory"] = "Member Employment Edit successfully";
                    return Json(new Response(true, "Member employment history add successfully!"));
                }
                else
                    return Json(new Response(false, "Member Employment Edit fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult DeleteEmploymentHistory(long id)
        {
            try
            {
                var employmentHistory = _employmentHistoryService.GetEmploymentHistory(id);
                if (_employmentHistoryService.DeleteEmploymentHistory(employmentHistory))
                {
                    //TempData["AddEmploymentHistory"] = "Member Employment Delete successfully";
                    return Json(new Response(true, "Member employment history Delete successfully!"));
                }
                else
                    return Json(new Response(false, "Member Employment Delete fail!"));

            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #region Salary History

        public ActionResult AddSalaryHistory(long memberId)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = Enumerable.Empty<SelectListItem>();
            ViewBag.DepartmentList = Enumerable.Empty<SelectListItem>();
            ViewBag.BranchList = Enumerable.Empty<SelectListItem>();
            ViewBag.CampusList = Enumerable.Empty<SelectListItem>();
            ViewBag.SalaryPurposeList = Enumerable.Empty<SelectListItem>();
            ViewBag.MemberId = 0;
            ViewBag.MemberPin = 0;
            try
            {
                TeamMember member = _hrMemberService.LoadById(memberId);
                long memberOrgId = _hrMemberService.GetTeamMemberOrganization(null, memberId).Id;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName", memberOrgId);
                ViewBag.DepartmentList = new SelectList(_hrDepartmentService.LoadAuthorizedDepartment(_userMenu, _commonHelper.ConvertIdToList(memberOrgId)), "Id", "Name");
                ViewBag.DesignationList = new SelectList(_hrDesignationService.LoadDesignation(_commonHelper.ConvertIdToList(memberOrgId)), "Id", "Name");
                ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(memberOrgId), null, null, false), "Id", "Name");
                ViewBag.SalaryPurposeList = new SelectList(_commonHelper.LoadEmumToDictionary<SalaryPurpose>(), "Key", "Value");
                ViewBag.MemberId = member.Id;
                ViewBag.MemberPin = member.Pin;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult AddSalaryHistory(SalaryHistoryViewModel salaryHistoryViewModel)
        {
            string message = "";
            bool state = false;
            try
            {
                if (salaryHistoryViewModel == null)
                    throw new InvalidDataException("Invalid salary history");
                SalaryHistory salaryHistory = new SalaryHistory();
                salaryHistoryViewModel.GetModelFromViewModel(salaryHistory);
                salaryHistory.SalaryOrganization = _organizationService.LoadById(salaryHistoryViewModel.OrganizationId);
                salaryHistory.SalaryDepartment = _hrDepartmentService.LoadById(salaryHistoryViewModel.DepartmentId);
                salaryHistory.SalaryDesignation = _hrDesignationService.LoadById(salaryHistoryViewModel.DesignationId);
                salaryHistory.SalaryCampus = _campusService.GetCampus(salaryHistoryViewModel.CampusId);
                salaryHistory.SalaryPurpose = (SalaryPurpose)salaryHistoryViewModel.SalaryPurpose;
                salaryHistory.TeamMember = _hrMemberService.LoadById(salaryHistoryViewModel.MemberId);
                salaryHistory.Rank = 10;
                salaryHistory.Status = SalaryHistory.EntityStatus.Active;
                _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistory);
                state = true;
                TempData["SaveOrUpdateSalaryHistory"] = "Team Member's Salary History is Successfully Saved.";
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);

            }
            return Json(new { IsSuccess = state, Message = message });
        }

        public ActionResult EditSalaryHistory(long id)
        {
            SalaryHistoryViewModel salaryHistoryViewModel = new SalaryHistoryViewModel();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                SalaryHistory salaryHistory = _salaryHistoryService.GetSalaryHistory(id);
                long memberSalaryOrgId = salaryHistory.SalaryCampus.Branch.Organization.Id;
                long memberBranchId = salaryHistory.SalaryCampus.Branch.Id;
                long memberCampusId = salaryHistory.SalaryCampus.Id;
                long memberDepartmentId = salaryHistory.SalaryDepartment.Id;
                long memberDesignationId = salaryHistory.SalaryDesignation.Id;
                int salaryPurpose = (int)salaryHistory.SalaryPurpose;

                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", memberSalaryOrgId);
                ViewBag.DepartmentList = new SelectList(_hrDepartmentService.LoadAuthorizedDepartment(_userMenu, _commonHelper.ConvertIdToList(memberSalaryOrgId)), "Id", "Name", memberDepartmentId);
                ViewBag.DesignationList = new SelectList(_hrDesignationService.LoadDesignation(_commonHelper.ConvertIdToList(memberSalaryOrgId)), "Id", "Name", memberDesignationId);
                ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(memberSalaryOrgId), null, null, false), "Id", "Name", memberBranchId);
                ViewBag.CampusList = new SelectList(_campusService.LoadAuthorizeCampus(_userMenu, _commonHelper.ConvertIdToList(memberSalaryOrgId), null, _commonHelper.ConvertIdToList(memberBranchId), null, false), "Id", "Name", memberCampusId);
                ViewBag.SalaryPurposeList = new SelectList(_commonHelper.LoadEmumToDictionary<SalaryPurpose>(), "Key", "Value", salaryPurpose);
                ViewBag.MemberId = salaryHistory.TeamMember.Id;
                ViewBag.MemberPin = salaryHistory.TeamMember.Pin;

                salaryHistoryViewModel.GetViewModelFromModel(salaryHistory);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return View(salaryHistoryViewModel);
        }

        [HttpPost]
        public ActionResult EditSalaryHistory(SalaryHistoryViewModel salaryHistoryViewModel)
        {
            string message = "";
            bool state = false;
            try
            {
                //SalaryHistory salaryHistory = _salaryHistoryService.GetSalaryHistory(salaryHistoryViewModel.Id);
                SalaryHistory salaryHistory = new SalaryHistory();
                salaryHistoryViewModel.GetModelFromViewModel(salaryHistory);
                salaryHistory.SalaryOrganization = _organizationService.LoadById(salaryHistoryViewModel.OrganizationId);
                salaryHistory.SalaryDepartment = _hrDepartmentService.LoadById(salaryHistoryViewModel.DepartmentId);
                salaryHistory.SalaryDesignation = _hrDesignationService.LoadById(salaryHistoryViewModel.DesignationId);
                salaryHistory.SalaryCampus = _campusService.GetCampus(salaryHistoryViewModel.CampusId);
                salaryHistory.SalaryPurpose = (SalaryPurpose)salaryHistoryViewModel.SalaryPurpose;
                salaryHistory.TeamMember = _hrMemberService.LoadById(salaryHistoryViewModel.MemberId);

                _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistory);
                state = true;
                TempData["SaveOrUpdateSalaryHistory"] = "Team Member's Salary History is Successfully Updated.";
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = state, Message = message });
        }

        [HttpPost]
        public ActionResult DeleteSalaryHistory(long id)
        {
            bool state = false;
            string message = "";
            try
            {
                var salaryHistory = _salaryHistoryService.GetSalaryHistory(id);
                _salaryHistoryService.DeleteSalaryHistory(salaryHistory);
                state = true;
                message = "Team Member's Salary History Delete successfully!";
            }
            catch (DependencyException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }

            return Json(new { IsSuccess = state, Message = message });
        }

        #endregion

        #region Tds History
        public ActionResult AddTdsHistory(long memberId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var member = _hrMemberService.LoadById(memberId);
                ViewBag.MemberId = member.Id;
                ViewBag.MemberPin = member.Pin;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }
        [HttpPost]
        public ActionResult AddTdsHistory(long memberId, string tdsAmount, string effectiveDate)
        {
            try
            {
                var member = _hrMemberService.LoadById(memberId);
                if (_tdsHistoryService.AddTdsHistory(member, tdsAmount, effectiveDate))
                {
                    TempData["AddEmploymentHistory"] = "Member TDS History Add successfully";
                    return Json(new Response(true, "Member TDS history add successfully!"));
                }
                else
                    return Json(new Response(false, "Member TDS history add fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }


        public ActionResult EditTdsHistory(long id)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var tdsHistory = _tdsHistoryService.GetTdsHistory(id);
                ViewBag.MemberId = tdsHistory.TeamMember.Id;
                ViewBag.MemberPin = tdsHistory.TeamMember.Pin;
                ViewBag.TdsHistory = tdsHistory;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }
        [HttpPost]
        public ActionResult EditTdsHistory(long id, string tdsAmount, string effectiveDate)
        {
            try
            {
                var tdsHistory = _tdsHistoryService.GetTdsHistory(id);
                if (_tdsHistoryService.UpdateTdsHistory(tdsHistory, tdsAmount, effectiveDate))
                {
                    TempData["AddEmploymentHistory"] = "Member TDS Edit successfully";
                    return Json(new Response(true, "Member TDS history add successfully!"));
                }
                else
                    return Json(new Response(false, "Member TDS Edit fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult DeleteTdsHistory(long id)
        {
            try
            {
                var tdsHistory = _tdsHistoryService.GetTdsHistory(id);
                if (_tdsHistoryService.DeleteTdsHistory(tdsHistory))
                {
                    return Json(new Response(true, "Member TDS history Delete successfully!"));
                }
                else
                    return Json(new Response(false, "Member TDS Delete fail!"));

            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Bank History

        public ActionResult AddBankHistory(long memberId)
        {
            TeamMemberBankHistoryViewModel teamMemberBankHistoryViewModel = new TeamMemberBankHistoryViewModel();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                TeamMember member = _hrMemberService.LoadById(memberId);

                AuthorizedTeamMemberPin(member.Pin);

                teamMemberBankHistoryViewModel.TeamMemberId = member.Id;
                teamMemberBankHistoryViewModel.TeamMemberPin = member.Pin;
                teamMemberBankHistoryViewModel.AccountTitle = !String.IsNullOrEmpty(member.FullNameEng) ? member.FullNameEng : member.Name;
                teamMemberBankHistoryViewModel.EffectiveDate = DateTime.Now.ToString("yyyy-MM-dd");
                List<Bank> bankList = _bankService.LoadBankList().ToList();
                ViewBag.BankSelectList = new SelectList(bankList, "Id", "Name");
                ViewBag.BankBranchSelectList = new SelectList(new List<BankBranch>(), "Id", "Name");
            }
            catch (DataNotFoundException ex)
            {
                ViewBag.ErrorMEssage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMEssage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View(teamMemberBankHistoryViewModel);
        }

        [HttpPost]
        public JsonResult SaveBankHistory(TeamMemberBankHistoryViewModel bankHistoryViewModel)
        {
            bool successState = false;
            string message = "";
            try
            {
                BankHistory teamMemberBankHistory;
                if (bankHistoryViewModel.Id > 0)
                {
                    teamMemberBankHistory = _bankHistoryService.GetBankHistory(bankHistoryViewModel.Id);
                    if (teamMemberBankHistory == null)
                        throw new InvalidDataException("Invalid Bank History Information.");
                }
                else
                {
                    teamMemberBankHistory = new BankHistory
                    {
                        Status = BankHistory.EntityStatus.Active
                    };
                }
                teamMemberBankHistory.AccountNumber = bankHistoryViewModel.AccountNumber;
                teamMemberBankHistory.Name = bankHistoryViewModel.AccountTitle;
                teamMemberBankHistory.TeamMember = _hrMemberService.LoadById(bankHistoryViewModel.TeamMemberId);
                teamMemberBankHistory.BankBranch = _bankBranchService.GetBankBranch(bankHistoryViewModel.BankBranchId);
                teamMemberBankHistory.EffectiveDate = Convert.ToDateTime(bankHistoryViewModel.EffectiveDate);
                _bankHistoryService.SaveOrUpdateBankHistory(teamMemberBankHistory);
                message = bankHistoryViewModel.Id > 0 ? "TeamMember Bank History Update successfully" : "TeamMember Bank History Add successfully";
                TempData["SaveOrUpdateBankHistory"] = message;
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        public ActionResult EditBankHistory(long id)
        {
            TeamMemberBankHistoryViewModel teamMemberBankHistoryViewModel = new TeamMemberBankHistoryViewModel();
            ViewBag.BankSelectList = new SelectList(new List<Bank>(), "Id", "Name");
            ViewBag.BankBranchSelectList = new SelectList(new List<BankBranch>(), "Id", "Name");
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                BankHistory bankHistory = _bankHistoryService.GetBankHistory(id);
                if (bankHistory == null)
                {
                    throw new InvalidDataException("No Bank Information Found");
                }

                AuthorizedTeamMemberPin(bankHistory.TeamMember.Pin);

                teamMemberBankHistoryViewModel.Id = bankHistory.Id;
                teamMemberBankHistoryViewModel.TeamMemberId = bankHistory.TeamMember.Id;
                teamMemberBankHistoryViewModel.TeamMemberPin = bankHistory.TeamMember.Pin;
                teamMemberBankHistoryViewModel.AccountTitle = bankHistory.Name;
                teamMemberBankHistoryViewModel.AccountNumber = bankHistory.AccountNumber;
                teamMemberBankHistoryViewModel.EffectiveDate = bankHistory.EffectiveDate.ToString("yyyy-MM-dd");

                List<Bank> bankList = _bankService.LoadBankList().ToList();
                ViewBag.BankSelectList = new SelectList(bankList, "Id", "Name", bankHistory.BankBranch.Bank.Id);

                List<BankBranch> bankBranchList = _bankBranchService.LoadBankBranchList(_commonHelper.ConvertIdToList(bankHistory.BankBranch.Bank.Id).ToArray()).ToList();
                ViewBag.BankBranchSelectList = new SelectList(bankBranchList, "Id", "Name", bankHistory.BankBranch.Id);

            }

            catch (DataNotFoundException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View(teamMemberBankHistoryViewModel);
        }

        [HttpPost]
        public ActionResult DeleteBankHistory(long id)
        {
            bool successState = false;
            string message = "";
            try
            {
                BankHistory bankHistory = _bankHistoryService.GetBankHistory(id);
                if (bankHistory == null)
                {
                    throw new InvalidDataException("Invalid Data Found!");
                }
                _bankHistoryService.DeleteBankHistory(bankHistory);
                message = "Member Bank history Delete successfully!";
                successState = true;
            }
            catch (DependencyException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        #endregion

        #region Mentor History
        public ActionResult AddMentorHistory(long memberId)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var member = _hrMemberService.LoadById(memberId);
            ViewBag.MemberId = member.Id;
            ViewBag.MemberPin = member.Pin;
            return View();
        }
        [HttpPost]
        public ActionResult AddMentorHistory(long memberId, int pin, string effectiveDate)
        {
            try
            {
                var member = _hrMemberService.LoadById(memberId);
                if (_hrMentorHistoryService.AddMentorHistory(member, pin, effectiveDate))
                {
                    TempData["AddEmploymentHistory"] = "Member Mentor History Add successfully";
                    return Json(new Response(true, "Member Mentor history add successfully!"));
                }
                else
                    return Json(new Response(false, "Member Mentor history add fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }


        public ActionResult EditMentorHistory(long id)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var mentorHistory = _hrMentorHistoryService.GetMentorHistory(id);
                ViewBag.MemberId = mentorHistory.TeamMember.Id;
                ViewBag.MemberPin = mentorHistory.TeamMember.Pin;
                ViewBag.MentorHistory = mentorHistory;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }
        [HttpPost]
        public ActionResult EditMentorHistory(long id, int pin, string effectiveDate)
        {
            try
            {
                //var mentorHistory = _hrMentorHistoryService.GetMentorHistory(id);
                if (_hrMentorHistoryService.UpdateMentorHistory(id, pin, effectiveDate))
                {
                    TempData["AddEmploymentHistory"] = "Member Mentor Edit successfully";
                    return Json(new Response(true, "Member Mentor history add successfully!"));
                }
                else
                    return Json(new Response(false, "Member Mentor Edit fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult DeleteMentorHistory(long id)
        {
            try
            {
                var mentorHistory = _hrMentorHistoryService.GetMentorHistory(id);
                if (_hrMentorHistoryService.DeleteMentorHistory(mentorHistory))
                {
                    //TempData["AddEmploymentHistory"] = "Member Employment Delete successfully";
                    return Json(new Response(true, "Member Mentor history Delete successfully!"));
                }
                else
                    return Json(new Response(false, "Member Mentor Delete fail!"));

            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Shift Weekend History
        public ActionResult AddShiftWeekendHistory(long memberId)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                var member = _hrMemberService.LoadById(memberId);
                Organization memberCurrentOrganization = _hrMemberService.GetTeamMemberOrganization(null, member.Id);
                long memberOrgId = memberCurrentOrganization.Id;
                List<Organization> organizationList = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                if (organizationList.Any())
                {
                    organizationList = organizationList.Where(x => x.Id == memberOrgId).ToList();
                }
                ViewBag.Organization = new SelectList(organizationList, "Id", "ShortName", memberOrgId);
                ViewBag.Shift = new SelectList(_shiftService.LoadShift(_userMenu, null, null, memberOrgId), "Id", "FormattedShiftTime");
                var weekDays = from WeekDay ed in Enum.GetValues(typeof(WeekDay))
                               select new { Id = (int)ed, Name = ed.ToString() };
                ViewBag.WeekDays = new SelectList(weekDays, "Id", "Name");
                ViewBag.MemberId = member.Id;
                ViewBag.MemberPin = member.Pin;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }
        [HttpPost]
        public ActionResult AddShiftWeekendHistory(long memberId, long organizationId, long shiftId, string weekend, string effectiveDate)
        {
            try
            {
                var member = _hrMemberService.LoadById(memberId);
                if (_hrShiftWeekendHistoryService.AddShiftWeekendHistory(member, organizationId, shiftId, weekend, effectiveDate))
                {
                    TempData["AddEmploymentHistory"] = "Member Shift Weekend History Add successfully";
                    return Json(new Response(true, "Member Shift Weekend history add successfully!"));
                }
                else
                    return Json(new Response(false, "Member Shift Weekend history add fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }


        public ActionResult EditShiftWeekendHistory(long id)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var shiftWeekendHistory = _hrShiftWeekendHistoryService.GetShiftWeekendHistory(id);
                var member = shiftWeekendHistory.TeamMember;
                List<Organization> organizationList = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                if (organizationList.Any())
                {
                    organizationList = organizationList.Where(x => x.Id == shiftWeekendHistory.Organization.Id).ToList();
                }
                ViewBag.Organization = new SelectList(organizationList, "Id", "ShortName", shiftWeekendHistory.Organization.Id);
                ViewBag.Shift = new SelectList(_shiftService.LoadShift(_userMenu, null, null, shiftWeekendHistory.Organization.Id), "Id", "FormattedShiftTime", shiftWeekendHistory.Shift.Id);
                var weekDays = from WeekDay ed in Enum.GetValues(typeof(WeekDay))
                               select new { Id = (int)ed, Name = ed.ToString() };
                ViewBag.WeekDays = new SelectList(weekDays, "Id", "Name", shiftWeekendHistory.Weekend);


                ViewBag.MemberId = member.Id;
                ViewBag.MemberPin = member.Pin;
                ViewBag.ShiftWeendHistory = shiftWeekendHistory;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }
        [HttpPost]
        public ActionResult EditShiftWeekendHistory(long id, long organizationId, long shiftId, string weekend, string effectiveDate)
        {
            try
            {
                if (_hrShiftWeekendHistoryService.UpdateShiftWeekendHistory(id, organizationId, shiftId, weekend, effectiveDate))
                {
                    TempData["AddEmploymentHistory"] = "Member Shift Weenkend Edit successfully";
                    return Json(new Response(true, "Member Shift Weenkend history add successfully!"));
                }
                else
                    return Json(new Response(false, "Member Shift Weenkend Edit fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult DeleteShiftWeekendHistory(long id)
        {
            try
            {
                var shiftWeekendHistory = _hrShiftWeekendHistoryService.GetShiftWeekendHistory(id);
                if (_hrShiftWeekendHistoryService.DeleteShiftWeekendHistory(shiftWeekendHistory))
                {
                    //TempData["AddEmploymentHistory"] = "Member Employment Delete successfully";
                    return Json(new Response(true, "Member Shift Weekend history Delete successfully!"));
                }
                else
                    return Json(new Response(false, "Member Shift Weekend Delete fail!"));

            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Edit Tax Information

        [HttpPost]
        public JsonResult SaveTaxInformation(TeamMemberTaxInformationViewModel taxInformationViewModel)
        {
            bool successState = false;
            string message = "";
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                TeamMember teamMember = _hrMemberService.LoadById(taxInformationViewModel.Id);
                if (teamMember == null)
                {
                    throw new InvalidDataException("No TeamMember Found!");
                }
                //var memberIdList = _hrMemberService.LoadHrAuthorizedTeamMemberId(_userMenu, DateTime.Now.Date, pinList: new List<int>() { teamMember.Pin });
                //if (memberIdList == null || memberIdList.Count == 0)
                //{
                //    throw new InvalidDataException("Member is not found/authorized.");
                //}

                teamMember.TaxesCircle = taxInformationViewModel.TaxesCircle;
                teamMember.TaxesZone = taxInformationViewModel.TaxesZone;
                teamMember.TinNo = taxInformationViewModel.TinNo;

                _hrMemberService.UpdateTaxInformation(_userMenu, teamMember);

                successState = true;
                message = "Tax Information Successfully Updated.";
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                throw;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }
        #endregion

        #endregion

        #region Edit Member Information
        public ActionResult EditMemberInfomation(string memberPin = "")
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.MemberHasInfo = null;
            try
            {
                if (TempData["EditEmploymentHistory"] != null)
                {
                    ViewBag.SuccessMessage = TempData["AddEmploymentHistory"];
                }
                if (TempData["updateAcademicInfo"] != null)
                {
                    ViewBag.SuccessMessage = TempData["updateAcademicInfo"];
                }
                if (TempData["UpdateTrainingInfo"] != null)
                {
                    ViewBag.SuccessMessage = TempData["UpdateTrainingInfo"];
                }
                if (TempData["UpdateProfessionalQInfo"] != null)
                {
                    ViewBag.SuccessMessage = TempData["UpdateProfessionalQInfo"];
                }
                if (TempData["UpdateJobExperienceInfo"] != null)
                {
                    ViewBag.SuccessMessage = TempData["UpdateJobExperienceInfo"];
                }
                if (TempData["UpdateSpecialization"] != null)
                {
                    ViewBag.SuccessMessage = TempData["UpdateSpecialization"];
                }
                if (TempData["UpdateNomineeInfo"] != null)
                {
                    ViewBag.SuccessMessage = TempData["UpdateNomineeInfo"];
                }

                ViewBag.MemberPin = memberPin;
                if (memberPin != "")
                {
                    var memberIdList = _hrMemberService.LoadHrAuthorizedTeamMemberId(_userMenu, DateTime.Now.Date, pinList: new List<int>() { Convert.ToInt32(memberPin) });
                    if (memberIdList == null || memberIdList.Count == 0)
                    {
                        throw new DataNotFoundException("Member is not found/authorized.");
                    }
                    int memPin = Convert.ToInt32(memberPin);
                    ViewBag.MemberHasInfo = true;
                    TeamMember member = _hrMemberService.GetByPin(memPin);
                    ViewBag.TeamMember = member;

                    #region Member Basic Inforamtion
                    object campus = new Campus();
                    if (member == null)
                        throw new InvalidDataException("There is no information found with this PIN number. Plase try again!");
                    string department, designation, joindate, todate, officeShift;
                    int empStatus, weekend, mentorPin;
                    _hrMemberService.GetCurrentInfos(member.Id, out campus, out department, out designation, out empStatus, out joindate, out todate, out officeShift, out weekend, out mentorPin);
                    var campusValue = (Campus)campus;
                    ViewBag.Organization = (campusValue != null) ? campusValue.Branch.Organization.ShortName : "N/A";
                    ViewBag.Branch = (campusValue != null) ? campusValue.Branch.Name : "N/A";
                    ViewBag.Campus = (campusValue != null) ? campusValue.Name : "N/A";
                    ViewBag.department = department;
                    ViewBag.empStatus = (empStatus != null) ? ((MemberEmploymentStatus)empStatus).ToString() : "N/A";
                    ViewBag.joindate = joindate;
                    ViewBag.todate = todate;
                    ViewBag.officeShift = officeShift;
                    ViewBag.weekend = ((WeekDay)weekend).ToString();
                    TeamMember mentorInfo = _hrMemberService.GetByPin(mentorPin);
                    ViewBag.mentorPin = (mentorInfo != null) ? mentorPin.ToString() : "N/A";
                    ViewBag.mentorName = (mentorInfo != null) ? mentorInfo.Name : "N/A";
                    ViewBag.designation = designation;
                    //ViewBag.Gender = ((Gender)Convert.ToInt32(member.Gender)).ToString();
                    //ViewBag.MaritalStatus = ((MaritalType)Convert.ToInt32(member.MaritalInfo.OrderByDescending(x => x.Id).FirstOrDefault().MaritalStatus)).ToString();
                    if (member.MemberOfficialDetails.Any())
                    {
                        ViewBag.CardNumber = member.MemberOfficialDetails[0].CardNo;
                        ViewBag.OfficialContact = member.MemberOfficialDetails[0].OfficialContact;
                        ViewBag.OfficialEmail = member.MemberOfficialDetails[0].OfficialEmail;
                    }
                    else
                    {
                        ViewBag.CardNumber = "";
                        ViewBag.OfficialContact = "";
                        ViewBag.OfficialEmail = "";
                    }
                    #endregion Member Basic Information

                    #region personal information

                    ViewBag.GenderList = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined }), "Key", "Value", member.Gender);
                    ViewBag.ReligionList = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(new List<int> { (int)Religion.Other }), "Key", "Value", member.Religion);
                    ViewBag.BloodGroupList = new SelectList(_commonHelper.LoadEmumToDictionary<BloodGroup>(), "Key", "Value", member.BloodGroup);

                    var teamMemberImage = _teamMemberImagesMediaService.GetTeamMemberImageMediaByRefId(member.Id);
                    ViewBag.TeamMemberImage = teamMemberImage;

                    Postoffice presentPostOffice = member.PresentPostOffice;
                    Postoffice permanentPostOffice = member.PermanentPostOffice;
                    Thana presentThana = member.PresentThana;
                    Thana permanentThana = member.PermanentThana;
                    var divisionList = _addressService.LoadDivision();

                    long? presentDivisionSelectId = null;
                    long? presentDistrictSelectId = null;
                    long? presentThanaSelectId = null;
                    long? presentPostofficeSelectId = null;
                    long? permanentDivisionSelectId = null;
                    long? permanentDistrictSelectId = null;
                    long? permanentThanaSelectId = null;
                    long? permanentPostofficeSelectId = null;
                    var presentDistrictList = new List<District>();
                    var presentThanaList = new List<Thana>();
                    var presentPostofficeList = new List<Postoffice>();
                    var permanentDistrictList = new List<District>();
                    var permanentThanaList = new List<Thana>();
                    var permanentPostofficeList = new List<Postoffice>();

                    if (presentPostOffice != null)
                    {
                        presentPostofficeSelectId = presentPostOffice.Id;
                        if (presentPostOffice.District != null)
                        {
                            presentDistrictSelectId = presentPostOffice.District.Id;
                            if (presentPostOffice.District != null && presentPostOffice.District.Division != null)
                                presentDivisionSelectId = presentPostOffice.District.Division.Id;
                        }
                    }
                    if (presentThana != null)
                    {
                        presentThanaSelectId = presentThana.Id;
                    }
                    if (presentPostOffice != null && presentThana != null)
                    {
                        presentDistrictList = _addressService.LoadDistrict((presentDivisionSelectId != null) ? _commonHelper.ConvertIdToList((long)presentDivisionSelectId) : null).ToList();
                        presentThanaList = _addressService.LoadThana((presentDivisionSelectId != null) ? _commonHelper.ConvertIdToList((long)presentDivisionSelectId) : null, (presentDistrictSelectId != null) ? _commonHelper.ConvertIdToList((long)presentDistrictSelectId) : null).ToList();
                        presentPostofficeList = _addressService.LoadPostOffice((presentDivisionSelectId != null) ? _commonHelper.ConvertIdToList((long)presentDivisionSelectId) : null, (presentDistrictSelectId != null) ? _commonHelper.ConvertIdToList((long)presentDistrictSelectId) : null).ToList();
                    }


                    if (permanentPostOffice != null)
                    {
                        permanentPostofficeSelectId = permanentPostOffice.Id;
                        if (permanentPostOffice.District != null)
                        {
                            permanentDistrictSelectId = permanentPostOffice.District.Id;
                            if (permanentPostOffice.District != null && permanentPostOffice.District.Division != null)
                                permanentDivisionSelectId = permanentPostOffice.District.Division.Id;
                        }
                    }
                    if (permanentThana != null)
                    {
                        permanentThanaSelectId = permanentThana.Id;
                    }
                    if (permanentPostOffice != null && permanentThana != null)
                    {
                        permanentDistrictList = _addressService.LoadDistrict((permanentDivisionSelectId != null) ? _commonHelper.ConvertIdToList((long)permanentDivisionSelectId) : null).ToList();
                        permanentThanaList = _addressService.LoadThana((permanentDivisionSelectId != null) ? _commonHelper.ConvertIdToList((long)permanentDivisionSelectId) : null, (permanentDistrictSelectId != null) ? _commonHelper.ConvertIdToList((long)permanentDistrictSelectId) : null).ToList();
                        permanentPostofficeList = _addressService.LoadPostOffice((permanentDivisionSelectId != null) ? _commonHelper.ConvertIdToList((long)permanentDivisionSelectId) : null, (permanentDistrictSelectId != null) ? _commonHelper.ConvertIdToList((long)permanentDistrictSelectId) : null).ToList();
                    }
                    ViewBag.PresentDivisionList = new SelectList(divisionList.ToList(), "Id", "Name", presentDivisionSelectId);
                    ViewBag.PresentDistrictList = new SelectList(presentDistrictList, "Id", "Name", presentDistrictSelectId);
                    ViewBag.PresentThanaList = new SelectList(presentThanaList, "Id", "Name", presentThanaSelectId);
                    ViewBag.PresentPostofficeList = new SelectList(presentPostofficeList, "Id", "DisplayName", presentPostofficeSelectId);

                    ViewBag.PermanentDivisionList = new SelectList(divisionList.ToList(), "Id", "Name", permanentDivisionSelectId);
                    ViewBag.PermanentDistrictList = new SelectList(permanentDistrictList, "Id", "Name", permanentDistrictSelectId);
                    ViewBag.PermanentThanaList = new SelectList(permanentThanaList, "Id", "Name", permanentThanaSelectId);
                    ViewBag.PermanentPostofficeList = new SelectList(permanentPostofficeList, "Id", "DisplayName", permanentPostofficeSelectId);

                    #endregion

                    #region Parental Information

                    #endregion

                    #region Material Information

                    var memberMaritalInfo = _maritalInfoService.GetTeamMemberMaritalInfo(member.Id);
                    ViewBag.MaritalInfo = memberMaritalInfo;
                    var maritalStatus = from MaritalType ed in Enum.GetValues(typeof(MaritalType))
                                        select new { Id = (int)ed, Name = ed.ToString() };
                    ViewBag.MaritalStatusList = new SelectList(maritalStatus, "Id", "Name", (memberMaritalInfo != null) ? memberMaritalInfo.MaritalStatus : 0);
                    #endregion

                    #region Children Information

                    var childrenInfo = _childrenInfoService.GetTeamMemberChildrenInfo(member.Id);
                    ViewBag.ChildrenInfo = childrenInfo;
                    #endregion

                    #region Nominee Information

                    var nomineeInfo = _nomineeInfoService.GetTeamMemberNomineeInfo(member.Id);
                    ViewBag.NomineeInfo = nomineeInfo;
                    if (nomineeInfo != null)
                    {
                        var teamMemberNomineeImage = _teamMemberNomineeImagesMediaService.GetTeamMemberNomineeImageMediaByRefId(member.Id);
                        ViewBag.TeamMemberNomineeImage = teamMemberNomineeImage;
                    }
                    else
                    {
                        ViewBag.TeamMemberNomineeImage = null;
                    }
                    //Team Member Image

                    #endregion

                    #region Academic Information

                    var academicInfo = _academicInfoService.LoadMemberAccademicInfo(member.Id);
                    ViewBagForAcademicInfoEdit();
                    ViewBag.AcademicInfo = academicInfo;
                    #endregion

                    #region Training Information
                    IList<TrainingInfo> trainingInfo = _trainingInfoService.LoadMemberTraningInfo(member.Id);
                    ViewBagForTrainingInfoEdit(trainingInfo);
                    #endregion

                    #region ProfessionalQualifications
                    ViewData["ProfessionalInformation"] = _trainingInfoService.LoadMemberProfessionalQualificationsInfo(member.Id);


                    #endregion

                    #region Job Experience
                    IList<JobExperience> jobExpList = _hrJobExpService.LoadMemberJobExperienceInfo(member.Id);
                    ViewBagForTmJobExpInfo(jobExpList);
                    #endregion

                    #region Specialization

                    ViewBag.SpecializationFields = !string.IsNullOrEmpty(member.Specialization) ? member.Specialization : "";
                    ViewBag.ExtraSpecialization = !string.IsNullOrEmpty(member.ExtracurricularActivities) ? member.ExtracurricularActivities : "";

                    #endregion
                }
            }
            catch (DataNotFoundException ex)
            {
                ViewBag.ErrorMEssage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.MemberHasInfo = false;
                _logger.Error(ex);
            }

            return View();
        }

        #region Media Image Operations
        [HttpPost]
        public ActionResult TeamMemberImageUpdate(string memberPin, HttpPostedFileBase file)
        {
            try
            {
                //string message = "";
                TeamMemberMediaImage teamMemberMediaImage = null;
                TeamMember teamMember = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                if (teamMember != null && file != null && file.ContentLength > 0)
                {
                    teamMemberMediaImage = _teamMemberImagesMediaService.FileUpload(teamMember, file, true);
                    if (teamMemberMediaImage != null)
                    {
                        string imageBase64 = Convert.ToBase64String(teamMemberMediaImage.Images);
                        string imageSrc = string.Format("data:image/gif;base64,{0}", imageBase64);
                        return Json(new { image = imageSrc, IsSuccess = true, Message = "Image successfully saved" });
                    }

                        //return Json(new Response(true, "Image successfully saved!"));
                    else
                        return Json(new Response(false, "Image saved faild!"));
                }
                return Json(new Response(false, "Invalid file or Team member"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult TeamMemberNomineeImageUpdate(string memberPin, HttpPostedFileBase file)
        {
            try
            {
                TeamMemberNomineeMediaImage teamMemberNomineeMediaImage = null;
                TeamMember teamMember = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                if (teamMember != null && file != null && file.ContentLength > 0)
                {
                    teamMemberNomineeMediaImage = _teamMemberImagesMediaService.NomineeImageFileUpload(teamMember, file, true);
                    if (teamMemberNomineeMediaImage != null)
                    {
                        string imageBase64 = Convert.ToBase64String(teamMemberNomineeMediaImage.Images);
                        string imageSrc = string.Format("data:image/gif;base64,{0}", imageBase64);
                        return Json(new { image = imageSrc, IsSuccess = true, Message = "Nominee Image Successfully Saved" });
                    }
                    else
                        return Json(new Response(false, "Nominee Image Saved Faild!"));
                }
                return Json(new Response(false, "Invalid file or Team member"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult TeamMemberImageDelete(string memberPin)
        {
            try
            {
                //string message = "";
                bool deleteImage = false;
                TeamMember teamMember = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                if (teamMember != null)
                {
                    deleteImage = _teamMemberImagesMediaService.FileDelete(teamMember);
                    if (deleteImage)
                    {
                        string imageSrc = "<img width=\"200\" height=\"200\" alt=\"NO IMAGE\" style=\"padding: 2px; padding-left: 2px; padding-right: 2px; border-color: dodgerblue\" src=\"data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAIAAACx0UUtAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAB1ZSURBVHhe7Z1fqF3FvcdbH8yLbTmkT/ZJH6x9EaQEBKEI98mHvvlQ+qBw3wqFcvJHCETQYCJ6XyxYpNW3hIpBuUk5f2LOVWMw2jSJBymHiybeGJNWRUOw+CBS8H7W/s1ea/asWbP3mj179pxzfl++JGfNzJr5rt/vu2etWXvtvb/3nUJRNjwe/fbbb7/55pt///vfZtuCVJkNCzSmnFqzbSFQNXYgb1XEQFLVdyBv1cVr/zp24drzb1763Z8vPPjs6V1Prt11YPWO/csLe1d+uKfibbuXdyyehJRQTi1taEl79mJfejB9DdFXA6CE8sDBBqp6DQSkymxYiNMwdqB2letRGn399dfeLij3VlEiVWbbAqNSTp9m24IM5D2MEjQMOqt6w1Lw0NIGPsNzty6e/N7u/5mG9LBz3wq90eeZS1/QOTKyHWygKpuGvkkf8ejNmze//PJLbxdUAa9WdqHKbFtgVKq8h0EhVd7DoCuvBgYaSPBooHFaDWsbn0KmPZzE1OiYLC13LK4+8MzrjCUvBqNgGAdvwAdh8MRh7MF6q2SgXgGnhF2oMtsW0iYdMFbjUdr985//bHdBo88HaGulU3ah93YVgqjyHoYM5D0MuuqrgcaiwWxbiNCASw4ef+/nB1cdJ+UhVwWY9dzHqLvJwbZzRkk4Dr0OFkQEvE662baQPOkU8q/xKBs0DWj1xouuqTLbFpJrFQ3teNGYXaaPF9bktPvrP57latLxTX7esnjq/sMnj567ak+rYKxBvXGQg/XGYWzA55t02kt55VEGZrstSLTSRUBrO17JtVbR6k4MVWbbwuQa8AHrmHsef80xSgnk8leuAdDJ4cvBdsUhrUGpCiTdbFtInnSq2EUOtvIojdpHXmttV43VGohXX62MAro0eAeaMF7knrkKHzjOKI0Le1dw6qvnr3Cw7TiMNWhEwKmKSPrYgDsIa7APtvJoW5C0A/N9MYmGtrxAYibRIHNn+e60ubBnqZ5Ta4w1qDcOEQGffkZw0EtDs2aqUbfbegY9+9ENViQsTRwHbBbedWB1beNTceqMDFpg0l2PSju6CGht+0a0BuKVyqCiwTvQ2HiR2t/9+YKT9c1I1nYcC/EJBNwbh3DAqeoKOLVm20Ieg4IRjxauNdqgXMxx6blz32zvdObkjsXV9qkfRAecqoQBj9PgTToY8ai0Q5nZHqIEg9KYXSLidWL9EyYeJ8dbgzKhmkMdBtwbh7EBLzPplIDGo/ROu5INGqHhpXcv//TAZlob9eXtjy5xhY1TJeDeOGxeg9Kef41H6ZcB2u3GamVss20hrVbR4B0oEC/WFgePv3fL4iknqVuSh5Y2uJ6JCDhVEUnPYFA00KHsUnmUbbouUyuN2SXCoL858jcnkVubj/zpTPvyNM4cEQFPnnTK694qj1LUpZWmZttCTq3sEmHQh557w0nhduCDz562bTo24KTYFA1RTtLtgUbWTDVqre0usmmNi9eZS1/cf3jax+c2L+95/DWxaTjgVJVsUEeDx6MlaBUN7Gi2LQQ0MINu7RXSJNy5b4U4BAJOVVfAqTXbFjIbtJ1016MlawVq0EnIYv/sRzdMXIYIBLzwpI94dPMalFP8vB76LJN3HVi1r03rgJNiUzRE4UmnceNRNkRruwvR6u0irVaJV1+DMoNu52vQLu56ck1sKgEnel0Bp9ZsWyjEoMB4lL9o5G2XTWu0QbfnKn4SykqfaBO9VAHPZlCqGIVdKo/yHxuM3e6iBK0BDRh0azwmMjs+8qczJ9Y/wY4mZEMUPoMyBGOJ7MqjYthB1Qgya6XKbFsIazjy9odOSpRttp8+KcegXRoorweqPNp+kQHR6vV4Wq0Sr64XE1Vd8Xrp3cs7pv4k8TbhuY+bGJY/gzoamjWTjTK11qCQ81eZH0Iqkzv3rchUGj0jUDWvpHs8WqxWAYXMoHoZ2pfyIB9RJRcmlBZKTrrrUWnn7SKt1mnOOCyVnAQoJ+HB4+9FzAhUpUq6GLRv0kc8mk3rNGccLq3uOqC362N42+5lZ/EEsiVdDNo36fTTeFS6KFaraGDfQ0sbTuiVk9N5dL9wgzKXNR5lg/2zaaXKbFuYJF7Ed5s8tjw7nrn0hUR1koCbbQvZDEoVMB7lP9mWOhtptU65qMSgDz572om4si/l+b1JAm62LWQzKIUAw/B35VF6aY8KaFSC1loDkXXCrYzj829eIkdjA+4gZ9LprR6o8qi41UEhWmsNGHTzfndDaVzYs7S28akE2UZpSRc0ayYbs9BKldm2MLlWvd+UlkylEtgaBSZd4PFoWq1TXoPKpk6iyVm/8yRIm3Qx6JRJr+F6tEytOonOgvVUWmbSa4x4tEytvNx1OT8LygK/zKTXYIjGo9JFKq1yiqfKbFvoq5U4OsFVpuKJ9U8SJj25Qemw8SgbdJ1Ka5Jr0Br6+MjseP/hk+3MgrknHVAoAiqP0nu97SBCK40TamUSvf3RJSeyylS8ZfGUvXISzD3pgCogA1UeZQBsOqgaQbRWqsy2hTit+qT9rOnchCrBoHRlD1R5tD0qiNCafLanSj9PN2s+8MzrJtzFGJQqe9Js1kw2StDKKFzRL+zdOl9sWybr030hSafKOat7PFqCVrpioGMXrjkBVc6CnO4lF3NPuleD69EIg8opniqzbWEarQykj4rmIat7Yh5h0ORJ966LRjwabVB2NNsW4rQySq1VP1WXhwt7qi+DlvjXGGvQGSXdAaM3HpUuIl5MybWKBi6S9KPJ2eh8h9m8km6DoSnkX+NRNhjV2y6bVrqytep79Dlp34GaY9JrMDR7SXnlUeZY9m/PtAGtyU/xotXWoBejOfngs6cl7PkN6jUeVfUulUcZuy0ooDW5QRmlrVWfI8nJuw6sEvNA0pMb1Jt0wNDOQJVH24JyaqUrqmRWr8HF6Ob6Pc/NTrlLOt+kA6/xmjVTjbEGpcpsW5hGa/vFRLycICpnzRPrn8w36V3Gcz0aMGjyU3xAK/FyIqicNVk2tZM+oxl0coOCEY8G2iXXyihUdc32B4+/50RQOWuySDU5GGJGSe9lUNB4NKdB5cXUZVBqdVGfn7/+41mThgEyJ91rPEpA41F697aTUzxVZtvCNFoDLyb+3m6/UlcCH3ruDUkEEIPmTLrXeHTVeJQNBugyaMIXE2om0bpVfym5ZN7z+GuSi+Qz6IRJt4EGOpRJt/Iof9G1t11arfJikoFttLXqzdH8vGP/MpFPnnTSOmHSa9CY8rq3yqMYvCiDAn2aJD937lsh8rNIet8Z1BmouR61kfwU31erejQ/F/aukMESDOpo8Hg0+QyKmr5a1aP5ecviKW/6siW9y3iuR5MbVF5MdGu2hwhopUR/+TM/8ahJgIVsSQ8Yb8Sj0o4uzLaFabT2fTGxl3o0P3csnjQ5GCJn0tnFa1AaNx6l0652mbXSm57r81PWTDVKMCj9NB7lLxp528VpRQ1VDG+2h5hQq35LXn7aHp0m6akMShWjsEvlUf5jwysoTitDUhVtUKAfq89PeYQUzCvpNsSQYvfKo2LYQdUIptE65YtJ32fKT3mfaY5Jr8EQ7FLbvfJou2sQp5Wuk2jV9+vzk3lBcjGvpAvEePZAzZrJRpxB5cVU279GhFZ97ik/ub4iR3NMOvAaz+PRuRsUvHr+ihNB5azJucub2bFJTzuDto3nenQagybUqh7Nz5fevWwSYGHuBgUjHo0zKGqSaz1z6Ytb9Pfs8lK+mcxG5qR7jUc/jUeli74GlRcT+5rtIabXqj9cm5M7Fk86Hp1L0h3wGmg8ygb7xxl0Ri8mfYQ0J+XGU424pFOe0KBUAeNR/pNtqbMxL4OC59+85MRROTvaH2YKJJ20zjTpNSgE0lvlUXoBg6oR0Cistb1XQq1ckjpxVM6OR89dlbDPN+kC2tNb/Uow86hs2AhopYsMWrk8um23Pv2UifLdjnNPOhAN9kDNmsnGWK0ZZnuq9Am9PLxj/zIzQglJl4EcDR6PBrSiJptWetNvgshDuXtfQtK9GlyPjn0x2ZOwILlW0cC+Zz+64URTOQsyF5C+EpLu1TDi0XIMKho4Aem3582aOxZXXz1/JZD0DDNowHgM0XhUupivVhnI1qA/xDhrhn+OcY6neECHjUfZoKm3HWroon0YybV6X0xMpU5MlWnp/M6dQJI+3xmUQnFd5VGk1NsOxOPzMijAo7q6nx1/uGeFCJtYDzH3pNM/XVErA1UeZYD2KwaI1vm+mOhKV/ezIyt6E+ghSki6M1Dl0faogEZ5tEpQ2lplIKp4oeuP4MyIziQqAS8h6fZAzZrJhnTB8GZ7iORawy8mqiRe+tGRWfCBZ163PSoBn2PSZaA66TU8Hi3NoODcxzed+Cqn57EL1yS8YO5J7zIocD0qWuc424O2Vl7u+qheWt7z+Gv1JDr3pAcMCkY8mk1rxItJH4NKy3oS7Qp48qRHGJQq0HiUfmnK8GZ7COkC8IcpGiKtQQGjeLVS8ur5K1w/OYFWxnHXk2syiYpvykw6QzMW/xqPsjOjFquVcmoJqxNrZRzlF2zFoN6AD3I+56Szlxiy8igt2L8tKLlWCUpbqwzUpZVy0YBH9a3R6cmVPZHMZtCIpFPCXvUulUcZuy0oudauF1NYK+W2BoK7c9+KE3Tl5JTfXMxm0Oik2wNVHm0LyqYVBLR6NRw9d9WJu3JyHlrakFyQRxPQIbIlXQaaPOnNmqlGNq2AUbxaGZpyr4ZzH9+8/7C+7RTDuw6srm182hXwQc6TJZ32VCVJuuvRbFplIK9WSrxagWgg0PruaARfevcyUe01I5SQ9BGPShfedtlmUEom0XDswjUnAcowWW6+ev4KMZRg1sicdNAr6aDxaGatVEUbFHDhr99ROjl/fnD1xPon7YBnS7oM1DfplADjUf6id2+7OK2076uVKson14BN9ft2JuFtu5c5y2cwaNqk05iuKDceZYNRU2mlsK9WSrq0BjQMLkzVpmN45O0PyzRoOOmMwr/8XXmUv+g6ldYug4KA1ipaPePFQOzCDOGkRGnz4PH35PsdbIhv+gY8Z9Ipr3urPNreH2TTikTKu7R2aZCB+Jczvn4zVBcf+dMZ4mNCNkQJBh2bdHugZs1kI04r7alqa5WgUOV9MVHeN15OUEiDfjd5mw8990YGg2ZIusejcVod39gIaK2iFWVQRwPJ0Lfybd5/2P0+UZDcoF1JT2hQ4Hp0MxpUwPpJf9VJWLJBIzSMeDROK+29WkGXViRS3ldrwKCigcWBzqZ5DJot6TRuPEqnXe0itEpQqPK+mCjvq1UM6g2KaGBf/iY92/nadMtcgwrYi/bGo/xFI2+7gNaAbwJaq2hFGTQQLzGogCRtz5V+Cat4EJF070BUMQq7VB7lPza87aK1UjUXgwpI1ZG3P9xWv0wy3/ugMhBV3qRTHqGBsaS3yqNi2EHVCAJaxRy9tFIVobUrKKDLoKIBkLPt8BvjC3uWeEFmMGjOpLNLndnKo+2uQUBrl28CWimJ0+odCIw1qAzEhLq1vz+CFVLJD4tQ3ldDe6BmzWQjQisIaK2iFaXVq2GsQW0N2PTouas/3LPVPmHClczv/nxhvgYFjEJV2qQ7A3k8mlYrEimP0+rVEDZol4aX3r28lZ7e3/XkWrYHljMnvT2Q69GAVjFHuwt0dGmlhPJUWsFYg7YHEg3syIR67MK1hb2be0LdsXiS08Laxqccr9cclINeAS886SMeDWgNdBHQWkUrSmsgXl0GBYF4me3BqZ9T5K2b86Mmv/7jWfSPjUOvgEckXQaiKiLpvQYC9NN4NK6LaK1zMWgN5qHNdep/4JnXWblvdoMGku51F3vRlfEovbN/wBy9DEq/lMdpDWjoSgxV7YECBpXEYFOyXv4HTnDn2Y9u4E6USxy8AedIQVfAT/39+v9+/jU0pQMEDNqV9DrgaZPudReFgN4qj/KfbEudjQittTlSaRUNtUEJ9N6XL7Kq/eXvTxN670CiwTsQQ1BVJ4bcw98c+VtpHzTlakTO7OJOEDAHVd44SMDf+uAzIkafvz16vrapEwcbJSSdKiC9VR6lFzCoGkGgi4DWKlpRWvnXbFuwDUp8Xzxz2V70/OyxZWduAHW8zLaFQGLOXPqC69QS7vn/9MDyoaWN2poCOw42xDeBgGNQXsx15zsWV59e2bh4tTPghSSd3uqBzDwqGzYitNIP5Wm1UsW+GPH961/d+4RnqvvxvhVqa6fGGbQ+WJzBiRWz5v803+2PLj303BvHLlxz3AkkDt6Ac6RdAaec88zdj3kO5CePLvFqb7+8I5JeBzwi6d6B6qSbbe/9USDt2l1IUKhKa9CwVkLJScqJssNXLl6j2ZQGNdsDyEkWs7K0mt1lAD1zucmsycWx98PvQOLQ16BL61eP/fVS+AOJzK+2TSOSnsGgwOPRri5mpNXrG9HK3Hl8/R8/muwtoodfeIfEeAfqa1ABXckRiV9xEheIzK/TWBbT3LF/mX7ojasLmTJFnldDV8AlFyLPFA0hBv3PF844Q3t56+KqTKiZk94VcKocgwLXo4EuAlqraCU1KL1hUPtCahJyCjt35YZzChMHeAeKiBc4sf7Jkbc/PHru6vNvXhLjPvjsaaZD7MulJOSiFu56co1ysePB4+/Bl969LKa0EaEhYFBy8d8X/o/LdCcyYe46uMpeXQOhIW3S+wZ8xKOBLrq0IpHyOK38a7Yt0J5pgHP3hNNnm6wJapumNajkzHuwgL28A9EPu8Rp8Aa8SwNHffgv69/fveYEZBKyV/sKlVG6NFDu1RCddKq8BmWIxqOSzna8JChUeV9MlMdp7UoMBv3VH950ItiXd+5fZukat3rtipfEoe/BSlT7amAUqiY3B97iBNJ3+mzzP/7rdbGpHKxXwyySTpU34BTyr/EoGzTtFS9KqIrT6s0Zi1Au8zlfO4GLJl5/64PPTO8W4uIVcbDTGHRyDbhKbn8mIacv1gBy4zki6YGD9VYFAk5wpLzyKAPX2w5QQxcZDIqZOE858ZqeRNw+9QPRUKxBRUM74F4NHBcXRT+ewRdbs+SygyaYJul9A04VO8pAlUdp1A4KQE1XvCiP0+rNmXOfOTk59TM3EHTxTd94DfJSlkE5FoLmvWGcivV5X8DQaOsbh7EH2xVwauuBKo+2R5V23nhRklYrpxU85ARoFmQUpmouUs3AFsLx6nuwcQZlFKrGGhTf8Hq771COj2oxQxMuRkye9HDAqbIHatZMNqSd16BVtKK0tnPGwY+9z5ycdz9m7ggaESXNoAEN/I1mzuwznTu9JFwsZPvGYZqDdYzn8SiN6ML7gqac2gitXoPO4gJ0QnKdyjoDDZwxOaJAvLoOliqzbWEag3oDziicZ9DJVfUsrjsnpPfydBYG9cZhxKOBdpRQHshZL60c8Ni3N/Nw10F3WgUSh74HG2dQRqGqHXDA7IW2mV6pT86HX3jHjtKMDNoOOBjxaFe8KKkylm4Gnf4OaFoyrZIDedMfhRxO34NNaFA0cMVJiBb2JLsNl4T1KmoWBvUGnBLQeJTeaZfBoByqc/DlkItjZlYuA96//pXko0Zyg0rOJKqMBWXWjH6DLQO5GpZ3RpIbtG08qmjfeJR+6cXbjq4TGhQHOIddLLn+wzFyexVy5vW6MM6gVMmFJr5kCp/jtWZf/uTRpcA7I944hA1KVdtdlNCV7FJ5FGsSx3Y7ytk/zqDtxGwug7ZJbjgDcBnNJcG5KzfkpgxxIAjt6AN52UvoaEl79mJfJulfPHXyzv3Lm/erfpjpOSI5TEFX0kGEQeXUXfdWeZT9u9qBCIO2X0wcUp6boDl56+LqD3Yv4927H1vlJAjvO3QKH/Pvzx5bhhwyXNi7kvn+WgZyULVNu5IOwgb1uktmRttdlUczGHT6xx2UpZHrEzILSHrEDAqwmSkaQgxKldkeoFkz1aALaZfKoJv6FK8MkNMIV9UJT/FiPLM9hOvRul2EQdta1aBbnlzq1Cf9GmNn0C6Det014lHadXURN4OWfJtJmYpcjts2HWtQbGaKhuiaQQWNRwMejzPohB+pUW4B1rf3wwalqu2ugEFpDIxH+QufxRnUe4pP+NStclPw4RfeeeuDz+IM6nUXVVjLeJT/2MBwEQb1zqBzfFhEOUdy5nz/+lfGB0OIQb3TX8CgGA9rid0rj/KXGHZQ2yDCoODYX/WXEbcv62ceBLVBsaMpGiJwiseNlNfn58qj7f1BnEFP/f36D3brrdBtzfopcjEoVumaQbsM6rirWTPZGGvQ2uM2uBzZem8mKftS3ikda1Cvu9oGBR6Pxs2gGLS0J+6U8+J9h07Jw+N9T/Fed7kejT7F6zpJabPrk6V9DQpGPBp3iqe9rpOUbR5f/4exyABxBuVSofFotEGX1q/qZaiyzR2LzftPfa9BBezVeJQN9ve2E4N6q8SgD7/wjiNOqRTK+09xMyiTJrXGo/zHBpA6G2GDMuorF685spRKm0+vbMhHn41vLGC5LndRRTnm5u/KoxgWDKpGIAb1eheDUvX+9a+23tO7yuTEo8Y3FgIGpRBP156sPCpudSAG9XZRG1TP8spJeO8TJ501ftigVNkzY7NmsjHWoNhaz/LKyTnNl8J6PDrWoEzCjFfyR2yVBVI82tegwPVoVztgG1TP8sq+vO/QKa4P+xoUjHhU2nm7qA3K34zkDK9UTsLDf1nva9CRe/iBSdg2KJOo9yd/lMqx/MFuz2++BQyK5RqPsoERw6d42XzxzGVnYKVyctq/BwkCBqVQXFd5NHAP3zEovetSSTkl6wdMAwalCuBM/q48igVrF9poG7SQL2RUbmr+cvD7egGDYrzPref6zDwqGzYcgwL6dQZTKuO4tH41YFDHeM2ayYbXoIV8WatyC/DO/cvtT+eBtvGAx6Pednq/SZmWztOlwGs84HrU204nUWVyMpXiK+OwboOCEY92tTt35YYzgFI5PV88c1kMFjDoyD18WWe122H2XzyV+/dWlNuB8kVRXQbFnRQ2HmWDpl4j62eVlLMjU6n9qGgNrMmqX8orj347+GKd9lOktDv19+t6JaqcHX/2mOfdUYzHjFnfmao8KjOqbNegBIPjUadTpTItnZtQYjw8Wnuy8miXQWmnbywpZ01528k2HrA92ayZatTt2FPfnVdmoHjUa1DgelTasc7iD33ESZmHe1++iPe8BgUjHrUNirX1OVFlHnK6ZuXjNShoPCoGBbLAf+uDz5yOlMrZ8fBf1tsGpQQ0Hr05+nuh+oklZU7ed+iUcxMKd8odJ+PRr0e/a1xXS8r8tD3KXIkh3Xv4tUGBfnZemZ+ycgJYk1N66B4+dtbfVVLmp/y4I25k1c4kauzYvvcEaOfsrFTmoffH8T0efXplw9lTqczDX/3hTXsGFXg8qr+RrJwXF/Z4fn10xKMsnpbWr35/95qzp1KZjW998Jmx4wDNvSfABosp/ekF5XxZr+4Bnmw8yl9cqHIpoCt65XxZf86JBb7cEq08yn9yD586vXWvnDvfv/4VBsWQ8q5n5VExLH/oe/TKEsgFp/scfv0evT7RrCyBv3jqJPOmeBI0ayZO9Pc+oZ//VM6fzldAjnjUaapUzot+j+rFqLIcPr2yYXxpe1QvRpXl8Je/Py22bO6Pgl0H9ZMhylL4o8GP4LOabzzK9i2Lp5x2SuUcKXdJm3v4ejGqLI1ckuJM/Fl5lBlVn8dTlkYWSNUpvl4z6SfslKXx3idOijmNR/Wj9MrSuLB3RcxZeZQFkz5KoiyN39+9Jl9XVnlUv6ZZWSZfuXjNeFQ/qawsk/Jl5JVH9bvHlGWSpbzxqC7qlWVy18FV49H7Duk7TMoSeef+ZeNR/nLqlMoSKO/aVx7VG0/KYln9wKI+TaIsmdWN0YtXbzqlSmU5rDyqN/CVJbO6MapP5SlL5t6XL35Pb+ArS2b1EabKp60KpbIQVm8wqUeVJbP6uVp9I1RZMqsvyVOPKkumelRZOqtPiOjP0ytLZuVR/VJcZcmsHnj67dHzP3l0ib+UytKIM6vz/HcKRcn47rv/B6GrUu3h/YF/AAAAAElFTkSuQmCC\">";
                        //string imageBase64 = Convert.ToBase64String(teamMemberMediaImage.Images);
                        //string imageSrc = string.Format("data:image/gif;base64,{0}", imageBase64);
                        return Json(new { image = imageSrc, IsSuccess = true, Message = "Image successfully removed" });
                    }

                        //return Json(new Response(true, "Image successfully saved!"));
                    else
                        return Json(new Response(false, "Image delete faild!"));
                }
                return Json(new Response(false, "Invalid file or Team member"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult TeamMemberNomineeImageDelete(string memberPin)
        {
            try
            {
                bool deleteImage = false;
                TeamMember teamMember = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                if (teamMember != null)
                {
                    deleteImage = _teamMemberImagesMediaService.FileNomineeDelete(teamMember);
                    if (deleteImage)
                    {
                        const string imageSrc = "<img width=\"200\" height=\"200\" alt=\"NO IMAGE\" style=\"padding: 2px; padding-left: 2px; padding-right: 2px; border-color: dodgerblue\" src=\"data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAIAAACx0UUtAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAB1ZSURBVHhe7Z1fqF3FvcdbH8yLbTmkT/ZJH6x9EaQEBKEI98mHvvlQ+qBw3wqFcvJHCETQYCJ6XyxYpNW3hIpBuUk5f2LOVWMw2jSJBymHiybeGJNWRUOw+CBS8H7W/s1ea/asWbP3mj179pxzfl++JGfNzJr5rt/vu2etWXvtvb/3nUJRNjwe/fbbb7/55pt///vfZtuCVJkNCzSmnFqzbSFQNXYgb1XEQFLVdyBv1cVr/zp24drzb1763Z8vPPjs6V1Prt11YPWO/csLe1d+uKfibbuXdyyehJRQTi1taEl79mJfejB9DdFXA6CE8sDBBqp6DQSkymxYiNMwdqB2letRGn399dfeLij3VlEiVWbbAqNSTp9m24IM5D2MEjQMOqt6w1Lw0NIGPsNzty6e/N7u/5mG9LBz3wq90eeZS1/QOTKyHWygKpuGvkkf8ejNmze//PJLbxdUAa9WdqHKbFtgVKq8h0EhVd7DoCuvBgYaSPBooHFaDWsbn0KmPZzE1OiYLC13LK4+8MzrjCUvBqNgGAdvwAdh8MRh7MF6q2SgXgGnhF2oMtsW0iYdMFbjUdr985//bHdBo88HaGulU3ah93YVgqjyHoYM5D0MuuqrgcaiwWxbiNCASw4ef+/nB1cdJ+UhVwWY9dzHqLvJwbZzRkk4Dr0OFkQEvE662baQPOkU8q/xKBs0DWj1xouuqTLbFpJrFQ3teNGYXaaPF9bktPvrP57latLxTX7esnjq/sMnj567ak+rYKxBvXGQg/XGYWzA55t02kt55VEGZrstSLTSRUBrO17JtVbR6k4MVWbbwuQa8AHrmHsef80xSgnk8leuAdDJ4cvBdsUhrUGpCiTdbFtInnSq2EUOtvIojdpHXmttV43VGohXX62MAro0eAeaMF7knrkKHzjOKI0Le1dw6qvnr3Cw7TiMNWhEwKmKSPrYgDsIa7APtvJoW5C0A/N9MYmGtrxAYibRIHNn+e60ubBnqZ5Ta4w1qDcOEQGffkZw0EtDs2aqUbfbegY9+9ENViQsTRwHbBbedWB1beNTceqMDFpg0l2PSju6CGht+0a0BuKVyqCiwTvQ2HiR2t/9+YKT9c1I1nYcC/EJBNwbh3DAqeoKOLVm20Ieg4IRjxauNdqgXMxx6blz32zvdObkjsXV9qkfRAecqoQBj9PgTToY8ai0Q5nZHqIEg9KYXSLidWL9EyYeJ8dbgzKhmkMdBtwbh7EBLzPplIDGo/ROu5INGqHhpXcv//TAZlob9eXtjy5xhY1TJeDeOGxeg9Kef41H6ZcB2u3GamVss20hrVbR4B0oEC/WFgePv3fL4iknqVuSh5Y2uJ6JCDhVEUnPYFA00KHsUnmUbbouUyuN2SXCoL858jcnkVubj/zpTPvyNM4cEQFPnnTK694qj1LUpZWmZttCTq3sEmHQh557w0nhduCDz562bTo24KTYFA1RTtLtgUbWTDVqre0usmmNi9eZS1/cf3jax+c2L+95/DWxaTjgVJVsUEeDx6MlaBUN7Gi2LQQ0MINu7RXSJNy5b4U4BAJOVVfAqTXbFjIbtJ1016MlawVq0EnIYv/sRzdMXIYIBLzwpI94dPMalFP8vB76LJN3HVi1r03rgJNiUzRE4UmnceNRNkRruwvR6u0irVaJV1+DMoNu52vQLu56ck1sKgEnel0Bp9ZsWyjEoMB4lL9o5G2XTWu0QbfnKn4SykqfaBO9VAHPZlCqGIVdKo/yHxuM3e6iBK0BDRh0azwmMjs+8qczJ9Y/wY4mZEMUPoMyBGOJ7MqjYthB1Qgya6XKbFsIazjy9odOSpRttp8+KcegXRoorweqPNp+kQHR6vV4Wq0Sr64XE1Vd8Xrp3cs7pv4k8TbhuY+bGJY/gzoamjWTjTK11qCQ81eZH0Iqkzv3rchUGj0jUDWvpHs8WqxWAYXMoHoZ2pfyIB9RJRcmlBZKTrrrUWnn7SKt1mnOOCyVnAQoJ+HB4+9FzAhUpUq6GLRv0kc8mk3rNGccLq3uOqC362N42+5lZ/EEsiVdDNo36fTTeFS6KFaraGDfQ0sbTuiVk9N5dL9wgzKXNR5lg/2zaaXKbFuYJF7Ed5s8tjw7nrn0hUR1koCbbQvZDEoVMB7lP9mWOhtptU65qMSgDz572om4si/l+b1JAm62LWQzKIUAw/B35VF6aY8KaFSC1loDkXXCrYzj829eIkdjA+4gZ9LprR6o8qi41UEhWmsNGHTzfndDaVzYs7S28akE2UZpSRc0ayYbs9BKldm2MLlWvd+UlkylEtgaBSZd4PFoWq1TXoPKpk6iyVm/8yRIm3Qx6JRJr+F6tEytOonOgvVUWmbSa4x4tEytvNx1OT8LygK/zKTXYIjGo9JFKq1yiqfKbFvoq5U4OsFVpuKJ9U8SJj25Qemw8SgbdJ1Ka5Jr0Br6+MjseP/hk+3MgrknHVAoAiqP0nu97SBCK40TamUSvf3RJSeyylS8ZfGUvXISzD3pgCogA1UeZQBsOqgaQbRWqsy2hTit+qT9rOnchCrBoHRlD1R5tD0qiNCafLanSj9PN2s+8MzrJtzFGJQqe9Js1kw2StDKKFzRL+zdOl9sWybr030hSafKOat7PFqCVrpioGMXrjkBVc6CnO4lF3NPuleD69EIg8opniqzbWEarQykj4rmIat7Yh5h0ORJ966LRjwabVB2NNsW4rQySq1VP1WXhwt7qi+DlvjXGGvQGSXdAaM3HpUuIl5MybWKBi6S9KPJ2eh8h9m8km6DoSnkX+NRNhjV2y6bVrqytep79Dlp34GaY9JrMDR7SXnlUeZY9m/PtAGtyU/xotXWoBejOfngs6cl7PkN6jUeVfUulUcZuy0ooDW5QRmlrVWfI8nJuw6sEvNA0pMb1Jt0wNDOQJVH24JyaqUrqmRWr8HF6Ob6Pc/NTrlLOt+kA6/xmjVTjbEGpcpsW5hGa/vFRLycICpnzRPrn8w36V3Gcz0aMGjyU3xAK/FyIqicNVk2tZM+oxl0coOCEY8G2iXXyihUdc32B4+/50RQOWuySDU5GGJGSe9lUNB4NKdB5cXUZVBqdVGfn7/+41mThgEyJ91rPEpA41F697aTUzxVZtvCNFoDLyb+3m6/UlcCH3ruDUkEEIPmTLrXeHTVeJQNBugyaMIXE2om0bpVfym5ZN7z+GuSi+Qz6IRJt4EGOpRJt/Iof9G1t11arfJikoFttLXqzdH8vGP/MpFPnnTSOmHSa9CY8rq3yqMYvCiDAn2aJD937lsh8rNIet8Z1BmouR61kfwU31erejQ/F/aukMESDOpo8Hg0+QyKmr5a1aP5ecviKW/6siW9y3iuR5MbVF5MdGu2hwhopUR/+TM/8ahJgIVsSQ8Yb8Sj0o4uzLaFabT2fTGxl3o0P3csnjQ5GCJn0tnFa1AaNx6l0652mbXSm57r81PWTDVKMCj9NB7lLxp528VpRQ1VDG+2h5hQq35LXn7aHp0m6akMShWjsEvlUf5jwysoTitDUhVtUKAfq89PeYQUzCvpNsSQYvfKo2LYQdUIptE65YtJ32fKT3mfaY5Jr8EQ7FLbvfJou2sQp5Wuk2jV9+vzk3lBcjGvpAvEePZAzZrJRpxB5cVU279GhFZ97ik/ub4iR3NMOvAaz+PRuRsUvHr+ihNB5azJucub2bFJTzuDto3nenQagybUqh7Nz5fevWwSYGHuBgUjHo0zKGqSaz1z6Ytb9Pfs8lK+mcxG5qR7jUc/jUeli74GlRcT+5rtIabXqj9cm5M7Fk86Hp1L0h3wGmg8ygb7xxl0Ri8mfYQ0J+XGU424pFOe0KBUAeNR/pNtqbMxL4OC59+85MRROTvaH2YKJJ20zjTpNSgE0lvlUXoBg6oR0Cistb1XQq1ckjpxVM6OR89dlbDPN+kC2tNb/Uow86hs2AhopYsMWrk8um23Pv2UifLdjnNPOhAN9kDNmsnGWK0ZZnuq9Am9PLxj/zIzQglJl4EcDR6PBrSiJptWetNvgshDuXtfQtK9GlyPjn0x2ZOwILlW0cC+Zz+64URTOQsyF5C+EpLu1TDi0XIMKho4Aem3582aOxZXXz1/JZD0DDNowHgM0XhUupivVhnI1qA/xDhrhn+OcY6neECHjUfZoKm3HWroon0YybV6X0xMpU5MlWnp/M6dQJI+3xmUQnFd5VGk1NsOxOPzMijAo7q6nx1/uGeFCJtYDzH3pNM/XVErA1UeZYD2KwaI1vm+mOhKV/ezIyt6E+ghSki6M1Dl0faogEZ5tEpQ2lplIKp4oeuP4MyIziQqAS8h6fZAzZrJhnTB8GZ7iORawy8mqiRe+tGRWfCBZ163PSoBn2PSZaA66TU8Hi3NoODcxzed+Cqn57EL1yS8YO5J7zIocD0qWuc424O2Vl7u+qheWt7z+Gv1JDr3pAcMCkY8mk1rxItJH4NKy3oS7Qp48qRHGJQq0HiUfmnK8GZ7COkC8IcpGiKtQQGjeLVS8ur5K1w/OYFWxnHXk2syiYpvykw6QzMW/xqPsjOjFquVcmoJqxNrZRzlF2zFoN6AD3I+56Szlxiy8igt2L8tKLlWCUpbqwzUpZVy0YBH9a3R6cmVPZHMZtCIpFPCXvUulUcZuy0oudauF1NYK+W2BoK7c9+KE3Tl5JTfXMxm0Oik2wNVHm0LyqYVBLR6NRw9d9WJu3JyHlrakFyQRxPQIbIlXQaaPOnNmqlGNq2AUbxaGZpyr4ZzH9+8/7C+7RTDuw6srm182hXwQc6TJZ32VCVJuuvRbFplIK9WSrxagWgg0PruaARfevcyUe01I5SQ9BGPShfedtlmUEom0XDswjUnAcowWW6+ev4KMZRg1sicdNAr6aDxaGatVEUbFHDhr99ROjl/fnD1xPon7YBnS7oM1DfplADjUf6id2+7OK2076uVKson14BN9ft2JuFtu5c5y2cwaNqk05iuKDceZYNRU2mlsK9WSrq0BjQMLkzVpmN45O0PyzRoOOmMwr/8XXmUv+g6ldYug4KA1ipaPePFQOzCDOGkRGnz4PH35PsdbIhv+gY8Z9Ipr3urPNreH2TTikTKu7R2aZCB+Jczvn4zVBcf+dMZ4mNCNkQJBh2bdHugZs1kI04r7alqa5WgUOV9MVHeN15OUEiDfjd5mw8990YGg2ZIusejcVod39gIaK2iFWVQRwPJ0Lfybd5/2P0+UZDcoF1JT2hQ4Hp0MxpUwPpJf9VJWLJBIzSMeDROK+29WkGXViRS3ldrwKCigcWBzqZ5DJot6TRuPEqnXe0itEpQqPK+mCjvq1UM6g2KaGBf/iY92/nadMtcgwrYi/bGo/xFI2+7gNaAbwJaq2hFGTQQLzGogCRtz5V+Cat4EJF070BUMQq7VB7lPza87aK1UjUXgwpI1ZG3P9xWv0wy3/ugMhBV3qRTHqGBsaS3yqNi2EHVCAJaxRy9tFIVobUrKKDLoKIBkLPt8BvjC3uWeEFmMGjOpLNLndnKo+2uQUBrl28CWimJ0+odCIw1qAzEhLq1vz+CFVLJD4tQ3ldDe6BmzWQjQisIaK2iFaXVq2GsQW0N2PTouas/3LPVPmHClczv/nxhvgYFjEJV2qQ7A3k8mlYrEimP0+rVEDZol4aX3r28lZ7e3/XkWrYHljMnvT2Q69GAVjFHuwt0dGmlhPJUWsFYg7YHEg3syIR67MK1hb2be0LdsXiS08Laxqccr9cclINeAS886SMeDWgNdBHQWkUrSmsgXl0GBYF4me3BqZ9T5K2b86Mmv/7jWfSPjUOvgEckXQaiKiLpvQYC9NN4NK6LaK1zMWgN5qHNdep/4JnXWblvdoMGku51F3vRlfEovbN/wBy9DEq/lMdpDWjoSgxV7YECBpXEYFOyXv4HTnDn2Y9u4E6USxy8AedIQVfAT/39+v9+/jU0pQMEDNqV9DrgaZPudReFgN4qj/KfbEudjQittTlSaRUNtUEJ9N6XL7Kq/eXvTxN670CiwTsQQ1BVJ4bcw98c+VtpHzTlakTO7OJOEDAHVd44SMDf+uAzIkafvz16vrapEwcbJSSdKiC9VR6lFzCoGkGgi4DWKlpRWvnXbFuwDUp8Xzxz2V70/OyxZWduAHW8zLaFQGLOXPqC69QS7vn/9MDyoaWN2poCOw42xDeBgGNQXsx15zsWV59e2bh4tTPghSSd3uqBzDwqGzYitNIP5Wm1UsW+GPH961/d+4RnqvvxvhVqa6fGGbQ+WJzBiRWz5v803+2PLj303BvHLlxz3AkkDt6Ac6RdAaec88zdj3kO5CePLvFqb7+8I5JeBzwi6d6B6qSbbe/9USDt2l1IUKhKa9CwVkLJScqJssNXLl6j2ZQGNdsDyEkWs7K0mt1lAD1zucmsycWx98PvQOLQ16BL61eP/fVS+AOJzK+2TSOSnsGgwOPRri5mpNXrG9HK3Hl8/R8/muwtoodfeIfEeAfqa1ABXckRiV9xEheIzK/TWBbT3LF/mX7ojasLmTJFnldDV8AlFyLPFA0hBv3PF844Q3t56+KqTKiZk94VcKocgwLXo4EuAlqraCU1KL1hUPtCahJyCjt35YZzChMHeAeKiBc4sf7Jkbc/PHru6vNvXhLjPvjsaaZD7MulJOSiFu56co1ysePB4+/Bl969LKa0EaEhYFBy8d8X/o/LdCcyYe46uMpeXQOhIW3S+wZ8xKOBLrq0IpHyOK38a7Yt0J5pgHP3hNNnm6wJapumNajkzHuwgL28A9EPu8Rp8Aa8SwNHffgv69/fveYEZBKyV/sKlVG6NFDu1RCddKq8BmWIxqOSzna8JChUeV9MlMdp7UoMBv3VH950ItiXd+5fZukat3rtipfEoe/BSlT7amAUqiY3B97iBNJ3+mzzP/7rdbGpHKxXwyySTpU34BTyr/EoGzTtFS9KqIrT6s0Zi1Au8zlfO4GLJl5/64PPTO8W4uIVcbDTGHRyDbhKbn8mIacv1gBy4zki6YGD9VYFAk5wpLzyKAPX2w5QQxcZDIqZOE858ZqeRNw+9QPRUKxBRUM74F4NHBcXRT+ewRdbs+SygyaYJul9A04VO8pAlUdp1A4KQE1XvCiP0+rNmXOfOTk59TM3EHTxTd94DfJSlkE5FoLmvWGcivV5X8DQaOsbh7EH2xVwauuBKo+2R5V23nhRklYrpxU85ARoFmQUpmouUs3AFsLx6nuwcQZlFKrGGhTf8Hq771COj2oxQxMuRkye9HDAqbIHatZMNqSd16BVtKK0tnPGwY+9z5ycdz9m7ggaESXNoAEN/I1mzuwznTu9JFwsZPvGYZqDdYzn8SiN6ML7gqac2gitXoPO4gJ0QnKdyjoDDZwxOaJAvLoOliqzbWEag3oDziicZ9DJVfUsrjsnpPfydBYG9cZhxKOBdpRQHshZL60c8Ni3N/Nw10F3WgUSh74HG2dQRqGqHXDA7IW2mV6pT86HX3jHjtKMDNoOOBjxaFe8KKkylm4Gnf4OaFoyrZIDedMfhRxO34NNaFA0cMVJiBb2JLsNl4T1KmoWBvUGnBLQeJTeaZfBoByqc/DlkItjZlYuA96//pXko0Zyg0rOJKqMBWXWjH6DLQO5GpZ3RpIbtG08qmjfeJR+6cXbjq4TGhQHOIddLLn+wzFyexVy5vW6MM6gVMmFJr5kCp/jtWZf/uTRpcA7I944hA1KVdtdlNCV7FJ5FGsSx3Y7ytk/zqDtxGwug7ZJbjgDcBnNJcG5KzfkpgxxIAjt6AN52UvoaEl79mJfJulfPHXyzv3Lm/erfpjpOSI5TEFX0kGEQeXUXfdWeZT9u9qBCIO2X0wcUp6boDl56+LqD3Yv4927H1vlJAjvO3QKH/Pvzx5bhhwyXNi7kvn+WgZyULVNu5IOwgb1uktmRttdlUczGHT6xx2UpZHrEzILSHrEDAqwmSkaQgxKldkeoFkz1aALaZfKoJv6FK8MkNMIV9UJT/FiPLM9hOvRul2EQdta1aBbnlzq1Cf9GmNn0C6Det014lHadXURN4OWfJtJmYpcjts2HWtQbGaKhuiaQQWNRwMejzPohB+pUW4B1rf3wwalqu2ugEFpDIxH+QufxRnUe4pP+NStclPw4RfeeeuDz+IM6nUXVVjLeJT/2MBwEQb1zqBzfFhEOUdy5nz/+lfGB0OIQb3TX8CgGA9rid0rj/KXGHZQ2yDCoODYX/WXEbcv62ceBLVBsaMpGiJwiseNlNfn58qj7f1BnEFP/f36D3brrdBtzfopcjEoVumaQbsM6rirWTPZGGvQ2uM2uBzZem8mKftS3ikda1Cvu9oGBR6Pxs2gGLS0J+6U8+J9h07Jw+N9T/Fed7kejT7F6zpJabPrk6V9DQpGPBp3iqe9rpOUbR5f/4exyABxBuVSofFotEGX1q/qZaiyzR2LzftPfa9BBezVeJQN9ve2E4N6q8SgD7/wjiNOqRTK+09xMyiTJrXGo/zHBpA6G2GDMuorF685spRKm0+vbMhHn41vLGC5LndRRTnm5u/KoxgWDKpGIAb1eheDUvX+9a+23tO7yuTEo8Y3FgIGpRBP156sPCpudSAG9XZRG1TP8spJeO8TJ501ftigVNkzY7NmsjHWoNhaz/LKyTnNl8J6PDrWoEzCjFfyR2yVBVI82tegwPVoVztgG1TP8sq+vO/QKa4P+xoUjHhU2nm7qA3K34zkDK9UTsLDf1nva9CRe/iBSdg2KJOo9yd/lMqx/MFuz2++BQyK5RqPsoERw6d42XzxzGVnYKVyctq/BwkCBqVQXFd5NHAP3zEovetSSTkl6wdMAwalCuBM/q48igVrF9poG7SQL2RUbmr+cvD7egGDYrzPref6zDwqGzYcgwL6dQZTKuO4tH41YFDHeM2ayYbXoIV8WatyC/DO/cvtT+eBtvGAx6Pednq/SZmWztOlwGs84HrU204nUWVyMpXiK+OwboOCEY92tTt35YYzgFI5PV88c1kMFjDoyD18WWe122H2XzyV+/dWlNuB8kVRXQbFnRQ2HmWDpl4j62eVlLMjU6n9qGgNrMmqX8orj347+GKd9lOktDv19+t6JaqcHX/2mOfdUYzHjFnfmao8KjOqbNegBIPjUadTpTItnZtQYjw8Wnuy8miXQWmnbywpZ01528k2HrA92ayZatTt2FPfnVdmoHjUa1DgelTasc7iD33ESZmHe1++iPe8BgUjHrUNirX1OVFlHnK6ZuXjNShoPCoGBbLAf+uDz5yOlMrZ8fBf1tsGpQQ0Hr05+nuh+oklZU7ed+iUcxMKd8odJ+PRr0e/a1xXS8r8tD3KXIkh3Xv4tUGBfnZemZ+ycgJYk1N66B4+dtbfVVLmp/y4I25k1c4kauzYvvcEaOfsrFTmoffH8T0efXplw9lTqczDX/3hTXsGFXg8qr+RrJwXF/Z4fn10xKMsnpbWr35/95qzp1KZjW998Jmx4wDNvSfABosp/ekF5XxZr+4Bnmw8yl9cqHIpoCt65XxZf86JBb7cEq08yn9yD586vXWvnDvfv/4VBsWQ8q5n5VExLH/oe/TKEsgFp/scfv0evT7RrCyBv3jqJPOmeBI0ayZO9Pc+oZ//VM6fzldAjnjUaapUzot+j+rFqLIcPr2yYXxpe1QvRpXl8Je/Py22bO6Pgl0H9ZMhylL4o8GP4LOabzzK9i2Lp5x2SuUcKXdJm3v4ejGqLI1ckuJM/Fl5lBlVn8dTlkYWSNUpvl4z6SfslKXx3idOijmNR/Wj9MrSuLB3RcxZeZQFkz5KoiyN39+9Jl9XVnlUv6ZZWSZfuXjNeFQ/qawsk/Jl5JVH9bvHlGWSpbzxqC7qlWVy18FV49H7Duk7TMoSeef+ZeNR/nLqlMoSKO/aVx7VG0/KYln9wKI+TaIsmdWN0YtXbzqlSmU5rDyqN/CVJbO6MapP5SlL5t6XL35Pb+ArS2b1EabKp60KpbIQVm8wqUeVJbP6uVp9I1RZMqsvyVOPKkumelRZOqtPiOjP0ytLZuVR/VJcZcmsHnj67dHzP3l0ib+UytKIM6vz/HcKRcn47rv/B6GrUu3h/YF/AAAAAElFTkSuQmCC\">";
                        return Json(new { image = imageSrc, IsSuccess = true, Message = "Image successfully removed" });
                    }
                    else
                        return Json(new Response(false, "Image delete faild!"));
                }
                return Json(new Response(false, "Invalid file or Team member"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public ActionResult TeamMemberTrainingCertificateUpdate(string memberPin, string trainingInfoId, HttpPostedFileBase file)
        {
            try
            {
                TeamMemberTrainingCertificateMediaImage tmTrainingCertificateMediaImage = null;
                TeamMember teamMember = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                TrainingInfo trainingEntity = _trainingInfoService.GetMemberTrainingInfoById(Convert.ToInt64(trainingInfoId));
                if (teamMember != null && trainingEntity != null)
                {
                    tmTrainingCertificateMediaImage = _teamMemberCertificateImagesMediaService.TrainingCertificateFileUpload(teamMember, trainingEntity, file, true);
                    if (_teamMemberCertificateImagesMediaService != null)
                    {
                        /*string imageBase64 = Convert.ToBase64String(teamMemberCertificateMediaImage.Images);
                        string imageSrc = string.Format("data:image/gif;base64,{0}", imageBase64);
                        return Json(new { image = imageSrc, IsSuccess = true, Message = "Certificate successfully saved!" });*/
                        return Json(new { IsSuccess = true, Message = "Training Certificate Successfully Saved!" });
                    }
                    else
                    {
                        return Json(new Response(false, "Training Certificate saved faild!"));
                    }

                }
                return Json(new Response(false, "Invalid file or Team member"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public ActionResult TeamMemberCertificateImageUpdate(string memberPin, string academicInfoId, HttpPostedFileBase file)
        {
            try
            {
                TeamMemberCertificateMediaImage teamMemberCertificateMediaImage = null;
                TeamMember teamMember = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                if (teamMember != null && file != null && file.ContentLength > 0)
                {
                    long academicId = Convert.ToInt64(academicInfoId);
                    teamMemberCertificateMediaImage = _teamMemberCertificateImagesMediaService.FileUpload(teamMember, academicId, file, true);
                    if (teamMemberCertificateMediaImage != null)
                    {
                        string imageBase64 = Convert.ToBase64String(teamMemberCertificateMediaImage.Images);
                        string imageSrc = string.Format("data:image/gif;base64,{0}", imageBase64);
                        return Json(new { image = imageSrc, IsSuccess = true, Message = "Certificate successfully saved!" });
                    }
                    else
                        return Json(new Response(false, "Certificate saved faild!"));
                }
                return Json(new Response(false, "Invalid file or Team member"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public ActionResult TeamMemberCertificateImageView(string memberPin, string academicInfoId)
        {
            try
            {
                ViewBag.HasImage = false;
                long aId = Convert.ToInt64(academicInfoId);
                TeamMember teamMember = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                AcademicInfo academicInfo = _academicInfoService.GetById(aId);
                if (teamMember != null && academicInfo != null)
                {
                    TeamMemberCertificateMediaImage imageFile = null;

                    imageFile = _teamMemberCertificateImagesMediaService.GetTeamMemberCertificateMediaImageByRefId(aId, teamMember.Id);
                    if (imageFile != null)
                    {
                        ViewBag.HasImage = true;
                        ViewBag.CertificateImage = imageFile;
                        ViewBag.TeamMember = teamMember;
                        ViewBag.AcademicInfo = academicInfo;
                        return View();
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
        }
        public ActionResult TeamMemberTrainingCertificate(string memberPin, string trainingInfoId)
        {
            try
            {
                ViewBag.HasImage = false;
                long trId = Convert.ToInt64(trainingInfoId);
                TeamMember teamMember = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                TrainingInfo trainingInfo = _trainingInfoService.GetMemberTrainingInfoById(trId);
                if (teamMember != null && trainingInfo != null)
                {
                    TeamMemberTrainingCertificateMediaImage imageFile = null;
                    imageFile = _teamMemberCertificateImagesMediaService.GetTeamMemberTrainingCertificateMediaImageByRefId(trId, teamMember.Id);
                    if (imageFile != null)
                    {
                        ViewBag.HasImage = true;
                        ViewBag.TrainingCertificateImage = imageFile;
                        ViewBag.TeamMember = teamMember;
                        ViewBag.TrainingInfo = trainingInfo;
                        return View();
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
        }
        #endregion

        [HttpPost]
        //public JsonResult EditPersonalInformation(string memberPin, string fullName, string fullNameBangla, string facebook, string skype, string personalEmail,
        //    int gender, int religion, string bloodGroup, int nationality, string nidNo, string bcNo, string passportNo, string dateofBirthCertificate,
        //    string dateofBirthActual, string emergencyName, string energencyRelation, string emergencyCellNo, long? presentPostOffice = null,
        //    string presentAddress = null, long? permanentPostOffice = null, string permanentAddress = null)
        public JsonResult EditPersonalInformation(string memberPin, string fullName, string fullNameBangla, string facebook, string skype, string personalEmail,
           int gender, int religion, string bloodGroup, int nationality, string nidNo, string bcNo, string passportNo, string dateofBirthCertificate,
           string dateofBirthActual, string emergencyName, string energencyRelation, string emergencyCellNo, long? presentThana = null, long? presentPostOffice = null,
           string presentAddress = null, long? permanentThana = null, long? permanentPostOffice = null, string permanentAddress = null)
        {
            try
            {
                int? convertedBloodGroup = null;
                if (!String.IsNullOrEmpty(bloodGroup))
                    convertedBloodGroup = Convert.ToInt32(bloodGroup);
                var member = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                if (member == null)
                    return Json(new Response(false, "Invalid member pin"));

                member.FullNameEng = fullName;
                member.FullNameBang = fullNameBangla;
                member.Facebook = facebook;
                member.Skype = skype;
                member.PersonalEmail = personalEmail;
                member.Gender = gender;
                member.Religion = religion;
                member.BloodGroup = convertedBloodGroup;
                member.Nationality = nationality;
                member.NID = nidNo;
                member.BCNo = bcNo;
                member.PassportNo = passportNo;
                if (dateofBirthCertificate != "")
                    member.CertificateDOB = Convert.ToDateTime(dateofBirthCertificate);
                if (dateofBirthActual != "")
                    member.ActualDOB = Convert.ToDateTime(dateofBirthActual);
                member.EmargencyContactName = emergencyName;
                member.PresentAddress = presentAddress;
                member.PermanentAddress = permanentAddress;
                member.EmargencyContactRelation = energencyRelation;
                member.EmargencyContactCellNo = emergencyCellNo;
                member.PresentAddress = presentAddress;
                member.PermanentAddress = permanentAddress;
                //if (_hrMemberService.UpdatePersonalInformation(member, presentPostOffice, permanentPostOffice))
                if (_hrMemberService.UpdatePersonalInformation(member, presentThana, permanentThana, presentPostOffice, permanentPostOffice))
                {
                    return Json(new Response(true, "Personal Information successfully saved!"));
                }
                else
                    return Json(new Response(false, "Personal Information save fail!"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public JsonResult EditParentalInformation(string memberPin, string fatherNameEng, string fatherNameBangla, string motherNameEng, string motherNameBangla,
            string fatherContactNumber, string motherContactNumber)
        {
            try
            {
                var member = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                if (member == null)
                    return Json(new Response(false, "Team member invalid!"));
                member.FatherNameEng = fatherNameEng;
                member.FatherNameBang = fatherNameBangla;
                member.MotherNameEng = motherNameEng;
                member.MotherNameBang = motherNameBangla;
                member.FatherContactNo = fatherContactNumber;
                member.MotherContactNo = motherContactNumber;

                if (_hrMemberService.UpdateParentalInformation(member))
                {
                    //   TempData["AddEmploymentHistory"] = "Member Salary Edit successfully";
                    return Json(new Response(true, "Parental Information successfully saved"));
                }
                else
                    return Json(new Response(false, "Parental Information save fail!"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public JsonResult EditMaritalInformation(string memberPin, int maritalStatus, string marriageDate, string spouseName, string spouseContactNumber)
        {
            try
            {
                var member = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                if (_maritalInfoService.UpdateMaritalInformation(member, maritalStatus, marriageDate, spouseName, spouseContactNumber))
                {
                    //   TempData["AddEmploymentHistory"] = "Member Salary Edit successfully";
                    return Json(new Response(true, "Marital Information successfully saved"));
                }
                else
                    return Json(new Response(false, "Marital Information save fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public JsonResult UpdateChildrenInfo(string memberPin, List<TeammemberChildrenArray> childrenInfoArray)
        {
            try
            {
                var member = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                List<long> idList = _childrenInfoService.UpdateChildrenInfo(member, childrenInfoArray);
                if (idList.Any())
                {
                    TempData["updateAcademicInfo"] = "Child Information save successfully";
                    return Json(new { idlist = idList, IsSuccess = true });
                }
                else
                {
                    return Json(new Response(false, "Child Information save fail!"));
                }
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public JsonResult DeleteChildrenInfo(long id)
        {
            try
            {
                if (_childrenInfoService.DeleteChildrenInfo(id))
                {
                    return Json(new Response(true, "Child Information delete successfully!"));
                }
                else
                    return Json(new Response(false, "Child Information save fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public JsonResult EditNomineeInformation(string memberPin, string nomineeName, string relation, string contactNumber)
        {
            try
            {
                var member = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                var entity = new NomineeInfo() { Name = nomineeName, Relation = relation, MobileNo = contactNumber };
                if (_nomineeInfoService.UpdateNomineeInformation(member, entity))
                {
                    return Json(new Response(true, "Nominee Information successfully saved!"));
                }
                else
                {
                    return Json(new Response(false, "Nominee Information save fail!"));
                }
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public JsonResult UpdateAcademicInfo(string memberPin, List<TeamMemberAcademicInfoArray> academicInfoArray)
        {
            try
            {
                var member = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                List<long> ids = _academicInfoService.UpdateAcademicInfo(member, academicInfoArray);
                if (ids.Any())
                {
                    TempData["updateAcademicInfo"] = "Academic Information Saved Successfully!";
                    return Json(new { idlist = ids, IsSuccess = true });
                }
                else
                    return Json(new Response(false, "Academic Information save fail!"));

            }
            catch (FormatException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        [HttpPost]
        public JsonResult UpdateTrainingInfo(string memberPin, List<TeamMemberTrainingInfoArray> trainingInfoArray)
        {
            try
            {
                var member = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                List<long> ids = _trainingInfoService.UpdateTrainingInfo(member, trainingInfoArray);
                if (ids.Any())
                {
                    TempData["updateTrainingInfo"] = "Training Information Saved Successfully!";
                    return Json(new { idlist = ids, IsSuccess = true });
                }
                else
                    return Json(new Response(false, "Training Information save fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        [HttpPost]
        public JsonResult UpdateProfessionalQualificationInfo(string memberPin,
            List<TeamMemberProfessionalQualificationInfoArray> pQInfoArray)
        {
            try
            {
                var member = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                List<long> ids = _trainingInfoService.UpdateProfessionalQualificationsInfo(member, pQInfoArray);
                if (ids.Any())
                {
                    TempData["updatePQInfo"] = "Professional Qualifications Saved Successfully!";
                    return Json(new { idlist = ids, IsSuccess = true });
                }
                else
                    return Json(new Response(false, "Professional Qualifications  save fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        public JsonResult DeleteTrainingInfo(long id)
        {
            try
            {
                if (_trainingInfoService.DeleteTrainingInfo(id))
                {
                    return Json(new Response(true, "Training Information delete successfully!"));
                }
                else
                    return Json(new Response(false, "Training Information save fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        [HttpPost]
        public JsonResult DeleteAcademicInfo(long id)
        {
            try
            {
                if (_academicInfoService.DeleteAcademicInfo(id))
                {
                    return Json(new Response(true, "Academic Information delete successfully!"));
                }
                else
                    return Json(new Response(false, "Academic Information save fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        [HttpPost]
        public JsonResult UpdateJobExperienceInfo(string memberPin, List<JobExperience> jobInfoArray)
        {
            try
            {
                var member = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                List<long> ids = _hrJobExpService.UpdateJobExperienceInfo(member, jobInfoArray);
                if (ids.Any())
                {
                    TempData["UpdateJobExperienceInfo"] = "Job Experience Info Saved Successfully!";
                    return Json(new { idlist = ids, IsSuccess = true });
                }
                else
                    return Json(new Response(false, "Job Experience Info Save Failed!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        [HttpPost]
        public JsonResult UpdateTeamMemberSpecialization(string fields, string extraC, string memberPin)
        {
            try
            {
                var member = _hrMemberService.GetByPin(Convert.ToInt32(memberPin));
                if (member == null)
                    return Json(new Response(false, "Invalid team member!"));
                member.Specialization = fields;
                member.ExtracurricularActivities = extraC;
                if (_hrMemberService.UpdateSpecialization(member))
                {
                    return Json(new Response(true, "Team Member Specialization successfully saved!"));
                }
                else
                    return Json(new Response(false, "Team Member Specialization save fail!"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        public JsonResult DeleteJobExperience(long id)
        {
            try
            {
                if (_hrJobExpService.DeleteJobExperienceInfo(id))
                {
                    return Json(new Response(true, "Job Experience Information delete successfully!"));
                }
                else
                    return Json(new Response(false, "Job Experience Information save fail!"));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        public ActionResult AddRowAcademicInformation()
        {
            ViewBagForAcademicInfoEdit();
            return PartialView("_partial/_AddRowAcademicInformation");
        }
        public ActionResult AddRowChildrenInformation()
        {
            var genders = from Gender ed in Enum.GetValues(typeof(Gender))
                          where (int)ed != 3
                          select new { Id = (int)ed, Name = ed.ToString() };
            ViewBag.GenderList = new SelectList(genders, "Id", "Name");
            return PartialView("_partial/_AddRowChildrenInformation");
        }

        public ActionResult AddRowTrainingInformation()
        {
            return PartialView("_partial/_AddRowTrainingInformation");
        }
        public ActionResult AddRowJobExperience()
        {
            var companyType = Enum.GetValues(typeof(HrCompanyType)).Cast<HrCompanyType>().ToList(); ;
            ViewBag.CompanyType = companyType.Select((r, index) => new SelectListItem { Text = r.ToString(), Value = (index + 1).ToString(CultureInfo.CurrentCulture) });
            return PartialView("_partial/_AddRowJobExperience");
        }
        public ActionResult AddRowProfessionalQualifications()
        {
            return PartialView("_partial/_AddRowProfessionalQualifications");
        }
        #endregion

        #region MemberList

        public ActionResult MemberList()
        {
            ViewBagForMemberList();
            return View();
        }

        [HttpPost]
        public ActionResult MemberList(MemberListViewModel memberListViewModel)
        {
            ViewBagForMemberList();
            try
            {
                if (ModelState.IsValid)
                {
                    return View("MemberListReport", memberListViewModel);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return View();
        }
        [HttpPost]
        public JsonResult LoadmemberList(int draw, int start, int length, long organizationId, long? departmentId, long[] branchIds, long[] campusIds, int? memberTypes
            , int? memberStatus, string[] informationViewList)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];

                }

                #endregion

                IList<MemberSearchListDto> mList = _hrMemberService.LoadTeamMemberListReport(draw, start, orderBy,
                    orderDir, length, _userMenu, organizationId, departmentId, branchIds, campusIds, memberTypes,
                    memberStatus);

                long totalCount = _hrMemberService.CountTeamMemberListReport(_userMenu, organizationId, departmentId,
                    branchIds, campusIds, memberTypes, memberStatus);

                long recordsTotal = totalCount;
                long recordsFiltered = recordsTotal;

                //var results = mList.GroupBy(m => m.Id, (key, group) => new { Key = key, Group = group.ToList() }).Skip(start).Take(length);

                var data = new List<object>();
                int sl = start + 1;
                foreach (var teamMember in mList)
                {
                    //var teamMember = teamMemberList.Group.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case "Organization":
                                    str.Add(teamMember.Organization);
                                    break;
                                case "Branch":
                                    str.Add(teamMember.Branch);
                                    break;
                                case "Campus":
                                    str.Add(teamMember.Campus);
                                    break;
                                case "Pin":
                                    str.Add("<a href='" + Url.Action("SearchOfficialDetails", "TeamMember") +
                                            "?memberPin=" +
                                            teamMember.Id + "' id='" + teamMember.Id + "'>" + teamMember.Pin.ToString() +
                                            "</a>");
                                    break;
                                case "FullName":
                                    str.Add(teamMember.FullName);
                                    break;
                                case "NickName":
                                    str.Add(teamMember.NickName);
                                    break;
                                case "Department":
                                    str.Add(teamMember.Department);
                                    break;
                                case "Designation":
                                    str.Add(teamMember.Designation);
                                    break;
                                case "EmploymentStatus":
                                    str.Add(Enum.GetName(typeof(MemberEmploymentStatus), teamMember.EmploymentStatus));
                                    break;
                                case "MentorNamePIN":
                                    str.Add(teamMember.MentorNamePin);
                                    break;
                                case "JoiningDate":
                                    str.Add(teamMember.JoiningDate != null
                                        ? teamMember.JoiningDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "PermanentDate":
                                    str.Add(teamMember.PermanentDate != null
                                        ? teamMember.PermanentDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "DiscontinuationDate":
                                    str.Add(teamMember.DiscontinuationDate != null
                                        ? teamMember.DiscontinuationDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "OfficialContact":
                                    str.Add(teamMember.OfficialContact != "" ? teamMember.OfficialContact : "");
                                    break;
                                case "PersonalContact":
                                    str.Add(teamMember.PersonalContact != "" ? teamMember.PersonalContact : "");
                                    break;
                                case "OfficialEmail":
                                    str.Add(teamMember.OfficialEmail != "" ? teamMember.OfficialEmail : "");
                                    break;
                                case "PersonalEmail":
                                    str.Add(teamMember.PersonalEmail);
                                    break;
                                case "OfficeShift":
                                    str.Add(teamMember.OfficeShift);
                                    break;

                                case "Gender":
                                    var gender = teamMember.Gender != null
                                        ? Enum.GetName(typeof(Gender), teamMember.Gender)
                                        : "";
                                    str.Add(gender);
                                    break;

                                case "Religion":
                                    var religion = teamMember.Religion != null
                                        ? Enum.GetName(typeof(Religion), teamMember.Religion)
                                        : "";
                                    str.Add(religion);
                                    break;

                                case "BloodGroup":
                                    var bg = "";
                                    if (teamMember.BloodGroup != null)
                                    {
                                        bg = Enum.GetName(typeof(BloodGroup), teamMember.BloodGroup);
                                        bg = bg != null ? bg.Replace("pos", "+").Replace("neg", "-") : "";
                                    }
                                    str.Add(bg);
                                    break;

                                case "DateOfBirth(certificate)":

                                    str.Add(teamMember.DateOfBirthCertificate != null
                                        ? teamMember.DateOfBirthCertificate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;

                                case "DateOfBirth(Actual)":
                                    str.Add(teamMember.DateOfBirthActual != null
                                        ? teamMember.DateOfBirthActual.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;

                                case "HomeDistrict":
                                    str.Add(teamMember.HomeDistrict);
                                    break;

                                case "Upozilla/Thana":
                                    str.Add(teamMember.Upozilla);
                                    break;

                                case "MarriageDate":
                                    str.Add(teamMember.MarriageDate != null
                                        ? teamMember.MarriageDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "MaritalStatus":
                                    str.Add(teamMember.MaritalStatus != null
                                        ? Enum.GetName(typeof(MaritalType), teamMember.MaritalStatus)
                                        : "");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    sl++;
                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            return Json(HttpNotFound());
        }
        #endregion

        #region Export Member List
        public JsonResult ExportMemberList(long organizationId, long departmentId, long[] branchIds, long[] campusIds, int? memberTypes
           , int? memberStatus, string[] informationViewList)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                bool columsAdded = false;
                var headerList = new List<string>();
                var columnList = new List<string>();
                var footerList = new List<string>();
                headerList.Add("Team Member List");
                footerList.Add("");
                if (informationViewList != null)
                {
                    foreach (var info in informationViewList)
                    {
                        columnList.Add(info);
                    }
                }
                IList<MemberSearchListDto> mList = _hrMemberService.LoadTeamMemberListReport(1, 0, "", "", 0, _userMenu,
                    organizationId, departmentId, branchIds, campusIds, memberTypes, memberStatus);
                //var results = mList.GroupBy(m => m.Id, (key, group) => new { Key = key, Group = group.ToList() });

                //IList<EmploymentHistory> memberList = _employmentHistoryService.LoadTeamMemberListReport(1, 0, 0, organizationId, departmentId,
                //    branchIds, campusIds, memberTypes, memberStatus);
                var data = new List<List<object>>();
                foreach (var teamMember in mList)
                {
                    //var teamMember = teamMemberList.Group.OrderByDescending(x => x.PermanentDate).FirstOrDefault();
                    var str = new List<object>();
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case "Organization":
                                    str.Add(teamMember.Organization);
                                    break;
                                case "Branch":
                                    str.Add(teamMember.Branch);
                                    break;
                                case "Campus":
                                    str.Add(teamMember.Campus);
                                    break;
                                case "Pin":
                                    str.Add(teamMember.Pin.ToString());
                                    break;
                                case "FullName":
                                    str.Add(teamMember.FullName);
                                    break;
                                case "NickName":
                                    str.Add(teamMember.NickName);
                                    break;
                                case "Department":
                                    str.Add(teamMember.Department);
                                    break;
                                case "Designation":
                                    str.Add(teamMember.Designation);
                                    break;
                                case "EmploymentStatus":
                                    str.Add(Enum.GetName(typeof(MemberEmploymentStatus), teamMember.EmploymentStatus));
                                    break;
                                case "MentorNamePIN":
                                    str.Add(teamMember.MentorNamePin);
                                    break;
                                case "JoiningDate":
                                    str.Add(teamMember.JoiningDate != null
                                        ? teamMember.JoiningDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "PermanentDate":
                                    str.Add(teamMember.PermanentDate != null
                                        ? teamMember.PermanentDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "DiscontinuationDate":
                                    str.Add(teamMember.DiscontinuationDate != null
                                        ? teamMember.DiscontinuationDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "OfficialContact":
                                    str.Add(teamMember.OfficialContact != "" ? teamMember.OfficialContact : "");
                                    break;
                                case "PersonalContact":
                                    str.Add(teamMember.PersonalContact != "" ? teamMember.PersonalContact : "");
                                    break;
                                case "OfficialEmail":
                                    str.Add(teamMember.OfficialEmail != "" ? teamMember.OfficialEmail : "");
                                    break;
                                case "PersonalEmail":
                                    str.Add(teamMember.PersonalEmail);
                                    break;
                                case "OfficeShift":
                                    str.Add(teamMember.OfficeShift);
                                    break;

                                case "Gender":
                                    var gender = teamMember.Gender != null
                                        ? Enum.GetName(typeof(Gender), teamMember.Gender)
                                        : "";
                                    str.Add(gender);
                                    break;

                                case "Religion":
                                    var religion = teamMember.Religion != null
                                        ? Enum.GetName(typeof(Religion), teamMember.Religion)
                                        : "";
                                    str.Add(religion);
                                    break;

                                case "BloodGroup":
                                    string bg = "-";
                                    var bloodGroupValue = new List<int>(Enum.GetValues(typeof(BloodGroup)).Cast<int>());
                                    if (teamMember.BloodGroup != null &&
                                        bloodGroupValue.Contains(Convert.ToInt32(teamMember.BloodGroup)))
                                    {
                                        bg =
                                            Enum.GetName(typeof(BloodGroup), teamMember.BloodGroup)
                                                .Replace("pos", "+")
                                                .Replace("neg", "-");
                                    }
                                    //var bg = teamMember.BloodGroup != null ? Enum.GetName(typeof(BloodGroup), teamMember.BloodGroup).Replace("pos", "+").Replace("neg", "-") : "";
                                    str.Add(bg);
                                    break;

                                case "DateOfBirth(certificate)":
                                    str.Add(teamMember.DateOfBirthCertificate != null
                                        ? teamMember.DateOfBirthCertificate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;

                                case "DateOfBirth(Actual)":
                                    str.Add(teamMember.DateOfBirthActual != null
                                        ? teamMember.DateOfBirthActual.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;

                                case "HomeDistrict":
                                    str.Add(teamMember.HomeDistrict);
                                    break;

                                case "Upozilla/Thana":
                                    str.Add(teamMember.Upozilla);
                                    break;

                                case "MarriageDate":
                                    str.Add(teamMember.MarriageDate != null
                                        ? teamMember.MarriageDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "MaritalStatus":
                                    str.Add(teamMember.MaritalStatus != null
                                        ? Enum.GetName(typeof(MaritalType), teamMember.MaritalStatus)
                                        : "");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    data.Add(str);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, data, footerList,
                    "team_member_list__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(HttpNotFound());
        }
        #endregion

        #region Member Search

        public ActionResult MemberSearch()
        {
            ViewBagForMemberList();
            return View();
        }

        [HttpPost]
        public ActionResult MemberSearch(MemberSearchViewModel memberSearchViewModel)
        {
            ViewBagForMemberList();
            try
            {
                if (ModelState.IsValid)
                {
                    return View("MemberSearchReport", memberSearchViewModel);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult LoadMemberSearch(int draw, int start, int length, long organizationId, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList)
        {
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];

                }

                #endregion

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;


                long totalCount = _hrMemberService.CountTeammemberSearchListReport(_userMenu, organizationId,
                    departmentId, memberTypes, memberStatus, keyword, informationViewList);
                long recordsTotal = totalCount;
                long recordsFiltered = recordsTotal;

                IList<MemberSearchListDto> memberLists = _hrMemberService.LoadTeamMemberSearchListReport(start, length,
                    orderBy, orderDir, _userMenu, organizationId, departmentId,
                    memberTypes, memberStatus, keyword, informationViewList);

                //var results = memberLists.GroupBy(m => m.Id, (key, group) => new { Key = key, Group = group.ToList() }).Skip(start).Take(length);

                var data = new List<object>();
                int sl = start + 1;
                foreach (var teamMember in memberLists)
                {
                    //var teamMember = teamMemberList.Group.OrderByDescending(x => x.PermanentDate).FirstOrDefault();
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case "Organization":
                                    str.Add(teamMember.Organization);
                                    break;
                                case "Branch":
                                    str.Add(teamMember.Branch);
                                    break;
                                case "Campus":
                                    str.Add(teamMember.Campus);
                                    break;
                                case "Pin":
                                    str.Add("<a href='" + Url.Action("SearchOfficialDetails", "TeamMember") +
                                            "?memberPin=" +
                                            teamMember.Id + "' id='" + teamMember.Id + "'>" + teamMember.Pin.ToString() +
                                            "</a>");
                                    break;
                                case "FullName":
                                    str.Add(teamMember.FullName);
                                    break;
                                case "NickName":
                                    str.Add(teamMember.NickName);
                                    break;
                                case "Department":
                                    str.Add(teamMember.Department);
                                    break;
                                case "Designation":
                                    str.Add(teamMember.Designation);
                                    break;
                                case "EmploymentStatus":
                                    str.Add(Enum.GetName(typeof(MemberEmploymentStatus), teamMember.EmploymentStatus));
                                    break;
                                case "MentorNamePIN":
                                    str.Add(teamMember.MentorNamePin);
                                    break;
                                case "JoiningDate":
                                    str.Add(teamMember.JoiningDate != null
                                        ? teamMember.JoiningDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "PermanentDate":
                                    str.Add(teamMember.PermanentDate != null
                                        ? teamMember.PermanentDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "DiscontinuationDate":
                                    str.Add(teamMember.DiscontinuationDate != null
                                        ? teamMember.DiscontinuationDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "OfficialContact":
                                    str.Add(teamMember.OfficialContact != "" ? teamMember.OfficialContact : "");
                                    break;
                                case "PersonalContact":
                                    str.Add(teamMember.PersonalContact != "" ? teamMember.PersonalContact : "");
                                    break;
                                case "OfficialEmail":
                                    str.Add(teamMember.OfficialEmail != "" ? teamMember.OfficialEmail : "");
                                    break;
                                case "PersonalEmail":
                                    str.Add(teamMember.PersonalEmail);
                                    break;
                                case "OfficeShift":
                                    str.Add(teamMember.OfficeShift);
                                    break;

                                case "Gender":
                                    var gender = teamMember.Gender != null
                                        ? Enum.GetName(typeof(Gender), teamMember.Gender)
                                        : "";
                                    str.Add(gender);
                                    break;

                                case "Religion":
                                    var religion = teamMember.Religion != null
                                        ? Enum.GetName(typeof(Religion), teamMember.Religion)
                                        : "";
                                    str.Add(religion);
                                    break;

                                case "BloodGroup":
                                    var bg = "";
                                    if (teamMember.BloodGroup != null)
                                    {
                                        bg = Enum.GetName(typeof(BloodGroup), teamMember.BloodGroup);
                                        bg = bg != null ? bg.Replace("pos", "+").Replace("neg", "-") : "";
                                    }
                                    str.Add(bg);
                                    break;

                                case "DateOfBirth(certificate)":
                                    str.Add(teamMember.DateOfBirthCertificate != null
                                        ? teamMember.DateOfBirthCertificate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;

                                case "DateOfBirth(Actual)":
                                    str.Add(teamMember.DateOfBirthActual != null
                                        ? teamMember.DateOfBirthActual.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;

                                case "HomeDistrict":
                                    str.Add(teamMember.HomeDistrict);
                                    break;

                                case "Upozilla":
                                    str.Add(teamMember.Upozilla);
                                    break;

                                case "MarriageDate":
                                    str.Add(teamMember.MarriageDate != null
                                        ? teamMember.MarriageDate.Value.ToString("yyyy-MM-dd")
                                        : "");
                                    break;
                                case "MaritalStatus":
                                    str.Add(teamMember.MaritalStatus != null
                                        ? Enum.GetName(typeof(MaritalType), teamMember.MaritalStatus)
                                        : "");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    sl++;
                    data.Add(str);
                }


                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(HttpNotFound());
        }

        public ActionResult ExportMemberSearchResult(long organizationId, long? departmentId, int? memberTypes, int? memberStatus, string keyword, string[] informationViewList)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<MemberSearchListDto> memberLists = _hrMemberService.LoadTeamMemberSearchListReport(0, 0, "", "", _userMenu, organizationId, departmentId, memberTypes, memberStatus, keyword, informationViewList);
                //var results = memberLists.GroupBy(m => m.Id, (key, group) => new { Key = key, Group = group.ToList() });

                //IList<MemberSearchListDto> memberList = _employmentHistoryService.LoadTeamMemberSearchListReport(0, 0, "", "", organizationId, departmentId,
                //     memberTypes, memberStatus, keyword, informationViewList);
                var headerList = new List<string>();
                headerList.Add("");
                var footerList = new List<string>();
                footerList.Add("");

                var columnList = new List<string>();
                foreach (var info in informationViewList)
                {
                    columnList.Add(info);
                }
                var memberExcelList = new List<List<object>>();
                foreach (var teamMember in memberLists)
                {
                    //var teamMember = teamMemberList.Group.OrderByDescending(x => x.PermanentDate).FirstOrDefault();
                    var str = new List<object>();
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case "Organization":
                                    str.Add(teamMember.Organization);
                                    break;
                                case "Branch":
                                    str.Add(teamMember.Branch);
                                    break;
                                case "Campus":
                                    str.Add(teamMember.Campus);
                                    break;
                                case "Pin":
                                    str.Add(teamMember.Pin.ToString());
                                    break;
                                case "FullName":
                                    str.Add(teamMember.FullName);
                                    break;
                                case "NickName":
                                    str.Add(teamMember.NickName);
                                    break;
                                case "Department":
                                    str.Add(teamMember.Department);
                                    break;
                                case "Designation":
                                    str.Add(teamMember.Designation);
                                    break;
                                case "EmploymentStatus":
                                    str.Add(Enum.GetName(typeof(MemberEmploymentStatus), teamMember.EmploymentStatus));
                                    break;
                                case "MentorNamePIN":
                                    str.Add(teamMember.MentorNamePin);
                                    break;
                                case "JoiningDate":
                                    str.Add(teamMember.JoiningDate != null ? teamMember.JoiningDate.Value.ToString("yyyy-MM-dd") : "");
                                    break;
                                case "PermanentDate":
                                    str.Add(teamMember.PermanentDate != null ? teamMember.PermanentDate.Value.ToString("yyyy-MM-dd") : "");
                                    break;
                                case "DiscontinuationDate":
                                    str.Add(teamMember.DiscontinuationDate != null ? teamMember.DiscontinuationDate.Value.ToString("yyyy-MM-dd") : "");
                                    break;
                                case "OfficialContact":
                                    str.Add(teamMember.OfficialContact != "" ? teamMember.OfficialContact : "");
                                    break;
                                case "PersonalContact":
                                    str.Add(teamMember.PersonalContact != "" ? teamMember.PersonalContact : "");
                                    break;
                                case "OfficialEmail":
                                    str.Add(teamMember.OfficialEmail != "" ? teamMember.OfficialEmail : "");
                                    break;
                                case "PersonalEmail":
                                    str.Add(teamMember.PersonalEmail);
                                    break;
                                case "OfficeShift":
                                    str.Add(teamMember.OfficeShift);
                                    break;

                                case "Gender":
                                    var gender = teamMember.Gender != null ? Enum.GetName(typeof(Gender), teamMember.Gender) : "";
                                    str.Add(gender);
                                    break;

                                case "Religion":
                                    var religion = teamMember.Religion != null ? Enum.GetName(typeof(Religion), teamMember.Religion) : "";
                                    str.Add(religion);
                                    break;

                                case "BloodGroup":
                                    var bg = teamMember.BloodGroup != null ? Enum.GetName(typeof(BloodGroup), teamMember.BloodGroup).Replace("pos", "+").Replace("neg", "-") : "";
                                    str.Add(bg);
                                    break;

                                case "DateOfBirth(certificate)":
                                    str.Add(teamMember.DateOfBirthCertificate != null ? teamMember.DateOfBirthCertificate.Value.ToString("yyyy-MM-dd") : "");
                                    break;

                                case "DateOfBirth(Actual)":
                                    str.Add(teamMember.DateOfBirthActual != null ? teamMember.DateOfBirthActual.Value.ToString("yyyy-MM-dd") : "");
                                    break;

                                case "HomeDistrict":
                                    str.Add(teamMember.HomeDistrict);
                                    break;

                                case "Upozilla":
                                    str.Add(teamMember.Upozilla);
                                    break;

                                case "MarriageDate":
                                    str.Add(teamMember.MarriageDate != null ? teamMember.MarriageDate.Value.ToString("yyyy-MM-dd") : "");
                                    break;
                                case "MaritalStatus":
                                    str.Add(teamMember.MaritalStatus != null ? Enum.GetName(typeof(MaritalType), teamMember.MaritalStatus) : "");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    memberExcelList.Add(str);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, memberExcelList, footerList,
                                        "member-info-report__"
                                        + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(HttpNotFound());
            }


        }

        #endregion

        #region Search Member List

        public ActionResult SearchMemberList()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new MultiSelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");
                ViewBag.DepartmentList = new MultiSelectList(new List<Department>(), "Id", "Name");
                ViewBag.DesignationList = new MultiSelectList(new List<Designation>(), "Id", "Name");
                var memberEmploymentStatusList = new SelectList(_commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(), "Key", "Value");
                List<SelectListItem> esList = memberEmploymentStatusList.ToList();
                esList.Insert(0, new SelectListItem() { Value = "0", Text = "All EmployMentList" });
                ViewBag.EmploymentStatusList = new MultiSelectList(esList, "Value", "Text");
                IList<SelectListItem> memberStatus = new List<SelectListItem>
                {
                    new SelectListItem() {Text = "All Member", Value = "0"},
                    new SelectListItem() {Text = "Current Member", Value = "1"},
                    new SelectListItem() {Text = "Past Member", Value = "2"}
                };
                ViewBag.memberStatus = memberStatus;
                ViewBag.InformationViewSelectList = new SelectList(TeamMemberReportConstants.GetTeamMemberSearchMemberListConstants().Select(x => new SelectListItem() { Text = x, Value = x }), "Text", "Value").ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public PartialViewResult SearchMemberList(SearchMemberListViewModel searchMemberListViewModel)
        {
            try
            {
                if (searchMemberListViewModel.InformationViewList == null || !searchMemberListViewModel.InformationViewList.Any())
                {
                    searchMemberListViewModel.InformationViewList = new List<string>
                    {
                        TeamMemberReportConstants.NickName
                        , TeamMemberReportConstants.Pin
                    };
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return PartialView("_partial/_SearchMemberListPartialView", searchMemberListViewModel);
        }

        public ActionResult LoadSearchMemberListDataTable(int draw, int start, int length, long organizationId, List<long> departmentIds, List<long> branchIds, List<long> campusIds, List<int> memberTypes, List<int> memberStatus, string keyword, List<string> informationViewList)
        {
            int recordsTotal = 0;
            int recordsFiltered = 0;
            var data = new List<object>();
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];

                }
                if (informationViewList == null || !informationViewList.Any() || informationViewList.Contains(""))
                {
                    informationViewList = new List<string>
                    {
                        TeamMemberReportConstants.NickName
                        , TeamMemberReportConstants.Pin
                    };
                }
                string orderByColumnName = "";
                if (Convert.ToInt32(orderBy) - 1 >= 0 && informationViewList.Any())
                {
                    orderByColumnName = informationViewList[Convert.ToInt32(orderBy) - 1];
                    if (!String.IsNullOrEmpty(orderByColumnName))
                    {
                        switch (orderByColumnName)
                        {
                            case TeamMemberReportConstants.Organization:
                                orderBy = "aa.Organization";
                                break;
                            case TeamMemberReportConstants.Branch:
                                orderBy = "aa.Branch";
                                break;
                            case TeamMemberReportConstants.Campus:
                                orderBy = "aa.Campus";
                                break;
                            case TeamMemberReportConstants.Pin:
                                orderBy = "aa.Pin";
                                break;
                            case TeamMemberReportConstants.FullName:
                                orderBy = "aa.FullName";
                                break;
                            case TeamMemberReportConstants.NickName:
                                orderBy = "aa.NickName";
                                break;
                            case TeamMemberReportConstants.Department:
                                orderBy = "aa.Department";
                                break;
                            case TeamMemberReportConstants.Designation:
                                orderBy = "aa.Designation";
                                break;
                            case TeamMemberReportConstants.EmploymentStatus:
                                orderBy = "aa.EmploymentStatus";
                                break;
                            case TeamMemberReportConstants.MentorNamePin:
                                orderBy = "aa.MentorNamePin";
                                break;
                            case TeamMemberReportConstants.JoiningDate:
                                orderBy = "aa.JoiningDate";
                                break;
                            case TeamMemberReportConstants.PermanentDate:
                                orderBy = "aa.PermanentDate";
                                break;
                            case TeamMemberReportConstants.DiscontinuationDate:
                                orderBy = "aa.DiscontinuationDate";
                                break;
                            case TeamMemberReportConstants.OfficialContact:
                                orderBy = "aa.OfficialContact";
                                break;
                            case TeamMemberReportConstants.PersonalContact:
                                orderBy = "aa.PersonalContact";
                                break;
                            case TeamMemberReportConstants.OfficialEmail:
                                orderBy = "aa.OfficialEmail";
                                break;
                            case TeamMemberReportConstants.PersonalEmail:
                                orderBy = "aa.PersonalEmail";
                                break;
                            case TeamMemberReportConstants.OfficeShift:
                                orderBy = "aa.OfficeShift";
                                break;
                            case TeamMemberReportConstants.Gender:
                                orderBy = "aa.Gender";
                                break;
                            case TeamMemberReportConstants.Religion:
                                orderBy = "aa.Religion";
                                break;
                            case TeamMemberReportConstants.BloodGroup:
                                orderBy = "aa.BloodGroup";
                                break;
                            case TeamMemberReportConstants.DateOfBirthCertificate:
                                orderBy = "aa.DateOfBirthCertificate";
                                break;
                            case TeamMemberReportConstants.DateOfBirthActual:
                                orderBy = "aa.DateOfBirthActual";
                                break;
                            case TeamMemberReportConstants.HomeDistrict:
                                orderBy = "aa.HomeDistrict";
                                break;
                            case TeamMemberReportConstants.Upozilla:
                                orderBy = "aa.Upozilla";
                                break;
                            case TeamMemberReportConstants.MaritalStatus:
                                orderBy = "aa.MaritalStatus";
                                break;
                            default:
                                orderBy = "aa.Pin";
                                break;
                        }
                    }
                    else
                    {
                        orderBy = "aa.Pin";
                    }
                }
                else
                {
                    orderBy = "aa.Pin";
                }
                #endregion

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                int totalCount = _hrMemberService.GetCountSearchTeammemberList(_userMenu, organizationId, departmentIds, branchIds, campusIds, memberTypes, memberStatus, keyword, informationViewList);
                recordsTotal = totalCount;
                recordsFiltered = recordsTotal;

                IList<MemberSearchListDto> searchMemberList = _hrMemberService.LoadSearchTeammemberList(start, length, orderBy, orderDir, _userMenu, organizationId, departmentIds, branchIds, campusIds, memberTypes, memberStatus, keyword, informationViewList);

                int sl = start + 1;
                foreach (var teamMember in searchMemberList)
                {
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case TeamMemberReportConstants.Organization:
                                    str.Add(teamMember.Organization);
                                    break;
                                case TeamMemberReportConstants.Branch:
                                    str.Add(teamMember.Branch);
                                    break;
                                case TeamMemberReportConstants.Campus:
                                    str.Add(teamMember.Campus);
                                    break;
                                case TeamMemberReportConstants.Pin:
                                    str.Add("<a href='" + Url.Action("SearchOfficialDetails", "TeamMember") + "?memberPin=" + teamMember.Id + "' id='" + teamMember.Id + "'>" + teamMember.Pin.ToString() + "</a>");
                                    break;
                                case TeamMemberReportConstants.FullName:
                                    str.Add(teamMember.FullName);
                                    break;
                                case TeamMemberReportConstants.NickName:
                                    str.Add(teamMember.NickName);
                                    break;
                                case TeamMemberReportConstants.Department:
                                    str.Add(teamMember.Department);
                                    break;
                                case TeamMemberReportConstants.Designation:
                                    str.Add(teamMember.Designation);
                                    break;
                                case TeamMemberReportConstants.EmploymentStatus:
                                    str.Add(_commonHelper.GetEmumIdToValue<MemberEmploymentStatus>(teamMember.EmploymentStatus));
                                    break;
                                case TeamMemberReportConstants.MentorNamePin:
                                    str.Add(teamMember.MentorNamePin);
                                    break;
                                case TeamMemberReportConstants.JoiningDate:
                                    str.Add(teamMember.JoiningDate != null ? teamMember.JoiningDate.Value.ToString("yyyy-MM-dd") : "-");
                                    break;
                                case TeamMemberReportConstants.PermanentDate:
                                    str.Add(teamMember.PermanentDate != null ? teamMember.PermanentDate.Value.ToString("yyyy-MM-dd") : "-");
                                    break;
                                case TeamMemberReportConstants.DiscontinuationDate:
                                    str.Add(teamMember.DiscontinuationDate != null ? teamMember.DiscontinuationDate.Value.ToString("yyyy-MM-dd") : "-");
                                    break;
                                case TeamMemberReportConstants.OfficialContact:
                                    str.Add(teamMember.OfficialContact != "" ? teamMember.OfficialContact : "-");
                                    break;
                                case TeamMemberReportConstants.PersonalContact:
                                    str.Add(teamMember.PersonalContact != "" ? teamMember.PersonalContact : "-");
                                    break;
                                case TeamMemberReportConstants.OfficialEmail:
                                    str.Add(teamMember.OfficialEmail != "" ? teamMember.OfficialEmail : "-");
                                    break;
                                case TeamMemberReportConstants.PersonalEmail:
                                    str.Add(teamMember.PersonalEmail);
                                    break;
                                case TeamMemberReportConstants.OfficeShift:
                                    str.Add(teamMember.OfficeShift);
                                    break;
                                case TeamMemberReportConstants.Gender:
                                    string gender = teamMember.Gender != null ? _commonHelper.GetEmumIdToValue<Gender>((int)teamMember.Gender) : "-";
                                    str.Add(gender);
                                    break;
                                case TeamMemberReportConstants.Religion:
                                    string religion = teamMember.Religion != null ? _commonHelper.GetEmumIdToValue<Religion>((int)teamMember.Religion) : "-";
                                    str.Add(religion);
                                    break;
                                case TeamMemberReportConstants.BloodGroup:
                                    string bg = teamMember.BloodGroup != null ? _commonHelper.GetEmumIdToValue<BloodGroup>((int)teamMember.BloodGroup) : "-";
                                    str.Add(bg);
                                    break;
                                case TeamMemberReportConstants.DateOfBirthCertificate:
                                    str.Add(teamMember.DateOfBirthCertificate != null ? teamMember.DateOfBirthCertificate.Value.ToString("yyyy-MM-dd") : "-");
                                    break;
                                case TeamMemberReportConstants.DateOfBirthActual:
                                    str.Add(teamMember.DateOfBirthActual != null ? teamMember.DateOfBirthActual.Value.ToString("yyyy-MM-dd") : "-");
                                    break;
                                case TeamMemberReportConstants.HomeDistrict:
                                    str.Add(teamMember.HomeDistrict);
                                    break;
                                case TeamMemberReportConstants.Upozilla:
                                    str.Add(teamMember.Upozilla);
                                    break;
                                case TeamMemberReportConstants.MarriageDate:
                                    str.Add(teamMember.MarriageDate != null ? teamMember.MarriageDate.Value.ToString("yyyy-MM-dd") : "");
                                    break;
                                case TeamMemberReportConstants.MaritalStatus:
                                    str.Add(teamMember.MaritalStatus != null ? Enum.GetName(typeof(MaritalType), teamMember.MaritalStatus) : "");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    sl++;
                    data.Add(str);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        public ActionResult ExportSearchMemberListDataTable(long organizationId, List<long> departmentIds, List<long> branchIds, List<long> campusIds, List<int> memberTypes, List<int> memberStatus, string keyword, List<string> informationViewList)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                IList<MemberSearchListDto> searchMemberList = _hrMemberService.LoadSearchTeammemberList(0, int.MaxValue, "aa.Pin", "ASC", _userMenu, organizationId, departmentIds, branchIds, campusIds, memberTypes, memberStatus, keyword, informationViewList);

                var headerList = new List<string>();
                headerList.Add("");
                var footerList = new List<string>();
                footerList.Add("");

                if (informationViewList == null || !informationViewList.Any() || informationViewList.Contains(""))
                {
                    informationViewList = new List<string>
                    {
                        TeamMemberReportConstants.NickName
                        , TeamMemberReportConstants.Pin
                    };
                }

                var columnList = new List<string>();
                foreach (var info in informationViewList)
                {
                    columnList.Add(info);
                }
                var memberExcelList = new List<List<object>>();
                foreach (var teamMember in searchMemberList)
                {
                    var str = new List<object>();
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case TeamMemberReportConstants.Organization:
                                    str.Add(teamMember.Organization);
                                    break;
                                case TeamMemberReportConstants.Branch:
                                    str.Add(teamMember.Branch);
                                    break;
                                case TeamMemberReportConstants.Campus:
                                    str.Add(teamMember.Campus);
                                    break;
                                case TeamMemberReportConstants.Pin:
                                    str.Add(teamMember.Pin.ToString());
                                    break;
                                case TeamMemberReportConstants.FullName:
                                    str.Add(teamMember.FullName);
                                    break;
                                case TeamMemberReportConstants.NickName:
                                    str.Add(teamMember.NickName);
                                    break;
                                case TeamMemberReportConstants.Department:
                                    str.Add(teamMember.Department);
                                    break;
                                case TeamMemberReportConstants.Designation:
                                    str.Add(teamMember.Designation);
                                    break;
                                case TeamMemberReportConstants.EmploymentStatus:
                                    str.Add(_commonHelper.GetEmumIdToValue<MemberEmploymentStatus>(teamMember.EmploymentStatus));
                                    break;
                                case TeamMemberReportConstants.MentorNamePin:
                                    str.Add(teamMember.MentorNamePin);
                                    break;
                                case TeamMemberReportConstants.JoiningDate:
                                    str.Add(teamMember.JoiningDate != null ? teamMember.JoiningDate.Value.ToString("yyyy-MM-dd") : "-");
                                    break;
                                case TeamMemberReportConstants.PermanentDate:
                                    str.Add(teamMember.PermanentDate != null ? teamMember.PermanentDate.Value.ToString("yyyy-MM-dd") : "-");
                                    break;
                                case TeamMemberReportConstants.DiscontinuationDate:
                                    str.Add(teamMember.DiscontinuationDate != null ? teamMember.DiscontinuationDate.Value.ToString("yyyy-MM-dd") : "-");
                                    break;
                                case TeamMemberReportConstants.OfficialContact:
                                    str.Add(teamMember.OfficialContact != "" ? teamMember.OfficialContact : "-");
                                    break;
                                case TeamMemberReportConstants.PersonalContact:
                                    str.Add(teamMember.PersonalContact != "" ? teamMember.PersonalContact : "-");
                                    break;
                                case TeamMemberReportConstants.OfficialEmail:
                                    str.Add(teamMember.OfficialEmail != "" ? teamMember.OfficialEmail : "-");
                                    break;
                                case TeamMemberReportConstants.PersonalEmail:
                                    str.Add(teamMember.PersonalEmail);
                                    break;
                                case TeamMemberReportConstants.OfficeShift:
                                    str.Add(teamMember.OfficeShift);
                                    break;
                                case TeamMemberReportConstants.Gender:
                                    string gender = teamMember.Gender != null ? _commonHelper.GetEmumIdToValue<Gender>((int)teamMember.Gender) : "-";
                                    str.Add(gender);
                                    break;
                                case TeamMemberReportConstants.Religion:
                                    string religion = teamMember.Religion != null ? _commonHelper.GetEmumIdToValue<Religion>((int)teamMember.Religion) : "-";
                                    str.Add(religion);
                                    break;
                                case TeamMemberReportConstants.BloodGroup:
                                    string bg = teamMember.BloodGroup != null ? _commonHelper.GetEmumIdToValue<BloodGroup>((int)teamMember.BloodGroup) : "-";
                                    str.Add(bg);
                                    break;
                                case TeamMemberReportConstants.DateOfBirthCertificate:
                                    str.Add(teamMember.DateOfBirthCertificate != null ? teamMember.DateOfBirthCertificate.Value.ToString("yyyy-MM-dd") : "-");
                                    break;
                                case TeamMemberReportConstants.DateOfBirthActual:
                                    str.Add(teamMember.DateOfBirthActual != null ? teamMember.DateOfBirthActual.Value.ToString("yyyy-MM-dd") : "-");
                                    break;
                                case TeamMemberReportConstants.HomeDistrict:
                                    str.Add(teamMember.HomeDistrict);
                                    break;
                                case TeamMemberReportConstants.Upozilla:
                                    str.Add(teamMember.Upozilla);
                                    break;
                                case TeamMemberReportConstants.MarriageDate:
                                    str.Add(teamMember.MarriageDate != null ? teamMember.MarriageDate.Value.ToString("yyyy-MM-dd") : "");
                                    break;
                                case TeamMemberReportConstants.MaritalStatus:
                                    str.Add(teamMember.MaritalStatus != null ? Enum.GetName(typeof(MaritalType), teamMember.MaritalStatus) : "");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    memberExcelList.Add(str);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, memberExcelList, footerList, "member-info-report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(HttpNotFound());
            }


        }

        #endregion

        #region Member Directory

        public ActionResult MemberDirectory()
        {
            ViewBagForMemberList();
            return View();
        }

        [HttpPost]
        public ActionResult LoadMemberDirectory(int draw, int start, int length, long? organizationId, long? branchId, long? campusId, long? departmentId, string name, int? pin, int? bloodGroup)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Pin";
                            break;
                        case "1":
                            orderBy = "Name";
                            break;
                        default:
                            orderBy = "Pin";
                            break;
                    }

                }
                #endregion
                long totalCount = _hrMemberService.CountTeamMemberDirectory(_userMenu, organizationId, branchId, campusId, departmentId, name, pin, bloodGroup);
                long recordsTotal = totalCount;
                long recordsFiltered = recordsTotal;
                IList<TeamMember> memberList = _hrMemberService.LoadTeamMemberDirectory(start, length, orderBy, orderDir, _userMenu, organizationId, branchId, campusId, departmentId, name, pin, bloodGroup);
                var data = new List<object>();
                //  int sl = start + 1;
                foreach (var teamMember in memberList)
                {
                    var tm = teamMember.EmploymentHistory.Where(x => x.Status == EmploymentHistory.EntityStatus.Active).OrderByDescending(x => x.EffectiveDate).FirstOrDefault();
                    //from BloodGroup bg in Enum.GetValues(typeof(BloodGroup))
                    //          select new { Id = (int)bg, Name = bg.GetDescription().ToString() };
                    string bloodGroupName = "-";
                    if (teamMember.BloodGroup != null)
                    {
                        bloodGroupName = (from BloodGroup bg in Enum.GetValues(typeof(BloodGroup))
                                          where (int)bg == teamMember.BloodGroup
                                          select bg.GetDescription()
                            ).FirstOrDefault();
                    }

                    MemberOfficialDetail memberOfficialDetail = teamMember.MemberOfficialDetails.OrderByDescending(x => x.OfficialContact).FirstOrDefault(x => !string.IsNullOrEmpty(x.OfficialContact));
                    var str = new List<string>();
                    // str.Add(sl.ToString());
                    str.Add(teamMember.Pin.ToString());
                    str.Add(teamMember.Name);
                    str.Add(teamMember.EmploymentHistory != null ? tm.Designation.Name : "");
                    str.Add(teamMember.EmploymentHistory != null ? tm.Department.Name : "");
                    str.Add(memberOfficialDetail != null ? memberOfficialDetail.OfficialContact.ToString() : teamMember.PersonalContact.ToString());
                    str.Add(teamMember.EmploymentHistory != null ? tm.Campus.Branch.Organization.ShortName : "");
                    str.Add(teamMember.EmploymentHistory != null ? tm.Campus.Branch.Name : "");
                    str.Add(teamMember.EmploymentHistory != null ? tm.Campus.Name : "");
                    str.Add(bloodGroupName);
                    str.Add(teamMember.MemberOfficialDetails.Count > 0 ? teamMember.MemberOfficialDetails.FirstOrDefault().OfficialEmail : "");
                    //  sl++;
                    data.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(HttpNotFound());
        }

        #endregion

        #region Ajax functions
        public JsonResult LoadOrganizationDependentFields(List<long> organizationIds = null)
        {
            try
            {
                IList<Department> departmentList = _hrDepartmentService.LoadDepartment(organizationIds);
                var departmentSelectList = new SelectList(departmentList.ToList(), "Id", "Name");
                IList<Designation> designationList = _hrDesignationService.LoadDesignation(organizationIds);
                var designationSelectList = new SelectList(designationList.ToList(), "Id", "Name");
                IList<Shift> shiftList = _shiftService.LoadShift(organizationIds);
                var shiftSelectList = new SelectList(shiftList.ToList(), "Id", "Name");
                return Json(new { returnDepartmentList = departmentSelectList, returnDesignationList = designationSelectList, returnShifttList = shiftSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred During Loading";
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public JsonResult LoadShift(List<long> organizationIds)
        {
            try
            {
                IList<Shift> shiftList = _shiftService.LoadShift(organizationIds);
                var shiftSelectList = new SelectList(shiftList.ToList(), "Id", "FormattedShiftTime");
                return Json(new { returnShifttList = shiftSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred During Loading";
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public JsonResult GetMentorNameByPin(int mentorPin, string joiningDate = "", bool isJoiningDateRequired = true)
        {
            try
            {
                TeamMember mentor = null;
                if (joiningDate != "" && isJoiningDateRequired)
                {
                    mentor = _hrMemberService.GetByPin(mentorPin, Convert.ToDateTime(joiningDate).Date);
                }
                else if (isJoiningDateRequired == false)
                {
                    mentor = _hrMemberService.GetByPin(mentorPin);
                }
                else
                {
                    // mentor = _hrMemberService.GetByPin(mentorPin);    
                    return Json(new Response(false, "Please select Joining date first"));
                }

                if (mentor == null) { return Json(new Response(false, "Mentor Not Found")); }
                else
                {
                    return Json(new { mentorName = mentor.Name, IsSuccess = true });
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred During Loading";
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public JsonResult LoadTeamMember(long organizationId, long branchId, long campusId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var teamMemberList = _hrMemberService.LoadHrAuthorizedTeamMember(userMenu, DateTime.Today, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(branchId), _commonHelper.ConvertIdToList(campusId));
                //var teamMemberSelectList = new SelectList(teamMemberList.ToList(), "Id", "FullNameEng");
                IList<dynamic> teamMemberCustomizedList = new List<dynamic>();
                foreach (var member in teamMemberList)
                {
                    var memberInfo = new
                    {
                        Id = member.Id,
                        Name = (member.Pin + "-" + member.FullNameEng).Trim() + " (" + member.Name + ")"
                    };
                    teamMemberCustomizedList.Add(memberInfo);
                }
                var teamMemberSelectList = new SelectList(teamMemberCustomizedList.ToList(), "Id", "Name");

                return Json(new { returnTeamMemberList = teamMemberSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Program Occurred During Load Team Member";
                _logger.Error(ex);
                return Json(new Response(false, ViewBag.ErrorMessage));
            }
        }

        [HttpPost]
        public JsonResult LoadUser(long organizationId, long branchId, long campusId, long teamMemberId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var teamMember = _hrMemberService.LoadById(teamMemberId);
                var allUsers = _userService.LoadAuthorizedUser(userMenu, null, _commonHelper.ConvertIdToList(organizationId), null, _commonHelper.ConvertIdToList(branchId), _commonHelper.ConvertIdToList(campusId));
                var userSelectList = new SelectList(allUsers.ToList(), "Id", "AspNetUser.Email", (teamMember.UserProfile != null) ? teamMember.UserProfile.AspNetUser.Id : 0);
                return Json(new { returnUserList = userSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Program Occurred During Load User";
                _logger.Error(ex);
                return Json(new Response(false, ViewBag.ErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult GetInstituteByNameLike(string query)
        {
            try
            {
                var dataList = _institutionService.GetInstituteByNameLike(query);
                if (dataList != null && dataList.Count > 0)
                {
                    SelectList list = new SelectList(dataList, "Id", "Name");
                    List<SelectListItem> instituteList = list.ToList();
                    return Json(new { returnList = instituteList, IsSuccess = true });
                }
                return Json(new Response(false, "Institute Not Found!!"));
            }
            catch (Exception e)
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #region helper

        private void ViewBagForMemberList()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
            ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");

            var memberTypes = Enum.GetValues(typeof(MemberEmploymentStatus)).Cast<MemberEmploymentStatus>().ToList();
            ViewBag.MemberTypes = memberTypes.Select((r, index) => new SelectListItem { Text = r.ToString(), Value = (index + 1).ToString() });
            var bloodGroups = from BloodGroup bg in Enum.GetValues(typeof(BloodGroup))
                              select new { Id = (int)bg, Name = bg.GetDescription().ToString() };
            ViewBag.BloodGroupList = new SelectList(bloodGroups, "Id", "Name");

            IList<SelectListItem> memberStatus = new List<SelectListItem>
            {
                new SelectListItem(){Text = "Current Member",Value = "1"},
                new SelectListItem(){Text = "Past Member",Value = "2"}
            };
            ViewBag.memberStatus = memberStatus;
        }

        private void ViewBagForAcademicInfoEdit()
        {
            ViewBag.IsAcademicInfoFormValid = true;

            var examLabels = _hrExamService.LoadExamList();
            var boardList = _hrBoardService.LoadBoard();

            if (examLabels != null && examLabels.Count > 0 && boardList != null && boardList.Count > 0)
            {
                ViewBag.ExamLabel = examLabels;
                ViewBag.Board = boardList;
                var examStatus = from HrAcademicExamStatus bg in Enum.GetValues(typeof(HrAcademicExamStatus))
                                 select new { Id = (int)bg, Name = bg.GetDescription().ToString(CultureInfo.CurrentCulture) };
                ViewBag.ExamStatusList = examStatus;

                var resultType = from HrAcademicResult bg in Enum.GetValues(typeof(HrAcademicResult))
                                 select new { Id = (int)bg, Name = bg.GetDescription().ToString(CultureInfo.CurrentCulture) };
                ViewBag.ResultList = resultType;
                var gradeScale = from HrGradeScale bg in Enum.GetValues(typeof(HrGradeScale))
                                 select new { Id = (int)bg, Name = bg.GetDescription().ToString(CultureInfo.CurrentCulture) };
                ViewBag.GradeScaleList = gradeScale;
            }
            else
            {
                ViewBag.IsAcademicInfoFormValid = false;
            }
        }

        private void ViewBagForTrainingInfoEdit(IList<TrainingInfo> trainingInfo)
        {
            ViewBag.TrainingInfo = null;
            if (trainingInfo != null && trainingInfo.Count > 0)
            {
                ViewBag.TrainingInfo = trainingInfo;
            }
        }

        private void ViewBagForTmJobExpInfo(IList<JobExperience> jobExpList)
        {
            ViewBag.ProfessionalQInfo = null;
            if (jobExpList != null && jobExpList.Count > 0)
            {
                ViewBag.JobExpInfo = jobExpList;
            }
            var companyType = Enum.GetValues(typeof(HrCompanyType)).Cast<HrCompanyType>().ToList();
            ViewBag.CompanyType = companyType.Select((r, index) => new SelectListItem { Text = r.ToString(), Value = (index + 1).ToString(CultureInfo.CurrentCulture) });
        }

        private void AuthorizedTeamMemberPin(int memberPin)
        {
            var memberIdList = _hrMemberService.LoadHrAuthorizedTeamMemberId(_userMenu, DateTime.Now.Date, pinList: new List<int>() { memberPin });
            if (memberIdList == null || memberIdList.Count == 0)
            {
                throw new DataNotFoundException("Member is not found/authorized.");
            }
        }

        #endregion
    }
}