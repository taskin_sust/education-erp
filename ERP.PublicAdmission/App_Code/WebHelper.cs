﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace UdvashERP.PublicAdmission 
{
    public static class WebHelper
    {
        private const int RequestTimeout = 60*1000;

        public static string CommonErrorMessage
        {
            get { return "Something went wrong"; }
        }

        internal static string MakeFirstCharUpper(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                char firstChar = name[0];
                firstChar = Char.ToUpper(firstChar);
                name = name.Remove(0, 1);
                return firstChar + name;
            }
            return name;
        }


        public static string GetRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.KeepAlive = false;
            request.Timeout = RequestTimeout;

            WebResponse response = request.GetResponse();
            using (Stream receiveStream = response.GetResponseStream())
            {
                if (receiveStream != null)
                {
                    StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    var responseString = readStream.ReadToEnd();
                    return responseString;
                }
            }
            response.Close();
            return string.Empty;
        }

        public static string PostRequest(string url, string data)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.KeepAlive = false;
            request.Timeout = RequestTimeout;

            byte[] byteArray = Encoding.UTF8.GetBytes(data);
            //request.ContentType = "application/x-www-form-urlencoded";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            request.ContentLength = byteArray.Length;

            using (Stream dataStream1 = request.GetRequestStream())
            {
                dataStream1.Write(byteArray, 0, byteArray.Length);
                dataStream1.Close();
            }

            WebResponse response = request.GetResponse();

            using (Stream receiveStream = response.GetResponseStream())
            {
                if (receiveStream != null)
                {
                    StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    var responseString = readStream.ReadToEnd();
                    return responseString;
                }
            }
            response.Close();
            return string.Empty;
        }

        //public static string GetResult(string url)
        //{
        //    try
        //    {
        //        var requestUrl = (HttpWebRequest)WebRequest.Create(url);
        //        //request request = new request();
        //        //request.user = data.Get(0);
        //        //request.pass = data.Get(1);
        //        //request.msisdn = data.Get(2);
        //        //request.trxid = transicationId;
        //        //var js = new JavaScriptSerializer();
        //        //var format = js.Serialize(request);

        //        //var formateddata = Encoding.ASCII.GetBytes(format);



        //        requestUrl.ContentType = "application/json";
        //        requestUrl.Accept = "application/json";
        //        //requestUrl.ContentLength = formateddata.Length;
        //        requestUrl.Method = "GET";

        //        //using (var stream = requestUrl.GetRequestStream())
        //        //{
        //        //    stream.Write(formateddata, 0, formateddata.Length);

        //        //}

        //        var response = (HttpWebResponse)requestUrl.GetResponse();

        //        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        //        response.Close();


        //        return responseString;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
    }
    
    public static class APIInformation 
    {
        public static string UrlLink()
        {
            return "http://localhost/";
        }
    }
  
}