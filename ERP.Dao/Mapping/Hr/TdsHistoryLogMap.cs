using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {


    public partial class TdsHistoryLogMap : ClassMap<TdsHistoryLog>
    {

        public TdsHistoryLogMap()
        {
			Table("HR_TdsHistoryLog");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			References(x => x.TdsHistory).Column("TdsHistoryId");
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
            Map(x => x.TdsAmount).Column("TdsAmount");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
        }
    }
}
