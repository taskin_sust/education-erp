using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class GoodsReturnDetailsFactory : ObjectFactoryBase<GoodsReturnDetails>
    {

        private readonly IGoodsReturnDetailService _goodsReturnDetailService;

        public GoodsReturnDetailsFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _goodsReturnDetailService = new GoodsReturnDetailsService(session);

        }

        #region Create

        public GoodsReturnDetailsFactory Create(Item item, GoodsReturn goodsReturn)
        {
            Object = new GoodsReturnDetails
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = "",
                CreateBy = 1,
                ModifyBy = 1,
                Status = WorkOrder.EntityStatus.Active,
                ReturnedQuantity = 1,
                ReturnStatus = (int)GoodsReturnStatus.Returned,
                Item = item,
                GoodsReturn = goodsReturn
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsReturnDetailsFactory CreateMore(Item item, int num)
        {
            CreateGoodsReturnDetailsList(item, num);
            return this;
        }

        private GoodsReturnDetailsFactory CreateGoodsReturnDetailsList(Item item, int num)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<GoodsReturnDetails>();
            for (int i = 0; i < num; i++)
            {
                var obj = new GoodsReturnDetails()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = "",
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = WorkOrder.EntityStatus.Active,
                    ReturnedQuantity = 10,
                    ReturnStatus = (int)GoodsReturnStatus.Returned,
                    Item = item
                };
                list.Add(obj);
            }
            ObjectList.AddRange(list);
            return this;
        }

        public GoodsReturnDetailsFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _goodsReturnDetailService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_goodsReturnDetailService.SaveOrUpdate(x); });

            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public GoodsReturnDetailsFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion

    }
}
