﻿using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface ISubjectDao : IBaseDao<Subject, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
       
        #endregion

        #region List Loading Function
        IList<Subject> LoadSubject();
        IList<Subject> LoadSubject(int start, int length, string orderBy, string orderDir, string name, string shortName, string rank, string status);      
        #endregion

        #region Others Function
        bool CheckDuplicateSubject(out string fieldName, string name = null, string code = null, long id = 0);

        int GetSubjectCount(string name, string shortName, string rank, string status);
        #endregion
    }
    public class SubjectDao : BaseDao<Subject, long>, ISubjectDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<Subject> LoadSubject()
        {
            ICriteria criteria = Session.CreateCriteria(typeof(Subject));
            criteria.Add(Restrictions.Eq("Status", Subject.EntityStatus.Active));
            return criteria.List<Subject>();
        }
        
        public IList<Subject> LoadSubject(int start, int length, string orderBy, string orderDir, string name, string shortName, string rank, string status)
        {
            ICriteria criteria = Session.CreateCriteria<Subject>()
                        .Add(Restrictions.Not(Restrictions.Eq("Status", Subject.EntityStatus.Delete)));
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(shortName))
            {
                criteria.Add(Restrictions.Like("ShortName", shortName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!string.IsNullOrEmpty(orderBy.Trim()))
            {
                if (orderDir == "ASC")
                {
                    criteria.AddOrder(Order.Asc(orderBy));
                }
                else
                {
                    criteria.AddOrder(Order.Desc(orderBy));
                } 
            }
            return (List<Subject>)criteria.SetFirstResult(start).SetMaxResults(length).List<Subject>();
        }

        #endregion

        #region Others Function

        public bool CheckDuplicateSubject(out string fieldName, string name = null, string code = null, long id = 0)
        {
            fieldName = "";

            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<Subject>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Subject.EntityStatus.Delete)));
            if (id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (name != null)
            {
                criteria.Add(Restrictions.Eq("Name", name));
                fieldName = "Name";
            }
            else if (code != null)
            {
                criteria.Add(Restrictions.Eq("ShortName", code));
                fieldName = "ShortName";
            }
            var subjectList = criteria.List<Subject>();
            if (subjectList != null && subjectList.Count > 0)
            {

                checkValue = true;
            }
            return checkValue;
        }


        public int GetSubjectCount(string name, string shortName, string rank, string status)
        {
            ICriteria criteria = Session.CreateCriteria<Subject>()
                .Add(Restrictions.Not(Restrictions.Eq("Status", Subject.EntityStatus.Delete)));
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(shortName))
            {
                criteria.Add(Restrictions.Like("ShortName", shortName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }
            IList list = criteria.List();
            return list.Count;
        }
        #endregion
    }
}