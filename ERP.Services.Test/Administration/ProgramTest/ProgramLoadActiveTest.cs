﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ProgramTest
{
    interface IProgramLoadActiveTest
    {
        #region Basic Test
       
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Program")]
    public class ProgramLoadActiveTest : ProgramBaseTest, IProgramLoadActiveTest
    {
        #region Basic Test
        
        [Fact]
        public void LoadActive_should_return_list()
        {
            _programFactory.CreateMore(50).Persist();
            IList<Program> programList = _programService.LoadProgram(0, 50, "", "", _programFactory.GetLastCreatedObjectList()[0].Organization.Id.ToString(), "", "", "", "", "", "");
            Assert.Equal(50, programList.Count);
        }

        [Fact]
        public void LoadActive_should_Not_return_list()
        {
            IList<Program> programList = _programService.LoadProgram(0, 100, "", "", "-1", "", "", "", "1", "1", "");
            Assert.Equal(0, programList.Count);
        } 
        #endregion
    }
}
