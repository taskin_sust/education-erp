﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.OverTimeAllowanceSettingTest
{
    public class OverTimeAllowanceSettingBaseTest : TestBase, IDisposable
    {
        #region Init
        private ISession _session;
        protected readonly CommonHelper _commonHelper;
        protected readonly OvertimeAllowanceSettingService _overtimeAllowanceSettingService;
        protected internal readonly IAllowanceSheetService _allowanceSheetService;
        protected internal readonly IChildrenAllowanceSettingService _childrenAllowanceSettingService;
        protected readonly OrganizationFactory _organizationFactory;
        protected readonly OvertimeAllowanceSettingFactory _overtimeAllowanceSettingFactory;
        protected readonly ChildrenAllowanceSettingFactory _childrenAllowanceSettingFactory;
        internal readonly string _name = "SettingName_" + DateTime.Now.Second + "_" + DateTime.Now.Millisecond;
        #endregion

        public OverTimeAllowanceSettingBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _allowanceSheetService = new AllowanceSheetService(Session);
            _overtimeAllowanceSettingService = new OvertimeAllowanceSettingService(_session);
            _overtimeAllowanceSettingFactory = new OvertimeAllowanceSettingFactory(null, _session);
            _organizationFactory = new OrganizationFactory(null, _session);
            _childrenAllowanceSettingFactory = new ChildrenAllowanceSettingFactory(null, _session);
            _childrenAllowanceSettingService = new ChildrenAllowanceSettingService(Session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        #region disposal

        public void Dispose()
        {
            _overtimeAllowanceSettingFactory.CleanUp();
            _organizationFactory.Cleanup();
            base.Dispose();
        }

        #endregion
    }
}
