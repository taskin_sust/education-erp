﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public partial class TeacherImagesMap:BaseClassMap<TeacherImages, long>
    {
        public TeacherImagesMap()
        {
            Table("TeacherImages");
            LazyLoad();
            Map(x => x.Hash);
            //Map(x => x.ImageIGuid);
            //Map(x => x.Images).Column("Images").CustomSqlType("VARBINARY (MAX) FILESTREAM").Length(2147483647).LazyLoad();
           
            References(x => x.Teacher).Column("TeacherId");
        }
    }
}
