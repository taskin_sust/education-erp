﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.Hr;
using ZXing;
using ZXing.Common;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Services.Helper
{
    public delegate IList<string> AuthorizeBranchNameDelegate(List<UserMenu> userMenu, List<long> organizationIdList, List<long> branchIds);
    public interface ICommonHelper
    {
        //Dictionary<string, double> GetMultiplyingFactor();
        Dictionary<string, string> GetCalculationOn();
        Dictionary<string, int> GetStatus();
        String GetSatus(string statusIntValue);
        List<T> ConvertIdToList<T>(T value);
        DateTime GetDateTimeFromString(string dateTimeString, int outputTye = (int)DateTimeType.FullDateTime);
        Dictionary<int, string> LoadEmumToDictionary<T>(List<int> excludeList = null);
        List<int> LoadSearchResultEmumToDictionaryForKeyWord<T>(string keywords = "", List<int> excludeList = null);
        List<int> ConvertEnumToIdList<T>();
        bool ContainsEnumId<T>(int enumId);
        string GetEmumIdToValue<T>(int enumId);
        string ExtractCountryCodeFromMobileNumber(string mobileNumber);
        string AddCountryCodeWithMobileNumber(string mobileNumber);
        bool CheckMobileNumberValidation(IList<string> mobileNumberList);
        string CheckMobileNumber(string mobile);
        //string GetReportCommonDateTimeFormat(DateTime? givenDateTime, bool showTimePart = true, string customFormat = "" );
        Dictionary<decimal, string> LoadStructToDictinary<T>();


        string GetBarCodeBase64Data(string content = "", int width = 30, int height = 4);
    }
    public class CommonHelper : ICommonHelper
    {
        public Dictionary<int, string> LoadEmumToDictionary<T>(List<int> excludeList = null)
        {
            try
            {
                var enumType = typeof(T);
                Dictionary<int, string> enumToDictionary = new Dictionary<int, string>();
                var enumAllItems = Enum.GetValues(enumType);
                foreach (var currentItem in enumAllItems)
                {
                    if ((excludeList != null && !excludeList.Contains((int)currentItem)) || (excludeList == null))
                    {
                        DescriptionAttribute[] allDescAttributes = (DescriptionAttribute[])enumType.GetField(currentItem.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
                        string description = allDescAttributes.Length > 0
                            ? allDescAttributes[0].Description
                            : currentItem.ToString();

                        enumToDictionary.Add((int)currentItem, description);
                    }
                }
                return enumToDictionary;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public Dictionary<decimal, string> LoadStructToDictinary<T>()
        {
            try
            {
                var structType = typeof(T);
                Dictionary<decimal, string> structToDictionary = new Dictionary<decimal, string>();
                var structList = structType.GetFields();
                foreach (var currentItem in structList)
                {
                    object ss = currentItem.GetRawConstantValue();
                    decimal yy = Convert.ToDecimal(ss);
                    DescriptionAttribute[] allDescAttributes = (DescriptionAttribute[])structType.GetField(currentItem.Name.ToString())
                        .GetCustomAttributes(typeof(DescriptionAttribute), false);
                    string description = allDescAttributes.Length > 0
                            ? allDescAttributes[0].Description
                            : currentItem.Name.ToString();
                    structToDictionary.Add(yy, description);
                }
                return structToDictionary;
            }
            catch (Exception)
            {
                throw;
            }
        }

       


        public List<int> LoadSearchResultEmumToDictionaryForKeyWord<T>(string keywords = "", List<int> excludeList = null)
        {
            try
            {
                List<int> enumValueList = new List<int>();
                Dictionary<int, string> enumToDictionary = LoadEmumToDictionary<T>(excludeList);
                if (enumToDictionary.Any())
                {
                    enumValueList = !String.IsNullOrEmpty(keywords) ? enumToDictionary.Where(x => x.Value.ToLower().Contains(keywords.ToLower())).Select(x => x.Key).ToList() : enumToDictionary.Select(x => x.Key).ToList();
                }
                return enumValueList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetEmumIdToValue<T>(int enumId)
        {
            try
            {
                string value = "-";
                Dictionary<int, string> enumToDictionary = LoadEmumToDictionary<T>();
                if (enumToDictionary.Any())
                {
                    value = enumToDictionary.Where(x => x.Key == enumId).Select(x => x.Value).Take(1).SingleOrDefault();
                }
                return value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<int> ConvertEnumToIdList<T>()
        {
            try
            {
                List<int> idlist = new List<int>(Enum.GetValues(typeof(T)).Cast<int>());
                return idlist;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool ContainsEnumId<T>(int enumId)
        {
            try
            {
                List<int> idlist = ConvertEnumToIdList<T>();
                return idlist.Contains(enumId);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Dictionary<string, string> GetCalculationOn()
        {
            try
            {
                var status = new Dictionary<string, string>
               {
                   {"1", "1"},
                   {"1.5", "1.5"},
                   {"2", "2"},
                   {"2.5", "2.5"},
                   {"3", "3"}
               };
                return status;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public Dictionary<string, int> GetStatus()
        {
            try
            {
                var status = new Dictionary<string, int>
               {
                   {"Active", BaseEntity<Course>.EntityStatus.Active},
                   {"Inactive", BaseEntity<Course>.EntityStatus.Inactive}
               };
                return status;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public String GetSatus(string statusIntValue)
        {
            if (!String.IsNullOrEmpty(statusIntValue))
            {
                if (statusIntValue.Equals("1"))
                {
                    return "Active";
                }
                return "Inactive";
            }
            return "";
        }

        public class BoardSubjectResult
        {
            public string Code { get; set; }
            public string Subject { get; set; }
            public string Grade { get; set; }
        }

        /// <summary>
        /// convert id to ids
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public List<T> ConvertIdToList<T>(T value)
        {
            List<T> ids = new List<T>();
            ids.Add(value);
            return ids;
        }

        public static List<T> ConvertToList<T>(T value)
        {
            List<T> list = new List<T>();
            if (value != null)
                list.Add(value);
            return list;
        }

        public static List<long> ConvertSelectedAllIdList(List<long> idList)
        {
            return (idList != null) ? ((idList.Contains(SelectionType.SelelectAll)) ? null : idList) : null;
        }

        public DateTime GetDateTimeFromString(string dateTimeString, int outputTye = (int) DateTimeType.FullDateTime)
        {
            try
            {
                return Convert.ToDateTime(dateTimeString);
            }
            catch (InvalidDataException ex)
            {

                throw ex;
            }

        }

        public string AddCountryCodeWithMobileNumber(string mobileNumber)
        {
            try
            {
                if (!mobileNumber.StartsWith("88") && mobileNumber.Length == 11)
                {
                    mobileNumber = "88" + mobileNumber;
                }
                return mobileNumber;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string ExtractCountryCodeFromMobileNumber(string mobileNumber)
        {
            try
            {
                if (mobileNumber.StartsWith("88") && mobileNumber.Length == 13)
                {
                    mobileNumber = mobileNumber.Substring(2);
                }
                return mobileNumber;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool CheckMobileNumberValidation(IList<string> mobileNumberList)
        {
            bool isValid = true;
            try
            {
                var rgx = new Regex(@"^(01([1]|[5-9])\d{8})$");
                foreach (string mobileNumber in mobileNumberList)
                {
                    var text = ExtractCountryCodeFromMobileNumber(mobileNumber);
                    isValid = rgx.IsMatch(text);
                    if (!isValid)
                    {
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return isValid;
        }

        public string CheckMobileNumber(string mobile)
        {
            if (mobile != null)
            {
                var regex = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
                var match = regex.Match(mobile);
                if (match.Success == true)
                {
                    switch (mobile.Length)
                    {
                        case 14:
                            mobile = mobile.Substring(3);
                            break;
                        case 13:
                            mobile = mobile.Substring(2);
                            break;
                        case 10:
                            mobile = "0" + mobile;
                            break;
                    }
                }
                else { mobile = null; }
            }
            return mobile;
        }

        public static string GetReportCommonDateTimeFormat(DateTime? givenDateTime, bool showTimePart = true, string customFormat = "")
        {
            string dateTimeString = "-";

            if (givenDateTime != null)
            {
                dateTimeString = String.IsNullOrEmpty(customFormat) ? givenDateTime.Value.ToString(showTimePart ? "dd, MMM yyyy hh:mm tt" : "dd, MMM yyyy") : givenDateTime.Value.ToString(customFormat);
            }
            return dateTimeString;

        }

        #region Board Result

        public static string EducationResult(string url, string refUrl, string roll, string reg, string board, string exam, string year)
        {
            //string url = "http://archive.educationboard.gov.bd/result.php";
            //string refUrl = "http://archive.educationboard.gov.bd/index.php";
            //string url = "http://www.educationboardresults.gov.bd/regular/result.php";
            //string refUrl = "http://www.educationboardresults.gov.bd/regular/index.php";
            //string url = UdvashERP.Properties.Settings.Default.UdvashERP_board_info_sync_url;
            //string refUrl = UdvashERP.Properties.Settings.Default.UdvashERP_board_info_sync_ref_url;
            var summationOfCaptcha = 0;
            var cookieCon = new CookieContainer();
            var z = GetHtml(refUrl, "", "", cookieCon);
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(z);
            if (htmlDoc.DocumentNode != null)
            {
                /*--------------------------------Quick Fixing Summation of Captcha-------------------------------------------*/
                if (htmlDoc.DocumentNode.SelectNodes("//table") != null)
                {
                    foreach (HtmlNode nodeDoc in htmlDoc.DocumentNode.SelectNodes("//table"))
                    {
                        //var v = nodeDoc.Descendants().Where(x => x.Name == "td").ToList();
                        foreach (var nd in nodeDoc.Descendants().Where(x => x.Attributes.Contains("class") && x.Attributes["class"].Value.Contains("black12bold")))
                        {
                            int counter = 0;
                            foreach (var tr in nd.Descendants().Where(x => x.Name == "tr"))
                            {
                                counter++;
                                int counterTd = 0;
                                if (counter == 7)
                                    foreach (var td in tr.Descendants().Where(x => x.Name == "td"))
                                    {
                                        counterTd++;
                                        if (counterTd == 2)
                                        {
                                            var numericCaptcha = td.InnerText;
                                            var summationOfCaptchaSplit = numericCaptcha.Split('+');
                                            foreach (var val in summationOfCaptchaSplit)
                                            {
                                                summationOfCaptcha = summationOfCaptcha + Convert.ToInt32(val);
                                            }

                                            break;
                                        }
                                    }
                            }
                        }
                        break;
                    }

                }
            }



            string sendParam = "sr=3&et=0";
            sendParam += "&exam=" + exam;
            sendParam += "&year=" + year;
            sendParam += "&board=" + board;
            sendParam += "&roll=" + roll;
            sendParam += "&reg=" + reg;
            sendParam += "&value_s=" + summationOfCaptcha;
            sendParam += "&button2=Submit";
            return GetHtml(url, refUrl, sendParam, cookieCon);
            //return new ContentResult() { Content = html };
            // return ContentResult(){Content=html)}
            //return Content(html);

        }

        private static string GetHtml(string url, string refUrl, string param, CookieContainer cookieCon)
        {
            string responseHtml = "";
            try
            {
                byte[] data = new ASCIIEncoding().GetBytes(param);
                var webRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
                webRequest.CookieContainer = cookieCon;
                webRequest.Method = "POST";
                webRequest.Referer = refUrl;
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = data.Length;

                Stream myStream = webRequest.GetRequestStream();
                myStream.Write(data, 0, data.Length);

                var webResponse = webRequest.GetResponse();
                var responseStream = webResponse.GetResponseStream();
                StreamReader sr = new StreamReader(responseStream);
                responseHtml = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                responseHtml = ex.ToString();
            }
            return responseHtml;
        }

        #endregion

        public static string DecimalToWords(decimal number)
        {
            string words = "";

            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + DecimalToWords(Math.Abs(number));

            int intPortion = (int)number;
            decimal fraction = (number - intPortion) * 100;
            int decPortion = (int)fraction;

            words = NumberToWords(intPortion);
            if (decPortion > 0)
            {
                words += " Taka and ";
                words += NumberToWords(decPortion);
                words += " Paisa";
            }
            else
            {
                words += " Taka";
            }
            return words + " Only. ";

        }

        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " Million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                //if (words != "")
                //    words += "and ";

                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += " " + unitsMap[number % 10];
                }
            }

            return words;
        }


        public string GetBarCodeBase64Data(string content = "", int width=30, int height=30)
        {


            var options = new EncodingOptions { Margin = 0, Width = width, Height = height };
            IBarcodeWriter writer = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = options,
            };


            var bitmap = writer.Write(content);
            var ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Gif);

            var base64Data = Convert.ToBase64String(ms.ToArray());
            return base64Data;
        }

    }
}
