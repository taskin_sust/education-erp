﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentNHibernate.Conventions;
using iTextSharp.text;
using log4net;
using NHibernate;
using NHibernate.Mapping;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class LeaveReportController : Controller
    {
        // GET: Hr/LeaveReport
        private ILog _logger = LogManager.GetLogger("HrArea");
        const string DropDownOptionView = "Partial/_leaveDetails";
        private readonly ILeaveService _hrLeaveService;
        private readonly IOrganizationService _organizationService;
        private ISession _session;
        private readonly ICommonHelper _commonHelper;
        private readonly ILeaveApplicationService _hrLeaveApplicationService;
        private readonly ITeamMemberService _hrMemberService;
        private readonly IMembersLeaveSummaryService _hrMembersLeaveSummaryService;
        private readonly IEmploymentHistoryService _employmentHistoryService;
        private readonly IMaritalInfoService _hrMaritalInfoService;
        private readonly IUserService _userService;
        private readonly IBranchService _branchService;
        private List<UserMenu> _userMenu;

        public LeaveReportController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _hrLeaveService = new LeaveService(_session);
            _organizationService = new OrganizationService(_session);
            _hrLeaveApplicationService = new LeaveApplicationService(_session);
            _hrMemberService = new TeamMemberService(_session);
            _hrMembersLeaveSummaryService = new MembersLeaveSummaryService(_session);
            _employmentHistoryService = new EmploymentHistoryService(_session);
            _hrMaritalInfoService = new MaritalInfoService(_session);
            _userService = new UserService(_session);
            _commonHelper = new CommonHelper();
            _branchService = new BranchService(_session);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HrLeaveReport()
        {
            try
            {
                ViewBag.MemberLeaveSummariesForReport = new List<MemberLeaveSummary>();
                LeaveReportViewbag();
                ViewBag.isSubmitted = false;
                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw e;
            }
        }

        [HttpPost]
        public ActionResult HrLeaveReport(LeaveReportEntryViewModel reportEntryViewModel, string year = "")
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (reportEntryViewModel.Pin == null)
                    throw new Exception("Enter PIN Number");
                var member = _hrMemberService.GetByPin(Convert.ToInt32(reportEntryViewModel.Pin.Trim()));
                if (member==null)
                {
                    throw new InvalidDataException("Invalid Pin.");
                }
                var hrEmploymentHistory = member.EmploymentHistory.Where(x => x.EffectiveDate <= DateTime.Now && x.Status != EmploymentHistory.EntityStatus.Delete)
                                                    .OrderByDescending(x => x.EffectiveDate)
                                                    .Take(1)
                                                    .FirstOrDefault();
                    ViewBag.memberOrg = "-";
                    ViewBag.memberDept = "-";
                    ViewBag.memberDesig = "-";
                if (hrEmploymentHistory != null)
                {
                    ViewBag.memberOrg = hrEmploymentHistory.Department.Organization.Name;
                    ViewBag.memberDept = hrEmploymentHistory.Department.Name;
                    ViewBag.memberDesig = hrEmploymentHistory.Designation.Name;
                }
                ViewBag.memberName = member.FullNameEng + " (" + member.Name + ")";
                if (member == null)
                {
                    throw new InvalidDataException("Member of this pin is not found.");
                }
                bool authorizedHrTeamMember = _hrMemberService.CheckHrAuthorizedTeamMember(_userMenu, member.Pin);
                if (!authorizedHrTeamMember)
                {
                    throw new InvalidDataException("You are not authorized to access this TeamMember Information.");
                }
                IList<MemberLeaveSummary> memberLeaveSummaries = _hrMembersLeaveSummaryService.LoadByMemberAndYear(member.Id, Convert.ToInt32(year));

                IList<MemberLeaveSummary> list = new List<MemberLeaveSummary>();
                foreach (var memberLeaveSummary in memberLeaveSummaries)
                {

                    // memberLeaveSummary.AvailableBalance += memberLeaveSummary.PendingLeave + memberLeaveSummary.ApprovedLeave;
                    list.Add(memberLeaveSummary);
                }
                ViewBag.Pin = reportEntryViewModel.Pin;
                var yearList = new List<SelectListItem>();
                for (int i = DateTime.Now.Year + 1; i > 2013; i--)
                {
                    yearList.Add(i == Convert.ToInt32(year)
                        ? new SelectListItem() { Text = i.ToString(), Value = i.ToString(), Selected = true }
                        : new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
                }
                ViewBag.Years = yearList;

                //return PartialView("PersonLeaveReport",memberLeaveSummaries);
                ViewBag.MemberLeaveSummariesForReport = list;
                ViewBag.isSubmitted = true;
            }

            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                ViewBag.MemberLeaveSummariesForReport = new List<MemberLeaveSummary>();
                ViewBag.isSubmitted = false;
                //throw e;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                ViewBag.ErrorMessage = e.Message;
                ViewBag.MemberLeaveSummariesForReport = new List<MemberLeaveSummary>();
                ViewBag.isSubmitted = false;
                //throw e;
            }
            LeaveReportViewbag();

            return View();
        }

        private void LeaveReportViewbag()
        {
            var yearList = new List<SelectListItem>();

            int year = DateTime.Now.Year;
            for (; year >= 2015; year--)
            {
                yearList.Add(new SelectListItem() { Text = year.ToString(), Value = year.ToString() });
            };
            ViewBag.Years = yearList;
            ViewBag.MemberLeaveSummaries = new List<MemberLeaveSummary>();
        }
    }

    public class SelectListModel
    {
        public String Text { get; set; }
        public String Value { get; set; }
    }
}