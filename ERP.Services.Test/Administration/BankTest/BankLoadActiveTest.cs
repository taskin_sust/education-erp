﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BankTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Bank")]
    public class BankLoadActiveTest : BankBaseTest
    {
        #region Basic Test
        [Fact]
        public void LoadBank_should_return_list()
        {
            _bankFactory.CreateMore(100).Persist();
            var statusArray = new[] { Bank.EntityStatus.Active };
            var bankList = _bankService.LoadBankList();
            Assert.True(bankList.Count > 0);
        }
        [Fact]
        public void LoadActive_should_return_list()
        {
            _bankFactory.CreateMore(100).Persist();
            var subjectList = _bankService.LoadBankList(0, 100, "Rank", "Asc", "", "", "", "1");
            Assert.Equal(100, subjectList.Count);
        }

        #endregion
    }
}
