﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;

namespace UdvashERP.Services.Hr
{
    public interface IAttendanceSynchronizerService : IBaseService
    {
        #region Operational Function

        void Save(AttendanceSynchronizer hrAttendanceSynchronizer);
        void Delete(AttendanceSynchronizer deleteHrAttendanceSynchronizer);
        void Update(AttendanceSynchronizer hrAttendanceSynchronizer);

        #endregion

        #region Single Instances Loading Function

        AttendanceSynchronizer GetAttendanceSynchronizer(long synchronizer);
        AttendanceSynchronizer GetAttendanceSynchronizer(string synchronizerKey);

        #endregion

        #region List Loading Function

        IList<AttendanceSynchronizer> LoadSynchronizer(List<long> organizationIds = null, List<long> branchIds = null, List<long> campusIds = null);
        List<AttendanceSynchronizer> LoadSynchronizer(int start, int length, string orderBy, string orderDir, string organizationId, string branchId, string campusId);
        
        #endregion

        #region Others Function

        int SynchronizerRowCount(string organizationId, string branchId, string campusId);

        #endregion

        #region Helper Function

        #endregion

        AttendanceSynchronizer GetAttendanceSynchronizerKey(string synchronizerKey);
 
    }

    public class AttendanceSynchronizerService : BaseService, IAttendanceSynchronizerService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly AttendanceSynchronizerDao _hrAttendanceSynchronizerDao;
        
        public AttendanceSynchronizerService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrAttendanceSynchronizerDao = new AttendanceSynchronizerDao() { Session = session };

        }

        #endregion

        #region Operational Functions

        public void Save(AttendanceSynchronizer hrAttendanceSynchronizer)
        {
            ITransaction transaction = null;
            try
            {

                if (hrAttendanceSynchronizer == null)
                {
                    throw new NullObjectException("Attendance Synchronizer can not be null");
                }

                var validationContext = new ValidationContext(hrAttendanceSynchronizer, null, null);
                var validationResults = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(hrAttendanceSynchronizer, validationContext, validationResults);

                if (!isValid)
                {
                    string errorMessage = "";
                    validationResults.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                    throw new MessageException(errorMessage);
                }

                DuplicatationCheck(hrAttendanceSynchronizer.SynchronizerKey);
                using (transaction = Session.BeginTransaction())
                {
                    _hrAttendanceSynchronizerDao.Save(hrAttendanceSynchronizer);
                    transaction.Commit();
                }

            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void Delete(AttendanceSynchronizer deleteHrAttendanceSynchronizer)
        {
            ITransaction transaction = null;
            try
            {
                if (deleteHrAttendanceSynchronizer == null)
                {
                    throw new NullObjectException("Attendance Synchronizer can not be null");
                }
                var newObj = _hrAttendanceSynchronizerDao.LoadById(deleteHrAttendanceSynchronizer.Id);
                if (newObj == null)
                {
                    throw new NullObjectException("Attendance Synchronizer can not be null");
                }

                if (newObj.AttendanceDevice.Any())
                {
                    throw new DependencyException("This Attendance Synchronizer have one or more child data.");
                }

                using (transaction = Session.BeginTransaction())
                {
                    _hrAttendanceSynchronizerDao.Update(deleteHrAttendanceSynchronizer);
                    transaction.Commit();
                }

            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void Update(AttendanceSynchronizer hrAttendanceSynchronizer)
        {
            ITransaction transaction = null;
            try
            {
                if (hrAttendanceSynchronizer == null)
                {
                    throw new NullObjectException("Attendance Synchronizer can not be null");
                }
                var newObj = _hrAttendanceSynchronizerDao.LoadById(hrAttendanceSynchronizer.Id);
                if (newObj == null)
                {
                    throw new NullObjectException("Attendance Synchronizer can not be null");
                }
                var validationContext = new ValidationContext(hrAttendanceSynchronizer, null, null);
                var validationResults = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(hrAttendanceSynchronizer, validationContext, validationResults);

                if (!isValid)
                {
                    string errorMessage = "";
                    validationResults.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                    throw new MessageException(errorMessage);
                }

                DuplicatationCheck(hrAttendanceSynchronizer.SynchronizerKey, hrAttendanceSynchronizer.Id);

                using (transaction = Session.BeginTransaction())
                {
                    _hrAttendanceSynchronizerDao.Update(hrAttendanceSynchronizer);
                    transaction.Commit();
                }


            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }

        }

        #endregion

        #region Single Instances Loading Function

        public AttendanceSynchronizer GetAttendanceSynchronizer(long synchronizer)
        {
            try
            {
                return _hrAttendanceSynchronizerDao.LoadById(synchronizer);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public AttendanceSynchronizer GetAttendanceSynchronizer(string synchronizerKey)
        {
            try
            {
                return _hrAttendanceSynchronizerDao.GetSynchronizer(synchronizerKey, 0);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }



        #endregion

        #region List Loading Function
        
        public IList<AttendanceSynchronizer> LoadSynchronizer(List<long> organizationIds = null, List<long> branchIds = null, List<long> campusIds = null)
        {
            try
            {
                return _hrAttendanceSynchronizerDao.LoadSynchronizer(organizationIds, branchIds, campusIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<AttendanceSynchronizer> LoadSynchronizer(int start, int length, string orderBy, string orderDir, string organizationId, string branchId,
            string campusId)
        {
            try
            {
                return _hrAttendanceSynchronizerDao.LoadSynchronizer(start, length, orderBy, orderDir, organizationId, branchId, campusId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int SynchronizerRowCount(string organizationId, string branchId, string campusId)
        {
            try
            {
                return _hrAttendanceSynchronizerDao.SynchronizerRowCount(organizationId, branchId, campusId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        private void DuplicatationCheck(string synchronizerKey, long id = 0)
        {
            var data = _hrAttendanceSynchronizerDao.GetSynchronizer(synchronizerKey, id);
            if (data != null)
                throw new DuplicateEntryException("Duplicate Synchronizer found");
        }

        #endregion

        public AttendanceSynchronizer GetAttendanceSynchronizerKey(string synchronizerKey)
        {
            try
            {
                return _hrAttendanceSynchronizerDao.GetSynchronizer(synchronizerKey);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

    }
}
