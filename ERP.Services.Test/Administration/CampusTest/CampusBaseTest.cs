﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.CampusTest
{
    public class CampusBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal IBranchService _branchService;
        internal ICampusService _campusService;
        internal IBatchService _batchService;
        internal ICampusRoomService _campusRoomService;
        internal List<long> IdList = new List<long>();
        private ISession _session;
        internal CampusFactory _campusFactory;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal string _code = Guid.NewGuid() + "_(1234 5678)";
        internal const long OrgId = 1;
        internal DateTime _startTime = DateTime.Now.AddDays(1).AddHours(1);
        internal DateTime _endTime = DateTime.Now.AddDays(1).AddHours(10);
        internal TestBaseService<Campus> _campusTestService;
        internal ProgramFactory _programFactory;
        internal BranchFactory _branchFactory;
        internal SessionFactory _sessionFactory;
        internal IStudentExamService _studentExamService;
        internal IProgramBranchSessionService _programBranchSessionService;
        internal IProgramService _programService;
        internal ISessionService _sessionService;
        internal readonly string _programName = Guid.NewGuid().ToString().Substring(0,15) + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal int _paidProgram = (int)Program.ProgramTypeStatus.Paid;
        internal int _unpaidProgram = (int)Program.ProgramTypeStatus.UnPaid;
        internal int _csrProgram = (int)Program.ProgramTypeStatus.Csr;
        #endregion

        public CampusBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _campusFactory = new CampusFactory(null, _session);
            _campusService = new CampusService(_session);
            _branchService = new BranchService(_session);
            _campusRoomService = new CampusRoomService(_session);
            _batchService = new BatchService(_session);
            _programFactory = new ProgramFactory(null, _session);
            _branchFactory = new BranchFactory(null, _session);
            _sessionFactory = new SessionFactory(null, _session);
            _campusTestService = new TestBaseService<Campus>(_session);
            _studentExamService = new StudentExamService(_session);
            _programBranchSessionService = new ProgramBranchSessionService(_session);
            _programService = new ProgramService(_session);
            _sessionService = new SessionService(_session);
        }
        public void CleanUpProgramBranchSession(long programId, long sessonId, long[] branchIdList)
        {
            _campusTestService.DeleteProgramBranchSession(programId, sessonId, branchIdList);
        }
        public void CleanUpProgramStudentExamSession(long programId, long sessionId)
        {
            _campusTestService.DeleteProgramStudentExamSession(programId, sessionId);
        }
        
        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _campusFactory.Cleanup();
            _programFactory.Cleanup();
            _branchFactory.Cleanup();
            _sessionFactory.Cleanup();
            base.Dispose();
        }
    }
}
