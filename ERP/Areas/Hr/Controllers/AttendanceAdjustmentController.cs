﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;
using Constants = UdvashERP.BusinessRules.Constants;
using UdvashERP.Services.Hr.AttendanceSummaryServices;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class AttendanceAdjustmentController : Controller
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        private readonly ITeamMemberService _memberService;
        private readonly IAttendanceAdjustmentService _hrAttendanceAdjustmentService;
        private readonly IOrganizationService _organizationService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IUserService _userService;
        private List<UserMenu> _userMenu;
        public AttendanceAdjustmentController()
        {
            ISession nHsession = NHibernateSessionFactory.OpenSession();
            _memberService = new TeamMemberService(nHsession);
            _hrAttendanceAdjustmentService = new AttendanceAdjustmentService(nHsession);
            _organizationService = new OrganizationService(nHsession);
            _branchService = new BranchService(nHsession);
            _campusService = new CampusService(nHsession);
            _userService = new UserService(nHsession);
        }

        #region ApplyFor
        [HttpGet]
        public ActionResult ApplyForAttendanceAdjustment()
        {
            try
            {
                #region Is Post Check
                TeamMember teamMember = _memberService.GetCurrentMember();
                if (teamMember == null)
                {
                    throw new InvalidDataException("Empty team member");
                }
                DateTime d1 = new DateTime(Constants.FirstBussinessYear, 01, 01);
                d1 = new DateTime(d1.Year, d1.Month, d1.Day, 0, 0, 0);
                var lastPostDate = _hrAttendanceAdjustmentService.GetTeamMemberAttendanceAdjustmentPostDateTime(teamMember.Id) ?? d1;
                ViewBag.LastPostDate = lastPostDate.AddDays(1);
                #endregion

            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Server Error, Please try again.";
            }

            return View();
        }

        [HttpPost]
        public ActionResult ApplyForAttendanceAdjustment(List<AttendanceAdjustmentAllRowArray> attendanceAdjustments)
        {
            try
            {
                TeamMember teamMember = _memberService.GetCurrentMember();
                if (teamMember == null)
                {
                    throw new InvalidDataException("Empty team member");
                }
                if (attendanceAdjustments != null)
                {

                    IList<AttendanceAdjustment> hrAttendanceAdjustments = new List<AttendanceAdjustment>();
                    foreach (var aAdjust in attendanceAdjustments)
                    {
                        var hrAttendanceAdjustment = new AttendanceAdjustment
                        {
                            Date = aAdjust.Date,
                            Reason = aAdjust.ReasonForAdjustment,
                            StartTime = aAdjust.TimeFrom,
                            EndTime = aAdjust.TimeTo,
                            TeamMember = teamMember,
                            AdjustmentStatus = (int?)AttendanceAdjustmentStatus.Pending
                        };
                        hrAttendanceAdjustments.Add(hrAttendanceAdjustment);
                    }
                    _hrAttendanceAdjustmentService.SaveOrUpdate(hrAttendanceAdjustments);
                    return Json(new { returnSuccess = "Successfully Save", IsSuccess = true });
                    //if (isSuccess)
                    //{
                    //    return Json(new { returnSuccess = "Successfully Save", IsSuccess = true });
                    //}
                    //else
                    //{
                    //    return Json(new Response(false, "Problem occured!"));
                    //}
                }
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                //return Json(new Response(false, WebHelper.CommonErrorMessage));
                return Json(new Response(false, ex.Message));
            }
            return Json(new Response(false, "You are not a team member!"));
        }

        public JsonResult AttendanceAdjustmentSelfList(int draw, int start, int length)
        {
            NameValueCollection nvc = Request.Form;
            TeamMember hrMember = _memberService.GetCurrentMember();
            long recordsFiltered = 0;
            long recordsTotal = 0;
            var getData = new List<object>();
            if (hrMember != null)
            {
                var lastPostDate = _hrAttendanceAdjustmentService.GetTeamMemberAttendanceAdjustmentPostDateTime(hrMember.Id);

                string orderBy = "";
                string orderDir = "";
                try
                {
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                                orderBy = "Date";
                                break;
                            default:
                                orderBy = "Date";
                                break;
                        }
                    }
                    int totalRow = _hrAttendanceAdjustmentService.CountRowAttendanceAdjustmentSelfMember(hrMember);
                    var rowList =
                        _hrAttendanceAdjustmentService.LoadAllRowAttendanceAdjustmentSelfMember(hrMember, start, length,
                            orderBy, orderDir.ToUpper()).ToList();
                    recordsFiltered = totalRow;
                    recordsTotal = recordsFiltered;
                    foreach (var row in rowList)
                    {
                        var str = new List<string>();
                        str.Add(row.Date != null ? row.Date.Value.ToString("yyyy-MM-dd") : "");
                        str.Add(row.Reason);
                        str.Add(row.StartTime != null ? row.StartTime.Value.ToShortTimeString() : "");
                        str.Add(row.EndTime != null ? row.EndTime.Value.ToShortTimeString() : "");
                        str.Add(
                            AttendanceAdjustmentStatusTypeText.GetAttendanceAdjustmentStatusText(row.AdjustmentStatus));
                        if (row.AdjustmentStatus == (int)AttendanceAdjustmentStatus.Pending && (lastPostDate == null || row.Date > lastPostDate))
                            str.Add("<a href='#' class='glyphicon glyphicon-remove changeAAStatus' id='" + row.Id +
                                    "'></a>");
                        else str.Add("");
                        getData.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = getData

                    });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }

        [HttpPost]
        public ActionResult DeleteAttendanceAdjustmentStatus(string id)
        {
            try
            {
                AttendanceAdjustment hrAttendanceAdjustment = _hrAttendanceAdjustmentService.LoadById(Convert.ToInt64(id));
                bool isSuccess = _hrAttendanceAdjustmentService.Delete(hrAttendanceAdjustment);
                return isSuccess ? Json(new { returnSuccess = "Successfully Save", IsSuccess = true }) : Json(new Response(false, "Problem occured!"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Problem occured!"));
            }
        }

        #endregion

        #region Mentor Adjustment & approval

        [HttpGet]
        public ActionResult MentorAttendanceAdjustment()
        {
            try
            {
                ViewBag.dateFromField = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
                ViewBag.dateToField = DateTime.Now.ToString("yyyy-MM-dd");
                TeamMember hrMember = _memberService.GetCurrentMember();
                if (hrMember != null)
                {
                    IList<AttendanceAdjustment> totalPendingList =
                        _hrAttendanceAdjustmentService.LoadPendingListForMentor(hrMember);
                    ViewBag.TotalPendingList = totalPendingList;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public ActionResult MentorApproveAllAttendanceAdjustment(List<AttendanceAdjustmentView> mentorAttendanceAdjustments)
        {
            string mesage = "";
            bool succesState = false;
            try
            {
                if (mentorAttendanceAdjustments != null)
                {
                    IList<AttendanceAdjustment> mAttendanceAds = new List<AttendanceAdjustment>();
                    foreach (var mentorAtt in mentorAttendanceAdjustments)
                    {
                        var mAdjustment = new AttendanceAdjustment()
                        {
                            Id = mentorAtt.Id,
                            TeamMember = _memberService.LoadById(mentorAtt.MemberId),
                            Date = Convert.ToDateTime(mentorAtt.Date),
                            Reason = mentorAtt.ReasonForAdjustment,
                            StartTime = DateTime.Parse(Convert.ToDateTime(mentorAtt.Date).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(mentorAtt.TimeFrom).ToString("H:mm")),//Convert.ToDateTime(mentorAtt.TimeFrom),
                            EndTime = DateTime.Parse(Convert.ToDateTime(mentorAtt.Date).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(mentorAtt.TimeTo).ToString("H:mm")),//Convert.ToDateTime(mentorAtt.TimeTo),
                            AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved
                        };
                        mAttendanceAds.Add(mAdjustment);
                    }
                    //     bool isSuccess = _hrAttendanceAdjustmentService.AttendanceAdjustmentApproved(mAttendanceAds);
                    _hrAttendanceAdjustmentService.SaveOrUpdate(mAttendanceAds);

                    succesState = true;
                    mesage = "Successfully Approved";
                }
            }
            catch (DuplicateEntryException ex)
            {
                mesage = ex.Message;
            }

            catch (InvalidDataException ex)
            {
                mesage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                mesage = WebHelper.CommonErrorMessage;
            }
            return Json(new { returnSuccess = mesage, IsSuccess = succesState });
        }

        [HttpPost]
        public ActionResult MentorAttendanceAdjustmentRecentHistory(int draw, int start, int length, string pin, string dateFrom, string dateTo)
        {
            NameValueCollection nvc = Request.Form;
            TeamMember hrMember = _memberService.GetCurrentMember();
            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            if (hrMember != null)
            {
                try
                {

                    DateTime dateFromDateTime = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).Date;
                    DateTime dateToDateTime = DateTime.Today.Date;
                    if (!String.IsNullOrEmpty(dateFrom))
                    {
                        dateFromDateTime = Convert.ToDateTime(dateFrom).Date;
                    }
                    if (!String.IsNullOrEmpty(dateTo))
                    {
                        dateToDateTime = Convert.ToDateTime(dateTo).Date;
                    }

                    if (dateFromDateTime > dateToDateTime)
                    {
                        DateTime temp = dateToDateTime;
                        dateToDateTime = dateFromDateTime;
                        dateFromDateTime = temp;
                    }


                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                                orderBy = "Date";
                                break;
                            default:
                                orderBy = "Date";
                                break;
                        }
                    }

                    int totalRow = _hrAttendanceAdjustmentService.CountMentorAttendanceAdjustmentRecentHistory(hrMember, pin, dateFromDateTime, dateToDateTime);
                    var rowList = _hrAttendanceAdjustmentService.LoadAllMentorAttendanceAdjustmentRecentHistory(start, length, orderBy, orderDir.ToUpper(), hrMember, pin, dateFromDateTime, dateToDateTime).ToList();

                    long recordsFiltered = totalRow;
                    long recordsTotal = recordsFiltered;
                    foreach (var row in rowList)
                    {
                        var str = new List<string>();
                        str.Add(row.Date != null ? row.Date.Value.ToString("yyyy-MM-dd") : "");
                        str.Add(row.TeamMember.Pin.ToString());
                        str.Add(row.TeamMember.Name);
                        str.Add(row.Reason);
                        str.Add(row.StartTime != null ? row.StartTime.Value.ToShortTimeString() : "");
                        str.Add(row.EndTime != null ? row.EndTime.Value.ToShortTimeString() : "");
                        str.Add(AttendanceAdjustmentStatusTypeText.GetAttendanceAdjustmentStatusText(row.AdjustmentStatus));
                        if (row.AdjustmentStatus == (int)AttendanceAdjustmentStatus.Approved)
                        {
                            str.Add("<a href='" + Url.Action("EditHistoryByMentor", "AttendanceAdjustment") + "?Id=" + row.Id + "' class='glyphicon glyphicon-pencil changeHistory' id='" + row.Id + "'></a>");
                        }
                        else
                        {
                            str.Add("");
                        }
                        getData.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = getData

                    });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }

        public ActionResult EditHistoryByMentor(string id)
        {
            try
            {
                var model = new AttendanceAdjustment();
                if (id != "")
                {
                    model = _hrAttendanceAdjustmentService.LoadById(Convert.ToInt64(id));
                }
                ViewBag.Model = model ?? null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public ActionResult MentorRejectedAttendanceAdjustment(int id)
        {
            string message = "";
            bool successState = false;
            try
            {
                AttendanceAdjustment model = _hrAttendanceAdjustmentService.LoadById(Convert.ToInt64(id));
                if (model != null)
                {
                    bool isSuccess = _hrAttendanceAdjustmentService.RejectedAttendanceAdjustment(model);
                    if (isSuccess)
                    {
                        message = "Successfully Canceled";
                        successState = true;
                    }
                    else
                    {
                        message = WebHelper.CommonErrorMessage;
                    }
                }
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
            }
            return Json(new { returnSuccess = message, IsSuccess = successState });
        }

        public ActionResult UpdateAttendanceAdjustmentByMentor(AttendanceAdjustmentView mentorAttendanceAdjustments)
        {
            try
            {
                if (mentorAttendanceAdjustments != null)
                {
                    var aAdjustment = new AttendanceAdjustment
                    {
                        Id = mentorAttendanceAdjustments.Id,
                        TeamMember = _memberService.LoadById(mentorAttendanceAdjustments.MemberId),
                        Reason = mentorAttendanceAdjustments.ReasonForAdjustment,
                        StartTime =
                            DateTime.Parse(Convert.ToDateTime(mentorAttendanceAdjustments.Date).ToString("yyyy-MM-dd") +
                                           " " +
                                           Convert.ToDateTime(mentorAttendanceAdjustments.TimeFrom).ToString("H:mm")),
                        EndTime =
                            DateTime.Parse(Convert.ToDateTime(mentorAttendanceAdjustments.Date).ToString("yyyy-MM-dd") +
                                           " " +
                                           Convert.ToDateTime(mentorAttendanceAdjustments.TimeTo).ToString("H:mm")),
                        Date = Convert.ToDateTime(mentorAttendanceAdjustments.Date)
                    };
                    IList<AttendanceAdjustment> mAttendanceAds = new List<AttendanceAdjustment>();
                    mAttendanceAds.Add(aAdjustment);
                    _hrAttendanceAdjustmentService.SaveOrUpdate(mAttendanceAds);
                    // return Json(new { returnSuccess = "Successfully Update", IsSuccess = true });
                    return Json(new Response(true, "Successfully Update"));
                    //bool isSuccess = _hrAttendanceAdjustmentService.Update(aAdjustment);
                    //return isSuccess ? Json(new { returnSuccess = "Successfully Update", Issuccess = true }) : Json(new Response(false, WebHelper.CommonErrorMessage));
                }
                else
                {
                    return Json(false, WebHelper.CommonErrorMessage);
                }
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            // return Json(new Response(false, WebHelper.CommonErrorMessage));
        }

        #endregion

        #region HR Adjustment & Approval

        [HttpGet]
        public ActionResult HrAdjustmentApproval()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var orgList = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
            IList<AttendanceAdjustmentHrDto> totalPendingList = _hrAttendanceAdjustmentService.LoadPendingListForHr(_userMenu, null);
            ViewBag.TotalPendingList = totalPendingList;

            ViewBag.OrgList = new SelectList(orgList, "Id", "ShortName");
            ViewBag.DeptList = new SelectList(new List<Department>(), "Id", "Name");
            ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.dateFromField = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
            ViewBag.dateToField = DateTime.Now.ToString("yyyy-MM-dd");
            return View();
        }

        [HttpPost]
        public ActionResult DisplayAttendanceAdjustmentForHr(string pin, long[] org, long[] branch, long[] campus, long[] department, string date)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<AttendanceAdjustmentHrDto> totalPendingList = _hrAttendanceAdjustmentService.LoadPendingListForHr(_userMenu, date, org.ToList(), branch.ToList(), campus.ToList(), department.ToList(), pin);
                return PartialView("Partial/_DisplaydataForHr", totalPendingList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult HrApproveAllAttendanceAdjustment(List<AttendanceAdjustmentView> hrAttendanceAdjustments)
        {
            try
            {
                if (hrAttendanceAdjustments != null)
                {
                    IList<AttendanceAdjustment> mAttendanceAds = new List<AttendanceAdjustment>();

                    foreach (var mentorAtt in hrAttendanceAdjustments)
                    {
                        var mAdjustment = new AttendanceAdjustment()
                        {
                            Id = mentorAtt.Id,
                            TeamMember = _memberService.LoadById(mentorAtt.MemberId),
                            Date = Convert.ToDateTime(mentorAtt.Date),
                            Reason = mentorAtt.ReasonForAdjustment,
                            StartTime =
                                DateTime.Parse(Convert.ToDateTime(mentorAtt.Date).ToString("yyyy-MM-dd") + " " +
                                               Convert.ToDateTime(mentorAtt.TimeFrom).ToString("H:mm")),
                            //Convert.ToDateTime(mentorAtt.TimeFrom),
                            EndTime =
                                DateTime.Parse(Convert.ToDateTime(mentorAtt.Date).ToString("yyyy-MM-dd") + " " +
                                               Convert.ToDateTime(mentorAtt.TimeTo).ToString("H:mm")),
                            //Convert.ToDateTime(mentorAtt.TimeTo),
                            AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved
                        };
                        mAttendanceAds.Add(mAdjustment);
                    }
                    _hrAttendanceAdjustmentService.SaveOrUpdate(mAttendanceAds);
                    return Json(new { returnSuccess = "Successfully Approved", IsSuccess = true });

                }
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }

        [HttpPost]
        public ActionResult HrAttendanceAdjustmentRecentHistory(int draw, int start, int length, string pin, string dateFrom, string dateTo, List<long> org, List<long> branch, List<long> campus, List<long> department)
        {
            NameValueCollection nvc = Request.Form;
            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                DateTime dateFromDateTime = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).Date;
                DateTime dateToDateTime = DateTime.Today.Date;
                if (!String.IsNullOrEmpty(dateFrom))
                {
                    dateFromDateTime = Convert.ToDateTime(dateFrom).Date;
                }
                if (!String.IsNullOrEmpty(dateTo))
                {
                    dateToDateTime = Convert.ToDateTime(dateTo).Date;
                }

                if (dateFromDateTime > dateToDateTime)
                {
                    DateTime temp = dateToDateTime;
                    dateToDateTime = dateFromDateTime;
                    dateFromDateTime = temp;
                }
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Date";
                            break;
                        default:
                            orderBy = "Date";
                            break;
                    }
                }
                int totalRow = _hrAttendanceAdjustmentService.CountHrAttendanceAdjustmentRecentHistory(_userMenu, pin, dateFromDateTime, dateToDateTime, org, branch, campus, department);
                var rowList = _hrAttendanceAdjustmentService.LoadAllHrAttendanceAdjustmentRecentHistory(_userMenu, start, length, orderBy, orderDir.ToUpper(), pin, dateFromDateTime, dateToDateTime, org, branch, campus, department).ToList();

                long recordsFiltered = totalRow;
                long recordsTotal = recordsFiltered;

                foreach (var row in rowList)
                {
                    var str = new List<string>();
                    str.Add(row.Date.ToString("yyyy-MM-dd"));
                    str.Add(row.Pin.ToString());
                    str.Add(row.Name);
                    str.Add(row.Department);
                    str.Add(row.Organization);
                    str.Add(row.Branch);
                    str.Add(row.Campus);
                    str.Add(row.Reason);
                    str.Add(row.StartTime.ToShortTimeString());
                    str.Add(row.EndTime.ToShortTimeString());
                    if (row.AdjustmentStatus == (int)AttendanceAdjustmentStatus.Approved)
                        str.Add("<li class='glyphicon glyphicon-ok'></li>");
                    else if (row.AdjustmentStatus == (int)AttendanceAdjustmentStatus.Rejected)
                        str.Add("<li class='glyphicon glyphicon-ban-circle'></li>");
                    str.Add(row.ModifyBy != null ? _userService.GetUserNameByAspNetUserId((long)row.ModifyBy) : "-");
                    str.Add(CommonHelper.GetReportCommonDateTimeFormat(row.LastModificationDateTime));
                    str.Add("<a href='" + Url.Action("EditHistoryByHr", "AttendanceAdjustment") + "?Id=" + row.Id + "' class='glyphicon glyphicon-edit changeHistory' id='" + row.Id + "'></a>");
                    getData.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData

                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }

        public ActionResult EditHistoryByHr(string id)
        {
            var model = new AttendanceAdjustment();
            try
            {
                if (id != "")
                {
                    model = _hrAttendanceAdjustmentService.LoadById(Convert.ToInt64(id));
                    if (model != null)
                    {
                        EmploymentHistory emplomentHistory = model.TeamMember.EmploymentHistory.Where(x => x.EffectiveDate <= model.Date && x.Status == EmploymentHistory.EntityStatus.Active).OrderByDescending(x => x.EffectiveDate).ThenByDescending(x => x.Id).FirstOrDefault();
                        if (model.Date != null) ViewBag.Date = model.Date.Value.ToString("yyyy-MM-dd");
                        if (model.StartTime != null) ViewBag.StartTime = model.StartTime.Value.ToString("hh:mm.tt");
                        if (model.EndTime != null) ViewBag.EndTime = model.EndTime.Value.ToString("hh:mm.tt");
                        ViewBag.Org = (emplomentHistory != null) ? emplomentHistory.Department.Organization.ShortName : "-";
                        ViewBag.Branch = (emplomentHistory != null) ? emplomentHistory.Campus.Branch.Name : "-";
                        ViewBag.Campus = (emplomentHistory != null) ? emplomentHistory.Campus.Name : "-";
                        ViewBag.Department = (emplomentHistory != null) ? emplomentHistory.Department.Name : "-";
                        ViewBag.Pin = model.TeamMember.Pin;
                        ViewBag.Name = model.TeamMember.Name;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult ApproveAttendanceAdjustmentByHr(AttendanceAdjustmentView hrAttendanceAdjustmentViewModel)
        {
            try
            {
                AttendanceAdjustment model = _hrAttendanceAdjustmentService.LoadById(hrAttendanceAdjustmentViewModel.Id);
                model.Date = Convert.ToDateTime(hrAttendanceAdjustmentViewModel.Date);
                model.StartTime = Convert.ToDateTime(hrAttendanceAdjustmentViewModel.TimeFrom);
                model.EndTime = Convert.ToDateTime(hrAttendanceAdjustmentViewModel.TimeTo);
                model.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
                IList<AttendanceAdjustment> mAttendanceAds = new List<AttendanceAdjustment>();
                mAttendanceAds.Add(model);
                _hrAttendanceAdjustmentService.SaveOrUpdate(mAttendanceAds);

                return Json(new { returnSuccess = "Successfully Approved", IsSuccess = true });
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult RejectAttendanceAdjustmentByHr(long id)
        {
            try
            {

                AttendanceAdjustment model = _hrAttendanceAdjustmentService.LoadById(id);
                if (model != null)
                {
                    bool isSuccess = _hrAttendanceAdjustmentService.RejectedAttendanceAdjustment(model);
                    return isSuccess ? Json(new { returnSuccess = "Successfully Canceled", IsSuccess = true }) : Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }

        [HttpPost]
        public ActionResult RejectAttendanceAdjustmentViewModelByHr(AttendanceAdjustmentView hrAttendanceAdjustmentViewModel)
        {
            bool successState = false;
            string message = "";
            try
            {
                AttendanceAdjustment model = _hrAttendanceAdjustmentService.LoadById(hrAttendanceAdjustmentViewModel.Id);
                //model.Date = Convert.ToDateTime(hrAttendanceAdjustmentViewModel.Date);
                //model.StartTime = Convert.ToDateTime(hrAttendanceAdjustmentViewModel.TimeFrom);
                //model.EndTime = Convert.ToDateTime(hrAttendanceAdjustmentViewModel.TimeTo);
                //model.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Rejected;
                //bool isSuccess = _hrAttendanceAdjustmentService.HrApproved(model);
                bool isSuccess = _hrAttendanceAdjustmentService.RejectedAttendanceAdjustment(model);
                if (isSuccess)
                {
                    successState = true;
                    message = "Successfully Rejected";
                }
                //return isSuccess ? Json(new { returnSuccess = "Successfully Rejected", IsSuccess = true }) : Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            catch (DependencyException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
                //return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        #endregion

    }
}