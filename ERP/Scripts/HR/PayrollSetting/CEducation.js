﻿$(document).ready(function () {

    $("#chk-employee-status").click(function () {
        if ($(this).is(':checked')) {
            showErrorMessageBelowCtrl("Err-empType", "", false);
            $(".chk-member-status").each(function () {
                $(this).prop('checked', true);
                $('#Err-empType').css("display", "none");
            });
        } else {
            showErrorMessageBelowCtrl("Err-empType", "", false);
            showErrorMessageBelowCtrl("Err-empType", "Please Select atleast one Employee Type", true);
            $(".chk-member-status").each(function () {
                $(this).prop('checked', false);
            });
        }
    });

    $(document).on('click', '.chk-member-status', function () {
        console.log("clicked");
        var allchecked = true;
        var errorcheck = false;
        $('.chk-member-status').each(function (index, value) {
            if ($(this).is(':checked')) {
                showErrorMessageBelowCtrl("Err-empType", "", false);
            } else {
                allchecked = false;
                $('.chk-member-status').each(function (index, value) {
                    /*put logic here*/
                    if ($(this).is(':checked')) {
                        errorcheck = true;
                        return;
                    }
                });
                if (errorcheck) {
                    showErrorMessageBelowCtrl("Err-empType", "", false);
                } else {
                    showErrorMessageBelowCtrl("Err-empType", "", false);
                    showErrorMessageBelowCtrl("Err-empType", "Please Select atleast one Employee Type", true);
                }

            }
        });
        if (allchecked) {
            $('#chk-employee-status').prop('checked', true);
        } else {
            $('#chk-employee-status').prop('checked', false);
        }
    });

    // validation check 
    //var allUncheckedEmpType = true;
    //$('.chk-member-status').each(function (index, value) {
    //    if ($(this).is(':checked')) {
    //        $('#Err-empType').css("display", "none");
    //        allUncheckedEmpType = false;
    //    }
    //});
    //if (allUncheckedEmpType) {
    //    $('#Err-empType').css("display", "block");
    //    return false;
    //}


});