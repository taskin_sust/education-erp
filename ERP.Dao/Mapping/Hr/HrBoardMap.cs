using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Hr {


    public partial class HrBoardMap : BaseClassMap<BusinessModel.Entity.Hr.Board, long>
    {
        
        public HrBoardMap() {
			Table("HR_Board");
			LazyLoad();
			Map(x => x.Name).Column("Name");
        }
    }
}
