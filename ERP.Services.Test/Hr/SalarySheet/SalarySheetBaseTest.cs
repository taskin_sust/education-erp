﻿using System.Web.UI.WebControls;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.SalarySheet
{
    public class SalarySheetBaseTest:TestBase,IDisposable
    {
        #region Propertise & Object Initilization

       // private ISession _session;

        protected readonly DayOffAdjustmentFactory _dayOffAdjustmentFactory;
        protected readonly EmployeeBenefitsFundSettingEntitlementFactory _employeeBenefitsFundSettingEntitlementFactory;
        protected readonly EmployeeBenefitsFundSettingFactory _employeeBenefitsFundSettingFactory;
        protected readonly HolidaySettingFactory _holidaySettingFactory;
        protected readonly HolidayWorkFactory _holidayWorkFactory;
        protected readonly IAttendanceAdjustmentService _attendanceAdjustmentService;
        protected readonly IEmploymentHistoryService _employmentHistoryService;
        protected readonly IEmployeeBenefitsFundSettingService _employeeBenefitsFundSettingService;
        protected readonly IHolidaySettingService _holidaySettingService;
        protected readonly IHolidayWorkService _holidayWorkService;
        protected readonly IOrganizationService _organizationService;
        protected readonly ISalaryHistoryService _salaryHistoryService;
        protected readonly ISalarySheetService _salarySheetService;
        protected readonly ITeamMemberService _teamMemberService;
        protected readonly IZoneSettingService _zoneSettingService;
        protected readonly LeaveApplicationService _leaveApplicationService;
        protected readonly LeaveApplicationDetailFactory _leaveApplicationDetailFactory;
        protected readonly LeaveService _leaveService;
        protected readonly MemberEbfHistoryFactory _memberEbfHistoryFactory;
        protected readonly MemberLoanApplicationFactory _memberLoanApplicationFactory;
        protected readonly MemberLoanFactory _memberLoanFactory;
        protected readonly MemberLoanRefundFactory _memberLoanRefundFactory;
        protected readonly NightWorkFactory _nightWorkFactory;
        protected readonly OverTimeFactory _overTimeFactory;
        protected readonly SalaryHistoryFactory _salaryHistoryFactory;
        protected readonly SalarySheetDetailsFactory _salarySheetDetailsFactory;
        protected readonly SalarySheetFactory _salarySheetFactory;
        
        #endregion

        public SalarySheetBaseTest()
        {
           // _session = NHibernateSessionFactory.OpenSession();
            _attendanceAdjustmentService = new AttendanceAdjustmentService(Session);
            _dayOffAdjustmentFactory = new DayOffAdjustmentFactory(null, Session);
            _employmentHistoryService = new EmploymentHistoryService(Session);
            _employeeBenefitsFundSettingEntitlementFactory = new EmployeeBenefitsFundSettingEntitlementFactory(null, Session);
            _employeeBenefitsFundSettingFactory = new EmployeeBenefitsFundSettingFactory(null, Session);
            _employeeBenefitsFundSettingService = new EmployeeBenefitsFundSettingService(Session);
            _holidaySettingFactory = new HolidaySettingFactory(null, Session);
            _holidaySettingService = new HolidaySettingService(Session);
            _holidayWorkFactory = new HolidayWorkFactory(null, Session);
            _holidayWorkService = new HolidayWorkService(Session);
            _leaveApplicationService = new LeaveApplicationService(Session);
            _leaveApplicationDetailFactory = new LeaveApplicationDetailFactory(null,Session);
            _leaveService = new LeaveService(Session);
            _memberEbfHistoryFactory = new MemberEbfHistoryFactory(null, Session);
            _memberLoanApplicationFactory = new MemberLoanApplicationFactory(null, Session);
            _memberLoanFactory = new MemberLoanFactory(null, Session);
            _memberLoanRefundFactory = new MemberLoanRefundFactory(null, Session);
            _organizationService = new OrganizationService(Session);
            _overTimeFactory = new OverTimeFactory(null, Session);
            _salaryHistoryFactory = new SalaryHistoryFactory(null, Session);
            _salaryHistoryService = new SalaryHistoryService(Session);
            _salarySheetDetailsFactory = new SalarySheetDetailsFactory(null, Session);
            _salarySheetFactory = new SalarySheetFactory(null, Session);
            _salarySheetService = new SalarySheetService(Session);
            _teamMemberService = new TeamMemberService(Session);
            _zoneSettingService = new ZoneSettingService(Session);
        }

        #region Disposal

        public void Dispose()
        {
            _salarySheetDetailsFactory.CleanUp();
            _memberEbfHistoryFactory.CleanUp();
            _salarySheetFactory.CleanUp();
            attendanceAdjustmentFactory.LogCleanUp(attendanceAdjustmentFactory);
            attendanceAdjustmentFactory.CleanUp(attendanceAdjustmentFactory);
            leaveApplicationFactory.LogCleanUp(leaveApplicationFactory.SingleObjectList);
            leaveApplicationFactory.CleanUp();
            _dayOffAdjustmentFactory.LogCleanUp(_dayOffAdjustmentFactory);
            _dayOffAdjustmentFactory.CleanUp();
            _employeeBenefitsFundSettingEntitlementFactory.CleanUp();
            _employeeBenefitsFundSettingFactory.CleanUp();
            _holidaySettingFactory.Cleanup();
            _holidayWorkFactory.LogCleanUp(_holidayWorkFactory);
            _holidayWorkFactory.CleanUpFactory();
            _memberEbfHistoryFactory.CleanUp();
            _memberLoanRefundFactory.CleanUp();
            _memberLoanFactory.CleanUp();
            _memberLoanApplicationFactory.CleanUp();
            _overTimeFactory.LogCleanUp(_overTimeFactory);
            _overTimeFactory.CleanUp();
            _salaryHistoryFactory.LogCleanUp(_salaryHistoryFactory);
            _salaryHistoryFactory.CleanUp();
            base.Dispose();
        }

        #endregion
    }
}
