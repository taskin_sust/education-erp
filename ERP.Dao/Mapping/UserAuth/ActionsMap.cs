﻿using FluentNHibernate.Mapping;
using NHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Mapping.UserAuth
{
    public partial class ActionsMap:BaseClassMap<Actions,long>
    {
        public ActionsMap()
        {
            Table("Actions");
            LazyLoad();
            Map(x => x.Name);
            Map(x => x.Rank).Column("Rank");

            References(x => x.AreaControllers).Column("ControllersId").LazyLoad();
            HasMany(x => x.MenuAccess).KeyColumn("ActionsId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            //HasOne(x => x.DefaultMenu).KeyColumn("ActionsId").Cascade.AllDeleteOrphan();
        }
    }
}
