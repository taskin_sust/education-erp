using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class SpecificationCriteriaLogFactory : ObjectFactoryBase<SpecificationCriteriaLog>
   {

       private readonly ISpecificationCriteriaLogService _specificationCriteriaLogService;

       public SpecificationCriteriaLogFactory(IObjectFactoryBase caller, ISession session): base(caller, session){

       _specificationCriteriaLogService = new SpecificationCriteriaLogService(session);

       }

       #region Create 

       public SpecificationCriteriaLogFactory Create(){

       Object = null;
       SingleObjectList.Add(Object);
       return this;
       }

       public SpecificationCriteriaLogFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public SpecificationCriteriaLogFactory Persist()       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _specificationCriteriaLogService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x =>{if (x.Id == 0)_specificationCriteriaLogService.SaveOrUpdate(x);});

          }
          return this;
       }

       public SpecificationCriteriaLogFactory CreateMore(int num){

       for (int i = 0; i < num; i++){
       var obj = new SpecificationCriteriaLog(){};
       ObjectList.Add(obj);}
       return this;}

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public SpecificationCriteriaLogFactory CleanUp()
       {

          DeleteAll();
          return this;
       }

       #endregion

    }
}
