﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using FluentNHibernate.Testing.Values;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class MaterialController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services & Initialization

        private readonly IMaterialService _materialService;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IMaterialTypeService _materialTypeService;
        private List<UserMenu> _userMenu;
        
        public MaterialController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _materialService = new MaterialService(session);
            _organizationService = new OrganizationService(session);
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _userService = new UserService(session);
            _materialTypeService = new MaterialTypeService(session);
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Index/Manage

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ManageMaterials()
        {
            SelectList program = null;
            SelectList organization = null;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                program = new SelectList(_programService.LoadProgram(null, null), "Id", "Name");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(e);
                _logger.Error(e);
                return View();
            }
            ViewBag.Organization = organization;
            ViewBag.Program = program;
            ViewBag.StatusText = _commonHelper.GetStatus();
            ViewBag.PageSize = Constants.PageSize;

            return View();
        }

        #endregion

        #region Render Data Table

        [HttpPost]
        public ActionResult ManageMaterials(int draw, int start, int length, string organizationId, string programId, string sessionName, string materialTypeName, string name, string status)
        {

            var data = new List<object>();
            long recordsFiltered = 0;
            int recordsTotal = 0;
            if (!Request.IsAjaxRequest()) return HttpNotFound();
            try
            {
                NameValueCollection nvc = Request.Form;

                string orderBy = "";
                string orderDir = "";

                #region OrderBy and Direction

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "organization.Name";
                            break;
                        case "1":
                            orderBy = "program.Name";
                            break;
                        case "2":
                            orderBy = "session.Name";
                            break;
                        case "3":
                            orderBy = "materialType.Name";
                            break;
                        case "4":
                            orderBy = "Name";
                            break;
                        case "5":
                            orderBy = "ShortName";
                            break;
                        case "6":
                            orderBy = "Rank";
                            break;
                        case "7":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                var material = new Material();
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                recordsTotal = _materialService.GetMaterialCount(_userMenu, orderBy, orderDir.ToUpper(), organizationId, programId, sessionName, materialTypeName, name, status);
                recordsFiltered = recordsTotal;
                List<Material> materialList = _materialService.LoadMaterial(_userMenu, start, length, orderBy, orderDir.ToUpper(), organizationId, programId, sessionName, materialTypeName, name, status).ToList();

                var maxMinRank = new int[2];
                maxMinRank[0] = _materialService.GetMinimumRank(material);//1;
                maxMinRank[1] = _materialService.GetMaximumRank(material);
                foreach (var m in materialList)
                {
                    var str = new List<string>();
                    str.Add(m.Program.Organization.ShortName);
                    str.Add(m.Program.Name);
                    str.Add((m.Session != null) ? m.Session.Name : "-");
                    str.Add((m.MaterialType != null) ? m.MaterialType.Name : "-");
                    str.Add(m.Name);
                    str.Add(m.ShortName ?? "N/A");
                    str.Add(LinkGenerator.GetRankLinkForModal(m.Id, m.Rank, maxMinRank[0], maxMinRank[1]));
                    str.Add(_commonHelper.GetSatus(m.Status.ToString()));
                    str.Add(LinkGenerator.GetGeneratedDetailsEditModelDeleteLink("Details", "Edit", "Material", m.Id, m.Name));
                    data.Add(str);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });

        }

        #endregion

        #region Operational Function

        #region Save Operation

        [HttpGet]
        public ActionResult AddMaterials()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionList = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.MateraTypelList = new SelectList(new List<MaterialType>(), "Id", "Name");
                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return HttpNotFound();

            }
        }

        [HttpPost]
        public ActionResult AddMaterials(string materialNames, string materialShortNames, long programId, long sessionId, long materialTypeId)
        {
            try
            {
                var materialList = new List<Material>();
                if (String.IsNullOrEmpty(materialNames) || String.IsNullOrEmpty(materialShortNames) || programId == 0 || sessionId == 0 || materialTypeId == 0)
                    return Json(new Response(false, "Please Enter All Fields Value"));
                var materialsNameDictionary = System.Web.Helpers.Json.Decode(materialNames);
                var materialsShortNameDictionary = System.Web.Helpers.Json.Decode(materialShortNames);

                //transfered from service
                var program = _programService.GetProgram(programId);
                var session = _sessionService.LoadById(sessionId);
                var materialType = _materialTypeService.GetMaterialType(materialTypeId);
                var i = 0;
                foreach (var materialName in materialsNameDictionary)
                {
                    var materialShortName = materialsShortNameDictionary[Convert.ToString(i)];
                    var material = new Material();
                    if (!String.IsNullOrEmpty(materialName.Value))
                    {
                        material.Name = materialName.Value;
                        material.ShortName = materialShortName;
                        material.Program = program;
                        material.Session = session;
                        material.MaterialType = materialType;
                        materialList.Add(material);
                    }
                    i++;
                }
                //transfered from service

                bool isSave = _materialService.MaterialSaveOrUpdate(materialList);
                if (isSave)
                {
                    return Json(new Response(true, "Material Add Successfully"));
                }
                return Json(new Response(false, "Failed!! Possible Duplicate Material Name Found"));
            }
            catch (DuplicateEntryException deex)
            {
                return Json(new Response(false, deex.Message));
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, WebHelper.SetExceptionMessage(e)));
            }
        }

        #endregion

        #region Update Operation

        [HttpGet]
        public ActionResult Edit(long Id)
        {

            try
            {
                var material = _materialService.GetMaterial(Id);
                InitializeViewBag(material.Program.Organization.Id, material.Program.Id, material.Session.Id, material.MaterialType.Id, material.Status);
                return View(material);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(long Id, Material material, long programId, long sessionId, long materialTypeId)
        {
            var materialStatusList = new List<SelectListItem>();
            var oldmaterial = new Material(); 
            try
            {
                var materialList = new List<Material>();
                oldmaterial = _materialService.GetMaterial(Id);
                var program = _programService.GetProgram(programId);
                var session = _sessionService.LoadById(sessionId);
                var materialType = _materialTypeService.GetMaterialType(materialTypeId);

                material.Program = program;
                material.Session = session;
                material.MaterialType = materialType;
                materialList.Add(material);
                bool isSuccessfullyUpdated = _materialService.MaterialSaveOrUpdate(materialList);
                if (isSuccessfullyUpdated)
                {
                    ViewBag.SuccessMessage = "Update Successfully";
                    InitializeViewBag(program.Organization.Id, program.Id, session.Id, materialType.Id, material.Status);
                    Material materialForView = _materialService.GetMaterial(material.Id);
                    return View(materialForView);
                    //return RedirectToAction("ManageMaterials");
                }
                ViewBag.ErrorMessage = "Not Updated";
                InitializeViewBag(oldmaterial.Program.Organization.Id, oldmaterial.Program.Id, oldmaterial.Session.Id, oldmaterial.MaterialType.Id, oldmaterial.Status);
                return View(oldmaterial);
            }
            catch (DuplicateEntryException deex)
            {
                ViewBag.ErrorMessage = deex.Message;
                InitializeViewBag(oldmaterial.Program.Organization.Id, oldmaterial.Program.Id, oldmaterial.Session.Id, oldmaterial.MaterialType.Id, oldmaterial.Status);
                return View(oldmaterial);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return HttpNotFound();
            }
        }

        #endregion

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            if (id != 0)
            {
                try
                {
                    var materialObj = _materialService.GetMaterial(id);
                    bool isDeleteSuccess = _materialService.IsDelete(materialObj);
                    if (isDeleteSuccess)
                    {
                        return Json(new Response(true, "Material Delete Successfully !"));
                    }
                    return Json(new Response(false, "Material Delete Fail !"));
                }
                catch (DependencyException ex)
                {
                    return Json(new Response(false, ex.Message));
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(e)));
                }
            }
            return HttpNotFound();
        }

        #endregion

        #region Details Operation

        [HttpGet]
        public ActionResult Details(long id)
        {
            try
            {
                long mId;
                bool isNum = long.TryParse(id.ToString(), out mId);
                if (!isNum)
                {
                    return HttpNotFound();
                }
                var material = _materialService.GetMaterial(id);
                if (material != null)
                {
                    material.CreateByText = _userService.GetUserNameByAspNetUserId(material.CreateBy);
                    material.ModifyByText = _userService.GetUserNameByAspNetUserId(material.ModifyBy);
                }
                return View(material);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(e);
                _logger.Error(e);
                return View();
            }
        }

        #endregion

        #region Rank Change Operation

        public ActionResult RankChange(int id, int current, string action)
        {
            if (!Request.IsAjaxRequest()) return HttpNotFound();
            try
            {
                Material materialUpdateObj = _materialService.GetMaterial(Convert.ToInt64(id));
                if (materialUpdateObj == null)
                {
                    return HttpNotFound();
                }
                int newRank;
                if (action == "up")
                    newRank = materialUpdateObj.Rank - 1;
                else
                    newRank = materialUpdateObj.Rank + 1;

                var materialGroupOldObj = _materialService.GetMaterialByRankNextOrPrevious(newRank, action);//LoadByRank(newRank);
                newRank = materialGroupOldObj.Rank;

                materialGroupOldObj.Rank = materialUpdateObj.Rank;
                materialUpdateObj.Rank = newRank;

                bool isSuccessfullyUpdateRank = _materialService.UpdateRank(materialGroupOldObj, materialUpdateObj);
                if (isSuccessfullyUpdateRank)
                {
                    return Json(new Response(true, "Successfully changed"));
                }
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, WebHelper.SetExceptionMessage(e)));
            }
        }

        #endregion

        #endregion

        #region Helper Function

        [HttpPost]
        public ActionResult GetMaterialType(string organization = "")
        {
            try
            {
                ViewBag.MaterialTypeList = new List<Program>();
                if (!String.IsNullOrEmpty(organization))
                {
                    ViewBag.MaterialTypeList = _materialTypeService.LoadMaterialType(Convert.ToInt64(organization.Trim()));
                }
                return PartialView("Partial/_MaterialTypeList");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, "MaterialType Loading Error (Possible Cause: Slow Internet Connection)"));
            }
        }

        private void InitializeViewBag(long organizationId = 0, long programId = 0, long sessionId = 0, long materialTypeId = 0, int status = 0)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", organizationId);
            ViewBag.ProgramList = new SelectList(_programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(organizationId)), "Id", "Name", programId);
            ViewBag.SessionList = new SelectList(_sessionService.LoadSession(_commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId)), "Id", "Name", sessionId);
            ViewBag.MaterialTypeList = new SelectList(_materialTypeService.LoadMaterialType(organizationId), "Id", "Name", materialTypeId);
            ViewBag.MaterialStatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key", status);
        }

        #endregion
    }
}