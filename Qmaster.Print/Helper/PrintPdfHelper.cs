﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ICSharpCode.SharpZipLib.Zip;
using Qmaster.Print.Forms;
using Qmaster.Print.Model;
using UdvashERP.FingerPrint.BusinessRule;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;
using System.Xml.Serialization;

namespace Qmaster.Print.Helper
{
    static class PrintPdfHelper
    {
        public static void PrintPdf(ExamMcqQuestionSetPrint examMcqQuestionSetPrint, FileInfo file, PrintConfigInfo printConfigInfo, int printCount, PrinterSettings ps, MargePdfSartEndCompare margePdf, string fileConcatNameString)
        {
            try
            {
                if (file != null && examMcqQuestionSetPrint != null)
                {
                    //string code = examMcqQuestionSetPrint.EanCheckSumCodeNo; 
                    string code = examMcqQuestionSetPrint.CodeNo; 
                    string str = file.Name.Trim();
                    str = str.Remove(str.Length - 4);
                    string oldFile = PrintConstant.QuestionPath + "\\" + str + file.Extension;
                    string newFile = PrintConstant.NewQuestionPath + "\\" + fileConcatNameString + printCount + PrintConstant.PdfFileExtension;

                    #region Bar & Qr Code Image Generate

                    iTextSharp.text.Image barCodeSignerImage = null;
                    iTextSharp.text.Image qrCodeSignerImage = null;
                    if (printConfigInfo.IsBarCode)
                    {
                        System.Drawing.Image barCodeImg = GetBitmap(code, true, printConfigInfo.BarWidth, printConfigInfo.BarHeight, showBarNo: printConfigInfo.ShowBarNo);
                        barCodeSignerImage = iTextSharp.text.Image.GetInstance(barCodeImg, color: null);
                        barCodeSignerImage.SetDpi(printConfigInfo.BarDipX, printConfigInfo.BarDipY);
                    }
                    if (printConfigInfo.IsQrCode)
                    {
                        System.Drawing.Image qrCodeImg = GetBitmap(code, false, qrWidth: printConfigInfo.QrWidth, qrHeight: printConfigInfo.QrHeight);
                        qrCodeSignerImage = iTextSharp.text.Image.GetInstance(qrCodeImg, color: null);
                        qrCodeSignerImage.SetDpi(printConfigInfo.QrDipX, printConfigInfo.QrDipY);
                    }

                    #endregion

                    if (File.Exists(newFile))
                    {
                        File.Delete(newFile);
                    }

                    #region Pdf Edit

                    PdfReader reader = new PdfReader(oldFile);
                    iTextSharp.text.Rectangle size = new iTextSharp.text.Rectangle(PageSize.A4);
                    iTextSharp.text.Document document = new iTextSharp.text.Document(size, 0f, 0f, 0f, 0f);
                    FileStream fs = new FileStream(newFile, FileMode.Create, FileAccess.Write);
                    PdfWriter writer = PdfWriter.GetInstance(document, fs);
                    document.Open();
                    int totalPage = reader.NumberOfPages;
                    string message = "";

                    if (totalPage < printConfigInfo.MinPages)
                    {
                        message = "This Pdf has total " + totalPage + " page(s), which is less than your setting Min. pages " + printConfigInfo.MinPages + " for set " + examMcqQuestionSetPrint.SetId;
                        fs.Close();
                        reader.Close();
                        fs.Dispose();
                        reader.Dispose();
                        throw new InvalidDataException(message);
                    }

                    if (totalPage > printConfigInfo.MaxPages)
                    {
                        message = "This Pdf has total " + totalPage + " page(s), which is greater than your setting Max. pages " + printConfigInfo.MaxPages + " for set " + examMcqQuestionSetPrint.SetId;
                        fs.Close();
                        reader.Close();
                        fs.Dispose();
                        reader.Dispose();
                        throw new InvalidDataException(message);
                    }

                    for (var i = 1; i <= reader.NumberOfPages; i++)
                    {
                        document.NewPage();
                        var importedPage = writer.GetImportedPage(reader, i);
                        var contentByte = writer.DirectContent;
                        contentByte.AddTemplate(importedPage, 0, 0);
                        if (i == printConfigInfo.ShowBarQrCodeOnPage)
                        {
                            if (barCodeSignerImage != null)
                            {
                                barCodeSignerImage.SetAbsolutePosition(printConfigInfo.BarAxis, printConfigInfo.BarYaxis);
                                if (i == printConfigInfo.ShowBarQrCodeOnPage)
                                    barCodeSignerImage.RotationDegrees = 90;
                                contentByte.AddImage(barCodeSignerImage, true);
                            }
                            if (qrCodeSignerImage != null)
                            {
                                qrCodeSignerImage.SetAbsolutePosition(printConfigInfo.QrAxis, printConfigInfo.QrYaxis);
                                contentByte.AddImage(qrCodeSignerImage, true);
                            }
                        }


                        //add text
                        if (printConfigInfo.ShowCodeNumber && (i == printConfigInfo.ShowCodeNumberOnPage || i == printConfigInfo.ShowCodeNumberOnPage2Nd))
                        {
                            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA_OBLIQUE, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            contentByte.BeginText();
                            contentByte.SetFontAndSize(bf, 8);
                            string text = "";
                            if (printConfigInfo.ShowCodeNumberWithExamNo)
                            {
                                text = "q" + examMcqQuestionSetPrint.ExamId + "u" + examMcqQuestionSetPrint.UniqueSet + "s" + examMcqQuestionSetPrint.SetId + "-";
                            }
                            text += code;
                            if (i == printConfigInfo.ShowCodeNumberOnPage)
                                contentByte.ShowTextAligned(Element.ALIGN_CENTER, text, printConfigInfo.CodeNumberX, printConfigInfo.CodeNumberY, 0);
                            if (i == printConfigInfo.ShowCodeNumberOnPage2Nd)
                                contentByte.ShowTextAligned(Element.ALIGN_RIGHT, text, printConfigInfo.CodeNumberX2Nd, printConfigInfo.CodeNumberY2Nd, 0);
                            contentByte.EndText();
                        }
                    }

                    document.Close();
                    fs.Close();
                    writer.Close();
                    reader.Close();

                    document.Dispose();
                    fs.Dispose();
                    writer.Dispose();
                    reader.Dispose();
                    
                    #endregion
                    
                    #region Marge Pdf into one

                    if (margePdf != null)
                    {
                        string newFileMarge = PrintConstant.MargeQuestionPath + "\\Marge" + margePdf.End + PrintConstant.PdfFileExtension;
                        if (File.Exists(newFileMarge))
                        {
                            File.Delete(newFileMarge);
                        }
                        PdfConcatenate pdfConcat = null;
                        FileStream fs2 = new FileStream(newFileMarge, FileMode.Create);
                        List<PdfReader> pdfReaderList = new List<PdfReader>();
                        try
                        {
                            pdfConcat = new PdfConcatenate(fs2);
                            for (int pdfStart = margePdf.Start; pdfStart <= margePdf.Compare; pdfStart++)
                            {
                                string newTempFilename = PrintConstant.NewQuestionPath + "\\" + fileConcatNameString + pdfStart + PrintConstant.PdfFileExtension;
                                PdfReader reader2 = new PdfReader(newTempFilename);
                                int n = reader2.NumberOfPages;
                                List<int> selectedPages = new List<int>();
                                int loopCount = n / 4;
                                int lastCount = n % 4;
                                if (loopCount > 0)
                                {
                                    for (int selectPage = 1; selectPage <= loopCount; selectPage++)
                                    {
                                        selectedPages.Add(selectPage * 4);
                                        selectedPages.Add((selectPage - 1) * 4 + 1);
                                        selectedPages.Add((selectPage - 1) * 4 + 2);
                                        selectedPages.Add((selectPage - 1) * 4 + 3);
                                    }
                                }
                                if (lastCount > 0)
                                {
                                    throw new InvalidDataException("Some Pdf found 4 less page.");
                                    //always 4 multiply page   
                                }
                                reader2.SelectPages(selectedPages);
                                pdfConcat.AddPages(reader2);
                                pdfReaderList.Add(reader2);
                            }
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                        finally
                        {
                            if (pdfConcat != null)
                                pdfConcat.Close();
                            fs2.Close();
                            if (pdfReaderList.Any())
                            {
                                foreach (PdfReader pdfReader in pdfReaderList)
                                {
                                    pdfReader.Close();
                                }
                            }

                            fs2.Dispose();
                            if (pdfReaderList.Any())
                            {
                                foreach (PdfReader pdfReader in pdfReaderList)
                                {
                                    pdfReader.Dispose();
                                }
                            }
                        }

                        if (printConfigInfo.IsPrint)
                        {
                            PrintPDF(ps, newFileMarge);
                        }

                    }
                    #endregion
                    
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static Bitmap GetBitmap(string codeString = "", bool isBarCode = false, int barWidth = 130, int barHeight = 50, int qrWidth = 50, int qrHeight = 50, bool showBarNo = true)
        {
            Bitmap res = null;

            if (isBarCode == false)
            {
                int width = qrWidth;
                int height = qrHeight;

                if (!String.IsNullOrEmpty(codeString))
                {
                    QrCodeEncodingOptions options = new QrCodeEncodingOptions
                    {
                        Width = width,
                        Height = height,
                        Margin = 0
                    };
                    ZXing.BarcodeWriter qr = new ZXing.BarcodeWriter();
                    qr.Format = ZXing.BarcodeFormat.QR_CODE;
                    qr.Options = options;
                    res = new Bitmap(qr.Write(codeString), width = width, height = height);
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(codeString))
                {
                    string barCode = codeString;
                    int width = barWidth;
                    int height = barHeight;
                    var options = new EncodingOptions { Margin = 6, Width = width, Height = height, PureBarcode = !showBarNo };
                    IBarcodeWriter writer = new BarcodeWriter
                    {
                        //Format = BarcodeFormat.EAN_13,
                        Format = BarcodeFormat.CODE_128,
                        Options = options,
                    };
                    var result = writer.Write(barCode);
                    res = new Bitmap(result, width = width, height = height);
                }
            }
            return res;
        }

        public static void SavePrintConfigInfo()
        {
            if (!File.Exists(Application.StartupPath + "\\" + PrintConstant.ConfigXmlFileName))
            {
                PrintConfigInfo printConfigInfo = new PrintConfigInfo();
                printConfigInfo.IsBarCode = true;
                printConfigInfo.IsQrCode = true;
                printConfigInfo.IsPrint = true;
                printConfigInfo.DownloadNewFile = true;
                printConfigInfo.IsDeleteAllGeneratedFile = true;
                printConfigInfo.IsDeleteUnzipFile = true;
                printConfigInfo.IsDeleteAllMargeFile = true;
                printConfigInfo.IsDeleteZipFile = true;
                printConfigInfo.ShowBarNo = false;
                printConfigInfo.BarWidth = 120;
                printConfigInfo.BarHeight = 30;
                printConfigInfo.QrWidth = 80;
                printConfigInfo.QrHeight = 60;
                printConfigInfo.BarDipX = 4800;
                printConfigInfo.BarDipY = 4800;
                printConfigInfo.QrDipX = 1200;
                printConfigInfo.QrDipY = 1200;
                printConfigInfo.MinPages = 4;
                printConfigInfo.MaxPages = 4;
                printConfigInfo.QrAxis = 80;
                printConfigInfo.QrYaxis = 710;
                printConfigInfo.BarAxis = 470;
                printConfigInfo.BarYaxis = 690;
                printConfigInfo.MargePdf = 20;
                printConfigInfo.ShowBarQrCodeOnPage = 2;
                printConfigInfo.IsOsl = true;
                printConfigInfo.IsAlpha = false;
                printConfigInfo.ShowCodeNumber = true;
                printConfigInfo.ShowCodeNumberWithExamNo = true;
                printConfigInfo.ShowCodeNumberOnPage = 1;
                printConfigInfo.CodeNumberX = 520;
                printConfigInfo.CodeNumberY = 20;
                printConfigInfo.ShowCodeNumberOnPage2Nd = 3;
                printConfigInfo.CodeNumberX2Nd = 575;
                printConfigInfo.CodeNumberY2Nd = 830;

                XmlSerializer xs = new XmlSerializer(typeof(PrintConfigInfo));
                // Create a new file stream to write the serialized object to a file
                TextWriter WriteFileStream = new StreamWriter(Application.StartupPath + "\\" + PrintConstant.ConfigXmlFileName);
                xs.Serialize(WriteFileStream, printConfigInfo);
                // Cleanup
                WriteFileStream.Close();
            }

        }

        public static bool PrintPDF(PrinterSettings ps, string filename)
        {
            try
            {
                // Now print the PDF document
                using (var document = PdfiumViewer.PdfDocument.Load(filename))
                {
                    using (var printDocument = document.CreatePrintDocument())
                    {
                        printDocument.PrinterSettings = ps;
                        printDocument.PrintController = new StandardPrintController();
                        printDocument.Print();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

    }

}
