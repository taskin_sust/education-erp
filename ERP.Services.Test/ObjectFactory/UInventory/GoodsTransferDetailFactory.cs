using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class GoodsTransferDetailFactory : ObjectFactoryBase<GoodsTransferDetail>
   {

       private readonly IGoodsTransferDetailService _goodsTransferDetailService;

       public GoodsTransferDetailFactory(IObjectFactoryBase caller, ISession session): base(caller, session){

       _goodsTransferDetailService = new GoodsTransferDetailService(session);

       }

       #region Create 

       public GoodsTransferDetailFactory Create(){

       Object = null;
       SingleObjectList.Add(Object);
       return this;
       }

       public GoodsTransferDetailFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public GoodsTransferDetailFactory Persist()       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _goodsTransferDetailService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x =>{if (x.Id == 0)_goodsTransferDetailService.SaveOrUpdate(x);});

          }
          return this;
       }

       public GoodsTransferDetailFactory CreateMore(int num){

       for (int i = 0; i < num; i++){
       var obj = new GoodsTransferDetail(){};
       ObjectList.Add(obj);}
       return this;}

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public GoodsTransferDetailFactory CleanUp()
       {

          DeleteAll();
          return this;
       }

       #endregion

    }
}
