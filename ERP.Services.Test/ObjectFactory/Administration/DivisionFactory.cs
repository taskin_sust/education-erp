﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.Services.Common;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class DivisionFactory : ObjectFactoryBase<Division>
    {
        private DivisionService _divisionService;
        public DivisionFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {            
            ObjectList = new List<Division>();            
            _divisionService = new DivisionService(_session);
        }

        public DivisionFactory WithName(string name)
        {
            Object.Name = name;
            return this;
        }

        public DivisionFactory Create()
        {
            Object = CreateDivision();
            SingleObjectList.Add(Object);
            return this;
        }

        private Division CreateDivision()
        {
            var division = new Division()
            {
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Name = "Div_" + DateTime.Now + "_" + Guid.NewGuid().ToString()
            };
            return division;
        }

        public DivisionFactory Persist()
        {
            
            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x =>
                {
                    if (x.Id == 0)
                        _divisionService.Save(x);
                });
            }

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x =>
                {
                    if (x.Id == 0)
                        _divisionService.Save(x);
                });
            }
            return this;
        }

        public void Cleanup()
        {
            DeleteAll();
        }

    }
}
