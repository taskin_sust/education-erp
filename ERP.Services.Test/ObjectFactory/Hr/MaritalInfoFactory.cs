﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class MaritalInfoFactory : ObjectFactoryBase<MaritalInfo>
    {
        private readonly IMaritalInfoService _maritalInfoService;
        private TeamMemberFactory _teamMemberFactory;
        public MaritalInfoFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _maritalInfoService = new MaritalInfoService(session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
        }

        #region Create

        public MaritalInfoFactory Create()
        {
            Object = new MaritalInfo()
            {
                //Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Department.EntityStatus.Active,
                MarriageDate = DateTime.Now,
                MaritalStatus = (int)MaritalType.Married,
            };
            SingleObjectList.Add(Object);
            return this;
        }
        
        public MaritalInfoFactory CreateWith(DateTime marriageDate, int maritalStatus)
        {
            Object = new MaritalInfo()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Department.EntityStatus.Active,
                MarriageDate = marriageDate,
                MaritalStatus = maritalStatus,
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MaritalInfoFactory CreateMore(int numOfCount)
        {
            for (int i = 0; i < numOfCount; i++)
            {
               var _object = new MaritalInfo()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = Name,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = MaritalInfo.EntityStatus.Active,
                    MarriageDate = DateTime.Now,
                    MaritalStatus = (int)MaritalType.Single,
                };
               ObjectList.Add(_object);
            }
            return this;
        }
          
        public MaritalInfoFactory WithTeamMember()
        {
            Object.TeamMember = _teamMemberFactory.Create().Persist().Object;
            return this;
        }

        public MaritalInfoFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public MaritalInfoFactory Persist()
        {

            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                    _maritalInfoService.Save(x);
            });

            ObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                    _maritalInfoService.Save(x);
            });
            
            return this;
        }

        #endregion

        #region Others

        #endregion

        #region CleanUp
        public void CleanUp()
        {            
            DeleteAll();
            //_teamMemberFactory.Cleanup();            
        }

        #endregion 

    }
}
