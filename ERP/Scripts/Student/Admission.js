﻿/*Written By Taskin */
var programId = "";
var sessionId = "";
var branchId = "";
var campusId = "";
var batchDay = "";
var batchTime = "";
var batchName = "";
var studentName = "";
var mobileNumber = "";

function StudentViewModel() {
    this.Id = 0;
    this.Name = 0;
    this.MobNumber = 0;
    this.ProgramId = 0;
    this.SessionId = "";
    this.BranchId = "";
    this.CampusId = "";
    this.BatchDays = "";
    this.BatchTime = "";
    this.BatchName = "";

    this.CourseViewModels = [];
    this.StudentViewModel = function () { };
};
function CourseViewModel() {
    this.Id = 0;
    this.Name = 0;
    this.ProgramId = "";
    this.SessionId = "";
    this.IsComplementaryCourse = false;
    this.IsTaken = false;
    this.SubjectViewModels = [];
    this.CourseViewModel = function () { };
};
function SubjectViewModel() {
    this.Id = 0;
    this.Name = 0;
    this.Payment = 0;
    this.IsTaken = false;
    this.SubjectViewModel = function () { };
};

/*Field Value Collection Scope*/
function valueCalculation() {
    studentName = $('#StudentName').val();
    mobileNumber = $('#MobileNumber').val();
    programId = $('#Program').val();
    sessionId = $('#Session').val();
    branchId = $('#Branch').val();
    campusId = $('#Campus').val();
    batchDay = $('#BatchDays').val();
    batchTime = $('#BatchTime').val();
    batchName = $('#Batch').val();
}

$(document).ready(function () {

    function ResetCampusBatchBatchDayTime() {
        $("#Campus").html("<option selected='selected'>Select Campus</option>");
        $("#Batch").html("<option selected='selected'>Select Batch Name</option>");
        $("#BatchDays").html("<option selected='selected'>Select Batch Days</option>");
        $("#BatchTime").html("<option selected='selected'>Select Batch Time</option>");
    }

    function ResetExceptProgram() {
        $("#Session").html("<option selected='selected'>Select Session</option>");
        $("#Branch").html("<option selected='selected'>Select Branch</option>");
        $("#Campus").html("<option selected='selected'>Select Campus</option>");
        $("#Batch").html("<option selected='selected'>Select Batch Name</option>");
        $("#BatchDays").html("<option selected='selected'>Select Batch Days</option>");
        $("#BatchTime").html("<option selected='selected'>Select Batch Time</option>");
    }

    $(document).on('change', '#Program', function () {

        console.log("Program changed");
        var programId = $("#Program").val();
        var sessionId = $("#Session").val();

        $.ajax({
            url: "/Student/Admission/GetSessionByProgram",
            type: "POST",
            data: { programId: programId, sessionId: sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $.unblockUI();
                ResetExceptProgram();
                $('.courseDetails').html(data.CourseView);
                $('#Session').html(data.SessionOptions);
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.unblockUI();
                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
            }
        });
    });

    $(document).on('change', '#Session', function () {

        console.log("Session changed");
        var programId = $("#Program").val();
        var sessionId = $("#Session").val();
        $.ajax({
            url: "/Student/Admission/GetBranchByProgramSession",
            type: "POST",
            data: { programId: programId, sessionId: sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $.unblockUI();
                $('.courseDetails').html(data.CourseView);
                $('#Branch').html(data.BrunchOptions);

                $("#Campus").html("<option selected='selected'>Select Campus</option>");
                $("#Batch").html("<option selected='selected'>Select Batch Name</option>");
                $("#BatchDays").html("<option selected='selected'>Select Batch Days</option>");
                $("#BatchTime").html("<option selected='selected'>Select Batch Time</option>");
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.unblockUI();
                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
            }
        });

    });

    $(document).on('change', '#Branch', function () {

        console.log("Branch changed");

        var branchId = $("#Branch").val();
        var campusId = $("#Campus").val();

        $.ajax({
            url: "/Student/Admission/GetCampusByProgramSessionAndBranch",
            type: "POST",
            data: { branchId: branchId, campusId: campusId },

            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $.unblockUI();
                $('#Campus').html(data.CampusOptions);
                $("#Batch").html("<option selected='selected'>Select Batch Name</option>");
                $("#BatchDays").html("<option selected='selected'>Select Batch Days</option>");
                $("#BatchTime").html("<option selected='selected'>Select Batch Time</option>");
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.unblockUI();
                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
            }
        });

    });

    /*Campus Update According to Branch */
    $(document).on('change', '#Campus', function () {

        console.log("Campus changed");

        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var branchId = $('#Branch').val();
        var campusId = $('#Campus').val();
        /*var batchDay = $('#BatchDays').val();
        var batchTime = $('#BatchTime').val();
        var batchId = $('#Batch').val();*/

        if (branchId.length > 0) {
            $.ajax({
                url: "/Student/Admission/GetBatchDayByProgramSessionBranchAndCampus",
                type: "POST",
                data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {

                    $.unblockUI();
                    $('#BatchDays').html(data.BatchDayOptioins);
                    $('#BatchTime').html("<option selected='selected'>Select Batch Time</option>");
                    $('#Batch').html("<option selected='selected'>Select Batch Name</option>");
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    $(document).on('change', '#BatchDays', function () {

        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var branchId = $('#Branch').val();
        var campusId = $('#Campus').val();
        var batchDay = $('#BatchDays').val();

        if (branchId.length > 0) {
            $.ajax({
                url: "/Student/Admission/GetBatchTimeByProgramSessionBranchCampusAndBatchDay",
                type: "POST",
                data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId, batchDay: batchDay },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {

                    $.unblockUI();
                    $('#BatchTime').html(data.BatchTimeOptions);
                    $('#Batch').html("<option selected='selected'>Select Batch Name</option>");

                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    $(document).on('change', '#BatchTime', function () {

        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var branchId = $('#Branch').val();
        var campusId = $('#Campus').val();
        var batchDay = $('#BatchDays').val();
        var batchTime = $('#BatchTime').val();

        if (branchId.length > 0) {
            $.ajax({
                url: "/Student/Admission/GetBatchByProgramSessionBranchCampusAndBatchDayTime",
                type: "POST",
                data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId, batchDay: batchDay, batchTime: batchTime },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    $.unblockUI();
                    $('#Batch').html(data.BatchOptions);
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    function ValidateNewRegistrationForm() {

        var isValid = true;

        var studentName = $('#StudentName').val();
        var mobileNumber = $('#MobileNumber').val();
        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var branchId = $('#Branch').val();
        var campusId = $('#Campus').val();
        var batchDay = $('#BatchDays').val();
        var batchTime = $('#BatchTime').val();
        var batchName = $('#Batch').val();

        if (studentName.length <= 0) { $('#StudentName').addClass('highlight'); isValid = false; }
        if (mobileNumber.length <= 0) { $('#MobileNumber').addClass('highlight'); isValid = false; }
        if (programId.length <= 0) { $('#Program').addClass('highlight'); isValid = false; }
        if (sessionId.length <= 0) { $('#Session').addClass('highlight'); isValid = false; }
        if (branchId.length <= 0) { $('#Branch').addClass('highlight'); isValid = false; }
        if (campusId.length <= 0) { $('#Campus').addClass('highlight'); isValid = false; }
        if (batchDay.length <= 0) { $('#BatchDays').addClass('highlight'); isValid = false; }
        if (batchTime.length <= 0) { $('#BatchTime').addClass('highlight'); isValid = false; }
        if (batchName.length <= 0) { $('#batch').addClass('highlight'); isValid = false; }

        return isValid;
    }

    /*Finally send to server for Student Registration with Program assign */
    $(document).on('click', '#saveStudent', function () {

        if (ValidateNewRegistrationForm()) {

            var student = new StudentViewModel();

            student.Name = studentName.trim();
            student.MobNumber = mobileNumber.trim();
            student.ProgramId = programId.trim();
            student.SessionId = sessionId.trim();
            student.BranchId = branchId.trim();
            student.CampusId = campusId.trim();
            student.BatchDays = batchDay.trim();
            student.BatchTime = batchTime.trim();
            student.BatchName = batchName.trim();

            $('.panel-heading input:checkbox').each(function (index, value) {

                if ($(this).is(':checked')) {
                    var courseDetail = new CourseViewModel();

                    var currentCheckboxClassParent = $(this).parent();
                    var courseName = currentCheckboxClassParent.next().attr('data-bodyname');

                    /*CourseViewModel object form */
                    courseDetail.Name = courseName.trim();
                    courseDetail.ProgramId = programId;
                    courseDetail.SessionId = sessionId;
                    var innerCheckboxClassName = currentCheckboxClassParent.next().attr('class');
                    var spil = innerCheckboxClassName.split(" ");
                    //alert(innerCheckboxClassName);
                    console.log("FLOG " + spil[1]);
                    //innerCheckboxClassName = innerCheckboxClassName.replace(/\s+/g, "");
                    //$(this).addClass(innerCheckboxClassName);

                    //console.log("SECLOG" +innerCheckboxClassName);
                    $('.' + spil[1] + ' input[type=checkbox]').each(function (index, value) {
                        console.log("Entered");
                        if ($(this).is(':checked')) {
                            console.log("Entered into checkbox");
                            /*Subject Object Form*/
                            var subject = new SubjectViewModel();
                            subject.Id = $(this).attr('data-coursesubjectid');
                            subject.Name = $(this).attr('data-subjectname');
                            subject.Payment = $(this).attr('data-payment');
                            courseDetail.SubjectViewModels.push(subject);
                        }
                    });
                    student.CourseViewModels.push(courseDetail);
                }
            });

            var studentObj = JSON.stringify(student);
            //console.log(studentObj);
            $.ajax({
                url: "/Student/Admission/StudentRegistration",
                type: "POST",
                data: { studentObj: studentObj },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    if (data.IsSuccess) {
                        var stdProgramId = data.Message;
                        $.ajax({
                            url: "/Student/Payment/DuePayment",
                            type: "POST",
                            data: { stdProgramId: stdProgramId },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data1) {
                                //alert(data);
                                $('.totalPage').html(data1);
                                $.unblockUI();
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (data1) {
                                bootbox.alert(data1.Message).css('margin-top', (($(window).height() / 4)));
                            }
                        });

                    } else {
                        bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                }
            });
            console.log(student);
        }
        else {

            $(".customMessage").html('<div class="alert alert-danger"> <a class="close" data-dismiss="alert"></a>You need to fillup required field</div>');

            //bootbox.alert("Field Missing").css('margin-top', (($(window).height() / 4)));
        }
    });

    /*Only New Program Assign to an Existing Student*/
    $(document).on('click', '#assignProgram', function () {

        valueCalculation();

        var isReqSendToServer = true;

        if (studentName.length <= 0) {
            $('#StudentName').addClass('highlight');
            isReqSendToServer = false;
        }

        if (mobileNumber.length <= 0) { $('#MobileNumber').addClass('highlight'); isReqSendToServer = false; }
        if (programId.length <= 0) { $('#program').addClass('highlight'); isReqSendToServer = false; }
        if (sessionId.length <= 0) { $('#session').addClass('highlight'); isReqSendToServer = false; }
        if (branchId.length <= 0) { $('#branch').addClass('highlight'); isReqSendToServer = false; }
        if (campusId.length <= 0) { $('#campus').addClass('highlight'); isReqSendToServer = false; }
        if (batchDay.length <= 0) { $('#batchDays').addClass('highlight'); isReqSendToServer = false; }
        if (batchTime.length <= 0) { $('#batchTime').addClass('highlight'); isReqSendToServer = false; }
        if (batchName.length <= 0) { $('#batch').addClass('highlight'); isReqSendToServer = false; }

        var stdId = $('#studentId').val();
        if (isReqSendToServer) {
            var student = new StudentViewModel();
            student.Name = studentName.trim();
            student.MobNumber = mobileNumber.trim();
            student.ProgramId = programId.trim();
            student.SessionId = sessionId.trim();
            student.BranchId = branchId.trim();
            student.CampusId = campusId.trim();
            student.BatchDays = batchDay.trim();
            student.BatchTime = batchTime.trim();
            student.BatchName = batchName.trim();

            $('.panel-heading input:checkbox').each(function (index, value) {
                if ($(this).is(':checked')) {
                    var courseDetail = new CourseViewModel();

                    var currentCheckboxClassParent = $(this).parent();
                    var courseName = currentCheckboxClassParent.next().attr('data-bodyname');

                    /*CourseViewModel object form */
                    courseDetail.Name = courseName.trim();
                    courseDetail.ProgramId = programId;
                    courseDetail.SessionId = sessionId;
                    var innerCheckboxClassName = currentCheckboxClassParent.next().attr('class');
                    var spil = innerCheckboxClassName.split(" ");

                    $('.' + spil[1] + ' input[type=checkbox]').each(function (index, value) {
                        console.log("Entered");
                        if ($(this).is(':checked')) {
                            console.log("Entered into checkbox");
                            /*Subject Object Form*/
                            var subject = new SubjectViewModel();
                            subject.Id = $(this).attr('data-coursesubjectid');
                            subject.Name = $(this).attr('data-subjectname');
                            subject.Payment = $(this).attr('data-payment');
                            courseDetail.SubjectViewModels.push(subject);
                        }
                    });
                    student.CourseViewModels.push(courseDetail);
                }
            });
            var studentObj = JSON.stringify(student);
            $.ajax({
                url: "/Student/Admission/NewProgramRegistration",
                type: "POST",
                data: { studentObj: studentObj, stdId: stdId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    //alert(data.Message);
                    if (data.IsSuccess) {
                        var stdProgramId = data.Message;
                        $.unblockUI();

                        $.ajax({
                            url: "/Student/Payment/DuePayment",
                            type: "POST",
                            data: { stdProgramId: stdProgramId },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data1) {
                                //alert(data);
                                $('.totalPage').html(data1);
                                $.unblockUI();
                            },

                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (data1) {
                                bootbox.alert(data1.Message).css('margin-top', (($(window).height() / 4)));
                            }
                        });
                    } else {
                        bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                    }

                },

                complete: function () {
                    $.unblockUI();
                },

                error: function (data) {
                    bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                }
            });
            console.log(student);
        }
        else {
            bootbox.alert("Field Missing").css('margin-top', (($(window).height() / 4)));
        }
    });

    /*Disable All checkBox */
    function DisbaleAllCheckBox() {
        $('.course-name-check').each(function (index, value) {

            $(this).attr('disabled', 'disabled');
        });
    }

    function DisableAllCheckBoxAtAddNewCourseForm() {

        $('.course-name-check').each(function (index, value) {
            $(this).attr('disabled', 'disabled');
        });

        $('.course-subject-check').each(function (index, value) {
            $(this).attr('disabled', 'disabled');
        });

    }

    ///* Add New Courses To a Student */
    //$(document).on('click', '#AddCourseToStudent', function () {

    //    var stdProRoll = $('#stdProRoll').val();
    //    var student = new StudentViewModel();

    //    $('.course-name-check').each(function (index, value) {

    //        var courseId = $(this).attr('data-course-id');

    //        if ($(this).is(':checked')) {

    //            var courseDetail = new CourseViewModel();
    //            courseDetail.Name = $(this).attr("data-course-name");
    //            courseDetail.ProgramId = $(this).attr('data-program-id');
    //            courseDetail.SessionId = $(this).attr('data-session-id');
    //            courseDetail.Id = courseId;
    //            var takenSubjectCount = 0;

    //            $('.course-' + courseId + '-subjects').each(function (index, value) {
    //                console.log("Entered");
    //                if ($(this).is(':checked') && $(this).attr('disabled') != 'disabled') {
    //                    var subject = new SubjectViewModel();
    //                    subject.Id = $(this).attr('data-course-subject-id');
    //                    subject.Name = $(this).attr('data-course-subject-name');
    //                    subject.Payment = $(this).attr('data-course-subject-payment');
    //                    courseDetail.SubjectViewModels.push(subject);
    //                    $(this).attr("disabled", "disabled");
    //                    takenSubjectCount++;
    //                } else {
    //                    var subjectId = $(this).attr('data-course-subject-id');
    //                    $(".course-" + courseId + "-subject-" + subjectId + "-container").remove();
    //                }
    //            });

    //            $(this).attr("disabled", "disabled");
    //            if (takenSubjectCount == 0) {
    //                $("#course-" + courseId + "-container").remove();
    //                $(".course-" + courseId + "-subject-container").remove();
    //            }
    //            student.CourseViewModels.push(courseDetail);

    //        } else {
    //            $("#course-" + courseId + "-container").remove();
    //            $(".course-" + courseId + "-subject-container").remove();
    //        }
    //    });

    //    var studentObj = JSON.stringify(student);
    //    console.log(studentObj);
    //    //DisableAllCheckBoxAtAddNewCourseForm();

    //    $.ajax({

    //        url: "/Student/AddCourse/NewCourseAssigned",
    //        type: "POST",
    //        data: { studentObj: studentObj, stdProRoll: stdProRoll },
    //        beforeSend: function () {
    //            $.blockUI({
    //                timeout: 0,
    //                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //            });
    //        },
    //        success: function (data) {

    //            $('.paymentDue').html(data);
    //            $.unblockUI();

    //            $("#AddCourseToStudent").hide();

    //            if (data.IsSuccess) {
    //                //$("#nextButtonContainer").hide();
    //                var stdProgramId = data.Message;
    //            } else {
    //                bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
    //            }
    //        },
    //        complete: function () {
    //            $.unblockUI();
    //        },

    //        error: function (data) {
    //            bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
    //        }
    //    });
    //    console.log(student);
    //});

    /* Cancel Courses To Student */
    $(document).on('click', '#CancelCourseToStudent', function () {
        valueCalculation();
        //var isReqSendToServer = true;
        var stdProRoll = $('#stdProRoll').val();
        var student = new StudentViewModel();
        $('.panel-heading input:checkbox').each(function (index, value) {
            //if ($(this).is(':checked')) {
            var courseDetail = new CourseViewModel();
            var currentCheckboxClassParent = $(this).parent();
            var courseName = currentCheckboxClassParent.next().attr('data-bodyname');

            /*CourseViewModel object form */
            courseDetail.Name = courseName.trim();
            courseDetail.ProgramId = programId;
            courseDetail.SessionId = sessionId;
            courseDetail.Id = $(this).attr('data-course-id');
            var innerCheckboxClassName = currentCheckboxClassParent.next().attr('class');
            var spil = innerCheckboxClassName.split(" ");

            console.log("FLOG " + spil[1]);

            $('.' + spil[1] + ' input[type=checkbox]').each(function (index, value) {

                if ($(this).is(':checked')) {
                    console.log("Entered");
                    //console.log("Entered into checkbox");
                    /*Subject Object Form*/
                } else {
                    console.log("Unchecked Entered");
                    var subject = new SubjectViewModel();
                    subject.Id = $(this).attr('data-course-subject-id');
                    subject.Name = $(this).attr('data-course-subject-name');
                    subject.Payment = $(this).attr('data-course-subject-payment');
                    courseDetail.SubjectViewModels.push(subject);
                }
            });
            student.CourseViewModels.push(courseDetail);
            //}
        });
        console.log(student);
        var studentObj = JSON.stringify(student);
        //console.log(studentObj);
        DisbaleAllCheckBox();
        $.ajax({
            url: "/Student/CancelCourse/CourseCanceledAndPaymentCalculation",
            type: "POST",
            data: { studentObj: studentObj, stdProRoll: stdProRoll },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },

            success: function (data) {

                if (data.IsSuccess) {
                    $('.paymentDueAfterCancellingCourseOrSubject').html(data);
                    $.unblockUI();
                    //var stdProgramId = data.Message;
                } else {
                    $.fn.customMessage({
                        displayMessage: data.Message,
                        displayMessageType: "e"
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
            }
        });
        console.log(student);
    });

    ///*Cancel Course Payment Part */
    //$(document).on('click', '#courseCancelPayment', function () {
    //    valueCalculation();
    //    $('#nonRefundableAmount').removeClass('highlight');
    //    $('#netRefundableAmount').removeClass('highlight');
    //    $('#referrerenceNote').removeClass('highlight');

    //    var isReqSendToServer = true;
    //    var student = new StudentViewModel();
    //    $('.panel-heading input:checkbox').each(function (index, value) {
    //        //if ($(this).is(':checked')) {
    //        var courseDetail = new CourseViewModel();
    //        var currentCheckboxClassParent = $(this).parent();
    //        var courseName = currentCheckboxClassParent.next().attr('data-bodyname');

    //        /* CourseViewModel object form */
    //        courseDetail.Name = courseName.trim();
    //        courseDetail.ProgramId = programId;
    //        courseDetail.SessionId = sessionId;
    //        courseDetail.Id = $(this).attr('data-course-id');
    //        var innerCheckboxClassName = currentCheckboxClassParent.next().attr('class');
    //        var spil = innerCheckboxClassName.split(" ");

    //        console.log("FLOG " + spil[1]);

    //        $('.' + spil[1] + ' input[type=checkbox]').each(function (index, value) {

    //            if ($(this).is(':checked')) {
    //                console.log("Entered WRONG");
    //                /*Subject Object Form*/
    //            } else {
    //                console.log("Entered RIGHT");
    //                var subject = new SubjectViewModel();
    //                subject.Id = $(this).attr('data-course-subject-id');
    //                subject.Name = $(this).attr('data-course-subject-name');
    //                subject.Payment = $(this).attr('data-course-subject-payment');
    //                courseDetail.SubjectViewModels.push(subject);
    //            }
    //        });
    //        student.CourseViewModels.push(courseDetail);
    //        // }
    //    });
    //    console.log(student);
    //    var studentObj = JSON.stringify(student);
    //    var stdPId = $('#stdProRoll').val();

    //    var dueAmount = $('#dueAmount').val();
    //    var nonRFund = $('#nonRefundableAmount').val();
    //    var netRFund = $('#netRefundableAmount').val();
    //    var referrer = $('#REFERRERLIST').val();
    //    var referrerenceNote = $('#referrerenceNote').val();
    //    console.log(nonRFund);
    //    console.log(netRFund);
    //    console.log(dueAmount);
    //    if (nonRFund.length <= 0) { $('#nonRefundableAmount').addClass('highlight'); isReqSendToServer = false; }
    //    if (netRFund.length <= 0) { $('#netRefundableAmount').addClass('highlight'); isReqSendToServer = false; }
    //    if (referrerenceNote.length <= 0) { $('#referrerenceNote').addClass('highlight'); isReqSendToServer = false; }
    //    //nonRFund = Number(nonRFund.replace(/[^0-9\.]+/g, ""));
    //    //dueAmount = Number(dueAmount.replace(/[^0-9\.]+/g, ""));
    //    //netRFund = Number(netRFund.replace(/[^0-9\.]+/g, ""));
    //    //console.log(nonRFund + netRFund + dueAmount);
    //    if (isReqSendToServer) {
    //        $.ajax({
    //            url: "/Student/CancelCourse/CourseCanceledAndPaymentCalculation",
    //            type: "POST",
    //            data: { studentObj: studentObj, stdPId: stdPId, dueAmount: dueAmount, nonRFund: nonRFund, netRFund: netRFund, referrer: referrer, referrerenceNote: referrerenceNote },
    //            beforeSend: function () {
    //                $.blockUI({
    //                    timeout: 0,
    //                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //                });
    //            },
    //            success: function (data) {

    //                if (data.IsSuccess) {
    //                    //$('#mainBody').html(data.Message);
    //                    //$('.totalPage').html(data.Message);

    //                    $.unblockUI();
    //                    var link = "/Student/Payment/GenerateMoneyReciept?id=" + data.Message + "";
    //                    console.log(link);
    //                    //alert(link);
    //                    /*Call BAck START*/
    //                    window.location.href = link;
    //                    /*END */
    //                } else {
    //                    $.fn.customMessage({
    //                        displayMessage: data.Message,
    //                        displayMessageType: "e",
    //                        //displayTime: 5000
    //                    });
    //                    //$('.customMessage').append(data.Message);
    //                }
    //            },
    //            complete: function () {
    //                $.unblockUI();
    //            },
    //            error: function (data) {
    //                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
    //            }
    //        });
    //    } else {
    //        //$.fn.customMessage({
    //        //    displayMessage: "Input Field Value Missing",
    //        //    displayMessageType: "e",
    //        //});
    //    }

    //});

});