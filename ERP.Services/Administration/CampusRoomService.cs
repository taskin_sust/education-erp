﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Administration
{
    public interface ICampusRoomService : IBaseService
    {
        #region Operational Function

        CampusRoom Save(CampusRoom room);

        #endregion

        #region Single Instances Loading Function

        CampusRoom GetCampusRoom(long id);
        
        #endregion

        #region List Loading Function
        IList<CampusRoom> LoadCampusRoom(long branchId);
        IList<CampusRoom> LoadCampusRoom(string[] campusIds);
        #endregion

        #region Others Function

        #endregion
    }
    public class CampusRoomService : BaseService, ICampusRoomService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ICampusRoomDao _campusRoomDao;
        public CampusRoomService(ISession session)
        {
            Session = session;
            _campusRoomDao = new CampusRoomDao() { Session = session };
        }
        #endregion

        #region Operational Function
        public CampusRoom Save(CampusRoom room)
        {
            _campusRoomDao.Save(room);
            return room;
        }

        #endregion

        #region Single Instances Loading Function
        public CampusRoom GetCampusRoom(long id)
        {
            try
            {
                return _campusRoomDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
         
        #endregion

        #region List Loading Function
        public IList<CampusRoom> LoadCampusRoom(long branchId)
        {
            try
            {
                return _campusRoomDao.LoadByBranchId(branchId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<CampusRoom> LoadCampusRoom(string[] campusIds)
        {
            try
            {
                return _campusRoomDao.LoadByCampusIds(campusIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}