﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.OpeningBalanceTest
{
    public class ItemOpeningBalanceBaseTest : TestBase, IDisposable
    {
        #region  Object Initialization
        private ISession _session;
        public GoodsReceiveFactory _goodsReceiveFactory;
        public GoodsReceiveDetailsFactory _goodsReceiveDetailsFactory;
        public GoodsReceiveService _goodsReceiveService;
        internal ProgramFactory _ProgramFactory;
        internal SessionFactory _SessionFactory;
        internal BranchFactory _BranchFactory;
        internal ItemFactory _itemFactory;
        internal ProgramSessionItemFactory _programSessionItemFactory;

        internal readonly CommonHelper CommonHelper;
        public ItemOpeningBalanceBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _goodsReceiveFactory = new GoodsReceiveFactory(null, _session);
            _goodsReceiveDetailsFactory = new GoodsReceiveDetailsFactory(null, _session);
            _ProgramFactory = new ProgramFactory(null, _session);
            _BranchFactory = new BranchFactory(null, _session);
            _SessionFactory = new SessionFactory(null, _session);
            _itemFactory = new ItemFactory(null, _session);
            _programSessionItemFactory = new ProgramSessionItemFactory(null, _session);
            _goodsReceiveService = new GoodsReceiveService(_session);
            CommonHelper = new CommonHelper();

        }

        #endregion

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        #region CleanUp

        public void CleanupStockSummary(List<long> programIdList, List<long> sessionIdList, List<long> branchIdList, List<long> itemIdList)
        {
            string query = " BranchId IN(" + string.Join(",", branchIdList) + ") AND ItemId IN(" + string.Join(",", itemIdList) + ")";
            if (programIdList.Count > 0)
            {
                query += "AND ProgramId IN(" + string.Join(",", programIdList) + ")";
            }
            if (sessionIdList.Count > 0)
            {
                query += "AND SessionId IN(" + string.Join(",", sessionIdList) + ")";
            }

            _session.CreateSQLQuery("Delete from UINV_CurrentStockSummary where " + query).ExecuteUpdate();
        }


        public void Dispose()
        {
            _programSessionItemFactory.CleanUp();
            CleanupStockSummary(_ProgramFactory.SingleObjectList.Select(x => x.Id).ToList(), _SessionFactory.SingleObjectList.Select(x => x.Id).ToList(), _BranchFactory.SingleObjectList.Select(x => x.Id).ToList(), _itemFactory.SingleObjectList.Select(x => x.Id).ToList());
            _goodsReceiveDetailsFactory.CleanUp();
            _goodsReceiveFactory.CleanUp();

            _ProgramFactory.Cleanup();
            _SessionFactory.Cleanup();
            _BranchFactory.Cleanup();
            _itemFactory.CleanUp();
            organizationFactory.Cleanup();
            base.Dispose();
        }

        #endregion
    }
}
