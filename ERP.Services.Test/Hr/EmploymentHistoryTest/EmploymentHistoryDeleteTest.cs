﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.EmploymentHistoryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "EmploymentHistory")]
    public class EmploymentHistoryDeleteTest : EmploymentHistoryBaseTest
    {
        #region Basic Test

        [Fact]
        public void DeleteEmploymentHistory_Should_Done_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryService.Save(_employmentHistoryFactory.Object);
            teamMember.EmploymentHistory.Add(_employmentHistoryFactory.Object);
            bool result = _employmentHistoryService.DeleteEmploymentHistory(_employmentHistoryFactory.Object);
            Assert.True(result);
        }

        #endregion

        #region Business logic test


        [Fact]
        public void DependencyException_DeleteEmploymentHistory_For_EmploymentHistory()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryService.Save(_employmentHistoryFactory.Object);
            Assert.Throws<DependencyException>(() => _employmentHistoryService.DeleteEmploymentHistory(_employmentHistoryFactory.Object));
        }


        #endregion
    }
}
