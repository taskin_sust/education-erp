﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.JobExperienceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "JobExperience")]
    public class JobExperienceDeleteTest : JobExperienceBaseTest
    {
        [Fact]
        public void Delete_Should_Return_True()
        {
            var teamMemberObj = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            jobExperienceService.UpdateJobExperienceInfo(teamMemberObj, objFactory.SingleObjectList);
            Assert.True(jobExperienceService.DeleteJobExperienceInfo(objFactory.Object.Id));
        }

        [Fact]
        public void Delete_Should_Return_Invalid_Id()
        {
            var teamMemberObj = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            jobExperienceService.UpdateJobExperienceInfo(teamMemberObj, objFactory.SingleObjectList);
            Assert.Throws<InvalidDataException>(() => jobExperienceService.DeleteJobExperienceInfo(0));
        }

    }
}
