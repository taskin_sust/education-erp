﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IMaterialDao : IBaseDao<Material, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<Material> LoadMaterial(long sessionId, long programId);
        IList<Material> LoadMaterial(long[] materialTypeId, long programId, long sessionId);
        IList<Material> LoadMaterial(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionName, string materialTypeName, string name, string status);
        #endregion

        #region Others Function
        bool IsItDuplicateMaterialByProgramAndSessionAndMaterialTypeValue(Program program, Session session, MaterialType materialType, string name, string shortName);
        bool IsItDuplicateMaterialForUpdatingByMaterial(long id, Program program, Session session, MaterialType materialType, string name, string shortName);
        int GetMaterialCount(string orderBy, string orderDir, List<long> organizationIdList , List<long> programIdList , string sessionName, string materialTypeName, string name, string status);
        long HasManyMaterialTypeObject(long materialTypeId);
        #endregion
    }

    public class MaterialDao : BaseDao<Material, long>, IMaterialDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<Material> LoadMaterial(long[] materialTypeId, long programId, long sessionId)
        {
            var returnObj = Session.QueryOver<Material>().Where(x => x.Status == Material.EntityStatus.Active && x.Program.Id == programId && x.Session.Id == sessionId);
            bool isAllSelect = Array.Exists(materialTypeId, item => item == SelectionType.SelelectAll);
            if (!isAllSelect)
            {
                returnObj = returnObj.WhereRestrictionOn(x => x.MaterialType.Id).IsIn(materialTypeId);
            }
            IList<Material> returnMaterialList = returnObj.List<Material>();
            return returnMaterialList;
        }
        public IList<Material> LoadMaterial(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionName, string materialTypeName, string name, string status)
        {
            ICriteria criteria = GetLoadMaterialCriteria(orderBy, orderDir, organizationIdList, programIdList, sessionName, materialTypeName, name, status);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Material>();
        }
        public IList<Material> LoadMaterial(long sessionId, long programId)
        {
            var returnObj = Session.QueryOver<Material>()
                   .Where(x => x.Program.Id == programId && x.Session.Id == sessionId && x.Status == Material.EntityStatus.Active)
                   .List<Material>();
            return returnObj;
        }
        #endregion

        #region Others Function
        public bool IsItDuplicateMaterialByProgramAndSessionAndMaterialTypeValue(Program program, Session session, MaterialType materialType, string name, string shortName)
        {
            var materialCount =
                Session.QueryOver<Material>()
                    .Where(
                        x =>
                            x.Program.Id == program.Id && x.Session.Id == session.Id && x.MaterialType.Id == materialType.Id &&
                            x.Name == name && x.Status != Material.EntityStatus.Delete && x.ShortName == shortName).RowCount();//.SingleOrDefault<Material>();
            if (materialCount > 0)//if (material != null)
            {
                return true;
            }
            return false;
        }
        public bool IsItDuplicateMaterialForUpdatingByMaterial(long id, Program program, Session session, MaterialType materialType, string name, string shortName)
        {
            int materialCount =
                Session.QueryOver<Material>()
                    .Where(
                        x =>
                            x.Program == program && x.Session == session && x.MaterialType == materialType &&
                            x.Name == name && x.Status != Program.EntityStatus.Delete && x.Id != id && x.ShortName == shortName).RowCount();
            if (materialCount > 0)
            {
                return true;
            }
            return false;
        }
        public int GetMaterialCount(string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionName, string materialTypeName, string name, string status)
        {
            ICriteria criteria = GetLoadMaterialCriteria(orderBy, orderDir, organizationIdList, programIdList, sessionName, materialTypeName, name, status, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
        public long HasManyMaterialTypeObject(long materialTypeId)
        {
            var returnObj = Session.QueryOver<Material>().Where(x => x.MaterialType.Id == materialTypeId && x.Status != Material.EntityStatus.Delete).List<Material>();
            return returnObj.Count;
        }

        #endregion

        #region Helper Function
        private ICriteria GetLoadMaterialCriteria(string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionName, string materialTypeName, string name, string status,
           bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<Material>().Add(Restrictions.Not(Restrictions.Eq("Status", Material.EntityStatus.Delete)));
            criteria.CreateAlias("Program", "program").Add(Restrictions.Eq("program.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Session", "session").Add(Restrictions.Eq("session.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("program.Organization", "organization").Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active));
            criteria.CreateAlias("MaterialType", "materialType").Add(Restrictions.Eq("materialType.Status", MaterialType.EntityStatus.Active));

            //if (!String.IsNullOrEmpty(organizationId))
            //{
            //    criteria.Add(Restrictions.Eq("organization.Id", Convert.ToInt64(organizationId.Trim())));
            //}
            //if (!String.IsNullOrEmpty(programId))
            //{
            //    criteria.Add(Restrictions.Eq("Program.Id", Convert.ToInt64(programId.Trim())));
            //}
            if (organizationIdList != null)
            {
                criteria.Add(Restrictions.In("organization.Id", organizationIdList));
            }
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("Program.Id", programIdList));
            }
            if (!String.IsNullOrEmpty(sessionName))
            {
                criteria.Add(Restrictions.Like("session.Name", sessionName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(materialTypeName))
            {
                criteria.Add(Restrictions.Like("materialType.Name", materialTypeName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.InsensitiveLike("Name", name.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }
        #endregion
    }
}