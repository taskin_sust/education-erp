﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Hr
{
    public class SalarySheetMap : BaseClassMap<SalarySheet,long>
    {
        public SalarySheetMap()
        {
            Table("HR_SalarySheet");
            LazyLoad();
            Map(x => x.Year).Column("Year");
            Map(x => x.Month).Column("Month");
            Map(x => x.BasicSalaryAmount).Column("BasicSalaryAmount");
            Map(x => x.HouseRentAmount).Column("HouseRentAmount");
            Map(x => x.ConveyanceAmount).Column("ConveyanceAmount");
            Map(x => x.MedicalAmount).Column("MedicalAmount");
            Map(x => x.GrossSalaryAmount).Column("GrossSalaryAmount");
            Map(x => x.IncreamentAmount).Column("IncreamentAmount");
            Map(x => x.ArrearAmount).Column("ArrearAmount");
            Map(x => x.TotalSalaryAmount).Column("TotalSalaryAmount");
            Map(x => x.AutoZoneAmount).Column("AutoZoneAmount");
            Map(x => x.FinalZoneAmount).Column("FinalZoneAmount");
            Map(x => x.LeaveWithoutPayAmount).Column("LeaveWithoutPayAmount");
            Map(x => x.AutoLoanRefund).Column("AutoLoanRefund");
            Map(x => x.FinalLoanRefund).Column("FinalLoanRefund");
            Map(x => x.TdsAmount).Column("TdsAmount");
            Map(x => x.BankAmount).Column("BankAmount");
            Map(x => x.CashAmount).Column("CashAmount");
            Map(x => x.TotalAmount).Column("TotalAmount");
            Map(x => x.Remarks).Column("Remarks");
            Map(x => x.IsSubmit).Column("IsSubmit");
            Map(x => x.StartDate).Column("StartDate");
            Map(x => x.EndDate).Column("EndDate");
            Map(x => x.AutoAbsentAmount).Column("AutoAbsentAmount");
            Map(x => x.FinalAbsentAmount).Column("FinalAbsentAmount");
            Map(x => x.EbfAmount).Column("EbfAmount");
            Map(x => x.EbfForEmplyer).Column("EbfForEmplyer");
            References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.SalaryHistory).Column("SalaryHistoryId");
            References(x => x.SalaryOrganization).Column("SalaryOrganizationId");
            References(x => x.SalaryBranch).Column("SalaryBranchId");
            References(x => x.SalaryCampus).Column("SalaryCampusId");
            References(x => x.SalaryDepartment).Column("SalaryDepartmentId");
            References(x => x.SalaryDesignation).Column("SalaryDesignationId");
            References(x => x.JobOrganization).Column("JobOrganizationId");
            References(x => x.JobBranch).Column("JobBranchId");
            References(x => x.JobCampus).Column("JobCampusId");
            References(x => x.JobDepartment).Column("JobDepartmentId");
            References(x => x.JobDesignation).Column("JobDesignationId");
            HasMany(x => x.SalarySheetDetailses).KeyColumn("SalarySheetId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.MemberEbfHistories).KeyColumn("SalarySheetId").Cascade.AllDeleteOrphan().Inverse();
        }

    }
}
