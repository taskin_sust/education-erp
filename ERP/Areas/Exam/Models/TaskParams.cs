﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UdvashERP.BusinessModel.Dto.Exam;

namespace UdvashERP.Areas.Exam.Models
{
    public class TaskParams
    {
        public ExamMcqQuestionSetPdfDto ExamMcqQuestionSetPdfDto { get; set; }
        public int SetId { get; set; }
        public long ExamId { get; set; }
        public int Version { get; set; }
        public string FileDir { get; set; }
    }
}