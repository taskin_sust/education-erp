﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.Administration.BankBranchTest
{
    public class BankBranchBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal IBankService _bankService;
        internal IBankBranchService _bankBranchService;
        private ITestBaseService<BankBranch> _TestBaseService;
        internal List<long> IdList = new List<long>();
        private ISession _session;
        internal BankBranchFactory _bankBranchFactory;
        internal readonly string _routingNo = Guid.NewGuid().ToString().Substring(0, 10) + "_(987654)";
        internal readonly string _swiftCode = Guid.NewGuid().ToString().Substring(0, 10) + "_(123456)";
        #endregion

        public BankBranchBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _bankService = new BankService(_session);
            _bankBranchFactory = new BankBranchFactory(null, _session);
            _bankBranchService=new BankBranchService(_session);
            _TestBaseService = new TestBaseService<BankBranch>(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }
        public void Dispose()
        {
            _bankBranchFactory.Cleanup();
            base.Dispose();
        }
    }


}
