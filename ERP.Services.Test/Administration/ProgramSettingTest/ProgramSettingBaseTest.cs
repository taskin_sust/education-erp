﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.Administration.ProgramSettingTest
{
    public class ProgramSettingBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        private ISession _session;
        internal ProgramSettingService _programSettingService;
        internal ProgramService _programService;
        internal SessionService _sessionService;

        internal ProgramFactory _programFactory;
        internal SessionFactory _sessionFactory;
        internal ProgramSettingFactory _programSettingFactory;
        #endregion
        public ProgramSettingBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _programFactory = new ProgramFactory(null, _session);
            _sessionFactory = new SessionFactory(null, _session);
            _programSettingFactory = new ProgramSettingFactory(null, _session);

            _programService = new ProgramService(_session);
            _sessionService = new SessionService(_session);
            _programSettingService = new ProgramSettingService(_session);
        }

        public void Dispose()
        {
            _programSettingFactory.Cleanup();
            _programFactory.Cleanup();
            _sessionFactory.Cleanup();
        }
    }

}
