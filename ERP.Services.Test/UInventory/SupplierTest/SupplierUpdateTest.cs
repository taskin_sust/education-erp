﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.SupplierTest
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "Supplier")]
    public class SupplierUpdateTest : SupplierBaseTest
    {
        [Fact]
        public void Should_Return_Update_Successfully()
        {
            var bankDetails = _supplierBankDetailsFactory.Create().WithBankBranch();
            var supplierFactory = _supplierFactory.Create().CreateWithSupplierBankDetails(bankDetails.SingleObjectList).Persist();
            supplierFactory.Object.Name = _name;
            supplierFactory.Persist();
            Assert.NotNull(supplierFactory.Object);
            Assert.Equal(supplierFactory.Object.Name, _name);
        }
    }
}
