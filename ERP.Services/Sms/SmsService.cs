﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Util;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Sms;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public interface ISmsService : IBaseService
    {
        #region Operational Function

        void Save(SmsSettings smsSetting, long[] SmsReceivers);
        void Update(SmsSettings smsSetting, long[] smsReceivers);
        void Delete(SmsSettings smsSettingDelete);
        #endregion

        #region Single Instances Loading Function
        SmsSettings LoadSmsSettingsById(long id);
        SmsSettings LoadSmsSettingsBySmsTypePrgram(long smsTypeId, long organizationId, long programId);
        #endregion

        #region List Loading Function
        //IList<SmsSettings> SmsSettingsList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string programId, string smsTypeId, string status, long? organizationId);
        IList<SmsSettings> SmsSettingsList(int start, int length, string orderBy, string orderDir, string programId, string smsTypeId, string status, long? organizationId);

        IList<SmsReceiver> LoadSmsReceiverByIds(long[] smsReceivers);

        IList<SmsHistory> LoadSmsHistory(int status, int top, int maxSendingTrySms);

        IList<SmsHistory> LoadSmsHistory(int start, int length, string orderBy, string orderDir, string dateFrom, string dateTo, string programId, string smsTypeId, string mobileNumber, string sms, string status, string userEmail);

        #endregion

        #region Others Function
        int SmsSettingsRowCount(string programId, string smsTypeId, string status, long? organizationId);
        //int SmsSettingsRowCount(List<UserMenu> userMenu, string programId, string smsTypeId, string status, long? organizationId);
        //IList<SmsDto> SendScheduleSms();
        int CountSmsHistory(string dateFrom, string dateTo, string programId, string smsTypeId, string mobileNumber, string sms, string status, string userEmail);

        #endregion

        #region Helper Function

        #endregion

        IList<Program> LoadAuthorizedProgram(List<UserMenu> userMenu, long organizationId);
        IList<SmsType> LoadSmsTypeActive();
        IList<SmsReceiver> LoadSmsReceiverActive();
        SmsType LoadSmsTypeById(long smsType);
        SmsType LoadSmsTypeName(string name);
        bool DuplicationCheck(Organization organization, Program programId, long smsType);
        Program LoadProgramById(long id);
        SmsReceiver LoadReceiverById(long smsReceiverId);
        void SaveSmsHistory(SmsHistory smsHistory);
        void UpdateSmsHistory(SmsHistory currentObj);

        //void SaveTemporarySmsData(List<global::UdvashERP.BusinessModel.ViewModel.ExamModuleView.MarksEntryByXmlSms> smsXmlList);

        void SaveTemporarySmsData(List<TemporarySms> temporarySmsList);

        List<TemporarySms> LoadAllPendingTempSms(int defaultCount = 100);

        bool SaveSmsHistoryListAndDeleteTemporarySmsData(IList<SmsHistory> smsHistoryList, List<TemporarySms> xmlSmsList, IList<TemporarySms> invalidSmsList);

        void UpdateHistoryList(IList<SmsHistory> historyList);
        IList<SmsHistory> SendScheduleSmsNew(long smsUserId); 
    }
    public class SmsService : BaseService, ISmsService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IProgramDao _programDao;
        private readonly ISmsTypeDao _smsTypeDao;
        private readonly ISmsReceiverDao _smsReceiverDao;
        private readonly ISmsDynamicOptionDao _smsDynamicOptionDao;
        private readonly ISmsSettingsDao _smsSettingsDao;
        private readonly ISmsHistoryDao _smsHistoryDao;
        private readonly IStudentDao _studentDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly ITemporarySmsDao _temporarySmsDao; 
        private readonly ICommonHelper _commonHelper;

        // public const long BirthDayReminder = 8, DueReminder = 7;   
        public SmsService(ISession session)
        {
            Session = session;
            _programDao = new ProgramDao() { Session = Session };
            _smsTypeDao = new SmsTypeDao() { Session = Session };
            _smsReceiverDao = new SmsReceiverDao() { Session = Session };
            _smsDynamicOptionDao = new SmsDynamicOptionDao() { Session = Session };
            _smsSettingsDao = new SmsSettingsDao() { Session = Session };
            _smsHistoryDao = new SmsHistoryDao() { Session = Session };
            _studentDao = new StudentDao() { Session = Session };
            _studentProgramDao = new StudentProgramDao() { Session = Session };
            _temporarySmsDao = new TemporarySmsDao() { Session = Session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function
        public void Save(SmsSettings smsSetting, long[] SmsReceivers)
        {
            try
            {
                bool exist = DuplicationCheck(smsSetting.Organization, smsSetting.Program, smsSetting.SmsType.Id);


                if (exist)
                {
                    throw new DuplicateEntryException("This SMS Template already exist !!!");
                }
                else
                {
                    using (var trans = Session.BeginTransaction())
                    {
                        try
                        {
                            int maxRank = _smsSettingsDao.GetMenuMaximumRank();
                            smsSetting.Rank = maxRank + 1;
                            smsSetting.SmsReceivers = _smsReceiverDao.LoadSmsReceiverByIds(SmsReceivers);

                            _smsSettingsDao.Save(smsSetting);
                            trans.Commit();
                        }
                        catch (Exception e)
                        {
                            trans.Rollback();
                            throw e;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(SmsSettings smsSetting, long[] smsReceivers)
        {
            try
            {
                smsSetting.SmsReceivers.Clear();
                _smsSettingsDao.Update(smsSetting);
                Session.Flush();

                var selected = _smsReceiverDao.LoadSmsReceiverByIds(smsReceivers);
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        selected.ForEach(x => smsSetting.SmsReceivers.Add(x));
                        _smsSettingsDao.Update(smsSetting);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(SmsSettings smsSettingDelete)
        {
            try
            {

                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        smsSettingDelete.Status = SmsSettings.EntityStatus.Delete;
                        _smsSettingsDao.Update(smsSettingDelete);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public SmsSettings LoadSmsSettingsBySmsTypePrgram(long smsTypeId, long organizationId, long programId)
        {
            try
            {
                return _smsSettingsDao.LoadSmsSettingsBySmsTypePrgram(smsTypeId, organizationId, programId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public SmsSettings LoadSmsSettingsById(long id)
        {
            try
            {
                return _smsSettingsDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function

        public IList<SmsSettings> SmsSettingsList(int start, int length, string orderBy, string orderDir, string programId, string smsTypeId, string status, long? organizationId)
        //public IList<SmsSettings> SmsSettingsList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string programId, string smsTypeId, string status, long? organizationId)
        {
            try
            {
                //long? convertedProgramId = String.IsNullOrEmpty(programId) ? (long?)null : Convert.ToInt64(programId);
                //List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                //List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (convertedProgramId != null) ? _commonHelper.ConvertIdToList((long)convertedProgramId) : null);

                //return _smsSettingsDao.SmsSettingsList(start, length, orderBy, orderDir, programIdList, smsTypeId, status, organizationIdList);
                return _smsSettingsDao.SmsSettingsList(start, length, orderBy, orderDir, programId, smsTypeId, status, organizationId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public IList<SmsReceiver> LoadSmsReceiverByIds(long[] SmsReceivers)
        {
            try
            {
                return _smsReceiverDao.LoadSmsReceiverByIds(SmsReceivers);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public IList<SmsHistory> LoadSmsHistory(int status, int top, int maxSendingTrySms)
        {
            try
            {
                //return _smsHistoryDao.LoadAll(status);
                return _smsHistoryDao.LoadSmsHistory(status, top, maxSendingTrySms);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<SmsHistory> LoadSmsHistory(int start, int length, string orderBy, string orderDir, string dateFrom, string dateTo, string programId, string smsType, string mobileNumber, string sms, string status, string userEmail)
        {
            try
            {
                DateTime searchingDateFrom = string.IsNullOrWhiteSpace(dateFrom) ? DateTime.Now : Convert.ToDateTime(dateFrom);
                DateTime searchingDatTo = string.IsNullOrWhiteSpace(dateTo) ? DateTime.Now : Convert.ToDateTime(dateTo);
                long? searcingProgramId = string.IsNullOrWhiteSpace(programId) ? (long?)null : Convert.ToInt64(programId);
                long? searcingSmsType = string.IsNullOrWhiteSpace(smsType) ? (long?)null : Convert.ToInt64(smsType);
                int? searcingStatus = string.IsNullOrWhiteSpace(status) ? (int?)null : Convert.ToInt32(status);
                return _smsHistoryDao.LoadSmsHistory(start, length, orderBy, orderDir, searchingDateFrom, searchingDatTo, searcingProgramId, searcingSmsType, mobileNumber, sms, searcingStatus, userEmail);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }



        #endregion

        #region Others Function
        //public int SmsSettingsRowCount(List<UserMenu> userMenu, string programId, string smsTypeId, string status, long? organizationId)
        public int SmsSettingsRowCount(string programId, string smsTypeId, string status, long? organizationId)
        {
            try
            {
                //long? convertedProgramId = String.IsNullOrEmpty(programId) ? (long?)null : Convert.ToInt64(programId);
                //List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                //List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (convertedProgramId != null) ? _commonHelper.ConvertIdToList((long)convertedProgramId) : null);

                //return _smsSettingsDao.SmsSettingsRowCount(programIdList, smsTypeId, status, organizationIdList);
                return _smsSettingsDao.SmsSettingsRowCount(programId, smsTypeId, status, organizationId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        

        public IList<SmsDto> SendScheduleSms(long smsUserId)
        {
            IList<SmsDto> smsDtos = new List<SmsDto>();
            //try
            //{
            //    List<string> smsTypes = new List<string>() { "Due Reminder", "Birthday Reminder" };
            //    IList<SmsSettings> smsSettingses = _smsSettingsDao.SmsSettingsList(smsTypes);
            //    IList<SmsHistory> smsHistoryList = new List<SmsHistory>();
               
            //    if (smsSettingses.Any())
            //    {
            //        foreach (var smsSettings in smsSettingses)
            //        {
            //            bool isSendingTime = smsSettings.SmsTime.Value.TimeOfDay <= DateTime.Now.TimeOfDay;
            //            if (isSendingTime)
            //            {
            //                var programList = new List<long>();
            //                bool isIn = true;
            //                //var programIdList;  
            //                if (smsSettings.Program != null)
            //                {
            //                    programList.Add(smsSettings.Program.Id);
            //                }
            //                else
            //                {
            //                    programList = smsSettingses.Where(x => x.Program != null && x.Organization.Id == smsSettings.Organization.Id).Select(x => x.Program.Id).ToList();
            //                    isIn = false;
            //                }

            //                IList<ScheduleSmsDto> scheduleSmsDtoList = new List<ScheduleSmsDto>();

            //                if (smsSettings.SmsType.Name == "Birthday Reminder")
            //                {
            //                   scheduleSmsDtoList = _smsSettingsDao.LoadBirthdaySmsStudentList(smsSettings.Organization.Id, programList, smsSettings, isIn);
            //                } 
                              
            //                else if (smsSettings.SmsType.Name == "Due Reminder")
            //                {
            //                    scheduleSmsDtoList = _smsSettingsDao.LoadDueSmsStudentList(smsSettings.Organization.Id, programList, smsSettings, isIn);
            //                }


            //                if (scheduleSmsDtoList.Any())
            //                {
            //                    var options = smsSettings.SmsType.DynamicOptions;
            //                    var message = smsSettings.Template;
            //                    var mask = smsSettings.MaskName;
            //                    var smsToBeSend = new List<SmsViewModel>();

                               
            //                    foreach (var scheduleSmsDto in scheduleSmsDtoList)
            //                    {
            //                        #region Generate SMS from Template
            //                        if (options.Any())
            //                        {
            //                            foreach (var o in options)
            //                            {
            //                                string optionField = "[[{{" + o.Name + "}}]]";
            //                                string optionValue = "";

            //                                switch (o.Name)
            //                                {
            //                                    case "Nick Name":
            //                                        optionValue = scheduleSmsDto.NickName;
            //                                        break;

            //                                    case "Due Amount":
            //                                        if (scheduleSmsDto.DueAmount != null && Convert.ToDecimal(scheduleSmsDto.DueAmount) > 0)
            //                                        {
            //                                            optionValue = Convert.ToDecimal(scheduleSmsDto.DueAmount).ToString("n0").Replace(",", ""); ;
            //                                        }
            //                                        else
            //                                        {
            //                                            optionValue = "0";
            //                                        }
            //                                        break;

            //                                    case "Next Payment Date":

            //                                        if (scheduleSmsDto.DueAmount != null && Convert.ToDecimal(scheduleSmsDto.DueAmount) > 0)
            //                                        {
            //                                            optionValue = scheduleSmsDto.NextReceivedDate.ToString("dd MMM yyyy");
            //                                        }
            //                                        else
            //                                        {
            //                                            optionValue = "N/A";
            //                                        }

            //                                        break;
            //                                    default:
            //                                        optionValue = "";
            //                                        break;
            //                                }
            //                                message = message.Replace(optionField, optionValue);
            //                            }
            //                        }

            //                    #endregion

            //                        #region Listing Receivers
            //                        foreach (var sr in smsSettings.SmsReceivers)
            //                        {
            //                            var svm = new SmsViewModel();
            //                            switch (sr.Name)
            //                            {
            //                                case "Mobile Number (Personal)":
            //                                    string studentMobile = CheckMobileNumber(scheduleSmsDto.PersonalMobile);
            //                                    if (studentMobile != null)
            //                                    {
            //                                        svm.SmsReceiverId = sr.Id;
            //                                        svm.ReceiverNumber = "88" + studentMobile;
            //                                    }
            //                                    break;
            //                                case "Mobile Number (Father)":
            //                                    string guardiansMobile1 = CheckMobileNumber(scheduleSmsDto.FatherMobile);
            //                                    if (guardiansMobile1 != null)
            //                                    {
            //                                        svm.SmsReceiverId = sr.Id;
            //                                        svm.ReceiverNumber = "88" + guardiansMobile1;
            //                                    }
            //                                    break;
            //                                case "Mobile Number (Mother)":
            //                                    string guardiansMobile2 = CheckMobileNumber(scheduleSmsDto.MothersMobile);
            //                                    if (guardiansMobile2 != null)
            //                                    {
            //                                        svm.SmsReceiverId = sr.Id;
            //                                        svm.ReceiverNumber = "88" + guardiansMobile2;
            //                                    }
            //                                    break;
            //                                case "Mobile Number (Campus Incharge)":
            //                                    string contactNumber = CheckMobileNumber(scheduleSmsDto.CampusContact);
            //                                    if (CheckMobileNumber(scheduleSmsDto.CampusContact) != null)
            //                                    {
            //                                        svm.SmsReceiverId = sr.Id;
            //                                        svm.ReceiverNumber = "88" + contactNumber;
            //                                    }
            //                                    break;
                                           
            //                                default:
            //                                    break;
            //                            }

            //                            if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
            //                            {
            //                                var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
            //                                if (!alreadyExist.Any())
            //                                {
            //                                    smsToBeSend.Add(svm);
            //                                }
            //                            }
            //                        }
            //                        #endregion
            //                    }

            //                    if (smsToBeSend.Any())
            //                    {
            //                        if (String.IsNullOrEmpty(mask))
            //                        {
            //                            mask = "";
            //                        }
            //                        foreach (var stbs in smsToBeSend)
            //                        {
            //                            var smsHistory = new SmsHistory();
            //                            var currentNumber = stbs;
            //                            smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
            //                            smsHistory.SmsReceiver =
            //                                smsSettings.SmsReceivers.FirstOrDefault(x => x.Id == currentNumber.SmsReceiverId);
            //                            smsHistory.Sms = message;
            //                           // smsHistory.Program = xmlsms.StudentProgram.Program;
            //                            //smsHistory.Organization = xmlsms.StudentProgram.Program.Organization;
            //                            smsHistory.SmsSettings = smsSettings;
            //                          //  smsHistory.Student = student;
            //                            smsHistory.Mask = mask;
            //                            smsHistory.Type = 6; //for result
            //                            smsHistory.Status = SmsHistory.EntityStatus.Pending;
            //                            smsHistory.CreateBy = xmlsms.CreateBy;
            //                            smsHistory.ModifyBy = xmlsms.ModifyBy;
            //                            smsHistory.Priority = 5;
            //                            smsHistoryList.Add(smsHistory);
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _logger.Error(ex);
            //    throw;
            //}
            return smsDtos;
        }
        public IList<SmsHistory> SendScheduleSmsNew(long smsUserId)
        {
            IList<SmsHistory> smsHistoryList = new List<SmsHistory>();
            try
            {
                var smsTypes = new List<string>() { "Due Reminder", "Birthday Reminder" };
                var smsSettingses = _smsSettingsDao.SmsSettingsList(smsTypes);
                var programList = new List<Program>();
                var organizationList = new List<Organization>();  

                if (smsSettingses.Any())
                {
                    foreach (var smsSettings in smsSettingses)
                    {
                        bool isSendingTime = smsSettings.SmsTime.Value.TimeOfDay <= DateTime.Now.TimeOfDay;
                        if (isSendingTime)
                        {
                            var programIdList = new List<long>();
                            bool isIn = true;
                            //var programIdList;  
                            if (smsSettings.Program != null)
                            {
                                programIdList.Add(smsSettings.Program.Id);
                            }
                            else
                            {
                                programIdList = smsSettingses.Where(x => x.Program != null && x.Organization.Id == smsSettings.Organization.Id).Select(x => x.Program.Id).ToList();
                                isIn = false;
                            }

                            IList<ScheduleSmsDto> scheduleSmsDtoList = new List<ScheduleSmsDto>();

                            if (smsSettings.SmsType.Name == "Birthday Reminder")
                            {
                                scheduleSmsDtoList = _smsSettingsDao.LoadBirthdaySmsStudentList(smsSettings.Organization.Id, programIdList, smsSettings, isIn);
                            }

                            else if (smsSettings.SmsType.Name == "Due Reminder")
                            {
                                scheduleSmsDtoList = _smsSettingsDao.LoadDueSmsStudentList(smsSettings.Organization.Id, programIdList, smsSettings, isIn);
                            }


                            if (scheduleSmsDtoList.Any())
                            {
                              
                               


                                foreach (var scheduleSmsDto in scheduleSmsDtoList)
                                {
                                    var options = smsSettings.SmsType.DynamicOptions;
                                    var message = smsSettings.Template;
                                    var mask = smsSettings.MaskName;
                                    var smsToBeSend = new List<SendSmsViewModel>();

                                    #region Generate SMS from Template
                                    if (options.Any())
                                    {
                                        foreach (var o in options)
                                        {
                                            string optionField = "[[{{" + o.Name + "}}]]";
                                            string optionValue = "";

                                            switch (o.Name)
                                            {
                                                case "Nick Name":
                                                    optionValue = scheduleSmsDto.NickName;
                                                    break;

                                                case "Due Amount":
                                                    if (scheduleSmsDto.DueAmount != null && Convert.ToDecimal(scheduleSmsDto.DueAmount) > 0)
                                                    {
                                                        optionValue = Convert.ToDecimal(scheduleSmsDto.DueAmount).ToString("n0").Replace(",", ""); ;
                                                    }
                                                    else
                                                    {
                                                        optionValue = "0";
                                                    }
                                                    break;

                                                case "Next Payment Date":

                                                    if (scheduleSmsDto.DueAmount != null && Convert.ToDecimal(scheduleSmsDto.DueAmount) > 0)
                                                    {
                                                        optionValue = scheduleSmsDto.NextReceivedDate.ToString("dd MMM yyyy");
                                                    }
                                                    else
                                                    {
                                                        optionValue = "N/A";
                                                    }

                                                    break;
                                                default:
                                                    optionValue = "";
                                                    break;
                                            }
                                            message = message.Replace(optionField, optionValue);
                                        }
                                    }

                                    #endregion

                                    #region Listing Receivers
                                    foreach (var sr in smsSettings.SmsReceivers)
                                    {
                                        var svm = new SendSmsViewModel();
                                        switch (sr.Name)
                                        {
                                            case "Mobile Number (Personal)":
                                                string studentMobile = CheckMobileNumber(scheduleSmsDto.PersonalMobile);
                                                if (studentMobile != null)
                                                {
                                                    svm.SmsReceiverId = sr.Id;
                                                    svm.ReceiverNumber = "88" + studentMobile;
                                                }
                                                break;
                                            case "Mobile Number (Father)":
                                                string guardiansMobile1 = CheckMobileNumber(scheduleSmsDto.FatherMobile);
                                                if (guardiansMobile1 != null)
                                                {
                                                    svm.SmsReceiverId = sr.Id;
                                                    svm.ReceiverNumber = "88" + guardiansMobile1;
                                                }
                                                break;
                                            case "Mobile Number (Mother)":
                                                string guardiansMobile2 = CheckMobileNumber(scheduleSmsDto.MothersMobile);
                                                if (guardiansMobile2 != null)
                                                {
                                                    svm.SmsReceiverId = sr.Id;
                                                    svm.ReceiverNumber = "88" + guardiansMobile2;
                                                }
                                                break;
                                            case "Mobile Number (Campus Incharge)":
                                                string contactNumber = CheckMobileNumber(scheduleSmsDto.CampusContact);
                                                if (CheckMobileNumber(scheduleSmsDto.CampusContact) != null)
                                                {
                                                    svm.SmsReceiverId = sr.Id;
                                                    svm.ReceiverNumber = "88" + contactNumber;
                                                }
                                                break;

                                            default:
                                                break;
                                        }

                                        if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                                        {
                                            var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                                            if (!alreadyExist.Any())
                                            {
                                                svm.ProgramId = scheduleSmsDto.ProgramId;
                                                svm.StudentId = scheduleSmsDto.StudentId;
                                                svm.OrganizationId = scheduleSmsDto.OrganizationId;
                                                
                                                smsToBeSend.Add(svm);
                                               // programList
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Generate Sms History List
                                    if (smsToBeSend.Any())
                                    {
                                        if (String.IsNullOrEmpty(mask))
                                        {
                                            mask = "";
                                        }
                                        foreach (var stbs in smsToBeSend)
                                        {
                                            var Program = programList.FirstOrDefault(x => x.Id == stbs.ProgramId) ?? _programDao.LoadById(stbs.ProgramId);
                                            var smsHistory = new SmsHistory();
                                            var currentNumber = stbs;
                                            smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                                            smsHistory.SmsReceiver =
                                                smsSettings.SmsReceivers.FirstOrDefault(x => x.Id == currentNumber.SmsReceiverId);
                                            smsHistory.Sms = message;
                                            smsHistory.Program = Program;
                                            smsHistory.Organization = Program.Organization;
                                            smsHistory.SmsSettings = smsSettings;
                                            smsHistory.Student = _studentDao.LoadById(stbs.StudentId);
                                            smsHistory.Mask = mask;
                                            smsHistory.Type = Convert.ToInt32(smsSettings.SmsType.Id);
                                            smsHistory.Status = SmsHistory.EntityStatus.Pending;
                                            smsHistory.CreateBy = smsUserId;
                                            smsHistory.ModifyBy = smsUserId;
                                            smsHistory.Priority = 4;
                                            smsHistoryList.Add(smsHistory);
                                        }
                                    }

                                    #endregion
                                }
                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return smsHistoryList;
        }

        public int CountSmsHistory(string dateFrom, string dateTo, string programId, string smsType, string mobileNumber, string sms, string status, string userEmail)
        {
            try
            {
                DateTime searchingDateFrom = string.IsNullOrWhiteSpace(dateFrom) ? DateTime.Now : Convert.ToDateTime(dateFrom);
                DateTime searchingDatTo = string.IsNullOrWhiteSpace(dateTo) ? DateTime.Now : Convert.ToDateTime(dateTo);
                long? searcingProgramId = string.IsNullOrWhiteSpace(programId) ? (long?)null : Convert.ToInt64(programId);
                long? searcingSmsType = string.IsNullOrWhiteSpace(smsType) ? (long?)null : Convert.ToInt64(smsType);
                int? searcingStatus = string.IsNullOrWhiteSpace(status) ? (int?)null : Convert.ToInt32(status);
                return _smsHistoryDao.CountSmsHistory(searchingDateFrom, searchingDatTo, searcingProgramId, searcingSmsType, mobileNumber, sms, searcingStatus, userEmail);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper Function

        #endregion

        public SmsReceiver LoadReceiverById(long smsReceiverId)
        {
            try
            {
                return _smsReceiverDao.LoadById(smsReceiverId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<Program> LoadAuthorizedProgram(List<UserMenu> userMenu, long organizationId)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                var programList = _programDao.LoadAuthorizedProgram(programIdList);
                programList = programList.Where(x => x.Organization.Id == organizationId).ToList();
                return programList.OrderBy(x => x.Rank).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<SmsType> LoadSmsTypeActive()
        {
            try
            {
                return _smsTypeDao.LoadSmsTypeActive();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public IList<SmsReceiver> LoadSmsReceiverActive()
        {
            try
            {
                return _smsReceiverDao.LoadSmsReceiverActive();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public SmsType LoadSmsTypeById(long smsType)
        {
            try
            {
                return _smsTypeDao.LoadById(smsType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public SmsType LoadSmsTypeName(string name)
        {
            try
            {
                return _smsTypeDao.LoadByTypeName(name); //LoadSingleByName(name);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool DuplicationCheck(Organization organization, Program programId, long smsType)
        {
            try
            {
                return _smsSettingsDao.DuplicationCheck(organization, programId, smsType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public Program LoadProgramById(long id)
        {
            try
            {
                return _programDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public void SaveSmsHistory(SmsHistory smsHistory)
        {
            try
            {
                smsHistory.SendingTryCount = 0;
                _smsHistoryDao.Save(smsHistory);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public void UpdateSmsHistory(SmsHistory currentObj)
        {
            ITransaction transaction = null;
            try
            {
                SmsHistory newSmsHistory = _smsHistoryDao.LoadById(currentObj.Id);
                newSmsHistory.ErrorCode = currentObj.ErrorCode;
                newSmsHistory.Status = currentObj.Status;
                newSmsHistory.ResponseCode = currentObj.ResponseCode;
                newSmsHistory.SendingTryCount = currentObj.SendingTryCount;
                using (transaction = Session.BeginTransaction())
                {
                    _smsHistoryDao.Update(newSmsHistory);
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public void SaveTemporarySmsData(List<TemporarySms> temporarySmsList)
        {
            using (var transication = Session.BeginTransaction())
            {
                try
                {
                    foreach (TemporarySms temporarySmse in temporarySmsList)
                    {
                        _temporarySmsDao.Save(temporarySmse);
                    }
                    transication.Commit();
                }
                catch (Exception ex)
                {
                    transication.Rollback();
                    _logger.Error(ex);
                }
            }
        }

        public List<TemporarySms> LoadAllPendingTempSms(int defaultCount = 100)
        {
            return _temporarySmsDao.LoadAllPendingTempSms(defaultCount);
        }

        public bool SaveSmsHistoryListAndDeleteTemporarySmsData(IList<SmsHistory> smsHistoryList, List<TemporarySms> tempSmsList, IList<TemporarySms> invalidSmsList)
        {
            using (var trans = Session.BeginTransaction())
            {
                try
                {
                    
                    //var joinTwoList = smsHistoryList.Zip(tempSmsList, (sHistory, sTemp) => new { History = sHistory, TempSms = sTemp });
                    //foreach (var combineObj in joinTwoList)
                    //{
                    //    _smsHistoryDao.Save(combineObj.History);
                    //    _temporarySmsDao.Delete(combineObj.TempSms);
                    //}
                    foreach (var history in smsHistoryList)
                    {
                        _smsHistoryDao.Save(history);
                    }
                    if (tempSmsList!=null && tempSmsList.Any())
                    {
                        foreach (var temporarySmse in tempSmsList)
                        {
                            _temporarySmsDao.Delete(temporarySmse);
                        }
                    }
                    
                    //foreach (var invalidSmsObj in invalidSmsList)
                    //{
                    //    invalidSmsObj.Status = -404;
                    //    _temporarySmsDao.Update(invalidSmsObj);
                    //}
                    trans.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    trans.Rollback();
                    return false;
                    throw e;
                }

            }
        }

        public void UpdateHistoryList(IList<SmsHistory> historyList)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    StringBuilder updateStringBuilder = new StringBuilder();
                    foreach (SmsHistory currentObj in historyList)
                    {
                        if (currentObj != null && currentObj.Id > 0)
                        {
                            //SmsHistory newSmsHistory = _smsHistoryDao.LoadById(currentObj.Id);
                            //if (newSmsHistory != null)
                            //{
                            //    newSmsHistory.ErrorCode = currentObj.ErrorCode;
                            //    newSmsHistory.Status = currentObj.Status;
                            //    newSmsHistory.ResponseCode = currentObj.ResponseCode;
                            //    newSmsHistory.SendingTryCount = currentObj.SendingTryCount;
                            //    _smsHistoryDao.SaveOrUpdate(newSmsHistory);
                            //}


                            string updateCodeCurrent = @" UPDATE [dbo].[SmsHistory]
                                                       SET [ModificationDate] = getdate() 
                                                          ,[Status] = " + currentObj.Status + @"
                                                          ,[ResponseCode] = '" + currentObj.ResponseCode + @"'
                                                          ,[ErrorCode] = '" + currentObj.ErrorCode + @"'
                                                          ,[SendingTryCount] = " + currentObj.SendingTryCount + @"
                                                     WHERE Id = " + currentObj.Id + "; ";

                            updateStringBuilder.Append(updateCodeCurrent);
                        }
                    }
                    string query = updateStringBuilder.ToString();
                    IQuery iQuery = Session.CreateSQLQuery(query);
                    iQuery.SetTimeout(2700);
                    transaction.Commit();
                    //Session.Flush();
                    
                }

            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public static string CheckMobileNumber(string mobile)
        {
            if (mobile != null)
            {
                var regex = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
                var match = regex.Match(mobile);
                if (match.Success == true)
                {
                    switch (mobile.Length)
                    {
                        case 14:
                            mobile = mobile.Substring(3);
                            break;
                        case 13:
                            mobile = mobile.Substring(2);
                            break;
                        case 10:
                            mobile = "0" + mobile;
                            break;
                    }
                }
                else { mobile = null; }
            }
            return mobile;
        }
    }
}
