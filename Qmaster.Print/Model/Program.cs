﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qmaster.Print.Model
{
    public class Program
    {
        public virtual long Id { get; set; }
        public virtual string ShortName { get; set; }
        public virtual string FullName { get; set; }
    }
}
