﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using ICSharpCode.SharpZipLib.Zip;
using log4net;
using Synchronizer.Updater.ApiReference;
using Synchronizer.Updater.BusinessRule;
using Synchronizer.Updater.Helper;
using Synchronizer.Updater.Model;
using Synchronizer.Updater.ProcessCommunication;

namespace Synchronizer.Updater.Services
{
    public class SynchronizerUpdater
    {
        #region Objects/Properties/Services/Dao & Initialization

        private static readonly ILog Log = LogManager.GetLogger("SynchronizerUpdaterLogger");

        private bool _isRunning;
        private Thread _synchronizerUpdaterThread;
        private readonly IpcClient _ipcClient;
        private readonly ApiRequest _apiRequest;
        private DirectoryInfo _synchronizerDirectoryPath;
        private DirectoryInfo _synchronizerDirectoryBackupPath;
        private readonly ViewMessageDelegate _viewMessage;
        private static string _downloadDirectoryPath;

        public SynchronizerUpdater(ViewMessageDelegate currentMessage)
        {
            _viewMessage = currentMessage;
            _apiRequest = new ApiRequest(currentMessage);
            _ipcClient = new IpcClient(currentMessage);
        }

        #endregion

        #region Delegate & Function

        public delegate void ViewMessageDelegate(string currentMessage);
        private void OnViewMessage(string message)
        {
            if (_viewMessage != null)
            {
                _viewMessage(message);
            }
        }

        #endregion

        #region Operational Function

        public void Start()
        {
            try
            {
                if (_synchronizerUpdaterThread == null || !_synchronizerUpdaterThread.IsAlive)
                {
                    _synchronizerUpdaterThread = new Thread(SoftwareUpdaterFunc);
                    _synchronizerUpdaterThread.Start();
                    _isRunning = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public void Stop()
        {
            try
            {
                _isRunning = false;
                _synchronizerUpdaterThread.Join();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public void Restart()
        {
            try
            {
                _isRunning = false;
                _synchronizerUpdaterThread.Join(1000 * 10);
                Start();
            }
            catch (Exception ex)
            {
                Log.Error("Restart failed due to " + ex.Message);
            }
        }

        private void SoftwareUpdaterFunc()
        {
            //connect synchronizer software using inter process communication of wcf
            if (_isRunning == false || _ipcClient.Connect() == false)
                return;

            //download latest sync software & verify 
            if (_isRunning == false || DownloadLatestSoftware() == false)
                return;

            //stop synchronizer
            if (_isRunning == false || StopSynchronizer(true))
                return;

            //backup existing software
            if (_isRunning == false || BackupCurrentSoftware() == false)
                return;

            //setup new software
            if (_isRunning == false || SetupNewSoftware() == false)
                return;

            //try to start new sync service
            if (_isRunning == false || StartSynchronizer() == false)
                return;

            //revert process
            if (_isRunning == false || _ipcClient.Connect() == false)
            {
                RestorePreviousSoftware();
                StartSynchronizer();
                string message = "Problem occured during setup latest synchronizer software. Restore previous software";
                PushOnlineNotification("False", message);
            }
            else
            {
                string message = "Synchronizer service sucessfully started.";
                PushOnlineNotification("True", message);
                OnViewMessage(message);
            }
            //Thread.Sleep(1000*5);
            if(SoftwareInfo.AutomaticExit)
                Environment.Exit(0);
        }

        #endregion

        #region Operation Sub Task

        //download latest synchronizer software
        private bool DownloadLatestSoftware(int operationCount = 0)
        {
            bool returnValue = false;
            try
            {
                //try twice time to download synchronizer software
                if (operationCount > UpdaterConstant.TryToDownloadSoftwareCount)
                {
                    OnViewMessage("Please try later for donwload synchronizer software.");
                    return returnValue;
                }

                //get synchronizer software version using inter process communication of wcf
                string syncVersion = _ipcClient.GetVersion();
                if (syncVersion == null)
                    return returnValue;
            
                //get url for download latest synchronizer software
                JsonResponse response = _apiRequest.GetUrl(syncVersion);
                if (response.IsSuccess == false)
                {
                    OnViewMessage(response.Data);
                    return returnValue;
                }

                OnViewMessage("Preparing for download latest software files.");
                _downloadDirectoryPath = _apiRequest.Download(response.Data);
                if (_downloadDirectoryPath == null)         
                    return returnValue;
              
                //varify synchronizer software zip file using download directory path
                ZipFile zip = new ZipFile(_downloadDirectoryPath);
                returnValue = zip.TestArchive(true, TestStrategy.FindFirstError, null);
                if (returnValue == false)
                {
                    operationCount = operationCount + 1;
                    OnViewMessage("File corrupted, Restarting download ( " + operationCount + " )");
                    returnValue = DownloadLatestSoftware(operationCount);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return returnValue;
        }

        private bool BackupCurrentSoftware()
        {
            bool returnValue = false;
            try
            {
                _synchronizerDirectoryPath = AppHelper.GetRequiredDirectory(AppHelper.RequiredDirectory.Synchronizer);
                _synchronizerDirectoryBackupPath = AppHelper.GetRequiredDirectory(AppHelper.RequiredDirectory.Backup);
                _synchronizerDirectoryBackupPath = new DirectoryInfo(_synchronizerDirectoryBackupPath.FullName + "\\" + "SU_" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".zip");

                //create backup zip file
                FastZip fastZip = new FastZip();
                fastZip.CreateZip(_synchronizerDirectoryBackupPath.FullName, _synchronizerDirectoryPath.FullName, true, "");

                //varify zip file
                ZipFile zip = new ZipFile(_synchronizerDirectoryBackupPath.FullName);
                returnValue = zip.TestArchive(true, TestStrategy.FindFirstError, null);
                OnViewMessage(returnValue ? "Synchronizer software successfully backup." : "Failed to backup Synchronizer software.");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return returnValue;
        }

        private bool SetupNewSoftware()
        {
            bool returnValue = false;
            try
            {
                OnViewMessage("Setup new synchronizer software.");

                //delete existing synchronizer software folder & files
                RemoveDirectoryFiles(_synchronizerDirectoryPath.FullName);

                //extract latest zip files to the synchronizer directory  
                FastZip fastZip = new FastZip();
                fastZip.ExtractZip(_downloadDirectoryPath, _synchronizerDirectoryPath.FullName, "");

                //extract essential files from backup directory to synchronizer directory 
                const string fileFilter = @"OslErp_Attendance.db3;Config.xml";
                fastZip.ExtractZip(_synchronizerDirectoryBackupPath.FullName, _synchronizerDirectoryPath.FullName, fileFilter);
                OnViewMessage("Logs folder and essential file copied");
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return returnValue;
        }

        private void RestorePreviousSoftware()
        {
            try
            {
                OnViewMessage("Try to restored previous synchronizer software version");

                //delete existing synchronizer software folder & files
                RemoveDirectoryFiles(_synchronizerDirectoryPath.FullName, true);

                //extract backup software
                FastZip fastZip = new FastZip();
                fastZip.ExtractZip(_synchronizerDirectoryBackupPath.FullName, _synchronizerDirectoryPath.FullName, "");
                OnViewMessage("Successfully restored previous synchronizer software version");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        #endregion

        #region Helper Function

        public bool StartSynchronizer()
        {
            bool returnValue = false;
            try
            {
                //Thread.Sleep(1000 * 20);
                string deviceSynchronizerPath = _synchronizerDirectoryPath.FullName + "\\" + SoftwareInfo.DeviceSynchronizerExeName;

                Process.Start(deviceSynchronizerPath);
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return returnValue;
        }

        public bool StopSynchronizer(bool exitApplication = false)
        {
            bool returnValue;
            try
            {
                _ipcClient.StopService(exitApplication);
                //Thread.Sleep(1000*20);
                returnValue = _ipcClient.IsServiceRunning();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                returnValue = true;

            }
            return returnValue;
        }

        private void PushOnlineNotification(string isSuccess, string message)
        {
            try
            {
                string appVersion = Helper.AppHelper.GetAppVersion();
                JsonResponse response = _apiRequest.GetLog(appVersion, isSuccess, message);
                OnViewMessage("Online Push Logger: " + response.Data);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void RemoveDirectoryFiles(string source, bool isDirectory = false)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(source);
                foreach (FileInfo file in di.GetFiles())
                {
                    //Thread.Sleep(1000);
                    file.Delete();
                }

                if (isDirectory)
                {
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }     
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        #endregion
    }
}
