using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{
    
    
    public partial class SpecialAllowanceSettingMap : BaseClassMap<SpecialAllowanceSetting,long> {

        public SpecialAllowanceSettingMap()
        {
			Table("HR_SpecialAllowanceSetting");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Amount).Column("Amount").Not.Nullable();
			Map(x => x.PaymentType).Column("PaymentType").Not.Nullable();
			Map(x => x.EffectiveDate).Column("EffectiveDate").Not.Nullable();
			Map(x => x.ClosingDate).Column("ClosingDate");
			Map(x => x.Remarks).Column("Remarks").Not.Nullable();

        }
    }
}
