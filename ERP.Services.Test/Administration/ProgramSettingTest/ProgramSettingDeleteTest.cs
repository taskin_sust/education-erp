﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.ProgramSettingTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "ProgramSetting")]
    public class ProgramSettingDeleteTest : ProgramSettingBaseTest
    {
        #region Basic Test
        [Fact]
        public void ProgramSetting_Delete_Should_Successful()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);
            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);
            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession).Persist();
            var programSettingObj =
                _programSettingService.GetProgramSetting(Convert.ToInt32(_programSettingFactory.Object.Id));
            Assert.True(_programSettingService.Delete(programSettingObj));
        }
        #endregion

        #region Business Logic Test
        [Fact]
        public void Count_ProgramSetting_Should_Return_Invalid_ProgramSettings()
        {
            Assert.Throws<NullObjectException>(() => _programSettingService.Delete(null));
        }
        #endregion
    }
}
