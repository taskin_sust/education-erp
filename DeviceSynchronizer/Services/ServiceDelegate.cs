﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceSynchronizer.Helper;

namespace DeviceSynchronizer.Services
{
    public class ServiceDelegate
    {
        public delegate void DeviceSynchronizedEventHandler(string message, MessageType messageType);
        public delegate void SetAppTitleDelegate(string name);
    }
}
