﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.ChildrenAllowanceSettingTest
{
    public class ChildrenAllowanceSettingBaseTest : TestBase, IDisposable
    {
        #region Properties & Object Initialization

        protected internal readonly ChildrenAllowanceSettingFactory _childrenAllowanceSettingFactory;
        protected readonly OrganizationFactory _organizationFactory;
        protected internal readonly IChildrenAllowanceSettingService _childrenAllowanceSettingService;
        protected internal readonly IAllowanceSheetService _allowanceSheetService;
        protected internal readonly IChildrenAllowanceBlockService _allowanceBlockService;
        protected internal readonly AllowanceBlockFactory _allowanceBlockFactory;
        #endregion

        #region Constructor
        public ChildrenAllowanceSettingBaseTest()
        {
            _organizationFactory = new OrganizationFactory(null, Session);
            _allowanceBlockFactory = new AllowanceBlockFactory(null, Session);
            _childrenAllowanceSettingFactory = new ChildrenAllowanceSettingFactory(null, Session);
            _allowanceSheetService = new AllowanceSheetService(Session);
            _allowanceBlockService = new ChildrenAllowanceBlockService(Session);
            _childrenAllowanceSettingService = new ChildrenAllowanceSettingService(Session);
        }
        #endregion

        #region disposal

        public void Dispose()
        {
            _allowanceBlockFactory.CleanUp();
            _childrenAllowanceSettingFactory.DeleteAll();
            _organizationFactory.Cleanup();
            base.Dispose();
        }

        #endregion
    }
}
