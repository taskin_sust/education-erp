﻿
//window.validateDropDownField("Organization", "Please Select Organization.");
//window.validateDropDownField("Branch", "Please Select Branch.");
//window.validateDropDownField("ItemGroup", "Please Select Item Group.");
//window.validateDropDownField("Item", "Please Select Item.");
//window.validateDropDownField("Supplier", "Please Select Supplier.");
//window.validateTextField("Value", "Invalid integer Number.");

$(document).ready(function () {

    $("#ReceivedQuantity, #Rate").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#ReceivedQuantity, #Rate").keyup(function (e) {
        var recQuantity = parseInt($("#ReceivedQuantity").val(), 10);
        var rate = parseFloat($("#Rate").val());
        recQuantity = recQuantity ? recQuantity : 0;
        rate = rate ? rate : 0;

        if (recQuantity >= 0 && rate >= 0) {
            var ceilValue = Math.floor(recQuantity * rate);
            $("#Value").val(ceilValue);
        } else {
            console.log("invalid value ");
        }
    });

    $(document).on("input propertychanges", "#ReceivedQuantity, #Rate", function () {

        var recQuantity = parseInt($("#ReceivedQuantity").val(), 10);
        var rate = parseFloat($("#Rate").val());
        recQuantity = recQuantity ? recQuantity : 0;
        rate = rate ? rate : 0;

        if (recQuantity >= 0 && rate >= 0) {
            var ceilValue = Math.floor(recQuantity * rate);
            $("#Value").val(ceilValue);
            if (ceilValue > 0) {
                $("#mulValue").empty();
            }
        } else {
            console.log("invalid value ");
        }

    });



    $(document).on('change', '#Organization', function () {

        try {
            //Get organization and item group ID
            $("#Branch").html("<option value='' selected='selected'>Please Select Branch</option>");
            //$("#ItemGroup").html("<option value='' selected='selected'>Please Select ItemGroup</option>");
            $("#Item").html("<option value='' selected='selected'>Please Select Item</option>");

            //$("#ItemGroup").removeAttr('selected').find('option:first').attr('selected', 'selected').refresh();
            $("#ItemGroup option:first").prop('selected', true);
            //$("#ItemGroup option")
            var orgId = $("#Organization").val();
            var url = $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch";
            var reqType = "Post";
            var data = { organizationIds: orgId, isAuthorized: true };
            //window.showErrorMessageBelowCtrl("Organization", "", false);
            if (orgId.length > 0) {
                $.ajax({
                    type: reqType,
                    url: url,
                    data: data,
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (response) {
                        $.each(response.returnBranchList, function (i, v) {
                            $('#Branch').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        $.unblockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (result) {
                        $.fn.customMessage({
                            displayMessage: window.AJAX_ERROR,
                            displayMessageType: "error",
                            displayTime: 5000
                        });
                    }
                });
            } else {
                //window.showErrorMessageBelowCtrl("Organization", "", false);
                //window.showErrorMessageBelowCtrl("Organization", "Please Select Organization", true);
            }
        } catch (e) {
            console.log(e);
        }
    });

    $(document).on('change', '#Branch', function () {

        var issend = true;
        var branchId = $("#Branch").val();
        var url = $("body").attr("data-project-root") + "UInventory/DirectGoodsReceive/FilterProgramSessionByBranch";
        var reqType = "Post";
        var data = { branchId: branchId };

        if (branchId.length <= 0) {
            issend = false;
        }
        $("#Item").html("<option value =''>Please Select Item</option>");
        $("#ProgramSession").html("<option value =''>Please Select Program & Session</option>");
        if (issend == true) {
            $.ajax({
                type: reqType,
                url: url,
                data: data,
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    if (response.IsSuccess == false) {
                        $("select option[value='0']").each(function () {
                            $(this).remove();
                        });
                    } else {
                        $("select option[value='0']").each(function () {
                            $(this).remove();
                        });
                        $("#Purpose option:first-child").after($('<option></option>').attr("value", '0').text('N/A'));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.fn.customMessage({
                        displayMessage: window.AJAX_ERROR,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    });


    //$(document).on('change', '#Purpose', function () {

    //    console.log("called");
    //    try {
    //        $("#ProgramSession").html("<option value='' selected='selected'>Please Select Program & Session</option>");
    //        var issend = true;
    //        var purpose = $("#Purpose").val();
    //        var branchId = $("#Branch").val();

    //        var url = $("body").attr("data-project-root") + "UInventory/DirectGoodsReceive/GetProgramAndSession";
    //        var reqType = "Post";
    //        var data = { purpose: purpose, branchId: branchId };
    //        //window.showErrorMessageBelowCtrl("Purpose", "", false);
    //        //window.showErrorMessageBelowCtrl("Branch", "", false);
    //        if (purpose.length <= 0) {
    //            //window.showErrorMessageBelowCtrl("Purpose", "", false);
    //            //window.showErrorMessageBelowCtrl("Purpose", "Please Select Purpose", true);
    //            issend = false;
    //        }
    //        if (branchId.length <= 0) {
    //            //window.showErrorMessageBelowCtrl("Branch", "", false);
    //            //window.showErrorMessageBelowCtrl("Branch", "Please Select Branch", true);
    //            issend = false;
    //        }
    //        if (issend == true) {
    //            $.ajax({
    //                type: reqType,
    //                url: url,
    //                data: data,
    //                beforeSend: function () {
    //                    $.blockUI({
    //                        timeout: 0,
    //                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //                    });
    //                },
    //                success: function (response) {
    //                    if (response.IsSuccess == false) {
    //                        $.fn.customMessage({
    //                            displayMessage: response.Message,
    //                            displayMessageType: "e",
    //                            displayTime: 120000,
    //                        });
    //                    } else {
    //                        $("#ProgramSession").html(response);
    //                        console.log(response);
    //                    }
    //                    $.unblockUI();
    //                },
    //                complete: function () {
    //                    $.unblockUI();
    //                },
    //                error: function (result) {
    //                    $.fn.customMessage({
    //                        displayMessage: window.AJAX_ERROR,
    //                        displayMessageType: "error",
    //                        displayTime: 5000
    //                    });
    //                }
    //            });
    //        }
    //    } catch (e) {
    //        console.log(e);
    //    }
    //});

    $(document).on('change', '#ItemGroup', function () {

        try {
            window.showErrorMessageBelowCtrl("ItemGroup", "", false);
            window.showErrorMessageBelowCtrl("Organization", "", false);
            $("#Item").html("<option value='' selected='selected'>Please Select Item</option>");
            $("#ProgramSession").html("<option value='' selected='selected'>Please Select Program & Session</option>");
            //LoadItemForOrganizationOrGroup
            var issend = true;
            var itemGroupId = $("#ItemGroup").val();
            var organizationId = $("#Organization").val();
            var url = $("body").attr("data-project-root") + "UInventory/CommonAjaxUInventory/LoadItem";
            var reqType = "Post";

            if (itemGroupId.length <= 0) {
                //window.showErrorMessageBelowCtrl("ItemGroup", "Please Select ItemGroup", true);
                issend = false;
            }
            if (organizationId.length <= 0) {
                //window.showErrorMessageBelowCtrl("Organization", "Please Select Organization", true);
                issend = false;
            }
            var data = { organizationId: organizationId, itemGroupIds: itemGroupId };

            if (issend == true) {

                $.ajax({
                    type: reqType,
                    url: url,
                    data: data,
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (response) {
                        if (response.IsSuccess == false) {
                            $.fn.customMessage({
                                displayMessage: response.Message,
                                displayMessageType: "e",
                                displayTime: 120000,
                            });
                        } else {
                            $.each(response.returnResult, function (i, v) {
                                $('#Item').append($('<option>').text(v.Text).attr('value', v.Value));
                            });
                        }
                        $.unblockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (result) {
                        $.fn.customMessage({
                            displayMessage: window.AJAX_ERROR,
                            displayMessageType: "error",
                            displayTime: 5000
                        });
                    }
                });
            }
        } catch (e) {
            console.log(e);
        }
    });

    $(document).on('change', '#Item, #Purpose', function () {

        try {
            $('#ProgramSession').empty();
            $("#ProgramSession").prepend("<option value=''>Select program & session</option>");
            //long organizationId, List<long> branchIdList, List<long> itemIdList = null,  int purposeId = 0, bool isReport = true, bool? withOutCurrentProgramSession = false
            var issend = true;
            var organization = $("#Organization").val();
            var purpose = $("#Purpose").val();
            var branchId = $("#Branch").val();
            var itemId = $("#Item").val();
            var url = $("body").attr("data-project-root") + "UInventory/CommonAjaxUInventory/GetProgramAndSession";
            var reqType = "Post";
            var data = { organizationId: organization, branchId: branchId, itemIdList: itemId, purposeId: purpose, isReport: false, };

            if (purpose.length <= 0) {
                issend = false;
            }
            if (branchId.length <= 0) {
                issend = false;
            }
            if (itemId.length <= 0) {
                issend = false;
            }
            if (issend == true) {
                $.ajax({
                    type: reqType,
                    url: url,
                    data: data,
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (response) {
                        if (response.isSuccess) {
                            if (response.programSessionList == null) {
                                $('#ProgramSession').append("<option value='0'>N/A</option>");
                            } else {
                                $.each(response.programSessionList, function (i, v) {
                                    $('#ProgramSession').append($('<option>').text(v.Text).attr('value', v.Value));
                                });
                            }

                        } else {
                            $.fn.customMessage({
                                displayMessage: response.Message,
                                displayMessageType: "error"
                            });
                        }

                        //if (response.IsSuccess == false) {
                        //    $.fn.customMessage({
                        //        displayMessage: response.Message,
                        //        displayMessageType: "e",
                        //        displayTime: 120000,
                        //    });
                        //} else {
                        //    $("#ProgramSession").html(response);
                        //    console.log(response);
                        //}
                        $.unblockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (result) {
                        $.fn.customMessage({
                            displayMessage: window.AJAX_ERROR,
                            displayMessageType: "error",
                            displayTime: 5000
                        });
                    }
                });
            }
        } catch (e) {
            console.log(e);
        }
    });

    $(document).on('click', '#submitBtn', function (e) {

        try {
            //alert("dd");
            $("#Organization_err_div").empty();
            $("#Branch_err_div").empty();
            $("#ItemGroup_err_div").empty();
            $("#Item_err_div").empty();
            $("#Supplier_err_div").empty();
            $("#Purpose_err_div").empty();
            $("#ProgramSession_err_div").empty();

            $("#DGRForm").submit();

        } catch (e) {
            console.log(e);
        }
    });

});
