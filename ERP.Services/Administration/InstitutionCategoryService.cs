﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Administration
{
    public interface IInstitutionCategoryService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        InstituteCategory LoadInstituteCategoryById(long id);
        #endregion

        #region List Loading Function
        IList<InstituteCategory> GetInstituteCategoryList(bool isUniversity);
        IList<InstituteCategory> GetInstituteCategoryList();

        #endregion

        #region Others Function

        #endregion
    }
    public class InstitutionCategoryService : BaseService, IInstitutionCategoryService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly InstituteCategoryDao _instituteCategoryDao;
        public InstitutionCategoryService(ISession session)
        {
            Session = session;
            _instituteCategoryDao = new InstituteCategoryDao() { Session = Session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public InstituteCategory LoadInstituteCategoryById(long id)
        {
            try
            {
                return _instituteCategoryDao.LoadInstituteCategoryById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region List Loading Function

        public IList<InstituteCategory> GetInstituteCategoryList()
        {
            try
            {
                return _instituteCategoryDao.GetInstituteCategoryList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public IList<InstituteCategory> GetInstituteCategoryList(bool isUniversity)
        {
            try
            {
                return _instituteCategoryDao.GetInstituteCategoryList(isUniversity);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region Others Function

        #endregion
    }
}
