﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.BusinessRules.Teachers
{
    public class TeacherSearchConstants
    {
        public const string Tpin = "TPIN";
        public const string NickName = "Nick Name";
        public const string MobileNumber1 = "Mobile Number 1";
        public const string Organization = "Organization";
        public const string FullName = "Full Name";
        public const string FatherName = "Father Name";
        public const string Institute = "Institute";
        public const string MobileNumber2 = "Mobile Number 2";
        public const string RoomMateMobileNumber = "Mobile Number(Room Mate)";
        public const string FatherMobileNumber = "Mobile Number(Father)";
        public const string MotherMobileNumber = "Mobile Number(Mother)";
        public const string Religion = "Religion";
        public const string Gender = "Gender";
        public const string DateOfBirth = "Date Of Birth";
        public const string BloodGroup = "Blood Group";
        public const string Department = "Department";
        public const string HscPassingYear = "Hsc Passing Year";
        public const string Email = "Email";
        public const string ActivityPriority = "Activity Priority";
        public const string SubjectPriority = "Subject Priority";
        public const string VersionPriority = "Version Priority";

        public static List<string> GetSearchTeacherInformationStringList()
        {
            return new List<string>
            {
                Tpin, NickName, MobileNumber1, Organization, FullName, FatherName, Institute, MobileNumber2, RoomMateMobileNumber, FatherMobileNumber, MotherMobileNumber, Religion, Gender, DateOfBirth, BloodGroup, Department, HscPassingYear, Email, ActivityPriority,SubjectPriority,VersionPriority
            };
        }
    }
}
