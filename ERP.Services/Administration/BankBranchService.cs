using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Administration
{
    public interface IBankBranchService : IBaseService
    {
        #region Operational Function

        bool IsSave(BankBranch bankBranchObj);
        bool IsUpdate(long bankBranchId, BankBranch bankBranchObj);
        bool IsDelete(long bankBranchId);
        bool UpdateRank(long id, int current, string action);

        #endregion

        #region Single Instances Loading Function

        BankBranch GetBankBranch(long bankBranchId);

        #endregion

        #region List Loading Function

        IList<BankBranch> LoadBankBranch(int start, int length, string orderBy, string orderDir, string bank, string name, string shortName, string contactPerson, string contactPersonMobile, string routingNo, string swifeCode, string rank, string status);
        IList<BankBranch> LoadBankBranchList(long[] bankIdArray);

        #endregion

        #region Others Function

        int GetBankBranchCount(string orderBy, string orderDir, string bank, string name, string shortName, string contactPerson, string contactPersonMobile, string routingNo, string swifeCode, string rank, string status);
        int GetMinimumRank(BankBranch obj);
        int GetMaximumRank(BankBranch obj);
        bool IsRouteNoUnique(string code, long? id);
        bool IsSwiftCodeUnique(string code, long? id);

        #endregion

        #region Helper Function

        #endregion


    }
    public class BankBranchService : BaseService, IBankBranchService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IBankBranchDao _bankBranchDao;
        private readonly IBankDao _bankDao;

        public BankBranchService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _bankBranchDao = new BankBranchDao() { Session = session };
            _bankDao = new BankDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool IsSave(BankBranch bankBranchObj)
        {

            ITransaction transaction = null;
            try
            {
                if (bankBranchObj == null)
                {
                    throw new NullObjectException("Bank Branch can not be empty");
                }
                //Uniqe Routing No Check
                var duplicateRnCheck = IsRouteNoUnique(bankBranchObj.RoutingNo);
                if (duplicateRnCheck == false)
                {
                    throw new DuplicateEntryException("Duplicate Routing No. found.");
                }
                //Uniqe Swift Code Check
                var duplicateSfCheck = IsSwiftCodeUnique(bankBranchObj.SwiftCode);
                if (duplicateSfCheck == false)
                {
                    throw new DuplicateEntryException("Duplicate Swift Code found.");
                }
                //var bank = _bankDao.LoadById(bankBranchObj.BankId);
                if (bankBranchObj.Bank == null)
                    throw new NullObjectException("Bank can not be empty");

                using (transaction = Session.BeginTransaction())
                {
                    //bankBranchObj.Bank = bank;
                    bankBranchObj.Rank = _bankBranchDao.GetMaximumRank(bankBranchObj) + 1;
                    //TODO: This should not happen;
                    _bankBranchDao.Save(bankBranchObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        //Model Validation Check
        public void ModelValidationCheck(BankBranch bankBranchObj)
        {
            var validationResult = ValidationHelper.ValidateEntity<BankBranch, BankBranch.BankBranchMetaData>(bankBranchObj);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }
        public bool IsUpdate(long bankBranchId, BankBranch bankBranchObj)
        {
            ITransaction transaction = null;
            try
            {
                if (bankBranchObj == null)
                {
                    throw new NullObjectException("Bank Branch can't Empty");
                }
                //Uniqe Routing No. Check
                var duplicateRnCheck = IsRouteNoUnique(bankBranchObj.RoutingNo, bankBranchId);
                if (duplicateRnCheck == false)
                {
                    throw new DuplicateEntryException("Duplicate Routing No. found.");
                }
                //Uniqe Swift Code Check
                var duplicateSfCheck = IsSwiftCodeUnique(bankBranchObj.SwiftCode, bankBranchId);
                if (duplicateSfCheck == false)
                {
                    throw new DuplicateEntryException("Duplicate Swift Code found.");
                }
                //Model Validation Before Save
                ModelValidationCheck(bankBranchObj);
                using (transaction = Session.BeginTransaction())
                {

                    var tempBankBranchObj = _bankBranchDao.LoadById(bankBranchId);
                    tempBankBranchObj.Name = bankBranchObj.Name;
                    tempBankBranchObj.ShortName = bankBranchObj.ShortName;
                    tempBankBranchObj.ContactPerson = bankBranchObj.ContactPerson;
                    tempBankBranchObj.ContactPersonMobile = bankBranchObj.ContactPersonMobile;
                    tempBankBranchObj.RoutingNo = bankBranchObj.RoutingNo;
                    tempBankBranchObj.SwiftCode = bankBranchObj.SwiftCode;
                    tempBankBranchObj.Status = bankBranchObj.Status;
                    tempBankBranchObj.Bank = _bankDao.LoadById(bankBranchObj.BankId);
                    _bankBranchDao.Update(tempBankBranchObj);
                    transaction.Commit();
                    return true;
                }
                
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        public bool IsDelete(long bankBranchId)
        {
            ITransaction trans = null;
            try
            {
                if (bankBranchId <= 0)
                {
                    throw new NullObjectException("Bank Branch id is not valid");
                }
                var tempBankBranchObjs = _bankBranchDao.LoadById(bankBranchId);
                CheckBeforeDelete(tempBankBranchObjs);
                using (trans = Session.BeginTransaction())
                {
                    var tempBankBranchObj = _bankBranchDao.LoadById(bankBranchId);
                    tempBankBranchObj.Status = Branch.EntityStatus.Delete;
                    _bankBranchDao.Update(tempBankBranchObj);
                    trans.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }
        //Bank Branch Dependency Check
        private void CheckBeforeDelete(BankBranch bankBranch)
        {
            try
            {
                if (bankBranch.SupplierBankDetails != null && bankBranch.SupplierBankDetails.Count > 0)
                    throw new DependencyException("You can't delete this Bank Branch, Bank Branch is already assigned to Supplier Bank Details.");
                //needs more dependency checks later
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public BankBranch GetBankBranch(long bankBranchId)
        {
            BankBranch bankBranch;
            try
            {
                bankBranch = _bankBranchDao.LoadById(bankBranchId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return bankBranch;

        }
        #endregion

        #region List Loading Function

        public IList<BankBranch> LoadBankBranch(int start, int length, string orderBy, string orderDir, string bank, string name, string shortName, string contactPerson, string contactPersonMobile, string routingNo, string swifeCode, string rank, string status)
        {
            IList<BankBranch> bankBranches;
            try
            {
                bankBranches = _bankBranchDao.LoadBankBranch(start, length, orderBy, orderDir, bank, name, shortName, contactPerson, contactPersonMobile, routingNo, swifeCode, rank, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return bankBranches;
        }
        public IList<BankBranch> LoadBankBranchList(long[] bankIdArray = null)
        {
            try
            {
                var bankBranchList = _bankBranchDao.LoadBankBranchList(bankIdArray);
                return bankBranchList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<BankBranch> LoadBankBranch(long[] bankBranchIds)
        {
            try
            {
                return _bankBranchDao.LoadBankBranchList(bankBranchIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function
        public int GetBankBranchCount(string orderBy, string orderDir, string bank, string name, string shortName, string contactPerson, string contactPersonMobile, string routingNo, string swifeCode, string rank, string status)
        {
            int bankBranchCount;
            try
            {
                bankBranchCount = _bankBranchDao.GetBankBranchCount(orderBy, orderDir, bank, name, shortName, contactPerson, contactPersonMobile, routingNo, swifeCode, rank, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return bankBranchCount;
        }
        public int GetMinimumRank(BankBranch obj)
        {
            try
            {
                if (obj == null)
                    throw new NullObjectException("Bank Branch Can not Null");
                return _bankBranchDao.GetMinimumRank(obj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public int GetMaximumRank(BankBranch obj)
        {
            try
            {
                if (obj == null)
                    throw new NullObjectException("Bank Branch Can not Null");
                return _bankBranchDao.GetMaximumRank(obj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        //Swift Code Duplication Check
        public bool IsRouteNoUnique(string code, long? id = null)
        {
            bool isExists = false;
            try
            {
                var entity = _bankBranchDao.IsRouteNoUnique(code, id);
                if (entity == null)
                {
                    isExists = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return isExists;
           
        }
        //Swift Code Duplication Check
        public bool IsSwiftCodeUnique(string code, long? id = null)
        {
            bool isExists = false;
            try
            {
                var entity = _bankBranchDao.IsSwiftCodeUnique(code, id);
                if (entity == null)
                {
                    isExists = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return isExists;
        }
        public bool UpdateRank(long id, int current, string action)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    BankBranch bankBranchUpdateObj = _bankBranchDao.LoadById(Convert.ToInt64(id));
                    if (bankBranchUpdateObj != null)
                    {
                        int newRank;
                        if (action == RankOperation.Up)
                            newRank = bankBranchUpdateObj.Rank - 1;
                        else
                            newRank = bankBranchUpdateObj.Rank + 1;
                        var bankBankOldObj = _bankBranchDao.LoadByRankDirection(newRank, action); //LoadByRank(newRank);
                        newRank = bankBankOldObj.Rank;
                        bankBankOldObj.Rank = bankBranchUpdateObj.Rank;
                        bankBranchUpdateObj.Rank = newRank;
                        _bankBranchDao.Update(bankBankOldObj);
                        _bankBranchDao.Update(bankBranchUpdateObj);
                        transaction.Commit();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Helper function

        #endregion
    }
}

