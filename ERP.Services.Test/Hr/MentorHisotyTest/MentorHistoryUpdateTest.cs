﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MentorHisotyTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "MentorHistory")]
    public class MentorHistoryUpdateTest : MentorHistoryBaseTest
    {
        [Fact]
        public void Mentor_History_UpdateMentorHistory_Should_Pass_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember).Persist();
            _mentorHistoryService.AddMentorHistory(_mentorHistoryFactory.Object.TeamMember, _mentorHistoryFactory.Object.Mentor.Pin, Convert.ToDateTime(_mentorHistoryFactory.Object.EffectiveDate).AddDays(2).ToString());
            _mentorHistoryService.UpdateMentorHistory(_mentorHistoryFactory.Object.Id, _mentorHistoryFactory.Object.Mentor.Pin, Convert.ToDateTime(_mentorHistoryFactory.Object.EffectiveDate).AddDays(3).ToString());
            Assert.True(true);
        }

        [Fact]
        public void InvalidDataException_UpdateMentorHistory_For_Id_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember).Persist();
            _mentorHistoryService.AddMentorHistory(_mentorHistoryFactory.Object.TeamMember, _mentorHistoryFactory.Object.Mentor.Pin, Convert.ToDateTime(_mentorHistoryFactory.Object.EffectiveDate).AddDays(2).ToString());
            Assert.Throws<NullReferenceException>(()=> _mentorHistoryService.UpdateMentorHistory(0, _mentorHistoryFactory.Object.Mentor.Pin, Convert.ToDateTime(_mentorHistoryFactory.Object.EffectiveDate).AddDays(3).ToString()));
        }

        [Fact]
        public void InvalidDataException_UpdateMentorHistory_For_Mentor()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember).Persist();
            _mentorHistoryService.AddMentorHistory(_mentorHistoryFactory.Object.TeamMember, _mentorHistoryFactory.Object.Mentor.Pin, Convert.ToDateTime(_mentorHistoryFactory.Object.EffectiveDate).AddDays(2).ToString());
            Assert.Throws<InvalidDataException>(() => _mentorHistoryService.UpdateMentorHistory(_mentorHistoryFactory.Object.Id, 0, Convert.ToDateTime(_mentorHistoryFactory.Object.EffectiveDate).AddDays(3).ToString()));
        }
    }
}
