﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.SpecificationTemplateTest
{
    public interface ISpecificationTemplateLoadByIdTest
    {
        #region Basic Test
        void Get_Specification_Template_NotNull_Check();
        #endregion
        
        #region Business Logic Test
        void LoadById_Null_Check(); 
        #endregion
       
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "SpecificationCriteria")]
    public class SpecificationTemplateLoadByIdTest : SpecificationTemplateBaseTest, ISpecificationTemplateLoadByIdTest
    {
        #region Business Logic Test
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _specificationTemplateService.GetSpecificationTemplate(-1);
            Assert.Null(obj);

        } 
        #endregion

        #region Basic Test
        [Fact]
        public void Get_Specification_Template_NotNull_Check()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name).Persist();
            var st = _specificationTemplateService.GetSpecificationTemplate(_specificationTemplateFactory.Object.Id);
            Assert.NotNull(st);
            var criteriaList = _specificationTemplateFactory.Object.SpecificationCriteriaList;
            CleanUpSpecificationCriteria(criteriaList);
        } 
        #endregion
    }
}
