﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class ItemOpeningBalanceController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IItemGroupService _itemGroupService;
        private readonly ItemService _itemService;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IGoodsReceiveService _goodsReceiveService;
        private readonly IBranchService _branchService;
        private List<UserMenu> _userMenu;

        public ItemOpeningBalanceController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _itemGroupService = new ItemGroupService(session);
            _itemService = new ItemService(session);
            _organizationService = new OrganizationService(session);
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _goodsReceiveService = new GoodsReceiveService(session);
            _branchService = new BranchService(session);
        }

        #endregion

        #region Operational Function

        public ActionResult Index()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.itemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
                ViewBag.itemTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<ItemType>(), "Key", "Value");
                ViewBag.BranchIdList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.programSessionList = new SelectList(string.Empty, "Value", "Text");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(ItemOpeningBalanceViewModel data)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                if (!ModelState.IsValid)
                {
                    string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    throw new MessageException("Invalid Data");
                }

                if (!data.ItemOpeningBalanceDetailsViewModelList.Any())
                {
                    throw new MessageException("Invalid Data");
                }

                var branch = _branchService.GetBranch(data.BranchId);

                //var b = AuthHelper.LoadBranchIdList(_userMenu, null, null, _commonHelper.ConvertIdToList(data.BranchId));

                var goodsReceive = new GoodsReceive();
                var goodsReceiveDetailList = new List<GoodsReceiveDetails>();

                goodsReceive.Branch = branch;
                goodsReceive.GoodsReceivedDate = data.ClosingDate;
               // goodsReceive.GoodsReceivedNo = branch.Organization.ShortName.Substring(0, 3) + branch.Code + "GR000001";
                goodsReceive.GoodsReceiveStatus = (int)GoodsReceivedStatus.Received;
                goodsReceive.Remarks = "Opening Balance";

                decimal totalCost = 0;
                var goodsReceiveId = data.ItemOpeningBalanceDetailsViewModelList[0].GoodsReceiveId;
                goodsReceive.Id = goodsReceiveId;

                Program program;
                Session session;
                if (!String.IsNullOrEmpty(data.ProgramSessionId) && data.ProgramSessionId != "0")
                {
                    var spltValue = data.ProgramSessionId.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                    program = _programService.GetProgram(Convert.ToInt64(spltValue[0]));
                    session = _sessionService.LoadById(Convert.ToInt64(spltValue[1]));
                }
                else
                {
                    program = null;
                    session = null;
                }

                foreach (var itemOpeningBalanceDetailsViewModel in data.ItemOpeningBalanceDetailsViewModelList)
                {
                    var goodsReceiveDetails = new GoodsReceiveDetails();
                    goodsReceiveDetails.GoodsReceive = goodsReceive;
                    goodsReceiveDetails.Item = _itemService.LoadById(itemOpeningBalanceDetailsViewModel.ItemId);
                    goodsReceiveDetails.Program = program;
                    goodsReceiveDetails.Session = session;

                    //check received quantity and rate
              

                    goodsReceiveDetails.ReceivedQuantity = itemOpeningBalanceDetailsViewModel.ReceivedQuantity;
                    goodsReceiveDetails.Rate = itemOpeningBalanceDetailsViewModel.Rate;
                    var tc = Math.Floor(itemOpeningBalanceDetailsViewModel.ReceivedQuantity * itemOpeningBalanceDetailsViewModel.Rate);
                    goodsReceiveDetails.TotalCost = tc;
                    totalCost = totalCost + tc;
                    goodsReceiveDetailList.Add(goodsReceiveDetails);
                }
                goodsReceive.GoodsReceiveDetails = goodsReceiveDetailList;
                goodsReceive.TotalCost = totalCost;
                _goodsReceiveService.SaveOrUpdate(goodsReceive, true);
                return Json(new Response(true, "Opening balance successfully saved."));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (MessageException ex)
            {
                //return Json(new { IsSuccess = true, Message = ex.ToString() });
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Helper Function

        [HttpPost]
        public ActionResult LoadItemList(string programSessionId, long? itemGroupId, long itemType, long organizationId, long branchId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var branch = _branchService.GetBranch(branchId);

                string grNoPrefix = branch.Organization.ShortName.Substring(0, 3) + branch.Code + "GR";
                IList<ItemOpeningBalanceDetailsViewModel> itemViewList = new List<ItemOpeningBalanceDetailsViewModel>();
                itemViewList = _goodsReceiveService.LoadOpeningBalanceItem(_userMenu, organizationId, programSessionId,grNoPrefix, itemGroupId, itemType);

                return PartialView("_partial/_itemList", itemViewList);
            }
            catch (MessageException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion
    }
}