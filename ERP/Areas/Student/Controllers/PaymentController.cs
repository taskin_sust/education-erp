﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.AspNet.Identity;
using UdvashERP.App_code;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.App_Start;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using WebGrease.Css.Extensions;
using Constants = UdvashERP.BusinessRules.Constants;
using System.Drawing;
using System.IO;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using UdvashERP.BusinessRules.Student;
using Rectangle = iTextSharp.text.Rectangle;
using UdvashERP.Properties;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class PaymentController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("StudentArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly int _duplicatePaymentBlockingTimeMinute = 10;

        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly IBatchService _batchService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly ICourseService _courseService;
        private readonly IStudentService _studentService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IStudentCourseDetailsService _studentCourseDetailsService;
        private readonly IDiscountService _discountService;
        private readonly ICommonHelper _commonHelper;
        private readonly IReferenceService _referenceService;
        private readonly IStudentPaymentService _studentPaymentService;
        private IOrganizationService _organizationService;
        private IDiscountDetailService _discountDetailService;
        private readonly IStudentImagesMediaService _studentImagesMediaService;

        private decimal receivableAmountWithoutDiscount = 0;
        private decimal receivableAmountWithDiscount = 0;
        private readonly IUserService _userService;
        private List<UserMenu> _userMenu;

        public PaymentController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _userService = new UserService(session);
                _sessionService = new SessionService(session);
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _batchService = new BatchService(session);
                _courseService = new CourseService(session);
                _studentService = new StudentService(session);
                _studentProgramService = new StudentProgramService(session);
                _studentCourseDetailsService = new StudentCourseDetailsService(session);
                _discountService = new DiscountService(session);
                _commonHelper = new CommonHelper();
                _referenceService = new ReferenceService(session);
                _studentPaymentService = new StudentPaymentService(session);
                _organizationService = new OrganizationService(session);
                _discountDetailService = new DiscountDetailService(session);
                _studentImagesMediaService = new StudentImagesMediaService();
                _duplicatePaymentBlockingTimeMinute = AppConfigHelper.GetInt32("DuplicatePaymentBlockingTimeInMinute", 10);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        #endregion

        #region Operational Function

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult StudentDuePayment(string message = "", string stdProgramId = "")
        {
            if (!String.IsNullOrEmpty(message) && !String.IsNullOrEmpty(stdProgramId))
            {
                ViewBag.ErrorMessage = message;
                ViewBag.StudentRoll = stdProgramId;
            }
            return View("StudentDuePayment");
        }
        
        public ActionResult DuePayment(string stdProgramId) /* stdProgramId : Program Roll Number */
        {
            try
            {
                SetDuePaymentFormViewData();
                ViewBag.StudentRoll = stdProgramId;
                string withLayout = Request.QueryString["withLayout"];

                if (!String.IsNullOrEmpty(stdProgramId))
                {
                    stdProgramId = stdProgramId.Trim();

                    if (stdProgramId.Length == 11)
                    {
                        var paymentdetail = new PaymentDetails();
                        var studentProgram = _studentProgramService.GetStudentProgram(stdProgramId.Trim());
                        _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                        //check authorization
                        var message = _studentProgramService.CheckAuthorizationForStudentProgram(_userMenu, studentProgram);
                        if (message != "")
                        {
                            ViewBag.StudentRoll = stdProgramId;
                            ViewBag.WithLayout = false;
                            ViewBag.ErrorMessage = message;
                            return View("StudentDuePayment");
                        }

                        if (studentProgram != null)
                        {
                            var studentViewModel = new StudentViewModel();
                            StudentPayment lastStudentPaymentTransication = _studentPaymentService.LoadByStudentProgram(studentProgram);
                            var student = _studentService.LoadById(studentProgram.Student.Id);
                            studentViewModel.Name = student.NickName;
                            studentViewModel.MobNumber = student.Mobile;
                            studentViewModel.Program = studentProgram.Program.Name;
                            studentViewModel.BatchDays = studentProgram.Batch.Days;
                            studentViewModel.BatchTime = studentProgram.Batch.FormatedBatchTime;
                            studentViewModel.Batch = studentProgram.Batch.Name;
                            studentViewModel.Id = studentProgram.Id;
                            studentViewModel.VersionOfStudy = studentProgram.VersionOfStudy;
                            studentViewModel.Gender = student.Gender;
                            studentViewModel.Religion = student.Religion;
                            var branch = _branchService.GetBranch(studentProgram.Batch.Branch.Id);
                            studentViewModel.Branch = branch.Name;
                            studentViewModel.Session = studentProgram.Batch.Session.Name;
                            studentViewModel.Campus = studentProgram.Batch.Campus.Name;

                            /* Student Course Assigned View */
                            var studentCourseDetailsList = _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);
                            var query = studentCourseDetailsList.GroupBy(x => x.CourseSubject.Course, (key, g) => new { Course = key, Subjects = g.ToList() });

                            var courseList = query.ToList();
                            foreach (var courseAndCorrespondingSubjectList in courseList)
                            {
                                var courseViewModel = new CourseViewModel
                                {
                                    Id = courseAndCorrespondingSubjectList.Course.Id,
                                    Name = courseAndCorrespondingSubjectList.Course.Name
                                };
                                var courseSubjectList = courseAndCorrespondingSubjectList.Subjects;
                                foreach (var courseSubject in courseSubjectList)
                                {
                                    var subjectViewModel = new SubjectViewModel();
                                    var subject = courseSubject.CourseSubject;
                                    subjectViewModel.Name = courseSubject.CourseSubject.Subject.Name;
                                    subjectViewModel.Payment = subject.Payment;
                                    subjectViewModel.Id = subject.Id;
                                    receivableAmountWithoutDiscount += Convert.ToDecimal(subject.Payment.ToString().Trim());
                                    courseViewModel.SubjectViewModels.Add(subjectViewModel);
                                }
                                studentViewModel.CourseViewModels.Add(courseViewModel);
                            }

                            /*New Student */
                            if (lastStudentPaymentTransication == null)
                            {
                                var existingDiscountList = _discountService.LoadDiscount(studentProgram.Program, studentProgram.Batch.Session, studentProgram.CreationDate);
                                Discount appliedDiscountObj = GetDiscount(existingDiscountList, studentViewModel);

                                if (appliedDiscountObj.Amount > 0 && appliedDiscountObj.Amount < receivableAmountWithoutDiscount)
                                {
                                    /*after calculating discount student have to pay below amount*/
                                    receivableAmountWithDiscount = (decimal)(receivableAmountWithoutDiscount - appliedDiscountObj.Amount);
                                    paymentdetail.PayableAmount = (decimal)(receivableAmountWithoutDiscount - appliedDiscountObj.Amount);
                                    paymentdetail.ConsiderationAmount = paymentdetail.PayableAmount;
                                    paymentdetail.DueAmount = paymentdetail.PayableAmount;
                                }
                                else
                                {
                                    paymentdetail.PayableAmount = receivableAmountWithoutDiscount;
                                    paymentdetail.ConsiderationAmount = paymentdetail.PayableAmount;
                                    paymentdetail.DueAmount = paymentdetail.ConsiderationAmount;
                                }
                            }
                            else
                            {
                                paymentdetail.PayableAmount = lastStudentPaymentTransication.DueAmount ?? 0;
                                paymentdetail.NetReceivable = lastStudentPaymentTransication.DueAmount ?? 0;
                                paymentdetail.ReceivedAmount = lastStudentPaymentTransication.ReceivedAmount ?? 0;
                                paymentdetail.DueAmount = lastStudentPaymentTransication.DueAmount ?? 0;

                            }
                            var refList = new SelectList(_referenceService.GetAllRefferer(), "Id", "Name");
                            ViewBag.REFERRERLIST = refList;
                            ViewBag.DISCOUNTDETAILS = paymentdetail;
                            ViewData["sPId"] = stdProgramId.Trim();

                            if (withLayout == "true")
                            {
                                return View(studentViewModel);
                            }
                            return View("DuePaymentNoLayout", studentViewModel);
                        }
                        return Json(new Response(false, "Invalid Program Roll Number", stdProgramId), JsonRequestBehavior.AllowGet);

                    }
                    return Json(new Response(false, "Invalid Program Roll Number", stdProgramId), JsonRequestBehavior.AllowGet);

                }
                return Json(new Response(false, "Invalid Program Roll Number", stdProgramId), JsonRequestBehavior.AllowGet);


            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, "Invalid Program Roll Number", stdProgramId), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Helper Function

        [HttpPost]
        public ActionResult PaymentPaidAndMoneyReceiptGeneration(string studentPaymentJson, string stdPId)
        {
            try
            {
                //string stdPId, decimal recavailAmount, decimal nRec, decimal sDis, decimal recvdAmount, string paymentMethod, string referrerenceNote, string nextRecDate = "", string referrer = ""
                var serializer = new JavaScriptSerializer();
                var studentPaymentObj = serializer.Deserialize<PaymentDetails>(studentPaymentJson);

                Referrer referrerObj = null;
                /*If Discount Applicable */
                if (studentPaymentObj.SpDiscountAmount > 0)
                {
                    referrerObj = _referenceService.LoadById(Convert.ToInt64(studentPaymentObj.ReferrerId));
                }

                if (string.IsNullOrEmpty(stdPId)) { return Json(new Response(false, "Transaction Process Failed(Possible Cause : Invalid Program Roll )")); }
                if (studentPaymentObj.ReceivableAmount < 0) { return Json(new Response(false, "Transaction Process Failed(Possible Cause : Invalid Receivable Amount )")); }
                if (studentPaymentObj.NetReceivable < 0) { return Json(new Response(false, "Transaction Process Failed(Possible Cause : Invalid Net-Receivable Amount )")); }
                if ((studentPaymentObj.ReceivedAmount + studentPaymentObj.SpDiscountAmount) <= 0) { return Json(new Response(false, "Transaction Process Failed(Possible Cause : Empty Transaction Not Supported )")); }

                if (
                    !String.IsNullOrEmpty(stdPId)
                    && studentPaymentObj.ReceivableAmount > 0
                    && studentPaymentObj.NetReceivable >= 0
                    && studentPaymentObj.ReceivedAmount >= 0
                    && (studentPaymentObj.ReceivedAmount + studentPaymentObj.SpDiscountAmount) > 0
                    && studentPaymentObj.DueAmount >= 0
                    && (studentPaymentObj.DueAmount == studentPaymentObj.NetReceivable - studentPaymentObj.ReceivedAmount)
                    )
                {
                    var studentProgram = _studentProgramService.GetStudentProgram(stdPId);
                    StudentPayment lastStudentPaymentTransication = _studentPaymentService.LoadByStudentProgram(studentProgram);
                    string recAmountFromStudent = GetReceivableAmountFromStudent(stdPId);
                    var recAmountDisAmount = recAmountFromStudent.Split('#');

                    StudentPayment studentPayment = null;
                    if (lastStudentPaymentTransication == null || lastStudentPaymentTransication.DueAmount == null)
                    {
                        studentPayment = _studentPaymentService.OrganizeObjectForNewAdmission(
                            studentPaymentObj.ReceivableAmount,
                            studentPaymentObj.ReceivedAmount,
                            studentPaymentObj.SpDiscountAmount,
                            studentPaymentObj.NetReceivable,
                            studentPaymentObj.Remarks,
                            studentPaymentObj.PaymentMethod.ToString(),
                            referrerObj, recAmountDisAmount, studentProgram.PrnNo
                        );

                        studentPayment.PaymentType = PaymentType.MoneyReceipt;
                        studentPayment.NextReceivedDate = GetNextReceivedDate(studentPaymentObj);
                        var studentCourseDetail = _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);
                        studentPayment.CourseSubjectList = new List<CourseSubject>();

                        foreach (var cs in studentCourseDetail)
                        {
                            studentPayment.CourseSubjectList.Add(cs.CourseSubject);
                        }
                    }
                    else if (lastStudentPaymentTransication.DueAmount > 0)
                    {
                        studentPayment = _studentPaymentService.OrganizeObjectForDuePayment(
                            (decimal)lastStudentPaymentTransication.DueAmount, studentPaymentObj.ReceivedAmount,
                            studentPaymentObj.SpDiscountAmount, (decimal)(lastStudentPaymentTransication.DueAmount - studentPaymentObj.SpDiscountAmount),
                            studentPaymentObj.Remarks,
                            studentPaymentObj.PaymentMethod.ToString(), referrerObj, recAmountDisAmount,
                            stdPId.Trim());
                        studentPayment.PaymentType = PaymentType.MoneyReceipt;
                        studentPayment.NextReceivedDate = GetNextReceivedDate(studentPaymentObj);
                    }
                    else
                    {
                        return Json(new Response(false, "Payment rejected. No due amount found for student PRN: ", studentProgram.PrnNo));
                    }

                    DateTime? lastPaymentTime;
                    if (!CheckDuplicateSubmission(studentPayment, stdPId.Trim(), out lastPaymentTime))
                    {
                        bool isPaymentProcessSuccess = _studentPaymentService.IsPaymentProcessSuccess(studentPayment, stdPId.Trim());
                        if (isPaymentProcessSuccess)
                        {
                            var sPayment = _studentPaymentService.LoadByStudentProgram(studentProgram);
                            bool flagForSms = SendSmsApi.GenarateInstantSmsAndNumber(2, sPayment.StudentProgram.Program.Organization.Id,
                                sPayment.StudentProgram.Program.Id, sPayment.StudentProgram.Batch.Session.Id,
                                sPayment.StudentProgram.Student.Id, sPayment.StudentProgram.Id);
                            if (studentProgram.IsPolitical == false && studentPayment.Referrer != null && studentPayment.Referrer.Code == "04")/*FIXED FOR POLITICAL REFERAR*/
                            {
                                var temStudentProgram = _studentProgramService.GetStudentProgram(studentProgram.PrnNo);
                                if (temStudentProgram != null)
                                {
                                    temStudentProgram.IsPolitical = true;
                                    _studentProgramService.Update(temStudentProgram);
                                }
                            }
                            return Json(new Response(true, "Payment success", studentPayment.Id.ToString()));
                        }
                    }
                    else
                    {
                        if (lastPaymentTime != null)
                        {
                            var timeStamp = lastPaymentTime.GetValueOrDefault().AddMinutes(_duplicatePaymentBlockingTimeMinute) - DateTime.Now;
                            var blockingTime = timeStamp.ToString(@"mm\:ss");
                            return Json(new Response(
                                    false,
                                    string.Format("For Student Program ID {0} paid {1} at {2}. Please try again after {3} minute",
                                        stdPId,
                                        studentPayment.ReceivedAmount,
                                        lastPaymentTime.GetValueOrDefault().ToString("yyyy-M-d hh:mm:ss tt"),
                                        blockingTime)
                            ));
                        }

                    }
                    return Json(new Response(false, "Transaction Process Failed( Possible cause: Invalid data is provided )"));
                }
                return Json(new Response(false, "Transaction Process Failed( Possible Cause : Invalid Program Roll or Payment informations are modified )"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                _logger.Error(" Student Program Id: " + stdPId);

                return Json(new Response(false, "Transaction Process Failed. Possible cause: " + ex.Message));
            }
        }

        private bool CheckDuplicateSubmission(StudentPayment studentPayment, string studentProgramId, out DateTime? lastPaymentTime)
        {
            StudentPayment lastPaymentWithInBlockingTime = _studentPaymentService.GetLastPaymentWithInBlockingTime(studentPayment, studentProgramId, _duplicatePaymentBlockingTimeMinute);
            if (lastPaymentWithInBlockingTime == null)
            {
                lastPaymentTime = null;
                return false;
            }
            lastPaymentTime = lastPaymentWithInBlockingTime.CreationDate;
            return true;
        }

        private DateTime GetNextReceivedDate(PaymentDetails studentPaymentObj)
        {
            if (studentPaymentObj.ReceivedAmount ==
                (studentPaymentObj.ReceivableAmount - studentPaymentObj.SpDiscountAmount) &&
                studentPaymentObj.ReceivableAmount >= studentPaymentObj.ReceivedAmount)
            {
                return Constants.MaxDateTime;
            }
            else
            {
                if (!String.IsNullOrEmpty(studentPaymentObj.NextReceivedDate))
                {
                    var nextRDate = Convert.ToDateTime(studentPaymentObj.NextReceivedDate);
                    if (nextRDate.Date >= DateTime.Now.Date)
                    {
                        return nextRDate;
                    }

                }
                throw new Exception("Transaction Process Failed(Next Receivable date must be greater than Today's Date)");
            }
        }

        #endregion

        #region Reports

        #region Due List Report

        //due list report 
        public ActionResult DueList()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organizationList = _organizationService.LoadAuthorizedOrganization(_userMenu);
                ViewBag.OrganizationId = new SelectList(organizationList, "Id", "ShortName");
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return View();
        }

        [HttpGet]
        public ActionResult GenerateDueListReport()
        {
            return RedirectToAction("DueList");
        }

        [HttpPost]
        public ActionResult GenerateDueListReport(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, int? paymentStatus, int? imageStatus, int? printingStatus, string[] informationViewList, DateTime tillDate, string[] batchDays = null, string[] batchTime = null)
        {

            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var programName = _programService.GetProgram(programId).Name;
            var sessionName = _sessionService.LoadById(sessionId).Name;
            ViewBag.sessionNamee = sessionName;
            ViewBag.programName = programName;
            ViewBag.brancheNames = "All";
            ViewBag.campusNames = "All";
            ViewBag.batchDay = "All";
            ViewBag.batcheNames = "All";
            ViewBag.courseNames = "All";

            if (!branchId.Contains(0))
            {
                string branchListNames = "";
                IList<Branch> branchList = _branchService.LoadBranch().Where(br => br.Id.In(branchId)).ToList();
                if (branchList != null)
                {
                    foreach (var b in branchList)
                    {
                        branchListNames += b.Name + ", ";
                    }
                }
                if (!String.IsNullOrEmpty(branchListNames))
                {
                    branchListNames = branchListNames.Remove(branchListNames.Length - 2);
                }
                ViewBag.brancheNames = branchListNames;
            }

            if (!campusId.Contains(0))
            {
                string campusListNames = "";
                IList<Campus> campusList = _campusService.LoadCampus().Where(x => x.Status == Campus.EntityStatus.Active).Where(c => c.Id.In(campusId)).ToList();
                if (campusList != null)
                {
                    foreach (var c in campusList)
                    {
                        campusListNames += c.Name + ", ";
                    }
                }
                if (!String.IsNullOrEmpty(campusListNames))
                {
                    campusListNames = campusListNames.Remove(campusListNames.Length - 2);
                }
                ViewBag.campusNames = campusListNames;
            }
            if (batchDays != null && !batchDays.Contains("0"))
            {
                string batchDaysString = "";
                foreach (var bd in batchDays)
                {
                    batchDaysString += bd + "; ";
                }
                batchDaysString = batchDaysString.Remove(batchDaysString.Length - 2);
                ViewBag.batchDay = batchDaysString;
            }


            if (!batchName.Contains(0))
            {
                string batchListNames = "";
                IList<Batch> batchList = _batchService.LoadBatch().Where(x => x.Status == Batch.EntityStatus.Active).Where(c => c.Id.In(batchName)).ToList();
                if (batchList != null)
                {
                    foreach (var b in batchList)
                    {
                        batchListNames += b.Name + ", ";
                    }
                }
                if (!String.IsNullOrEmpty(batchListNames))
                {
                    batchListNames = batchListNames.Remove(batchListNames.Length - 2);
                }
                ViewBag.batcheNames = batchListNames;
            }

            if (!course.Contains(0))
            {
                string courseListNames = "";
                IList<Course> courseList = _courseService.LoadCourse(null, null, null).Where(x => x.Status == Course.EntityStatus.Active).Where(c => c.Id.In(course)).ToList();
                if (courseList != null)
                {
                    foreach (var b in courseList)
                    {
                        courseListNames += b.Name + ", ";
                    }
                }
                if (!String.IsNullOrEmpty(courseListNames))
                {
                    courseListNames = courseListNames.Remove(courseListNames.Length - 2);
                }
                ViewBag.courseNames = courseListNames;
            }

            ViewBag.programId = programId;
            ViewBag.sessionId = sessionId;
            ViewBag.branchId = branchId;
            ViewBag.campusId = campusId;
            ViewBag.batchName = batchName;
            ViewBag.course = course;
            ViewBag.paymentStatus = paymentStatus;
            ViewBag.imageStatus = imageStatus;
            ViewBag.printingStatus = printingStatus;
            ViewBag.informationViewList = informationViewList;
            ViewBag.batchDays = batchDays;
            ViewBag.batchTime = batchTime;
            ViewBag.tillDate = tillDate;

            ViewBag.PageSize = Constants.PageSize;
            return View();
        }

        #endregion

        #region Transaction Report

        public ActionResult TransactionReport()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;             
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                organizations.Insert(0, new Organization() { ShortName = "All", Id = 0 });
                ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");

                //get session list
                var sessionList = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Select Session", Value = "" } }, "Value", "Text");
                List<SelectListItem> sessionListAll = sessionList.ToList();
                ViewBag.SessionId = new SelectList(sessionListAll, "Value", "Text");
               
                //get payment method
                SelectList paymentMethodSelectList = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentMethod>(new List<int> { (int)PaymentMethod.Cheque }), "Key", "Value");
                List<SelectListItem> paymentMethodlist = paymentMethodSelectList.ToList();
                paymentMethodlist.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
                ViewBag.PaymentMethod = new SelectList(paymentMethodlist, "Value", "Text");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return View();
        }

        [HttpGet]
        public ActionResult GenerateTransactionReport()
        {
            return RedirectToAction("TransactionReport");
        }

        [HttpPost]
        public ActionResult GenerateTransactionReport(long[] organizationId, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] UserIds, string datef, string datet, long[] userBranchId, long[] userCampusId, int PaymentMethod = 0) 
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
            if (!organizationId.Contains(SelectionType.SelelectAll))
            {
                organizations = organizations.Where(x => x.Id.In(organizationId)).ToList();
            }
            var organizationIdList = organizations.Select(x => x.Id).ToList();
            IList<Program> programs = _programService.LoadProgram(organizationIdList, null);
            if (!programId.Contains(SelectionType.SelelectAll))
            {
                programs = programs.Where(x => x.Id.In(programId)).ToList();
            }
            var programIdList = programs.Select(x => x.Id).ToList();
            IList<Session> sessions = _sessionService.LoadSession(organizationIdList, programIdList);
            
            if (!sessionId.Contains(SelectionType.SelelectAll))
            {
                sessions = sessions.Where(x => x.Id.In(sessionId)).ToList();
            }
            var sessionIdList = sessions.Select(x => x.Id).ToList();
            IList<Branch> branches = _branchService.LoadBranch(organizationIdList, programIdList, sessionIdList,branchId.ToList());

            var branchIdList = branches.Select(x => x.Id).ToList();
            IList<Campus> campuses = _campusService.LoadCampus(organizationIdList, programIdList, sessionIdList, branchIdList, campusId.ToList(), false);

            IList<Branch> userBranches = _branchService.LoadAuthorizedBranch(_userMenu);
            if (!userBranchId.Contains(SelectionType.SelelectAll))
            {
                userBranches = userBranches.Where(x => x.Id.In(userBranchId)).ToList();
            }
            var userBranchIdList = userBranches.Select(x => x.Id).ToList();
            IList<Campus> userCampuses = _campusService.LoadAuthorizeCampus(_userMenu, null, null, userBranchIdList);
            if (!userCampusId.Contains(SelectionType.SelelectAll))
            {
                userCampuses = userCampuses.Where(x => x.Id.In(userCampusId)).ToList();
            }
            var userCampusIdList = userCampuses.Select(x => x.Id).ToList();
            var campusIdList = campuses.Select(x => x.Id).ToList();
            IList<long> aspNetUserIdList = _userService.LoadAuthorizedUser(_userMenu, null, null, null, userBranchIdList, userCampusIdList).Where(x => x.AspNetUser.Id != 1).Select(x=>x.AspNetUser.Id).ToList();
            if (!UserIds.Contains(SelectionType.SelelectAll))
            {
                aspNetUserIdList = aspNetUserIdList.Where(x => x.In(UserIds)).ToList();
            }
            if (!organizationId.Contains(0))
            {
                ViewBag.orgName = string.Join(", ", organizations.Select(o => o.Name).ToList());
            }
            else
            {
                ViewBag.orgName = "All";
            }

            if (!programId.Contains(0))
            {
                ViewBag.programName = string.Join(", ", programs.Select(p => p.Name).ToList());
            }
            else
            {
                ViewBag.programName = "All";
            }

            if (!sessionId.Contains(0))
            {
                ViewBag.sessionName = string.Join(", ", sessions.Select(s=>s.Name).ToList());
            }
            else
            {
                ViewBag.sessionName = "All";
            }

            if (!campusId.Contains(0))
            {
                ViewBag.campusName = string.Join(", ", campuses.Select(c => c.Name).ToList());
            }
            else
            {
                ViewBag.campusName = "All";
            }
            if (!branchId.Contains(0))
            {
                ViewBag.branchName = string.Join(", ", branches.Select(br => br.Name).ToList());
            }
            else
            {
                ViewBag.branchName = "All";
            }
            ViewBag.organizationId = organizationIdList.ToArray();
            ViewBag.programId = programIdList.ToArray();
            ViewBag.sessionId = sessionIdList.ToArray();
            ViewBag.branchId = branchIdList.ToArray();
            ViewBag.campusId = campusIdList.ToArray();
            ViewBag.UserBranchIds = userBranchIdList.ToArray();
            ViewBag.UserCampusIds = userCampusIdList.ToArray();
            ViewBag.UserIds = aspNetUserIdList.ToArray();
            ViewBag.datef = datef;
            ViewBag.datet = datet;
            ViewBag.PaymentMethod = PaymentMethod;
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }

        [HttpPost]
        public JsonResult TransactionReportResult(int draw, int start, int length, long[] organizationId, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] userBranchIds, long[] userCampusIds, long[] userIds, DateTime datef, DateTime datet, int PaymentMethod)
        { 
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //IList<UserProfile> users = _userService.LoadUser(userBranchIds, userCampusIds).Where(x => x.AspNetUser.Id != 1).OrderBy(x => x.AspNetUser.Email).ToList();
                    int recordsTotal = _studentPaymentService.GetTransactionReportCount(_userMenu,organizationId,programId,branchId,sessionId,campusId,null, userIds, 
                        datef, datet, PaymentMethod);
                    long recordsFiltered = recordsTotal;


                    IList<StudentPayment> studentPaymentList = _studentPaymentService.GetTransactionReport(_userMenu,organizationId,programId,branchId,sessionId,campusId,null, userIds, 
                        datef, datet, PaymentMethod, start, length);

                    #region datatable functionalities
                    decimal totalIncome = 0,
                                 totalConsiderationAmount = 0,
                                 totalPreviousDue = 0,
                                 totalReceivable = 0,
                                 totalSpDiscount = 0,
                                 totalReceived = 0,
                                 totalCashBack = 0,
                                 totalCurrentDue = 0;
                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var c in studentPaymentList)
                    {
                        var str = new List<string>();
                        if (studentPaymentList != null)
                        {
                            //sl.
                            str.Add(sl.ToString());
                            //date
                            if (c.CreationDate != null)
                            {
                                str.Add(String.Format("{0:dd/MM/yyyy}", c.CreationDate));
                            }
                            else
                            {
                                str.Add("");
                            }
                            str.Add(c.Batch.Branch.Name);
                            str.Add(c.Batch.Campus.Name);
                            //program name
                            str.Add(!string.IsNullOrEmpty(c.StudentProgram.Program.ShortName) ? c.StudentProgram.Program.ShortName : "");
                            //program roll
                            str.Add(c.StudentProgram.PrnNo);
                            str.Add(c.StudentProgram.Student.RegistrationNo);
                            //mrn
                            if (c.PaymentType == 1)
                            {
                                if (c.ReceiptNo != null)
                                {
                                    str.Add("<a class='mrnclass' href='GenerateMoneyReciept/" + c.Id + "' name='" + c.Id +
                                            "' id='" + c.Id + "'>" + c.ReceiptNo + "<a/>");
                                }
                                else
                                {
                                    str.Add("");
                                }
                            }
                            else
                            {
                                str.Add("");
                            }
                            //crn
                            if (c.PaymentType == 2)
                            {
                                if (c.ReceiptNo != null)
                                {
                                    str.Add("<a class='mrnclass' href='GenerateMoneyReciept/" + c.Id + "' name='" + c.Id +
                                            "' id='" + c.Id + "'>" + c.ReceiptNo + "<a/>");
                                }
                                else
                                {
                                    str.Add("");
                                }
                            }
                            else
                            {
                                str.Add("");
                            }
                            //nickname
                            if (c.StudentProgram.Student != null && c.StudentProgram.Student.Name != null && c.StudentProgram.Student.Name.Trim() != "")
                            {
                                str.Add(c.StudentProgram.Student.NickName);
                            }
                            else
                            {
                                str.Add(c.StudentProgram.Student.NickName == null ? "" : c.StudentProgram.Student.NickName);
                            }
                            //income
                            decimal coursefees = 0;
                            decimal discount = 0;
                            decimal income = 0;
                            if (c.CourseFees != null)
                            {
                                coursefees = Convert.ToDecimal(c.CourseFees);
                            }

                            if (c.DiscountAmount != null)
                            {
                                discount = Convert.ToDecimal(c.DiscountAmount);
                            }

                            income = Convert.ToDecimal(coursefees - discount);
                            if (income != 0)
                            {
                                str.Add(String.Format("{0:0}", income));
                                totalIncome += Convert.ToDecimal(income);
                            }
                            else
                            {
                                str.Add("");
                            }
                            //ConsiderationAmount
                            if (c.ConsiderationAmount != null)
                            {
                                str.Add(String.Format("{0:0}", c.ConsiderationAmount));
                                totalConsiderationAmount += Convert.ToDecimal(c.ConsiderationAmount);
                            }
                            else
                            {
                                str.Add("");
                            }
                            //previous due
                            decimal previousDue = 0;
                            decimal payableAmount = 0;
                            decimal consideration = 0;
                            if (c.PayableAmount != null)
                            {
                                payableAmount = Convert.ToDecimal(c.PayableAmount);
                            }

                            if (c.ConsiderationAmount != null)
                            {
                                consideration = Convert.ToDecimal(c.ConsiderationAmount);
                            }

                            previousDue = Convert.ToDecimal(payableAmount - income);
                            str.Add(String.Format("{0:0}", previousDue));
                            totalPreviousDue += previousDue;
                            //recievable
                            str.Add(String.Format("{0:0}", payableAmount));
                            totalReceivable += payableAmount;
                            //special discount
                            if (c.SpDiscountAmount != null)
                            {
                                if (c.SpDiscountAmount != 0)
                                {
                                    str.Add(String.Format("{0:0}", c.SpDiscountAmount));
                                    totalSpDiscount += Convert.ToDecimal(c.SpDiscountAmount);
                                }
                                else
                                {
                                    str.Add("");
                                }
                            }
                            else
                            {
                                str.Add("");
                            }
                            //recieved
                            if (c.ReceivedAmount != null)
                            {
                                if (c.ReceivedAmount != 0)
                                {
                                    str.Add(String.Format("{0:0}", c.ReceivedAmount));
                                    totalReceived += Convert.ToDecimal(c.ReceivedAmount);
                                }
                                else
                                {
                                    str.Add("");
                                }
                            }
                            else
                            {
                                str.Add("");
                            }
                            //cash back
                            if (c.CashBackAmount != null)
                            {
                                if (c.CashBackAmount != 0)
                                {
                                    str.Add(String.Format("{0:0}", c.CashBackAmount));
                                    totalCashBack += Convert.ToDecimal(c.CashBackAmount);
                                }
                                else { str.Add(""); }
                            }
                            else
                            {
                                str.Add("");
                            }
                            //current due
                            if (c.DueAmount != null)
                            {
                                str.Add(String.Format("{0:0}", c.DueAmount));
                                totalCurrentDue += Convert.ToDecimal(c.DueAmount);
                            }
                            else
                            {
                                str.Add("0.00");
                            }
                            //payment method
                            if (c.PaymentMethod != null)
                            {
                                str.Add(_commonHelper.GetEmumIdToValue<PaymentMethod>((int)c.PaymentMethod));
                            }
                            else
                            {
                                str.Add("");
                            }
                            //referrer

                            if (c.Referrer != null && c.Referrer.Name != null)
                            {
                                str.Add(c.Referrer.Name);
                            }
                            else
                            {
                                str.Add("");
                            }

                            //referrer note

                            if (c.PaymentMethod == (int)BusinessRules.Student.PaymentMethod.Cash)
                            {
                                if (!string.IsNullOrEmpty(c.SpReferenceNote))
                                {
                                    str.Add(c.SpReferenceNote);
                                }
                                else
                                {
                                    str.Add("");
                                }
                            }
                            else if (c.PaymentMethod == (int)BusinessRules.Student.PaymentMethod.BKash)
                            {
                                var transcationIds = "";
                                if (c.OnlineTransactionses != null)
                                {
                                    var onlineTransactions =
                                        c.OnlineTransactionses.Where(
                                            x => x.CreationDate.Date >= datef.Date && x.CreationDate.Date <= datet.Date).ToList();

                                    if (onlineTransactions != null && onlineTransactions.Count > 0)
                                    {
                                        transcationIds = string.Join(", ", onlineTransactions.Select(x => x.TransactionId).ToList());
                                    }
                                }
                                str.Add(transcationIds);
                            }

                            //remarks
                            if (c.Remarks != null)
                            {
                                str.Add(c.Remarks);
                            }
                            else
                            {
                                str.Add("");
                            }
                            //user
                            if (c.CreateBy != null && c.CreateBy > 0)
                            {
                                var username = _userService.LoadAspNetUserById(c.CreateBy);
                                str.Add(username.UserName);
                            }
                            else
                            {
                                str.Add("");
                            }
                            sl++;
                            data.Add(str);
                        }
                    }
                    var summary = new List<string>();
                    summary.Add("");
                    summary.Add("<strong>Total Summary</strong>");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("<strong>" + String.Format("{0:0}", totalReceived) + "<strong>");
                    summary.Add("<strong>" + String.Format("{0:0}", totalCashBack) + "<strong>");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    data.Add(summary);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                    #endregion
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return Json(HttpNotFound());
        }

        [HttpGet]
        public ActionResult TransactionReportToExcel(long[] organizationId, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] userBranchIds, long[] userCampusIds, long[] UserIds, string programName, string sessionName, string campusName, string branchName, string datef, string datet, int PaymentMethod)
        { 
            try
            {
                decimal totalIncome = 0,
                              totalConsiderationAmount = 0,
                              totalPreviousDue = 0,
                              totalReceivable = 0,
                              totalSpDiscount = 0,
                              totalReceived = 0,
                              totalCashBack = 0,
                              totalCurrentDue = 0;


                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
               
                DateTime dateFrom = Convert.ToDateTime(datef);
                DateTime dateTo = Convert.ToDateTime(datet);
               
                List<string> headerList = new List<string>();
                headerList.Add(ErpInfo.OrganizationNameFull);
                headerList.Add(programName);
                headerList.Add("Transaction Report");
                headerList.Add("Session: " + sessionName);
                headerList.Add("Campus: " + campusName);
                headerList.Add("Branch: " + branchName);
                // headerList.Add("course: " + courseName);
                List<string> footerList = new List<string>();

                List<string> columnList = new List<string>();
                columnList.Add("Date");
                columnList.Add("Branch");
                columnList.Add("Campus");
                columnList.Add("Program");
                columnList.Add("Program Roll");
                columnList.Add("Registration No.");
                columnList.Add("MRN");
                columnList.Add("CRN");
                columnList.Add("Nick Name");
                columnList.Add("Income");
                columnList.Add("Consideration Amount");
                columnList.Add("Previous Due");
                columnList.Add("Receivable");
                columnList.Add("Special Discount");
                columnList.Add("Received");
                columnList.Add("Cash Back");
                columnList.Add("Current Due");
                columnList.Add("Payment Method");
                columnList.Add("Referrer");
                columnList.Add("Reference Note");
                columnList.Add("Remarks");
                columnList.Add("User");
                var transactionXlsList = new List<List<object>>();

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //IList<UserProfile> users = _userService.LoadUser(userBranchIds, userCampusIds).Where(x => x.AspNetUser.Id != 1).OrderBy(x => x.AspNetUser.Email).ToList();
                
                IList<StudentPayment> studentPaymentList = _studentPaymentService.GetTransactionReport(_userMenu, organizationId, programId, branchId, sessionId, campusId, null, UserIds,
                    dateFrom, dateTo, PaymentMethod, 0, 0);
                foreach (var c in studentPaymentList)
                {
                    string creationDate = "",
                              branchNameValue = "",
                              campusNameValue = "",
                              programNameValue = "",
                              prnNo = "",
                              RegistrationNo = "",
                              mrn = "",
                              crn = "",
                              nickName = "",
                              incomeValue = "",
                              considerationAmountValue = "",
                              previousDueValue = "",
                              recievableValue = "",
                              spDiscountAmountValue = "",
                              receivedAmountValue = "",
                              cashBackAmountValue = "",
                              dueAmountValue = "",
                              paymentMethodValue = "",
                              refererName = "",
                              refererNote = "",
                              remarksValue = "",
                              usernameValue = "";
                    if (studentPaymentList != null)
                    {

                        if (c.CreationDate != null)
                        {
                            creationDate = String.Format("{0:dd/MM/yyyy}", c.CreationDate);
                        }
                        branchNameValue = c.Batch.Branch.Name;
                        campusNameValue = c.Batch.Campus.Name;
                        //program name
                        programNameValue = !string.IsNullOrEmpty(c.StudentProgram.Program.ShortName) ? c.StudentProgram.Program.ShortName : "";
                        //program roll
                        prnNo = c.StudentProgram.PrnNo;
                        RegistrationNo = c.StudentProgram.Student.RegistrationNo;
                        //mrn
                        if (c.PaymentType == 1)
                        {
                            if (c.ReceiptNo != null)
                            {
                                mrn = c.ReceiptNo;
                            }

                        }
                        //crn
                        if (c.PaymentType == 2)
                        {
                            if (c.ReceiptNo != null)
                            {
                                crn = c.ReceiptNo;
                            }

                        }

                        //nickname
                        if (c.StudentProgram.Student != null && c.StudentProgram.Student.NickName != null && c.StudentProgram.Student.NickName.Trim() != "")
                        {
                            nickName = c.StudentProgram.Student.NickName;
                        }
                        //income
                        decimal coursefees = 0;
                        decimal discount = 0;
                        decimal income = 0;
                        if (c.CourseFees != null)
                        {
                            coursefees = Convert.ToDecimal(c.CourseFees);
                        }

                        if (c.DiscountAmount != null)
                        {
                            discount = Convert.ToDecimal(c.DiscountAmount);
                        }

                        income = Convert.ToDecimal(coursefees - discount);
                        if (income != 0)
                        {
                            incomeValue = String.Format("{0:0}", income);
                            totalIncome += Convert.ToDecimal(income);
                        }

                        //ConsiderationAmount
                        if (c.ConsiderationAmount != null)
                        {
                            considerationAmountValue = String.Format("{0:0}", c.ConsiderationAmount);
                            totalConsiderationAmount += Convert.ToDecimal(c.ConsiderationAmount);
                        }

                        //previous due
                        decimal previousDue = 0;
                        decimal payableAmount = 0;
                        decimal consideration = 0;
                        if (c.PayableAmount != null)
                        {
                            payableAmount = Convert.ToDecimal(c.PayableAmount);
                        }

                        if (c.ConsiderationAmount != null)
                        {
                            consideration = Convert.ToDecimal(c.ConsiderationAmount);
                        }

                        previousDue = Convert.ToDecimal(payableAmount - income);
                        previousDueValue = String.Format("{0:0}", previousDue);
                        totalPreviousDue += previousDue;
                        //recievable
                        recievableValue = String.Format("{0:0}", payableAmount);
                        totalReceivable += payableAmount;
                        //special discount
                        if (c.SpDiscountAmount != null)
                        {
                            if (c.SpDiscountAmount != 0)
                            {
                                spDiscountAmountValue = String.Format("{0:0}", c.SpDiscountAmount);
                                totalSpDiscount += Convert.ToDecimal(c.SpDiscountAmount);
                            }

                        }

                        //recieved
                        if (c.ReceivedAmount != null)
                        {
                            if (c.ReceivedAmount != 0)
                            {
                                receivedAmountValue = String.Format("{0:0}", c.ReceivedAmount);
                                totalReceived += Convert.ToDecimal(c.ReceivedAmount);
                            }

                        }

                        //cash back
                        if (c.CashBackAmount != null)
                        {
                            if (c.CashBackAmount != 0)
                            {
                                cashBackAmountValue = String.Format("{0:0}", c.CashBackAmount);
                                totalCashBack += Convert.ToDecimal(c.CashBackAmount);
                            }

                        }

                        //current due
                        if (c.DueAmount != null)
                        {
                            dueAmountValue = String.Format("{0:0}", c.DueAmount);
                            totalCurrentDue += Convert.ToDecimal(c.DueAmount);
                        }
                        else
                        {
                            dueAmountValue = "0.00";
                        }
                        //payment method
                        if (c.PaymentMethod != null)
                        {
                            paymentMethodValue = _commonHelper.GetEmumIdToValue<PaymentMethod>((int)c.PaymentMethod);
                        }

                        //referer
                        if (c.Referrer != null && c.Referrer.Name != null)
                        {
                            refererName = c.Referrer.Name;
                        }
                        else
                        {
                            refererName = "";
                        }


                        //referrer note
                        if (c.PaymentMethod == (int)BusinessRules.Student.PaymentMethod.Cash)
                        {
                            if (!string.IsNullOrEmpty(c.SpReferenceNote))
                            {
                                refererNote = c.SpReferenceNote;
                            }

                            else
                            {
                                refererNote = "";
                            }
                        }
                        else if (c.PaymentMethod == (int)BusinessRules.Student.PaymentMethod.BKash)
                        {
                            var transcationIds = "";
                            if (c.OnlineTransactionses != null)
                            {
                                var onlineTransactions =
                                    c.OnlineTransactionses.Where(
                                        x => x.CreationDate.Date >= dateFrom.Date && x.CreationDate.Date <= dateTo.Date).ToList();

                                if (onlineTransactions != null && onlineTransactions.Count > 0)
                                {
                                    transcationIds = string.Join(", ", onlineTransactions.Select(x => x.TransactionId).ToList());
                                }
                            }
                            refererNote = transcationIds;
                        }

                        //remarks
                        if (c.Remarks != null)
                        {
                            remarksValue = c.Remarks;
                        }

                        //user
                        if (c.CreateBy != null && c.CreateBy > 0)
                        {
                            var username = _userService.LoadAspNetUserById(c.CreateBy);
                            usernameValue = username.UserName;
                        }

                    }
                    var xlsRow = new List<object> { creationDate, branchNameValue, campusNameValue, programNameValue, prnNo, RegistrationNo, mrn, crn, nickName, incomeValue, considerationAmountValue, previousDueValue, recievableValue, spDiscountAmountValue, receivedAmountValue, cashBackAmountValue, dueAmountValue, paymentMethodValue, refererName, refererNote, remarksValue, usernameValue };
                    transactionXlsList.Add(xlsRow);
                }
                var summaryRow = new List<object> { "Total Summary", "", "", "", "", "", "", "", "", "", "", "", "", "", String.Format("{0:0}", totalReceived), String.Format("{0:0}", totalCashBack), "", "", "", "", "", "" };
                transactionXlsList.Add(summaryRow);
                footerList.Add("Total Received Amount: " + String.Format("{0:0}", totalReceived));
                footerList.Add("Total Cash Back: " + String.Format("{0:0}", totalCashBack));
                footerList.Add("Total: " + String.Format("{0:0}", (totalReceived - totalCashBack)));
                footerList.Add("");
                //footerList.Add("Developed By OnnoRokom Software Ltd.");
                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "all-Transaction-list__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }

        }


        #endregion

        #region Payment History

        //payment history
        public ActionResult PaymentHistory(string message = "", string roll = "")
        {
            if (!String.IsNullOrEmpty(message))
            {
                ViewBag.ErrorMessage = message;
                ViewBag.roll = roll;
            }
            return View();
        }

        [HttpGet]
        public ActionResult HistoryOfPayment(string stdProRoll, long programId = 0, long sessionId = 0)
        {
            try
            {
                if (!String.IsNullOrEmpty(stdProRoll) && stdProRoll.Trim().Length == 11 && System.Text.RegularExpressions.Regex.IsMatch(stdProRoll, "^[0-9]*$"))
                {
                    stdProRoll = stdProRoll.Trim();
                    var studentProgram = _studentProgramService.GetStudentProgram(stdProRoll, true);
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    if (studentProgram != null)
                    {
                        //check authorization
                        var message = _studentProgramService.CheckAuthorizationForStudentProgram(_userMenu, studentProgram);
                        if (message != "")
                            return RedirectToAction("PaymentHistory", new { message = "You are not authorized to view this history", roll = stdProRoll });
                        //if (programs.Contains(studentProgram.Program) && authorizedSession.Contains(studentProgram.Batch.Session) && authorizedBranches.Contains(studentProgram.Batch.Branch))
                        //{
                        ViewBag.stdProRoll = stdProRoll;
                        var studentPaymentHistories = new List<StudentPaymentHistory>();
                        var studentBasicInformation = new StudentBasicInformation();
                        {
                            if (studentProgram.StudentImage.Count > 0)
                            {
                                StudentMediaImage mediaStudentImages =
                                    _studentImagesMediaService.GetStudentImageMediaByRefId(
                                        studentProgram.StudentImage[0].Id);
                                if (mediaStudentImages != null)
                                {
                                    studentProgram.MediaStudentImage = mediaStudentImages;
                                    ViewBag.StudentProgram = studentProgram;
                                }
                                else
                                {
                                    ViewBag.StudentProgram = new StudentProgram();
                                }
                            }
                            else
                            {
                                ViewBag.StudentProgram = new StudentProgram();
                            }
                        }

                        ViewBag.ProgramName = studentProgram.Program.Name;
                        ViewBag.SessionName = studentProgram.Batch.Session.Name;
                        ViewBag.StudentName = studentProgram.Student.NickName;
                        ViewBag.prn = studentProgram.PrnNo;
                        studentBasicInformation.DateOfAdmission = studentProgram.CreationDate.ToString("dd/MM/yyyy");
                        studentBasicInformation.ProgramRoll = studentProgram.PrnNo;
                        studentBasicInformation.StudentName = studentProgram.Student.NickName;
                        studentBasicInformation.ContactNumber = studentProgram.Student.Mobile;
                        studentBasicInformation.Branch = studentProgram.Batch.Branch.Name;
                        studentBasicInformation.BatchDays = studentProgram.Batch.Days.Replace(','.ToString(), ", ");
                        studentBasicInformation.BatchName = studentProgram.Batch.Name;
                        var studentCList = _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);
                        studentBasicInformation.StudentCourseDetails = studentCList;
                        IList<StudentPayment> studentPaymentList = _studentPaymentService.GetByStudentProgram(studentProgram).Where(x => x.Status == 1).OrderBy(x=>x.ReceivedDate).ThenBy(x=>x.Id).ToList();
                        decimal totalIncome = 0, totalConsiderationAmount = 0, totalPreviousDue = 0, totalSpDiscount = 0, totalReceived = 0, totalCashBack = 0, totalCurrentDue = 0, totalReceivable = 0;
                        foreach (var studentPayment in studentPaymentList)
                        {
                            var studentPaymentHistory = new StudentPaymentHistory();
                            studentPaymentHistory.Id = studentPayment.Id;
                            studentPaymentHistory.Date = studentPayment.CreationDate.ToString("dd/MM/yyyy");
                            if (studentPayment.PaymentType == 1)
                            {
                                studentPaymentHistory.MRN = studentPayment.ReceiptNo ?? "";
                            }
                            else
                            {
                                studentPaymentHistory.MRN = "";
                            }
                            //crn
                            if (studentPayment.PaymentType == 2)
                            {
                                studentPaymentHistory.CRN = studentPayment.ReceiptNo ?? "";
                            }
                            else
                            {
                                studentPaymentHistory.CRN = "";
                            }
                            decimal coursefees = 0, discount = 0, income = 0;
                            if (studentPayment.CourseFees != null)
                                coursefees = Convert.ToDecimal(studentPayment.CourseFees);
                          
                            if (studentPayment.DiscountAmount != null)
                                discount = Convert.ToDecimal(studentPayment.DiscountAmount);                           

                            income = Convert.ToDecimal(coursefees - discount);
                            if (income != 0)
                            {
                                studentPaymentHistory.Income = String.Format("{0:0}", income);
                                totalIncome += Convert.ToDecimal(income);
                            }
                            else
                            {
                                studentPaymentHistory.Income = "";
                            }
                            if (studentPayment.ConsiderationAmount != null)
                            {
                                studentPaymentHistory.ConsiderationAmount = String.Format("{0:0}", studentPayment.ConsiderationAmount);
                                totalConsiderationAmount += Convert.ToDecimal(studentPayment.ConsiderationAmount);
                            }
                            else
                            {
                                studentPaymentHistory.ConsiderationAmount = "";
                            }
                            decimal previousDue = 0, payableAmount = 0, consideration = 0;
                            if (studentPayment.PayableAmount != null)
                                payableAmount = Convert.ToDecimal(studentPayment.PayableAmount);
                            
                            if (studentPayment.ConsiderationAmount != null)
                            {
                                consideration = Convert.ToDecimal(studentPayment.ConsiderationAmount);
                            }
                            
                            previousDue = Convert.ToDecimal(payableAmount - income);
                            studentPaymentHistory.PreviousDue = String.Format("{0:0}", previousDue);
                            totalPreviousDue += previousDue;
                            studentPaymentHistory.Receivable = String.Format("{0:0}", payableAmount);
                            totalReceivable += payableAmount;
                            if (studentPayment.SpDiscountAmount != null)
                            {
                                if (studentPayment.SpDiscountAmount != 0)
                                {
                                    studentPaymentHistory.SpecialDiscount = String.Format("{0:0}", studentPayment.SpDiscountAmount);
                                    totalSpDiscount += Convert.ToDecimal(studentPayment.SpDiscountAmount);
                                }
                                else
                                {
                                    studentPaymentHistory.SpecialDiscount = "";
                                }
                            }
                            else
                            {
                                studentPaymentHistory.SpecialDiscount = "";
                            }
                            if (studentPayment.ReceivedAmount != null)
                            {
                                if (studentPayment.ReceivedAmount != 0)
                                {
                                    studentPaymentHistory.Received = string.Format("{0:0}", studentPayment.ReceivedAmount);
                                    totalReceived += Convert.ToDecimal(studentPayment.ReceivedAmount);
                                }
                                else
                                {
                                    studentPaymentHistory.Received = "";
                                }
                            }
                            else
                            {
                                studentPaymentHistory.Received = "";
                            }
                            if (studentPayment.CashBackAmount != null)
                            {
                                if (studentPayment.CashBackAmount != 0)
                                {
                                    studentPaymentHistory.CashBack = String.Format("{0:0}", studentPayment.CashBackAmount);
                                    totalCashBack += Convert.ToDecimal(studentPayment.CashBackAmount);
                                }
                                else
                                {
                                    studentPaymentHistory.CashBack = "";
                                }
                            }
                            else
                            {
                                studentPaymentHistory.CashBack = "";
                            }
                            //current due
                            if (studentPayment.DueAmount != null)
                            {
                                studentPaymentHistory.CurrentDue = String.Format("{0:0}", studentPayment.DueAmount);
                                totalCurrentDue += Convert.ToDecimal(studentPayment.DueAmount);
                            }
                            else
                            {
                                studentPaymentHistory.CurrentDue = "";
                            }
                            if (studentPayment.PaymentMethod != null)
                            {
                                studentPaymentHistory.PaymentMethod = _commonHelper.GetEmumIdToValue<PaymentMethod>((int)studentPayment.PaymentMethod);
                            }
                            else
                            {
                                studentPaymentHistory.PaymentMethod = "";
                            }
                            if (studentPayment.Referrer != null && studentPayment.Referrer.Name != null)
                            {
                                studentPaymentHistory.Referrer = studentPayment.Referrer.Name;
                            }
                            else
                            {
                                studentPaymentHistory.Referrer = "";
                            }
                           
                            studentPaymentHistory.ReferrerNote = !string.IsNullOrEmpty(studentPayment.SpReferenceNote) ? studentPayment.SpReferenceNote : "";
                           
                            studentPaymentHistory.Remarks = studentPayment.Remarks ?? "";
                            //user

                            if (studentPayment.CreateBy != null && studentPayment.CreateBy > 0)
                            {
                                var username = _userService.LoadAspNetUserById(studentPayment.CreateBy);
                                studentPaymentHistory.User = username.UserName;
                            }
                            else
                            {
                                studentPaymentHistory.User = "";
                            }
                            studentPaymentHistories.Add(studentPaymentHistory);
                        }
                        //transfer data from controller to view
                        ViewBag.totalIncome = String.Format("{0:0}", totalIncome);
                        ViewBag.totalConsiderationAmount = String.Format("{0:0}", totalConsiderationAmount);
                        ViewBag.totalPreviousDue = String.Format("{0:0}", totalPreviousDue);
                        ViewBag.totalReceivable = String.Format("{0:0}", totalIncome - totalConsiderationAmount);
                        ViewBag.totalSpDiscount = String.Format("{0:0}", totalSpDiscount);
                        ViewBag.totalReceived = String.Format("{0:0}", totalReceived);
                        ViewBag.totalCashBack = String.Format("{0:0}", totalCashBack);
                        ViewBag.totalCurrentDue = String.Format("{0:0}", totalIncome - totalConsiderationAmount - totalSpDiscount - totalReceived + totalCashBack);
                        if (programId == 0 && sessionId == 0)
                        {
                            ViewBag.STDBASINFO = studentBasicInformation;
                            return View(studentPaymentHistories);
                        }
                        ViewBag.STDBASINFO = studentBasicInformation;
                        return View(studentPaymentHistories);
                        //}


                    }
                    return RedirectToAction("PaymentHistory", new { message = "Program Roll not found", roll = stdProRoll });
                }
                return RedirectToAction("PaymentHistory", new { message = "Program Roll must contain 11 digits number", roll = stdProRoll });
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("PaymentHistory");

            }
        }

        [HttpGet]
        public ActionResult PaymentHistoryToExcel(string stdProRoll)
        {
            try
            {
                List<string> headerList = new List<string>();
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                columnList.Add("Date");
                columnList.Add("MRN");
                columnList.Add("CRN");
                columnList.Add("Income");
                columnList.Add("Consideration Amount");
                columnList.Add("Previous Due");
                columnList.Add("Receivable");
                columnList.Add("Special Discount");
                columnList.Add("Received");
                columnList.Add("Cash Back");
                columnList.Add("Current Due");
                columnList.Add("Payment Method");
                columnList.Add("Referrer");
                columnList.Add("Reference Note");
                columnList.Add("Remarks");
                columnList.Add("User");
                var transactionXlsList = new List<List<object>>();
                stdProRoll = stdProRoll.Trim();
                var studentProgram = _studentProgramService.GetStudentProgram(stdProRoll, true);
                // var studentPaymentHistories = new List<StudentPaymentHistory>();
                headerList.Add(ErpInfo.OrganizationNameFull);
                headerList.Add(studentProgram.Program.Name + " " + studentProgram.Batch.Session.Name);

                headerList.Add(studentProgram.Student.NickName);
                headerList.Add(studentProgram.PrnNo);
                headerList.Add("Payment History");

                var studentBasicInformation = new StudentBasicInformation();
                studentBasicInformation.DateOfAdmission = studentProgram.CreationDate.ToString("dd/MM/yyyy");
                studentBasicInformation.ProgramRoll = studentProgram.PrnNo;
                studentBasicInformation.StudentName = studentProgram.Student.NickName;
                studentBasicInformation.ContactNumber = studentProgram.Student.Mobile;
                studentBasicInformation.Branch = studentProgram.Batch.Branch.Name;
                studentBasicInformation.BatchDays = studentProgram.Batch.Days.Replace(','.ToString(), ", ");
                studentBasicInformation.BatchName = studentProgram.Batch.Name;
                var studentCList = _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);
                studentBasicInformation.StudentCourseDetails = studentCList;
                headerList.Add("Date of Admission: " + studentBasicInformation.DateOfAdmission);
                headerList.Add("Contact Number: " + studentBasicInformation.ContactNumber);
                headerList.Add("Branch: " + studentBasicInformation.Branch);
                headerList.Add("Batch Days :" + studentBasicInformation.BatchDays);
                headerList.Add("Batch Name :" + studentBasicInformation.BatchName);
                // headerList.Add("Branch: " + studentBasicInformation.Branch);
                var query = studentBasicInformation.StudentCourseDetails.GroupBy(x => x.CourseSubject.Course, (key, g) => new { Course = key, Subjects = g.ToList() });
                var courseList = query.ToList();
                if (courseList.Count > 0)
                {
                    foreach (var courseAndCorrespondingSubjectList in courseList)
                    {
                        if (courseAndCorrespondingSubjectList.Course != null)
                        {
                            var courseName = courseAndCorrespondingSubjectList.Course.Name;
                            var courseSubjectList = courseAndCorrespondingSubjectList.Subjects;
                            string courseNameValue = courseName;
                            string subjectNameByCourse = "";
                            int i = 0;

                            foreach (var courseSubject in courseSubjectList)
                            {
                                var subName = courseSubject.CourseSubject.Subject.ShortName;
                                if (i < courseSubjectList.Count - 1)
                                {
                                    subjectNameByCourse += subName + ",";
                                }
                                else
                                {
                                    subjectNameByCourse += subName;
                                }
                                i++;
                            }
                            headerList.Add(courseNameValue + ":" + subjectNameByCourse);
                        }
                    }
                }
                IList<StudentPayment> studentPaymentList =
                                _studentPaymentService.GetByStudentProgram(studentProgram)
                                    .Where(x => x.Status == 1)
                                    .ToList();
                decimal totalIncome = 0,
                    totalConsiderationAmount = 0,
                    totalPreviousDue = 0,
                    totalReceivable = 0,
                    totalSpDiscount = 0,
                    totalReceived = 0,
                    totalCashBack = 0,
                    totalCurrentDue = 0;

                foreach (var studentPayment in studentPaymentList)
                {

                    var studentPaymentHistory = new StudentPaymentHistory();
                    studentPaymentHistory.Id = studentPayment.Id;
                    studentPaymentHistory.Date = studentPayment.CreationDate.ToString("dd/MM/yyyy");
                    if (studentPayment.PaymentType == 1)
                    {
                        if (studentPayment.ReceiptNo != null)
                        {
                            studentPaymentHistory.MRN = studentPayment.ReceiptNo;
                        }
                        else
                        {
                            studentPaymentHistory.MRN = "";
                        }
                    }
                    else
                    {
                        studentPaymentHistory.MRN = "";
                    }
                    //crn
                    if (studentPayment.PaymentType == 2)
                    {
                        if (studentPayment.ReceiptNo != null)
                        {
                            studentPaymentHistory.CRN = studentPayment.ReceiptNo;
                        }
                        else
                        {
                            studentPaymentHistory.CRN = "";
                        }
                    }
                    else
                    {
                        studentPaymentHistory.CRN = "";
                    }
                    decimal coursefees = 0;
                    decimal discount = 0;
                    decimal income = 0;
                    if (studentPayment.CourseFees != null)
                    {
                        coursefees = Convert.ToDecimal(studentPayment.CourseFees);
                    }
                    else
                    {
                        coursefees = 0;
                    }
                    if (studentPayment.DiscountAmount != null)
                    {
                        discount = Convert.ToDecimal(studentPayment.DiscountAmount);
                    }
                    else
                    {
                        discount = 0;
                    }
                    income = Convert.ToDecimal(coursefees - discount);
                    if (income != 0)
                    {
                        studentPaymentHistory.Income = String.Format("{0:0}", income);
                        totalIncome += Convert.ToDecimal(income);
                    }
                    else
                    {
                        studentPaymentHistory.Income = "";
                    }
                    if (studentPayment.ConsiderationAmount != null)
                    {
                        studentPaymentHistory.ConsiderationAmount = String.Format("{0:0}",
                            studentPayment.ConsiderationAmount);
                        totalConsiderationAmount += Convert.ToDecimal(studentPayment.ConsiderationAmount);
                    }
                    else
                    {
                        studentPaymentHistory.ConsiderationAmount = "";
                    }
                    decimal previousDue = 0;
                    decimal payableAmount = 0;
                    decimal consideration = 0;
                    if (studentPayment.PayableAmount != null)
                    {
                        payableAmount = Convert.ToDecimal(studentPayment.PayableAmount);
                    }
                    else
                    {
                        payableAmount = 0;
                    }
                    if (studentPayment.ConsiderationAmount != null)
                    {
                        consideration = Convert.ToDecimal(studentPayment.ConsiderationAmount);
                    }
                    else
                    {
                        consideration = 0;
                    }
                    previousDue = Convert.ToDecimal(payableAmount - income);
                    studentPaymentHistory.PreviousDue = String.Format("{0:0}", previousDue);
                    totalPreviousDue += previousDue;
                    studentPaymentHistory.Receivable = String.Format("{0:0}", payableAmount);
                    totalReceivable += payableAmount;
                    if (studentPayment.SpDiscountAmount != null)
                    {
                        if (studentPayment.SpDiscountAmount != 0)
                        {
                            studentPaymentHistory.SpecialDiscount = String.Format("{0:0}",
                                studentPayment.SpDiscountAmount);
                            totalSpDiscount += Convert.ToDecimal(studentPayment.SpDiscountAmount);
                        }
                        else
                        {
                            studentPaymentHistory.SpecialDiscount = "";
                        }
                    }
                    else
                    {
                        studentPaymentHistory.SpecialDiscount = "";
                    }
                    if (studentPayment.ReceivedAmount != null)
                    {
                        if (studentPayment.ReceivedAmount != 0)
                        {
                            studentPaymentHistory.Received = string.Format("{0:0}",
                                studentPayment.ReceivedAmount);
                            totalReceived += Convert.ToDecimal(studentPayment.ReceivedAmount);
                        }
                        else
                        {
                            studentPaymentHistory.Received = "";
                        }
                    }
                    else
                    {
                        studentPaymentHistory.Received = "";
                    }
                    if (studentPayment.CashBackAmount != null)
                    {
                        if (studentPayment.CashBackAmount != 0)
                        {
                            studentPaymentHistory.CashBack = String.Format("{0:0}",
                                studentPayment.CashBackAmount);
                            totalCashBack += Convert.ToDecimal(studentPayment.CashBackAmount);
                        }
                        else
                        {
                            studentPaymentHistory.CashBack = "";
                        }
                    }
                    else
                    {
                        studentPaymentHistory.CashBack = "";
                    }
                    //current due
                    if (studentPayment.DueAmount != null)
                    {
                        studentPaymentHistory.CurrentDue = String.Format("{0:0}",
                            studentPayment.DueAmount);
                        totalCurrentDue += Convert.ToDecimal(studentPayment.DueAmount);
                    }
                    else
                    {
                        studentPaymentHistory.CurrentDue = "";
                    }
                    if (studentPayment.PaymentMethod != null)
                    {
                        //var paymentMethod =
                        //    _commonHelper.LoadEmumToDictionary<PaymentMethod>(new List<int> { (int)PaymentMethod.BKash, (int)PaymentMethod.Cheque })
                        //        .Where(pm => pm.Key == studentPayment.PaymentMethod)
                        //        .ToList();
                        //string pmthd = paymentMethod[0].Key.ToString();
                        studentPaymentHistory.PaymentMethod = _commonHelper.GetEmumIdToValue<PaymentMethod>((int)studentPayment.PaymentMethod);
                    }
                    else
                    {
                        studentPaymentHistory.PaymentMethod = "";
                    }
                    if (studentPayment.Referrer != null && studentPayment.Referrer.Name != null)
                    {
                        studentPaymentHistory.Referrer = studentPayment.Referrer.Name;
                    }
                    else
                    {
                        studentPaymentHistory.Referrer = "";
                    }
                    //referrer note
                    if (!string.IsNullOrEmpty(studentPayment.SpReferenceNote))
                    {
                        studentPaymentHistory.ReferrerNote = studentPayment.SpReferenceNote;
                    }
                    else
                    {
                        studentPaymentHistory.ReferrerNote = "";
                    }
                    //remarks
                    if (studentPayment.Remarks != null)
                    {
                        studentPaymentHistory.Remarks = studentPayment.Remarks;
                    }
                    else
                    {
                        studentPaymentHistory.Remarks = "";
                    }

                    //user

                    if (studentPayment.CreateBy != null && studentPayment.CreateBy > 0)
                    {
                        var username = _userService.LoadAspNetUserById(studentPayment.CreateBy);
                        studentPaymentHistory.User = username.UserName;
                    }
                    else
                    {
                        studentPaymentHistory.User = "";
                    }
                    // studentPaymentHistories.Add(studentPaymentHistory);
                    var xlsRow = new List<object> { studentPaymentHistory.Date, studentPaymentHistory.MRN, studentPaymentHistory.CRN, studentPaymentHistory.Income, studentPaymentHistory.ConsiderationAmount, studentPaymentHistory.PreviousDue, studentPaymentHistory.Receivable, studentPaymentHistory.SpecialDiscount, studentPaymentHistory.Received, studentPaymentHistory.CashBack, studentPaymentHistory.CurrentDue, studentPaymentHistory.PaymentMethod, studentPaymentHistory.Referrer, studentPaymentHistory.ReferrerNote, studentPaymentHistory.Remarks, studentPaymentHistory.User };
                    transactionXlsList.Add(xlsRow);
                }
                string totalIncomeValue = String.Format("{0:0}", totalIncome),
                       totalConsiderationAmountValue = String.Format("{0:0}", totalConsiderationAmount),
                       totalPreviousDueValue = String.Format("{0:0}", totalPreviousDue),
                       totalReceivableValue = String.Format("{0:0}", totalIncome - totalConsiderationAmount),
                       totalSpDiscountValue = String.Format("{0:0}", totalSpDiscount),
                       totalReceivedValue = String.Format("{0:0}", totalReceived),
                       totalCashBackValue = String.Format("{0:0}", totalCashBack),
                       totalCurrentDueValue = String.Format("{0:0}", totalIncome - totalConsiderationAmount - totalSpDiscount - totalReceived + totalCashBack);
                var summaryRow = new List<object> { "Total Summary", "", "", totalIncomeValue, totalConsiderationAmountValue, "", totalReceivableValue, totalSpDiscountValue, totalReceivedValue, totalCashBackValue, totalCurrentDueValue, "", "", "", "", "" };
                transactionXlsList.Add(summaryRow);
                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "payment-History-Report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }

        }

        #endregion

        #region My Transaction
        
        public ActionResult MyTransactionReport()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;         
            ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu), "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MyTransactionReport(long[] branchId, long[] campusId, string date)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;

            //TEMPORARY FIX START
             date = DateTime.Today.ToString("yyyy-MM-dd");
            //TEMPORARY FIX END           
            IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
            IList<Branch> branchByIds = new List<Branch>();
            IList<Campus> campusList = new List<Campus>();

            if (branchId.Contains(0))
            {
                branchByIds = branches;
            }
            else
            {
                branchByIds = branches.Where(br => br.Id.In(branchId)).ToList();
            }
            if (!campusId.Contains(0))
            {
                campusList = _campusService.LoadCampus(null, null, null, null, campusId.ToList());
                IList<string> campusName = campusList.Select(c => c.Name).Distinct().ToList();
                ViewBag.campusName = string.Join(", ", campusName);
            }
            else
            {
                ViewBag.campusName = "All";
            }
            if (branchId.Contains(0))
            {
                ViewBag.branchName = "All";
            }
            else
            {
                ViewBag.branchName = string.Join(", ", branchByIds.Select(x => x.Name).ToList());
            }
            ViewBag.branchId = branchId;
            ViewBag.campusId = campusId;
            ViewBag.date = date;
            ViewBag.PageSize = Constants.PageSize;
            return View("GenerateMyTransactionReport");
        }

        #endregion

        #region Transaction Receive Report

        public ActionResult TransactionReceiveReport() 
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                //organizations.Insert(0, new Organization() { ShortName = "All", Id = 0 });
                //ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");

                List<Branch> userBranchList = _branchService.LoadBranch(organizations.Select(o => o.Id).ToList(), null, null, null, null, null).ToList();
                userBranchList.Insert(0, new Branch() { Name = "All", Id = 0 });
                ViewBag.UserBranchList = new SelectList(userBranchList, "Id", "Name");

                List<Branch> receivedByUserBranchList = _branchService.LoadAuthorizedBranch(_userMenu).ToList();
                receivedByUserBranchList.Insert(0, new Branch() { Name = "All", Id = 0 });
                ViewBag.ReceivedByUserBranchList = new SelectList(receivedByUserBranchList, "Id", "Name");
                
                ViewBag.PageSize = Constants.PageSize;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public JsonResult TransactionReportReceiveResult(int draw, int start, int length, long[] userBranchIds, long[] userCampusIds, long[] UserIds, DateTime datef, DateTime datet, long[] receivedByUserBranchIds, long[] receivedByUserCampusIds, long[] receivedByUserIds)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    NameValueCollection nvc = Request.Form;
                    DateTime dateFrom = Convert.ToDateTime(datef);
                    DateTime dateTo = Convert.ToDateTime(datet);

                    var branchIdList = AuthHelper.LoadBranchIdList(_userMenu).ToArray();
                    IList<Campus> campusList = new List<Campus>();
                    campusList = _campusService.LoadAuthorizeCampus(_userMenu);
                    var campusIdList = campusList.Select(x => x.Id).ToArray();

                    //var margedBranchArray = array1.Union(array2); 
                    if (userBranchIds.Contains(SelectionType.SelelectAll))
                    {
                        userBranchIds = branchIdList;
                    }
                    if (userCampusIds.Contains(SelectionType.SelelectAll))
                    {
                        userCampusIds = campusIdList;
                    }

                    IList<UserProfile> users = _userService.LoadUser(userBranchIds, userCampusIds).Where(x => x.AspNetUser.Id != 1).OrderBy(x => x.AspNetUser.Email).ToList();
                    if (!UserIds.Contains(SelectionType.SelelectAll))
                    {
                        users = users.Where(x => UserIds.Contains(x.AspNetUser.Id)).ToList();
                    }

                    IList<UserProfile> receiveByUsers = _userService.LoadUser(receivedByUserBranchIds, receivedByUserCampusIds).Where(x => x.AspNetUser.Id != 1).OrderBy(x => x.AspNetUser.Email).ToList();
                    if (!receivedByUserIds.Contains(SelectionType.SelelectAll))
                    {
                        receiveByUsers = receiveByUsers.Where(x => receivedByUserIds.Contains(x.AspNetUser.Id)).ToList();
                    }

                    //int recordsTotal = _studentPaymentService.GetTransactionReceiveReportCount(users, UserIds, datef, datet);                    
                    int recordsTotal = _studentPaymentService.GetTransactionReceiveReportCount(users, receiveByUsers.Select(r => r.Id).ToArray(), datef, datet);
                    long recordsFiltered = recordsTotal;
                    IList<StudentPaymentReceiveByCampus> studentPaymentReceiveByCampusList = _studentPaymentService.LoadTransactionReceiveReportList(users, receiveByUsers.Select(r => r.Id).ToArray(), datef, datet, start, length);     

                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var c in studentPaymentReceiveByCampusList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        str.Add(c.ReceiveDate != null ? c.ReceiveDate.Value.ToString("dd/MM/yyyy") : "");
                        str.Add(c.ReceiveFromUser.AspNetUser.Email);
                        str.Add(c.ReceiveFromCampus.Branch.Name);
                        str.Add(c.ReceiveFromCampus.Name);
                        str.Add(c.ReceiveByUser.AspNetUser.Email);
                        str.Add(c.ReceiveByCampus.Branch.Name);
                        str.Add(c.ReceiveByCampus.Name);
                        str.Add(String.Format("{0:0}", c.ReceiveAmount));
                        str.Add(String.Format("{0:0}", c.CashBackAmount));
                        str.Add(String.Format("{0:0}", c.NetReceive));
                        str.Add(String.Format("{0:0}", c.CashReceive));
                        str.Add(String.Format("{0:0}", c.VoucherReceive));
                        str.Add(String.Format("{0:0}", c.IouReceive));
                        sl++;
                        data.Add(str);
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return Json(HttpNotFound());
        }

        public ActionResult TransactionReceiveReportToExcel(long[] userIds, long[] userBranchIds, long[] userCampusIds, DateTime datef, DateTime datet, long[] receivedByUserBranchIds, long[] receivedByUserCampusIds, long[] receivedByUserIds) 
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var branchIdList = AuthHelper.LoadBranchIdList(_userMenu).ToArray();
                IList<Campus> campusList = new List<Campus>();
                campusList = _campusService.LoadAuthorizeCampus(_userMenu);
                var campusIdList = campusList.Select(x => x.Id).ToArray();

                //var margedBranchArray = array1.Union(array2); 
                if (userBranchIds.Contains(SelectionType.SelelectAll))
                {
                    userBranchIds = branchIdList;
                }
                if (userCampusIds.Contains(SelectionType.SelelectAll))
                {
                    userCampusIds = campusIdList;
                }

                IList<UserProfile> users = _userService.LoadUser(userBranchIds, userCampusIds).Where(x => x.AspNetUser.Id != 1).OrderBy(x => x.AspNetUser.Email).ToList();
                if (!userIds.Contains(SelectionType.SelelectAll))
                {
                    users = users.Where(x => userIds.Contains(x.AspNetUser.Id)).ToList();
                }

                IList<UserProfile> receiveByUsers = _userService.LoadUser(receivedByUserBranchIds, receivedByUserCampusIds).Where(x => x.AspNetUser.Id != 1).OrderBy(x => x.AspNetUser.Email).ToList();
                if (!receivedByUserIds.Contains(SelectionType.SelelectAll))
                {
                    receiveByUsers = receiveByUsers.Where(x => receivedByUserIds.Contains(x.AspNetUser.Id)).ToList();
                }

               // DateTime dateFrom = Convert.ToDateTime(datef);
                //DateTime dateTo = Convert.ToDateTime(datet);

                List<string> headerList = new List<string>();
                headerList.Add(ErpInfo.OrganizationNameFull);
                headerList.Add("Transaction Receive Report");

                List<string> footerList = new List<string>();
                List<string> columnList = new List<string>();
                columnList.Add("Receive Date");
                columnList.Add("User");
                columnList.Add("User Branch");
                columnList.Add("User Campus");
                columnList.Add("Receive By");
                columnList.Add("Receive Branch");
                columnList.Add("Receive Campus");
                columnList.Add("Received Amount");
                columnList.Add("Cashback Amount");
                columnList.Add("Net Received Amount");
                columnList.Add("Cash Receive");
                columnList.Add("Voucher Receive");
                columnList.Add("IOU Receive");


                decimal totalReceiveAmount = 0, totalCashBackAmount = 0, totalNetReceive = 0,totalCashReceive = 0, totalVoucherReceive = 0, totalIouReceive = 0;
                var transactionXlsList = new List<List<object>>();
                IList<StudentPaymentReceiveByCampus> studentPaymentReceiveByCampusList = _studentPaymentService.LoadTransactionReceiveReportList(users, receiveByUsers.Select(r => r.Id).ToArray(), datef, datet, 0, 0);
                foreach (var c in studentPaymentReceiveByCampusList)
                {
                    decimal receiveAmount = 0;
                    decimal cashBackAmount = 0;
                    decimal netReceive = 0;
                    decimal cashReceive = 0;
                    decimal voucherReceive = 0;
                    decimal iouReceive = 0;

                    if (c.ReceiveAmount!=null)
                    {
                        receiveAmount = Convert.ToDecimal(c.ReceiveAmount);
                    }
                    if (c.CashBackAmount != null)
                    {
                        cashBackAmount = Convert.ToDecimal(c.CashBackAmount);
                    }
                    if (c.NetReceive != null)
                    {
                        netReceive = Convert.ToDecimal(c.NetReceive); 
                    }
                    if (c.CashReceive != null)
                    {
                        cashReceive = Convert.ToDecimal(c.CashReceive);
                    }
                    if (c.VoucherReceive != null)
                    {
                        voucherReceive = Convert.ToDecimal(c.VoucherReceive);
                    }
                    if (c.IouReceive != null)
                    {
                        iouReceive = Convert.ToDecimal(c.IouReceive);
                    }
                    totalReceiveAmount += receiveAmount;
                    totalCashBackAmount += cashBackAmount;
                    totalNetReceive += netReceive;
                    totalCashReceive += cashReceive;
                    totalVoucherReceive += 0;
                    totalIouReceive += voucherReceive;

                    var xlsRow = new List<object> { c.ReceiveDate != null ? c.ReceiveDate.Value.ToString("dd/MM/yyyy") : "", 
                        c.ReceiveFromUser.AspNetUser.Email,
                        c.ReceiveFromCampus.Branch.Name,
                        c.ReceiveFromCampus.Name,
                        c.ReceiveByUser.AspNetUser.Email,
                        c.ReceiveByCampus.Branch.Name,
                        c.ReceiveByCampus.Name,
                        String.Format("{0:0}", receiveAmount),
                        String.Format("{0:0}", cashBackAmount),
                        String.Format("{0:0}", netReceive),
                        String.Format("{0:0}", cashReceive),
                        String.Format("{0:0}", voucherReceive),
                        String.Format("{0:0}", iouReceive)};
                    transactionXlsList.Add(xlsRow);
                   
                   
                }
               var summaryRow = new List<object> { "Total Summary", "", "", "", "", "", "",
                   String.Format("{0:0}", totalReceiveAmount),String.Format("{0:0}", totalCashBackAmount),String.Format("{0:0}", totalNetReceive),
                    String.Format("{0:0}", totalCashReceive),String.Format("{0:0}", totalVoucherReceive),String.Format("{0:0}", totalIouReceive)
                };
                transactionXlsList.Add(summaryRow);
                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "all-Transaction-receive-list__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }
        }
        
        #endregion

        #endregion

        #region Daily Transaction Receive

        public ActionResult DailyTransactionReceive(string message="") 
        {
            try
            {
                if (!String.IsNullOrEmpty(message))
                {
                    ViewBag.SuccessMessage = message;
                }
                
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu), "Id", "Name");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
           
            return View();
        }
        
        [HttpPost]
        public JsonResult GetTransactionReceiveSummery(long[] branchId, long[] campusId, DateTime date, long userId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //  List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu) ?? _branchService.LoadBranch().Select(x => x.Id).ToList();
                // List<long> branchIdList = _branchService.LoadBranch().Select(x => x.Id).ToList();
                //   IList<Campus> campusList = new List<Campus>();
                // campusList = _campusService.LoadAuthorizeCampus(_userMenu, null, null, branchId.ToList());
                // campusList = _campusService.LoadCampus( null, null, branchId.ToList());
                //  var campusIdList = campusList.Select(x => x.Id).ToArray();

                // IList<StudentPayment> studentPaymentList = _studentPaymentService.GetMyTransactionReport(userId, branchId, branchIdList, campusId, campusIdList, date, null, null);

                IList<StudentPayment> studentNowPaymentList = _studentPaymentService.GetTransaction(userId, date, false);
                IList<StudentPayment> studentPrevPaymentList = _studentPaymentService.GetTransaction(userId, date);

                decimal
                    totalAlreadyReceived = 0,
                    totalAlreadyCashBack = 0,
                    totalNowReceived = 0,
                    totalNowCashBack = 0
                    ;
                List<StudentPayment> studentPaymentList = new List<StudentPayment>();
                studentPaymentList.AddRange(studentNowPaymentList);
                studentPaymentList.AddRange(studentPrevPaymentList);



                var branchWisePayments = studentPaymentList.GroupBy(m => m.Batch.Campus, (key, group) => new { Key = key, Group = group.ToList() });

                IList<DailyTransactionDto> dailyTransactionDtoList = new List<DailyTransactionDto>();

                foreach (var r in branchWisePayments)
                {
                    decimal
                        btotalAlreadyReceived = 0,
                        btotalAlreadyCashBack = 0,
                        btotalNowReceived = 0,
                        btotalNowCashBack = 0;

                    foreach (var c in r.Group)
                    {



                        if (c.CampusReceiveFrom != null)
                        {
                            if (c.ReceivedAmount != null && c.ReceivedAmount != 0)
                            {
                                btotalAlreadyReceived += Convert.ToDecimal(c.ReceivedAmount);

                            }
                            if (c.CashBackAmount != null && c.CashBackAmount != 0)
                            {
                                btotalAlreadyCashBack += Convert.ToDecimal(c.CashBackAmount);
                            }
                        }
                        else
                        {
                            if (c.ReceivedAmount != null && c.ReceivedAmount != 0)
                            {
                                btotalNowReceived += Convert.ToDecimal(c.ReceivedAmount);

                            }
                            if (c.CashBackAmount != null && c.CashBackAmount != 0)
                            {
                                btotalNowCashBack += Convert.ToDecimal(c.CashBackAmount);
                            }
                        }

                    }

                    totalAlreadyReceived += btotalAlreadyReceived;
                    totalAlreadyCashBack += btotalAlreadyCashBack;
                    totalNowReceived += btotalNowReceived;
                    totalNowCashBack += btotalNowCashBack;

                    DailyTransactionDto dailyTransactionDto = new DailyTransactionDto();

                    dailyTransactionDto.Branch = r.Key.Branch.Name;
                    dailyTransactionDto.Campus = r.Key.Name;
                    dailyTransactionDto.TotalAlreadyCashBack = btotalAlreadyCashBack;
                    dailyTransactionDto.TotalAlreadyReceived = btotalAlreadyReceived;
                    dailyTransactionDto.TotalNowCashBack = btotalNowCashBack;
                    dailyTransactionDto.TotalNowReceived = btotalNowReceived;
                    dailyTransactionDtoList.Add(dailyTransactionDto);
                }

                dailyTransactionDtoList = dailyTransactionDtoList.Where(x => x.TotalAlreadyReceived > 0 || x.TotalAlreadyCashBack > 0 || x.TotalNowReceived > 0 || x.TotalNowCashBack > 0).OrderBy(x => x.Branch).ToList();
                return Json(new
                {
                    totalAlreadyReceived = totalAlreadyReceived,
                    totalAlreadyCashBack = totalAlreadyCashBack,
                    totalNowReceived = totalNowReceived,
                    totalNowCashBack = totalNowCashBack,
                    dailyTransactionDtoList = dailyTransactionDtoList,
                    IsSuccess = true
                });

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public JsonResult SubmitTransactionReceive(long[] branchId, long[] campusId, DateTime date, long userCampusId, long userId, long rcampusId, decimal cashReceive, decimal bKashReceive, decimal voucherReceive, decimal iouReceive)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var currentUserId = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());

                IList<long> paymentIdList = new List<long>();
                IList<StudentPayment> studentPaymentList = _studentPaymentService.GetTransaction(userId, date, false);
                decimal totalAlreadyReceived = 0, totalAlreadyCashBack = 0, totalNowReceived = 0, totalNowCashBack = 0;

                foreach (var c in studentPaymentList)
                {
                    if (c.ReceivedAmount != null && c.ReceivedAmount != 0)
                    {
                        totalNowReceived += Convert.ToDecimal(c.ReceivedAmount);
                        paymentIdList.Add(c.Id);
                    }
                    if (c.CashBackAmount != null && c.CashBackAmount != 0)
                    {
                        totalNowCashBack += Convert.ToDecimal(c.CashBackAmount);
                        paymentIdList.Add(c.Id);
                    }
                }
                //var words = LongToWords(7348275438);

                var totalTemp = (cashReceive + bKashReceive + voucherReceive + iouReceive);

                if (totalTemp != (totalNowReceived - totalNowCashBack))
                {
                    return Json(new Response(false, "(CashReceive+VoucherReceive+IouReceive) must equal to total receivable anount."));
                }

                if (totalNowReceived >= 0 || totalNowCashBack >= 0)
                {
                    _studentPaymentService.Update(paymentIdList, userCampusId, userId, currentUserId, rcampusId, date, totalNowReceived, totalNowCashBack, cashReceive, bKashReceive, voucherReceive, iouReceive);
                    return Json(new Response(true, "Success"));
                }

                return Json(new Response(false, "The receivable amount is zero!! "));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }

        }
      
        #endregion

        #region Ajax Request Function

        [HttpPost]
        public JsonResult LoadProgram(long organizatonId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<SelectListItem> programList = new SelectList(_programService.LoadAuthorizedProgram(userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(organizatonId))), "Id", "Name").ToList();
                return Json(new { returnList = programList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        
        //load user by campus
        [HttpPost]
        public JsonResult LoadUser(long[] branchIds,long[] campusIds, bool isAutho = false)
        {
            try
            {
                if (isAutho)
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    branchIds = AuthHelper.LoadBranchIdList(_userMenu, null, null, (branchIds.ToList().Contains(SelectionType.SelelectAll)) ? null : branchIds.ToList()).ToArray();
                }
                IList<UserProfile> users = _userService.LoadUser(branchIds,campusIds).Where(x => x.AspNetUser.Id != 1).OrderBy(x=>x.AspNetUser.Email).ToList();
                var userList = new SelectList(users, "AspNetUser.Id", "AspNetUser.UserName");
                List<SelectListItem> userListAll = userList.ToList();
                userListAll.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
                ViewBag.UserIds = new SelectList(userListAll, "Value", "Text");
                IList<SelectListItem> userLIst = new SelectList(userListAll, "Value", "Text").ToList();
                return Json(new { returnedUserList = userLIst, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public JsonResult LoadUserForTransactionReport(long[] branchIds, long[] campusIds)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<UserProfile> users = _userService.LoadAuthorizedUser(userMenu,null,null,null,branchIds.ToList(),campusIds.ToList()).Where(x => x.AspNetUser.Id != 1).OrderBy(x => x.AspNetUser.Email).ToList();
                var userList = new SelectList(users, "AspNetUser.Id", "AspNetUser.UserName");
                List<SelectListItem> userListAll = userList.ToList();
                if (users.Count>0)
                {
                    userListAll.Insert(0, new SelectListItem() { Value = "0", Text = "All" }); 
                }
                ViewBag.UserIds = new SelectList(userListAll, "Value", "Text");
                IList<SelectListItem> userLIst = new SelectList(userListAll, "Value", "Text").ToList();
                return Json(new { returnedUserList = userLIst, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        
        public JsonResult GetSession(long programId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var sessionList = _sessionService.LoadAuthorizedSession(_userMenu, null, _commonHelper.ConvertIdToList(programId));
                var sessionSelectList = new SelectList(sessionList.ToList(), "Id", "Name");
                ViewBag.SessionId = sessionSelectList;

                return Json(new { sessionList = sessionSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public ActionResult AjaxRequestForBranch(long programId, long sessionId)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                List<SelectListItem> branchList = (new SelectList(_branchService.LoadAuthorizedBranch(userMenu, null, _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId)).ToList(), "Id", "Name")).ToList();
                if (branchList.Any())
                    branchList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Branch" });
                return Json(new { branchList = branchList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult AjaxRequestForCampus(long programId, long sessionId, long[] branchId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var campusList = _campusService.LoadAuthorizeCampus(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId));
                List<SelectListItem> campusSelecteList = (new SelectList(campusList.ToList(), "Id", "Name")).ToList();
                if (campusList.Any())
                    campusSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Campus" });

                return Json(new { campusList = campusSelecteList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public ActionResult AjaxRequestForBatchDay(long programId, long sessionId, long[] branchId, long[] campusId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var batchDayList = _batchService.LoadAuthorizeBatchGroupByDays(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList());

                List<SelectListItem> batchSelecteList = (new SelectList(batchDayList)).ToList();

                if (batchSelecteList.Any())
                    batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Day" });

                return Json(new { batchDays = batchSelecteList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public ActionResult AjaxRequestForBatchTime(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var batchTimeList = _batchService.LoadAuthorizeBatchGroupByTimes(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays);
                SelectList batchTimeSelectList = new SelectList(batchTimeList);
                List<SelectListItem> batchTimes = batchTimeSelectList.ToList();
                if (batchTimes.Any())
                    batchTimes.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Time" });
                return Json(new { batchTime = batchTimes, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public ActionResult AjaxRequestForBatch(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, string[] batchTimeText)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var batchList = _batchService.LoadAuthorizeBatch(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime);
                List<SelectListItem> batchSelecteList = (new SelectList(batchList.ToList(), "Id", "Name")).ToList();
                if (batchSelecteList.Any())
                    batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch" });
                return Json(new { batchName = batchSelecteList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public JsonResult GetProgram(long sessionId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var authPrograms = _programService.LoadAuthorizedProgram(_userMenu, null, _commonHelper.ConvertIdToList(sessionId));
                var programSelectList = new SelectList(authPrograms.ToList(), "Id", "Name");
                ViewBag.ProgramId = programSelectList;
                return Json(new { programList = programSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        public JsonResult GetCourseList(long programId, long sessionId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Course> courseListDB = _courseService.LoadCourse(programId, sessionId);
                if (courseListDB != null && courseListDB.Count > 0)
                {
                    Course obj = new Course();
                    obj.Id = SelectionType.SelelectAll;
                    obj.Name = "ALL COURSE";
                    courseListDB.Insert(0, obj);
                }
                var courseFullList = new SelectList(courseListDB.OrderBy(x => x.Rank).ToList(), "Id", "Name");
                ViewBag.ProgramId = courseFullList;
                return Json(new { courseList = courseFullList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        public JsonResult GetBranch(long[] programId, long[] sessionId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<SelectListItem> branchList = new List<SelectListItem>();
                if (programId[0] == 0)
                {
                    branchList = (new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, null, null, _commonHelper.ConvertIdToList(sessionId[0])), "Id", "Name")).ToList();
                }
                else
                {
                    branchList = (new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, null, _commonHelper.ConvertIdToList(programId[0]), _commonHelper.ConvertIdToList(sessionId[0])), "Id", "Name")).ToList();
                }

                if (branchList.Any())
                    branchList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Branch" });
                ViewBag.ProgramId = branchList;
                return Json(new { branchList = branchList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public JsonResult GetCampus(long[] programId, long[] sessionId, long[] branchId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Campus> campusList = new List<Campus>();
                if (programId[0] == 0)
                    campusList = _campusService.LoadAuthorizeCampus(_userMenu, null, null, branchId.ToList(), _commonHelper.ConvertIdToList(sessionId[0]));
                else
                    campusList = _campusService.LoadAuthorizeCampus(_userMenu, null, _commonHelper.ConvertIdToList(programId[0]), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId[0]));


                List<SelectListItem> campusSelecteList = (new SelectList(campusList.ToList(), "Id", "Name")).ToList();
                if (campusList.Any())
                    campusSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All" });
                ViewBag.SelectedCampus = campusSelecteList;

                return Json(new { campusList = campusSelecteList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public JsonResult GetCampusByBranch(long[] branchId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                var campusList = _campusService.LoadAuthorizeCampus(_userMenu, null, null, branchId.ToList());
                List<SelectListItem> campusSelecteList = (new SelectList(campusList.ToList(), "Id", "Name")).ToList();
                if (campusList.Any())
                    campusSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Campus" });
                ViewBag.SelectedCampus = campusSelecteList;

                return Json(new { campusList = campusSelecteList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Generate Money Receipt PDF

        private float ConvertPixelTopoint(double pixel)
        {
            float points = (float)pixel * 75 / 100;
            return points;
        }

        private float ConvertInchTopoint(double inch)
        {
            float point = (float)inch * 72;
            return point;
        }

        private void AssignMoneyReceiptDetails(PdfPCell keyCell, PdfPCell cloneCell, PdfPCell valueCell)
        {
            keyCell.Border = 0;
            keyCell.PaddingTop = 0;
            keyCell.PaddingBottom = 0;
            keyCell.HorizontalAlignment = Element.ALIGN_TOP;
            keyCell.VerticalAlignment = Element.ALIGN_LEFT;

            cloneCell.Border = 0;
            cloneCell.PaddingTop = 0;
            cloneCell.PaddingBottom = 0;
            cloneCell.HorizontalAlignment = Element.ALIGN_TOP;
            cloneCell.VerticalAlignment = Element.ALIGN_LEFT;

            valueCell.Border = 0;
            valueCell.PaddingTop = 0;
            valueCell.PaddingBottom = 0;
            valueCell.HorizontalAlignment = Element.ALIGN_TOP;
            valueCell.VerticalAlignment = Element.ALIGN_LEFT;
        }

        private void AssignMoneyReceiptDetailsToTable(PdfPTable middleTable, PdfPCell keyCell, PdfPCell cloneCell, PdfPCell valueCell)
        {
            middleTable.AddCell(keyCell);
            middleTable.AddCell(cloneCell);
            middleTable.AddCell(valueCell);
        }

        public ActionResult SavePdfMoneyReciept(long id, string comingFromAddCourse = "")
        {
            double dpiX, dpiY;
            try
            {
                var docName = "";


                #region Get MoneyReceipt Original Code
                var studentPayment = _studentPaymentService.LoadById(id);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                List<long> programIdList = AuthHelper.LoadProgramIdList(_userMenu);

                var errorMessage = "";
                if (studentPayment.StudentProgram != null)
                {
                    var userBranchId = studentPayment.StudentProgram.Batch.Branch.Id;
                    var userProgramId = studentPayment.StudentProgram.Program.Id;
                    if (branchIdList != null)
                    {
                        //bool isBranchSelect = Array.Exists(branchIdList, item => item == userBranchId);
                        bool isBranchSelect = branchIdList.Contains(userBranchId);
                        if (!isBranchSelect)
                            errorMessage = "You are not authorized to this branch";
                        ViewBag.NotAuthorize = errorMessage;
                    }
                    if (programIdList != null)
                    {
                        //bool isProgramSelect = Array.Exists(programIdList, item => item == userProgramId);
                        bool isProgramSelect = programIdList.Contains(userProgramId);
                        if (!isProgramSelect)
                            errorMessage = "You are not authorized to this program";
                        ViewBag.NotAuthorize = errorMessage;
                    }
                }
              
                MoneyReceipt receipt = GetMoneyReceipt(studentPayment, comingFromAddCourse);
                #endregion


                #region Start PDF Generation Code
                using (Graphics graphics = Graphics.FromHwnd(IntPtr.Zero))
                {
                    dpiX = graphics.DpiX;
                    dpiY = graphics.DpiY;
                }
                double pixelsWidth = 720;//dpiY * 7.5;
                double pixelsHeight = 460;//dpiX * 4.69;

                var width = ConvertPixelTopoint(pixelsWidth);
                var height = ConvertPixelTopoint(pixelsHeight);//72*5;
                var width99 = ConvertPixelTopoint(pixelsWidth * 99 / 100);
                var height99 = ConvertPixelTopoint(pixelsHeight * 99 / 100);

                // var rect = new Rectangle(width, height);

                var doc = new Document(PageSize.A4);
                doc.SetMargins(ConvertInchTopoint(.25), ConvertInchTopoint(.25), ConvertInchTopoint(.25), ConvertInchTopoint(.25));
                using (var memoryStream = new MemoryStream())
                {
                    var writer = PdfWriter.GetInstance(doc, memoryStream);
                    writer.AddViewerPreference(PdfName.PRINTSCALING, PdfName.NONE);
                    doc.Open();
                    docName = receipt.PrRollNo;
                    docName += "_" + receipt.StudentPayment.ReceiptNo;

                    var companyImagePath = Server.MapPath(Url.Content("~") + studentPayment.StudentProgram.Program.Organization.ReportImageUrl);
                    iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(companyImagePath);
                    logo.ScaleToFit(ConvertPixelTopoint(352), ConvertPixelTopoint(28));
                    logo.Alignment = Element.ALIGN_CENTER;

                    FontFactory.RegisterDirectory(Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts");
                    var udvashName = new Paragraph();
                    udvashName.Add(logo);
                    udvashName.Alignment = Element.ALIGN_CENTER;
                    udvashName.SpacingAfter = 0;
                    //Organization name end
                    var course = new Paragraph(receipt.Program + " " + receipt.Session, FontFactory.GetFont("Verdana-bold", ConvertPixelTopoint(20)));
                    course.Alignment = Element.ALIGN_CENTER;
                    course.SpacingBefore = 0;

                    PdfPTable MainTable = new PdfPTable(1);
                    MainTable.SpacingBefore = 0;
                    MainTable.SpacingAfter = 0;
                    MainTable.WidthPercentage = 100;
                    MainTable.DefaultCell.PaddingLeft = ConvertPixelTopoint(2);
                    MainTable.DefaultCell.PaddingRight = ConvertPixelTopoint(2);


                    #region Top Cell Organization Name, Program & reg, money & date
                    PdfPCell cellTop = new PdfPCell();
                    cellTop.Colspan = 1;
                    cellTop.FixedHeight = (height99 * 21) / 100;
                    cellTop.HorizontalAlignment = Element.ALIGN_CENTER; //0=Left, 1=Centre, 2=Right
                    cellTop.VerticalAlignment = Element.ALIGN_TOP;
                    cellTop.AddElement(logo);
                    cellTop.AddElement(course);


                    #region Inner Top Section Table for Registration, Money Receipt & Date
                    PdfPTable topTable = new PdfPTable(3);
                    topTable.WidthPercentage = 100;
                    float[] widths = new float[] { (width99 * 25) / 100, (width99 * 50) / 100, (width99 * 25) / 100 };
                    topTable.SetWidths(widths);

                    PdfPCell regCell = new PdfPCell();
                    regCell.Border = 0;
                    regCell.PaddingTop = 0;
                    regCell.PaddingBottom = 0;
                    regCell.Colspan = 1;
                    regCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    regCell.VerticalAlignment = Element.ALIGN_TOP;
                    var reg = new Paragraph("Reg. No.: " + receipt.RegNo, FontFactory.GetFont("Verdana", ConvertPixelTopoint(14)));
                    reg.Alignment = Element.ALIGN_CENTER;
                    regCell.AddElement(reg);
                    topTable.AddCell(regCell);

                    PdfPCell moneyCell = new PdfPCell();
                    moneyCell.Border = 0;
                    moneyCell.PaddingTop = 0;
                    moneyCell.PaddingBottom = 0;
                    regCell.Colspan = 1;
                    moneyCell.HorizontalAlignment = Element.ALIGN_TOP;
                    moneyCell.VerticalAlignment = Element.ALIGN_CENTER;

                    var moneyReceiptText = "";
                    if (receipt.StudentPayment.PaymentType == PaymentType.CancellationReceipt)
                    {
                        moneyReceiptText = "Cancellation Receipt(";
                        moneyReceiptText += (receipt.MoneyReceiptCount + 1) + ") #" + receipt.StudentPayment.ReceiptNo;
                        docName += "_" + (receipt.MoneyReceiptCount + 1);
                    }
                    else if (receipt.StudentPayment.PaymentType == PaymentType.MoneyReceipt)
                    {
                        moneyReceiptText = "Money Receipt(";
                        moneyReceiptText += (receipt.MoneyReceiptCount + 1) + ") #" + receipt.StudentPayment.ReceiptNo;
                        docName += "_" + (receipt.MoneyReceiptCount + 1);
                    }
                    else
                        moneyReceiptText += "N/A";

                    var recip = new Paragraph(moneyReceiptText, FontFactory.GetFont("Verdana-bold", ConvertPixelTopoint(14)));
                    recip.Alignment = Element.ALIGN_CENTER;
                    moneyCell.AddElement(recip);
                    topTable.AddCell(moneyCell);

                    PdfPCell dateCell = new PdfPCell();
                    dateCell.Border = 0;
                    dateCell.PaddingTop = 0;
                    dateCell.PaddingBottom = 0;
                    regCell.Colspan = 1;
                    dateCell.HorizontalAlignment = Element.ALIGN_TOP;
                    dateCell.VerticalAlignment = Element.ALIGN_CENTER;
                    var date = new Paragraph("Date: " + receipt.StudentPayment.CreationDate.ToString("dd-MM-yyyy"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(14)));
                    date.Alignment = Element.ALIGN_CENTER;
                    dateCell.AddElement(date);
                    topTable.AddCell(dateCell);

                    cellTop.AddElement(topTable);
                    #endregion Inner Top Section Table for Registration, Money Receipt & Date

                    MainTable.AddCell(cellTop);
                    #endregion Top Cell Organization Name, Program & reg, money & date


                    #region Middle Cell
                    PdfPCell cellMiddle = new PdfPCell();
                    cellMiddle.Colspan = 1;
                    cellMiddle.FixedHeight = (height99 * 70) / 100;
                    cellMiddle.PaddingRight = (width99 * 2 / 100);
                    cellMiddle.PaddingTop = (width99 * 1 / 100);
                    cellMiddle.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    cellMiddle.VerticalAlignment = Element.ALIGN_TOP;


                    #endregion



                    #region Table For Money Receipt Detail

                    var receiptDetailTable = new PdfPTable(2);
                    receiptDetailTable.WidthPercentage = 100;
                    widths = new float[] { (width99 * (float)18.7) / 100, (width99 * (float)81.3) / 100 };
                    receiptDetailTable.SetWidths(widths);
                    receiptDetailTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    receiptDetailTable.SpacingAfter = 0;
                    receiptDetailTable.SpacingBefore = 0;

                    var receiptDetailCell1 = new PdfPCell();
                    receiptDetailCell1.FixedHeight = (height99 * 71 * 88) / 10000;
                    receiptDetailCell1.Border = 0;
                    receiptDetailCell1.Colspan = 1;
                    receiptDetailTable.AddCell(receiptDetailCell1);

                    var receiptDetailCell2 = new PdfPCell();
                    receiptDetailCell2.FixedHeight = (height99 * 71 * 88) / 10000;
                    receiptDetailCell2.Border = 0;
                    receiptDetailCell2.Colspan = 1;


                    PdfPTable MiddleTable = new PdfPTable(3);
                    widths = new float[] { (width99 * 28) / 100, (width99 * 3) / 100, (width99 * 69) / 100 };
                    MiddleTable.SetWidths(widths);
                    MiddleTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    MiddleTable.SpacingAfter = 0;
                    MiddleTable.SpacingBefore = 0;
                    #region Student Name
                    PdfPCell keyCell1 = new PdfPCell();
                    PdfPCell cloneCell1 = new PdfPCell();
                    PdfPCell valueCell1 = new PdfPCell();
                    keyCell1.AddElement(new Chunk("Student Name", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    cloneCell1.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    valueCell1.AddElement(new Chunk(receipt.StudentName, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    AssignMoneyReceiptDetails(keyCell1, cloneCell1, valueCell1);
                    AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell1, cloneCell1, valueCell1);
                    #endregion Student Name

                    #region Program Roll
                    PdfPCell keyCell2 = new PdfPCell();
                    PdfPCell cloneCell2 = new PdfPCell();
                    PdfPCell valueCell2 = new PdfPCell();
                    keyCell2.AddElement(new Chunk("Program Roll", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    cloneCell2.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    valueCell2.AddElement(new Chunk(receipt.PrRollNo, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    //docName += "_" + receipt.PrRollNo;
                    AssignMoneyReceiptDetails(keyCell2, cloneCell2, valueCell2);
                    AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell2, cloneCell2, valueCell2);
                    #endregion Program Roll

                    #region Date of Admission
                    PdfPCell keyCell3 = new PdfPCell();
                    PdfPCell cloneCell3 = new PdfPCell();
                    PdfPCell valueCell3 = new PdfPCell();
                    keyCell3.AddElement(new Chunk("Date of Admission", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    cloneCell3.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    valueCell3.AddElement(new Chunk(receipt.DateOFSubmission.ToString("dd-MM-yyyy"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    AssignMoneyReceiptDetails(keyCell3, cloneCell3, valueCell3);
                    AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell3, cloneCell3, valueCell3);
                    #endregion Date of Admission

                    #region Branch
                    PdfPCell keyCell4 = new PdfPCell();
                    PdfPCell cloneCell4 = new PdfPCell();
                    PdfPCell valueCell4 = new PdfPCell();
                    keyCell4.AddElement(new Chunk("Branch", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    cloneCell4.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    valueCell4.AddElement(new Chunk(receipt.Branch, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    AssignMoneyReceiptDetails(keyCell4, cloneCell4, valueCell4);
                    AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell4, cloneCell4, valueCell4);
                    #endregion Branch

                    #region Batch Days
                    PdfPCell keyCell5 = new PdfPCell();
                    PdfPCell cloneCell5 = new PdfPCell();
                    PdfPCell valueCell5 = new PdfPCell();
                    keyCell5.AddElement(new Chunk("Batch Days", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    cloneCell5.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    valueCell5.AddElement(new Chunk(receipt.BatchDays, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    AssignMoneyReceiptDetails(keyCell5, cloneCell5, valueCell5);
                    AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell5, cloneCell5, valueCell5);
                    #endregion Batch Days

                    #region Batch Time
                    PdfPCell keyCell6 = new PdfPCell();
                    PdfPCell cloneCell6 = new PdfPCell();
                    PdfPCell valueCell6 = new PdfPCell();
                    keyCell6.AddElement(new Chunk("Batch Time", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    cloneCell6.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    valueCell6.AddElement(new Chunk(receipt.BatchTime, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    AssignMoneyReceiptDetails(keyCell6, cloneCell6, valueCell6);
                    AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell6, cloneCell6, valueCell6);
                    #endregion Batch Time

                    #region Batch Name
                    PdfPCell keyCell7 = new PdfPCell();
                    PdfPCell cloneCell7 = new PdfPCell();
                    PdfPCell valueCell7 = new PdfPCell();
                    keyCell7.AddElement(new Chunk("Batch Name", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    cloneCell7.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    valueCell7.AddElement(new Chunk(receipt.BatchName, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    AssignMoneyReceiptDetails(keyCell7, cloneCell7, valueCell7);
                    AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell7, cloneCell7, valueCell7);
                    #endregion Batch Name

                    #region courseName
                    PdfPCell keyCell8 = new PdfPCell();
                    PdfPCell cloneCell8 = new PdfPCell();
                    PdfPCell valueCell8 = new PdfPCell();
                    var query = receipt.StudentPayment.CourseSubjectList.GroupBy(x => x.Course, (key, g) => new { Course = key, Subjects = g.ToList() }).OrderBy(x => x.Course.Rank);
                    var courseList = query.ToList();
                    foreach (var courseAndCorrespondingSubjectList in courseList)
                    {
                        var courseName = courseAndCorrespondingSubjectList.Course.Name;
                        var courseSubjectList = courseAndCorrespondingSubjectList.Subjects;

                        keyCell8.AddElement(new Chunk(courseName, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        cloneCell8.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));

                        var subjectListsString = "";
                        int i = 1;
                        foreach (var courseSubject in courseSubjectList)
                        {
                            var subName = courseSubject.Subject.ShortName;
                            if (courseSubjectList.Count > i)
                            {
                                subjectListsString += subName + ", ";
                            }
                            else
                            {
                                subjectListsString += subName;
                            }
                            i++;
                        }
                        if (receipt.StudentPayment.PaymentType == PaymentType.CancellationReceipt)
                        {
                            subjectListsString += " (Cancelled)";
                        }

                        valueCell8.AddElement(new Chunk(subjectListsString, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                    }
                    AssignMoneyReceiptDetails(keyCell8, cloneCell8, valueCell8);
                    AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell8, cloneCell8, valueCell8);
                    #endregion courseName

                    if (receipt.StudentPayment.PaymentType == 2)
                    {
                        #region Payable Amount

                        PdfPCell keyCell9 = new PdfPCell();
                        PdfPCell cloneCell9 = new PdfPCell();
                        PdfPCell valueCell9 = new PdfPCell();
                        keyCell9.AddElement(new Chunk("Payable Amount", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        cloneCell9.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        if (receipt.StudentPayment.PayableAmount != null && receipt.StudentPayment.PayableAmount > 0)
                        {
                            valueCell9.AddElement(new Chunk(Convert.ToDecimal(receipt.StudentPayment.PayableAmount).ToString("n2"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        else
                        {
                            valueCell9.AddElement(new Chunk("0.00", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        AssignMoneyReceiptDetails(keyCell9, cloneCell9, valueCell9);
                        AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell9, cloneCell9, valueCell9);

                        #endregion Payable Amount

                        #region Consideration Amount

                        PdfPCell keyCell10 = new PdfPCell();
                        PdfPCell cloneCell10 = new PdfPCell();
                        PdfPCell valueCell10 = new PdfPCell();
                        keyCell10.AddElement(new Chunk("Consideration Amount", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        cloneCell10.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        if (receipt.StudentPayment.ConsiderationAmount != null && receipt.StudentPayment.ConsiderationAmount > 0)
                        {
                            valueCell10.AddElement(new Chunk(Convert.ToDecimal(receipt.StudentPayment.ConsiderationAmount).ToString("n2"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        else
                        {
                            valueCell10.AddElement(new Chunk("0.00", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        AssignMoneyReceiptDetails(keyCell10, cloneCell10, valueCell10);
                        AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell10, cloneCell10, valueCell10);
                        #endregion Consideration Amount

                        if (receipt.StudentPayment.CashBackAmount != null && receipt.StudentPayment.CashBackAmount > 0)
                        {
                            #region Cash Back Amount
                            PdfPCell keyCell11 = new PdfPCell();
                            PdfPCell cloneCell11 = new PdfPCell();
                            PdfPCell valueCell11 = new PdfPCell();
                            keyCell11.AddElement(new Chunk("Cash Back Amount", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            cloneCell11.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            valueCell11.AddElement(new Chunk(Convert.ToDecimal(receipt.StudentPayment.CashBackAmount).ToString("n2"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            AssignMoneyReceiptDetails(keyCell11, cloneCell11, valueCell11);
                            AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell11, cloneCell11, valueCell11);
                            #endregion Cash Back Amount
                        }

                        #region Due Amount

                        PdfPCell keyCell12 = new PdfPCell();
                        PdfPCell cloneCell12 = new PdfPCell();
                        PdfPCell valueCell12 = new PdfPCell();
                        keyCell12.AddElement(new Chunk("Due Amount", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        cloneCell12.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        if (receipt.StudentPayment.DueAmount != null && receipt.StudentPayment.DueAmount > 0)
                        {
                            valueCell12.AddElement(new Chunk(Convert.ToDecimal(receipt.StudentPayment.DueAmount).ToString("n2"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        else
                        {
                            valueCell12.AddElement(new Chunk("0.00", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        AssignMoneyReceiptDetails(keyCell12, cloneCell12, valueCell12);
                        AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell12, cloneCell12, valueCell12);
                        #endregion Due Amount

                        //if (Model.StudentPayment.NextReceivedDate.ToString().Length > 0 && Convert.ToInt64(Model.NextPaymentDate.ToString("yyyyMMdd")) > 20000101 && Convert.ToInt64(Model.NextPaymentDate.ToString("yyyyMMdd")) < 30000101)
                        if (receipt.StudentPayment.DueAmount != null && receipt.StudentPayment.DueAmount > 0)
                        {
                            #region Next Payment Date
                            PdfPCell keyCell13 = new PdfPCell();
                            PdfPCell cloneCell13 = new PdfPCell();
                            PdfPCell valueCell13 = new PdfPCell();
                            keyCell13.AddElement(new Chunk("Next Payment Date", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            cloneCell13.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            valueCell13.AddElement(new Chunk(receipt.NextPaymentDate.ToString("dd-MM-yyyy"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            AssignMoneyReceiptDetails(keyCell13, cloneCell13, valueCell13);
                            AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell13, cloneCell13, valueCell13);
                            #endregion Next Payment Date
                        }
                    }
                    else
                    {
                        #region Payable Amount
                        PdfPCell keyCell14 = new PdfPCell();
                        PdfPCell cloneCell14 = new PdfPCell();
                        PdfPCell valueCell14 = new PdfPCell();
                        keyCell14.AddElement(new Chunk("Payable Amount", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        cloneCell14.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        if (receipt.StudentPayment.PayableAmount != null && receipt.StudentPayment.PayableAmount > 0)
                        {
                            valueCell14.AddElement(new Chunk(Convert.ToDecimal(receipt.StudentPayment.PayableAmount).ToString("n2"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        else
                        {
                            valueCell14.AddElement(new Chunk("0.00", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        AssignMoneyReceiptDetails(keyCell14, cloneCell14, valueCell14);
                        AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell14, cloneCell14, valueCell14);

                        #endregion Payable Amount

                        if (receipt.StudentPayment.SpDiscountAmount != null && receipt.StudentPayment.SpDiscountAmount > 0)
                        {
                            #region Special Discount
                            PdfPCell keyCell15 = new PdfPCell();
                            PdfPCell cloneCell15 = new PdfPCell();
                            PdfPCell valueCell15 = new PdfPCell();
                            keyCell15.AddElement(new Chunk("Special Discount", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            cloneCell15.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            valueCell15.AddElement(new Chunk(Convert.ToDecimal(receipt.StudentPayment.SpDiscountAmount).ToString("n2"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            AssignMoneyReceiptDetails(keyCell15, cloneCell15, valueCell15);
                            AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell15, cloneCell15, valueCell15);
                            #endregion Special Discount
                        }
                        #region Paid Amount
                        PdfPCell keyCell16 = new PdfPCell();
                        PdfPCell cloneCell16 = new PdfPCell();
                        PdfPCell valueCell16 = new PdfPCell();
                        keyCell16.AddElement(new Chunk("Paid Amount", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        cloneCell16.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        if (receipt.StudentPayment.ReceivedAmount != null && receipt.StudentPayment.ReceivedAmount > 0)
                        {
                            valueCell16.AddElement(new Chunk(Convert.ToDecimal(receipt.StudentPayment.ReceivedAmount).ToString("n2"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        else
                        {
                            valueCell16.AddElement(new Chunk("0.00", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        AssignMoneyReceiptDetails(keyCell16, cloneCell16, valueCell16);
                        AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell16, cloneCell16, valueCell16);
                        #endregion Paid Amount
                        #region Due Amount
                        PdfPCell keyCell17 = new PdfPCell();
                        PdfPCell cloneCell17 = new PdfPCell();
                        PdfPCell valueCell17 = new PdfPCell();
                        keyCell17.AddElement(new Chunk("Due Amount", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        cloneCell17.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        if (receipt.StudentPayment.DueAmount != null && receipt.StudentPayment.DueAmount > 0)
                        {
                            valueCell17.AddElement(new Chunk(Convert.ToDecimal(receipt.StudentPayment.DueAmount).ToString("n2"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        else
                        {
                            valueCell17.AddElement(new Chunk("0.00", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                        }
                        AssignMoneyReceiptDetails(keyCell17, cloneCell17, valueCell17);
                        AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell17, cloneCell17, valueCell17);
                        #endregion Due Amount
                        //if (Model.NextPaymentDate.ToString().Length > 0 && Convert.ToInt64(Model.NextPaymentDate.ToString("yyyyMMdd")) > 20000101 && Convert.ToInt64(Model.NextPaymentDate.ToString("yyyyMMdd")) < 30000101)
                        if (receipt.StudentPayment.DueAmount != null && receipt.StudentPayment.DueAmount > 0)
                        {
                            #region Next Payment Date
                            PdfPCell keyCell18 = new PdfPCell();
                            PdfPCell cloneCell18 = new PdfPCell();
                            PdfPCell valueCell18 = new PdfPCell();
                            keyCell18.AddElement(new Chunk("Next Payment Date", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            cloneCell18.AddElement(new Chunk(":", FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            valueCell18.AddElement(new Chunk(receipt.NextPaymentDate.ToString("dd-MM-yyyy"), FontFactory.GetFont("Verdana", ConvertPixelTopoint(11))));
                            AssignMoneyReceiptDetails(keyCell18, cloneCell18, valueCell18);
                            AssignMoneyReceiptDetailsToTable(MiddleTable, keyCell18, cloneCell18, valueCell18);
                            #endregion Next Payment Date
                        }
                    }

                    ////finally add to table
                    //MiddleTable.AddCell(keyCell);
                    //MiddleTable.AddCell(cloneCell);
                    //MiddleTable.AddCell(valueCell);
                    receiptDetailCell2.AddElement(MiddleTable);
                    receiptDetailTable.AddCell(receiptDetailCell2);

                    var receiptDetailCell3 = new PdfPCell();
                    receiptDetailCell3.FixedHeight = (height99 * 71 * 12) / 10000;
                    receiptDetailCell3.Border = 0;
                    receiptDetailCell3.Colspan = 2;
                    receiptDetailCell3.PaddingLeft = ConvertInchTopoint(5.5);
                    receiptDetailCell3.PaddingRight = ConvertPixelTopoint(5);
                    receiptDetailCell3.HorizontalAlignment = Element.ALIGN_TOP;
                    var autorizePersion = new Chunk(receipt.UserFullName, FontFactory.GetFont("Verdana-bold", ConvertPixelTopoint(12)));
                    autorizePersion.SetUnderline(ConvertPixelTopoint(1), ConvertPixelTopoint(15));
                    var name = new Paragraph(autorizePersion);
                    name.Alignment = Element.ALIGN_CENTER;
                    receiptDetailCell3.AddElement(name);
                    receiptDetailTable.AddCell(receiptDetailCell3);

                    cellMiddle.AddElement(receiptDetailTable);
                    #endregion Table For Money Receipt Detail
                    MainTable.AddCell(cellMiddle);

                    #region Table For Footer Detail
                    PdfPCell cellBottom = new PdfPCell();
                    cellBottom.Colspan = 1;
                    cellBottom.FixedHeight = (height99 * 9) / 100;
                    cellBottom.PaddingLeft = (width99 * (float)1.5) / 100;
                    cellBottom.PaddingRight = (width99 * (float)1.5) / 100;
                    cellBottom.HorizontalAlignment = Element.ALIGN_CENTER;
                    cellBottom.VerticalAlignment = Element.ALIGN_MIDDLE;
                    var campusNamePre = new Chunk("Campus Address: ", FontFactory.GetFont("Verdana-bold", ConvertPixelTopoint(11)));
                    var campusNamePost = new Chunk(receipt.StudentPayment.StudentProgram.Batch.Campus.Location, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11)));
                    var phonePre = new Chunk(" Phone: ", FontFactory.GetFont("Verdana-bold", ConvertPixelTopoint(11)));
                    var phonePost = new Chunk(receipt.StudentPayment.StudentProgram.Batch.Campus.ContactNumber, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11)));
                    var website = new Chunk(", " + studentPayment.StudentProgram.Program.Organization.Website, FontFactory.GetFont("Verdana", ConvertPixelTopoint(11)));
                    var bottom = new Paragraph { campusNamePre, campusNamePost, phonePre, phonePost, website };
                    bottom.Alignment = Element.ALIGN_CENTER;
                    bottom.SetLeading(0, 1.35f);
                    cellBottom.AddElement(bottom);
                    MainTable.AddCell(cellBottom);
                    #endregion Table For Footer Detail

                    doc.Add(MainTable);
                  
                    doc.Close();
                    Response.BinaryWrite(memoryStream.GetBuffer());
                    //File.WriteAllBytes(notUriPath, memoryStream.ToArray());
                }

                //theIframeForPrint.Attributes.Add("src", fullFilePath);

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + docName + ".pdf");
                Response.End();
                #endregion End PDF code generation

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }
        }

        #endregion

        #region Collection Summary Report

        public ActionResult CollectionSummaryReport()
        {
            ViewBag.IsPost = false;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.ProgramId = null;
            ViewBag.SessionId = null;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionList = new SelectList(new List<Session>(), "Id", "Name");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult CollectionSummaryReport(long organizationId, long programId, long sessionId, string dateFrom, string dateTo)
        {
            ViewBag.IsPost = true;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.ProgramList = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionList = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.ProgramId = null;
            ViewBag.SessionId = null;
            ViewBag.OrganizationName = "";
            ViewBag.ProgramName = "";
            ViewBag.SessionName = "";
            ViewBag.CollectionSummaryReportList = new List<CollectionSummaryReportDto>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationName = _organizationService.LoadById(organizationId).Name;
                ViewBag.ProgramName = (programId != 0) ? _programService.GetProgram(programId).Name : "All Program";
                ViewBag.SessionName = (sessionId != 0) ? _sessionService.LoadById(sessionId).Name : "All Session";
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", organizationId);

                List<SelectListItem> programSelectList = new SelectList(_programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(organizationId)), "Id", "Name", programId).ToList();
                programSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Program" });
                ViewBag.ProgramList = new SelectList(programSelectList, "Value", "Text", programId);

                List<SelectListItem> sessionSelectList = new SelectList(_sessionService.LoadAuthorizedSession(_userMenu, _commonHelper.ConvertIdToList(organizationId), (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null), "Id", "Name", sessionId).ToList();
                sessionSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Session" });
                ViewBag.SessionList = new SelectList(sessionSelectList, "Value", "Text", sessionId);


                ViewBag.DateFrom = Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd");
                ViewBag.DateTo = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd");
                if (Convert.ToDateTime(dateFrom) > Convert.ToDateTime(dateTo))
                    throw new InvalidDataException("From Date must be equal or Less than To Date");

                List<CollectionSummaryReportDto> collectionSummaryReportList = _studentPaymentService.LoadCollectionSummaryReport(_userMenu, organizationId, programId, sessionId, dateFrom, dateTo);
                ViewBag.CollectionSummaryReportList = collectionSummaryReportList;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #endregion Collection Summary Report

        #region Due Summary Report

        public ActionResult DueSummaryReport()
        {
            ViewBag.IsPostDueSummary = false;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.ProgramId = null;
            ViewBag.SessionId = null;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionList = new SelectList(new List<Session>(), "Id", "Name");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult DueSummaryReport(long organizationId, long programId, long sessionId, string dateFrom, string dateTo)
        {
            ViewBag.IsPostDueSummary = true;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.ProgramId = null;
            ViewBag.SessionId = null;
            ViewBag.OrganizationName = "";
            ViewBag.ProgramName = "";
            ViewBag.SessionName = "";
            ViewBag.DueSummaryReportList = new List<DueSummaryReportDto>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationName = _organizationService.LoadById(organizationId).Name;
                ViewBag.ProgramName = (programId != 0) ? _programService.GetProgram(programId).Name : "All Program";
                ViewBag.SessionName = (sessionId != 0) ? _sessionService.LoadById(sessionId).Name : "All Session";
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", organizationId);

                List<SelectListItem> programSelectList = new SelectList(_programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(organizationId)), "Id", "Name", programId).ToList();
                programSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Program" });
                ViewBag.ProgramList = new SelectList(programSelectList, "Value", "Text", programId);

                List<SelectListItem> sessionSelectList = new SelectList(_sessionService.LoadAuthorizedSession(_userMenu, _commonHelper.ConvertIdToList(organizationId), (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null), "Id", "Name", sessionId).ToList();
                sessionSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Session" });
                ViewBag.SessionList = new SelectList(sessionSelectList, "Value", "Text", sessionId);

                ViewBag.DateFrom = Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd");
                ViewBag.DateTo = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd");
                if (Convert.ToDateTime(dateFrom) > Convert.ToDateTime(dateTo))
                    throw new InvalidDataException("From Date must be equal or Less than To Date");

                List<DueSummaryReportDto> dueSummaryReportList = _studentPaymentService.LoadDueSummaryReport(_userMenu, organizationId, programId, sessionId, dateFrom, dateTo);
                ViewBag.DueSummaryReportList = dueSummaryReportList;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #endregion Due Summary Report

        #region Collection V Due Report

        public ActionResult CollectionVDueReport()
        {
            ViewBag.IsPostCollectionVDue = false;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateTill = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.ProgramId = null;
            ViewBag.SessionId = null;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionList = new SelectList(new List<Session>(), "Id", "Name");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult CollectionVDueReport(long organizationId, long programId, long sessionId, string dateTill)
        {
            ViewBag.IsPostCollectionVDue = true;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateTill = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.ProgramId = null;
            ViewBag.SessionId = null;
            ViewBag.OrganizationName = "";
            ViewBag.ProgramName = "";
            ViewBag.SessionName = "";
            ViewBag.CollectionVDueReportList = new List<CollectionVDueReportDto>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationName = _organizationService.LoadById(organizationId).Name;
                ViewBag.ProgramName = (programId != 0) ? _programService.GetProgram(programId).Name : "All Program";
                ViewBag.SessionName = (sessionId != 0) ? _sessionService.LoadById(sessionId).Name : "All Session";
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", organizationId);

                List<SelectListItem> programSelectList = new SelectList(_programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(organizationId)), "Id", "Name", programId).ToList();
                programSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Program" });
                ViewBag.ProgramList = new SelectList(programSelectList, "Value", "Text", programId);

                List<SelectListItem> sessionSelectList = new SelectList(_sessionService.LoadAuthorizedSession(_userMenu, _commonHelper.ConvertIdToList(organizationId), (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null), "Id", "Name", sessionId).ToList();
                sessionSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Session" });
                ViewBag.SessionList = new SelectList(sessionSelectList, "Value", "Text", sessionId);

                ViewBag.DateTill = Convert.ToDateTime(dateTill).ToString("yyyy-MM-dd");
                if (Convert.ToDateTime(dateTill) > DateTime.Today)
                    throw new InvalidDataException("Till Date must be equal or Less than ToDay");
                List<CollectionVDueReportDto> collectionVDueReportList = _studentPaymentService.LoadCollectionVDueReport(_userMenu, organizationId, programId, sessionId, dateTill);
                ViewBag.CollectionVDueReportList = collectionVDueReportList;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #endregion Collection V Due Report

        #region Branch Wise Transaction Report

        public ActionResult BranchWiseTransactionReport()
        {
            ViewBag.IsPost = false;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.BranchId = null;
            ViewBag.CampusId = null;
            ViewBag.ProgramBranchWiseType = 1; //1: Branch Wise 2: Program Wise
            ViewBag.DailySummaryType = 2; //1: Daily 2: Summary
            ViewBag.ReportType = 1; //1: Receiver 2: Receiver From
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult BranchWiseTransactionReport(int reportType, long organizationId, long branchId, List<long> campusIds, int programBranchWiseType, int dailySummaryType, string dateFrom, string dateTo)
        {
            ViewBag.IsPost = true;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            //ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-dd");
            //ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateFrom = Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd");
            ViewBag.DateTo = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.BranchId = null;
            ViewBag.CampusId = null;
            ViewBag.ProgramBranchWiseType = programBranchWiseType; //1: Branch Wise 2: Program Wise
            ViewBag.DailySummaryType = dailySummaryType; //1: Daily 2: Summary
            ViewBag.ReportType = reportType; //1: Service Branch 2: Collection Branch
            ViewBag.OrganizationName = "";
            ViewBag.BranchName = "";
            ViewBag.CampusName = "";
            List<string> campusName = new List<string>();
            ViewBag.BranchWiseTransactionReportList = new List<BranchWiseTransactionReportDto>();
            ViewBag.BranchWiseTransactionReportDueList = new List<BranchWiseTransactionReportDueDto>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationName = _organizationService.LoadById(organizationId).Name;
                ViewBag.BranchName = _branchService.GetBranch(branchId).Name;

                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", organizationId);
                ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(organizationId)), "Id", "Name", branchId);
                List<Campus> campusList = _campusService.LoadAuthorizeCampus(_userMenu, _commonHelper.ConvertIdToList(organizationId), null, _commonHelper.ConvertIdToList(branchId)).ToList();

                if (campusIds.Contains(SelectionType.SelelectAll))
                    ViewBag.CampusName = "All Campus";
                else
                {
                    if (campusList.Any())
                    {
                        foreach (var campus in campusList)
                        {
                            if (campusIds.Contains(campus.Id))
                                campusName.Add(campus.Name);
                        }
                        ViewBag.CampusName = string.Join(",", campusName);
                    }
                }

                var campusSelectList = new SelectList(campusList, "Id", "Name");
                List<SelectListItem> cList = campusSelectList.ToList();
                cList.Insert(0, new SelectListItem() { Value = "0", Text = "All Campus" });
                ViewBag.CampusList = new MultiSelectList(cList, "Value", "Text", campusIds);



                //ViewBag.CampusList = new MultiSelectList(_campusService.LoadAuthorizeCampus(_userMenu, _commonHelper.ConvertIdToList(organizationId), null, _commonHelper.ConvertIdToList(branchId)), "Id", "Name", campusIds);
                //ViewBag.ProgramBranchWiseType = programBranchWiseType; //1: Branch Wise 2: Program Wise
                //ViewBag.DailySummaryType = dailySummaryType; //1: Daily 2: Summary
                //ViewBag.DateFrom = Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd");
                //ViewBag.DateTo = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd");
                if (Convert.ToDateTime(dateFrom) > Convert.ToDateTime(dateTo))
                    throw new InvalidDataException("From Date must be equal or Less than To Date");
                List<BranchWiseTransactionReportDto> branchWiseTransactionReportList = _studentPaymentService.BranchWiseTransactionReportDtoList(_userMenu, organizationId, branchId, campusIds, programBranchWiseType, dailySummaryType, dateFrom, dateTo, reportType);
                List<BranchWiseTransactionReportDueDto> branchWiseTransactionReportDueList = new List<BranchWiseTransactionReportDueDto>();
                if (reportType == 1)
                {
                    branchWiseTransactionReportDueList = _studentPaymentService.BranchWiseTransactionReportDueDtoList(_userMenu, organizationId, branchId, campusIds, programBranchWiseType, dailySummaryType, dateFrom, dateTo, reportType);
                }
                ViewBag.BranchWiseTransactionReportList = branchWiseTransactionReportList;
                ViewBag.BranchWiseTransactionReportDueList = branchWiseTransactionReportDueList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #endregion Branch Wise Transaction Report

        #region Transaction Summary Report
        public ActionResult TransactionSummaryReport()
        {
            ViewBag.IsPost = false;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.ProgramId = null;
            ViewBag.BranchId = null;
            ViewBag.CampusId = null;
            ViewBag.Method = 0;
            ViewBag.DailySummaryType = 1; //1: Daily 2: Summary
            try
            {
                ViewBag.DateFrom = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.ProgramList = new MultiSelectList(new List<Program>(), "Id", "Name");
                ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }
        [HttpPost]
        public ActionResult TransactionSummaryReport(long organizationId, long branchId, List<long> programIds, List<long> campusIds, int method, int dailySummaryType, string dateFrom, string dateTo)
        {
            ViewBag.IsPost = true;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateFrom = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.ProgramId = null;
            ViewBag.BranchId = null;
            ViewBag.CampusId = null;
            ViewBag.Method = 0;
            ViewBag.DailySummaryType = 2; //1: Daily 2: Summary
            ViewBag.OrganizationName = "";
            ViewBag.BranchName = "";
            ViewBag.CampusName = "";
            List<string> campusName = new List<string>();
            ViewBag.TransactionSummaryReportList = new List<TransactionSummaryReportDto>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationName = _organizationService.LoadById(organizationId).Name;
                ViewBag.BranchName = _branchService.GetBranch(branchId).Name;

                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", organizationId);
                ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(organizationId)), "Id", "Name", branchId);
                List<Program> programList = _programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(organizationId), null, null, null, _commonHelper.ConvertIdToList(branchId)).ToList();
                
                List<Campus> campusList = _campusService.LoadAuthorizeCampus(_userMenu, _commonHelper.ConvertIdToList(organizationId), null, _commonHelper.ConvertIdToList(branchId)).ToList();

                if (campusIds.Contains(SelectionType.SelelectAll))
                    ViewBag.CampusName = "All Campus";
                else
                {
                    if (campusList.Any())
                    {
                        foreach (var campus in campusList)
                        {
                            if (campusIds.Contains(campus.Id))
                                campusName.Add(campus.Name);
                        }
                        ViewBag.CampusName = string.Join(",", campusName);
                    }
                }

                var programSelectList = new SelectList(programList, "Id", "Name");
                List<SelectListItem> pList = programSelectList.ToList();
                pList.Insert(0, new SelectListItem() { Value = "0", Text = "All Program" });
                ViewBag.ProgramList = new MultiSelectList(pList, "Value", "Text", campusIds);
                
                
                var campusSelectList = new SelectList(campusList, "Id", "Name");
                List<SelectListItem> cList = campusSelectList.ToList();
                cList.Insert(0, new SelectListItem() { Value = "0", Text = "All Campus" });
                ViewBag.CampusList = new MultiSelectList(cList, "Value", "Text", campusIds);

                ViewBag.Method = method; //0:All 1: Daily 2: Summary
                ViewBag.DailySummaryType = dailySummaryType; //1: Daily 2: Summary
                ViewBag.DateFrom = Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd");
                ViewBag.DateTo = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd");
                if (Convert.ToDateTime(dateFrom) > Convert.ToDateTime(dateTo))
                    throw new InvalidDataException("From Date must be equal or Less than To Date");
                List<TransactionSummaryReportDto> transactionSummaryReportList = _studentPaymentService.TransactioSummarynReportDtoList(_userMenu, organizationId, branchId, programIds, campusIds, method, dailySummaryType, dateFrom, dateTo);
                ViewBag.TransactionSummaryReportList = transactionSummaryReportList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }
        #endregion Branch Wise Transaction Report

        //need to check
        public void SetDuePaymentFormViewData()
        {
            var refList = new SelectList(_referenceService.GetAllRefferer(), "Id", "Name");
            ViewBag.RefererList = refList;
            ViewBag.PaymentMethods = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentMethod>(new List<int> { (int)PaymentMethod.BKash, (int)PaymentMethod.Cheque }), "Key", "Value");
        }

        private string GetReceivableAmountFromStudent(string stdProId)
        {
            decimal rreceivableAmountWithoutDiscount = 0;
            decimal recavailableAmount = 0;
            var studentProgram = _studentProgramService.GetStudentProgram(stdProId.Trim());
            var studentCourseDetailsList =
                    _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);
            var query = studentCourseDetailsList.GroupBy(x => x.CourseSubject.Course,
                (key, g) => new { Course = key, Subjects = g.ToList() });

            var courseList = query.ToList();
            var studentViewModel = new StudentViewModel();
            foreach (var courseAndCorrespondingSubjectList in courseList)
            {
                var courseViewModel = new CourseViewModel
                {
                    Id = courseAndCorrespondingSubjectList.Course.Id,
                    Name = courseAndCorrespondingSubjectList.Course.Name
                };
                var courseSubjectList = courseAndCorrespondingSubjectList.Subjects;
                foreach (var courseSubject in courseSubjectList)
                {
                    var subjectViewModel = new SubjectViewModel();
                    var subject = courseSubject.CourseSubject;
                    subjectViewModel.Name = courseSubject.CourseSubject.Subject.Name;
                    subjectViewModel.Payment = subject.Payment;
                    subjectViewModel.Id = subject.Id;
                    receivableAmountWithoutDiscount += Convert.ToDecimal(subject.Payment.ToString().Trim());
                    courseViewModel.SubjectViewModels.Add(subjectViewModel);
                }
                studentViewModel.CourseViewModels.Add(courseViewModel);
            }
            StudentPayment lastStudentPaymentTransication = _studentPaymentService.LoadByStudentProgram(studentProgram);
            if (lastStudentPaymentTransication == null)
            {
                /*First Transication in this Program*/
                /*Calculate Discount*/
                var existingDiscountList =
                        _discountService.LoadDiscount(studentProgram.Program,
                            studentProgram.Batch.Session, studentProgram.CreationDate);
                /*Discount Calculation Start */
                Discount appliedDiscountObj = GetDiscount(existingDiscountList, studentViewModel);
                var courseCount = studentViewModel.CourseViewModels.Count;

                if (appliedDiscountObj.Amount > 0)
                {
                    recavailableAmount = (decimal)(receivableAmountWithoutDiscount - appliedDiscountObj.Amount);
                    return recavailableAmount.ToString() + '#' + appliedDiscountObj.Amount;
                }
                recavailableAmount = receivableAmountWithoutDiscount - 0;
                return recavailableAmount.ToString() + '#' + "0";
            }
            /*Already paid some amount */
            /*Pick Discount amount from db for this Program*/
            if (lastStudentPaymentTransication.DueAmount != null)
                recavailableAmount = (decimal)lastStudentPaymentTransication.DueAmount;
            return recavailableAmount.ToString() + '#' + lastStudentPaymentTransication.DiscountAmount.ToString();
        }

        private Discount GetDiscount(IList<Discount> existingDiscountList, StudentViewModel studentViewModel)
        {
            IList<Discount> discountApplicableListForStd = new List<Discount>();
            foreach (var discountObj in existingDiscountList)
            {
                IList<DiscountDetail> discountDetailsList =
                    _discountDetailService.GetAllDiscountDetailByDiscountId(discountObj.Id);
                /*Check : does student deserve discount or not */
                if (discountDetailsList.Count <= studentViewModel.CourseViewModels.Count)
                {
                    var countdiscountDetailsList = discountDetailsList.Count;
                    int jj = 0;
                    foreach (var discountDetail in discountDetailsList)
                    {
                        var dCourse = discountDetail.Course;
                        var dMinSub = discountDetail.MinSubject;
                        int ii = 0;
                        foreach (var stdAssignedCourse in studentViewModel.CourseViewModels)
                        {
                            var stdCourse = _courseService.GetCourse(stdAssignedCourse.Id);
                            if (stdCourse == dCourse && dMinSub <= stdAssignedCourse.SubjectViewModels.Count)
                            {
                                /*TODO*/
                                jj++;
                                break;
                            }
                        }
                    }
                    /*Contain each and Every Course and their minSubject which allows discount */
                    if (countdiscountDetailsList == jj)
                    {
                        discountApplicableListForStd.Add(discountObj);
                    }
                }
            }
            var appliedDiscountObj = new Discount();
            if (discountApplicableListForStd.Count > 1)
            {
                appliedDiscountObj = _discountService.GetMostApplicableDiscountObj(discountApplicableListForStd);
            }
            if (discountApplicableListForStd.Count == 1)
            {
                appliedDiscountObj = discountApplicableListForStd[0];
            }
            return appliedDiscountObj;
        }

        private MoneyReceipt GetMoneyReceipt(StudentPayment studentPayment, string comingFromAddCourse = "")
        {
            var moneyReceipt = new MoneyReceipt();
            moneyReceipt.MrNo = studentPayment.ReceiptNo;
            //moneyReceipt.Session = studentPayment.StudentProgram.Batch.Session.Name;
            moneyReceipt.Session = studentPayment.Batch.Session.Name;
            moneyReceipt.Program = studentPayment.StudentProgram.Program.Name;
            moneyReceipt.RegNo = studentPayment.StudentProgram.Student.RegistrationNo;
            moneyReceipt.DateOFSubmission = studentPayment.StudentProgram.CreationDate;
            moneyReceipt.PrRollNo = studentPayment.StudentProgram.PrnNo;
            moneyReceipt.StudentName = studentPayment.StudentProgram.Student.NickName;
            // moneyReceipt.Branch = studentPayment.StudentProgram.Batch.Branch.Name;
            moneyReceipt.Branch = studentPayment.Batch.Branch.Name;
            moneyReceipt.BatchDays = studentPayment.Batch.Days;
            moneyReceipt.BatchName = studentPayment.Batch.Name;
            moneyReceipt.BatchTime = studentPayment.Batch.FormatedBatchTime;
            if (studentPayment.DueAmount != null && studentPayment.ReceivedAmount != null)
            {
                moneyReceipt.PayableAmount = (studentPayment.DueAmount + studentPayment.ReceivedAmount + studentPayment.SpDiscountAmount).ToString();
            }
            else if (studentPayment.DueAmount != null && studentPayment.ReceivedAmount == null)
            {
                moneyReceipt.PayableAmount = studentPayment.DueAmount.ToString();
            }
            else if (studentPayment.DueAmount == null && studentPayment.ReceivedAmount != null)
            {
                /*will not arise this case */
                moneyReceipt.PayableAmount = (studentPayment.ReceivedAmount + studentPayment.SpDiscountAmount).ToString();
            }
            else
            {
                moneyReceipt.PayableAmount = (studentPayment.SpDiscountAmount + 0).ToString();
            }
            var paymentCreator = _userService.LoadAspNetUserById(studentPayment.CreateBy);
            if (paymentCreator != null)
                moneyReceipt.UserFullName = paymentCreator.FullName;

            moneyReceipt.PaidAmount = studentPayment.ReceivedAmount.ToString();
            moneyReceipt.CampusAddress = studentPayment.StudentProgram.Batch.Campus.Location;
            moneyReceipt.CampusContact = studentPayment.StudentProgram.Batch.Campus.ContactNumber;

            int count =
                _studentPaymentService.GetAllStudentPaymentListCountBeforeAParticularPayment(studentPayment);
            moneyReceipt.MoneyReceiptCount = count;
            moneyReceipt.StudentPayment = studentPayment;

            if (comingFromAddCourse == "YES")
            {
                var studentCourseDetailList =
              _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(
                  studentPayment.StudentProgram.Id);
                IList<CourseSubject> cSubList = studentCourseDetailList.Select(cs => cs.CourseSubject).ToList();
                moneyReceipt.StudentPayment.CourseSubjectList = cSubList;
            }

            //moneyReceipt.CourseDetails = studentCList;
            moneyReceipt.DueAmount = studentPayment.DueAmount.ToString();
            if (studentPayment.NextReceivedDate != null)
                moneyReceipt.NextPaymentDate = (DateTime)studentPayment.NextReceivedDate;
            return moneyReceipt;
        }

        public ActionResult GenerateMoneyReciept(long id, string withLayout, string comingFromAddCourse = "")
        {
            try
            {
                ViewBag.withLayout = withLayout;
                var studentPayment = _studentPaymentService.LoadById(id);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                List<long> programIdList = AuthHelper.LoadProgramIdList(_userMenu);

                var errorMessage = "";
                if (studentPayment.StudentProgram != null)
                {
                    var userBranchId = studentPayment.StudentProgram.Batch.Branch.Id;
                    var userProgramId = studentPayment.StudentProgram.Program.Id;
                    if (branchIdList != null)
                    {
                        bool isBranchSelect = branchIdList.Contains(userBranchId);
                        if (!isBranchSelect)
                            errorMessage = "You are not authorized to this branch";
                        ViewBag.NotAuthorize = errorMessage;
                    }
                    if (programIdList != null)
                    {
                        //bool isProgramSelect = Array.Exists(programIdList, item => item == userProgramId);
                        bool isProgramSelect = programIdList.Contains(userProgramId);
                        if (!isProgramSelect)
                            errorMessage = "You are not authorized to this program";
                        ViewBag.NotAuthorize = errorMessage;
                    }
                }
                ViewBag.NotAuthorize = "";
                ///
                ViewBag.PaymentId = id;
                ViewBag.comingFromAddCourse = comingFromAddCourse;
                /// 
                MoneyReceipt receipt = GetMoneyReceipt(studentPayment, comingFromAddCourse);
                return View(receipt);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Occured Wrong";
                return View();
            }
        }

        /*DUE LIST DATA TABLE IMPLEMENTATION*/
        [HttpPost]
        public JsonResult StudentProgramListForDueListResult(int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course,
            string[] informationViewList, DateTime tillDate, string[] batchDays = null, string[] batchTime = null)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    NameValueCollection nvc = Request.Form;

                    string orderBy = "";
                    string orderDir = "";
                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        int informationViewListLength = informationViewList.Length;
                        if (Convert.ToInt32(orderBy) == 0)
                            orderBy = "A.Id";
                        else if (Convert.ToInt32(orderBy) == informationViewList.Length + 1)
                        {
                            orderBy = "A.DueAmount";
                        }
                        else
                        {//A.NextReceivedDate,A.PrnNo,A.Batch,A.Branch,s1_.NickName,s1_.Mobile,s1_.GuardiansMobile1,i_.Name as Institute
                            switch (informationViewList[Convert.ToInt32(orderBy) - 1])
                            {
                                case "Program Roll":
                                    orderBy = "A.PrnNo";
                                    break;
                                case "Nick Name":
                                    orderBy = "s1_.NickName";
                                    break;
                                case "Personal Mobile":
                                    orderBy = "s1_.Mobile";
                                    break;
                                case "Guardians Mobile":
                                    orderBy = "s1_.GuardiansMobile1";
                                    break;
                                case "Next Payment Date":
                                    orderBy = "A.NextReceivedDate";
                                    break;
                                case "School/College Name":
                                    orderBy = "i_.Name";
                                    break;
                                case "Branch":
                                    orderBy = "A.Branch";
                                    break;
                                case "Batch":
                                    orderBy = "A.Batch";
                                    break;
                                default:
                                    orderBy = "A.Id";
                                    break;
                            }
                        }
                    }

                    #endregion
                    long[] brances;
                    if (branchId.Contains(SelectionType.SelelectAll))
                    {
                        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                        IList<Branch> branchList = _branchService.LoadAuthorizedBranch(userMenu, null, _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId));

                        brances = new long[branchList.Count];
                        int i = 0;
                        foreach (var c in branchList)
                        {
                            brances[i++] = c.Id;
                        }
                    }
                    else
                    {
                        brances = branchId;
                    }

                    int recordsTotal = _studentPaymentService.LoadDueListCount(programId, sessionId, brances, campusId, batchName, course, tillDate.AddHours(23).AddMinutes(59).AddSeconds(59), batchDays, batchTime);

                    long recordsFiltered = recordsTotal;
                    IList<StudentDueListDto> studenDueList = _studentPaymentService.LoadDueList(programId, sessionId, brances, campusId, batchName, course,                     tillDate.AddHours(23).AddMinutes(59).AddSeconds(59), batchDays, batchTime, start, length, orderBy, orderDir);
                    
                    var data = new List<object>();


                    foreach (var c in studenDueList)
                    {
                        var str = new List<string>();
                        str.Add(c.RowNum.ToString());

                        if (informationViewList != null)
                        {

                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Program Roll":
                                        str.Add(c.PrnNo);
                                        break;
                                    case "Nick Name":
                                        str.Add(c.NickName);
                                        break;
                                    case "Personal Mobile":
                                        str.Add(c.Mobile);
                                        break;

                                    case "Guardians Mobile":
                                        str.Add(c.GuardiansMobile1);
                                        break;

                                    case "Next Payment Date":
                                        str.Add(c.NextReceivedDate.Value.ToString("dd-MM-yyyy"));
                                        break;

                                    case "School/College Name":
                                        str.Add(c.Institute);
                                        break;

                                    case "Branch":
                                        str.Add(c.Branch);
                                        break;

                                    case "Batch":
                                        str.Add(c.Batch);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        str.Add(String.Format("{0:0}", c.DueAmount));
                        str.Add("<a class='glyphicon glyphicon-credit-card' target='_blank' href='HistoryOfPayment?stdProRoll=" + c.PrnNo + "'></a>");
                        data.Add(str);

                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return Json(HttpNotFound());
        }

        private List<StudentPayment> GetProgramWiseLastDuePayments(IList<StudentPayment> studenPayments)
        {
            var paymentList = new List<StudentPayment>();
            var sProgramPaymentGroup = studenPayments.GroupBy(x => x.StudentProgram, (key, group) => new { Key = key, Group = group.OrderBy(x => x.Id).ToList() });
            sProgramPaymentGroup.ForEach(x =>
            {
                var lastPayment = x.Group.LastOrDefault();
                if (lastPayment != null && lastPayment.DueAmount != null && lastPayment.DueAmount > 0)
                {
                    paymentList.Add(lastPayment);
                }
            });
            return paymentList;
        }

        [HttpGet]
        public ActionResult ExportDueListExcel(string programHeader, string branchHeader, string campusHeader,
            string batchDayHeader
            , string batchHeader, string courseHeader, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course,
             string[] batchDays, string[] batchTime, DateTime tillDate, string[] informationViewList)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                long[] brances;
                if (branchId.Contains(SelectionType.SelelectAll))
                {
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    IList<Branch> branchList = _branchService.LoadAuthorizedBranch(userMenu, null, _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId));

                    brances = new long[branchList.Count];
                    int i = 0;
                    foreach (var c in branchList)
                    {
                        brances[i++] = c.Id;
                    }
                }
                else
                {
                    brances = branchId;
                }

                IList<StudentDueListDto> studenDueList = _studentPaymentService.LoadDueList(programId, sessionId, brances, campusId, batchName, course,
                      tillDate.AddHours(23).AddMinutes(59).AddSeconds(59), batchDays, batchTime, 0, 0, "A.PrnNo", "asc");

                List<string> headerList = new List<string>();
                headerList.Add("Student Due List Report");
                headerList.Add(programHeader);
                headerList.Add("Branch : " + branchHeader);
                headerList.Add("Campus : " + campusHeader);
                headerList.Add("Batch : " + batchHeader);
                headerList.Add("Course : " + courseHeader);
                headerList.Add("Date : " + tillDate.ToString("dd-MMM-yyyy"));
                headerList.Add("");

                List<string> footerList = new List<string>();
                footerList.Add("");
                //footerList.Add("Developed By OnnoRokom Software Ltd.");

                List<string> columnList = new List<string>();
                List<object> TotalDueAmountRow = new List<object>();
                columnList.Add("Sl.");
                TotalDueAmountRow.Add("Total");
                if (informationViewList.Any())
                {
                    foreach (string informationView in informationViewList)
                    {
                        columnList.Add(informationView);
                        TotalDueAmountRow.Add("");
                    }
                }
                columnList.Add("Due Amount");


                decimal totalDueAmount = 0;
                var studentExcelList = new List<List<object>>();

                foreach (var c in studenDueList)
                {
                    //  var currentStudentTillDateDue = c.Group.OrderByDescending(x => x.ReceivedDate).FirstOrDefault();

                    var str = new List<string>();
                    var xlsRow = new List<object>();
                    xlsRow.Add(c.RowNum.ToString());


                    if (informationViewList != null)
                    {

                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case "Program Roll":
                                    xlsRow.Add(c.PrnNo);
                                    break;
                                case "Nick Name":
                                    xlsRow.Add(c.NickName);
                                    break;
                                case "Personal Mobile":
                                    xlsRow.Add(c.Mobile);
                                    break;

                                case "Guardians Mobile":
                                    xlsRow.Add(c.GuardiansMobile1);
                                    break;

                                case "Next Payment Date":
                                    xlsRow.Add(c.NextReceivedDate.Value.ToString("dd-MM-yyyy"));
                                    break;

                                case "School/College Name":
                                    xlsRow.Add(c.Institute);
                                    break;

                                case "Branch":
                                    xlsRow.Add(c.Branch);
                                    break;

                                case "Batch":
                                    xlsRow.Add(c.Batch);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    xlsRow.Add(String.Format("{0:0}", c.DueAmount));
                    totalDueAmount += (decimal)c.DueAmount;
                    studentExcelList.Add(xlsRow);

                }

                TotalDueAmountRow.Add(String.Format("{0:0}", totalDueAmount));
                studentExcelList.Add(TotalDueAmountRow);

                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "student-due-list-report__"
                   + DateTime.Now.ToString("yyyyMMdd-HHmmss"), false);

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }
        }

        
        [HttpPost]
        public JsonResult MyTransactionReportResult(int draw, int start, int length, long[] branchId, long[] campusId, DateTime date)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu) ??
                                              _branchService.LoadBranch().Select(x => x.Id).ToList();

                    IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
                    IList<Campus> campusList = new List<Campus>();
                    campusList = _campusService.LoadAuthorizeCampus(_userMenu, null, null, branchId.ToList());
                    var campusIdList = campusList.Select(x => x.Id).ToArray();

                    NameValueCollection nvc = Request.Form;
                    var currentUser = HttpContext.User.Identity;
                    // DateTime date = Convert.ToDateTime(date);
                    var currentUserId = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    IList<StudentPayment> studentPaymentList = _studentPaymentService.GetMyTransactionReport(currentUserId, branchId, branchIdList, campusId, campusIdList, date, start, length);
                    int recordsTotal = _studentPaymentService.GetMyTransactionReportCount(currentUserId, branchId, branchIdList, campusId, campusIdList, date);
                    long recordsFiltered = recordsTotal;
                    #region datatable functionalities
                    decimal totalIncome = 0,
                                 totalConsiderationAmount = 0,
                                 totalPreviousDue = 0,
                                 totalReceivable = 0,
                                 totalSpDiscount = 0,
                                 totalReceived = 0,
                                 totalCashBack = 0,
                                 totalCurrentDue = 0;
                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var c in studentPaymentList)
                    {
                        var str = new List<string>();
                        if (studentPaymentList != null)
                        {
                            //sl.
                            str.Add(sl.ToString());
                            //date
                            if (c.CreationDate != null)
                            {
                                str.Add(String.Format("{0:dd/MM/yyyy}", c.CreationDate));
                            }
                            else
                            {
                                str.Add("");
                            }
                            //branch name
                            str.Add(c.Batch.Branch.Name);
                            str.Add(c.Batch.Campus.Name);
                            //program name
                            str.Add(!string.IsNullOrEmpty(c.StudentProgram.Program.ShortName) ? c.StudentProgram.Program.ShortName : "");
                            //program roll
                            str.Add(c.StudentProgram.PrnNo);
                            str.Add(c.StudentProgram.Student.RegistrationNo);
                            //mrn
                            if (c.PaymentType == 1)
                            {
                                if (c.ReceiptNo != null)
                                {
                                    str.Add("<a class='mrnclass' href='GenerateMoneyReciept/" + c.Id + "' name='" + c.Id +
                                            "' id='" + c.Id + "'>" + c.ReceiptNo + "<a/>");
                                }
                                else
                                {
                                    str.Add("");
                                }
                            }
                            else
                            {
                                str.Add("");
                            }
                            //crn
                            if (c.PaymentType == 2)
                            {
                                if (c.ReceiptNo != null)
                                {
                                    str.Add("<a class='mrnclass' href='GenerateMoneyReciept/" + c.Id + "' name='" + c.Id +
                                            "' id='" + c.Id + "'>" + c.ReceiptNo + "<a/>");
                                }
                                else
                                {
                                    str.Add("");
                                }
                            }
                            else
                            {
                                str.Add("");
                            }
                            //nickname
                            if (c.StudentProgram.Student != null && c.StudentProgram.Student.Name != null && c.StudentProgram.Student.Name.Trim() != "")
                            {
                                str.Add(c.StudentProgram.Student.NickName);
                            }
                            else
                            {
                                str.Add(c.StudentProgram.Student.NickName == null ? "" : c.StudentProgram.Student.NickName);
                            }
                            //income
                            decimal coursefees = 0;
                            decimal discount = 0;
                            decimal income = 0;
                            if (c.CourseFees != null)
                            {
                                coursefees = Convert.ToDecimal(c.CourseFees);
                            }

                            if (c.DiscountAmount != null)
                            {
                                discount = Convert.ToDecimal(c.DiscountAmount);
                            }

                            income = Convert.ToDecimal(coursefees - discount);
                            if (income != 0)
                            {
                                str.Add(String.Format("{0:0}", income));
                                totalIncome += Convert.ToDecimal(income);
                            }
                            else
                            {
                                str.Add("");
                            }
                            //ConsiderationAmount
                            if (c.ConsiderationAmount != null)
                            {
                                str.Add(String.Format("{0:0}", c.ConsiderationAmount));
                                totalConsiderationAmount += Convert.ToDecimal(c.ConsiderationAmount);
                            }
                            else
                            {
                                str.Add("");
                            }
                            //previous due
                            decimal previousDue = 0;
                            decimal payableAmount = 0;
                            decimal consideration = 0;
                            if (c.PayableAmount != null)
                            {
                                payableAmount = Convert.ToDecimal(c.PayableAmount);
                            }

                            if (c.ConsiderationAmount != null)
                            {
                                consideration = Convert.ToDecimal(c.ConsiderationAmount);
                            }

                            previousDue = Convert.ToDecimal(payableAmount - income);
                            str.Add(String.Format("{0:0}", previousDue));
                            totalPreviousDue += previousDue;
                            //recievable
                            str.Add(String.Format("{0:0}", payableAmount));
                            totalReceivable += payableAmount;
                            //special discount
                            if (c.SpDiscountAmount != null)
                            {
                                if (c.SpDiscountAmount != 0)
                                {
                                    str.Add(String.Format("{0:0}", c.SpDiscountAmount));
                                    totalSpDiscount += Convert.ToDecimal(c.SpDiscountAmount);
                                }
                                else
                                {
                                    str.Add("");
                                }
                            }
                            else
                            {
                                str.Add("");
                            }
                            //recieved
                            if (c.ReceivedAmount != null)
                            {
                                if (c.ReceivedAmount != 0)
                                {
                                    str.Add(String.Format("{0:0}", c.ReceivedAmount));
                                    totalReceived += Convert.ToDecimal(c.ReceivedAmount);
                                }
                                else
                                {
                                    str.Add("");
                                }
                            }
                            else
                            {
                                str.Add("");
                            }
                            //cash back
                            if (c.CashBackAmount != null)
                            {
                                if (c.CashBackAmount != 0)
                                {
                                    str.Add(String.Format("{0:0}", c.CashBackAmount));
                                    totalCashBack += Convert.ToDecimal(c.CashBackAmount);
                                }
                                else { str.Add(""); }
                            }
                            else
                            {
                                str.Add("");
                            }
                            //current due
                            if (c.DueAmount != null)
                            {
                                str.Add(String.Format("{0:0}", c.DueAmount));
                                totalCurrentDue += Convert.ToDecimal(c.DueAmount);
                            }
                            else
                            {
                                str.Add("0.00");
                            }
                            //payment method
                            if (c.PaymentMethod != null)
                            {
                                //var paymentMethod = _commonHelper.GetPaymentMethodsForReport().Where(pm => pm.Value == c.PaymentMethod).ToList();
                                //string pmthd = paymentMethod[0].Key.ToString();
                                str.Add(_commonHelper.GetEmumIdToValue<PaymentMethod>((int)c.PaymentMethod));
                            }
                            else
                            {
                                str.Add("");
                            }
                            //referrer
                            if (c.Referrer != null && c.Referrer.Name != null)
                            {
                                str.Add(c.Referrer.Name);
                            }
                            else
                            {
                                str.Add("");
                            }
                           
                            var transcationIds = "";
                            if (c.OnlineTransactionses != null)
                            {
                                var onlineTransactions =
                                    c.OnlineTransactionses.Where(
                                        x => x.CreationDate.Date >= date && x.CreationDate.Date < date.AddDays(1)).ToList();

                                if (onlineTransactions != null && onlineTransactions.Count > 0)
                                {
                                    transcationIds = string.Join(", ", onlineTransactions.Select(x => x.TransactionId).ToList());
                                }
                            }
                            str.Add(transcationIds);
                            //remarks
                            if (c.Remarks != null)
                            {
                                str.Add(c.Remarks);
                            }
                            else
                            {
                                str.Add("");
                            }
                            
                            sl++;
                            data.Add(str);
                        }
                    }
                    var summary = new List<string>();
                    summary.Add("");
                    summary.Add("<strong>Total Summary</strong>");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("<strong>" + String.Format("{0:0}", totalReceived) + "<strong>");
                    summary.Add("<strong>" + String.Format("{0:0}", totalCashBack) + "<strong>");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("");
                    data.Add(summary);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                    #endregion
                }

                catch (Exception ex)
                {
                    var data = new List<object>();
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return Json(HttpNotFound());
        }
        [HttpGet]
        public ActionResult MyTransactionReportToExcel(DateTime date, long[] branchId, long[] campusId, string campusName, string branchName)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                bool allBranch = false;
                //long[] branchIdList = AuthHelper.GetUserBranchIdList(_userMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                //branchIdList = branchIdList.Di;
                if (branchIdList == null)
                {
                    branchIdList = _branchService.LoadBranch().Select(x => x.Id).ToList();
                }
                IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
                IList<Campus> campusList = new List<Campus>();
                campusList = _campusService.LoadAuthorizeCampus(_userMenu, null, null, branchId.ToList());//LoadAuthorizedCampusesByBranch(branchId, branches);
                var campusIdList = campusList.Select(x => x.Id).ToArray();


                decimal totalIncome = 0,
                              totalConsiderationAmount = 0,
                              totalPreviousDue = 0,
                              totalReceivable = 0,
                              totalSpDiscount = 0,
                              totalReceived = 0,
                              totalCashBack = 0,
                              totalCurrentDue = 0;
                NameValueCollection nvc = Request.Form;
                var currentUser = HttpContext.User.Identity;
                var currentUserId = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                List<string> headerList = new List<string>();
                //headerList.Add(ErpInfo.OrganizationNameFull);
                headerList.Add("My Transaction Report");
                headerList.Add("Campus: " + campusName);
                headerList.Add("Branch: " + branchName);
                List<string> footerList = new List<string>();
                List<string> columnList = new List<string>();
                columnList.Add("Date");
                columnList.Add("Branch");
                columnList.Add("Campus");
                columnList.Add("Program");
                columnList.Add("Program Roll");
                columnList.Add("Registration No.");
                columnList.Add("MRN");
                columnList.Add("CRN");
                columnList.Add("Nick Name");
                columnList.Add("Income");
                columnList.Add("Consideration Amount");
                columnList.Add("Previous Due");
                columnList.Add("Receivable");
                columnList.Add("Special Discount");
                columnList.Add("Received");
                columnList.Add("Cash Back");
                columnList.Add("Current Due");
                columnList.Add("Payment Method");
                columnList.Add("Referrer");
                columnList.Add("Reference Note");
                columnList.Add("Remarks");
                // columnList.Add("User");
                var transactionXlsList = new List<List<object>>();
                IList<StudentPayment> studentPaymentList = _studentPaymentService.GetMyTransactionReport(currentUserId, branchId, branchIdList, campusId, campusIdList, date);
                foreach (var c in studentPaymentList)
                {
                    string creationDate = "",
                        branchNameValue = "",
                        campusNameValue = "",
                        programNameValue = "",
                        prnNo = "",
                        RegistrationNo = "",
                        mrn = "",
                        crn = "",
                        nickName = "",
                        incomeValue = "",
                        considerationAmountValue = "",
                        previousDueValue = "",
                        recievableValue = "",
                        spDiscountAmountValue = "",
                        receivedAmountValue = "",
                        cashBackAmountValue = "",
                        dueAmountValue = "",
                        paymentMethodValue = "",
                        refererName = "",
                        refererNote = "",
                        remarksValue = "";
                    //usernameValue = "";
                    if (studentPaymentList != null)
                    {

                        if (c.CreationDate != null)
                        {
                            creationDate = String.Format("{0:dd/MM/yyyy}", c.CreationDate);
                        }
                        branchNameValue = c.Batch.Branch.Name;
                        campusNameValue = c.Batch.Campus.Name;
                        //program name
                        programNameValue = !string.IsNullOrEmpty(c.StudentProgram.Program.ShortName) ? c.StudentProgram.Program.ShortName : "";
                        //program roll
                        prnNo = c.StudentProgram.PrnNo;
                        RegistrationNo = c.StudentProgram.Student.RegistrationNo;
                        //mrn
                        if (c.PaymentType == 1)
                        {
                            if (c.ReceiptNo != null)
                            {
                                mrn = c.ReceiptNo;
                            }

                        }
                        //crn
                        if (c.PaymentType == 2)
                        {
                            if (c.ReceiptNo != null)
                            {
                                crn = c.ReceiptNo;
                            }

                        }

                        //nickname
                        if (c.StudentProgram.Student != null && c.StudentProgram.Student.NickName != null && c.StudentProgram.Student.NickName.Trim() != "")
                        {
                            nickName = c.StudentProgram.Student.NickName;
                        }
                        //income
                        decimal coursefees = 0;
                        decimal discount = 0;
                        decimal income = 0;
                        if (c.CourseFees != null)
                        {
                            coursefees = Convert.ToDecimal(c.CourseFees);
                        }

                        if (c.DiscountAmount != null)
                        {
                            discount = Convert.ToDecimal(c.DiscountAmount);
                        }

                        income = Convert.ToDecimal(coursefees - discount);
                        if (income != 0)
                        {
                            incomeValue = String.Format("{0:0}", income);
                            totalIncome += Convert.ToDecimal(income);
                        }

                        //ConsiderationAmount
                        if (c.ConsiderationAmount != null)
                        {
                            considerationAmountValue = String.Format("{0:0}", c.ConsiderationAmount);
                            totalConsiderationAmount += Convert.ToDecimal(c.ConsiderationAmount);
                        }

                        //previous due
                        decimal previousDue = 0;
                        decimal payableAmount = 0;
                        decimal consideration = 0;
                        if (c.PayableAmount != null)
                        {
                            payableAmount = Convert.ToDecimal(c.PayableAmount);
                        }

                        if (c.ConsiderationAmount != null)
                        {
                            consideration = Convert.ToDecimal(c.ConsiderationAmount);
                        }

                        previousDue = Convert.ToDecimal(payableAmount - income);
                        previousDueValue = String.Format("{0:0}", previousDue);
                        totalPreviousDue += previousDue;
                        //recievable
                        recievableValue = String.Format("{0:0}", payableAmount);
                        totalReceivable += payableAmount;
                        //special discount
                        if (c.SpDiscountAmount != null)
                        {
                            if (c.SpDiscountAmount != 0)
                            {
                                spDiscountAmountValue = String.Format("{0:0}", c.SpDiscountAmount);
                                totalSpDiscount += Convert.ToDecimal(c.SpDiscountAmount);
                            }

                        }

                        //recieved
                        if (c.ReceivedAmount != null)
                        {
                            if (c.ReceivedAmount != 0)
                            {
                                receivedAmountValue = String.Format("{0:0}", c.ReceivedAmount);
                                totalReceived += Convert.ToDecimal(c.ReceivedAmount);
                            }

                        }

                        //cash back
                        if (c.CashBackAmount != null)
                        {
                            if (c.CashBackAmount != 0)
                            {
                                cashBackAmountValue = String.Format("{0:0}", c.CashBackAmount);
                                totalCashBack += Convert.ToDecimal(c.CashBackAmount);
                            }

                        }

                        //current due
                        if (c.DueAmount != null)
                        {
                            dueAmountValue = String.Format("{0:0}", c.DueAmount);
                            totalCurrentDue += Convert.ToDecimal(c.DueAmount);
                        }
                        else
                        {
                            dueAmountValue = "0.00";
                        }
                        //payment method
                        if (c.PaymentMethod != null)
                        {
                            //var paymentMethod = _commonHelper.GetPaymentMethodsForReport().Where(pm => pm.Value == c.PaymentMethod).ToList();
                            //string pmthd = paymentMethod[0].Key.ToString();
                            paymentMethodValue = _commonHelper.GetEmumIdToValue<PaymentMethod>((int)c.PaymentMethod);
                        }

                        //referer
                        if (c.Referrer != null && c.Referrer.Name != null)
                        {
                            refererName = c.Referrer.Name;
                        }

                        //referrer note
                        if (!string.IsNullOrEmpty(c.SpReferenceNote))
                        {
                            refererNote = c.SpReferenceNote;
                        }

                        //remarks
                        if (c.Remarks != null)
                        {
                            remarksValue = c.Remarks;
                        }
                    }
                    var xlsRow = new List<object> { creationDate, branchNameValue, campusNameValue, programNameValue, prnNo, RegistrationNo, mrn, crn, nickName, incomeValue, considerationAmountValue, previousDueValue, recievableValue, spDiscountAmountValue, receivedAmountValue, cashBackAmountValue, dueAmountValue, paymentMethodValue, refererName, refererNote, remarksValue };
                    transactionXlsList.Add(xlsRow);
                }
                var summaryRow = new List<object> { "Total Summary", "", "", "", "", "", "", "", "", "", "", "", "", "", String.Format("{0:0}", totalReceived), String.Format("{0:0}", totalCashBack), "", "", "", "", "", "" };
                transactionXlsList.Add(summaryRow);
                footerList.Add("Total Received Amount: " + String.Format("{0:0}", totalReceived));
                footerList.Add("Total Cash Back: " + String.Format("{0:0}", totalCashBack));
                footerList.Add("Total: " + String.Format("{0:0}", (totalReceived - totalCashBack)));
                //footerList.Add("");
                //footerList.Add("Developed By OnnoRokom Software Ltd.");
                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "my-Transaction-list__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }

        }
        [HttpPost]
        public JsonResult GetBatchInformation(long[] programId, long[] sessionId, long[] branchId, long[] campusId, string[] batchDaysList = null, string[] batchTimeList = null)
        {
            if (Request.IsAjaxRequest())
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //long[] allAuthorizedBranches = _branchService.LoadAuthorizedBranch(_userMenu).Select(x => x.Id).ToArray();
                    List<long> allAuthorizedBranches = _branchService.LoadAuthorizedBranch(_userMenu).Select(x => x.Id).ToList();
                    long[] authorizedProgramIds = _programService.LoadAuthorizedProgram(_userMenu).Select(x => x.Id).ToArray();
                    long[] authorizedSessionIds = _sessionService.LoadAuthorizedSession(_userMenu, null).Select(x => x.Id).ToArray();

                    IList<Branch> authorizedBranches = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, allAuthorizedBranches).Distinct().ToList();

                    IList<Batch> batches = _batchService.LoadBatch().Where(x => x.Status == StudentProgram.EntityStatus.Active).ToList();
                    IList<Campus> campusList = _campusService.LoadAuthorizeCampus(_userMenu, null, programId.ToList(), branchId.ToList(), sessionId.ToList());//LoadAuthorizedCampusesByBranch(branchId, authorizedBranches);
                    IList<Batch> batch =
                       batches.Where(b => b.Campus.Id.In(campusList.Select(c => c.Id).Distinct().ToArray()) && b.Branch.Id.In(authorizedBranches.Select(ab => ab.Id).Distinct().ToArray()) && b.Program.Id.In(programId) && b.Session.Id.In(sessionId)).Distinct().ToList();
                    //.Where(b => b.Branch.Id.In(authorizedBranches.Select(ab => ab.Id).Distinct().ToArray())).Distinct().ToList();
                    if (batchDaysList != null)
                    {
                        if (batchDaysList.Contains("0"))
                        {
                            batch = batch;
                        }
                        else
                        {
                            batch = batch.Where(b => b.Days.In(batchDaysList)).Distinct().ToList();
                        }
                    }
                    if (batchTimeList != null)
                    {
                        if (batchTimeList.Contains("0"))
                        {
                            batch = batch;
                        }
                        else
                        {
                            batch = batch.Where(b => b.BatchTime.In(batchTimeList)).Distinct().ToList();
                        }
                    }
                    List<string> batchdays = batch.Select(b => b.Days).Distinct().ToList();
                    SelectList batchDaysSelectList = new SelectList(batchdays.Select(x => new KeyValuePair<string, string>(x, x)), "Value", "Value");
                    List<SelectListItem> batchDays = batchDaysSelectList.ToList();
                    if (batchDays.Count() > 0)
                    {
                        batchDays.Insert(0, new SelectListItem() { Value = "0", Text = "ALL DAY" });
                    }

                    SelectList batchTimeSelectList = new SelectList(batch, "BatchTime", "FormatedBatchTime");
                    List<SelectListItem> batchTimes = batchTimeSelectList.ToList();

                    if (batchTimes.Count() > 0)
                    {
                        batchTimes.Insert(0, new SelectListItem() { Value = "0", Text = "ALL TIME" });
                    }

                    SelectList batchNameSelectList = new SelectList(batch, "Id", "Name");
                    List<SelectListItem> batchNames = batchNameSelectList.ToList();
                    if (batchNames.Count() > 0)
                    {
                        batchNames.Insert(0, new SelectListItem() { Value = "0", Text = "ALL BATCH" });
                    }

                    return Json(new { batchDays = batchDays, batchTimes = batchTimes, batchNames = batchNames, IsSuccess = true });

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }
    }
}