﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MemberLoanOpening
{
    public interface IMemberLoanOpeningSaveTest
    {
        void Should_Return_Invalid_OpeningBalance();
        void Should_Save_Successfully();
    }

    [Trait("Area", "Hr")]
    [Trait("Service", "MemberLoanService")]
    public class MemberLoanOpeningSaveTest : MemberLoanOpeningBaseTest, IMemberLoanOpeningSaveTest
    {
        [Fact]
        public void Should_Return_Invalid_OpeningBalance()
        {
            var member = PersistTeamMember();
            var memberLoanApplication = _memberLoanApplicationFactory.Create();
            memberLoanApplication.Object.TeamMember = member;
            var memberLoan = _memberLoanFactory.Create().WithMemberLoanApplication(memberLoanApplication.Object);
            _memberLoanService.SaveOrUpdate(new List<MemberLoan>() { memberLoan.Object });
            Assert.Throws<InvalidDataException>(() => _memberLoanService.SaveOrUpdate(new List<MemberLoan>() { memberLoan.Object }));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var member = PersistTeamMember();
            var memberLoanApplication = _memberLoanApplicationFactory.Create();
            memberLoanApplication.Object.TeamMember = member;
            var memberLoan = _memberLoanFactory.Create().WithMemberLoanApplication(memberLoanApplication.Object);
            _memberLoanService.SaveOrUpdate(new List<MemberLoan>() { memberLoan.Object });
            Assert.True(memberLoan.Object.Id > 0);
        }
    }
}
