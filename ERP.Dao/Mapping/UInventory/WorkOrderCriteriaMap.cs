using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class WorkOrderCriteriaMap :  BaseClassMap<WorkOrderCriteria, long>
    {
        public WorkOrderCriteriaMap()
        {
            Table("UINV_WorkOrderCriteria");
            LazyLoad();
            References(x => x.WorkOrderDetail).Column("WorkOrderDetailId");
            Map(x => x.Criteria).Column("Criteria").Not.Nullable();
            Map(x => x.CriteriaValue).Column("CriteriaValue").Not.Nullable();
        }
    }
}
