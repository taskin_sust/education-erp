﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class ReferrerController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly IReferenceService _referrerService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        public ReferrerController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _referrerService = new ReferenceService(session);
                _userService = new UserService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
        }
        #endregion

        #region Manage
        public ActionResult ManageReferrer()
        {
            try
            {
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            return View();

        }

        #region Render Data Table
        [HttpPost]
        public ActionResult ReferrerListResult(int draw, int start, int length, string Name, string Code, string Status, string Rank)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    int sr = start + 1;
                    string orderBy = "";
                    string orderDir = "";
                    #region OrderBy and Direction
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {

                            case "1":
                                orderBy = "Name";
                                break;
                            case "2":
                                orderBy = "Code";
                                break;
                            case "3":
                                orderBy = "Rank";
                                break;
                            case "4":
                                orderBy = "Status";
                                break;

                            default:
                                orderBy = "";
                                break;
                        }
                    }


                    #endregion
                    var referrer = new Referrer();
                    int recordsTotal = _referrerService.ReferrerRowCount(Name, Code, Status, Rank);
                    long recordsFiltered = recordsTotal;
                    List<Referrer> referrerList = _referrerService.GetReferrerList(start, length, orderBy, orderDir.ToUpper(), Name, Code, Status, Rank).ToList();
                    var maxMinRank = new int[2];
                    maxMinRank[0] = _referrerService.GetMinRank(referrer);
                    maxMinRank[1] = _referrerService.GetMaximumRank(referrer);

                    var data = new List<object>();
                    foreach (var c in referrerList)
                    {
                        var str = new List<string>();
                        str.Add((sr++).ToString());
                        str.Add(c.Name);
                        str.Add(c.Code);
                        string rankbtn = c.Rank.ToString();
                        if (c.Rank > maxMinRank[0])
                        {
                            rankbtn += "<a href='#'  id='" + c.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                        }
                        if (c.Rank < maxMinRank[1])
                        {
                            rankbtn += "<a href='#'  id='" + c.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                        }

                        str.Add(rankbtn);
                        if (c.Status == 1)
                        {
                            str.Add("Active");
                        }
                        else
                        {
                            str.Add("Inactive");
                        }
                        //str.Add("<a href='" + Url.Action("Details", "Referrer") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;<a href='" + Url.Action("Edit", "Referrer") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;<a id='" + c.Id.ToString() + "' href='#' data-name='" + c.Name.ToString() + "' class='glyphicon glyphicon-trash'> </a>");

                        if (c.IsEditable)
                        {
                              str.Add(LinkGenerator.GetGeneratedLink("Details", "Edit", "Delete", "Referrer", c.Id, c.Name));
                        }
                        else
                        {
                            str.Add(LinkGenerator.GetDetailsLink("Details", "Referrer", c.Id));
                        }

                        
                        data.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return HttpNotFound();
            }
        }
        #endregion
        #endregion

        #region Save Operation
        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Referrer referrer)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    IList<Referrer> referrerByCode = _referrerService.LoadReferrerByCode(referrer.Code, referrer.Id);
                    IList<Referrer> referrerByName = _referrerService.LoadReferrerByName(referrer.Name, referrer.Id);
                    referrer.IsEditable = true;
                    int _messageCode = 1;
                    if (referrerByName.Count == 0)
                    {
                        if (referrerByCode.Count == 0)
                        {
                            _messageCode = _referrerService.Save(referrer);

                            if (_messageCode == Constants.MessageSuccess)
                            {
                                ViewBag.SuccessMessage = "Referrer Addded Successfully";

                                ModelState.Clear();
                                return View(new Referrer());
                            }
                            else if (_messageCode == Constants.MessageError)
                            {
                                ViewBag.ErrorMessage = "Referrer Addded Fail";

                            }
                            else
                            {
                                ViewBag.InfoMessage = "";

                            }
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "Code can not be duplicate";

                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "Referrer Name can not be duplicate";

                    }
                }

            }

            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Referrer referrer = new Referrer();
            try
            {
                referrer = _referrerService.LoadById(id);
                if (referrer == null)
                {
                    return HttpNotFound();
                }
                //ViewBag.Status = new SelectList(db.Programs, "Id", "Name", material.ProgramId);
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);

            }
            return View(referrer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Referrer referrer)
        {
            Referrer updateReferrer = new Referrer();
            try
            {
                bool isUpdate = true;
                updateReferrer = _referrerService.LoadById(referrer.Id);

                if (updateReferrer.IsEditable)
                {
                    updateReferrer.Name = referrer.Name;
                    updateReferrer.Code = referrer.Code;
                    updateReferrer.Status = referrer.Status;
                    updateReferrer.IsEditable = referrer.IsEditable;
                }
                else
                {
                    //ViewBag.ErrorMessage = "You can not update this referrer";
                    isUpdate = false;
                }

                if (isUpdate)
                {
                    IList<Referrer> referrerByCode = _referrerService.LoadReferrerByCode(referrer.Code, referrer.Id);
                    int _messageCode = 1;
                    if (referrerByCode.Count == 0)
                    {
                        _messageCode = _referrerService.Update(updateReferrer);

                        if (_messageCode == Constants.MessageSuccess)
                        {
                            ViewBag.SuccessMessage = "Referrer Updated Successfully";
                            // return RedirectToAction("ManageReferrer");
                        }
                        else if (_messageCode == Constants.MessageError)
                        {
                            ViewBag.ErrorMessage = "Referrer Update Fail";
                        }
                        else
                        {
                            ViewBag.InfoMessage = "";
                        }
                        ViewBag.STATUSTEXT = _commonHelper.GetStatus();

                    }
                    else
                    {
                        ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                        ViewBag.ErrorMessage = "Code can not be duplicate";

                    }
                }
                else
                {
                    ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                    ViewBag.ErrorMessage = "You can not update this referrer";
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View(updateReferrer);
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                Referrer deleteReferrer = _referrerService.LoadById(id);
                if (deleteReferrer.IsEditable)
                {
                    deleteReferrer.Status = Referrer.EntityStatus.Delete;
                    _referrerService.Delete(deleteReferrer);
                    return Json(new Response(true, "Referrer Delete Successful !"));
                }
                return Json(new Response(false, "You can not delete this referrer !"));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return Json(new Response(false, "Referrer Delete Fail !"));
        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Referrer referrer = new Referrer();
            try
            {
                referrer = _referrerService.LoadById(id);
                if (referrer != null)
                {
                    referrer.CreateByText = _userService.GetUserNameByAspNetUserId(referrer.CreateBy);
                    referrer.ModifyByText = _userService.GetUserNameByAspNetUserId(referrer.ModifyBy);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }

            if (referrer == null)
            {
                return HttpNotFound();
            }
            return View(referrer);
        }
        #endregion

        #region Rank Operation
        public ActionResult RankChange(int id, int current, string action)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    Referrer referrerUpdateObj = _referrerService.LoadById(Convert.ToInt64(id));
                    if (referrerUpdateObj == null)
                    {
                        return HttpNotFound();
                    }
                    int newRank;
                    if (action == "up")
                        newRank = referrerUpdateObj.Rank - 1;
                    else
                        newRank = referrerUpdateObj.Rank + 1;
                    var referrerOldObj = _referrerService.LoadByRankNextOrPrevious(newRank, action);//LoadByRank(newRank);
                    newRank = referrerOldObj.Rank;
                    referrerOldObj.Rank = referrerUpdateObj.Rank;
                    referrerUpdateObj.Rank = newRank;
                    bool isSuccessfullyUpdateRank = _referrerService.UpdateRank(referrerOldObj, referrerUpdateObj);
                    return Json(isSuccessfullyUpdateRank ? new Response(true, "Successfully changed") : new Response(false, "Something Wrong Occured!!"));
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                }
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return HttpNotFound();
        }
        #endregion

        #region Other Function
        public ActionResult GetReferrer()
        {

            if (Request.IsAjaxRequest())
            {
                try
                {
                    IList<Referrer> referrerList = _referrerService.LoadAll();
                    return PartialView("Partial/_Referrer", referrerList);
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                }
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return HttpNotFound();

        }
        #endregion
    }
}
