using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class MamberAchievementMap : BaseClassMap<MemberAchievement,long> {

        public MamberAchievementMap()
        {
            Table("TM_MamberAchievement");
			LazyLoad();
            References(x => x.TeamMember).Column("TeamMemberId");
            Map(x => x.Year).Column("Year");
            Map(x => x.Month).Column("Month");
            Map(x => x.TotalWorkingHour).Column("TotalWorkingHour");
            Map(x => x.TotalCompletePoint).Column("TotalCompletePoint");
            Map(x => x.CompleteTaskCount).Column("CompleteTaskCount");
            Map(x => x.RunningTaskCount).Column("RunningTaskCount");
            Map(x => x.IsPost).Column("IsPost");

            HasMany(x => x.MemberAchievementDetails).KeyColumn("MemberAchievementId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
