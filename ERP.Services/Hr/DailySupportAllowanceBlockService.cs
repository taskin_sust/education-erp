using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Authentication;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.Hr
{
    public interface IDailyAllowanceBlockService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(List<UserMenu> userMenu, IList<DailySupportAllowanceBlock> entityList, TeamMember currenTeamMember, bool isMentor = false);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<DailySupportAllowanceBlockDto> LoadDailyAllowance(DateTime date, List<UserMenu> userMenu, string pinList, bool isMentor = false, TeamMember currentTeamMember = null);

        IList<DailySupportAllowanceBlockListDto> LoadDailyAllowance(int start, int length, string orderBy, string orderDir
                                                                , List<UserMenu> userMenu, long orgId, long brId, long deptId
                                                                , long capId, int status, string pinStr, DateTime dateFromDateTime
                                                                , DateTime dateToDateTime, bool isMentor, TeamMember currentTeamMember = null);

        #endregion

        #region Others Function

        DateTime? GetTeamMemberDailySupportAllowanceBlockIsPostDateTime(long? teamMemberId = null, int? pin = null);

        int CountDailyAllowance(List<UserMenu> userMenu, long organizationId, long branchId, long deptId,
            long capId, int status, string pinStr, DateTime dateFromDateTime, DateTime dateToDateTime, bool isMentor, TeamMember currentTeamMember = null);

        #endregion

        #region Helper Function

        #endregion


    }
    public class DailySupportAllowanceBlockService : BaseService, IDailyAllowanceBlockService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("PayrollService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly IMentorHistoryDao _hrMentorHistoryDao;
        private readonly IUserProfileDao _userProfileDao;
        private readonly IAttendanceSummaryDao _attendanceSummaryDao;
        private readonly IAttendanceAdjustmentDao _attendanceAdjustmentDao;
        private readonly IDailyAllowanceBlockDao _dailyAllowanceBlockDao;
        private readonly IDailyAllowanceBlockLogDao _dailyAllowanceBlockLogDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;

        public DailySupportAllowanceBlockService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _teamMemberDao = new TeamMemberDao() { Session = session };
            _hrMentorHistoryDao = new MentorHistoryDao() { Session = session };
            _userProfileDao = new UserProfileDao() { Session = session };
            _attendanceSummaryDao = new AttendanceSummaryDao() { Session = session };
            _attendanceAdjustmentDao = new AttendanceAdjustmentDao() { Session = session };
            _dailyAllowanceBlockDao = new DailyAllowanceBlockDao { Session = session };
            _dailyAllowanceBlockLogDao = new DailyAllowanceBlockLogDao { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public void SaveOrUpdate(List<UserMenu> userMenu, IList<DailySupportAllowanceBlock> list, TeamMember currentTeamMember, bool isMentor = false)
        {
            ITransaction trans = null;
            try
            {
                if (userMenu == null || userMenu.Count <= 0)
                {
                    if (!isMentor)
                        throw new ArgumentNullException("userMenu", "Authentication failed");
                }
                if (!list.Any())
                    throw new InvalidDataException("No Team Member found for blocking");
                if (isMentor)
                {
                    if (currentTeamMember == null || currentTeamMember.Id <= 0) throw new InvalidDataException("You don't have Team Member account!");
                    var memberIds = list.Select(x => x.TeamMember.Id).ToList();
                    var authorizedMemberIdList = _hrMentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(currentTeamMember.Id), DateTime.Today.Date);
                    if (memberIds.Except(authorizedMemberIdList).Any())
                        throw new InvalidDataException("You don't have enough permission!");
                }

                var authorizeBranchs = AuthHelper.LoadBranchIdList(userMenu);
                var vaildDailySupportAllowanceBlockList = new List<DailySupportAllowanceBlock>();

                #region check Vaild

                foreach (DailySupportAllowanceBlock row in list)
                {
                    CheckValidation(row);
                    if (row.TeamMember == null)
                        throw new InvalidDataException("Invalid TeamMember found.");
                    if (row.IsBlocked != null && (row.Id <= 0 && !row.IsBlocked.Value))
                    {
                        throw new InvalidDataException("Invalid data submitted for Pin (" + row.TeamMember.Pin + ").");
                    }
                    var memberBranch = _teamMemberDao.GetTeamMemberBranch(row.BlockingDate, row.TeamMember.Id, row.TeamMember.Pin);
                    if (memberBranch == null)
                    {
                        throw new InvalidDataException("Team Member Branch Not Found for Pin (" + row.TeamMember.Pin + ").");
                    }
                    if (!isMentor && !authorizeBranchs.Contains(memberBranch.Id))
                    {
                        throw new InvalidDataException("Permission denied for selected branch for Pin (" + row.TeamMember.Pin + ").");
                    }
                    CheckBeforeSaveUpdate(row);
                    DateTime? postDateTime = GetTeamMemberDailySupportAllowanceBlockIsPostDateTime(row.TeamMember.Id);
                    if (postDateTime != null && postDateTime.Value.Date >= row.BlockingDate.Date)
                        throw new InvalidDataException("TeamMember can only applicable for Daily Support Allowance Block after Date " +
                                                       "(" + postDateTime.Value.ToString("d MMM, yyyy") + ") for Pin (" + row.TeamMember.Pin + ") .");
                    if (row.Id > 0)
                    {
                        DailySupportAllowanceBlock entity = _dailyAllowanceBlockDao.LoadById(row.Id);
                        entity.IsBlocked = row.IsBlocked;
                        entity.Reason = row.Reason;
                        if (row.BusinessId != string.Empty) entity.BusinessId = row.BusinessId;
                        vaildDailySupportAllowanceBlockList.Add(entity);
                    }
                    else
                    {
                        var entity = new DailySupportAllowanceBlock
                        {
                            IsBlocked = row.IsBlocked,
                            Reason = row.Reason,
                            TeamMember = row.TeamMember,
                            BlockingDate = row.BlockingDate,
                            IsPost = false
                        };
                        if (row.BusinessId != string.Empty) entity.BusinessId = row.BusinessId;
                        vaildDailySupportAllowanceBlockList.Add(entity);
                    }
                }
                if (!vaildDailySupportAllowanceBlockList.Any())
                    throw new InvalidDataException("No vaild Team Member found for blocking");

                #endregion

                #region Save

                using (trans = Session.BeginTransaction())
                {
                    foreach (DailySupportAllowanceBlock dailySupportAllowanceBlock in vaildDailySupportAllowanceBlockList)
                    {
                        _dailyAllowanceBlockDao.SaveOrUpdate(dailySupportAllowanceBlock);
                        _dailyAllowanceBlockLogDao.SaveLog(AddToLog(dailySupportAllowanceBlock));
                    }
                    trans.Commit();
                }

                #endregion

                #region old code
                //using (trans = Session.BeginTransaction())
                //{
                //    foreach (var row in list)
                //    {
                //        CheckValidation(row);
                //        if (row.IsBlocked != null && (row.Id <= 0 && !row.IsBlocked.Value))
                //        {
                //            throw new InvalidDataException("Invalid data submitted!");
                //        }
                //        var memberBranch = _teamMemberDao.GetTeamMemberBranch(row.BlockingDate, row.TeamMember.Id, row.TeamMember.Pin);
                //        if (memberBranch == null)
                //        {
                //            throw new InvalidDataException("Team Member Branch Not Found!");
                //        }
                //        if (!authorizeBranchs.Contains(memberBranch.Id))
                //        {
                //            throw new InvalidDataException("Permission denied for selected branch!");
                //        }
                //        CheckBeforeSaveUpdate(row);
                //        if (row.Id > 0)
                //        {
                //            var entity = _dailyAllowanceBlockDao.LoadById(row.Id);
                //            entity.IsBlocked = row.IsBlocked;
                //            entity.Reason = row.Reason;
                //            entity.TeamMember = row.TeamMember;
                //            entity.BlockingDate = row.BlockingDate;
                //            if (!string.IsNullOrWhiteSpace(row.BusinessId))
                //                entity.BusinessId = row.BusinessId;
                //            _dailyAllowanceBlockDao.Update(entity);
                //            logList.Add(AddToLog(entity));
                //        }
                //        else
                //        {
                //            var entity = new DailySupportAllowanceBlock
                //            {
                //                IsBlocked = row.IsBlocked,
                //                Reason = row.Reason,
                //                TeamMember = row.TeamMember,
                //                BlockingDate = row.BlockingDate
                //            };
                //            if (!string.IsNullOrWhiteSpace(row.BusinessId))
                //                entity.BusinessId = row.BusinessId;
                //            _dailyAllowanceBlockDao.Save(entity);
                //            row.Id = entity.Id;
                //            logList.Add(AddToLog(entity));
                //        }
                //    }
                //    foreach (var log in logList)
                //    {
                //        _dailyAllowanceBlockLogDao.SaveLog(log);
                //    }
                //    trans.Commit();
                //}
                #endregion

            }
            catch (AuthenticationException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<DailySupportAllowanceBlockDto> LoadDailyAllowance(DateTime date, List<UserMenu> userMenu, string pinStr, bool isMentor = false, TeamMember currentTeamMember = null)
        {
            IList<DailySupportAllowanceBlockDto> list = null;
            try
            {
                if (!isMentor && (userMenu == null || userMenu.Count <= 0))
                {
                    throw new InvalidDataException("Permission error");
                }

                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu);
                if (!isMentor && organizationIdList.Count <= 0) throw new InvalidDataException("Permission error");
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList);
                if (!isMentor && (branchIdList.Count <= 0 || branchIdList[0] <= 0)) return null;

                var pinList = (!String.IsNullOrEmpty(pinStr)) ? pinStr.Trim() : "";
                var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                pinList = regex.Replace(pinList, @",");
                var memberPinList = pinList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList();
                if (isMentor)
                {
                    if (currentTeamMember == null || currentTeamMember.Id <= 0) throw new InvalidDataException("Permission error");
                    TeamMember mentor = _teamMemberDao.LoadById(currentTeamMember.Id);

                    if (mentor == null)
                    {
                        throw new InvalidDataException("You don't have a \"Team Member\" profile!");
                    }
                    List<int> mentorTeamMemberPinList = _hrMentorHistoryDao.LoadMentorTeamMemberPinList(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now, null, memberPinList).ToList();
                    if (memberPinList.Except(mentorTeamMemberPinList).Any())
                    {
                        throw new InvalidDataException("You don't have enough permission!");
                    }
                    memberPinList = mentorTeamMemberPinList;
                }
                list = _dailyAllowanceBlockDao.LoadDailyAllownceBlockingList(organizationIdList, branchIdList, date, memberPinList);
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return list;
        } 

        public IList<DailySupportAllowanceBlockListDto> LoadDailyAllowance(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long organizationId, long branchId, long deptId,
            long capId, int status, string pinStr, DateTime dateFromDateTime, DateTime dateToDateTime, bool isMentor, TeamMember currentTeamMember = null)
        {
            IList<DailySupportAllowanceBlockListDto> list = null;
            try
            {
                if (!isMentor && (userMenu == null || userMenu.Count <= 0))
                {
                    throw new InvalidDataException("Permission error");
                }

                List<long> authOrgList = organizationId <= 0 ? AuthHelper.LoadOrganizationIdList(userMenu)
                                            : AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                if (!isMentor && authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }

                List<long> bIds = branchId <= 0 ? AuthHelper.LoadBranchIdList(userMenu, authOrgList).ToList()
                                    : AuthHelper.LoadBranchIdList(userMenu, authOrgList, null, new List<long>() { branchId }).ToList();

                if (!isMentor && (bIds.Count <= 0 || bIds[0] <= 0))
                {
                    throw new InvalidDataException("Permission denied for branch!");
                }

                var pinList = (!String.IsNullOrEmpty(pinStr)) ? pinStr.Trim() : "";
                var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                pinList = regex.Replace(pinList, @",");

                var memberPinList = pinList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList();
                if (isMentor)
                {
                    if (currentTeamMember == null || currentTeamMember.Id <= 0) throw new InvalidDataException("You don't have enough permission!");
                    TeamMember mentor = _teamMemberDao.LoadById(currentTeamMember.Id);

                    if (mentor == null)
                    {
                        throw new InvalidDataException("You don't have a \"Team Member\" profile!");
                    }
                    List<int> mentorTeamMemberPinList = _hrMentorHistoryDao.LoadMentorTeamMemberPinList(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now, null, memberPinList).ToList();
                    if (memberPinList.Except(mentorTeamMemberPinList).Any())
                    {
                        throw new InvalidDataException("You don't have enough permission!");
                    }
                    memberPinList = mentorTeamMemberPinList;
                }
                list = _dailyAllowanceBlockDao.LoadDailyAllownceBlockingList(start, length, orderBy, orderDir, authOrgList, bIds, deptId, capId, memberPinList, status, dateFromDateTime, dateToDateTime);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return list;
        }

        #endregion

        #region Others Function

        public int CountDailyAllowance(List<UserMenu> userMenu, long organizationId, long branchId, long deptId,
            long capId, int status, string pinStr, DateTime dateFromDateTime, DateTime dateToDateTime, bool isMentor, TeamMember currentTeamMember = null)
        {
            int count = 0;
            try
            {
                if (!isMentor && (userMenu == null || userMenu.Count <= 0))
                {
                    throw new InvalidDataException("Permission error");
                }

                List<long> authOrgList = organizationId <= 0 ? AuthHelper.LoadOrganizationIdList(userMenu)
                                            : AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                if (!isMentor && authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }

                List<long> bIds = branchId <= 0 ? AuthHelper.LoadBranchIdList(userMenu, authOrgList).ToList()
                                    : AuthHelper.LoadBranchIdList(userMenu, authOrgList, null, new List<long>() { branchId }).ToList();

                if (!isMentor && (bIds.Count <= 0 || bIds[0] <= 0))
                {
                    throw new InvalidDataException("Permission denied for branch!");
                }

                var pinList = (!String.IsNullOrEmpty(pinStr)) ? pinStr.Trim() : "";
                var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                pinList = regex.Replace(pinList, @",");

                var memberPinList = pinList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList();
                if (isMentor)
                {
                    if (currentTeamMember == null)
                    {
                        throw new InvalidDataException("This team member has no user!");
                    }
                    List<int> mentorTeamMemberPinList = _hrMentorHistoryDao.LoadMentorTeamMemberPinList(_commonHelper.ConvertIdToList(currentTeamMember.Id)
                                                                                                        , DateTime.Now, null, memberPinList).ToList();
                    if (memberPinList.Except(mentorTeamMemberPinList).Any())
                    {
                        throw new InvalidDataException("You don't have enough permission!");
                    }
                    memberPinList = mentorTeamMemberPinList;
                }
                count = _dailyAllowanceBlockDao.CountDailyAllownceBlockingList(authOrgList, bIds, deptId, capId, memberPinList, status, dateFromDateTime, dateToDateTime);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return count;
        }

        public DateTime? GetTeamMemberDailySupportAllowanceBlockIsPostDateTime(long? teamMemberId = null, int? pin = null)
        {
            DateTime? postDateTime = null;
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            AllowanceSheet allowanceSheet = _allowanceSheetDao.GetTeamMemberLastIsSubmittedAllowanceSheet(teamMemberId, pin);
            DailySupportAllowanceBlock dailySupportAllowanceBlock = _dailyAllowanceBlockDao.GetTeamMemberLastIsPostDailySupportAllowanceBlock(teamMemberId, pin);

            if (allowanceSheet != null && dailySupportAllowanceBlock != null)
            {
                postDateTime = allowanceSheet.DateTo;
                if (dailySupportAllowanceBlock.BlockingDate > postDateTime)
                    postDateTime = dailySupportAllowanceBlock.BlockingDate;
            }
            else if (allowanceSheet != null)
            {
                postDateTime = allowanceSheet.DateTo;
            }
            else if (dailySupportAllowanceBlock != null)
            {
                postDateTime = dailySupportAllowanceBlock.BlockingDate;
            }
            return postDateTime;
        }

        #endregion

        #region Helper function
        
        private void CheckBeforeSaveUpdate(DailySupportAllowanceBlock entity)
        {
            var att = _attendanceSummaryDao.GetByPinAndDate(entity.TeamMember.Pin, entity.BlockingDate.Date);
            if (att != null) return;
            var adj = _attendanceAdjustmentDao.GetByPinAndDate(entity.TeamMember.Pin, entity.BlockingDate.Date);
            if (adj == null)
            {
                throw new InvalidDataException("Attendance not found as expected date for blocking allowance for Pin (" + entity.TeamMember.Pin + ").");
            }
        }

        private DailySupportAllowanceBlockLog AddToLog(DailySupportAllowanceBlock entity)
        {
            var log = new DailySupportAllowanceBlockLog
            {
                Id = 0,
                CreateBy = GetCurrentUserId(),
                CreationDate = DateTime.Now,
                IsBlocked = entity.IsBlocked != null && entity.IsBlocked.Value,
                Reason = entity.Reason,
                BlockAllowanceDate = entity.BlockingDate,
                BlockAllowance = entity
            };
            if (!string.IsNullOrWhiteSpace(entity.BusinessId))
                log.BusinessId = entity.BusinessId;
            return log;
        }

        private static void CheckValidation(DailySupportAllowanceBlock entity)
        {
            var validationResult = ValidationHelper.ValidateEntity<DailySupportAllowanceBlock, DailySupportAllowanceBlock>(entity);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        #endregion

    }
}

