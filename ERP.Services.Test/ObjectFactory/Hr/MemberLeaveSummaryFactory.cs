﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class MemberLeaveSummaryFactory : ObjectFactoryBase<MemberLeaveSummary>
    {
        private readonly IMembersLeaveSummaryService _membersLeaveSummaryService;
        public MemberLeaveSummaryFactory(IObjectFactoryBase caller, ISession session): base(caller,session)
        {
            _membersLeaveSummaryService = new MembersLeaveSummaryService(session);
        }

        public MemberLeaveSummaryFactory Create()
        {
            Object = new MemberLeaveSummary()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                AvailableBalance = 4,
                TotalLeaveBalance = 16,
                ApprovedLeave = 12,
                PendingLeave = 0,
                CarryForwardLeave = 2,
                CurrentYearLeave = 14,
                EncashLeave = 0,
                VoidLeave = 0,
                Year = DateTime.Now.Year
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLeaveSummaryFactory CreateWith(long id, int availableBalance, int totalLeaveBalance, int approvedLeave, int pendingLeave, int carryForwardLeave,
            int currentYearLeave, int encashLeave, int? voidLeave, int year)
        {
            Object = new MemberLeaveSummary()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                AvailableBalance = availableBalance,
                TotalLeaveBalance = totalLeaveBalance,
                ApprovedLeave = approvedLeave,
                PendingLeave = pendingLeave,
                CarryForwardLeave = carryForwardLeave,
                CurrentYearLeave = currentYearLeave,
                EncashLeave = encashLeave,
                VoidLeave = voidLeave,
                Year = year
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLeaveSummaryFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public MemberLeaveSummaryFactory WithLeave(Leave leave)
        {
            Object.Leave = leave;
            return this;
        }

        public MemberLeaveSummaryFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public void Cleanup()
        {

        }
    }
}
