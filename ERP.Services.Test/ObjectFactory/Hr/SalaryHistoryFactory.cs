﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Type;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class SalaryHistoryFactory : ObjectFactoryBase<SalaryHistory>
    {
        private OrganizationFactory _organizationFactory;
        private CampusFactory _campusFactory;
        private TeamMemberFactory _teamMemberFactory;
        private readonly ISalaryHistoryService _salaryHistoryService;
        private readonly TestBaseService<SalaryHistoryLog> _salaryHistoryLogService;
        private readonly BranchFactory _branchFactory;
        private readonly CampusRoomFactory _campusRoomFactory;
        private readonly DesignationFactory _designationFactory;
        private readonly DepartmentFactory _deaDepartmentFactory;
        public SalaryHistoryFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _organizationFactory = new OrganizationFactory(this, session);
            _campusFactory = new CampusFactory(this, session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _salaryHistoryService = new SalaryHistoryService(session);
            _salaryHistoryLogService = new TestBaseService<SalaryHistoryLog>(session);
            _branchFactory = new BranchFactory(this, session);
            _campusRoomFactory = new CampusRoomFactory(this, session);
            _designationFactory = new DesignationFactory(this,session);
            _deaDepartmentFactory = new DepartmentFactory(this,session);
        }

        #region Create
        public SalaryHistoryFactory Create()
        {
            Object = new SalaryHistory()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Salary = 500,
                BankAmount = 300,
                CashAmount = 200,
                EffectiveDate = DateTime.Now,
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public SalaryHistoryFactory CreateWith(decimal salary, decimal bankAmount, decimal cashAmount, DateTime effectiveDate)
        {
            Object = new SalaryHistory()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Salary = salary,
                BankAmount = bankAmount,
                CashAmount = cashAmount,
                EffectiveDate = effectiveDate,
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public SalaryHistoryFactory WithPurpose(SalaryPurpose salaryPurpose = SalaryPurpose.AdministrativePurpose)
        {
            Object.SalaryPurpose = salaryPurpose;
            return this;
        }

        public SalaryHistoryFactory WithOrganization()
        {
            Object.SalaryOrganization = _organizationFactory.Create().Persist().Object;
            return this;
        }

        public SalaryHistoryFactory WithOrganization(Organization organization)
        {
            Object.SalaryOrganization = organization;
            SingleObjectList.Add(Object);
            return this;
        }

        public SalaryHistoryFactory WithSalaryDesignation(Designation designation)
        {
            Object.SalaryDesignation = designation;
            SingleObjectList.Add(Object);
            return this;
        }

        public SalaryHistoryFactory WithCampus()
        {
            Object.SalaryCampus = _campusFactory.Create().Persist().Object;
            return this;
        }

        public SalaryHistoryFactory WithCampus(Campus campus)
        {
            Object.SalaryCampus = campus;
            SingleObjectList.Add(Object);
            return this;
        }

        public SalaryHistoryFactory WithTeamMember()
        {
            Object.TeamMember = _teamMemberFactory.Create().Persist().Object;
            return this;
        }

        public SalaryHistoryFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            SingleObjectList.Add(Object);
            return this;
        }

        public SalaryHistoryFactory WithDepartment(Department department)
        {
            Object.SalaryDepartment = department;
            SingleObjectList.Add(Object);
            return this;
        }

        public SalaryHistoryFactory WithDesignation(Designation designation)
        {
            Object.SalaryDesignation = designation;
            SingleObjectList.Add(Object);
            return this;
        }

        public IList<SalaryHistory> CreateMore(int no, Department dept, Designation designation, Organization organization)
        {
            for (int index = 0; index < no; index++)
            {
                _branchFactory.Create().WithOrganization(organization).Persist();
                _campusRoomFactory.CreateMore(1, null);
                _campusFactory.Create().WithCampusRoomList(_campusRoomFactory.ObjectList).WithBranch(_branchFactory.Object).Persist();

                var _object = new SalaryHistory()
                {
                    Salary = 2500,
                    BankAmount = 1500,
                    CashAmount = 1000,
                    SalaryCampus = _campusFactory.Object,
                    SalaryDepartment = dept,
                    SalaryDesignation = designation,
                    SalaryOrganization = organization,
                    Status = SalaryHistory.EntityStatus.Active
                };
                ObjectList.Add(_object);
            }
            return ObjectList;
        }

        public SalaryHistoryFactory Persist()
        {
            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    _salaryHistoryService.Save(x);
                }
            });

            ObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    _salaryHistoryService.Save(x);
                }
            });
            return this;
        }

        #endregion

        #region Others

        public void LogCleanUp(SalaryHistoryFactory salaryHistoryFactory)
        {
            if (salaryHistoryFactory.ObjectList.Any())
            {
                DeleteLog(salaryHistoryFactory.ObjectList);
            }
            if (salaryHistoryFactory.SingleObjectList.Any())
            {
                DeleteLog(salaryHistoryFactory.SingleObjectList);
            }
        }

        private void DeleteLog(IEnumerable<SalaryHistory> list)
        {
            foreach (var salaryHistory in list)
            {
                var tableName = GetEntityTableName(typeof(SalaryHistoryLog));
                string swData = "DELETE FROM " + tableName + " WHERE [TeamMemberId] = " + salaryHistory.Id + ";";
                _session.CreateSQLQuery(swData).ExecuteUpdate();
            }
        }

        #endregion

        #region CleanUp

        public void CleanupTeamMemberSalaryHistoryLog(SalaryHistoryFactory salaryHistoryFactory)
        {
            if (salaryHistoryFactory.SingleObjectList.Any())
            {
                var tableName = GetEntityTableName(typeof(SalaryHistoryLog));
                _session.CreateSQLQuery("Delete from " + tableName + " where SalaryHistoryId in(" + string.Join(", ", salaryHistoryFactory.SingleObjectList.Select(x => x.Id).ToList()) + ")").ExecuteUpdate();
            }
            if (salaryHistoryFactory.ObjectList.Any())
            {
                var tableName = GetEntityTableName(typeof(SalaryHistoryLog));
                _session.CreateSQLQuery("Delete from " + tableName + " where SalaryHistoryId in(" + string.Join(", ", salaryHistoryFactory.ObjectList.Select(x => x.Id).ToList()) + ")").ExecuteUpdate();
            }
        }


        public SalaryHistoryFactory CleanUp()
        {
            DeleteAll();
            _campusRoomFactory.CleanUp();
            _campusFactory.Cleanup();
            _designationFactory.Cleanup();
            _deaDepartmentFactory.Cleanup();
            _organizationFactory.Cleanup();
           
            return this;
        }

        #endregion

        public SalaryHistoryFactory CleanUpByTeamMember(long teamMemberId)
        {
            string queryFinal = "delete from HR_SalaryHistory where TeamMemberId in ( " + string.Join(",", teamMemberId) + ")";
            _session.CreateSQLQuery(queryFinal).ExecuteUpdate();
            return this;
        }
    }
}
