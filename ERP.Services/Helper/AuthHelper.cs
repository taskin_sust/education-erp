﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Cache;

namespace UdvashERP.Services.Helper
{
    public static class AuthHelper
    {
        public static List<long> LoadOrganizationIdList(List<UserMenu> userMenuList, List<long> givenOrgIdList = null)
        {
            List<long> returnList;
            if (userMenuList.Any(um => um.Organization == null))
            {
                //if allowAll
                returnList = AuthorizationCache.LoadOrganizationIdList();
            }
            else
            {
                //by get orgIds
                returnList = userMenuList.Select(x => x.Organization.Id)
                    .Distinct()
                    .ToList();
            }

            //filter by given orgIdList
            if (givenOrgIdList != null)
                returnList = returnList.Where(givenOrgIdList.Contains).ToList();

            //return data
            return returnList;
        }

        public static List<long> LoadProgramIdList(List<UserMenu> userMenuList, List<long> orgIdList = null, List<long> branchIdList = null, List<long> givenProgramIdList = null)
        {
            var filterUserMenuList = new List<UserMenu>();
            var filterOrgIdList = LoadOrganizationIdList(userMenuList, orgIdList);

            //if (filterOrgIdList == null && branchIdList == null)
            //{
            //    filterUserMenuList = userMenuList;
            //}
            //else if (filterOrgIdList == null)
            //{
            //    filterUserMenuList = (from um in userMenuList
            //        where um.Branch == null
            //              || branchIdList.Contains(um.Branch.Id)
            //        select um).ToList();
            //}
            //else
            if (branchIdList == null)
            {
                filterUserMenuList = (from um in userMenuList
                                      where um.Organization == null
                                            || filterOrgIdList.Contains(um.Organization.Id)
                                      select um).ToList();
            }
            else
            {
                filterUserMenuList = (from um in userMenuList
                                      where (um.Organization == null
                                             || filterOrgIdList.Contains(um.Organization.Id))
                                            && (um.Branch == null
                                                || branchIdList.Contains(um.Branch.Id))
                                      select um).ToList();
            }


            List<long> returnList;
            if (filterUserMenuList.Any(um => um.Program == null))
            {
                //all allowed (return full list)
                returnList = AuthorizationCache.LoadProgramIdList(filterOrgIdList);
            }
            else
            {
                //return list
                returnList = filterUserMenuList.Select(um => um.Program.Id)
                    .Distinct()
                    .ToList();
            }

            //filter by given givenProgramIdList
            if (givenProgramIdList != null && givenProgramIdList.Count > 0)
                returnList = returnList.Where(givenProgramIdList.Contains).ToList();

            //return data
            return returnList;
        }

        public static List<long> LoadBranchIdList(List<UserMenu> userMenuList, List<long> orgIdList = null, List<long> programIdList = null, List<long> givenBranchIdList = null)
        {
            var filterUserMenuList = new List<UserMenu>();
            var filterOrgIdList = LoadOrganizationIdList(userMenuList, orgIdList);

            //if (filterOrgIdList == null && programIdList == null)
            //{
            //    filterUserMenuList = userMenuList;
            //}
            //else if (filterOrgIdList == null)
            //{
            //    filterUserMenuList = (from um in userMenuList
            //        where um.Program == null
            //              || programIdList.Contains(um.Program.Id)
            //        select um).ToList();
            //}
            //else 
            if (programIdList == null)
            {
                filterUserMenuList = (from um in userMenuList
                                      where um.Organization == null
                                            || filterOrgIdList.Contains(um.Organization.Id)
                                      select um).ToList();
            }
            else
            {
                filterUserMenuList = (from um in userMenuList
                                      where (um.Organization == null
                                             || filterOrgIdList.Contains(um.Organization.Id))
                                            && (um.Program == null
                                                || programIdList.Contains(um.Program.Id))
                                      select um).ToList();
            }


            //return data
            List<long> returnList;
            if (filterUserMenuList.Any(um => um.Branch == null))
            {
                //all allowed (return full list)
                returnList = AuthorizationCache.LoadBranchIdList(filterOrgIdList);
            }
            else
            {
                //return list
                returnList = filterUserMenuList.Select(um => um.Branch.Id)
                    .Distinct()
                    .ToList();
            }

            //filter by given givenBranchIdList
            if (givenBranchIdList != null)
                returnList = returnList.Where(givenBranchIdList.Contains).ToList();

            //return data
            return returnList;
        }


        public static List<ProgramBranchPair> LoadProgramBranchPairList(List<UserMenu> userMenuList, List<long> orgIdList = null, List<long> programIdList = null, List<long> branchIdList = null)
        {
            //filter Organization
            var filterUserMenuList = new List<UserMenu>();
            var filterOrgIdList = LoadOrganizationIdList(userMenuList, orgIdList);

            //filter Program & Branch
            if (programIdList == null && branchIdList == null)
            {
                //do nothing
                filterUserMenuList = userMenuList;
            }
            else if (programIdList == null)
            {
                filterUserMenuList = (from um in userMenuList
                                      where um.Branch == null
                                            || branchIdList.Contains(um.Branch.Id)
                                      select um).ToList();
            }
            else if (branchIdList == null)
            {
                filterUserMenuList = (from um in userMenuList
                                      where um.Program == null
                                            || programIdList.Contains(um.Program.Id)
                                      select um).ToList();
            }
            else
            {
                filterUserMenuList = (from um in userMenuList
                                      where (um.Program == null
                                                || programIdList.Contains(um.Program.Id))
                                            && (um.Branch == null
                                                || branchIdList.Contains(um.Branch.Id))
                                      select um).ToList();
            }

            //get final data
            List<ProgramBranchPair> returnList = new List<ProgramBranchPair>();
            foreach (var orgId in filterOrgIdList)
            {
                var dataByOrgList = filterUserMenuList.Where(um => um.Organization == null || um.Organization.Id == orgId).ToList();
                bool hasAllProgramPermission = dataByOrgList.Any(um => um.Program == null);
                bool hasAllBranchPermission = dataByOrgList.Any(um => um.Branch == null);
                List<long> filterProgramIdList = hasAllProgramPermission ? programIdList
                                                : dataByOrgList.Select(um => um.Program.Id).Distinct().ToList();
                List<long> filterBranchIdList = hasAllBranchPermission ? branchIdList
                                                : dataByOrgList.Select(um => um.Branch.Id).Distinct().ToList();
                var dataList = AuthorizationCache.LoadProgramBranchPairList(new List<long>() { orgId }, filterProgramIdList, filterBranchIdList);

                //if program & branch list is not null
                if (hasAllProgramPermission == false && hasAllBranchPermission == false)
                {
                    dataList = (from first in dataByOrgList
                                join second in dataList
                                    on first.Program.Id equals second.ProgramId
                                where first.Branch.Id == second.BranchId
                                select second).ToList();
                }
                returnList.AddRange(dataList);
            }

            //return data
            return returnList;
        }

    }
}
