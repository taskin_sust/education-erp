﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ExtradayAllowanceSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ExtraDayAllowanceSetting")]
    public class ExtradayAllowanceSettingLoadTest : ExtradayAllowanceSettingBaseTest
    {
        [Fact]
        public void Extra_Day_Load_Test()
        {
            const int count = 3;
            var orgFactory = organizationFactory.CreateMore(2).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[0]).Persist(userMenu);
            var objFactory1 = _extradayAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[1]).Persist(userMenu);
            var list = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, 10, "Organization", "DESC", userMenu, 0
                , new List<AllowanceSettingHolidayType>(), new List<MemberEmploymentStatus>());
            Assert.Equal(list.Count, count * 2);
        }

        [Fact]
        public void Extra_Day_Setting_Count_Test()
        {
            const int count = 3;
            var orgFactory = organizationFactory.CreateMore(2).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[0]).Persist(userMenu);
            var objFactory1 = _extradayAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[1]).Persist(userMenu);
            int counts = _extradayAllowanceSettingService.GetExtradayAllowanceSettingsCount(userMenu, 0
                , new List<AllowanceSettingHolidayType>(), new List<MemberEmploymentStatus>());
            Assert.Equal(counts, count * 2);
        }

        [Fact]
        public void Extra_Day_Settings_Load_By_Id()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]).Persist(userMenu);
            var entity = _extradayAllowanceSettingService.GetExtradayAllowanceSetting(objFactory.Object.Id);
            Assert.NotNull(entity);
            Assert.Equal(entity.Id, objFactory.Object.Id);
        }


        //TODO: Load by page number test.
        //TODO: Load by 1. Organization 2. Employment Status 3. Holiday Type

        [Fact]
        public void Load_ExtraDay_Settings_By_Organization()
        {
            const int count = 3;
            var orgFactory = organizationFactory.CreateMore(2).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[0]).Persist(userMenu);
            var objFactory1 = _extradayAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[1]).Persist(userMenu);
            var list = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, 10, "", "", userMenu, orgFactory.ObjectList[0].Id
                , new List<AllowanceSettingHolidayType>(), new List<MemberEmploymentStatus>());
            Assert.Equal(list.Count, count * 1);
            Assert.True(list.Contains(objFactory.ObjectList[0]));
            Assert.True(list.Contains(objFactory.ObjectList[1]));
            Assert.True(list.Contains(objFactory.ObjectList[2]));
        }

        [Fact]
        public void Load_ExtraDay_Settings_By_Page_Size()
        {
            const int count = 13;
            var orgFactory = organizationFactory.CreateMore(2).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[0]).Persist(userMenu);
            var list = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(10, count, "", "DESC", userMenu, 0
                , new List<AllowanceSettingHolidayType>(), new List<MemberEmploymentStatus>());
            Assert.Equal(list.Count, 3);
            Assert.True(list.Contains(objFactory.ObjectList[10]));
            Assert.True(list.Contains(objFactory.ObjectList[11]));
            Assert.True(list.Contains(objFactory.ObjectList[12]));
        }

        [Fact]
        public void Load_ExtraDay_Settings_By_Holiday_Types()
        {
            const int count = 5;
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[0]);
            for (int i = 0; i < objFactory.ObjectList.Count; i++)
            {
                if (i < 2)
                {
                    objFactory.ObjectList[i].IsGazetted = false;
                }
                else if (i >= 2)
                {
                    objFactory.ObjectList[i].IsManagement = false;
                    objFactory.ObjectList[i].IsWeekend = false;
                }
            }
            objFactory.Persist(userMenu);

            var holidayTypeList1 = new List<AllowanceSettingHolidayType>
            {
                AllowanceSettingHolidayType.Gazetted,
                AllowanceSettingHolidayType.Weekend
            };

            var holidayTypeList2 = new List<AllowanceSettingHolidayType>
            {
                AllowanceSettingHolidayType.Gazetted
            };

            var holidayTypeList3 = new List<AllowanceSettingHolidayType>
            {
                AllowanceSettingHolidayType.Management,
                AllowanceSettingHolidayType.Weekend
            };

            var list1 = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, count, "Organization", "DESC", userMenu, 0
                            , holidayTypeList1, new List<MemberEmploymentStatus>());
            var list2 = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, count, "Organization", "DESC", userMenu, 0
                            , holidayTypeList2, new List<MemberEmploymentStatus>());
            var list3 = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, count, "Organization", "DESC", userMenu, 0
                            , holidayTypeList3, new List<MemberEmploymentStatus>());

            Assert.Equal(list1.Count, 0);
            Assert.Equal(list2.Count, 3);
            Assert.Equal(list3.Count, 2);
        }

        [Fact]
        public void Load_ExtraDay_Settings_By_Employment_Status()
        {
            const int count = 9;
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[0]);
            for (int i = 0; i < objFactory.ObjectList.Count; i++)
            {
                if (i < 3)
                {
                    objFactory.ObjectList[i].IsIntern = false;
                    objFactory.ObjectList[i].IsPartTime = false;
                    objFactory.ObjectList[i].IsContractual = false;
                }
                else if (i >= 3)
                {
                    objFactory.ObjectList[i].IsPermanent = false;
                    objFactory.ObjectList[i].IsProbation = false;
                }
            }
            objFactory.Persist(userMenu);

            var employmentTypeList1 = new List<MemberEmploymentStatus>
            {
                MemberEmploymentStatus.PartTime,
                MemberEmploymentStatus.Permanent
            };

            var employmentTypeList2 = new List<MemberEmploymentStatus>
            {
                MemberEmploymentStatus.Permanent,
                MemberEmploymentStatus.Probation
            };

            var employmentTypeList3 = new List<MemberEmploymentStatus>
            {
                MemberEmploymentStatus.Contractual,
                MemberEmploymentStatus.PartTime,
                MemberEmploymentStatus.Intern
            };

            var list1 = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, count, "Organization", "DESC", userMenu, 0
                            , new List<AllowanceSettingHolidayType>(), employmentTypeList1);
            var list2 = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, count, "Organization", "DESC", userMenu, 0
                            , new List<AllowanceSettingHolidayType>(), employmentTypeList2);
            var list3 = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, count, "Organization", "DESC", userMenu, 0
                            , new List<AllowanceSettingHolidayType>(), employmentTypeList3);

            Assert.Equal(list1.Count, 0);
            Assert.Equal(list2.Count, 3);
            Assert.Equal(list3.Count, 6);
        }
        
        [Fact]
        public void Load_ExtraDay_Settings_By_Employment_And_Holiday_Type()
        {
            const int count = 5;
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[0]);
            for (int i = 0; i < objFactory.ObjectList.Count; i++)
            {
                if (i < 2)
                {
                    objFactory.ObjectList[i].IsIntern = false;
                    objFactory.ObjectList[i].IsPartTime = false;
                    objFactory.ObjectList[i].IsContractual = false;
                    objFactory.ObjectList[i].IsGazetted = false;
                }
                else if (i >= 2)
                {
                    objFactory.ObjectList[i].IsPermanent = false;
                    objFactory.ObjectList[i].IsProbation = false;
                    objFactory.ObjectList[i].IsManagement = false;
                    objFactory.ObjectList[i].IsWeekend = false;
                }
            }
            objFactory.Persist(userMenu);

            var employmentTypeList1 = new List<MemberEmploymentStatus>
            {
                MemberEmploymentStatus.PartTime,
                MemberEmploymentStatus.Permanent
            };

            var employmentTypeList2 = new List<MemberEmploymentStatus>
            {
                MemberEmploymentStatus.Permanent,
                MemberEmploymentStatus.Probation
            };

            var employmentTypeList3 = new List<MemberEmploymentStatus>
            {
                MemberEmploymentStatus.Contractual,
                MemberEmploymentStatus.PartTime,
                MemberEmploymentStatus.Intern
            };

            var holidayTypeList1 = new List<AllowanceSettingHolidayType>
            {
                AllowanceSettingHolidayType.Gazetted,
                AllowanceSettingHolidayType.Weekend
            };
            var holidayTypeList2 = new List<AllowanceSettingHolidayType>
            {
                AllowanceSettingHolidayType.Management,
                AllowanceSettingHolidayType.Weekend
            };
            var holidayTypeList3 = new List<AllowanceSettingHolidayType>
            {
                AllowanceSettingHolidayType.Gazetted
            };

            var list1 = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, count, "Organization", "DESC", userMenu, 0
                            , holidayTypeList1, employmentTypeList1);
            var list2 = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, count, "Organization", "DESC", userMenu, 0
                            , holidayTypeList2, employmentTypeList2);
            var list3 = _extradayAllowanceSettingService.LoadExtradayAllowanceSetting(0, count, "Organization", "DESC", userMenu, 0
                            , holidayTypeList3, employmentTypeList3);

            Assert.Equal(list1.Count, 0);
            Assert.Equal(list2.Count, 2);
            Assert.Equal(list3.Count, 3);
        }
    }
}
