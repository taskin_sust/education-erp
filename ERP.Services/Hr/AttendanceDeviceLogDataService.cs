﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Hr
{
    public interface IAttendanceDeviceLogDataService : IBaseService
    {
        #region Operational Method

        void Save(IList<AttendanceDeviceLogData> logList);
        void Save(IList<AttendanceDeviceLogData> logList, IList<AttendanceDeviceRawData> rowList);

        #endregion
    }
    public class AttendanceDeviceLogDataService : BaseService, IAttendanceDeviceLogDataService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Properties & Object Initialization

        private readonly IAttendanceDeviceLogDataDao _attendanceDeviceLogDataDao;
        private readonly IAttendanceDeviceSynchronizationDao _attendanceDeviceSynchronizationDao;

        public AttendanceDeviceLogDataService(ISession session)
        {
            Session = session;
            _attendanceDeviceLogDataDao = new AttendanceDeviceLogDataDao() { Session = Session };
            _attendanceDeviceSynchronizationDao = new AttendanceDeviceSynchronizationDao() { Session = Session };
        }

        #endregion

        #region Operational Method

        public void Save(IList<AttendanceDeviceLogData> logList)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var attendanceDeviceLogData in logList)
                    {
                        _attendanceDeviceLogDataDao.Save(attendanceDeviceLogData);
                    }
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void Save(IList<AttendanceDeviceLogData> logList, IList<AttendanceDeviceRawData> rawList)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var attendanceDeviceLogData in logList)
                    {
                        _attendanceDeviceLogDataDao.Save(attendanceDeviceLogData);
                    }
                    foreach (var attendanceDevicerawData in rawList)
                    {
                        _attendanceDeviceSynchronizationDao.Save(attendanceDevicerawData);
                    }
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion
    }
}
