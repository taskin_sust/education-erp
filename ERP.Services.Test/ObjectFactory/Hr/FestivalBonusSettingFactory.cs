using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

   public class FestivalBonusSettingFactory : ObjectFactoryBase<FestivalBonusSetting>
   {

       private readonly IFestivalBonusSettingService _prFestivalBonusService;
       internal readonly OrganizationFactory _organizationFactory;
       public FestivalBonusSettingFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
       {
            _prFestivalBonusService = new FestivalBonusSettingService(session);
            _organizationFactory = new OrganizationFactory(null,session);
       }

       #region Create 

       public FestivalBonusSettingFactory Create()
       {

           Object = new FestivalBonusSetting()
           {
               Id = 0,
               BusinessId = GetPcUserName(),
               CreateBy = 1,
               ModifyBy = 1,
               Status = 1,
               Name = Guid.NewGuid().ToString(),
               EffectiveDate = DateTime.Now.AddDays(2),
               ClosingDate =  DateTime.Now.AddDays(4),
               IsIslam = true,
               IsBuddhism = true,
               IsChristianity = true,
               IsHinduism = true,
               IsOthers = true
           };
           SingleObjectList.Add(Object);
           return this;
       }

       public FestivalBonusSettingFactory CreateWith(long id,string name, DateTime effectiveDate, DateTime closingDate, bool isISlam, bool isBuddhism, bool isChristianity,
           bool isHinduism, bool isOthers)
       {

          Object = new FestivalBonusSetting()
          {
              Id = id,
              BusinessId = GetPcUserName(),
              CreateBy = 1,
              ModifyBy = 1,
              Status = 1,
              Name = name,
              EffectiveDate = effectiveDate,
              ClosingDate = closingDate,
              IsIslam = isISlam,
              IsBuddhism = isBuddhism,
              IsChristianity = isChristianity,
              IsHinduism = isHinduism,
              IsOthers = isOthers
          };
          SingleObjectList.Add(Object);
          return this;
       }

       public FestivalBonusSettingFactory CreateMore(int num,Organization organization)
       {
           //_organizationFactory.Create().Persist();
           for (int i = 0; i < num; i++)
           {
               var obj = new FestivalBonusSetting()
               {
                   Id = 0,
                   BusinessId = GetPcUserName(),
                   CreateBy = 1,
                   ModifyBy = 1,
                   Status = 1,
                   Name = Guid.NewGuid()+i.ToString(),
                   EffectiveDate = DateTime.Now.AddDays(2),
                   ClosingDate = DateTime.Now.AddDays(4),
                   IsIslam = true,
                   IsBuddhism = true,
                   IsChristianity = true,
                   IsHinduism = true,
                   IsOthers = true,
                   Organization = organization
               };

               var bcList = Enum.GetValues(typeof(MemberEmploymentStatus)).Cast<int>().ToArray();
               IList<FestivalBonusSettingCalculation> list = bcList.Select(bc => new FestivalBonusSettingCalculation()
               {
                   Id = 0, PrFestivalBonusSetting = obj, Status = FestivalBonusSettingCalculation.EntityStatus.Active, CreateBy = 1, ModifyBy = 1, EmploymentStatus = bc, CalculationOn = 1, BonusPercentage = 10,
               }).ToList();

               obj.PrFestivalBonusCalculation = list;
               ObjectList.Add(obj);
           }
           return this;
       }

       public FestivalBonusSettingFactory WithOrganization()
       {
           _organizationFactory.Create().Persist();
           Object.Organization = _organizationFactory.Object;
           return this;
       }

       public FestivalBonusSettingFactory WithOrganization(Organization organization)
       {
           Object.Organization = organization;
           return this;
       }

       public FestivalBonusSettingFactory WithFestivalBonusSettingCalculation(FestivalBonusSetting festivalBonusSetting)
       {
           IList<FestivalBonusSettingCalculation>  list= new List<FestivalBonusSettingCalculation>();
           var bcList = Enum.GetValues(typeof (MemberEmploymentStatus)).Cast<int>().ToArray();
           foreach (var bc in bcList)
           {
               var fbsc = new FestivalBonusSettingCalculation()
               {
                   Id = 0,
                   PrFestivalBonusSetting = festivalBonusSetting,
                   Status = FestivalBonusSettingCalculation.EntityStatus.Active,
                   CreateBy = 1,
                   ModifyBy = 1,
                   EmploymentStatus = bc,
                   CalculationOn = 1,
                   BonusPercentage = 10,
               };
               list.Add(fbsc);
           }
           Object.PrFestivalBonusCalculation = list;
           return this;
       }

       public FestivalBonusSettingFactory Persist()       
       {

          if (SingleObjectList != null && SingleObjectList.Count > 0)
          {

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _prFestivalBonusService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0)
          {
              ObjectList.ForEach(x =>{if (x.Id == 0)_prFestivalBonusService.SaveOrUpdate(x);});
          }
          return this;
       }
       
       #endregion

       #region other

       #endregion

       #region CleanUp 

       internal void FestivalBonusSettingCalculationCleanUp(FestivalBonusSettingFactory festivalBonusSettingFactory)
       {
           var tableName = GetEntityTableName(typeof(FestivalBonusSettingCalculation));
           if (festivalBonusSettingFactory.ObjectList != null)
           {
               foreach (var obj in festivalBonusSettingFactory.ObjectList)
               {
                   string swData = "DELETE FROM " + tableName + " WHERE [FestivalBonusSettingId] = " + obj.Id + ";";
                   _session.CreateSQLQuery(swData).ExecuteUpdate();
               }
           }
           if (festivalBonusSettingFactory.SingleObjectList != null)
           {
               foreach (var obj in festivalBonusSettingFactory.SingleObjectList)
               {
                   string swData = "DELETE FROM " + tableName + " WHERE [FestivalBonusSettingId] = " + obj.Id + ";";
                   _session.CreateSQLQuery(swData).ExecuteUpdate();
               }
           }
       }

       public FestivalBonusSettingFactory CleanUp()
       {
          DeleteAll();
          _organizationFactory.Cleanup();
          return this;
       }

       #endregion
   }
}
