﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text.pdf.qrcode;
using log4net;
using NHibernate.Criterion;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class FestivalBonusController : Controller
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IFestivalBonusSettingService _festivalBonusService;
        private readonly IUserService _userService;
        private FestivalBonusSetting _model;
        private List<UserMenu> _userMenu;
        public FestivalBonusController()
        {
            var nHSession = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(nHSession);
            _commonHelper = new CommonHelper();
            _festivalBonusService = new FestivalBonusSettingService(nHSession);
            _userService = new UserService(nHSession);
            _model = new FestivalBonusSetting();
        }

        #endregion

        #region Index/Manage

        public ActionResult Index()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.EmploymentStatusList = new SelectList(_commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(), "Key", "Value");
                ViewBag.ReligionList = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value");
                ViewBag.CalculationOnList = new SelectList(_commonHelper.LoadEmumToDictionary<CalculationOn>(), "Key", "Value");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        public JsonResult FestivalBonusList(int draw, int start, int length, string organization, string employmentStatus, string religion, string calculationOn)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            int recordsFiltered = 0;
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "org.ShortName";
                            break;
                        case "1":
                            orderBy = "fb.Name";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion


                recordsTotal = _festivalBonusService.LoadFestivalBonusListCount(_userMenu, organization, employmentStatus, religion, calculationOn);
                recordsFiltered = recordsTotal;
                var festivelist = _festivalBonusService.LoadFestivalBonusList(orderBy, orderDir.ToUpper(), _userMenu, organization, employmentStatus, religion, calculationOn).ToList();
                if (festivelist != null || festivelist.Any())
                {
                    var a = festivelist.Skip(start).Take(length).OrderByDescending(x=>x.EffectiveDate).ToList();
                    foreach (var value in a)
                    {
                        var str = new List<string>();
                        str.Add(value.Organization != null ? value.Organization.ShortName : "");
                        str.Add(value.Name);
                        string religionText = "";
                        if (value.IsIslam) religionText += "Islam , ";
                        if (value.IsHinduism) religionText += "Hindu , ";
                        if (value.IsBuddhism) religionText += "Buddhu , ";
                        if (value.IsChristianity) religionText += "Chiristianity , ";
                        if (value.IsOthers) religionText += "Others , ";
                        var rArray = religionText.Remove(religionText.Length - 2).Split(',').Distinct().ToArray();
                        str.Add(String.Join(",", rArray));

                        string employmentStatusText = "";
                        string calculationOnText = "";
                        foreach (var festivalBonusSettingCalculation in value.PrFestivalBonusCalculation)
                        {
                            employmentStatusText +=
                                _commonHelper.GetEmumIdToValue<MemberEmploymentStatus>(
                                    (int)festivalBonusSettingCalculation.EmploymentStatus) + " ,";

                            calculationOnText += _commonHelper.GetEmumIdToValue<CalculationOn>((int)festivalBonusSettingCalculation.CalculationOn) + " ,";
                        }

                        var emText = employmentStatusText.Remove(employmentStatusText.Length - 1).Split(',').Distinct().ToArray();
                        str.Add(String.Join(",", emText));

                        var coText = calculationOnText.Remove(calculationOnText.Length - 1).Split(',').Distinct().ToArray();
                        str.Add(String.Join(",", coText));

                        str.Add(value.EffectiveDate.ToString("MMMM d, yyyy"));
                        str.Add(value.ClosingDate != null ? value.ClosingDate.Value.ToString("MMMM d, yyyy") : "");
                        string quotStatus = LinkGenerator.GetDetailsLink("Details", "FestivalBonus", value.Id);
                        quotStatus += LinkGenerator.GetEditLink("Edit", "FestivalBonus", value.Id);
                        quotStatus += LinkGenerator.GetDeleteLinkForModal(value.Id, "FestivalBonus");
                        
                        str.Add(quotStatus);
                        getData.Add(str);
                    }
                }
                
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData
            });
        }

        public ActionResult Details(int id)
        {
            var fbViewModel = new FestivalBonusViewModel();
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var fbcViewModelList = new List<FestivalBonusCalculationViewModel>();
            try
            {
                var model = _festivalBonusService.LoadById(id);
                if (model == null)
                    throw new InvalidDataException("Invalid Festival BOnus");

                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu),
                    "Id", "ShortName", model.Organization.Id);

                var calculationOn = _commonHelper.LoadEmumToDictionary<CalculationOn>().ToList();
                ViewBag.CalculationOn = new SelectList(calculationOn.OrderBy(x => x.Key), "Key", "Value");

                fbViewModel.Id = model.Id;
                fbViewModel.OrganizationId = model.Organization.Id;
                fbViewModel.BonusTitle = model.Name;
                fbViewModel.EffectiveDate = model.EffectiveDate;
                fbViewModel.ClosingDate = model.ClosingDate;

                var rList = new List<ReligionViewModel>()
                {
                    new ReligionViewModel() {Religion = (int) Religion.Islam, Name = "Islam", Checked = model.IsIslam},
                    new ReligionViewModel()
                    {
                        Religion = (int) Religion.Hinduism,
                        Name = "Hinduism",
                        Checked = model.IsHinduism
                    },
                    new ReligionViewModel()
                    {
                        Religion = (int) Religion.Christianity,
                        Name = "Christianity",
                        Checked = model.IsChristianity
                    },
                    new ReligionViewModel()
                    {
                        Religion = (int) Religion.Buddhism,
                        Name = "Buddhism",
                        Checked = model.IsBuddhism
                    },
                    new ReligionViewModel() {Religion = (int) Religion.Other, Name = "Others", Checked = model.IsOthers}
                };
                fbViewModel.ReligionViewModels = rList;
                foreach (var fbCalculation in model.PrFestivalBonusCalculation)
                {
                    var fbc = new FestivalBonusCalculationViewModel();
                    fbc.BonusPercentage = fbCalculation.BonusPercentage;
                    fbc.CalculationOn = fbCalculation.CalculationOn;
                    fbc.EmploymentStatus = fbCalculation.EmploymentStatus;
                    fbc.EmploymentStatusName = _commonHelper.GetEmumIdToValue<MemberEmploymentStatus>((int)fbCalculation.EmploymentStatus);
                    fbcViewModelList.Add(fbc);
                }
                fbViewModel.FestivalBonusCalculationViewModels = fbcViewModelList;
                ViewBag.CreateBy =  _userService.GetByAspNetUser(model.CreateBy).AspNetUser.UserName;
                ViewBag.ModifiedBy =  _userService.GetByAspNetUser(model.ModifyBy).AspNetUser.UserName;
                ViewBag.CreateDate = model.CreationDate.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture);
                ViewBag.ModifiedDate = model.ModificationDate.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture);
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View(fbViewModel);
        }

        #endregion

        #region Operational Function

        public ActionResult Create()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            var rList = new List<ReligionViewModel>()
            {
                new ReligionViewModel(){Religion = (int)Religion.Islam,Name = "Islam",Checked = false},
                new ReligionViewModel(){Religion = (int)Religion.Hinduism,Name = "Hinduism",Checked = false},
                new ReligionViewModel(){Religion = (int)Religion.Christianity,Name = "Christianity",Checked = false},
                new ReligionViewModel(){Religion = (int)Religion.Buddhism,Name = "Buddhism",Checked = false},
                new ReligionViewModel(){Religion = (int)Religion.Other,Name = "Others",Checked = false}
            };

            var bcList = new List<FestivalBonusCalculationViewModel>()
            {
                new FestivalBonusCalculationViewModel(){EmploymentStatusName = "Probation",EmploymentStatus = (int)MemberEmploymentStatus.Probation},
                new FestivalBonusCalculationViewModel(){EmploymentStatusName = "Permanent",EmploymentStatus = (int)MemberEmploymentStatus.Permanent},
                new FestivalBonusCalculationViewModel(){EmploymentStatusName = "Part Time",EmploymentStatus = (int)MemberEmploymentStatus.PartTime},
                new FestivalBonusCalculationViewModel(){EmploymentStatusName = "Contractual",EmploymentStatus = (int)MemberEmploymentStatus.Contractual},
                new FestivalBonusCalculationViewModel(){EmploymentStatusName = "Intern",EmploymentStatus = (int)MemberEmploymentStatus.Intern},
            };

            var calculationOn = _commonHelper.LoadEmumToDictionary<CalculationOn>().ToList();
            ViewBag.CalculationOn = new SelectList(calculationOn.OrderBy(x => x.Key), "Key", "Value");

            ViewBag.BcList = bcList;
            ViewBag.ReligionViewModels = rList;
            return View();
        }

        [HttpPost]
        public ActionResult Create(FestivalBonusViewModel model)
        {
            string errorMessage = "";
            try
            {
                model.Id = 0;
                if (!ModelState.IsValid)
                {
                    string validationErrors = string.Join(",", this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors).GroupBy(x => x.ErrorMessage).Select(x => x.Key));
                    return Json(new Response(false, validationErrors));
                }
                else
                {
                    var fb = new FestivalBonusSetting();
                    fb.Name = model.BonusTitle;
                    
                    fb.Organization = _organizationService.LoadById(model.OrganizationId);
                    var religionViewModel = model.ReligionViewModels.FirstOrDefault(x => x.Religion == (int)Religion.Islam);
                    if (religionViewModel != null)
                        fb.IsIslam = religionViewModel.Checked;

                    var firstOrDefault = model.ReligionViewModels.FirstOrDefault(x => x.Religion == (int)Religion.Hinduism);
                    if (firstOrDefault != null)
                        fb.IsHinduism = firstOrDefault.Checked;

                    var orDefault = model.ReligionViewModels.FirstOrDefault(x => x.Religion == (int)Religion.Christianity);
                    if (orDefault != null)
                        fb.IsChristianity = orDefault.Checked;

                    var viewModel = model.ReligionViewModels.FirstOrDefault(x => x.Religion == (int)Religion.Buddhism);
                    if (viewModel != null)
                        fb.IsBuddhism = viewModel.Checked;

                    var @default = model.ReligionViewModels.FirstOrDefault(x => x.Religion == (int)Religion.Other);
                    if (@default != null)
                        fb.IsOthers = @default.Checked;

                    fb.EffectiveDate = model.EffectiveDate;
                    fb.ClosingDate = model.ClosingDate;

                    var fbcList = new List<FestivalBonusSettingCalculation>();
                    foreach (var fbcModel in model.FestivalBonusCalculationViewModels)
                    {
                        var fbc = new FestivalBonusSettingCalculation()
                        {
                            PrFestivalBonusSetting = fb,
                            EmploymentStatus = (int)fbcModel.EmploymentStatus,
                            CalculationOn = (int)fbcModel.CalculationOn,
                            BonusPercentage = (decimal)fbcModel.BonusPercentage,
                            CreateBy = _festivalBonusService.GetCurrentUserId()

                        };
                        fbcList.Add(fbc);
                    }
                    fb.PrFestivalBonusCalculation = fbcList;

                    _festivalBonusService.SaveOrUpdate(fb);
                    return Json(new { isSuccess = true, returnMessage = "Festival Bonus submit successfully." });
                }
            }
            catch (NullObjectException ex)
            {
                errorMessage = ex.Message;
            }
            catch (MessageException ex)
            {
                errorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                errorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                errorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
            return Json(new Response(false, errorMessage));
        }

        public ActionResult Edit(int id)
        {
            var fbViewModel = new FestivalBonusViewModel();
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var fbcViewModelList = new List<FestivalBonusCalculationViewModel>();
            try
            {
                var model = _festivalBonusService.LoadById(id);
                if (model == null)
                    throw new InvalidDataException("Invalid Festival BOnus");

                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu),
                    "Id", "ShortName", model.Organization.Id);

                var calculationOn = _commonHelper.LoadEmumToDictionary<CalculationOn>().ToList();
                ViewBag.CalculationOn = new SelectList(calculationOn.OrderBy(x => x.Key), "Key", "Value");

                fbViewModel.Id = model.Id;
                fbViewModel.OrganizationId = model.Organization.Id;
                fbViewModel.BonusTitle = model.Name;
                fbViewModel.EffectiveDate = model.EffectiveDate;
                fbViewModel.ClosingDate = model.ClosingDate;

                var rList = new List<ReligionViewModel>()
                {
                    new ReligionViewModel() {Religion = (int) Religion.Islam, Name = "Islam", Checked = model.IsIslam},
                    new ReligionViewModel()
                    {
                        Religion = (int) Religion.Hinduism,
                        Name = "Hinduism",
                        Checked = model.IsHinduism
                    },
                    new ReligionViewModel()
                    {
                        Religion = (int) Religion.Christianity,
                        Name = "Christianity",
                        Checked = model.IsChristianity
                    },
                    new ReligionViewModel()
                    {
                        Religion = (int) Religion.Buddhism,
                        Name = "Buddhism",
                        Checked = model.IsBuddhism
                    },
                    new ReligionViewModel() {Religion = (int) Religion.Other, Name = "Others", Checked = model.IsOthers}
                };
                fbViewModel.ReligionViewModels = rList;
                foreach (var fbCalculation in model.PrFestivalBonusCalculation)
                {
                    var fbc = new FestivalBonusCalculationViewModel();
                    fbc.BonusPercentage = fbCalculation.BonusPercentage;
                    fbc.CalculationOn = fbCalculation.CalculationOn;
                    fbc.EmploymentStatus = fbCalculation.EmploymentStatus;
                    fbc.EmploymentStatusName = _commonHelper.GetEmumIdToValue<MemberEmploymentStatus>((int)fbCalculation.EmploymentStatus);
                    fbcViewModelList.Add(fbc);
                }
                fbViewModel.FestivalBonusCalculationViewModels = fbcViewModelList;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View(fbViewModel);
        }

        [HttpPost]
        public ActionResult Edit(FestivalBonusViewModel model)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var fb = new FestivalBonusSetting();
                var checkModel = _festivalBonusService.LoadById(model.Id);
                if (checkModel == null)
                    return Json(new Response(false, "Invalid festival bonus"));
                else if (!ModelState.IsValid)
                {
                    string validationErrors = string.Join(",",
                        this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors)
                            .GroupBy(x => x.ErrorMessage)
                            .Select(x => x.Key));
                    return Json(new Response(false, validationErrors));
                }
                else
                {
                    fb.Id = model.Id;
                    fb.Name = model.BonusTitle;
                    fb.Organization = _organizationService.LoadById(model.OrganizationId);
                    var religionViewModel = model.ReligionViewModels.FirstOrDefault(x => x.Religion == (int)Religion.Islam);
                    if (religionViewModel != null)
                        fb.IsIslam = religionViewModel.Checked;

                    var firstOrDefault = model.ReligionViewModels.FirstOrDefault(x => x.Religion == (int)Religion.Hinduism);
                    if (firstOrDefault != null)
                        fb.IsHinduism = firstOrDefault.Checked;

                    var orDefault = model.ReligionViewModels.FirstOrDefault(x => x.Religion == (int)Religion.Christianity);
                    if (orDefault != null)
                        fb.IsChristianity = orDefault.Checked;

                    var viewModel = model.ReligionViewModels.FirstOrDefault(x => x.Religion == (int)Religion.Buddhism);
                    if (viewModel != null)
                        fb.IsBuddhism = viewModel.Checked;

                    var @default = model.ReligionViewModels.FirstOrDefault(x => x.Religion == (int)Religion.Other);
                    if (@default != null)
                        fb.IsOthers = @default.Checked;

                    fb.EffectiveDate = model.EffectiveDate;
                    fb.ClosingDate = model.ClosingDate;

                    var fbcList = new List<FestivalBonusSettingCalculation>();
                    foreach (var fbcModel in model.FestivalBonusCalculationViewModels)
                    {
                        var fbc = new FestivalBonusSettingCalculation()
                        {
                            PrFestivalBonusSetting = fb,
                            EmploymentStatus = (int)fbcModel.EmploymentStatus,
                            CalculationOn = (int)fbcModel.CalculationOn,
                            BonusPercentage = (decimal)fbcModel.BonusPercentage

                        };
                        fbcList.Add(fbc);
                    }
                    fb.PrFestivalBonusCalculation = fbcList;
                    _festivalBonusService.SaveOrUpdate(fb);
                    return Json(new { isSuccess = true, returnMessage = "Festival Bonus update successfully." });
                }
            }
            catch (NullObjectException ex) { return Json(new Response(false, ex.Message)); }

            catch (MessageException ex) { return Json(new Response(false, ex.Message)); }

            catch (EmptyFieldException ex) { return Json(new Response(false, ex.Message)); }

            catch (DuplicateEntryException ex) { return Json(new Response(false, ex.Message)); }

            catch (InvalidDataException ex) { return Json(new Response(false, ex.Message)); }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            string message = "";
            try
            {
                bool isDeleted = _festivalBonusService.IsDelete(id);
                if (isDeleted)
                {
                    return Json(new Response(true, "Festival bonus sucessfully deleted."));
                }
                message = "Problem Occurred. Retry";
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (MessageException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new Response(false, message));
        }

        #endregion

        #region Others function

        [HttpPost]
        public JsonResult DoesFbTitleExist(string name, long organizationId, long? id, DateTime effectiveDate, DateTime? closingDate)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (id != null && id>0)
                    {
                        bool check = _festivalBonusService.HasDuplicateByFbTitle(name, organizationId, id, effectiveDate, closingDate);
                        return Json(check);
                    }
                    else
                    {
                        bool check = _festivalBonusService.HasDuplicateByFbTitle(name, organizationId, null, effectiveDate, closingDate);
                        return Json(check);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            else { return Json(false); }
        }

        #endregion

        #region Helper function

        public JsonResult LoadFestivalBonusSettings(List<long> organizationIds,int bonusMonth,int bonusYear, bool isAuthorized = false)
        {
            try
            {
                DateTime startDate = new DateTime(bonusYear, bonusMonth, 1);
                var userMenu = (List<UserMenu>) ViewBag.UserMenu;
                IList<FestivalBonusSetting> festivalBonusSettingsList = (isAuthorized)
                    ? _festivalBonusService.LoadAuthorizedFestivalBonusSettings(userMenu, organizationIds, startDate)
                    : _festivalBonusService.LoadFestivalBonusSettings(organizationIds, startDate);
                if (festivalBonusSettingsList != null && festivalBonusSettingsList.Any())
                {
                    var festivalBonusSettingsSelectList = new SelectList(festivalBonusSettingsList.ToList(), "Id",
                        "Name");
                    return Json(new {festivalBonusSettingsList = festivalBonusSettingsSelectList, IsSuccess = true});
                }
                else
                {
                    return Json(new {festivalBonusSettingsList = "", IsSuccess = false});
                }
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, ViewBag.ErrorMessage));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

    }
}
