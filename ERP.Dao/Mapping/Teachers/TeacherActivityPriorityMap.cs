﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherActivityPriorityMap : BaseClassMap<TeacherActivityPriority, long>
    {
        public TeacherActivityPriorityMap()
        {
            Table("TeacherActivityPriority");
            LazyLoad();
            //Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Priority);
            References(x => x.Teacher).Column("TeacherId");
            References(x => x.TeacherActivity).Column("TeacherActivityId");
        }
    }
}