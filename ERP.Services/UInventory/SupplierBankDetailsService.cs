using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
public interface ISupplierBankDetailService : IBaseService
{
    #region Operational Function

    bool SaveOrUpdate(SupplierBankDetails supplierBankDetail);
    void Delete(SupplierBankDetails deleteSupplier);

    #endregion

    #region Single Instances Loading Function
    
    SupplierBankDetails LoadById(long id);

    #endregion

    #region List Loading Function

    IList<SupplierBankDetails> LoadAll();

    #endregion

    #region Others Function

    #endregion

    #region Helper Function

    #endregion
}
public class SupplierBankDetailsService : BaseService, ISupplierBankDetailService {
    #region Logger
    private readonly ILog _logger = LogManager.GetLogger("InventoryService");
    #endregion

    #region Propertise & Object Initialization
    private readonly CommonHelper _commonHelper;
    private readonly ISupplierBankDetailsDao _supplierBankDetailDao;
    public SupplierBankDetailsService(ISession session)
    {
        Session = session;
        _commonHelper = new CommonHelper();
        _supplierBankDetailDao=new SupplierBankDetailsDao{Session = session};}
    #endregion

    #region Operational Functions

    public bool SaveOrUpdate(SupplierBankDetails supplierBankDetail)
    {
        return true;
    }

    public void Delete(SupplierBankDetails deleteSupplier)
    {
        ITransaction trans = null;
        try
        {
            if (deleteSupplier == null)
                throw new NullObjectException("Supplier Bank Details Not Found!");
            using (trans = Session.BeginTransaction())
            {
                _supplierBankDetailDao.Delete(deleteSupplier);
                trans.Commit();
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
            throw;
        }
        finally
        {
            if (trans != null && trans.IsActive)
                trans.Rollback();
        }
    }

    #endregion

    #region Single Instances Loading Function

    public SupplierBankDetails LoadById(long id)
    {
        try
        {
            return _supplierBankDetailDao.LoadById(id);
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
            throw;
        }
    }

    public IList<SupplierBankDetails> LoadAll()
    {
        var list = new List<SupplierBankDetails>();
        try
        {
            list = _supplierBankDetailDao.LoadAll().ToList();
            return list;
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
        return list;
    }

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function


    #endregion

    #region Helper function



    #endregion
}
}
                                    
