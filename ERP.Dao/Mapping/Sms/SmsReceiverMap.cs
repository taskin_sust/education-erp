﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Mapping.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public class SmsReceiverMap : BaseClassMap<SmsReceiver, long>
    {
        public SmsReceiverMap()
        {
            Table("SmsReceiver");
            LazyLoad();

            Map(x => x.Rank);
            Map(x => x.Name);

            //HasManyToMany(x => x.SmsSettingses).Table("SmsReceiverSettings").Cascade.AllDeleteOrphan(); 
            HasManyToMany(x => x.SmsSettingses).Cascade.All().Table("SmsReceiverSettings");

            HasMany(x => x.SmsHistories).KeyColumn("SmsReceiverId").Cascade.AllDeleteOrphan().Inverse().Where(ss => ss.Status != SmsHistory.EntityStatus.Delete).OrderBy("Rank");
            HasMany(x => x.SmsStudentRegistrationReceiverList).KeyColumn("SmsReceiverId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
