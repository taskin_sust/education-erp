﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using Microsoft.AspNet.Identity;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.BusinessRules.Student;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class CancelCourseController : Controller
    {
        private readonly ILog _logger = LogManager.GetLogger("StudentArea");

        #region Objects/Propertise/Services/Dao & Initialization

        //private ILog logger = LogManager.GetLogger("CancelCourseController");
        private readonly IBranchService _branchService;
        private readonly ICourseService _courseService;
        private readonly IStudentService _studentService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IStudentPaymentService _studentPaymentService;
        private readonly IStudentCourseDetailsService _studentCourseDetailsService;
        private readonly IDiscountService _discountService;
        private readonly ICommonHelper _commonHelper;
        private readonly IReferenceService _referenceService;
        private readonly ICourseSubjectService _courseSubjectService;
        private readonly IStudentClassAttendanceService _studentClassAttendanceService;
        private IDiscountDetailService _discountDetailService;
        private readonly IStudentExamAttendanceService _studentExamAttendanceService;
        private List<UserMenu> _userMenu;
        public CancelCourseController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _branchService = new BranchService(session);
                _courseService = new CourseService(session);
                _studentService = new StudentService(session);
                _studentProgramService = new StudentProgramService(session);
                _studentPaymentService = new StudentPaymentService(session);
                _studentCourseDetailsService = new StudentCourseDetailsService(session);
                _discountService = new DiscountService(session);
                _commonHelper = new CommonHelper();
                _referenceService = new ReferenceService(session);
                _courseSubjectService = new CourseSubjectService(session);
                _studentClassAttendanceService = new StudentClassAttendanceService(session);
                _discountDetailService = new DiscountDetailService(session);
                _studentExamAttendanceService = new StudentExamAttendanceService(session);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        #endregion

        #region Operational Function
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult CancelCourseOrSubject(string message = "", string inValidAmount = "")
        {
            if (!String.IsNullOrEmpty(message))
            {
                ViewBag.ErrorMessage = message;
                ViewBag.InvalidAmount = inValidAmount;
            }
            return View();
        }
        [HttpGet]
        public ActionResult CancelCourse(string stdProRoll)
        {
            try
            {
                decimal rreceivableAmountWithoutDiscount = 0;
                //decimal recavailableAmount = 0;
                if (String.IsNullOrEmpty(stdProRoll))
                    return RedirectToAction("CancelCourseOrSubject", new { message = "Invalid Program Roll", inValidAmount = stdProRoll });
                SelectList PaymentM;
                var paymentdetail = new PaymentDetails();
                var studentProgram = _studentProgramService.GetStudentProgram(stdProRoll.Trim());
                /*Authorization check */
                //StudentProgram studentProgram = _studentProgramService.LoadByPrnNo(prnumber);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                //check authorization
                var message = _studentProgramService.CheckAuthorizationForStudentProgram(_userMenu, studentProgram);
                if (message != "")
                    return RedirectToAction("CancelCourseOrSubject", new { message = message, inValidAmount = stdProRoll });

                if (studentProgram == null)
                {
                    return RedirectToAction("CancelCourseOrSubject", new { message = "Invalid Program Roll", inValidAmount = stdProRoll });
                }

                var studentViewModel = new StudentViewModel();
                StudentPayment lastStudentPaymentTransication = _studentPaymentService.LoadByStudentProgram(studentProgram);
                /*Basic Information of student and their assigned courses and corresponding subject fees*/
                if (true)
                {
                    var student = _studentService.LoadById(studentProgram.Student.Id);
                    studentViewModel.Name = student.NickName;
                    studentViewModel.MobNumber = student.Mobile;
                    studentViewModel.Program = studentProgram.Program.Name;
                    studentViewModel.BatchDays = studentProgram.Batch.Days;
                    studentViewModel.BatchTime = studentProgram.Batch.FormatedBatchTime;
                    studentViewModel.Batch = studentProgram.Batch.Name;
                    studentViewModel.Id = studentProgram.Id;
                    var branch = _branchService.GetBranch(studentProgram.Batch.Branch.Id);
                    studentViewModel.Branch = branch.Name;
                    studentViewModel.Session = studentProgram.Batch.Session.Name;
                    studentViewModel.Campus = studentProgram.Batch.Campus.Name;
                    studentViewModel.VersionOfStudy = studentProgram.VersionOfStudy;
                    studentViewModel.Gender = student.Gender;
                    studentViewModel.Religion = student.Religion;
                    /*Student Course Assigned View */
                    var studentCourseDetailsList =
                        _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);
                    var query = studentCourseDetailsList.GroupBy(x => x.CourseSubject.Course,
                        (key, g) => new { Course = key, Subjects = g.ToList() });

                    var courseList = query.ToList();
                    foreach (var courseAndCorrespondingSubjectList in courseList)
                    {
                        var courseViewModel = new CourseViewModel
                        {
                            Id = courseAndCorrespondingSubjectList.Course.Id,
                            Name = courseAndCorrespondingSubjectList.Course.Name
                        };
                        var courseSubjectList = courseAndCorrespondingSubjectList.Subjects;
                        foreach (var courseSubject in courseSubjectList)
                        {
                            var subjectViewModel = new SubjectViewModel();
                            //var subject = courseSubject.CourseSubject;
                            subjectViewModel.Name = courseSubject.CourseSubject.Subject.Name;
                            subjectViewModel.Payment = courseSubject.CourseSubject.Payment;
                            subjectViewModel.Id = courseSubject.CourseSubject.Subject.Id;
                            rreceivableAmountWithoutDiscount += Convert.ToDecimal(subjectViewModel.Payment);
                            courseViewModel.SubjectViewModels.Add(subjectViewModel);
                        }
                        studentViewModel.CourseViewModels.Add(courseViewModel);
                    }
                    if (lastStudentPaymentTransication == null)
                    {
                        return RedirectToAction("CancelCourseOrSubject", new { message = "Invalid Student !! You have not paid a single penny yet!! First pay your due amount", inValidAmount = stdProRoll });
                    }
                    if (studentProgram.DueAmount != null) paymentdetail.DueAmount = (decimal)studentProgram.DueAmount;
                    paymentdetail.CancellationAmount = 0;
                    paymentdetail.CashBack = 0;
                    var damount = _studentPaymentService.GetByStudentProgram(studentProgram);

                    var sum = damount.Sum(x => x.TotalDiscountAmount);
                    if (sum > 0)
                        paymentdetail.DiscountAmount = (decimal)sum;
                }
                //else
                //{
                //    return RedirectToAction("CancelCourseOrSubject", new { message = "Invalid Program Roll", inValidAmount = stdProRoll });
                //}
                //PaymentM = new SelectList(_commonHelper.GetPaymentMethods(), "Key", "Value");
                //var paymentMethodList = (from KeyValuePair<string, int> statusKeyValuePair in PaymentM.Items
                //                         select new SelectListItem()
                //                         {
                //                             Text = statusKeyValuePair.Key,
                //                             Value = statusKeyValuePair.Value.ToString()
                //                         }).ToList();
                var refList = new SelectList(_referenceService.GetAllRefferer(), "Id", "Name");
                ViewBag.REFERRERLIST = refList;
                //ViewBag.PAYMENTMETHODLIST = paymentMethodList;
                ViewBag.PAYMENTDETAIL = paymentdetail;

                ViewData["stdPId"] = stdProRoll;
                return View(studentViewModel);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return RedirectToAction("CancelCourseOrSubject", new { message = "Invalid Program Roll", inValidAmount = stdProRoll });

            }
        }
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function


        #endregion

        #region Others Function
        [HttpPost]
        public JsonResult HistoryOfCancellingCourse(string stdProgramRoll, string courseId, string courseName)
        {
            if (String.IsNullOrEmpty(stdProgramRoll) || String.IsNullOrEmpty(courseId) ||
                String.IsNullOrEmpty(courseName)) return Json(new Response(false, "Failed"));

            var courseHistory = new CancellingCourseHistory();
            var stdProRoll = stdProgramRoll.Trim();
            StudentProgram studentProgram = _studentProgramService.GetStudentProgram(stdProRoll);
            if (studentProgram == null)
            {
                return Json(new Response(false, "Failed"));
            }
            var studentCourseDetails = studentProgram.StudentCourseDetails;
            var query = studentCourseDetails.GroupBy(x => x.CourseSubject.Course,
                (key, g) => new { Course = key, Subjects = g.ToList() });

            var courseList = query.ToList();
            var course = courseList.SingleOrDefault(x => x.Course.Id == Convert.ToInt64(courseId));
            if (course != null) courseHistory.CourseName = course.Course.Name;
            courseHistory.TakenMaterialName = studentProgram.Materials;
            IList<SubjectList> sublist = new List<SubjectList>();
            if (course != null)
                foreach (var subject in course.Subjects)
                {
                    var subObj = new SubjectList();
                    subObj.SubjectName = subject.CourseSubject.Subject.Name;
                    subObj.ClassAttendance = _studentClassAttendanceService.GetCountOfTakenClass(subject.CourseSubject.Subject.Id, studentProgram.Id, course.Course.Id);
                    subObj.ExamAttendance = _studentExamAttendanceService.GetCountOfAttendExam(subject.CourseSubject.Subject.Id, studentProgram.Id, course.Course.Id);
                    sublist.Add(subObj);
                }
            courseHistory.SubjectList = sublist;
            string cHistory = this.RenderRazorViewToString("Partial/_CourseHistory", courseHistory);

            return Json(new Response(true, cHistory));
        }
        [HttpPost]
        public ActionResult CourseCanceledAndPaymentCalculation(string studentObj, string stdPId, string dueAmount, string cancleAmount, string considerAmount, string cashBack, string currentDue, string nextDate, string referrer, string referrerenceNote)
        {
            try
            {
                decimal totalCanceledCourseFees = 0;
                var courseAndSubList = new List<Dictionary<long, long>>();
                if (String.IsNullOrEmpty(stdPId))
                    return Json(new Response(false, "Course Cancel Failed(Possible Cause: Invalid Student Program Roll) "));

                SelectList paymentM;
                stdPId = stdPId.Trim();
                var studentCourseDetailList = new List<StudentCourseDetail>();
                var jjScriptSerializer = new JavaScriptSerializer();
                var studentViewModel = jjScriptSerializer.Deserialize<StudentViewModel>(studentObj);
                var studentProgram = _studentProgramService.GetStudentProgram(stdPId);
                foreach (var course in studentViewModel.CourseViewModels)
                {
                    foreach (var courseSubject in course.SubjectViewModels)
                    {
                        var dictionary = new Dictionary<long, long> { { course.Id, courseSubject.Id } };
                        courseAndSubList.Add(dictionary);
                        var studentCourseDetailObj = new StudentCourseDetail();
                        var courseSubjectObj = _courseSubjectService.LoadCourseSubject(course.Id, courseSubject.Id);
                        studentCourseDetailObj.CourseSubject = courseSubjectObj;
                        studentCourseDetailObj.StudentProgram = studentProgram;
                        totalCanceledCourseFees += Convert.ToDecimal(courseSubject.Payment);
                        studentCourseDetailList.Add(studentCourseDetailObj);
                    }
                }
                if (String.IsNullOrEmpty(cancleAmount) || String.IsNullOrEmpty(considerAmount))
                    return Json(new Response(false, "Course Cancel Failed(Possible Cause: You have Not Canceled Any Course Or Subject) "));
                var canceledAmount = Convert.ToDecimal(cancleAmount.Trim());

                if (String.IsNullOrEmpty(considerAmount) || String.IsNullOrEmpty(considerAmount))
                    return Json(new Response(false, "Course Cancel Failed(Possible Cause: Consider Amount is less then minimum value of 1 taka) "));
                var considerationAmount = Convert.ToDecimal(considerAmount.Trim());
                if (considerationAmount <= 0)
                    return Json(new Response(false, "Course Cancel Failed(Possible Cause: Consider Amount is less then minimum value of 1 taka) "));

                var dueFlag = 0;
                if (canceledAmount <= 0)
                    return Json(new Response(false, "Course Cancel Failed(Possible Cause: You have Not Canceled Any Course Or Subject) "));

                if (canceledAmount != totalCanceledCourseFees)
                    return Json(new Response(false, "Course Cancel Failed(Possible Cause: Manually Data Tempered Into Cancelled Amount Field !! Serious Crime) "));

                IList<StudentCourseDetail> studentCourseDetails = new List<StudentCourseDetail>();
                if (String.IsNullOrEmpty(dueAmount))
                    return Json(new Response(false, "Course Cancel Failed(Possible Cause: Input Field Value Missing) "));

                var dueAmountComingFromClientSide = Convert.ToDecimal(dueAmount.Trim());
                if (dueAmountComingFromClientSide != studentProgram.DueAmount)
                    return Json(new Response(false, "Course Cancel Failed(Possible Cause: Manually Data Tempered Into Due Amount Field !! Serious Crime) "));
                var studentPayment = new StudentPayment();
                if (!(studentProgram.DueAmount > 0))
                {
                    studentPayment.NextReceivedDate = DateTime.MaxValue;
                }
                else if (studentProgram.DueAmount <= considerationAmount) { studentPayment.NextReceivedDate = DateTime.MaxValue; }
                else
                {
                    if (!String.IsNullOrEmpty(nextDate))
                    {
                        if (Convert.ToDateTime(nextDate) >= DateTime.Now.Date)
                        {
                            studentPayment.NextReceivedDate = Convert.ToDateTime(nextDate);
                        }
                        else
                        {
                            return Json(new Response(false, "Next Payment Date Must Be Higher Than Or Equal Today's Date"));
                        }
                    }
                    else
                    {
                        return Json(new Response(false, "Next Date Field Missing Or Invalid"));
                    }
                }

                if (considerationAmount > totalCanceledCourseFees)
                    return Json(new Response(false, "Course Cancel Failed(Possible Cause: Consideration Amount Can't Be Higher Than Your Cancellation Amount) "));
                studentPayment.ConsiderationAmount = considerationAmount;
                if (!(studentProgram.DueAmount - considerationAmount < 0))
                {
                    studentPayment.CashBackAmount = 0;
                    if (!String.IsNullOrEmpty(currentDue) && Convert.ToDecimal(currentDue.Trim()) == studentProgram.DueAmount - considerationAmount)
                    {
                        /*current due amount*/
                        studentPayment.PayableAmount = studentProgram.DueAmount;
                    }
                    else
                    {
                        return Json(new Response(false, "Data Tempering Found For Current Due Amount Field"));
                    }
                    studentPayment.DueAmount = studentProgram.DueAmount - considerationAmount;
                }
                else
                {
                    if (considerationAmount >= 0)
                    {
                        studentPayment.CashBackAmount = considerationAmount - studentProgram.DueAmount;
                        studentPayment.PayableAmount = studentProgram.DueAmount;
                        studentPayment.DueAmount = 0;
                    }
                }
                studentPayment.ReceivedDate = DateTime.Now;
                studentPayment.CreateBy = Convert.ToInt64(User.Identity.GetUserId());
                studentPayment.ModifyBy = Convert.ToInt64(User.Identity.GetUserId());
                studentPayment.Status = StudentPayment.EntityStatus.Active;
                studentPayment.PaymentMethod = (int)PaymentMethod.Cash;
                if (!String.IsNullOrEmpty(referrerenceNote) && !String.IsNullOrEmpty(referrer))
                {
                    studentPayment.SpReferenceNote = referrerenceNote.Trim();
                    studentPayment.Referrer = _referenceService.LoadById(Convert.ToInt64(referrer.Trim()));
                }
                else
                {
                    return Json(new Response(false, "Referrer Or ReferrerenceNote Field Missing"));
                }
                studentPayment.StudentProgram = studentProgram;
                var flag = 0;
                var arrList = new List<string>();
                var referenceNoteGenerateAccordingToCanceledCourse = "";
                foreach (
                    StudentCourseDetail studentCourseDetail in
                        courseAndSubList.SelectMany(courseAndSubDic =>
                            (from courseAndSub in courseAndSubDic
                             let cancelCourseId = courseAndSub.Key
                             let cancelSubId = courseAndSub.Value
                             select
                                 _courseSubjectService
                                     .LoadCourseSubject(
                                         Convert.ToInt64(cancelCourseId),
                                         Convert.ToInt64(cancelSubId)
                                     )
                                 into courseSubject
                                 select _studentCourseDetailsService
                                     .LoadStudentCourseDetailByCourseSubjectAndStudentProgram
                                     (
                                         courseSubject,
                                         studentProgram))))
                {
                    if (flag == 0)
                    {
                        if (referenceNoteGenerateAccordingToCanceledCourse != "")
                            referenceNoteGenerateAccordingToCanceledCourse =
                                referenceNoteGenerateAccordingToCanceledCourse.Remove(
                                    referenceNoteGenerateAccordingToCanceledCourse.LastIndexOf(",", System.StringComparison.Ordinal)) + " ), ";
                        referenceNoteGenerateAccordingToCanceledCourse += studentCourseDetail.CourseSubject.Course.Name + " : ( ";
                        referenceNoteGenerateAccordingToCanceledCourse += studentCourseDetail.CourseSubject.Subject.ShortName + ", ";
                        flag = 1;
                    }
                    if (arrList.Count > 0)
                    {
                        if (
                            arrList.Contains(
                                studentCourseDetail.CourseSubject.Course.Id.ToString(
                                    CultureInfo.InvariantCulture)))
                        {
                            referenceNoteGenerateAccordingToCanceledCourse +=
                                studentCourseDetail.CourseSubject.Subject.ShortName + ", ";
                        }
                        else
                        {
                            referenceNoteGenerateAccordingToCanceledCourse =
                                referenceNoteGenerateAccordingToCanceledCourse.Remove(
                                    referenceNoteGenerateAccordingToCanceledCourse
                                        .LastIndexOf(
                                            ",", System.StringComparison.Ordinal)) + " ), ";
                            referenceNoteGenerateAccordingToCanceledCourse +=
                                studentCourseDetail.CourseSubject.Course.Name + " : ( ";
                            referenceNoteGenerateAccordingToCanceledCourse +=
                                studentCourseDetail.CourseSubject.Subject.ShortName + ", ";
                        }
                    }
                    arrList.Add(
                        studentCourseDetail.CourseSubject.Course.Id.ToString(
                            CultureInfo.InvariantCulture));
                    studentCourseDetails.Add(studentCourseDetail);
                }
                referenceNoteGenerateAccordingToCanceledCourse = referenceNoteGenerateAccordingToCanceledCourse.Remove(referenceNoteGenerateAccordingToCanceledCourse.LastIndexOf(",", System.StringComparison.Ordinal)) + " ) ";
                studentPayment.Remarks = referenceNoteGenerateAccordingToCanceledCourse + " Cancelled.";
                studentPayment.PaymentType = PaymentType.CancellationReceipt;
                foreach (var cs in studentCourseDetails)
                {
                    studentPayment.CourseSubjectList.Add(cs.CourseSubject);
                }
                long programId = studentProgram.Program.Id; 
                long organizationId = studentProgram.Program.Organization.Id;
                long sessionId = studentProgram.Batch.Session.Id;
                long studentId = studentProgram.Student.Id;

                var isCancelledSuccess = _studentPaymentService.IsCourseCancelledSuccess(studentCourseDetails, studentPayment);
                if (isCancelledSuccess)
                {
                    //var sPayment = _studentPaymentService.LoadByStudentProgram(studentProgram);
                    bool flagForSms = SendSmsApi.GenarateInstantSmsAndNumber(4, organizationId, programId, sessionId, studentId, studentProgram.Id);
                    var updatedStudentProgram = _studentProgramService.GetStudentProgram(stdPId, true);
                    bool tmpIsUpdate = false;
                    if (updatedStudentProgram.IsPolitical == false && studentPayment.Referrer != null && studentPayment.Referrer.Code == "04")/*HARD CODED FOR POLITICAL REFERANCE*/
                    {
                        tmpIsUpdate = true;
                        updatedStudentProgram.IsPolitical = true;
                    }
                    if (updatedStudentProgram.StudentCourseDetails.Count() <= 0)
                    {
                        tmpIsUpdate = true;
                        updatedStudentProgram.Status = StudentProgram.EntityStatus.Inactive;                        
                    }
                    if (tmpIsUpdate == true)
                    {
                        _studentProgramService.Update(updatedStudentProgram);
                    }
                    return Json(new Response(true, studentPayment.Id.ToString(CultureInfo.InvariantCulture)));
                }
                return Json(new Response(false, "Course Cancel Failed(Possible Cause: Database Operation Failed)"));
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, "Course Cancel Failed(Slow Internet Connection) "));
            }
        }
        #endregion


        private Discount GetDiscount(IList<Discount> existingDiscountList, StudentViewModel studentViewModel)
        {
            IList<Discount> discountApplicableListForStd = (from discountObj in existingDiscountList
                                                            let discountDetailsList
                       = _discountDetailService.GetAllDiscountDetailByDiscountId(discountObj.Id)
                                                            where discountDetailsList.Count <= studentViewModel.CourseViewModels.Count
                                                            let countdiscountDetailsList = discountDetailsList.Count
                                                            let jj = (from discountDetail in discountDetailsList
                                                                      let dCourse = discountDetail.Course
                                                                      let dMinSub = discountDetail.MinSubject
                                                                      let ii = 0
                                                                      where (from stdAssignedCourse in studentViewModel.CourseViewModels
                                                                             let stdCourse = _courseService.GetCourse(stdAssignedCourse.Id)
                                                                             where stdCourse == dCourse && dMinSub <= stdAssignedCourse.SubjectViewModels.Count
                                                                             select stdAssignedCourse).Any()
                                                                      select dCourse).Count()
                                                            where countdiscountDetailsList == jj
                                                            select discountObj).ToList();
            var appliedDiscountObj = new Discount();
            if (discountApplicableListForStd.Count > 1)
            {
                appliedDiscountObj = _discountService.GetMostApplicableDiscountObj(discountApplicableListForStd);
            }
            if (discountApplicableListForStd.Count == 1)
            {
                appliedDiscountObj = discountApplicableListForStd[0];
            }
            return appliedDiscountObj;
        }

        public string GetCouseViewDetails(string viewName, IList<Course> courseList, bool isChecked)
        {
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

    }
}