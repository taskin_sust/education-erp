﻿using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.GoodsTransferTest
{
    interface IGoodsTransferReportTest
    {
        void Save_Should_Be_Return_Successfully();
        void Invalid_Selection_of_ReportType();
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsTransfer")]
    public class GoodsTransferReportTest : GoodsTransferBaseTest, IGoodsTransferReportTest
    {

        [Fact]
        public void Save_Should_Be_Return_Successfully()
        {
            organizationFactory.Create().Persist();
            ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            //var fromBranch = BranchFactory.Create().IsCorporate(false).WithOrganization(_organizationFactory.Object).Persist();
            //var toBranch = BranchFactory.Create().IsCorporate(false).WithOrganization(_organizationFactory.Object).Persist();
            var branchList = BranchFactory.CreateMore(2, organizationFactory.Object).Persist();
            var fromBranch = branchList.ObjectList[0];
            var toBranch = branchList.ObjectList[1];
            SessionFactory.Create().Persist();
            ItemFactory.Create(ItemType.ProgramItem).WithItemGroup().WithOrganization(organizationFactory.Object).Persist();
            ProgramSessionItemFactory.Create().WithItem(ItemFactory.Object).WithProgramAndSession(ProgramFactory.Object, SessionFactory.Object).Persist();
            GoodsReceiveDetailsFactory.Create().WithPurpose().WithItem(ItemFactory.Object).WithProgram(ProgramFactory.Object).WithSession(SessionFactory.Object);

            GoodsReceiveFactory.Create().WithGoodsReceiveDetails(GoodsReceiveDetailsFactory.Object);
            GoodsReceiveFactory.WithBranch(BranchFactory.Object).Persist(true);

            GoodsTransferDetailsFactory.Create().WithGoodsTransfer(GoodsTransferFactory.Object).WithItem(ItemFactory.Object).WithProgram(ProgramFactory.Object).WithSession(SessionFactory.Object);
            IList<GoodsTransferDetails> goodsTransferDetailses = new List<GoodsTransferDetails>();
            goodsTransferDetailses.Add(GoodsTransferDetailsFactory.Object);
            GoodsTransferFactory.Create().WithDetails(goodsTransferDetailses).WithFromBranch(fromBranch).WithToBranch(toBranch).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, fromBranch);

            var count = _goodsTransferService.GetGoodsTransferByBranchRowCount(userMenu, organizationFactory.Object.Id, fromBranch.Id, 1, commonHelper.ConvertIdToList(toBranch.Id).ToArray(), commonHelper.ConvertIdToList(ItemFactory.Object.ItemGroupId).ToArray(), commonHelper.ConvertIdToList(ItemFactory.Object.Id).ToArray(), DateTime.Now, DateTime.Now);
            Assert.Equal(1, count);

        }

        [Fact]
        public void Invalid_Selection_of_ReportType()
        {
            throw new NotImplementedException();
        }
    }
}
