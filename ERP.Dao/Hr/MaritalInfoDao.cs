﻿using System;
using System.Collections.Generic;
using System.Linq;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IMaritalInfoDao : IBaseDao<MaritalInfo, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        MaritalInfo GetTeamMemberMaritalInfo(long teamMemberId);
        #endregion

        #region List Loading Function
        IList<MaritalInfo> LoadByMemberId(long memeId);
        #endregion

        #region Others Function

        int GetCurrentMaritalStatusForLeave(long memeId);

        #endregion

        #region Helper Function

        #endregion


    }
    public class MaritalInfoDao : BaseDao<MaritalInfo, long>, IMaritalInfoDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public MaritalInfo GetTeamMemberMaritalInfo(long teamMemberId)
        {
            return Session.QueryOver<MaritalInfo>().Where(x => x.TeamMember.Id == teamMemberId && x.Status == MaritalInfo.EntityStatus.Active).Take(1).SingleOrDefault();
        }
        #endregion

        #region List Loading Function


        public IList<MaritalInfo> LoadByMemberId(long memeId)
        {

            var ss = Session.QueryOver<MaritalInfo>().Where(x => x.TeamMember.Id == memeId && x.Status == MaritalInfo.EntityStatus.Active).List<MaritalInfo>();
            if (ss != null) return ss;
            return new List<MaritalInfo>();
        }
        #endregion

        #region Others Function

        public int GetCurrentMaritalStatusForLeave(long memeId)
        {
            IList<MaritalInfo> hrMaritalInfos = LoadByMemberId(memeId);
            var singleOrDefault = hrMaritalInfos.OrderByDescending(x => x.Id).Take(1).SingleOrDefault();
            if (singleOrDefault != null)
                if (singleOrDefault.MaritalStatus != null) return (int)singleOrDefault.MaritalStatus;
            return -404;

        }

        #endregion

        #region Helper Function

        #endregion


    }
}
