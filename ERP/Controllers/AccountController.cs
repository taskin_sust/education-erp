﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using UdvashERP.Areas.Administration.Controllers;
using UdvashERP.App_code;
using UdvashERP.Areas.Student.Controllers;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Models;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Cache;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using System.Text;
using System.IO;
using UdvashERP.BusinessRules.Administration;

namespace UdvashERP.Controllers
{
    [UerpArea("NoArea")]
    [Authorize]
    [AuthorizeAccess]
    public class AccountController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("Log_UMS");
        #endregion
        private readonly IUserService _userService;
        private readonly IAreaControllersService _areaControllersService;
        private readonly IActionsService _actionsService;
        private IStudentProgramService _studentProgramService;
        private readonly ILoginActivityLogService _loginActivityLogService;
        private UserStoreLongPk _userStore;
        public AccountController(): this(new UserManager<ApplicationUser, long>(new UserStoreLongPk(new ApplicationDbContext())))
        {
            var session = NHibernateSessionFactory.OpenSession();
            _userService = new UserService(session);
            _areaControllersService = new AreaControllersService(session);
            _actionsService = new ActionsService(session);
            _studentProgramService = new StudentProgramService(session);
            _loginActivityLogService = new LoginActivityLogService(session);
            _userStore = new UserStoreLongPk(ApplicationDbContext.Create());
        }

        //public ActionResult Test()
        //{
        //    var p = _studentProgramService.GetStudentProgram("12141000057");
        //    return View();
        //}

        public ActionResult InitilizeControllerActions()
        {
            //var list = typeof(HomeController).Assembly.GetTypes()
            //    .Where(t => t.BaseType != null && t.BaseType.Name == "Controller")
            //    .SelectMany(type => type.GetMethods())
            //    .Where(method => method.IsPublic)
            //    .ToArray();


            List<AreaControllers> controllerList = new List<AreaControllers>();

            var controllers = GetAllControllerClassType();
            foreach (Type cType in controllers)
            {

                string areaName = "NoArea";
                List<UerpAreaAttribute> areas = cType.GetCustomAttributes<UerpAreaAttribute>().ToList();
                if (areas != null && areas.Count > 0)
                {
                    var area = areas[0];
                    areaName = area.Name;
                }
                string controllerName = cType.Name;
                //var allActions = ActionNames(controllerName);
                controllerList.Add(new AreaControllers()
                {
                    Name = controllerName,
                    Area = areaName,
                    Rank = 1
                });
            }
            var areaControllers = controllerList.GroupBy((m) => m.Area, (key, group) => new AreaControllerView() { AreaName = key, Controllers = group.ToList() });
            List<AreaControllersListViewModel> checkedModelForViewBagList = new List<AreaControllersListViewModel>();

            foreach (var areaControllerView in areaControllers)
            {
                var checkedModelForViewBag = new AreaControllersListViewModel();
                checkedModelForViewBag.ControllersName = new List<ViewModelCheckBox>();
                checkedModelForViewBag.AreaName = areaControllerView.AreaName;
                foreach (var getControllers in areaControllerView.Controllers)
                {
                    var subStringController = getControllers.Name.Substring(0, getControllers.Name.Length - 10);
                    ViewModelCheckBox v = new ViewModelCheckBox();

                    v.AName = areaControllerView.AreaName;
                    //v.Name = getControllers.Name;
                    v.Name = subStringController;


                    var existsController = _areaControllersService.LoadByControllerName(subStringController);
                    if (existsController != null)
                    {
                        v.IsSelected = true;
                    }
                    checkedModelForViewBag.ControllersName.Add(v);
                }
                checkedModelForViewBagList.Add(checkedModelForViewBag);
            }
            ViewBag.AreaControllers = checkedModelForViewBagList;

            return View(checkedModelForViewBagList);
        }

        private List<string> ActionNames(string controllerName)
        {
            var types =
                from a in AppDomain.CurrentDomain.GetAssemblies()
                from t in a.GetTypes()
                where typeof(Controller).IsAssignableFrom(t) &&
                        string.Equals(controllerName + "Controller", t.Name, StringComparison.OrdinalIgnoreCase)
                select t;

            var controllerType = types.FirstOrDefault();

            if (controllerType == null)
            {
                return Enumerable.Empty<string>().ToList();
            }
            return new ReflectedControllerDescriptor(controllerType)
                .GetCanonicalActions().Select(x => x.ActionName)
                .ToList();
        }

        [HttpPost]
        public ActionResult InitilizeControllerActions(AreaControllersListViewModel data)
        {
            try
            {
                foreach (var controllers in data.ControllersName)
                {
                    try
                    {
                        AreaControllers ac = new AreaControllers();
                        if (controllers.IsSelected)
                        {
                            bool isExists = _areaControllersService.CheckDataByAreaController(controllers.AName,
                                controllers.Name);

                            if (!isExists)
                            {
                                ac.Name = controllers.Name;
                                ac.Area = controllers.AName;
                                _areaControllersService.Save(ac);
                                var allActions = ActionNames(controllers.Name);
                                if (allActions != null)
                                {
                                    foreach (var singleAction in allActions)
                                    {
                                        Actions checkAction = _actionsService.LoadByActionsNameAndController(singleAction, ac);
                                        if (checkAction == null)
                                        {
                                            Actions action = new Actions();
                                            action.AreaControllers = ac;
                                            action.Name = singleAction;
                                            _actionsService.Save(action);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                AreaControllers acontroller = _areaControllersService.LoadByAreaController(controllers.AName, controllers.Name);
                                var allActions = ActionNames(controllers.Name);
                                if (allActions != null)
                                {
                                    foreach (var singleAction in allActions)
                                    {
                                        Actions checkAction = _actionsService.LoadByActionsNameAndController(singleAction, acontroller);
                                        if (checkAction == null)
                                        {
                                            Actions action = new Actions();
                                            action.AreaControllers = acontroller;
                                            action.Name = singleAction;
                                            _actionsService.Save(action);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            bool isExists = _areaControllersService.CheckDataByAreaController(controllers.AName,
                                controllers.Name);
                            if (isExists)
                            {
                                ac = _areaControllersService.LoadByAreaController(controllers.AName, controllers.Name);
                                // var allActions = ActionNames(controllers.Name);
                                if (ac.Actions != null)
                                {
                                    _areaControllersService.Delete(ac.Id, ac);
                                }
                            }
                        }
                    }
                    catch (DuplicateEntryException ex)
                    {
                        throw ex;
                    }
                    catch (ReflectionTypeLoadException ex)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (Exception exSub in ex.LoaderExceptions)
                        {
                            sb.AppendLine(exSub.Message);
                            FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
                            if (exFileNotFound != null)
                            {
                                if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                                {
                                    sb.AppendLine("Fusion Log:");
                                    sb.AppendLine(exFileNotFound.FusionLog);
                                }
                            }
                            sb.AppendLine();
                        }
                        string errorMessage = sb.ToString();
                        TempData["UpdateControllerErrorMessage"] = "[" + controllers.AName + "--" + controllers.Name + "] " + errorMessage;
                        _logger.Error(errorMessage);
                    }
                    catch (Exception ex)
                    {
                        TempData["UpdateControllerErrorMessage"] = "[" + controllers.AName + "-" + controllers.Name + "] " + ex;
                        _logger.Error(ex);
                    }
                }
                TempData["UpdateControllerSuccessMessage"] = "Successfully updated.";
            }
            catch (DuplicateEntryException ex)
            {
                TempData["UpdateControllerErrorMessage"] = ex;
                //ViewBag.ErrorMessage = ex;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                TempData["UpdateControllerErrorMessage"] = e;
            }
            return RedirectToAction("InitilizeControllerActions");
        }

        private Type[] GetAllControllerClassType()
        {
            var list = typeof(HomeController).Assembly.GetTypes()
                .Where(t => t.BaseType != null && t.BaseType.Name == "Controller")
                .ToArray();
            return list;
        }

        public AccountController(UserManager<ApplicationUser, long> userManager)
        {
            UserManager = new UserManager<ApplicationUser, long>(new UserStoreLongPk(new ApplicationDbContext()));
            RoleManager = new RoleManager<RoleLongPk, long>(new RoleStoreLongPk(new ApplicationDbContext()));
        }

        //[AllowAnonymous]
        public async Task<ActionResult> SetupAccount()
        {
            try
            {
                bool roleCreateRulst = await RoleManager.RoleExistsAsync(ApplicationRoles.User);
                if (!roleCreateRulst)
                {
                    var result = await RoleManager.CreateAsync(new RoleLongPk(ApplicationRoles.User));
                    ViewBag.UserRole = "User role created successfully.";
                }
                else
                    ViewBag.UserRole = "User role already exists";


                bool devRoleCreateRulst = await RoleManager.RoleExistsAsync(ApplicationRoles.Developer);
                if (!devRoleCreateRulst)
                {
                    var result = await RoleManager.CreateAsync(new RoleLongPk(ApplicationRoles.Developer));
                    ViewBag.DeveloperRole = "Developer role created successfully.";
                }
                else
                    ViewBag.DeveloperRole = "Developer Role already exists.";

                var oldUser = UserManager.Users.FirstOrDefault(u => u.UserName == ApplicationUsers.SuperAdmin);

                if (oldUser == null)
                {
                    var superAdmin = new ApplicationUser()
                                     {
                                         Email = ApplicationUsers.SuperAdmin + "@onnorokom.com",
                                         UserName = ApplicationUsers.SuperAdmin,
                                         LockoutEndDateUtc = DateTime.Now
                                     };
                    var result = await UserManager.CreateAsync(superAdmin, "0nn0R0k0m12#");
                    if (result.Succeeded)
                    {
                        var res = await UserManager.AddToRoleAsync(superAdmin.Id, ApplicationRoles.Developer);
                        UserProfile userObj = new UserProfile();
                        userObj.Name = superAdmin.UserName;
                        userObj.Branch = null;
                        userObj.Campus = null;
                        userObj.AspNetUser = _userService.LoadAspNetUserById(superAdmin.Id);
                        _userService.Save(userObj,null);
                        ViewBag.User = "User created successfully";
                    }
                }
                else
                    ViewBag.User = "User already exists";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        public UserManager<ApplicationUser, long> UserManager { get; private set; }
        public RoleManager<RoleLongPk, long> RoleManager { get; set; }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string message = null)
        {
            //if already logined 
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            //prepair for login
            ViewBag.Message = message;
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                //Login activity log
                var isMobile = Request.Browser.IsMobileDevice;
                var temUserAgent = Request.UserAgent;
                if (temUserAgent.Length > 995)
                {
                    temUserAgent = temUserAgent.Substring(0, 995);
                }
                var lAl = new LoginActivityLog
                          {
                              UserName = model.UserName,
                              IpAddress = Request.UserHostAddress,
                              Browser = Request.Browser.Browser + "_v" + Request.Browser.Version + " (Type : " + Request.Browser.Type + ")",
                              Device = isMobile ? "Mobile(" + Request.Browser.MobileDeviceModel + ")" : "Desktop",
                              OsName = GetOs(Request.UserAgent),
                              UserAgent = temUserAgent,
                              CreationDate = DateTime.Now
                          };
                // find user by username first
                var user = await UserManager.FindByNameAsync(model.UserName);
                //var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null && user.Status == AspNetUser.EntityStatus.Active)
                {
                    var validCredentials = await UserManager.FindAsync(model.UserName, model.Password);

                    if (await UserManager.GetLockoutEnabledAsync(user.Id) && validCredentials != null)
                    {
                        lAl.OperationStatus = (int)LoginOperationStatus.Lock;
                        lAl.Remarks = "Account has been locked";
                        _loginActivityLogService.Save(lAl);

                        string message = string.Format("Your account has been locked. Please contact your admin");
                        ViewBag.ErrorMessage = message;
                        return View(model);
                    }
                    if (validCredentials == null)
                    {
                        if (user.AccessFailedCount > Convert.ToInt32(ConfigurationManager.AppSettings["MaxFailedAccessAttemptsBeforeLockout"]))
                        {
                            lAl.OperationStatus = (int)LoginOperationStatus.Lock;
                            lAl.Remarks = "more attempt(s) for login";
                            _loginActivityLogService.Save(lAl);

                            await UserManager.SetLockoutEnabledAsync(user.Id, true);
                            string message = string.Format("Invalid credentials. You have (0) more attempt(s) before your account gets locked out. Please contact your admin");
                            ViewBag.ErrorMessage = message;
                        }
                        else if (user.AccessFailedCount == Convert.ToInt32(ConfigurationManager.AppSettings["MaxFailedAccessAttemptsBeforeLockout"]))
                        {
                            lAl.OperationStatus = (int)LoginOperationStatus.Lock;
                            lAl.Remarks = "more attempt(s) for login";
                            _loginActivityLogService.Save(lAl);

                            user.AccessFailedCount = user.AccessFailedCount + 1;
                            await UserManager.UpdateAsync(user);
                            string message = string.Format("Invalid credentials. You have({0})more attempt(s) before your account gets locked out.", ConfigurationManager.AppSettings["MaxFailedAccessAttemptsBeforeLockout"]);
                            ViewBag.ErrorMessage = message;
                        }
                        else
                        {
                            lAl.OperationStatus = (int)LoginOperationStatus.Fail;
                            lAl.Remarks = "Invalid password";
                            _loginActivityLogService.Save(lAl);

                            user.AccessFailedCount = user.AccessFailedCount + 1;
                            await UserManager.UpdateAsync(user);
                            ViewBag.ErrorMessage = "Invalid credentials. Please try again.";
                        }
                    }
                    else
                    {

                        //SPECIAL CODE BLOCK TO PROTECT UN-AUTHORIZED APP TO ACCESS MAIN WEBSITE 
                        string UserAgentFilterOnLogin = ConfigurationManager.AppSettings["UserAgentFilterOnLogin"];
                        List<string> UserAgentFilterList = UserAgentFilterOnLogin.Split(new string[] { "|||" }, StringSplitOptions.None).ToList();
                        bool isBrowserBlocked = false;
                        foreach (var i in UserAgentFilterList)
                        {
                            if (temUserAgent.Contains(i) == true) { isBrowserBlocked = true; break; }
                        }
                        if (isBrowserBlocked == true)
                        {
                            lAl.OperationStatus = (int)LoginOperationStatus.Fail;
                            lAl.Remarks = "Login Denied For Security Reason. (Browser is not safe)";
                            _loginActivityLogService.Save(lAl);
                            ViewBag.ErrorMessage = "This browser is not safe. Please try from another browser.";
                        }
                        else
                        {
                            lAl.OperationStatus = (int)LoginOperationStatus.Success;
                            lAl.Remarks = "Successfully login";
                            _loginActivityLogService.Save(lAl);

                            ApplicationUser Model = await _userStore.FindByIdAsync(user.Id);
                            Model.ModificationDate = DateTime.Now;
                            await _userStore.UpdateAsync(Model);
                            var ctx = _userStore.Context;
                            var result = ctx.SaveChanges();

                            await SignInAsync(user, model.RememberMe);
                            await UserManager.SetLockoutEnabledAsync(user.Id, false);
                            // When token is verified correctly, clear the access failed count used for lockout
                            await UserManager.ResetAccessFailedCountAsync(user.Id);
                            System.Web.HttpContext.Current.Session["SessionUser"] = user;
                            return RedirectToLocal(returnUrl);
                        }
                    }
                }
                else
                {
                    lAl.OperationStatus = (int)LoginOperationStatus.Fail;
                    lAl.Remarks = "Invalid password";
                    _loginActivityLogService.Save(lAl);
                    ViewBag.ErrorMessage = "Invalid username or password.";
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        //[AllowAnonymous]
        //public ActionResult Register()
        //{
        //    return View();
        //}

        //
        // POST: /Account/Register
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Register(RegisterViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = new ApplicationUser() { UserName = model.UserName };
        //        try
        //        {
        //            var result = await UserManager.CreateAsync(user, model.Password);
        //            if (result.Succeeded)
        //            {
        //                await SignInAsync(user, isPersistent: false);
        //                return RedirectToAction("Index", "Home");
        //            }
        //            else
        //            {
        //                AddErrors(result);
        //            }
        //        }
        //        catch (Exception e)
        //        {

        //            throw e;
        //        }

        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(long.Parse(User.Identity.GetUserId()), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        [AuthorizeRequired]
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed. Please logout your account & login by new password"
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [AuthorizeRequired]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            //Login activity log
            var lAl = new LoginActivityLog
            {
                UserName = User.Identity.GetUserName(),
                IpAddress = Request.UserHostAddress,
                Browser = Request.Browser.Browser,
                Device = Request.UserAgent,
                CreationDate = DateTime.Now
            };

            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(long.Parse(User.Identity.GetUserId()), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        long userId = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                        ApplicationUser Model = await _userStore.FindByIdAsync(userId);
                        Model.ModificationDate = DateTime.Now;
                        Model.ModifyBy = Convert.ToInt64(userId);
                        await _userStore.UpdateAsync(Model);
                        var ctx = _userStore.Context;
                        ctx.SaveChanges();
                        
                        UserProfile user = _userService.LoadByAspNetUserWithoutStatus(_userService.LoadbyAspNetUserId(Model.Id));
                        user.BusinessId = null;
                        _userService.Update(Model.Id, user,null);

                        lAl.OperationStatus = (int)LoginOperationStatus.Success;
                        lAl.Remarks = "Successfully password changed.";
                        _loginActivityLogService.Save(lAl);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        lAl.OperationStatus = (int)LoginOperationStatus.Fail;
                        lAl.Remarks = "Error password changed";
                        _loginActivityLogService.Save(lAl);

                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(long.Parse(User.Identity.GetUserId()), model.NewPassword);
                    if (result.Succeeded)
                    {
                        //ApplicationUser Model = await UserManager.FindByIdAsync(userUpdateForm.Id);
                        lAl.OperationStatus = (int)LoginOperationStatus.Success;
                        lAl.Remarks = "Successfully password changed";
                        _loginActivityLogService.Save(lAl);

                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        lAl.OperationStatus = (int)LoginOperationStatus.Fail;
                        lAl.Remarks = "Error password changed";
                        _loginActivityLogService.Save(lAl);

                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(long.Parse(User.Identity.GetUserId()), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // GET: /Account/LogOff
        [HttpGet]
        [AllowAnonymous]
        public ActionResult LogOff(String message)
        {
            AuthenticationManager.SignOut();
            this.Session.Clear();
            return RedirectToAction("Index", "Home", new { Message = message });
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            this.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(long.Parse(User.Identity.GetUserId()));
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            var claims = await UserManager.GetClaimsAsync(user.Id);
            var userNameClaim = claims.FirstOrDefault(c => c.Type == "FullName");
            if (userNameClaim == null)
            {
                await UserManager.AddClaimAsync(user.Id, new Claim("FullName", user.FullName ?? ""));
            }
            else if (userNameClaim.Value != user.FullName)
            {
                await UserManager.RemoveClaimAsync(user.Id, userNameClaim);
                await UserManager.AddClaimAsync(user.Id, new Claim("FullName", user.FullName ?? ""));
            }

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
            AuthorizationCache.SetUserPermissionChangeTime(new AspNetUser(){Id = user.Id});
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                if (error.StartsWith("Name"))
                {
                    var nameToEmail = Regex.Replace(error, "Name", "Email");
                    ModelState.AddModelError("", nameToEmail);
                }
                else
                {
                    ModelState.AddModelError("", error);
                }
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(long.Parse(User.Identity.GetUserId()));
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        
        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        private string GetOs(string userAgent)
        {
            if (userAgent.IndexOf("Windows NT 10.0", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows 10";
            }
            if (userAgent.IndexOf("Windows NT 6.3", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows 8.1";
            }
            if (userAgent.IndexOf("Windows NT 6.2", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows 8";
            }
            if (userAgent.IndexOf("Windows NT 6.1", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows 7";
            }
            if (userAgent.IndexOf("Windows NT 6.0", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows Vista";
            }
            if (userAgent.IndexOf("Windows NT 5.2", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows XP x64 Edition";
            }
            if (userAgent.IndexOf("Windows NT 5.1", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows XP";
            }
            if (userAgent.IndexOf("Windows NT 5.01", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows 2000, Service Pack 1 (SP1)";
            }
            if (userAgent.IndexOf("Windows NT 5.0", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows 2000";
            }
            if (userAgent.IndexOf("Windows NT 4.0", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows NT 4.0";
            }
            if (userAgent.IndexOf("Win 9x 4.90", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows Millennium Edition (Windows Me)";
            }
            if (userAgent.IndexOf("Windows 98", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows 98";
            }
            if (userAgent.IndexOf("Windows 95", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows 95";
            }
            if (userAgent.IndexOf("Windows CE", System.StringComparison.Ordinal) > 0)
            {
                return "Microsoft Windows CE";
            }
            if (userAgent.IndexOf("Ubuntu", System.StringComparison.Ordinal) > 0)
            {
                return "Linux Ubuntu";
            }
            if (userAgent.IndexOf("Gentoo", System.StringComparison.Ordinal) > 0)
            {
                return "Linux Gentoo";
            }
            if (userAgent.IndexOf("Debian", System.StringComparison.Ordinal) > 0)
            {
                return "Linux Debian";
            }
            if (userAgent.IndexOf("Fedora", System.StringComparison.Ordinal) > 0)
            {
                return "Linux Fedora";
            }
            if (userAgent.IndexOf("Mac OS X", System.StringComparison.Ordinal) > 0)
            {
                return "Mac OS X";
            }
            if (userAgent.IndexOf("Android", System.StringComparison.Ordinal) > 0)
            {
                return "Android";
            }
            return "Invalid OS";
        }
        #endregion
    }

    public class AreaControllerView
    {
        public virtual string AreaName { get; set; }
        public virtual List<AreaControllers> Controllers { get; set; }

    }
}