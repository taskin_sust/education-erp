﻿namespace Qmaster.Print.Forms
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnPrintTest = new System.Windows.Forms.Button();
            this.txtQrHeight = new System.Windows.Forms.TextBox();
            this.txtQrWidth = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtQrDipY = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBarDipY = new System.Windows.Forms.TextBox();
            this.txtQrDipX = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBarDipX = new System.Windows.Forms.TextBox();
            this.txtBarHeight = new System.Windows.Forms.TextBox();
            this.txtBarWidth = new System.Windows.Forms.TextBox();
            this.cbDeleteUnzipFile = new System.Windows.Forms.CheckBox();
            this.cbDeleteAllGeneratedFile = new System.Windows.Forms.CheckBox();
            this.cbIsPrint = new System.Windows.Forms.CheckBox();
            this.cbbarNo = new System.Windows.Forms.CheckBox();
            this.cbIsQrCode = new System.Windows.Forms.CheckBox();
            this.cbIsBarcode = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.bsPrintingConfigInfo = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtMinPage = new System.Windows.Forms.TextBox();
            this.btnSaveConfig = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtQrXaxis = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtQrYaxis = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMaxPage = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMargePdf = new System.Windows.Forms.TextBox();
            this.cbDeleteZipFile = new System.Windows.Forms.CheckBox();
            this.cbDeleteMargeFile = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBarYaxis = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBarXaxis = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtShowBarQrCodeOnPage = new System.Windows.Forms.TextBox();
            this.cbIsOsl = new System.Windows.Forms.CheckBox();
            this.cbIsAlpha = new System.Windows.Forms.CheckBox();
            this.cbShowCodeNumber = new System.Windows.Forms.CheckBox();
            this.cbShowCodeNumberWithExamNo = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtShowCodeNumberOnPage = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCodeNumberX = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCodeNumberY = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.cbDownloadNewFile = new System.Windows.Forms.CheckBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCodeNumberY2nd = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtCodeNumberX2nd = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtShowCodeNumberOnPage2nd = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.bsPrintingConfigInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPrintTest
            // 
            this.btnPrintTest.Location = new System.Drawing.Point(346, 235);
            this.btnPrintTest.Name = "btnPrintTest";
            this.btnPrintTest.Size = new System.Drawing.Size(75, 23);
            this.btnPrintTest.TabIndex = 1;
            this.btnPrintTest.Text = "Print Test";
            this.btnPrintTest.UseVisualStyleBackColor = true;
            this.btnPrintTest.Click += new System.EventHandler(this.btnPrintTest_Click);
            // 
            // txtQrHeight
            // 
            this.txtQrHeight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtQrHeight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtQrHeight.Location = new System.Drawing.Point(512, 44);
            this.txtQrHeight.Name = "txtQrHeight";
            this.txtQrHeight.Size = new System.Drawing.Size(47, 20);
            this.txtQrHeight.TabIndex = 46;
            // 
            // txtQrWidth
            // 
            this.txtQrWidth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtQrWidth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtQrWidth.Location = new System.Drawing.Point(420, 44);
            this.txtQrWidth.Name = "txtQrWidth";
            this.txtQrWidth.Size = new System.Drawing.Size(47, 20);
            this.txtQrWidth.TabIndex = 45;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(672, 51);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "Q DiY:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(670, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 43;
            this.label14.Text = "B DiY:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(571, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 13);
            this.label15.TabIndex = 41;
            this.label15.Text = "Q DiX:";
            // 
            // txtQrDipY
            // 
            this.txtQrDipY.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtQrDipY.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtQrDipY.Location = new System.Drawing.Point(718, 44);
            this.txtQrDipY.Name = "txtQrDipY";
            this.txtQrDipY.Size = new System.Drawing.Size(47, 20);
            this.txtQrDipY.TabIndex = 37;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(571, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 13);
            this.label13.TabIndex = 42;
            this.label13.Text = "B DiX:";
            // 
            // txtBarDipY
            // 
            this.txtBarDipY.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtBarDipY.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtBarDipY.Location = new System.Drawing.Point(718, 10);
            this.txtBarDipY.Name = "txtBarDipY";
            this.txtBarDipY.Size = new System.Drawing.Size(47, 20);
            this.txtBarDipY.TabIndex = 38;
            // 
            // txtQrDipX
            // 
            this.txtQrDipX.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtQrDipX.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtQrDipX.Location = new System.Drawing.Point(615, 44);
            this.txtQrDipX.Name = "txtQrDipX";
            this.txtQrDipX.Size = new System.Drawing.Size(47, 20);
            this.txtQrDipX.TabIndex = 35;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(385, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 40;
            this.label9.Text = "BW:";
            // 
            // txtBarDipX
            // 
            this.txtBarDipX.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtBarDipX.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtBarDipX.Location = new System.Drawing.Point(617, 10);
            this.txtBarDipX.Name = "txtBarDipX";
            this.txtBarDipX.Size = new System.Drawing.Size(47, 20);
            this.txtBarDipX.TabIndex = 36;
            // 
            // txtBarHeight
            // 
            this.txtBarHeight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtBarHeight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtBarHeight.Location = new System.Drawing.Point(512, 10);
            this.txtBarHeight.Name = "txtBarHeight";
            this.txtBarHeight.Size = new System.Drawing.Size(47, 20);
            this.txtBarHeight.TabIndex = 39;
            // 
            // txtBarWidth
            // 
            this.txtBarWidth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtBarWidth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtBarWidth.Location = new System.Drawing.Point(419, 10);
            this.txtBarWidth.Name = "txtBarWidth";
            this.txtBarWidth.Size = new System.Drawing.Size(47, 20);
            this.txtBarWidth.TabIndex = 34;
            // 
            // cbDeleteUnzipFile
            // 
            this.cbDeleteUnzipFile.AutoSize = true;
            this.cbDeleteUnzipFile.Location = new System.Drawing.Point(217, 80);
            this.cbDeleteUnzipFile.Name = "cbDeleteUnzipFile";
            this.cbDeleteUnzipFile.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbDeleteUnzipFile.Size = new System.Drawing.Size(106, 17);
            this.cbDeleteUnzipFile.TabIndex = 33;
            this.cbDeleteUnzipFile.Text = "Delete Unzip File";
            this.cbDeleteUnzipFile.UseVisualStyleBackColor = true;
            // 
            // cbDeleteAllGeneratedFile
            // 
            this.cbDeleteAllGeneratedFile.AutoSize = true;
            this.cbDeleteAllGeneratedFile.Location = new System.Drawing.Point(329, 80);
            this.cbDeleteAllGeneratedFile.Name = "cbDeleteAllGeneratedFile";
            this.cbDeleteAllGeneratedFile.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbDeleteAllGeneratedFile.Size = new System.Drawing.Size(129, 17);
            this.cbDeleteAllGeneratedFile.TabIndex = 32;
            this.cbDeleteAllGeneratedFile.Text = "Delete Generated File";
            this.cbDeleteAllGeneratedFile.UseVisualStyleBackColor = true;
            // 
            // cbIsPrint
            // 
            this.cbIsPrint.AutoSize = true;
            this.cbIsPrint.Location = new System.Drawing.Point(220, 140);
            this.cbIsPrint.Name = "cbIsPrint";
            this.cbIsPrint.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbIsPrint.Size = new System.Drawing.Size(47, 17);
            this.cbIsPrint.TabIndex = 31;
            this.cbIsPrint.Text = "Print";
            this.cbIsPrint.UseVisualStyleBackColor = true;
            // 
            // cbbarNo
            // 
            this.cbbarNo.AutoSize = true;
            this.cbbarNo.Location = new System.Drawing.Point(278, 140);
            this.cbbarNo.Name = "cbbarNo";
            this.cbbarNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbbarNo.Size = new System.Drawing.Size(89, 17);
            this.cbbarNo.TabIndex = 29;
            this.cbbarNo.Text = "Show Bar No";
            this.cbbarNo.UseVisualStyleBackColor = true;
            // 
            // cbIsQrCode
            // 
            this.cbIsQrCode.AutoSize = true;
            this.cbIsQrCode.Location = new System.Drawing.Point(116, 47);
            this.cbIsQrCode.Name = "cbIsQrCode";
            this.cbIsQrCode.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbIsQrCode.Size = new System.Drawing.Size(76, 17);
            this.cbIsQrCode.TabIndex = 30;
            this.cbIsQrCode.Text = "Is Qr Code";
            this.cbIsQrCode.UseVisualStyleBackColor = true;
            // 
            // cbIsBarcode
            // 
            this.cbIsBarcode.AutoSize = true;
            this.cbIsBarcode.Location = new System.Drawing.Point(116, 14);
            this.cbIsBarcode.Name = "cbIsBarcode";
            this.cbIsBarcode.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbIsBarcode.Size = new System.Drawing.Size(81, 17);
            this.cbIsBarcode.TabIndex = 28;
            this.cbIsBarcode.Text = "Is Bar Code";
            this.cbIsBarcode.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(481, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 47;
            this.label10.Text = "BH:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(481, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 49;
            this.label11.Text = "QH:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(385, 51);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 48;
            this.label12.Text = "QW:";
            // 
            // bsPrintingConfigInfo
            // 
            this.bsPrintingConfigInfo.DataSource = typeof(Qmaster.Print.Model.PrintConfigInfo);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(116, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Min Page:";
            // 
            // txtMinPage
            // 
            this.txtMinPage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtMinPage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMinPage.Location = new System.Drawing.Point(177, 110);
            this.txtMinPage.Name = "txtMinPage";
            this.txtMinPage.Size = new System.Drawing.Size(47, 20);
            this.txtMinPage.TabIndex = 50;
            // 
            // btnSaveConfig
            // 
            this.btnSaveConfig.Location = new System.Drawing.Point(431, 235);
            this.btnSaveConfig.Name = "btnSaveConfig";
            this.btnSaveConfig.Size = new System.Drawing.Size(75, 23);
            this.btnSaveConfig.TabIndex = 52;
            this.btnSaveConfig.Text = "Save Config";
            this.btnSaveConfig.UseVisualStyleBackColor = true;
            this.btnSaveConfig.Click += new System.EventHandler(this.btnSaveConfig_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(210, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 54;
            this.label2.Text = "QX :";
            // 
            // txtQrXaxis
            // 
            this.txtQrXaxis.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtQrXaxis.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtQrXaxis.Location = new System.Drawing.Point(244, 44);
            this.txtQrXaxis.Name = "txtQrXaxis";
            this.txtQrXaxis.Size = new System.Drawing.Size(47, 20);
            this.txtQrXaxis.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(296, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "QY:";
            // 
            // txtQrYaxis
            // 
            this.txtQrYaxis.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtQrYaxis.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtQrYaxis.Location = new System.Drawing.Point(329, 44);
            this.txtQrYaxis.Name = "txtQrYaxis";
            this.txtQrYaxis.Size = new System.Drawing.Size(47, 20);
            this.txtQrYaxis.TabIndex = 55;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(245, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 58;
            this.label4.Text = "Max Page:";
            // 
            // txtMaxPage
            // 
            this.txtMaxPage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtMaxPage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaxPage.Location = new System.Drawing.Point(310, 110);
            this.txtMaxPage.Name = "txtMaxPage";
            this.txtMaxPage.Size = new System.Drawing.Size(47, 20);
            this.txtMaxPage.TabIndex = 57;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(369, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 60;
            this.label5.Text = "Pdf Marge No:";
            // 
            // txtMargePdf
            // 
            this.txtMargePdf.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtMargePdf.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMargePdf.Location = new System.Drawing.Point(451, 110);
            this.txtMargePdf.Name = "txtMargePdf";
            this.txtMargePdf.Size = new System.Drawing.Size(47, 20);
            this.txtMargePdf.TabIndex = 59;
            // 
            // cbDeleteZipFile
            // 
            this.cbDeleteZipFile.AutoSize = true;
            this.cbDeleteZipFile.Location = new System.Drawing.Point(116, 80);
            this.cbDeleteZipFile.Name = "cbDeleteZipFile";
            this.cbDeleteZipFile.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbDeleteZipFile.Size = new System.Drawing.Size(94, 17);
            this.cbDeleteZipFile.TabIndex = 61;
            this.cbDeleteZipFile.Text = "Delete Zip File";
            this.cbDeleteZipFile.UseVisualStyleBackColor = true;
            // 
            // cbDeleteMargeFile
            // 
            this.cbDeleteMargeFile.AutoSize = true;
            this.cbDeleteMargeFile.Location = new System.Drawing.Point(464, 80);
            this.cbDeleteMargeFile.Name = "cbDeleteMargeFile";
            this.cbDeleteMargeFile.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbDeleteMargeFile.Size = new System.Drawing.Size(109, 17);
            this.cbDeleteMargeFile.TabIndex = 62;
            this.cbDeleteMargeFile.Text = "Delete Marge File";
            this.cbDeleteMargeFile.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(297, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 66;
            this.label6.Text = "BY:";
            // 
            // txtBarYaxis
            // 
            this.txtBarYaxis.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtBarYaxis.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtBarYaxis.Location = new System.Drawing.Point(327, 10);
            this.txtBarYaxis.Name = "txtBarYaxis";
            this.txtBarYaxis.Size = new System.Drawing.Size(47, 20);
            this.txtBarYaxis.TabIndex = 65;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(210, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 64;
            this.label7.Text = "BX :";
            // 
            // txtBarXaxis
            // 
            this.txtBarXaxis.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtBarXaxis.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtBarXaxis.Location = new System.Drawing.Point(244, 10);
            this.txtBarXaxis.Name = "txtBarXaxis";
            this.txtBarXaxis.Size = new System.Drawing.Size(47, 20);
            this.txtBarXaxis.TabIndex = 63;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(771, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 13);
            this.label8.TabIndex = 68;
            this.label8.Text = "Show Qr Bar On Page:";
            // 
            // txtShowBarQrCodeOnPage
            // 
            this.txtShowBarQrCodeOnPage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtShowBarQrCodeOnPage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtShowBarQrCodeOnPage.Location = new System.Drawing.Point(892, 28);
            this.txtShowBarQrCodeOnPage.Name = "txtShowBarQrCodeOnPage";
            this.txtShowBarQrCodeOnPage.Size = new System.Drawing.Size(47, 20);
            this.txtShowBarQrCodeOnPage.TabIndex = 67;
            // 
            // cbIsOsl
            // 
            this.cbIsOsl.AutoSize = true;
            this.cbIsOsl.Location = new System.Drawing.Point(116, 208);
            this.cbIsOsl.Name = "cbIsOsl";
            this.cbIsOsl.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbIsOsl.Size = new System.Drawing.Size(52, 17);
            this.cbIsOsl.TabIndex = 69;
            this.cbIsOsl.Text = "Is Osl";
            this.cbIsOsl.UseVisualStyleBackColor = true;
            // 
            // cbIsAlpha
            // 
            this.cbIsAlpha.AutoSize = true;
            this.cbIsAlpha.Location = new System.Drawing.Point(174, 209);
            this.cbIsAlpha.Name = "cbIsAlpha";
            this.cbIsAlpha.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbIsAlpha.Size = new System.Drawing.Size(64, 17);
            this.cbIsAlpha.TabIndex = 70;
            this.cbIsAlpha.Text = "Is Alpha";
            this.cbIsAlpha.UseVisualStyleBackColor = true;
            // 
            // cbShowCodeNumber
            // 
            this.cbShowCodeNumber.AutoSize = true;
            this.cbShowCodeNumber.Location = new System.Drawing.Point(116, 175);
            this.cbShowCodeNumber.Name = "cbShowCodeNumber";
            this.cbShowCodeNumber.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbShowCodeNumber.Size = new System.Drawing.Size(98, 17);
            this.cbShowCodeNumber.TabIndex = 71;
            this.cbShowCodeNumber.Text = "Show Code No";
            this.cbShowCodeNumber.UseVisualStyleBackColor = true;
            // 
            // cbShowCodeNumberWithExamNo
            // 
            this.cbShowCodeNumberWithExamNo.AutoSize = true;
            this.cbShowCodeNumberWithExamNo.Location = new System.Drawing.Point(220, 174);
            this.cbShowCodeNumberWithExamNo.Name = "cbShowCodeNumberWithExamNo";
            this.cbShowCodeNumberWithExamNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbShowCodeNumberWithExamNo.Size = new System.Drawing.Size(103, 17);
            this.cbShowCodeNumberWithExamNo.TabIndex = 72;
            this.cbShowCodeNumberWithExamNo.Text = "Show Exam Info";
            this.cbShowCodeNumberWithExamNo.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(341, 176);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 13);
            this.label17.TabIndex = 74;
            this.label17.Text = "1st Page No";
            // 
            // txtShowCodeNumberOnPage
            // 
            this.txtShowCodeNumberOnPage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtShowCodeNumberOnPage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtShowCodeNumberOnPage.Location = new System.Drawing.Point(410, 173);
            this.txtShowCodeNumberOnPage.Name = "txtShowCodeNumberOnPage";
            this.txtShowCodeNumberOnPage.Size = new System.Drawing.Size(47, 20);
            this.txtShowCodeNumberOnPage.TabIndex = 73;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(471, 176);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 13);
            this.label18.TabIndex = 76;
            this.label18.Text = "X:";
            // 
            // txtCodeNumberX
            // 
            this.txtCodeNumberX.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtCodeNumberX.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCodeNumberX.Location = new System.Drawing.Point(494, 173);
            this.txtCodeNumberX.Name = "txtCodeNumberX";
            this.txtCodeNumberX.Size = new System.Drawing.Size(47, 20);
            this.txtCodeNumberX.TabIndex = 75;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(555, 176);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 13);
            this.label19.TabIndex = 78;
            this.label19.Text = "Y:";
            // 
            // txtCodeNumberY
            // 
            this.txtCodeNumberY.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtCodeNumberY.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCodeNumberY.Location = new System.Drawing.Point(578, 173);
            this.txtCodeNumberY.Name = "txtCodeNumberY";
            this.txtCodeNumberY.Size = new System.Drawing.Size(47, 20);
            this.txtCodeNumberY.TabIndex = 77;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 172);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(98, 13);
            this.label20.TabIndex = 79;
            this.label20.Text = "Code Show Config:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(51, 12);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 13);
            this.label21.TabIndex = 80;
            this.label21.Text = "Bar Config:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(56, 44);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 13);
            this.label22.TabIndex = 81;
            this.label22.Text = "Qr Config:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(50, 76);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 13);
            this.label23.TabIndex = 82;
            this.label23.Text = "File Option:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(50, 208);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(58, 13);
            this.label24.TabIndex = 83;
            this.label24.Text = "Api Config:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(45, 140);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(65, 13);
            this.label25.TabIndex = 84;
            this.label25.Text = "Print Option:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(17, 108);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(93, 13);
            this.label26.TabIndex = 85;
            this.label26.Text = "PDF Page Option:";
            // 
            // cbDownloadNewFile
            // 
            this.cbDownloadNewFile.AutoSize = true;
            this.cbDownloadNewFile.Location = new System.Drawing.Point(117, 140);
            this.cbDownloadNewFile.Name = "cbDownloadNewFile";
            this.cbDownloadNewFile.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbDownloadNewFile.Size = new System.Drawing.Size(92, 17);
            this.cbDownloadNewFile.TabIndex = 86;
            this.cbDownloadNewFile.Text = "Download Zip";
            this.cbDownloadNewFile.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(851, 176);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 13);
            this.label27.TabIndex = 92;
            this.label27.Text = "Y:";
            // 
            // txtCodeNumberY2nd
            // 
            this.txtCodeNumberY2nd.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtCodeNumberY2nd.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCodeNumberY2nd.Location = new System.Drawing.Point(874, 173);
            this.txtCodeNumberY2nd.Name = "txtCodeNumberY2nd";
            this.txtCodeNumberY2nd.Size = new System.Drawing.Size(47, 20);
            this.txtCodeNumberY2nd.TabIndex = 91;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(767, 176);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 13);
            this.label28.TabIndex = 90;
            this.label28.Text = "X:";
            // 
            // txtCodeNumberX2nd
            // 
            this.txtCodeNumberX2nd.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtCodeNumberX2nd.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCodeNumberX2nd.Location = new System.Drawing.Point(790, 173);
            this.txtCodeNumberX2nd.Name = "txtCodeNumberX2nd";
            this.txtCodeNumberX2nd.Size = new System.Drawing.Size(47, 20);
            this.txtCodeNumberX2nd.TabIndex = 89;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(632, 176);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(70, 13);
            this.label29.TabIndex = 88;
            this.label29.Text = "2nd Page No";
            // 
            // txtShowCodeNumberOnPage2nd
            // 
            this.txtShowCodeNumberOnPage2nd.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtShowCodeNumberOnPage2nd.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtShowCodeNumberOnPage2nd.Location = new System.Drawing.Point(706, 173);
            this.txtShowCodeNumberOnPage2nd.Name = "txtShowCodeNumberOnPage2nd";
            this.txtShowCodeNumberOnPage2nd.Size = new System.Drawing.Size(47, 20);
            this.txtShowCodeNumberOnPage2nd.TabIndex = 87;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1051, 328);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.txtCodeNumberY2nd);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.txtCodeNumberX2nd);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.txtShowCodeNumberOnPage2nd);
            this.Controls.Add(this.cbDownloadNewFile);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtCodeNumberY);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtCodeNumberX);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtShowCodeNumberOnPage);
            this.Controls.Add(this.cbShowCodeNumberWithExamNo);
            this.Controls.Add(this.cbShowCodeNumber);
            this.Controls.Add(this.cbIsAlpha);
            this.Controls.Add(this.cbIsOsl);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtShowBarQrCodeOnPage);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtBarYaxis);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBarXaxis);
            this.Controls.Add(this.cbDeleteMargeFile);
            this.Controls.Add(this.cbDeleteZipFile);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtMargePdf);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtMaxPage);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtQrYaxis);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtQrXaxis);
            this.Controls.Add(this.btnSaveConfig);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMinPage);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtQrHeight);
            this.Controls.Add(this.txtQrWidth);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtQrDipY);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtBarDipY);
            this.Controls.Add(this.txtQrDipX);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtBarDipX);
            this.Controls.Add(this.txtBarHeight);
            this.Controls.Add(this.txtBarWidth);
            this.Controls.Add(this.cbDeleteUnzipFile);
            this.Controls.Add(this.cbDeleteAllGeneratedFile);
            this.Controls.Add(this.cbIsPrint);
            this.Controls.Add(this.cbbarNo);
            this.Controls.Add(this.cbIsQrCode);
            this.Controls.Add(this.cbIsBarcode);
            this.Controls.Add(this.btnPrintTest);
            this.Name = "TestForm";
            this.Text = "TestForm";
            this.Load += new System.EventHandler(this.TestForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bsPrintingConfigInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrintTest;
        private System.Windows.Forms.TextBox txtQrHeight;
        private System.Windows.Forms.TextBox txtQrWidth;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtQrDipY;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBarDipY;
        private System.Windows.Forms.TextBox txtQrDipX;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBarDipX;
        private System.Windows.Forms.TextBox txtBarHeight;
        private System.Windows.Forms.TextBox txtBarWidth;
        private System.Windows.Forms.CheckBox cbDeleteUnzipFile;
        private System.Windows.Forms.CheckBox cbDeleteAllGeneratedFile;
        private System.Windows.Forms.CheckBox cbIsPrint;
        private System.Windows.Forms.CheckBox cbbarNo;
        private System.Windows.Forms.CheckBox cbIsQrCode;
        private System.Windows.Forms.CheckBox cbIsBarcode;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.BindingSource bsPrintingConfigInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMinPage;
        private System.Windows.Forms.Button btnSaveConfig;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtQrXaxis;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtQrYaxis;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMaxPage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMargePdf;
        private System.Windows.Forms.CheckBox cbDeleteZipFile;
        private System.Windows.Forms.CheckBox cbDeleteMargeFile;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBarYaxis;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBarXaxis;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtShowBarQrCodeOnPage;
        private System.Windows.Forms.CheckBox cbIsOsl;
        private System.Windows.Forms.CheckBox cbIsAlpha;
        private System.Windows.Forms.CheckBox cbShowCodeNumber;
        private System.Windows.Forms.CheckBox cbShowCodeNumberWithExamNo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtShowCodeNumberOnPage;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtCodeNumberX;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtCodeNumberY;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox cbDownloadNewFile;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCodeNumberY2nd;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtCodeNumberX2nd;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtShowCodeNumberOnPage2nd;

    }
}