﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.Hr
{
    public interface IManagementTimeDao : IBaseDao<ManagementTime, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<ManagementTime> LoadManagementTimeList(int start, int length, string orderBy, string orderDir, List<int> memberPinList, List<long> authoMemberIdList, DateTime? workingDateFrom, DateTime? workingDateTo, long? createdByUserId);

        #endregion

        #region Others Function

        int GetManagementTimeCount(string orderBy, string orderDir, List<int> memberPinList, List<long> authoMemberIdList, DateTime? workingDateFrom, DateTime? workingDateTo, long? createdByUserId);

        #endregion

        #region Helper Function

        #endregion

    }
    public class ManagementTimeDao : BaseDao<ManagementTime, long>, IManagementTimeDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<ManagementTime> LoadManagementTimeList(int start, int length, string orderBy, string orderDir, List<int> memberPinList, List<long> authoMemberIdList, DateTime? workingDateFrom, DateTime? workingDateTo, long? createdByUserId)
        {
            ICriteria criteria = GetManagementTime(orderBy, orderDir, memberPinList, authoMemberIdList, workingDateFrom, workingDateTo, createdByUserId);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<ManagementTime>();
        }

        #endregion

        #region Others Function

        public int GetManagementTimeCount(string orderBy, string orderDir, List<int> memberPinList, List<long> authoMemberIdList, DateTime? workingDateFrom, DateTime? workingDateTo, long? createdByUserId)
        {
            ICriteria criteria = GetManagementTime(orderBy, orderDir, memberPinList, authoMemberIdList, workingDateFrom, workingDateTo, createdByUserId, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function

        private ICriteria GetManagementTime(string orderBy, string orderDir, List<int> memberPinList, List<long> authoMemberIdList, DateTime? workingDateFrom, DateTime? workingDateTo, long? createdByUserId, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<ManagementTime>().Add(Restrictions.Not(Restrictions.Eq("Status", ManagementTime.EntityStatus.Delete)));
            criteria.CreateAlias("TeamMember", "teamMember");
            //if (teamMemberPin!=null)
            //{
            //    criteria.Add(Restrictions.Eq("teamMember.Pin", teamMemberPin));
            //}
            if (memberPinList != null && memberPinList.Any() && !memberPinList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("teamMember.Pin", memberPinList));
            }
            if (authoMemberIdList != null && authoMemberIdList.Any())
            {
                criteria.Add(Restrictions.In("teamMember.Id", authoMemberIdList));
            }
            if (workingDateFrom != null)
            {
                criteria.Add(Restrictions.Gt("WorkDate", workingDateFrom.Value.Date));
            }
            if (workingDateTo != null)
            {
                criteria.Add(Restrictions.Le("WorkDate", workingDateTo.Value.Date));
            }
            if (createdByUserId !=null)
            {
                criteria.Add(Restrictions.Eq("CreateBy", Convert.ToInt64(createdByUserId)));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }

        #endregion
        
    }

}
