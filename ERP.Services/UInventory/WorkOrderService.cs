using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Engine;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.UInventory;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.UInventory
{
    public interface IWorkOrderService : IBaseService
    {

        #region Operational Function

        bool SaveOrUpdate(WorkOrder workOrder);//Direct Work Order
        bool CancelWorkOrder(long id);//Direct Work Order
        void WorkOrderSave(WorkOrder workOrder);//Quotation with work order
        bool IsCancel(long id);//Quotation with work order

        #endregion

        #region Single Instances Loading Function

        WorkOrder GetWorkOrder(long id);
        WorkOrder GetByWorkOrderNo(string workOrderNo);

        #endregion

        #region List Loading Function

        IList<WorkOrderDto> LoadWorkOrder(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, string itemName = "", int? status = null);
        IList<WorkOrderReportDto> LoadWorkOrderReport(int start, int length, List<UserMenu> userMenus, long organizationId, List<long> branchIdList, int workOrderType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, int workOrderstatus, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Others Function

        int WorkOrderCount(List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, string itemName = "", int? status = null);
        string GetLastWorkOrderNo(long branchId);
        int GetWorkOrderReportCount(List<UserMenu> userMenus, long organizationId, List<long> branchIdList, int workOrderType, List<long> purposeList,
            List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int workOrderstatus, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Helper Function
        #endregion

    }
    public class WorkOrderService : BaseService, IWorkOrderService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IWorkOrderDao _workOrderDao;
        private readonly ISupplierPriceQuoteDao _supplierPriceQuoteDao;
        public WorkOrderService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _workOrderDao = new WorkOrderDao { Session = session };
            _supplierPriceQuoteDao = new SupplierPriceQuoteDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        //Direct Work Order
        public bool SaveOrUpdate(WorkOrder workOrder)
        {
            try
            {
                if (workOrder == null)
                    throw new NullObjectException("Work order can not be null.");
                
                ITransaction transaction;
                using (transaction = Session.BeginTransaction())
                {
                    var branchId = workOrder.Branch.Id;
                    var sequentialNo = GenerateSeqeuntialNo(branchId);
                    workOrder.WorkOrderDate = DateTime.Now;
                    workOrder.WorkOrderNo = workOrder.Branch.Organization.ShortName.Substring(0, 3) +
                                            workOrder.Branch.Code + "WO" + sequentialNo;
                    ModelValidationCheck(workOrder);
                    CheckBeforeSave(workOrder);
                    workOrder.Status = WorkOrder.EntityStatus.Active;
                    workOrder.WorkOrderDetails[0].Status = WorkOrderDetails.EntityStatus.Active;

                    workOrder.WorkOrderDetails[0].CreateBy = GetCurrentUserId();
                    workOrder.WorkOrderDetails[0].ModifyBy = GetCurrentUserId();
                    
                    _workOrderDao.SaveOrUpdate(workOrder);
                    transaction.Commit();
                    return true;
                }
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool CancelWorkOrder(long id)
        {
            ITransaction transaction = null;
            try
            {
                var tempWorkOrderObj = _workOrderDao.LoadById(id);
                CheckBeforeDelete(tempWorkOrderObj);
                using (transaction = Session.BeginTransaction())
                {
                    tempWorkOrderObj.WorkOrderStatus = WorkOrderStatus.Cancelled;
                    _workOrderDao.Update(tempWorkOrderObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        //Quotation with work order
        public void WorkOrderSave(WorkOrder workOrder)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeWorkOrderSave(workOrder);
                using (transaction = Session.BeginTransaction())
                {

                    foreach (var workOrderDetail in workOrder.WorkOrderDetails)
                    {
                        workOrderDetail.CreateBy = GetCurrentUserId();
                        workOrderDetail.ModifyBy = GetCurrentUserId();
                        workOrderDetail.Status = WorkOrderDetails.EntityStatus.Active;
                        foreach (var workOrderCriteria in workOrderDetail.WorkOrderCriteriaList)
                        {
                            workOrderCriteria.CreateBy = GetCurrentUserId();
                            workOrderCriteria.ModifyBy = GetCurrentUserId();
                            workOrderCriteria.Status = WorkOrderCriteria.EntityStatus.Active;
                        }
                    }
                    _workOrderDao.SaveOrUpdate(workOrder);
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        private void CheckBeforeWorkOrderSave(WorkOrder workOrder)
        {
            try
            {
                if (workOrder == null)
                    throw new NullObjectException("Work order can't empty");

                CustomModelValidationCheck.DataAnnotationCheck(workOrder, new WorkOrder());

                if (workOrder.WorkOrderDetails == null || workOrder.WorkOrderDetails.Count <= 0)
                    throw new NullObjectException("Work order Details can't empty");

                if (workOrder.WorkOrderStatus != WorkOrderStatus.Issued)
                    throw new InvalidDataException("This workOrder is Invalid");

                if (workOrder.Quotation == null)
                    throw new NullObjectException("Quotation can't empty");

                if (workOrder.Quotation.QuotationStatus == QuotationStatus.Cancelled ||
                    workOrder.Quotation.QuotationStatus == QuotationStatus.SubmissionClosed ||
                    workOrder.Quotation.QuotationStatus == QuotationStatus.Running ||
                    workOrder.Quotation.QuotationStatus == QuotationStatus.NotPublished)
                {
                    throw new InvalidDataException("Quotation is invalid");
                }

                if (workOrder.OrderedQuantity < 1)
                    throw new NullObjectException("Order quantity can't empty or negative");

                var totalPreviosQuantity = workOrder.Quotation.WorkOrderList.Where(x => x.Status == WorkOrder.EntityStatus.Active && x.WorkOrderStatus != WorkOrderStatus.Cancelled)
                    .Sum(x => x.OrderedQuantity);

                var totalOrderQuantity = workOrder.OrderedQuantity + totalPreviosQuantity;

                if (totalOrderQuantity > workOrder.Quotation.QuotationQuantity)
                    throw new InvalidDataException("Quotation remaining quantity empty");

                if (totalOrderQuantity == workOrder.Quotation.QuotationQuantity && workOrder.Quotation.QuotationStatus != QuotationStatus.Issued)
                    throw new InvalidDataException("This Quotation is Invalid");

                if (totalOrderQuantity < workOrder.Quotation.QuotationQuantity && workOrder.Quotation.QuotationStatus != QuotationStatus.PertialIssued)
                    throw new InvalidDataException("This Quotation is Invalid");

                if (workOrder.WorkOrderDetails.Count(x => x.Item == null) > 0)
                    throw new NullObjectException("Item can't empty");

                if (workOrder.WorkOrderDetails.Count(x => x.WorkOrderCriteriaList == null || x.WorkOrderCriteriaList.Count < 1) > 0)
                    throw new NullObjectException("WorkOrder criteria can't empty");

                var workOrderCriteriaList = workOrder.WorkOrderDetails.Select(x => x.WorkOrderCriteriaList
                    .Where(y => y.Criteria == null || y.CriteriaValue == null || y.WorkOrderDetail == null)).ToList();

                if (workOrderCriteriaList.Count < 1)
                    throw new NullObjectException("WorkOrder specification can't empty");

                if (workOrder.WorkOrderDetails.Count(x => x.Item.ItemType == (int)ItemType.CommonItem && (x.Program != null || x.Session != null)) > 0)
                    throw new NullObjectException("Program and session must be N/A if your item type common");

                if (workOrder.WorkOrderDetails.Count(x => x.Item.ItemType == (int)ItemType.ProgramItem && (x.Program == null || x.Session == null)) > 0)
                    throw new NullObjectException("Program and session can't N/A if your item type program type");

                if (workOrder.WorkOrderDetails.Count(x => x.Item.ItemType == (int)ItemType.ProgramItem && x.WorkOrder.Quotation.Purpose != x.Purpose &&
                    (x.PurposeProgram != x.WorkOrder.Quotation.PurposeProgram || x.PurposeSession != x.WorkOrder.Quotation.PurposeSession)) > 0)
                    throw new InvalidDataException("if item type program and purpose N/A so Purpose of program and session must be N/A");

                if (workOrder.WorkOrderDetails.Count(x => x.Item.ItemType == (int)ItemType.CommonItem && x.WorkOrder.Quotation.Purpose != x.Purpose &&
                    (x.PurposeProgram != x.WorkOrder.Quotation.PurposeProgram || x.PurposeSession != x.WorkOrder.Quotation.PurposeSession)) > 0)
                    throw new InvalidDataException("if item type common and quotation purpose does't match work order purpose");

            }
            catch (MessageException) { throw; }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public WorkOrder GetWorkOrder(long id)
        {
            try
            {
                if (id <= 0) throw new InvalidDataException("Invalid work Order Id");
                var workOrder = _workOrderDao.LoadById(id);
                return workOrder;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public WorkOrder GetByWorkOrderNo(string workOrderNo)
        {
            try
            {
                if (String.IsNullOrEmpty(workOrderNo)) throw new InvalidDataException("Invalid work Order No.");
                return _workOrderDao.GetByWorkOrderNo(workOrderNo);
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw e;
            }
        }

        #endregion

        #region List Loading Function

        public IList<WorkOrderDto> LoadWorkOrder(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, string itemName = "", int? status = null)
        {
            try
            {
                var organizationIdList = organizationId != null
                    ? _commonHelper.ConvertIdToList(organizationId.Value)
                    : null;
                var branchIdList = branchId != null ? _commonHelper.ConvertIdToList(branchId.Value) : null;

                if (userMenu == null)
                    throw new InvalidDataException("You are not authorized to see this report.");

                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null,
                    branchIdList);
                if (authBranchIdList == null || authBranchIdList.Count == 0)
                {
                    throw new InvalidDataException("You are not authorized to see this report.");
                }
                var workOrderList = _workOrderDao.LoadWorkOrder(start, length, orderBy, orderDir, authBranchIdList,
                    itemName, status);
                return workOrderList;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<WorkOrderReportDto> LoadWorkOrderReport(int start, int length, List<UserMenu> userMenus, long organizationId,
            List<long> branchIdList, int workOrderType, List<long> purposeList, List<long> itemGroupIdList,
            List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, int workOrderstatus, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if(userMenus==null)
                    throw new MemberAccessException("You are not authorized to access.");

                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, null, branchIdList);
                if (authBranchIdList == null || authBranchIdList.Count == 0)
                {
                    throw new MemberAccessException("You are not authorized to access.");
                }
                var workOrderList = _workOrderDao.LoadWorkOrderReport(start, length, authBranchIdList.Distinct().ToList(), workOrderType, purposeList.Distinct().ToList(), itemGroupIdList.Distinct().ToList(),
                    itemIdList.Distinct().ToList(), programIdList.Distinct().ToList(), sessionIdList.Distinct().ToList(), workOrderstatus, dateFrom, dateTo);
                return workOrderList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int WorkOrderCount(List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, string itemName = "", int? status = null)
        {
            try
            {
                if(userMenu==null)
                    throw new MemberAccessException("You are not authorized to access.");

                var organizationIdList = organizationId != null ? _commonHelper.ConvertIdToList(organizationId.Value) : null;
                var branchIdList = branchId != null ? _commonHelper.ConvertIdToList(branchId.Value) : null;
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchIdList);
                if (authBranchIdList == null || authBranchIdList.Count == 0)
                {
                    throw new MemberAccessException("You are not authorized to access.");
                }
                int count = _workOrderDao.WorkOrderCount(authBranchIdList, itemName, status);
                return count;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public string GetLastWorkOrderNo(long branchId)
        {
            try
            {
                var workOrderObj = Session.QueryOver<WorkOrder>().Where(x => x.Branch.Id == branchId).OrderBy(x => x.Id).Desc.List<WorkOrder>().FirstOrDefault();
                return workOrderObj == null ? "" : workOrderObj.WorkOrderNo;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetWorkOrderReportCount(List<UserMenu> userMenus, long organizationId, List<long> branchIdList, int workOrderType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, int workOrderstatus, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, null, branchIdList);
                var count = _workOrderDao.GetWorkOrderReportCount(authBranchIdList.Distinct().ToList(), workOrderType, purposeList.Distinct().ToList(), itemGroupIdList.Distinct().ToList(),
                    itemIdList.Distinct().ToList(), programIdList.Distinct().ToList(), sessionIdList.Distinct().ToList(), workOrderstatus, dateFrom, dateTo);
                return count;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool IsCancel(long id)
        {
            ITransaction trans = null;
            try
            {
                var obj = _workOrderDao.LoadById(id);
                if (obj == null)
                    throw new NullObjectException("Work order is not valid.");

                if (obj.GoodsReceiveList != null && obj.GoodsReceiveList.Any())
                    throw new InvalidDataException("Already work order received.");

                using (trans = Session.BeginTransaction())
                {
                    obj.WorkOrderStatus = WorkOrderStatus.Cancelled;
                    var supplierPriceQuote = obj.Supplier.SupplierPriceQuoteList.FirstOrDefault(x => x.Quotation == obj.Quotation);
                    if (supplierPriceQuote != null)
                        supplierPriceQuote.PriceQuoteStatus = (int)PriceQouteStatus.Cancelled;
                    else throw new NullObjectException("Supplier can't empty.");

                    _workOrderDao.Update(obj);
                    _supplierPriceQuoteDao.Update(supplierPriceQuote);
                    trans.Commit();
                    return true;
                }
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        #endregion

        #region Helper function

        private string GenerateSeqeuntialNo(long branchId)
        {
            try
            {
                string workOrderNo = _workOrderDao.GenerateSeqeuntialNo(branchId);
                int zeroCount = 6 - workOrderNo.Length;
                var zeroString = "";
                for (int i = 0; i < zeroCount; i++)
                {
                    zeroString += "0";
                }
                workOrderNo = zeroString + workOrderNo;
                return workOrderNo;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        //Direct Work Order
        private void CheckBeforeSave(WorkOrder workOrder)
        {
            try
            {
                if (workOrder.WorkOrderDetails == null)
                    throw new InvalidDataException("Work order details is requird.");

                var workOrderDetails = workOrder.WorkOrderDetails[0];

                if (workOrderDetails.Item == null)
                    throw new InvalidDataException("Item is requird.");
                
                var itemType = workOrderDetails.Item.ItemType;
                if (workOrderDetails.WorkOrderCriteriaList == null || workOrderDetails.WorkOrderCriteriaList[0].Criteria == null)
                    throw new InvalidDataException("criteria is requird.");
                
                if (workOrderDetails.WorkOrderCriteriaList[0].CriteriaValue == null)
                    throw new InvalidDataException("Criteria value is requird.");
                
                if (workOrder.Branch != null)
                {
                    var purpose = workOrderDetails.Purpose;
                    if (!workOrder.Branch.IsCorporate && purpose == null)
                        throw new InvalidDataException("Purpose can not be null");
                    
                }
                if (workOrderDetails.Item.ItemType == (int)ItemType.CommonItem && (workOrderDetails.Program != null || workOrderDetails.Session != null))
                    throw new InvalidDataException("Program and session should be N/A");
                

                if (workOrderDetails.Item.ItemType == (int)ItemType.ProgramItem &&
                    (workOrderDetails.Program == null || workOrderDetails.Session == null))
                {
                    throw new InvalidDataException("Program and session should not be N/A");
                }

                if (itemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose == null && (workOrderDetails.PurposeProgram != null && workOrderDetails.PurposeSession != null))
                    throw new InvalidDataException("Program and Session must be selected while item type is program item.");

                if (itemType == (int)ItemType.ProgramItem && (workOrderDetails.Purpose > 0 && workOrderDetails.Purpose <= 5) && (workOrderDetails.PurposeProgram == null || workOrderDetails.PurposeSession == null))
                    throw new InvalidDataException("Program and Session must be selected while item type is program item.");


                if (itemType == (int)ItemType.CommonItem && (workOrderDetails.Purpose > 0 && workOrderDetails.Purpose <= 3) && (workOrderDetails.PurposeProgram == null || workOrderDetails.PurposeSession == null))
                    throw new InvalidDataException("Program and Session must be selected while purpose is selected by program or marketing or giftAndPromotion purpose " +
                                                   "and item type common item");

                if (itemType == (int)ItemType.CommonItem && (workOrderDetails.Purpose == null || workOrderDetails.Purpose <= 0) && (workOrderDetails.PurposeProgram != null || workOrderDetails.PurposeSession != null))
                    throw new InvalidDataException("Program and Session must be selected by N/A while item type is common item and purpose is N/A.");

            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public void ModelValidationCheck(WorkOrder workOrderObj)
        {
            var validationResult = ValidationHelper.ValidateEntity<WorkOrder, WorkOrder>(workOrderObj);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private void CheckBeforeDelete(WorkOrder tempWorkOrderObj)
        {
            try
            {
                if (tempWorkOrderObj.GoodsReceiveList.Count > 0)
                {
                    throw new DependencyException("Goods already fully or partially received.");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion
    }
}

