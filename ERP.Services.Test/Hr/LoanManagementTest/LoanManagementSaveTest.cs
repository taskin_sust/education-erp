﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.LoanManagementTest
{
    public interface ILoanManagementOperationalTest
    {
        #region zakat validation

        void Should_Return_Invalid_Zakat_PaymentType_Exception();
        void Should_Return_Invalid_ZakatClosingDate_Exception();
        void Should_Return_Invalid_ZakatEffectiveDate_Exception();
        void Should_Return_Invalid_Zakat_Funded_Exception();

        #endregion

        #region Csr validation

        void Should_Return_Invalid_Csr_PaymentType_Exception();
        void Should_Return_Invalid_CsrClosingDate_Exception();
        void Should_Return_Invalid_CsrEffectiveDate_Exception();

        #endregion

        #region MemberLoan Validation

        void Should_Return_Invalid_RefundedAmount_Exception();
        void Should_Return_Invalid_RefundStartMonth_Exception();
        void Should_Return_ApprovedAmount_GreaterThan_RequestedAmount_Exception();
        void Should_Save_successfully();

        #endregion

    }

    [Trait("Area", "Hr")]
    [Trait("Service", "MemberLoanApplication")]
    public class LoanManagementSaveTest : LoanManagementBaseTest, ILoanManagementOperationalTest
    {
        #region zakat validation

        [Fact]
        public void Should_Return_Invalid_Zakat_PaymentType_Exception()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanZakatFactory.Object.PaymentType = 0;
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            Assert.Throws<InvalidDataException>(() => _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_ZakatClosingDate_Exception()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanZakatFactory.Object.EffectiveDate = DateTime.Now.AddDays(5);
            _memberLoanZakatFactory.Object.ClosingDate = DateTime.Now;
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            Assert.Throws<InvalidDataException>(() => _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_ZakatEffectiveDate_Exception()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanZakatFactory.Object.EffectiveDate = new DateTime(2017, 07, 23);
            _memberLoanZakatFactory.Object.ClosingDate = new DateTime(2015, 07, 23);

            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            Assert.Throws<InvalidDataException>(() => _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_Zakat_Funded_Exception()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanZakatFactory.Object.FundedBy = 0;

            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            Assert.Throws<InvalidDataException>(() => _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object));
        }

        #endregion

        #region Csr validation

        [Fact]
        public void Should_Return_Invalid_Csr_PaymentType_Exception()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanCsrFactory.Object.PaymentType = 0;
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            Assert.Throws<InvalidDataException>(() => _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_CsrClosingDate_Exception()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanCsrFactory.Object.EffectiveDate = DateTime.Now.AddDays(5);
            _memberLoanCsrFactory.Object.ClosingDate = DateTime.Now;
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            Assert.Throws<InvalidDataException>(() => _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_CsrEffectiveDate_Exception()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanCsrFactory.Object.EffectiveDate = new DateTime(2015, 07, 23);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            Assert.Throws<InvalidDataException>(() => _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object));
        }

        #endregion

        #region MemberLoan Validation

        [Fact]
        public void Should_Return_Invalid_RefundedAmount_Exception()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanFactory.Object.MonthlyRefund = 15000;
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            Assert.Throws<InvalidDataException>(() => _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_RefundStartMonth_Exception()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanFactory.Object.RefundStart = new DateTime(2015, 7, 23);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            Assert.Throws<InvalidDataException>(() => _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object));
        }

        [Fact(Skip = "Logic Change-- Approved amount can be greater than Req amount")]
        public void Should_Return_ApprovedAmount_GreaterThan_RequestedAmount_Exception()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanFactory.Object.LoanApprovedAmount = 15000;
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            Assert.Throws<InvalidDataException>(() => _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object));
        }

        [Fact]
        public void Should_Save_successfully()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object);
            Assert.True(_memberLoanApplicationFactory.Object.Id > 0);
        }

        #endregion
    }
}
