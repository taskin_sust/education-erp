﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentInfoHistoryDao : IBaseDao<StudentInfoHistory, long>
    {

    }
    public class StudentInfoHistoryDao : BaseDao<StudentInfoHistory, long>, IStudentInfoHistoryDao
    {
        public StudentInfoHistoryDao()
        {

        }
    }
}
