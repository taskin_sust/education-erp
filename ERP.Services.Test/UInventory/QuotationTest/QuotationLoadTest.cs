﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.QuotationTest
{
    interface IQuotationLoadTest
    {
        #region GetLastQuotationNo

        void Should_Return_GetLastQuotationNo_Successfully();
        void Should_Return_GetLastQuotationNo_Empty_String();

        #endregion

        #region GetQuotation
        void Should_Return_GetQuotation_Successfully();
        
        #endregion

        #region LoadQuotationCount

        void Should_Return_LoadQuotationCount_Successfully();
        void Should_Return_LoadQuotationCount_UserMenus_Invalid_Exception();
        void Should_Return_LoadQuotationCount_AuthOrganization_Invalid_Exception();
        void Should_Return_LoadQuotationCount_AuthBranch_Invalid_Exception();
        void Should_Return_LoadQuotationCount_Organizatrion_Invalid_Exception();
        void Should_Return_LoadQuotationCount_OrganizatrionAndUsermenu_Invalid_Exception();

        #endregion

        #region LoadQuotationReportCount

        void Should_Return_LoadQuotationReportCount_Successfully();
        void Should_Return_LoadQuotationReportCount_UserMenus_Invalid_Exception();
        void Should_Return_LoadQuotationReportCount_AuthOrganization_Invalid_Exception();
        void Should_Return_LoadQuotationReportCount_AuthBranch_Invalid_Exception();
        void Should_Return_LoadQuotationReportCount_Auth_Invalid_Exception();
        void Should_Return_LoadQuotationReportCount_SuccessFull_Quotation_Count();
        
        
        #endregion

        #region LoadQuotationReport

        void Should_Return_LoadQuotationReport_Successfully();
        void Should_Return_LoadQuotationReport_UserMenus_Invalid_Exception();
        void Should_Return_LoadQuotationReport_AuthOrganization_Invalid_Exception();
        void Should_Return_LoadQuotationReport_AuthBranch_Invalid_Exception();
        
        #endregion

        #region LoadQuotation

        void Should_Return_LoadQuotation_Successfully();
        void Should_Return_LoadQuotation_UserMenus_Invalid_Exception();
        void Should_Return_LoadQuotation_AuthOrganization_Invalid_Exception();
        void Should_Return_LoadQuotation_AuthBranch_Invalid_Exception();

        #endregion

    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "Quotation")]
    public class QuotationLoadTest : QuotationBaseTest, IQuotationLoadTest
    {
        #region GetLastQuotationNo

        [Fact]
        public void Should_Return_GetLastQuotationNo_Successfully()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var check = _quotationService.GetLastQuotationNo(_branchFactory.Object.Id);
            Assert.Contains("10", check);
        }

        [Fact]
        public void Should_Return_GetLastQuotationNo_Empty_String()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var check = _quotationService.GetLastQuotationNo(_branchFactory.Object.Id + 2);
            Assert.DoesNotContain("10", check);
        }

        #endregion

        #region GetQuotation

        [Fact]
        public void Should_Return_GetQuotation_Successfully()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithPurposeprogramAndPurposeSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();
            var obj = _quotationService.GetQuotation(_quotationFactory.Object.Id);
            var obj2 = _quotationService.GetQuotation(_quotationFactory.Object.Id, _itemFactory.Object.Id);
            Assert.NotNull(obj);
            Assert.NotNull(obj2);
        }

        #endregion

        #region LoadQuotationCount
        
        [Fact]
        public void Should_Return_LoadQuotationCount_Successfully()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            var count = _quotationService.LoadQuotationCount("", "", userMenu, organizationFactory.Object.Id.ToString(), "", "", "");
            Assert.Equal(10, count);
        }

        [Fact]
        public void Should_Return_LoadQuotationCount_UserMenus_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            Assert.Throws<InvalidDataException>(() => _quotationService.LoadQuotationCount("", "", null, organizationFactory.Object.Id.ToString(),
                _branchFactory.Object.Id.ToString(), "", ""));
        }

        [Fact]
        public void Should_Return_LoadQuotationCount_Organizatrion_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.LoadQuotationCount("", "", userMenu, (organizationFactory.Object.Id+1).ToString(),
                _branchFactory.Object.Id.ToString(), "", ""));
        }

        [Fact]
        public void Should_Return_LoadQuotationCount_OrganizatrionAndUsermenu_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            Assert.Throws<InvalidDataException>(() => _quotationService.LoadQuotationCount("", "", null, "",
                _branchFactory.Object.Id.ToString(), "", ""));
        }

        [Fact]
        public void Should_Return_LoadQuotationCount_AuthOrganization_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.LoadQuotationCount("", "", userMenu, (organizationFactory.Object.Id + 1).ToString(), (_branchFactory.Object.Id).ToString(), "", ""));
        }

        [Fact]
        public void Should_Return_LoadQuotationCount_AuthBranch_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.LoadQuotationCount("", "", userMenu, organizationFactory.Object.Id.ToString(), (_branchFactory.Object.Id + 1).ToString(), "", ""));
        }
        
        #endregion

        #region LoadQuotationReportCount

        [Fact]
        public void Should_Return_LoadQuotationReportCount_Successfully()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            var count = _quotationService.LoadQuotationReportCount(userMenu, organizationFactory.Object.Id.ToString(),
                null, null, null, null, null, (int)QuotationStatus.NotPublished, "", "");
            Assert.Equal(10, count);
        }

        [Fact]
        public void Should_Return_LoadQuotationReportCount_UserMenus_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();

            Assert.Throws<InvalidDataException>(
                () =>
                    _quotationService.LoadQuotationReportCount(null, organizationFactory.Object.Id.ToString(), new long[] { _branchFactory.Object.Id }, null, null, null, null,
                    (int)QuotationStatus.NotPublished, "", ""));
        }

        [Fact]
        public void Should_Return_LoadQuotationReportCount_AuthOrganization_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(
                () =>
                    _quotationService.LoadQuotationReportCount(userMenu, (organizationFactory.Object.Id + 1).ToString(), new long[] { _branchFactory.Object.Id }, null, null, null, null,
                    (int)QuotationStatus.NotPublished, "", ""));
        }

        [Fact]
        public void Should_Return_LoadQuotationReportCount_AuthBranch_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(
                () =>
                    _quotationService.LoadQuotationReportCount(userMenu, (organizationFactory.Object.Id).ToString(),
                    _branchFactory.SingleObjectList.Select(x => x.Id + 1).ToArray(), null, null, null, null, (int)QuotationStatus.NotPublished, "", ""));
        }

        [Fact]
        public void Should_Return_LoadQuotationReportCount_SuccessFull_Quotation_Count()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            var count = _quotationService.LoadQuotationReportCount(userMenu, "", null, null, null, null, null,
                (int)QuotationStatus.NotPublished, "", "");
            Assert.Equal(10, count);
        }

        [Fact]
        public void Should_Return_LoadQuotationReportCount_Auth_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);

            Assert.Throws<InvalidDataException>(
                () => _quotationService.LoadQuotationReportCount(userMenu, (organizationFactory.Object.Id + 1).ToString(), new long[] { _branchFactory.Object.Id + 1 }, null, null, null, null, (int)QuotationStatus.NotPublished, "", ""));
        }


        #endregion

        #region LoadQuotationReport

        [Fact]
        public void Should_Return_LoadQuotationReport_Successfully()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            var count = _quotationService.LoadQuotationReport(0, 100, "", "", userMenu, organizationFactory.Object.Id.ToString(),
                null, null, null, null, null, (int)QuotationStatus.NotPublished, "", "").Count();
            Assert.Equal(10, count);
        }

        [Fact]
        public void Should_Return_LoadQuotationReport_UserMenus_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();

            Assert.Throws<InvalidDataException>(
                () =>
                    _quotationService.LoadQuotationReport(0, 100, "", "", null,
                        organizationFactory.Object.Id.ToString(),
                        null, null, null, null, null, (int)QuotationStatus.NotPublished, "", ""));
        }

        [Fact]
        public void Should_Return_LoadQuotationReport_AuthOrganization_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(
                () =>
                    _quotationService.LoadQuotationReport(0, 100, "", "", userMenu,
                        (organizationFactory.Object.Id + 1).ToString(), null, null, null, null, null, (int)QuotationStatus.NotPublished, "", ""));
        }

        [Fact]
        public void Should_Return_LoadQuotationReport_AuthBranch_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(
                () =>
                    _quotationService.LoadQuotationReport(0, 100, "", "", userMenu,
                        (organizationFactory.Object.Id).ToString(), _branchFactory.SingleObjectList.Select(x => x.Id + 1).ToArray(), null, null, null, null, (int)QuotationStatus.NotPublished, "", ""));
        }

        #endregion

        #region LoadQuotation

        [Fact]
        public void Should_Return_LoadQuotation_Successfully()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate)
                .WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            var count = _quotationService.LoadQuotation(0, 100, "", "", userMenu, organizationFactory.Object.Id.ToString(), "", "", "").Count();
            Assert.Equal(10, count);
        }

        [Fact]
        public void Should_Return_LoadQuotation_UserMenus_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            Assert.Throws<InvalidDataException>(() => _quotationService.LoadQuotation(0, 100, "", "", null, organizationFactory.Object.Id.ToString(),
                _branchFactory.Object.Id.ToString(), "", "").Count());
        }

        [Fact]
        public void Should_Return_LoadQuotation_AuthOrganization_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.LoadQuotation(0, 100, "", "", userMenu, (organizationFactory.Object.Id + 1).ToString(),
                (_branchFactory.Object.Id).ToString(), "", "").Count());
        }

        [Fact]
        public void Should_Return_LoadQuotation_AuthBranch_Invalid_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _quotationFactory.CreateMore(10, organizationFactory.Object, _branchFactory.Object, _itemFactory.Object).Persist();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, _branchFactory.Object);
            Assert.Throws<InvalidDataException>(() => _quotationService.LoadQuotation(0, 100, "", "", userMenu, organizationFactory.Object.Id.ToString(), (_branchFactory.Object.Id + 1).ToString(), "", "").Count());
        }

        #endregion
        
    }
}
