/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

    // The toolbar groups arrangement, optimized for two toolbar rows.

    config.toolbarGroups = [
		{ name: 'document', groups: ['mode', 'document', 'doctools'] },
		{ name: 'clipboard', groups: ['clipboard', 'undo'] },
		{ name: 'editing', groups: ['find', 'selection', 'align', 'spellchecker', 'editing'] },
		{ name: 'links', groups: ['links'] },
		{ name: 'insert', groups: ['insert'] },
		{ name: 'forms', groups: ['forms'] },
		{ name: 'tools', groups: ['tools'] },
		{ name: 'others', groups: ['others'] },
		'/',
		{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
		{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
		{ name: 'styles', groups: ['styles'] },
		{ name: 'colors', groups: ['colors'] },
		{ name: 'about', groups: ['about'] }
    ];

    config.removeButtons = 'Subscript,Superscript,Cut,Copy,Paste,PasteFromWord,Undo,Redo,Scayt,Link,Unlink,Anchor,HorizontalRule,SpecialChar,Maximize,Strike,' +
        'RemoveFormat,NumberedList,Indent,Outdent,Blockquote,Styles,Format,About,Save,NewPage,Preview,Print,Templates,Find,Replace,SelectAll,Flash,Smiley,PageBreak,' +
        'Iframe,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CopyFormatting,BidiLtr,BidiRtl,Language,TextColor,BGColor,FontSize';


    //config.toolbarGroups = [
	//	{ name: 'document', groups: ['mode', 'document', 'doctools'] },
	//	{ name: 'clipboard', groups: ['clipboard', 'undo'] },
	//	{ name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
	//	{ name: 'links', groups: ['links'] },
	//	{ name: 'insert', groups: ['insert'] },
	//	{ name: 'forms', groups: ['forms'] },
	//	{ name: 'tools', groups: ['tools'] },
	//	{ name: 'others', groups: ['others'] },
	//	{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
	//	{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
	//	{ name: 'styles', groups: ['styles'] },
	//	{ name: 'colors', groups: ['colors'] },
	//	{ name: 'about', groups: ['about'] }
    //];

    //config.removeButtons = 'Subscript,Superscript,Cut,Copy,Paste,PasteFromWord,Undo,Redo,Scayt,Link,Unlink,Anchor,Table,HorizontalRule,SpecialChar,Maximize,Strike,RemoveFormat,NumberedList,BulletedList,Indent,Outdent,Blockquote,Styles,Format,About';


	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	//config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	//config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
    //config.removeDialogTabs = 'image:advanced;link:advanced';


	config.mathJaxLib = '/Content/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML';
	config.extraPlugins = 'justify,lineutils,widgetselection,widget,mathjax';
};
