using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class InstituteMap : BaseClassMap<Institute, long>
    {
        
        public InstituteMap() {
			Table("Institute");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");

            Map(x => x.Name);
			Map(x => x.ShortName).Column("ShortName");
			Map(x => x.District).Column("District");
			//Map(x => x.Thana).Column("Thana");
			Map(x => x.Eiin).Column("Eiin");
			Map(x => x.Website).Column("Website");
            Map(x => x.Rank);
            
            References(x => x.InstituteCategory).Column("InstituteCategoryId");

            HasMany(x => x.StudentUniversityInfos).KeyColumn("InstituteId").Cascade.AllDeleteOrphan().Inverse();
          
        }
    }
}
