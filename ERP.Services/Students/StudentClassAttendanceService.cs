﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using log4net;
using NHibernate;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.Dao.Teachers;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentClassAttendanceService : IBaseService
    {
        #region Operational Function
        bool ClearClassAttendace(string lectureIds, string teacherIds,string userIds, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
        List<BusinessModel.Dto.ResponseMessage> Save(List<BusinessModel.Entity.UserAuth.UserMenu> _userMenu,long programId, long lectureId, long teacherId, DateTime heldDate, DateTime startDate, string teacherLate, string[] studentRolls, string[] studentFeedback, out List<string> successRollsList);
        IList<IndividualClassAttendanceDto> GetIndividualReport(string programRoll, int attendanceType, DateTime dateFrom, DateTime dateTo);
        IList<ClassEvaluationReportDto> LoadClassEvaluation(List<UserMenu> usermenu, long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIdList, long[] lectureIdList, long[] teacherIdList, DateTime dateTime1, DateTime dateTime2,long generateBy, int start, int length);        
        IList<ClassAttendanceReportDto> LoadClassAttendanceReport(int start,int length,List<UserMenu> usermenu, long programId,long sessionId, long[] courseId, DateTime startDate,
            DateTime endDate, string batchIds,long[] paymentStatus,long attendanceStatus,string lectureIds);
        #endregion

        #region Others Function
        int GetCountOfTakenClass(long subjectId, long stdProgramId, long courseId);
        int GetStudentCount(long[] batchIdList, long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo, bool? clearClassAttendance = null, long[] userIdList=null);


        int GetClassEvaluationReportCount(List<UserMenu> usermenu, long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIdList, long[] lectureIdList, long[] teacherIdList, DateTime fromTime, DateTime toTime,long generateBy);

        int GetStudentCount(long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo,long[] userIdList);

        #endregion

        #region Clean
        //ClassAttendanceReportDto GetClassAttendanceReport(long studentProgramId, long batchId, DateTime startDate, DateTime endDate);
        //IList<ClassAttendanceReportDto> LoadClassAttendanceReport(List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, long[] courseId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds);
        #endregion

    }
    public class StudentClassAttendanceService : BaseService, IStudentClassAttendanceService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion
        
        #region Propertise & Object Initialization
        private readonly IStudentClassAttendanceDao _studentClassAttendenceDao;
        private readonly IStudentProgramDao _StudentProgramDao;
        private readonly IBatchDao _batchDao;
        private readonly ILectureDao _lectureDao;
        private readonly ICourseProgressDao _courseProgressDao;
        private readonly ITeacherDao _teacherDao;
        private readonly IStudentClassAttendenceDetailsDao _studentClassAttendenceDetailsDao;
        private readonly ICommonHelper _commonHelper;
        public StudentClassAttendanceService(ISession _session)
        {
            Session = _session;
            _studentClassAttendenceDao = new StudentClassAttendanceDao() { Session = _session };
            _StudentProgramDao = new StudentProgramDao() { Session = _session };
            _batchDao = new BatchDao() { Session = _session };
            _lectureDao = new LectureDao() { Session = _session };
            _teacherDao = new TeacherDao() { Session = _session };
            _courseProgressDao=new CourseProgressDao(){Session = _session};
            _studentClassAttendenceDetailsDao=new StudentClassAttendenceDetailsDao(){Session = _session};
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function

        public bool ClearClassAttendace(string lectureIds, string teacherIds, string userIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                _studentClassAttendenceDao.ClearClassAttendace(lectureIds, teacherIds, userIds,dateFrom, dateTo);
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
               return false;
            }
            
        }

        #endregion

        #region Single Instances Loading Function

        
        #endregion

        #region List Loading Function
        public List<BusinessModel.Dto.ResponseMessage> Save(List<BusinessModel.Entity.UserAuth.UserMenu> _userMenu,long programId, long lectureId, long teacherId, DateTime heldDate, DateTime startDate, string teacherLate, string[] studentRolls, string[] studentFeedback, out List<string> successRollsList)
        {
            successRollsList = new List<string>();
            var messageList = new List<ResponseMessage>();
            
                using (var trans = Session.BeginTransaction())
                {
                    var studentClassAttendence = new StudentClassAttendence()
                    {
                        Lecture = _lectureDao.LoadById(lectureId),
                        Teacher = _teacherDao.LoadById(teacherId),
                        HeldDate = heldDate,
                        Classstart = new DateTime(2000, 01, 01, startDate.Hour, startDate.Minute, startDate.Second),
                        Teacherlate = Convert.ToInt32(teacherLate)
                    };
                    _studentClassAttendenceDao.Save(studentClassAttendence);
                    var studentClassAttendenceId = studentClassAttendence.Id;
                    int i = 0;
                    foreach (var studentRoll in studentRolls)
                    {

                        try
                        {

                            var studentProgram = _StudentProgramDao.GetStudentProgram(studentRoll,false,true);
                            if (null == studentProgram)
                            {
                                messageList.Add(
                                    new ResponseMessage(errorMessage:
                                        studentRoll + "does not exist. "));
                                continue;
                            }
                            if (studentProgram.Program.Id != programId)
                            {
                                messageList.Add(
                                    new ResponseMessage(errorMessage:
                                        studentRoll + "is not valid for this program. "));
                                continue;
                            }
                            if (studentProgram.CreationDate.Date > heldDate.Date)
                            {
                                messageList.Add(
                                    new ResponseMessage(errorMessage:
                                        studentRoll + "is not valid for this date. "));
                            }
                            else
                            {
                                var classAttendanceCount =
                                    _studentClassAttendenceDetailsDao.ClassAttendanceCount(
                                        lectureId, studentProgram.Id, teacherId);
                                if ((int) classAttendanceCount < 1)
                                {
                                    
                                    var lecture = _lectureDao.LoadById(lectureId);
                                    var teacher = _teacherDao.LoadById(teacherId);
                                    var scad = new StudentClassAttendanceDetails();
                                    scad.StudentClassAttendence =
                                        _studentClassAttendenceDao.LoadById(studentClassAttendenceId);
                                    scad.StudentProgram = studentProgram;
                                    if (!string.IsNullOrEmpty(studentFeedback[i]))
                                    {
                                        scad.StudentFeedback =Convert.ToInt32(studentFeedback[i]);
                                    }

                                    var studentCourseDetails =
                                        studentProgram.StudentCourseDetails.Where(
                                            x =>
                                                x.CourseSubject == lecture.LectureSettings.CourseSubject).ToList();
                                    if (studentCourseDetails.Count > 0)
                                    {
                                        scad.IsRegistered = true;
                                    }
                                    else
                                    {
                                        var complementaryCourseIdList = new List<long>();
                                        var courses =
                                            studentProgram.StudentCourseDetails.Where(x=>x.Status==1).Select(
                                                x => x.CourseSubject.Course).ToList();
                                        foreach (var course in courses)
                                        {
                                            complementaryCourseIdList.AddRange(course.ComplementaryCourses.Where(x=>x.Program.Id==studentProgram.Program.Id && x.Session.Id==studentProgram.Batch.Session.Id).Select(x=>x.CompCourse.Id));
                                        }
                                        if (complementaryCourseIdList.Count>0 && complementaryCourseIdList.Contains(lecture.LectureSettings.CourseSubject.Course.Id))
                                        {
                                            scad.IsRegistered = true;
                                        }
                                        else
                                        {
                                            scad.IsRegistered = false; 
                                        }
                                    }
                                    _studentClassAttendenceDetailsDao.Save(scad);
                                    successRollsList.Add(studentRoll);
                                    messageList.Add(
                                  new ResponseMessage(successMessage: studentRoll + " has attended.")); 

                                    
                                }
                                else
                                {
                                    messageList.Add(
                                        new ResponseMessage(errorMessage:
                                            "Attendance is given for " + studentRoll + " already. "));
                                }

                            }

                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                            messageList.Add(
                                new ResponseMessage(errorMessage:
                                    "Something went wrong"));
                        }
                        i++;
                    }
                    trans.Commit();
                }

            return messageList;

        }

        public IList<IndividualClassAttendanceDto> GetIndividualReport(string programRoll, int attendanceType, DateTime dateFrom, DateTime dateTo)
        {

            try
            {
                var studentProgram = _StudentProgramDao.GetStudentProgram(programRoll,true);
                IList<IndividualClassAttendanceDto>individualAttendanceReportList= _studentClassAttendenceDao.LoadIndividualReport(studentProgram, dateFrom, dateTo);
                return individualAttendanceReportList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<ClassEvaluationReportDto> LoadClassEvaluation(List<UserMenu> usermenu, long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIdList, long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo, long generateBy, int start, int length)
        {
            try
            {
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                var programIdList = _commonHelper.ConvertIdToList(programId);
                var sessionIdList = _commonHelper.ConvertIdToList(sessionId);
                if (batchIdList.Contains(0))
                {
                    List<long> authProgramIdList = AuthHelper.LoadProgramIdList(usermenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(branchIds.ToList()), CommonHelper.ConvertSelectedAllIdList(programIdList));
                    List<long> authBranchIdList = AuthHelper.LoadBranchIdList(usermenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(programIdList), CommonHelper.ConvertSelectedAllIdList(branchIds.ToList()));
                    var batchList = _batchDao.LoadAuthorizeBatch(authProgramIdList, authBranchIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList), CommonHelper.ConvertSelectedAllIdList(campusIds.ToList()), batchDays, batchTimes);
                    //calling private function
                    batchList = LoadAuthorizeBatch(batchList, batchDays, batchTimes);
                   // batchIdList = batchList.Select(t => t != null ? t.Id : (long?)null).ToArray();
                    batchIdList = batchList.Select(x => x.Id).ToArray();
                }
                return _studentClassAttendenceDao.LoadClassEvaluation(batchIdList, lectureIdList, teacherIdList, dateFrom, dateTo,generateBy, start, length);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }       

        public IList<ClassAttendanceReportDto> LoadClassAttendanceReport(int start, int length, List<UserMenu> usermenu, long programId, long sessionId, long[] courseId, DateTime startDate,
            DateTime endDate, string batchIds, long[] paymentStatus, long attendanceStatus,string lectureIds)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(usermenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programId));
                return _studentClassAttendenceDao.LoadClassAttendanceReport(start, length, programId, sessionId, programIdList, branchIdList, courseId, startDate,
                    endDate, batchIds, paymentStatus, attendanceStatus, lectureIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetClassEvaluationReportCount(List<UserMenu> usermenu, long organizationId, long programId, long sessionId,
            long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIdList,
            long[] lectureIdList, long[] teacherIdList, DateTime fromTime, DateTime toTime,long generateBy)
        {
            try
            {
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                var programIdList = _commonHelper.ConvertIdToList(programId);
                var sessionIdList = _commonHelper.ConvertIdToList(sessionId);
                if (batchIdList.Contains(0))
                {
                    List<long> authProgramIdList = AuthHelper.LoadProgramIdList(usermenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(branchIds.ToList()), CommonHelper.ConvertSelectedAllIdList(programIdList));
                    List<long> authBranchIdList = AuthHelper.LoadBranchIdList(usermenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(programIdList), CommonHelper.ConvertSelectedAllIdList(branchIds.ToList()));
                    var batchList = _batchDao.LoadAuthorizeBatch(authProgramIdList, authBranchIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList), CommonHelper.ConvertSelectedAllIdList(campusIds.ToList()), batchDays, batchTimes);
                    //calling private function
                    batchList = LoadAuthorizeBatch(batchList, batchDays, batchTimes);
                    // batchIdList = batchList.Select(t => t != null ? t.Id : (long?)null).ToArray();
                    batchIdList = batchList.Select(x => x.Id).ToArray();
                }
                return _studentClassAttendenceDao.GetClassEvaluationReportCount(batchIdList, lectureIdList, teacherIdList, fromTime, toTime,generateBy);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region Others Function
        public int GetStudentCount(long[] batchIdList, long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo, bool? clearClassAttendance = null,long[] userIdList=null)
        {
            try
            {
                return _studentClassAttendenceDao.GetStudentCount(batchIdList, lectureIdList,
                        teacherIdList,
                        dateFrom, dateTo,clearClassAttendance, userIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public int GetCountOfTakenClass(long subjectId, long stdProgramId, long courseId)
        {
            try
            {
                return _studentClassAttendenceDao.GetCountOfTakenClassByStudentProgramAndSubjectAndCourse(subjectId, stdProgramId, courseId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetStudentCount(long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo,
            long[] userIdList)
        {
            try
            {
                var query = @"select count(distinct scad.studentprogramid)  from StudentClassAttendence sca 
                        inner join StudentClassAttendanceDetails scad on scad.StudentClassAttendanceId=sca.Id
                        inner join Lecture l on l.Id=sca.LectureId
                        inner join LectureSettings ls on l.LectureSettingsId=ls.Id
                        inner join teacher t on t.id=sca.teacherid
                        and sca.lectureid in({0}) 
                        and t.id in({3}) 
                        and sca.HeldDate>='{1}' and sca.HeldDate<'{2}' and sca.createby in({4})";
                query = string.Format(query, string.Join(",", lectureIdList), dateFrom, dateTo.AddDays(1), string.Join(",", teacherIdList), string.Join(",", userIdList));
                IQuery iQuery = Session.CreateSQLQuery(query);
                iQuery.SetTimeout(2700);
                var count = iQuery.UniqueResult<int>();
                return count;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            
        }
        #endregion

        #region Helper Function
        private IList<Batch> LoadAuthorizeBatch(IList<Batch> batchList, string[] batchDaysList, string[] batchTimeList = null)
        {
            try
            {
                if (batchDaysList != null)
                {
                    if (!batchDaysList.Contains(SelectionType.SelelectAll.ToString()))
                        batchList = (from x in batchList where batchDaysList.Any(y => y == x.Days) select x).ToList();
                }
                if (batchTimeList != null)
                {
                    if (!batchTimeList.Contains(SelectionType.SelelectAll.ToString()))
                        batchList = (from x in batchList where batchTimeList.Any(y => y == x.FormatedBatchTime) select x).ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchList;
        }
        #endregion

        #region Clean
        //public ClassAttendanceReportDto GetClassAttendanceReport(long studentProgramId, long batchId, DateTime startDate,
        //    DateTime endDate)
        //{
        //    return _studentClassAttendenceDao.GetClassAttendanceReport(studentProgramId, batchId, startDate,
        //        endDate);
        //}
        //public IList<ClassAttendanceReportDto> LoadClassAttendanceReport(List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, long[] courseId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds)
        //{
        //    List<long> programIdList = AuthHelper.LoadProgramIdList(usermenu);
        //    List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programId));
        //    return _studentClassAttendenceDao.LoadClassAttendanceReport(programIdList, branchIdList, programId, sessionId, branchId, campusId, courseId, batchDays, batchTime, batchList, paymentStatus, attendanceStatus, startDate, endDate, batchIds);

        //}
        #endregion
    }
}
