﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Students
{
    public interface IMaterialDistributionDao : IBaseDao<StudentProgram, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
       
       

        #endregion

        #region Others Function
        #endregion

        #region without Pair
        IList<string> LoadStudentProgramByCriteria(int start, int length, List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId
            , string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus, string[] informationViewList);
        IList<StudentProgram> LoadStudentProgramByCriteriaForExport(int start, int length, List<long> programIdList,
            List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId
            , string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId,
            long materialStatus
            , long paymentStatus, string[] informationViewList);

        int GetStudentProgramCount(int start, int length, List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus, string[] informationViewList);
        #endregion

        #region with Pair
        //IList<string> LoadStudentProgramByCriteria(int start, int length, long organizationId, List<ProgramBranchPair> programBranchPairList, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus);

        //IList<StudentProgram> LoadStudentProgramByCriteriaForExport(long organizationId, List<ProgramBranchPair> programBranchPairList, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus);

        //int GetStudentProgramCount(long organizationId, List<ProgramBranchPair> programBranchPairList, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus);
        #endregion

    }

    public class MaterialDistributionDao : BaseDao<StudentProgram, long>, IMaterialDistributionDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function


     

        #endregion

        #region Others Function

        #endregion

        #region Helper Function


        #endregion

        #region with Pair

        //public IList<string> LoadStudentProgramByCriteria(int start, int length, long organizationId,
        //    List<ProgramBranchPair> programBranchPairList, long sessionId, long[] campusId, string[] batchDays,
        //    string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus,
        //    long paymentStatus)
        //{

        //    var criteria = GetAuthorizedStudentCriteria(organizationId, programBranchPairList, sessionId, campusId,
        //        batchDays, batchTime, batch, materialType, materialId, materialStatus
        //        , paymentStatus);
        //    criteria.SetProjection(Projections.GroupProperty("PrnNo"));
        //    criteria.AddOrder(Order.Asc("PrnNo"));
        //    if (length > 0)
        //    {
        //        criteria.SetFirstResult(start).SetMaxResults(length);
        //    }
        //    var returnObj = (List<string>)criteria.List<string>();
        //    return returnObj;
        //}
        //public IList<StudentProgram> LoadStudentProgramByCriteriaForExport(long organizationId,
        //    List<ProgramBranchPair> programBranchPairList, long sessionId, long[] campusId, string[] batchDays,
        //    string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus,
        //    long paymentStatus)
        //{

        //    var criteria = GetAuthorizedStudentCriteria(organizationId, programBranchPairList, sessionId, campusId,
        //        batchDays, batchTime, batch, materialType, materialId, materialStatus
        //        , paymentStatus);
        //    var returnObj = (List<StudentProgram>)criteria.List<StudentProgram>();
        //    return returnObj;
        //}
        //public int GetStudentProgramCount(long organizationId, List<ProgramBranchPair> programBranchPairList,
        //    long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType,
        //    long[] materialId, long materialStatus, long paymentStatus)
        //{
        //    var criteria = GetAuthorizedStudentCriteria(organizationId, programBranchPairList, sessionId, campusId,
        //        batchDays, batchTime, batch, materialType, materialId, materialStatus
        //        , paymentStatus);
        //    criteria.SetProjection(Projections.CountDistinct("Id"));
        //    var returnObj = (int)criteria.UniqueResult();
        //    ;
        //    return returnObj;
        //}
        //private ICriteria GetAuthorizedStudentCriteria(long organizationId,
        //    List<ProgramBranchPair> programBranchPairList, long sessionId, long[] campusId, string[] batchDays,
        //    string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus,
        //    long paymentStatus)
        //{
        //    ICriteria criteria = Session.CreateCriteria<StudentProgram>();

        //    criteria.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
        //    criteria.CreateCriteria("Batch", "b").Add(Restrictions.Eq("Session.Id", sessionId));
        //    criteria.Add(Restrictions.Eq("Program.Id", Convert.ToInt64(68)));
        //    criteria.CreateCriteria("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
        //    var disjunctionProgramBranchPair = Restrictions.Disjunction(); // for OR statement 
        //    foreach (var programBranch in programBranchPairList)
        //    {
        //        disjunctionProgramBranchPair.Add(Restrictions.Eq("Program.Id", programBranch.ProgramId) &&
        //                                         Restrictions.Eq("b.Branch.Id", programBranch.BranchId));
        //    }
        //    criteria.Add(disjunctionProgramBranchPair);
        //    //var programIdList = new List<long>
        //    //{
        //    //    60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80
        //    //};
        //    //var branchIdList = new List<long>
        //    //{
        //    //    78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101
        //    //};
        //    //if (programIdList != null)
        //    //{
        //    //    criteria.Add(Restrictions.In("p.Id", programIdList));
        //    //}
        //    //if (branchIdList != null)
        //    //{
        //    //    criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
        //    //}
        //    if (!(Array.Exists(campusId, item => item == 0)))
        //    {
        //        criteria.Add(Restrictions.In("b.Campus.Id", campusId));
        //    }

        //    if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) &&
        //        !(Array.Exists(batchDays, item => item == "")))
        //    {
        //        criteria.Add(Restrictions.In("b.Days", batchDays));
        //    }

        //    if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
        //    {
        //        var disjunction = Restrictions.Disjunction(); // for OR statement 
        //        foreach (string bt in batchTime)
        //        {
        //            var batchArrays = bt.Replace("To", ",");
        //            var batchArray = batchArrays.Split(',');
        //            var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
        //            var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
        //            disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
        //        }
        //        criteria.Add(disjunction);
        //    }

        //    if (!(Array.Exists(batch, item => item == 0)))
        //    {
        //        criteria.Add(Restrictions.In("b.Id", batch));
        //    }


        //    if (paymentStatus == SelectionType.IsDue)
        //    {
        //        criteria.Add(Restrictions.Gt("DueAmount", (decimal)0));
        //    }
        //    else if (paymentStatus == SelectionType.IsPaid)
        //    {
        //        criteria.Add(Restrictions.Eq("DueAmount", (decimal)0));
        //    }

        //    if (materialStatus == SelectionType.IsMaterialProvided)
        //    {
        //        criteria.Add(Restrictions.IsNotEmpty("Materials"));
        //    }
        //    else if (materialStatus == SelectionType.IsNotMaterialProvided)
        //    {
        //        criteria.Add(Restrictions.IsEmpty("Materials"));
        //    }

        //    criteria.CreateAlias("Materials", "m").Add(Restrictions.In("m.Id", materialId));
        //    criteria.SetTimeout(3000);
        //    return criteria;
        //}
        #endregion


        #region Without Pair

        public IList<StudentProgram> LoadStudentProgramByCriteriaForExport(int start, int length, List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId
            , string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus
            , long paymentStatus, string[] informationViewList)
        {

            var criteria = GetAuthorizedStudentCriteria(programIdList, branchIdList, programId, sessionId, branchId, campusId
            , batchDays, batchTime, batch, materialType, materialId, materialStatus
            , paymentStatus, informationViewList);
            var returnObj = (List<StudentProgram>)criteria.List<StudentProgram>();
            return returnObj;
        }
        public int GetStudentProgramCount(int start, int length, List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus, string[] informationViewList)
        {
            var criteria = GetAuthorizedStudentCriteria(programIdList, branchIdList, programId, sessionId, branchId, campusId
            , batchDays, batchTime, batch, materialType, materialId, materialStatus
            , paymentStatus, informationViewList);
            criteria.SetProjection(Projections.CountDistinct("Id"));
            var returnObj = (int)criteria.UniqueResult(); ;
            return returnObj;
        }
        private ICriteria GetAuthorizedStudentCriteria(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId
            , string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus
            , long paymentStatus, string[] informationViewList)
        {
            ICriteria criteria = Session.CreateCriteria<StudentProgram>();

            criteria.Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.CreateCriteria("Batch", "b").Add(Restrictions.Eq("Session.Id", sessionId));
            criteria.CreateCriteria("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("p.Id", programIdList));
            }
            if (branchIdList != null)
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchIdList));
            }
            //criteria.CreateCriteria("Program", "p").Add(Restrictions.Eq("Program.Id", programId));

            //criteria.Add(Restrictions.Eq("p.", programId));
            if (!(Array.Exists(branchId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Branch.Id", branchId));
            }

            if (!(Array.Exists(campusId, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Campus.Id", campusId));
            }

            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) &&
                !(Array.Exists(batchDays, item => item == "")))
            {
                criteria.Add(Restrictions.In("b.Days", batchDays));
            }

            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                var disjunction = Restrictions.Disjunction(); // for OR statement 
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));

                    //var batchArray = bt.Split(',');
                    //var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    //var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    //disjunction.Add(Restrictions.Ge("b.StartTime", startTime) &&
                    //                Restrictions.Le("b.EndTime", endTime));
                }
                criteria.Add(disjunction);
            }

            if (!(Array.Exists(batch, item => item == 0)))
            {
                criteria.Add(Restrictions.In("b.Id", batch));
            }


            if (paymentStatus == SelectionType.IsDue)
            {
                criteria.Add(Restrictions.Gt("DueAmount", (decimal)0));
            }
            else if (paymentStatus == SelectionType.IsPaid)
            {
                criteria.Add(Restrictions.Eq("DueAmount", (decimal)0));
            }

            if (materialStatus == SelectionType.IsMaterialProvided)
            {
                criteria.Add(Restrictions.IsNotEmpty("Materials"));
            }
            else if (materialStatus == SelectionType.IsNotMaterialProvided)
            {
                criteria.Add(Restrictions.IsEmpty("Materials"));
            }

            criteria.CreateAlias("Materials", "m").Add(Restrictions.In("m.Id", materialId));

            return criteria;
        }

        public IList<string> LoadStudentProgramByCriteria(int start, int length, List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId
                , string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus
                , long paymentStatus, string[] informationViewList)
        {

            var criteria = GetAuthorizedStudentCriteria(programIdList, branchIdList, programId, sessionId, branchId, campusId
            , batchDays, batchTime, batch, materialType, materialId, materialStatus
            , paymentStatus, informationViewList);
            criteria.SetProjection(Projections.GroupProperty("PrnNo"));
            criteria.AddOrder(Order.Asc("PrnNo"));
            if (length > 0)
            {
                criteria.SetFirstResult(start).SetMaxResults(length);
            }
            var returnObj = (List<string>)criteria.List<string>();
            return returnObj;
        }
        #endregion


    }
}
