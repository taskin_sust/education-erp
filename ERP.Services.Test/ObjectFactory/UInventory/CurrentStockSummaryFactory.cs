using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class CurrentStockSummaryFactory : ObjectFactoryBase<CurrentStockSummary>
    {

        private readonly ICurrentStockSummaryService _currentStockSummaryService;

        public CurrentStockSummaryFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _currentStockSummaryService = new CurrentStockSummaryService(session);

        }

        #region Create

        public CurrentStockSummaryFactory Create()
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;
        }

        public CurrentStockSummaryFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public CurrentStockSummaryFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _currentStockSummaryService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_currentStockSummaryService.SaveOrUpdate(x); });

            }
            return this;
        }

        public CurrentStockSummaryFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new CurrentStockSummary() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public CurrentStockSummaryFactory CleanUp()
        {
            DeleteAll();
            return this;
        }

        #endregion

    }
}
