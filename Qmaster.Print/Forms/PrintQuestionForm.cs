﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Qmaster.Print.Helper;
using Qmaster.Print.Model;
using UdvashERP.FingerPrint.BusinessRule;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;
using System.Web.Script.Serialization;
using iTextSharp.text.pdf;
using ICSharpCode.SharpZipLib.Zip;
using ZXing.QrCode.Internal;

namespace Qmaster.Print.Forms
{
    public partial class PrintQuestionForm : Form
    {
        #region object\initial\load

        private string _key = ConfigurationManager.AppSettings["apiKey"];
        //private string _baseUrl = ConfigurationManager.AppSettings["baseApiUrlOsl"];
        private string _baseUrl = ConfigurationManager.AppSettings["baseApiUrl"];

        private List<Organization> organizationList = null;
        private List<Qmaster.Print.Model.Program> programList = null;
        private List<Session> sessionList = null;
        private List<Course> courseList = null;
        private List<ExamCode> examCodeList = null;
        private List<Exam> examList = null;
        private List<Model.Version> versionList = null;
        private List<ExamUniqueSet> examUniqueSetList = null;
        private Exam examObj = null;
        private PrintConfigInfo _printConfigInfo = null;

        private ExamMcqQuestionSetPrintThread sssn = null;//new ExamQuestionPrintThread();
        private static int successCounter = 0;
        private static int errorCounter = 0;
        private List<ExamMcqQuestionSetPrint> examMcqQuestionSetPrintList = null;
        private List<MargePdfSartEndCompare> margePdfList = null;
        private List<FileInfo> allPdfFiles = null;
        private string questionZipUrlPath = "";
        private string zipFileName = "";
        private string fileConcatNameString = "";

        WebClient webClient;               // Our WebClient that will be doing the downloading for us
        private delegate void DelegateSuccessFailCounter(ExamMcqQuestionSetPrintThread.AlertEventArg e);

        public PrintQuestionForm()
        {
            InitializeComponent();
            btnStop.Enabled = false;
        }

        private void PrintQuestionForm_Load(object sender, EventArgs e)
        {
            organizationList = new List<Organization>();
            programList = new List<Model.Program>();
            sessionList = new List<Session>();
            courseList = new List<Course>();
            examCodeList = new List<ExamCode>();
            examList = new List<Exam>();
            versionList = new List<Model.Version>();
            examUniqueSetList = new List<ExamUniqueSet>();

            examMcqQuestionSetPrintList = new List<ExamMcqQuestionSetPrint>();
            margePdfList = new List<MargePdfSartEndCompare>();
            examObj = new Exam();
            _printConfigInfo = new PrintConfigInfo();
            PrintPdfHelper.SavePrintConfigInfo();
            LoadPrintConfigInfoFromXml();
            //if(_printConfigInfo.IsAlpha)
            //    _baseUrl = ConfigurationManager.AppSettings["baseApiUrlAlpha"];
        }

        #endregion

        #region Print Question

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                #region validate

                Organization org = (Organization)cbOrganization.SelectedItem;
                if (org.Id == 0)
                {
                    throw new InvalidDataException("Please Select Organization");
                }
                Model.Program program = (Model.Program)cbProgram.SelectedItem;
                if (program.Id == 0)
                {
                    throw new InvalidDataException("Please Select Program");
                }
                Session session = (Session)cbSession.SelectedItem;
                if (session.Id == 0)
                {
                    throw new InvalidDataException("Please Select Session");
                }
                Course course = (Course)cbCourse.SelectedItem;
                if (course.Id == 0)
                {
                    throw new InvalidDataException("Please Select Course");
                }
                string examCode = txtbExamCode.Text;
                if (String.IsNullOrEmpty(examCode))
                {
                    throw new InvalidDataException("Please Enter Exam Code");
                }

                if (!examCodeList.Any())
                    throw new InvalidDataException("Please Enter Exam Code");
                if (examCodeList.Any() && !examCodeList.Where(x => x.Code == examCode).ToList().Any())
                    throw new InvalidDataException("Please Select Right Exam Code");

                Model.Version version = (Model.Version)cbVersion.SelectedItem;
                if (version.Id == 0)
                {
                    throw new InvalidDataException("Please Select Version");
                }

                string printnumberString = txtbPrintQuestionNo.Text;
                if (String.IsNullOrEmpty(printnumberString))
                {
                    throw new InvalidDataException("Please Enter Exam Print Question Number");
                }
                int printNo = 0;
                if (!Int32.TryParse(printnumberString, out printNo))
                {
                    throw new InvalidDataException("Please Enter valid Exam Print Question Number");
                }
                if (printNo <= 0)
                {
                    throw new InvalidDataException("Please Enter valid Exam Print Question Number");
                }

                List<int> selectedUniqueSet = new List<int>();
                if (listUniqueSet.Items.Count <= 0)
                {
                    throw new InvalidDataException("This exam has no unique set.");
                }
                if (listUniqueSet.SelectedItems.Count <= 0)
                {
                    throw new InvalidDataException("Please select unique Set.");
                }

                foreach (int item in listUniqueSet.SelectedItems)
                {
                    selectedUniqueSet.Add(item);
                }
                
                #endregion

                PrinterSettings ps = new PrinterSettings();
                if (_printConfigInfo.IsPrint)
                {
                    TestForm testForm = new TestForm();
                    testForm.BtnPrintTestClick();
                    ps = testForm.ps;
                }

                margePdfList = MargePdfGenerate(_printConfigInfo.MargePdf, printNo);

                if (true)
                {
                    #region Print Question

                    //call api for collect examQuestionPrintSet 

                    //if (examObj != null)
                    //    examMcqQuestionSetPrintList = CreateDummyExamMcqQuestionSetPrintList(printNo, examObj.Id);
                    //else
                    //    examMcqQuestionSetPrintList = CreateDummyExamMcqQuestionSetPrintList(printNo, 1);

                    if (examObj != null)
                    {
                        LoadQuestion(examObj.Id, selectedUniqueSet, version.Id, printNo);

                        //if (examObj != null)
                        //    examMcqQuestionSetPrintList = CreateDummyExamMcqQuestionSetPrintList(printNo, examObj.Id);
                        //else
                        //    examMcqQuestionSetPrintList = CreateDummyExamMcqQuestionSetPrintList(printNo, 1);

                    }

                    //questionZipUrlPath = @"C:\Users\Imran\Desktop\a3image\QuestionSet_1.zip";

                    if (!examMcqQuestionSetPrintList.Any() || String.IsNullOrEmpty(questionZipUrlPath))
                    {
                        throw new InvalidDataException("No Set Print option found!");
                    }

                    #region Uuzip

                    if (!Directory.Exists(PrintConstant.QuestionPath))
                    {
                        Directory.CreateDirectory(PrintConstant.QuestionPathDir);
                    }
                    if (!Directory.Exists(PrintConstant.NewQuestionPathDir))
                    {
                        Directory.CreateDirectory(PrintConstant.NewQuestionPathDir);
                    }
                    if (!Directory.Exists(PrintConstant.MargeQuestionPath))
                    {
                        Directory.CreateDirectory(PrintConstant.MargeQuestionPath);
                    }

                    fileConcatNameString = DateTime.Now.ToString("yyyyMMddHHmmss_");
                    
                    Uri uri = new Uri(questionZipUrlPath);
                    string downloadDirectorPath = "";
                    bool downloadFile = true;
                    if (!_printConfigInfo.DownloadNewFile)
                    {
                        fileConcatNameString = "";
                        zipFileName = Path.GetFileName(uri.LocalPath);
                        downloadDirectorPath = PrintConstant.QuestionPath + "\\" + zipFileName;
                        if (File.Exists(downloadDirectorPath))
                        {
                            downloadFile = false;
                        }
                    }
                    if (downloadFile)
                    {
                        zipFileName = fileConcatNameString + Path.GetFileName(uri.LocalPath);
                        downloadDirectorPath = PrintConstant.QuestionPath + "\\" + zipFileName;
                        WebClient webClient = new WebClient();
                        webClient.DownloadFile(uri, downloadDirectorPath);
                    }
                    string zipPath = downloadDirectorPath;
                    ZipFile zip = new ZipFile(zipPath);
                    bool returnValue = zip.TestArchive(true, TestStrategy.FindFirstError, null);
                    if (returnValue == false)
                    {
                        throw new InvalidDataException("Invalid zip!!");
                    }
                    zip.Close();

                    FastZip fastZip = new FastZip();
                    fastZip.ExtractZip(zipPath, PrintConstant.QuestionPath, "");
                    DirectoryInfo directory = new DirectoryInfo(PrintConstant.QuestionPath);
                    allPdfFiles =
                        directory.GetFiles()
                            .Where(obj => obj.Extension.Contains(".pdf") || obj.Extension.Contains(".PDF"))
                            .ToList();
                    if (allPdfFiles != null && !allPdfFiles.Any())
                    {
                        throw new InvalidDataException("No pdf file find to print question");
                    }

                    #endregion

                    #region Start Printing through Thread

                    if (examMcqQuestionSetPrintList.Any() && !String.IsNullOrEmpty(questionZipUrlPath))
                    {
                        DisableDuringImport();

                        sssn = new ExamMcqQuestionSetPrintThread();
                        sssn.OnNewExamMcqQuestionSetPrint += new AlertHandlerThreadExamMcqQuestionSetPrint(sssn_OnNewExamMcqQuestionSetPrint);
                        sssn.AllPdfFiles = allPdfFiles;
                        sssn.ExamMcqQuestionSetPrintList = examMcqQuestionSetPrintList;
                        sssn.PrintConfigInfo = _printConfigInfo;
                        sssn.PrinterSettings = ps;
                        sssn.MargePdfList = margePdfList;
                        sssn.FileConcatNameString = fileConcatNameString;
                        sssn.Start();
                    }

                    #endregion

                    #endregion
                }
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                //view a custom confirmation dialog
                String strMsg = "Are you sure to stop Question Print?";
                if (MessageBox.Show(strMsg, "Confirmation...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return;

                sssn.Stop();
                EnableAfterImport();
                DeleteFiles();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            try
            {
                //if (_printConfigInfo.IsAlpha)
                //    _baseUrl = ConfigurationManager.AppSettings["baseApiUrlAlpha"];
                LoadPrintConfigInfoFromXml();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnLoadOrganization_Click(object sender, EventArgs e)
        {
            try
            {
                LoadOrganization();
                bsOrganizationList.DataSource = organizationList;
                if (cbOrganization.Items.Count > 1)
                {
                    cbOrganization.SelectedIndex = 0;
                }
                examObj = new Exam();
                examCodeList = new List<ExamCode>();
                txtbExamCode.Text = "";
                txtbExamName.Text = "";
                txtbPrintQuestionNo.Text = "";
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void sssn_OnNewExamMcqQuestionSetPrint(object sender, ExamMcqQuestionSetPrintThread.AlertEventArg e)
        {

            if (e == null || e.LastExamMcqQuestionSetPrintUploaded == null)
            {
                btnStop.Invoke(new DelegateSuccessFailCounter(QuestionPrintError), new object[] { e });
            }
            else
                probarPrintQuestion.Invoke(new DelegateSuccessFailCounter(SuccessCounter), new object[] { e });
        }

        private void QuestionPrintError(ExamMcqQuestionSetPrintThread.AlertEventArg e)
        {
            btnStop_Click(null, null);
        }

        private void SuccessCounter(ExamMcqQuestionSetPrintThread.AlertEventArg e)
        {
            //stop to show progress if user stopped the process

            // probarPrintQuestion.Text = e.LastExamMcqQuestionSetPrintUploaded.ProgressString;
            probarPrintQuestion.Value = e.LastExamMcqQuestionSetPrintUploaded.Progress;
            progressText.Text = e.LastExamMcqQuestionSetPrintUploaded.ProgressString + " %";

            if (e.LastExamMcqQuestionSetPrintUploaded.Message.ToUpper() == "PRINT SUCCESSFULLY" || e.LastExamMcqQuestionSetPrintUploaded.Message.ToUpper() == "SUCCESSFULLY")
            {
                string questionAcknowledgementJsonString = WebHelper.GetRequest(_baseUrl + PrintConstant.QuestionAcknowledgementUrl + "?key=" + _key + "&id=" + e.LastExamMcqQuestionSetPrintUploaded.Id + "&printStatus=1");
                successCounter++;
                //teSuccess.Text = successCounter.ToString();
            }
            else
            {
                errorCounter++;
                //teFaild.Text = errorCounter.ToString();
            }

            if (e.LastExamMcqQuestionSetPrintUploaded.IsAllUploaded == false)
            {
                return;
            }


            // probarPrintQuestion.Text = "100";
            probarPrintQuestion.Value = 100;
            progressText.Text = "100 %";
            int totalPrintExamQuestion = examMcqQuestionSetPrintList.Count;
            int totalPrintSuccess = (from x in examMcqQuestionSetPrintList where (x.Message.ToUpper() == "PRINT SUCCESSFULLY" || x.Message.ToUpper() == "SUCCESSFULLY") select x).Count();
            int totalPrintFail = totalPrintExamQuestion - totalPrintSuccess;

            MessageBox.Show(this, "Exam Question Print completed." + Environment.NewLine
                                  + "Total Exam questions : " + examMcqQuestionSetPrintList.Count + Environment.NewLine
                                  + "Successfully Print : " + totalPrintSuccess + Environment.NewLine
                                  + "Print failed : " + totalPrintFail
                , "Task done...", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            EnableAfterImport();
            DeleteFiles();
            // probarPrintQuestion.Text = "0";
            probarPrintQuestion.Value = 0;
            progressText.Text = "0 %";
            //  this.Close();

        }

        #endregion

        #region Change Event

        private void cbOrganization_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadProgram();
            bsProgramList.DataSource = programList;
            if (cbProgram.Items.Count > 1)
            {
                cbProgram.SelectedIndex = 0;
            }
        }

        private void cbProgram_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSession();
            bsSessionList.DataSource = sessionList;
            if (cbSession.Items.Count > 1)
            {
                cbSession.SelectedIndex = 0;
            }
        }

        private void cbSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCourse();
            bsCourseList.DataSource = courseList;
            if (cbCourse.Items.Count > 1)
            {
                cbCourse.SelectedIndex = 0;
            }
        }

        private void cbCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadExam();
            AutoSuggestionExamCode();
        }

        private void txtbExamCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtbExamName.Text = "";
            bsVersionList.DataSource = null;
        }

        private void txtbExamCode_Leave(object sender, EventArgs e)
        {
            ExamInfoLoad();
        }

        private void txtbExamCode_TextChanged(object sender, EventArgs e)
        {
            ExamInfoLoad();
        }

        private void txtbExamCode_Enter(object sender, EventArgs e)
        {
            ExamInfoLoad();
        }

        private void txtbPrintQuestionNo_TextChanged(object sender, EventArgs e)
        {
            List<Char> numberList = new List<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            string questionNo = txtbPrintQuestionNo.Text;
            string intQuestionNo = "";
            if (!String.IsNullOrEmpty(questionNo))
            {
                foreach (char c in questionNo)
                {
                    if (numberList.Any(x => x == c))
                    {
                        intQuestionNo += c;
                    }
                }
            }

            txtbPrintQuestionNo.Text = intQuestionNo;
        }

        #endregion

        #region helper Function

        private void LoadOrganization()
        {
            string organizationJsonString = WebHelper.GetRequest(_baseUrl + PrintConstant.OrganizationUrl + "?key=" + _key);
            JsonResponseOrganizationViewModel organizationJsonList = new JavaScriptSerializer().Deserialize<JsonResponseOrganizationViewModel>(organizationJsonString);
            List<Organization> orgList = new List<Organization> { new Organization { Id = 0, ShortName = "Select Organization" } };
            if (organizationJsonList.IsSuccess && organizationJsonList.Data.Any())
            {
                orgList.AddRange(organizationJsonList.Data);
            }
            else
            {
                throw new InvalidDataException(organizationJsonList.Message);
            }
            organizationList = orgList;
        }

        private void LoadProgram()
        {
            List<Model.Program> proList = new List<Model.Program> { new Model.Program { Id = 0, FullName = "Select Program" } };
            Organization org = (Organization)cbOrganization.SelectedItem;
            if (org != null && org.Id >= 0)
            {
                string programJsonString = WebHelper.GetRequest(_baseUrl + PrintConstant.ProgramUrl + "?key=" + _key + "&organizationId=" + org.Id);
                JsonResponseProgramViewModel programJsonList = new JavaScriptSerializer().Deserialize<JsonResponseProgramViewModel>(programJsonString);
                if (programJsonList.IsSuccess && programJsonList.Data.Any())
                {
                    proList.AddRange(programJsonList.Data);
                }
                else
                {
                    throw new InvalidDataException(programJsonList.Message);
                }
            }
            programList = proList;
        }

        private void LoadSession()
        {
            List<Session> sesList = new List<Session> { new Session { Id = 0, Name = "Select Session" } };
            Organization org = (Organization)cbOrganization.SelectedItem;
            Model.Program pro = (Model.Program)cbProgram.SelectedItem;
            if (org != null && org.Id > 0 && pro != null && pro.Id > 0)
            {
                string sessionJsonString = WebHelper.GetRequest(_baseUrl + PrintConstant.SessionUrl + "?key=" + _key + "&organizationId=" + org.Id + "&programId=" + pro.Id);
                JsonResponseSessionViewModel sessionJsonList = new JavaScriptSerializer().Deserialize<JsonResponseSessionViewModel>(sessionJsonString);
                if (sessionJsonList.IsSuccess && sessionJsonList.Data.Any())
                {
                    sesList.AddRange(sessionJsonList.Data);
                }
                else
                {
                    throw new InvalidDataException(sessionJsonList.Message);
                }
            }
            sessionList = sesList;
        }

        private void LoadCourse()
        {
            List<Course> courList = new List<Course> { new Course { Id = 0, Name = "Select Course" } };
            Organization org = (Organization)cbOrganization.SelectedItem;
            Model.Program pro = (Model.Program)cbProgram.SelectedItem;
            Session ses = (Session)cbSession.SelectedItem;
            if (org != null && org.Id > 0 && pro != null && pro.Id > 0 && ses != null && ses.Id > 0)
            {
                string courseJsonString = WebHelper.GetRequest(_baseUrl + PrintConstant.CourseUrl + "?key=" + _key + "&organizationId=" + org.Id + "&programId=" + pro.Id + "&sessionId=" + ses.Id);
                JsonResponseCourseViewModel courseJsonList = new JavaScriptSerializer().Deserialize<JsonResponseCourseViewModel>(courseJsonString);
                if (courseJsonList.IsSuccess && courseJsonList.Data.Any())
                {
                    courList.AddRange(courseJsonList.Data);
                }
                else
                {
                    throw new InvalidDataException(courseJsonList.Message);
                }
            }
            courseList = courList;
        }

        private void LoadExam()
        {
            examList = new List<Exam>();

            Organization org = (Organization)cbOrganization.SelectedItem;
            Model.Program pro = (Model.Program)cbProgram.SelectedItem;
            Session ses = (Session)cbSession.SelectedItem;
            Course cou = (Course)cbCourse.SelectedItem;
            if (org != null && org.Id > 0 && pro != null && pro.Id > 0 && ses != null && ses.Id > 0 && cou != null && cou.Id > 0)
            {
                string examJsonString = WebHelper.GetRequest(_baseUrl + PrintConstant.ExamUrl + "?key=" + _key + "&organizationId=" + org.Id + "&programId=" + pro.Id + "&sessionId=" + ses.Id + "&courseId=" + cou.Id);
                JsonResponseExamViewModel examJsonList = new JavaScriptSerializer().Deserialize<JsonResponseExamViewModel>(examJsonString);
                if (examJsonList.IsSuccess && examJsonList.Data.Any())
                {
                    examList.AddRange(examJsonList.Data);
                }
                else
                {
                    throw new InvalidDataException(examJsonList.Message);
                }
            }
        }

        private void LoadExamCode()
        {
            if (examList != null && examList.Any())
            {
                examCodeList = new List<ExamCode>();
                foreach (Exam exam in examList)
                {
                    examCodeList.Add(new ExamCode { Code = exam.Code });
                }
            }
        }

        private void AutoSuggestionExamCode()
        {
            LoadExamCode();
            if (examCodeList != null && examCodeList.Any())
            {
                AutoCompleteStringCollection autoCompleteStringCollectionForHeader = new AutoCompleteStringCollection();
                List<string> slist = new List<string>();
                foreach (ExamCode ecode in examCodeList)
                {
                    autoCompleteStringCollectionForHeader.Add(ecode.Code);
                    slist.Add(ecode.Code);
                }
                txtbExamCode.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtbExamCode.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtbExamCode.AutoCompleteCustomSource = autoCompleteStringCollectionForHeader;
            }
        }

        private void LoadVersion()
        {
            List<Model.Version> verList = new List<Model.Version> { new Model.Version { Id = 0, Name = "Select Version" } };
            if (examObj != null)
            {
                if (examObj.IsBanglaVersion)
                {
                    verList.Add(new Model.Version { Id = 1, Name = "Bangla Version" });
                }
                if (examObj.IsEnglishVersion)
                {
                    verList.Add(new Model.Version { Id = 2, Name = "English Version" });
                }
            }
            versionList = verList;
        }

        private void LoadUniqueSet()
        {
            if (listUniqueSet.Items.Count > 0)
                listUniqueSet.Items.Clear();
           // List<ExamUniqueSet> uqSetList = new List<ExamUniqueSet> { new ExamUniqueSet { Id = 0, Name = "Select Unique Set" } };
            if (examObj != null && examObj.UniqueSet > 0)
            {
                for (int lo = 1; lo <= examObj.UniqueSet; lo++)
                {
                    listUniqueSet.Items.Add(lo);
                    //uqSetList.Add(new ExamUniqueSet { Id = lo, Name = lo.ToString() });
                }
            }
            //examUniqueSetList = uqSetList;
        }

        private void ExamInfoLoad()
        {
            string examCode = txtbExamCode.Text;
            if (!String.IsNullOrEmpty(examCode) && examList.Any() && examCode.Length >= 3)
            {
                Exam exa = examList.FirstOrDefault(x => x.Code == examCode);
                if (exa != null)
                {
                    examObj = exa;
                    txtbExamName.Text = examObj.Name;
                }
            } 
            LoadVersion();
            LoadUniqueSet();
            bsVersionList.DataSource = versionList;
            if (cbVersion.Items.Count > 1)
            {
                cbVersion.SelectedIndex = 0;
            }
        }

        private void LoadQuestion(long examId, List<int> uniqueSetList, int versionNo, int printingCount)
        {
            if (examId > 0 && versionNo > 0 && printingCount > 0 && uniqueSetList.Any())
            {
                string uniqueSetListString = string.Join(",", uniqueSetList.ToArray());
                string url = _baseUrl + PrintConstant.QuestionUrl + "?key=" + _key + "&examId=" + examId + "&version=" + versionNo + "&printCount=" + printingCount + "&uniqueSet=" + uniqueSetListString.Replace('"',' ').Trim();
                string questionJsonString = WebHelper.GetRequest(url);
                JsonResponseQuestionViewModel questionJsonList = new JavaScriptSerializer().Deserialize<JsonResponseQuestionViewModel>(questionJsonString);
                if (questionJsonList.IsSuccess && questionJsonList.Data != null)
                {
                    questionZipUrlPath = questionJsonList.Data.QuestionSetZipPath;
                    ExamQuestionSetPrintApiDto examQuestionSetPrintApiDto = questionJsonList.Data;
                    examMcqQuestionSetPrintList = examQuestionSetPrintApiDto.ExamQuestionSetPrintApiObjects.ToList();
                }
                else
                {
                    throw new InvalidDataException(questionJsonList.Message);
                }
            }
        }

        private int _checksum_ean13(string data)
        {
            // Test string for correct length
            if (data.Length != 12 && data.Length != 13)
                return -1;

            // Test string for being numeric
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] < 0x30 || data[i] > 0x39)
                    return -1;
            }

            int sum = 0;

            int index = 1;
            for (int i = 11; i >= 0; i--)
            {
                Char c = '0';
                c = data[i];
                int de = 0;
                if (index % 2 == 0) //even
                {

                    de = (int)Char.GetNumericValue(c);
                    sum += de;
                }
                else
                {
                    de = (int)Char.GetNumericValue(c) * 3;
                    sum += de;
                }
                index++;
                //int digit = data[i] - 0x30;
                //if ((i & 0x01) == 1)
                //    sum += digit;
                //else
                //    sum += digit * 3;
            }
            int mod = sum % 10;
            return mod == 0 ? 0 : 10 - mod;
        }

        private List<ExamMcqQuestionSetPrint> CreateDummyExamMcqQuestionSetPrintList(int numberOfobjects, long examId)
        {
            List<ExamMcqQuestionSetPrint> dummyEmcqQprintSetList = new List<ExamMcqQuestionSetPrint>();
            if (numberOfobjects > 0)
            {
                for (int i = 1; i <= numberOfobjects; i++)
                {
                    int setId = (i % 3 == 0) ? 4 : i % 3;
                    string code = DateTime.Now.ToString("yyyyMMddhms");
                    if (code.Length > 12)
                    {
                        code = code.Substring(0, 12);
                    }
                    else if (code.Length < 12)
                    {
                        code = code.PadLeft(12, '0');
                    }
                    int checkSum = _checksum_ean13(code);
                    code += checkSum.ToString();
                    ExamMcqQuestionSetPrint dummyQuestionSetPrint = new ExamMcqQuestionSetPrint();
                    dummyQuestionSetPrint.Id = i;
                    dummyQuestionSetPrint.ExamId = examId;
                    dummyQuestionSetPrint.SetId = setId;
                    dummyQuestionSetPrint.SetNo = setId.ToString();
                    dummyQuestionSetPrint.UniqueSet = 1;
                    dummyQuestionSetPrint.PrintStatus = 1;
                    dummyQuestionSetPrint.CodeNo = code;
                    dummyQuestionSetPrint.EanCheckSumCodeNo = code;
                    dummyEmcqQprintSetList.Add(dummyQuestionSetPrint);
                }
            }

            return dummyEmcqQprintSetList;
        }

        private void DisableDuringImport()
        {
            probarPrintQuestion.Text = "0";
            btnStop.Enabled = true;
            btnPrint.Enabled = false;
            btnClose.Enabled = false;
        }

        private void EnableAfterImport()
        {
            //  probarPrintQuestion.Text = "0";
            probarPrintQuestion.Value = 0;
            progressText.Text = "0 %";
            btnStop.Enabled = false;
            btnPrint.Enabled = true;
            btnClose.Enabled = true;
        }

        private void DeleteFiles()
        {

            #region Delete Marge File

            if (_printConfigInfo.IsDeleteAllMargeFile)
            {
                if (margePdfList.Any())
                {
                    foreach (var margePdf in margePdfList)
                    {
                        string deleFile = PrintConstant.MargeQuestionPath + "\\Marge" + margePdf.End + PrintConstant.PdfFileExtension;
                        if (File.Exists(deleFile))
                        {
                            File.Delete(deleFile);
                        }
                    }
                }

            }

            #endregion

            #region Delete Generate File

            if (_printConfigInfo.IsDeleteAllGeneratedFile)
            {
                if (examMcqQuestionSetPrintList.Any())
                {
                    int i = 1;
                    foreach (var examMcqQuestionSetPrint in examMcqQuestionSetPrintList)
                    {
                        string deleFile = PrintConstant.NewQuestionPath + "\\" + fileConcatNameString + i + PrintConstant.PdfFileExtension;
                        if (File.Exists(deleFile))
                        {
                            File.Delete(deleFile);
                        }
                        i++;
                    }
                }
                
            }

            #endregion

            #region Unzip File Delete
            if (_printConfigInfo.IsDeleteUnzipFile)
            {
                if (allPdfFiles != null && allPdfFiles.Any())
                {
                    foreach (FileInfo file in allPdfFiles)
                    {
                        string deleFile = PrintConstant.QuestionPath + "\\" + file.Name.Trim();
                        if (File.Exists(deleFile))
                        {
                            File.Delete(deleFile);
                        }
                    }
                }
            }
            #endregion

            #region Delete Zip File
            if ( _printConfigInfo.IsDeleteZipFile && File.Exists(PrintConstant.QuestionPath + "\\" + zipFileName))
            {
                File.Delete(PrintConstant.QuestionPath + "\\" + zipFileName);
            }
            #endregion
            
        }

        private void  LoadPrintConfigInfoFromXml()
        {
            string _xmlFileName = PrintConstant.ConfigXmlFileName;
            XmlSerializer xs = new XmlSerializer(typeof(PrintConfigInfo));
            FileStream fs = new FileStream(Application.StartupPath + "\\" + _xmlFileName, FileMode.Open);
            _printConfigInfo = (PrintConfigInfo)xs.Deserialize(fs);
            fs.Close();
        }

        private List<MargePdfSartEndCompare> MargePdfGenerate(int margeNo, int totalCount)
        {
            List<MargePdfSartEndCompare> margePdfList = new List<MargePdfSartEndCompare>();
            if (totalCount <= margeNo)
            {
                MargePdfSartEndCompare tempMargePdf = new MargePdfSartEndCompare
                {
                    Start = 1,
                    End = totalCount,
                    Compare = totalCount
                };
                margePdfList.Add(tempMargePdf);
            }
            else
            {
                int listCount = totalCount / margeNo;
                int lastListCount = totalCount % margeNo;
                for (int i = 1; i <= listCount; i++)
                {
                    MargePdfSartEndCompare tempMargePdf = new MargePdfSartEndCompare
                    {
                        Start = (i == 1) ? 1 : (((i - 1) * margeNo) + 1),
                        End = margeNo * i,
                        Compare = margeNo * i
                    };
                    margePdfList.Add(tempMargePdf);
                }
                if (lastListCount > 0)
                {
                    MargePdfSartEndCompare tempMargePdf = new MargePdfSartEndCompare
                    {
                        Start = listCount * margeNo + 1,
                        End = (listCount * margeNo) + lastListCount,
                        Compare = (listCount * margeNo) + lastListCount
                    };
                    margePdfList.Add(tempMargePdf);
                }
            }

            return margePdfList;
        }

        #endregion



    }
}
