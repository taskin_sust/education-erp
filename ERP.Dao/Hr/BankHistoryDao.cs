﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IBankHistoryDao : IBaseDao<BankHistory, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<BankHistory> LoadTeamMemberBankHistory(long teamMemberId, DateTime dateTime);
        #endregion

        #region Others Function
        bool HasSameEffectiveDateEntry(long teamMemberId, DateTime effectiveDate, long id = 0);
        bool HasSameAccountNumber(long bankId, string accountNumber, long id = 0);
        #endregion

        #region Helper Function

        #endregion

    }
    public class BankHistoryDao : BaseDao<BankHistory, long>, IBankHistoryDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<BankHistory> LoadTeamMemberBankHistory(long teamMemberId, DateTime dateTime)
        {
            return Session.QueryOver<BankHistory>()
                    .Where(x => x.TeamMember.Id == teamMemberId && x.Status == BankHistory.EntityStatus.Active && x.EffectiveDate <= dateTime)
                    .OrderBy(x => x.EffectiveDate)
                    .Desc.ThenBy(x => x.Id)
                    .Desc.List<BankHistory>();
        }

        #endregion

        #region Others Function

        public bool HasSameEffectiveDateEntry(long teamMemberId, DateTime effectiveDate, long id = 0)
        {
            int rowCount = Session.QueryOver<BankHistory>()
                    .Where(x => x.TeamMember.Id == teamMemberId && x.Status != BankHistory.EntityStatus.Delete && x.EffectiveDate.Date == effectiveDate.Date && x.Id != id).RowCount();
            if (rowCount > 0)
                return true;
            return false;
        }

        public bool HasSameAccountNumber(long bankId, string accountNumber, long id = 0)
        {
            ICriteria criteria = Session.CreateCriteria<BankHistory>().Add(Restrictions.Not(Restrictions.Eq("Status", BankHistory.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("AccountNumber", accountNumber));
            criteria.CreateAlias("BankBranch", "bankBranch").Add(Restrictions.Not(Restrictions.Eq("bankBranch.Status", BankBranch.EntityStatus.Delete)));
            criteria.CreateAlias("bankBranch.Bank", "bank").Add(Restrictions.Not(Restrictions.Eq("bank.Status", Bank.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("bank.Id", bankId));
            criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            criteria.SetProjection(Projections.RowCount());
            int rowCount = Convert.ToInt32(criteria.UniqueResult());
            if (rowCount > 0)
                return true;
            return false;
        }

        #endregion

        #region Helper Function

        #endregion
    }

}
