﻿using System;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Test.Hr.HolidaySettingTest;
using Xunit;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.HolidaySettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "HolidaySetting")]
    public class HolidaySettingSaveTest : HolidaySettingBaseTest
    {

        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _holidaySettingService.Save(null));
        }

        [Fact]
        public void Save_Should_Check_Duplicate()
        {
            var dFactory1 = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            var dFactory2 = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            dFactory1.Object.HolidayType = dFactory2.Object.HolidayType = (int)HolidayTypeEnum.Management;
            dFactory1.Object.RepeatationType = dFactory2.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            _holidaySettingService.Save(dFactory1.Object);
            Assert.Throws<DuplicateEntryException>(() => _holidaySettingService.Save(dFactory2.Object));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var factoryObj = _holidaySettingFactory.Create().WithOrganization();
            factoryObj.Object.HolidayType = (int)HolidayTypeEnum.Management;
            factoryObj.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            factoryObj.Persist();
            Assert.NotNull(factoryObj.Object.Id);
            Assert.NotEqual(factoryObj.Object.Id, 0);
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Save_With_YearlyHoliday_Successfully()
        {
            var holidaySettingFactory = _holidaySettingFactory.Create().WithOrganization();
            holidaySettingFactory.Object.HolidayType = (int)HolidayTypeEnum.Management;
            holidaySettingFactory.Object.RepeatationType = (int)HolidayRepeatationType.Yearly;
            holidaySettingFactory.Persist();
            Assert.NotNull(holidaySettingFactory.Object.Id);
            Assert.NotEqual(holidaySettingFactory.Object.Id, 0);
            var yearlyHoliday = _YearlyHolidayService.GetYearlyHoliday(holidaySettingFactory.Object.Name, holidaySettingFactory.Object.Organization.Id);
            var yearlyHolidayId = yearlyHoliday.Id;
            _holidaySettingFactory.DeleteYearlyHoliday(yearlyHolidayId);
            Assert.NotNull(yearlyHolidayId);
            Assert.NotEqual(yearlyHolidayId, 0);
            
        }

        [Fact]
        public void Save_Should_Check_Property()
        {
            HolidaySettingFactory factoryObj = _holidaySettingFactory.Create();
            Assert.Throws<ValueNotAssignedException>(() => _holidaySettingService.Save(factoryObj.Object));
        }

        [Fact]
        public void ValueNotAssignedException_For_Organization_Null()
        {
            var factoryObj = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            factoryObj.Object.HolidayType = (int)HolidayTypeEnum.Management;
            factoryObj.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            factoryObj.Object.Organization = null;
            Assert.Throws<ValueNotAssignedException>(() => _holidaySettingService.Save(factoryObj.Object));
        }
        
        [Fact]
        public void ValueNotAssignedException_For_HolidayType_Invalid()
        {
            var factoryObj = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            factoryObj.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            factoryObj.Object.HolidayType = 0;
            Assert.Throws<ValueNotAssignedException>(() => _holidaySettingService.Save(factoryObj.Object));
        }


        [Fact]
        public void ValueNotAssignedException_For_Name_Empty()
        {
            var factoryObj = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            factoryObj.Object.HolidayType = (int)HolidayTypeEnum.Management;
            factoryObj.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            factoryObj.Object.Name = string.Empty;
            Assert.Throws<ValueNotAssignedException>(() => _holidaySettingService.Save(factoryObj.Object));
        }

        [Fact]
        public void Save_Should_Check_DateFrom_DateTo()
        {
            var factoryObj = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            factoryObj.Object.HolidayType = (int)HolidayTypeEnum.Management;
            factoryObj.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            factoryObj.Object.DateFrom = DateTime.Now.AddDays(3);
            factoryObj.Object.DateTo = DateTime.Now.AddDays(1);
            Assert.Throws<InvalidDataException>(() => _holidaySettingService.Save(factoryObj.Object));
        }

        [Fact]
        public void Save_Should_Check_SavingDate()
        {
            var factoryObj = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            factoryObj.Object.HolidayType = (int)HolidayTypeEnum.Management;
            factoryObj.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            factoryObj.Object.DateFrom = DateTime.Now.AddDays(-1);
            Assert.Throws<InvalidDataException>(() => _holidaySettingService.Save(factoryObj.Object));
        }
        #endregion

    }
}
