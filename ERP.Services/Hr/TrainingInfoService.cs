﻿using System;
using System.Collections;
using System.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{
    public interface ITrainingInfoService : IBaseService
    {
        #region Operational Function
        List<long> UpdateTrainingInfo(TeamMember member, List<TeamMemberTrainingInfoArray> trainingInfoArray);
        List<long> UpdateProfessionalQualificationsInfo(TeamMember member, List<TeamMemberProfessionalQualificationInfoArray> pqInfoArray);
        bool DeleteTrainingInfo(long id);
        #endregion

        #region Single Instances Loading Function
        TrainingInfo GetMemberTrainingInfoById(long trainingInfoId);
        #endregion

        #region List Loading Function
        IList<TrainingInfo> LoadMemberTraningInfo(long memberId);
        IList<TrainingInfo> LoadMemberProfessionalQualificationsInfo(long memberId);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class TrainingInfoService : BaseService, ITrainingInfoService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly ITrainingInfoDao _hrTrainingInfoDao;
        private readonly IInstituteDao _instituteDao;

        public TrainingInfoService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrTrainingInfoDao = new TrainingInfoDao() { Session = session };
            _instituteDao = new InstituteDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public List<long> UpdateTrainingInfo(TeamMember member, List<TeamMemberTrainingInfoArray> trainingInfoArray)
        {
            List<long> idList = new List<long>();
            try
            {
                ITransaction trans = null;
                if (member == null)
                {
                    throw new NullObjectException("MemberNullFound");
                }
                if (trainingInfoArray == null)
                {
                    throw new NullObjectException("NullObject");
                }
                using (trans = Session.BeginTransaction())
                {
                    if (trainingInfoArray.Any())
                    {
                        int rank = 1;
                        foreach (var row in trainingInfoArray)
                        {
                            TrainingInfo trainingInfoEntity = null;
                            if (row.Id > 0)
                            {
                                var oldEntity = _hrTrainingInfoDao.LoadById(row.Id);
                                trainingInfoEntity = oldEntity ?? new TrainingInfo();
                            }
                            else
                            {
                                trainingInfoEntity = new TrainingInfo();
                            }
                            var instituteEnitty = _instituteDao.LoadById(Convert.ToInt64(row.TrainingInstitute));

                            trainingInfoEntity.TrainingTitle = row.TrainingTitle;
                            trainingInfoEntity.TopicCovere = row.TrainingTopic;
                            trainingInfoEntity.Duration = row.TrainingDuration;
                            trainingInfoEntity.Institute = instituteEnitty;
                            trainingInfoEntity.Location = row.TrainingLocation;
                            trainingInfoEntity.Rank = rank;
                            trainingInfoEntity.IsTraining = true;
                            trainingInfoEntity.CreationDate = DateTime.Now;
                            trainingInfoEntity.ModificationDate = DateTime.Now;
                            trainingInfoEntity.Status = AcademicInfo.EntityStatus.Active;
                            trainingInfoEntity.TeamMember = member;
                            if (row.PassingYear > 0)
                            {
                                trainingInfoEntity.Year = row.PassingYear;
                            }

                            _hrTrainingInfoDao.SaveOrUpdate(trainingInfoEntity);
                            
                            //row.Id = trainingInfoEntity.Id;
                            idList.Add(trainingInfoEntity.Id);
                            rank++;
                        }
                    }
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            return idList;
        }
        public List<long> UpdateProfessionalQualificationsInfo(TeamMember member, List<TeamMemberProfessionalQualificationInfoArray> pQInfoArray)
        {
            List<long> idList = new List<long>();
            try
            {
                ITransaction trans = null;
                using (trans = Session.BeginTransaction())
                {
                    if (pQInfoArray.Any())
                    {
                        int rank = 1;
                        foreach (var row in pQInfoArray)
                        {
                            TrainingInfo pQInfoEntity = null;
                            if (row.Id > 0)
                            {
                                var oldEntity = _hrTrainingInfoDao.LoadById(row.Id);
                                pQInfoEntity = oldEntity ?? new TrainingInfo();
                            }
                            else
                            {
                                pQInfoEntity = new TrainingInfo();
                            }
                            pQInfoEntity.Certification = row.Certification;
                            pQInfoEntity.Duration = row.Duration;
                            pQInfoEntity.Institute = _instituteDao.LoadById(Convert.ToInt64(row.Institute));
                            pQInfoEntity.Location = row.Location;
                            pQInfoEntity.Rank = rank;
                            pQInfoEntity.IsTraining = false;
                            pQInfoEntity.CreationDate = DateTime.Now;
                            pQInfoEntity.ModificationDate = DateTime.Now;
                            pQInfoEntity.Status = AcademicInfo.EntityStatus.Active;
                            pQInfoEntity.TeamMember = member;

                            pQInfoEntity.Year = row.Year > 0 ? row.Year : 0;

                            _hrTrainingInfoDao.SaveOrUpdate(pQInfoEntity);
                            idList.Add(pQInfoEntity.Id);
                            rank++;
                        }
                    }
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            return idList;
        }
        public bool DeleteTrainingInfo(long id)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {

                    TrainingInfo trainingInfo = _hrTrainingInfoDao.LoadById(id);
                    trainingInfo.Status = TrainingInfo.EntityStatus.Delete;
                    _hrTrainingInfoDao.SaveOrUpdate(trainingInfo);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public TrainingInfo GetMemberTrainingInfoById(long trainingInfoId)
        {
            try
            {
                TrainingInfo entity = _hrTrainingInfoDao.LoadById(trainingInfoId);
                return entity ?? null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function

        public IList<TrainingInfo> LoadMemberTraningInfo(long memberId)
        {
            try
            {
                IList<TrainingInfo> list = _hrTrainingInfoDao.LoadAllByMemberId(memberId);
                return list != null && list.Count > 0 ? list : null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<TrainingInfo> LoadMemberProfessionalQualificationsInfo(long memberId)
        {
            try
            {
                IList<TrainingInfo> list = _hrTrainingInfoDao.LoadAllProfessionalQByMemberId(memberId);
                return list != null && list.Count > 0 ? list : null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}
