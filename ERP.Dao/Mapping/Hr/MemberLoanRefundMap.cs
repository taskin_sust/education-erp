using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{
    public partial class MemberLoanRefundMap : BaseClassMap<MemberLoanRefund, long>
    {
        public MemberLoanRefundMap()
        {
            Table("HR_MemberLoanRefund");
            LazyLoad();

            Map(x => x.RefundAmount).Column("RefundAmount").Not.Nullable();
            Map(x => x.Remarks).Column("Remarks");
            Map(x => x.RefundDate).Column("RefundDate").Not.Nullable();
            Map(x => x.ReceiptNo).Column("ReceiptNo").Not.Nullable();
            //Map(x => x.MemberPin).Column("MemberPin");
            //Map(x => x.MemberName).Column("MemberName");
            //Map(x => x.ReceivedBy).Column("ReceivedBy");
            Map(x => x.ReceivedById).Column("ReceivedById");
            Map(x => x.LoanAmount).Column("LoanAmount");
            Map(x => x.DueAmount).Column("DueAmount");

            References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.SalaryOrganization).Column("SalaryOrganizationId");
            References(x => x.SalaryBranch).Column("SalaryBranchId");
            References(x => x.SalaryCampus).Column("SalaryCampusId");
        }
    }
}
