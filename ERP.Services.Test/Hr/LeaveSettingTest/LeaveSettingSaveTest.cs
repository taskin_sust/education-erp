﻿using System;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.LeaveSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "LeaveSettings")]
    public class LeaveSettingSaveTest : LeaveSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Save_Successfully()
        {
            leaveFactory.Create().Persist();
            var obj = _leaveService.GetById(leaveFactory.Object.Id);
            Assert.Equal(leaveFactory.Object.Id, obj.Id);
            idList.Add(leaveFactory.Object.Id);
        }

        [Fact]
        public void Should_Return_Null_Exception()
        {
            Leave leave = null;
            Assert.Throws<NullObjectException>(() => _leaveService.Save(leave));
        }

        #endregion

        #region Business Logic Test
        
        [Fact]
        public void Should_Return_EmptyLeaveException()
        {
            Leave leave = leaveFactory.Create().WithOrganization().Object;
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;

            Assert.Throws<InvalidDataException>(() => _leaveService.LeaveSettingCheckAndSave(leave.Organization.Id, "", isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                leave.EffectiveDate.ToString(), new string[] { "1", "2" }, new string[] { "1", "2" },
                new string[] { "1", "2" }, isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(),
                ""));
        }

        [Fact]
        public void Should_Return_EmptyEffectivedateException()
        {
            Leave leave = leaveFactory.Create().WithOrganization().Object;
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;
            Assert.Throws<InvalidDataException>(() => _leaveService.LeaveSettingCheckAndSave(leave.Organization.Id, leave.Name, isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                "", new string[] { "1", "2" }, new string[] { "1", "2" },
                new string[] { "1", "2" }, isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(),
                ""));
        }

        [Fact]
        public void Should_Return_EmptyGenderException()
        {
            Leave leave = leaveFactory.Create().WithOrganization().Object;
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;
            Assert.Throws<InvalidDataException>(() => _leaveService.LeaveSettingCheckAndSave(leave.Organization.Id, leave.Name, isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                DateTime.Now.ToString(), new string[] { }, new string[] { "1", "2" },
                new string[] { "1", "2" }, isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(),
                ""));
        }

        [Fact]
        public void Should_Return_EmptyemployeementException()
        {
            Leave leave = leaveFactory.Create().WithOrganization().Object;
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;
            Assert.Throws<InvalidDataException>(() => _leaveService.LeaveSettingCheckAndSave(leave.Organization.Id, leave.Name, isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                DateTime.Now.ToString(), new string[] { "1", "2" }, new string[] { },
                new string[] { "1", "2" }, isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(),
                ""));
        }

        [Fact]
        public void Should_Return_EmptyMaritalException()
        {
            Leave leave = leaveFactory.Create().WithOrganization().Object;
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;
            Assert.Throws<InvalidDataException>(() => _leaveService.LeaveSettingCheckAndSave(leave.Organization.Id, leave.Name, isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                DateTime.Now.ToString(), new string[] { "1", "2" }, new string[] { "1", "2" }, new string[] { },
                 isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(),
                ""));
        }

        [Fact]
        public void Should_Return_EmptyMaxCarryDaysException()
        {
            Leave leave = leaveFactory.Create().WithOrganization().Object;
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;
            Assert.Throws<InvalidDataException>(() => _leaveService.LeaveSettingCheckAndSave(leave.Organization.Id, leave.Name, isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                DateTime.Now.ToString(), new string[] { "1", "2" }, new string[] { "1", "2" }, new string[] { "1", "2" },
                 isCarryforward.ToString(), "",
                isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(),
                ""));
        }

        [Fact]
        public void Should_Return_EmptyMaxTakingException()
        {
            Leave leave = leaveFactory.Create().WithOrganization().Object;
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;
            Assert.Throws<InvalidDataException>(() => _leaveService.LeaveSettingCheckAndSave(leave.Organization.Id, leave.Name, isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                DateTime.Now.ToString(), new string[] { "1", "2" }, new string[] { "1", "2" }, new string[] { "1", "2" },
                 isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                isTakingLimit.ToString(), "", leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(),
                ""));
        }

        [Fact]
        public void Should_Return_EmptyMinEncashException()
        {
            Leave leave = leaveFactory.Create().WithOrganization().Object;
            //Leave leave = GenerateLocalLeave();
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;
            Assert.Throws<InvalidDataException>(() => _leaveService.LeaveSettingCheckAndSave(leave.Organization.Id, leave.Name, isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                DateTime.Now.ToString(), new string[] { "1", "2" }, new string[] { "1", "2" }, new string[] { "1", "2" },
                 isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), "",
                ""));
        }

        [Fact]
        public void Should_Return_EmptyOrganizationException()
        {
            Leave leave = leaveFactory.Create().WithOrganization().Object;
            //Leave leave = GenerateLocalLeave();
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;
            Assert.Throws<InvalidDataException>(() => _leaveService.LeaveSettingCheckAndSave(0, leave.Name, isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                DateTime.Now.ToString(), new string[] { "1", "2" }, new string[] { "1", "2" }, new string[] { "1", "2" },
                 isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), "",
                ""));
        }

        #endregion
        
    }
}
