using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Mapping.UserAuth {


    public partial class UserMenuMap : BaseClassMap<UserMenu, long>
    {
        
        public UserMenuMap() {
			Table("UserMenu");
			LazyLoad();
            //Not.LazyLoad();
            Map(x => x.Name);
            References(x => x.User).Column("UserProfileId").LazyLoad();
            References(x => x.Menu).Column("MenuId").LazyLoad();
            References(x => x.Branch).Column("BranchId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Organization).Column("OrganizationId");
        }
    }
}
