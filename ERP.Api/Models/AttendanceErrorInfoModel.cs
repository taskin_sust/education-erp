﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UdvashERP.Api.Models
{
    public class AttendanceErrorInfoModel
    {
        public int EnrollNo { get; set; }
        public long DeviceId { get; set; }
        public int Pin { get; set; }
        public DateTime LogTime { get; set; }
    }
}