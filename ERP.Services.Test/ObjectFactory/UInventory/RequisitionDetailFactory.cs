using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class RequisitionDetailFactory : ObjectFactoryBase<RequisitionDetail>
   {

       private readonly IRequisitionDetailService _requisitionDetailService;

       public RequisitionDetailFactory(IObjectFactoryBase caller, ISession session): base(caller, session){

       _requisitionDetailService = new RequisitionDetailService(session);

       }

       #region Create 

       public RequisitionDetailFactory Create(){

       Object = null;
       SingleObjectList.Add(Object);
       return this;
       }

       public RequisitionDetailFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public RequisitionDetailFactory Persist()       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _requisitionDetailService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x =>{if (x.Id == 0)_requisitionDetailService.SaveOrUpdate(x);});

          }
          return this;
       }

       public RequisitionDetailFactory CreateMore(int num){

       for (int i = 0; i < num; i++){
       var obj = new RequisitionDetail(){};
       ObjectList.Add(obj);}
       return this;}

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public RequisitionDetailFactory CleanUp()
       {

          DeleteAll();
          return this;
       }

       #endregion

    }
}
