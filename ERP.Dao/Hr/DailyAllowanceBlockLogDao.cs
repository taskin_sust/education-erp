using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IDailyAllowanceBlockLogDao : IBaseDao<DailySupportAllowanceBlockLog, long>
    {
        void SaveLog(DailySupportAllowanceBlockLog log);
    }

    public class DailyAllowanceBlockLogDao : BaseDao<DailySupportAllowanceBlockLog, long>, IDailyAllowanceBlockLogDao
    {
        #region Properties & Object & Initialization

        #endregion

        #region Operational Function
        public void SaveLog(DailySupportAllowanceBlockLog log)
        {
            if (log != null)
            {
                Session.Save(log);
            }
        }
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
