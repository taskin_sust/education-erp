﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Authentication;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.Areas.Student.Controllers;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class ChildrenEducationController : Controller
    {
        #region Logger

        private ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> authorizeMenu;
        private ISession _session;

        private readonly IOrganizationService _organizationService;
        private readonly IChildrenAllowanceSettingService _childrenAllowanceSettingService;
        private readonly IChildrenAllowanceBlockService _allowanceBlockService;
        private readonly IUserService _userService;

        public ChildrenEducationController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();

            _organizationService = new OrganizationService(_session);
            _childrenAllowanceSettingService = new ChildrenAllowanceSettingService(_session);
            _userService = new UserService(_session);
            _allowanceBlockService = new ChildrenAllowanceBlockService(_session);
        }

        #endregion

        #region Operational

        #region Create

        [HttpGet]
        public ActionResult Create(string message = "", int? type = 0)
        {
            try
            {
                Initialize();
                switch (type)
                {
                    case 1:
                        ViewBag.SuccessMessage = message;
                        break;
                    case 2:
                        ViewBag.ErrorMessage = message;
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ChildrenEduViewModel childrenEduViewModel)
        {
            var messsage = "";
            try
            {
                if (ModelState.IsValid)
                {
                    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    ChildrenAllowanceSetting childrenAllowanceSetting = FormModelFromVm(childrenEduViewModel);
                    _childrenAllowanceSettingService.SaveOrUpdate(authorizeMenu, childrenAllowanceSetting);
                    messsage = "Operation completed successfully";
                    return RedirectToAction("Create", new { message = messsage, type = 1 /*success*/ });
                }
                IEnumerable<ModelError> modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
                messsage = modelStateErrors.Aggregate(messsage, (current, modelStateError) => current + (modelStateError.ErrorMessage + " "));
                return RedirectToAction("Create", new { message = messsage, type = 2 /*failed*/ });
            }
            catch (AuthenticationException ae)
            {
                messsage = ae.Message;
            }
            catch (MessageException e)
            {
                messsage = e.Message;
            }
            catch (DuplicateEntryException dex)
            {
                messsage = dex.Message;
            }
            catch (InvalidDataException ide)
            {
                messsage = ide.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                messsage = WebHelper.CommonErrorMessage;
            }
            return RedirectToAction("Create", new { message = messsage, type = 2 /*fail*/});
        }

        #endregion

        #region Edit

        [HttpGet]
        public ActionResult Edit(long id, string message = "", int? type = 0)
        {
            ChildrenEduViewModel childrenAllowanceSettingVm = null;
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                ChildrenAllowanceSetting childrenAllowanceSetting = _childrenAllowanceSettingService.GetChildrenAllowanceSetting(id);
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName", childrenAllowanceSetting.Organization.Id);
                ViewBag.employmentStatus = _commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int>() { (int)MemberEmploymentStatus.Retired });
                ViewBag.paymentTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentType>(new List<int>()
                        {
                            (int) PaymentType.Once,
                            (int) PaymentType.Daily
                        }), "Key", "Value", childrenAllowanceSetting.PaymentType);
                childrenAllowanceSettingVm = FormVmFromModel(childrenAllowanceSetting);
                switch (type)
                {
                    case 1:
                        ViewBag.SuccessMessage = message;
                        break;
                    case 2:
                        ViewBag.ErrorMessage = message;
                        break;
                }
            }
            catch (InvalidDataException e)
            {
                ViewBag.ErrorMessage = e.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View(childrenAllowanceSettingVm);
        }

        [HttpPost]
        public ActionResult Edit(ChildrenEduViewModel childrenEduViewModel)
        {
            var proxyObj = new ChildrenAllowanceSetting();
            var messsage = "";
            try
            {
                if (!ModelState.IsValid)
                {
                    IEnumerable<ModelError> modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
                    messsage = modelStateErrors.Aggregate(messsage, (current, modelStateError) => current + (modelStateError.ErrorMessage + " "));
                    return RedirectToAction("Edit", new { message = messsage, type = 2 /*failed*/ });
                }
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                proxyObj = _childrenAllowanceSettingService.GetChildrenAllowanceSetting(childrenEduViewModel.Id);
                proxyObj.Name = childrenEduViewModel.AllowanceName;
                proxyObj.Amount = childrenEduViewModel.Amount;
                proxyObj.MaxNoOfChild = childrenEduViewModel.NoOfChild;
                proxyObj.Organization = _organizationService.LoadById(childrenEduViewModel.OrganizationId);
                proxyObj.PaymentType = childrenEduViewModel.PaymentType;
                proxyObj.StartingAge = childrenEduViewModel.StartingAge;
                proxyObj.ClosingAge = childrenEduViewModel.ClosingAge;
                proxyObj.EffectiveDate = childrenEduViewModel.EffectiveFrom;
                proxyObj.ClosingDate = childrenEduViewModel.ClosingDate;
                proxyObj.IsContractual = false;
                proxyObj.IsPartTime = false;
                proxyObj.IsProbation = false;
                proxyObj.IsPermanent = false;
                proxyObj.IsIntern = false;
                foreach (var empStatus in childrenEduViewModel.EmloymentStatus)
                {
                    switch (empStatus)
                    {
                        case MemberEmploymentStatus.Contractual:
                            proxyObj.IsContractual = true;
                            break;
                        case MemberEmploymentStatus.PartTime:
                            proxyObj.IsPartTime = true;
                            break;
                        case MemberEmploymentStatus.Probation:
                            proxyObj.IsProbation = true;
                            break;
                        case MemberEmploymentStatus.Permanent:
                            proxyObj.IsPermanent = true;
                            break;
                        case MemberEmploymentStatus.Intern:
                            proxyObj.IsIntern = true;
                            break;
                    }
                }
                proxyObj.MemberEmploymentStatuses = childrenEduViewModel.EmloymentStatus;
                _childrenAllowanceSettingService.SaveOrUpdate(authorizeMenu, proxyObj);
                messsage = "Operation completed successfully";
                return RedirectToAction("Edit", new { id = proxyObj.Id, message = messsage, type = 1 /*success*/ });
            }
            catch (AuthenticationException ae)
            {
                messsage = ae.Message;
            }
            catch (MessageException e)
            {
                messsage = e.Message;
            }
            catch (NullObjectException nullObjectException)
            {
                messsage = nullObjectException.Message;
            }
            catch (InvalidDataException invalidDataException)
            {
                messsage = invalidDataException.Message;
            }
            catch (DuplicateEntryException duplicateEntryException)
            {
                messsage = duplicateEntryException.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                messsage = WebHelper.CommonErrorMessage;
            }
            return RedirectToAction("Edit", new { id = proxyObj.Id, message = messsage, type = 2 /*fail*/});
        }

        #endregion

        #region Details

        [HttpGet]
        public ActionResult Details(long id)
        {
            try
            {
                var entity = _childrenAllowanceSettingService.GetChildrenAllowanceSetting(id);
                var fm = DetailsVmFromModel(entity);
                return View(fm);
            }
            catch (InvalidDataException invalidDataException)
            {
                ViewBag.ErrorMessage = invalidDataException.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("ManageChildrenEducationAllowance");
            }
            return View(new ChildrenAllowanceSettingDetailsVm());
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                var list = _allowanceBlockService.LoadAll(id);
                if (list.Count > 0)
                    return Json(new { isSuccess = false, Text = "Some members are blocked! InOrder to delete please unblock them first" });
                var entity = _childrenAllowanceSettingService.GetChildrenAllowanceSetting(id);
                _childrenAllowanceSettingService.Delete(entity);
                return Json(new { isSuccess = true, Text = "Settings Successfully Deleted!" });
            }
            catch (AuthenticationException ae)
            {
                return Json(new { isSuccess = false, Text = ae.Message });
            }
            catch (DuplicateEntryException dex)
            {
                return Json(new { isSuccess = false, Text = dex.Message });
            }
            catch (InvalidDataException ide)
            {
                return Json(new { isSuccess = false, Text = ide.Message });
            }
            catch (MessageException e)
            {
                return Json(new { isSuccess = false, Text = e.Message });
            }
            catch (DependencyException ex)
            {
                return Json(new { isSuccess = false, Text = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { isSuccess = false, Text = WebHelper.CommonErrorMessage });
            }
        }

        #endregion

        #endregion

        #region Manage Children Allowance Settings

        [HttpGet]
        public ActionResult ManageChildrenEducationAllowance()
        {
            try
            {
                Initialize();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        public ActionResult ManageChildrenEducationAllowance(int draw, int start, int length, long? organizationId, int? empStatus, int? payType)
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<ChildrenAllowanceSetting> cEAllowances = _childrenAllowanceSettingService.LoadChilderenAllowanceSetting(draw, start, length, authorizeMenu, organizationId, empStatus, payType);
                int count = _childrenAllowanceSettingService.GetChildrenAllowanceSettingAllowanceCount(authorizeMenu, organizationId, empStatus, payType);

                var data = new List<object>();
                foreach (var allowance in cEAllowances)
                {
                    var str = new List<string>();
                    var employeeStatus = "";
                    str.Add(allowance.Organization.ShortName);
                    str.Add(allowance.Name);
                    if (allowance.IsContractual) employeeStatus += MemberEmploymentStatus.Contractual + ", ";
                    if (allowance.IsPartTime) employeeStatus += MemberEmploymentStatus.PartTime + ", ";
                    if (allowance.IsPermanent) employeeStatus += MemberEmploymentStatus.Permanent + ", ";
                    if (allowance.IsProbation) employeeStatus += MemberEmploymentStatus.Probation + ", ";
                    if (allowance.IsIntern) employeeStatus += MemberEmploymentStatus.Intern + ", ";
                    str.Add(employeeStatus.Trim().Remove(employeeStatus.Length - 2));
                    str.Add(allowance.MaxNoOfChild.ToString());
                    str.Add(allowance.Amount.ToString());
                    str.Add(allowance.StartingAge.ToString());
                    str.Add(allowance.ClosingAge.ToString());
                    switch (allowance.PaymentType)
                    {
                        case (int)PaymentType.Monthly: str.Add(PaymentType.Monthly.ToString()); break;
                        case (int)PaymentType.Yearly: str.Add(PaymentType.Yearly.ToString()); break;
                    }
                    str.Add(DateTime.MinValue != allowance.EffectiveDate ? allowance.EffectiveDate.ToString("MMM dd,yyyy") : " ");
                    str.Add(allowance.ClosingDate != null ? allowance.ClosingDate.Value.ToString("MMM dd,yyyy") : " ");
                    str.Add(LinkGenerator.GetGeneratedDetailsEditLink("Details", "Edit", "ChildrenEducation", allowance.Id)
                            + LinkGenerator.GetDeleteLink("Delete", "ChildrenEducation", allowance.Id));
                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = count,
                    recordsFiltered = count,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                var data = new List<object>();
                _logger.Error(ex);
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });
            }
        }

        public ActionResult Index()
        {
            return RedirectToAction("ManageChildrenEducationAllowance");
        }

        #endregion

        #region Helper

        public void Initialize()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            ViewBag.paymentTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentType>(new List<int>() { (int)PaymentType.Once, (int)PaymentType.Daily }), "Key", "Value");
            ViewBag.employmentStatus = _commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int>() { (int)MemberEmploymentStatus.Retired });
            ViewBag.employmentDropdownStatus = new SelectList(_commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int>() { (int)MemberEmploymentStatus.Retired }), "Key", "Value");
        }

        private ChildrenAllowanceSetting FormModelFromVm(ChildrenEduViewModel childrenEduViewModel)
        {
            var cEAllowancereturn = new ChildrenAllowanceSetting
            {
                Organization = _organizationService.LoadById(childrenEduViewModel.OrganizationId),
                Amount = childrenEduViewModel.Amount,
                Name = childrenEduViewModel.AllowanceName,
                MaxNoOfChild = childrenEduViewModel.NoOfChild,
                StartingAge = childrenEduViewModel.StartingAge,
                ClosingAge = childrenEduViewModel.ClosingAge,
                EffectiveDate = childrenEduViewModel.EffectiveFrom,
                ClosingDate = childrenEduViewModel.ClosingDate,
                PaymentType = childrenEduViewModel.PaymentType
            };
            cEAllowancereturn.MemberEmploymentStatuses = childrenEduViewModel.EmloymentStatus;
            foreach (var empStatus in childrenEduViewModel.EmloymentStatus)
            {
                switch (empStatus)
                {
                    case MemberEmploymentStatus.Contractual:
                        cEAllowancereturn.IsContractual = true;
                        break;
                    case MemberEmploymentStatus.PartTime:
                        cEAllowancereturn.IsPartTime = true;
                        break;
                    case MemberEmploymentStatus.Probation:
                        cEAllowancereturn.IsProbation = true;
                        break;
                    case MemberEmploymentStatus.Permanent:
                        cEAllowancereturn.IsPermanent = true;
                        break;
                    case MemberEmploymentStatus.Intern:
                        cEAllowancereturn.IsIntern = true;
                        break;
                }
            }
            return cEAllowancereturn;
        }

        private ChildrenEduViewModel FormVmFromModel(ChildrenAllowanceSetting childrenAllowanceSetting)
        {
            var model = new ChildrenEduViewModel
            {
                Id = childrenAllowanceSetting.Id,
                OrgName = childrenAllowanceSetting.Organization.ShortName,
                OrganizationId = childrenAllowanceSetting.Organization.Id,
                AllowanceName = childrenAllowanceSetting.Name,
                NoOfChild = childrenAllowanceSetting.MaxNoOfChild,
                Amount = childrenAllowanceSetting.Amount,
                StartingAge = childrenAllowanceSetting.StartingAge,
                ClosingAge = childrenAllowanceSetting.ClosingAge,
                EffectiveFrom = childrenAllowanceSetting.EffectiveDate,
                ClosingDate = childrenAllowanceSetting.ClosingDate,
                PaymentType = childrenAllowanceSetting.PaymentType,
            };
            var list = new List<MemberEmploymentStatus>();
            model.EmloymentStatus = new List<MemberEmploymentStatus>();
            if (childrenAllowanceSetting.IsContractual)
                list.Add(MemberEmploymentStatus.Contractual);
            if (childrenAllowanceSetting.IsPartTime)
                list.Add(MemberEmploymentStatus.PartTime);
            if (childrenAllowanceSetting.IsPermanent)
                list.Add(MemberEmploymentStatus.Permanent);
            if (childrenAllowanceSetting.IsProbation)
                list.Add(MemberEmploymentStatus.Probation);
            if (childrenAllowanceSetting.IsIntern)
                list.Add(MemberEmploymentStatus.Intern);
            model.EmloymentStatus = list;
            return model;
        }

        private ChildrenAllowanceSettingDetailsVm DetailsVmFromModel(ChildrenAllowanceSetting entity)
        {
            ViewBag.EmploymentStatusList = new List<MemberEmploymentStatus>()
                                        {
                                            MemberEmploymentStatus.Contractual,
                                            MemberEmploymentStatus.PartTime,
                                            MemberEmploymentStatus.Permanent,
                                            MemberEmploymentStatus.Probation,
                                            MemberEmploymentStatus.Intern
                                        };
            var model = new ChildrenAllowanceSettingDetailsVm
            {
                OrganizationName = entity.Organization.ShortName,
                AllowanceName = entity.Name,
                NoOfChild = entity.MaxNoOfChild.ToString(CultureInfo.InvariantCulture),
                Amount = entity.Amount.ToString(CultureInfo.InvariantCulture),
                StartingAge = entity.StartingAge.ToString(CultureInfo.InvariantCulture),
                ClosingAge = entity.ClosingAge.ToString(CultureInfo.InvariantCulture),
                EffectiveFrom = entity.EffectiveDate.ToString("d MMM, yyyy"),
                ClosingDate = entity.ClosingDate != null ? entity.ClosingDate.Value.ToString("d MMM, yyyy") : string.Empty,
                PaymentType = entity.PaymentType.ToString(CultureInfo.InvariantCulture),
                CreateBy = _userService.GetUserNameByAspNetUserId(entity.CreateBy),
                ModifiedBy = _userService.GetUserNameByAspNetUserId(entity.ModifyBy),
                CreationDate = entity.CreationDate.ToString("d MMM, yyyy"),
                ModificationDate = entity.ModificationDate.ToString("d MMM, yyyy")
            };
            var list = new List<MemberEmploymentStatus>();
            model.EmloymentStatus = new List<MemberEmploymentStatus>();
            if (entity.IsContractual)
                list.Add(MemberEmploymentStatus.Contractual);
            if (entity.IsPartTime)
                list.Add(MemberEmploymentStatus.PartTime);
            if (entity.IsPermanent)
                list.Add(MemberEmploymentStatus.Permanent);
            if (entity.IsProbation)
                list.Add(MemberEmploymentStatus.Probation);
            if (entity.IsIntern)
                list.Add(MemberEmploymentStatus.Intern);
            model.EmloymentStatus = list;
            return model;
        }

        #endregion
    }
}