using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IDailyAllowanceBlockDao : IBaseDao<DailySupportAllowanceBlock, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        DailySupportAllowanceBlock GetTeamMemberDailyAllowanceBlock(long teamMemberId, DateTime date);
        DailySupportAllowanceBlock GetTeamMemberLastIsPostDailySupportAllowanceBlock(long? teamMemberId = null, int? pin = null);
        DailySupportAllowanceBlock GetTeamMemberLastIsBlockedDailySupportAllowanceBlock(long? teamMemberId = null, int? pin = null);

        #endregion

        #region List Loading Function

        IList<DailySupportAllowanceBlockDto> LoadDailyAllownceBlockingList(List<long> organizationIdList, List<long> branchIdList, DateTime date, List<int> pinList);
        IList<DailySupportAllowanceBlockListDto> LoadDailyAllownceBlockingList(int start, int length, string orderBy, string orderDir, List<long> authOrgList, List<long> bIds, long deptId, long capId, List<int> mentorTeamMemberIdList, int status, DateTime dateFromDateTime, DateTime dateToDateTime);
        IList<DailySupportAllowanceBlock> LoadDailyAllownceBlocking(DateTime dateFrom, DateTime dateTo, long teamMemberId);


        #endregion

        #region Others Function

        int CountDailyAllownceBlockingList(List<long> authOrgList, List<long> bIds, long deptId, long capId, List<int> memberPinList, int status, DateTime dateFromDateTime, DateTime dateToDateTime);

        #endregion

    }

    public class DailyAllowanceBlockDao : BaseDao<DailySupportAllowanceBlock, long>, IDailyAllowanceBlockDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public DailySupportAllowanceBlock GetTeamMemberDailyAllowanceBlock(long teamMemberId, DateTime date)
        {
            ICriteria criteria = Session.CreateCriteria<DailySupportAllowanceBlock>().Add(Restrictions.Eq("Status", DailySupportAllowanceBlock.EntityStatus.Active)).Add(Restrictions.Eq("BlockingDate", date.Date)).Add(Restrictions.Eq("IsBlocked", true));
            criteria.CreateAlias("TeamMember", "member").Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active)).Add(Restrictions.Eq("member.Id", teamMemberId));

            criteria.SetTimeout(3000);
            return criteria.UniqueResult<DailySupportAllowanceBlock>();
        }

        public DailySupportAllowanceBlock GetTeamMemberLastIsPostDailySupportAllowanceBlock(long? teamMemberId = null, int? pin = null)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<DailySupportAllowanceBlock>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", DailySupportAllowanceBlock.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("IsPost", true));
            criteria.CreateAlias("TeamMember", "member");
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("BlockingDate"));
            return criteria.List<DailySupportAllowanceBlock>().Take(1).FirstOrDefault();
        }

        public DailySupportAllowanceBlock GetTeamMemberLastIsBlockedDailySupportAllowanceBlock(long? teamMemberId = null, int? pin = null)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<DailySupportAllowanceBlock>();
            criteria.Add(Restrictions.Eq("Status", DailySupportAllowanceBlock.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("IsBlocked", true));
            criteria.CreateAlias("TeamMember", "member");
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("BlockingDate"));
            return criteria.List<DailySupportAllowanceBlock>().Take(1).FirstOrDefault();
        }

        #endregion

        #region List Loading Function

        public IList<DailySupportAllowanceBlockDto> LoadDailyAllownceBlockingList(List<long> organizationIdList, List<long> branchIdList, DateTime date, List<int> pinList)
        {
            string sqlQuery = GenerateBlockingListQuery(organizationIdList, branchIdList, date, pinList);
            IQuery iQuery = Session.CreateSQLQuery(sqlQuery);
            iQuery.SetResultTransformer(Transformers.AliasToBean<DailySupportAllowanceBlockDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<DailySupportAllowanceBlockDto>().ToList();
        }

        public IList<DailySupportAllowanceBlockListDto> LoadDailyAllownceBlockingList(int start, int length, string orderBy, string orderDir, List<long> authOrgList, List<long> bIds,
            long deptId, long capId, List<int> pinList, int status, DateTime dateFrom, DateTime dateTo)
        {
            string queryStr = GenerateBlockingListQuery(orderBy, orderDir, authOrgList, bIds, deptId, capId, pinList, status, dateFrom, dateTo);
            queryStr += " ORDER BY dsab.BlockingDate OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            IQuery iQuery = Session.CreateSQLQuery(queryStr);
            iQuery.SetResultTransformer(Transformers.AliasToBean<DailySupportAllowanceBlockListDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<DailySupportAllowanceBlockListDto>().ToList();
        }

        public IList<DailySupportAllowanceBlock> LoadDailyAllownceBlocking(DateTime dateFrom, DateTime dateTo, long teamMemberId)
        {
            ICriteria criteria = Session.CreateCriteria<DailySupportAllowanceBlock>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", DailySupportAllowanceBlock.EntityStatus.Delete)));
            criteria.Add(Restrictions.Not(Restrictions.Eq("IsPost", true)));
            criteria.CreateAlias("TeamMember", "member");
            criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            criteria.Add(Restrictions.Ge("BlockingDate", dateFrom.Date));
            criteria.Add(Restrictions.Le("BlockingDate", dateTo.Date));
            return criteria.List<DailySupportAllowanceBlock>();
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        private static string GenerateBlockingListQuery(string orderBy, string orderDir, IEnumerable<long> orgIds, IEnumerable<long> brIds, long deptId, long capId
                                                , IEnumerable<int> pinList, int status, DateTime dateFrom, DateTime dateTo)
        {
            string dateTimeStart = "'" + dateFrom.ToString("yyyy-MM-dd") + " 00:00:00'";
            string dateTimeEnd = "'" + dateTo.ToString("yyyy-MM-dd") + " 23:59:59'";
            string pinListStr = string.Join(", ", pinList);
            string organizationIdListStr = string.Join(", ", orgIds);
            string branchIdListStr = string.Join(", ", brIds);
            string query = @"Select 
	  tm.Pin AS Pin
	, tm.Name AS Name
	, org.ShortName AS Organization
	, dp.Name AS Department
	, dg.Name AS Designation
	, br.Name AS Branch
	, cap.Name AS Campus
	, FORMAT(dsab.BlockingDate, 'MMM dd, yyy') as BlockingDate
	, CASE WHEN dsab.IsBlocked = 0 THEN 'Unblocked' ELSE 'Blocked' END AS IsBlocked
	, CASE WHEN attds.InTime IS NULL THEN '-' ELSE FORMAT(InTime, 'hh:mm tt') END as TimeFrom 
	, CASE WHEN attds.OutTime IS NULL THEN '-' ELSE FORMAT(OutTime, 'hh:mm tt') END as TimeTo 
	, dsab.Reason
	, IsNULL(aspnetUser.UserName, 'Supar Admin') AS ModifyBy
	, FORMAT(dsab.ModificationDate, 'MMM dd, yyy') as LastModificationDate
From HR_DailySupportAllowanceBlock dsab
LEFT JOIN HR_AttendanceSummary attds ON dsab.TeamMemberId = attds.TeamMemberId AND CAST(attds.AttendanceDate as DATE) = CAST(dsab.BlockingDate as DATE) ";
            query += @" 
LEFT JOIN UserProfile usp ON dsab.ModifyBy = usp.Id
LEFT JOIN AspNetUsers aspnetUser ON usp.AspNetUserId = aspnetUser.Id
LEFT Join HR_TeamMember AS tm ON dsab.TeamMemberId = tm.Id AND tm.Status = 1
CROSS APPLY
(
	Select * from 
	(
		Select 
			emh.CampusId as CampusId
			, emh.DesignationId as DesignationId
			, emh.DepartmentId as DepartmentId
			, emh.TeamMemberId as TeamMemberId
			, emh.EffectiveDate As EffectiveDate
			, emh.EmploymentStatus as EmploymentStatus
			, Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		from HR_EmploymentHistory as emh
		where emh.Status = 1
		AND emh.EffectiveDate <=  dsab.BlockingDate 
		AND emh.TeamMemberId = tm.Id
	) as employmentHistory 
	where 1=1
	AND employmentHistory.r = 1
) as currentPosition
LEFT Join Campus AS cap ON cap.Id = currentPosition.CampusId and cap.Status =  " + Campus.EntityStatus.Active + @" 
LEFT Join Branch AS br ON br.Id = cap.BranchId and br.Status =  " + Branch.EntityStatus.Active + @" 
LEFT Join Organization AS org ON org.Id = br.OrganizationId AND org.Status =  " + Organization.EntityStatus.Active + @" 
LEFT Join HR_Department AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  " + Department.EntityStatus.Active + @" 
LEFT Join HR_Designation AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  " + Designation.EntityStatus.Active + @" 
WHERE dsab.BlockingDate BETWEEN " + dateTimeStart + @" AND " + dateTimeEnd;
            if (status > -1)
            {
                query += @"
AND dsab.IsBlocked = " + status;
            }
            query += @"
AND attds.AttendanceDate BETWEEN " + dateTimeStart + @" AND " + dateTimeEnd;
            if (!string.IsNullOrEmpty(pinListStr))
            {
                query += @"
AND tm.Pin IN(" + pinListStr + ")";
            }
            if (!string.IsNullOrEmpty(organizationIdListStr))
            {
                query += @"
AND org.Id IN(" + organizationIdListStr + ")";
            }

            if (!string.IsNullOrEmpty(branchIdListStr))
            {
                query += @"
AND br.Id IN(" + branchIdListStr + ")";
            }
            if (deptId > 0)
            {
                query += @"
AND dp.Id IN(" + deptId + ")";
            }
            if (capId > 0)
            {
                query += @"
AND cap.Id IN(" + capId + ")";
            }
            query += @" ";
            return query;
        }

        private static string GenerateBlockingListQuery(IEnumerable<long> organizationIdList, IEnumerable<long> branchIdList, DateTime date, IEnumerable<int> pinList)
        {
            string dateTimeStart = "'" + date.ToString("yyyy-MM-dd") + " 00:00:00'";
            string dateTimeEnd = "'" + date.ToString("yyyy-MM-dd") + " 23:59:59'";
            string pinListStr = string.Join(", ", pinList);
            string organizationIdListStr = string.Join(", ", organizationIdList);
            string branchIdListStr = string.Join(", ", branchIdList);

            string query = @"SELECT
	ISNULL(hrab.Id , 0) as Id
	, hras.*
	, hrm.Name
	, hrm.Pin
	, c.Name AS Campus
	, br.Name AS Branch
	, o.ShortName AS Organization
	, dp.Name AS Department
	, dg.Name AS Designation
	, ISNULL(hrab.IsBlocked,0) AS IsBlocked
	, ISNULL(hrab.Reason,'') AS Reason
	, hrab.ModifyBy
	, hrab.ModificationDate as LastModificationDate
FROM
(
	Select
		TeamMemberId AS TeamMemberId
		, AttendanceDate AS BlockingDate
		--, InTime as TimeFrom
		--, OutTime as TimeTo
        , CASE WHEN InTime IS NULL THEN '-' ELSE FORMAT(InTime, 'hh:mm tt') END as TimeFrom 
		, CASE WHEN OutTime IS NULL THEN '-' ELSE FORMAT(OutTime, 'hh:mm tt') END as TimeTo 
	From HR_AttendanceSummary
	Where  AttendanceDate BETWEEN " + dateTimeStart + @" AND " + dateTimeEnd + @"
	AND TeamMemberId 
	IN
	(
		SELECT Id FROM [dbo].[HR_TeamMember] Where 1 = 1";
            if (!string.IsNullOrEmpty(pinListStr))
            {
                query += @" AND Pin IN(" + pinListStr + ")";
            }
            query += @"
	)
	AND Status = " + TeamMember.EntityStatus.Active + @"
) AS hras
Left Join HR_DailySupportAllowanceBlock AS hrab ON hrab.TeamMemberId = hras.TeamMemberId AND hrab.BlockingDate BETWEEN " + dateTimeStart + @" AND " + dateTimeEnd + @"
Left Join HR_TeamMember AS hrm ON hrm.Id = hras.TeamMemberId AND hrm.Status = " + TeamMember.EntityStatus.Active + @"

CROSS APPLY
(
	Select * from 
	(
		Select 
			emh.CampusId as CampusId
			, emh.DesignationId as DesignationId
			, emh.DepartmentId as DepartmentId
			, emh.TeamMemberId as TeamMemberId
			, emh.EffectiveDate As EffectiveDate
			, emh.EmploymentStatus as EmploymentStatus
			, Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		from HR_EmploymentHistory as emh
		where emh.Status = 1
		AND emh.EffectiveDate <=  hras.BlockingDate 
		AND emh.TeamMemberId = hras.TeamMemberId
	) as employmentHistory 
	where 1=1
	AND employmentHistory.r = 1
) as currentPosition
Left Join Campus AS c ON c.Id = currentPosition.CampusId and c.Status =  " + Campus.EntityStatus.Active + @"
Left Join Branch AS br ON br.Id = c.BranchId and br.Status =  " + Branch.EntityStatus.Active + @"
Left Join Organization AS o ON o.Id = br.OrganizationId AND o.Status =  " + Organization.EntityStatus.Active + @"
Left Join HR_Department AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  " + Department.EntityStatus.Active + @"
Left Join HR_Designation AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  " + Designation.EntityStatus.Active + @" 
Where 1=1";
            if (!string.IsNullOrEmpty(organizationIdListStr))
            {
                query += @" AND o.Id IN(" + organizationIdListStr + ")";
            }

            if (!string.IsNullOrEmpty(branchIdListStr))
            {
                query += @" AND br.Id IN(" + branchIdListStr + ")";
            }
            return query;
        }

        public int CountDailyAllownceBlockingList(List<long> authOrgList, List<long> bIds,
            long deptId, long capId, List<int> pinList, int status, DateTime dateFrom, DateTime dateTo)
        {
            string queryStr = "SELECT COUNT(countNum.Pin) FROM ( ";
            queryStr += GenerateBlockingListQuery("", "", authOrgList, bIds, deptId, capId, pinList, status, dateFrom, dateTo);
            queryStr += " ) as countNum ";
            IQuery iQuery = Session.CreateSQLQuery(queryStr);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion
    }
}
