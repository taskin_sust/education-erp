﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.MediaServices;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Api.Controllers
{
    public class MemberInfoController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("MemberInfo");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private List<UserMenu> authorizeMenu;
        private ISession _session;
        private ISession _sessionMedia;

        private readonly IAttendanceDeviceTypeService _attendanceDeviceTypeService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IMemberFingerPrintService _memberFingerPrintService;
        private readonly ITeamMemberImagesMediaService _teamMemberImagesMediaService;

        #endregion

        #region Constructor

        public MemberInfoController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _sessionMedia = NHibernateMediaSessionFactory.OpenSession();
            _attendanceDeviceTypeService = new AttendanceDeviceTypeService(_session);
            _teamMemberService = new TeamMemberService(_session);
            _memberFingerPrintService = new MemberFingerPrintService(_session);
            _teamMemberImagesMediaService = new TeamMemberImagesMediaService(_sessionMedia);
        }

        #endregion

        #region Index

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetMemberInfo(string token, int pin)
        {
            var api = new MemberInfoDeviceApi();
            try
            {
                IList<MemberInfoApi> memberInfoApi = _memberFingerPrintService.GetMemberInfoForDeviceApiByPin(pin);
                if (memberInfoApi == null)
                    return Json(new JsonResponse(false, "No info found!"), JsonRequestBehavior.AllowGet);
                var list = memberInfoApi.ToList().GroupBy(x => x.ModelName).ToList();
                if (list.Count == 0) return Json(new JsonResponse(false, "No device type added!"), JsonRequestBehavior.AllowGet);
                IList<AttendanceDeviceApiModel> deviceApiModels = new List<AttendanceDeviceApiModel>();
                foreach (var grouping in list)
                {
                    var newApiModel = new AttendanceDeviceApiModel();
                    foreach (var infoApi in grouping.ToList())
                    {
                        newApiModel.ModelId = infoApi.ModelId;
                        newApiModel.ModelName = infoApi.ModelName;
                        newApiModel.IsFinger = infoApi.IsFinger;
                        newApiModel.Index0 = infoApi.Index0;
                        newApiModel.Index1 = infoApi.Index1;
                        newApiModel.Index2 = infoApi.Index2;
                        newApiModel.Index3 = infoApi.Index3;
                        newApiModel.Index4 = infoApi.Index4;
                        newApiModel.Index5 = infoApi.Index5;
                        newApiModel.Index6 = infoApi.Index6;
                        newApiModel.Index7 = infoApi.Index7;
                        newApiModel.Index8 = infoApi.Index8;
                        newApiModel.Index9 = infoApi.Index9;
                    }
                    deviceApiModels.Add(newApiModel);
                }
                api.Branch = memberInfoApi[0].Branch;
                api.Campus = memberInfoApi[0].Campus;
                api.Organization = memberInfoApi[0].Organization;
                api.Name = memberInfoApi[0].Name;
                api.Pin = memberInfoApi[0].Pin;
                api.Department = memberInfoApi[0].Department;
                api.Designation = memberInfoApi[0].Designation;
                api.Id = memberInfoApi[0].TeamMemberId;
                var memberImage = _teamMemberImagesMediaService.GetTeamMemberImageMediaByRefId(api.Id);
                if (memberImage != null)
                    api.Image = memberImage.Images;
                api.AttendanceDeviceApiModel = deviceApiModels;
            }
            catch (InvalidDataException ide)
            {
                return Json(new JsonResponse(false, "Invalid Pin!"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new JsonResponse(false, "Database access problem!"), JsonRequestBehavior.AllowGet);
            }
            return Json(new JsonResponse(true, api), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Operational Function

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SetFingerPrint(string token, string memberInfo)
        {
            string message = "";
            bool success = false;
            try
            {
                //No use now 
                token = "jUMkVPY1HX";
                if (Authenticate(token) > 0)
                    message = "Request not authenticated";
                if (!String.IsNullOrEmpty(message))
                    return Json(new JsonResponse(success, message));
                var memberInfoApiObj = new JavaScriptSerializer().Deserialize<MemberInfoDeviceApi>(memberInfo);
                _memberFingerPrintService.SaveOrUpdate(memberInfoApiObj);
                success = true;
            }
            catch (DuplicateEntryException e)
            {
                return Json(new JsonResponse(success, e.Message));
            }
            catch (InvalidDataException ide)
            {
                return Json(new JsonResponse(success, ide.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new JsonResponse(success, "Can't process at this time.please try again later"));
            }
            return Json(new JsonResponse(true, "success"));
        }

        //[HttpGet]
        //[AllowAnonymous]
        //public ContentResult Demo()
        //{
        //    //var memberInfoApiObj = new JavaScriptSerializer().Deserialize<MemberInfoDeviceApi>(memberInfo);
        //    var memberInfoApiObj = new MemberInfoDeviceApi();
        //    memberInfoApiObj.Branch = "branchdata";
        //    _memberFingerPrintService.SaveOrUpdate(memberInfoApiObj);
        //    return Content("");
        //}

        #region Save

        #endregion

        #region Edit

        #endregion

        #region Delete

        #endregion

        #region Details

        #endregion

        #endregion

        #region Helper

        private int Authenticate(string key)
        {
            int error = 0;
            var apiKey = ConfigurationManager.AppSettings["ApiKey"];

            if (key != apiKey)
            {
                error = 1;
            }
            return error;
        }

        #endregion
    }
}