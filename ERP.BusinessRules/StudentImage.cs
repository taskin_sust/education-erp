﻿using System;
using System.Collections.Generic;

namespace UdvashERP.BusinessRules
{
    public static class StudentImageSize
    {
        public static double MinimumSizeKB = 0;
        public static double MaximumSizeKB = 100;
         
        private static double _showSize;
        private const double Kb = 1024;

        public static double CalculateSize(double size)
        {
            try
            {
                if (size < Kb)
                {
                    _showSize = size;
                }
                else if (size.Equals(Kb))
                {
                    _showSize = size / Kb;
                }
                else
                {
                    _showSize = size / Kb;
                }

            }
            catch (Exception)
            {
                return 0;
            }
            return _showSize;
        }
    }
}