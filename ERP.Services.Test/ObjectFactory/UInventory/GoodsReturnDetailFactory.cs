using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class GoodsReturnDetailFactory : ObjectFactoryBase<GoodsReturnDetail>
   {

       private readonly IGoodsReturnDetailService _goodsReturnDetailService;

       public GoodsReturnDetailFactory(IObjectFactoryBase caller, ISession session): base(caller, session){

       _goodsReturnDetailService = new GoodsReturnDetailService(session);

       }

       #region Create 

       public GoodsReturnDetailFactory Create(){

       Object = null;
       SingleObjectList.Add(Object);
       return this;
       }

       public GoodsReturnDetailFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public GoodsReturnDetailFactory Persist()       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _goodsReturnDetailService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x =>{if (x.Id == 0)_goodsReturnDetailService.SaveOrUpdate(x);});

          }
          return this;
       }

       public GoodsReturnDetailFactory CreateMore(int num){

       for (int i = 0; i < num; i++){
       var obj = new GoodsReturnDetail(){};
       ObjectList.Add(obj);}
       return this;}

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public GoodsReturnDetailFactory CleanUp()
       {

          DeleteAll();
          return this;
       }

       #endregion

    }
}
