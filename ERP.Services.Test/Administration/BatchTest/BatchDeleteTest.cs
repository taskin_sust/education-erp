﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BatchTest
{

    [Trait("Area", "Administration")]
    [Trait("Service", "Batch")]
    public class BatchDeleteTest : BatchBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            var success = _batchService.Delete(batchFactory.Object.Id, batchFactory.Object);
            Batch batch = _batchService.GetBatch(batchFactory.Object.Id);
            Assert.True(success);
            Assert.Null(batch);
        }

        [Fact]
        public void Batch_Delete_Should_Not_Delete_Other_Objects()
        {
            var batchFactory = _batchFactory.Create()
                   .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            Batch batch = _batchFactory.Object;
            var program = batch.Program;
            var session = batch.Session;
            var branch = batch.Branch;
            var campus = batch.Campus;
            var success = _batchService.Delete(batchFactory.Object.Id, batchFactory.Object);
            batch = _batchService.GetBatch(batchFactory.Object.Id);
            Assert.True(success);
            Assert.Null(batch);
            program = _programService.GetProgram(program.Id);
            session = _sessionService.LoadById(session.Id);
            branch = _branchService.GetBranch(branch.Id);
            campus = _campusService.GetCampus(campus.Id);
            Assert.NotNull(program);
            Assert.NotNull(session);
            Assert.NotNull(branch);
            Assert.NotNull(campus);
        }

        #endregion

    }
}
