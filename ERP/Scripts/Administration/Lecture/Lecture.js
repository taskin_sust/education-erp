﻿$(document).ready(function() {
    //validateDropDownField("Organization", "Select a Organization.");
    //validateDropDownField("Program", "Select a Program.");
    //validateDropDownField("Session", "Select a Session.");
    //validateDropDownField("Course", "Select a Course.");

});

$(document).on('change', '#Organization', function (e) {
    $("#Program option").remove();
    $('#Program').append("<option value=''>Select Program</option>");
    $("#Session option").remove();
    $('#Session').append("<option value=''>Select Session</option>");
    $("#Course option").remove();
    $('#Course').append("<option value=''>Select Course</option>");
    $(".subList").empty();
    var orgId = $("#Organization").val();
    if (orgId != "") {
        $("#Organization").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Organization").removeClass("highlight");
        $.ajax({
            type: "POST",
            //url: $("body").attr("data-project-root") + "Administration/Lecture/GetProgramByOrg",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
            cache: false,
            async: true,
            data: { organizationIds: orgId,isAuthorized:true },
            beforeSend: function() {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                //$.unblockUI();
                //if (result.IsSuccess) {
                //    $.each(result.returnList, function(i, v) {
                //        $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                //    });
                //} else {
                //    $.fn.customMessage({
                //        displayMessage: result.Message,
                //        displayMessageType: "error",
                //    });
                //}
                $.unblockUI();
                if (response.IsSuccess) {
                    $.each(response.returnProgramList, function (i, v) {
                        $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "error",
                    });
                }

            },
            complete: function() {
                $.unblockUI();
            },
            error: function(result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
    else {
        $("#Organization").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("<span>The Organization field is required.</span>");
        $("#Organization").addClass("highlight");
    }

});

$(document).on('change', '#Program', function (e) {
    $("#Session option").remove();
    $('#Session').append("<option value=''>Select Session</option>");
    $("#Course option").remove();
    $('#Course').append("<option value=''>Select Course</option>");
    $(".subList").empty();

    var organizationIds = $("#Organization").val();
    var prId = $("#Program").val();
   

    if (prId != "" && organizationIds!="") {
        $("#Program").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Program").removeClass("highlight");
        $.ajax({
            type: "POST",
            //url: $("body").attr("data-project-root") + "Administration/Lecture/GetSessionByProgram",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
            cache: false,
            async: true,
            data: { organizationIds: organizationIds, programIds: prId, isAuthorized:true },
            beforeSend: function() {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                //$.unblockUI();
                //if (result.IsSuccess) {
                //    $.each(result.returnList, function(i, v) {
                //        $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                //    });
                //} else {
                //    $.fn.customMessage({
                //        displayMessage: result.Message,
                //        displayMessageType: "error",
                //    });
                //}
                if (response.IsSuccess) {
                    $.each(response.returnSessionList, function (i, v) {
                        $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "error",
                    });
                }

            },
            complete: function() {
                $.unblockUI();
            },
            error: function (response) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
    else {
        $("#Program").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("<span>The Program field is required.</span>");
        $("#Program").addClass("highlight");
    }
   

});

$(document).on('change', '#Session', function (e) { 
    $("#Course option").remove();
    $('#Course').append("<option value=''>Select a Course</option>");
    $(".subList").empty();
    var organizationIds = $("#Organization").val();
    var sId = $("#Session").val();
    var prId = $("#Program").val();
    if (sId=="") {
        $("#Session").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("<span>The Session field is required.</span>");
        $("#Session").addClass("highlight");
    }
    if (sId != "" && prId != "") {
        $("#Session").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Session").removeClass("highlight");
        $.ajax({
            type: "POST",
            //url: $("body").attr("data-project-root") + "Administration/Lecture/GetCourseBySession",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
            cache: false,
            async: true,
            data: { organizationIds: organizationIds, programIds: prId, sessionIds: sId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                $.unblockUI();
                if (response.IsSuccess) {
                    $.each(response.returnCourse, function (i, v) {
                        $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "error",
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }

});

$(document).on('change', '#Course', function(e) {
    var sId = $("#Session").val();
    var prId = $("#Program").val();
    var cId = $("#Course").val();
    $(".subList").empty();
    if (cId=="") {
        $("#Course").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("<span>The Course field is required.</span>");
        $("#Course").addClass("highlight");
    }
    if (sId != "" && prId != "" && cId != "") {
        $("#Course").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Course").removeClass("highlight");
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/Lecture/GetSubjectByCourseSessionProgram", 
            cache: false,
            async: true,
            data: { "programId": prId, "sessionId": sId, "courseId":cId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (!(result.hasOwnProperty("IsSuccess"))) {
                    $(".subList").html(result);
                }
                else {
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "error",
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});

$(document).on('click', '.subjectChk', function (event) {
    var parentTr = $(this).parent().parent();
    if ($(this).is(':checked')) {       
        parentTr.find("input[type=text]").removeAttr('disabled');
    }
    else {
        parentTr.find("input[type=text]").attr('disabled', 'disabled');
        parentTr.find("input[type=text]").removeClass("highlight");
    }       
});

$(document).on('input propertychange', '.noOfClasses', function (event) {
    var noOfClasses = $(this).val();
    if (noOfClasses != "" && noOfClasses != "0" && /^\d+$/.test(noOfClasses)) {
        $(this).removeClass("highlight");
    }
    else {
        $(this).addClass("highlight");
    }
    console.log(noOfClasses);
});

$(document).on('input propertychange', '.classNamePrefix', function (event) {
    var classNamePrefix = $(this).val();
    if (classNamePrefix != "") {
        $(this).removeClass("highlight");
    }
    else {
        $(this).addClass("highlight");
    }
    console.log(classNamePrefix);
});


 
$(document).on('click', '#lecturePlanAdd', function (event) {
    var oId = $("#Organization").val();
    var sId = $("#Session").val();
    var prId = $("#Program").val();
    var cId = $("#Course").val();
    var isSuccess = 0;
    if (oId == "") {
        $("#Organization").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("<span>The Organization field is required.</span>");
        $("#Organization").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Organization").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Organization").removeClass("highlight");
    }
    if (prId == "") {
        $("#Program").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("<span>The Program field is required.</span>");
        $("#Program").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Program").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Program").removeClass("highlight");
    }
    if (sId == "") {
        $("#Session").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("<span>The Session field is required.</span>");
        $("#Session").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Session").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Session").removeClass("highlight");
    }
    if (cId == "") {
        $("#Course").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("<span>The Course field is required.</span>");
        $("#Course").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Course").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Course").removeClass("highlight");
    }
    var lectureSettingsViewModel = [];
   // var totalChecked = $(this).parents(".panel-default").find(".course-subject-check:checked").length;
    $(".subjectChk:checked").each(function () {
        var parentTr = $(this).parent().parent();
        var noOfClasses = parentTr.find(".noOfClasses").val();
        var classNamePrefix = parentTr.find(".classNamePrefix").val();
        var courseSubjectId = $(this).attr("data-course-subject-id");
        var validCourseSubject = 0;
    
        if (noOfClasses != "" && noOfClasses!="0" && /^\d+$/.test(noOfClasses)) {
            parentTr.find(".noOfClasses").removeClass("highlight");
        }
        else {          
            parentTr.find(".noOfClasses").addClass("highlight");
            validCourseSubject = validCourseSubject + 1;
        }

        if (classNamePrefix != "") {
            parentTr.find(".classNamePrefix").removeClass("highlight");
        }
        else {           
            parentTr.find(".classNamePrefix").addClass("highlight");
            validCourseSubject = validCourseSubject + 1;
        }

        if (validCourseSubject==0) {
            lectureSetting = {};
            lectureSetting["CourseSubjectId"] = courseSubjectId;
            lectureSetting["CourseId"] =$("#Course").val();
            lectureSetting["NumberOfClasses"] = noOfClasses;
            lectureSetting["ClassNamePrefix"] = classNamePrefix;
            lectureSettingsViewModel.push(lectureSetting);
        }
    });

    if (lectureSettingsViewModel.length < 1) {
        $.fn.customMessage({
            displayMessage: "Please give at least one lecture plan",
            displayMessageType: "error",
        });
        isSuccess = isSuccess + 1;
    }
    else {
        $(".customMessage").empty();
    }

    if (lectureSettingsViewModel.length != $(".subjectChk:checked").length) {
        $.fn.customMessage({
            displayMessage: "Please give required data",
            displayMessageType: "error",
        });
        isSuccess = isSuccess + 1;
    }
    else {
        $(".customMessage").empty();
    }

    if (isSuccess == 0) {
        console.log(lectureSettingsViewModel);
        
        var stringify = JSON.stringify(lectureSettingsViewModel);
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/Lecture/SaveLecturePlan",
            cache: false,
            async: true,
            dataType: "json",
            contentType: "application/json",
            data: stringify,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    //$("#Organization option").remove();
                    //$('#Organization').append("<option value=''>Select a Organization</option>");
                    //$("#Program option").remove();
                    //$('#Program').append("<option value=''>Select a Program</option>");
                    //$("#Session option").remove();
                    //$('#Session').append("<option value=''>Select a Session</option>");
                    //$("#Course option").remove();
                    //$('#Course').append("<option value=''>Select a Course</option>");
                    //$(".subList").html("");

                    $(".subjectChk:checked").each(function() {
                        $(this).prop("checked", false);
                        var parentTr = $(this).parent().parent();
                        parentTr.find("input[type=text]").val("").attr('disabled', 'disabled');
                        parentTr.find("input[type=text]").removeClass("highlight");
                    });

                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "s",
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "error",
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }

});