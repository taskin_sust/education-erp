﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.AdminisTration.ProgramTest;
using Xunit;

namespace UdvashERP.Services.Test.Administration.ProgramTest
{
    interface IProgramUpdateRankChangeTest
    {
        #region Basic Test
        void UpdateRank_Should_Be_Fail_Without_Program();
        void UpdateRank_Should_Be_Success_For_Up_Rank();
        void UpdateRank_Should_Be_Success_For_Up_Down();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Program")]
    public class ProgramUpdateRankChangeTest : ProgramBaseTest, IProgramUpdateRankChangeTest
    {
        [Fact]
        public void UpdateRank_Should_Be_Fail_Without_Program()
        {
            string action = "up";
            Assert.Throws<NullObjectException>(() => _programService.UpdateRank(null, action));
        }

        [Fact]
        public void UpdateRank_Should_Be_Success_For_Up_Rank()
        {
            string action = "up";
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            Assert.True(_programService.UpdateRank(program, action));
        }

        [Fact]
        public void UpdateRank_Should_Be_Success_For_Up_Down()
        {
            string action = "down";
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
           // Program program = _programService.GetProgram(programFactory.Object.Id);
            Assert.True(_programService.UpdateRank(programFactory.Object, action));
        }
    }
}
