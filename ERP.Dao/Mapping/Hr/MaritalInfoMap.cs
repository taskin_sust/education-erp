using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class MaritalInfoMap : BaseClassMap<MaritalInfo,long> {
        
        public MaritalInfoMap() {
			Table("HR_MaritalInfo");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Name).Column("Name");
			Map(x => x.MaritalStatus).Column("MaritalStatus");
			Map(x => x.MarriageDate).Column("MarriageDate");
			Map(x => x.SpouseName).Column("SpouseName");
			Map(x => x.SpouseContatctNo).Column("SpouseContatctNo");
        }
    }
}
