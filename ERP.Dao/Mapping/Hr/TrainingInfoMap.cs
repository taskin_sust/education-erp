using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class TrainingInfoMap : BaseClassMap<TrainingInfo,long> {
        
        public TrainingInfoMap() {
			Table("HR_TrainingInfo");
			LazyLoad();
			References(x => x.Institute).Column("InstituteId");
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.TrainingTitle).Column("TrainingTitle");
			Map(x => x.TopicCovere).Column("TopicCovere");
			Map(x => x.Location).Column("Location");
			Map(x => x.Duration).Column("Duration");
			Map(x => x.Year).Column("Year");
			Map(x => x.Certification).Column("Certification");
			Map(x => x.IsTraining).Column("IsTraining");
			HasMany(x => x.Certificate).KeyColumn("TrainingInfoId");
        }
    }
}
