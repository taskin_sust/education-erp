﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.MediaDao
{
    public interface ITeamMemberCertificateImageMediaDao : IBaseDao<TeamMemberCertificateMediaImage, long>
    {
        TeamMemberCertificateMediaImage GetTeamMemberCertificateImageMediaByRefId(long academicInfoId, long teamMemberId);
        void DeleteTeamMemberMediaImage(long academicInfoId, long teamMemberId);
    }
    public class TeamMemberCertificateImageMediaDao : BaseDao<TeamMemberCertificateMediaImage, long>, ITeamMemberCertificateImageMediaDao
    {
        public TeamMemberCertificateMediaImage GetTeamMemberCertificateImageMediaByRefId(long academicInfoId, long teamMemberId)
        {
            return
                Session.QueryOver<TeamMemberCertificateMediaImage>()
                    .Where(x => x.AcademicInfoId == academicInfoId)
                    .Where(x => x.TeamMemberId == teamMemberId)
                    .Take(1)
                    .SingleOrDefault<TeamMemberCertificateMediaImage>();
        }
        public void DeleteTeamMemberMediaImage(long academicInfoId, long teamMemberId)
        {
            var certificateMediaImage = Session.QueryOver<TeamMemberCertificateMediaImage>()
                                            .Where(x => x.AcademicInfoId == academicInfoId)
                                            .Where(x => x.TeamMemberId == teamMemberId)
                                            .SingleOrDefault<TeamMemberCertificateMediaImage>();
            Session.Delete(certificateMediaImage);
        }
    }
}
