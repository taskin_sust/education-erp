﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.AdminisTration.ProgramTest;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ActionTest
{
    interface IActionLoadActiveTest
    {
        #region Basic Test
       
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Action")]
    public class ActionLoadActiveTest : ActionBaseTest, IActionLoadActiveTest
    {
        #region Basic Test
        
        [Fact]
        public void LoadActive_should_return_list()
        {
            _actionsFactory.CreateMore(100).Persist();
            var controller = _actionsFactory.GetLastCreatedObjectList()[0].AreaControllers;
            var actionList = _actionsService.LoadActive(0, 100, "", "", "", controller.Id.ToString(), "1", "");
            Assert.Equal(100, actionList.Count);
        }

        #endregion
    }
}
