﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Exam;

namespace UdvashERP.Dao.Mapping.Exam
{
    public partial class ExamsStudentMarksMap:BaseClassMap<ExamsStudentMarks,long>
    {
        public ExamsStudentMarksMap()
        {
            Table("ExamsStudentMarks");
            LazyLoad();

            Map(x => x.SetCode).Column("SetCode");
            Map(x => x.ObtainMark).Column("ObtainMark");
            Map(x => x.McqAnswer).Column("McqAnswer");
            Map(x => x.IsNonPayable).Column("IsNonPayable");
            Map(x => x.TotalCorrectAnswer).Column("TotalCorrectAnswer");
            Map(x => x.TotalWrongAnswer).Column("TotalWrongAnswer");
            Map(x => x.TotalAnswered).Column("TotalAnswered");
            Map(x => x.TotalSkip).Column("TotalSkip");

            References(x => x.StudentProgram).Column("StudentProgramId");
            References(x => x.ExamsDetails).Column("ExamsDetailsId");
        }
    }
}
