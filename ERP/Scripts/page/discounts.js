$(document).ready(function () {
    var discount = 0;
    var program = $("#ProgramId").val();
    var session = $("#SessionId").val();
    if ($("#Id").val() != "" && $("#Id").val() != null)
        discount = $("#Id").val();

    if (program != "" && session != "") {
        var data = { "ProgramId": program, "SessionId": session, "DiscountId": discount };
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Discount/GetCoursesList",
            cache: false,
            async: true,
            data: data,
            success: function (result) {
                $("#courseListData").html(result);
            },
            error: function (result) {
                bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
            }
        });
    }
    else {

    }

    /* Dispaly Programs Session Wise*/
    $("#ProgramId").change(function () {
        var discount = 0;
        var program = $("#ProgramId").val();
        var session = $("#SessionId").val();
        if ($("#Id").val() != "" && $("#Id").val() != null)
            discount = $("#Id").val();
        
        if (program != "" && session != "") {
            var data = { "ProgramId": program, "SessionId": session, "DiscountId": discount };
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Discount/GetCoursesList",
                cache: false,
                async: true,
               // contenttype: "application/json; charset=utf-8",
                data: data,
                success: function (result) {
                    $("#courseListData").html(result);
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
        else {

        }
    });
    $("#SessionId").change(function () {
        var discount = 0;
        var program = $("#ProgramId").val();
        var session = $("#SessionId").val();

        if ($("#Id").val() != "" && $("#Id").val()!=null)
            discount = $("#Id").val();
       
        if (program != "" && session != "") {
            var data = { "ProgramId": program, "SessionId": session, "DiscountId": discount };
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Discount/GetCoursesList",
                // contenttype: "application/json; charset=utf-8",
                cache: false,
                async: true,
                data: data,
                success: function (result) {
                    $("#courseListData").html(result);
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
        else {

        }
    });

    /* Select/Deselect All Programs*/
    $("#allPrograms").click(function () {
        if ($(this).is(":checked")) {
            var c = this.checked;
            $('.programCheckbox').prop('checked', c);
        }
        else {
            var c = this.checked;
            $('.programCheckbox').prop('checked', c);
        }
    });

});



