﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using log4net;
using Microsoft.AspNet.Identity;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.Areas.Student.Models;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using Constants = UdvashERP.BusinessRules.Constants;
using InstituteService = UdvashERP.Services.Administration.InstituteService;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class StudentVisitedController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("StudentArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IStudentVisitedService _studentVisitedService;
        private readonly IOrganizationService _organizationService;
        private readonly IBranchService _branchService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IInstitutionService _institutionService;
        //authorization
        private List<UserMenu> _userMenu;

        public StudentVisitedController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _studentVisitedService = new StudentVisitedService(session);
                _organizationService = new OrganizationService(session);
                _branchService = new BranchService(session);
                _userService = new UserService(session);
                _commonHelper = new CommonHelper();
                _institutionService = new InstituteService(session);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        #endregion

        #region Operational Function

        public ActionResult Index()
        {
            return RedirectToAction("VisitedStudentList");
        }

        [HttpGet]
        public ActionResult Create(string message = "", string StudentVisitType = "")
        {
            if (message != "")
                ViewBag.SuccessMessage = message;

            var userMenu = (List<UserMenu>)ViewBag.UserMenu;

            ViewBag.VersionOfStudy = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int> { (int)VersionOfStudy.Combined }), "Key", "Value");
            ViewBag.Gender = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined }), "Key", "Value");
            ViewBag.Religion = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value");
            ViewBag.StudentVisitType = StudentVisitType;
            ViewBag.StudentVisitTypes = new SelectList(_commonHelper.LoadEmumToDictionary<StudentVisitType>(), "Key", "Value");
            ViewBag.InterestedBranch = new SelectList(_branchService.LoadAuthorizedBranch(userMenu), "Id", "Name", SelectionType.SelelectAll.ToString());
            return View();
        }

        [HttpPost]
        public ActionResult Create(StudentVisited studentVisited, string InterestedBranch = null)
        {
            try
            {
                studentVisited.IsAdmitted = false;

                if (studentVisited.IsProfessionDoctor == true)
                {
                    studentVisited.Profession += studentVisited.Profession == null ? "Doctor" : ", Doctor";
                }
                if (studentVisited.IsProfessionEngineer == true)
                {
                    studentVisited.Profession += studentVisited.Profession == null ? "Engineer" : ", Engineer";
                }
                if (studentVisited.IsProfessionOthers == true)
                {
                    studentVisited.Profession += studentVisited.Profession == null ? "Others" : ", Others";
                }

                String UserId = User.Identity.GetUserId();
                AspNetUser anu = _userService.LoadbyAspNetUserId(Convert.ToInt64(UserId));
                UserProfile up = _userService.LoadByAspNetUser(anu);
                studentVisited.VisitedBranch = up.Branch;
                
                if (InterestedBranch != null && InterestedBranch != "")
                {
                    studentVisited.InterestedBranch = _branchService.GetBranch(Convert.ToInt64(InterestedBranch));
                    studentVisited.InterestedOrganization = studentVisited.InterestedBranch.Organization;
                }

                bool isSuccess = _studentVisitedService.SaveOrUpdate(studentVisited);
                if (isSuccess)
                {
                    return RedirectToAction("Create", new { message = "Information successfully saved.", StudentVisitType = studentVisited.StudentVisitType.ToString() });
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }

            ViewBag.VersionOfStudy = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int> { (int)VersionOfStudy.Combined }), "Key", "Value");
            ViewBag.Gender = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined }), "Key", "Value");
            ViewBag.Religion = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value");
            ViewBag.StudentVisitType = new SelectList(_commonHelper.LoadEmumToDictionary<StudentVisitType>(), "Key", "Value");
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.InterestedBranch = new SelectList(_branchService.LoadAuthorizedBranch(userMenu), "Id", "Name", SelectionType.SelelectAll.ToString());
            return View();
        }

        [HttpGet]
        public ActionResult Edit(long Id, string message = "")
        {
            if (message != "")
                ViewBag.SuccessMessage = message;

            var sv = _studentVisitedService.LoadById(Id);
            if (sv == null)
            {
                ViewBag.ErrorMessage = "Invalid Student Visit";
            }
            else
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var eiinNumber = "";
                var institute = _institutionService.GetInstituteByName(sv.Institute);
                if (institute!=null)
                {
                    eiinNumber = institute.Eiin;
                }
                ViewBag.Eiin = eiinNumber;
                ViewBag.VersionOfStudy = sv.VersionOfStudy.ToString();
                ViewBag.VersionOfStudys = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int> { (int)VersionOfStudy.Combined }), "Key", "Value");
                ViewBag.Gender = sv.Gender;
                ViewBag.Genders = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined }), "Key", "Value");
                ViewBag.Religion = sv.Religion.ToString();
                ViewBag.Religions = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value");
                ViewBag.StudentVisitType = sv.StudentVisitType;
                ViewBag.StudentVisitTypes = new SelectList(_commonHelper.LoadEmumToDictionary<StudentVisitType>(), "Key", "Value");
                ViewBag.InterestedBranch = sv.InterestedBranch == null ? 0 : sv.InterestedBranch.Id;
                ViewBag.InterestedBranchs = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, null, null, null), "Id", "Name", SelectionType.SelelectAll.ToString());
            }
            return View(sv);
        }

        [HttpPost]
        public ActionResult Edit(StudentVisited studentVisited, string InterestedBranch = null)
        {
            var sv = _studentVisitedService.LoadById(studentVisited.Id);
            try
            {
                if (studentVisited.IsProfessionDoctor == true)
                {
                    studentVisited.Profession += studentVisited.Profession == null ? "Doctor" : ", Doctor";
                }
                if (studentVisited.IsProfessionEngineer == true)
                {
                    studentVisited.Profession += studentVisited.Profession == null ? "Engineer" : ", Engineer";
                }
                if (studentVisited.IsProfessionOthers == true)
                {
                    studentVisited.Profession += studentVisited.Profession == null ? "Others" : ", Others";
                }

                if (InterestedBranch != null && InterestedBranch != "")
                {
                    studentVisited.InterestedBranch = _branchService.GetBranch(Convert.ToInt64(InterestedBranch));
                    studentVisited.InterestedOrganization = studentVisited.InterestedBranch.Organization;
                }

                sv.NickName = studentVisited.NickName;
                sv.Mobile = studentVisited.Mobile;
                sv.VersionOfStudy = studentVisited.VersionOfStudy;
                sv.Gender = studentVisited.Gender;
                sv.Religion = studentVisited.Religion;
                sv.Institute = studentVisited.Institute;
                sv.Class = studentVisited.Class;
                sv.Year = studentVisited.Year;
                sv.Exam = studentVisited.Exam;
                sv.Gpa = studentVisited.Gpa;
                sv.MeritPosition = studentVisited.MeritPosition;
                sv.Section = studentVisited.Section;
                sv.MobileFather = studentVisited.MobileFather;
                sv.MobileMother = studentVisited.MobileMother;
                sv.StudentVisitType = studentVisited.StudentVisitType;
                sv.Profession = studentVisited.Profession;
                sv.InterestedBranch = studentVisited.InterestedBranch;
                sv.InterestedOrganization = studentVisited.InterestedOrganization;

                bool isSuccess = _studentVisitedService.SaveOrUpdate(sv);
                if (isSuccess)
                {
                    return RedirectToAction("Edit", new { Id = studentVisited.Id, message = "Information successfully updated." });
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }

            if (sv == null)
            {
                ViewBag.ErrorMessage = "Invalid Student Visit";
            }
            else
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.VersionOfStudy = sv.VersionOfStudy.ToString();
                ViewBag.VersionOfStudys = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int> { (int)VersionOfStudy.Combined }), "Key", "Value");
                ViewBag.Gender = sv.Gender;
                ViewBag.Genders = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined }), "Key", "Value");
                ViewBag.Religion = sv.Religion.ToString();
                ViewBag.Religions = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value");
                ViewBag.StudentVisitType = sv.StudentVisitType;
                ViewBag.StudentVisitTypes = new SelectList(_commonHelper.LoadEmumToDictionary<StudentVisitType>(), "Key", "Value");
                ViewBag.InterestedBranch = sv.InterestedBranch == null ? 0 : sv.InterestedBranch.Id;
                ViewBag.InterestedBranchs = new SelectList(_branchService.LoadAuthorizedBranch(userMenu), "Id", "Name", SelectionType.SelelectAll.ToString());
            }
            return View(sv);
        }

        #endregion

        #region Others Function

        #endregion

        #region Ajax Request Function

        [HttpPost]
        public JsonResult LoadInstitute(string query, string isEiin = "")
        {
            try
            {
                if (String.IsNullOrEmpty(isEiin))
                {
                    SelectList instituteSelectList = new SelectList(_studentVisitedService.LoadInstituteByQuery(query),
                        "Id", "Name");
                    List<SelectListItem> instituteList = instituteSelectList.ToList();
                    return Json(new {returnList = instituteList, IsSuccess = true});
                }
                else
                {
                    if (isEiin == "Id")
                    {
                        var institute = _institutionService.LoadById(Convert.ToInt64(query));
                        if (institute != null)
                        {
                            return Json(new Response(true, institute.Eiin));
                        }
                    }
                    else
                    {
                        var institute = _institutionService.GetInstitute(query);
                        if (institute != null)
                        {
                            return Json(new Response(true, institute.Name));
                        }
                    }
                    
                    return Json(new Response(false, "Institute Not Found!!"));
                }
               
            }
            catch (Exception e)
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public JsonResult LoadYear(string Class)
        {
            try
            {
                var yearList = _studentVisitedService.LoadYear(Class);
                return Json(new { returnList = yearList, IsSuccess = true });
            }
            catch (Exception e)
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public JsonResult LoadUserList(string Class = null, string Year = null, string StudentVisitType = null)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                List<long> programIdList = AuthHelper.LoadProgramIdList(_userMenu);
                
                var allUsers = _userService.LoadActiveAuthorizedUserByBranch(programIdList, branchIdList);
                allUsers = allUsers.OrderBy(x => x.AspNetUser.Email).ToList();
                if (StudentVisitType == "") { StudentVisitType = null; }
                var UniqueUserList = _studentVisitedService.LoadUniqueUser(Class, Year, StudentVisitType);
                allUsers = allUsers.Where(x => UniqueUserList.Contains(x.AspNetUser.Id)).ToList();

                var msUser = new MultiSelectList(allUsers, "AspNetUser.Id", "AspNetUser.Email");
                return Json(new { returnList = msUser, IsSuccess = true });
            }
            catch (Exception e)
            {
                return Json(new Response(false, WebHelper.SetExceptionMessage(e)));
            }
        }

        [HttpPost]
        public JsonResult GetStudentVisitedCount(string Class, string Year, string StudentVisitType = "", string UserId = "", string InterestedOrganizationId = "", long[] InterestedBranchIds = null)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (InterestedBranchIds == null || (InterestedBranchIds.Length > 0 && InterestedBranchIds[0] == 0))
                {
                    InterestedBranchIds = new long[] { };
                }
                int studentVisitedCount = _studentVisitedService.GetStudentVisitedCount(Class, Year, "", "", "", StudentVisitType, UserId, InterestedOrganizationId, InterestedBranchIds);

                return Json(new { studentVisitedCount = studentVisitedCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _logger.Error("StudentVisited. Count Problem." + ex.InnerException);
                else
                    _logger.Error("StudentVisited. Count Problem." + ex.Message);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        
        [HttpPost]
        public JsonResult GetStudentAdmissionVisitedCount(string Class, string Year, long ProgramId, long SessionId, int StudentType, string StudentVisitType = "", long[] UserIds = null, long[] BranchIds = null)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                if (UserIds == null || (UserIds.Length > 0 && UserIds[0] == 0))
                {
                    UserIds = new long[] { };
                }
                if (BranchIds == null || (BranchIds.Length > 0 && BranchIds[0] == 0))
                {
                    BranchIds = new long[] { };
                }
                int studentVisitedCount = _studentVisitedService.GetStudentAdmissionVisitedCount(Class, Year, ProgramId, SessionId, "", "", "", StudentVisitType, StudentType, UserIds, BranchIds);
                return Json(new { studentVisitedCount = studentVisitedCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _logger.Error("StudentVisited. Count Problem." + ex.InnerException);
                else
                    _logger.Error("StudentVisited. Count Problem." + ex.Message);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Reports

        #region Main Report Actions

        [HttpGet]
        public ActionResult VisitedStudentList()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.Year = new SelectList(new List<SelectListItem>() { }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
            ViewBag.StudentVisitType = new SelectList(_commonHelper.LoadEmumToDictionary<StudentVisitType>(), "Key", "Value");
            ViewBag.InterestedBranch = new SelectList(_branchService.LoadAuthorizedBranch(userMenu), "Id", "Name", SelectionType.SelelectAll.ToString());
            ViewBag.UserList = new SelectList(new List<SelectListItem>() { }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
            return View();
        }

        [HttpPost]
        public ActionResult VisitedStudentList(VisitedStudentSearch visitedStudentSearch, string InterestedOrganizationId = "", long[] InterestedBranchId = null)
        {
            try
            {
                if (visitedStudentSearch != null)
                {
                    if (visitedStudentSearch.InformationViewList == null)
                    {
                        var infoView = new List<string>(new[] { "Visited User", "User Branch", "Nick Name", "Mobile Number (Student)" });
                        visitedStudentSearch.InformationViewList = infoView.ToArray();
                    }
                    else
                    {
                        var infoView = new List<string>(new[] { "Visited User", "User Branch" });
                        infoView.AddRange(visitedStudentSearch.InformationViewList);
                        visitedStudentSearch.InformationViewList = infoView.ToArray();
                    }
                    ViewBag.PageSize = Constants.PageSize;
                    ViewBag.Year = visitedStudentSearch.Year;
                    ViewBag.Class = visitedStudentSearch.Class;
                    ViewBag.StudentVisitType = visitedStudentSearch.StudentVisitType;
                    ViewBag.UserId = visitedStudentSearch.UserList;
                    ViewBag.InterestedBranchIds = InterestedBranchId;
                }
                else
                {
                    ViewBag.ErrorMessage = "Information Not Found .";
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return View();
            }
            return View("LoadVisitedStudentListReport", visitedStudentSearch);
        }

        [HttpGet]
        public ActionResult VisitedStudentAdmissionReport()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.Year = new SelectList(new List<SelectListItem>() { }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
            ViewBag.StudentVisitType = new SelectList(_commonHelper.LoadEmumToDictionary<StudentVisitType>(), "Key", "Value");
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<SelectListItem>() { }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
            ViewBag.SessionId = new SelectList(new List<SelectListItem>() { }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
            ViewBag.BranchIds = new SelectList(new List<SelectListItem>() { }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
            ViewBag.UserIds = new SelectList(new List<SelectListItem>() { }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
            ViewBag.studentType = new SelectList(new List<SelectListItem>()
                                                               {
                                                                   new SelectListItem() {Value = StudentType.Admitted.ToString(), Text = "Admitted"},
                                                                   new SelectListItem() {Value =StudentType.Nonadmitted.ToString() , Text = "Not Admitted"}
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
            return View();
        }

        [HttpPost]
        public ActionResult VisitedStudentAdmissionReport(VisitedStudentSearch visitedStudentSearch)
        {
            try
            {
                if (visitedStudentSearch != null)
                {
                    if (visitedStudentSearch.InformationViewList == null)
                    {
                        var infoView = new List<string>(new[] { "Visited User", "User Branch", "Nick Name", "Mobile Number (Student)" });
                        visitedStudentSearch.InformationViewList = infoView.ToArray();
                    }
                    else
                    {
                        var infoView = new List<string>(new[] { "Visited User", "User Branch" });
                        infoView.AddRange(visitedStudentSearch.InformationViewList);
                        visitedStudentSearch.InformationViewList = infoView.ToArray();
                    }
                    ViewBag.PageSize = Constants.PageSize;
                    ViewBag.Year = visitedStudentSearch.Year;
                    ViewBag.Class = visitedStudentSearch.Class;
                    ViewBag.StudentVisitType = visitedStudentSearch.StudentVisitType;
                    ViewBag.OrganizationId = visitedStudentSearch.OrganizationId;
                    ViewBag.ProgramId = visitedStudentSearch.ProgramId;
                    ViewBag.SessionId = visitedStudentSearch.SessionId;
                    ViewBag.BranchIds = visitedStudentSearch.BranchIds;
                    ViewBag.UserIds = visitedStudentSearch.UserIds;
                    ViewBag.StudentType = visitedStudentSearch.StudentType;

                }
                else
                {
                    ViewBag.ErrorMessage = "Information Not Found .";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return View();
            }
            return View("LoadVisitedStudentAdmissionListReport", visitedStudentSearch);
        }
        #endregion

        #region Render Data Table Student Information List

        [HttpPost]
        public JsonResult VisitedStudentAjaxRequest(int draw, int start, int length, string classes, string year, string[] informationViewList, string name, string mobile, string institute, string StudentVisitType = "", string UserId = "", string InterestedOrganizationId = "", long[] InterestedBranchIds = null)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    #region OrderBy and Direction
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                                orderBy = "CreateBy";
                                break;
                            case "1":
                                orderBy = "";
                                break;
                            case "2":
                                orderBy = "NickName";
                                break;
                            case "3":
                                orderBy = "Mobile";
                                break;
                            case "4":
                                orderBy = "Institute";
                                break;
                            case "5":
                                orderBy = "Class";
                                break;
                            case "6":
                                orderBy = "Year";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }
                    #endregion

                    if (InterestedBranchIds == null || (InterestedBranchIds.Length == 1 && InterestedBranchIds[0] == 0))
                    {
                        InterestedBranchIds = new long[] { };
                    }
                    int recordsTotal = _studentVisitedService.GetStudentVisitedCount(classes, year, name, mobile, institute, StudentVisitType, UserId, InterestedOrganizationId, InterestedBranchIds);
                    IList<StudentVisited> visitedStudentList = _studentVisitedService.LoadStudentVisited(classes, year, orderBy, orderDir.ToUpper(), start, length, name, mobile, institute, StudentVisitType, UserId, InterestedOrganizationId, InterestedBranchIds).ToList();
                    long recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    int sl = start + 1;
                    #region Datatable Grid Generation
                    foreach (var c in visitedStudentList)
                    {
                        var str = new List<string>();

                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Visited User":
                                        if (c.CreateBy != null)
                                        {
                                            //This is a Quick Fix
                                            //Need To magre with the main Query and resolve higher DB calling problem
                                            AspNetUser u = _userService.LoadAspNetUserById(c.CreateBy);
                                            str.Add(u.FullName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "User Branch":
                                        if (c.VisitedBranch != null)
                                        {
                                            str.Add(c.VisitedBranch.Name);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Nick Name":
                                        if (c.NickName != null)
                                        {
                                            str.Add(c.NickName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Student)":
                                        if (c.Mobile != null)
                                        {
                                            str.Add(c.Mobile);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Institute":
                                        if (c.Institute != null)
                                        {
                                            str.Add(c.Institute);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Class":
                                        if (c.Class != null)
                                        {
                                            str.Add(c.Class);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Year":
                                        if (c.Year != null)
                                        {
                                            str.Add(c.Year);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "S/C Last Exam":
                                        if (c.Exam != null)
                                        {
                                            str.Add(c.Exam);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "GPA":
                                        if (c.Gpa != null)
                                        {
                                            str.Add(c.Gpa.ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Merit Position":
                                        if (c.MeritPosition != null)
                                        {
                                            str.Add(c.MeritPosition);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Section":
                                        if (c.Section != null)
                                        {
                                            str.Add(c.Section);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Profession":
                                        if (c.Profession != null)
                                        {
                                            str.Add(c.Profession);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Father)":
                                        if (c.MobileFather != null)
                                        {
                                            str.Add(c.MobileFather);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Mother)":
                                        if (c.MobileMother != null)
                                        {
                                            str.Add(c.MobileMother);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Interested Branch":
                                        if (c.InterestedBranch != null)
                                        {
                                            str.Add(c.InterestedBranch.Name);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Version of Study":
                                        if (c.VersionOfStudy != null || c.VersionOfStudy > 0)
                                        {
                                            VersionOfStudy v = (VersionOfStudy)c.VersionOfStudy;
                                            str.Add(v.ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Gender":
                                        if (c.Gender != null || c.Gender > 0)
                                        {
                                            Gender g = (Gender)c.Gender;
                                            str.Add(g.ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Religion":
                                        if (c.Religion != null || c.Religion > 0)
                                        {
                                            Religion r = (Religion)c.Religion;
                                            str.Add(r.ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }

                                    default:
                                        break;
                                }

                            }
                            //str.Add("<a target='_blank' href='" + Url.Action("InformationView", "Information") + "?PrnNo=" + c.PrnNo + "'  class='glyphicon glyphicon-th-list'></a>");
                        }
                        sl++;
                        str.Add("<a target='_blank' href='/Student/StudentVisited/Edit/" + c.Id.ToString() + "' data-id='" + c.Id.ToString() + "' class='glyphicon glyphicon-pencil'> </a>");
                        data.Add(str);
                    }
                    #endregion
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else { return Json(HttpNotFound()); }

        }

        [HttpPost]
        public JsonResult VisitedStudentAdmissionAjaxRequest(int draw, int start, int length, string Class, string Year, string[] InformationViewList, string Name
            , string Mobile, string Institute, long ProgramId, long SessionId, int StudentType, string StudentVisitType = "", long[] UserIds = null, long[] BranchIds = null)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    #region OrderBy and Direction
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                                orderBy = "CreateBy";
                                break;
                            case "1":
                                orderBy = "";
                                break;
                            case "2":
                                orderBy = "NickName";
                                break;
                            case "3":
                                orderBy = "Mobile";
                                break;
                            case "4":
                                orderBy = "Institute";
                                break;
                            case "5":
                                orderBy = "Class";
                                break;
                            case "6":
                                orderBy = "Year";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }
                    #endregion

                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    if (UserIds == null || (UserIds.Length > 0 && UserIds[0] == 0))
                    {
                        UserIds = new long[] { };
                    }
                    if (BranchIds == null || (BranchIds.Length > 0 && BranchIds[0] == 0))
                    {
                        BranchIds = new long[] { };
                    }
                    int recordsTotal = _studentVisitedService.GetStudentAdmissionVisitedCount(Class, Year, ProgramId, SessionId, Name, Mobile, Institute, StudentVisitType, StudentType, UserIds, BranchIds);
                    IList<StudentVisited> visitedStudentList = _studentVisitedService.LoadStudentAdmissionVisited(orderBy, orderDir.ToUpper(), start, length, Class, Year, ProgramId, SessionId, Name, Mobile, Institute, StudentVisitType, StudentType, UserIds, BranchIds).ToList();
                    long recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    int sl = start + 1;
                    #region Datatable Grid Generation
                    foreach (var c in visitedStudentList)
                    {
                        var str = new List<string>();

                        if (InformationViewList != null)
                        {
                            foreach (string informationView in InformationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Visited User":
                                        if (c.CreateBy != null)
                                        {
                                            //This is a Quick Fix
                                            //Need To magre with the main Query and resolve higher DB calling problem
                                            AspNetUser u = _userService.LoadAspNetUserById(c.CreateBy);
                                            str.Add(u.FullName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "User Branch":
                                        if (c.VisitedBranch != null)
                                        {
                                            str.Add(c.VisitedBranch.Name);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Nick Name":
                                        if (c.NickName != null)
                                        {
                                            str.Add(c.NickName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Student)":
                                        if (c.Mobile != null)
                                        {
                                            str.Add(c.Mobile);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Institute":
                                        if (c.Institute != null)
                                        {
                                            str.Add(c.Institute);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Class":
                                        if (c.Class != null)
                                        {
                                            str.Add(c.Class);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Year":
                                        if (c.Year != null)
                                        {
                                            str.Add(c.Year);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "S/C Last Exam":
                                        if (c.Exam != null)
                                        {
                                            str.Add(c.Exam);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "GPA":
                                        if (c.Gpa != null)
                                        {
                                            str.Add(c.Gpa.ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Merit Position":
                                        if (c.MeritPosition != null)
                                        {
                                            str.Add(c.MeritPosition);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Section":
                                        if (c.Section != null)
                                        {
                                            str.Add(c.Section);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Profession":
                                        if (c.Profession != null)
                                        {
                                            str.Add(c.Profession);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Father)":
                                        if (c.MobileFather != null)
                                        {
                                            str.Add(c.MobileFather);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Mother)":
                                        if (c.MobileMother != null)
                                        {
                                            str.Add(c.MobileMother);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Interested Branch":
                                        if (c.InterestedBranch != null)
                                        {
                                            str.Add(c.InterestedBranch.Name);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Version of Study":
                                        if (c.VersionOfStudy != null || c.VersionOfStudy > 0)
                                        {
                                            VersionOfStudy v = (VersionOfStudy)c.VersionOfStudy;
                                            str.Add(v.ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Gender":
                                        if (c.Gender != null || c.Gender > 0)
                                        {
                                            Gender g = (Gender)c.Gender;
                                            str.Add(g.ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Religion":
                                        if (c.Religion != null || c.Religion > 0)
                                        {
                                            Religion r = (Religion)c.Religion;
                                            str.Add(r.ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }

                                    default:
                                        break;
                                }

                            }
                            //str.Add("<a target='_blank' href='" + Url.Action("InformationView", "Information") + "?PrnNo=" + c.PrnNo + "'  class='glyphicon glyphicon-th-list'></a>");
                        }
                        sl++;
                        data.Add(str);

                    }
                    #endregion
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                    //return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return Json(HttpNotFound()); }
        }
        
        #endregion

        #endregion

        #region Helper Function

        [HttpGet]
        public ActionResult ExportFiles(string classes, string year, string[] informationViewList, string StudentVisitType = "", string UserId = "", string InterestedOrganizationId = "", long[] InterestedBranchIds = null)
        {
            try
            {
                if (InterestedBranchIds == null || (InterestedBranchIds.Length == 1 && InterestedBranchIds[0] == 0))
                {
                    InterestedBranchIds = new long[] { };
                }
                IList<StudentVisited> studentVisitedList = _studentVisitedService.LoadStudentVisited(classes, year, "", "", 0, 0, "", "", "", StudentVisitType, UserId, InterestedOrganizationId, InterestedBranchIds).ToList();
                List<string> headerList = new List<string>();
                headerList.Add("Visited Student List Report");
                headerList.Add("");

                List<string> footerList = new List<string>();
                footerList.Add("");

                List<string> columnList = new List<string>();
                if (informationViewList != null)
                {
                    if (informationViewList.Contains("Visited User"))
                    {
                        columnList.Add("Visited User");
                    }
                    if (informationViewList.Contains("User Branch"))
                    {
                        columnList.Add("User Branch");
                    }
                    if (informationViewList.Contains("Nick Name"))
                    {
                        columnList.Add("Nick Name");
                    }
                    if (informationViewList.Contains("Mobile Number (Student)"))
                    {
                        columnList.Add("Mobile Number (Student)");
                    }
                    if (informationViewList.Contains("Institute"))
                    {
                        columnList.Add("Institute");
                    }
                    if (informationViewList.Contains("Class"))
                    {
                        columnList.Add("Class");
                    }
                    if (informationViewList.Contains("Year"))
                    {
                        columnList.Add("Year");
                    }
                    if (informationViewList.Contains("S/C Last Exam"))
                    {
                        columnList.Add("S/C Last Exam");
                    }
                    if (informationViewList.Contains("GPA"))
                    {
                        columnList.Add("GPA");
                    }
                    if (informationViewList.Contains("Merit Position"))
                    {
                        columnList.Add("Merit Position");
                    }
                    if (informationViewList.Contains("Section"))
                    {
                        columnList.Add("Section");
                    }
                    if (informationViewList.Contains("Profession"))
                    {
                        columnList.Add("Profession");
                    }
                    if (informationViewList.Contains("Mobile Number (Father)"))
                    {
                        columnList.Add("Mobile Number (Father)");
                    }
                    if (informationViewList.Contains("Mobile Number (Mother)"))
                    {
                        columnList.Add("Mobile Number (Mother)");
                    }
                    if (informationViewList.Contains("Interested Branch"))
                    {
                        columnList.Add("Interested Branch");
                    }
                    if (informationViewList.Contains("Version of Study"))
                    {
                        columnList.Add("Version of Study");
                    }
                    if (informationViewList.Contains("Gender"))
                    {
                        columnList.Add("Gender");
                    }
                    if (informationViewList.Contains("Religion"))
                    {
                        columnList.Add("Religion");
                    }
                }

                var studentExcelList = new List<List<object>>();
                foreach (var xSes in studentVisitedList)
                {
                    var xlsRow = new List<object>();
                    if (informationViewList != null)
                    {
                        if (informationViewList.Contains("Visited User"))
                        {
                            //This is a Quick Fix
                            //Need To magre with the main Query and resolve higher DB calling problem
                            AspNetUser u = _userService.LoadAspNetUserById(xSes.CreateBy);
                            xlsRow.Add(u.FullName ?? "");
                        }
                        if (informationViewList.Contains("User Branch"))
                        {
                            xlsRow.Add(xSes.VisitedBranch.Name ?? "");
                        }
                        if (informationViewList.Contains("Nick Name"))
                        {
                            xlsRow.Add(xSes.NickName ?? "");
                        }
                        if (informationViewList.Contains("Mobile Number (Student)"))
                        {
                            xlsRow.Add(xSes.Mobile ?? "");
                        }
                        if (informationViewList.Contains("Institute"))
                        {
                            xlsRow.Add(xSes.Institute ?? "");
                        }
                        if (informationViewList.Contains("Class"))
                        {
                            xlsRow.Add(xSes.Class ?? "");
                        }
                        if (informationViewList.Contains("Year"))
                        {
                            xlsRow.Add(xSes.Year ?? "");
                        }
                        if (informationViewList.Contains("S/C Last Exam"))
                        {
                            xlsRow.Add(xSes.Exam ?? "");
                        }
                        if (informationViewList.Contains("GPA"))
                        {
                            xlsRow.Add(xSes.Gpa.ToString() ?? "");
                        }
                        if (informationViewList.Contains("Merit Position"))
                        {
                            xlsRow.Add(xSes.MeritPosition ?? "");
                        }
                        if (informationViewList.Contains("Section"))
                        {
                            xlsRow.Add(xSes.Section ?? "");
                        }
                        if (informationViewList.Contains("Profession"))
                        {
                            xlsRow.Add(xSes.Profession ?? "");
                        }
                        if (informationViewList.Contains("Mobile Number (Father)"))
                        {
                            xlsRow.Add(xSes.MobileFather ?? "");
                        }
                        if (informationViewList.Contains("Mobile Number (Father)"))
                        {
                            xlsRow.Add(xSes.MobileMother ?? "");
                        }
                        if (informationViewList.Contains("Interested Branch"))
                        {
                            xlsRow.Add(xSes.InterestedBranch == null ? "" : xSes.InterestedBranch.Name);
                        }
                        if (informationViewList.Contains("Version of Study"))
                        {
                            if (xSes.VersionOfStudy > 0)
                            {
                                VersionOfStudy v = (VersionOfStudy)xSes.VersionOfStudy;
                                xlsRow.Add(v.ToString());
                            }
                            else { xlsRow.Add(""); }
                        }
                        if (informationViewList.Contains("Gender"))
                        {
                            if (xSes.Gender > 0)
                            {
                                Gender g = (Gender)xSes.Gender;
                                xlsRow.Add(g.ToString());
                            }
                            else { xlsRow.Add(""); }
                        }
                        if (informationViewList.Contains("Religion"))
                        {
                            if (xSes.Religion > 0)
                            {
                                Religion r = (Religion)xSes.Religion;
                                xlsRow.Add(r.ToString());
                            }
                            else { xlsRow.Add(""); }
                        }
                    }
                    studentExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "visited_student-list-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return View("AutoClose");
            }
        }

        [HttpGet]
        public ActionResult ExportFilesStudentAdmission(string Class, string Year, string[] InformationViewList, string Name
            , string Mobile, string Institute, long ProgramId, long SessionId, int StudentType, string StudentVisitType = "", long[] UserIds = null, long[] BranchIds = null)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                if (UserIds == null || (UserIds.Length > 0 && UserIds[0] == 0))
                {
                    UserIds = new long[] { };
                }
                if (BranchIds == null || (BranchIds.Length > 0 && BranchIds[0] == 0))
                {
                    BranchIds = new long[] { };
                }
                IList<StudentVisited> studentVisitedList = _studentVisitedService.LoadStudentAdmissionVisited("", "", 0, 0, Class, Year, ProgramId, SessionId, "", "", "", StudentVisitType, StudentType, UserIds, BranchIds).ToList();
                List<string> headerList = new List<string>();
                headerList.Add("Visited Student Admission List Report");
                headerList.Add("");

                List<string> footerList = new List<string>();
                footerList.Add("");

                List<string> columnList = new List<string>();
                if (InformationViewList != null)
                {
                    if (InformationViewList.Contains("Visited User"))
                    {
                        columnList.Add("Visited User");
                    }
                    if (InformationViewList.Contains("User Branch"))
                    {
                        columnList.Add("User Branch");
                    }
                    if (InformationViewList.Contains("Nick Name"))
                    {
                        columnList.Add("Nick Name");
                    }
                    if (InformationViewList.Contains("Mobile Number (Student)"))
                    {
                        columnList.Add("Mobile Number (Student)");
                    }
                    if (InformationViewList.Contains("Institute"))
                    {
                        columnList.Add("Institute");
                    }
                    if (InformationViewList.Contains("Class"))
                    {
                        columnList.Add("Class");
                    }
                    if (InformationViewList.Contains("Year"))
                    {
                        columnList.Add("Year");
                    }
                    if (InformationViewList.Contains("S/C Last Exam"))
                    {
                        columnList.Add("S/C Last Exam");
                    }
                    if (InformationViewList.Contains("GPA"))
                    {
                        columnList.Add("GPA");
                    }
                    if (InformationViewList.Contains("Merit Position"))
                    {
                        columnList.Add("Merit Position");
                    }
                    if (InformationViewList.Contains("Section"))
                    {
                        columnList.Add("Section");
                    }
                    if (InformationViewList.Contains("Profession"))
                    {
                        columnList.Add("Profession");
                    }
                    if (InformationViewList.Contains("Mobile Number (Father)"))
                    {
                        columnList.Add("Mobile Number (Father)");
                    }
                    if (InformationViewList.Contains("Mobile Number (Mother)"))
                    {
                        columnList.Add("Mobile Number (Mother)");
                    }
                    if (InformationViewList.Contains("Interested Branch"))
                    {
                        columnList.Add("Interested Branch");
                    }
                    if (InformationViewList.Contains("Version of Study"))
                    {
                        columnList.Add("Version of Study");
                    }
                    if (InformationViewList.Contains("Gender"))
                    {
                        columnList.Add("Gender");
                    }
                    if (InformationViewList.Contains("Religion"))
                    {
                        columnList.Add("Religion");
                    }
                }

                var studentExcelList = new List<List<object>>();
                foreach (var xSes in studentVisitedList)
                {
                    var xlsRow = new List<object>();
                    if (InformationViewList != null)
                    {
                        if (InformationViewList.Contains("Visited User"))
                        {
                            //This is a Quick Fix
                            //Need To magre with the main Query and resolve higher DB calling problem
                            AspNetUser u = _userService.LoadAspNetUserById(xSes.CreateBy);
                            xlsRow.Add(u.FullName ?? "");
                        }
                        if (InformationViewList.Contains("User Branch"))
                        {
                            xlsRow.Add(xSes.VisitedBranch.Name ?? "");
                        }
                        if (InformationViewList.Contains("Nick Name"))
                        {
                            xlsRow.Add(xSes.NickName ?? "");
                        }
                        if (InformationViewList.Contains("Mobile Number (Student)"))
                        {
                            xlsRow.Add(xSes.Mobile ?? "");
                        }
                        if (InformationViewList.Contains("Institute"))
                        {
                            xlsRow.Add(xSes.Institute ?? "");
                        }
                        if (InformationViewList.Contains("Class"))
                        {
                            xlsRow.Add(xSes.Class ?? "");
                        }
                        if (InformationViewList.Contains("Year"))
                        {
                            xlsRow.Add(xSes.Year ?? "");
                        }
                        if (InformationViewList.Contains("S/C Last Exam"))
                        {
                            xlsRow.Add(xSes.Exam ?? "");
                        }
                        if (InformationViewList.Contains("GPA"))
                        {
                            xlsRow.Add(xSes.Gpa.ToString() ?? "");
                        }
                        if (InformationViewList.Contains("Merit Position"))
                        {
                            xlsRow.Add(xSes.MeritPosition ?? "");
                        }
                        if (InformationViewList.Contains("Section"))
                        {
                            xlsRow.Add(xSes.Section ?? "");
                        }
                        if (InformationViewList.Contains("Profession"))
                        {
                            xlsRow.Add(xSes.Profession ?? "");
                        }
                        if (InformationViewList.Contains("Mobile Number (Father)"))
                        {
                            xlsRow.Add(xSes.MobileFather ?? "");
                        }
                        if (InformationViewList.Contains("Mobile Number (Father)"))
                        {
                            xlsRow.Add(xSes.MobileMother ?? "");
                        }
                        if (InformationViewList.Contains("Interested Branch"))
                        {
                            xlsRow.Add(xSes.InterestedBranch == null ? "" : xSes.InterestedBranch.Name);
                        }
                        if (InformationViewList.Contains("Version of Study"))
                        {
                            if (xSes.VersionOfStudy > 0)
                            {
                                VersionOfStudy v = (VersionOfStudy)xSes.VersionOfStudy;
                                xlsRow.Add(v.ToString());
                            }
                            else { xlsRow.Add(""); }
                        }
                        if (InformationViewList.Contains("Gender"))
                        {
                            if (xSes.Gender > 0)
                            {
                                Gender g = (Gender)xSes.Gender;
                                xlsRow.Add(g.ToString());
                            }
                            else { xlsRow.Add(""); }
                        }
                        if (InformationViewList.Contains("Religion"))
                        {
                            if (xSes.Religion > 0)
                            {
                                Religion r = (Religion)xSes.Religion;
                                xlsRow.Add(r.ToString());
                            }
                            else { xlsRow.Add(""); }
                        }
                    }
                    studentExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "visited_student-list-report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return View("AutoClose");
            }
        }

        #endregion
    }
}