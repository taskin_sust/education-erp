﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UdvashERP.FingerPrint.BusinessModel;

namespace UdvashERP.FingerPrint
{
    public partial class BaseForm : XtraForm
    {
        public UserProfile CurrentUserProfile { get; set; }

        public BaseForm()
        {
            InitializeComponent();
        }
    }
}
