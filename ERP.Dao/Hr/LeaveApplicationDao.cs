﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Engine.Query;
using NHibernate.Impl;
using NHibernate.Transform;
using NHibernate.Linq;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface ILeaveApplicationDao : IBaseDao<LeaveApplication, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        Leave GetByLeaveAppId(long leaveAppId);
        LeaveApplication GetApprovedLeaveByPinAndDate(long memberId, DateTime attendenceDate);
        LeaveApplication GetTeamMemberLastAppliedApplication(long? teamMemberId, int? pin = null);

        #endregion

        #region List Loading Function
        
        IList<LeaveApplication> LoadTeamMemberLeaveApplication(int start, int length, DateTime dateTime, long memId, string leaveTypeId = "", string leaveStatus = "");
        IList<LeaveApplicationViewModel> LoadMentorLeave(DateTime? dateTime = null, LeaveStatus? leaveStatus = null, List<long> mentorMemberIdList = null, int start = 0, int length = 0);
        IList<LeaveApplicationViewModel> LoadHrLeave(DateTime? searchingDateTime = null, List<long> authBranchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<int> memberPinList = null, LeaveStatus? leaveStatus = null, int start = 0, int length = 0);
        
        #endregion

        #region Others Function

        int GetTeamMemberLeaveApplicationCount(DateTime dateTime, long memId, string leaveTypeId = "", string leaveStatus = "");
        int GetMentorLeaveCount(DateTime? dateTime, LeaveStatus? leaveStatus, List<long> mentorMemberIdList);
        int GetHrLeaveCount(DateTime? searchingDateTime = null, List<long> authBranchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<int> memberPinList = null, LeaveStatus? leaveStatus = null);
        int TeamMemberPendingLeaveCount(TeamMember teamMember, DateTime datefrom, DateTime dateTo, LeaveStatus leaveStatus);
        bool HasPendingLeaveApplication(int year, long teamMemberId);
        int GetTotalLeaveApplicationCountForRepeatTypeLeave(long leaveId, long teamMemberId);

        #endregion

        #region Helper Function

        bool IsDuplicateLeaveApplicationByTeamMemberAndDate(long teamMemberId, DateTime date, long leaveApplicationId);
        bool MemberLeaveDates(long teamMemberId, DateTime date);
        LeaveApplication MemberResponsibleLeaveApplication(long responsiblePersonId, DateTime date);
        
        #endregion
        
    }

    public class LeaveApplicationDao : BaseDao<LeaveApplication, long>, ILeaveApplicationDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        public Leave GetByLeaveAppId(long leaveAppId)
        {
            var laObj = Session.QueryOver<LeaveApplication>().Where(x => x.Id == leaveAppId && x.Status == LeaveApplication.EntityStatus.Active).SingleOrDefault<LeaveApplication>();
            return laObj.Leave;
        }

        public LeaveApplication GetApprovedLeaveByPinAndDate(long memberId, DateTime attendenceDate)
        {
            LeaveApplication leaveObj = Session.QueryOver<LeaveApplication>()
                                        .Where(x => x.DateFrom <= attendenceDate && x.DateTo >= attendenceDate
                                            && x.TeamMember.Id == memberId
                                            && x.LeaveStatus == (int)LeaveStatus.Approved
                                        )
                                        .List()
                                        .FirstOrDefault();
            return leaveObj;
        }

        public LeaveApplication GetTeamMemberLastAppliedApplication(long? teamMemberId, int? pin = null)
        {
            var leaveQuery = Session.QueryOver<LeaveApplication>()
                                        .Where(x =>x.Status == LeaveApplication.EntityStatus.Active 
                                            && (x.LeaveStatus == (int)LeaveStatus.Approved || x.LeaveStatus== (int)LeaveStatus.Pending)
                                        );
            if (teamMemberId != null)
                leaveQuery.And(x => x.TeamMember.Id == teamMemberId);
            if (pin != null)
                leaveQuery.And(x => x.TeamMember.Pin == pin);
            LeaveApplication leaveObj = leaveQuery.OrderBy(x => x.DateTo.Date).Desc.Take(1).SingleOrDefault<LeaveApplication>();
            return leaveObj;
        }

        #endregion

        #region List Loading Function

        public IList<LeaveApplication> LoadTeamMemberLeaveApplication(int start, int length, DateTime dateTime, long memId, string leaveTypeId = "", string leaveStatus = "")
        {
            ICriteria criteria = GetLoadTeamMemberLeaveApplicationQuery(dateTime, memId, leaveTypeId, leaveStatus); 
            criteria.AddOrder(new Order("CreationDate", false));
            return criteria.SetFirstResult(start).SetMaxResults(length).List<LeaveApplication>();
        }
        
        public IList<LeaveApplicationViewModel> LoadMentorLeave(DateTime? dateTime = null, LeaveStatus? leaveStatus = null, List<long> mentorMemberIdList = null, int start = 0, int length = 0)
        {
            string query = LoadMentorLeaveQuery(dateTime, leaveStatus, mentorMemberIdList);
            var fianalQuery = "DECLARE @rowsperpage INT DECLARE @start INT SET @start = " + (start + 1) + " SET @rowsperpage = " + length + @" SELECT 
                [LeaveApplicationId],
	            [TeamMemberPin],
	            [TeamMemberName],
	            [DateFrom],
	            [DateTo],
	            [TotalLeaveDay],
	            [LeaveNote],
	            [ResponsibleMemberPin],
	            [ResponsibleMemberName],
                IsPost,
                LeaveStatus,
                LeaveId,
                ApplicationDate
                FROM (select *, RANK() over (Partition BY 1 ORDER BY [LeaveApplicationId] DESC) AS RowNum from ( " + query + @"
                 ) AS A ) as pagination ";
            if (length > 0)
            {
                fianalQuery += " where  pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            var criteria = Session.CreateSQLQuery(fianalQuery).SetResultTransformer(Transformers.AliasToBean<LeaveApplicationViewModel>());
            return criteria.List<LeaveApplicationViewModel>();

        }

        public IList<LeaveApplicationViewModel> LoadHrLeave(DateTime? searchingDateTime = null, List<long> authBranchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<int> memberPinList = null, LeaveStatus? leaveStatus = null, int start = 0, int length = 0)
        {
            string query = LoadHrLeaveQuery(searchingDateTime, authBranchIdList, campusIdList, departmentIdList, memberPinList, leaveStatus);
            var fianalQuery = @"
                DECLARE @rowsperpage INT; 
                DECLARE @start INT; 
                SET @start = " + (start + 1) + @"; 
                SET @rowsperpage = " + length + @"; 

                SELECT 
                    [LeaveApplicationId],
	                [TeamMemberPin],
	                [TeamMemberName],
                    [Department],
                    [Designation],
                    [Organization],
                    [Branch],
                    [Campus],
	                [DateFrom],
	                [DateTo],
	                [TotalLeaveDay],
	                [LeaveNote],
	                [ResponsibleMemberPin],
	                [ResponsibleMemberName],
                    IsPost,
                    LeaveStatus,
                    LeaveId,
                    ApplicationDate,
                    ModifyBy,
                    LastModificationDate
                FROM (
                    SELECT *, RANK() OVER (PARTITION BY 1 ORDER BY [LeaveApplicationId] DESC) AS RowNum 
                    FROM ( " + query + @" ) AS A 
                ) as pagination ";
            if (length > 0)
            {
                fianalQuery += " WHERE pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            var criteria = Session.CreateSQLQuery(fianalQuery).SetResultTransformer(Transformers.AliasToBean<LeaveApplicationViewModel>());
            return criteria.List<LeaveApplicationViewModel>();
        }
        
        #endregion

        #region Others Function

        public int GetTeamMemberLeaveApplicationCount( DateTime dateTime, long memId, string leaveTypeId = "", string leaveStatus = "")
        {
            ICriteria criteria = GetLoadTeamMemberLeaveApplicationQuery(dateTime, memId, leaveTypeId, leaveStatus);
            criteria.SetProjection(Projections.RowCount());
            
            return criteria.UniqueResult<int>();
        }

        public int GetMentorLeaveCount(DateTime? dateTime, LeaveStatus? leaveStatus, List<long> mentorMemberIdList)
        {
            string query = LoadMentorLeaveQuery(dateTime, leaveStatus, mentorMemberIdList);
            string countQuery = @" 
                        Select count(*) AS TotalRowCount from ( " + query + @" ) AS A
                ";
            IQuery iQuery = Session.CreateSQLQuery(countQuery);
            int count = iQuery.UniqueResult<int>();
            
            return count;
        }

        public int GetHrLeaveCount(DateTime? searchingDateTime = null, List<long> authBranchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<int> memberPinList = null, LeaveStatus? leaveStatus = null)
        {
            string query = LoadHrLeaveQuery(searchingDateTime, authBranchIdList, campusIdList, departmentIdList, memberPinList, leaveStatus);
            string countQuery = @" 
                        Select count(*) AS TotalRowCount from ( " + query + @" ) AS A
                ";
            IQuery iQuery = Session.CreateSQLQuery(countQuery);
            int count = iQuery.UniqueResult<int>();
            
            return count;
        }

        public int TeamMemberPendingLeaveCount(TeamMember teamMember, DateTime datefrom, DateTime dateTo, LeaveStatus leaveStatus)
        {
            return
                Session.QueryOver<LeaveApplication>()
                    .Where(
                        x =>
                            x.TeamMember == teamMember && x.DateFrom >= datefrom && x.DateTo <= dateTo &&
                            x.LeaveStatus == (int) leaveStatus)
                    .List<LeaveApplication>().Count();
        }

        public bool HasPendingLeaveApplication(int year, long teamMemberId)
        {
            int pendingLeave = Session.QueryOver<LeaveApplication>().Where(x => x.TeamMember.Id == teamMemberId && x.DateTo.Year == year && x.LeaveStatus == (int)LeaveStatus.Pending && x.Status == LeaveApplication.EntityStatus.Active).List<LeaveApplication>().Count();
            if (pendingLeave > 0)
                return true;
            return false;
        }

        public int GetTotalLeaveApplicationCountForRepeatTypeLeave(long leaveId, long teamMemberId)
        {
            return Session.QueryOver<LeaveApplication>().Where(x => x.TeamMember.Id == teamMemberId  && x.LeaveStatus == (int)LeaveStatus.Approved && x.Status == LeaveApplication.EntityStatus.Active && x.Leave.Id == leaveId).List<LeaveApplication>().Count();

        }

        #endregion

        #region Helper Function
        
        private string LoadHrLeaveQuery(DateTime? searchingDateTime = null, List<long> authBranchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<int> memberPinList = null, LeaveStatus? leaveStatus = null)
        {
            string leaveStatusQuery = "";
            string memberPinListQuery = "";
            string authBranchIdListQuery = "";
            string campusIdListQuery = "";
            string departmentIdListQuery = "";
            string searchingDateTimeQuery = "";
            
            if (leaveStatus != null)
                leaveStatusQuery = " AND la.LeaveStatus= " + (int)leaveStatus + "  ";
            else
                leaveStatusQuery = " AND la.LeaveStatus != " + (int)LeaveStatus.Pending + "  ";

            if (memberPinList != null && !memberPinList.Contains(0) && memberPinList.Any())
                memberPinListQuery = " AND tm.Pin IN (" + string.Join(",", memberPinList) + ")  ";

            if (authBranchIdList != null)
                authBranchIdListQuery = " AND a.BranchId IN (" + string.Join(",", authBranchIdList) + ")  ";

            if (campusIdList != null && !campusIdList.Contains(0) && campusIdList.Any())
                campusIdListQuery = " AND a.CampusId IN (" + string.Join(",", campusIdList) + ")  ";

            if (departmentIdList != null && !departmentIdList.Contains(0) && departmentIdList.Any())
                departmentIdListQuery = " AND a.DepartmentId IN (" + string.Join(",", departmentIdList) + ")  ";

            if (searchingDateTime != null)
            {
                searchingDateTimeQuery = " AND '" + searchingDateTime.Value.ToString("yyyy-MM-dd") + "' >= la.DateFrom AND '" + searchingDateTime.Value.ToString("yyyy-MM-dd") + "' <= la.DateTo ";
            }
            string query = GetMentorHrLeaveCommonQuery() + leaveStatusQuery + memberPinListQuery + authBranchIdListQuery + campusIdListQuery + departmentIdListQuery + searchingDateTimeQuery;
            return query;
        }
        
        private string LoadMentorLeaveQuery(DateTime? dateTime = null, LeaveStatus? leaveStatus = null, List<long> mentorMemberIdList = null)
        {
            string leaveStatusQuery = "";
            string mentorMemberIdListQuery = "";
            string searchingDateTimeQuery = "";

            if (leaveStatus != null)
            {
                leaveStatusQuery = " AND [LeaveStatus]= " + (int)leaveStatus + @"  ";
            }
            else
            {
                leaveStatusQuery = " AND [LeaveStatus] != " + (int)LeaveStatus.Pending + @"  ";
            }
            if (mentorMemberIdList != null)
            {
                mentorMemberIdListQuery = " AND a.TeamMemberId IN (" + string.Join(",", mentorMemberIdList) + ")  ";
            }
            if (dateTime != null)
            {
                searchingDateTimeQuery = " AND '" + dateTime.Value.ToString("yyyy-MM-dd") + "' >= la.DateFrom AND '" + dateTime.Value.ToString("yyyy-MM-dd") + "' <= la.DateTo ";
            }

            string query = GetMentorHrLeaveCommonQuery() + leaveStatusQuery + mentorMemberIdListQuery + searchingDateTimeQuery;
            return query;
        }

        private string GetMentorHrLeaveCommonQuery()
        {
            string query = @"Select 
	                        tm.Pin as TeamMemberPin
	                        , tm.Name as TeamMemberName
	                        , la.Id as LeaveApplicationId
	                        , l.Id as LeaveId
	                        , SUBSTRING(CONVERT(VARCHAR,la.DateFrom,120),1,10) AS DateFrom
	                        , SUBSTRING(CONVERT(VARCHAR,la.DateTo,120),1,10) AS DateTo
	                        , la.LeaveNote as LeaveNote
	                        , la.Remarks as LeaveReason
	                        , r.Pin as ResponsibleMemberPin
	                        , r.Name as ResponsibleMemberName
	                        , a.DesignationName as Designation
	                        , la.LeaveStatus as LeaveStatus
	                        , la.TeamMemberId as TeamMemberId
	                        , la.TotalLeaveDay as TotalLeaveDay
	                        , la.Status as Status
	                        , (SELECT TOP 1 CAST(IsPost AS Int) FROM HR_LeaveApplicationDetails WHERE Status != -404 AND leaveApplicationId = la.Id ORDER BY IsPost ASC) AS IsPost
	                        , a.DepartmentName as Department
	                        , a.OrganizationShortName as Organization
	                        , a.BranchName as Branch
	                        , a.CampusName as Campus
	                        , SUBSTRING(CONVERT(VARCHAR,la.CreationDate,120),1,10) as ApplicationDate
                            , la.ModifyBy as ModifyBy
                            , la.ModificationDate as LastModificationDate
                        from HR_LeaveApplication as la
                        inner join HR_TeamMember as tm on tm.Id = la.TeamMemberId and tm.Status = " + TeamMember.EntityStatus.Active + @"
                        LEFT JOIN HR_TeamMember AS r ON r.id = la.ResponsiblePersonId and tm.Status = " + TeamMember.EntityStatus.Active + @"
                        inner join HR_Leave as l on l.Id = la.LeaveId and l.Status = " + Leave.EntityStatus.Active + @"
                        CROSS APPLY  (
	                        Select * from (
		                        Select 
		                         Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		                        , emh.EmploymentStatus as EmployeeStatus
		                        , emh.EffectiveDate As EffectiveDate
		                        , emh.CampusId as CampusId
		                        , campus.Name as CampusName
		                        , branch.Id as BranchId
		                        , branch.Name as BranchName
		                        , emh.DepartmentId as DepartmentId
		                        , department.Name as DepartmentName
		                        , emh.DesignationId as DesignationId
		                        , designation.Name as DesignationName
		                        , organization.Id as OrganizationId
		                        , organization.ShortName as OrganizationShortName
		                        , emh.TeamMemberId as TeamMemberId
		                        from HR_EmploymentHistory as emh
		                        inner join Campus as campus on campus.Id = emh.CampusId and campus.Status = " + Campus.EntityStatus.Active + @"
		                        inner join Branch as branch on branch.Id = campus.BranchId and branch.Status = " + Branch.EntityStatus.Active + @"
		                        inner join HR_Department as department on department.Id = emh.DepartmentId and department.Status = " + Department.EntityStatus.Active + @"
		                        inner join HR_Designation as designation on designation.Id = emh.DesignationId and designation.Status = " + Designation.EntityStatus.Active + @"
		                        inner join Organization as organization on organization.Id = department.OrganizationId and organization.Status = " + Organization.EntityStatus.Active + @"
		                        where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		                        and  emh.EffectiveDate <=  la.DateFrom 
		                        and emh.TeamMemberId = la.TeamMemberId
	                        ) as employmentHistory 
	                        where 1=1
	                        and employmentHistory.r = 1
                        ) as a 
                        where 1=1
                        and la.Status = " + LeaveApplication.EntityStatus.Active + @"  
                        ";
            return query;
        }

        public ICriteria GetLoadTeamMemberLeaveApplicationQuery(DateTime dateTime, long memId, string leaveTypeId = "", string leaveStatus = "")
        {
            DateTime startDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var criteria = Session.CreateCriteria<LeaveApplication>().Add(Restrictions.Eq("Status", LeaveApplication.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "mem").Add(Restrictions.Eq("mem.Status", TeamMember.EntityStatus.Active));
            criteria.CreateCriteria("Leave", "leav").Add(Restrictions.Eq("mem.Status", Leave.EntityStatus.Active));
            //criteria.Add(Restrictions.Lt("CreationDate", dateTime));
            criteria.Add(Restrictions.Eq("mem.Id", memId));
            if (!String.IsNullOrEmpty(leaveTypeId))
            {
                criteria.Add(Restrictions.Eq("leav.Id", Convert.ToInt64(leaveTypeId)));
            }
            if (!String.IsNullOrEmpty(leaveStatus))
            {
                criteria.Add(Restrictions.Eq("LeaveStatus", Convert.ToInt32(leaveStatus)));
            }

            return criteria;
        }

        public bool IsDuplicateLeaveApplicationByTeamMemberAndDate(long teamMemberId, DateTime date, long leaveApplicationId)
        {
            DateTime d = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second);
            bool ret = false;
            var leaveApplication = Session.Query<LeaveApplication>().Where(x => x.DateFrom <= d && x.DateTo >= d
                                                                && x.TeamMember.Id == teamMemberId
                                                                && (x.LeaveStatus == (int)LeaveStatus.Approved || x.LeaveStatus == (int)LeaveStatus.Pending)
                                                                && x.Id != leaveApplicationId
                                                                && x.Status == LeaveApplication.EntityStatus.Active);

            if (leaveApplication.ToList().Count > 0)
                ret = true;
            return ret;
        }
        
        public bool MemberLeaveDates(long teamMemberId, DateTime date)
        {
            bool ret = false;
            var leaveApplication = Session.Query<LeaveApplication>().Where(x => x.DateFrom <= date && x.DateTo >= date
                                                                && x.TeamMember.Id == teamMemberId
                                                                && (x.LeaveStatus == (int)LeaveStatus.Approved || x.LeaveStatus == (int)LeaveStatus.Pending)
                                                                && x.Status == LeaveApplication.EntityStatus.Active);
            if (leaveApplication.ToList().Count > 0)
                ret = true;
            return ret;
        }

        public LeaveApplication MemberResponsibleLeaveApplication(long responsiblePersonId, DateTime date)
        {
            return Session.Query<LeaveApplication>().Where(x => x.DateFrom <= date && x.DateTo >= date
                                                                && x.ResponsiblePerson.Id == responsiblePersonId
                                                                && (x.LeaveStatus == (int)LeaveStatus.Approved || x.LeaveStatus == (int)LeaveStatus.Pending)
                                                                && x.Status == LeaveApplication.EntityStatus.Active)
                                                                .FirstOrDefault();
        }
        
        #endregion

    }
}