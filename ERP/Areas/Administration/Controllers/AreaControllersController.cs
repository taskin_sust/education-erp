﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class AreaControllersController : Controller
    {
        #region Objects/Propertise/Services/Dao & Initialization
        private ILog logger = LogManager.GetLogger("AdministrationArea");
        private readonly ICommonHelper _commonHelper;
        private readonly IAreaControllersService _areacontrollersService;
        public AreaControllersController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _areacontrollersService = new AreaControllersService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
            }

        }
        #endregion

        #region Index/Manage
        public ActionResult Index()
        {
            try
            {
                GetStatusText();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                GetStatusText();
                return View();
            }
        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            var _areaControllersObj = _areacontrollersService.LoadById(id);
            return View(_areaControllersObj);
        }
        #endregion

        #region Save Operation
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AreaControllers areaControllers)
        {
            try
            {
                AreaControllers areaControllersObj = new AreaControllers();
                areaControllersObj.Name = areaControllers.Name;
                areaControllersObj.Area = areaControllers.Area;

                _areacontrollersService.Save(areaControllersObj);
                ViewBag.SuccessMessage = "AreaControllers Addded Successfully";
                ModelState.Clear();
                return View();
            }
            catch (DuplicateEntryException ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            AreaControllers areaControllers = _areacontrollersService.LoadById(id);
            if (areaControllers == null)
                return HttpNotFound();
            AreaControllersViewModel areaControllersViewModel = new AreaControllersViewModel();
            areaControllersViewModel.Name = areaControllers.Name;
            areaControllersViewModel.Area = areaControllers.Area;
            return View(areaControllersViewModel);
        }
        [HttpPost]
        public ActionResult Edit(long id, AreaControllersViewModel areaControllers)
        {
            AreaControllers ac = _areacontrollersService.LoadById(id);

            if (ac == null)
                return HttpNotFound();
            ac.Name = areaControllers.Name;
            ac.Area = areaControllers.Area;
            _areacontrollersService.Update(id, ac);
            ViewBag.SuccessMessage = "AreaControllers update Successfully";
            return View(areaControllers);
            
        }
        #endregion

        #region Delete Operation
        public ActionResult Delete(long id)
        {
            try
            {
                AreaControllers ac = _areacontrollersService.LoadById(id);
                if (ac == null)
                    return HttpNotFound();
                return View(ac);

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(AreaControllers ac)
        {
            AreaControllers deleteControllers = _areacontrollersService.LoadById(ac.Id);
            var success=_areacontrollersService.Delete(deleteControllers.Id, deleteControllers);
            return RedirectToAction("Index");
        }
        #endregion

        #region Render Data Table
        public JsonResult AreaControllersList(int draw, int start, int length, string name, string area, string status, string rank)
        {
            NameValueCollection nvc = Request.Form;

            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "Area";
                            break;
                        case "2":
                            orderBy = "Rank";
                            break;
                        case "3":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                int recordsTotal = _areacontrollersService.AreaControllersRowCount(name, area, status, rank);
                long recordsFiltered = recordsTotal;

                var _areaControllersList = _areacontrollersService.LoadActive(start, length, orderBy, orderDir.ToUpper(), name, area, status, rank).ToList();

                AreaControllers obj = null;
                int[] maxMinRank = new int[2];
                maxMinRank[0] = 1;
                maxMinRank[1] = _areacontrollersService.GetMaxRank(obj);

                foreach (var ac in _areaControllersList)
                {
                    var str = new List<string>();
                    str.Add(ac.Name);
                    str.Add(ac.Area);

                    string rankbtn = ac.Rank.ToString();

                    if (ac.Rank > maxMinRank[0])
                    {
                        rankbtn += "<a href='#'  id='" + ac.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    }
                    if (ac.Rank < maxMinRank[1])
                    {
                        rankbtn += "<a href='#'  id='" + ac.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    }

                    str.Add(rankbtn);
                    str.Add(StatusTypeText.GetStatusText(ac.Status));

                    str.Add("<a href='" + Url.Action("Details", "AreaControllers") + "?Id=" + ac.Id + "' data-id='" + ac.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;" +
                        "<a href='" + Url.Action("Edit", "AreaControllers") + "?Id=" + ac.Id + "' data-id='" + ac.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; " +
                            "<a href='" + Url.Action("Delete", "AreaControllers") + "?Id=" + ac.Id + "' data-id='" + ac.Id + "' class='glyphicon glyphicon-trash'></a> "
                           );
                    getData.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData

                });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });


        }
        #endregion

        #region Helper Function
        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }
        #endregion
    }
}