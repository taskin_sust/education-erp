using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{     
    public partial class HolidaySettingMap : BaseClassMap<HolidaySetting,long> {
        
        public HolidaySettingMap() {
			Table("HR_HolidaySetting");
			LazyLoad();
			References(x => x.Organization).Column("OrganizationId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.DateFrom).Column("DateFrom");
			Map(x => x.DateTo).Column("DateTo");
            Map(x => x.HolidayType).Column("HolidayType");
			Map(x => x.RepeatationType).Column("RepeatationType");
            Map(x => x.Description).Column("Description");
            HasMany(x => x.AttendanceSummary).KeyColumn("HolidaySettingId");
        }
    }
}
