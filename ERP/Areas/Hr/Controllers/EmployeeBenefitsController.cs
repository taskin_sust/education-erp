﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class EmployeeBenefitsController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private EmployeeBenefitsFundSetting _employeeBenefitsFundSetting;

        private readonly IOrganizationService _organizationService;
        private readonly IEmployeeBenefitsFundSettingService _employeeBenefitsFundService;
        private IMemberEbfBalanceService _memberEbfBalanceService;

        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _userMenus;

        public EmployeeBenefitsController()
        {
            var session = NHibernateSessionFactory.OpenSession();    
            _employeeBenefitsFundSetting = new EmployeeBenefitsFundSetting();

            _organizationService = new OrganizationService(session);
            _employeeBenefitsFundService = new EmployeeBenefitsFundSettingService(session);
            _memberEbfBalanceService = new MemberEbfBalanceService(session);

            _commonHelper = new CommonHelper();       
        }

        #endregion

        #region Operational Function

        #region Manage

        public ActionResult Index()
        {
            try
            {
                Init();
                //InitIndexView();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #region Render Data Table

        [HttpPost]
        public JsonResult EmployeeBenefitFundSettingList(int draw, int start, int length, long? organizationId, long? employeeStatus, long? calculationOn)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            bool isSuccess = false;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Organization";
                            break;
                        case "1":
                            orderBy = "Name";
                            break;
                        case "2":
                            orderBy = "EmploymentStatus";
                            break;
                        case "3":
                            orderBy = "CalculationOn";
                            break;
                        case "4":
                            orderBy = "EmployeeContribution";
                            break;
                        case "5":
                            orderBy = "EmployerContribution";
                            break;

                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                _userMenus = (List<UserMenu>)ViewBag.UserMenu;

                recordsTotal = _employeeBenefitsFundService.LoadEmployeeBenefitFundSettingCount(_userMenus, orderBy, orderDir, organizationId, employeeStatus, calculationOn);
                recordsFiltered = recordsTotal;
                IList<EmployeeBenefitsFundSetting> employeeBenefitsFundSettings = _employeeBenefitsFundService.LoadEmployeeBenefitFundSetting(_userMenus, start, length, orderBy, orderDir, organizationId, employeeStatus, calculationOn).ToList();
                foreach (var employeeBenefits in employeeBenefitsFundSettings)
                {
                    if (employeeBenefits.ClosingDate != null)
                    {
                        DateTime x;
                        DateTime.TryParse(employeeBenefits.ClosingDate.ToString(), out x);
                        employeeBenefits.ClosingDateString = x.ToString("MMM dd, yyyy");
                    }
                    else
                    {
                        employeeBenefits.ClosingDateString = "";
                    }
                    var str = new List<string>
                    {
                        employeeBenefits.Organization.ShortName,
                        employeeBenefits.Name,
                        _commonHelper.GetEmumIdToValue<MemberEmploymentStatus>(employeeBenefits.EmploymentStatus),
                        _commonHelper.GetEmumIdToValue<CalculationOn>(employeeBenefits.CalculationOn),
                        employeeBenefits.EmployeeContribution.ToString(),
                        employeeBenefits.EmployerContribution.ToString(),
                        employeeBenefits.EffectiveDate.ToString("MMM dd, yyyy"),
                        employeeBenefits.ClosingDateString,
                        LinkGenerator.GetGeneratedLink("EmployeeBenefitsFundDetail", "EmployeeBenefitsFundUpdate", "Delete", "EmployeeBenefits", employeeBenefits.Id, employeeBenefits.Name),
                        //LinkGenerator.GetEditLink("EmployeeBenefitsFundUpdate", "EmployeeBenefits", employeeBenefits.Id)
                    };
                    getData.Add(str);
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        #endregion

        #endregion

        #region Save

        public ActionResult EmployeeBenefitsFund()
        {
            Init();
            return View(new EmployeeBenefitsFundSetting());
        }

        [HttpPost]
        public ActionResult EmployeeBenefitsFund(EmployeeBenefitsFundSetting employeeBenefits)
        {
            try
            {
                #region Basic Validation

                if (employeeBenefits.OrganizationId <= 0)
                    throw new NullObjectException("Organization not found");

                if (String.IsNullOrEmpty(employeeBenefits.Name))
                    throw new NullObjectException("Funds name can't empty");

                var employmentStatus = employeeBenefits.EmploymentStatus;
                if (!Enum.IsDefined(typeof(MemberEmploymentStatus), employmentStatus))
                    throw new InvalidDataException("Invalid employment status");

                var calculationStatus = employeeBenefits.CalculationOn;
                if (!Enum.IsDefined(typeof(CalculationOn), calculationStatus))
                    throw new InvalidDataException("Invalid calculation on data");

                if (employeeBenefits.EmployeeContribution < 0)
                    throw new InvalidDataException("Invalid calculation on data");

                if (!employeeBenefits.EmployeeBenefitsFundSettingEntitlement.Any())
                    throw new InvalidDataException("One or more employee benefits fund entitlement required");

                if (employeeBenefits.ClosingDate < employeeBenefits.EffectiveDate)
                    throw new InvalidDataException("Effective date can not exceed closing date.");

                #endregion

                _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                _employeeBenefitsFundService.Save(employeeBenefits, _userMenus);
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (MessageException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
            Init();
            return Json(new Response(true, "EBF settings successfully saved"));
        }


        #endregion

        #region Update

        public ActionResult EmployeeBenefitsFundUpdate(long id)
        {
            try
            {
                InitForUpdate(id);
                return View(_employeeBenefitsFundSetting);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                return View("Index");
            }
        }

        [HttpPost]
        public ActionResult EmployeeBenefitsFundUpdate(long id, EmployeeBenefitsFundSetting employeeBenefitsFundSetting)
        {
            try
            {
                #region Basic Validation

                employeeBenefitsFundSetting.Organization = _organizationService.LoadById(employeeBenefitsFundSetting.OrganizationId);

                if (employeeBenefitsFundSetting.Organization == null)
                    throw new NullObjectException("Organization not found");

                if (String.IsNullOrEmpty(employeeBenefitsFundSetting.Name))
                    throw new NullObjectException("Funds name can't empty");

                //var employmentStatus = employeeBenefits.EmploymentStatus;
                //bool exist;
                //exist = Enum.IsDefined(typeof (MemberEmploymentStatus), employmentStatus);

                var employmentStatus = employeeBenefitsFundSetting.EmploymentStatus;
                if (!Enum.IsDefined(typeof(MemberEmploymentStatus), employmentStatus))
                    throw new InvalidDataException("Invalid employment status");

                var calculationStatus = employeeBenefitsFundSetting.CalculationOn;
                if (!Enum.IsDefined(typeof(CalculationOn), calculationStatus))
                    throw new InvalidDataException("Invalid calculation on data");

                if (employeeBenefitsFundSetting.EmployeeContribution < 0)
                    throw new InvalidDataException("Invalid calculation on data");

                if (!employeeBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement.Any())
                    throw new InvalidDataException("One or more employee benefits fund entitlement required");

                if (employeeBenefitsFundSetting.ClosingDate < employeeBenefitsFundSetting.EffectiveDate)
                    throw new System.IO.InvalidDataException("Effective date can not exceed closing date.");

                #endregion

                _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                _employeeBenefitsFundService.Update(_userMenus, id, employeeBenefitsFundSetting);
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }

            catch (MessageException ex)
            {
                return Json(new Response(false, ex.Message));
            }

            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            InitForUpdate(id);
            ViewBag.SuccessMessage = "EBF settings successfully updated.";
            return Json(new Response(true, "EBF settings successfully updated."));
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                bool isSuccess = _employeeBenefitsFundService.Delete(_userMenus, id);
                return Json(isSuccess ? new Response(true, "Employee benefit settings sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (MessageException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Employee benefit settings Delete Fail !"));
            }
        }

        #endregion

        #region Details

        public ActionResult EmployeeBenefitsFundDetail(long id)
        {
            try
            {
                InitForUpdate(id);
                return View(_employeeBenefitsFundSetting);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                return View("Index");
            }
        }

        #endregion

        #endregion

        #region Ajax Request

        [HttpGet]
        public ActionResult NewEmployerContribution(int index)
        {
            try
            {
                ViewBag.Index = index;
                return PartialView("_partial/_NewEmployerContribution");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Employee Fund Opening

        private void InitFundOpening()
        {
            _userMenus = (List<UserMenu>)ViewBag.UserMenu;
            var organizationList = _organizationService.LoadAuthorizedOrganization(_userMenus);
            ViewBag.Organization = new SelectList(organizationList, "Id", "ShortName");
            ViewBag.Branch = new SelectList(string.Empty, "Value", "Text");
            ViewBag.Campus = new SelectList(string.Empty, "Value", "Text");
            ViewBag.Designation = new SelectList(string.Empty, "Value", "Text");
            ViewBag.Department = new SelectList(string.Empty, "Value", "Text");
            ViewBag.EmploymentStatus = new SelectList(_commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(), "Key", "Value");
        }

        public ActionResult EmployeeBenefitFundOpening()
        {
            InitFundOpening();
            return View();
        }

        [HttpPost]
        public ActionResult EmployeeBenefitFundOpening(EmployeeBenefitFundSettingOpeningView openingFundSetting)
        {
            try
            {
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                IList<EmployeeBenefitFundBalanceView> ebfBalanceViews = new List<EmployeeBenefitFundBalanceView>();
                ebfBalanceViews = _employeeBenefitsFundService.LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(_userMenus, openingFundSetting.Organization, openingFundSetting.Branch, openingFundSetting.Campus, openingFundSetting.Department, openingFundSetting.Designation, openingFundSetting.EmploymentStatus, openingFundSetting.DepositDate);
                openingFundSetting.EmployeeBenefitFundBalanceViewList = ebfBalanceViews;
                return PartialView("_partial/_EmployeeBenefitFundBalance", ebfBalanceViews);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public ActionResult EmployeeBenefitFundOpeningBalance(EmployeeBenefitFundSettingOpeningView empBenefitFundSettingOpeningView)
        {
            try
            {
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;

                if (empBenefitFundSettingOpeningView.Organization <= 0)
                {
                    throw new NullObjectException("Null organization found");
                }
                _memberEbfBalanceService.SaveOrUpdate(_userMenus, empBenefitFundSettingOpeningView);
                return Json(new Response(true, "Success"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (MessageException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }


        #endregion

        #region Helper

        private void Init()
        {
            try
            {
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                var organizationList = _organizationService.LoadAuthorizedOrganization(_userMenus);
                ViewBag.Organization = new SelectList(organizationList, "Id", "ShortName");

                ViewBag.EmploymentStatus = new SelectList(_commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(), "Key", "Value");
                ViewBag.CalculationOn = new SelectList(_commonHelper.LoadEmumToDictionary<CalculationOn>(), "Key", "Value");
                _employeeBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement = new List<EmployeeBenefitsFundSettingEntitlement>();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

            }
        }

        private void InitForUpdate(long id)
        {
            try
            {
                _employeeBenefitsFundSetting = _employeeBenefitsFundService.GetEmployeeBenefitFundSetting(id);
               
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                var organizationList = _organizationService.LoadAuthorizedOrganization(_userMenus);
                _employeeBenefitsFundSetting.OrganizationId = _employeeBenefitsFundSetting.Organization.Id;
                ViewBag.Organization = new SelectList(organizationList, "Id", "ShortName", _employeeBenefitsFundSetting.Organization.Id);
                ViewBag.SelectedOrganization = _employeeBenefitsFundSetting.Organization != null ? _employeeBenefitsFundSetting.Organization.Id.ToString() : "";

                ViewBag.EmploymentStatus = new SelectList(_commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(), "Key", "Value");
                ViewBag.SelectedEmploymentStatus = _employeeBenefitsFundSetting.EmploymentStatus;

                ViewBag.CalculationOn = new SelectList(_commonHelper.LoadEmumToDictionary<CalculationOn>(), "Key", "Value");
                ViewBag.SelectedCalculationOn = _employeeBenefitsFundSetting.CalculationOn;

               

                if (_employeeBenefitsFundSetting != null)
                {
                    _employeeBenefitsFundSetting.CreateByText = _employeeBenefitsFundService.GetUserNameByAspNetUserId(_employeeBenefitsFundSetting.CreateBy);
                    _employeeBenefitsFundSetting.ModifyByText = _employeeBenefitsFundService.GetUserNameByAspNetUserId(_employeeBenefitsFundSetting.ModifyBy);
                }

                //ViewBag.SelectedEffectiveDate = _employeeBenefitsFundSetting.EffectiveDate.ToString("yyyy-MM-dd");
                //ViewBag.SelectedClosingDate = _employeeBenefitsFundSetting.ClosingDate.ToString();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

            }
        }

        #endregion
    }
}