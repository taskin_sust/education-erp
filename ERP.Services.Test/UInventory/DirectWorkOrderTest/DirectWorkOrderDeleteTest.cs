﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.UInventory.SpecificationTemplateTest;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.DirectWorkOrderTest
{
    public interface IDirectWorkOrderDeleteTest
    {
        #region Basic Test
        
        void Should_Delete_Successfully(); 
        
        #endregion

        #region Business Logic Test
        
        void Delete_Should_Throw_Dependency_Exception(); 
        
        #endregion
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "WorkOrder")]
    public class DirectWorkOrderDeleteTest : DirectWorkOrderBaseTest, IDirectWorkOrderDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            _directWorkOrderFactory.Create().WithBranch().WithSupplier().WithWorkOrderDetails().Persist();
            var workOrder = _workOrderService.GetWorkOrder(_directWorkOrderFactory.Object.Id);
            var success = _workOrderService.CancelWorkOrder(workOrder.Id);
            Assert.True(success);
        }

        #endregion

        #region Business Logic Test
        
        [Fact]
        public void Delete_Should_Throw_Dependency_Exception()
        {
            _directWorkOrderFactory.Create().WithBranch().WithSupplier().WithWorkOrderDetails().Persist();
            var workOrder = _workOrderService.GetWorkOrder(_directWorkOrderFactory.Object.Id);
            workOrder.GoodsReceiveList = new List<GoodsReceive>()
            {
                new GoodsReceive()
            };
            Assert.Throws<DependencyException>(() => _workOrderService.CancelWorkOrder(workOrder.Id));
        }

        #endregion

    }
}
