﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.AspNet.Identity;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using Remotion.Linq.Clauses;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class TeamMemberProfileController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ITeamMemberService _hrMemberService;
        private readonly ITrainingInfoService _hrTrainingService;
        private readonly IJobExperienceService _hrJobExperienceService;
        private readonly ICommonHelper _commonHelper;


        private readonly IChildrenInfoService _childrenInfoService;
        private readonly IAcademicInfoService _academicInfoService;
        private readonly ITeamMemberImagesMediaService _teamMemberImagesMediaService;
        public TeamMemberProfileController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                var sessionMedia = NHibernateMediaSessionFactory.OpenSession();
                _hrMemberService = new TeamMemberService(session);
                _hrTrainingService = new TrainingInfoService(session);
                _hrJobExperienceService = new JobExperienceService(session);
                _childrenInfoService = new ChildrenInfoService(session);
                _academicInfoService = new AcademicInfoService(session);
                _commonHelper = new CommonHelper();
                _teamMemberImagesMediaService = new TeamMemberImagesMediaService(sessionMedia);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Index/Manage Page
        public ActionResult Index()
        {
            try
            {
                var mainModel = new MemberProfileViewModel();
                ViewBag.ValidForm = false;

                var teamMemberEntity = _hrMemberService.GetCurrentMember();
                if (teamMemberEntity == null)
                {
                
                    ViewBag.ErrorMessage = "Account Information not found!";
                    return View();
                }
                ViewBag.TeamMemberPin = teamMemberEntity.Pin;
                ViewBag.ValidForm = true;
                object campus = new Campus();
                string department, designation, joindate, todate, officeShift;
                int empStatus, weekend, mentorPin;
                _hrMemberService.GetCurrentInfos(teamMemberEntity.Id, out campus, out department, out designation, out empStatus, out joindate, out todate, out officeShift, out weekend, out mentorPin);
                var campusEntity = (Campus)campus;

                mainModel.NickName = teamMemberEntity.Name.ToString(CultureInfo.CurrentCulture);
                mainModel.PersonalContact = teamMemberEntity.PersonalContact.ToString(CultureInfo.CurrentCulture);

                mainModel.Organization = (campusEntity != null) ? campusEntity.Branch.Organization.ShortName : "N/A";
                mainModel.Branch = (campusEntity != null) ? campusEntity.Branch.Name : "N/A";
                mainModel.Campus = (campusEntity != null) ? campusEntity.Name : "N/A";

                mainModel.Department = department;
                mainModel.Designation = designation;
                mainModel.EmploymentStatus = (empStatus != null) ? ((MemberEmploymentStatus)empStatus).ToString() : "N/A";
                mainModel.JoiningDate = joindate;
                mainModel.DateTo = todate;
                mainModel.OfficeShift = officeShift;
                mainModel.WeekendName = ((WeekDay)weekend).ToString();


                var mentorEntity = _hrMemberService.GetByPin(mentorPin);
                mainModel.MentorPin = (mentorEntity != null) ? mentorPin.ToString() : "N/A";
                mainModel.MentorName = (mentorEntity != null) ? mentorEntity.Name : "N/A";

                mainModel.Gender = ((Gender)Convert.ToInt32(teamMemberEntity.Gender)).ToString();

                if (teamMemberEntity.MemberOfficialDetails.Any())
                {
                    mainModel.OfficialContact = teamMemberEntity.MemberOfficialDetails[0].OfficialContact;
                    mainModel.OfficialEmail = teamMemberEntity.MemberOfficialDetails[0].OfficialEmail;
                }
                else
                {
                    mainModel.OfficialContact = "N/A";
                    mainModel.OfficialEmail = "N/A";
                }

                mainModel.FullNameInBangla = (!string.IsNullOrWhiteSpace(teamMemberEntity.FullNameBang)) ? teamMemberEntity.FullNameBang.ToString(CultureInfo.CurrentCulture) : "N/A";
                mainModel.FullNameInEnglish = (!string.IsNullOrWhiteSpace(teamMemberEntity.FullNameEng)) ? teamMemberEntity.FullNameEng.ToString(CultureInfo.CurrentCulture) : "N/A";
                mainModel.FacebookProfile = (!string.IsNullOrWhiteSpace(teamMemberEntity.Facebook)) ? teamMemberEntity.Facebook.ToString(CultureInfo.CurrentCulture) : "N/A";
                mainModel.PersonalEmail = (!string.IsNullOrWhiteSpace(teamMemberEntity.PersonalEmail)) ? teamMemberEntity.PersonalEmail.ToString(CultureInfo.CurrentCulture) : "N/A";
                mainModel.Religion = (teamMemberEntity.Religion != null) ? ((Religion)Convert.ToInt32(teamMemberEntity.Religion)).ToString() : "N/A";
                mainModel.BloodGroup = teamMemberEntity.BloodGroup != null ? ((BloodGroup)teamMemberEntity.BloodGroup.Value).GetDescription() : "";

                var nationality = teamMemberEntity.Nationality != null ? ((Nationality)Convert.ToInt32(teamMemberEntity.Nationality)).ToString() : "";
                mainModel.Nationality = nationality;

                mainModel.Nid = (!string.IsNullOrWhiteSpace(teamMemberEntity.NID)) ? teamMemberEntity.NID.ToString(CultureInfo.CurrentCulture) : "N/A";
                mainModel.PassportNo = (!string.IsNullOrWhiteSpace(teamMemberEntity.PassportNo)) ? teamMemberEntity.PassportNo.ToString(CultureInfo.CurrentCulture) : "N/A";
                mainModel.DobCertificate = (!string.IsNullOrWhiteSpace(teamMemberEntity.CertificateDOB.ToString())) ? ((DateTime)(teamMemberEntity.CertificateDOB)).ToString("yyyy-MM-dd") : "N/A";
                mainModel.DobActual = (!string.IsNullOrWhiteSpace(teamMemberEntity.ActualDOB.ToString())) ? ((DateTime)(teamMemberEntity.ActualDOB)).ToString("yyyy-MM-dd") : "N/A";

                #region Permanent And Present Address

                string cDivision = "", cDistrict = "", cThana = "", cPostOffice = "";
                Postoffice presentPostoffice = teamMemberEntity.PresentPostOffice;
                Thana presentThana = teamMemberEntity.PresentThana;
                if (presentPostoffice != null)
                {
                    cPostOffice = presentPostoffice.DisplayName;
                    if (presentPostoffice.District != null)
                    {
                        cDistrict = presentPostoffice.District.Name;
                        cDivision = (presentPostoffice.District.Division != null)? presentPostoffice.District.Division.Name: "";
                    }
                    //cPostOffice = teamMemberEntity.PresentPostOffice.Name ?? "";
                    //if (teamMemberEntity.PresentPostOffice.Thana != null)
                    //{
                    //    cThana = teamMemberEntity.PresentPostOffice.Thana.Name ?? "";
                    //    if (teamMemberEntity.PresentPostOffice.Thana.District != null)
                    //    {
                    //        cDistrict =
                    //            teamMemberEntity.PresentPostOffice.Thana.District.Name ?? "";
                    //        if (teamMemberEntity.PresentPostOffice.Thana.District.Division != null)
                    //        {
                    //            cDivision =
                    //                teamMemberEntity.PresentPostOffice.Thana.District.Division.Name ?? "";
                    //        }
                    //    }

                    //}
                }
                cThana = (presentThana != null) ? presentThana.Name : "";
                
                string pDivision = "", pDistrict = "", pThana = "", pPostOffice = "";

                Postoffice permanenetPostoffice = teamMemberEntity.PermanentPostOffice;
                Thana permanentThana = teamMemberEntity.PermanentThana;
                if (permanenetPostoffice != null)
                {
                    pPostOffice = permanenetPostoffice.DisplayName;
                    if (permanenetPostoffice.District != null)
                    {
                        pDistrict = permanenetPostoffice.District.Name;
                        pDivision = (permanenetPostoffice.District.Division != null) ? permanenetPostoffice.District.Division.Name : "";
                    }
                }
                pThana = (permanentThana != null) ? permanentThana.Name : "";
                //if (teamMemberEntity.PermanentPostOffice != null)
                //{
                //    pPostOffice = teamMemberEntity.PermanentPostOffice.Name ?? "N/A";
                //    if (teamMemberEntity.PermanentPostOffice.Thana != null)
                //    {
                //        pThana = teamMemberEntity.PermanentPostOffice.Thana.Name ?? "N/A";
                //        if (teamMemberEntity.PermanentPostOffice.Thana.District != null)
                //        {
                //            pDistrict =
                //                teamMemberEntity.PermanentPostOffice.Thana.District.Name ?? "N/A";
                //            if (teamMemberEntity.PermanentPostOffice.Thana.District.Division != null)
                //            {
                //                pDivision =
                //                    teamMemberEntity.PermanentPostOffice.Thana.District.Division.Name ?? "N/A";
                //            }
                //        }

                //    }
                //}
                mainModel.PermanentDivision = pDivision;
                mainModel.PermanentDistrict = pDistrict;
                mainModel.PermanentUpozilla = pThana;
                mainModel.PermanentPostOffice = pPostOffice;
                string permanentAddressText = "";
                permanentAddressText = !string.IsNullOrEmpty(teamMemberEntity.PermanentAddress) ? teamMemberEntity.PermanentAddress.ToString(CultureInfo.CurrentCulture) : "";
                permanentAddressText += !string.IsNullOrEmpty(pPostOffice)
                                        ? ", " + pPostOffice
                                        : "";
                permanentAddressText += !string.IsNullOrEmpty(pThana)
                                        ? ", " + pThana
                                        : "";
                permanentAddressText += !string.IsNullOrEmpty(pDistrict)
                                        ? ", " + pDistrict
                                        : "";
                permanentAddressText += !string.IsNullOrEmpty(pDivision)
                                        ? ", " + pDivision
                                        : "";
                mainModel.PermanentAddress = permanentAddressText;


                mainModel.PresentDivision = cDivision;
                mainModel.PresentDistrict = cDistrict;
                mainModel.PresentUpozilla = cThana;
                mainModel.PresentPostOffice = cPostOffice;

                string presentAddressText = "";
                presentAddressText = !string.IsNullOrEmpty(teamMemberEntity.PresentAddress) ? teamMemberEntity.PresentAddress.ToString(CultureInfo.CurrentCulture) : "";
                presentAddressText += !string.IsNullOrEmpty(cPostOffice)
                                        ? ", " + cPostOffice
                                        : "";
                presentAddressText += !string.IsNullOrEmpty(cThana)
                                        ? ", " + cThana
                                        : "";
                presentAddressText += !string.IsNullOrEmpty(cDistrict)
                                        ? ", " + cDistrict
                                        : "";
                presentAddressText += !string.IsNullOrEmpty(cDivision)
                                        ? ", " + cDivision
                                        : "";
                mainModel.PresentAddress = presentAddressText;
                #endregion
                //mainModel.PresentAddress = !string.IsNullOrEmpty(teamMemberEntity.PresentAddress) ? teamMemberEntity.PermanentAddress.ToString(CultureInfo.CurrentCulture) : "N/A";

                //Emargency Contact Information 
                mainModel.EmergencyContactName = !string.IsNullOrWhiteSpace(teamMemberEntity.EmargencyContactName) ? teamMemberEntity.EmargencyContactName.ToString(CultureInfo.CurrentCulture) : "N/A";
                mainModel.EmergencyPersonCellNo = !string.IsNullOrWhiteSpace(teamMemberEntity.EmargencyContactCellNo) ? teamMemberEntity.EmargencyContactCellNo.ToString(CultureInfo.CurrentCulture) : "N/A";
                mainModel.RelationsWithEmergencyPerson = !string.IsNullOrWhiteSpace(teamMemberEntity.EmargencyContactRelation) ? teamMemberEntity.EmargencyContactRelation.ToString(CultureInfo.CurrentCulture) : "N/A";

                //Parental Information
                mainModel.FathersName = (!string.IsNullOrWhiteSpace(teamMemberEntity.FatherNameEng)) ? teamMemberEntity.FatherNameEng.ToString(CultureInfo.CurrentCulture) : "";
                mainModel.FathersNameInBangla = (!string.IsNullOrWhiteSpace(teamMemberEntity.FatherNameBang)) ? teamMemberEntity.FatherNameBang.ToString(CultureInfo.CurrentCulture) : "";
                mainModel.FathersContactNumber = (!string.IsNullOrWhiteSpace(teamMemberEntity.FatherContactNo)) ? teamMemberEntity.FatherContactNo.ToString(CultureInfo.CurrentCulture) : "";
                mainModel.MothersName = (!string.IsNullOrWhiteSpace(teamMemberEntity.MotherNameEng)) ? teamMemberEntity.MotherNameEng.ToString(CultureInfo.CurrentCulture) : "";
                mainModel.MothersNameInBangla = (!string.IsNullOrWhiteSpace(teamMemberEntity.MotherNameBang)) ? teamMemberEntity.MotherNameBang.ToString(CultureInfo.CurrentCulture) : "";
                mainModel.MothersContactNumber = (!string.IsNullOrWhiteSpace(teamMemberEntity.MotherContactNo)) ? teamMemberEntity.MotherContactNo.ToString(CultureInfo.CurrentCulture) : "";

                //Marital Information
                var maritalInfoEntity = teamMemberEntity.MaritalInfo.OrderByDescending(x => x.Id).FirstOrDefault();
                if (maritalInfoEntity != null)
                {
                    mainModel.MaritalStatus = ((MaritalType)Convert.ToInt32(maritalInfoEntity.MaritalStatus)).ToString();
                    mainModel.MarriageDate = maritalInfoEntity.MarriageDate != null ? Convert.ToDateTime(maritalInfoEntity.MarriageDate).ToShortDateString() : "N/A";
                    mainModel.SpouseName = !string.IsNullOrEmpty(maritalInfoEntity.SpouseName) ? maritalInfoEntity.SpouseName.ToString(CultureInfo.CurrentCulture) : "N/A";
                    mainModel.SpouseContactNumber = !string.IsNullOrEmpty(maritalInfoEntity.SpouseContatctNo) ? maritalInfoEntity.SpouseContatctNo.ToString(CultureInfo.CurrentCulture) : "N/A";

                }

                //Nominee Contact Number
                string nomineeName, nomineeRelation, nomineeContactNumber;
                _hrMemberService.GetCurrentTeamMemberNomineeInfo(teamMemberEntity.Id, out nomineeName, out nomineeRelation, out nomineeContactNumber);
                mainModel.NomineeName = nomineeName;
                mainModel.NomineeRelation = nomineeRelation;
                mainModel.NomineeContactNumber = nomineeContactNumber;

                //Children Info
                var childrenInfo = _childrenInfoService.GetTeamMemberChildrenInfo(teamMemberEntity.Id);
                ViewBag.ChildrenInfo = childrenInfo;

                //Team Member Image
                var teamMemberInage = _teamMemberImagesMediaService.GetTeamMemberImageMediaByRefId(teamMemberEntity.Id);
                ViewBag.TeamMemberImage = teamMemberInage;

                //Member Nominee Image
                var memberNomineeImage = _teamMemberImagesMediaService.GetMemberNomineeMediaImageByRefId(teamMemberEntity.Id);
                ViewBag.MemberNomineeImage = memberNomineeImage;

                //Educational Information
                ViewBag.HasAcademicInfo = false;
                var academicInfo = _academicInfoService.LoadMemberAccademicInfo(teamMemberEntity.Id);
                if (academicInfo != null && academicInfo.Count > 0)
                {
                    ViewBag.HasAcademicInfo = true;
                    ViewBag.AcademicInfo = academicInfo;
                }
                //Training Information
                ViewBag.HasTrainingInfo = false;
                var trainingInfo = _hrTrainingService.LoadMemberTraningInfo(teamMemberEntity.Id);
                if (trainingInfo != null && trainingInfo.Count > 0)
                {
                    ViewBag.HasTrainingInfo = true;
                    ViewBag.TrainingInfo = trainingInfo;
                }

                //Professional Qualifications Information
                ViewBag.HasProfessionalQInfo = false;
                var professionalQInfo = _hrTrainingService.LoadMemberProfessionalQualificationsInfo(teamMemberEntity.Id);
                if (professionalQInfo != null && professionalQInfo.Count > 0)
                {
                    ViewBag.HasProfessionalQInfo = true;
                    ViewBag.ProfessionalQInfo = professionalQInfo;
                }
                //Job Experience Information
                ViewBag.HasJobExperienceInfo = false;
                var jobExperienceInfo = _hrJobExperienceService.LoadMemberJobExperienceInfo(teamMemberEntity.Id);
                if (jobExperienceInfo != null && jobExperienceInfo.Count > 0)
                {
                    ViewBag.HasJobExperienceInfo = true;
                    ViewBag.JobExperienceInfo = jobExperienceInfo;
                }

                //Specialization Information
                ViewBag.IsSpecialization = false;
                if (!string.IsNullOrEmpty(teamMemberEntity.Specialization) || !string.IsNullOrEmpty(teamMemberEntity.ExtracurricularActivities))
                {
                    ViewBag.IsSpecialization = true;
                    ViewBag.SpecializationInformation = teamMemberEntity.Specialization;
                    ViewBag.ExtraCularActivities = teamMemberEntity.ExtracurricularActivities;
                }
                return View(mainModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.ToString();
                return View();
            }
        }
        #endregion

        #region Operational Function

        #region Save Operation
        #endregion

        #region Report/Query


        #endregion

        #endregion

        #region Ajax functions
        #endregion

        #region helper
        #endregion
    }
}