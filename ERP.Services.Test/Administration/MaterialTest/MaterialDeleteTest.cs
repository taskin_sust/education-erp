﻿using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MaterialTest
{
    public interface IMaterialDeleteTest
    {
        #region Basic test
        [Fact]
        void Should_Delete_Successfully();
        
        #endregion

        #region Business Logic Test
        [Fact]
        void Delete_Should_Check_Dependency();
        #endregion
    }


    [Trait("Area", "Administration")]
    [Trait("Service", "Material")]
    public class MaterialDeleteTest : MaterialBaseTest, IMaterialDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            var materialFactory =
                _materialFactory.Create().Persist();
            var success = _materialService.IsDelete(materialFactory.Object);
            Material material = _materialService.GetMaterial(materialFactory.Object.Id);
            Assert.True(success);
            Assert.Null(material);
        }
        
        #endregion

        #region Business Logic Test
        [Fact]
        public void Delete_Should_Check_Dependency()
        {
            var materialFactory =
                _materialFactory.Create().Persist();
            var material = materialFactory.Object;
            material.StudentPrograms = new List<StudentProgram>() { new StudentProgram() };
            Assert.Throws<DependencyException>(() => _materialService.IsDelete(material));
        }
        #endregion
    }
}
