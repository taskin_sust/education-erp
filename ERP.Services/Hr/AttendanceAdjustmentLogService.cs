﻿using System;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface IAttendanceAdjustmentLogService : IBaseService
    {
        #region Operational Function

        void Save(AttendanceAdjustmentLog attendanceAdjustmentLog);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class AttendanceAdjustmentLogService  : BaseService, IAttendanceAdjustmentLogService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        public AttendanceAdjustmentLogService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public void Save(AttendanceAdjustmentLog attendanceAdjustmentLog)
        {
            try
            {
                Session.Save(attendanceAdjustmentLog);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
       
        #endregion

        #region Others Function
       
       
        #endregion

        #region Helper function

       

        #endregion
    }
}
