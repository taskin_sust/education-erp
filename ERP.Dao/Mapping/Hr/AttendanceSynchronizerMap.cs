using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{
    public partial class AttendanceSynchronizerMap : BaseClassMap<AttendanceSynchronizer, long>
    {

        public AttendanceSynchronizerMap()
        {
            Table("HR_AttendanceSynchronizer");
            LazyLoad();
            Map(x => x.Rank).Column("Rank");
            Map(x => x.Name).Column("Name");
            Map(x => x.SynchronizerKey).Column("SynchronizerKey").Unique();
            Map(x => x.DataCallingInterval).Column("DataCallingInterval").Not.Nullable();
            Map(x => x.OperatorContact).Column("OperatorContact");
            Map(x => x.IpWhiteList).Column("IpWhiteList");
            Map(x => x.LastMemberInfoUpdateTime).Column("LastMemberInfoUpdateTime");

            References(x => x.Campus).Column("CampusId");

            HasMany(x => x.AttendanceDevice).KeyColumn("AttendanceSynchronizerId");
            HasMany(x => x.AttendanceDeviceActivationLog).KeyColumn("AttendanceSynchronizerId");
        }
    }
}
