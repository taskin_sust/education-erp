using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Base;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{
    public class ItemFactory : ObjectFactoryBase<Item>
    {

        private readonly IItemService _itemService;
        internal readonly ItemGroupFactory _itemGroupFactory;
        internal readonly OrganizationFactory _organizationFactory;
        internal readonly SpecificationTemplateFactory _specificationTemplateFactory;
        internal readonly TestBaseService<SpecificationCriteria> _testBaseService; 
        public ItemFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
        {
            _itemService = new ItemService(session);
            _itemGroupFactory = new ItemGroupFactory(this, _session);
            _organizationFactory = new OrganizationFactory(this, _session);
            _specificationTemplateFactory = new SpecificationTemplateFactory(this,_session);
            _testBaseService = new TestBaseService<SpecificationCriteria>(_session);
        }

        #region Create

        public ItemFactory Create()
        {
            Object = new Item()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString(),                                                                                                                                                                              
                ItemType = (int)ItemType.CommonItem,
                ItemUnit = (int)ItemUnit.Kg,
                CostBearer = (int)CostBearer.Corporate,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Item.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public ItemFactory Create(ItemType itemType)
        {
            Object = new Item()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString(),
                ItemType = (int)itemType,
                ItemUnit = (int)ItemUnit.Kg,
                CostBearer = (int)CostBearer.Corporate,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Item.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public ItemFactory CreateWith(long id, string name, int itemType, int itemUnit, int costBearer)
        {
            Object = new Item()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                Name = name,
                ItemType = itemType,
                ItemUnit = itemUnit,
                CostBearer = costBearer,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Item.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }
        
        public ItemFactory CreateMore(int num)
        {
            CreateItemList(num);
            return this;
        }

        public ItemFactory CreateMore(int num,List<Organization> orgList)
        {
            _specificationTemplateFactory.Create().WithSpecificationCriteria().Persist();
            _itemGroupFactory.Create().Persist();
            var list = new List<Item>();
            decimal j = num / 2;
            foreach (var organization in orgList)
            {
                for (int i = 0; i < num; i++)
                {
                    var itemObj = new Item()
                    {
                        Id = 0,
                        Name = Name + "_" + i,
                        BusinessId = GetPcUserName(),
                        ItemType = i > Math.Floor(j) ? (int)ItemType.CommonItem : (int)ItemType.ProgramItem,
                        ItemUnit = (int)ItemUnit.Kg,
                        CostBearer = (int)CostBearer.Corporate,
                        Rank = i,
                        CreateBy = 1,
                        ModifyBy = 1,
                        Status = Item.EntityStatus.Active,
                        Organization = organization,
                        ItemGroup = _itemGroupFactory.Object,
                        SpecificationTemplate = _specificationTemplateFactory.Object
                    };
                    list.Add(itemObj);
                }    
            }
            ObjectList.AddRange(list);
            return this;
        }

        public ItemFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        public ItemFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public ItemFactory WithItemGroup()
        {
            _itemGroupFactory.Create().Persist();
            Object.ItemGroup = _itemGroupFactory.Object;
            return this;
        }
        
        public ItemFactory WithItemGroup(ItemGroup itemGroup)
        {
            Object.ItemGroup = itemGroup;
            return this;
        }

        public ItemFactory WithSpecifationTemplate()
        {
            _specificationTemplateFactory.Create().WithSpecificationCriteria().Persist();
            Object.SpecificationTemplate = _specificationTemplateFactory.Object;
            return this;
        }

        public ItemFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _itemService.SaveOrUpdate(new List<Item>() { x }); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x => { if (x.Id == 0)_itemService.SaveOrUpdate(new List<Item>() { x }); });
            }
            return this;
        }

        #endregion
        
        #region other

        private void CreateItemList(int numOfItem)
        {
            ListStartIndex = ObjectList.Count;
            _organizationFactory.Create().Persist();
            _itemGroupFactory.Create().Persist();
            var list = new List<Item>();
            for (int i = 0; i < numOfItem; i++)
            {
                var itemObj = new Item()
                {
                    Id = 0,
                    Name = Name + "_" + i,
                    BusinessId = GetPcUserName(),
                    ItemType = (int)ItemType.CommonItem,
                    ItemUnit = (int)ItemUnit.Kg,
                    CostBearer = (int)CostBearer.Corporate,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Item.EntityStatus.Active,
                    Organization = _organizationFactory.Object,
                    ItemGroup = _itemGroupFactory.Object
                };
                list.Add(itemObj);
            }
            ObjectList.AddRange(list);
            return;
        }

        #endregion

        #region CleanUp

        public ItemFactory CleanUp()
        {
            ItemFactory itemFactory = this;
            DeleteAll();
            _organizationFactory.Cleanup();
            _itemGroupFactory.CleanUp();
            if (itemFactory.Object != null && itemFactory.Object.SpecificationTemplate != null)
                _testBaseService.DeleteSpecificationCriteria(itemFactory.Object.SpecificationTemplate.SpecificationCriteriaList);
            _specificationTemplateFactory.CleanUp();
            return this;
        }

        #endregion

    }
}