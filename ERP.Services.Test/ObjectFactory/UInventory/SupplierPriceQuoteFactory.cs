using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class SupplierPriceQuoteFactory : ObjectFactoryBase<SupplierPriceQuote>
   {

       private readonly ISupplierPriceQuoteService _supplierPriceQuoteService;
       internal SupplierFactory _supplierFactory;
       public SupplierPriceQuoteFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
       {
           _supplierPriceQuoteService = new SupplierPriceQuoteService(session);
           _supplierFactory = new SupplierFactory(this,session);
       }

       #region Create 

       public SupplierPriceQuoteFactory Create()
       {

           Object = new SupplierPriceQuote()
           {
               Id = 0,
               BusinessId = GetPcUserName(),
               CreateBy = 1,
               ModifyBy = 1,
               PriceQuoteStatus = (int)PriceQouteStatus.Submitted,
               Rank = 100,
               QuotedUnitCost = 2,
               TotalCost = 100,
           };
           SingleObjectList.Add(Object);
           return this;

       }

       public SupplierPriceQuoteFactory CreateWith(long id,PriceQouteStatus priceQouteStatus,int quotedUnitCost,int totalCost)
       {

          Object = new SupplierPriceQuote()
          {
              Id = id,
              BusinessId = GetPcUserName(),
              CreateBy = 1,
              ModifyBy = 1,
              PriceQuoteStatus = (int)PriceQouteStatus.Submitted,
              Rank = 100,
              QuotedUnitCost = quotedUnitCost,
              TotalCost = totalCost
          };
          SingleObjectList.Add(Object);
          return this;

       }

       //public SupplierPriceQuoteFactory WithSupplier()
       //{
       //    _supplierFactory.Create().CreateWithSupplierBankDetails(1).Persist();
       //    Object.Supplier = _supplierFactory.Object;
       //    return this;
       //}

       public SupplierPriceQuoteFactory WithSupplier(Supplier supplier)
       {
           Object.Supplier = supplier;
           return this;
       }
       public SupplierPriceQuoteFactory WithQuotation(Quotation quotation)
       {
           Object.Quotation = quotation;
           return this;
       }
       public SupplierPriceQuoteFactory Persist()       
       {

          if (SingleObjectList != null && SingleObjectList.Count > 0)
          {

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _supplierPriceQuoteService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0)
          {

              ObjectList.ForEach(x =>{if (x.Id == 0)_supplierPriceQuoteService.SaveOrUpdate(x);});
          }
          return this;
       }

       public SupplierPriceQuoteFactory CreateMore(int num)
       {
           for (int i = 0; i < num; i++)
           {
               var obj = new SupplierPriceQuote()
               {
                   Id = 0,
                   BusinessId = GetPcUserName(),
                   CreateBy = 1,
                   ModifyBy = 1,
                   PriceQuoteStatus = (int)PriceQouteStatus.Submitted,
                   Rank = 100,
                   QuotedUnitCost = 2,
                   TotalCost = 100,
               };
               ObjectList.Add(obj);
           }
            return this;
       }

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public SupplierPriceQuoteFactory CleanUp()
       {
          DeleteAll();
          return this;
       }

       #endregion

    }
}
