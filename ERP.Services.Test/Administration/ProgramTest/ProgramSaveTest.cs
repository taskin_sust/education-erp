﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ProgramTest
{
    interface IProgramSaveTest
    {
        #region Basic Test
        void Save_Without_Data_Should_Retrun_NullException();
        void Save_Should_Be_Success();
        void Save_Should_Be_Fail_Without_Organization();
        void Save_Should_Be_Fail_Without_Program_Name();
        void Save_Should_Be_Fail_Without_Program_ShortName();
        void Save_Should_Be_Fail_Without_Program_Code();
        void Save_Should_Be_Fail_Without_Program_Type();
        void Save_Should_Be_Fail_String_Overflow_Program_Name();
        void Save_Should_Be_Fail_String_Overflow_Program_ShortName();
        void Save_Should_Be_Fail_String_Overflow_Program_Code();
        void Save_Should_Be_Fail_Passing_String_Instead_of_Int_Program_Code();

        #endregion

        #region Business Logic Test
        void Save_Should_Be_Fail_For_Duplicate_Program_Name();
        void Save_Should_Be_Fail_For_Duplicate_Program_ShortName();
        void Save_Should_Be_Fail_For_Duplicate_Program_Code();
        void Save_Should_Be_Fail_For_Invalid_ProgramType();

        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Program")]
    public class ProgramSaveTest : ProgramBaseTest, IProgramSaveTest
    {
        #region Basic Test

        [Fact]
        public void Save_Without_Data_Should_Retrun_NullException()
        {
            Assert.Throws<NullObjectException>(() => _programService.ProgramSaveOrUpdate(null));
        }

        [Fact]
        public void Save_Should_Be_Success()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            Assert.NotNull(program);
            Assert.True(program.Id > 0);
        }

        [Fact]
        public void Save_Should_Be_Fail_Without_Organization()
        {
            var programFactory = _programFactory.Create().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram);
            Assert.Throws<InvalidDataException>(() => programFactory.Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_Without_Program_Name()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(" ", _shortName, _programFactory.GetRandomNumber(), _paidProgram);
            Assert.Throws<InvalidDataException>(() => programFactory.Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_Without_Program_ShortName()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, " ", _programFactory.GetRandomNumber(), _paidProgram);
            Assert.Throws<InvalidDataException>(() => programFactory.Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_Without_Program_Code()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, " ", _paidProgram);
            Assert.Throws<InvalidDataException>(() => programFactory.Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_Without_Program_Type()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), -1);
            Assert.Throws<InvalidDataException>(() => programFactory.Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_String_Overflow_Program_Name()
        {
            string overflowProgramName = "I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my  project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to  in my project, it had a drtretre v wehyroiyhweioroweroi eoiroiewr eiwroiweiorweio oiewrioweyhior ioewioroweyhrio weqrioioewn asi9uyer90qw32e uw90quer90wq09ed 09awu90duew09qrw0 we09dwqy90e40qwyud 09qwu3094uejhdkasjpldj uqweuqwiue q09we qwey09qwe0qw   wejrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr 0qwu38209euqwoaujdkaskdhaskdoias asojdfoiasoiashiduasi0dui0asudiasudi0ausd0iu";
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(overflowProgramName, _shortName, _programFactory.GetRandomNumber(), _unpaidProgram);
            Assert.Throws<InvalidDataException>(() => programFactory.Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_String_Overflow_Program_ShortName()
        {
            string overflowShortName = "I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my  project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to  in my project, it had a drtretre v wehyroiyhweioroweroi eoiroiewr eiwroiweiorweio oiewrioweyhior ioewioroweyhrio weqrioioewn asi9uyer90qw32e uw90quer90wq09ed 09awu90duew09qrw0 we09dwqy90e40qwyud 09qwu3094uejhdkasjpldj uqweuqwiue q09we qwey09qwe0qw   wejrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr 0qwu38209euqwoaujdkaskdhaskdoias asojdfoiasoiashiduasi0dui0asudiasudi0ausd0iu";
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, overflowShortName, _programFactory.GetRandomNumber(), _unpaidProgram);
            Assert.Throws<InvalidDataException>(() => programFactory.Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_String_Overflow_Program_Code()
        {
            string overflowCode = "I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my  project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to  in my project, it had a drtretre v wehyroiyhweioroweroi eoiroiewr eiwroiweiorweio oiewrioweyhior ioewioroweyhrio weqrioioewn asi9uyer90qw32e uw90quer90wq09ed 09awu90duew09qrw0 we09dwqy90e40qwyud 09qwu3094uejhdkasjpldj uqweuqwiue q09we qwey09qwe0qw   wejrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr 0qwu38209euqwoaujdkaskdhaskdoias asojdfoiasoiashiduasi0dui0asudiasudi0ausd0iu";
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, overflowCode, _programFactory.GetRandomNumber(), _unpaidProgram);
            Assert.Throws<InvalidDataException>(() => programFactory.Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_Passing_String_Instead_of_Int_Program_Code()
        {
            string code = "SS";
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, code, _unpaidProgram);
            Assert.Throws<InvalidDataException>(() => programFactory.Persist());
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Save_Should_Be_Fail_For_Duplicate_Program_Name()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            var organization = programFactory.Object.Organization;
            var programFactoryForDuplication = _programFactory.Create().WithOrganization(organization).WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram);
            Assert.Throws<DuplicateEntryException>(() => programFactoryForDuplication.Persist());
        }
        [Fact]
        public void Save_Should_Be_Fail_For_Duplicate_Program_ShortName()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            var organization = programFactory.Object.Organization;
            var programFactoryForDuplication = _programFactory.Create().WithOrganization(organization).WithProperties(_programName + " DIF", _shortName, _programFactory.GetRandomNumber(), _paidProgram);
            Assert.Throws<DuplicateEntryException>(() => programFactoryForDuplication.Persist());
        }
        [Fact]
        public void Save_Should_Be_Fail_For_Duplicate_Program_Code()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            var organization = programFactory.Object.Organization;
            var programFactoryForDuplication = _programFactory.Create().WithOrganization(organization).WithProperties(_programName + " DIF", _shortName + " DIF", _programFactory.GetRandomNumber(), _paidProgram);
            Assert.Throws<DuplicateEntryException>(() => programFactoryForDuplication.Persist());
        }
        [Fact]
        public void Save_Should_Be_Fail_For_Invalid_ProgramType()
        {
            Assert.Throws<InvalidDataException>(() => _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), -1).Persist());

        }


        #endregion
    }
}
