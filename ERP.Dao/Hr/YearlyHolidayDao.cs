﻿using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IYearlyHolidayDao : IBaseDao<YearlyHoliday, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        YearlyHoliday GetYearlyHoliday(string holidayName, long organizationId, bool loadActive = true);
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        void Delete(long id);

        #endregion

        #region Helper Function

        #endregion




    }
    public class YearlyHolidayDao : BaseDao<YearlyHoliday, long>, IYearlyHolidayDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        public void Delete(long id)
        {
            var yearlyHoliday =
                Session.QueryOver<YearlyHoliday>().Where(x => x.Id == id).SingleOrDefault<YearlyHoliday>();
            Session.Delete(yearlyHoliday);
        }
        #endregion

        #region Single Instances Loading Function

        public YearlyHoliday GetYearlyHoliday(string holidayName, long organizationId,bool loadActive=true)
        {
            YearlyHoliday yearlyHoliday;
            if (!loadActive)
            {
                yearlyHoliday=Session.QueryOver<YearlyHoliday>()
                    .Where(x => x.Name == holidayName && x.Organization.Id == organizationId)
                    .SingleOrDefault<YearlyHoliday>();
                return yearlyHoliday;
            }
            yearlyHoliday= Session.QueryOver<YearlyHoliday>()
                    .Where(x => x.Name == holidayName && x.Organization.Id == organizationId && x.Status == YearlyHoliday.EntityStatus.Active)
                    .SingleOrDefault<YearlyHoliday>();
            return yearlyHoliday;
        }
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion
    }

}
