﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Areas.Student.Models
{
    public class StudentInfoSearch
    {

        #region Passing Data View To Controller
        [Required(ErrorMessage = "You need to select organization")]
        public long OrganizationId { get; set; }
        
        [Required(ErrorMessage = "You need to select program")]
        public long ProgramId { get; set; }

        [Required(ErrorMessage = "You need to select session")]
        public long SessionId { get; set; }


        //[Required(ErrorMessage = "You need to give search keyword")]
        public string SearchKey { get; set; }
        //[MinimumSelect("InformationViewList", ErrorMessage = "Please select one or more searching fields")]
        public string[] InformationViewList { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; } 

        #endregion

        public int SelectedVersion { get; set; }
        public int SelectedGender { get; set; }

        //[Required(ErrorMessage = "You need to select religion")]
        public int[] SelectedReligion { get; set; }

      
        [Required(ErrorMessage = "You need to select branch")]
        public long[] SelectedBranch { get; set; }

        [Required(ErrorMessage = "You need to select campus")]
        public long[] SelectedCampus { get; set; }

        [Required(ErrorMessage = "You need to select batch day")]
        public string[] SelectedBatchDays { get; set; }

        [Required(ErrorMessage = "You need to select batch time")]
        public string[] SelectedBatchTime { get; set; }

        public int SelectedStatus { get; set; }  

        [Required(ErrorMessage = "You need to select batch")]
        public long[] SelectedBatch { get; set; }

        [Required(ErrorMessage = "You need to select course")]
        public long[] SelectedCourse { get; set; }
        [Required(ErrorMessage = "You need to select Version of study")]
        public List<int> SelectedVersions { get; set; }
        [Required(ErrorMessage = "You need to select gender")]
        public List<int> SelectedGenders { get; set; }

    }
}