using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class BankHistoryMap : BaseClassMap<BankHistory,long> {

        public BankHistoryMap()
        {
			Table("HR_BankHistory");
			LazyLoad();
            References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.BankBranch).Column("BankBranchId");
            Map(x => x.Name).Column("Name");
            Map(x => x.AccountNumber).Column("AccountNumber");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
        }
    }
}
