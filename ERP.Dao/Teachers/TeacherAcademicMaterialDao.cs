﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Teachers
{
    public interface ITeacherAcademicMaterialDao : IBaseDao<TeacherAcademicMaterials, long>
    {
        #region Single Instance loading function
        TeacherAcademicMaterials GetTeacherAcademicMaterial();
        #endregion
    }

    public class TeacherAcademicMaterialDao:BaseDao<TeacherAcademicMaterials,long>,ITeacherAcademicMaterialDao
    {
        #region Single Instance Loading function
        public TeacherAcademicMaterials GetTeacherAcademicMaterial()
        {
            return Session.QueryOver<TeacherAcademicMaterials>()
                     .Where(x => x.Status == TeacherAcademicMaterials.EntityStatus.Active).OrderBy(tx => tx.SerialNumber).Desc.Take(1)
                     .SingleOrDefault<TeacherAcademicMaterials>();

        }
        #endregion

    }
   
}
