using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface IMemberLoanRefundDao : IBaseDao<MemberLoanRefund, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        MemberLoanAmountForRefundView GetLoanBalance(long teamMemberId);
        //MemberLoanRefund GetMemberLoanRefund(long id);
        // LoanRefundReceipt GetMemberLoanRefund(long id);

        #endregion

        #region List Loading Function

        //IList<MemberLoanRefund> LoadMemberLoanRefund();
        IList<MemberLoanRefund> LoadMemberLoanRefund(int start, int length, string orderBy, string orderDir, int? pin, string name, string receiptNo);

        #endregion

        #region Others Function

        int LoadMemberLoanRefundCount(int start, int length, string orderBy, string orderDir, int? pin, string name, string receiptNo);

        #endregion

        #region Helper Function

        long GetMaxReceiptNo(long organizationId);

        #endregion
    }

    public class MemberLoanRefundDao : BaseDao<MemberLoanRefund, long>, IMemberLoanRefundDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public MemberLoanAmountForRefundView GetLoanBalance(long teamMemberId)
        {
            //            string query2 = string.Format(@"
            //                       SELECT   TOP 1 la.Id AS LoanApplicationId,
            //                                la.TeamMemberId ,
            //                                la.LoanStatus ,
            //                                ( SUM(ml.LoanApprovedAmount) - SUM(ISNULL(lr.RefundAmount, 0)) ) AS LoanBalance 
            //                        FROM    HR_MemberLoanApplication AS la
            //                                INNER JOIN HR_MemberLoan ml ON ml.MemberLoanApplicationId = la.Id
            //                                LEFT JOIN HR_MemberLoanRefund lr ON lr.TeamMemberId = la.TeamMemberId
            //                        WHERE   la.TeamMemberId = {0}
            //                        GROUP BY la.TeamMemberId ,
            //                                la.Id ,
            //                                la.TeamMemberId ,
            //                                la.LoanStatus ,
            //                                ml.LoanApprovedAmount ,
            //                                lr.RefundAmount,
            //                                lr.CreationDate
            //                                ORDER By lr.CreationDate ASC"
            //                , teamMemberId);

            string query = string.Format(@"
                                    SELECT  a.* ,b.RefundAmount ,( a.LoanApprovedAmount - ( ISNULL(b.RefundAmount, 0) ) ) AS LoanBalance
                                    FROM    ( SELECT    SUM(ml.LoanApprovedAmount) AS LoanApprovedAmount ,
                                            la.TeamMemberId AS TeamMemberId
                                            FROM      HR_MemberLoanApplication la
                                            INNER JOIN HR_MemberLoan ml ON ml.MemberLoanApplicationId = la.Id
                                            WHERE     la.TeamMemberId = {0}
                                            GROUP BY  la.TeamMemberId
                                    ) AS a
                                    LEFT JOIN ( SELECT  SUM(RefundAmount) AS RefundAmount,lr.TeamMemberId AS TeamMemberId 
                                                FROM HR_MemberLoanRefund lr
                                                WHERE   lr.TeamMemberId = {0}
                                                GROUP BY lr.TeamMemberId
                                    ) AS b ON a.TeamMemberId = b.TeamMemberId", teamMemberId);

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<MemberLoanAmountForRefundView>());
            MemberLoanAmountForRefundView memberLoanAmountForRefundView = iQuery.UniqueResult<MemberLoanAmountForRefundView>();
            return memberLoanAmountForRefundView;

        }
        //        public LoanRefundReceipt GetMemberLoanRefund(long id)
        //        {
        //            string query = String.Format(@"SELECT  lr.Id ,
        //                                        o.Name AS OrganizationName,
        //                                        lr.RefundAmount ,
        //                                        lr.Remarks ,
        //                                        lr.RefundDate ,
        //                                        lr.ReceiptNo ,
        //                                        lr.MemberPin ,
        //                                        lr.MemberName ,
        //                                        lr.ReceivedById ,
        //                                        au.FullName AS ReceivedBy ,
        //                                        lr.LoanAmount ,
        //                                        lr.DueAmount
        //                                FROM    HR_MemberLoanRefund lr
        //                                        INNER JOIN AspNetUsers au ON au.Id = lr.ReceivedById
        //                                        INNER JOIN Organization o ON o.Id=lr.SalaryOrganizationId
        //                                        WHERE lr.Id= {0}", id);
        //            IQuery iQuery = Session.CreateSQLQuery(query);
        //            iQuery.SetResultTransformer(Transformers.AliasToBean<LoanRefundReceipt>());
        //            LoanRefundReceipt memberLoanAmountForRefundView = iQuery.UniqueResult<LoanRefundReceipt>();
        //            return memberLoanAmountForRefundView;
        //        }

        #endregion

        #region List Loading Function


        public IList<MemberLoanRefund> LoadMemberLoanRefund()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Others Function


        #endregion

        #region Helper Function

        public long GetMaxReceiptNo(long organizationId)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(MemberLoanRefund));
            criteria.Add(Restrictions.Eq("Status", MemberLoanRefund.EntityStatus.Active));
            criteria.CreateCriteria("SalaryOrganization", "org").Add(Restrictions.Eq("org.Status", Organization.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("org.Id", organizationId));
            criteria.SetProjection(Projections.Max(Projections.Cast(NHibernateUtil.Int64, Projections.Property("ReceiptNo"))));
            //criteria.SetProjection(Projections.Max("ReceiptNo"));

            if (criteria.UniqueResult() != null)
            {
                var returnValue = (long)criteria.UniqueResult();
                return Convert.ToInt64(returnValue + 1);
            }
            else
            {
                return Convert.ToInt64(1);
            }

        }

        private ICriteria GetLoanRefundCriteria(string orderBy, string orderDir, int? pin, string name, string receiptNo, bool count = false)
        {
            ICriteria criteria = Session.CreateCriteria<MemberLoanRefund>().Add(Restrictions.Not(Restrictions.Eq("Status", MemberLoanRefund.EntityStatus.Delete)));
            criteria.CreateAlias("TeamMember", "tm").Add(Restrictions.Not(Restrictions.Eq("tm.Status", TeamMember.EntityStatus.Delete)));
            //criteria.SetProjection(Projections.ProjectionList().Add(Projections.Property("tm.Name"), "tm.Name"));
            if (pin != null)
            {
                criteria.Add(Restrictions.Eq("tm.Pin", pin));
            }

            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("tm.Name", name, MatchMode.Anywhere));
            }

            if (!String.IsNullOrEmpty(receiptNo))
            {
                criteria.Add(Restrictions.Like("ReceiptNo", receiptNo));
            }

            if (!count)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }


        #endregion



        public IList<MemberLoanRefund> LoadMemberLoanRefund(int start, int length, string orderBy, string orderDir, int? pin, string name, string receiptNo)
        {
            ICriteria criteria = GetLoanRefundCriteria(orderBy, orderDir, pin, name, receiptNo);
            // criteria.SetResultTransformer(Transformers.AliasToBean(typeof(MemberLoanRefund))).List();
            return criteria.SetFirstResult(start).SetMaxResults(length).List<MemberLoanRefund>();
        }


        public int LoadMemberLoanRefundCount(int start, int length, string orderBy, string orderDir, int? pin, string name, string receiptNo)
        {
            ICriteria criteria = GetLoanRefundCriteria(orderBy, orderDir, pin, name, receiptNo);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
    }
}
