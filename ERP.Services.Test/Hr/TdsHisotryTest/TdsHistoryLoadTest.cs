﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UdvashERP.Services.Test.Hr.TdsHisotryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "TdsHistory")]
    public class TdsHistoryLoadTest : TdsHistoryBaseTest
    {
        [Fact]
        public void GetTdsHistory_Should_TdsHistory_Entity_Successfully()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            var objFactory = _tdsHistoryFactory.Create().WithTeamMember(teamMemberObj);
           _tdsHistoryService.AddTdsHistory(teamMemberObj, objFactory.Object.TdsAmount.ToString(), objFactory.Object.EffectiveDate.ToString());
            var tdsObj = _tdsHistoryService.GetTdsHistory(teamMemberObj.Id);
            Assert.NotNull(tdsObj);
        }

        [Fact]
        public void LoadTdsHistories_Tds_History()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            _tdsHistoryFactory.Create().WithTeamMember(teamMemberObj);
            _tdsHistoryService.Save(_tdsHistoryFactory.Object);            
            var list = _tdsHistoryService.LoadTdsHistories(teamMemberObj, _tdsHistoryFactory.Object.EffectiveDate);
            Assert.NotNull(list);
        }
    }
}
