﻿using System;
using System.Collections.Generic;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using System.Linq;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;

namespace UdvashERP.Services.Hr
{
    public interface IMemberAchievementService : IBaseService
    {
        #region Operational Function

        void SaveMemberAchievement(MemberAchievement memberAchievement);
        void UpdateMemberAchievement(MemberAchievement memberAchievement);
        void DeleteMemberAchievement(long id);

        #endregion

        #region Single Instances Loading Function

        MemberAchievement GetMemberAchievement(long id);
        MemberAchievement GetMemberAchievement(long teamMemberId, int year, int month);

        #endregion

        #region List Loading Function

        IList<MemberAchievement> LoadMemberAchievementList(List<UserMenu> userMenuList, int start, int length, string orderBy, string orderDir, int year, int month, string teamMemberPins);

        List<MyAchievementReport> LoadMyAchievementReports(List<UserMenu> userMenu, long temMemberId, string dateFrom, string dateTo);
        #endregion

        #region Others Function

        int GetMemberAchievementCount(List<UserMenu> userMenuList, string orderBy, string orderDir, int year, int month, string teamMemberPins);

        #endregion

        #region Helper Function

        #endregion

    }
    public class MemberAchievementService : BaseService, IMemberAchievementService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        
        private readonly CommonHelper _commonHelper;
        private readonly IMemberAchievementDao _memberAchievementDao;
        private readonly IMemberAchievementDetailsDao _memberAchievementDetailsDao;
        private readonly IEmploymentHistoryDao _employmentHistoryDao;
        private readonly ITeamMemberDao _teamMemberDao;

        public MemberAchievementService(ISession session)
        {
            Session = session;
            _memberAchievementDao = new MemberAchievementDao { Session = session };
            _employmentHistoryDao = new EmploymentHistoryDao { Session = session };
            _memberAchievementDetailsDao = new MemberAchievementDetailsDao { Session = session };
            _teamMemberDao = new TeamMemberDao { Session = session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public void SaveMemberAchievement(MemberAchievement memberAchievement)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSave(memberAchievement);
                using (transaction = Session.BeginTransaction())
                {
                    _memberAchievementDao.Save(memberAchievement);
                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void UpdateMemberAchievement(MemberAchievement memberAchievement)
        {
            ITransaction transaction = null;
            DateTime dateTime = DateTime.Now;
            try
            {
                long currentUserId = 0;
                if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
                    currentUserId = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                CheckBeforeSave(memberAchievement);
                MemberAchievement oldMemberAchievement = _memberAchievementDao.LoadById(memberAchievement.Id);
                if (oldMemberAchievement == null)
                    throw new InvalidDataException("Invalid Achievement entry!");
                if (oldMemberAchievement.TeamMember.Id != memberAchievement.TeamMember.Id)
                    throw new InvalidDataException("Invalid Achievement entry for you!");
                List<MemberAchievementDetails> allOldTaskTypeDetailsList = new List<MemberAchievementDetails>();
                List<MemberAchievementDetails> allNewTaskTypeDetailsList = new List<MemberAchievementDetails>();
                List<MemberAchievementDetails> newTaskTypeDetailsList = new List<MemberAchievementDetails>();
                List<MemberAchievementDetails> editTaskTypeDetailsList = new List<MemberAchievementDetails>();
                List<MemberAchievementDetails> deleteTaskTypeDetailsList = new List<MemberAchievementDetails>();
                if (oldMemberAchievement.MemberAchievementDetails.Any())
                    allOldTaskTypeDetailsList = oldMemberAchievement.MemberAchievementDetails.Where(x => x.Status == MemberAchievementDetails.EntityStatus.Active).ToList();
                if (memberAchievement.MemberAchievementDetails.Any())
                    allNewTaskTypeDetailsList = memberAchievement.MemberAchievementDetails.Where(x => x.Status == MemberAchievementDetails.EntityStatus.Active).ToList();

                #region New List
                if (allNewTaskTypeDetailsList.Any())
                    newTaskTypeDetailsList = allNewTaskTypeDetailsList.Where(x => x.Id == 0).ToList();
                #endregion

                #region Edit List
                if (allNewTaskTypeDetailsList.Any())
                    editTaskTypeDetailsList = allNewTaskTypeDetailsList.Where(x => x.Id != 0).ToList();
                #endregion

                #region Delete List
                if (!allNewTaskTypeDetailsList.Any())
                {
                    deleteTaskTypeDetailsList = allOldTaskTypeDetailsList;
                }
                else if (allNewTaskTypeDetailsList.Any() && allOldTaskTypeDetailsList.Any())
                {
                    List<long> tempIdList = allNewTaskTypeDetailsList.Select(x => x.Id).ToList();
                    deleteTaskTypeDetailsList = allOldTaskTypeDetailsList.Where(x => !tempIdList.Contains(x.Id)).ToList();
                }
                #endregion

                using (transaction = Session.BeginTransaction())
                {
                    oldMemberAchievement.TeamMember = memberAchievement.TeamMember;
                    oldMemberAchievement.Year = memberAchievement.Year;
                    oldMemberAchievement.Month = memberAchievement.Month;
                    oldMemberAchievement.TotalWorkingHour = memberAchievement.TotalWorkingHour;
                    oldMemberAchievement.TotalCompletePoint = memberAchievement.TotalCompletePoint;
                    oldMemberAchievement.CompleteTaskCount = memberAchievement.CompleteTaskCount;
                    oldMemberAchievement.RunningTaskCount = memberAchievement.RunningTaskCount;
                    _memberAchievementDao.Update(oldMemberAchievement);

                    if (newTaskTypeDetailsList.Any())
                    {
                        foreach (MemberAchievementDetails memberAchievementDetails in newTaskTypeDetailsList)
                        {
                            memberAchievementDetails.MemberAchievement = oldMemberAchievement;
                            _memberAchievementDetailsDao.Save(memberAchievementDetails);
                        }
                    }
                    if (editTaskTypeDetailsList.Any())
                    {
                        foreach (MemberAchievementDetails memberAchievementDetails in editTaskTypeDetailsList)
                        {
                            MemberAchievementDetails oldMemberAchievementDetails = _memberAchievementDetailsDao.LoadById(memberAchievementDetails.Id);
                            if (oldMemberAchievementDetails == null)
                                throw  new InvalidDataException("Invalid Task Type found!");

                            oldMemberAchievementDetails.TaskName = memberAchievementDetails.TaskName;
                            oldMemberAchievementDetails.TaskType = memberAchievementDetails.TaskType;
                            oldMemberAchievementDetails.ModificationDate = dateTime;
                            oldMemberAchievementDetails.ModifyBy = currentUserId;
                            _memberAchievementDetailsDao.Update(oldMemberAchievementDetails);
                        }
                    }

                    if (deleteTaskTypeDetailsList.Any())
                    {
                        foreach (MemberAchievementDetails memberAchievementDetails in deleteTaskTypeDetailsList)
                        {
                            MemberAchievementDetails oldMemberAchievementDetails = _memberAchievementDetailsDao.LoadById(memberAchievementDetails.Id);
                            if (oldMemberAchievementDetails == null)
                                throw new InvalidDataException("Invalid Task Type found!");

                            oldMemberAchievementDetails.Status = MemberAchievementDetails.EntityStatus.Delete;
                            oldMemberAchievementDetails.ModificationDate = dateTime;
                            oldMemberAchievementDetails.ModifyBy = currentUserId;
                            _memberAchievementDetailsDao.Update(oldMemberAchievementDetails);
                        }
                    }

                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void DeleteMemberAchievement(long id)
        {
            ITransaction transaction = null;
            try
            {
                MemberAchievement memberAchievement = _memberAchievementDao.LoadById(id);
                if (memberAchievement == null)
                    throw new InvalidDataException("Invalid Achievement entry!");

                using (transaction = Session.BeginTransaction())
                {
                    memberAchievement.Status = MemberAchievement.EntityStatus.Delete;
                    _memberAchievementDao.Update(memberAchievement);
                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public MemberAchievement GetMemberAchievement(long id)
        {
            try
            {
                return _memberAchievementDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public MemberAchievement GetMemberAchievement(long teamMemberId, int year, int month)
        {
            try
            {
                return _memberAchievementDao.GetMemberAchievement(teamMemberId, year, month);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function

        public IList<MemberAchievement> LoadMemberAchievementList(List<UserMenu> userMenuList, int start, int length, string orderBy, string orderDir, int year, int month, string teamMemberPins = "")
        {
            try
            {
                List<int> memberPinList = new List<int>();
                List<long> authoMemberIdList = new List<long>();
                if (!String.IsNullOrEmpty(teamMemberPins))
                {
                    teamMemberPins = teamMemberPins.Replace(',', ' ');
                    string[] pinArray = teamMemberPins.Split(' ');
                    memberPinList.AddRange(pinArray.Select(pin => Convert.ToInt32(pin)));
                }
                else
                {
                    memberPinList.Add(Convert.ToInt32(SelectionType.SelelectAll));
                }
                if (userMenuList != null && userMenuList.Any())
                {
                    List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenuList);
                    List<long> authoBranchIdList = AuthHelper.LoadBranchIdList(userMenuList, authoOrganizationIdList);
                    DateTime dateTime = DateTime.Now;
                    authoMemberIdList = _teamMemberDao.LoadHrAuthorizedTeamMemberId(dateTime, authoBranchIdList).ToList();
                }
                return _memberAchievementDao.LoadMemberAchievementList(start, length, orderBy, orderDir, year, month, memberPinList, authoMemberIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public List<MyAchievementReport> LoadMyAchievementReports(List<UserMenu> userMenu, long temMemberId, string dateFrom, string dateTo)
        {
            try
            {           
                //if (userMenu == null)
                //    throw new InvalidDataException("Invalid menu.");
                
                if (string.IsNullOrEmpty(dateTo))
                    throw new InvalidDataException("DateTo can't empty.");

                if (userMenu != null)
                {
                    List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu);
                    List<long> authoBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationIdList);
                    DateTime dateTime = DateTime.Now;
                    List<long> teamMemberIdList = _teamMemberDao.LoadHrAuthorizedTeamMemberId(dateTime, authoBranchIdList).ToList();
                    if (!teamMemberIdList.Any() || (teamMemberIdList.Any() && !teamMemberIdList.Contains(temMemberId)))
                    {
                        throw new InvalidDataException("Your are not authorized to perform this operation!");
                    }
                }

                return _memberAchievementDao.LoadMyAchievementReports(temMemberId, dateFrom, dateTo ).ToList();
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int GetMemberAchievementCount(List<UserMenu> userMenuList, string orderBy, string orderDir, int year, int month, string teamMemberPins = "")
        {
            try
            {
                List<int> memberPinList = new List<int>();
                List<long> authoMemberIdList = new List<long>();
                if (!String.IsNullOrEmpty(teamMemberPins))
                {
                    teamMemberPins = teamMemberPins.Replace(',', ' ');
                    string[] pinArray = teamMemberPins.Split(' ');
                    memberPinList.AddRange(pinArray.Select(pin => Convert.ToInt32(pin)));
                }
                else
                {
                    memberPinList.Add(Convert.ToInt32(SelectionType.SelelectAll));
                }
                if (userMenuList != null && userMenuList.Any())
                {
                    List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenuList);
                    List<long> authoBranchIdList = AuthHelper.LoadBranchIdList(userMenuList, authoOrganizationIdList);
                    DateTime dateTime = DateTime.Now;
                    authoMemberIdList = _teamMemberDao.LoadHrAuthorizedTeamMemberId(dateTime, authoBranchIdList).ToList();
                }
                return _memberAchievementDao.GetMemberAchievementCount(orderBy, orderDir, year, month, memberPinList, authoMemberIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Helper function

        private void CheckBeforeSave(MemberAchievement memberAchievement)
        {
            if (memberAchievement.TeamMember == null)
                throw new InvalidDataException("No team member found!");

            if (memberAchievement.Year <= 0)
                throw new InvalidDataException("Please select year of your Achievement!");

            if (memberAchievement.Month <= 0)
                throw new InvalidDataException("Please select Month of your Achievement!");

            if ((int)memberAchievement.Month > 12)
                throw new InvalidDataException("Invalid Month selected for your Achievement!");

            if (memberAchievement.TotalWorkingHour < 0)
                throw new InvalidDataException("Invalid Total Working Hour!");

            if (memberAchievement.TotalCompletePoint < 0)
                throw new InvalidDataException("Invalid Total Complete Point!");

            if (memberAchievement.CompleteTaskCount < 0)
                throw new InvalidDataException("Invalid Complete Task Count!");

            if (memberAchievement.RunningTaskCount < 0)
                throw new InvalidDataException("Invalid Running Task Count!");

            EmploymentHistory joinEmploymentHistory = _employmentHistoryDao.GetTeamMemberJoiningEmploymentHistory(memberAchievement.TeamMember.Id);
            DateTime dateTime = new DateTime(memberAchievement.Year, (int)memberAchievement.Month, 1).AddMonths(1).AddDays(-1);
            if (joinEmploymentHistory != null && dateTime < joinEmploymentHistory.EffectiveDate.Date)
                throw new InvalidDataException("This team member joining date is " + joinEmploymentHistory.EffectiveDate.Date.ToString("yyyy-MM-dd") + ". Please enter Achievement after this date.");

            MemberAchievement tempMemberAchievement = _memberAchievementDao.GetMemberAchievement(memberAchievement.TeamMember.Id, memberAchievement.Year, (int)memberAchievement.Month);
            if (tempMemberAchievement != null && (memberAchievement.Id == 0 || (memberAchievement.Id != 0 && memberAchievement.Id != tempMemberAchievement.Id)))
            {
                throw new InvalidDataException("You have already added Achievement for this month!");

            }

        }
        
        #endregion

       
    }
}
