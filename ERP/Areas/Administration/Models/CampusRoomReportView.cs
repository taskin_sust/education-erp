﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Areas.Administration.Models
{
    public class CampusRoomReportView
    {
        #region Paasing Data View To Controller
        public SelectList OrgList { get; set; } 
        [Required(ErrorMessage = "You need to select Organization")]
        public long OrganizationId { get; set; }   
        public SelectList BranchList { get; set; }
        [Required(ErrorMessage = "You need to select Brach")]
        public long BranchId { get; set; }


        public SelectList CampusList { get; set; }
        [Required(ErrorMessage = "You need to select campus")]
        public long[] SelectedCampus { get; set; }

        public IList<CampusRoom> CampusRoomList { get; set; }
         
        #endregion
    }
}