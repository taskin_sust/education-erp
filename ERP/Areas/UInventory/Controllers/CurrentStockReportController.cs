﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class CurrentStockReportController : Controller
    {
        #region logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private List<UserMenu> authorizeMenu;

        private readonly ICommonHelper _commonHelper;
        private readonly IGoodsReceiveDetailService _goodsReceiveDetailService;
        private readonly IOrganizationService _organizationService;
        private readonly IItemService _itemService;
        private readonly IItemGroupService _itemGroupService;
        private readonly ISupplierService _supplierService;
        private readonly IBranchService _branchService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ICurrentStockSummaryService _currentStockSummary;

        private AuthorizeBranchNameDelegate branchNameDelegate;
        public CurrentStockReportController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _goodsReceiveDetailService = new GoodsReceiveDetailsService(session);
            _organizationService = new OrganizationService(session);
            _itemService = new ItemService(session);
            _itemGroupService = new ItemGroupService(session);
            _supplierService = new SupplierService(session);
            _branchService = new BranchService(session);
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _programBranchSessionService = new ProgramBranchSessionService(session);
            _currentStockSummary = new CurrentStockSummaryService(session);

            branchNameDelegate = new AuthorizeBranchNameDelegate(OnGettingAuthorizeBranchNameList);
        }

        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region Report

        public ActionResult CurrentStock()
        {
            try
            {
                Initialize();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CurrentStock(CurrentStockViewModel currentStockVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    if (currentStockVm.BranchId == null) currentStockVm.BranchId = new long[] { 0 };
                    if (currentStockVm.ItemGroupId == null) currentStockVm.ItemGroupId = new long[] { 0 };
                    if (currentStockVm.ItemId == null) currentStockVm.ItemId = new long[] { 0 };
                    if (currentStockVm.ProgramSession == null)
                        currentStockVm.ProgramSession = new string[] { SelectionType.SelelectAll.ToString() };
                    List<long> branchIds = new List<long>(currentStockVm.BranchId.ToArray());

                    ViewBag.organizationName = _organizationService.LoadById(currentStockVm.OrganizationId).Name;
                    var branchName = !currentStockVm.BranchId.Contains(SelectionType.SelelectAll)
                        ? string.Join(", ", _branchService.LoadAuthorizedBranchNameList(
                            userMenu, _commonHelper.ConvertIdToList(currentStockVm.OrganizationId), branchIds).ToList())
                        : "All Branch";
                    var organizationIdList = _commonHelper.ConvertIdToList(currentStockVm.OrganizationId);

                    var bName = !currentStockVm.BranchId.Contains(SelectionType.SelelectAll)
                        ? string.Join(", ",
                            _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList,
                                currentStockVm.BranchId.Skip(0).Take(currentStockVm.BranchId.Length).ToList())
                            )
                        : string.Join(", ",
                            _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds)
                                .ToList());
                    ViewBag.branchAry = bName.Split(',');

                    ViewBag.branchName = branchName;
                    ViewBag.itemGroup = !currentStockVm.ItemGroupId.Contains(SelectionType.SelelectAll)
                        ? string.Join(", ",
                            _itemGroupService.LoadItemGroup(currentStockVm.ItemGroupId.ToList())
                                .Select(x => x.Name)
                                .ToList())
                        : "All Item Groups";
                    ViewBag.item = !currentStockVm.ItemId.Contains(SelectionType.SelelectAll)
                        ? string.Join(", ",
                            _itemService.LoadItemList(currentStockVm.ItemId).Select(x => x.Name).ToList())
                        : "All Items";
                    string programSession = "";
                    int countshouldhave = CountProgramAndSession(currentStockVm.OrganizationId, branchIds, currentStockVm.ItemId.ToList());
                    int counthave = currentStockVm.ProgramSession.Count();
                    if ((counthave == 1 && currentStockVm.ProgramSession[0] == SelectionType.SelelectAll.ToString()) ||
                        ((counthave - 1) == countshouldhave))
                    {
                        programSession = "All Program & Session";
                        var programSessionColName = GetProgramSessionName(currentStockVm.ProgramSession, currentStockVm.OrganizationId, currentStockVm.BranchId, currentStockVm.ItemId);
                        ViewBag.programSessionAry = programSessionColName.Split(',');
                    }
                    else
                    {
                        programSession = GetProgramSessionName(currentStockVm.ProgramSession, currentStockVm.OrganizationId, currentStockVm.BranchId, currentStockVm.ItemId);
                        ViewBag.programSessionAry = programSession.Split(',');
                    }
                    ViewBag.programSession = programSession;
                    return PartialView("Partial/_StockReport", currentStockVm);
                }
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        #endregion

        #region data table render

        [HttpPost]
        public JsonResult CurrentStockReportAjaxRequest(int draw, int start, int length, long organizationId, List<long> branchIdList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, List<string> programsession)
        {
            try
            {
                return GetCurrentStock(draw, start, length, organizationId, branchIdList, itemGroupIdList, itemIdList, programIdList, sessionIdList, programsession);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                throw ex;
            }
        }

        private JsonResult GetCurrentStock(int draw, int start, int length, long organizationId, List<long> branchIdList, List<long> itemGroupIdList, List<long> itemIdList, 
            List<long> programIdList, List<long> sessionIdList, List<string> programsession)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<dynamic> goodrecList = _currentStockSummary.LoadCurrentStock(branchNameDelegate, start, length, userMenu, organizationId, branchIdList, itemGroupIdList, itemIdList, programIdList,
                    sessionIdList, programsession);
            int count = _currentStockSummary.CountCurrentStock(branchNameDelegate, userMenu, organizationId, branchIdList, itemGroupIdList, itemIdList, programIdList, sessionIdList, programsession);
            int sl = ++start;
            var data = new List<object>();
            foreach (var goodRecObj in goodrecList)
            {
                var str = new List<object>();
                str.Add(sl.ToString());
                IDictionary<string, object> dic;
                str.AddRange(from entry in dic = goodRecObj select entry.Value);
                sl++;
                data.Add(str);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = count,
                recordsFiltered = count,
                start = start,
                length = length,
                data = data,

            });
        }

        #endregion

        #region Helper

        public void Initialize()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var authorgList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");

            ViewBag.organizationList = authorgList;
            ViewBag.branchIdList = new SelectList(string.Empty, "Value", "Text");

            var list = _itemGroupService.LoadItemGroup(0, int.MaxValue, "", "", "", ItemGroup.EntityStatus.Active.ToString());
            list.Insert(0, new ItemGroup() { Id = 0, Name = "All Selected" });
            ViewBag.itemGroupList = new SelectList(list, "Id", "Name");

            ViewBag.itemList = new SelectList(string.Empty, "Value", "Text");

            var psList = new List<ProgramSessionDto>();
            psList.Insert(0, new ProgramSessionDto() { ProgramAndSessionId = SelectionType.SelelectAll.ToString(), ProgramAndSessionName = "All Selected" });
            ViewBag.programSessionList = new SelectList(psList, "ProgramAndSessionId", "ProgramAndSessionName");
        }

        public int CountProgramAndSession(long organizationId, List<long> branchIds, List<long> itemIdList)
        {
            IList<ProgramSessionDto> list = new List<ProgramSessionDto>();
            try
            {
                if (branchIds.Count == 1 && branchIds.Contains(0)) branchIds = null;
                if (itemIdList.Count == 1 && itemIdList.Contains(0)) itemIdList = null;
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                list = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIds, itemIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return list.Count();
        }

        private string GetProgramSessionName(string[] programSessionAryStrings, long organizationId, long[] branchId, long[] itemId)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<long> programIdList = new List<long>();
            IList<long> sessionIdList = new List<long>();

            var programSession = "";
            string[] programSessionArrayList;
            if (programSessionAryStrings.Contains(SelectionType.SelectNone.ToString()))
            {
                programSession = "N/A,";
                programSessionArrayList = programSessionAryStrings.Skip(1).Take(programSessionAryStrings.Length - 1).ToArray();
            }
            else
                programSessionArrayList = programSessionAryStrings;
            if (programSessionAryStrings.Contains(SelectionType.SelelectAll.ToString()))
            {
                programSession = "N/A,";
                programSessionArrayList = _goodsReceiveDetailService.BuildAuthProgramSession(userMenu, organizationId, branchId, itemId);
            }
            foreach (var programsession in programSessionArrayList)
            {
                string[] programSessionArray = programsession.Split(new string[] { "::" }, StringSplitOptions.None);
                Program program = _programService.GetProgram(Convert.ToInt64(programSessionArray[0]));
                Session session = _sessionService.LoadById(Convert.ToInt64(programSessionArray[1]));
                if (program != null && session != null)
                {
                    programIdList.Add(program.Id);
                    sessionIdList.Add(session.Id);
                    string programName = program.ShortName;
                    string sessionName = session.Name;
                    string programSessionName = programName + " & " + sessionName + ", ";
                    programSession += programSessionName;
                }
            }
            programSession = programSession.Trim(' ', ',');
            ViewBag.ProgramIdList = programIdList;
            ViewBag.SessionIdList = sessionIdList;
            return programSession;
        }

        private IList<string> OnGettingAuthorizeBranchNameList(List<UserMenu> userMenu, List<long> organizationIdList, List<long> branchIds)
        {
            try
            {
                var bName = !branchIds.Contains(SelectionType.SelelectAll) ? string.Join(", ",
                   _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds.Skip(0).Take(branchIds.Count).ToList())
                   ) : string.Join(", ", _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIds).ToList());
                return bName.Split(',');
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Export

        public ActionResult ExportReport(long organizationId, List<long> branchIdList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, List<string> programsession)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                int start = 0;
                int length = int.MaxValue;
                IList<dynamic> gooodRecList = new List<dynamic>();
                string organizationName = _organizationService.LoadById(organizationId).Name;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);

                List<string> headerList = new List<string>();
                var bName = !branchIdList.Contains(SelectionType.SelelectAll) ? string.Join(", ",
                     _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIdList.Skip(0).Take(branchIdList.Count).ToList())
                     ) : string.Join(", ", _branchService.LoadAuthorizedBranchNameList(userMenu, organizationIdList, branchIdList).ToList());
                var branchAry = bName.Split(',');

                //bool isAddNa = false;
                //bool isOnlyNa = false;
                //string[] psAry = programsession.ToArray();
                //if (psAry.Length > 0)
                //    psAry = psAry.Distinct().ToArray();

                //if (psAry.Contains(SelectionType.SelectNone.ToString()))
                //{
                //    isAddNa = true;
                //    psAry = psAry.Skip(1).Take(psAry.Length - 1).ToArray();
                //    if (psAry.Length <= 0) isOnlyNa = true;
                //}
                //if (psAry.Length == 1 && psAry.Contains(SelectionType.SelelectAll.ToString()))
                //    isAddNa = true;

                //string[] programSessionAuthArrayList = _goodsReceiveDetailService.BuildAuthProgramSession(userMenu, organizationId, branchIdList.ToArray(), itemIdList.ToArray(), psAry);
                //var proSessionList = programSessionAuthArrayList.ToList();

                //if (isOnlyNa) { proSessionList = new List<string>(); }
                //if (isAddNa) { proSessionList.Insert(0, SelectionType.SelectNone.ToString()); }

                headerList.Add(organizationName);
                headerList.Add("Current Stock Report");
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                columnList.Add("Item Group");
                columnList.Add("Item Name");
                columnList.Add("Program & Session");
                columnList.AddRange(branchAry);
                gooodRecList = _currentStockSummary.LoadCurrentStock(branchNameDelegate, start, length, userMenu, organizationId, branchIdList, itemGroupIdList, itemIdList, programIdList,
                    sessionIdList, programsession);

                columnList.Add("Total");
                var transactionXlsList = new List<List<object>>();
                foreach (var goodRecObj in gooodRecList)
                {
                    var str = new List<object>();
                    IDictionary<string, object> dic;
                    str.AddRange(from entry in dic = goodRecObj select entry.Value);
                    transactionXlsList.Add(str);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "CurrentStock_Report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("Autoclose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion


    }
}