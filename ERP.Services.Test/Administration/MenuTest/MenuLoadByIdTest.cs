﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MenuTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Menu")]
    public class MenuLoadByIdTest : MenuBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _menuService.MenuLoadById(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            var menuFactory =
                _menuFactory.Create().WithNameAndRank(_name, 1).WithMenuGroup().WithAction()
                    .Persist();
            var obj = _menuService.MenuLoadById(menuFactory.Object.Id);
            Assert.NotNull(obj);
            
        }

    }
}
