﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSynchronizerSettingTest
{

    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSynchronizerSettingService")]
    public class AttendanceSynchronizerSettingLoadTest : AttendanceSynchronizerSettingBaseTest
    {

        #region List Load Test

        [Fact]
        void Should_Return_List_Successfully()
        {
            _attendanceSynchronizerSettingFactory.Create().Persist();
            Assert.True(_attendanceSynchronizerSettingService.LoadSynchronizerSetting(0, 10, "Name", "ASC").Count > 0);
        }

        #endregion

        #region Single Object Load

        [Fact]
        void Should_Return_InvalidData_Exception()
        {
            _attendanceSynchronizerSettingFactory.Create().Persist();
            Assert.Throws<InvalidDataException>(
                () => _attendanceSynchronizerSettingService.GetAttendanceSynchronizerSetting(0));
        }

        [Fact]
        void Should_Return_Successfully()
        {
            _attendanceSynchronizerSettingFactory.Create().Persist();
            Assert.Equal(
                _attendanceSynchronizerSettingService.GetAttendanceSynchronizerSetting(
                    _attendanceSynchronizerSettingFactory.Object.Id).Id, _attendanceSynchronizerSettingFactory.Object.Id);
        }

        [Fact]
        void Should_Return_NextAttdSync_Successfully()
        {
            _attendanceSynchronizerSettingFactory.Create();
            _attendanceSynchronizerSettingFactory.Object.SynchronizerVersion = 2;
            _attendanceSynchronizerSettingFactory.Persist();

            _attendanceSynchronizerSettingFactory.Create(0, "test", DateTime.Now, "www.randomurl.com" + "/a",3);
            //_attendanceSynchronizerSettingFactory.Object.SynchronizerVersion = 3;
            _attendanceSynchronizerSettingFactory.Persist();

            Assert.Equal(_attendanceSynchronizerSettingFactory.SingleObjectList[1].Id,
                _attendanceSynchronizerSettingService.GetNextAttendanceSynchronizerSetting(_attendanceSynchronizerSettingFactory.SingleObjectList[0].SynchronizerVersion).Id);
        }

        #endregion
        
        #region Others Test

        [Fact]
        void Should_Return_Count_Successfully()
        {
            _attendanceSynchronizerSettingFactory.Create().Persist();
            Assert.True(_attendanceSynchronizerSettingService.SynchronizerSettingRowCount()>= 1);
        }

        #endregion
    }
}
