using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class WorkOrderCriteriaFactory : ObjectFactoryBase<WorkOrderCriteria>
    {

        private readonly IWorkOrderCriteriaService _workOrderCriteriaService;

        public WorkOrderCriteriaFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _workOrderCriteriaService = new WorkOrderCriteriaService(session);

        }

        #region Create

        public WorkOrderCriteriaFactory Create()
        {
            Object = new WorkOrderCriteria()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Criteria = "Test Criteria",
                CriteriaValue = "Test Criteria Value",
                Status = 1
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public WorkOrderCriteriaFactory WithWorkOrderDetails(WorkOrderDetails workOrderDetails)
        {
            Object.WorkOrderDetail = workOrderDetails;
            return this;
        }

        public WorkOrderCriteriaFactory Persist()
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _workOrderCriteriaService.Save(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x => { if (x.Id == 0)  _workOrderCriteriaService.Save(x); });
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public WorkOrderCriteriaFactory CleanUp()
        {
            DeleteAll();
            return this;
        }

        #endregion

    }
}
