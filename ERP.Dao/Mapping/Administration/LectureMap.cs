﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class LectureMap : BaseClassMap<Lecture, long>
    {
        public LectureMap()
        {
            Table("Lecture");
            LazyLoad();
            Map(x => x.Name);
            Map(x => x.ClassNameSuffix);
            References(x => x.LectureSettings).Column("LectureSettingsId");
            HasMany(x => x.StudentClassAttendence).KeyColumn("LectureId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.CourseProgresses).KeyColumn("LectureId").Cascade.AllDeleteOrphan().Inverse();
            //HasManyToMany(x => x.Batches).Table("CourseProgress")
            //    .ParentKeyColumn("LectureId").ChildKeyColumn("BatchId")
            //    .Inverse();
        }
    }
}
