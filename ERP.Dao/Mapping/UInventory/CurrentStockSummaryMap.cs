using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory {
    
    
    public partial class CurrentStockSummaryMap : BaseClassMap<CurrentStockSummary, long> {
        
        public CurrentStockSummaryMap() {
			Table("UINV_CurrentStockSummary");
			LazyLoad();
			References(x => x.Program).Column("ProgramId");
			References(x => x.Session).Column("SessionId");
			References(x => x.Item).Column("ItemId");
			References(x => x.Branch).Column("BranchId").Not.Nullable();
			Map(x => x.StockQuantity).Column("StockQuantity");
        }
    }
}
