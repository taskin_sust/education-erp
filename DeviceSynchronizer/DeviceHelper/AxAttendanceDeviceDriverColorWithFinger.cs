﻿using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model.ApiModel;
using DeviceSynchronizer.Model.BusinessModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceSynchronizer.DeviceHelper
{
    public class AxAttendanceDeviceDriverColorWithFinger : AxAttendanceDeviceDriverColor
    {
        public AxAttendanceDeviceDriverColorWithFinger(AttendanceDeviceModel attendanceDevice, MessagePass throwMessage)
            : base(attendanceDevice, throwMessage)
        {
        }

        protected override bool Register(TeamMemberModel member, string sPassword, int iPrivilege)
        {
            bool returnResult = false;
            try
            {
                returnResult = base.Register(member, sPassword, iPrivilege);
                if (Helper.Helper.FingerPrintAvailable(member) > 0)
                {
                    if (returnResult)
                        for (int fingerIndex = 0; fingerIndex < 10; fingerIndex++)
                        {
                            byte[] fingerData;
                            Helper.Helper.RegisterFingerTemplate(fingerIndex, member, _attendanceDevice.MachineNo, out fingerData);
                            //remove & update finger 
                            AxDevice.SSR_DelUserTmpExt(_attendanceDevice.MachineNo, member.EnrollNo.ToString(), fingerIndex);
                            bool isSuccess = AxDevice.SetUserTmpEx(_attendanceDevice.MachineNo, member.EnrollNo.ToString(), fingerIndex, 1, ref fingerData[0]);
                            if (isSuccess)
                                OnThrowingMessage("Register user " + member.Name + " in device with Finger " + member.Pin, MessageType.Info);
                        }
                }
                return returnResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

       public override List<AttendanceLog> LoadLogList()
       {
           var result= base.LoadLogList();
           return result;
       }
    }
}
