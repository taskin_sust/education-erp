﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{

    public interface IStudentBoardService : IBaseService
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        StudentBoard GetByBoardName(string boardName);
        StudentBoard GetByBoardId(long id);
        #endregion

        #region List Loading Function
        IList<StudentBoard> LoadActive();
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion        
        
    }

    public class StudentBoardService : BaseService, IStudentBoardService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion
       
        #region Propertise & Object Initialization
        private readonly StudentBoardDao _studentBoardDao;

        public StudentBoardService(ISession session)
        {
            Session = session;
            _studentBoardDao = new StudentBoardDao() { Session = Session };

        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public StudentBoard GetByBoardId(long id)
        {
            return _studentBoardDao.LoadById(id);
        }
        #endregion

        #region List Loading Function

        public StudentBoard GetByBoardName(string boardName)
        {
            return _studentBoardDao.GetByBoardName(boardName);
        }

        public IList<StudentBoard> LoadActive()
        {
            try
            {
                return _studentBoardDao.LoadAllOk();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
