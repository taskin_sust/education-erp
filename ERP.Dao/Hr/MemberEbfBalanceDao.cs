using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Base;


namespace UdvashERP.Dao.Hr
{
    public interface IMemberEbfBalanceDao : IBaseDao<MemberEbfBalance, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion
    }

    public class MemberEbfBalanceDao : BaseDao<MemberEbfBalance, long>, IMemberEbfBalanceDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion
    }
}
