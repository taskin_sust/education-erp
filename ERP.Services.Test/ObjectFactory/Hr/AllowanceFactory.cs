﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class AllowanceFactory : ObjectFactoryBase<Allowance>
    {
        private readonly IAllowanceService _allowanceService;
        private TestBaseService<AllowanceLog> _allowanceServiceLogService;
        private TeamMemberFactory _teamMemberFactory;
        public AllowanceFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _allowanceService = new AllowanceService(_session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _allowanceServiceLogService = new TestBaseService<AllowanceLog>(session);
        }
        #region Create

        public AllowanceFactory Create()
        {
            Object = new Allowance()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Allowance.EntityStatus.Active,
                DailyAllowanceDate = DateTime.Now,
                IsApproved = true
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public AllowanceFactory CreateWith(string name, DateTime dailyAllowanceDate, bool isApproved)
        {
            Object = new Allowance()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Department.EntityStatus.Active,
                DailyAllowanceDate = dailyAllowanceDate,
                IsApproved = isApproved
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public AllowanceFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }
        public AllowanceFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        //public AllowanceFactory Persist(List<UserMenu> userMenu = null)
        //{            
        //    if (Object != null)
        //    {
        //        _allowanceService.Save(userMenu, Object.DailyAllowanceDate.ToString()
        //                                , Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                                , Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "");
        //    }

        //    return this;
        //}

        #endregion

        #region Others
        #endregion

        #region CleanUp

        public void LogCleanUp(AllowanceFactory allowanceFactory)
        {
            if (allowanceFactory.ObjectList.Any())
            {
                DeleteLog(allowanceFactory.ObjectList);
            }
            if (allowanceFactory.SingleObjectList.Any())
            {
                DeleteLog(allowanceFactory.SingleObjectList);
            }
        }

        private void DeleteLog(IEnumerable<Allowance> list)
        {
            foreach (var allowance in list)
            {
                var tableName = GetEntityTableName(typeof(AllowanceLog));
                string swData = "DELETE FROM " + tableName + " WHERE [AllowanceId] = " + allowance.Id + ";";
                _session.CreateSQLQuery(swData).ExecuteUpdate();
            }
        }

        public void CleanUpWithLog(TeamMember teamMember)
        {
            DeleteLog(_session.QueryOver<Allowance>().Where(x => x.TeamMember.Id == teamMember.Id).List<Allowance>());
            var tableName = GetEntityTableName(typeof(Allowance));
            string swData = "DELETE FROM " + tableName + " WHERE [TeamMemberId] = " + teamMember.Id + ";";
            _session.CreateSQLQuery(swData).ExecuteUpdate();
        }

        public AllowanceFactory CleanUp()
        {
            //if (Object != null && Object.Id > 0)
            //{
            //    var adIdList = _session.QueryOver<AllowanceLog>().Select(x => x.Id).Where(x => x.Allowance == Object).List<long>();
            //    if (adIdList.Count > 0)
            //        _allowanceServiceLogService.DeleteByIdList(adIdList.ToList());
            //}
            DeleteAll();
            _teamMemberFactory.Cleanup();
            return this;
        }

        #endregion


    }
}
