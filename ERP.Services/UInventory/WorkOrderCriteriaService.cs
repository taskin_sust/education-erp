using System;
using log4net;
using NHibernate;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface IWorkOrderCriteriaService : IBaseService
    {

        #region Operational Function

        void Save(WorkOrderCriteria workOrderCriteria);

        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion


    }
    public class WorkOrderCriteriaService : BaseService, IWorkOrderCriteriaService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");
        #endregion

        #region Propertise & Object Initialization

        public WorkOrderCriteriaService(ISession session)
        {
            Session = session;
        }

        #endregion

        #region Operational Functions
        public void Save(WorkOrderCriteria xWorkOrderCriteria)
        {
            try
            {
                Session.Save(xWorkOrderCriteria);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper function
        #endregion


    }
}

