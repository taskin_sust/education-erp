﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IHolidaySettingDao : IBaseDao<HolidaySetting, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        HolidaySetting GetHolidayByDate(DateTime dateTime, Organization organization);
        #endregion

        #region List Loading Function
        //IList<HolidaySetting> LoadHoliday(int start, int length, string orderBy, string dir, long? organizationId, int? month, int? year, bool isExport = false);
        IList<HolidaySetting> LoadHoliday(int start, int length, string orderBy, string dir, List<long> organizationIdList, int? month, int? year, bool isExport = false);
        IList<HolidaySetting> LoadHolidayByOrganization(List<long> organizationIdList);
        #endregion

        #region Others Function
        //int GetHolidayCount(long? organizationId, int? month, int? year);
        int GetHolidayCount(List<long> organizationIdList, int? month, int? year);
        #endregion

        #region Helper Function
        bool IsHolidayByOrganizationAndDate(long organizationId, DateTime date); 
        bool CheckExistingHoliday(long organizationId, string name,DateTime dateFrom, long? id = null);
        #endregion
    }
    public class HolidaySettingDao : BaseDao<HolidaySetting, long>, IHolidaySettingDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public HolidaySetting GetHolidayByDate(DateTime dateTime, Organization organization)
        {
            var entity = Session.QueryOver<HolidaySetting>()
                            .Where(x => dateTime >= x.DateFrom)
                            .And(x => dateTime <= x.DateTo)
                            .And(x => x.Status == HolidaySetting.EntityStatus.Active);
            if (organization != null)
            {
                entity.And(x => x.Organization == organization);
            }
            var result = entity.OrderBy(x => x.HolidayType).Desc.List<HolidaySetting>();
            if (result != null && result.Count > 0)
            {
                return result[0];
            }
            return null;
        }

        //public IList<HolidaySetting> LoadHoliday(int start, int length, string orderBy, string dir, long? organizationId, int? month, int? year,bool isExport=false)
        public IList<HolidaySetting> LoadHoliday(int start, int length, string orderBy, string dir, List<long> organizationIdList, int? month, int? year,
            bool isExport = false)
        {
            var holidays = Session.Query<HolidaySetting>().Where(x => x.Status == HolidaySetting.EntityStatus.Active);
            //if (organizationId != null)
            //{
            //    holidays = holidays.Where(x => x.Organization.Id == organizationId);
            //}
            if (organizationIdList != null && organizationIdList.Count>0)
            {
                holidays = holidays.Where(x => organizationIdList.Contains(x.Organization.Id));
            }
            if (month != null)
            {
                holidays = holidays.Where(x => x.DateFrom.Month == month);
            }
            if (year != null)
            {
                holidays = holidays.Where(x => x.DateFrom.Year == year);
            }
            List<HolidaySetting> holidayList;
            if (isExport)
            {
                holidayList = holidays.ToList();
                
            }
            else if (length > 0)
            {
                holidayList = holidays.Skip(start).Take(length).ToList();

            }
            else
            {
                holidayList = new List<HolidaySetting>();
            }
            return holidayList;
        }

        public IList<HolidaySetting> LoadHolidayByOrganization(List<long> organizationIdList)
        {
            var holidays = Session.Query<HolidaySetting>().Where(x => x.Status == HolidaySetting.EntityStatus.Active);
            if (organizationIdList != null)
            {
                holidays = holidays.Where(x => organizationIdList.Contains(x.Organization.Id));
            }
            return holidays.ToList();
        }

        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        public bool IsHolidayByOrganizationAndDate(long organizationId, DateTime date)
        {
            DateTime d = new DateTime(date.Year, date.Month, date.Day);
            bool ret = false;
            var holidays = Session.Query<HolidaySetting>().Where(x => x.DateFrom >= d && x.DateTo <= d
                                                                && x.Organization.Id == organizationId
                                                                && x.Status == HolidaySetting.EntityStatus.Active);

            if (holidays.ToList().Count > 0)
                ret = true;
            return ret;
        }

        public bool CheckExistingHoliday( long organizationId, string name,DateTime dateFrom, long? id = null)
        {
            List<HolidaySetting> checkExistingHoliday;
            if (id != null)
            {
                checkExistingHoliday = Session.QueryOver<HolidaySetting>()
                    .Where(
                        x =>
                            x.Id != id
                            && x.Organization.Id == organizationId
                            && x.Name == name
                            && x.DateFrom.Date == dateFrom.Date 
                            && x.Status == HolidaySetting.EntityStatus.Active 
                            ).List<HolidaySetting>().ToList();
            }
            else
            {
                checkExistingHoliday = Session.QueryOver<HolidaySetting>()
                     .Where(
                         x =>
                             x.Organization.Id == organizationId 
                             && x.Name == name 
                             && x.DateFrom.Date == dateFrom.Date 
                             && x.Status == HolidaySetting.EntityStatus.Active
                             ).List<HolidaySetting>().ToList(); 
            }
            if (checkExistingHoliday.Count > 0)
            {
                return false;
            }
            return true;
        }

        //public int GetHolidayCount(long? organizationId, int? month, int? year)
        public int GetHolidayCount(List<long> organizationIdList, int? month, int? year)
        {
            var holidays = Session.Query<HolidaySetting>().Where(x => x.Status == HolidaySetting.EntityStatus.Active);
            //if (organizationId != null)
            //{
            //    holidays = holidays.Where(x => x.Organization.Id == organizationId);
            //}
            if (organizationIdList != null)
            {
                holidays = holidays.Where(x => organizationIdList.Contains(x.Organization.Id));
            }
            if (month != null)
            {
                holidays = holidays.Where(x => x.DateFrom.Month == month);
            }
            if (year != null)
            {
                holidays = holidays.Where(x => x.DateFrom.Year == year);
            }
            var holidayCount = holidays.Count();
            return holidayCount;
        }
        #endregion
    }

}
