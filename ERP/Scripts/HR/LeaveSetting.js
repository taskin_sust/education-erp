﻿$(document).ready(function () {
    //gender validation check 
    window.validateTextField("Name", "Please enter valid name.");
    window.validateTextField("MaxCarryDays", "Please enter Max. Days.");
    window.validateTextField("MaxTakingLimit", "Please enter Max. Days.");
    window.validateTextField("MinEncashReserveDays", "Please enter Max. Days.");

    window.validateTextField("effectiveDate", "Please enter effectiveDate");
    window.validateTextField("closingDate", "Please enter closingDate");

    window.validateTextField("NoOfDays", "Please enter valid Number of days.");
    window.validateDropDownField("OrganizationId", "Please Select Organization.");
    window.validateDropDownField("IsPublic", "Please Select Is Public.");
    window.validateDropDownField("PayType", "Please Select PayType.");
    window.validateDropDownField("RepeatType", "Please Select RepeatType.");
    window.validateDropDownField("StartFrom", "Please Select StartFrom.");

    $('#Err-Gender').css("display", "none");

    $("#chk-gender-both").click(function () {

        if ($(this).is(':checked')) {
            $(".chk-gender").each(function () {
                $(this).prop('checked', true);
                $('#Err-Gender').css("display", "none");
            });
        } else {
            $(".chk-gender").each(function () {
                $(this).prop('checked', false);
            });
        }
    });

    $(document).on('click', '.chk-gender', function () {
        console.log("clicked");
        var allchecked = true;
        $('.chk-gender').each(function (index, value) {
            if ($(this).is(':checked')) {
            } else {
                allchecked = false;
            }
        });
        if (allchecked) {
            $('#chk-gender-both').prop('checked', true);
        } else {
            $('#chk-gender-both').prop('checked', false);
        }

    });

    $("#chk-employee-status").click(function () {
        if ($(this).is(':checked')) {
            $(".chk-member-status").each(function () {
                $(this).prop('checked', true);
                $('#Err-empType').css("display", "none");
            });
        } else {
            $(".chk-member-status").each(function () {
                $(this).prop('checked', false);
            });
        }
    });

    $(document).on('click', '.chk-member-status', function () {
        console.log("clicked");
        var allchecked = true;
        $('.chk-member-status').each(function (index, value) {
            if ($(this).is(':checked')) {
            } else {
                allchecked = false;
            }
        });
        if (allchecked) {
            $('#chk-employee-status').prop('checked', true);
        } else {
            $('#chk-employee-status').prop('checked', false);
        }
    });

    $("#chk-marital-status").click(function () {

        if ($(this).is(':checked')) {
            $(".chk-member-maritalstatus").each(function () {
                $(this).prop('checked', true);
                $('#Err-MaritalStatus').css("display", "none");
            });
        } else {
            $(".chk-member-maritalstatus").each(function () {
                $(this).prop('checked', false);
            });
        }
    });

    $(document).on('click', '.chk-member-maritalstatus', function () {
        console.log("clicked");
        var allchecked = true;
        $('.chk-member-maritalstatus').each(function (index, value) {
            if ($(this).is(':checked')) {
            } else {
                allchecked = false;
            }
        });
        if (allchecked) {
            $('#chk-marital-status').prop('checked', true);
        } else {
            $('#chk-marital-status').prop('checked', false);
        }
    });

    $("input[name=cf-inlineRadioOptions]:radio").change(function () {

        var value = $(this).val();
        if (value == "1") {
            $(this).attr('checked', 'checked');
        } else {
            $("#MaxCarryDays").val("");
            $('#MaxCarryDays:input').attr('disabled', 'disabled');
            //$('div').removeClass('highlight');
            $("#MaxCarryDays_err_div").css('display', 'none');
            return;
        }
        if ($("#r1").attr("checked")) {
            $('#MaxCarryDays:input').removeAttr('disabled');
        } else {
            $('#MaxCarryDays:input').attr('disabled', 'disabled');
        }
    });

    $("input[name=tl-inlineRadioOptions]:radio").change(function () {
        var value = $(this).val();
        if (value == "1") {
            $(this).attr('checked', 'checked');
        } else {
            $("#MaxTakingLimit").val("");
            $("#MaxTakingLimit_err_div").css('display', 'none');
            $('#MaxTakingLimit:input').attr('disabled', 'disabled');
            return;
        }
        if ($("#r_tak1").attr("checked")) {
            $('#MaxTakingLimit:input').removeAttr('disabled');
        } else {
            $('#MaxTakingLimit:input').attr('disabled', 'disabled');
        }
    });

    $("input[name=ea-inlineRadioOptions]:radio").change(function () {

        var value = $(this).val();
        if (value == "1") {

            $(this).attr('checked', 'checked');
        } else {
            $("#MinEncashReserveDays").val("");
            $("#MinEncashReserveDays_err_div").css('display', 'none');
            $('#MinEncashReserveDays:input').attr('disabled', 'disabled');
            return;
        }
        if ($("#r_en1").attr("checked")) {
            $('#MinEncashReserveDays:input').removeAttr('disabled');
        } else {
            $('#MinEncashReserveDays:input').attr('disabled', 'disabled');
        }
    });

    $("#PayType").change(function () {
        if ($(this).val() == "1") {
            $('.withPaySelection-hide').show();

        } else {
            $('.withPaySelection-hide').hide();
        }
    });

    $("#IsPublic").change(function () {
        $("#PayType").val('');
    });

    function validateNewRegistrationForm() {

        var org = $('#OrganizationId').val();
        var leaveName = $('#Name').val();
        var isPub = $('#IsPublic').val();
        var payType = $('#PayType').val();
        var repeatType = $('#RepeatType').val();
        var noOfDays = $('#NoOfDays').val();
        var startFrom = $('#StartFrom').val();
        var effectiveDate = $('#effectiveDate').val();
        //var closingDate = $('#closingDate').val();

        if (org.length <= 0) {
            window.showErrorMessageBelowCtrl("OrganizationId", "", false);
            window.showErrorMessageBelowCtrl("OrganizationId", "Please Select Organization", true);
            return false;
        } else {
            window.showErrorMessageBelowCtrl("OrganizationId", "", false);
        }
        if (leaveName.length <= 0) {
            window.showErrorMessageBelowCtrl("Name", "", false);
            window.showErrorMessageBelowCtrl("Name", "Leave Name Missing", true);
            return false;
        } else {
            window.showErrorMessageBelowCtrl("Name", "", false);
        }
        if (isPub.length <= 0) {
            window.showErrorMessageBelowCtrl("IsPublic", "", false);
            window.showErrorMessageBelowCtrl("IsPublic", "Selection Missing", true);
            return false;
        } else {
            window.showErrorMessageBelowCtrl("IsPublic", "", false);
        }
        if (payType.length <= 0) {
            window.showErrorMessageBelowCtrl("PayType", "", false);
            window.showErrorMessageBelowCtrl("PayType", "Please Select PayType", true);
            return false;
        } else {
            window.showErrorMessageBelowCtrl("PayType", "", false);
        }
        if (repeatType.length <= 0) {
            window.showErrorMessageBelowCtrl("RepeatType", "", false);
            window.showErrorMessageBelowCtrl("RepeatType", "Please Select RepeatType", true);
            return false;
        } else {
            window.showErrorMessageBelowCtrl("RepeatType", "", false);
        }
        if (noOfDays.length <= 0) {
            window.showErrorMessageBelowCtrl("NoOfDays", "", false);
            window.showErrorMessageBelowCtrl("NoOfDays", "Days Missing", true);
            return false;
        } else {
            window.showErrorMessageBelowCtrl("NoOfDays", "", false);
        }
        if (startFrom.length <= 0) {
            window.showErrorMessageBelowCtrl("StartFrom", "", false);
            window.showErrorMessageBelowCtrl("StartFrom", "Please Select StartFrom", true);
            return false;
        } else {
            window.showErrorMessageBelowCtrl("StartFrom", "", false);
        }
        if (effectiveDate.length <= 0) {
            window.showErrorMessageBelowCtrl("effectiveDate", "", false);
            window.showErrorMessageBelowCtrl("effectiveDate", "Please Select Effective Date", true);
            return false;
        } else {
            window.showErrorMessageBelowCtrl("effectiveDate", "", false);
        }
        if (closingDate.length <= 0) {
            window.showErrorMessageBelowCtrl("closingDate", "", false);
            window.showErrorMessageBelowCtrl("closingDate", "Please Select Closing Date", true);
            return false;
        } else {
            window.showErrorMessageBelowCtrl("closingDate", "", false);
        }


        var allUnchecked = true;
        $('.chk-gender').each(function (index, value) {
            if ($(this).is(':checked')) {
                $('#Err-Gender').css("display", "none");
                //showErrorMessageBelowCtrl("chk-gender-both", "", false);
                allUnchecked = false;
            }
        });
        if (allUnchecked) {
            $('#Err-Gender').css("display", "block");
            //showErrorMessageBelowCtrl("chk-gender-both", "", false);
            //showErrorMessageBelowCtrl("chk-gender-both", "Please Select Gender", true);
            return false;
        }
        var allUncheckedEmpType = true;
        $('.chk-member-status').each(function (index, value) {
            if ($(this).is(':checked')) {
                $('#Err-empType').css("display", "none");
                //showErrorMessageBelowCtrl("chk-employee-status", "", false);
                allUncheckedEmpType = false;
            }
        });
        if (allUncheckedEmpType) {
            $('#Err-empType').css("display", "block");
            //showErrorMessageBelowCtrl("chk-employee-status", "", false);
            //showErrorMessageBelowCtrl("chk-employee-status", "Please Select Employee Type", true);
            return false;
        }
        var allUncheckedMaritalStatus = true;
        $('.chk-member-maritalstatus').each(function (index, value) {
            if ($(this).is(':checked')) {
                $('#Err-MaritalStatus').css("display", "none");
                //showErrorMessageBelowCtrl("chk-marital-status", "", false);
                allUncheckedMaritalStatus = false;
            }
        });
        if (allUncheckedMaritalStatus) {
            $('#Err-MaritalStatus').css("display", "block");
            //showErrorMessageBelowCtrl("chk-marital-status", "", false);
            //showErrorMessageBelowCtrl("chk-marital-status", "Please Select Marital Status", true);
            return false;
        }

        var selectedValueCarry = $("input[name='cf-inlineRadioOptions']:checked").val();
        console.log(selectedValueCarry);
        if (selectedValueCarry == "1") {
            var maxCarryDays = $('#MaxCarryDays').val();
            if (maxCarryDays.length <= 0) {
                window.showErrorMessageBelowCtrl("MaxCarryDays", "", false);
                window.showErrorMessageBelowCtrl("MaxCarryDays", "Max. Days Missing", true);
                return false;
            } else {
                window.showErrorMessageBelowCtrl("MaxCarryDays", "", false);
            }
        }
        var selectedValuetl = $("input[name='tl-inlineRadioOptions']:checked").val();
        console.log(selectedValuetl);
        if (selectedValuetl == "1") {
            var maxCarryDaystl = $('#MaxTakingLimit').val();
            if (maxCarryDaystl.length <= 0) {
                window.showErrorMessageBelowCtrl("MaxTakingLimit", "", false);
                window.showErrorMessageBelowCtrl("MaxTakingLimit", "Max. Days Missing", true);
                return false;
            } else {
                window.showErrorMessageBelowCtrl("MaxTakingLimit", "", false);
            }
        }

        var selectedValueea = $("input[name='ea-inlineRadioOptions']:checked").val();
        console.log(selectedValueea);
        if (selectedValueea == "1") {
            var maxCarryDaysea = $('#MinEncashReserveDays').val();
            if (maxCarryDaysea.length <= 0) {
                window.showErrorMessageBelowCtrl("MinEncashReserveDays", "", false);
                window.showErrorMessageBelowCtrl("MinEncashReserveDays", "Max. Days Missing", true);
                return false;
            } else {
                window.showErrorMessageBelowCtrl("MinEncashReserveDays", "", false);
            }
        }

        var selectedValueat = $("input[name='at-inlineRadioOptions']:checked").val();

        var maxCarryDaysat = $('#ApplicationDays').val();
        if (maxCarryDaysat.length <= 0) {
            window.showErrorMessageBelowCtrl("ApplicationDays", "", false);
            window.showErrorMessageBelowCtrl("ApplicationDays", "Max. Days Missing", true);
            return false;
        } else {
            window.showErrorMessageBelowCtrl("ApplicationDays", "", false);
        }

        return true;
    }

    function WithoutPayValidation(isPublic) {
        var orgP = $('#OrganizationId').val();
        var leaveNameP = $('#Name').val();
        var isPubP = $('#IsPublic').val();
        //validation check
        if (orgP.length <= 0) {
            showErrorMessageBelowCtrl("OrganizationId", "", false);
            showErrorMessageBelowCtrl("OrganizationId", "Please Select Organization", true);
            return false;
        } else {
            showErrorMessageBelowCtrl("OrganizationId", "", false);
        }
        if (leaveNameP.length <= 0) {
            showErrorMessageBelowCtrl("Name", "", false);
            showErrorMessageBelowCtrl("Name", "Leave Name Missing", true);
            return false;
        } else {
            showErrorMessageBelowCtrl("Name", "", false);
        }
        if (isPublic == 1) {
            if (isPubP.length <= 0) {
                showErrorMessageBelowCtrl("IsPublic", "", false);
                showErrorMessageBelowCtrl("IsPublic", "Selection Missing", true);
                return false;
            } else {
                showErrorMessageBelowCtrl("IsPublic", "", false);

            }
            //var allUnchecked = true;
            //$('.chk-gender').each(function (index, value) {
            //    if ($(this).is(':checked')) {
            //        $('#Err-Gender').css("display", "none");
            //        allUnchecked = false;
            //    }
            //});
            //if (allUnchecked) {
            //    $('#Err-Gender').css("display", "block");
            //    return false;
            //}
        }
        return true;
    }

    $(document).on('click', '#leaveBtn', function () {

        var payType = $('#PayType').val();
        var orgP = $('#OrganizationId').val();
        var leaveNameP = $('#Name').val();
        var isPubP = $('#IsPublic').val();
        var id = $('#id').val();
        //alert(isPub);
        if (isPubP.length <= 0) {
            showErrorMessageBelowCtrl("IsPublic", "", false);
            showErrorMessageBelowCtrl("IsPublic", "Please Select IsPublic", true);
            return false;
        }
        else {
            showErrorMessageBelowCtrl("IsPublic", "", false);
        }
        if (isPubP == 1) {
            console.log("Is Public true");
            if (payType.length <= 0) {
                showErrorMessageBelowCtrl("PayType", "", false);
                showErrorMessageBelowCtrl("PayType", "Please Select PayType", true);
                return false;
            } else {
                showErrorMessageBelowCtrl("PayType", "", false);
            }
        }
        if (payType == "0" || isPubP == "0") {
            //alert();
            console.log("wpv" + WithoutPayValidation(isPubP));
            if (WithoutPayValidation(isPubP)) {
                //alert();
                var genAryP = [];
                //$('.chk-gender').each(function (index, value) {
                //    if ($(this).is(':checked')) {
                //        genAryP[index] = $(this).val();
                //        index++;
                //    }
                //});
                //genAryP = $('.chk-gender:checkbox:checked').map(function () {
                //    return this.value;
                //}).get();
                var appTypeP = $('input[name=at-inlineRadioOptions]:checked').val();
                var appTypeDaysP = $('#ApplicationDays').val();
                var url = "/Hr/LeaveSetting/LeaveSettingWithoutPay";
                var data = { orgP: orgP, leaveNameP: leaveNameP, isPubP: isPubP, payType: payType, genAryP: genAryP, appTypeP: appTypeP, appTypeDaysP: appTypeDaysP };
                if (id > 0) {
                    url = "/Hr/LeaveSetting/EditLeaveSetting";
                    data = { id: id, org: orgP, leaveName: leaveNameP, isPub: isPubP, payType: payType, genAry: genAryP, appType: appTypeP, appTypeDays: appTypeDaysP };
                }
                $.ajax({
                    url: url,
                    type: "POST",
                    data: data,
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (data) {
                        $.unblockUI();
                        if (data.IsSuccess) {
                            if (id > 0) {
                                window.location.href = "/Hr/LeaveSetting/ManageLeave?message=Leave edited successfully";
                            } else {
                                window.location.href = "/Hr/LeaveSetting/ManageLeave?message=Leave added successfully";
                            }

                            //showMessage(data.Message, "success");

                        } else {
                            showMessage(data.Message, "error");
                        }
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (data) {
                        $.unblockUI();
                        bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                    }
                });
            }
        }
        isPub = $('#IsPublic').val();
        console.log("payt" + payType + ",ispub" + isPub);

        if (isPub == "1" && payType == "1") {

            if (validateNewRegistrationForm()) {
                //alert("Entered");
                var org = $('#OrganizationId').val();
                var leaveName = $('#Name').val();
                var isPub = $('#IsPublic').val();
                //var payType = $('#PayType').val();
                var repeatType = $('#RepeatType').val();
                var noOfDays = $('#NoOfDays').val();
                var startFrom = $('#StartFrom').val();
                var effectiveDate = $('#effectiveDate').val();
                var closingDate = $('#closingDate').val();
                var genAry = [];
                var empStatus = [];
                var maritalStatus = [];
                //$('.chk-gender').each(function (index, value) {
                //    if ($(this).is(':checked')) {
                //        genAry[index] = $(this).val();
                //        index++;
                //    }
                //});
                var genAry = $('.chk-gender:checkbox:checked').map(function () {
                    return this.value;
                }).get();
                var empStatus = $('.chk-member-status:checkbox:checked').map(function () {
                    return this.value;
                }).get();
                var maritalStatus = $('.chk-member-maritalstatus:checkbox:checked').map(function () {
                    return this.value;
                }).get();
                //$('.chk-member-status').each(function (index1, value1) {
                //    if ($(this).is(':checked')) {
                //        empStatus[index1] = $(this).val();
                //        index1++;
                //    }
                //});
                //$('.chk-member-maritalstatus').each(function (index2, value2) {
                //    if ($(this).is(':checked')) {
                //        maritalStatus[index2] = $(this).val();
                //        index2++;
                //    }
                //});
                var isForward = $('input[name=cf-inlineRadioOptions]:checked').val();
                var maxForward = "";
                if (isForward == "1") {
                    maxForward = $('#MaxCarryDays').val();
                }

                var isTakingLimit = $('input[name=tl-inlineRadioOptions]:checked').val();
                var maxTaking = "";
                if (isTakingLimit == "1") {
                    maxTaking = $('#MaxTakingLimit').val();
                }

                var appType = $('input[name=at-inlineRadioOptions]:checked').val();
                var appTypeDays = $('#ApplicationDays').val();

                var isEncash = $('input[name=ea-inlineRadioOptions]:checked').val();
                var maxEncash = "";
                if (isEncash == "1") {
                    maxEncash = $('#MinEncashReserveDays').val();
                }
                url = "/Hr/LeaveSetting/LeaveSetting";
                data = { org: org, leaveName: leaveName, isPub: isPub, payType: payType, repeatType: repeatType, noOfDays: noOfDays, startFrom: startFrom, effectiveDate: effectiveDate, genAry: genAry, empStatus: empStatus, maritalStatus: maritalStatus, isForward: isForward, maxForward: maxForward, isTakingLimit: isTakingLimit, maxTaking: maxTaking, appType: appType, appTypeDays: appTypeDays, isEncash: isEncash, maxEncash: maxEncash, closingDate: closingDate };
                if (id > 0) {
                    url = "/Hr/LeaveSetting/EditLeaveSetting";
                    data = { id: id, org: org, leaveName: leaveName, isPub: isPub, payType: payType, repeatType: repeatType, noOfDays: noOfDays, startFrom: startFrom, effectiveDate: effectiveDate, genAry: genAry, empStatus: empStatus, maritalStatus: maritalStatus, isForward: isForward, maxForward: maxForward, isTakingLimit: isTakingLimit, maxTaking: maxTaking, appType: appType, appTypeDays: appTypeDays, isEncash: isEncash, maxEncash: maxEncash, closingDate: closingDate };
                }
                $.ajax({
                    url: url,
                    type: "POST",
                    data: data,
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (data) {
                        $.unblockUI();
                        if (data.IsSuccess) {
                            if (id > 0) {
                                window.location.href = "/Hr/LeaveSetting/ManageLeave?message=Leave edited successfully";
                            } else {
                                window.location.href = "/Hr/LeaveSetting/ManageLeave?message=Leave added successfully";
                            }

                        } else {
                            showMessage(data.Message, "error");
                        }
                        //ResetExceptProgram();
                        //$('.courseDetails').html(data.CourseView);
                        //$('#Session').html(data.SessionOptions);
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (data) {
                        $.unblockUI();
                        bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                    }
                });
                //alert(isForward);
            }
        }
    });

    $(document).on("input", ".numeric", function (e) {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    var MESSAGE_SHOW_INTERVAL = 10000;

    function showMessage(msg, type) {
        if (type == "success") {
            $(".customMessage").append('<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><strong>Success!</strong> ' + msg + '.</div>');
        }
        else if (type == "error") {
            $(".customMessage").append('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><strong>Error!</strong> ' + msg + '.</div>');
        }
        scrollToElement(".customMessage");
        setTimeout(function () {
            $(".customMessage").html("");
        }, MESSAGE_SHOW_INTERVAL);
    }

    $(document).on('click', '#indvLeaveSettingBtn', function () {
        var memberId = $('#memberObj').val();
        var orgId = $('#orgObj').val();

        var leaveOrgId = [];
        $('.LeaveOrgId').each(function (index, value) {
            leaveOrgId[index] = $(this).val();
            index++;
            //console.log($(this).val());  
        });

        var leaveId = [];
        $('.LeaveId').each(function (index, value) {
            leaveId[index] = $(this).val();
            index++;
            //console.log($(this).val());  
        });

        var carryForwardLeave = [];
        $('.CarryForwardLeave').each(function (index, value) {
            carryForwardLeave[index] = $(this).val();
            index++;
            //console.log($(this).val());  
        });

        var currentYearLeave = [];
        $('.CurrentYearLeave').each(function (index, value) {
            currentYearLeave[index] = $(this).val();
            index++;
            //console.log($(this).val());  
        });

        var encashLeave = [];
        $('.EncashLeave').each(function (index, value) {
            encashLeave[index] = $(this).val();
            index++;
            //console.log($(this).val());  
        });

        var voidLeave = [];
        $('.VoidLeave').each(function (index, value) {
            voidLeave[index] = $(this).val();
            index++;
            //console.log($(this).val());  
        });

        var totalLeaveBalance = [];
        $('.TotalLeaveBalance').each(function (index, value) {
            totalLeaveBalance[index] = $(this).val();
            index++;
            //console.log($(this).val());  
        });

        var approvedLeave = [];
        $('.ApprovedLeave').each(function (index, value) {
            approvedLeave[index] = $(this).val();
            index++;
            //console.log($(this).val());  
        });

        var pendingLeave = [];
        $('.PendingLeave').each(function (index, value) {
            pendingLeave[index] = $(this).val();
            index++;
            //console.log($(this).val());  
        });

        
        //var genAryP = [];
        //$('.leaveCal input').each(function (index, value) {
        //    genAryP[index] = $(this).attr('data-leaveid') + "#" + $(this).val();
        //    index++;
        //});

        var urlDir = "/Hr/LeaveSetting/SetIndividualLeaveSetting";
        var dataObj = { orgId: orgId, memberId: memberId, leaveOrgId: leaveOrgId, leaveId: leaveId, carryForwardLeave: carryForwardLeave, currentYearLeave: currentYearLeave, encashLeave: encashLeave, voidLeave: voidLeave, totalLeaveBalance: totalLeaveBalance, approvedLeave: approvedLeave, pendingLeave: pendingLeave };
        $.ajax({
            url: urlDir,
            type: "POST",
            data: dataObj,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $.unblockUI();
                if (data.IsSuccess) {
                    //showMessage(data.Message, "success");
                    window.location = window.location.href + "&message=" + data.Message;
                } else {

                    showMessage(data.Message, "error");
                }
            },
            complete: function () {
                $.unblockUI();
                
            },
            error: function (data) {
                $.unblockUI();
                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
            }
        });

    });

    $('.form_datetime').datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: false,
        showMeridian: true,
        initialDate: new Date(),
        startView: 2,
        minView: 2,
        maxView: 4
    });

    //by sajjad
    $("#IsPublic").change(function () {
        if ($(this).val() == "1") {
            $('.isPublicSelection-Hide').show();

        } else {
            $('.isPublicSelection-Hide').hide();
        }
    });
    //by sajjad

});