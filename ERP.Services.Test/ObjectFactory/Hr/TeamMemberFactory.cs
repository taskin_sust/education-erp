﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UserAuth;
using UdvashERP.Services.UserAuth;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class TeamMemberFactory : ObjectFactoryBase<TeamMember>
    {
        private DateTime _startDate { get; set; }
        private DateTime _endDate { get; set; }

        private readonly ITeamMemberService _teamMemberService;
        private readonly IUserService _userService;

        private readonly UserProfileFactory _userProfileFactory;
        private readonly CommonFactory _commonFactory;

        private readonly TestBaseService<AttendanceSummary> _attendanceSummaryService;

        public List<Designation> DesignationList { get; set; }
        public List<MaritalInfo> MaritalInfoList { get; set; }
        public List<ShiftWeekendHistory> ShiftWeekendHistoryList { get; set; }

        public TeamMemberFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _teamMemberService = new TeamMemberService(session);
            _userService = new UserService(session);
            _userProfileFactory = new UserProfileFactory(this, session);
            _attendanceSummaryService = new TestBaseService<AttendanceSummary>(session);
            _commonFactory = new CommonFactory(this, session);
        }

        #region Create

        internal string GetRandomNumber()
        {
            Random r = new Random();
            var x = r.Next(0, 1000000);
            string s = x.ToString("000000");
            return s;
        }

        internal string GetRandomNumber(int min, int max)
        {
            Random r = new Random();
            var x = r.Next(min, 1000000 + max);
            string s = x.ToString("000000");
            return s;
        }

        internal int GetNewPin()
        {
            return _teamMemberService.GetNewPin();
        }

        public TeamMemberFactory Create()
        {
            Object = new TeamMember()
            {
                PersonalContact = "8801841" + GetRandomNumber(),
                FullNameEng = "MIKE TYSON",
                Name = "MIKE",
                FullNameBang = "টাইসন",
                Facebook = "https://www.facebook.com/",
                Skype = "tyson_usa",
                PersonalEmail = "tyson@onnorokom.com",
                Gender = (int)Gender.Male,
                Religion = (int)Religion.Islam,
                BloodGroup = (int)BloodGroup.Bpos,
                Nationality = (int)Nationality.Bangladeshi,
                NID = "11235813213455",
                BCNo = "112358",
                PassportNo = "c1498982",
                CertificateDOB = DateTime.Now.AddYears(-25),
                ActualDOB = DateTime.Now.AddYears(-25),
                PresentAddress = "H-22,R-3 Dhaka",
                PermanentAddress = "H-22,R-09,Dhaka",
                EmargencyContactName = "ONNOROKOM GROUP",
                EmargencyContactRelation = "EMPLOYEE",
                EmargencyContactCellNo = "01811419555",
                FatherNameBang = "সিনিওর টাইসন",
                FatherNameEng = "SR.TYSON",
                MotherNameBang = "সুইফট",
                MotherNameEng = "SWIFT",
                FatherContactNo = "01811111111",
                MotherContactNo = "01811111111",
                Specialization = "NIL",
                ExtracurricularActivities = "NIL",
                Pin = GetNewPin(),
                BusinessId = GetPcUserName(),
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public TeamMemberFactory WithIsSalarySheet(bool isSalarySheet)
        {
            Object.IsSalarySheet = isSalarySheet;
            return this;
        }

        public TeamMemberFactory WithIsAutoDeduction(bool isAutoDeduction)
        {
            Object.IsAutoDeduction = isAutoDeduction;
            return this;
        }

        public TeamMemberFactory WithPersonalContact(int personalContct)
        {
            Object.PersonalContact = "880184100000" + personalContct;
            return this;
        }

        public TeamMemberFactory CreateWith(long id, int pin, string personalContact, string fullNameEng, string fullNameBang, string facebook, string skype, string personalEmail,
            int gender, int religion, int bloodGroup, int nationality, string nid, string bcNo, string passportNo, DateTime certificateDob, DateTime actualDob,
            string presentAddress, string permanentAddress, string emargencyContactName, string emargencyContactRelation, string emargencyContactCellNo,
            string fatherNameBang, string fatherNameEng, string motherNameBang, string motherNameEng, string fatherContactNo, string motherContactNo, string specialization,
            string extracurricularActivities)
        {
            Object = new TeamMember()
            {
                Id = id,
                PersonalContact = personalContact,
                FullNameEng = fullNameEng,
                FullNameBang = fullNameBang,
                Facebook = facebook,
                Skype = skype,
                PersonalEmail = personalEmail,
                Gender = (int)gender,
                Religion = (int)religion,
                BloodGroup = (int)bloodGroup,
                Nationality = (int)nationality,
                NID = nid,
                BCNo = bcNo,
                PassportNo = passportNo,
                CertificateDOB = certificateDob,
                ActualDOB = actualDob,
                PresentAddress = presentAddress,
                PermanentAddress = permanentAddress,
                EmargencyContactName = emargencyContactName,
                EmargencyContactRelation = emargencyContactRelation,
                EmargencyContactCellNo = emargencyContactCellNo,
                FatherNameBang = fatherNameBang,
                FatherNameEng = fatherNameEng,
                MotherNameBang = motherNameBang,
                MotherNameEng = motherNameEng,
                FatherContactNo = fatherContactNo,
                MotherContactNo = motherContactNo,
                Specialization = specialization,
                ExtracurricularActivities = extracurricularActivities,
                Pin = (int)pin,
                BusinessId = GetPcUserName(),
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public TeamMemberFactory WithTaxInformation(string taxesCircle, string taxesZone, string tinNo)
        {
            Object.TaxesCircle = taxesCircle;
            Object.TaxesZone = taxesZone;
            Object.TinNo = tinNo;
            return this;
        }

        public TeamMemberFactory WithUserProfile()
        {
            _userProfileFactory.Create().WithCampus().Persist();
            Object.UserProfile = _userProfileFactory.Object;
            return this;
        }

        public TeamMemberFactory WithUserProfile(Campus campus)
        {
            AspNetUser baseUser = _userService.LoadbyAspNetUserId(72);
            _userProfileFactory.Create().WithCampus(campus).WithBranch(campus.Branch).WithAspNetUser(baseUser).Persist();
            Object.UserProfile = _userProfileFactory.Object;
            return this;
        }

        public TeamMemberFactory WithPostoffice()
        {
            var prepostOffice = _commonFactory.WithPostOffice();
            var perpostOffice = _commonFactory.WithPostOffice();
            if (prepostOffice != null && perpostOffice != null)
            {
                Object.PresentPostOffice = prepostOffice;
                Object.PermanentPostOffice = perpostOffice;
            }
            return this;
        }

        public TeamMemberFactory WithEmploymentHistory(List<EmploymentHistory> listObj)
        {
            Object.EmploymentHistory = listObj;
            return this;
        }
        public TeamMemberFactory WithSalaryHistory(List<SalaryHistory> listObj)
        {
            Object.SalaryHistory = listObj;
            return this;
        }

        public TeamMemberFactory WithMaritalInfo(List<MaritalInfo> listObj)
        {
            Object.MaritalInfo = listObj;
            return this;
        }

        public TeamMemberFactory WithMentorHistory(List<MentorHistory> listObj)
        {
            Object.MentorHistory = listObj;
            return this;
        }
        public TeamMemberFactory WithShiftWeekend(IList<ShiftWeekendHistory> objList)
        {
            Object.ShiftWeekendHistory = objList;
            return this;
        }

        public TeamMemberFactory WithEffectiveStartAndEndDate(DateTime startDate, DateTime endDate)
        {
            this._startDate = startDate;
            this._endDate = endDate;
            return this;
        }

        public TeamMemberFactory WithMemberOfficialDetails(IList<MemberOfficialDetail> objList)
        {
            Object.MemberOfficialDetails = objList;
            return this;
        }

        public TeamMemberFactory Persist()
        {
            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _teamMemberService.SaveNew(item, this._startDate.ToString("yyyy-MM-dd"), this._endDate.ToString("yyyy-MM-dd"));
                    }
                }
            }

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _teamMemberService.SaveNew(item, this._startDate.ToString("yyyy-MM-dd"), this._endDate.ToString("yyyy-MM-dd"));
                    }
                }
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region CleanUp
        public void Cleanup()
        {
            DeleteAll();
            //_designationFactory.Cleanup();
            //_userProfileFactory.Cleanup();
            //_organizationFactory.Cleanup();
            //_employmentHistoryFactory.CleanUp();
            //_maritalInfoFactory.CleanUp();
            //_shiftWeekendHistoryFactory.CleanUp();
        }

        public void TeamMemberAttendanceSummaryCleanUp()
        {
            var adIdList = new List<long>();
            if (SingleObjectList.Count > 0)
            {
                var tmIds = SingleObjectList.Select(x => x.Id).ToList();
                IList<long> attSummaryIds = _session.QueryOver<AttendanceSummary>().Select(x => x.Id).Where(x => x.TeamMember.Id.IsIn(tmIds.ToArray())).List<long>();
                adIdList.AddRange(attSummaryIds);
            }
            if (ObjectList.Count > 0)
            {
                var tmIds = ObjectList.Select(x => x.Id).ToList();
                IList<long> attSummaryIds = _session.QueryOver<AttendanceSummary>().Select(x => x.Id).Where(x => x.TeamMember.Id.IsIn(tmIds.ToArray())).List<long>();
                adIdList.AddRange(attSummaryIds);
            }
            if (adIdList.Count > 0)
            {
                foreach (var id in adIdList)
                {
                    var tableName = GetEntityTableName(typeof(AttendanceSummary));
                    string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
            //_attendanceSummaryService.DeleteByIdList(adIdList.ToList());
        }

        public void TeamMemberChieldCleanup()
        {
            if (SingleObjectList.Count > 0)
            {
                //using (var txn = _session.BeginTransaction())
                //{
                foreach (var singleObject in SingleObjectList)
                {
                    if (singleObject.Id != 0)
                        GetValue(singleObject);
                }
                //txn.Commit();
                //}
            }
            if (ObjectList.Count > 0)
            {
                //using (var txn = _session.BeginTransaction())
                //{
                foreach (var singleObject in ObjectList)
                {
                    if (singleObject.Id != 0)
                        GetValue(singleObject);
                }
                //txn.Commit();
                //}
            }
        }

        private void GetValue(TeamMember singleObject)
        {
            TeamMember tm = singleObject;
            var swHistoryIdList =
                _session.QueryOver<ShiftWeekendHistory>()
                    .Select(x => x.Id)
                    .Where(x => x.TeamMember.Id == tm.Id || x.TeamMember.Id == null)
                    .List<long>();
            if (swHistoryIdList.Count > 0)
            {
                foreach (var id in swHistoryIdList)
                {
                    var tableName = GetEntityTableName(typeof(ShiftWeekendHistory));
                    string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
            var tdsHistoryIdList =
             _session.QueryOver<TdsHistory>()
               .Select(x => x.Id)
               .Where(x => x.TeamMember.Id == tm.Id || x.TeamMember.Id == null)
               .List<long>();
            if (tdsHistoryIdList.Count > 0)
            {
                foreach (var id in tdsHistoryIdList)
                {
                    var tableName = GetEntityTableName(typeof(TdsHistory));
                    string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
            var emHistoryIdList =
                _session.QueryOver<EmploymentHistory>()
                    .Select(x => x.Id)
                    .Where(x => x.TeamMember.Id == tm.Id || x.TeamMember.Id == null)
                    .List<long>();
            if (emHistoryIdList.Count > 0)
            {
                foreach (var id in emHistoryIdList)
                {
                    var tableName = GetEntityTableName(typeof(EmploymentHistory));
                    string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
            var mHistoryIdList =
                _session.QueryOver<MentorHistory>()
                    .Select(x => x.Id)
                    .Where(x => x.TeamMember.Id == tm.Id || x.TeamMember.Id == null)
                    .List<long>();
            if (mHistoryIdList.Count > 0)
            {
                foreach (var id in mHistoryIdList)
                {
                    var tableName = GetEntityTableName(typeof(MentorHistory));
                    string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }

            var mIIdList =
                _session.QueryOver<MaritalInfo>()
                    .Select(x => x.Id)
                    .Where(x => x.TeamMember.Id == tm.Id || x.TeamMember.Id == null)
                    .List<long>();
            if (mIIdList.Count > 0)
            {
                foreach (var id in mIIdList)
                {
                    var tableName = GetEntityTableName(typeof(MaritalInfo));
                    string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
            var moDList =
                _session.QueryOver<MemberOfficialDetail>()
                    .Select(x => x.Id)
                    .Where(x => x.TeamMember.Id == tm.Id || x.TeamMember.Id == null)
                    .List<long>();
            if (moDList.Count > 0)
            {
                foreach (var id in moDList)
                {
                    var tableName = GetEntityTableName(typeof(MemberOfficialDetail));
                    string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
            var lsList =
                _session.QueryOver<MemberLeaveSummary>()
                    .Select(x => x.Id)
                    .Where(x => x.TeamMember.Id == tm.Id || x.TeamMember.Id == null)
                    .List<long>();
            if (lsList.Count > 0)
            {
                foreach (var id in lsList)
                {
                    var tableName = GetEntityTableName(typeof(MemberLeaveSummary));
                    string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }

            //member ebf
            var ebfBalanceList =
               _session.QueryOver<MemberEbfBalance>()
                   .Select(x => x.Id)
                   .Where(x => x.TeamMember.Id == tm.Id || x.TeamMember.Id == null)
                   .List<long>();
            if (ebfBalanceList.Count > 0)
            {
                foreach (var id in ebfBalanceList)
                {
                    var tableName = GetEntityTableName(typeof(MemberEbfBalance));
                    string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }

            var macademicInfos =
                _session.QueryOver<AcademicInfo>()
                    .Select(x => x.Id)
                    .Where(x => x.TeamMember.Id == tm.Id || x.TeamMember.Id == null)
                    .List<long>();
            if (macademicInfos!=null && macademicInfos.Count > 0)
            {
                foreach (var id in macademicInfos)
                {
                    var tableName = GetEntityTableName(typeof(AcademicInfo));
                    string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }


        }

        public void CleanUpWithLeaveSummary()
        {
            if (SingleObjectList.Count > 0)
            {
                var tmIds = SingleObjectList.Select(x => x.Id).ToList();
                var leaveSummaryList = _session.QueryOver<MemberLeaveSummary>()
                   .Select(x => x.Id)
                   .Where(x => x.TeamMember.Id.IsIn(tmIds.ToArray()))
                   .List<MemberLeaveSummary>();
                if (leaveSummaryList.Count > 0)
                {
                    using (var txn = _session.BeginTransaction())
                    {
                        foreach (var item in leaveSummaryList)
                        {
                            var tableName = GetEntityTableName(typeof(MemberLeaveSummary));
                            string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + item.Id + ";";
                            _session.CreateSQLQuery(swData).ExecuteUpdate();
                            // _session.Delete(item);
                        }
                        txn.Commit();
                    }
                }
            }
            if (ObjectList.Count > 0)
            {
                var tmIds = ObjectList.Select(x => x.Id).ToList();
                var leaveSummaryList = _session.QueryOver<MemberLeaveSummary>()
                   .Select(x => x.Id)
                   .Where(x => x.TeamMember.Id.IsIn(tmIds.ToArray()))
                   .List<MemberLeaveSummary>();
                if (leaveSummaryList.Count > 0)
                {
                    using (var txn = _session.BeginTransaction())
                    {
                        foreach (var item in leaveSummaryList)
                        {
                            var tableName = GetEntityTableName(typeof(MemberLeaveSummary));
                            string swData = "DELETE FROM " + tableName + " WHERE [Id] = " + item.Id + ";";
                            _session.CreateSQLQuery(swData).ExecuteUpdate();
                            //_session.Delete(item);
                        }
                        txn.Commit();
                    }
                }
            }
        }

        #endregion

        public TeamMemberFactory WithChildrenInfo(List<ChildrenInfo> list)
        {
            Object.ChildrenInfo = list;
            return this;
        }
    }
}
