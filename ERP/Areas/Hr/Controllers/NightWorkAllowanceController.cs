﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Authentication;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class NightWorkAllowanceController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> authorizeMenu;
        private ISession _session;

        private readonly IOrganizationService _organizationService;
        private readonly INightWorkAllowanceSettingService _nightWorkAllowanceSettingService;
        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public NightWorkAllowanceController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();

            _organizationService = new OrganizationService(_session);
            _nightWorkAllowanceSettingService = new NightWorkAllowanceSettingService(_session);
            _userService = new UserService(_session);
        }

        public ActionResult Index()
        {
            return RedirectToAction("ManageNightWorkAllowance");
        }

        #endregion

        #region Index

      [HttpGet]
        public ActionResult ManageNightWorkAllowance()
        {
            try
            {
                Initialize();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult ManageNightWorkAllowance(int draw, int start, int length, long? organizationId, int? empStatus)
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<NightWorkAllowanceSetting> cEAllowances = _nightWorkAllowanceSettingService.LoadAll(draw, start, length, authorizeMenu, organizationId, empStatus);
                int count = _nightWorkAllowanceSettingService.AllowanceCount(authorizeMenu, organizationId, empStatus);

                var data = new List<object>();
                foreach (var allowance in cEAllowances)
                {
                    var str = new List<string>();
                    var employeeStatus = "";
                    str.Add(allowance.Organization.ShortName);
                    str.Add(allowance.Name);
                    if (allowance.IsContractual) employeeStatus += MemberEmploymentStatus.Contractual + ", ";
                    if (allowance.IsPartTime) employeeStatus += MemberEmploymentStatus.PartTime + ", ";
                    if (allowance.IsPermanent) employeeStatus += MemberEmploymentStatus.Permanent + ", ";
                    if (allowance.IsProbation) employeeStatus += MemberEmploymentStatus.Probation + ", ";
                    if (allowance.IsIntern) employeeStatus += MemberEmploymentStatus.Intern + ", ";
                    str.Add(employeeStatus.Trim().Remove(employeeStatus.Length - 2));
                    str.Add(allowance.CalculationOn == (int)CalculationOn.Basic
                        ? CalculationOn.Basic.ToString()
                        : CalculationOn.Gross.ToString());
                    if (allowance.InHouseMultiplyingFactor == (decimal)MultiplyingFactor.One) { str.Add(MultiplyingFactor.One.ToString()); }
                    else if (allowance.InHouseMultiplyingFactor == (decimal)MultiplyingFactor.OneAndHalf) { str.Add(MultiplyingFactor.OneAndHalf.ToString()); }
                    else if (allowance.InHouseMultiplyingFactor == (decimal)MultiplyingFactor.Two) { str.Add(MultiplyingFactor.Two.ToString()); }
                    else if (allowance.InHouseMultiplyingFactor == (decimal)MultiplyingFactor.TwoAndHalf) { str.Add(MultiplyingFactor.TwoAndHalf.ToString()); }
                    else if (allowance.InHouseMultiplyingFactor == (decimal)MultiplyingFactor.Three) { str.Add(MultiplyingFactor.Three.ToString()); }

                    if (allowance.OutHouseMultiplyingFactor == (decimal)MultiplyingFactor.One) { str.Add(MultiplyingFactor.One.ToString()); }
                    else if (allowance.OutHouseMultiplyingFactor == (decimal)MultiplyingFactor.OneAndHalf) { str.Add(MultiplyingFactor.OneAndHalf.ToString()); }
                    else if (allowance.OutHouseMultiplyingFactor == (decimal)MultiplyingFactor.Two) { str.Add(MultiplyingFactor.Two.ToString()); }
                    else if (allowance.OutHouseMultiplyingFactor == (decimal)MultiplyingFactor.TwoAndHalf) { str.Add(MultiplyingFactor.TwoAndHalf.ToString()); }
                    else if (allowance.OutHouseMultiplyingFactor == (decimal)MultiplyingFactor.Three) { str.Add(MultiplyingFactor.Three.ToString()); }

                    str.Add(DateTime.MinValue != allowance.EffectiveDate ? allowance.EffectiveDate.ToString("MMM dd,yyyy") : " ");
                    str.Add(!String.IsNullOrEmpty(allowance.ClosingDate.ToString()) ? allowance.ClosingDate.Value.ToString("MMM dd,yyyy") : "");
                    str.Add(LinkGenerator.GetGeneratedDetailsEditLink("Details", "Edit", "NightWorkAllowance", allowance.Id)
                            + LinkGenerator.GetDeleteLink("Delete", "NightWorkAllowance", allowance.Id));
                    //str.Add("<a href='" + Url.Action("Edit", "NightWorkAllowance") + "?id=" + allowance.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp; ");

                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = count,
                    recordsFiltered = count,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                var data = new List<object>();
                _logger.Error(ex);
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });
            }
        }

        #endregion

        #region Operational Function

        #region Save

        public ActionResult Create(string message = "", int? type = 0)
        {
            try
            {
                Initialize();
                switch (type)
                {
                    case 1:
                        ViewBag.SuccessMessage = message;
                        break;
                    case 2:
                        ViewBag.ErrorMessage = message;
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NightworkAllowanceViewModel nightworkAllowanceViewModel)
        {
            var messsage = "";
            try
            {
                if (ModelState.IsValid)
                {
                    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    NightWorkAllowanceSetting nightWorkAllowanceSetting = FormModelFromVm(nightworkAllowanceViewModel);
                    _nightWorkAllowanceSettingService.SaveOrUpdate(authorizeMenu, nightWorkAllowanceSetting);
                    messsage = "Operation completed successfully";
                    return RedirectToAction("Create", new { message = messsage, type = 1 /*success*/ });
                }
                IEnumerable<ModelError> modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
                messsage = modelStateErrors.Aggregate(messsage, (current, modelStateError) => current + (modelStateError.ErrorMessage + " "));
                return RedirectToAction("Create", new { message = messsage, type = 2 /*failure*/ });
            }
            catch (AuthenticationException ae)
            {
               messsage = ae.Message;
            }
            catch (DuplicateEntryException dex)
            {
               messsage = dex.Message;
            }
            catch (InvalidDataException ide)
            {
               messsage = ide.Message;
            }
            catch (MessageException me)
            {
               messsage = me.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                messsage = ex.Message;
            }
            return RedirectToAction("Create", new { message = messsage, type = 2 /*fail*/});
        }


        #endregion

        #region Edit

        [HttpGet]
        public ActionResult Edit(long id, string message = "", int? type = 0)
        {
            NightworkAllowanceViewModel nightworkAllowanceViewModel = null;
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                NightWorkAllowanceSetting nightWorkAllowanceSetting = _nightWorkAllowanceSettingService.GetById(id);
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName", nightWorkAllowanceSetting.Organization.Id);
                ViewBag.employmentStatus = _commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int>() { (int)MemberEmploymentStatus.Intern, (int)MemberEmploymentStatus.Retired });
                ViewBag.calculationOnList = new SelectList(_commonHelper.LoadEmumToDictionary<CalculationOnTypeEnum>(), "Key", "Value", nightWorkAllowanceSetting.CalculationOn);
                var ihmFactor = nightWorkAllowanceSetting.InHouseMultiplyingFactor.ToString("0.#####");
                var ohmFactor = nightWorkAllowanceSetting.OutHouseMultiplyingFactor.ToString("0.#####");
                ViewBag.inHousemultiplyingFatcorList = new SelectList(_commonHelper.GetCalculationOn(), "Key", "Value", ihmFactor);
                ViewBag.outHousemultiplyingFatcorList = new SelectList(_commonHelper.GetCalculationOn(), "Key", "Value", ohmFactor);

                nightworkAllowanceViewModel = FormVmFromModel(nightWorkAllowanceSetting);
                switch (type)
                {
                    case 1:
                        ViewBag.SuccessMessage = "Updated Successfully";
                        break;
                    case 2:
                        ViewBag.ErrorMessage = message;
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View(nightworkAllowanceViewModel);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(NightworkAllowanceViewModel nightworkAllowanceViewModel)
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            var proxyObj = _nightWorkAllowanceSettingService.GetById(nightworkAllowanceViewModel.Id);
            var messsage = "";
            try
            {
                if (!ModelState.IsValid)
                {
                    IEnumerable<ModelError> modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
                    messsage = modelStateErrors.Aggregate(messsage, (current, modelStateError) => current + (modelStateError.ErrorMessage + " "));
                    return RedirectToAction("Create", new { message = messsage, type = 2 /*failure*/ });
                }
                proxyObj.Name = nightworkAllowanceViewModel.AllowanceName;
                proxyObj.MemberEmploymentStatuses = new List<MemberEmploymentStatus>();
                proxyObj.CalculationOn = nightworkAllowanceViewModel.CalculationOn;
                proxyObj.InHouseMultiplyingFactor = Convert.ToDecimal(nightworkAllowanceViewModel.InHouseMultiplyingFactor);
                proxyObj.OutHouseMultiplyingFactor = Convert.ToDecimal(nightworkAllowanceViewModel.OutHouseMultiplyingFactor);
                proxyObj.EffectiveDate = nightworkAllowanceViewModel.EffectiveFrom;
                proxyObj.ClosingDate = nightworkAllowanceViewModel.ClosingDate;
                proxyObj.MemberEmploymentStatuses = nightworkAllowanceViewModel.EmloymentStatus;
                _nightWorkAllowanceSettingService.SaveOrUpdate(authorizeMenu, proxyObj);
                messsage = "Operation completed successfully";
                return RedirectToAction("Edit", new { id = proxyObj.Id, message = messsage, type = 1 /*success*/ });
            }
            catch (NullObjectException nullObjectException)
            {
                messsage = nullObjectException.Message;
            }
            catch (InvalidDataException invalidDataException)
            {
                messsage = invalidDataException.Message;
            }
            catch (DuplicateEntryException duplicateEntryException)
            {
                messsage = duplicateEntryException.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                messsage = WebHelper.CommonErrorMessage;
            }
            return RedirectToActionPermanent("Edit", new { id = proxyObj.Id, message = messsage, type = 2 /*fail*/});
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                var entity = _nightWorkAllowanceSettingService.GetById(id);
                entity = GetEntityWithAdditionalList(entity);
                _nightWorkAllowanceSettingService.Delete(entity);
                return Json(new { isSuccess = true, Text = "Settings Successfully Delete!" });
            }
            catch (AuthenticationException ae)
            {
               return Json(new { isSuccess = false, Text = ae.Message });
            }
            catch (DuplicateEntryException dex)
            {
               return Json(new { isSuccess = false, Text = dex.Message });
            }
            catch (InvalidDataException ide)
            {
               return Json(new { isSuccess = false, Text = ide.Message });
            }
            catch (MessageException me)
            {
               return Json(new { isSuccess = false, Text = me.Message });
            }
            catch (DependencyException ex)
            {
               return Json(new { isSuccess = false, Text = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { isSuccess = false, Text = "Settings Delete Failed!" });
            }
        }

        #endregion

        #region Details
        public ActionResult Details(long id)
        {
            try
            {
                var entity = _nightWorkAllowanceSettingService.GetById(id);
                var fm = DetailsVmFromModel(entity);
                return View(fm);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("ManageNightWorkAllowance");
            }
        }

        #endregion

        #endregion

        #region Helper

        private void Initialize()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            ViewBag.employmentStatus = _commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int>() { (int)MemberEmploymentStatus.Retired });
            ViewBag.calculationOnList = new SelectList(_commonHelper.LoadEmumToDictionary<CalculationOnTypeEnum>(), "Key", "Value");
            ViewBag.inHousemultiplyingFatcorList = new SelectList(_commonHelper.LoadStructToDictinary<MultiplyingFactor>(), "Key", "Key");
            ViewBag.outHousemultiplyingFatcorList = new SelectList(_commonHelper.LoadStructToDictinary<MultiplyingFactor>(), "Key", "Key");
            ViewBag.employmentDropdownStatus = new SelectList(_commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int>() { (int)MemberEmploymentStatus.Retired }), "Key", "Value");
        }

        private NightWorkAllowanceSetting FormModelFromVm(NightworkAllowanceViewModel nightworkAllowanceViewModel)
        {
            return new NightWorkAllowanceSetting()
            {
                Organization = _organizationService.LoadById(nightworkAllowanceViewModel.OrganizationId),
                Name = nightworkAllowanceViewModel.AllowanceName,
                MemberEmploymentStatuses = nightworkAllowanceViewModel.EmloymentStatus,
                CalculationOn = nightworkAllowanceViewModel.CalculationOn,
                InHouseMultiplyingFactor = Convert.ToDecimal(nightworkAllowanceViewModel.InHouseMultiplyingFactor),
                OutHouseMultiplyingFactor = Convert.ToDecimal(nightworkAllowanceViewModel.OutHouseMultiplyingFactor),
                EffectiveDate = nightworkAllowanceViewModel.EffectiveFrom,
                ClosingDate = nightworkAllowanceViewModel.ClosingDate
            };
        }
        private NightWorkAllowanceSetting GetEntityWithAdditionalList(NightWorkAllowanceSetting model)
        {
            List<MemberEmploymentStatus> list = new List<MemberEmploymentStatus>();
            model.MemberEmploymentStatuses = new List<MemberEmploymentStatus>();
            if (model.IsContractual)
                list.Add(MemberEmploymentStatus.Contractual);
            if (model.IsPartTime)
                list.Add(MemberEmploymentStatus.PartTime);
            if (model.IsPermanent)
                list.Add(MemberEmploymentStatus.Permanent);
            if (model.IsProbation)
                list.Add(MemberEmploymentStatus.Probation);
            if (model.IsIntern)
                list.Add(MemberEmploymentStatus.Intern);
            model.MemberEmploymentStatuses = list;
            return model;
        }

        private NightworkAllowanceViewModel FormVmFromModel(NightWorkAllowanceSetting nightWorkAllowanceSetting)
        {
            var model = new NightworkAllowanceViewModel
            {
                Id = nightWorkAllowanceSetting.Id,
                OrgName = nightWorkAllowanceSetting.Organization.ShortName,
                OrganizationId = nightWorkAllowanceSetting.Organization.Id,
                AllowanceName = nightWorkAllowanceSetting.Name,
                CalculationOn = nightWorkAllowanceSetting.CalculationOn,
                InHouseMultiplyingFactor = nightWorkAllowanceSetting.InHouseMultiplyingFactor.ToString("0.#####"),
                OutHouseMultiplyingFactor = nightWorkAllowanceSetting.OutHouseMultiplyingFactor.ToString("0.#####"),
                EffectiveFrom = nightWorkAllowanceSetting.EffectiveDate,
                ClosingDate = nightWorkAllowanceSetting.ClosingDate,

            };
            List<MemberEmploymentStatus> list = new List<MemberEmploymentStatus>();
            model.EmloymentStatus = new List<MemberEmploymentStatus>();
            if (nightWorkAllowanceSetting.IsContractual)
                list.Add(MemberEmploymentStatus.Contractual);
            if (nightWorkAllowanceSetting.IsPartTime)
                list.Add(MemberEmploymentStatus.PartTime);
            if (nightWorkAllowanceSetting.IsPermanent)
                list.Add(MemberEmploymentStatus.Permanent);
            if (nightWorkAllowanceSetting.IsProbation)
                list.Add(MemberEmploymentStatus.Probation);
            if (nightWorkAllowanceSetting.IsIntern)
                list.Add(MemberEmploymentStatus.Intern);
            model.EmloymentStatus = list;
            return model;
        }

        private NightworkAllowanceDetailsViewModel DetailsVmFromModel(NightWorkAllowanceSetting entity)
        {
            ViewBag.EmploymentStatusList = new List<MemberEmploymentStatus>()
                                        {
                                            MemberEmploymentStatus.Contractual,
                                            MemberEmploymentStatus.Intern,
                                            MemberEmploymentStatus.PartTime,
                                            MemberEmploymentStatus.PartTime,
                                            MemberEmploymentStatus.Permanent,
                                            MemberEmploymentStatus.Probation
                                        };
            var model = new NightworkAllowanceDetailsViewModel
            {
                Organization = entity.Organization.ShortName,
                AllowanceName = entity.Name,
                CalculationOn = entity.CalculationOn.ToString(CultureInfo.InvariantCulture),
                InHouseMultiplyingFactor = entity.InHouseMultiplyingFactor.ToString(CultureInfo.InvariantCulture),
                OutHouseMultiplyingFactor = entity.OutHouseMultiplyingFactor.ToString(CultureInfo.InvariantCulture)
            };

            var list = new List<MemberEmploymentStatus>();
            model.EmploymentStatus = new List<MemberEmploymentStatus>();
            if (entity.IsContractual)
                list.Add(MemberEmploymentStatus.Contractual);
            if (entity.IsPartTime)
                list.Add(MemberEmploymentStatus.PartTime);
            if (entity.IsPermanent)
                list.Add(MemberEmploymentStatus.Permanent);
            if (entity.IsProbation)
                list.Add(MemberEmploymentStatus.Probation);
            if (entity.IsIntern)
                list.Add(MemberEmploymentStatus.Intern);
            model.EmploymentStatus = list;

            ViewBag.EffectiveFrom = entity.EffectiveDate.ToString("d MMM, yyyy");
            ViewBag.ClosingDate = entity.ClosingDate != null ? entity.ClosingDate.Value.ToString("d MMM, yyyy") : string.Empty;
            ViewBag.CreateByStr = _userService.GetByAspNetUser(entity.CreateBy).AspNetUser.UserName;
            ViewBag.ModifiedByStr = _userService.GetByAspNetUser(entity.ModifyBy).AspNetUser.UserName;
            ViewBag.CreationDateStr = entity.CreationDate.ToString("d MMM, yyyy");
            ViewBag.ModifiedDateStr = entity.ModificationDate.ToString("d MMM, yyyy");

            return model;
        }

        #endregion
    }
}