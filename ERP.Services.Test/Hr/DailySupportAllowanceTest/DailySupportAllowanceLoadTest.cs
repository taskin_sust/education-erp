﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DailySupportAllowanceTest
{
   
    [Trait("Area", "Hr")]
    [Trait("Service", "DailySupportAllowanceService")]
    public class DailySupportAllowanceLoadTest : DailySupportAllowanceBaseTest
    {
        #region List Load Test

        [Fact]
        public void Should_Return_Invalid_Permission_Exception()
        {
            //save portion
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object);

            Assert.Throws<InvalidDataException>(() => _dailySupportAllowanceService.LoadDailySupportAllowanceSetting(0, 0, int.MaxValue, null, _organizationFactory.Object.Id));
        }

        [Fact]
        public void Should_Return_UnAuthorize_UserPermission_Of_Organization_Successfully()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object);

            Assert.Throws<UnauthorizeDataException>(() => _dailySupportAllowanceService.LoadDailySupportAllowanceSetting(0, 0, int.MaxValue, menu, _organizationFactory.Create().Persist().Object.Id));
        }

        [Fact]
        public void Should_Return_AllData_Successfully()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object);
            var list = _dailySupportAllowanceService.LoadDailySupportAllowanceSetting(0, 0, int.MaxValue, menu, _organizationFactory.Object.Id);
            Assert.True(list.Count == 1);
        }

        #endregion

        #region Single Object Load Test

        [Fact]
        public void Should_Return_Invalid_Data_Exception()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object);
            Assert.Throws<InvalidDataException>(() => _dailySupportAllowanceService.GetDailySupportAllowanceSetting(0));
        }

        [Fact]
        public void Should_Return_Obj_Successfully()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object);
            Assert.Equal(_dailySupportAllowanceSettingFactory.Object.Id, _dailySupportAllowanceService.GetDailySupportAllowanceSetting(_dailySupportAllowanceSettingFactory.Object.Id).Id);
        }

        #endregion

        #region Count Test 

        [Fact]
        public void Should_Return_Invalid_Data_Exception_ForCount()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object);

            Assert.Throws<InvalidDataException>(() => _dailySupportAllowanceService.GetDailySupportAllowanceCount(null, _organizationFactory.Object.Id));
        }

        [Fact]
        public void Should_Return_One_Data_Successfully()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object);

            Assert.True(_dailySupportAllowanceService.GetDailySupportAllowanceCount(menu, _organizationFactory.Object.Id) == 1);
        }
        
        #endregion

    }
}
