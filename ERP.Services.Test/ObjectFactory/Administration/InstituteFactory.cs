﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class InstituteFactory : ObjectFactoryBase<Institute>
    {
        IInstitutionService _institutionService;
        private IInstitutionCategoryService _institutionCategoryService;
        private InstituteCategoryFactory _instituteCategoryFactory;
        public List<StudentUniversityInfo> StudentUniversityInfoList { get; set; }

        public InstituteFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _institutionService = new InstituteService(_session);
            _institutionCategoryService = new InstitutionCategoryService(_session);
            _instituteCategoryFactory = new InstituteCategoryFactory(this, _session);
            StudentUniversityInfoList = new List<StudentUniversityInfo>();
        }

        public InstituteFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }
        public InstituteFactory Create()
        {
            var generator = new Random();
            Object = CreateInstitute(Name, 1, "insShort_" + Guid.NewGuid().ToString().Substring(0, 5), Guid.NewGuid().ToString().Substring(0, 10), generator.Next(0, 1000000).ToString("D6"), Guid.NewGuid().ToString().Substring(0, 10));
            Object.InstituteCategory = _institutionCategoryService.GetInstituteCategoryList()[0];
            SingleObjectList.Add(Object);
            return this;
        }

        public InstituteFactory WithCategory(InstituteCategory instituteCategory)
        {
            Object.InstituteCategory = instituteCategory;
            return this;
        }
        public InstituteFactory CreateWith(string instituteName,
              int rank,
              string shortName,
              string district,
              string eiin,
              string website)
        {
            Object = CreateInstitute(instituteName, rank, shortName, district, eiin, website);
            SingleObjectList.Add(Object);
            return this;
        }

        public InstituteFactory WithInstituteCategory()
        {
            Object.InstituteCategory = _instituteCategoryFactory.Object;
            return this;
        }

        public InstituteFactory WithInstituteCategory(InstituteCategory instituteCategory)
        {
            Object.InstituteCategory = instituteCategory;
            return this;
        }

        public InstituteFactory WithInstituteCategory(string instituteCategoryName, int rank, bool isUniversity)
        {
            _instituteCategoryFactory.CreateWith(instituteCategoryName, rank, isUniversity);
            Object.InstituteCategory = _instituteCategoryFactory.Object;
            return this;
        }

        private Institute CreateInstitute(string instituteName,
              int rank,
              string shortName,
              string district,
              string eiin,
              string website)
        {
            _instituteCategoryFactory.GetRandomInstituteCategory();
            var org = new Institute()
            {
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Name = instituteName,
                Rank = rank,
                ShortName = shortName,
                Status = Institute.EntityStatus.Active,
                Website = website,
                District = district,
                Eiin = eiin,
            };
            return org;
        }

        public InstituteFactory CreateMore(int numberOfInstitute)
        {
            CreateInstitute(numberOfInstitute);
            return this;
        }

        private List<Institute> CreateInstitute(int numberOfInstitute)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Institute>();
            var instituteNamePrefix = Guid.NewGuid().ToString().Substring(0, 6);
            var generator = new Random();

            for (int i = 0; i < numberOfInstitute; i++)
            {
                var name = instituteNamePrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                var institute = CreateInstitute(name, i, Guid.NewGuid().ToString().Substring(0, 6),
                    Guid.NewGuid().ToString().Substring(0, 10), generator.Next(0, 1000000).ToString("D6"),
                    Guid.NewGuid().ToString().Substring(0, 10));
                institute.InstituteCategory = _instituteCategoryFactory.Object;
                list.Add(institute);
            }
            ObjectList.AddRange(list);
            return list;
        }

        public InstituteFactory Persist()
        {

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _institutionService.Save(item);
                    }
                }
            }

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _institutionService.Save(item);
                    }
                }
            }
            return this;
        }
        public void CleanupInstitute(List<long> instituteIdList)
        {
            var tableName = GetEntityTableName(typeof(Institute));
            _session.CreateSQLQuery("delete from " + tableName + " where id in (" + string.Join(", ", instituteIdList) + ")").ExecuteUpdate();
        }
        public void Cleanup()
        {
            DeleteAll();
            _instituteCategoryFactory.Cleanup();
        }
    }
}
