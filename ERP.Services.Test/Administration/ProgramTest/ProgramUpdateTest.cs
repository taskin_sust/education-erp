﻿using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ProgramTest
{
    interface IProgramUpdateTest
    {
        #region Basic Test
        void Update_Without_Data_Should_Be_Retrun_NullException();
        void Update_Should_Be_Success();
        void Update_Should_Be_Fail_Without_Organization();
        void Update_Should_Be_Fail_Without_Program_Name();
        void Update_Should_Be_Fail_Without_Program_ShortName();
        void Update_Should_Be_Fail_Without_Program_Code();
        void Update_Should_Be_Fail_Without_Program_Type();
        void Update_Should_Be_Fail_String_Overflow_Program_Name();
        void Update_Should_Be_Fail_String_Overflow_Program_ShortName();
        void Update_Should_Be_Fail_String_Overflow_Program_Code();

        #endregion

        #region Business Logic Test
        void Update_Should_Be_Fail_For_Duplicate_Program_Name();
        void Update_Should_Be_Fail_For_Duplicate_Program_ShortName();
        void Update_Should_Be_Fail_For_Duplicate_Program_Code();
        void Update_Should_Be_Fail_For_Invalid_ProgramType();
        void Update_should_Be_Fail_For_Already_Assign_Program_Inacticve();

        #endregion

    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Program")]
    public class ProgramUpdateTest : ProgramBaseTest, IProgramUpdateTest
    {
        #region Basic Test
        [Fact]
        public void Update_Without_Data_Should_Be_Retrun_NullException()
        {
            Assert.Throws<NullObjectException>(() => _programService.ProgramSaveOrUpdate(null));
        }

        [Fact]
        public void Update_Should_Be_Success()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            program.Name = program.Name + "_UPDATE";
            _programService.ProgramSaveOrUpdate(program);
            Program returnProgram = _programService.GetProgram(programFactory.Object.Id);
            Assert.True(program.Name == returnProgram.Name);
        }

        [Fact]
        public void Update_Should_Be_Fail_Without_Organization()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            program.Organization = null;
            Assert.Throws<InvalidDataException>(() => _programService.ProgramSaveOrUpdate(program));
        }

        [Fact]
        public void Update_Should_Be_Fail_Without_Program_Name()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            program.Name = null;
            Assert.Throws<InvalidDataException>(() => _programService.ProgramSaveOrUpdate(program));
        }

        [Fact]
        public void Update_Should_Be_Fail_Without_Program_ShortName()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            program.ShortName = null;
            Assert.Throws<InvalidDataException>(() => _programService.ProgramSaveOrUpdate(program));
        }

        [Fact]
        public void Update_Should_Be_Fail_Without_Program_Code()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            program.Code = null;
            Assert.Throws<InvalidDataException>(() => _programService.ProgramSaveOrUpdate(program));
        }

        [Fact]
        public void Update_Should_Be_Fail_Without_Program_Type()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            program.Type = 420;
            Assert.Throws<InvalidDataException>(() => _programService.ProgramSaveOrUpdate(program));
        }

        [Fact]
        public void Update_Should_Be_Fail_String_Overflow_Program_Name()
        {
            string overflowProgramName = "I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my  project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to  in my project, it had a drtretre v wehyroiyhweioroweroi eoiroiewr eiwroiweiorweio oiewrioweyhior ioewioroweyhrio weqrioioewn asi9uyer90qw32e uw90quer90wq09ed 09awu90duew09qrw0 we09dwqy90e40qwyud 09qwu3094uejhdkasjpldj uqweuqwiue q09we qwey09qwe0qw   wejrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr 0qwu38209euqwoaujdkaskdhaskdoias asojdfoiasoiashiduasi0dui0asudiasudi0ausd0iu";
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            program.Name = overflowProgramName;
            Assert.Throws<InvalidDataException>(() => _programService.ProgramSaveOrUpdate(program));
        }
        [Fact]
        public void Update_Should_Be_Fail_String_Overflow_Program_ShortName()
        {
            string overflowProgramName = "I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my  project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to  in my project, it had a drtretre v wehyroiyhweioroweroi eoiroiewr eiwroiweiorweio oiewrioweyhior ioewioroweyhrio weqrioioewn asi9uyer90qw32e uw90quer90wq09ed 09awu90duew09qrw0 we09dwqy90e40qwyud 09qwu3094uejhdkasjpldj uqweuqwiue q09we qwey09qwe0qw   wejrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr 0qwu38209euqwoaujdkaskdhaskdoias asojdfoiasoiashiduasi0dui0asudiasudi0ausd0iu";
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            program.ShortName = overflowProgramName;
            Assert.Throws<InvalidDataException>(() => _programService.ProgramSaveOrUpdate(program));
        }
        [Fact]
        public void Update_Should_Be_Fail_String_Overflow_Program_Code()
        {
            string overflowProgramName = "I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. AlthoughI upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my  project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to in my project, it had a I upgraded from Silverlight 4 to Silverlight 5 and then I was having this issue. Although I had a reference to  in my project, it had a drtretre v wehyroiyhweioroweroi eoiroiewr eiwroiweiorweio oiewrioweyhior ioewioroweyhrio weqrioioewn asi9uyer90qw32e uw90quer90wq09ed 09awu90duew09qrw0 we09dwqy90e40qwyud 09qwu3094uejhdkasjpldj uqweuqwiue q09we qwey09qwe0qw   wejrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr 0qwu38209euqwoaujdkaskdhaskdoias asojdfoiasoiashiduasi0dui0asudiasudi0ausd0iu";
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            Program program = _programService.GetProgram(programFactory.Object.Id);
            program.Code = overflowProgramName;
            Assert.Throws<InvalidDataException>(() => _programService.ProgramSaveOrUpdate(program));
        }



        #endregion

        #region Business Logic Test
        [Fact]
        public void Update_Should_Be_Fail_For_Duplicate_Program_Name()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            var organization = programFactory.Object.Organization;
            var programFactoryForDuplication = _programFactory.Create().WithOrganization(organization).WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram);
            Assert.Throws<DuplicateEntryException>(() => programFactoryForDuplication.Persist());
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Duplicate_Program_ShortName()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            var organization = programFactory.Object.Organization;
            var programFactoryForDuplication = _programFactory.Create().WithOrganization(organization).WithProperties(_programName + " DIF", _shortName, _programFactory.GetRandomNumber(), _paidProgram);
            Assert.Throws<DuplicateEntryException>(() => programFactoryForDuplication.Persist());
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Duplicate_Program_Code()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            var organization = programFactory.Object.Organization;
            var programFactoryForDuplication = _programFactory.Create().WithOrganization(organization).WithProperties(_programName + " DIF", _shortName + " DIF", _programFactory.GetRandomNumber(), _paidProgram);
            Assert.Throws<DuplicateEntryException>(() => programFactoryForDuplication.Persist());
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Invalid_ProgramType()
        {
            Assert.Throws<InvalidDataException>(() => _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), -1).Persist());

        }

        [Fact]
        public void Update_should_Be_Fail_For_Already_Assign_Program_Inacticve()
        {
            var program = AssignProgram();
            program.Status =Program.EntityStatus.Inactive;
            Assert.Throws<DependencyException>(() => _programService.ProgramSaveOrUpdate(program));
        }

        #endregion
    }
}
