using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students {


    public partial class StudentAcademicInfoMap : BaseClassMap<StudentAcademicInfo, long>
    {
        
        public StudentAcademicInfoMap() {
			Table("StudentAcademicInfo");
			LazyLoad();

			Map(x => x.Year).Column("Year");

            Map(x => x.BoradRoll).Column("BoradRoll");
            Map(x => x.RegistrationNumber).Column("RegistrationNumber");
			
            References(x => x.Student).Column("StudentId");
            References(x => x.StudentBoard).Column("BoardId");
            References(x => x.StudentExam).Column("StudentExamId");

            //new...
            HasMany(x => x.StudentAcademicInfoBaseData).KeyColumn("StudentAcademicInfoId").Cascade.AllDeleteOrphan().Inverse();

        }
    }
}
