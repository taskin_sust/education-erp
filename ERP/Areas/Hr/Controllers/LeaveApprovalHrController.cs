﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using NHibernate;
using Org.BouncyCastle.Crypto.Tls;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class LeaveApprovalHrController : Controller
    {
        // GET: Hr/LeaveApprovalHr
        private ILog _logger = LogManager.GetLogger("HrArea");
        private readonly ILeaveService _hrLeaveService;
        private readonly IOrganizationService _organizationService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IDepartmentService _departmentService;
        private ISession _session;
        private readonly ICommonHelper _commonHelper;
        private readonly ILeaveApplicationService _hrLeaveApplicationService;
        private readonly ITeamMemberService _hrMemberService;
        private List<UserMenu> _userMenu;
        private readonly IUserService _userService;

        public LeaveApprovalHrController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _hrLeaveService = new LeaveService(_session);
            _organizationService = new OrganizationService(_session);
            _branchService = new BranchService(_session);
            _campusService = new CampusService(_session);
            _departmentService = new DepartmentService(_session);
            _hrLeaveApplicationService = new LeaveApplicationService(_session);
            _hrMemberService = new TeamMemberService(_session);
            _commonHelper = new CommonHelper();
            _userService = new UserService(_session);
        }

        public ActionResult Index()
        {
            return RedirectToAction("LeaveApprovedByHr");
        }

        #region Leave approval Hr

        public ActionResult LeaveApprovedByHr(string memberPin = "", string organization = "", string branch = "", string campus = "", string department = "", string date = "")
        {
            IList<LeaveApplicationViewModel> pendingLeaveApplicationViewModelList = new List<LeaveApplicationViewModel>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                #region filter part
                ViewBag.MemberPin = "";
                ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
                ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.DepartmentList = new SelectList(new List<Department>(), "Id", "Name");
                ViewBag.SearchingDate = "";
                List<int> memberPinList = null;
                long? organizationId = null;
                long? branchId = null;
                long? campusId = null;
                long? departmentId = null;
                string dateTimeString = "";
                List<Organization> organizationList = new List<Organization>();
                List<Branch> branchList = new List<Branch>();
                List<Campus> campusList = new List<Campus>();
                List<Department> departmentList = new List<Department>();
                List<long> organizationIdList = null;
                List<long> branchIdList = null;
                List<long> campusIdList = null;
                List<long> departmentIdList = null;
                if (!String.IsNullOrEmpty(memberPin))
                {
                    ViewBag.MemberPin = memberPin;
                    memberPinList = new List<int>{Convert.ToInt32(memberPin)};
                }
                if (!String.IsNullOrEmpty(organization))
                {
                    organizationId = Convert.ToInt64(organization);
                    organizationIdList = (organizationId != null) ? _commonHelper.ConvertIdToList((long) organizationId) : null;
                    branchList = _branchService.LoadAuthorizedBranch(_userMenu, organizationIdList, null, null, false).ToList();
                    departmentList = _departmentService.LoadDepartment(organizationIdList).ToList();
                }
                if (!String.IsNullOrEmpty(branch))
                {
                    branchId = Convert.ToInt64(branch);
                    branchIdList = (branchId != null) ? _commonHelper.ConvertIdToList((long) branchId) : null;
                    campusList = _campusService.LoadAuthorizeCampus(_userMenu, organizationIdList, null, branchIdList, null, false).ToList();
                }
                if (!String.IsNullOrEmpty(campus))
                {
                    campusId = Convert.ToInt64(campus);
                    campusIdList = (campusId != null) ? _commonHelper.ConvertIdToList((long) campusId) : null;
                }
                if (!String.IsNullOrEmpty(department))
                {
                    departmentId = Convert.ToInt64(department);
                     departmentIdList = (departmentId != null) ? _commonHelper.ConvertIdToList((long) departmentId) : null;
                }
                if (!String.IsNullOrEmpty(date))
                {
                    DateTime dateTime2;
                    if (DateTime.TryParse(date, out dateTime2))
                    {
                        dateTimeString = dateTime2.ToString("yyyy-MM-dd");
                        ViewBag.SearchingDate = dateTimeString;
                    }
                }
                organizationList = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();

                List<SelectListItem> organizationSelectList = new SelectList(organizationList, "Id", "ShortName", organizationId).ToList();
                organizationSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Organization" });
                ViewBag.OrganizationList = new SelectList(organizationSelectList, "Value", "Text", organizationId);

                List<SelectListItem> branchSelectList = new SelectList(branchList, "Id", "Name", branchId).ToList();
                branchSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Branch" });
                ViewBag.BranchList = new SelectList(branchSelectList, "Value", "Text", branchId);

                List<SelectListItem> campusSelectList = new SelectList(campusList, "Id", "Name", campusId).ToList();
                campusSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Campus" });
                ViewBag.CampusList = new SelectList(campusSelectList, "Value", "Text", campusId);

                List<SelectListItem> departmentSelectList = new SelectList(departmentList, "Id", "Name", departmentId).ToList();
                departmentSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Department" });
                ViewBag.DepartmentList = new SelectList(departmentSelectList, "Value", "Text", departmentId);


                #endregion filter part

                IList<LeaveApplicationViewModel> teampPendingLeaveApplicationViewModelList = _hrLeaveApplicationService.LoadHrLeave(_userMenu, dateTimeString, organizationIdList,
                        departmentIdList, branchIdList, campusIdList, memberPinList, LeaveStatus.Pending).ToList(); //, null, null, null, null, memberPinList, LeaveStatus.Pending).ToList();
                if (teampPendingLeaveApplicationViewModelList.Any())
                {
                    foreach (var teampPendingLeaveApplicationViewMode in teampPendingLeaveApplicationViewModelList)
                    {
                        //var leave = _hrLeaveService.GetByLeaveAppId(Convert.ToInt64(teampPendingLeaveApplicationViewMode.LeaveApplicationId));
                        var member = _hrMemberService.GetByPin(Convert.ToInt32(teampPendingLeaveApplicationViewMode.TeamMemberPin));
                        var leaveTypesList = _hrMemberService.LoadMembersPendingLeave(member.Id);
                        var leaveTypeDtoLists = new List<LeaveTypeDtoList>();
                        foreach (var leaveTypeDtoObj in leaveTypesList)
                        {
                            if (leaveTypeDtoObj.LeaveId == Convert.ToInt64(teampPendingLeaveApplicationViewMode.LeaveId))
                            {
                                leaveTypeDtoObj.IsSelected = true;
                                leaveTypeDtoLists.Add(leaveTypeDtoObj);
                            }
                            else
                            {
                                leaveTypeDtoLists.Add(leaveTypeDtoObj);
                            }
                        }
                        teampPendingLeaveApplicationViewMode.Leaves = leaveTypeDtoLists;
                        pendingLeaveApplicationViewModelList.Add(teampPendingLeaveApplicationViewMode);
                    }
                }

            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            return View(pendingLeaveApplicationViewModelList);
        }

        [HttpPost]
        public JsonResult HrLeaveApprove(List<LeaveApplicationViewModel> leaveApplicationViewModelList)
        {
            string message = "Error";
            bool successState = false;
            try
            {
                if (leaveApplicationViewModelList.Any())
                {
                    List<LeaveApplication> leaveApplicationList = new List<LeaveApplication>();
                    foreach (var leaveApplicationViewModel in leaveApplicationViewModelList)
                    {
                        TeamMember member = _hrMemberService.GetMember(Convert.ToInt32(leaveApplicationViewModel.TeamMemberPin));
                        Leave leave = _hrLeaveService.GetById(leaveApplicationViewModel.LeaveId);

                        LeaveApplication leaveApplication = new LeaveApplication();
                        if (leaveApplicationViewModel.LeaveApplicationId != null && leaveApplicationViewModel.LeaveApplicationId > 0)
                        {
                            leaveApplication = _hrLeaveApplicationService.GetById(Convert.ToInt64(leaveApplicationViewModel.LeaveApplicationId));
                        }
                        leaveApplicationViewModel.FillLeaveApplicationValue(leaveApplication);
                        leaveApplication.Leave = leave;
                        leaveApplication.TeamMember = member;
                        leaveApplication.Status = LeaveApplication.EntityStatus.Active;
                        leaveApplication.LeaveStatus = Convert.ToInt32(leaveApplicationViewModel.LeaveStatus);
                        if (leaveApplicationViewModel.ResponsibleMemberPin != null && leaveApplicationViewModel.ResponsibleMemberPin > 0)
                        {
                            TeamMember responsiblePerson = _hrMemberService.GetByPin(leaveApplicationViewModel.ResponsibleMemberPin.Value);
                            leaveApplication.ResponsiblePerson = responsiblePerson;
                        }
                        leaveApplicationList.Add(leaveApplication);
                    }
                    _hrLeaveApplicationService.SaveOrUpdate(leaveApplicationList, false, true);
                    message = "Total " + leaveApplicationViewModelList.Count().ToString() + " leave(s) Save Successfully.";
                    successState = true;
                }
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        public ActionResult HrLeaveHistory()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.DepartmentList = new SelectList(new List<Department>(), "Id", "Name");
            try
            {
                ViewBag.LeaveStatus = new SelectList(_commonHelper.LoadEmumToDictionary<LeaveStatus>(new List<int> { (int)LeaveStatus.Pending }), "Key", "Value");
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                return PartialView("Partial/_RecentLeaveHistoryHr");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        
        [HttpPost]
        public JsonResult HrLeaveHistoryDataTable(int draw, int start, int length, string organizationId = "", string departmentId = "", string branchId = "", string campusId = "", string pin = "", string date = "", string leaveStatus = "")
        {
            try
            {
                var data = new List<object>();
                int recordsTotal = 0;
                int recordsFiltered = 0;
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                
                #region filter result
                List<long> organizationIdList = null;
                List<long> departmentIdList = null;
                List<long> branchIdList = null;
                List<long> campusIdList = null;
                string dateTimeString = "";
                LeaveStatus? convertedLeaveStatus = null;

                List<int> memberPinList = new List<int>();
                if (!String.IsNullOrEmpty(organizationId))
                {
                    organizationIdList = new List<long> { (Convert.ToInt32(organizationId)) };
                }
                if (!String.IsNullOrEmpty(departmentId))
                {
                    departmentIdList = new List<long> { (Convert.ToInt32(departmentId)) };
                }
                if (!String.IsNullOrEmpty(branchId))
                {
                    branchIdList = new List<long> { (Convert.ToInt32(branchId)) };
                }
                if (!String.IsNullOrEmpty(campusId))
                {
                    campusIdList = new List<long> { (Convert.ToInt32(campusId)) };
                }
                if (!String.IsNullOrEmpty(pin))
                {
                    memberPinList.Add(Convert.ToInt32(pin));
                }
                if (!String.IsNullOrEmpty(date))
                {
                    DateTime dateTime2;
                    if (DateTime.TryParse(date, out dateTime2))
                    {
                        dateTimeString = dateTime2.ToString("yyyy-MM-dd");
                        ViewBag.SearchingDate = dateTimeString;
                    }
                }
                if (!String.IsNullOrEmpty(leaveStatus))
                {
                    convertedLeaveStatus = (LeaveStatus)Convert.ToInt32(leaveStatus);
                }
                #endregion filter result

                recordsTotal = _hrLeaveApplicationService.GetHrLeaveCount(_userMenu, dateTimeString, organizationIdList, departmentIdList, branchIdList, campusIdList, memberPinList, convertedLeaveStatus);
                recordsFiltered = recordsTotal;
                IList<LeaveApplicationViewModel> hrLeaveApplicationViewModelList = _hrLeaveApplicationService.LoadHrLeave(_userMenu, dateTimeString, organizationIdList, departmentIdList, branchIdList, campusIdList, memberPinList, convertedLeaveStatus, start, length).ToList();

                #region for Datatable
                foreach (var hrLeaveApplicationViewModel in hrLeaveApplicationViewModelList)
                {
                    Leave leave = _hrLeaveService.GetById(hrLeaveApplicationViewModel.LeaveId);
                    string leaveStatusText = "-";
                    string leaveStatusActionLink = "-";
                    string editLink = "-";
                    string responsiblePinName = "-";
                    if (hrLeaveApplicationViewModel.ResponsibleMemberPin != null && !String.IsNullOrEmpty(hrLeaveApplicationViewModel.ResponsibleMemberName))
                    {
                        responsiblePinName = hrLeaveApplicationViewModel.ResponsibleMemberName + " (" + hrLeaveApplicationViewModel.ResponsibleMemberPin + ") ";
                    }
                    var str = new List<string>();
                    str.Add(hrLeaveApplicationViewModel.TeamMemberPin.ToString());
                    str.Add(hrLeaveApplicationViewModel.TeamMemberName);
                    str.Add(hrLeaveApplicationViewModel.Department);
                    str.Add(hrLeaveApplicationViewModel.Organization);
                    str.Add(hrLeaveApplicationViewModel.Branch);
                    str.Add(hrLeaveApplicationViewModel.Campus);
                    str.Add(leave.Name);
                    str.Add(hrLeaveApplicationViewModel.DateFrom);
                    str.Add(hrLeaveApplicationViewModel.DateTo);
                    str.Add(hrLeaveApplicationViewModel.TotalLeaveDay.ToString());
                    str.Add(hrLeaveApplicationViewModel.LeaveNote);
                    if (hrLeaveApplicationViewModel.LeaveStatus != null)
                    {
                        if (hrLeaveApplicationViewModel.LeaveStatus == (int)LeaveStatus.Approved)
                        {
                            leaveStatusText = LeaveStatus.Approved.ToString();
                            if (hrLeaveApplicationViewModel.IsPost != null &&
                                hrLeaveApplicationViewModel.IsPost == 0)
                            {
                                leaveStatusActionLink = "<a title='Reject' href='#' data-id='" + hrLeaveApplicationViewModel.LeaveApplicationId + "' data-pin='" + hrLeaveApplicationViewModel.TeamMemberPin + "' data-leave='" + hrLeaveApplicationViewModel.LeaveId + "' data-leaveStatus='" + hrLeaveApplicationViewModel.LeaveStatus + "' data-note='" + hrLeaveApplicationViewModel.LeaveNote + "' class='glyphicon glyphicon-remove hrHistoryReject'> </a>";
                            }
                        }
                        else if (hrLeaveApplicationViewModel.LeaveStatus == (int)LeaveStatus.Cancelled)
                        {
                            leaveStatusText = LeaveStatus.Cancelled.ToString();
                        }
                        else if (hrLeaveApplicationViewModel.LeaveStatus == (int)LeaveStatus.Pending)
                        {
                            leaveStatusText = LeaveStatus.Pending.ToString();
                        }
                        else if (hrLeaveApplicationViewModel.LeaveStatus == (int)LeaveStatus.Rejected)
                        {
                            leaveStatusText = LeaveStatus.Rejected.ToString();
                            if (hrLeaveApplicationViewModel.IsPost != null &&
                                hrLeaveApplicationViewModel.IsPost == 0)
                            {
                                leaveStatusActionLink = "<a title='Approve' href='#' data-id='" + hrLeaveApplicationViewModel.LeaveApplicationId + "' data-pin='" + hrLeaveApplicationViewModel.TeamMemberPin + "' data-leave='" + hrLeaveApplicationViewModel.LeaveId + "' data-leaveStatus='" + hrLeaveApplicationViewModel.LeaveStatus + "' data-note='" + hrLeaveApplicationViewModel.LeaveNote + "' class='glyphicon glyphicon-ok hrHistoryApproved'> </a>";
                            }
                        }
                    }
                    str.Add("<span id='leaveStatus_" + hrLeaveApplicationViewModel.LeaveApplicationId + "'>" + leaveStatusText + "</span>");
                    str.Add("<span id='leaveApplicationDate_" + hrLeaveApplicationViewModel.LeaveApplicationId + "'>" + hrLeaveApplicationViewModel.ApplicationDate + "</span>");
                    str.Add("<span id='leaveApplicationRPinName_" + hrLeaveApplicationViewModel.LeaveApplicationId + "'>" + responsiblePinName + "</span>");
                    str.Add(hrLeaveApplicationViewModel.ModifyBy != null ? _userService.GetUserNameByAspNetUserId((long)hrLeaveApplicationViewModel.ModifyBy) : "");
                    str.Add(CommonHelper.GetReportCommonDateTimeFormat(hrLeaveApplicationViewModel.LastModificationDate));
                    str.Add("<span id='leaveStatusActionLink_" + hrLeaveApplicationViewModel.LeaveApplicationId + "'>" + leaveStatusActionLink + "</span>");
                    if (hrLeaveApplicationViewModel.LeaveStatus != (int) LeaveStatus.Cancelled && hrLeaveApplicationViewModel.IsPost != null && hrLeaveApplicationViewModel.IsPost != 1)
                    {
                        editLink = "<a href='#' data-toggle='modal' data-target='#myModal' data-id='' class='glyphicon glyphicon-edit editHrLeaveApplication' data-laid='" +hrLeaveApplicationViewModel.LeaveApplicationId + "'></a>";
                    }
                    str.Add(editLink);
                    data.Add(str);

                }
                #endregion for Datatable

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        
        [HttpPost]
        public PartialViewResult GenerateLeaveApplicationModalView(long leaveApplicationId)
        {
            try
            {
                LeaveApplicationViewModel leaveApplicationViewModel = new LeaveApplicationViewModel();
                LeaveApplication leaveApplication = _hrLeaveApplicationService.GetById(leaveApplicationId);
                leaveApplicationViewModel.FillLeaveApplicationViewModelValue(leaveApplication);

                TeamMember member = leaveApplication.TeamMember;
                Leave leave = leaveApplication.Leave;
                Organization organization = _hrMemberService.GetTeamMemberOrganization(leaveApplication.DateFrom.ToString("yyyy-MM-dd"), member.Id);
                Department department = _hrMemberService.GetTeamMemberDepartment(leaveApplication.DateFrom.ToString("yyyy-MM-dd"), member.Id);
                Branch branch = _hrMemberService.GetTeamMemberBranch(leaveApplication.DateFrom.ToString("yyyy-MM-dd"), member.Id);
                Campus campus = _hrMemberService.GetTeamMemberCampus(leaveApplication.DateFrom.ToString("yyyy-MM-dd"), member.Id);

                leaveApplicationViewModel.Department = department.Name;
                leaveApplicationViewModel.Organization = organization.ShortName;
                leaveApplicationViewModel.Branch = branch.Name;
                leaveApplicationViewModel.Campus = campus.Name;

                var leaveTypesList = _hrMemberService.LoadMembersPendingLeave(member.Id, true);
                var leaveTypeDtoLists = new List<LeaveTypeDtoList>();
                foreach (var leaveTypeDtoObj in leaveTypesList)
                {
                    if (leaveTypeDtoObj.LeaveId == leave.Id)
                    {
                        leaveTypeDtoObj.IsSelected = true;
                        leaveTypeDtoLists.Add(leaveTypeDtoObj);
                    }
                    else
                    {
                        leaveTypeDtoLists.Add(leaveTypeDtoObj);
                    }
                }
                leaveApplicationViewModel.Leaves = leaveTypeDtoLists;
                return PartialView("Partial/_LeaveApplicationModalHr", leaveApplicationViewModel);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion
    }
}