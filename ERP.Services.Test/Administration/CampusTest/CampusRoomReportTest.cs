﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.CampusTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Campus")]
    public class CampusRoomReportTest : CampusBaseTest
    {
        #region Basic Test

        [Fact]
        public void LoadCampusRoomsByBranchId_should_return_list()
        {
            Organization organization = organizationFactory.Create().Persist().Object;
            //Create New Program
            _code = _programFactory.GetRandomNumber();
            Program program =
                _programFactory.Create().WithOrganization(organization).WithNameAndRank(_programName, 1).WithProperties(_programName, _code, _paidProgram)
                    .Persist().Object;

            //Create New Session
            _sessionFactory.Create().Persist();
            Session sessionObj = _sessionFactory.Object;

            //Create New Branches and generate branchArray
            IList<Organization> organizations = new List<Organization>();
            organizations.Add(program.Organization);
            var branchFactory = _branchFactory.CreateMore(1, organizations).Persist();
            IList<Branch> branchList = branchFactory.GetLastCreatedObjectList();
            var branchIdArray = branchList.Select(x => x.Id).ToArray();
            //Generate selected Student Exam List
            var studentExamList = _studentExamService.LoadBoardExam(true);
            int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y => (int?)Convert.ToInt32(y.Id)).ToArray();

            //generate selected Admission Type List
            var selectedAdmissionTypeList = new int?[] { 1, 2 };
            List<long> nonDeletableBranchIds;
            _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id,
                branchIdArray, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);
            var campusRooms = new List<CampusRoom>()
            {
                new CampusRoom()
                {
                    RoomNo = (101).ToString(CultureInfo.InvariantCulture),
                    ClassCapacity = 100,
                    ExamCapacity = 100
                }
                ,new CampusRoom()
                {
                    RoomNo = (102).ToString(CultureInfo.InvariantCulture),
                    ClassCapacity = 200,
                    ExamCapacity = 200
                }
            };
            _campusFactory.CreateMore(100, branchList, campusRooms).Persist();
            _campusFactory.GetLastCreatedObjectList();
            var campusRoomList = _campusRoomService.LoadCampusRoom(branchIdArray[0]);
            Assert.True(campusRoomList.Count > 0);
            var campusRoomIdList = (from campus in _campusFactory.GetLastCreatedObjectList() from campusRoom in campus.CampusRooms select campusRoom.Id).ToList();
            CleanUpProgramStudentExamSession(program.Id, sessionObj.Id);
            CleanUpProgramBranchSession(program.Id, sessionObj.Id, branchIdArray);
            _campusTestService.DeleteCampusRoom(campusRoomIdList);
        }

        [Fact]
        public void LoadCampusRoomsByCampusIds_should_return_list()
        {
            Organization organization = organizationFactory.Create().Persist().Object;
            //Create New Program
            _code = _programFactory.GetRandomNumber();
            Program program =
                _programFactory.Create().WithOrganization(organization).WithNameAndRank(_programName, 1).WithProperties(_programName, _code, _paidProgram)
                    .Persist().Object;

            //Create New Session
            _sessionFactory.Create().Persist();
            Session sessionObj = _sessionFactory.Object;

            //Create New Branches and generate branchArray
            IList<Organization> organizations = new List<Organization>();
            organizations.Add(program.Organization);
            var branchFactory = _branchFactory.CreateMore(1, organizations).Persist();
            IList<Branch> branchList = branchFactory.GetLastCreatedObjectList();
            var branchIdArray = branchList.Select(x => x.Id).ToArray();
            //Generate selected Student Exam List
            var studentExamList = _studentExamService.LoadBoardExam(true);
            int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y => (int?)Convert.ToInt32(y.Id)).ToArray();

            //generate selected Admission Type List
            var selectedAdmissionTypeList = new int?[] { 1, 2 };
            List<long> nonDeletableBranchIds;
            _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id,
                branchIdArray, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);
            var campusRooms = new List<CampusRoom>()
            {
                new CampusRoom()
                {
                    RoomNo = "101",
                    ClassCapacity = 100,
                    ExamCapacity = 100
                }
                ,new CampusRoom()
                {
                    RoomNo = "102",
                    ClassCapacity = 200,
                    ExamCapacity = 200
                }
            };
            _campusFactory.CreateMore(100, branchList, campusRooms).Persist();
            var campusIdStringArray = _campusFactory.GetLastCreatedObjectList().Select(x=>x.Id.ToString()).ToArray();
            var campusRoomList = _campusRoomService.LoadCampusRoom(campusIdStringArray);
            Assert.True(campusRoomList.Count > 0);
            var campusRoomIdList = (from campus in _campusFactory.GetLastCreatedObjectList() from campusRoom in campus.CampusRooms select campusRoom.Id).ToList();
            CleanUpProgramStudentExamSession(program.Id, sessionObj.Id);
            CleanUpProgramBranchSession(program.Id, sessionObj.Id, branchIdArray);
            _campusTestService.DeleteCampusRoom(campusRoomIdList);
        }

        #endregion
    }
}
