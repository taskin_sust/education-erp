using System.Collections.Generic;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface ISupplierPriceQuoteDao : IBaseDao<SupplierPriceQuote, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        
        bool CheckActiveQuotation(long quotationId);

        #endregion
        
    }

    public class SupplierPriceQuoteDao : BaseDao<SupplierPriceQuote, long>, ISupplierPriceQuoteDao
    {

        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function

        public bool CheckActiveQuotation(long quotationId)
        {
            IList<SupplierPriceQuote> list =
                Session.QueryOver<SupplierPriceQuote>()
                    .Where(x => x.Quotation.Id == quotationId && x.Status == SupplierPriceQuote.EntityStatus.Active)
                    .List<SupplierPriceQuote>();
            return list.Count <= 0;
        }

        #endregion

        #region Helper Function
        #endregion
        
    }
}
