﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessRules;
namespace UdvashERP.Services.Hr
{
    public interface IDayOffAdjustmentService : IBaseService
    {
        #region Operational Function

        bool Save(List<DayOffAdjustment> dayOffAdjustments);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<DayOffAdjustmentMentorDto> LoadDayOffRecentAdjustmentForMentor(TeamMember mentor, int start, int length, string orderBy, string toUpper, string pin, DateTime dateFrom, DateTime dateTo);
        IList<DayOffAdjustmentHrDto> LoadAllHrDayOffAdjustmentHistory(List<UserMenu> userMenus, int start, int length, string orderBy, string toUpper, string pin, DateTime dateFrom, DateTime dateTo, long? org, long? branch, long? campus, long? department);

        #endregion

        #region Others Function

        int CountDayOffRecentAdjustmentForMentor(TeamMember mentor, string pin, DateTime dateFrom, DateTime dateTo);
        int CountHrDayOffAdjustmentHistory(List<UserMenu> userMenus, string pin, DateTime dateFrom, DateTime dateTo, long? org, long? branch, long? campus, long? department);
        DayOffAdjustment LoadById(long id);

        DateTime? GetTeamMemberAttendanceAdjustmentPostDateTime(long? teamMemberId, int? pin = null); 
        #endregion

        #region Helper Function

        #endregion

    }
    public class DayOffAdjustmentService : BaseService, IDayOffAdjustmentService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IDayOffAdjustmentDao _dayOffAdjustmentDao;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly IHolidaySettingDao _holidaySettingDao;
        private readonly IHolidayWorkDao _holidayWorkDao;
        private readonly IMentorHistoryDao _mentorHistoryDao;
        private readonly ISalarySheetDao _salarySheetDao;
        public DayOffAdjustmentService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _dayOffAdjustmentDao = new DayOffAdjustmentDao() { Session = session };
            _teamMemberDao = new TeamMemberDao { Session = session };
            _holidaySettingDao = new HolidaySettingDao { Session = session };
            _holidayWorkDao = new HolidayWorkDao { Session = session };
            _mentorHistoryDao = new MentorHistoryDao { Session = session };
            _salarySheetDao = new SalarySheetDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool Save(List<DayOffAdjustment> dayOffAdjustments)
        {
            ITransaction transaction = null;
            try
            {
                if (dayOffAdjustments != null)
                {

                    #region check Dublicate Dayoff Object

                    List<DayOffAdjustment> tempDayOffAdjustmentListWithId = new List<DayOffAdjustment>();

                    foreach (DayOffAdjustment temp in dayOffAdjustments)
                    {
                        if (temp.Id <= 0)
                        {
                            
                            tempDayOffAdjustmentListWithId.Add((DayOffAdjustment) temp.Clone());
                           
                        }
                    }
                    foreach (var dayOffAdjustment in tempDayOffAdjustmentListWithId)
                    {

                        DayOffAdjustment tempDayOffAdjustment =
                            tempDayOffAdjustmentListWithId.FirstOrDefault(
                                x =>
                                    x.Id != dayOffAdjustment.Id && x.TeamMember == dayOffAdjustment.TeamMember &&
                                    x.DayOffDate == dayOffAdjustment.DayOffDate);
                        if (tempDayOffAdjustment != null)
                        {
                            throw new InvalidDataException("Pin " + dayOffAdjustment.TeamMember.Pin +
                                                           " has dublicate Application " +
                                                           dayOffAdjustment.DayOffDate.Value.ToString("dd MMM, yyyy") +
                                                           " and Instead of date " +
                                                           dayOffAdjustment.InsteadOfDate.Value.ToString("dd MMM, yyyy"));
                        }

                        tempDayOffAdjustment =
                            tempDayOffAdjustmentListWithId.FirstOrDefault(
                                x =>
                                    x.Id != dayOffAdjustment.Id && x.TeamMember == dayOffAdjustment.TeamMember &&
                                    x.InsteadOfDate == dayOffAdjustment.InsteadOfDate);
                        if (tempDayOffAdjustment != null)
                        {
                            throw new InvalidDataException("Pin " + dayOffAdjustment.TeamMember.Pin +
                                                           " has dublicate Application " +
                                                           dayOffAdjustment.DayOffDate.Value.ToString("dd MMM, yyyy") +
                                                           " and Instead of date " +
                                                           dayOffAdjustment.InsteadOfDate.Value.ToString("dd MMM, yyyy"));
                        }

                    }

                    #endregion

                    using (transaction = Session.BeginTransaction())
                    {
                        foreach (var dayOffAdjustment in dayOffAdjustments)
                        {
                            #region form check

                            if (String.IsNullOrEmpty(dayOffAdjustment.Reason.Trim()))
                            {
                                throw new InvalidDataException("Pin " + dayOffAdjustment.TeamMember.Pin +
                                                               " , please fill Reason for Dayoff " +
                                                               dayOffAdjustment.DayOffDate.Value.ToString("dd MMM, yyyy") +
                                                               " and Instead of date " +
                                                               dayOffAdjustment.InsteadOfDate.Value.ToString(
                                                                   "dd MMM, yyyy"));
                            }
                            if (dayOffAdjustment.InsteadOfDate == null)
                            {
                                throw new InvalidDataException("Pin " + dayOffAdjustment.TeamMember.Pin +
                                                               " , please fill Instead of date for Dayoff " +
                                                               dayOffAdjustment.DayOffDate.Value.ToString("dd MMM, yyyy"));
                            }
                            if (dayOffAdjustment.DayOffDate == null)
                            {
                                throw new InvalidDataException("Pin " + dayOffAdjustment.TeamMember.Pin +
                                                               " , please fill Dayoff of date for Instead of " +
                                                               dayOffAdjustment.InsteadOfDate.Value.ToString(
                                                                   "dd MMM, yyyy"));
                            }

                            #endregion

                            #region Is Post Check 

                            DateTime dayOffDate = dayOffAdjustment.DayOffDate.Value.Date;
                            DateTime insteadOfDate = dayOffAdjustment.InsteadOfDate.Value.Date;
                            var lastPostDate =
                                GetTeamMemberAttendanceAdjustmentPostDateTime(dayOffAdjustment.TeamMember.Id,
                                    dayOffAdjustment.TeamMember.Pin);
                            if (lastPostDate != null && (lastPostDate >= dayOffDate || lastPostDate >= insteadOfDate))
                            {
                                throw new InvalidDataException("[PIN : " + dayOffAdjustment.TeamMember.Pin + "(" +
                                                               dayOffDate.ToString("dd MMM, yyyy") + ")" +
                                                               "] This Dayoff Adjustment is already finalized(" +
                                                               lastPostDate.Value.ToString("dd MMM, yyyy") + ")" +
                                                               ". You can't Approve this.");
                            }

                            #endregion


                            #region holiday & Weekend Check

                            HolidayWeekendCheck(dayOffAdjustment);

                            #endregion

                            #region Dublicate DayOff Check

                            bool check = _dayOffAdjustmentDao.CheckHasDuplicate(dayOffAdjustment);
                            if (check)
                            {
                                throw new InvalidDataException("Pin " + dayOffAdjustment.TeamMember.Pin +
                                                               " has Already a day off for Dayoff " +
                                                               dayOffAdjustment.DayOffDate.Value.ToString("dd MMM, yyyy") +
                                                               " and Instead of date " +
                                                               dayOffAdjustment.InsteadOfDate.Value.ToString(
                                                                   "dd MMM, yyyy"));
                            }

                            #endregion

                            #region HolidayWork Check

                            HolidayWork holidayWorkCheck =
                                _holidayWorkDao.GetTeamMemberHolidayWork(dayOffAdjustment.TeamMember.Id,
                                    dayOffAdjustment.InsteadOfDate);
                            if (holidayWorkCheck != null)
                            {
                                throw new InvalidDataException("Pin " + dayOffAdjustment.TeamMember.Pin +
                                                               " has Already taken HolidayWork For " +
                                                               dayOffAdjustment.InsteadOfDate.Value.ToString(
                                                                   "dd MMM, yyyy"));
                            }

                            #endregion

                            _dayOffAdjustmentDao.Save(dayOffAdjustment);
                            var dayOffAdjustmentLog = new DayOffAdjustmentLog
                            {
                                DayOffAdjustment = dayOffAdjustment,
                                DayOffDate = dayOffAdjustment.DayOffDate,
                                DayOffStatus = dayOffAdjustment.DayOffStatus,
                                Reason = dayOffAdjustment.Reason,
                                InsteadOfDate = dayOffAdjustment.InsteadOfDate,
                                CreationDate = DateTime.Now,
                                CreateBy = GetCurrentUserId()
                            };
                            Session.Save(dayOffAdjustmentLog);
                            //}

                        }
                        transaction.Commit();
                        return true;
                    }
                }
                return false;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public DayOffAdjustment LoadById(long id)
        {
            try
            {
                return _dayOffAdjustmentDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<DayOffAdjustmentMentorDto> LoadDayOffRecentAdjustmentForMentor(TeamMember mentor, int start, int length, string orderBy, string toUpper,
            string pin, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (mentor == null)
                    throw new InvalidDataException("Invalid mentor");

                List<long> mentorTeamMemberIdList =
                    _mentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), dateTo);
                return _dayOffAdjustmentDao.LoadDayOffRecentAdjustmentForMentor(mentorTeamMemberIdList, start, length,
                    orderBy, toUpper, pin, dateFrom, dateTo);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<DayOffAdjustmentHrDto> LoadAllHrDayOffAdjustmentHistory(List<UserMenu> userMenus, int start, int length, string orderBy, string toUpper, string pin, DateTime dateFrom, DateTime dateTo, long? org, long? branch, long? campus, long? department)
        {
            try
            {
                if (userMenus == null)
                    throw new InvalidDataException("Invalid menu");

                var organizationIdList = (org != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(org)) : null;
                var branchIdIdList = (branch != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(branch)) : null;
                var departmentIdList = (department != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(department)) : null;
                var campusIdList = (campus != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(campus)) : null;
                var convertedPin = (pin != "") ? Convert.ToInt32(pin) : 0;
                List<long> authOrganizationList = AuthHelper.LoadOrganizationIdList(userMenus, organizationIdList);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, authOrganizationList, null, branchIdIdList);
                return _dayOffAdjustmentDao.LoadAllHrDayOffAdjustmentHistory(start, length, orderBy, toUpper, convertedPin, dateFrom, dateTo, authBranchIdList, campusIdList, departmentIdList);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int CountDayOffRecentAdjustmentForMentor(TeamMember mentor, string pin, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (mentor == null)
                    throw new InvalidDataException("Invalid mentor");
                List<long> mentorTeamMemberIdList = _mentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), dateTo);
                return _dayOffAdjustmentDao.CountDayOffRecentAdjustmentForMentor(mentorTeamMemberIdList, pin, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public int CountHrDayOffAdjustmentHistory(List<UserMenu> userMenus, string pin, DateTime dateFrom, DateTime dateTo, long? org, long? branch, long? campus, long? department)
        {
            try
            {
                if (userMenus == null)
                    throw new InvalidDataException("Invalid menu");

                var organizationIdList = (org != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(org)) : null;
                var branchIdIdList = (branch != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(branch)) : null;
                var departmentIdList = (department != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(department)) : null;
                var campusIdList = (campus != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(campus)) : null;
                var convertedPin = (pin != "") ? Convert.ToInt32(pin) : 0;
                List<long> authOrganizationList = AuthHelper.LoadOrganizationIdList(userMenus, organizationIdList);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, authOrganizationList, null, branchIdIdList);
                return _dayOffAdjustmentDao.CountHrDayOffAdjustmentHistory(convertedPin, dateFrom, dateTo, authBranchIdList, campusIdList, departmentIdList);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public DateTime? GetTeamMemberAttendanceAdjustmentPostDateTime(long? teamMemberId, int? pin = null)
        {
            DateTime? postDateTime = null;
            DateTime? lastPostDayOffDateTime = null; 

            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(teamMemberId, pin);

            DayOffAdjustment dayOffAdjustmentOrderByDayOff =
                _dayOffAdjustmentDao.GetTeamMemberLastIsPostDayOffAdjustment(teamMemberId, pin);

            DayOffAdjustment dayOffAdjustmentOrderByInstedDate =
               _dayOffAdjustmentDao.GetTeamMemberLastIsPostDayOffAdjustment(teamMemberId, pin, "InsteadOfDate");

            if (dayOffAdjustmentOrderByDayOff != null && dayOffAdjustmentOrderByInstedDate!=null)
            {
                lastPostDayOffDateTime = dayOffAdjustmentOrderByDayOff.DayOffDate;
                if (dayOffAdjustmentOrderByInstedDate.InsteadOfDate>lastPostDayOffDateTime)
                {
                    lastPostDayOffDateTime = dayOffAdjustmentOrderByDayOff.InsteadOfDate;
                }
            }


            if (salarySheet != null && lastPostDayOffDateTime != null)
            {
                postDateTime = salarySheet.EndDate;
                if (lastPostDayOffDateTime > postDateTime)
                    postDateTime = lastPostDayOffDateTime;
            }
            else if (salarySheet != null)
            {
                postDateTime = salarySheet.EndDate;
            }
            else if (lastPostDayOffDateTime != null)
            {
                postDateTime = lastPostDayOffDateTime;
            }
            return postDateTime;
        }

        #endregion

        #region Helper function

        private void HolidayWeekendCheck(DayOffAdjustment dayOffAdjustment)
        {
            TeamMember teamMemnber = dayOffAdjustment.TeamMember;
            int memberWeekend = _teamMemberDao.GetMemberWeekend(teamMemnber.Id);
            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), dayOffAdjustment.DayOffDate.Value.DayOfWeek.ToString()))
            {
                throw new InvalidDataException("Pin " + dayOffAdjustment.TeamMember.Pin + " has weekend for Dayoff " + dayOffAdjustment.DayOffDate.Value.ToString("dd MMM ,yyyy") + " and Instead of date " + dayOffAdjustment.InsteadOfDate.Value.ToString("dd MMM, yyyy"));
            }
            Organization organization = _teamMemberDao.GetTeamMemberOrganization(DateTime.Now, teamMemnber.Id, null);
            HolidaySetting holidaySetting = _holidaySettingDao.GetHolidayByDate(dayOffAdjustment.DayOffDate.Value.Date, organization);
            if (holidaySetting != null)
            {
                throw new InvalidDataException("Pin " + dayOffAdjustment.TeamMember.Pin + " has Holiday for Dayoff " + dayOffAdjustment.DayOffDate.Value.ToString("dd MMM ,yyyy") + " and Instead of date " + dayOffAdjustment.InsteadOfDate.Value.ToString("dd MMM, yyyy"));
            }
        }

        #endregion

    }
}
