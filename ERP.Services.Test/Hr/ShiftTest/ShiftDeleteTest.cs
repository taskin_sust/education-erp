﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ShiftTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Shift")]
    public class ShiftDeleteTest : ShiftBaseTest
    {
        #region Basic Test
        
        [Fact]
        public void Should_Delete_Successfully()
        {
            var shiftFactory1 =
                _shiftFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithStartTimeEndTime(_startTime,_endTime).Persist();
            Shift shift = _shiftService.LoadById(shiftFactory1.Object.Id);
            var success = _shiftService.Delete(shift.Id);
            Assert.True(success);
            shift = _shiftService.LoadById(shiftFactory1.Object.Id);
            Assert.Null(shift);

        }

        [Fact]
        public void Delete_Should_Return_Null_Exception()
        {
            Assert.Throws<InvalidDataException>(() => _shiftService.Delete(0));
        }
        
        [Fact]
        public void Delete_Holiday_Setting_Should_Not_Delete_Organization()
        {
            var shiftFactory1 =
                _shiftFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithStartTimeEndTime(_startTime,_endTime).Persist();
            Shift zoneSetting = _shiftService.LoadById(shiftFactory1.Object.Id);
            var organizationId = zoneSetting.Organization.Id;
            _shiftService.Delete(zoneSetting.Id);
            var organization = _organizationService.LoadById(organizationId);
            Assert.NotNull(organization);
        } 
        #endregion

    }
}
