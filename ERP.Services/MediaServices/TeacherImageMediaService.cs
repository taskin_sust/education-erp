﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.MediaDao;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.MediaServices
{
    public interface ITeacherImageMediaService
    {

        void Save(BusinessModel.Entity.MediaDb.TeacherMediaImage mediaImage);

        IEnumerable<TeacherMediaImage> GetByHash(string hash);
        TeacherMediaImage GetByRefId(long teacherImageId);
    }
    public class TeacherImageMediaService : ITeacherImageMediaService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeacherService");
        #endregion

        private readonly IStudentImageMediaDao _studentImageMediaDao;
        private readonly ITeacherImageMediaDao _teacherImageMediaDao;
        private ISession session;
        public TeacherImageMediaService(ISession mediaSession = null)
        {
            try
            {
                if (mediaSession == null)
                {
                    var sessionMedia = NHibernateMediaSessionFactory.OpenSession();
                    session = sessionMedia;
                    //_studentImageMediaDao = new StudentImageMediaDao(sessionMedia);
                    //_teacherImageMediaDao = new TeacherImageMediaDao(sessionMedia);
                }
                else
                {
                    session = mediaSession;
                }

                _studentImageMediaDao = new StudentImageMediaDao(session);
                _teacherImageMediaDao = new TeacherImageMediaDao {Session = session}; //(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }

        public void Save(TeacherMediaImage mediaImage)
        {
            using (var trans = session.BeginTransaction())
            {
                try
                {
                    _teacherImageMediaDao.Save(mediaImage);
                    trans.Commit();
                }
                catch (Exception e)
                {
                    throw e;
                }

            }

        }

        public IEnumerable<TeacherMediaImage> GetByHash(string hash)
        {
            return _teacherImageMediaDao.GetByHash(hash);
        }

        public TeacherMediaImage GetByRefId(long teacherImageId)
        {
            return _teacherImageMediaDao.GetByRefId(teacherImageId);
        }
    }
}
