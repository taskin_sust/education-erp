﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ChildrenAllowanceSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ChildrenAllowanceSettingService")]
    public class ChildrenAllowanceSettingLoadTest : ChildrenAllowanceSettingBaseTest
    {
        #region List Load Test

        [Fact]
        public void Should_Return_Invalid_Permission_Exception()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.LoadChilderenAllowanceSetting(0, 0, int.MaxValue, null, _organizationFactory.Object.Id, (int)MemberEmploymentStatus.Contractual, _childrenAllowanceSettingFactory.Object.PaymentType));
        }

        [Fact]
        public void Should_Return_UnAuthozize_UserPermission_Of_Organization_Successfully()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            Assert.Throws<UnauthorizeDataException>(() => _childrenAllowanceSettingService.LoadChilderenAllowanceSetting(0, 0, int.MaxValue, menu, _organizationFactory.Create().Persist().Object.Id, (int)MemberEmploymentStatus.Contractual, _childrenAllowanceSettingFactory.Object.PaymentType));
        }

        #endregion 

        #region Single Object Load

        [Fact]
        public void Should_Return_Invalid_Data_Exception()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));

            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.GetChildrenAllowanceSetting(0));
        }

        [Fact]
        public void Should_Return_Obj_Successfully()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            Assert.Equal(_childrenAllowanceSettingFactory.Object.Id, _childrenAllowanceSettingService.GetChildrenAllowanceSetting(_childrenAllowanceSettingFactory.Object.Id).Id);
        }

        #endregion

        #region Count Test

        [Fact]
        public void Should_Return_Invalid_Permission_Exception_ForCount()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            Assert.Throws<AuthenticationException>(() => _childrenAllowanceSettingService.GetChildrenAllowanceSettingAllowanceCount(null, _organizationFactory.Object.Id, (int)MemberEmploymentStatus.Contractual, _childrenAllowanceSettingFactory.Object.PaymentType));
        }
        
        [Fact]
        public void Should_Return_One_Data_Successfully()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            var count = _childrenAllowanceSettingService.GetChildrenAllowanceSettingAllowanceCount(menu, _organizationFactory.Object.Id, (int)MemberEmploymentStatus.Contractual, _childrenAllowanceSettingFactory.Object.PaymentType);
            Assert.True(count == 1);
        }

        #endregion
    }
}
