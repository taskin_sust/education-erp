﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Teachers
{

    public interface ITeacherClassEntryDetailsDao : IBaseDao<TeacherClassEntryDetails, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<TeacherClassEntryDto> LoadTeacherClassEntryDetails(int start, int length, string orderBy, string orderDir, long organization, long program, long session, long branch, long campus, int paymentStatus, string teacherName, string heldDate, string trackingId, string tpin);        
        #endregion

        #region Others Function
        long TeacherClassEntryDetailsRowCount(long organization, long program, long session, long branch, long campus, int paymentStatus, string teacherName, string heldDate, string trackingId, string tpin);      
        #endregion
    }

    public class TeacherClassEntryDetailsDao : BaseDao<TeacherClassEntryDetails, long>, ITeacherClassEntryDetailsDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<TeacherClassEntryDto> LoadTeacherClassEntryDetails(int start, int length, string orderBy, string orderDir, long organization, long program, long session, long branch, long campus, int paymentStatus, string teacherName, string heldDate, string trackingId, string tpin)
        {
            var criteria = GetTeacherClassEntryQuery(organization, program, session, branch, campus, paymentStatus, teacherName, heldDate, trackingId, tpin);
            criteria.AddOrder(Order.Desc(Projections.Id()));
            criteria.SetFirstResult(start);
            criteria.SetMaxResults(length);
            criteria.SetProjection(Projections.ProjectionList()
                                        //.Add(Projections.SqlFunction("concat",
                                        //    NHibernateUtil.String,
                                        //    Projections.Property("t.NickName"),
                                        //    Projections.Constant('('),
                                        //    Projections.SqlFunction("CONVERT",NHibernateUtil.String, Projections.Property("t.HscPassingYear")),
                                        //   // Projections.Property("t.HscPassingYear"),
                                        //    Projections.Constant(')')), "TeacherName")
                                         .Add(Projections.Property("t.Tpin"), "Tpin")
                                         .Add(Projections.Property("t.NickName"), "TeacherName")
                                         .Add(Projections.Property("t.FullName"), "TeacherFullName")
                                         .Add(Projections.Property("t.HscPassingYear"), "HscPassingYear")
                                         .Add(Projections.Property("Id"), "Id")
                                         .Add(Projections.Property("tc.Id"), "ClassEntryId")
                                         .Add(Projections.Property("Description"), "Description")
                                         .Add(Projections.Property("Duration"), "Duration")
                                         .Add(Projections.Property("Quantity"), "Quantity")
                                         .Add(Projections.Property("Amount"), "Amount")
                                         .Add(Projections.Property("PaymentStatus"), "PaymentStatus")
                                         .Add(Projections.Property("tc.HeldDate"), "HeldDate")
                                         );
            criteria.SetResultTransformer(Transformers.AliasToBean(typeof(TeacherClassEntryDto)));
            return criteria.List<TeacherClassEntryDto>().ToList();

            //return criteria.List<TeacherClassEntryDetails>().ToList();    Projections.Cast(NHibernateUtil.String, Projections.Property("Id")       
        }

        #endregion

        #region Others Function

       
        public long TeacherClassEntryDetailsRowCount(long organization, long program, long session, long branch, long campus,
            int paymentStatus, string teacherName, string heldDate, string trackingId, string tpin)
        {
            var criteria = GetTeacherClassEntryQuery(organization, program, session, branch, campus, paymentStatus, teacherName, heldDate, trackingId, tpin);
            criteria.SetProjection(Projections.CountDistinct("Id"));
            return (int)criteria.UniqueResult();
        }

        

        #endregion

        #region Helper Function
        private ICriteria GetTeacherClassEntryQuery(long organization, long program, long session, long branch, long campus, int paymentStatus, string teacherName, string heldDate, string trackingId, string tpin)
        {
           // throw new NotImplementedException();
            ICriteria criteria = Session.CreateCriteria<TeacherClassEntryDetails>().Add(Restrictions.Eq("Status", TeacherClassEntryDetails.EntityStatus.Active));
            //criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));PaymentStatusHeldDate
            criteria.CreateAlias("TeacherClassEntry", "tc").Add(Restrictions.Eq("tc.Status", TeacherClassEntry.EntityStatus.Active));
            criteria.CreateAlias("Teacher", "t").Add(Restrictions.Eq("t.Status", Teacher.EntityStatus.Active));
            //criteria.CreateAlias("tc.Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            //criteria.CreateAlias("tc.Branch", "b").Add(Restrictions.Eq("b.Status", Branch.EntityStatus.Active));
            //criteria.CreateAlias("tc.Campus", "c").Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));
            //criteria.CreateAlias("tc.Session", "s").Add(Restrictions.Eq("s.Status", Model.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("tc.Organization", "o").Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active)).Add(Restrictions.Eq("o.Id", organization));
           
            //criteria.Add(program == -1 ? Restrictions.IsNull("p") : Restrictions.Eq("p.Id", program));
            //criteria.Add(session == -1 ? Restrictions.IsNull("s") : Restrictions.Eq("s.Id", session));
            //criteria.Add(branch == -1 ? Restrictions.IsNull("b") : Restrictions.Eq("b.Id", branch));
            //criteria.Add(campus == -1 ? Restrictions.IsNull("c") : Restrictions.Eq("c.Id", campus));

            if (!String.IsNullOrEmpty(tpin))
            {
                int pin;
                if (int.TryParse(tpin, out pin))
                {
                    criteria.Add(Restrictions.Eq("t.Tpin", pin)); 
                }
            }

            if (program == -1)
            {
                criteria.Add(Restrictions.IsNull("tc.Program"));
            }
            else
            {
                criteria.CreateAlias("tc.Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("p.Id", program));
            }
            if (session == -1)
            {
                criteria.Add(Restrictions.IsNull("tc.Session"));
            }
            else
            {
                criteria.CreateAlias("tc.Session", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("s.Id", session));
            }
            if (branch == -1)
            {
                criteria.Add(Restrictions.IsNull("tc.Branch"));
            }
            else
            {
                criteria.CreateAlias("tc.Branch", "b").Add(Restrictions.Eq("b.Status", Branch.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("b.Id", branch));
            }
            if (campus == -1)
            {
                criteria.Add(Restrictions.IsNull("tc.Campus"));
            }
            else
            {
                criteria.CreateAlias("tc.Campus", "c").Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("c.Id", campus));
            }

            if (paymentStatus != 0)
            {
                criteria.Add(Restrictions.Eq("PaymentStatus", paymentStatus));
            }
            if (!String.IsNullOrEmpty(teacherName))
            {
                criteria.Add(Restrictions.Like("t.NickName", teacherName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(heldDate))
            {
                //DateTime dt = Convert.ToDateTime(heldDate);
                DateTime dt = DateTime.ParseExact(heldDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
               
                criteria.Add(
                    Expression.Eq(Projections.SqlFunction("date", NHibernateUtil.DateTime,
                        Projections.Property("tc.HeldDate")),dt));
              //  return session.CreateQuery("from MyClass where date(MyDate) = :day")
              //.SetParameter("day", day.Date)
              //.List<MyClass>(); //executes
              //  return session.CreateCriteria<Foo>()
              //.Add(Restrictions.Eq(
              //         Projections.SqlFunction("date",
              //                                 NHibernateUtil.Date,
              //                                 Projections.Property("MyDate")),
              //         day.Date))
              //  .List<MyClass>(); //executes
            }
            if (!String.IsNullOrEmpty(trackingId))
            {
                long convertTrackingId;
                var result = long.TryParse(trackingId, out convertTrackingId);
                if (result)
                    criteria.Add(Restrictions.Eq("tc.Id", Convert.ToInt64(convertTrackingId)));
            }
            return criteria;
        }
        
        #endregion

        
    }
}
