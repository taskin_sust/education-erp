﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Exam;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentExamAttendanceService : IBaseService
    {
        #region Operational Function
        bool ClearExamAttendace(string examIdList,string userIds, DateTime dateFrom, DateTime dateTo);
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Functions

        List<ResponseMessage> Save(List<BusinessModel.Entity.UserAuth.UserMenu> _userMenu, long programId, long examId, DateTime heldDate, string[] studentRolls, string[] studentFeedback, out List<string> successRollsList);
        IList<IndividualExamAttendanceDto> GetIndividualReport(string programRoll, int attendanceType, DateTime dateFrom, DateTime datetTo);
        IList<ExamEvaluationReportDto> LoadExamEvaluationDto(int start, int length, string orderby, string orderDir, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList,List<long>batchIds );
        IList<StudentExamAttendanceDetails> LoadExamEvaluationForIndividualReport(List<UserMenu> userMenus, int start, int length, long programId, long sessionId, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, string batchIds);

        #endregion

        #region Other Functions
       
        int CountRowCountForIndividualExamEvaluationReport(List<UserMenu> userMenus, long programId, long sessionId, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, DateTime fromDate, DateTime toDate, List<long> examIdList,string batchIds);
        int GetTotalStudentCount(List<UserMenu> userMenu, long programId, long sessionId, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, DateTime dateFrom, DateTime dateTo, List<long> examIdList, string batchIds,string userIds=null);
        IList<ExamAttendanceReportDto> LoadExamAttendanceReport(int start, int length, List<UserMenu> usermenu, long programId, long sessionId, long[] courseId, DateTime startDate,
            DateTime endDate, string batchIds, long[] paymentStatus, long attendanceStatus,long[]examIds);
        int GetExamAttendanceCount(List<UserMenu> usermenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds, long[] courseIds,long[] examIds);
        int GetTotalStudentCount(List<UserMenu> userMenu,long organizationId, long programId, long sessionId, DateTime dateFrom, DateTime dateTo, List<long> examIdlist, string userIds);
        #endregion
        int GetExamEvaluationCountDto(int start, int length, string orderby, string orderDir, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, List<long> batchIds);
        int GetCountOfAttendExam(long subjectId, long stdProgramId, long courseId);

        #region Clean
        
        #endregion
    }

    public class StudentExamAttendanceService : BaseService, IStudentExamAttendanceService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization

        private readonly IStudentExamAttendanceDao _studentExamAttendenceDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly ICommonHelper _commonHelper;
        private readonly IExamsDao _examsDao;
        private readonly IBatchDao _batchDao;
        private readonly IStudentExamAttendanceDetailsDao _studentExamAttendanceDetailsDao;
        private readonly IExamProgressDao _examProgressDao;
        private IComplementaryCourseDao _complementaryCourseDao;
        private readonly ICourseSubjectDao _courseSubjectDao;
        public StudentExamAttendanceService(ISession session)
        {
            Session = session;
            _studentExamAttendenceDao = new StudentExamAttendanceDao {Session = session};
            _studentProgramDao = new StudentProgramDao {Session = session};
            _examsDao = new ExamsDao {Session = session};
            _batchDao = new BatchDao {Session = session};
            _commonHelper = new CommonHelper();
            _complementaryCourseDao = new ComplementaryCourseDao() { Session = session };
            _courseSubjectDao = new CourseSubjectDao { Session = session };
            _studentExamAttendanceDetailsDao=new StudentExamAttendanceDetailsDao(){Session = session};
            _examProgressDao = new ExamProgressDao()
            { Session = session };
        }

        #endregion

        #region Operational Function

        public bool ClearExamAttendace(string examIdList,string userIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                _studentExamAttendenceDao.ClearExamAttendace(examIdList,userIds, dateFrom, dateTo);
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<IndividualExamAttendanceDto> GetIndividualReport(string programRoll, int attendanceType, DateTime dateFrom,
            DateTime datetTo)
        {
            try
            {
                IList<IList<string>> individualAttendanceReportList = new List<IList<string>>();
                var studentProgram = _studentProgramDao.GetStudentProgram(programRoll, true);
                var examsInStudentBatch = studentProgram.Batch.ExamProgresses.Where(x => x.HeldDate.Date >= dateFrom.Date && x.HeldDate.Date <= datetTo.Date).Select(x => x.Exam).ToList();
                var examsDoneByStudent =
                    studentProgram.StudentExamAttendanceDetails.Where(x => x.StudentExamAttendance.HeldDate >= dateFrom && x.StudentExamAttendance.HeldDate <= datetTo.Date && x.Status==1 && x.StudentExamAttendance.Status==1).Select(x => x.StudentExamAttendance.Exams).ToList();
                var individualAttendanceList = new List<IndividualExamAttendanceDto>();
                foreach (var exams in examsDoneByStudent)
                {
                    var individualAttendance = new IndividualExamAttendanceDto();
                    var heldDate = _examProgressDao.GetExamDate(exams.Id);
                    if (heldDate != null && heldDate.Count > 0)
                    {
                        individualAttendance.HeldDate = heldDate.FirstOrDefault().ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        continue;

                    }
                    heldDate = _examProgressDao.GetExamDate(exams.Id, studentProgram.Batch.Id);
                    if (heldDate != null && heldDate.Count > 0)
                    {
                        individualAttendance.HeldDate = heldDate.FirstOrDefault().ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        individualAttendance.HeldDate = "N/A";
                    }
                    individualAttendance.ExamName = exams.Name;
                    if (exams.ExamDetails[0].SubjectType != 3)
                    {
                        //var subjectName = "";
                        //foreach (var ed in exams.ExamDetails)
                        //{
                        //    subjectName += ed.Subject.Name + ", ";
                        //}
                        //subjectName = subjectName.Trim(new[] { ',', ' ' });
                        //individualAttendance.SubjectName = subjectName;
                        individualAttendance.SubjectName = string.Join(", ", exams.ExamDetails.Select(x => x.Subject.Name).Distinct().ToList());
                    }
                    else
                    {
                        individualAttendance.SubjectName = "Combined";
                    }

                    var studentExam = studentProgram.StudentExamAttendanceDetails.Where(
                        x => x.StudentExamAttendance.Exams.Id == exams.Id && x.Status==1 && x.StudentExamAttendance.Status==1).ToList().FirstOrDefault();
                    individualAttendance.AttendingDate = Convert.ToDateTime(studentExam.StudentExamAttendance.HeldDate).ToString("dd-MM-yyyy");
                    individualAttendance.Status = studentExam.IsRegistered ? "Registered" : "Unregistered";
                    individualAttendanceList.Add(individualAttendance);
                }
                var examsNotDoneByStudent = examsInStudentBatch.Except(examsDoneByStudent).ToList();
                if (examsNotDoneByStudent.Count > 0)
                {
                    foreach (var exam in examsNotDoneByStudent)
                    {



                        var studentComplementaryCourseIds =
                                    _complementaryCourseDao.LoadComplementaryCourse(studentProgram.Id);
                        var isregistered = false;
                        if (studentComplementaryCourseIds.Count > 0 && studentComplementaryCourseIds.Contains(exam.Course.Id))
                        {
                            isregistered = true;
                        }
                        else
                        {
                            var studentCourseSubjects =
                            studentProgram.StudentCourseDetails.Where(
                                x => x.Status == 1).Select(x => x.CourseSubject.Id).ToList();
                            var studentCourses =
                            studentProgram.StudentCourseDetails.Where(
                                x => x.Status == 1).Select(x => x.CourseSubject.Course.Id).ToList();
                            foreach (var ed in exam.ExamDetails)
                            {
                                if (ed.SubjectType != 3)
                                {
                                    var courseSubject =
                                        _courseSubjectDao.LoadCourseSubjectByCourseIdAndSubjectId(
                                            exam.Course.Id, ed.Subject.Id);
                                    if (studentCourseSubjects.Contains(courseSubject.Id))
                                    {
                                        isregistered = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (studentCourses.Contains(exam.Course.Id))
                                    {
                                        isregistered = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (isregistered)
                        {
                            var individualAttendance = new IndividualExamAttendanceDto();
                            individualAttendance.ExamName = exam.Name;
                            if (exam.ExamDetails[0].SubjectType != 3)
                            {
                                individualAttendance.SubjectName = string.Join(", ",exam.ExamDetails.Select(x => x.Subject.Name).Distinct().ToList());

                            }
                            else
                            {
                                individualAttendance.SubjectName = "Combined";
                            }
                            var heldDate = _examProgressDao.GetExamDate(exam.Id, studentProgram.Batch.Id); 
                            if (heldDate != null)
                            {
                                individualAttendance.HeldDate = heldDate.FirstOrDefault().ToString("dd-MM-yyyy");
                            }
                            else
                            {
                                individualAttendance.HeldDate = "N/A";
                            }
                            individualAttendance.AttendingDate = "N/A";
                            individualAttendance.Status = "Absent";
                            individualAttendanceList.Add(individualAttendance);
                        }
                    }

                }

                return individualAttendanceList.OrderBy(x => x.ExamName).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }

        }
        public IList<ExamEvaluationReportDto> LoadExamEvaluationDto(int start, int length, string orderby, string orderDir, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, List<long> batchIds)
        {
            try
            {
                return _studentExamAttendenceDao.LoadExamEvaluationDto(start, length, orderby, orderDir, programId, sessionId, courseIdList, branchIdList, campusIdList, batchIds, subjectIdList, fromDate, toDate, examIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public IList<StudentExamAttendanceDetails> LoadExamEvaluationForIndividualReport(List<UserMenu> userMenus, int start, int length, long programId, long sessionId, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, string batchIds)
        {
            try
            {
                List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenus, null, CommonHelper.ConvertSelectedAllIdList(branchIdList), _commonHelper.ConvertIdToList(programId));
                List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenus, null, _commonHelper.ConvertIdToList(programId), CommonHelper.ConvertSelectedAllIdList(branchIdList));
                return _studentExamAttendenceDao.LoadExamEvaluationForIndividualReport(start, length, authorizedProgramIdList, sessionId, authorizedBranchIdList, campusIdList, batchIdList, fromDate, toDate, examIdList,batchIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public List<ResponseMessage> Save(List<BusinessModel.Entity.UserAuth.UserMenu> _userMenu, long programId, long examId, DateTime heldDate, string[] studentRolls, string[] studentFeedback, out List<string> successRollsList)
        {
            successRollsList = new List<string>();
            var messageList = new List<ResponseMessage>();
            using (var trans = Session.BeginTransaction())
            {
                var examAttendance = _studentExamAttendenceDao.LoadExamAttendance(examId,heldDate);
                long studentExamAttendenceId = 0;
                if (examAttendance.Count > 0)
                {
                    _studentExamAttendenceDao.Update(examAttendance.FirstOrDefault());
                    studentExamAttendenceId = examAttendance.FirstOrDefault().Id;
                }
                else
                {
                    var exam = _examsDao.LoadById(examId);
                    if (exam == null)
                    {
                        throw new Exception("Exam not found");
                    }
                    var studentExamAttendance = new StudentExamAttendance()
                    {
                        Exams=exam,
                        HeldDate = heldDate,
                    };
                    _studentExamAttendenceDao.Save(studentExamAttendance);
                studentExamAttendenceId=studentExamAttendance.Id;
                }
                int i = 0;
                foreach (var studentRoll in studentRolls)
                {

                    try
                    {

                        var studentProgram = _studentProgramDao.GetStudentProgram(studentRoll, false, true);
                        if (null == studentProgram)
                        {
                            messageList.Add(
                                new ResponseMessage(errorMessage:
                                    studentRoll + "does not exist. "));
                            continue;
                        }
                        if (studentProgram.Program.Id != programId)
                        {
                            messageList.Add(
                                new ResponseMessage(errorMessage:
                                    studentRoll + "is not valid for this program. "));
                            continue;
                        }
                        if (studentProgram.CreationDate.Date > heldDate.Date)
                        {
                            messageList.Add(
                                new ResponseMessage(errorMessage:
                                    studentRoll + "is not valid for this date. "));
                        }
                        else
                        {
                            var examAttendanceCount =
                                _studentExamAttendenceDao.ExamAttendanceCount(
                                    examId, studentProgram.Id);
                            if ((int)examAttendanceCount < 1)
                            {

                               var sead = new StudentExamAttendanceDetails();
                               sead.StudentExamAttendance =
                                    _studentExamAttendenceDao.LoadById(studentExamAttendenceId);
                               sead.StudentProgram = studentProgram;
                                if (!string.IsNullOrEmpty(studentFeedback[i]))
                                {
                                    sead.StudentFeedback = Convert.ToInt32(studentFeedback[i]);
                                }
                                sead.IsRegistered = _studentExamAttendenceDao.IsRegisteredExam(studentProgram.Id, examId, heldDate);
                                _studentExamAttendanceDetailsDao.Save(sead);
                                successRollsList.Add(studentRoll);
                                messageList.Add(
                              new ResponseMessage(successMessage: studentRoll + " has attended."));


                            }
                            else
                            {
                                messageList.Add(
                                    new ResponseMessage(errorMessage:
                                        "Attendance is given for " + studentRoll + " already. "));
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        messageList.Add(
                            new ResponseMessage(errorMessage:
                                "Something went wrong"));
                    }
                    i++;
                }
                trans.Commit();
            }

            return messageList;

        }

        #endregion

        #region Other Functions
        public int CountRowCountForIndividualExamEvaluationReport(List<UserMenu> userMenus, long programId, long sessionId, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, DateTime fromDate, DateTime toDate, List<long> examIdList,string batchIds)
        {
            try
            {
                List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenus, null, CommonHelper.ConvertSelectedAllIdList(branchIdList), _commonHelper.ConvertIdToList(programId));
                List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenus, null, _commonHelper.ConvertIdToList(programId), CommonHelper.ConvertSelectedAllIdList(branchIdList));
                return _studentExamAttendenceDao.CountRowCountForIndividualExamEvaluationReport(authorizedProgramIdList, sessionId, authorizedBranchIdList, campusIdList, batchIdList, fromDate, toDate, examIdList,batchIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetExamEvaluationCountDto(int start, int length, string orderby, string orderDir, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, List<long> batchIds)
        {
            try
            {
                return _studentExamAttendenceDao.GetExamEvaluationCountDto(start, length, orderby, orderDir, programId, sessionId, courseIdList, branchIdList, campusIdList, batchIds, subjectIdList, fromDate, toDate, examIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetCountOfAttendExam(long subjectId, long stdProgramId, long courseId)
        {
            try
            {
                return _studentExamAttendenceDao.GetCountOfAttendExam(subjectId, stdProgramId, courseId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetTotalStudentCount(List<UserMenu> userMenu, long programId, long sessionId, List<long> branchIdList, List<long> campusIdList,
            List<long> batchIdList, DateTime dateFrom, DateTime dateTo, List<long> examIdList, string batchIds, string userIds=null)
        {
            try
            {
                List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenu, null, CommonHelper.ConvertSelectedAllIdList(branchIdList), _commonHelper.ConvertIdToList(programId));
                List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId), CommonHelper.ConvertSelectedAllIdList(branchIdList));
                return _studentExamAttendenceDao.CountRowCountForIndividualExamEvaluationReport(authorizedProgramIdList, sessionId, authorizedBranchIdList, campusIdList, batchIdList, dateFrom, dateTo, examIdList, batchIds, userIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }                      
        }

        public IList<ExamAttendanceReportDto> LoadExamAttendanceReport(int start, int length, List<UserMenu> usermenu,
            long programId, long sessionId, long[] courseId, DateTime startDate,
            DateTime endDate, string batchIds, long[] paymentStatus, long attendanceStatus, long[] examIds)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(usermenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programId));
                return _studentExamAttendenceDao.LoadExamAttendanceReport(start, length, programId, sessionId, programIdList, branchIdList, courseId, startDate,
                    endDate, batchIds, paymentStatus, attendanceStatus, examIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            
        }

        public int GetExamAttendanceCount(List<UserMenu> usermenu, long programId, long sessionId, long[] branchId,
            long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus,
            long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds, long[] courseIds,long[]examIds)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(usermenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programId));
                return _studentExamAttendenceDao.GetExamAttendanceCount(programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchList, paymentStatus, attendanceStatus, startDate, endDate, batchIds, courseIds, examIds);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public int GetTotalStudentCount(List<UserMenu> userMenu,long organizationId, long programId, long sessionId, DateTime dateFrom, DateTime dateTo, List<long> examIdlist, string userIds)
        {
            try
            {
                List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                return _studentExamAttendenceDao.GetStudentCount(authorizedProgramIdList, programId, sessionId, dateFrom, dateTo, examIdlist, userIds);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region Helper Function

        #endregion

       
    }
}
