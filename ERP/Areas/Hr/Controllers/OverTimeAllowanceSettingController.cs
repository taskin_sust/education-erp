﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using log4net;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class OverTimeAllowanceSettingController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IOvertimeAllowanceSettingService _overTimeAllowanceSettingService;
        private readonly IUserService _userService;

        public OverTimeAllowanceSettingController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _overTimeAllowanceSettingService = new OvertimeAllowanceSettingService(session);
            _userService = new UserService(session);
        }

        #endregion

        #region Index

        public ActionResult Index(string message, string messageType)
        {
            try
            {
                switch (messageType)
                {
                    case "s":
                        ViewBag.SusscessMessage = message;
                        break;
                    case "e":
                        ViewBag.ErrorMessage = message;
                        break;
                }
                InitBasic();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult SettingList(int draw, int start, int length, string organizationIdStr, string employementStatusStr, string holidayTypeStr)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            bool isSuccess = false;
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "1":
                            orderBy = "Organization";
                            break;
                        case "2":
                            orderBy = "Name";
                            break;
                        case "5":
                            orderBy = "CalculationOn";
                            break;
                        case "6":
                            orderBy = "MultiplyingFactor";
                            break;
                        case "7":
                            orderBy = "EffectiveDate";
                            break;
                        default:
                            orderBy = "Organization";
                            break;
                    }
                }
                #endregion

                long organizationId = !string.IsNullOrEmpty(organizationIdStr) ? Convert.ToInt64(organizationIdStr) : 0;
                var userMenus = (List<UserMenu>)ViewBag.UserMenu;
                IList<OvertimeAllowanceSetting> oList = _overTimeAllowanceSettingService.LoadOvertimeAllowanceSetting(userMenus, start, length, orderBy, orderDir, organizationId, employementStatusStr, holidayTypeStr);
                recordsTotal = _overTimeAllowanceSettingService.GetOvertimeAllowanceSettingCount(userMenus, orderBy, orderDir, organizationId, employementStatusStr, holidayTypeStr);
                recordsFiltered = recordsTotal;

                foreach (var row in oList)
                {
                    row.EmploymentStatusString = GetEmploymentStatusString(row.IsProbation, row.IsPermanent, row.IsPartTime, row.IsContractual, row.IsIntern);
                    row.OvertimeTypeString = GetOvertimeTypeString(row.IsWorkingDay, row.IsManagementWeekend, row.IsGazzeted);
                    row.CalculationOnString = Enum.GetName(typeof(CalculationOnTypeEnum), row.CalculationOn);
                    var str = new List<string>
                      {
                         
                          row.Organization.ShortName,
                          row.Name,
                          row.EmploymentStatusString,
                          row.OvertimeTypeString,
                          row.CalculationOnString,
                          row.MultiplyingFactor.ToString("n2"),
                          row.EffectiveDate.ToString("yyyy-MM-dd"),
                           row.ClosingDate!=null?row.ClosingDate.Value.ToString("yyyy-MM-dd"):"",
                          LinkGenerator.GetGeneratedLink("Details", "Edit", "Delete", "OverTimeAllowanceSetting", row.Id, row.Name),
                      };
                    getData.Add(str);
                   
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        #endregion

        #region Oparetional function

        #region Create

        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                InitBasic();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
            }
            return View(new OvertimeAllowanceSettingForm());
        }

        [HttpPost]
        public ActionResult Create(OvertimeAllowanceSettingForm formViewData)
        {
            try
            {
                #region User Interface Validation

                if (!ModelState.IsValid)
                {
                    InitBasic();
                    return View(formViewData);
                }

                var userMenus = (List<UserMenu>)ViewBag.UserMenu;
                var authOrgId = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(formViewData.OrganizationId));
                if (authOrgId.Count <= 0)
                {
                    throw new InvalidDataException("Permission Denied For Organization");
                }

                if (formViewData.EmploymentStatus == null)
                {
                    throw new InvalidDataException("Employement status not selected.");
                }

                if (formViewData.OvertimeTypes == null)
                {
                    throw new InvalidDataException("Overtime type not selected.");
                }

                #endregion

                #region Member Status & Overtime Type

                bool isProbation, isPermanent, isPartTime, isContractual, isIntern = false;
                //GetSelectedEmploymentStatus(formViewData.EmploymentStatus, out isProbation, out isPermanent, out isPartTime, out isContractual, out isIntern);

                isProbation = formViewData.EmploymentStatus.Contains(MemberEmploymentStatus.Probation) ? true : false;
                isPermanent = formViewData.EmploymentStatus.Contains(MemberEmploymentStatus.Permanent) ? true : false;
                isPartTime = formViewData.EmploymentStatus.Contains(MemberEmploymentStatus.PartTime) ? true : false;
                isContractual = formViewData.EmploymentStatus.Contains(MemberEmploymentStatus.Contractual) ? true : false;
                isIntern = formViewData.EmploymentStatus.Contains(MemberEmploymentStatus.Intern) ? true : false;

                bool isGazetted, isManagementWeekend, isRegular = false;
                // GetSelectedOvertimeTypes(formViewData.OvertimeTypes, out isRegular, out isGazetted, out isManagementWeekend);

                isRegular = formViewData.OvertimeTypes.Contains(AllowanceSettingOverTimeType.Regular) ? true : false;
                isGazetted = formViewData.OvertimeTypes.Contains(AllowanceSettingOverTimeType.Gazetted) ? true : false;
                isManagementWeekend = formViewData.OvertimeTypes.Contains(AllowanceSettingOverTimeType.Management) ? true : false;


                #endregion

                #region Assign Value From View Object To Main Object

                var overtimeAllowanceSetting = new OvertimeAllowanceSetting()
                {
                    Organization = _organizationService.LoadById(formViewData.OrganizationId),
                    Name = formViewData.Name,
                    IsProbation = isProbation,
                    IsPermanent = isPermanent,
                    IsPartTime = isPartTime,
                    IsContractual = isContractual,
                    IsIntern = isIntern,
                    IsWorkingDay = isRegular,
                    IsGazzeted = isGazetted,
                    IsManagementWeekend = isManagementWeekend,
                    CalculationOn = formViewData.CalculationOn,
                    MultiplyingFactor = Convert.ToDecimal(formViewData.MultiplyingFactor),
                    EffectiveDate = formViewData.EffectiveDate,
                    ClosingDate = formViewData.ClosingDate != null ? formViewData.ClosingDate : null
                };

                #endregion

                var returnValue = _overTimeAllowanceSettingService.SaveOrUpdate(userMenus, overtimeAllowanceSetting);
                if (returnValue)
                {
                    ViewBag.SuccessMessage = "Overtime successfully saved.";
                    ModelState.Clear();
                }
                else
                {
                    GetSelectedCheckBox(formViewData);
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (NullObjectException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            finally
            {
                GetSelectedCheckBox(formViewData);
            }
            InitBasic();

            return View(new OvertimeAllowanceSettingForm());
        }


        #endregion

        #region Update

        [HttpGet]
        public ActionResult Edit(long id)
        {
            var errorMessage = "";
            try
            {
                InitBasic();
                if (id <= 0)
                {
                    return RedirectToAction("Index", "ExtraDayAllowanceSetting", new { message = "Invalid request submitted. Please try again!", type = "e" });
                }
                OvertimeAllowanceSetting entity = _overTimeAllowanceSettingService.GetOvertimeAllowanceSetting((List<UserMenu>)ViewBag.UserMenu, id);
                if (entity == null)
                {
                    return RedirectToAction("Index", WebHelper.CommonErrorMessage, "e");
                }
                var authOrg = AuthHelper.LoadOrganizationIdList((List<UserMenu>)ViewBag.UserMenu, _commonHelper.ConvertIdToList(entity.Organization.Id));
                if (authOrg.Count <= 0)
                    return RedirectToAction("Index", "OverTimeAllowanceSetting", new { message = "You don't have enough permission", type = "e" });
                OvertimeAllowanceSettingForm viewDataModel = UpdateViewModel(entity);
                return View(viewDataModel);
            }
            catch (NullObjectException ex)
            {
                ViewBag.ErrorMessage = errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return RedirectToAction("Index", "OverTimeAllowanceSetting", new { message = errorMessage, type = "e" });
        }

        [HttpPost]
        public ActionResult Edit(long id, OvertimeAllowanceSettingForm formViewData)
        {
            //var retriveOvertimeSetting = new OvertimeAllowanceSetting();
            var previousOvertimeSetting = new OvertimeAllowanceSetting();
            bool returnValue = false;
            try
            {
                #region User Interface Validation

                if (!ModelState.IsValid)
                {
                    InitBasic();
                    return View(formViewData);
                }

                var userMenus = (List<UserMenu>)ViewBag.UserMenu;
                var authOrganizationId = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(formViewData.OrganizationId));
                if (authOrganizationId.Count <= 0)
                {
                    throw new NullObjectException("Permission Denied For This Organization");
                }

                if (formViewData.EmploymentStatus == null)
                {
                    throw new NullObjectException("Employement status not selected.");
                }

                if (formViewData.OvertimeTypes == null)
                {
                    throw new NullObjectException("Overtime type not selected.");
                }

                previousOvertimeSetting = _overTimeAllowanceSettingService.GetOvertimeAllowanceSetting(id);
                if (previousOvertimeSetting == null)
                {
                    throw new InvalidDataException("Invalid data found.");
                }

                #endregion

                #region Member Status & Overtime Type

                //bool isProbation, isPermanent, isPartTime, isContractual, isIntern = false;
                //GetSelectedEmploymentStatus(formViewData.EmploymentStatus, out isProbation, out isPermanent, out isPartTime, out isContractual, out isIntern);

                //bool isGazetted, isManagementWeekend, isRegular = false;
                //GetSelectedOvertimeTypes(formViewData.OvertimeTypes, out isRegular, out isGazetted, out isManagementWeekend);

                bool isProbation, isPermanent, isPartTime, isContractual, isIntern = false;
                //GetSelectedEmploymentStatus(formViewData.EmploymentStatus, out isProbation, out isPermanent, out isPartTime, out isContractual, out isIntern);

                isProbation = formViewData.EmploymentStatus.Contains(MemberEmploymentStatus.Probation) ? true : false;
                isPermanent = formViewData.EmploymentStatus.Contains(MemberEmploymentStatus.Permanent) ? true : false;
                isPartTime = formViewData.EmploymentStatus.Contains(MemberEmploymentStatus.PartTime) ? true : false;
                isContractual = formViewData.EmploymentStatus.Contains(MemberEmploymentStatus.Contractual) ? true : false;
                isIntern = formViewData.EmploymentStatus.Contains(MemberEmploymentStatus.Intern) ? true : false;

                bool isGazetted, isManagementWeekend, isRegular = false;
                // GetSelectedOvertimeTypes(formViewData.OvertimeTypes, out isRegular, out isGazetted, out isManagementWeekend);

                isRegular = formViewData.OvertimeTypes.Contains(AllowanceSettingOverTimeType.Regular) ? true : false;
                isGazetted = formViewData.OvertimeTypes.Contains(AllowanceSettingOverTimeType.Gazetted) ? true : false;
                isManagementWeekend = formViewData.OvertimeTypes.Contains(AllowanceSettingOverTimeType.Management) ? true : false;

                #endregion

                #region Assign Value From View Object To Main Object

                var overtimeAllowanceSetting = new OvertimeAllowanceSetting()
                {
                    Id = id,
                    Organization = _organizationService.LoadById(formViewData.OrganizationId),
                    Name = formViewData.Name,
                    IsProbation = isProbation,
                    IsPermanent = isPermanent,
                    IsPartTime = isPartTime,
                    IsContractual = isContractual,
                    IsIntern = isIntern,
                    IsWorkingDay = isRegular,
                    IsGazzeted = isGazetted,
                    IsManagementWeekend = isManagementWeekend,
                    CalculationOn = formViewData.CalculationOn,
                    MultiplyingFactor = Convert.ToDecimal(formViewData.MultiplyingFactor),
                    EffectiveDate = formViewData.EffectiveDate,
                    ClosingDate = formViewData.ClosingDate != null ? formViewData.ClosingDate : null
                };

                #endregion

                returnValue = _overTimeAllowanceSettingService.SaveOrUpdate(userMenus, overtimeAllowanceSetting);
                if (returnValue)
                {
                    previousOvertimeSetting = _overTimeAllowanceSettingService.GetOvertimeAllowanceSetting(id);
                    ViewBag.SuccessMessage = "Overtime Allowance setting updated.";
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            finally
            {
                GetSelectedCheckBox(formViewData);
            }
            InitBasic();
            return View(UpdateViewModel(previousOvertimeSetting));
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                var userMenus = (List<UserMenu>)ViewBag.UserMenu;
                var entity = _overTimeAllowanceSettingService.GetOvertimeAllowanceSetting(userMenus, id);
                _overTimeAllowanceSettingService.Delete(entity);
                return Json(new { isSuccess = true, Text = "Overtime allowance setting deleted!" });
            }
            catch (MessageException ex)
            {
                return Json(new { isSuccess = false, Text = ex.Message });
            }
            catch (DependencyException ex)
            {
                return Json(new { isSuccess = false, Text = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { isSuccess = false, Text = WebHelper.CommonErrorMessage });
            }
        }

        #endregion

        #region Details

        public ActionResult Details(long id)
        {
            string message = "";
            try
            {
                var userMenus = (List<UserMenu>)ViewBag.UserMenu;
                var entity = _overTimeAllowanceSettingService.GetOvertimeAllowanceSetting(userMenus, id);
                if (entity != null)
                {
                    return View(DetailsViewModel(entity));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
            }
            return RedirectToAction("Index", "OverTimeAllowanceSetting", new { message = message, type = "e" });
        }

        #endregion

        #endregion

        #region Ajax Function

        public JsonResult DoesSettingNameExist(string name, long organizationId, long? id)
        {
            bool returnValue = false;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    returnValue = _overTimeAllowanceSettingService.IsOvertimeAllowanceSettingNameExist(name.Trim(), organizationId, id);

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(returnValue);
                }
            }
            return Json(returnValue);
        }

        #endregion

        #region Helper Function

        private void InitBasic()
        {
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization((List<UserMenu>)ViewBag.UserMenu), "Id", "ShortName");
            ViewBag.OvertimeTypeList = _commonHelper.LoadEmumToDictionary<AllowanceSettingOverTimeType>();
            ViewBag.EmploymentStatusList = _commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int>() { (int)MemberEmploymentStatus.Retired });
            var calculationOnList = Enum.GetValues(typeof(CalculationOnTypeEnum)).Cast<CalculationOnTypeEnum>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString(CultureInfo.InvariantCulture) }).ToList();
            ViewBag.CalculationOnList = new SelectList(calculationOnList, "Value", "Text");
            var multipleFactor = new Dictionary<string, decimal> { { "1.0", 1.0M }, { "1.5", 1.5M }, { "2.0", 2.0M }, { "2.5", 2.5M } };
            ViewBag.MultipleFactorList = new SelectList(multipleFactor.ToList().OrderBy(x => x.Key), "Key", "Value");
        }


        private void GetSelectedCheckBox(OvertimeAllowanceSettingForm formViewData)
        {
            ViewBag.EmployementStatusList = formViewData.EmploymentStatus;
            ViewBag.OverTimeList = formViewData.OvertimeTypes;
        }

        private OvertimeAllowanceSettingForm UpdateViewModel(OvertimeAllowanceSetting entity)
        {
            var calculationOn = Enum.Parse(typeof(CalculationOnTypeEnum), entity.CalculationOn.ToString(CultureInfo.InvariantCulture));
            ViewBag.CalculationOn = calculationOn;
            var multipleFactorx = String.Format("{0:0.0#}", entity.MultiplyingFactor);
            var multipleFactor = new Dictionary<string, decimal> { { "1.0", 1.0M }, { "1.5", 1.5M }, { "2.0", 2.0M }, { "2.5", 2.5M } };
            ViewBag.MultipleFactorList = new SelectList(multipleFactor.ToList().OrderBy(x => x.Key), "Key", "Value", multipleFactorx);
            var organization = AuthHelper.LoadOrganizationIdList((List<UserMenu>)ViewBag.UserMenu, _commonHelper.ConvertIdToList(entity.Organization.Id));

            var organizationName = "";
            if (organization.Count > 0)
            {
                organizationName = _organizationService.LoadById(organization[0]).ShortName;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization((List<UserMenu>)ViewBag.UserMenu), "Id", "ShortName", organization[0]);
                ViewBag.SelectedOrganization = organization[0];
            }
            else
            {
                throw new NullObjectException("Permission Denied For This Organization");
            }

            var closingDateStr = "";
            if (entity.ClosingDate.ToString() == "01-Jan-01 12:00:00 AM")
            {
                closingDateStr = "x";
            }
            else
            {
                closingDateStr = entity.ClosingDate.ToString();
            }

            ViewBag.ClosingDateForView = closingDateStr;

            var viewDataModel = new OvertimeAllowanceSettingForm
            {
                Id = entity.Id,
                ClosingDate = entity.ClosingDate,
                EffectiveDate = entity.EffectiveDate,
                Name = entity.Name,
                OrganizationId=organization[0],
                MultiplyingFactor = multipleFactorx
            };

            #region Employment Status & Overtime Type

            var employementlist = new List<MemberEmploymentStatus>();
            viewDataModel.EmploymentStatus = new List<MemberEmploymentStatus>();
            if (entity.IsProbation)
            {
                employementlist.Add(MemberEmploymentStatus.Probation);
            }
            if (entity.IsPermanent)
            {
                employementlist.Add(MemberEmploymentStatus.Permanent);
            }
            if (entity.IsPartTime)
            {
                employementlist.Add(MemberEmploymentStatus.PartTime);
            }
            if (entity.IsContractual)
            {
                employementlist.Add(MemberEmploymentStatus.Contractual);
            }
            if (entity.IsIntern)
            {
                employementlist.Add(MemberEmploymentStatus.Intern);
            }

            viewDataModel.EmploymentStatus = employementlist;

            viewDataModel.OvertimeTypes = new List<AllowanceSettingOverTimeType>();
            var overtimeTypeList = new List<AllowanceSettingOverTimeType>();

            if (entity.IsWorkingDay)
            {
                overtimeTypeList.Add(AllowanceSettingOverTimeType.Regular);
            }
            if (entity.IsManagementWeekend)
            {
                overtimeTypeList.Add(AllowanceSettingOverTimeType.Management);
            }
            if (entity.IsGazzeted)
            {
                overtimeTypeList.Add(AllowanceSettingOverTimeType.Gazetted);
            }
            viewDataModel.OvertimeTypes = overtimeTypeList;

            #endregion

            ViewBag.SelectedOverTimeList = overtimeTypeList;
            ViewBag.SelectedEmployementStatusList = employementlist;
            return viewDataModel;
        }

        private OvertimeAllowanceSettingForm DetailsViewModel(OvertimeAllowanceSetting entity)
        {
            ViewBag.EmploymentStatusList = new List<MemberEmploymentStatus>()
                                        {
                                            MemberEmploymentStatus.Probation,
                                             MemberEmploymentStatus.Permanent,
                                                 MemberEmploymentStatus.PartTime,
                                            MemberEmploymentStatus.Contractual,
                                            MemberEmploymentStatus.Intern,
                                            
                                        };
            ViewBag.HolidayTypeList = new List<AllowanceSettingOverTimeType>()
                                        {
                                               AllowanceSettingOverTimeType.Regular,
                                            AllowanceSettingOverTimeType.Gazetted,
                                            AllowanceSettingOverTimeType.Management
                                         
                                        };


            var calculationOn = Convert.ToString((Enum.Parse(typeof(CalculationOnTypeEnum), entity.CalculationOn.ToString(CultureInfo.InvariantCulture))));
            ViewBag.CalculationOnVal = calculationOn;
            var multipleFactorVal = String.Format("{0:0.0#}", entity.MultiplyingFactor);
            ViewBag.MultipleFactorVal = multipleFactorVal;

            ViewBag.OrganizationName = entity.Organization.ShortName;
            ViewBag.EffectiveFrom = entity.EffectiveDate.ToString("yyyy-MM-dd");
            ViewBag.ClosingDate = entity.ClosingDate != null ? entity.ClosingDate.Value.ToString("yyyy-MM-dd") : "N/A";
            ViewBag.CreateByStr = _userService.GetByAspNetUser(entity.CreateBy).AspNetUser.FullName;
            ViewBag.ModifiedByStr = _userService.GetByAspNetUser(entity.ModifyBy).AspNetUser.FullName;
            ViewBag.CreationDateStr = entity.CreationDate.ToString("yyyy-MM-dd");
            ViewBag.ModifiedDateStr = entity.ModificationDate.ToString("yyyy-MM-dd");

            var model = new OvertimeAllowanceSettingForm
            {
                Id = entity.Id,
                ClosingDate = entity.ClosingDate,
                EffectiveDate = entity.EffectiveDate,
                Name = entity.Name,
            };

            #region Employment Status & Holiday Types

            var employementlist = new List<MemberEmploymentStatus>();
            model.EmploymentStatus = new List<MemberEmploymentStatus>();
            if (entity.IsContractual)
            {
                employementlist.Add(MemberEmploymentStatus.Contractual);
            }
            if (entity.IsPartTime)
            {
                employementlist.Add(MemberEmploymentStatus.PartTime);
            }
            if (entity.IsPermanent)
            {
                employementlist.Add(MemberEmploymentStatus.Permanent);
            }
            if (entity.IsProbation)
            {
                employementlist.Add(MemberEmploymentStatus.Probation);
            }
            if (entity.IsIntern)
            {
                employementlist.Add(MemberEmploymentStatus.Intern);
            }
            model.EmploymentStatus = employementlist;

            model.OvertimeTypes = new List<AllowanceSettingOverTimeType>();
            var holidays = new List<AllowanceSettingOverTimeType>();
            if (entity.IsGazzeted)
            {
                holidays.Add(AllowanceSettingOverTimeType.Gazetted);
            }
            if (entity.IsManagementWeekend)
            {
                holidays.Add(AllowanceSettingOverTimeType.Management);
            }
            if (entity.IsWorkingDay)
            {
                holidays.Add(AllowanceSettingOverTimeType.Regular);
            }
            model.OvertimeTypes = holidays;

            #endregion

            return model;
        }


        private string GetOvertimeTypeString(bool isRegular, bool isManagementWeekend, bool isGazetted)
        {
            string concatedString;
            var overtimeTypesList = new List<AllowanceSettingOverTimeType>();

            if (isRegular)
            {
                overtimeTypesList.Add(AllowanceSettingOverTimeType.Regular);
            }
            if (isManagementWeekend)
            {
                overtimeTypesList.Add(AllowanceSettingOverTimeType.Management);
            }
            if (isGazetted)
            {
                overtimeTypesList.Add(AllowanceSettingOverTimeType.Gazetted);
            }
            return concatedString = string.Join(", ", overtimeTypesList);
        }

        private string GetEmploymentStatusString(bool isProbation, bool isPermanent, bool isPartTime, bool isContractual, bool isIntern)
        {
            string textStr;
            var employementlist = new List<MemberEmploymentStatus>();

            if (isProbation)
            {
                employementlist.Add(MemberEmploymentStatus.Probation);
            }
            if (isPermanent)
            {
                employementlist.Add(MemberEmploymentStatus.Permanent);
            }
            if (isPartTime)
            {
                employementlist.Add(MemberEmploymentStatus.PartTime);
            }
            if (isContractual)
            {
                employementlist.Add(MemberEmploymentStatus.Contractual);
            }
            if (isIntern)
            {
                employementlist.Add(MemberEmploymentStatus.Intern);
            }

            return textStr = string.Join(", ", employementlist);
        }

        #endregion
    }
}