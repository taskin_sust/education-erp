﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class TdsHistoryFactory : ObjectFactoryBase<TdsHistory>
    {
        private readonly ITdsHistoryService _tdsHistroryService;
        private readonly TestBaseService<TdsHistoryLog> _tdsHisotryLogService;
        private TeamMemberFactory _teamMemberFactory;
        public TdsHistoryFactory(IObjectFactoryBase caller, ISession session)
            : base(caller,session)
        {
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _tdsHistroryService = new TdsHistoryService(session);
            _tdsHisotryLogService = new TestBaseService<TdsHistoryLog>(session);

        }

        #region create

        public TdsHistoryFactory Create()
        {
            Object = new TdsHistory()
            {
                TdsAmount = 1000,
                EffectiveDate = DateTime.Now,
                CreateBy = 1,
                ModifyBy = 1,
                BusinessId = GetPcUserName()
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public TdsHistoryFactory CreateWith(decimal tdsAmount, DateTime effectiveDate)
        {
            Object = new TdsHistory()
            {
                TdsAmount = tdsAmount,
                EffectiveDate = effectiveDate,
                CreateBy = 1,
                ModifyBy = 1,
                BusinessId = GetPcUserName()
            };
            SingleObjectList.Add(Object);
            return this;

        }

        public TdsHistoryFactory CreateMore(int numberOfTds)
        {
            CreateTdsList(numberOfTds);
            return this;
        }

        private List<TdsHistory> CreateTdsList(int numberOfTds)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<TdsHistory>();
            for (int i = 0; i < numberOfTds; i++)
            {
                var tds = new TdsHistory
                {
                    TdsAmount = 1000 + i,
                    EffectiveDate = DateTime.Now.AddDays(1),
                    CreateBy = 1,
                    ModifyBy = 1,
                    BusinessId = GetPcUserName()
                };
                list.Add(tds);
            }
            ObjectList.AddRange(list);
            return list;
        }

        public TdsHistoryFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }

        public TdsHistoryFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            //teamMember.TdsHistory.Add(Object);
            return this;
        }

        #endregion

        #region Others
        
        #endregion

        #region CleanUp

        public void CleanUp()
        {
            DeleteAll();
        }

        public void LogCleanUp(TdsHistoryFactory tdsHistoryFactory)
        {
            if (tdsHistoryFactory.ObjectList.Any())
            {
                DeleteLog(tdsHistoryFactory.ObjectList);
            }
            if (tdsHistoryFactory.SingleObjectList.Any())
            {
                DeleteLog(tdsHistoryFactory.SingleObjectList);
            }
        }

        private void DeleteLog(IEnumerable<TdsHistory> list)
        {
            foreach (var tdsHisotry in list)
            {
                var tableName = GetEntityTableName(typeof(TdsHistoryLog));
                string swData = "DELETE FROM " + tableName + " WHERE [TeamMemberId] in (" + string.Join(", ", tdsHisotry.TeamMember.Id) + ")";
               _session.CreateSQLQuery(swData).ExecuteUpdate();
            }
        }
        


        

        #endregion
    }
}
