﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Security.Authentication;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Hr
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class DailySupportAllowanceBlockController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IMentorHistoryService _mentorHistoryService;
        private readonly IDailyAllowanceBlockService _hrDailyAllowanceBlockService;

        public DailySupportAllowanceBlockController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _teamMemberService = new TeamMemberService(session);
            _branchService = new BranchService(session);
            _campusService = new CampusService(session);
            _mentorHistoryService = new MentorHistoryService(session);
            _hrDailyAllowanceBlockService = new DailySupportAllowanceBlockService(session);
        }

        #endregion

        #region Save/Update

        #region By Mentor

        public ActionResult BlockAllowanceByMentor()
        {
            ViewBag.IsShowView = false;
            return View();
        }

        [HttpPost]
        public ActionResult BlockAllowanceByMentor(DailyAllowanceBlockFormViewModel vm)
        {
            try
            {
                ViewBag.IsShowView = false;
                if (ModelState.IsValid)
                {
                    var mentor = _teamMemberService.GetCurrentMember();
                    if (mentor == null)
                    {
                        throw new InvalidDataException("You don't have a \"Team Member\" profile!");
                    }
                    var pinList = (!String.IsNullOrEmpty(vm.PinList)) ? vm.PinList.Trim() : "";
                    var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");
                    
                    var memberPinList = pinList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList();
                    var mentorTeamMemberIdList = _mentorHistoryService.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now, null, memberPinList);
                    
                    if (mentorTeamMemberIdList.Count <= 0)
                    {
                        throw new InvalidDataException("You don't have enough permission!");
                    }
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    IList<DailySupportAllowanceBlockDto> list = _hrDailyAllowanceBlockService.LoadDailyAllowance(vm.Date, userMenu, string.Join(", ", mentorTeamMemberIdList), true, mentor);
                    if (list.Count > 0)
                    {
                        ViewBag.IsShowView = true;
                        ViewBag.DataList = list;
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
            }
            return View();
        }

        public ActionResult BlockAllowanceSaveByMentor(string date, List<string[]> dataArray)
        {
            try
            {
                TeamMember member = _teamMemberService.GetCurrentMember();
                if (member == null)
                {
                    throw new InvalidDataException("This team member has no user!");
                }
                IList<DailySupportAllowanceBlock> entityList = new List<DailySupportAllowanceBlock>();
                bool isChecked = true;
                if (dataArray.Count <= 0)
                {
                    throw new InvalidDataException("form not submitted properly!");
                }
                foreach (var dArray in dataArray)
                {
                    var entity = new DailySupportAllowanceBlock();
                    if (Convert.ToInt32(dArray[2]) == 0)
                    {
                        if (Convert.ToInt32(dArray[0]) <= 0)
                        {
                            isChecked = false;
                            continue;
                        }
                        entity.IsBlocked = false;
                    }
                    else if (Convert.ToInt32(dArray[2]) == 1)
                    {
                        entity.IsBlocked = true;
                    }
                    else
                    {
                        continue;
                    }
                    entity.Id = Convert.ToInt64(dArray[0]);
                    entity.TeamMember = _teamMemberService.GetMember(Convert.ToInt32(dArray[1]));
                    entity.Reason = dArray[3];
                    entity.BlockingDate = Convert.ToDateTime(date);
                    entityList.Add(entity);
                }
                if (!isChecked)
                {
                    throw new InvalidDataException("Invalid row submitted! Please select action for all new entry!");
                }
                _hrDailyAllowanceBlockService.SaveOrUpdate((List<UserMenu>)ViewBag.UserMenu, entityList, member, true);
                return Json(new Response(true, "Daily Support Allowance Successfully Updated!"));
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region By Hr
        public ActionResult BlockAllowance()
        {
            ViewBag.IsShowView = false;
            return View();
        }

        [HttpPost]
        public ActionResult BlockAllowance(DailyAllowanceBlockFormViewModel vm)
        {
            try
            {
                ViewBag.IsShowView = false;
                if (ModelState.IsValid)
                {
                    var pinList = (!String.IsNullOrEmpty(vm.PinList)) ? vm.PinList.Trim() : "";
                    var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    IList<DailySupportAllowanceBlockDto> list = _hrDailyAllowanceBlockService.LoadDailyAllowance(vm.Date, userMenu, pinList);
                    if (list.Count > 0)
                    {
                        ViewBag.IsShowView = true;
                        ViewBag.DataList = list;
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        public ActionResult BlockAllowanceSaveByHr(string date, List<string[]> dataArray)
        {
            try
            {
                TeamMember member = _teamMemberService.GetCurrentMember();
                if (member == null)
                {
                    throw new InvalidDataException("You can't oparate this. you have no team member account!");
                }
                IList<DailySupportAllowanceBlock> entityList = new List<DailySupportAllowanceBlock>();
                bool isChecked = true;
                foreach (var dArray in dataArray)
                {
                    var entity = new DailySupportAllowanceBlock();
                    if (Convert.ToInt32(dArray[2]) == 0)
                    {
                        if (Convert.ToInt32(dArray[0]) <= 0)
                        {
                            isChecked = false;
                            continue;
                        }
                        entity.IsBlocked = false;
                    }
                    else if (Convert.ToInt32(dArray[2]) == 1)
                    {
                        entity.IsBlocked = true;
                    }
                    else
                    {
                        continue;
                    }
                    entity.Id = Convert.ToInt64(dArray[0]);
                    entity.TeamMember = _teamMemberService.GetMember(Convert.ToInt32(dArray[1]));
                    entity.Reason = dArray[3];
                    entity.BlockingDate = Convert.ToDateTime(date);
                    entityList.Add(entity);
                }
                if (!isChecked)
                {
                    throw new InvalidDataException("Invalid row submitted! Please select action for all new entry!");
                }
                _hrDailyAllowanceBlockService.SaveOrUpdate((List<UserMenu>)ViewBag.UserMenu, entityList, member);
                ViewBag.IsShowView = false;
                return Json(new Response(true, "Daily Support Allowance Successfully Updated!"));
            }
            catch (AuthenticationException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #endregion


        #region Ajax functions

        #region History

        public ActionResult DailyAllowanceBlockHistory()
        {
            try
            {
                InitializeSearchView();
                return PartialView("_DailySupportAllowanceBlockHistory");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public ActionResult LoadBlockedAllowanceHistory(int draw, int start, int length, long? organizationId, long? departmentId, long? branchId, long? campusId
            , string pin, string dateFrom, string dateTo, int? isBlock, bool isMentor = false)
        {
            long recordsTotal = 0;
            
            if (Request.IsAjaxRequest())
            {
                try
                {
                    TeamMember currentTeamMember = null;
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    if (isMentor)
                    {
                        currentTeamMember = _teamMemberService.GetCurrentMember();
                        if (currentTeamMember == null || currentTeamMember.Id <= 0)
                        {
                            throw new InvalidDataException("You don't have a \"Team Member\" profile!");
                        }
                    }

                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    
                    long orgId = organizationId != null ? organizationId.Value : 0;
                    long deptId = departmentId != null ? departmentId.Value : 0;
                    long brId = branchId != null ? branchId.Value : 0;
                    long capId = campusId != null ? campusId.Value : 0;
                    int status = isBlock != null ? isBlock.Value : -1;
                    string pinStr = !string.IsNullOrEmpty(pin) ? pin : "";

                    DateTime dateFromDateTime = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).Date;
                    DateTime dateToDateTime = DateTime.Today.Date;
                    if (!String.IsNullOrEmpty(dateFrom))
                    {
                        dateFromDateTime = Convert.ToDateTime(dateFrom).Date;
                    }
                    if (!String.IsNullOrEmpty(dateTo))
                    {
                        dateToDateTime = Convert.ToDateTime(dateTo).Date;
                    }

                    if (dateFromDateTime > dateToDateTime)
                    {
                        DateTime temp = dateToDateTime;
                        dateToDateTime = dateFromDateTime;
                        dateFromDateTime = temp;
                    }
                    
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = "Pin";
                        orderDir = nvc["order[0][dir]"];
                    }
                    
                    var data = new List<object>();

                    IList<DailySupportAllowanceBlockListDto> list = _hrDailyAllowanceBlockService.LoadDailyAllowance(start, length, orderBy, orderDir, userMenu, orgId, brId, deptId, capId
                                                                                                                    , status, pinStr, dateFromDateTime, dateToDateTime, isMentor, currentTeamMember);
                    int rowCount = _hrDailyAllowanceBlockService.CountDailyAllowance(userMenu, orgId, brId, deptId, capId
                                                                                                                    , status, pinStr, dateFromDateTime, dateToDateTime, isMentor, currentTeamMember);
                    recordsTotal = rowCount;
                    long recordsFiltered = recordsTotal;
                    foreach (var row in list)
                    {
                        var str = new List<string>();
                        str.Add(row.Pin.ToString(CultureInfo.InvariantCulture));
                        str.Add(row.Name);
                        str.Add(row.Department);
                        str.Add(row.Organization);
                        str.Add(row.Branch);
                        str.Add(row.Campus);
                        str.Add(row.Designation);
                        str.Add(row.BlockingDate);
                        str.Add(row.TimeFrom);
                        str.Add(row.TimeTo);
                        str.Add(row.IsBlocked);
                        str.Add(row.Reason);
                        str.Add(row.ModifyBy);
                        str.Add(row.LastModificationDate);
                        data.Add(str);
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });

                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return HttpNotFound();
        }


        #endregion

        #endregion

        #region Helper functions

        private void InitializeSearchView()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.CampusId = new SelectList(string.Empty, "Value", "Text");
            ViewBag.DepartmentList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.dateFromField = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
            ViewBag.dateToField = DateTime.Now.ToString("yyyy-MM-dd");
            ViewBag.IsBlockStatus = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Block", Value = "1" }, new SelectListItem() { Text = "Unblock", Value = "0" } }, "Value", "Text").ToList();
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
        }

        #endregion

    }
}