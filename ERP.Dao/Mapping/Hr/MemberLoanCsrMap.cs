using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{


    public partial class MemberLoanCsrMap : BaseClassMap<MemberLoanCsr,long>
    {

        public MemberLoanCsrMap()
        {
			Table("HR_MemberLoanCsr");
			LazyLoad();
			References(x => x.MemberLoanApplication).Column("MemberLoanApplicationId");
			Map(x => x.PaymentType).Column("PaymentType").Not.Nullable();
			Map(x => x.Amount).Column("Amount").Not.Nullable();
            Map(x => x.EffectiveDate).Column("EffectiveDate").Not.Nullable();
			Map(x => x.ClosingDate).Column("ClosingDate").Not.Nullable();
			Map(x => x.TotalAmount).Column("TotalAmount").Not.Nullable();
			Map(x => x.ApprovedDate).Column("ApprovedDate");
        }
    }
}
