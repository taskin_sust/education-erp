﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using log4net;
using NHibernate.Engine;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]

    public class OrderedGoodsReceiveController : Controller
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private List<UserMenu> authorizeMenu;

        private readonly ICommonHelper _commonHelper;
        private readonly IWorkOrderService _workOrderService;
        private readonly IGoodsReceiveService _goodsReceiveService;
        private readonly IGoodsReceiveDetailService _goodsReceiveDetailService;
        private readonly IGoodsReturnService _goodsReturnService;
        private readonly IOrganizationService _organizationService;

        public OrderedGoodsReceiveController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _workOrderService = new WorkOrderService(session);
            _goodsReceiveService = new GoodsReceiveService(session);
            _goodsReceiveDetailService = new GoodsReceiveDetailsService(session);
            _goodsReturnService = new GoodsReturnService(session);
            _organizationService = new OrganizationService(session);
        }

        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region create

        public ActionResult GoodsReceive()
        {
            return View();
        }

        #endregion

        #region details

        public ActionResult Details(string goodsRecNo)
        {
            var orderedGoodsReceiveViewModel = new OrderedGoodsReceiveViewModel();
            try
            {
                if (String.IsNullOrEmpty(goodsRecNo))
                {
                    ViewBag.ErrorMessage = "Invalid Received No.";
                    return View(new OrderedGoodsReceiveViewModel());
                }
                GoodsReceive goodsReceive = _goodsReceiveService.GetGoodsReceive(goodsRecNo);
                if (goodsReceive == null) return PartialView("Partial/_OrderedGoodsReceive", orderedGoodsReceiveViewModel);
                orderedGoodsReceiveViewModel = GenerateGoodsReceiveModel(goodsReceive);
                if (goodsReceive.Workorder != null)
                    orderedGoodsReceiveViewModel.ReturnedQuantity = _goodsReturnService.GetTotalReturnAmountAgainstWorkorderNo(goodsReceive.Workorder.WorkOrderNo); //_goodsReturnService.GetTotalReturnAmountAgainstReceiveNo(goodsReceive.Id);//
                var prevReceAmount = goodsReceive.Workorder == null ? 0 : _goodsReceiveDetailService.GetPrevReceivedAmountFromSelectedReceivedNo(goodsRecNo);
                if (prevReceAmount > 0)
                    orderedGoodsReceiveViewModel.PreviouslyReceivedQuantity = prevReceAmount - orderedGoodsReceiveViewModel.ReturnedQuantity;
                orderedGoodsReceiveViewModel.CurrentlyReceivedQuantity = _goodsReceiveDetailService.
                    GetLastReceivedAmountByReceiveDetailsId(goodsReceive.GoodsReceiveDetails[goodsReceive.GoodsReceiveDetails.Count - 1].Id);
                //this viewmodel is used as workorder as well as receive viewmodel
                orderedGoodsReceiveViewModel.WorkOrderNo = goodsRecNo;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View(orderedGoodsReceiveViewModel);
        }

        #endregion

        #region manage

        public ActionResult ManageOrderedReceive()
        {
            try
            {
                Initialize();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        public ActionResult ManageOrderedReceive(int draw, int start, int length, string organizationId, string branchId, string itemName, string receivedDate,
            string workOrderNo, string workOrderDate, string goodStatus)
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (String.IsNullOrEmpty(organizationId)) { organizationId = "0"; }
                if (String.IsNullOrEmpty(branchId)) { branchId = "0"; }
                List<OrderedGoodsRecViewModel> goodsRecViewModels = _goodsReceiveService.LoadGoodsReceived(draw, start, length, authorizeMenu, Convert.ToInt64(organizationId),
                    Convert.ToInt64(branchId), itemName, receivedDate, workOrderNo, workOrderDate, goodStatus);
                int count = _goodsReceiveService.CountGoodsReceive(authorizeMenu, Convert.ToInt64(organizationId), Convert.ToInt64(branchId), itemName, receivedDate, workOrderNo,
                    workOrderDate, goodStatus);
                var data = new List<object>();
                foreach (var goodrec in goodsRecViewModels)
                {
                    var str = new List<string>();
                    str.Add(goodrec.OrganizationName);
                    str.Add(goodrec.BranchName);
                    str.Add(goodrec.ItemName);
                    if (DateTime.MinValue != goodrec.WorkOrderDate)
                        str.Add(goodrec.WorkOrderDate.ToString("MMM dd,yyyy"));
                    else str.Add(" ");
                    str.Add(goodrec.WorkOrderNo);
                    str.Add(goodrec.GoodRecDate != null ? goodrec.GoodRecDate.ToString("MMM dd,yyyy") : " ");
                    str.Add(goodrec.GoodRecNo);
                    str.Add("<a href='" + Url.Action("Details", "OrderedGoodsReceive") + "?goodsRecNo=" + goodrec.GoodRecNo + "' class='glyphicon glyphicon-th-list'></a>&nbsp; ");

                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = count,
                    recordsFiltered = count,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception ex)
            {
                var data = new List<object>();
                _logger.Error(ex);
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });
            }
        }

        #endregion

        #region helper

        private void Initialize()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            ViewBag.branchList = new SelectList(String.Empty, "Value", "Text");
            ViewBag.itemList = new SelectList(String.Empty, "Value", "Text");
            var enumList = _commonHelper.LoadEmumToDictionary<WorkOrderStatus>(new List<int>() { (int)WorkOrderStatus.Cancelled, (int)WorkOrderStatus.Issued }).ToList();
            ViewBag.StatusText = new SelectList(enumList, "key", "value");
            ViewBag.PageSize = Constants.PageSize;
        }

        private OrderedGoodsReceiveViewModel GenerateGoodsReceiveModel(WorkOrder workOrder)
        {
            var goodsReceiveViewModel = new OrderedGoodsReceiveViewModel()
            {
                Branch = workOrder.Branch.Name,
                ItemGroup = workOrder.WorkOrderDetails[0].Item.ItemGroup.Name,
                Organization = workOrder.Branch.Organization.Name,
                OrganizationShortName = workOrder.Branch.Organization.ShortName,
                ItemName = workOrder.WorkOrderDetails[0].Item.Name,
                SupplierName = workOrder.Supplier.Name,
                WorkOrderNo = workOrder.WorkOrderNo,
                WorkOrderDate = (DateTime)workOrder.WorkOrderDate,
                Remarks = workOrder.Remarks,
                OrderedQuantity = (int)workOrder.OrderedQuantity,
            };
            List<KeyValuePair<int, string>> enumList = _commonHelper.LoadEmumToDictionary<Purpose>().ToList();
            foreach (var keyValuePair in enumList.Where(keyValuePair => workOrder.WorkOrderDetails != null).Where(keyValuePair => keyValuePair.Key.ToString() == workOrder.WorkOrderDetails[0].Purpose.ToString()))
            {
                goodsReceiveViewModel.Purpose = keyValuePair.Value;
                break;
            }
            var program = workOrder.WorkOrderDetails[0].Program;
            if (program != null)
            {
                var session = workOrder.WorkOrderDetails[0].Session;
                if (session != null)
                    goodsReceiveViewModel.ProgramSession = program.Name + " " + session.Name;
            }
            goodsReceiveViewModel.PreviouslyReceivedQuantity = _goodsReceiveDetailService.GetPrevSumReceiveAmountByWorkOrderDetailId(workOrder.WorkOrderDetails[0].Id);
            goodsReceiveViewModel.ReturnedQuantity = _goodsReturnService.GetTotalReturnAmountAgainstWorkorderNo(workOrder.WorkOrderNo);
            return goodsReceiveViewModel;
        }

        private OrderedGoodsReceiveViewModel GenerateGoodsReceiveModel(GoodsReceive goodsReceive)
        {
            var goodsReceiveViewModel = new OrderedGoodsReceiveViewModel()
            {
                Branch = goodsReceive.Branch.Name,
                ItemGroup = goodsReceive.GoodsReceiveDetails[0].Item.ItemGroup.Name,
                Organization = goodsReceive.Branch.Organization.Name,
                OrganizationShortName = goodsReceive.Branch.Organization.ShortName,
                ItemName = goodsReceive.GoodsReceiveDetails[0].Item.Name,
                WorkOrderNo = goodsReceive.GoodsReceivedNo,
                WorkOrderDate = (DateTime)goodsReceive.GoodsReceivedDate,
                Remarks = goodsReceive.Remarks,
            };
            if (goodsReceive.Supplier != null) goodsReceiveViewModel.SupplierName = goodsReceive.Supplier.Name;
            if (goodsReceive.Workorder == null)
                goodsReceiveViewModel.OrderedQuantity = goodsReceive.GoodsReceiveDetails[0].ReceivedQuantity;
            else if (goodsReceive.Workorder.OrderedQuantity != null)
                goodsReceiveViewModel.OrderedQuantity = (int)goodsReceive.Workorder.OrderedQuantity;
            List<KeyValuePair<int, string>> enumList = _commonHelper.LoadEmumToDictionary<Purpose>().ToList();
            foreach (var keyValuePair in enumList.Where(keyValuePair => goodsReceive.GoodsReceiveDetails != null).
                Where(keyValuePair => keyValuePair.Key.ToString() == goodsReceive.GoodsReceiveDetails[0].Purpose.ToString()))
            {
                goodsReceiveViewModel.Purpose = keyValuePair.Value;
                break;
            }
            if (goodsReceive.GoodsReceiveDetails != null)
            {
                var program = goodsReceive.GoodsReceiveDetails[0].Program;
                if (program != null)
                {
                    var session = goodsReceive.GoodsReceiveDetails[0].Session;
                    if (session != null)
                        goodsReceiveViewModel.ProgramSession = program.Name + " " + session.Name;
                }
            }
            return goodsReceiveViewModel;
        }
        #endregion

        #region delete
        #endregion

        #region update
        #endregion

        #region ajax request

        [HttpPost]
        public ActionResult GoodsReceiveInformation(string workOrderNo)
        {
            try
            {
                if (!Request.IsAjaxRequest()) return Json(new Response(false, "invalid ajax request."));
                if (String.IsNullOrEmpty(workOrderNo)) return Json(new Response(false, "invalid work order no."));

                WorkOrder workOrder = _workOrderService.GetByWorkOrderNo(workOrderNo);
                if (workOrder == null) return Json(new Response(false, "invalid work order no."));
                if (workOrder.WorkOrderDetails == null) return Json(new Response(false, "invalid work order"));

                OrderedGoodsReceiveViewModel orderedGoodsReceiveViewModel = GenerateGoodsReceiveModel(workOrder);
                return PartialView("Partial/_OrderedGoods", orderedGoodsReceiveViewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult GoodsReceive(long currentlyRecQuantity, string workOrderNo, string remark = "")
        {
            try
            {
                if (!Request.IsAjaxRequest()) return Json(new Response(false, "invalid ajax request."));
                if (currentlyRecQuantity <= 0) return Json(new Response(false, "invalid received quantity"));
                if (workOrderNo.Length <= 0) return Json(new Response(false, "invalid work order no"));
                WorkOrder workOrder = _workOrderService.GetByWorkOrderNo(workOrderNo);
                if (workOrder == null) return Json(new Response(false, "invalid work order no"));
                //generate for view 
                workOrder.Remarks = remark;
                OrderedGoodsReceiveViewModel orderedGoodsReceiveViewModel = GenerateGoodsReceiveModel(workOrder);
                orderedGoodsReceiveViewModel.WorkOrderDate = DateTime.Now;
                orderedGoodsReceiveViewModel.CurrentlyReceivedQuantity = currentlyRecQuantity;

                //if introduced multiple item then it will be changed because prev quantity varies from item+p+s to item 
                long prevrecQuantity =
                    workOrder.WorkOrderDetails.Sum(
                        wod => _goodsReceiveDetailService.GetPrevSumReceiveAmountByWorkOrderDetailId(wod.Id));
                if (currentlyRecQuantity >
                    (workOrder.OrderedQuantity - prevrecQuantity) + orderedGoodsReceiveViewModel.ReturnedQuantity)
                {
                    return Json(new Response(false, "invalid amount received."));
                }
                GoodsReceive goodsReceive = CalculateDirectGoodsRec(workOrder, currentlyRecQuantity, prevrecQuantity, remark);
                _goodsReceiveService.SaveOrUpdate(goodsReceive);
                orderedGoodsReceiveViewModel.WorkOrderNo = goodsReceive.GoodsReceivedNo;
                return PartialView("Partial/_OrderedGoodsReceive", orderedGoodsReceiveViewModel);
            }
            catch (NullObjectException e)
            {
                _logger.Error(e);
                return Json(new Response(false, e.Message));
            }
            catch (InvalidDataException e)
            {
                _logger.Error(e);
                return Json(new Response(false, e.Message));
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region helper

        public GoodsReceive CalculateDirectGoodsRec(WorkOrder workOrder, long currentlyRecQuantity, long prevrecQuantity, string remark = "")
        {

            if (workOrder == null) throw new NullObjectException("invalid work order no.");
            if (workOrder.WorkOrderDetails == null) throw new NullObjectException("invalid work order");

            var goodrec = new GoodsReceive
            {
                Branch = workOrder.Branch,
                GoodsReceiveStatus = (int)GoodsReceivedStatus.Received,
                Workorder = workOrder,
                Supplier = workOrder.Supplier,
                GoodsReceivedDate = DateTime.Now,
                Remarks = remark
            };

            goodrec.GoodsReceiveStatus = workOrder.OrderedQuantity == currentlyRecQuantity + prevrecQuantity
                ? (int)GoodsReceivedStatus.Received
                : (int)GoodsReceivedStatus.PartiallyReceived;
            var goodrecDetails = new GoodsReceiveDetails();
            var program = workOrder.WorkOrderDetails[0].Program;
            if (program != null)
            {
                var session = workOrder.WorkOrderDetails[0].Session;
                if (session != null)
                    goodrecDetails.Session = workOrder.WorkOrderDetails[0].Session;
                goodrecDetails.Program = workOrder.WorkOrderDetails[0].Program;
            }

            goodrecDetails.Item = workOrder.WorkOrderDetails[0].Item;
            goodrecDetails.ReceivedQuantity = (int)currentlyRecQuantity;
            goodrecDetails.Rate = (decimal)workOrder.WorkOrderDetails[0].UnitCost;
            goodrecDetails.TotalCost = (decimal)Math.Floor(currentlyRecQuantity * workOrder.WorkOrderDetails[0].UnitCost);
            goodrecDetails.Purpose = workOrder.WorkOrderDetails[0].Purpose;
            goodrecDetails.WorkOrderDetail = workOrder.WorkOrderDetails[0];
            goodrec.GoodsReceiveDetails.Add(goodrecDetails);

            //as only one item 
            goodrec.TotalCost = goodrecDetails.TotalCost;
            return goodrec;
        }


        #endregion
    }
}