﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.Areas.Student.Models;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using HtmlAgilityPack;
using System.Configuration;
using UdvashERP.BusinessRules.Student;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class BoardInfoController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentArea");
        private void LogError(string message, Exception exception)
        {
            if (exception != null)
            {
                _logger.Error(message, exception);
            }
            else
            {
                _logger.Error(message);
            }
        }
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly IProgramService _programService;
        private readonly IStudentExamService _studentExamService;
        private readonly ISessionService _sessionService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IBatchService _batchService;
        private readonly IBoardInformationService _boardInformationService;
        private readonly IStudentBoardService _studentBoardService;
        private readonly IStudentAcademicInfoService _studentAcademicInfoService;
        private readonly IStudentService _studentService;
        private readonly IStudentAcademicInfoExamSubjectService _studentAcademicInfoExamSubjectService;
        private readonly IStudentAcademicInfoSubjectResultService _studentAcademicInfoSubjectResultService;
        private readonly IStudentAcademicInfoBaseDataService _studentAcademicInfoBaseDataService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IOrganizationService _organizationService;
        private readonly IInformationService _informationService;
        private readonly ICourseService _courseService;
        private readonly CommonHelper _commonHelper;
        //authorization
        private List<UserMenu> _userMenu;
        public BoardInfoController()
        {
            var nHSession = NHibernateSessionFactory.OpenSession();
            _programService = new ProgramService(nHSession);
            _studentExamService = new StudentExamService(nHSession);
            _sessionService = new SessionService(nHSession);
            _branchService = new BranchService(nHSession);
            _campusService = new CampusService(nHSession);
            _batchService = new BatchService(nHSession);
            _boardInformationService = new BoardInformationService(nHSession);
            _studentBoardService = new StudentBoardService(nHSession);
            _studentAcademicInfoService = new StudentAcademicInfoService(nHSession);
            _studentService = new StudentService(nHSession);
            _studentAcademicInfoExamSubjectService = new StudentAcademicInfoExamSubjectService(nHSession);
            _studentAcademicInfoSubjectResultService = new StudentAcademicInfoSubjectResultService(nHSession);
            _studentAcademicInfoBaseDataService = new StudentAcademicInfoBaseDataService(nHSession);
            _studentProgramService = new StudentProgramService(nHSession);
            _organizationService = new OrganizationService(nHSession);
            _informationService = new InformationService(nHSession);
            _courseService = new CourseService(nHSession);
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Missing Board Roll

        [HttpGet]
        public ActionResult MissingBoardRoll()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            //ViewBag.ProgramId = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedExam = new SelectList(_studentExamService.LoadBoardExam(true), "Id", "Name");
            var boardInfo = new BoardInformation();
            return View(boardInfo);
        }

        [HttpPost]
        public ActionResult MissingBoardRoll(BoardInformation boardInformation)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.ProgramId = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedExam = new SelectList(_studentExamService.LoadBoardExam(true), "Id", "Name");
            var boardInfo = new BoardInformation();
            try
            {
                if (boardInformation != null)
                {
                    ViewBag.PageSize = Constants.PageSize;
                    #region Authorization Check
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //IList<StudentProgram> studentProgramList = _boardInformationService.LoadStudentProgram(_userMenu, boardInformation.ProgramId, boardInformation.SessionId
                    //    , boardInformation.SelectedBranch, boardInformation.SelectedCampus, boardInformation.SelectedBatchDays, boardInformation.SelectedBatchTime
                    //    , boardInformation.SelectedBatch, boardInformation.StudentExamId, boardInformation.InformationViewList);
                    #endregion

                    #region Header Info
                    IList<string> reportHeadInfo = new List<string>();
                    var resultList = new List<string>();

                    #region Program
                    Program program = _programService.GetProgram(boardInformation.ProgramId);
                    if (program != null && program.Id > 0)
                        reportHeadInfo.Add(program.Name);
                    else
                        reportHeadInfo.Add("");
                    #endregion

                    #region Session
                    Session session = _sessionService.LoadById(boardInformation.SessionId);
                    if (session != null && session.Id > 0)
                        reportHeadInfo.Add(session.Name);
                    else
                        reportHeadInfo.Add("");

                    #endregion

                    #region Branch

                    if (boardInformation.SelectedBranch.Contains(SelectionType.SelelectAll))
                    {
                        reportHeadInfo.Add("All Branch");
                    }
                    else
                    {
                        var selectedBranch = _branchService.LoadBranch(boardInformation.SelectedBranch);
                        resultList = (from xx in selectedBranch select xx.Name).ToList();
                        reportHeadInfo.Add(string.Join(", ", resultList));
                    }

                    #endregion

                    #region Campus

                    if (boardInformation.SelectedCampus.Contains(SelectionType.SelelectAll))
                    {
                        reportHeadInfo.Add("All Campus");
                    }
                    else
                    {
                        var selectedCampus = _campusService.LoadCampus(boardInformation.SelectedCampus);
                        resultList = (from xx in selectedCampus select xx.Name).ToList();
                        reportHeadInfo.Add(string.Join(", ", resultList));
                    }

                    #endregion

                    #region Batch Days

                    if (boardInformation.SelectedBatchDays.Contains(SelectionType.SelelectAll.ToString()))
                    {
                        reportHeadInfo.Add("All Batch Days");
                    }
                    else
                    {
                        reportHeadInfo.Add(string.Join("; ", boardInformation.SelectedBatchDays));
                    }

                    #endregion

                    #region Batch

                    if (boardInformation.SelectedBatch.Contains(SelectionType.SelelectAll))
                    {
                        reportHeadInfo.Add("All Batch");
                    }
                    else
                    {
                        var selectedBatch = _batchService.LoadBatch(boardInformation.SelectedBatch);
                        resultList = (from xx in selectedBatch select xx.Name).ToList();
                        reportHeadInfo.Add(string.Join(", ", resultList));
                    }

                    #endregion

                    #region Course

                    //if (Array.Exists(boardInformation.SelectedCourse, item => item == 0))
                    //    resultList = (from x in
                    //                      (from xx in studentProgramList select xx.Batch.Program.Courses).FirstOrDefault()
                    //                  where
                    //                      x.Program.Id == studentListReportObj.ProgramId && x.RefSession.Id == studentListReportObj.SessionId
                    //                  select x.Name).ToList();
                    //else
                    //{
                    //    IList<Course> courseList = _courseService.LoadByCourselIdList(studentListReportObj.SelectedCourse);
                    //    resultList = (from x in courseList select x.Name).ToList();
                    //}
                    //string reportHeadInfoCourseName = string.Join(", ", resultList);
                    //reportHeadInfo.Add(reportHeadInfoCourseName);

                    #endregion



                    #region Exam

                    if (boardInformation.StudentExamId == SelectionType.SelelectAll)
                    {
                        reportHeadInfo.Add("All Exam");
                    }
                    else
                    {
                        StudentExam exam = _studentExamService.LoadById(boardInformation.StudentExamId);
                       reportHeadInfo.Add(exam.Name);
                    }

                    #endregion

                    #endregion

                    ViewBag.ReportHeadInfo = reportHeadInfo;

                    if (boardInformation.InformationViewList != null)
                    {
                        if (!boardInformation.InformationViewList.Contains("Program Roll"))
                        {
                            var items = new[] { "Program Roll" };
                            var combinedList = new List<string>();
                            combinedList.AddRange(items);
                            combinedList.AddRange(boardInformation.InformationViewList);
                            boardInformation.InformationViewList = combinedList.ToArray();
                        }
                    }
                    else
                    {
                        var items = new[] { "Program Roll" };
                        var combinedList = new List<string>();
                        combinedList.AddRange(items);
                        boardInformation.InformationViewList = combinedList.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View(boardInfo);
            }
            return View("GetMissingBoardRollReport", boardInformation);
        }

        #region Missing Student Count
        [HttpPost]
        public JsonResult GetMissingBoardRollStudent(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId)
        {
            try
            {
                #region Authorization Check
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                long missingBoardRollCount = _boardInformationService.GetMissingBoardRollStudentCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId);
                #endregion
                return Json(new { missingBoardRollCount = missingBoardRollCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _logger.Error("MissingBoardRoll. AjaxRequestForMissingBoardRollCount." + ex.InnerException);
                else
                    _logger.Error("MissingBoardRoll. AjaxRequestForMissingBoardRollCount." + ex.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Render Data Table

        [HttpPost]
        public JsonResult GetMissingBoardRollStudentAjaxRequest(int draw, int start, int length, long programId, long sessionId, long[] branchId
            , long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string[] informationViewList, string prnNo = null, string name = null
            , string mobile = null)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    #region OrderBy and Direction
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                    }
                    #endregion

                    #region Authorization Check
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    long missingBoardRollCount = _boardInformationService.GetMissingBoardRollStudentCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);
                    long recordsTotal = missingBoardRollCount;
                    long recordsFiltered = recordsTotal;
                    IList<MissingBoardRollListDto> studentPrograms = _boardInformationService.GetMissingBoardRollStudents(_userMenu, start, length, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);
                    #endregion

                    // studentPrograms = studentPrograms.Skip(start).Take(length).ToList();
                    var data = new List<object>();
                    int sl = start + 1;
                    var boradList = _studentBoardService.LoadActive();
                    foreach (var c in studentPrograms)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Program Roll":
                                        str.Add(c.ProgramRoll);
                                        break;

                                    case "Nick Name":
                                        str.Add(c.NickName);
                                        break;
                                    case "Mobile Number (Student)":
                                        str.Add(c.MobileNumber);
                                        break;
                                    case "Mobile Number (Father)":
                                        str.Add(c.MobileNumberFather);
                                        break;
                                    case "Mobile Number (Mother)":
                                        str.Add(c.MobileNumberMother);
                                        break;
                                    case "Institute Name":
                                        str.Add("");
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        str.Add("<input type='text' name='year[]' id='year" + c.StudentId + "' class='year form-control' maxlength='4' onKeyPress='return isNumber(event)' placeholder='Year' value='" + c.Year + "'  />");
                        var selectedBoard = "<select name='board[]' id='board" + c.StudentId + "' class='board form-control'><option value=''>Select Board</option>";
                        foreach (var b in boradList)
                        {
                            if (c.BoardId > 0 && c.BoardId == b.Id)
                                selectedBoard += "<option value='" + b.Id + "' selected='selected'>" + b.Name + "</option>";
                            else
                                selectedBoard += "<option value='" + b.Id + "'>" + b.Name + "</option>";
                        }
                        selectedBoard += "</select>";
                        str.Add(selectedBoard);
                        str.Add("<input type='text' name='boardRoll[]' id='boardRoll" + c.StudentId + "' class='boardRoll form-control' placeholder='Board Roll'  value='" + c.BoradRoll + "' onKeyPress='return isNumber(event)' />");
                        str.Add("<input type='text' name='registrationNumber[]' id='registrationNumber" + c.StudentId + "' class='registrationNumber form-control' placeholder='Reg. No' value='" + c.RegistrationNumber + "'  onKeyPress='return isNumber(event)' />");
                        sl++;
                        data.Add(str);
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return Json(HttpNotFound());
            }
        }
        #endregion
        #region Export
        public ActionResult ExportMissingBoardRollStudentAjaxRequest(string programSessionHeader, string branchNameHeader, string campusNameHeader, string batchDaysHeader, string batchNameHeader, long programId, long sessionId, long[] branchId , long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, string[] informationViewList, string prnNo = null, string name = null
           , string mobile = null)
        {

            try
            {
                var headerList = new List<string>();
                headerList.Add(programSessionHeader);
                headerList.Add("Missing Board Roll Information");
                headerList.Add("Branch : " + branchNameHeader);
                headerList.Add("Campus : " + campusNameHeader);
                headerList.Add("Batch Days : " + batchDaysHeader);
                headerList.Add("Batch : " + batchNameHeader);
                headerList.Add("Exam : " + _studentExamService.LoadById(studentExamId).Name);
                headerList.Add("");
                var columnList = new List<string>();
                foreach (var information in informationViewList)
                {
                    columnList.Add(information);
                }
                columnList.AddRange(new List<string> { "Year", "Board", "Board Roll", "Reg. No." });
                var footerList = new List<string>();
                footerList.Add("");
                var excelList = new List<List<object>>();
                #region Authorization Check
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<MissingBoardRollListDto> studentPrograms = _boardInformationService.GetMissingBoardRollStudents(_userMenu, 0, 0, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);
                #endregion
                var boradList = _studentBoardService.LoadActive();
                foreach (var c in studentPrograms)
                {
                    var str = new List<object>();
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case "Program Roll":
                                    str.Add(c.ProgramRoll);
                                    break;
                                case "Nick Name":
                                    str.Add(c.NickName);
                                    break;
                                case "Mobile Number (Student)":
                                    str.Add(c.MobileNumber);
                                    break;
                                case "Mobile Number (Father)":
                                    str.Add(c.MobileNumberFather);
                                    break;
                                case "Mobile Number (Mother)":
                                    str.Add(c.MobileNumberFather);
                                    break;
                                case "Institute Name":
                                    str.Add("");
                                    break;
                                default:
                                    break;
                            }
                        }
                        str.AddRange(new List<Object> { c.Year, c.BoardName, c.BoradRoll, c.RegistrationNumber });

                    }

                    excelList.Add(str);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, excelList, footerList, "missing-board-roll-report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }
        #endregion

        #region Update Row Data
        [HttpPost]
        public JsonResult UpdateMissingBoardRoll(List<StudentAcademicInfo> data)
        {
            try
            {
                List<StudentAcademicInfo> validAcademicInfoList = new List<StudentAcademicInfo>();
                foreach (var d in data)
                {
                    if (d.Student != null && d.Student.Id > 0 && !String.IsNullOrEmpty(d.Year) && !String.IsNullOrEmpty(d.BoradRoll) && !String.IsNullOrEmpty(d.RegistrationNumber) && d.StudentExam != null && d.StudentExam.Id > 0 && d.StudentBoard != null && d.StudentBoard.Id > 0)
                    {
                        //bool isSuccess = _studentAcademicInfoService.Update(d);
                        validAcademicInfoList.Add(d);
                    }
                }
                _studentAcademicInfoService.SaveOrUpdateStudentAcademicInfoFromList(validAcademicInfoList);
                var s = "sucessfully saved";
                return Json(new { s = s, IsSuccess = true });
            }
            catch (Exception ex)
            {
                //if (ex.InnerException != null)
                //    LogError("UpdateMissingBoardRoll", ex.InnerException);
                //else
                //    LogError("UpdateMissingBoardRoll", ex);
                _logger.Error(ex.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #endregion

        #region Update Board Result
        [HttpGet]
        public ActionResult UpdateBoardResult()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            //ViewBag.ProgramId = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedExam = new SelectList(_studentExamService.LoadBoardExam(true), "Id", "Name");
            IList<SelectListItem> statusType = new List<SelectListItem>
            {
                new SelectListItem {Text="All",Value ="",Selected = true},
                new SelectListItem {Text="Update Missing Info",Value ="0" },
                new SelectListItem {Text="Update Existing Info",Value ="1" },
            };
            ViewBag.SelectStatus = statusType;
            var boardInfo = new BoardInformation();
            return View(boardInfo);
        }

        [HttpPost]
        public ActionResult UpdateBoardResult(BoardInformation info)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.Usermenu;
                var authorizedprogram = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
                ViewBag.ProgramId = authorizedprogram;
                ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedExam = new SelectList(_studentExamService.LoadBoardExam(true), "Id", "Name");
                IList<SelectListItem> statusType = new List<SelectListItem>
                {
                    new SelectListItem {Text="All",Value ="",Selected = true},
                    new SelectListItem {Text="Update Missing Info",Value ="0" },
                    new SelectListItem {Text="Update Existing Info",Value ="1" },
                };
                ViewBag.SelectStatus = statusType;

                List<string> notSyncPrn = new List<string>();
                List<StudentProgram> studentProgramsList = _boardInformationService.LoadStudentProgramForUpdateStudentInfo(_userMenu, info.ProgramId, info.SessionId,
                      info.SelectedBranch, info.SelectedCampus, info.SelectedBatchDays, info.SelectedBatchTime, info.SelectedBatch, info.StudentExamId, info.SelectStatus);
                bool checkUpdate = false;
                int countFalse = 0;
                int countTrue = 0;
                if (studentProgramsList != null && studentProgramsList.Count > 0)
                {
                    foreach (var studentProgram in studentProgramsList)
                    {
                        StudentAcademicInfo studentAcademicInfo =
                            _studentAcademicInfoService.LoadByStudentId(studentProgram.Student.Id, info.StudentExamId);
                        if (studentAcademicInfo != null)
                        {
                            StudentAcademicInfoBaseData studentAcademicInfoBase =
                                _studentAcademicInfoBaseDataService.LoadByAcademicInfoId(studentAcademicInfo.Id);
                            if (studentAcademicInfoBase != null)
                            {
                                checkUpdate = _studentService.UpdateStudentinfo(studentProgram.PrnNo, studentAcademicInfo, studentAcademicInfoBase);
                                if (checkUpdate == false)
                                    countFalse++;
                                else countTrue++;
                            }
                            else
                            {
                                notSyncPrn.Add(studentProgram.PrnNo);
                                //ViewBag.ErrorMessage = "Please Sync your student then update";
                                //return View(new BoardInformation());
                            }
                        }
                    }
                    if (notSyncPrn.Count > 0)
                    {
                        var errorPrn = ExtractResultNotFoundPrn(notSyncPrn);
                        errorPrn = errorPrn.Remove(errorPrn.Length - 2);
                        ViewBag.ErrorMessage = "Please synchronize student info (" + errorPrn + ")";
                    }
                    if (countFalse > 0)
                        ViewBag.InfoMessage = "Student update missing info ( " + countFalse + " )";
                    if (countTrue > 0)
                        ViewBag.SuccessMessage = "Successfully Student info update ( " + countTrue + " )";
                }
                else
                {
                    ViewBag.ErrorMessage = "Error updating of student info";
                }

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _logger.Error("BoardInfo. UpdateBoardResult." + ex.InnerException);
                else
                    _logger.Error("BoardInfo. UpdateBoardResult." + ex.Message);

                ViewBag.ErrorMessage = "Error updating of student info";
            }

            var boardInfo = new BoardInformation();
            return View(boardInfo);
        }

        #region Get Board Result Status

        [HttpPost]
        public JsonResult GetBoardResultStatus(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, int? status)
        {
            try
            {
                #region Authorization Check
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                long boardResultStatus = _boardInformationService.GetBoardResultStatus(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId, status);
                #endregion
                return Json(new { boardResultStatus = boardResultStatus, IsSuccess = true });
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _logger.Error("MissingBoardRoll. AjaxRequestForMissingBoardRollCount." + ex.InnerException);
                else
                    _logger.Error("MissingBoardRoll. AjaxRequestForMissingBoardRollCount." + ex.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #endregion

        #region View Board Result
        [HttpGet]
        public ActionResult ViewBoardResult()
        {
            ViewbagForViewBoardResult();
            var boardInfo = new BoardInformation();
            return View(boardInfo);
        }

        [HttpPost]
        public ActionResult ViewBoardResult(BoardInformation boardInformation)
        {
            try
            {
                if (boardInformation != null)
                {
                    ViewBag.PageSize = Constants.PageSize;

                    #region Authorization Check
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //IList<StudentProgram> studentProgramList = _boardInformationService.LoadStudentProgram(_userMenu, boardInformation.ProgramId, boardInformation.SessionId, boardInformation.SelectedBranch
                    //    , boardInformation.SelectedCampus, boardInformation.SelectedBatchDays, boardInformation.SelectedBatchTime, boardInformation.SelectedBatch, boardInformation.StudentExamId, boardInformation.InformationViewList);

                    //IList<StudentProgram> studentProgramList =
                    //    _boardInformationService.LoadStudentProgramForBoardResult(_userMenu, boardInformation.ProgramId,
                    //        boardInformation.SessionId, boardInformation.SelectedBranch, boardInformation.SelectedCampus,
                    //        boardInformation.SelectedBatchDays, boardInformation.SelectedBatchTime,
                    //        boardInformation.SelectedBatch, boardInformation.StudentExamId);
                    #endregion

                    ViewBag.StudentExam = _studentExamService.LoadById(boardInformation.StudentExamId);
                    ViewBag.StudentExamSubject = _studentAcademicInfoExamSubjectService.LoadExamSubject(boardInformation.StudentExamId);

                    #region Header Info
                    IList<string> reportHeadInfo = new List<string>();
                    var resultList = new List<string>();

                    #region Program
                    Program program = _programService.GetProgram(boardInformation.ProgramId);
                    if (program != null && program.Id > 0)
                        reportHeadInfo.Add(program.Name);
                    else
                        reportHeadInfo.Add("");
                    #endregion

                    #region Session
                    Session session = _sessionService.LoadById(boardInformation.SessionId);
                    if (session != null && session.Id > 0)
                        reportHeadInfo.Add(session.Name);
                    else
                        reportHeadInfo.Add("");

                    #endregion

                    #region Branch

                    if (boardInformation.SelectedBranch.Contains(SelectionType.SelelectAll))
                    {
                        reportHeadInfo.Add("All Branch");
                    }
                    else
                    {
                        var selectedBranch = _branchService.LoadBranch(boardInformation.SelectedBranch);
                        resultList = (from xx in selectedBranch select xx.Name).ToList();
                        reportHeadInfo.Add(string.Join(", ", resultList));
                    }

                    #endregion

                    #region Campus

                    if (boardInformation.SelectedCampus.Contains(SelectionType.SelelectAll))
                    {
                        reportHeadInfo.Add("All Campus");
                    }
                    else
                    {
                        var selectedCampus = _campusService.LoadCampus(boardInformation.SelectedCampus);
                        resultList = (from xx in selectedCampus select xx.Name).ToList();
                        reportHeadInfo.Add(string.Join(", ", resultList));
                    }

                    #endregion

                    #region Batch Days

                    if (boardInformation.SelectedBatchDays.Contains(SelectionType.SelelectAll.ToString()))
                    {
                        reportHeadInfo.Add("All Batch Days");
                    }
                    else
                    {
                        reportHeadInfo.Add(string.Join("; ", boardInformation.SelectedBatchDays));
                    }

                    #endregion

                    #region Batch

                    if (boardInformation.SelectedBatch.Contains(SelectionType.SelelectAll))
                    {
                        reportHeadInfo.Add("All Batch");
                    }
                    else
                    {
                        var selectedBatch = _batchService.LoadBatch(boardInformation.SelectedBatch);
                        resultList = (from xx in selectedBatch select xx.Name).ToList();
                        reportHeadInfo.Add(string.Join(", ", resultList));
                    }

                    #endregion

                    #endregion
                    ViewBag.ReportHeadInfo = reportHeadInfo;
                }

            }
            catch (Exception ex)
            {
                ViewbagForViewBoardResult();
                _logger.Error("ViewBoardResult", ex);
                return View(new BoardInformation());
            }
            //if (boardInformation != null && boardInformation.SelectReportType == 1)
            //    return View("GetViewBoardResultSummaryReport", boardInformation);
            return View("GetViewBoardResultReport", boardInformation);
        }

        #region  Board Result Student Count

        private void ViewbagForViewBoardResult()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedExam = new SelectList(_studentExamService.LoadBoardExam(true), "Id", "Name");
            IList<SelectListItem> gradeType = new List<SelectListItem>
            {
                new SelectListItem {Text="Letter Grade",Value ="1",Selected = true},
                new SelectListItem {Text="Grade Point",Value ="2" },
            };
            ViewBag.SelectGrade = gradeType;
            IList<SelectListItem> reportType = new List<SelectListItem>
            {
                new SelectListItem {Text="Summary Result",Value ="1",Selected = true},
                new SelectListItem {Text="Subject Wise Result",Value ="2" },
            };
            ViewBag.SelectReportType = reportType;
        }

        [HttpPost]
        public JsonResult GetBoardResultStudentCount(long programId, long sessionId, long[] branchId, long[] campusId,
                   string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId)
        {
            try
            {
                #region Authorization Check
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                long boardResultStudentCount = _boardInformationService.GetBoardResultStudentCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId);
                #endregion
                return Json(new { boardResultStudentCount = boardResultStudentCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _logger.Error("BoardResultStudentCount. AjaxRequestForBoardResultStudentCount." + ex.InnerException);
                else
                    _logger.Error("BoardResultStudentCount. AjaxRequestForBoardResultStudentCount." + ex.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Render Data Table For Summary Result

        [HttpPost]
        public JsonResult GetStudentBoardResultSummaryAjaxRequest(int draw, int start, int length, long programId,
            long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batchIds,
            long studentExamId, string[] informationViewList, string prnNo = null, string name = null,
            string mobile = null)
        {
            return Json("");
        }
        #endregion

        #region Render Data Table For Subject Wise Result

        [HttpPost]
        public JsonResult GetStudentBoardResultAjaxRequest(int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId
            , string[] batchDays, string[] batchTimes, long[] batchIds, long studentExamId, int selectReportType, string[] informationViewList, string prnNo = null, string name = null,
            string mobile = null)
        {

            if (Request.IsAjaxRequest())
            {
                try
                {
                    #region OrderBy and Direction
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];

                    }
                    #endregion

                    IList<StudentAcademicInfoExamSubject> examSubjects = _studentAcademicInfoExamSubjectService.LoadExamSubject(studentExamId);
                    int totalSubject = examSubjects.Count;
                    #region Authorization Check

                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //long boardResultStudentCount = _boardInformationService.GetBoardResultStudentCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId);
                    long boardResultStudentCount = _boardInformationService.GetBoardResultStudentCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId, prnNo, name, mobile);
                    long recordsTotal = boardResultStudentCount;
                    long recordsFiltered = recordsTotal;


                    IList<StudentProgram> studentPrograms = _boardInformationService.GetBoardResultStudent(_userMenu, start, length, programId, sessionId, branchId, campusId, batchDays, batchTimes, batchIds, studentExamId
                        , prnNo, name, mobile);

                    #endregion

                    // studentPrograms = studentPrograms.Skip(start).Take(length).ToList();
                    var data = new List<object>();
                    int sl = start + 1;
                    var boradList = _studentBoardService.LoadActive();
                    foreach (var c in studentPrograms)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Program Roll":
                                        str.Add(c.PrnNo);
                                        break;

                                    case "Nick Name":
                                        if (c.Student != null && c.Student.NickName != null)
                                        {
                                            str.Add(c.Student.NickName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    default:
                                        break;
                                }
                            }
                        }
                        //old code
                        if (c.Student != null && c.Student.StudentAcademicInfos != null)
                        {
                            foreach (StudentAcademicInfo academicInfo in c.Student.StudentAcademicInfos)
                            {
                                if (academicInfo.StudentExam.Id == studentExamId)
                                {
                                    if (academicInfo.StudentAcademicInfoBaseData != null)
                                    {

                                        foreach (var bd in academicInfo.StudentAcademicInfoBaseData)
                                        {
                                            int count = 0;
                                            if (selectReportType == 2)
                                            {
                                                str.Add(bd.ExamGroup);
                                                if (bd.StudentAcademicInfoSubjectResult != null)
                                                {
                                                    totalSubject = bd.StudentAcademicInfoSubjectResult.Count();
                                                    //foreach (StudentAcademicInfoSubjectResult subjectResult in bd.StudentAcademicInfoSubjectResult)
                                                    //{
                                                    //    foreach (var checkSubject in examSubjects)
                                                    //    {
                                                    //        if (checkSubject.Id == subjectResult.StudentAcademicInfoExamSubject.Id)
                                                    //            str.Add(subjectResult.Grade);
                                                    //        else
                                                    //            str.Add("");
                                                    //    }
                                                    //}

                                                    foreach (var checkSubject in examSubjects)
                                                    {
                                                        if (bd.StudentAcademicInfoSubjectResult.Where(a => a.StudentAcademicInfoExamSubject.Id == checkSubject.Id).Count() > 0)
                                                        {
                                                            string grade =
                                                                bd.StudentAcademicInfoSubjectResult.Where(
                                                                    a =>
                                                                        a.StudentAcademicInfoExamSubject.Id ==
                                                                        checkSubject.Id).FirstOrDefault().Grade;
                                                            if (grade == "A+") count++;
                                                            str.Add(grade);
                                                        }

                                                        else
                                                            str.Add("");
                                                    }
                                                }
                                            }
                                            str.Add(bd.Gpa);
                                            if (selectReportType == 2)
                                            {
                                                str.Add(count == totalSubject ? "Yes" : "No");
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        //old code

                        //optimized code
                        //c.Student.StudentAcademicInfos.SelectMany(x => x.StudentAcademicInfoBaseData).Where(x => x.StudentAcademicInfo.StudentExam.Id == studentExamId);
                        sl++;
                        data.Add(str);
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return Json(HttpNotFound());
            }
        }
        #endregion

        #endregion

        #region Info Synchronizer
        [HttpGet]
        public ActionResult InfoSynchronizer()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            //ViewBag.ProgramId = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedExam = new SelectList(_studentExamService.LoadBoardExam(true), "Id", "Name");

            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");

            IList<SelectListItem> syncStatus = new List<SelectListItem>
            {
                new SelectListItem{Text = "ALL",Value = "",Selected = true},
                new SelectListItem{Text = "Synchronized",Value = "1"},
                new SelectListItem{Text = "Not Synchronized",Value = "0"}
            };
            ViewBag.SelectStatus = syncStatus;
            ViewBag.InformationViewSelectList = new SelectList(StudentListConstant.GetInfoSynchronizerInformationStringList().Select(x => new SelectListItem() { Text = x, Value = x }), "Text", "Value").ToList();

            var boardInfo = new BoardInformation();
            return View(boardInfo);
        }

        [HttpPost]
        public ActionResult InfoSynchronizer(BoardInformation boardInformation)
        {
            try
            {
                if (boardInformation != null)
                {
                    ViewBag.PageSize = Constants.PageSize;
                    if (boardInformation.InformationViewList != null)
                    {
                        if (!boardInformation.InformationViewList.Contains(StudentListConstant.ProgramRoll))
                        {
                            var items = new[] { StudentListConstant.ProgramRoll };
                            var combinedList = new List<string>();
                            combinedList.AddRange(items);
                            combinedList.AddRange(boardInformation.InformationViewList);
                            boardInformation.InformationViewList = combinedList.ToArray();
                        }
                    }
                    else
                    {
                        var items = new[] { StudentListConstant.ProgramRoll };
                        var combinedList = new List<string>();
                        combinedList.AddRange(items);
                        boardInformation.InformationViewList = combinedList.ToArray();
                    }
                }

                #region Report Head Info

                ViewBag.OrganizationHeaderName = "";
                ViewBag.ProgramHeaderName = "";
                ViewBag.SessonHeaderName = "";
                ViewBag.BranchHeaderName = "";
                ViewBag.CampusHeaderName = "";
                ViewBag.BatchDaysHeaderName = "";
                ViewBag.BatchTimesHeaderName = "";
                ViewBag.BatchHeaderName = "";
                ViewBag.ExamHeaderName = "";
                ViewBag.YearHeaderName = "";
                ViewBag.StatusHeaderName = "";

                if (boardInformation != null)
                {
                    List<string> resultList = new List<string>();
                    if (boardInformation.OrganizationId != SelectionType.SelelectAll)
                    {
                        Organization organization = _organizationService.LoadById(boardInformation.OrganizationId);
                        ViewBag.OrganizationHeaderName = (organization != null) ? organization.Name : "-";
                    }
                    if (boardInformation.ProgramId != SelectionType.SelelectAll)
                    {
                        Program program = _programService.GetProgram(boardInformation.ProgramId);
                        ViewBag.ProgramHeaderName = (program != null) ? program.Name : "-";
                    }
                    if (boardInformation.SessionId != SelectionType.SelelectAll)
                    {
                        Session session = _sessionService.LoadById(boardInformation.SessionId);
                        ViewBag.SessonHeaderName = (session != null) ? session.Name : "-";
                    }
                    if (!boardInformation.SelectedBatch.Contains(SelectionType.SelelectAll))
                    {
                        List<Branch> selectedBranch = _branchService.LoadBranch(boardInformation.SelectedBranch).ToList();
                        resultList = (from xx in selectedBranch select xx.Name).ToList();
                        ViewBag.BranchHeaderName = string.Join(", ", resultList);
                    }
                    else
                    {
                        ViewBag.BranchHeaderName = "All Branch";
                    }
                    if (!boardInformation.SelectedCampus.Contains(SelectionType.SelelectAll))
                    {
                        List<Campus> selectedCampuseList = _campusService.LoadCampus(boardInformation.SelectedCampus).ToList();
                        resultList = (from xx in selectedCampuseList select xx.Name).ToList();
                        ViewBag.CampusHeaderName = string.Join(", ", resultList);
                    }
                    else
                    {
                        ViewBag.CampusHeaderName = "All Campus";
                    }
                    ViewBag.BatchDaysHeaderName = !boardInformation.SelectedBatchDays.Contains(SelectionType.SelelectAll.ToString()) ? string.Join("; ", boardInformation.SelectedBatchDays) : "All Batch Days";
                    ViewBag.BatchTimesHeaderName = !boardInformation.SelectedBatchTime.Contains(SelectionType.SelelectAll.ToString()) ? string.Join("; ", boardInformation.SelectedBatchTime) : "All Batch Times";
                    if (!boardInformation.SelectedBatch.Contains(SelectionType.SelelectAll))
                    {
                        List<Batch> selectedBatcheList = _batchService.LoadBatch(boardInformation.SelectedBatch).ToList();
                        resultList = (from xx in selectedBatcheList select xx.Name).ToList();
                        ViewBag.BatchHeaderName = string.Join(", ", resultList);
                    }
                    else
                    {
                        ViewBag.BatchHeaderName = "All Batch";
                    }
                    if (boardInformation.StudentExamId != SelectionType.SelelectAll)
                    {
                        StudentExam exam = _studentExamService.LoadById(boardInformation.StudentExamId);
                        ViewBag.ExamHeaderName = (exam != null) ? exam.Name : "-";
                    }
                    ViewBag.YearHeaderName = boardInformation.ExamYear;
                    if (boardInformation.SyncStatus == null)
                    {
                        ViewBag.StatusHeaderName = "All";
                    }else if (boardInformation.SyncStatus == 1)
                    {
                        ViewBag.StatusHeaderName = "Synchronized";
                    }
                    else if (boardInformation.SyncStatus == 0)
                    {
                        ViewBag.StatusHeaderName = "Not Synchronized";
                    }                   
                }

                #endregion

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
            return View("GetInfoSynchronizer", boardInformation);
        }

        #region Count Student For Synchronizer

        [HttpPost]
        public JsonResult CountStudentForSynchronizer(long programId, long sessionId, long studentExamId, string year, long[] branch, long[] campus, string[] batchDays, string[] batchTimes, long[] batch, int? status)
        {
            try
            {
                #region Authorization Check
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                long synchronizerStudentCount = _boardInformationService.StudentForInfoSynchronizerCount(_userMenu, programId, sessionId, studentExamId, year, branch, campus, batchDays, batchTimes, batch, status);
                #endregion

                return Json(new { synchronizerStudentCount = synchronizerStudentCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Render Data Table  Get Student For InfoSynchronizer AjaxRequest

        [HttpPost]
        public JsonResult GeStudentForInfoSynchronizerAjaxRequest(int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTimes, long[] batch, int? syncStatus, long studentExamId, string year, string[] informationViewList, string prnNo, string name, string mobile)
        {
            long recordsTotal = 0;
            long recordsFiltered = 0;
            var data = new List<object>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            
                long studentCountThroughPrograms = _boardInformationService.StudentForInfoSynchronizerCount(_userMenu, programId, sessionId, studentExamId, year, branchId, campusId, batchDays, batchTimes, batch, syncStatus);
                List<ViewBoardInfoSynchronizerDto> studentList = _boardInformationService.LoadStudentForInfoSynchronizer(_userMenu, start, length, programId, sessionId, studentExamId, year, branchId, campusId, batchDays, batchTimes, batch, syncStatus, prnNo, name, mobile).ToList();
                recordsTotal = studentCountThroughPrograms;
                recordsFiltered = recordsTotal;

                int sl = start + 1;
                foreach (var student in studentList)
                {
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    if (informationViewList != null)
                    {

                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case StudentListConstant.ProgramRoll:
                                    str.Add(student.ProgramRoll);
                                    break;
                                case StudentListConstant.StudentsFullName:
                                    str.Add(student.FullName);
                                    break;
                                case StudentListConstant.MobileNumberStudent:
                                    str.Add(student.MobileNumber);
                                    break;
                                case StudentListConstant.FathersName:
                                    str.Add(student.FatherName);
                                    break;
                                case StudentListConstant.InstituteName:
                                    str.Add(student.Institute);
                                    break;
                                case StudentListConstant.NickName:
                                    str.Add(student.NickName);
                                    break;
                                case StudentListConstant.MobileNumberFather:
                                    str.Add(student.FatherMobileNumber);
                                    break;
                                case StudentListConstant.MobileNumberMother:
                                    str.Add(student.MotherMobileNumber);
                                    break;
                                case StudentListConstant.AcademicInfoYear:
                                    str.Add(student.Year);
                                    break;
                                case StudentListConstant.AcademicInfoBoard:
                                    str.Add(student.BoardName);
                                    break;
                                case StudentListConstant.AcademicInfoBoardRoll:
                                    str.Add(student.BoardRoll);
                                    break;
                                case StudentListConstant.AcademicInfoRegistrationNumber:
                                    str.Add(student.RegistrationNumber);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    sl++;
                    data.Add(str);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }
        
        #endregion

        #endregion

        #region Sync Data

        [HttpPost]
        public JsonResult GetSyncData(string[] studentPrn, long studentExamId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    List<string> resultNotFountPrn = new List<string>();
                    List<string> successPrn = new List<string>();
                    foreach (var sPrn in studentPrn)
                    {
                        StudentProgram studentProgram = _studentProgramService.GetStudentProgram(sPrn);
                        //Model.Entity.Students.Student studentObj = _studentService.LoadById(Convert.ToInt64(sId));
                        if (studentProgram != null)
                        {
                            BusinessModel.Entity.Students.Student studentObj = studentProgram.Student;
                            foreach (StudentAcademicInfo saInfo in studentObj.StudentAcademicInfos)
                            {
                                if (saInfo.StudentExam.Id == studentExamId)
                                {
                                    if (saInfo.BoradRoll != null && saInfo.RegistrationNumber != null && saInfo.StudentBoard.BusinessId != null && saInfo.StudentExam.BusinessId != null && saInfo.Year != null)
                                    {
                                        var r = EducationResult(saInfo.BoradRoll, saInfo.RegistrationNumber, saInfo.StudentBoard.BusinessId, saInfo.StudentExam.BusinessId, saInfo.Year);
                                        if (r != null)
                                        {
                                            HtmlDocument htmlDoc = new HtmlDocument();
                                            htmlDoc.LoadHtml(r.Content);

                                            if (htmlDoc.DocumentNode != null)
                                            {
                                                /*--------------------------------Check For Result Not Found-------------------------------------------*/
                                                if (htmlDoc.DocumentNode.SelectNodes("//table") != null)
                                                {
                                                    foreach (HtmlNode nodeDoc in htmlDoc.DocumentNode.SelectNodes("//table"))
                                                    {
                                                        //var v = nodeDoc.Descendants().Where(x => x.Name == "td").ToList();
                                                        foreach (var nd in nodeDoc.Descendants().Where(x => x.Name == "td"))
                                                        {
                                                            if (nd.InnerText.Contains("RESULT NOT FOUND!"))
                                                            {
                                                                var sp = studentProgram.PrnNo;//studentObj.StudentPrograms[0].PrnNo;
                                                                if (!resultNotFountPrn.Contains(sp))
                                                                    resultNotFountPrn.Add(sp);
                                                            }
                                                        }
                                                    }
                                                }
                                                /*--------------------------------End Check For Result Not Found-------------------------------------------*/

                                                #region Get Html Content From Board
                                                List<string> headDataList = new List<string>();
                                                List<string> resultDataList = new List<string>();
                                                int count = 0;
                                                if (htmlDoc.DocumentNode.SelectNodes("//table[@class='black12']") != null)
                                                {
                                                    foreach (HtmlNode nodeDoc in htmlDoc.DocumentNode.SelectNodes("//table[@class='black12']"))
                                                    {
                                                        count++;
                                                        if (count == 1)
                                                        {
                                                            int loop = 0;
                                                            foreach (var nd in nodeDoc.Descendants().Where(x => x.Name == "td"))
                                                            {
                                                                loop++;
                                                                if (loop % 2 == 0)
                                                                    headDataList.Add(nd.InnerText);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            foreach (var nd in nodeDoc.Descendants().Where(x => x.Name == "td"))
                                                            {
                                                                resultDataList.Add(nd.InnerText);
                                                            }
                                                        }
                                                    }

                                                    #region Assign HeadData To AcademicInfo Base Data

                                                    StudentAcademicInfoBaseData academicInfoBaseData = new StudentAcademicInfoBaseData();
                                                    academicInfoBaseData.StudentAcademicInfo = saInfo;
                                                    if (headDataList.Count > 0)
                                                    {
                                                        academicInfoBaseData.RollNo = headDataList[0];
                                                        academicInfoBaseData.Name = headDataList[1];
                                                        academicInfoBaseData.Board = headDataList[2];
                                                        academicInfoBaseData.FathersName = headDataList[3];
                                                        academicInfoBaseData.ExamGroup = headDataList[4];
                                                        academicInfoBaseData.MothersName = headDataList[5];
                                                        //academicInfoBaseData.Session = headDataList[6];
                                                        //academicInfoBaseData.RegistrationNo = headDataList[7];
                                                        academicInfoBaseData.StudentType = headDataList[6];
                                                        //13-04-1997
                                                        var stringDate = headDataList[7];
                                                        DateTime exactDate = new DateTime();

                                                        if (stringDate != "N/A")
                                                        {
                                                            exactDate = DateTime.ParseExact(stringDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                                        }
                                                        else
                                                        {
                                                            exactDate = Constants.MinDateTime;
                                                        }
                                                        academicInfoBaseData.DateOfBirth = exactDate;
                                                        academicInfoBaseData.Result = headDataList[8];
                                                        academicInfoBaseData.Institute = headDataList[9];
                                                        academicInfoBaseData.Gpa = headDataList[10];
                                                    }

                                                    //persist student academic info base data
                                                    _studentAcademicInfoBaseDataService.SaveUpdate(academicInfoBaseData, saInfo.Id);
                                                    //load info base data
                                                    academicInfoBaseData = _studentAcademicInfoBaseDataService.LoadByAcademicInfoId(saInfo.Id);
                                                    #endregion

                                                    #region Result Data
                                                    int lengthToSplit = 3;
                                                    int arrayLength = resultDataList.Count;
                                                    //here adding board result of a student
                                                    IList<CommonHelper.BoardSubjectResult> boardResults = new List<CommonHelper.BoardSubjectResult>();
                                                    for (int i = 0; i < arrayLength; i = i + lengthToSplit)
                                                    {
                                                        CommonHelper.BoardSubjectResult obj = new CommonHelper.BoardSubjectResult();
                                                        obj.Code = resultDataList[i];
                                                        obj.Subject = resultDataList[i + 1];
                                                        obj.Grade = resultDataList[i + 2];
                                                        boardResults.Add(obj);
                                                    }

                                                    #region Get Student Exam Subject & Save New Subject Using
                                                    //List<StudentAcademicInfoSubjectResult> saisr=new List<StudentAcademicInfoSubjectResult>();
                                                    StudentAcademicInfoExamSubject ies = new StudentAcademicInfoExamSubject();
                                                    foreach (var bResult in boardResults)
                                                    {
                                                        bool isResultSave = false;
                                                        if (bResult.Code != "Code" && bResult.Subject != "Subject" && bResult.Grade != "Grade")
                                                        {
                                                            ies = _studentAcademicInfoExamSubjectService.LoadExamSubject(bResult.Subject, bResult.Code, studentExamId);
                                                            if (ies == null)
                                                            {
                                                                bool isExamSubjectSave = _studentAcademicInfoExamSubjectService.Save(bResult, studentExamId);
                                                                if (isExamSubjectSave)
                                                                    //load exam subject
                                                                    ies = _studentAcademicInfoExamSubjectService.LoadExamSubject(bResult.Subject, bResult.Code, studentExamId);
                                                            }
                                                            isResultSave = _studentAcademicInfoSubjectResultService.Save(bResult, ies, academicInfoBaseData);
                                                        }
                                                        //var sp = studentObj.StudentPrograms[0].PrnNo;
                                                        var sp = studentProgram.PrnNo;
                                                        if (isResultSave && !successPrn.Contains(sp))
                                                        {
                                                            successPrn.Add(sp);
                                                        }
                                                    }
                                                    #endregion

                                                    #endregion
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var retrunResult = "";
                    var errorPrn = "";
                    if (resultNotFountPrn.Count > 0)
                    {
                        errorPrn = ExtractResultNotFoundPrn(resultNotFountPrn);
                        errorPrn = errorPrn.Remove(errorPrn.Length - 2);
                    }

                    retrunResult += errorPrn + "[]" + successPrn.Count;

                    return Json(new { resultNotFountPrn = retrunResult, IsSuccess = true });

                    //Json(new { branchList = branchList, IsSuccess = true });
                }
                catch (Exception ex)
                {
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(HttpNotFound());
            }
        }

        private string ExtractResultNotFoundPrn(List<string> resultNotFountPrn)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string value in resultNotFountPrn)
            {
                builder.Append(value);
                builder.Append(", ");
            }
            return builder.ToString();
        }

        #endregion

        #region Helper Function

        private ContentResult EducationResult(string roll, string reg, string board, string exam, string year)
        {
            //string url = "http://archive.educationboard.gov.bd/result.php";
            //string refUrl = "http://archive.educationboard.gov.bd/index.php";
            //string url = "http://www.educationboardresults.gov.bd/regular/result.php";
            //string refUrl = "http://www.educationboardresults.gov.bd/regular/index.php";
            string url = UdvashERP.Properties.Settings.Default.UdvashERP_board_info_sync_url;
            string refUrl = UdvashERP.Properties.Settings.Default.UdvashERP_board_info_sync_ref_url;
            string html = CommonHelper.EducationResult(url, refUrl, roll, reg, board, exam, year);
            return Content(html);
        }

        //private ContentResult EducationResult(string roll, string reg, string board, string exam, string year)
        //{

        //    //string url = "http://archive.educationboard.gov.bd/result.php";
        //    //string refUrl = "http://archive.educationboard.gov.bd/index.php";
        //    //string url = "http://www.educationboardresults.gov.bd/regular/result.php";
        //    //string refUrl = "http://www.educationboardresults.gov.bd/regular/index.php";
        //    string url = UdvashERP.Properties.Settings.Default.UdvashERP_board_info_sync_url;
        //    string refUrl = UdvashERP.Properties.Settings.Default.UdvashERP_board_info_sync_ref_url;
        //    var summationOfCaptcha = 0;
        //    var cookieCon = new CookieContainer();
        //    var z = GetHtml(refUrl, "", "", cookieCon);
        //    HtmlDocument htmlDoc = new HtmlDocument();
        //    htmlDoc.LoadHtml(z);
        //    if (htmlDoc.DocumentNode != null)
        //    {
        //        /*--------------------------------Quick Fixing Summation of Captcha-------------------------------------------*/
        //        if (htmlDoc.DocumentNode.SelectNodes("//table") != null)
        //        {
        //            foreach (HtmlNode nodeDoc in htmlDoc.DocumentNode.SelectNodes("//table"))
        //            {
        //                //var v = nodeDoc.Descendants().Where(x => x.Name == "td").ToList();
        //                foreach (var nd in nodeDoc.Descendants().Where(x => x.Attributes.Contains("class") && x.Attributes["class"].Value.Contains("black12bold")))
        //                {
        //                    int counter = 0;
        //                    foreach (var tr in nd.Descendants().Where(x => x.Name == "tr"))
        //                    {
        //                        counter++;
        //                        int counterTd = 0;
        //                        if (counter == 7)
        //                            foreach (var td in tr.Descendants().Where(x => x.Name == "td"))
        //                            {
        //                                counterTd++;
        //                                if (counterTd == 2)
        //                                {
        //                                    var numericCaptcha = td.InnerText;
        //                                    var summationOfCaptchaSplit = numericCaptcha.Split('+');
        //                                    foreach (var val in summationOfCaptchaSplit)
        //                                    {
        //                                        summationOfCaptcha = summationOfCaptcha + Convert.ToInt32(val);
        //                                    }

        //                                    break;
        //                                }
        //                            }
        //                    }
        //                }
        //                break;
        //            }

        //        }
        //    }



        //    string sendParam = "sr=3&et=0";
        //    sendParam += "&exam=" + exam;
        //    sendParam += "&year=" + year;
        //    sendParam += "&board=" + board;
        //    sendParam += "&roll=" + roll;
        //    sendParam += "&reg=" + reg;
        //    sendParam += "&value_s=" + summationOfCaptcha;
        //    sendParam += "&button2=Submit";
        //    var html = GetHtml(url, refUrl, sendParam, cookieCon);
        //    //return new ContentResult() { Content = html };
        //    // return ContentResult(){Content=html)}
        //    return Content(html);
        //}

        //private string GetHtml(string url, string refUrl, string param, CookieContainer cookieCon)
        //{
        //    string responseHtml = "";
        //    try
        //    {
        //        byte[] data = new ASCIIEncoding().GetBytes(param);
        //        var webRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
        //        webRequest.CookieContainer = cookieCon;
        //        webRequest.Method = "POST";
        //        webRequest.Referer = refUrl;
        //        webRequest.ContentType = "application/x-www-form-urlencoded";
        //        webRequest.ContentLength = data.Length;

        //        Stream myStream = webRequest.GetRequestStream();
        //        myStream.Write(data, 0, data.Length);

        //        var webResponse = webRequest.GetResponse();
        //        var responseStream = webResponse.GetResponseStream();
        //        StreamReader sr = new StreamReader(responseStream);
        //        responseHtml = sr.ReadToEnd();
        //    }
        //    catch (Exception ex)
        //    {
        //        responseHtml = ex.ToString();
        //    }
        //    return responseHtml;
        //}

        #endregion

        #region  Student Compare With Board
        [HttpGet]
        public ActionResult GetStudentCompareWithBoard()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.SelectedOrganization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            //ViewBag.ProgramId = new SelectList(_programService.LoadAuthorizedProgram(userMenu), "Id", "Name");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
            ViewBag.SelectedExam = new SelectList(_studentExamService.LoadBoardExam(true), "Id", "Name");

            //StudentInfoSearch studentInfoObj = new StudentInfoSearch();
            return View();
        }

        [HttpPost]
        public ActionResult GetStudentCompareWithBoard(StudentCompareWithBoard studentCompareWithBoard)
        {
            try
            {
                if (studentCompareWithBoard != null)
                {
                    if (studentCompareWithBoard.InformationViewList == null)
                    {
                        var infoView = new List<string>(new[] { "Program Roll", "Student's Name", "Mobile Number (Student)" });
                        studentCompareWithBoard.InformationViewList = infoView.ToArray();
                    }
                    ViewBag.PageSize = Constants.PageSize;
                    ViewBag.Version = studentCompareWithBoard.SelectedVersion;
                    ViewBag.Gender = studentCompareWithBoard.SelectedGender;
                    ViewBag.Religion = studentCompareWithBoard.SelectedReligion;
                }
                else
                {
                    ViewBag.ErrorMessage = "Student Info Search Option Not Found .";
                }
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
            }
            #region Report Head Info
            //IList<StudentProgram> studentProgramList = new List<StudentProgram>();
            IList<string> reportHeadInfo = new List<string>();
            List<string> resultList = new List<string>();

            #region Authorization Check
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //if (studentCompareWithBoard != null)
            //    studentProgramList = _informationService.LoadStudentProgramCompareWithBoard(_userMenu, studentCompareWithBoard.ProgramId, studentCompareWithBoard.SessionId
            //        , studentCompareWithBoard.SelectedBranch, studentCompareWithBoard.SelectedCampus, studentCompareWithBoard.SelectedBatchDays, studentCompareWithBoard.SelectedBatchTime
            //        , studentCompareWithBoard.SelectedBatch, studentCompareWithBoard.InformationViewList).ToList();

            //LoadStudentProgramCompareWithBoard
            #endregion
            //Program Name 
            try
            {
                if (studentCompareWithBoard != null)
                {
                    Program p = _programService.GetProgram(studentCompareWithBoard.ProgramId);
                    if (p != null && p.Id > 0)
                        reportHeadInfo.Add(p.Name);
                    else
                        reportHeadInfo.Add("");
                }
            }
            catch { reportHeadInfo.Add(""); }
            //Session Name
            try
            {
                if (studentCompareWithBoard != null)
                {
                    Session s = _sessionService.LoadById(studentCompareWithBoard.SessionId);
                    if (s != null && s.Id > 0)
                        reportHeadInfo.Add(s.Name);
                    else
                        reportHeadInfo.Add("");
                }
            }
            catch { reportHeadInfo.Add(""); }
            //Branch Name 
            try
            {

                if (studentCompareWithBoard.SelectedBranch.Contains(0))
                {
                    reportHeadInfo.Add("All Branch");
                }
                else
                {
                    var selectedBranch = _branchService.LoadBranch(studentCompareWithBoard.SelectedBranch);
                    resultList = (from xx in selectedBranch select xx.Name).ToList();
                    reportHeadInfo.Add(string.Join(", ", resultList));
                }
            }
            catch { reportHeadInfo.Add(""); }
            //Campus Name
            try
            {
                if (studentCompareWithBoard.SelectedCampus.Contains(0))
                {
                    reportHeadInfo.Add("All Campus");
                }
                else
                {
                    var selectedCampus = _campusService.LoadCampus(studentCompareWithBoard.SelectedCampus);
                    resultList = (from xx in selectedCampus select xx.Name).ToList();
                    reportHeadInfo.Add(string.Join(", ", resultList));
                }
            }
            catch { reportHeadInfo.Add(""); }
            //Batch Days    
            try
            {
                if (studentCompareWithBoard.SelectedBatchDays.Contains("0"))
                {
                    reportHeadInfo.Add("All Batch Days");
                }
                else
                {
                    var reportHeadInfoBatchDays = string.Join("; ", studentCompareWithBoard.SelectedBatchDays);
                    reportHeadInfo.Add(reportHeadInfoBatchDays);
                }
            }
            catch { reportHeadInfo.Add(""); }
            //Batch Name
            try
            {
                if (studentCompareWithBoard.SelectedBatch.Contains(0))
                {
                    reportHeadInfo.Add("All Batch");
                }
                else
                {
                    var selectedBatch = _batchService.LoadBatch(studentCompareWithBoard.SelectedBatch);
                    resultList = (from xx in selectedBatch select xx.Name).ToList();
                    reportHeadInfo.Add(string.Join(", ", resultList));
                }
            }
            catch { reportHeadInfo.Add(""); }
            //Course Name
            try
            {

                if (Array.Exists(studentCompareWithBoard.SelectedCourse, item => item == 0))
                {
                    string reportHeadInfoCourseName = "All Course";
                    reportHeadInfo.Add(reportHeadInfoCourseName);
                }
                else
                {
                    IList<Course> courseList = _courseService.LoadCourse(studentCompareWithBoard.SelectedCourse);
                    resultList = (from x in courseList select x.Name).ToList();
                    string reportHeadInfoCourseName = string.Join(", ", resultList);
                    reportHeadInfo.Add(reportHeadInfoCourseName);
                }
            }
            catch { reportHeadInfo.Add(""); }
            reportHeadInfo.Add(studentCompareWithBoard.ExamYear);
            if (studentCompareWithBoard.StudentExamId > 0)
            {
                StudentExam se = _studentExamService.LoadById(studentCompareWithBoard.StudentExamId);
                reportHeadInfo.Add(se.Name);
            }
            ViewBag.ReportHeadInfo = reportHeadInfo;
            #endregion
            return View("StudentCompareWithBoardReport", studentCompareWithBoard);
        }

        #region Render Data Table Student info Compare with board

        [HttpPost]
        public JsonResult StudentInfoCompareWithBoardAjaxRequest(int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId
            , long[] batchName, long[] courseIds, string year, long studentExamId, string[] informationViewList, string[] batchDays, string[] batchTime, int version, int gender, int[] religion
            , string prnNo, string fullName, string fatherName, string institute)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    #region OrderBy and Direction
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        //switch (orderBy)
                        //{
                        //    case "0":
                        //        orderBy = "Name";
                        //        break;
                        //    case "1":
                        //        orderBy = "Code";
                        //        break;
                        //    case "2":
                        //        orderBy = "Rank";
                        //        break;
                        //    case "3":
                        //        orderBy = "Status";
                        //        break;
                        //    default:
                        //        orderBy = "";
                        //        break;
                        //}
                    }
                    #endregion

                    #region Authorization Check
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    //int recordsTotal = _studentProgramService.GetAuthorizedStudentsCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, courseId, version, gender, religion, prnNo, name, mobile);
                    long recordsTotal = _boardInformationService.CountStudentCompareWithBoard(_userMenu, programId, sessionId, studentExamId, year,
                    branchId, campusId, batchDays, batchTime, batchName, religion, gender, version);

                    IList<StudentProgram> studentProgramList = _boardInformationService.LoadStudentCompareWithBoard(start, length, _userMenu, programId, sessionId, studentExamId, year,
                    branchId, campusId, batchDays, batchTime, batchName, religion, gender, version, prnNo, fullName, fatherName, institute);

                    #endregion

                    long recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var c in studentProgramList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        IList<StudentAcademicInfo> academicInfo = c.Student.StudentAcademicInfos.Where(x => x.Year == year && x.StudentExam.Id == studentExamId).ToList();
                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Program Roll":
                                        str.Add(c.PrnNo);
                                        break;
                                    case "Student's Name":
                                        if (c.Student != null && c.Student.FullName != null)
                                        {
                                            str.Add(c.Student.FullName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Student's Name(Board)":
                                        if (c.Student != null && c.Student.StudentAcademicInfos != null)
                                        {
                                            str.Add(academicInfo[0].StudentAcademicInfoBaseData != null ? academicInfo[0].StudentAcademicInfoBaseData[0].Name : "");
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Nick Name":
                                        if (c.Student != null && c.Student.NickName != null)
                                        {
                                            str.Add(c.Student.NickName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Student)":
                                        if (c.Student != null && c.Student.Mobile != null)
                                        {
                                            str.Add(c.Student.Mobile);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Father)":
                                        if (c.Student != null && c.Student.GuardiansMobile1 != null)
                                        {
                                            str.Add(c.Student.GuardiansMobile1);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Mobile Number (Mother)":
                                        if (c.Student != null && c.Student.GuardiansMobile2 != null)
                                        {
                                            str.Add(c.Student.GuardiansMobile2);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Father's Name":
                                        if (c.Student != null && c.Student.FatherName != null)
                                        {
                                            str.Add(c.Student.FatherName);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Father's Name(Board)":
                                        if (c.Student != null && c.Student.StudentAcademicInfos != null)
                                        {
                                            str.Add(academicInfo[0].StudentAcademicInfoBaseData != null ? academicInfo[0].StudentAcademicInfoBaseData[0].FathersName : "");
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Institute Name":
                                        if (c.Student.StudentPrograms[0].Institute != null)
                                        {
                                            str.Add(c.Student.StudentPrograms[0].Institute.Name);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Institute Name(Board)":
                                        //if (c.Student.StudentPrograms[0].Institute != null && c.Student.StudentAcademicInfos[0].StudentAcademicInfoBaseData != null)
                                        if (c.Student != null && c.Student.StudentAcademicInfos[0].StudentAcademicInfoBaseData != null)
                                        {
                                            str.Add(academicInfo[0].StudentAcademicInfoBaseData != null ? academicInfo[0].StudentAcademicInfoBaseData[0].Institute : "");
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Email":
                                        if (c.Student != null && c.Student.Email != null)
                                        {
                                            str.Add(c.Student.Email);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Branch":
                                        if (c.Batch != null && c.Batch.Branch != null)
                                        {
                                            str.Add(c.Batch.Branch.Name);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Batch":
                                        if (c.Batch != null && c.Batch.Name != null)
                                        {
                                            str.Add(c.Batch.Name);
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Version of Study":
                                        if (c.VersionOfStudy != null)
                                        {
                                            str.Add(((VersionOfStudy)c.VersionOfStudy).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Gender":
                                        if (c.Student != null && c.Student.Gender != null)
                                        {
                                            str.Add(((Gender)c.Student.Gender).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case "Religion":
                                        if (c.Student != null && c.Student.Religion != null)
                                        {
                                            str.Add(((Religion)c.Student.Religion).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    default:
                                        break;
                                }

                            }
                            //str.Add("<a target='_blank' href='" + Url.Action("InformationView", "Information") + "?PrnNo=" + c.PrnNo + "'  class='glyphicon glyphicon-th-list'></a>");
                        }
                        sl++;
                        data.Add(str);

                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else { return Json(HttpNotFound()); }

        }

        #endregion

        #region Count Student For Compare With Board
        [HttpPost]
        public JsonResult CountStudentForCompareWithBoard(long programId, long sessionId, long studentExamId, string year, long[] branch,
            long[] campus, string[] batchDays, string[] batchTimes, long[] batch, int[] religion, int? gender, int? versionOfStudy)
        {
            try
            {
                #region Authorization Check
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                long studentCount = _boardInformationService.CountStudentCompareWithBoard(_userMenu, programId, sessionId, studentExamId, year,
                    branch, campus, batchDays, batchTimes, batch, religion, gender, versionOfStudy);

                #endregion

                return Json(new { studentCount = studentCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _logger.Error("CountStudentForCompareWithBoard. AjaxRequestForCountStudentForSynchronizer." + ex.InnerException);
                else
                    _logger.Error("CountStudentForCompareWithBoard. AjaxRequestForCountStudentForSynchronizer." + ex.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #region Export Student List Compare With Board
        [HttpGet]
        public ActionResult ExportStudentCompareWithBoard(long programId, long sessionId, long[] branchId, long[] campusId
            , long[] batchName, long[] courseIds, string year, long studentExamId, string[] informationViewList, string[] batchDays, string[] batchTime, int version, int gender, int[] religion
            , string prnNo, string fullName, string fatherName, string institute)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            long recordsTotal = _boardInformationService.CountStudentCompareWithBoard(_userMenu, programId, sessionId, studentExamId, year,
                    branchId, campusId, batchDays, batchTime, batchName, religion, gender, version);

            IList<StudentProgram> studentProgramList = _boardInformationService.LoadStudentCompareWithBoard(0, Convert.ToInt32(recordsTotal), _userMenu, programId, sessionId, studentExamId, year,
            branchId, campusId, batchDays, batchTime, batchName, religion, gender, version, prnNo, fullName, fatherName, institute);

            var headerList = new List<string> { ErpInfo.OrganizationNameFull, "Student List" };

            var footerList = new List<string> { "" };

            var columnList = new List<string>();

            if (informationViewList != null)
            {
                foreach (string informationView in informationViewList)
                {
                    switch (informationView)
                    {
                        case "Program Roll":
                            columnList.Add("Program Roll");
                            break;
                        case "Student's Name":
                            columnList.Add("Student's Name");
                            break;
                        case "Student's Name(Board)":
                            columnList.Add("Student's Name(Board)");
                            break;
                        case "Nick Name":
                            columnList.Add("Nick Name");
                            break;
                        case "Mobile Number (Student)":
                            columnList.Add("Mobile Number(Student)");
                            break;
                        case "Mobile Number (Father)":
                            columnList.Add("Mobile Number (Father)");
                            break;
                        case "Mobile Number (Mother)":
                            columnList.Add("Mobile Number (Mother)");
                            break;
                        case "Father's Name":
                            columnList.Add("Father's Name");
                            break;
                        case "Father's Name(Board)":
                            columnList.Add("Father's Name(Board)");
                            break;
                        case "Institute Name":
                            columnList.Add("Institute Name");
                            break;
                        case "Institute Name(Board)":
                            columnList.Add("Institute Name(Board)");
                            break;
                        case "Email":
                            columnList.Add("Email");
                            break;
                        case "Branch":
                            columnList.Add("Branch");
                            break;
                        case "Batch":
                            columnList.Add("Batch");
                            break;
                        case "Version of Study":
                            columnList.Add("Version of Study");
                            break;
                        case "Gender":
                            columnList.Add("Gender");
                            break;
                        case "Religion":
                            columnList.Add("Religion");
                            break;
                        default:
                            break;
                    }
                }
            }

            var studentExcelList = new List<List<object>>();

            foreach (var c in studentProgramList)
            {
                var str = new List<object>();
                IList<StudentAcademicInfo> academicInfo = c.Student.StudentAcademicInfos.Where(x => x.Year == year && x.StudentExam.Id == studentExamId).ToList();
                if (informationViewList != null)
                {
                    foreach (string informationView in informationViewList)
                    {
                        switch (informationView)
                        {
                            case "Program Roll":
                                str.Add(c.PrnNo);
                                break;
                            case "Student's Name":
                                if (c.Student != null && c.Student.FullName != null)
                                {
                                    str.Add(c.Student.FullName);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Student's Name(Board)":
                                if (c.Student != null && c.Student.StudentAcademicInfos != null)
                                {
                                    str.Add(academicInfo[0].StudentAcademicInfoBaseData != null ? academicInfo[0].StudentAcademicInfoBaseData[0].Name : "");
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Nick Name":
                                if (c.Student != null && c.Student.NickName != null)
                                {
                                    str.Add(c.Student.NickName);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Mobile Number (Student)":
                                if (c.Student != null && c.Student.Mobile != null)
                                {
                                    str.Add(c.Student.Mobile);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Mobile Number (Father)":
                                if (c.Student != null && c.Student.GuardiansMobile1 != null)
                                {
                                    str.Add(c.Student.GuardiansMobile1);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Mobile Number (Mother)":
                                if (c.Student != null && c.Student.GuardiansMobile2 != null)
                                {
                                    str.Add(c.Student.GuardiansMobile2);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Father's Name":
                                if (c.Student != null && c.Student.FatherName != null)
                                {
                                    str.Add(c.Student.FatherName);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Father's Name(Board)":
                                if (c.Student != null && c.Student.StudentAcademicInfos != null)
                                {
                                    str.Add(academicInfo[0].StudentAcademicInfoBaseData != null ? academicInfo[0].StudentAcademicInfoBaseData[0].FathersName : "");
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Institute Name":
                                if (c.Student.StudentPrograms[0].Institute != null)
                                {
                                    str.Add(c.Student.StudentPrograms[0].Institute.Name);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Institute Name(Board)":
                                if (c.Student.StudentPrograms[0].Institute != null && c.Student.StudentAcademicInfos[0].StudentAcademicInfoBaseData != null)
                                {
                                    str.Add(academicInfo[0].StudentAcademicInfoBaseData != null ? academicInfo[0].StudentAcademicInfoBaseData[0].Institute : "");
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Email":
                                if (c.Student != null && c.Student.Email != null)
                                {
                                    str.Add(c.Student.Email);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Branch":
                                if (c.Batch != null && c.Batch.Branch != null)
                                {
                                    str.Add(c.Batch.Branch.Name);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Batch":
                                if (c.Batch != null && c.Batch.Name != null)
                                {
                                    str.Add(c.Batch.Name);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Version of Study":
                                if (c.VersionOfStudy != null)
                                {
                                    str.Add(((VersionOfStudy)c.VersionOfStudy).ToString());
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Gender":
                                if (c.Student != null && c.Student.Gender != null)
                                {
                                    str.Add(((Gender)c.Student.Gender).ToString());
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Religion":
                                if (c.Student != null && c.Student.Religion != null)
                                {
                                    str.Add(((Religion)c.Student.Religion).ToString());
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                }
                studentExcelList.Add(str);
            }
            ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "student-List-report__"
                + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

            return View("AutoClose");

        }

        #endregion
        #endregion

        #region View Board Roll

        public ActionResult ViewBoardRoll()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            //ViewBag.ProgramId = new SelectList(_programService.LoadAuthorizedProgram(userMenu), "Id", "Name");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            var viewBoardRollViewModelObj = new ViewBoardRollViewModel();
            return View(viewBoardRollViewModelObj);

        }

        [HttpPost]
        public ActionResult ViewBoardRoll(ViewBoardRollViewModel viewBoardRollViewModelObj)
        {

            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            if (ModelState.IsValid)
            {
                try
                {
                    if (viewBoardRollViewModelObj != null)
                    {
                        if (viewBoardRollViewModelObj.InformationViewList == null)
                        {
                            var infoView = new List<string>(new[] { "Program Roll", "Student's Name", "Mobile Number (Student)" });
                            viewBoardRollViewModelObj.InformationViewList = infoView.ToArray();
                        }
                        ViewBag.PageSize = Constants.PageSize;
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "Student Board Roll Search Option Not Found .";
                    }
                }
                catch (Exception)
                {
                    ViewBag.ErrorMessage = "Problem Occurred.";
                    return View();
                }

                #region Report Head Info

                IList<string> reportHeadInfo = new List<string>();
                var resultList = new List<string>();
                try
                {
                    if (viewBoardRollViewModelObj != null)
                    {
                        var p = _programService.GetProgram(viewBoardRollViewModelObj.ProgramId);
                        if (p != null && p.Id > 0)
                            reportHeadInfo.Add(p.Name);
                        else
                            reportHeadInfo.Add("");
                    }
                }
                catch { reportHeadInfo.Add(""); }

                //Session Name
                try
                {
                    if (viewBoardRollViewModelObj != null)
                    {
                        var s = _sessionService.LoadById(viewBoardRollViewModelObj.SessionId);
                        if (s != null && s.Id > 0)
                            reportHeadInfo.Add(s.Name);
                        else
                            reportHeadInfo.Add("");
                    }
                }
                catch { reportHeadInfo.Add(""); }


                //Branch Name 
                try
                {
                    if (viewBoardRollViewModelObj != null && viewBoardRollViewModelObj.SelectedBranch.Contains(0))
                    {
                        reportHeadInfo.Add("All Branch");
                    }
                    else
                    {
                        var selectedBranch = _branchService.LoadBranch(viewBoardRollViewModelObj.SelectedBranch);
                        resultList = (from xx in selectedBranch select xx.Name).ToList();
                        reportHeadInfo.Add(string.Join(", ", resultList));
                    }
                }
                catch { reportHeadInfo.Add(""); }


                //Campus Name
                try
                {
                    if (viewBoardRollViewModelObj != null && viewBoardRollViewModelObj.SelectedCampus.Contains(0))
                    {
                        reportHeadInfo.Add("All Campus");
                    }
                    else
                    {
                        var selectedCampus = _campusService.LoadCampus(viewBoardRollViewModelObj.SelectedCampus);
                        resultList = (from xx in selectedCampus select xx.Name).ToList();
                        reportHeadInfo.Add(string.Join(", ", resultList));
                    }
                }
                catch { reportHeadInfo.Add(""); }
                //Batch Days    
                try
                {
                    if (viewBoardRollViewModelObj != null && viewBoardRollViewModelObj.SelectedBatchDays.Contains("0"))
                    {
                        reportHeadInfo.Add("All Batch Days");
                    }
                    else
                    {
                        var reportHeadInfoBatchDays = string.Join("; ", viewBoardRollViewModelObj.SelectedBatchDays);
                        reportHeadInfo.Add(reportHeadInfoBatchDays);
                    }
                }
                catch { reportHeadInfo.Add(""); }
                //Batch Name
                try
                {
                    if (viewBoardRollViewModelObj != null && viewBoardRollViewModelObj.SelectedBatch.Contains(0))
                    {
                        reportHeadInfo.Add("All Batch");
                    }
                    else
                    {
                        var selectedBatch = _batchService.LoadBatch(viewBoardRollViewModelObj.SelectedBatch);
                        resultList = (from xx in selectedBatch select xx.Name).ToList();
                        reportHeadInfo.Add(string.Join(", ", resultList));
                    }
                }
                catch { reportHeadInfo.Add(""); }

                ViewBag.ReportHeadInfo = reportHeadInfo;
                #endregion
                return View("GetViewBoardRollListReport", viewBoardRollViewModelObj);

            }
            else
            {
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", viewBoardRollViewModelObj.OrganizationId);
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                return View(viewBoardRollViewModelObj);
            }
        }


        #region Render Data Table Student Information List

        [HttpPost]
        public JsonResult StudentViewBoardRollAjaxRequest(int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId
            , long[] batchName, long[] course, string[] informationViewList, string[] batchDays, string[] batchTime, int selectStatus, string prnNo, string name
            , string mobile)
        {

            if (Request.IsAjaxRequest())
            {
                try
                {
                    #region OrderBy and Direction
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        int informationViewListLength = informationViewList.Length;
                        if (Convert.ToInt32(orderBy) == 0)
                            orderBy = "A.Id";
                        else
                        {
                            switch (informationViewList[Convert.ToInt32(orderBy) - 1])
                            {
                                case "Program Roll":
                                    orderBy = "A.PrnNo";
                                    break;
                                default:
                                    orderBy = "A.Id";
                                    break;
                            }
                        }
                    }

                    #endregion

                    #region Authorization Check
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    long recordsTotal = _boardInformationService.GetAuthorizedStudentsCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, selectStatus, prnNo, name, mobile, informationViewList);
                    IList<ViewBoardRollListDto> viewBoardRollListDtoList = _boardInformationService.LoadAuthorizedStudents(
                        _userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, selectStatus, orderBy, orderDir.ToUpper(), start, length, prnNo, name, mobile, informationViewList).ToList();

                    // int recordsTotal = 10;
                    //IList<ViewBoardRollListDto> viewBoardRollListDtoList = new List<ViewBoardRollListDto>();
                    #endregion

                    long recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var c in viewBoardRollListDtoList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Program Roll":
                                        str.Add(c.PrnNo);
                                        break;

                                    case "Student's Name":
                                        str.Add(c.FullName);
                                        break;
                                    case "Nick Name":
                                        str.Add(c.NickName);
                                        break;
                                    case "Mobile Number (Student)":
                                        str.Add(c.Mobile);
                                        break;
                                    case "Mobile Number (Father)":
                                        str.Add(c.GuardiansMobile1);
                                        break;
                                    case "Mobile Number (Mother)":
                                        str.Add(c.GuardiansMobile2);
                                        break;

                                    case "JSC Year":
                                        str.Add(c.JscYear);
                                        break;
                                    case "JSC Board":
                                        str.Add(c.JscBoard);
                                        break;
                                    case "JSC Roll":
                                        str.Add(c.JscRoll);
                                        break;
                                    case "JSC Registration Number":
                                        str.Add(c.JscRegistrationNumber);
                                        break;
                                    case "SSC Year":
                                        str.Add(c.SscYear);
                                        break;
                                    case "SSC Board":
                                        str.Add(c.SscBoard);
                                        break;
                                    case "SSC Roll":
                                        str.Add(c.SscRoll);
                                        break;
                                    case "SSC Registration Number":
                                        str.Add(c.SscRegistrationNumber);
                                        break;
                                    case "HSC Year":
                                        str.Add(c.HscYear);
                                        break;
                                    case "HSC Board":
                                        str.Add(c.HscBoard);
                                        break;
                                    case "HSC Roll":
                                        str.Add(c.HscRoll);
                                        break;
                                    case "HSC Registration Number":
                                        str.Add(c.HscRegistrationNumber);
                                        break;


                                    default:
                                        break;
                                }
                            }
                        }
                        sl++;
                        data.Add(str);

                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                    //return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return Json(HttpNotFound()); }

        }

        #endregion

        #region Export view Board roll files
        public ActionResult ExportViewBoardRollFiles(string programHeader,
            string branchHeader,
            string campusHeader,
            string batchDayHeader,
            string batchHeader, long programId, long sessionId, long[] branchId, long[] campusId
            , long[] batchName, long[] course, string[] informationViewList, string[] batchDays, string[] batchTime, int selectStatus, string prnNo, string name
            , string mobile)
        {
            try
            {
                var headerList = new List<string>();
                headerList.Add(programHeader);
                headerList.Add("Missing Board Roll Information");
                headerList.Add("Program : " + programHeader);
                headerList.Add("Branch : " + branchHeader);
                headerList.Add("Branch : " + branchHeader);
                headerList.Add("Campus : " + campusHeader);
                headerList.Add("Batch Days : " + batchDayHeader);
                headerList.Add("Batch : " + batchHeader);
                headerList.Add("");
                var columnList = new List<string>();
                foreach (var information in informationViewList)
                {
                    columnList.Add(information);
                }
                var footerList = new List<string>();
                footerList.Add("");
                #region Authorization Check
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                long recordsTotal = _boardInformationService.GetAuthorizedStudentsCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, selectStatus, prnNo, name, mobile, informationViewList);
                IList<ViewBoardRollListDto> viewBoardRollListDtoList = _boardInformationService.LoadAuthorizedStudents(
                    _userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, selectStatus, "prnno", "ASC", 0, (int)recordsTotal, prnNo, name, mobile, informationViewList).ToList();
                #endregion
                var excelList = new List<List<object>>();
                foreach (var c in viewBoardRollListDtoList)
                {
                    var str = new List<object>();
                    foreach (string informationView in informationViewList)
                    {
                        switch (informationView)
                        {
                            case "Program Roll":
                                str.Add(c.PrnNo);
                                break;

                            case "Student's Name":
                                str.Add(c.FullName);
                                break;
                            case "Nick Name":
                                str.Add(c.NickName);
                                break;
                            case "Mobile Number (Student)":
                                str.Add(c.Mobile);
                                break;
                            case "Mobile Number (Father)":
                                str.Add(c.GuardiansMobile1);
                                break;
                            case "Mobile Number (Mother)":
                                str.Add(c.GuardiansMobile2);
                                break;

                            case "JSC Year":
                                str.Add(c.JscYear);
                                break;
                            case "JSC Board":
                                str.Add(c.JscBoard);
                                break;
                            case "JSC Roll":
                                str.Add(c.JscRoll);
                                break;
                            case "JSC Registration Number":
                                str.Add(c.JscRegistrationNumber);
                                break;
                            case "SSC Year":
                                str.Add(c.SscYear);
                                break;
                            case "SSC Board":
                                str.Add(c.SscBoard);
                                break;
                            case "SSC Roll":
                                str.Add(c.SscRoll);
                                break;
                            case "SSC Registration Number":
                                str.Add(c.SscRegistrationNumber);
                                break;
                            case "HSC Year":
                                str.Add(c.HscYear);
                                break;
                            case "HSC Board":
                                str.Add(c.HscBoard);
                                break;
                            case "HSC Roll":
                                str.Add(c.HscRoll);
                                break;
                            case "HSC Registration Number":
                                str.Add(c.HscRegistrationNumber);
                                break;
                            default:
                                break;
                        }
                    }
                    excelList.Add(str);

                }
                ExcelGenerator.GenerateExcel(headerList, columnList, excelList, footerList, "view_board_roll__"
                + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }
        #endregion

        #endregion


    }
}