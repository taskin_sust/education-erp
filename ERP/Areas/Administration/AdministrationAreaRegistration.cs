﻿using System.Web.Mvc;

namespace UdvashERP.Areas.Administration
{
    public class AdministrationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Administration";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Administration_default",
                "Administration/{controller}/{action}/{id}",
                //new { controller = "AdministrationHome", action = "CashReset", id = UrlParameter.Optional }
                new { controller = "AdministrationHome", action = "Index", id = UrlParameter.Optional }
                 
            );
        }
    }
}