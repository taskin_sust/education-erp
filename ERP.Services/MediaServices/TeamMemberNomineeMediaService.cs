﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.Dao.MediaDao;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.MediaServices
{
    public interface ITeamMemberNomineeMediaService
    {

        TeamMemberNomineeMediaImage GetTeamMemberNomineeImageMediaByRefId(long refId);
    }
    public class TeamMemberNomineeMediaService:ITeamMemberNomineeMediaService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        
        private readonly ITeamMemberNomineeImageMediaDao _teamMemberNomineeMediaImageDao;
        private ISession session;
        public TeamMemberNomineeMediaService(ISession sessionParam = null)
        {
            if (sessionParam == null)
            {

                var sessionMedia = NHibernateMediaSessionFactory.OpenSession();
                session = sessionMedia;   
            }
            else
            {
                session = sessionParam;
            }
            _teamMemberNomineeMediaImageDao = new TeamMemberNomineeImageMediaDao { Session = session };
        }

        public TeamMemberNomineeMediaImage GetTeamMemberNomineeImageMediaByRefId(long refId)
        {
            try
            {
                return _teamMemberNomineeMediaImageDao.GetTeamMemberNomineeImageMediaByRefId(refId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
    }
}
