using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class GoodsTransferDetailsFactory : ObjectFactoryBase<GoodsTransferDetails>
    {

        private readonly IGoodsTransferDetailService _goodsTransferDetailService;

        public GoodsTransferDetailsFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _goodsTransferDetailService = new GoodsTransferDetailsService(session);

        }

        #region Create

        public GoodsTransferDetailsFactory Create()
        {
            Object = new GoodsTransferDetails()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                TransferQuantity = 40,
                Status = GoodsTransfer.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsTransferDetailsFactory CreateWith(long id)
        {
            Object = null;
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsTransferDetailsFactory WithGoodsTransfer(GoodsTransfer goodsTransfer)
        {
            Object.GoodsTransfer = goodsTransfer;
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsTransferDetailsFactory WithItem(Item item)
        {
            Object.Item = item;
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsTransferDetailsFactory WithProgram(Program program)
        {
            Object.Program = program;
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsTransferDetailsFactory WithSession(Session session)
        {
            Object.Session = session;
            SingleObjectList.Add(Object);
            return this;
        }


        public GoodsTransferDetailsFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _goodsTransferDetailService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_goodsTransferDetailService.SaveOrUpdate(x); });

            }
            return this;
        }

        public GoodsTransferDetailsFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new GoodsTransferDetails() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public GoodsTransferDetailsFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion

    }
}
