﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Teachers
{
    public interface ITeacherAcademicMaterialDetailsDao : IBaseDao<TeacherAcademicMaterialsDetails, long>
    {
        #region List loading function

        IList<TeacherAcademicMaterialsDetails> TeacherAcademicMaterialsList(int start, int length, long organizationId, List<long> programIdlist, List<long> branchIdlist, long sessionId, string paymentStatus, string date, string teacherName, long? forBranchId, string trackingId, string tpin);

        #endregion

        #region Others function

        long CountTeacherAcademicMaterials(long organizationId, List<long> programIdlist, List<long> branchIdlist, long sessionId, string paymentStatus, string date, string teacherName, long? forBranchId, string trackingId, string tpin);

        #endregion
    }

    public class TeacherAcademicMaterialDetailsDao : BaseDao<TeacherAcademicMaterialsDetails, long>, ITeacherAcademicMaterialDetailsDao
    {
        #region List loading function

        public IList<TeacherAcademicMaterialsDetails> TeacherAcademicMaterialsList(int start, int length, long organizationId, List<long> programIdlist, List<long> branchIdlist, long sessionId, string paymentStatus, string date, string teacherName, long? forBranchId, string trackingId, string tpin)
        {
            var criteria = GetTeacherAcademicMaterialsCriteria(organizationId, programIdlist, branchIdlist, sessionId, paymentStatus, date, teacherName, forBranchId, trackingId, tpin);
            criteria.AddOrder(Order.Desc(Projections.Id()));
            criteria.SetFirstResult(start).SetMaxResults(length);

            List<TeacherAcademicMaterialsDetails> teacherAcademicMaterialsDetails = criteria.List<TeacherAcademicMaterialsDetails>().ToList();
            return teacherAcademicMaterialsDetails;
        }

        #endregion

        #region Others function

        public long CountTeacherAcademicMaterials(long organizationId, List<long> programIdlist, List<long> branchIdlist, long sessionId, string paymentStatus, string date, string teacherName, long? forBranchId, string trackingId, string tpin)
        {
            var criteria = GetTeacherAcademicMaterialsCriteria(organizationId, programIdlist, branchIdlist, sessionId, paymentStatus, date, teacherName, forBranchId, trackingId, tpin);

            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt64(criteria.UniqueResult());
        }

        private ICriteria GetTeacherAcademicMaterialsCriteria(long organizationId, List<long> programIdlist, List<long> branchIdlist, long sessionId, string paymentStatus, string date, string teacherName, long? forBranchId, string trackingId, string tpin)
        {
            ICriteria criteria =
                Session.CreateCriteria<TeacherAcademicMaterialsDetails>()
                    .Add(Restrictions.Eq("Status", TeacherAcademicMaterialsDetails.EntityStatus.Active));
            criteria.CreateAlias("TeacherAcademicMaterials", "tam").Add(Restrictions.Eq("Status", TeacherAcademicMaterials.EntityStatus.Active));
            criteria.CreateAlias("Teacher", "t").Add(Restrictions.Eq("Status", Teacher.EntityStatus.Active));
            criteria.CreateAlias("tam.Organization", "org").Add(Restrictions.Eq("Status", Organization.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("org.Id", organizationId));

            if (!String.IsNullOrEmpty(tpin))
            {
                int teacherPin;
                if (int.TryParse(tpin, out teacherPin))
                {
                    criteria.Add(Restrictions.Eq("t.Tpin", teacherPin));
                }
            }

            if (programIdlist.Count > 0)
            {
                Disjunction drProgram = new Disjunction();
                drProgram.Add(Restrictions.In("tam.Program", programIdlist));
                drProgram.Add(Restrictions.IsNull("tam.Program"));
                criteria.Add(drProgram);
            }
            else
                criteria.Add(Restrictions.IsNull("tam.Program"));

            if (branchIdlist.Count > 0)
            {
                Disjunction drBranch = new Disjunction();
                drBranch.Add(Restrictions.In("ForBranch.Id", branchIdlist));
                drBranch.Add(Restrictions.IsNull("ForBranch"));
                criteria.Add(drBranch);
            }
            else
                criteria.Add(Restrictions.IsNull("ForBranch"));

            if (sessionId != -1)
            {
                criteria.CreateAlias("tam.Session", "s");
                criteria.Add(Restrictions.Eq("s.Id", sessionId));
            }
            else
            {
                criteria.Add(Restrictions.IsNull("tam.Session"));
            }

            if (forBranchId != null && forBranchId != -1)
            {
                criteria.CreateAlias("ForBranch", "fB");
                criteria.Add(Restrictions.Eq("fB.Id", forBranchId));
            }
            else if (forBranchId == -1)
            {
                criteria.Add(Restrictions.IsNull("ForBranch"));
            }

            if (!String.IsNullOrEmpty(paymentStatus))
            {
                criteria.Add(Restrictions.Eq("PaymentStatus", Convert.ToInt32(paymentStatus)));
            }

            if (!String.IsNullOrEmpty(teacherName))
            {
                criteria.Add(Restrictions.Like("t.NickName", teacherName, MatchMode.Anywhere));
            }
            //if (forBranchId != null)
            //{
            //    criteria.Add(Restrictions.Eq("fB.Id", forBranchId));
            //}
            if (!String.IsNullOrEmpty(date))
            {
                //DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //criteria.Add(Restrictions.Eq(Projections.SqlFunction("date", NHibernateUtil.DateTime,
                //        Projections.Property("tam.SubmissionDate")), dt));
                DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                criteria.Add(Expression.Eq(Projections.SqlFunction("date", NHibernateUtil.DateTime, Projections.Property("tam.SubmissionDate")), dt));
            }
            if (!String.IsNullOrEmpty(trackingId))
            {
                long convertTrackingId;
                var result = long.TryParse(trackingId, out convertTrackingId);
                if (result)
                    criteria.Add(Restrictions.Eq("tam.Id", Convert.ToInt64(convertTrackingId)));
            }
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            return criteria;
        }

        #endregion

    }
}
