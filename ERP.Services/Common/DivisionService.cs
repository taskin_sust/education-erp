﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Dao.Common;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Common
{
    public interface IDivisionService : IBaseService
    {

    }
    public class DivisionService : BaseService, IDivisionService
    {
        private readonly IDivisionDao _divisionDao;
        public DivisionService(ISession session)
        {
            Session = session;
            _divisionDao = new DivisionDao() { Session = session };
        }

        public void Save(BusinessModel.Entity.Common.Division Object)
        {
            _divisionDao.Save(Object);
        }
    }
}
