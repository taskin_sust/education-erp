﻿using System;

namespace UdvashERP.MessageExceptions
{
    public class DataNotFoundException : Exception
    {
        public DataNotFoundException():base("Not Found")
        {

        }

        public DataNotFoundException(string message):base(message)
        {

        }

    }
}
