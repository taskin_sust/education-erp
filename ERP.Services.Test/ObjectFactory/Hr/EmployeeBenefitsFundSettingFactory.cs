using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class EmployeeBenefitsFundSettingFactory : ObjectFactoryBase<EmployeeBenefitsFundSetting>
    {
       // private OrganizationFactory _organizationFactory;
        private readonly IEmployeeBenefitsFundSettingService _employeeBenefitsFundSettingService;

        public EmployeeBenefitsFundSettingFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
           // _organizationFactory = new OrganizationFactory(null, session);
            _employeeBenefitsFundSettingService = new EmployeeBenefitsFundSettingService(session);

        }

        #region Create

        public EmployeeBenefitsFundSettingFactory Create(string name)
        {
            Object = new EmployeeBenefitsFundSetting()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = EmployeeBenefitsFundSetting.EntityStatus.Active,
                Name = name,
            };

            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingFactory WithEmploymentStatus(MemberEmploymentStatus employmentStatus)
        {
            Object.EmploymentStatus = (int) employmentStatus;
            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            Object.OrganizationId = organization.Id;
            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingFactory WithCalculationOn(CalculationOn calculationOn)
        {
            Object.CalculationOn = (int) calculationOn;
            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingFactory WithEmployeeEmployerContribution(decimal employeeContribution, decimal employerContribution)
        {
            Object.EmployeeContribution = employeeContribution;
            Object.EmployerContribution = employerContribution;         
            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingFactory WithEffectiveClosingDate(DateTime effectiveTime, DateTime closingDate)
        {
            Object.EffectiveDate = effectiveTime;
            Object.ClosingDate = closingDate;      
            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingFactory WithEmployeeBenefitFundSettingEntitlement(IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList)
        {
            Object.EmployeeBenefitsFundSettingEntitlement = employeeBenefitsFundSettingEntitlementList;
            SingleObjectList.Add(Object);
            return this;
        }

        public EmployeeBenefitsFundSettingFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public EmployeeBenefitsFundSettingFactory Update(List<UserMenu> userMenus, long id, EmployeeBenefitsFundSetting employeeBenefitsFundSetting)
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _employeeBenefitsFundSettingService.Update( userMenus,id,x); });
            }

            return this;
        }

        public EmployeeBenefitsFundSettingFactory Persist(List<UserMenu> userMenus)
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _employeeBenefitsFundSettingService.Save(x, userMenus); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x => { if (x.Id == 0)_employeeBenefitsFundSettingService.Save(x, userMenus); });
            }
            return this;
        }

        public EmployeeBenefitsFundSettingFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new EmployeeBenefitsFundSetting() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

       

        public EmployeeBenefitsFundSettingFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion

    }
}
