﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Utils;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface IFestivalBonusSheetService : IBaseService
    {

        #region Operational Function

        void SaveOrUpdate(List<FestivalBonusSheet> objectList);

        #endregion

        #region Single Instances Loading Function

        BusinessModel.Entity.Hr.FestivalBonusSheet LoadById(long id);

        #endregion

        #region List Loading Function

        List<FestivalBonusSheet> LoadFestivalBonusSheetsList(List<long> teamMemberIds, long festivalBonusSettingId, DateTime startDate, DateTime endDate, bool isSubmit = true);

        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion

    }
    public class FestivalBonusSheetService : BaseService, IFestivalBonusSheetService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("PayrollService");

        #endregion

        #region Propertise & Object Initialization

       
        private readonly FestivalBonusSheetDao _festivalBonusSheetDao;
        public FestivalBonusSheetService(ISession session)
        {
            Session = session;
            _festivalBonusSheetDao = new FestivalBonusSheetDao() { Session = session };
        }

        #endregion

        #region Operational Function

        public void SaveOrUpdate(List<FestivalBonusSheet> objectList)
        {
            ITransaction trans = null;
            try
            {
                if (objectList==null || objectList.Count()<=0)
                    throw new InvalidDataException("Object can not empty");

                using (trans = Session.BeginTransaction())
                {
                    foreach (var festivalBonusSheet in objectList)
                    {
                        CheckBeforeSaveOrUpdate(festivalBonusSheet);
                        FestivalBonusSheet fbSheet = _festivalBonusSheetDao.GetTeamMemberObject(festivalBonusSheet.TeamMember,
                            festivalBonusSheet.StartDate, festivalBonusSheet.EndDate, festivalBonusSheet.FestivalBonusSetting);

                        if (fbSheet == null)
                        {
                            festivalBonusSheet.Status = FestivalBonusSheet.EntityStatus.Active;
                            fbSheet = festivalBonusSheet;
                        }
                        else if (fbSheet.IsSubmit)
                        {
                            throw new InvalidDataException("Invalid Data found .");
                        }
                        else
                        {
                            //var fbSheet = _festivalBonusSheetDao.LoadById(festivalBonusSheet.Id);
                            fbSheet.TeamMember = festivalBonusSheet.TeamMember;
                            fbSheet.FestivalBonusSetting = festivalBonusSheet.FestivalBonusSetting;
                            fbSheet.SalaryOrganization = festivalBonusSheet.SalaryOrganization;
                            fbSheet.SalaryBranch = festivalBonusSheet.SalaryBranch;
                            fbSheet.SalaryCampus = festivalBonusSheet.SalaryCampus;
                            fbSheet.SalaryDepartment = festivalBonusSheet.SalaryDepartment;
                            fbSheet.SalaryDesignation = festivalBonusSheet.JobDesignation;
                            fbSheet.JobOrganization = festivalBonusSheet.JobOrganization;
                            fbSheet.JobBranch = festivalBonusSheet.JobBranch;
                            fbSheet.JobCampus = festivalBonusSheet.JobCampus;
                            fbSheet.JobDepartment = festivalBonusSheet.JobDepartment;
                            fbSheet.JobDesignation = festivalBonusSheet.JobDesignation;
                            fbSheet.BankAccount = festivalBonusSheet.BankAccount;
                            fbSheet.CashAccount = festivalBonusSheet.CashAccount;
                            fbSheet.Remarks = festivalBonusSheet.Remarks;
                            fbSheet.IsSubmit = festivalBonusSheet.IsSubmit;
                            fbSheet.StartDate = festivalBonusSheet.StartDate;
                            fbSheet.EndDate = festivalBonusSheet.EndDate;
                            fbSheet.Year = festivalBonusSheet.Year;
                            fbSheet.Month = festivalBonusSheet.Month;
                        }
                        _festivalBonusSheetDao.SaveOrUpdate(fbSheet);
                    }
                    trans.Commit();
                }
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (MessageException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }
        

        #endregion

        #region Single Instances Loading Function

        public FestivalBonusSheet LoadById(long id)
        {
            try
            {
                return _festivalBonusSheetDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public List<FestivalBonusSheet> LoadFestivalBonusSheetsList(List<long> teamMemberIds, long festivalBonusSettingId, DateTime startDate, DateTime endDate, bool isSubmit = true)
        {
            try
            {
                if (teamMemberIds == null || !teamMemberIds.Any())
                    throw new NullObjectException("Team member can't empty");

                return _festivalBonusSheetDao.LoadFestivalBonusSheetsList(teamMemberIds, festivalBonusSettingId,startDate, endDate, isSubmit);
                
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        private void CheckBeforeSaveOrUpdate(FestivalBonusSheet obj)
        {

            if (obj == null)
                throw new NullObjectException("Object can not be null !!");
            CustomModelValidationCheck.DataAnnotationCheck(obj, new FestivalBonusSheet());

        }

        #endregion

    }
}
