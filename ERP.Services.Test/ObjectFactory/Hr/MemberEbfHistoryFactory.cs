﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class MemberEbfHistoryFactory:ObjectFactoryBase<MemberEbfHistory>
    {
        public MemberEbfHistoryFactory(IObjectFactoryBase caller,ISession session):base(caller,session)
        {

        }

        public MemberEbfHistoryFactory Create()
        {
            Object = new MemberEbfHistory()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = MemberEbfHistory.EntityStatus.Active,
                CalculationOn = 5,
                EmployeeContributionAmount = 5,
                EmployoyerContributionAmount = 5,
                DateFrom = DateTime.Now,
                DateTo = DateTime.Now
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberEbfHistoryFactory CreateWith(long id, int calculationOn, decimal employeeContributionAmount,decimal employoyerContributionAmount,
            DateTime dateFrom,DateTime? dateTo)
        {
            Object = new MemberEbfHistory()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = MemberEbfHistory.EntityStatus.Active,
                CalculationOn = calculationOn,
                EmployeeContributionAmount = employeeContributionAmount,
                EmployoyerContributionAmount = employoyerContributionAmount,
                DateFrom = dateFrom,
                DateTo = dateTo
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberEbfHistoryFactory WithSalarySheet(SalarySheet salarySheet)
        {
            Object.SalarySheet = salarySheet;
            return this;
        }

        public MemberEbfHistoryFactory WithEmployeeBenifitsFundSetting(EmployeeBenefitsFundSetting employeeBenefitsFundSetting)
        {
            Object.EmployeeBenefitsFundSetting = employeeBenefitsFundSetting;
            return this;
        }

        public MemberEbfHistoryFactory CleanUp()
        {
            DeleteAll();
            return this;
        }
    }
}