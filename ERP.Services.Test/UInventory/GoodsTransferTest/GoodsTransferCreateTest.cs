﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Hr;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.GoodsTransferTest
{
    interface IGoodsTransferCreateTest
    {
        void Save_Should_Be_Successfully();
        void Save_Should_Be_Fail_For_Null_From_Branch();
        void Save_Should_Be_Fail_For_Null_To_Branch();
        void Save_Should_Be_Fail_For_Same_Branch();
        void Save_Should_Be_Fail_If_To_Branch_IsCorporate();
        void Save_Should_Be_Fail_Without_GoodsDetails();
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsTransfer")]
    public class GoodsTransferCreateTest : GoodsTransferBaseTest, IGoodsTransferCreateTest
    {
        [Fact]
        public void Save_Should_Be_Successfully()
        {
            organizationFactory.Create().Persist();
            ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            SessionFactory.Create().Persist();
            var formBranch = BranchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            GoodsReceiveFactory.Create().WithBranch(formBranch.Object);
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Same_Branch()
        {
            organizationFactory.Create().Persist();
            ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            SessionFactory.Create().Persist();
            var formBranch = BranchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            var toBranch = BranchFactory.Create().WithOrganization().Persist();
            ItemFactory.CreateWith(0, "TEST ITEM", 1, 1, 1).WithItemGroup().WithOrganization(organizationFactory.Object).Persist();
            ProgramSessionItemFactory.Create().WithItem(ItemFactory.Object).WithProgramAndSession(ProgramFactory.Object, SessionFactory.Object).Persist();
            GoodsTransferDetailsFactory.Create().WithGoodsTransfer(GoodsTransferFactory.Object).WithItem(ItemFactory.Object).WithProgram(ProgramFactory.Object).WithSession(SessionFactory.Object);
            Assert.Throws<NullObjectException>(() => GoodsTransferFactory.Create().WithDetails(GoodsTransferDetailsFactory.ObjectList).WithFromBranch(formBranch.Object).WithToBranch(toBranch.Object).Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Null_From_Branch()
        {
            organizationFactory.Create().Persist();
            ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            SessionFactory.Create().Persist();
            //var formBranch = BranchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var toBranch = BranchFactory.Create().WithOrganization().Persist();
            ItemFactory.CreateWith(0, "TEST ITEM", 1, 1, 1).WithItemGroup().WithOrganization(organizationFactory.Object).Persist();
            ProgramSessionItemFactory.Create().WithItem(ItemFactory.Object).WithProgramAndSession(ProgramFactory.Object, SessionFactory.Object).Persist();
            GoodsTransferDetailsFactory.Create().WithGoodsTransfer(GoodsTransferFactory.Object).WithItem(ItemFactory.Object).WithProgram(ProgramFactory.Object).WithSession(SessionFactory.Object);
            Assert.Throws<NullObjectException>(() => GoodsTransferFactory.Create().WithDetails(GoodsTransferDetailsFactory.ObjectList).WithFromBranch(null).WithToBranch(toBranch.Object).Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Null_To_Branch()
        {
            organizationFactory.Create().Persist();
            ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            SessionFactory.Create().Persist();
            var formBranch = BranchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            //var toBranch = BranchFactory.Create().WithOrganization().Persist();
            ItemFactory.CreateWith(0, "TEST ITEM", 1, 1, 1).WithItemGroup().WithOrganization(organizationFactory.Object).Persist();
            ProgramSessionItemFactory.Create().WithItem(ItemFactory.Object).WithProgramAndSession(ProgramFactory.Object, SessionFactory.Object).Persist();
            GoodsTransferDetailsFactory.Create().WithGoodsTransfer(GoodsTransferFactory.Object).WithItem(ItemFactory.Object).WithProgram(ProgramFactory.Object).WithSession(SessionFactory.Object);
            Assert.Throws<InvalidDataException>(() => GoodsTransferFactory.Create().WithDetails(GoodsTransferDetailsFactory.ObjectList).WithFromBranch(formBranch.Object).WithToBranch(null).Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_If_To_Branch_IsCorporate()
        {
            organizationFactory.Create().Persist();
            ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            SessionFactory.Create().Persist();
            var formBranch = BranchFactory.Create().IsCorporate(false).WithOrganization(organizationFactory.Object).Persist();
            var toBranch = BranchFactory.Create().IsCorporate(false).WithOrganization().Persist();
            ItemFactory.CreateWith(0, "TEST ITEM", 1, 1, 1).WithItemGroup().WithOrganization(organizationFactory.Object).Persist();
            ProgramSessionItemFactory.Create().WithItem(ItemFactory.Object).WithProgramAndSession(ProgramFactory.Object, SessionFactory.Object).Persist();
            GoodsTransferDetailsFactory.Create().WithGoodsTransfer(GoodsTransferFactory.Object).WithItem(ItemFactory.Object).WithProgram(ProgramFactory.Object).WithSession(SessionFactory.Object);
            Assert.Throws<InvalidDataException>(() => GoodsTransferFactory.Create().WithDetails(GoodsTransferDetailsFactory.ObjectList).WithFromBranch(formBranch.Object).WithToBranch(toBranch.Object).Persist());
        }

        [Fact]
        public void Save_Should_Be_Fail_Without_GoodsDetails()
        {
            organizationFactory.Create().Persist();
            ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            SessionFactory.Create().Persist();
            var formBranch = BranchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            var toBranch = BranchFactory.Create().WithOrganization().Persist();
            ItemFactory.CreateWith(0, "TEST ITEM", 1, 1, 1).WithItemGroup().WithOrganization(organizationFactory.Object).Persist();
            ProgramSessionItemFactory.Create().WithItem(ItemFactory.Object).WithProgramAndSession(ProgramFactory.Object, SessionFactory.Object).Persist();
           // GoodsTransferDetailsFactory.Create().WithGoodsTransfer(GoodsTransferFactory.Object).WithItem(ItemFactory.Object).WithProgram(ProgramFactory.Object).WithSession(SessionFactory.Object);
            Assert.Throws<NullObjectException>(() => GoodsTransferFactory.Create().WithDetails(null).WithFromBranch(formBranch.Object).WithToBranch(toBranch.Object).Persist());
        }
    }
}
