﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Exam;

namespace UdvashERP.Dao.Mapping.Exam
{
    public partial class ExamStudentAnswerScriptMap:BaseClassMap<ExamsStudentAnswerScript,long>
    {
        public ExamStudentAnswerScriptMap()
        {
            Table("ExamsStudentAnswerScript");
            LazyLoad();

            Map(x => x.SetCode).Column("SetCode");
            Map(x => x.QuestionNo).Column("QuestionNo");
            Map(x => x.QuestionAnswer).Column("QuestionAnswer");

            References(x => x.ExamsDetails).Column("ExamsDetailsId");
        }
    }
}
