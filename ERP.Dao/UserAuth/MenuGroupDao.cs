﻿using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.UserAuth
{
    public interface IMenuGroupDao : IBaseDao<MenuGroup, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        MenuGroup LoadByRank(int rank);  
        #endregion

        #region List Loading Function
        List<MenuGroup> GetMenuGroupList(int start, int length, string orderBy, string direction, string name, string status, string rank);
        #endregion

        #region Others Function
        bool DuplicationCheck(string name, long? id);
        int MenuGroupRowCount(string name, string status, string rank);        
        #endregion
    }

    public class MenuGroupDao : BaseDao<MenuGroup, long>, IMenuGroupDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public MenuGroup LoadByRank(int rank)
        {
            return Session.QueryOver<MenuGroup>().Where(x => x.Rank == rank).SingleOrDefault<MenuGroup>();
        }
        #endregion

        #region List Loading Function
        public List<MenuGroup> GetMenuGroupList(int start, int length, string orderBy, string direction, string name, string status, string rank)
        {
            ICriteria criteria =
                    Session.CreateCriteria<MenuGroup>()
                        .Add(Restrictions.Not(Restrictions.Eq("Status", MenuGroup.EntityStatus.Delete)));


            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));

            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }

            if (!string.IsNullOrEmpty(orderBy.Trim()))
            {
                if (direction == "ASC")
                {
                    criteria.AddOrder(Order.Asc(orderBy));
                }
                else
                {
                    criteria.AddOrder(Order.Desc(orderBy));
                } 
            }

            return (List<MenuGroup>)criteria.SetFirstResult(start).SetMaxResults(length).List<MenuGroup>();
        }
        #endregion

        #region Others Function
        public int MenuGroupRowCount(string name, string status, string rank)
        {
            try
            {
                ICriteria criteria =
                   Session.CreateCriteria<MenuGroup>();
                if (!String.IsNullOrEmpty(name))
                {
                    criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
                }
                if (!String.IsNullOrEmpty(status))
                {
                    criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));

                }
                if (!String.IsNullOrEmpty(rank))
                {
                    criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
                }


                IList list = criteria.Add(Restrictions.Not(Restrictions.Eq("Status", MenuGroup.EntityStatus.Delete))).List();
                return list.Count;
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public bool DuplicationCheck(string name, long? id)
        {
            var menuGroup = Session.QueryOver<MenuGroup>().Where(x => x.Status != Menu.EntityStatus.Delete && x.Name == name);
            if (id != null)
            {
                menuGroup = menuGroup.Where(x => x.Id != id);
            }
            IList<MenuGroup> catList = menuGroup.List<MenuGroup>();
            if (catList == null || catList.Count < 1)
                return true;
            else
                return false;
        }       
        #endregion
    }
}