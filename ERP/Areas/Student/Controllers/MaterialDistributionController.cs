﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using FluentNHibernate.Testing.Values;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using NHibernate.Criterion;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.Areas.Student.Models;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using Constants = UdvashERP.BusinessRules.Constants;
using System.IO;
using System.Xml;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class MaterialDistributionController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("StudentArea");

        private void LogError(string message, Exception exception)
        {
            if (exception != null)
            {
                _logger.Error(message, exception);
            }
            else
            {
                _logger.Error(message);
            }
        }

        #endregion

        #region Objects/Propertise/Services/Dao

        #region Services

        private readonly IBatchService _batchService;

        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;

        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;

        private readonly IMaterialService _materialService;
        private IMaterialTypeService _materialTypeService;
        private readonly IStudentProgramService _studentProgramService;

        private readonly IMaterialDistributionService _materialDistributionService;
        private readonly IUserMenuService _userMenuService;
        private readonly IUserService _userService;
        private readonly IOrganizationService _organizationService;
        private readonly CommonHelper _commonHelper;
        private readonly IStudentImagesMediaService _studentImagesMediaService;
        private readonly IServiceBlockService _serviceBlockService;
        // private readonly IProgramBranchSessionService _programBranchSessionService;


        #endregion


        //private IList<ProgramBranchSession> _programBranchSessionList;
        private List<UserMenu> _userMenu;

        #endregion

        #region Initialize Controller

        public MaterialDistributionController()
        {
            #region Open Session

            var nHSession = NHibernateSessionFactory.OpenSession();

            #endregion

            #region Services

            _batchService = new BatchService(nHSession);

            _branchService = new BranchService(nHSession);
            _campusService = new CampusService(nHSession);

            _sessionService = new SessionService(nHSession);
            _programService = new ProgramService(nHSession);
            _materialService = new MaterialService(nHSession);
            _materialTypeService = new MaterialTypeService(nHSession);
            _studentProgramService = new StudentProgramService(nHSession);
            _materialDistributionService = new MaterialDistributionService(nHSession);
            _userMenuService = new UserMenuService(nHSession);
            _userService = new UserService(nHSession);
            _organizationService = new OrganizationService(nHSession);
            _commonHelper = new CommonHelper();
            _studentImagesMediaService = new StudentImagesMediaService();
            _serviceBlockService = new ServiceBlockService(nHSession);
            
            // _programBranchSessionService = new ProgramBranchSessionService(nHSession);



            #endregion
            // _programBranchSessionList = new List<ProgramBranchSession>();
        }


        #endregion

        #region Index/ Manage Material Distribution

        // GET: Student/MaterialDistribution
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        private bool UserAuthenticationCheck(List<long> programIdList, List<long> branchIdList, long userBranchId, long userProgramId)
        {
            if (branchIdList != null)
            {
                bool isBranchSelect = branchIdList.Contains(userBranchId);
                //bool isBranchSelect = Array.Exists(branchIdList, item => item == userBranchId);
                if (!isBranchSelect)
                    return false;
            }
            if (programIdList != null)
            {

                bool isProgramSelect = programIdList.Contains(userProgramId);
                //bool isProgramSelect = Array.Exists(programIdList, item => item == userProgramId);
                if (!isProgramSelect)
                    return false;
            }
            return true;
        }

        #region Material Distribution

        private void InitializeMaterialDistribution()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;

            #region get organization

            ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu),"Id","ShortName");
            #endregion

            #region Get Authorize Session

            //var sessionList = _sessionService.LoadAuthorizedSession(_userMenu);
            //var sessionSelectList = new SelectList(sessionList, "Id", "Name");
            //ViewBag.Session = sessionSelectList;
            ViewBag.Session = new SelectList(new List<Session>(),"Id","Name");

            #endregion

            #region Initialize Program

            //var programList = _programService.LoadAuthorizedProgram(_userMenu);
            //var programSelectList = new SelectList(programList, "Id", "Name");
            //ViewBag.Program = programSelectList;
            ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");

            #endregion

            #region Initialize Material

            ViewBag.Material = new SelectList(new List<Material>(), "Id", "Name");


            #endregion

        }

        public ActionResult MaterialDistribution(string errorMsg)
        {
            if (!String.IsNullOrWhiteSpace(errorMsg))
            {
                ViewBag.ErrorMessage = errorMsg;
            }
            ViewBag.LastPrn = "";
            InitializeMaterialDistribution();

            MaterialDistribution materialDistributionView = new MaterialDistribution();

            return View(materialDistributionView);
            //return View();
        }

        [HttpPost]
        public ActionResult MaterialDistribution(long? sessionId, long? programId, long[] materialIds, string studentRoll)
        {
            try
            {
                InitializeMaterialDistribution();
                if (programId == null || programId == 0)
                {
                    ViewBag.ErrorMessage = "You need to select program.";
                    return View("MaterialDistribution");
                }
                if (sessionId == null || sessionId == 0)
                {
                    ViewBag.ErrorMessage = "You need to select session.";
                    return View("MaterialDistribution");
                }
                if (materialIds == null)
                {
                    return RedirectToAction("MaterialDistribution", new { errorMsg = "You need to select materials." });
                }

                if (studentRoll.IsNullOrWhiteSpace())
                {
                    ViewBag.ErrorMessage = "Empty or invalid entry detect in the student program roll number.";
                    return View("MaterialDistribution");
                }
                string[] splitedRoll = studentRoll.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                
                var allExistsMaterialWithPrn = "";
                var notmatchPrn = "";
                var bStudentPrn = "";
                var programSessionNotMatchPrn = new List<string>();
                var blockStudent = new List<string>();
                int errorCount = 0;
                int successCount = 0;
                List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                List<long> programIdList = AuthHelper.LoadProgramIdList(_userMenu);
                
                foreach (var checRoll in splitedRoll)
                {
                    StudentProgram studentProgramCheck = _studentProgramService.GetStudentProgram(checRoll);
                    var existsMaterials = "";
                    if (studentProgramCheck != null)
                    {
                        var checkBlocked = _serviceBlockService.IsBlocked(studentProgramCheck.Program.Id, studentProgramCheck.Batch.Session.Id, (int)BlockService.MaterialDistribution, studentProgramCheck);
                        if (!checkBlocked)
                        {
                            IList<Material> alreadyExistsMaterials = new List<Material>();
                            long userBranchId = studentProgramCheck.Batch.Branch.Id;
                            long userProgramId = studentProgramCheck.Program.Id;
                            bool isChecked = UserAuthenticationCheck(programIdList, branchIdList, userBranchId,
                                userProgramId);
                            if (!isChecked)
                            {
                                errorCount++;
                                programSessionNotMatchPrn.Add(checRoll);
                            }
                            else if (studentProgramCheck.Program.Id == programId &&
                                     studentProgramCheck.Batch.Session.Id == sessionId)
                            {
                                if (materialIds != null && materialIds.Count() > 0)
                                {
                                    IList<Material> assignMaterials = new List<Material>();
                                    bool checkExists = false;
                                    foreach (var checkMaterials in studentProgramCheck.Program.Materials)
                                    {
                                        foreach (var i in materialIds)
                                        {
                                            if (checkMaterials.Id == i)
                                            {
                                                if (studentProgramCheck.Materials != null &&
                                                    studentProgramCheck.Materials.Count > 0)
                                                {
                                                    if (checkExists != true)
                                                    {
                                                        foreach (
                                                            var existingMaterial in studentProgramCheck.Materials)
                                                        {
                                                            checkExists = true;
                                                            assignMaterials.Add(existingMaterial);
                                                            alreadyExistsMaterials.Add(existingMaterial);
                                                        }
                                                    }
                                                }
                                                assignMaterials.Add(checkMaterials);
                                                break;
                                            }
                                        }
                                    }

                                    studentProgramCheck.Materials = assignMaterials;
                                    bool isSucsess = _studentProgramService.Save(studentProgramCheck);
                                    if (isSucsess)
                                    {
                                        successCount++;
                                        if (alreadyExistsMaterials.Count > 0)
                                        {
                                            alreadyExistsMaterials = alreadyExistsMaterials.Distinct().ToList();
                                            foreach (var material in alreadyExistsMaterials)
                                            {
                                                existsMaterials += material.Name + ",";
                                            }
                                            existsMaterials = existsMaterials.Remove(existsMaterials.Length - 1);
                                            allExistsMaterialWithPrn += checRoll + "(" + existsMaterials + ")" +
                                                                        Environment.NewLine;
                                        }
                                    }
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = "Need to check materials";
                                }
                            }
                            else
                            {
                                errorCount++;
                                programSessionNotMatchPrn.Add(checRoll);
                            }
                        }
                        else
                        {
                            blockStudent.Add(checRoll);
                        }

                    }
                    else
                    {
                        errorCount++;
                        programSessionNotMatchPrn.Add(checRoll);
                       
                    }
                }

                #region old logic
                //foreach (var checRoll in splitedRoll)
                //{
                //    StudentProgram studentProgramCheck = _studentProgramService.LoadByPrnNo(checRoll);
                //    if (studentProgramCheck != null)
                //    {
                //        if (studentProgramCheck.Program.Id == programId && studentProgramCheck.Batch.Session.Id == sessionId)
                //        {
                //            if (materialIds != null && materialIds.Count() > 0)
                //            {
                //                IList<Material> assignMaterials = new List<Material>();
                //                foreach (var checkMaterials in studentProgramCheck.Program.Materials)
                //                {
                //                    foreach (var i in materialIds)
                //                    {
                //                        if (checkMaterials.Id == i)
                //                        {
                //                            if (studentProgramCheck.Materials != null && studentProgramCheck.Materials.Count > 0)
                //                            {
                //                                foreach (var existingMaterial in studentProgramCheck.Materials)
                //                                {
                //                                    assignMaterials.Add(existingMaterial);
                //                                }
                //                            }
                //                            assignMaterials.Add(checkMaterials);
                //                            break;
                //                        }
                //                    }
                //                }

                //                studentProgramCheck.Materials = assignMaterials;
                //                isSucsess = _studentProgramService.Save(studentProgramCheck);
                //            }
                //            else
                //            {
                //                ViewBag.ErrorMessage = "Need to check materials";
                //            }
                //        }
                //        else
                //        {
                //            programSessionNotMatchPrn.Add(checRoll);

                //        }

                //    }
                //    else
                //    {
                //        //studentProgramCheck = new StudentProgram();
                //        wrongPrnNoContainer += checRoll + " ,";
                //        wrongPrnNo.Add(checRoll);

                //        //if (studentProgramCheck.PrnNo == null)
                //        //  wrongPrnNo.Add(studentProgramCheck.PrnNo.ToString());
                //    }

                //}

                #endregion
                if (successCount>0)
                    ViewBag.SuccessMessage = "Material successfully distribute(" + successCount + ")";
                if (errorCount>0)
                    ViewBag.ErrorMessage = "Invalid program roll(" + errorCount + ")";
                if (allExistsMaterialWithPrn.Any() )
                    ViewBag.WarningMessage = allExistsMaterialWithPrn;

                if (programSessionNotMatchPrn.Count > 0)
                {
                    foreach (var notPrn in programSessionNotMatchPrn)
                    {
                        notmatchPrn += notPrn + "\n";
                        ViewBag.LastPrn = notPrn;
                    }
                    notmatchPrn = notmatchPrn.Remove(notmatchPrn.Length - 1);
                    ViewBag.NotMatchprn = notmatchPrn;
                }
                if (blockStudent.Count > 0)
                {
                    bStudentPrn = blockStudent.Aggregate(bStudentPrn, (current, bSt) => current + (bSt + "\n"));
                    ViewBag.BlockStudent = bStudentPrn;
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred during material distribution.";
                _logger.Error(ex);
            }
            //MaterialDistribution materialDistributionView = new MaterialDistribution();
            return Json(new { successMsg = ViewBag.SuccessMessage,errorMsg=ViewBag.ErrorMessage,warningMsg=ViewBag.WarningMessage,
                              notMatchPrn = ViewBag.NotMatchprn,blockStudent= ViewBag.BlockStudent,lastPrn = ViewBag.LastPrn,
                              isSuccess = true
            });
            //return View();
            //return View(materialDistributionView);
        }

        private ActionResult InitializeMaterialDistributionViewReturn(string errorMsg)
        {
            ViewBag.ErrorMessage = errorMsg;
            InitializeMaterialDistribution();
            MaterialDistribution mdv = new MaterialDistribution();

            return View("MaterialDistribution", mdv);
        }


        #region Ajax Request Function

        #region Get Session

        [HttpPost]
        public ActionResult GetProgram(long programId)
        {
            try
            {
                //for authorization
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //load program list
                var sessionList = _sessionService.LoadAuthorizedSession(userMenu,null,_commonHelper.ConvertIdToList(programId));
                //var programList = _programService.LoadAuthorizedProgram(userMenu, sessionId);
                if (sessionList != null)
                {
                    //get the distinct result
                    //var distinctProgramList = programList.Distinct();
                    ViewBag.SessionList = sessionList;
                    return View("_ProgramList");
                }
                else
                {
                    //return Json("Program Not Found");
                    return Json(new Response(false, "session not found"));
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred. " + ex.Message;

            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }

        #endregion

        #endregion

        #region Get Materials Using Session Program

        [HttpPost]
        public ActionResult GetMaterialsBySessionProgram(long sessionId, long programId)
        {
            try
            {
                var materialList = _materialService.LoadMaterial(sessionId, programId);

                if (materialList != null && materialList.Count > 0)
                {
                    MaterialDistribution materialDistributionView = new MaterialDistribution();

                    // materialDistributionView.Materials = materialList;
                    ViewBag.MaterialList = materialList;

                    return View("_MaterialList", materialDistributionView);

                }
                //return Json(new Response(false, WebHelper.CommonErrorMessage));
                return Json(new Response(false, "No records found"));


            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred. " + ex.Message;

            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }

        #endregion

        #region Get Materials Using MaterialType Program Session
        [HttpPost]
        public JsonResult GetMaterialByMaterialTypeProgramSession(long[] materialTypeId, long programId, long sessionId)
        {

            try
            {
                IList<Material> materialList = _materialService.LoadMaterial(materialTypeId, programId, sessionId);
                Material materialObj = new Material();
                materialObj.Id = -200;
                materialObj.Name = "All Material";

                materialList.Insert(0, materialObj);

                var materialSelectList = new SelectList(materialList, "Id", "Name");
                ViewBag.SelectedMaterial = materialSelectList;

                return Json(materialSelectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Problem Occurred";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }

        #endregion
        #endregion
        
        #region Material Distribution Update
        [HttpGet]
        public ActionResult MaterialDistributionUpdate()
        {
            //var materialList = _materialService.LoadActive();
            //ViewBag.Material = materialList;

            return View();
        }

        [HttpPost]
        public ActionResult MaterialDistributionUpdate(StudentProgramView StudentProgram_PrnNo, long[] materialIds)
        {
            StudentProgramView studentProgramView = new StudentProgramView();
            try
            {
                #region OLD CODE
                studentProgramView.StudentProgram = _studentProgramService.GetStudentProgram(StudentProgram_PrnNo.StudentProgram.PrnNo);


                if (studentProgramView.StudentProgram != null)
                {
                    var whichBlocked = _serviceBlockService.WhichBlocked(studentProgramView.StudentProgram.Program.Id, studentProgramView.StudentProgram.Batch.Session.Id, (int)BlockService.MaterialDistribution, studentProgramView.StudentProgram);
                    var checkBlocked = _serviceBlockService.IsBlocked(studentProgramView.StudentProgram.Program.Id,studentProgramView.StudentProgram.Batch.Session.Id, (int)BlockService.MaterialDistribution, studentProgramView.StudentProgram);
                    IList<Material> assignMaterials = new List<Material>();
                    if (!checkBlocked)
                    {
                        foreach (var checkMaterials in studentProgramView.StudentProgram.Program.Materials)
                        {
                            foreach (var i in materialIds)
                            {
                                if (checkMaterials.Id == i)
                                {
                                    assignMaterials.Add(checkMaterials);

                                }
                            }
                        }
                        studentProgramView.StudentProgram.Materials = assignMaterials;
                        _studentProgramService.Save(studentProgramView.StudentProgram);
                        ViewBag.SuccessMessage = "Material Details Updated.";
                        TempData["MaterialDistributionSuccessMessage"] = "Material Details Updated.";
                    }
                    else
                    {
                        TempData["MaterialDistributionErrorMessage"] = whichBlocked + " blocked for this service";
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                _logger.Error("Problem Occurred in Material Distribution Check", ex);
                TempData["MaterialDistributionErrorMessage"] = "Problem Occurred in Material Distribution Check";
              
            }
            //return View("MaterialDistributionChecking");
            return RedirectToAction("MaterialDistributionChecking");
        }

        #endregion

        #region Material Distribution Check
        [HttpGet]
        public ActionResult MaterialDistributionChecking(string prnNo, string errorMsg)
        {
            if (!String.IsNullOrWhiteSpace(errorMsg))
                ViewBag.ErrorMessage = errorMsg;
            return View();
        }

        [HttpPost]
        public ActionResult MaterialDistributionChecking(string prnNo)
        {
            StudentProgramView studentProgramView = new StudentProgramView();
            try
            {
                if (prnNo.IsNullOrWhiteSpace())
                {
                    return RedirectToAction("MaterialDistributionChecking", new { errorMsg = "Program roll no couldn't empty!." });
                }

                if (prnNo.Length.ToString() != Constants.PrnNoLength)
                {
                    return RedirectToAction("MaterialDistributionChecking", new { errorMsg = "Program roll number would be " + Constants.PrnNoLength + " digits" });
                }

                studentProgramView.StudentProgram = _studentProgramService.GetStudentProgram(prnNo);

                if (studentProgramView.StudentProgram == null)
                {
                    return RedirectToAction("MaterialDistributionChecking", new { prnNo = prnNo, errorMsg = "Program roll number is not matching with any student." });
                }

                var checkBlocked = _serviceBlockService.IsBlocked(studentProgramView.StudentProgram.Program.Id, studentProgramView.StudentProgram.Batch.Session.Id, (int)BlockService.StudentTransfer, studentProgramView.StudentProgram);
                ViewBag.CheckBlocked = checkBlocked;
                
                //if (!checkBlocked)
                //{
                var lastOrDefault = (from a in studentProgramView.StudentProgram.StudentImage select a).LastOrDefault();
                if (lastOrDefault != null)
                {
                    StudentMediaImage mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(lastOrDefault.Id);
                    studentProgramView.StudentImage = mediaStudentImages != null ? mediaStudentImages.Images : new byte[0];
                }
                //}
                //else
                //{
                //    var whichBlocked = _serviceBlockService.WhichBlocked(studentProgramView.StudentProgram.Program.Id, studentProgramView.StudentProgram.Batch.Session.Id, (int)BlockService.StudentTransfer, studentProgramView.StudentProgram);
                //    return RedirectToAction("MaterialDistributionChecking", new { prnNo = prnNo, errorMsg = whichBlocked + "  blocked for this service;" });
                //}

                #region old code
                //if (studentProgramView.StudentProgram.StudentImage != null)
                //{
                //    var lastOrDefault = (from a in studentProgramView.StudentProgram.StudentImage select a).LastOrDefault();
                //    if (lastOrDefault != null)
                //    {
                //        StudentMediaImage mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(lastOrDefault.Id);
                //        studentProgramView.StudentImage = mediaStudentImages != null ? mediaStudentImages.Images : new byte[0];
                //    }
                //}
                #endregion

                #region Student Course Details

                studentProgramView.StudentCourseDetail = studentProgramView.StudentProgram.StudentCourseDetails;
                string courseName = "";
                List<long> existCourseName = new List<long>();
                foreach (var courseSubjectInfo in studentProgramView.StudentCourseDetail)
                {

                    if (courseSubjectInfo.CourseSubject.Course != null)
                    {
                        bool displayCourse = false;
                        if (!existCourseName.Contains(courseSubjectInfo.CourseSubject.Course.Id))
                        {
                            displayCourse = true;
                            existCourseName.Add(courseSubjectInfo.CourseSubject.Course.Id);
                        }
                        if (displayCourse)
                        {
                            courseName += courseSubjectInfo.CourseSubject.Course.Name + " ,";
                        }
                    }

                }
                courseName = courseName.Remove(courseName.Length - 1);
                studentProgramView.CourseName = courseName;

                #endregion
                
                if (studentProgramView.StudentProgram.Materials.Count == 0 || studentProgramView.StudentProgram.Materials == null)
                {
                    return RedirectToAction("MaterialDistributionChecking", new { errorMsg = "Material details not found! " + " Program roll no: " + prnNo });
                }
                
                var materialTypeList = _materialTypeService.LoadMaterialType(studentProgramView.StudentProgram.Batch.Session.Id,
                    studentProgramView.StudentProgram.Program.Id);
                ViewBag.Material = materialTypeList;

                //if (studentProgramView.StudentProgram.Materials != null && studentProgramView.StudentProgram.Materials.Count > 0)
                //{
                //    return View("MaterialDistributionUpdate", studentProgramView);
                //}
                
                return View("MaterialDistributionUpdate", studentProgramView);


            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred while material distribution is checking";
            }


            return View();
        }


        #endregion

        #region Material Distribution Report

        #region GetMaterial List

        /*initialize Get Material List*/
        [HttpGet]
        public ActionResult GetMaterialList()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName"); 
            #region Get Authorize Program

            //program list will be filter using branchId and sessionId
            //var programList = _programService.LoadAuthorizedProgram(_userMenu);
            //var programSelectList = new SelectList(programList, "Id", "Name");
            ViewBag.Program = new SelectList(new List<Program>(),"Id","Name");

            #endregion


            #region Initialize Session Branch Campus

            ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");

            #endregion


            #region Branch

            //var branchList = _branchService.LoadActive();

            //Branch branchObj = new Branch();
            //branchObj.Id = SelectionType.SelelectAll;
            //branchObj.Name = "All Branch";
            //branchList.Insert(0, branchObj);
            //var selectBranchList = new SelectList(branchList, "Id", "Name");
            //ViewBag.SelectedBranch = selectBranchList;


            #endregion


            #region Initialize Material Type/ Material Status/ Payment Status

            var materialTypeList = _materialTypeService.LoadMaterialType();
            var materialType = new MaterialType();
            materialType.Id = SelectionType.SelelectAll;
            materialType.Name = "All Type";
            materialTypeList.Insert(0, materialType);
            var materialTypeSelectList = new SelectList(materialTypeList, "Id", "Name");
            ViewBag.SelectedMaterialType = materialTypeSelectList;


            ViewBag.SelectedMaterial = new SelectList(new List<Material>(), "Id", "Name");

            var paymentStatus = new List<SelectListItem>()
            {
                new SelectListItem(){Value = "-200",Text = "All", Selected = true},
                new SelectListItem(){Value = SelectionType.IsPaid.ToString(),Text = "Paid"},
                new SelectListItem(){Value = SelectionType.IsDue.ToString(),Text = "Due"}
            };

            var materialStatus = new List<SelectListItem>()
            {
                new SelectListItem(){Value = "-200",Text = "All", Selected = true},
                new SelectListItem(){Value = SelectionType.IsMaterialProvided.ToString(),Text = "Provided"},
                new SelectListItem(){Value = SelectionType.IsNotMaterialProvided.ToString(),Text = "Not Provided"}
            };



            #endregion

            //MaterialFilterView materialFilterView = new MaterialFilterView();
            var msInfo = new MaterialSearchInfo();
            msInfo.PaymentStatusList = paymentStatus;
            msInfo.MaterialStatusList = materialStatus;
            msInfo.PaymentStatus = -200;
            msInfo.MaterialStatus = -200;

            return View(msInfo);
        }

        [HttpGet]
        public ActionResult GenerateMaterialListReport()
        {
            return RedirectToAction("GetMaterialList");
        }

        [HttpPost]
        public ActionResult GenerateMaterialListReport(MaterialSearchInfo materialSearchInfo)
        {
            try
            {
                var program = _programService.GetProgram(materialSearchInfo.ProgramId);
                var session = _sessionService.LoadById(materialSearchInfo.SessionId);
                var batchName = "ALL";
                var branchName = "ALL";
                var batchDays = "ALL";
                var campusName = "ALL";

                if (!materialSearchInfo.SelectedBatchDays.Contains("0"))
                {

                    batchDays = string.Join("; ", materialSearchInfo.SelectedBatchDays);
                }

                if (!materialSearchInfo.SelectedBatch.Contains(0))
                {
                    IList<Batch> batches = _batchService.LoadBatch(materialSearchInfo.SelectedBatch);
                    batchName = string.Join(", ", batches.Select(x => x.Name.Trim()).Distinct().ToList());
                    
                }

                if (!materialSearchInfo.SelectedBranch.Contains(0))
                {
                    IList<Branch> branches = _branchService.LoadBranch(materialSearchInfo.SelectedBranch);
                    branchName = string.Join(", ", branches.Select(x => x.Name.Trim()).Distinct().ToList());
                }


                if (!materialSearchInfo.SelectedCampus.Contains(0))
                {
                    IList<Campus> campuses = _campusService.LoadCampus(materialSearchInfo.SelectedCampus);
                    campusName = string.Join(", ", campuses.Select(x => x.Name.Trim()).Distinct().ToList()); 
                }


                var json = new JavaScriptSerializer().Serialize(materialSearchInfo);
                ViewData["materialSearchInfo"] = json;
                ViewBag.ProgramSession = program.Name + "-" + session.Name;
                ViewBag.Branch = branchName;
                ViewBag.BatchName = batchName;
                ViewBag.BatchDays = batchDays;
                ViewBag.CampusName = campusName;
                return View("MaterialDistributionReport", materialSearchInfo);
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }
        
        [HttpPost]
        public ActionResult GetMaterialList(int draw, int start, int length, string materialSearchInfoObj)
        {
            var materialSearchInfo = new JavaScriptSerializer().Deserialize<MaterialSearchInfo>(materialSearchInfoObj);
           
            #region previous without data table
            //_userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //#region

            //IList<StudentProgram> studentProgramList = new List<StudentProgram>();
            //var materialReportList = new List<MaterialReports>();
            //try
            //{
            //    if (materialSearchInfo != null)
            //    {

            //        IList<Material> materialList = new List<Material>();
            //        long[] materials;
            //        if (Array.Exists(materialSearchInfo.SelectedMaterial, item => item == -200))
            //        {
            //            materialList = _materialService.LoadMaterial(materialSearchInfo.SelectedMaterialType,materialSearchInfo.ProgramId, materialSearchInfo.SessionId);
            //            materials = materialList.Select(x => x.Id).ToArray();
            //        }
            //        else
            //        {
            //            materials = materialSearchInfo.SelectedMaterial;
            //        }

            //        studentProgramList = _materialDistributionService.LoadStudentProgramByCriteria(_userMenu,materialSearchInfo.ProgramId, materialSearchInfo.SessionId, materialSearchInfo.SelectedBranch
            //       , materialSearchInfo.SelectedCampus, materialSearchInfo.SelectedBatchDays, materialSearchInfo.SelectedBatchTime, materialSearchInfo.SelectedBatch, materialSearchInfo.SelectedMaterialType
            //       , materials, materialSearchInfo.MaterialStatus, materialSearchInfo.PaymentStatus, materialSearchInfo.InformationViewList);

            //    }
            //    //responsible for report header
            //    var firstStudentProgram = studentProgramList.FirstOrDefault();
            //    var headerTitle = firstStudentProgram.Program.Name + " " + firstStudentProgram.Batch.Session.Name;

            //    if (materialSearchInfo != null)
            //    {
            //        ViewBag.sSessionId = materialSearchInfo.SessionId;
            //        ViewBag.sProgramId = materialSearchInfo.ProgramId;

            //        ViewBag.ReportHeadProgramSession = headerTitle;


            //            IList<Material> materialList = firstStudentProgram.Program.Materials.ToList();
            //            ViewBag.HeaderMaterialType = materialList;
            //            var mt = new MaterialReports
            //            {
            //                SudentProgramDetailsList = new List<StudentProgramDetails>(),
            //                BranchName = (Array.Exists(materialSearchInfo.SelectedBranch, item => item == 0)) ? "All" : string.Join(", ",_branchService.LoadBranch(materialSearchInfo.SelectedBranch).Select(x=>x.Name).ToList()),
            //                CampusName = (Array.Exists(materialSearchInfo.SelectedCampus, item => item == 0)) ? "All" : string.Join(", ", _campusService.LoadCampus(materialSearchInfo.SelectedCampus).Select(x => x.Name).ToList()),
            //                BatchName = (Array.Exists(materialSearchInfo.SelectedBatch, item => item == 0)) ? "All" : string.Join(", ", _batchService.LoadBatch(materialSearchInfo.SelectedBatch).Select(x => x.Name).ToList()),
            //                BatchDays = (Array.Exists(materialSearchInfo.SelectedBatchDays, item => item == "0")) ? "All" : string.Join("; ", materialSearchInfo.SelectedBatchDays)
            //            };

            //            foreach (StudentProgram sp in studentProgramList.Distinct())
            //            {
            //                var spd = new StudentProgramDetails();
            //                var str = new List<string>();

            //                spd.StudentMaterial = new List<StudentMaterial>();
            //                if (materialSearchInfo.InformationViewList != null)
            //                {
            //                    var info = materialSearchInfo.InformationViewList;
            //                    foreach (string displayInfo in info)
            //                    {
            //                        #region Display info
            //                        switch (displayInfo)
            //                        {
            //                            case "Program Roll":
            //                                str.Add("PRN :" + sp.PrnNo);
            //                                break;
            //                            case "StudentName":
            //                                if (sp.Student != null && sp.Student.NickName != null)
            //                                {
            //                                    str.Add("Name :" + sp.Student.NickName);
            //                                    break;
            //                                }
            //                                else
            //                                {
            //                                    str.Add("");
            //                                    break;
            //                                }
            //                            case "Mobile No":
            //                                if (sp.Student != null && sp.Student.Mobile != null)
            //                                {
            //                                    str.Add("Mobile No :" + sp.Student.Mobile);
            //                                    break;
            //                                }
            //                                else
            //                                {
            //                                    str.Add("");
            //                                    break;
            //                                }
            //                            case "Due Payment":
            //                                if (sp.DueAmount != null)
            //                                {
            //                                    str.Add("Due :" + sp.DueAmount);
            //                                    break;
            //                                }
            //                                else
            //                                {
            //                                    str.Add("Due :0.00");
            //                                    break;
            //                                }
            //                            case "FatherName":
            //                                if (sp.Student != null && sp.Student.FatherName != null)
            //                                {
            //                                    str.Add("Father's Name :" + sp.Student.FatherName);
            //                                    break;
            //                                }
            //                                else
            //                                {
            //                                    str.Add("");
            //                                    break;
            //                                }
            //                            #region old code
            //                            //case "Father's Occupation":
            //                            //    str.Add("---");
            //                            //    break;

            //                            //case "Mother's Occupation":
            //                            //    str.Add("---");
            //                            //    break;

            //                            //case "Institute":
            //                            //    if (sp.Student != null && sp.Student.StudentAcademicInfos != null && sp.Student.StudentAcademicInfos.Count > 0)
            //                            //    {
            //                            //        str.Add("Institute :" + sp.Student.StudentAcademicInfos.LastOrDefault().Institute.Name);
            //                            //        break;
            //                            //    }
            //                            //    else
            //                            //    {
            //                            //        str.Add("Institute :Empty");
            //                            //        break;
            //                            //    }
            //                            //case "Branch":
            //                            //    if (sp.Batch != null && sp.Batch.Branch != null)
            //                            //    {
            //                            //        str.Add("Branch :" + sp.Batch.Branch.Name);
            //                            //        break;
            //                            //    }
            //                            //    else
            //                            //    {
            //                            //        str.Add("Branch :Empty");
            //                            //        break;
            //                            //    }
            //                            //case "Batch":
            //                            //    if (sp.Batch != null && sp.Batch.Name != null)
            //                            //    {
            //                            //        str.Add("Batch :" + sp.Batch.Name);
            //                            //        break;
            //                            //    }
            //                            //    else
            //                            //    {
            //                            //        str.Add("Batch :Empty");
            //                            //        break;
            //                            //    }
            //                            #endregion
            //                            case "Email":
            //                                if (sp.Student != null && sp.Student.Email != null)
            //                                {
            //                                    str.Add("Email:" + sp.Student.Email);
            //                                    break;
            //                                }
            //                                else
            //                                {
            //                                    str.Add("");
            //                                    break;
            //                                }

            //                            default:
            //                                break;
            //                        }
            //                        #endregion
            //                    }
            //                    spd.StudentInfo = str.ToArray();
            //                }
            //                else
            //                {
            //                    spd.StudentInfo = new string[]
            //                                      {
            //                                          "PRN :" + sp.PrnNo,
            //                                          "Name :" + sp.Student.NickName,
            //                                          "Due :"+sp.DueAmount
            //                                      };
            //                }
            //                foreach (Material m in sp.Program.Materials)
            //                {
            //                    var sm = new StudentMaterial
            //                             {
            //                                 Item = m.ShortName,
            //                                 ItemId = m.Id,
            //                                 ItemTypeId = m.MaterialType.Id,
            //                                 IsDistributed = false
            //                             };

            //                    sm.TotalMaterialCount = 0;
            //                    if (sp.Materials != null && sp.Materials.Count > 0)
            //                    {
            //                        foreach (Material given in sp.Materials)
            //                        {
            //                            if (m.Id == given.Id)
            //                            {
            //                                sm.IsDistributed = true;
            //                                sm.TotalMaterialCount += 1;
            //                                //break;
            //                            }
            //                        }
            //                    }
            //                    spd.StudentMaterial.Add(sm);
            //                }
            //                mt.SudentProgramDetailsList.Add(spd);
            //            }
            //            materialReportList.Add(mt); 

            //    }
            //}
            //catch (Exception)
            //{
            //    ViewBag.ErrorMessage = "Problem Occurred";
            //}
            //var mrl = new MaterialReportList { MaterialReportses = materialReportList, MaterialSearchInfos= new[] {materialSearchInfo}};

            //return View("MaterialDistributionReport", mrl);
            //#endregion 
            #endregion

            _userMenu = (List<UserMenu>)ViewBag.UserMenu;



            IList<string> studentProgramList = new List<string>();
            var materialReportList = new List<MaterialReports>();

            
            IList<Material> materialLists = new List<Material>();
            long[] materials;
            if (Array.Exists(materialSearchInfo.SelectedMaterial, item => item == -200))
            {
                materialLists = _materialService.LoadMaterial(materialSearchInfo.SelectedMaterialType, materialSearchInfo.ProgramId, materialSearchInfo.SessionId);
                materials = materialLists.Select(x => x.Id).ToArray();
            }
            else
            {
                materials = materialSearchInfo.SelectedMaterial;
            }
            //int recordsTotal = _materialDistributionService.GetStudentProgramCount(_userMenu, materialSearchInfo.OrganizationId, materialSearchInfo.ProgramId, materialSearchInfo.SelectedBranch, materialSearchInfo.SessionId, materialSearchInfo.SelectedCampus, materialSearchInfo.SelectedBatchDays, materialSearchInfo.SelectedBatchTime, materialSearchInfo.SelectedBatch, materialSearchInfo.SelectedMaterialType, materials, materialSearchInfo.MaterialStatus, materialSearchInfo.PaymentStatus);

            int recordsTotal = _materialDistributionService.GetStudentProgramCount(start, length, _userMenu, materialSearchInfo.ProgramId, materialSearchInfo.SessionId, materialSearchInfo.SelectedBranch, materialSearchInfo.SelectedCampus, materialSearchInfo.SelectedBatchDays, materialSearchInfo.SelectedBatchTime, materialSearchInfo.SelectedBatch, materialSearchInfo.SelectedMaterialType, materials, materialSearchInfo.MaterialStatus, materialSearchInfo.PaymentStatus, materialSearchInfo.InformationViewList);
            int recordsFiltered = recordsTotal;

            //studentProgramList = _materialDistributionService.LoadStudentProgramByCriteria(_userMenu, start, length, materialSearchInfo.OrganizationId, materialSearchInfo.ProgramId, materialSearchInfo.SelectedBranch, materialSearchInfo.SessionId, materialSearchInfo.SelectedCampus, materialSearchInfo.SelectedBatchDays, materialSearchInfo.SelectedBatchTime, materialSearchInfo.SelectedBatch, materialSearchInfo.SelectedMaterialType, materials, materialSearchInfo.MaterialStatus, materialSearchInfo.PaymentStatus);

            studentProgramList = _materialDistributionService.LoadStudentProgramByCriteria(start, length, _userMenu, materialSearchInfo.ProgramId, materialSearchInfo.SessionId, materialSearchInfo.SelectedBranch, materialSearchInfo.SelectedCampus, materialSearchInfo.SelectedBatchDays, materialSearchInfo.SelectedBatchTime, materialSearchInfo.SelectedBatch, materialSearchInfo.SelectedMaterialType, materials, materialSearchInfo.MaterialStatus, materialSearchInfo.PaymentStatus, materialSearchInfo.InformationViewList);

            var firstStudentProgram =_studentProgramService.GetStudentProgram(studentProgramList.FirstOrDefault());
            IList<Material> materialList = firstStudentProgram.Program.Materials.ToList();
            var mt = new MaterialReports
            {
                SudentProgramDetailsList = new List<StudentProgramDetails>(),
            };
            foreach (var spId in studentProgramList.Distinct())
            {
                var spd = new StudentProgramDetails();
                var str = new List<string>();
                var sp = _studentProgramService.GetStudentProgram(spId);
                spd.StudentMaterial = new List<StudentMaterial>();
                //if (informationViewList != null)
                if (!Array.Exists(materialSearchInfo.InformationViewList, item => item == ""))
                {
                    var info = materialSearchInfo.InformationViewList;

                    foreach (string displayInfo in info)
                    {
                        #region Display info
                        switch (displayInfo)
                        {
                            case "Program Roll":
                                str.Add("PRN :" + sp.PrnNo);
                                break;
                            case "StudentName":
                                if (sp.Student != null && sp.Student.NickName != null)
                                {
                                    str.Add("Name :" + sp.Student.NickName);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Mobile No":
                                if (sp.Student != null && sp.Student.Mobile != null)
                                {
                                    str.Add("Mobile No :" + sp.Student.Mobile);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Due Payment":
                                if (sp.DueAmount != null)
                                {
                                    str.Add("Due :" + sp.DueAmount);
                                    break;
                                }
                                else
                                {
                                    str.Add("Due :0.00");
                                    break;
                                }
                            case "FatherName":
                                if (sp.Student != null && sp.Student.FatherName != null)
                                {
                                    str.Add("Father's Name :" + sp.Student.FatherName);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }

                            case "Email":
                                if (sp.Student != null && sp.Student.Email != null)
                                {
                                    str.Add("Email:" + sp.Student.Email);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }

                            default:
                                break;
                        }
                        #endregion
                    }
                    spd.StudentInfo = str.ToArray();
                }
                else
                {
                    spd.StudentInfo = new string[]
                                                  {
                                                      "PRN :" + sp.PrnNo,
                                                      "Name :" + sp.Student.NickName,
                                                      "Due :"+sp.DueAmount
                                                  };
                }
                //
                foreach (Material m in sp.Program.Materials)
                {
                    var sm = new StudentMaterial
                    {
                        Item = m.ShortName,
                        ItemId = m.Id,
                        ItemTypeId = m.MaterialType.Id,
                        IsDistributed = false
                    };

                    sm.TotalMaterialCount = 0;
                    if (sp.Materials != null && sp.Materials.Count > 0)
                    {
                        foreach (Material given in sp.Materials)
                        {
                            if (m.Id == given.Id)
                            {
                                sm.IsDistributed = true;
                                sm.TotalMaterialCount += 1;
                                //break;
                            }
                        }
                    }
                    spd.StudentMaterial.Add(sm);
                }
                mt.SudentProgramDetailsList.Add(spd);
            }
            materialReportList.Add(mt);


            try
            {
                var sl = start;
                var mDistributionList = new List<List<object>>();
                var materialTypeList = materialList.GroupBy(mat => mat.MaterialType, (materialType, material) => new { Key = materialType, materials = material.ToList() });
                materialTypeList = materialTypeList.Where(x => x.Key.Status == MaterialType.EntityStatus.Active).OrderBy(x => x.Key.Rank);
                foreach (MaterialReports mReports in materialReportList)
                {
                    foreach (StudentProgramDetails studentProgramDetails in mReports.SudentProgramDetailsList)
                    {
                        sl++;
                        var xlsRow = new List<object>();
                        xlsRow.Add(sl);
                        string sInfo = "  ";
                        foreach (var studentInfo in studentProgramDetails.StudentInfo)
                        {
                            sInfo += studentInfo + ", ";
                        }
                        xlsRow.Add(sInfo.Remove(sInfo.Length - 2));
                        if (materialList != null)
                        {
                            string provided = "  ";
                            foreach (var mTList in materialTypeList)
                            {
                                var materialListByProgramSession = mTList.materials.Where(x => x.Status == Material.EntityStatus.Active && x.Session.Id == materialSearchInfo.SessionId && x.Program.Id == materialSearchInfo.ProgramId).ToList();
                                if (materialListByProgramSession.Count > 0)
                                {
                                    string matList = "  ";
                                    foreach (var mList in materialListByProgramSession)
                                    {
                                        foreach (StudentMaterial xStudentMaterial in studentProgramDetails.StudentMaterial)
                                        {
                                            if (xStudentMaterial.ItemId == mList.Id)
                                            {
                                                if (xStudentMaterial.IsDistributed)
                                                {
                                                    matList += xStudentMaterial.Item + "(" + xStudentMaterial.TotalMaterialCount + "), ";
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    if (matList.Length > 2)
                                    {
                                        provided += mTList.Key.Name + ":" + matList.Remove(matList.Length - 2) + ", ";
                                    }
                                }
                                else { }
                            }
                            string notProvided = "  ";
                            foreach (var mTList in materialTypeList)
                            {
                                var materialListByProgramSession = mTList.materials.Where(x => x.Status == Material.EntityStatus.Active && x.Session.Id == materialSearchInfo.SessionId && x.Program.Id == materialSearchInfo.ProgramId).ToList();
                                if (materialListByProgramSession.Count > 0)
                                {
                                    string matListNot = "  ";
                                    foreach (var mList in materialListByProgramSession)
                                    {
                                        foreach (StudentMaterial xStudentMaterial in studentProgramDetails.StudentMaterial)
                                        {
                                            if (xStudentMaterial.ItemId == mList.Id)
                                            {
                                                if (xStudentMaterial.IsDistributed)
                                                { }
                                                else
                                                {
                                                    matListNot += xStudentMaterial.Item + ", ";
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    if (matListNot.Length > 2)
                                    {
                                        notProvided += mTList.Key.Name + ":" + matListNot.Remove(matListNot.Length - 2) + ", ";
                                    }
                                }
                                else { }
                            }
                            xlsRow.Add(provided.Remove(provided.Length - 2));
                            xlsRow.Add(notProvided.Remove(notProvided.Length - 2));
                            mDistributionList.Add(xlsRow);
                        }
                    }
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = mDistributionList
                });
            }
            catch (Exception ex)
            {
               _logger.Error(ex);
                throw ex;
            }

            
        }

        #endregion

        #region ExportMaterialDistributionList

        public ActionResult ExportMaterialDistributionList(long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchs, long[] materialTypeIds, long[] materialIds, long materialStatus, long paymentStatus, string[] informationViewList)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;



            IList<StudentProgram> studentProgramList = new List<StudentProgram>();
            var materialReportList = new List<MaterialReports>();

            IList<Material> materialLists = new List<Material>();
            long[] materials;
            if (Array.Exists(materialIds, item => item == -200))
            {
                materialLists = _materialService.LoadMaterial(materialTypeIds, programId, sessionId);
                materials = materialLists.Select(x => x.Id).ToArray();

            }
            else
            {
                materials = materialIds;
            }
            //studentProgramList = _materialDistributionService.LoadStudentProgramByCriteriaForExport(_userMenu, organizationId, programId, branchIds, sessionId, campusIds, batchDays, batchTimes, batchs, materialTypeIds, materials, materialStatus, paymentStatus);
            studentProgramList = _materialDistributionService.LoadStudentProgramByCriteriaForExport(0, 0, _userMenu, programId, sessionId, branchIds, campusIds, batchDays, batchTimes, batchs, materialTypeIds, materials, materialStatus, paymentStatus, informationViewList);

            var firstStudentProgram = studentProgramList.FirstOrDefault();

            IList<Material> materialList = firstStudentProgram.Program.Materials.ToList();
            var mt = new MaterialReports
            {
                SudentProgramDetailsList = new List<StudentProgramDetails>(),
                BranchName = (Array.Exists(branchIds, item => item == 0)) ? "All" : string.Join(", ", _branchService.LoadBranch(branchIds).Select(x => x.Name).ToList()),
                CampusName = (Array.Exists(campusIds, item => item == 0)) ? "All" : string.Join(", ", _campusService.LoadCampus(campusIds).Select(x => x.Name).ToList()),
                BatchName = (Array.Exists(batchs, item => item == 0)) ? "All" : string.Join(", ", _batchService.LoadBatch(batchs).Select(x => x.Name).ToList()),
                BatchDays = (Array.Exists(batchDays, item => item == "0")) ? "All" : string.Join("; ", batchDays)
            };

            foreach (var sp in studentProgramList.Distinct())
            {
                var spd = new StudentProgramDetails();
                var str = new List<string>();
                spd.StudentMaterial = new List<StudentMaterial>();
                //if (informationViewList != null)
                if (!Array.Exists(informationViewList, item => item == ""))
                {
                    var info = informationViewList;

                    foreach (string displayInfo in info)
                    {
                        #region Display info
                        switch (displayInfo)
                        {
                            case "Program Roll":
                                str.Add("PRN :" + sp.PrnNo);
                                break;
                            case "StudentName":
                                if (sp.Student != null && sp.Student.NickName != null)
                                {
                                    str.Add("Name :" + sp.Student.NickName);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Mobile No":
                                if (sp.Student != null && sp.Student.Mobile != null)
                                {
                                    str.Add("Mobile No :" + sp.Student.Mobile);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }
                            case "Due Payment":
                                if (sp.DueAmount != null)
                                {
                                    str.Add("Due :" + sp.DueAmount);
                                    break;
                                }
                                else
                                {
                                    str.Add("Due :0.00");
                                    break;
                                }
                            case "FatherName":
                                if (sp.Student != null && sp.Student.FatherName != null)
                                {
                                    str.Add("Father's Name :" + sp.Student.FatherName);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }

                            case "Email":
                                if (sp.Student != null && sp.Student.Email != null)
                                {
                                    str.Add("Email:" + sp.Student.Email);
                                    break;
                                }
                                else
                                {
                                    str.Add("");
                                    break;
                                }

                            default:
                                break;
                        }
                        #endregion
                    }
                    spd.StudentInfo = str.ToArray();
                }
                else
                {
                    spd.StudentInfo = new string[]
                                                  {
                                                      "PRN :" + sp.PrnNo,
                                                      "Name :" + sp.Student.NickName,
                                                      "Due :"+sp.DueAmount
                                                  };
                }
                //
                foreach (Material m in sp.Program.Materials)
                {
                    var sm = new StudentMaterial
                    {
                        Item = m.ShortName,
                        ItemId = m.Id,
                        ItemTypeId = m.MaterialType.Id,
                        IsDistributed = false
                    };

                    sm.TotalMaterialCount = 0;
                    if (sp.Materials != null && sp.Materials.Count > 0)
                    {
                        foreach (Material given in sp.Materials)
                        {
                            if (m.Id == given.Id)
                            {
                                sm.IsDistributed = true;
                                sm.TotalMaterialCount += 1;
                                //break;
                            }
                        }
                    }
                    spd.StudentMaterial.Add(sm);
                }
                mt.SudentProgramDetailsList.Add(spd);
            }
            materialReportList.Add(mt);


            try
            {
                List<string> headerList = new List<string>();
                headerList.Add(ErpInfo.OrganizationNameFull);
                //headerList.Add("Material Distribution List");

                List<string> footerList = new List<string>();
                footerList.Add("");

                List<string> columnList = new List<string>();
                columnList.Add("Student Info");
                columnList.Add("Provided");
                columnList.Add("Not Provided");

                var mDistributionExcelList = new List<List<object>>();
                var materialTypeList = materialList.GroupBy(mat => mat.MaterialType, (materialType, material) => new { Key = materialType, materials = material.ToList() });
                materialTypeList = materialTypeList.Where(x => x.Key.Status == MaterialType.EntityStatus.Active).OrderBy(x => x.Key.Rank);
                foreach (MaterialReports mReports in materialReportList)
                {
                    foreach (StudentProgramDetails studentProgramDetails in mReports.SudentProgramDetailsList)
                    {
                        var xlsRow = new List<object>();

                        string sInfo = "  ";
                        foreach (var studentInfo in studentProgramDetails.StudentInfo)
                        {
                            sInfo += studentInfo + ", ";
                        }
                        xlsRow.Add(sInfo.Remove(sInfo.Length - 2));
                        if (materialList != null)
                        {
                            string provided = "  ";
                            foreach (var mTList in materialTypeList)
                            {
                                var materialListByProgramSession = mTList.materials.Where(x => x.Status == Material.EntityStatus.Active && x.Session.Id == sessionId && x.Program.Id == programId).ToList();
                                if (materialListByProgramSession.Count > 0)
                                {
                                    string matList = "  ";
                                    foreach (var mList in materialListByProgramSession)
                                    {
                                        foreach (StudentMaterial xStudentMaterial in studentProgramDetails.StudentMaterial)
                                        {
                                            if (xStudentMaterial.ItemId == mList.Id)
                                            {
                                                if (xStudentMaterial.IsDistributed)
                                                {
                                                    matList += xStudentMaterial.Item + "(" + xStudentMaterial.TotalMaterialCount + "), ";
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    if (matList.Length > 2)
                                    {
                                        provided += mTList.Key.Name + ":" + matList.Remove(matList.Length - 2) + ", ";
                                    }
                                }
                                else { }
                            }
                            string notProvided = "  ";
                            foreach (var mTList in materialTypeList)
                            {
                                var materialListByProgramSession = mTList.materials.Where(x => x.Status == Material.EntityStatus.Active && x.Session.Id == sessionId && x.Program.Id == programId).ToList();
                                if (materialListByProgramSession.Count > 0)
                                {
                                    string matListNot = "  ";
                                    foreach (var mList in materialListByProgramSession)
                                    {
                                        foreach (StudentMaterial xStudentMaterial in studentProgramDetails.StudentMaterial)
                                        {
                                            if (xStudentMaterial.ItemId == mList.Id)
                                            {
                                                if (xStudentMaterial.IsDistributed)
                                                { }
                                                else
                                                {
                                                    matListNot += xStudentMaterial.Item + ", ";
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    if (matListNot.Length > 2)
                                    {
                                        notProvided += mTList.Key.Name + ":" + matListNot.Remove(matListNot.Length - 2) + ", ";
                                    }
                                }
                                else { }
                            }
                            xlsRow.Add(provided.Remove(provided.Length - 2));
                            xlsRow.Add(notProvided.Remove(notProvided.Length - 2));
                            mDistributionExcelList.Add(xlsRow);
                        }
                    }
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, mDistributionExcelList, footerList, "material-Distribution-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                return View("AutoClose");
            }

            return null;

            
        }

        #endregion

        #endregion

        #region Ajax Request Function

        #region Get Program

        //public JsonResult GetProgramByOrganization(long organizationId)
        //{
        //    try
        //    {
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        IList<Program> programList = _programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(organizationId));
        //        Program obj = new Program();
        //        obj.Id = 0;
        //        obj.Name = "-Select Program-";
        //        programList.Insert(0, obj);

        //        var programSelectList = new SelectList(programList, "Id", "Name");
        //        //ViewBag.ProgramIds = programSelectList;

        //        //var sessionList=programBranchSessionList

        //        return Json(programSelectList, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception)
        //    {
        //        ViewBag.ErrorMessage = "Problem Occurred";
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        #endregion

        #region Get Authorize Session
        //public JsonResult GetSession(long programId)
        //{
        //    try
        //    {
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        IList<Session> sessionList = _sessionService.LoadAuthorizedSession(_userMenu,null,_commonHelper.ConvertIdToList( programId));
        //        Session obj = new Session();
        //        obj.Id = 0;
        //        obj.Name = "-Select Session-";
        //        sessionList.Insert(0, obj);

        //        var sessionSelectList = new SelectList(sessionList, "Id", "Name");
        //        ViewBag.SessionId = sessionSelectList;

        //        //var sessionList=programBranchSessionList

        //        return Json(sessionSelectList, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception)
        //    {
        //        ViewBag.ErrorMessage = "Problem Occurred";
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }

        //}

        #endregion

        #region Loading Branch Using Program & Session

        public JsonResult LoadMaterial(long[] materialTypeId,List<long> organizationIds = null, List<long> programIds=null, List<long> sessionIds = null)
        {
            try
            {
                //IList<ProgramBranchSession> programBranchSessionList = _programBranchSessionService.LoadBranchByProgramSession(programId, sessionId);
                //_userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //IList<Branch> branchList = _branchService.LoadAuthorizedBranch(_userMenu, organizationIds, programIds, sessionIds);

                //Branch obj = new Branch();
                //obj.Id = SelectionType.SelelectAll;
                //obj.Name = "All Branch";
                //branchList.Insert(0, obj);
                //var branchSelectList = new SelectList(branchList, "Id", "Name");

                IList<Material> materialList = new List<Material>();

                if (materialTypeId != null)
                {
                    bool isAllSelect = Array.Exists(materialTypeId, item => item == -200);
                    if (!isAllSelect)
                    {
                        materialList = _materialService.LoadMaterial(materialTypeId, Convert.ToInt64(programIds),Convert.ToInt64(sessionIds));
                        Material materialObj = new Material();
                        materialObj.Id = -200;
                        materialObj.Name = "All Material";
                        materialList.Insert(0, materialObj);
                    }
                }

                var materialSelectList = new SelectList(materialList, "Id", "Name");
                return Json(new {materialSelectList = materialSelectList, JsonRequestBehavior.AllowGet });
            }
            catch (Exception ex)
            {
                _logger.Error("Batch. AjaxRequestForBranch.", ex);
                ViewBag.ErrorMessage = "Problem Occurred";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }


        }

        #endregion

        #region Loading Campus Using Branch/Branches

        //"programId": programId, "sessionId": sessionId, "branchId": branchId
        //public JsonResult GetCampus(long programId, long sessionId, long[] branchId)
        //{
        //    try
        //    {
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;

        //        IList<Campus> campusList;
        //        campusList = _campusService.LoadAuthorizeCampus(_userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId));
        //        //IList<Campus> campusList = _campusService.LoadByBranchIds(branchId);

        //        Campus campusObj = new Campus();
        //        campusObj.Id = SelectionType.SelelectAll;
        //        campusObj.Name = "All Campus";
        //        campusList.Insert(0, campusObj);

        //        var campusSelectList = new SelectList(campusList, "Id", "Name");
        //        ViewBag.SelectedCampus = campusSelectList;

        //        return Json(campusSelectList, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception)
        //    {
        //        ViewBag.ErrorMessage = "Problem Occurred";
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }


        //}

        #endregion
        
        public ActionResult MaterialXmlUpload(HttpPostedFileBase xmlFile)
        {
            const string errorMessageForvalidXml = "Please upload a valid XML file.";
            try
            {

                if (xmlFile != null && xmlFile.ContentLength > 0 && xmlFile.ContentType.ToLower() == "text/xml")
                {

                    XElement element = XElement.Load(new StreamReader(xmlFile.InputStream));
                    var studentProgramList = (from student in
                                                   (from students in element.Elements("Students")
                                                    select students).Elements("Student")
                                               let roll = student.Attribute("Roll").Value
                                               select new
                                               {
                                                   Roll = roll,
                                               })
                        .ToList();
                    if (studentProgramList.Count > 0)
                    {
                        return Json(new { studentPrograms = studentProgramList, isSuccess = true });
                    }
                    else
                    {
                        return Json(new { errmsg = errorMessageForvalidXml, isSuccess = false });
                    }
                }
                else
                {
                    return Json(new { errmsg = errorMessageForvalidXml, isSuccess = false });
                }
            }
            catch (XmlException ex)
            {

                return Json(new { errmsg = errorMessageForvalidXml, isSuccess = false });
            }
            catch (Exception ex)
            {

                return Json(new { errmsg = WebHelper.CommonErrorMessage, isSuccess = false });
            }
        }

        #endregion

        #region Ajax Function Responsible For GetBatchDays, GetBatchTime, GetBatch

        //public JsonResult GetBatchDays(long programId, long sessionId, long[] branchId, long[] campusId)
        //{
        //    #region old
        //    //try
        //    //{

        //    //    IList<Batch> batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampus(programId, sessionId, branchId, campusId);

        //    //    if (branchId != null && !branchId.Contains(SelectionType.SelelectAll) && campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //    //        batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampus(programId, sessionId, branchId, campusId);
        //    //    //else if (branchId != null && !branchId.Contains(SelectionType.SelelectAll))
        //    //    //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchHabibXp(programIdLong, sessionIdLong, branchId);
        //    //    //else if (campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //    //    //    batchList = _batchService.LoadBatchDaysByProgramSessionCampusHabibXp(programIdLong, sessionIdLong, campusId);
        //    //    //else
        //    //    //    batchList = _batchService.LoadBatchDaysByProgramSessionHabibXp(programIdLong, sessionIdLong);

        //    //    //Check by Batch Day
        //    //    IList<string> batchDayList = (from x in batchList select x.Days).Distinct().ToList();
        //    //    List<SelectListItem> selectBatchList = (new SelectList(batchDayList)).ToList();

        //    //    if (selectBatchList.Any())
        //    //        selectBatchList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Days" });
        //    //    return Json(new { batchDays = selectBatchList, JsonRequestBehavior.AllowGet });

        //    //}
        //    //catch (Exception)
        //    //{
        //    //    ViewBag.ErrorMessage = "Problem Occurred";
        //    //    return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    //}
        //    #endregion

        //    try
        //    {
        //        #region old code
        //        //long programIdLong = 0;
        //        //if (programId != null)
        //        //    long.TryParse(programId.ToString(), out programIdLong);

        //        //long sessionIdLong = 0;
        //        //if (sessionId != null)
        //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);

        //        ////set Authorize branch. when all branch is seleced 
        //        //if ((Array.Exists(branchId, item => item == 0)))
        //        //{
        //        //    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        //    IList<Branch> authorizedBranchList = _branchService.LoadAuthorizedBranch(userMenu);
        //        //    if (authorizedBranchList != null && authorizedBranchList.Count > 0)
        //        //        branchId = (from x in authorizedBranchList select x.Id).ToArray();
        //        //    else
        //        //        branchId = new[] { -1l };
        //        //}

        //        //IList<Batch> batchList = new List<Batch>();
        //        //if (branchId != null && !branchId.Contains(SelectionType.SelelectAll) && campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampusHabibXp(programIdLong, sessionIdLong, branchId, campusId);
        //        //else if (branchId != null && !branchId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchHabibXp(programIdLong, sessionIdLong, branchId);
        //        //else if (campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionCampusHabibXp(programIdLong, sessionIdLong, campusId);
        //        //else
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionHabibXp(programIdLong, sessionIdLong);

        //        ////Check by Batch Day
        //        //IList<string> batchDayList = (from x in batchList select x.Days).Distinct().ToList();
        //        //List<SelectListItem> batchSelecteList = (new SelectList(batchDayList)).ToList();

        //        //if (batchSelecteList.Any())
        //        //    batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Day" });


        //        #endregion

        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var batchList = _batchService.LoadAuthorizeBatchGroupByDays(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList());
        //        //Check by Batch Day
        //        //IList<string> batchDayList = (from x in batchList select x.Days).Distinct().ToList();
        //        List<SelectListItem> batchSelecteList = (new SelectList(batchList)).ToList();
        //        if (batchSelecteList.Any())
        //            batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Day" });


        //        return Json(new { batchDays = batchSelecteList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        //logger.Error("Batch. AjaxRequestForBatchDay.", ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        //public JsonResult GetBatchTime(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays)
        //{
        //    #region old
        //    //try
        //    //{
        //    //    IList<Batch> batchTimeList = _batchService.LoadBatchDaysByProgramSessionBranchCampusBatch(programId, sessionId, branchId, campusId, batchDaysL);

        //    //    SelectList selectBatchTimeList = new SelectList(batchTimeList.DistinctBy(x => x.BatchTime), "BatchTime", "FormatedBatchTime");
        //    //    List<SelectListItem> batchTime = selectBatchTimeList.ToList();

        //    //    if (batchTime.Count > 0)
        //    //    {
        //    //        batchTime.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Time" });
        //    //    }

        //    //    //ViewBag.SelectedBatchTimxe = batchTime;
        //    //    return Json(batchTime, JsonRequestBehavior.AllowGet);

        //    //}
        //    //catch (Exception)
        //    //{
        //    //    ViewBag.ErrorMessage = "Problem Occurred";
        //    //    return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    //}
        //    #endregion
        //    try
        //    {
        //        #region old code
        //        //long programIdLong = 0;
        //        //if (programId != null)
        //        //    long.TryParse(programId.ToString(), out programIdLong);

        //        //long sessionIdLong = 0;
        //        //if (sessionId != null)
        //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);
        //        ////set Authorize branch. when all branch is seleced 
        //        //if ((Array.Exists(branchId, item => item == 0)))
        //        //{
        //        //    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        //    IList<Branch> authorizedBranchList = _branchService.LoadAuthorizedBranch(userMenu);
        //        //    if (authorizedBranchList != null && authorizedBranchList.Count > 0)
        //        //        branchId = (from x in authorizedBranchList select x.Id).ToArray();
        //        //    else
        //        //        branchId = new[] { -1l };
        //        //}

        //        //IList<Batch> batchList = new List<Batch>();
        //        //if (branchId != null && !branchId.Contains(SelectionType.SelelectAll) && campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampusHabibXp(programIdLong, sessionIdLong, branchId, campusId);
        //        //else if (branchId != null && !branchId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchHabibXp(programIdLong, sessionIdLong, branchId);
        //        //else if (campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionCampusHabibXp(programIdLong, sessionIdLong, campusId);
        //        //else
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionHabibXp(programIdLong, sessionIdLong);

        //        ////Check Batch Day 
        //        //if (batchDays != null && !batchDays.Contains(SelectionType.SelelectAll.ToString()))
        //        //    batchList = (from a in batchList where batchDays.Any(ae => ae == a.Days) select a).ToList();

        //        //SelectList batchTimeSelectList = new SelectList(batchList, "FormatedBatchTime", "FormatedBatchTime");
        //        //List<SelectListItem> batchTimes = batchTimeSelectList.DistinctBy(a => a.Value).ToList();

        //        //if (batchTimes.Any())
        //        //    batchTimes.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Time" });
        //        #endregion
        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var batchList = _batchService.LoadAuthorizeBatchGroupByTimes(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays);
        //        //batchList = batchList.DistinctBy(x => x.BatchTime).ToList();
        //        //SelectList batchTimeSelectList = new SelectList(batchList, "BatchTime", "FormatedBatchTime");
        //        var batchTimeSelectList = new SelectList(batchList);
        //        List<SelectListItem> batchTimes = batchTimeSelectList.ToList();
        //        if (batchTimes.Any())
        //            batchTimes.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Time" });
                
        //        return Json(new { batchTime = batchTimes, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        //logger.Error("Batch. AjaxRequestForBatchTime.", ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        //public JsonResult GetBatch(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, string[] batchTimeText)
        //{
        //    #region old
        //    //try
        //    //{
        //    //    IList<Batch> batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampusBatch(programId, sessionId, branchId, campusId, batchId);

        //    //    Batch batchobj = new Batch();
        //    //    batchobj.Id = SelectionType.SelelectAll;
        //    //    batchobj.Name = "All Batch";
        //    //    batchList.Insert(0, batchobj);
        //    //    var batchSelectList = new SelectList(batchList.DistinctBy(x => x.Name), "Id", "Name");
        //    //    //ViewBag.SelectedBatch = batchSelectList;

        //    //    return Json(batchSelectList, JsonRequestBehavior.AllowGet);

        //    //}
        //    //catch (Exception)
        //    //{
        //    //    ViewBag.ErrorMessage = "Problem Occurred";
        //    //    return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    //}
        //    #endregion
        //    try
        //    {
        //        #region old code
        //        //long programIdLong = 0;
        //        //if (programId != null)
        //        //    long.TryParse(programId.ToString(), out programIdLong);


        //        //long sessionIdLong = 0;
        //        //if (sessionId != null)
        //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);

        //        ////set Authorize branch. when all branch is seleced 
        //        //if ((Array.Exists(branchId, item => item == 0)))
        //        //{
        //        //    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        //    IList<Branch> authorizedBranchList = _branchService.LoadAuthorizedBranch(userMenu);
        //        //    if (authorizedBranchList != null && authorizedBranchList.Count > 0)
        //        //        branchId = (from x in authorizedBranchList select x.Id).ToArray();
        //        //    else
        //        //        branchId = new[] { -1l };
        //        //}

        //        //IList<Batch> batchList = new List<Batch>();
        //        //if (branchId != null && !branchId.Contains(SelectionType.SelelectAll) && campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampusHabibXp(programIdLong, sessionIdLong, branchId, campusId);
        //        //else if (branchId != null && !branchId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchHabibXp(programIdLong, sessionIdLong, branchId);
        //        //else if (campusId != null && !campusId.Contains(SelectionType.SelelectAll))
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionCampusHabibXp(programIdLong, sessionIdLong, campusId);
        //        //else
        //        //    batchList = _batchService.LoadBatchDaysByProgramSessionHabibXp(programIdLong, sessionIdLong);


        //        ////Check Batch Day
        //        //if (batchDays != null && !batchDays.Contains(SelectionType.SelelectAll.ToString()))
        //        //    batchList = (from a in batchList where batchDays.Any(ae => ae == a.Days) select a).ToList();


        //        ////Check Batch Time
        //        //if (batchTime != null && !batchTime.Contains(SelectionType.SelelectAll.ToString()))
        //        //    batchList = (from x in batchList where batchTime.Any(y => y == x.FormatedBatchTime) select x).ToList();


        //        //List<SelectListItem> batchSelecteList = (new SelectList(batchList.OrderBy(x => x.Rank), "Id", "Name")).ToList();
        //        //if (batchSelecteList.Any())
        //        //    batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch" });


        //        #endregion

        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var batchList = _batchService.LoadAuthorizeBatch(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime);
        //        List<SelectListItem> batchSelecteList = (new SelectList(batchList.ToList(), "Id", "Name")).ToList();
        //        if (batchSelecteList.Any())
        //            batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch" });
               

        //        return Json(new { batchName = batchSelecteList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        //logger.Error("Batch. AjaxRequestForBatch.", ex);
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }


        //}


        #endregion

    }
}