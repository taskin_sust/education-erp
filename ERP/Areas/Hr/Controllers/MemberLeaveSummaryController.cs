﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FluentNHibernate.Conventions;
using iTextSharp.text;
using log4net;
using NHibernate;
using NHibernate.Mapping;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class MemberLeaveSummaryController : Controller
    {
        // GET: Hr/LeaveReport
        private ILog _logger = LogManager.GetLogger("HrArea");
        private readonly ILeaveService _hrLeaveService;
        private readonly IOrganizationService _organizationService;
        private ISession _session;
        private readonly ICommonHelper _commonHelper;
        private readonly ILeaveApplicationService _hrLeaveApplicationService;
        private readonly ITeamMemberService _hrMemberService;
        private readonly IMembersLeaveSummaryService _hrMembersLeaveSummaryService;
        private readonly IEmploymentHistoryService _employmentHistoryService;
        private readonly IMaritalInfoService _hrMaritalInfoService;
        private readonly IUserService _userService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IDepartmentService _departmentService;
        private readonly IDesignationService _designationService;
        private List<UserMenu> _userMenu;

        public MemberLeaveSummaryController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _hrLeaveService = new LeaveService(_session);
            _organizationService = new OrganizationService(_session);
            _hrLeaveApplicationService = new LeaveApplicationService(_session);
            _hrMemberService = new TeamMemberService(_session);
            _hrMembersLeaveSummaryService = new MembersLeaveSummaryService(_session);
            _employmentHistoryService = new EmploymentHistoryService(_session);
            _hrMaritalInfoService = new MaritalInfoService(_session);
            _userService = new UserService(_session);
            _commonHelper = new CommonHelper();
            _branchService = new BranchService(_session);
            _campusService = new CampusService(_session);
            _departmentService = new DepartmentService(_session);
            _designationService = new DesignationService(_session);
        }

        public ActionResult Index()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new MultiSelectList(new List<Branch>(), "Id", "Name");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(MemberYearlyLeaveSummaryAssignFormViewModel memberYearlyLeaveSummaryFormViewModel)
        {
            string message = "";
            bool state = false;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                _hrMembersLeaveSummaryService.AssignMemberYearlyLeaveSummary(_userMenu, memberYearlyLeaveSummaryFormViewModel);
                message = "Leave Summary Successfully closed for year " + memberYearlyLeaveSummaryFormViewModel.ClosingYear.ToString();
                state = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = state, Message = message });
        }

        public ActionResult LeaveSummaryReport()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new MultiSelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");
                ViewBag.DepartmentList = new MultiSelectList(new List<Department>(), "Id", "Name");
                ViewBag.NumberOfData = 100;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult LeaveSummaryReport(MemberYearlyLeaveSummaryReportFormViewModel memberYearlyLeaveSummaryReportFormViewModel)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            bool isError = false;
            try
            {
                 DateTime searchingDate = new DateTime(memberYearlyLeaveSummaryReportFormViewModel.LeaveYear, 12, 31, 23,59, 59);
                List<string> informationViewList = new List<string>(new[] { "Name", "Pin" });
                List<string> leaveNameList = new List<string>();
                List<Leave> organizationLeaveList = _hrLeaveService.LoadOrganizationLeaveByDate(_userMenu, memberYearlyLeaveSummaryReportFormViewModel.OrganizationId,searchingDate)
                        .ToList();
                if (organizationLeaveList.Any())
                {
                    foreach (Leave leave in organizationLeaveList)
                    {
                        leaveNameList.Add(leave.Name);
                    }
                }
                ViewBag.InformationViewList = informationViewList;
                ViewBag.LeaveNameList = leaveNameList;
                Organization organization = _organizationService.LoadById(memberYearlyLeaveSummaryReportFormViewModel.OrganizationId);
                Branch branch = (memberYearlyLeaveSummaryReportFormViewModel.BranchId != SelectionType.SelelectAll) ? _branchService.GetBranch(memberYearlyLeaveSummaryReportFormViewModel.BranchId) : null;
                Campus campus = (memberYearlyLeaveSummaryReportFormViewModel.CampusId != SelectionType.SelelectAll) ? _campusService.GetCampus(memberYearlyLeaveSummaryReportFormViewModel.CampusId) : null;
                Department department = (memberYearlyLeaveSummaryReportFormViewModel.DepartmentId != SelectionType.SelelectAll) ? _departmentService.LoadById(memberYearlyLeaveSummaryReportFormViewModel.DepartmentId) : null;
                Designation designation = (memberYearlyLeaveSummaryReportFormViewModel.DesignationId != SelectionType.SelelectAll) ? _designationService.LoadById(memberYearlyLeaveSummaryReportFormViewModel.DesignationId) : null;

                ViewBag.OrganizationName = (organization != null) ? organization.Name : "-";
                ViewBag.OrganizationShortName = (organization != null) ? organization.ShortName : "-";
                ViewBag.BranchName = (branch != null) ? branch.Name : "All Branch";
                ViewBag.CampusName = (campus != null) ? campus.Name : "All Campus";
                ViewBag.DepartmentName = (department != null) ? department.Name : "All Department";
                ViewBag.DesignationName = (designation != null) ? designation.Name : "All Designation";
                ViewBag.SheetName = "Member-Leave-Summary__" + searchingDate.Year.ToString();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            ViewBag.IsSuccess = isError;
            return PartialView("Partial/_GetLeaveSummaryReport");
        }

        public ActionResult LeaveSummaryReportDataTable(int draw, int start, int length, MemberYearlyLeaveSummaryReportFormViewModel memberYearlyLeaveSummaryReportFormView)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            int recordsTotal = 0;
            int recordsFiltered = 0;
            var data = new List<object>();
            List<string> fieldsStringList = new List<string>();
            try
            {
                length = memberYearlyLeaveSummaryReportFormView.NoOfData;
                #region datatable

                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                #endregion

                MemberYearlyLeaveSummaryReportFormViewModel myrv = memberYearlyLeaveSummaryReportFormView;
                DateTime searchingDate = new DateTime(myrv.LeaveYear, 12, 31, 23, 59, 59);

                int rowCount = _hrMemberService.GetTeamMemberForLeaveSummaryCount(_userMenu, searchingDate, myrv.OrganizationId, myrv.BranchId, myrv.CampusId, myrv.DepartmentId, myrv.DesignationId, myrv.PinList);
                List<TeamMemberInfoForLeaveSummaryDto> teamMemberList = _hrMemberService.LoadTeamMemberForLeaveSummary(start, length, "", "", _userMenu, searchingDate, myrv.OrganizationId, myrv.BranchId, myrv.CampusId, myrv.DepartmentId, myrv.DesignationId, myrv.PinList);
                List<Leave> organizationLeaveList = _hrLeaveService.LoadOrganizationLeaveByDate(_userMenu, myrv.OrganizationId, searchingDate)
                        .ToList();
                recordsTotal = rowCount;
                recordsFiltered = recordsTotal;

                if (teamMemberList.Any())
                {
                    List<long> teamMemberIdList = teamMemberList.Select(x => x.TeamMemberId).ToList();
                    List<long> leaveIdList = new List<long>();
                    List<MemberLeaveSummary> memberLeaveSummaryList = new List<MemberLeaveSummary>();
                    if (organizationLeaveList.Any())
                    {
                        leaveIdList = organizationLeaveList.Select(x => x.Id).ToList();
                        memberLeaveSummaryList = _hrMembersLeaveSummaryService.LoadMemberLeaveSummaryByOrgLeaveMemberYear(searchingDate.Year, myrv.OrganizationId, teamMemberIdList, leaveIdList);
                    }

                    foreach (TeamMemberInfoForLeaveSummaryDto teamMember in teamMemberList)
                    {
                        List<string> str = new List<string>();

                        #region datatable

                        str.Add(teamMember.NickName);
                        str.Add(teamMember.Pin.ToString());
                        if (organizationLeaveList.Any())
                        {
                            foreach (Leave leave in organizationLeaveList)
                            {
                                MemberLeaveSummary memberLeaveSummary = memberLeaveSummaryList.SingleOrDefault(x => x.TeamMember.Id == teamMember.TeamMemberId && x.Leave.Id == leave.Id);
                                if (memberLeaveSummary != null)
                                {
                                    str.Add(memberLeaveSummary.CarryForwardLeave.ToString());
                                    str.Add(memberLeaveSummary.CurrentYearLeave.ToString());
                                    str.Add(memberLeaveSummary.EncashLeave.ToString());
                                    str.Add(memberLeaveSummary.VoidLeave.ToString());
                                    str.Add(memberLeaveSummary.TotalLeaveBalance.ToString());
                                    str.Add(memberLeaveSummary.ApprovedLeave.ToString());
                                    str.Add(memberLeaveSummary.PendingLeave.ToString());
                                    str.Add("<span style='font-weight:bold;'>"+memberLeaveSummary.AvailableBalance.ToString()+"</span>");
                                }
                                else
                                {
                                    str.Add("-");
                                    str.Add("-");
                                    str.Add("-");
                                    str.Add("-");
                                    str.Add("-");
                                    str.Add("-");
                                    str.Add("-");
                                    str.Add("<span style='font-weight:bold;'>-</span>");
                                }
                            }
                        }

                        #endregion

                        data.Add(str);
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }

            return Json(new { draw = draw, recordsTotal = recordsTotal, recordsFiltered = recordsFiltered, start = start, length = length, data = data });
        }

        public ActionResult ExportMemberLeaveSummaryReport(string memberYearlyLeaveSummaryReportFormViewString)
        {
            

            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            List<List<object>> leaveSummaryExcelList = new List<List<object>>();
            try
            {

                MemberYearlyLeaveSummaryReportFormViewModel memberYearlyLeaveSummaryReportFormView = new JavaScriptSerializer().Deserialize<MemberYearlyLeaveSummaryReportFormViewModel>(memberYearlyLeaveSummaryReportFormViewString);

                MemberYearlyLeaveSummaryReportFormViewModel myrv = memberYearlyLeaveSummaryReportFormView;
                DateTime searchingDate = new DateTime(myrv.LeaveYear, 12, 31, 23, 59, 59);
                List<TeamMemberInfoForLeaveSummaryDto> teamMemberList = _hrMemberService.LoadTeamMemberForLeaveSummary(0, 0, "", "", _userMenu, searchingDate, myrv.OrganizationId, myrv.BranchId, myrv.CampusId, myrv.DepartmentId, myrv.DesignationId, myrv.PinList);
                List<Leave> organizationLeaveList = _hrLeaveService.LoadOrganizationLeaveByDate(_userMenu, myrv.OrganizationId, searchingDate).ToList();
                var headerList = new List<string>();
                Organization organization = _organizationService.LoadById(myrv.OrganizationId);
                Branch branch = (myrv.BranchId != SelectionType.SelelectAll) ? _branchService.GetBranch(myrv.BranchId) : null;
                Campus campus = (myrv.CampusId != SelectionType.SelelectAll) ? _campusService.GetCampus(myrv.CampusId) : null;
                Department department = (myrv.DepartmentId != SelectionType.SelelectAll) ? _departmentService.LoadById(myrv.DepartmentId) : null;
                Designation designation = (myrv.DesignationId != SelectionType.SelelectAll) ? _designationService.LoadById(myrv.DesignationId) : null;
                string sheetName = "Member-Leave-Summary__" + searchingDate.Year.ToString();
                headerList.Add((organization != null) ? organization.Name : "-");
                headerList.Add(((branch != null) ? branch.Name : "All Branch") + " - " + ((campus != null) ? campus.Name : "All Campus"));
                headerList.Add(((department != null) ? department.Name : "All Department") + " - " + ((designation != null) ? designation.Name : "All Designation"));
                headerList.Add(sheetName);
                var footerList = new List<string>();
                footerList.Add("");

                List<string> columnList = new List<string> { "Name", "Pin" };

                List<object> xlsRowSubHeaderName = new List<object> { "", "" };
                if (organizationLeaveList.Any())
                {
                    foreach (Leave leave in organizationLeaveList)
                    {
                        columnList.Add(leave.Name);
                        columnList.Add("");
                        columnList.Add("");
                        columnList.Add("");
                        columnList.Add("");
                        columnList.Add("");
                        columnList.Add("");
                        columnList.Add("");
                        xlsRowSubHeaderName.Add("Carry Forward");
                        xlsRowSubHeaderName.Add("Current Year");
                        xlsRowSubHeaderName.Add("Encash");
                        xlsRowSubHeaderName.Add("Void");
                        xlsRowSubHeaderName.Add("Total Balance");
                        xlsRowSubHeaderName.Add("Taken");
                        xlsRowSubHeaderName.Add("Pending");
                        xlsRowSubHeaderName.Add("Available Balance");
                    }
                }
                leaveSummaryExcelList.Add(xlsRowSubHeaderName);

                if (teamMemberList.Any())
                {
                    List<long> teamMemberIdList = teamMemberList.Select(x => x.TeamMemberId).ToList();
                    List<long> leaveIdList = new List<long>();
                    List<MemberLeaveSummary> memberLeaveSummaryList = new List<MemberLeaveSummary>();
                    if (organizationLeaveList.Any())
                    {
                        leaveIdList = organizationLeaveList.Select(x => x.Id).ToList();
                        memberLeaveSummaryList = _hrMembersLeaveSummaryService.LoadMemberLeaveSummaryByOrgLeaveMemberYear(searchingDate.Year, myrv.OrganizationId, teamMemberIdList, leaveIdList);
                    }

                    foreach (TeamMemberInfoForLeaveSummaryDto teamMember in teamMemberList)
                    {
                        List<object> xlsRow = new List<object>();

                        #region datatable

                        xlsRow.Add(teamMember.NickName);
                        xlsRow.Add(teamMember.Pin.ToString());
                        if (organizationLeaveList.Any())
                        {
                            foreach (Leave leave in organizationLeaveList)
                            {
                                MemberLeaveSummary memberLeaveSummary = memberLeaveSummaryList.SingleOrDefault(x => x.TeamMember.Id == teamMember.TeamMemberId && x.Leave.Id == leave.Id);
                                if (memberLeaveSummary != null)
                                {
                                    xlsRow.Add(memberLeaveSummary.CarryForwardLeave.ToString());
                                    xlsRow.Add(memberLeaveSummary.CurrentYearLeave.ToString());
                                    xlsRow.Add(memberLeaveSummary.EncashLeave.ToString());
                                    xlsRow.Add(memberLeaveSummary.VoidLeave.ToString());
                                    xlsRow.Add(memberLeaveSummary.TotalLeaveBalance.ToString());
                                    xlsRow.Add(memberLeaveSummary.ApprovedLeave.ToString());
                                    xlsRow.Add(memberLeaveSummary.PendingLeave.ToString());
                                    xlsRow.Add(memberLeaveSummary.AvailableBalance.ToString());
                                }
                                else
                                {
                                    xlsRow.Add("-");
                                    xlsRow.Add("-");
                                    xlsRow.Add("-");
                                    xlsRow.Add("-");
                                    xlsRow.Add("-");
                                    xlsRow.Add("-");
                                    xlsRow.Add("-");
                                    xlsRow.Add("-");
                                }
                            }
                        }

                        #endregion

                        leaveSummaryExcelList.Add(xlsRow);
                    }
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, leaveSummaryExcelList, footerList, "Member-Leave-Summary__" + searchingDate.Year.ToString());
                return View("AutoClose");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }



        }
    }

}