﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface IAttendanceDeviceTypeDao : IBaseDao<AttendanceDeviceType, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        AttendanceDeviceType GetByName(string deviceTypeName);

        #endregion

        #region List Loading Function

        IList<AttendanceDeviceType> LoadAttendanceDeviceType(string code = "", long id = 0, string name = "");
        IList<AttendanceDeviceType> LoadAttendanceDeviceType(int draw, int start, int length, string name, string code, string finger);

        #endregion

        #region Others Function
        int AttendanceDevicetypeCount(string name, string code, string finger);

        #endregion

        #region Helper Function

        #endregion

    }

    public class AttendanceDeviceTypeDao : BaseDao<AttendanceDeviceType, long>, IAttendanceDeviceTypeDao
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public AttendanceDeviceType GetByName(string deviceTypeName)
        {
            return Session.QueryOver<AttendanceDeviceType>().Where(x => x.Name == deviceTypeName && x.Status == AttendanceDeviceType.EntityStatus.Active).SingleOrDefault<AttendanceDeviceType>();
        }

        #endregion

        #region List Loading Function

        public IList<AttendanceDeviceType> LoadAttendanceDeviceType(string code = "", long id = 0, string name = "")
        {
            var criteria = Session.CreateCriteria<AttendanceDeviceType>();
            if (!String.IsNullOrEmpty(code))
                criteria.Add(Restrictions.Eq("Code", code));
            if (!String.IsNullOrEmpty(name))
                criteria.Add(Restrictions.Eq("Name", name));
            criteria.Add(Restrictions.Eq("Status", AttendanceDeviceType.EntityStatus.Active));
            if (id > 0)
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            return criteria.List<AttendanceDeviceType>();
        }

        public IList<AttendanceDeviceType> LoadAttendanceDeviceType(int draw, int start, int length, string name, string code, string finger)
        {
            var criteria = Session.CreateCriteria<AttendanceDeviceType>();
            criteria = GetQueryCriteria(criteria, name, code, finger);

            return criteria.SetFirstResult(start).SetMaxResults(length).List<AttendanceDeviceType>();
        }

        #endregion

        #region Others Function
        public int AttendanceDevicetypeCount(string name, string code, string finger)
        {
            var criteria = Session.CreateCriteria<AttendanceDeviceType>();
            criteria = GetQueryCriteria(criteria, name, code, finger);
            return criteria.List<AttendanceDeviceType>().Count;
        }

        #endregion

        #region Helper Function
        private ICriteria GetQueryCriteria(ICriteria criteria, string name, string code, string finger)
        {
            criteria.Add(Restrictions.Eq("Status", AttendanceDeviceType.EntityStatus.Active));
            if (!String.IsNullOrEmpty(name))
                criteria.Add(Restrictions.Eq("Name", name));
            if (!String.IsNullOrEmpty(code))
                criteria.Add(Restrictions.Eq("Code", code));
            if (finger != "0")
            {
                if (finger == "1")
                    criteria.Add(Restrictions.Eq("IsFinger", true));
                if (finger == "2")
                    criteria.Add(Restrictions.Eq("IsFinger", false));
            }
            return criteria;
        }
        #endregion
    }
}
