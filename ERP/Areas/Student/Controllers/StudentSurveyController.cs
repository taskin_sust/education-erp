﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.Areas.Student.Models;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.BusinessRules.Student;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Students;


namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]

    public class StudentSurveyController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly ISurveyQuestionAnswerService _surveyQuestionAnswerService;
        private IProgramSessionSurveyQuestionService _programSessionSurveyQuestionService;
        private readonly ISurveyQuestionService _surveyQuestionService;
        private readonly IStudentSurveyService _studentSurveyService;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ICampusService _campusService;
        private readonly IBranchService _branchService;
        private readonly IBatchService _batchService;
        private readonly IStudentProgramService _studentProgramService;
        private List<UserMenu> _userMenu;
        private readonly CommonHelper _commonHelper;
        private readonly IStudentImagesMediaService _studentImagesMediaService;
        public StudentSurveyController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _surveyQuestionService = new SurveyQuestionService(session);
                _studentSurveyService = new StudentSurveyService(session);
                _organizationService = new OrganizationService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _courseService = new CourseService(session);
                _branchService = new BranchService(session);
                _batchService = new BatchService(session);
                _campusService = new CampusService(session);
                _studentProgramService = new StudentProgramService(session);
                _surveyQuestionAnswerService = new SurveyQuestionAnswerService(session);
                _programSessionSurveyQuestionService = new ProgramSessionSurveyQuestionService(session);
                _commonHelper = new CommonHelper();
                _studentImagesMediaService = new StudentImagesMediaService();
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }
        #endregion

        #region Make Survey

        [HttpGet]
        public ActionResult MakeStudentSurvey(string programRoll)
        {
            try
            {
                string message = "";
                StudentProgram studentDetails = GetStudentProgram(programRoll, out message);
                if (!string.IsNullOrEmpty(message))
                    ViewBag.ErrorMessage = message;

                if (studentDetails != null)
                {
                    if (studentDetails.StudentImage.Count > 0)
                    {
                        StudentMediaImage mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(studentDetails.StudentImage[0].Id);
                        studentDetails.MediaStudentImage = mediaStudentImages != null ? mediaStudentImages : new StudentMediaImage();
                    }
                    else
                    {
                        studentDetails.MediaStudentImage = new StudentMediaImage();
                    }
                    ViewBag.StudentProgram = studentDetails;
                    IList<ProgramSessionSurveyQuestion> surveyQuestionProgramSession = _programSessionSurveyQuestionService.LoadProgramSessionSurveyQuestion(studentDetails.Program.Id, studentDetails.Batch.Session.Id, Convert.ToInt32(SurveyType.ProgramEvaluationByMgt));
                    ViewBag.SurveyQuestionProgramSession = surveyQuestionProgramSession;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }
        
        [HttpPost]
        public ActionResult SaveSurvey(IList<StudentSurveyAnswerViewModel> data)
        {
            try
            {
                if (!(data.Any()))
                {
                    return Json(new Response(false, "Student Survey Fail"));
                }
                IList<StudentSurveyAnswer> studentSurveyAnswerList = new List<StudentSurveyAnswer>();
                string message = "";
                foreach (var studentSurveyAnswerViewModel in data)
                {
                    string messageAuthorize = "";
                    StudentProgram studentDetails = GetStudentProgram(studentSurveyAnswerViewModel.PrnNo, out messageAuthorize);
                    if (studentDetails != null)
                    {
                        if (!string.IsNullOrEmpty(studentSurveyAnswerViewModel.Answer))
                        {
                            SurveyQuestionAnswer sqa = _surveyQuestionAnswerService.LoadSurveyQuestionAnswer(Convert.ToInt64(studentSurveyAnswerViewModel.Answer), studentDetails.Program.Id, studentDetails.Batch.Session.Id);
                            if (sqa != null)
                            {
                                StudentSurveyAnswer isExistAnswer = _studentSurveyService.LoadStudentAnswerByQuestionIdStudentProgramId(sqa.SurveyQuestion.Id, studentDetails.Id);
                                if (isExistAnswer != null)
                                {
                                    isExistAnswer.Advice = studentSurveyAnswerViewModel.Advice;
                                    isExistAnswer.Complain = studentSurveyAnswerViewModel.Complain;
                                    isExistAnswer.SurveyQuestionAnswer = sqa;
                                    isExistAnswer.SurveyQuestion = sqa.SurveyQuestion;
                                    isExistAnswer.StudentProgram = studentDetails;
                                    studentSurveyAnswerList.Add(isExistAnswer);
                                }
                                else
                                {
                                    StudentSurveyAnswer studentSurveyAnswer = new StudentSurveyAnswer();
                                    studentSurveyAnswer.Advice = studentSurveyAnswerViewModel.Advice;
                                    studentSurveyAnswer.Complain = studentSurveyAnswerViewModel.Complain;
                                    studentSurveyAnswer.SurveyQuestionAnswer = sqa;
                                    studentSurveyAnswer.SurveyQuestion = sqa.SurveyQuestion;
                                    studentSurveyAnswer.StudentProgram = studentDetails;
                                    studentSurveyAnswerList.Add(studentSurveyAnswer);
                                }
                            }
                        }
                    }
                    else
                    {
                        message = messageAuthorize;
                        break;
                    }
                }
                if (!string.IsNullOrEmpty(message))
                {
                    return Json(new Response(false, "Invalid Program Roll"));
                }

                if (!(studentSurveyAnswerList.Any()))
                {
                    return Json(new Response(false, "You Mast answer at least one question"));
                }

                _studentSurveyService.SaveSurvey(studentSurveyAnswerList);
                return Json(new Response(true, "Make Student Survey Successfull"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Student Survey Fail"));
            }
        }
        
        [HttpGet]
        public ActionResult MakeSurvey()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");

            ViewBag.InformationStringList = new SelectList(StudentListConstant.GetStudentMakeSurveyInformationStringList().Select(x => new SelectListItem() { Text = x, Value = x }), "Text", "Value").ToList();

            return View();
        }

        [HttpPost]
        public ActionResult MakeSurvey(StudentMakeSurvey studentListReportObj)
        {
            bool isError = true;
            try
            {
                if (studentListReportObj != null)
                {
                    if (studentListReportObj.InformationViewList == null)
                    {
                        var infoView = new List<string>(new[] { StudentListConstant.ProgramRoll, StudentListConstant.StudentsName, StudentListConstant.MobileNumberStudent });
                        studentListReportObj.InformationViewList = infoView.ToArray();
                    }

                    Organization orgObj = _organizationService.LoadById(studentListReportObj.OrganizationId);
                    Program programOrg = _programService.GetProgram(studentListReportObj.ProgramId);
                    Session sessionOrg = _sessionService.LoadById(studentListReportObj.SessionId);
                    ViewBag.organizationName = orgObj.Name;
                    ViewBag.programName = programOrg.Name;
                    ViewBag.sessionName = sessionOrg.Name;
                    ViewBag.PageSize = Constants.PageSize;
                    ViewBag.SurveyStatus = studentListReportObj.SelectedSuveyStatus;
                    isError = false;
                }
                else
                {
                    ViewBag.ErrorMessage = "Student List Not Found .";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            ViewBag.IsSuccess = !isError;
            return PartialView("Partial/_GetStudentList", studentListReportObj);
        }

        [HttpPost]
        public JsonResult StudentListAjaxRequest(int draw, int start, int length, long organizationId, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, string[] informationViewList, string[] batchDays, string[] batchTime, long[] courseId, int surveyStatus, string programRoll, string name)
        {
            int recordsTotal = 0;
            long recordsFiltered = 0;
            var data = new List<object>();
            if (Request.IsAjaxRequest())
            {
                try
                {
                    #region OrderBy and Direction
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        orderBy = "PrnNo";
                    }
                    #endregion

                    #region Authorization Check
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    recordsTotal = _studentSurveyService.GetAuthorizedStudentsCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, courseId, surveyStatus, programRoll, name);
                    IList<StudentMakeSurveyDto> studentProgramList = _studentSurveyService.LoadAuthorizedStudentsMakeSurveyDto(_userMenu, start, length, orderBy, orderDir.ToUpper(), programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, courseId, surveyStatus, programRoll, name).ToList();

                    #endregion

                    recordsFiltered = recordsTotal;
                    int sl = start + 1;
                    foreach (StudentMakeSurveyDto c in studentProgramList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case StudentListConstant.ProgramRoll:
                                        str.Add(c.PrnNo);
                                        break;
                                    case StudentListConstant.StudentsName:
                                        str.Add(c.FullName);
                                        break;
                                    case StudentListConstant.NickName:
                                        str.Add(c.NickName);
                                        break;
                                    case StudentListConstant.MobileNumberStudent:
                                        str.Add(c.Mobile);
                                        break;
                                    case StudentListConstant.MobileNumberFather:
                                        str.Add(c.GuardiansMobile1);
                                        break;
                                    case StudentListConstant.MobileNumberMother:
                                        str.Add(c.GuardiansMobile2);
                                        break;
                                    case StudentListConstant.FathersName:
                                        str.Add(c.FatherName);
                                        break;
                                    case StudentListConstant.InstituteName:
                                        str.Add(c.Institute);
                                        break;
                                    case StudentListConstant.Email:
                                        str.Add(c.Email);
                                        break;
                                    case StudentListConstant.Branch:
                                        str.Add(c.Branch);
                                        break;
                                    case StudentListConstant.Campus:
                                        str.Add(c.Campus);
                                        break;
                                    case StudentListConstant.BatchDays:
                                        str.Add(c.BatchDays);
                                        break;
                                    case StudentListConstant.BatchTime:
                                        str.Add(c.BatchTime);
                                        break;
                                    case StudentListConstant.Batch:
                                        str.Add(c.Batch);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            str.Add("<a target='_blank' href='" + Url.Action("MakeStudentSurvey", "StudentSurvey") + "?programRoll=" + c.PrnNo + "'  class='glyphicon glyphicon-earphone'></a>&nbsp;&nbsp;<a target='_blank' href='" + Url.Action("InformationView", "Information") + "?PrnNo=" + c.PrnNo + "'  class='glyphicon glyphicon-edit'></a>&nbsp;&nbsp;<a target='_blank' href='" + Url.Action("IndividualReport", "StudentSurvey") + "?programRollNumber=" + c.PrnNo + "'  class='glyphicon glyphicon-list'></a>");
                        }
                        sl++;
                        data.Add(str);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }
        
        #endregion

        #region Indivisual Report
        public ActionResult IndividualReport(string programRollNumber)
        {
            ViewBag.PrnNumber = programRollNumber;
            return View();
        }
        public ActionResult GetDataByPrn(string prnumber)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    string message = "";
                    StudentProgram studentDetails = GetStudentProgram(prnumber, out message);
                    if (studentDetails == null || !string.IsNullOrEmpty(message))
                    {
                        return Json(new Response(false, message));
                    }

                    if (!(studentDetails.StudentSurveyAnswers.Any()))
                    {
                        return Json(new Response(false, "No Survey Data found for this student"));
                    }

                    if (studentDetails.StudentImage != null && studentDetails.StudentImage.Count > 0)
                    {
                        //calling ums_erp_media db for image viewing 
                        {
                            StudentMediaImage mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(studentDetails.StudentImage[0].Id);
                            studentDetails.MediaStudentImage = mediaStudentImages;
                            //ssiv.StuentImage = mediaStudentImages != null ? mediaStudentImages.Images : null;
                        }
                    }

                    ViewBag.StudentProgram = studentDetails;
                    IList<ProgramSessionSurveyQuestion> surveyQuestionProgramSession =
                        _programSessionSurveyQuestionService.LoadProgramSessionSurveyQuestion(studentDetails.Program.Id, studentDetails.Batch.Session.Id, Convert.ToInt32(SurveyType.ProgramEvaluationByMgt));
                    ViewBag.SurveyQuestionProgramSession = surveyQuestionProgramSession;
                    return PartialView("Partial/_StudentSurveyData");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Student Wise Survey Report

        public ActionResult SurveyReport()
        {
            Initialize();
            return View();
        }

        [HttpPost]
        public ActionResult SurveyReport(StudentSurveyReport studentSurveyReportObj)
        {
            bool isSuccess = false;
            try
            {
                if (studentSurveyReportObj != null)
                {
                    List<string> extraColomn = new List<string>();
                    List<string> infoView = new List<string>();
                    if (studentSurveyReportObj.InformationViewList == null)
                    {
                        infoView = new List<string>(new[]
                            {
                                StudentListConstant.ProgramRoll, StudentListConstant.StudentsName,
                                StudentListConstant.MobileNumberStudent
                            });
                    }
                    else
                    {
                        infoView = studentSurveyReportObj.InformationViewList.ToList();
                    }

                    if (infoView.Any())
                    {
                        bool isComplain = infoView.Contains(StudentListConstant.Complain) ? true : false;
                        bool isAdvice = infoView.Contains(StudentListConstant.Advice) ? true : false;
                       

                        if (isComplain || isAdvice)
                        {
                            if (isComplain)
                                infoView.Remove(StudentListConstant.Complain);
                            if (isAdvice)
                                infoView.Remove(StudentListConstant.Advice);

                            //if (studentSurveyReportObj.SelectedQuestion.ToList().Contains(SelectionType.SelelectAll))
                            //{
                                IList<ProgramSessionSurveyQuestion> questionList = _programSessionSurveyQuestionService.LoadProgramSessionSurveyQuestion(studentSurveyReportObj.ProgramId, studentSurveyReportObj.SessionId, Convert.ToInt32(SurveyType.ProgramEvaluationByMgt), studentSurveyReportObj.SelectedAnswer, (studentSurveyReportObj.SelectedQuestion.ToList().Contains(SelectionType.SelelectAll)) ? null : studentSurveyReportObj.SelectedQuestion);
                                if (questionList.Any())
                                {
                                    foreach (ProgramSessionSurveyQuestion programSessionSurveyQuestion in questionList)
                                    {
                                        extraColomn.Add(programSessionSurveyQuestion.SurveyQuestion.Question);
                                        if (isComplain)
                                            extraColomn.Add(StudentListConstant.Complain);
                                        if (isAdvice)
                                            extraColomn.Add(StudentListConstant.Advice);
                                    }
                                }
                            //}
                        }
                    }
                    studentSurveyReportObj.InformationViewList = infoView.ToArray();
                    ViewBag.OrganizationName = _organizationService.LoadById(studentSurveyReportObj.OrganizationId).Name;
                    ViewBag.OrganizationShortName = _organizationService.LoadById(studentSurveyReportObj.OrganizationId).ShortName;
                    ViewBag.SessionName = _sessionService.LoadById(studentSurveyReportObj.SessionId).Name;
                    ViewBag.ProgramName = _programService.GetProgram(studentSurveyReportObj.ProgramId).Name;
                    ViewBag.ExtraColomn = extraColomn;
                    ViewBag.PageSize = Constants.PageSize;
                   
                    isSuccess = true;
                }
                else
                {
                    ViewBag.ErrorMessage = "Student List Not Found .";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            ViewBag.IsSuccess = isSuccess;
            return PartialView("Partial/_GetStudentListForStudetWiseSurveyReport", studentSurveyReportObj);
        }

        [HttpPost]
        public JsonResult StudentWiseSurveyReportListAjaxRequest(int draw, int start, int length, long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long[] questionIds, string[] answers, string[] informationViewList, string programRoll, string name, string mobile)
        {
            var data = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;

            try
            {
         
                List<string> infoView = new List<string>();
                bool isComplain = false;
                bool isAdvice = false;
                if (informationViewList == null || (informationViewList.Any() && informationViewList.Contains("")) || !informationViewList.Any())
                {
                    infoView = new List<string>(new[]
                            {
                                StudentListConstant.ProgramRoll, StudentListConstant.StudentsName,
                                StudentListConstant.MobileNumberStudent
                            });

                }
                else
                {
                    infoView = informationViewList.ToList();
                }
                if (infoView.Any())
                {
                    isComplain = infoView.Contains(StudentListConstant.Complain) ? true : false;
                    isAdvice = infoView.Contains(StudentListConstant.Advice) ? true : false;
                }

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                }

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                recordsTotal = _studentSurveyService.GetAuthorizedStudentWiseSurveyReportCount(_userMenu, programId, sessionId, branchIds, campusIds, batchDays, batchTimes, batchIds, courseIds, questionIds, answers, programRoll, name, mobile);
                recordsFiltered = recordsTotal;

                IList<StudentWiseSurveyReportDto> studentWiseSurveyReportDtoList = _studentSurveyService.GetAuthorizedStudentWiseSurveyReportDto(_userMenu, start, length, orderBy, orderDir, organizationId, programId, sessionId, branchIds, campusIds, batchDays, batchTimes, batchIds, courseIds, questionIds, answers, programRoll, name, mobile);

                IList<ProgramSessionSurveyQuestion> questionList = _programSessionSurveyQuestionService.LoadProgramSessionSurveyQuestion(programId, sessionId, Convert.ToInt32(SurveyType.ProgramEvaluationByMgt), answers, (questionIds.ToList().Contains(SelectionType.SelelectAll)) ? null : questionIds);

                List<long> questionIdList = (questionList.Any()) ? questionList.Select(x => x.SurveyQuestion.Id).ToList() : new List<long>();
                List<long> studentProgramIdList = (studentWiseSurveyReportDtoList.Any()) ? studentWiseSurveyReportDtoList.Select(x => x.Id).ToList() : new List<long>();

                List<StudentWiseSurveyQuestionAnswerDto> studentWiseSurveyQuestionAnswerDtoList = _studentSurveyService.LoadStudentWiseSurveyQuestionAnswerDto(studentProgramIdList, questionIdList).ToList();

                int sl = start + 1;
                foreach (StudentWiseSurveyReportDto c in studentWiseSurveyReportDtoList)
                {
                    List<string> str = new List<string> { sl.ToString() };
                    if (infoView.Any())
                    {
                        foreach (string informationView in infoView)
                        {
                            switch (informationView)
                            {
                                case StudentListConstant.ProgramRoll:
                                    str.Add(c.PrnNo);
                                    break;
                                case StudentListConstant.StudentsName:
                                    str.Add(c.FullName);
                                    break;
                                case StudentListConstant.NickName:
                                    str.Add(c.NickName);
                                    break;
                                case StudentListConstant.MobileNumberStudent:
                                    str.Add(c.Mobile);
                                    break;
                                case StudentListConstant.MobileNumberFather:
                                    str.Add(c.GuardiansMobile1);
                                    break;
                                case StudentListConstant.MobileNumberMother:
                                    str.Add(c.GuardiansMobile2);
                                    break;
                                case StudentListConstant.FathersName:
                                    str.Add(c.FatherName);
                                    break;
                                case StudentListConstant.InstituteName:
                                    str.Add(c.Institute);
                                    break;
                                case StudentListConstant.Email:
                                    str.Add(c.Email);
                                    break;
                                case StudentListConstant.Branch:
                                    str.Add(c.Branch);
                                    break;
                                case StudentListConstant.Campus:
                                    str.Add(c.Campus);
                                    break;
                                case StudentListConstant.BatchDays:
                                    str.Add(c.BatchDays);
                                    break;
                                case StudentListConstant.BatchTime:
                                    str.Add(c.BatchTime);
                                    break;
                                case StudentListConstant.Batch:
                                    str.Add(c.Batch);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    if (questionList.Any() && (isComplain || isAdvice))
                    {
                        foreach (ProgramSessionSurveyQuestion programSessionSurveyQuestion in questionList)
                        {
                            StudentWiseSurveyQuestionAnswerDto tempSurveyQuestionAnswerDto = (studentWiseSurveyQuestionAnswerDtoList.Any()) ? studentWiseSurveyQuestionAnswerDtoList.Where(x => x.StudentProgramId == c.Id && programSessionSurveyQuestion.SurveyQuestion.Id == x.SurveyQuestionId).Take(1).SingleOrDefault() : null;
                            string answer = "-";
                            string complain = "-";
                            string advice = "-";
                            if (tempSurveyQuestionAnswerDto != null)
                            {
                                answer = tempSurveyQuestionAnswerDto.Answer;
                                complain = tempSurveyQuestionAnswerDto.Complain;
                                advice = tempSurveyQuestionAnswerDto.Advice;
                            }

                            str.Add(answer);
                            if(isComplain)
                                str.Add(complain);
                            if (isAdvice)
                                str.Add(advice);
                        }  
                    }
                    sl++;
                    data.Add(str);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        #region Export Student Wise Survey Report List

        public ActionResult ExportStudentWiseSurveyReportList(long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long[] questionIds, string[] answers, string[] informationViewList, string programRoll, string name, string mobile, string organizationNameHeader, string programSessionHeader)
        {
            try
            {
                string organizationNameHeaderTemp = _organizationService.LoadById(organizationId).Name;
                string programSessionHeaderTemp = _programService.GetProgram(programId).Name + " - " + _sessionService.LoadById(sessionId).Name;
                List<string> headerList = new List<string>
                {
                    organizationNameHeaderTemp,
                    programSessionHeaderTemp,
                    "Student List",
                    ""
                };
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                var studentWiseSurveyExcelList = new List<List<object>>();

                List<string> infoView = new List<string>();
                bool isComplain = false;
                bool isAdvice = false;
                if (informationViewList == null || (informationViewList.Any() && informationViewList.Contains("")) || !informationViewList.Any())
                {
                    infoView = new List<string>(new[]
                            {
                                StudentListConstant.ProgramRoll, StudentListConstant.StudentsName,
                                StudentListConstant.MobileNumberStudent
                            });

                }
                else
                {
                    infoView = informationViewList.ToList();
                }
                if (infoView.Any())
                {
                    isComplain = infoView.Contains(StudentListConstant.Complain) ? true : false;
                    isAdvice = infoView.Contains(StudentListConstant.Advice) ? true : false;

                }
                
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<StudentWiseSurveyReportDto> studentWiseSurveyReportDtoList = _studentSurveyService.GetAuthorizedStudentWiseSurveyReportDto(_userMenu, 0, 0, "", "", organizationId, programId, sessionId, branchIds, campusIds, batchDays, batchTimes, batchIds, courseIds, questionIds, answers, programRoll, name, mobile);

                IList<ProgramSessionSurveyQuestion> questionList = _programSessionSurveyQuestionService.LoadProgramSessionSurveyQuestion(programId, sessionId, Convert.ToInt32(SurveyType.ProgramEvaluationByMgt), answers, (questionIds.ToList().Contains(SelectionType.SelelectAll)) ? null : questionIds);

                List<long> questionIdList = (questionList.Any()) ? questionList.Select(x => x.SurveyQuestion.Id).ToList() : new List<long>();
                List<long> studentProgramIdList = (studentWiseSurveyReportDtoList.Any()) ? studentWiseSurveyReportDtoList.Select(x => x.Id).ToList() : new List<long>();

                IList<StudentWiseSurveyQuestionAnswerDto> studentWiseSurveyQuestionAnswerDtoList = _studentSurveyService.LoadStudentWiseSurveyQuestionAnswerDto(studentProgramIdList, questionIdList);

                if (isComplain)
                    infoView.Remove(StudentListConstant.Complain);
                if (isAdvice)
                    infoView.Remove(StudentListConstant.Advice);
                columnList = infoView;
                if (questionList.Any() && (isComplain || isAdvice))
                {
                    foreach (ProgramSessionSurveyQuestion programSessionSurveyQuestion in questionList)
                    {
                        columnList.Add(programSessionSurveyQuestion.SurveyQuestion.Question);
                        if (isComplain)
                            columnList.Add(StudentListConstant.Complain);
                        if (isAdvice)
                            columnList.Add(StudentListConstant.Advice);
                    }
                }


                foreach (StudentWiseSurveyReportDto c in studentWiseSurveyReportDtoList)
                {
                    var xlsRow = new List<object>();
                    if (infoView.Any())
                    {
                        foreach (string informationView in infoView)
                        {
                            switch (informationView)
                            {
                                case StudentListConstant.ProgramRoll:
                                    xlsRow.Add(c.PrnNo);
                                    break;
                                case StudentListConstant.StudentsName:
                                    xlsRow.Add(c.FullName);
                                    break;
                                case StudentListConstant.NickName:
                                    xlsRow.Add(c.NickName);
                                    break;
                                case StudentListConstant.MobileNumberStudent:
                                    xlsRow.Add(c.Mobile);
                                    break;
                                case StudentListConstant.MobileNumberFather:
                                    xlsRow.Add(c.GuardiansMobile1);
                                    break;
                                case StudentListConstant.MobileNumberMother:
                                    xlsRow.Add(c.GuardiansMobile2);
                                    break;
                                case StudentListConstant.FathersName:
                                    xlsRow.Add(c.FatherName);
                                    break;
                                case StudentListConstant.InstituteName:
                                    xlsRow.Add(c.Institute);
                                    break;
                                case StudentListConstant.Email:
                                    xlsRow.Add(c.Email);
                                    break;
                                case StudentListConstant.Branch:
                                    xlsRow.Add(c.Branch);
                                    break;
                                case StudentListConstant.Campus:
                                    xlsRow.Add(c.Campus);
                                    break;
                                case StudentListConstant.BatchDays:
                                    xlsRow.Add(c.BatchDays);
                                    break;
                                case StudentListConstant.BatchTime:
                                    xlsRow.Add(c.BatchTime);
                                    break;
                                case StudentListConstant.Batch:
                                    xlsRow.Add(c.Batch);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    if (questionList.Any() && (isComplain || isAdvice))
                    {
                        foreach (ProgramSessionSurveyQuestion programSessionSurveyQuestion in questionList)
                        {
                            StudentWiseSurveyQuestionAnswerDto tempSurveyQuestionAnswerDto = studentWiseSurveyQuestionAnswerDtoList.Where(x => x.StudentProgramId == c.Id && programSessionSurveyQuestion.SurveyQuestion.Id == x.SurveyQuestionId).Take(1).SingleOrDefault();
                            string answer = "-";
                            string complain = "-";
                            string advice = "-";
                            if (tempSurveyQuestionAnswerDto != null)
                            {
                                answer = tempSurveyQuestionAnswerDto.Answer;
                                complain = tempSurveyQuestionAnswerDto.Complain;
                                advice = tempSurveyQuestionAnswerDto.Advice;
                            }

                            xlsRow.Add(answer);
                            if (isComplain)
                                xlsRow.Add(complain);
                            if (isAdvice)
                                xlsRow.Add(advice);
                        }
                    }
                    studentWiseSurveyExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, studentWiseSurveyExcelList, footerList, "student-wise-survey-report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"), false);

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }
        }

        #endregion
        
        public JsonResult GetStudentWiseSurveyReportCount(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, long[] questionId, string[] answerNames)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                int studentSurveyCount = _studentSurveyService.GetAuthorizedStudentWiseSurveyReportCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, questionId, answerNames, null, null, null);

                return Json(new { studentSurveyCount = studentSurveyCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Question Wise Survey Report
        
        public ActionResult QuestionWiseSurveyReport()
        {
            StudentSurveyReport studentSurveyReportObj = new StudentSurveyReport();
            //Initialize(studentSurveyReportObj);
            Initialize();
            return View(studentSurveyReportObj);
        }
        [HttpPost]
        public ActionResult QuestionWiseSurveyReport(StudentSurveyReport studentSurveyReportObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var organizationName = _organizationService.LoadById(studentSurveyReportObj.OrganizationId).Name;
                    var organizationShortName = _organizationService.LoadById(studentSurveyReportObj.OrganizationId).ShortName;
                    var programName = _programService.GetProgram(studentSurveyReportObj.ProgramId).Name;
                    var sessionName = _sessionService.LoadById(studentSurveyReportObj.SessionId).Name;
                    ViewBag.organizationName = organizationName;
                    ViewBag.organizationShortName = organizationShortName;
                    ViewBag.sessionName = sessionName;
                    ViewBag.programName = programName;
                    ViewBag.PageSize = Constants.PageSize;
                    IList<ProgramSessionSurveyQuestion> questionList = new List<ProgramSessionSurveyQuestion>();
                    questionList = _programSessionSurveyQuestionService.LoadProgramSessionSurveyQuestion(studentSurveyReportObj.ProgramId, studentSurveyReportObj.SessionId, Convert.ToInt32(SurveyType.ProgramEvaluationByMgt), studentSurveyReportObj.SelectedAnswer);
                    var json = new JavaScriptSerializer().Serialize(studentSurveyReportObj);
                    ViewBag.SelectedQuestion = questionList;
                    ViewBag.PageSize = Constants.PageSize;
                    ViewData["StudentSurveyReportObj"] = json;
                    return View("GenerateQuestionWiseSurveyReport");
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                //Initialize(studentSurveyReportObj);
                Initialize();
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View(studentSurveyReportObj);
        }
        public JsonResult QuestionwiseSurveyReportListAjaxRequest(int draw, int start, int length,
            string studentSurveyReportObj)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentSurveyReportViewModel =
                        new JavaScriptSerializer().Deserialize<StudentSurveyReport>(
                            studentSurveyReportObj);
                var questionWiseSurveyList = _studentSurveyService.LoadQuestionWiseSurvey(_userMenu, start, length, studentSurveyReportViewModel.ProgramId, studentSurveyReportViewModel.SessionId, studentSurveyReportViewModel.SelectedBranch, studentSurveyReportViewModel.SelectedCampus, studentSurveyReportViewModel.SelectedBatchDays, studentSurveyReportViewModel.SelectedBatchTime, studentSurveyReportViewModel.SelectedBatch, studentSurveyReportViewModel.SelectedCourse, studentSurveyReportViewModel.SelectedQuestion, studentSurveyReportViewModel.SelectedAnswer);
                var data = new List<object>();
                int recordsTotal = _studentSurveyService.QuestionWiseSurveyCount(start, length, studentSurveyReportViewModel.SelectedQuestion);
                long recordsFiltered = recordsTotal;
                int sl = start;
                foreach (var questionWiseSurvey in questionWiseSurveyList)
                {
                    sl++;
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    str.Add(questionWiseSurvey[0]);
                    str.Add(questionWiseSurvey[1]);
                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception ex)
            {
                var data = new List<object>();
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });
            }
        }
        public ActionResult ExportQuestionwiseSurveyReportList(string organizationHeader, string programSessionHeader,
           string studentSurveyReportObj)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentSurveyReportViewModel =
                       new JavaScriptSerializer().Deserialize<StudentSurveyReport>(
                           studentSurveyReportObj);
                var headerList = new List<string>();
                headerList.Add(_organizationService.LoadById(studentSurveyReportViewModel.OrganizationId).Name);
                headerList.Add(programSessionHeader);
                headerList.Add("Questionwise Survey Report");
                headerList.Add("");
                var columnList = new List<string>();
                columnList.Add("Question");
                columnList.Add("Answer");
                var footerList = new List<string>();
                footerList.Add("");
                var questionWiseSurveyList = _studentSurveyService.LoadQuestionWiseSurvey(_userMenu, 0, 0, studentSurveyReportViewModel.ProgramId, studentSurveyReportViewModel.SessionId, studentSurveyReportViewModel.SelectedBranch, studentSurveyReportViewModel.SelectedCampus, studentSurveyReportViewModel.SelectedBatchDays, studentSurveyReportViewModel.SelectedBatchTime, studentSurveyReportViewModel.SelectedBatch, studentSurveyReportViewModel.SelectedCourse, studentSurveyReportViewModel.SelectedQuestion, studentSurveyReportViewModel.SelectedAnswer);
                var data = new List<object>();
                var questionWiseSurveyExcelList = new List<List<object>>();
                foreach (var questionWiseSurvey in questionWiseSurveyList)
                {
                    var xlsRow = new List<object>();
                    xlsRow.Add(questionWiseSurvey[0]);
                    xlsRow.Add(questionWiseSurvey[1].ToString().Replace("<br/>", ", ").Trim(',', ' '));
                    questionWiseSurveyExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, questionWiseSurveyExcelList, footerList, "questionwise-survey-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }
        
        #endregion

        #region Helper Function

        private StudentProgram GetStudentProgram(string programRoll, out string message)
        {
            string errorMessage = "";
            StudentProgram studentDetails = new StudentProgram();
            if (string.IsNullOrEmpty(programRoll))
            {
                errorMessage = "Invalid Program Roll";
            }
            else
            {
                studentDetails = _studentProgramService.GetStudentProgram(programRoll);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                //bool allBranch = false;
                //bool allProgram = false;
                //long[] branchIdList = AuthHelper.GetUserBranchIdList(_userMenu, out allBranch);
                //long[] programIdList = AuthHelper.GetUserProgramIdList(_userMenu, out allProgram);

                List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                List<long> programIdList = AuthHelper.LoadProgramIdList(_userMenu);

                if (studentDetails != null)
                {
                    var userBranchId = studentDetails.Batch.Branch.Id;
                    var userProgramId = studentDetails.Program.Id;
                    if (branchIdList != null)
                    {
                        bool isBranchSelect = branchIdList.Contains(userBranchId);
                        //bool isBranchSelect = Array.Exists(branchIdList, item => item == userBranchId);
                        if (!isBranchSelect)
                            errorMessage = "You are not authorized to this branch";
                    }
                    if (programIdList != null)
                    {

                        bool isProgramSelect = programIdList.Contains(userProgramId);
                        //bool isProgramSelect = Array.Exists(programIdList, item => item == userProgramId);
                        if (!isProgramSelect)
                            errorMessage = "You are not authorized to this program";
                    }
                }
            }
            if (!string.IsNullOrEmpty(errorMessage))
            {
                message = errorMessage;
                return new StudentProgram();
            }
            message = null;
            return studentDetails;
        }

        private void Initialize()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
            ViewBag.SelectedQuestion = new SelectList(new List<SurveyQuestion>(), "Id", "Name");
            ViewBag.SelectedAnswer = new SelectList(new List<SurveyQuestionAnswer>(), "Id", "Name");
            ViewBag.InformationStringList = new SelectList(StudentListConstant.GetStudentWiseSurveyReportInformationStringList().Select(x => new SelectListItem() { Text = x, Value = x }), "Text", "Value").ToList();
        }

        #region Ajax Functions

        [HttpPost]
        public JsonResult GetStudentCount(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int surveyStatus, string programRoll, string name)
        {
            bool successState = false;
            int studentCount = 0;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                studentCount = _studentSurveyService.GetAuthorizedStudentsCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, surveyStatus, programRoll, name);
                successState = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new { studentSurveyCount = studentCount, IsSuccess = successState });
        }

        [HttpPost]
        public ActionResult GetQuestion(long programId, long sessionId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    IList<ProgramSessionSurveyQuestion> surveyQuestionProgramSession =
                                    _programSessionSurveyQuestionService.LoadProgramSessionSurveyQuestion(programId, sessionId, Convert.ToInt32(SurveyType.ProgramEvaluationByMgt));
                    var questionSelectList = new SelectList(surveyQuestionProgramSession, "SurveyQuestion.Id", "SurveyQuestion.Question");
                    List<SelectListItem> questionList = questionSelectList.ToList();
                    if (questionList.Count > 0)
                        questionList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Question" });
                    return Json(new { questionList = questionList, IsSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult GetAnswer(long programId, long sessionId, long[] questionId)
        {
            try
            {
                IList<SurveyQuestionAnswer> answerList = _surveyQuestionAnswerService.LoadSurveyQuestionAnswer(programId, sessionId, questionId);
                var groupedAnswerList = answerList.GroupBy(m => m.Answer, (key, group) => new { Key = key, Group = group });
                List<SelectListItem> answerSelectedList = (new SelectList(groupedAnswerList, "Key", "Key")).ToList();
                if (answerList.Any())
                    answerSelectedList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Answer" });

                return Json(new { answerList = answerSelectedList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult GetQuestionWiseSurveyReportCount(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, long[] questionId, string[] answerNames)
        {
            try
            {
                #region Authorization Check
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                int studentSurveyCount = _studentSurveyService.GetAuthorizedStudentWiseSurveyReportCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, questionId, answerNames, null, null, null);
                #endregion
                return Json(new { studentSurveyCount = studentSurveyCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }


        #endregion
        
        #endregion
    }
}