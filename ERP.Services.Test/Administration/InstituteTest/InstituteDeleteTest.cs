﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.InstituteTest
{

    [Trait("Area", "Administration")]
    [Trait("Service", "Institute")]
    public class InstituteDeleteTest : InstituteBaseTest
    {

        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            InstituteFactory instituteFactory =
                 _InstituteFactory.Create()
                     .WithNameAndRank(_name, 1)
                     .WithInstituteCategory().Persist();
            _InstituteService.Delete(instituteFactory.Object);
            var institute = _InstituteService.LoadById(instituteFactory.Object.Id);
            Assert.Null(institute);
            
        }

        [Fact]
        public void Institute_Delete_Should_Not_Delete_InstituteCategory()
        {
            InstituteFactory instituteFactory =
                  _InstituteFactory.Create()
                      .WithNameAndRank(_name, 1)
                      .WithInstituteCategory().Persist();
            _InstituteService.Delete(instituteFactory.Object);
            var instituteCategory = _institutionCategoryService.LoadInstituteCategoryById(instituteFactory.Object.InstituteCategory.Id);
            Assert.NotNull(instituteCategory);
            Assert.True(instituteCategory.Id>0);
            
        }

        [Fact]
        public void Delete_Should_Throw_Dependency_Exception()
        {
            InstituteFactory instituteFactory =
                 _InstituteFactory.Create()
                     .WithNameAndRank(_name, 1)
                     .WithInstituteCategory().Persist();
            instituteFactory.Object.StudentUniversityInfos=new List<StudentUniversityInfo>()
            {
                new StudentUniversityInfo()
            };
            Assert.Throws<DependencyException>(() =>_InstituteService.Delete(instituteFactory.Object));
        }

        #endregion
    }
}
