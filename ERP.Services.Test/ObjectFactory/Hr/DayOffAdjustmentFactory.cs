﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Engine;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class DayOffAdjustmentFactory : ObjectFactoryBase<DayOffAdjustment>
    {
        private readonly IDayOffAdjustmentService _dayOffAdjustmentService;
        private TeamMemberFactory _teamMemberFactory;
        public DayOffAdjustmentFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _dayOffAdjustmentService = new DayOffAdjustmentService(session);
        }

        #region Create

        public DayOffAdjustmentFactory Create()
        {
            Object = new DayOffAdjustment()
            {
                Id = 0,
                CreateBy = 1,
                ModifyBy = 1,
                DayOffDate = DateTime.Now,
                InsteadOfDate = DateTime.Now.Subtract(new TimeSpan(23, 2, 2, 2)),
                Reason = "Official Purposes",
                DayOffStatus = 1,
                BusinessId = GetPcUserName()
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public DayOffAdjustmentFactory CreateWith(DateTime? dayOffDateTime, DateTime? insteadOffDate, string reason,
            int dayOffStatus,int status=1, long id = 0,bool isPost = false)
        {
            Object = new DayOffAdjustment()
            {
                Id=id,
                Status = status,
                CreateBy = 1,
                ModifyBy = 1,
                DayOffDate = dayOffDateTime,
                InsteadOfDate = insteadOffDate,
                Reason = reason,
                DayOffStatus = dayOffStatus,
                IsPost = isPost,
                BusinessId = GetPcUserName()
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public DayOffAdjustmentFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }

        public DayOffAdjustmentFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public DayOffAdjustmentFactory CreateMore(int num)
        {
            _teamMemberFactory.Create().Persist();
            for (int i = 0; i < num; i++)
            {
                var obj = new DayOffAdjustment()
                {
                    DayOffDate = DateTime.Now,
                    InsteadOfDate = DateTime.Now.Subtract(new TimeSpan(23, 2, 2, 2)),
                    Reason = "Official Purposes "+i,
                    DayOffStatus = 1,
                    BusinessId = GetPcUserName()
                };
                obj.TeamMember = _teamMemberFactory.Object;
                ObjectList.Add(obj);
            }
            return this;
        }

        public DayOffAdjustmentFactory Persist()
        {
            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { item });
                    }
                }
            }

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { item });
                    }
                }
            }
            return this;
        }
       

        #endregion

        #region Others
        #endregion

        #region CleanUp

        public DayOffAdjustmentFactory LogCleanUp(DayOffAdjustmentFactory dayOffAdjustmentFactory)
        {
            if (dayOffAdjustmentFactory.ObjectList.Any())
            {
                DeleteLog(dayOffAdjustmentFactory.ObjectList);
            }
            if (dayOffAdjustmentFactory.SingleObjectList.Any())
            {
                DeleteLog(dayOffAdjustmentFactory.SingleObjectList);
            }
            return this;
        }

        private void DeleteLog(IEnumerable<DayOffAdjustment> list)
        {
            foreach (var dayOffAdjustment in list)
            {
                var tableName = GetEntityTableName(typeof(DayOffAdjustmentLog));
                string swData = "DELETE FROM " + tableName + " WHERE [DayOffAdjustmentId] = " + dayOffAdjustment.Id + ";";
                _session.CreateSQLQuery(swData).ExecuteUpdate();
            }
        }

        public DayOffAdjustmentFactory CleanUp()
        {
            DeleteAll();
            _teamMemberFactory.Cleanup();
            return this;
        }
        #endregion

    }

}
