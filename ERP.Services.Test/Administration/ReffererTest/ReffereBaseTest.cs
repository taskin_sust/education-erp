﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.ReffererTest
{
    public class ReffererBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal IReferenceService _referenceService;
        internal List<long> IdList = new List<long>();
        private ISession _session;
        internal ReferrerFactory _referrerFactory;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal readonly string _code = Guid.NewGuid() + "_(1234 5678)";
        #endregion

        public ReffererBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _referrerFactory = new ReferrerFactory(null, _session);
            _referenceService = new ReferenceService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _referrerFactory.Cleanup();
            base.Dispose();
        }
    }
}
