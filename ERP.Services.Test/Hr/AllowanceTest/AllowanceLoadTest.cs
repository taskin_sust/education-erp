﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AllowanceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Allowance")]
    public class AllowanceLoadTest : AllowanceBaseTest
    {
        #region Basic Test

        //[Fact]
        //public void Get_Allowance_Test_Should_Return_True()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(1);
        //    var teamMember = PersistTeamMember(MentorId);
        //    var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
        //    var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
        //    var campus = teamMember.EmploymentHistory.Select(x => x.Campus).LastOrDefault();

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(_teamMemberFactory.Object);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });
        //    var userMenu = BuildUserMenu(new List<Organization>() { campus.Branch.Organization },
        //                                    null, new List<Branch>() { campus.Branch });

        //    var allowanceFactory = _allowanceFactory.CreateWith(Name, date, false).WithTeamMember(teamMember);
        //    _allowanceService.Save(userMenu, date.ToString("MM-dd-yyyy")
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "", true);
        //    var idList = Session.QueryOver<Allowance>().Select(x => x.Id).Where(x => x.TeamMember == _teamMemberFactory.Object).List<long>();
        //    _allowanceFactory.Object.Id = idList[0];
        //    var entity = _allowanceService.GetAllowance(allowanceFactory.Object.Id);
        //    Assert.NotNull(entity);
        //}

        //[Fact]
        //public void Load_Allowance_Test_Should_Return_True()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(1);
        //    var teamMember = PersistTeamMember(MentorId);
        //    var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
        //    var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
        //    var campus = teamMember.EmploymentHistory.Select(x => x.Campus).LastOrDefault();

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(_teamMemberFactory.Object);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });
        //    var userMenu = BuildUserMenu(new List<Organization>() { campus.Branch.Organization },
        //                                    null, new List<Branch>() { campus.Branch });

        //    var allowanceFactory = _allowanceFactory.CreateWith(Name, date, false).WithTeamMember(teamMember);
        //    _allowanceService.Save(userMenu, date.ToString("MM-dd-yyyy")
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "", true);
        //    var idList = Session.QueryOver<Allowance>().Select(x => x.Id).Where(x => x.TeamMember == _teamMemberFactory.Object).List<long>();
        //    _allowanceFactory.Object.Id = idList[0];

        //    var entityList = _allowanceService.LoadDailyAllowance(Convert.ToDateTime(date), Convert.ToDateTime(date.AddDays(1)), new List<long>() { teamMember.Id }, userMenu);
        //    Assert.NotEqual(0, entityList.Count());
        //}

        #endregion

        #region Business logic test

        [Fact]
        public void ArgumentNullException_Load_Allowance_For_UserMenu()
        {
            DateTime date = DateTime.Now.AddDays(-1);
            Assert.Throws<ArgumentNullException>(() => _allowanceService.LoadDailyAllowance(Convert.ToDateTime(date), Convert.ToDateTime(date), new List<long>() { }));
        }

        //[Fact]
        //public void InvalidDataException_AllowanceLoad_Unauthorize_TeamMember()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(1);
        //    var teamMember = CreateTeamMember();
        //    var startTime = new DateTime(date.Year, date.Month, date.AddDays(1).Day, 10, 10, 10);
        //    var endTime = new DateTime(date.Year, date.Month, date.AddDays(1).Day, 17, 10, 10);
        //    var branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).LastOrDefault();

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });
        //    var userMenu = BuildUserMenu(new List<Organization>() { branch.Organization }, null, new List<Branch>() { branch });

        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);
        //    _allowanceService.Save(userMenu, date.ToString("MM-dd-yyyy")
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "", true);
        //    var idList = Session.QueryOver<Allowance>().Select(x => x.Id).Where(x => x.TeamMember == teamMember).List<long>();
        //    _allowanceFactory.Object.Id = idList[0];
        //    using (ITransaction trans = Session.BeginTransaction())
        //    {
        //        for (int i = 0; i < _teamMemberFactory.Object.MentorHistory.Count; i++)
        //        {
        //            _teamMemberFactory.Object.MentorHistory.RemoveAt(i);
        //        }
        //        trans.Commit();
        //    }
        //    Assert.Throws<InvalidDataException>(() => _allowanceService.LoadDailyAllowance(Convert.ToDateTime(date), Convert.ToDateTime(date), new List<long>() { teamMember.Id }, userMenu));
        //}

    //    [Fact]
    //    public void InvalidDataException_AllowanceLoad_Unauthorize_Branch()
    //    {
    //        const string reason = "testing";
    //        DateTime date = DateTime.Now.AddDays(1);
    //        var teamMember = PersistTeamMember(MentorId);
    //        var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
    //        var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
    //        var campus = teamMember.EmploymentHistory.Select(x => x.Campus).LastOrDefault();

    //        _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
    //        _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
    //        _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });
    //        var userMenu = BuildUserMenu(new List<Organization>() { campus.Branch.Organization },
    //                                        null, new List<Branch>() { campus.Branch });
    //        var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);
    //        _allowanceService.Save(userMenu, date.ToString("MM-dd-yyyy")
    //                                , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
    //                                , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "", true);
    //        var idList = Session.QueryOver<Allowance>().Select(x => x.Id).Where(x => x.TeamMember == teamMember).List<long>();
    //        _allowanceFactory.Object.Id = idList[0];
    //        _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
    //        Assert.Throws<InvalidDataException>(() => _allowanceService.LoadDailyAllowance(Convert.ToDateTime(date), Convert.ToDateTime(date), new List<long>() { teamMember.Id }, userMenu,
    //                                                        null, null, _branchFactory.Object.Id));
        //    }

        #endregion

    }
}
