﻿using System;
using System.Collections.Generic;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BankTest
{
    interface IBankDeleteTest
    {
        #region Basic Test

        [Fact]
        void Should_Delete_Successfully();

        #endregion

        #region Business Logic Test
        [Fact]
        void Delete_Should_Throw_Dependency_Exception();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Bank")]
    public class BankDeleteTest : BankBaseTest, IBankDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            BankFactory bankFactory =
                    _bankFactory.Create()
                    .WithName(_name)
                    .Persist();
            Bank bank = _bankService.GetBank(bankFactory.Object.Id);
            var success = _bankService.Delete(bank.Id);
            Assert.True(success);
            bank = _bankService.GetBank(bankFactory.Object.Id);
            Assert.Null(bank);

        }
        #endregion

        #region Business Logic Test
        [Fact]
        public void Delete_Should_Throw_Dependency_Exception()
        {
            BankFactory bankFactory =
                    _bankFactory.Create()
                    .WithName(_name)
                    .Persist();
            Bank bank = _bankService.GetBank(bankFactory.Object.Id);
            bank.BankBranch = new List<BankBranch>() { new BankBranch() };
            Assert.Throws<DependencyException>(() => _bankService.Delete(bank.Id));
        }
        #endregion
    }
}
