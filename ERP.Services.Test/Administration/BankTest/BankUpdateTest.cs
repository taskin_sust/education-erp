﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BankTest
{
    public interface IBankUpdateTest
    {
        #region basic test
        [Fact]
        void Update_Should_Return_Null_Exception();

        [Fact]
        void Should_Update_Successfully();

        [Theory]
        [InlineData("up")]
        [InlineData("down")]
        void Should_Update_Rank_Successfully(string action);

        [Fact]
        void Should_Not_Update_Empty_Field(int index);

        #endregion

        #region business logic test
        [Fact]
        void Update_Should_Check_Duplicate_Name_Entry();

        [Fact]
        void Update_Should_Check_Duplicate_ShortName_Entry();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Bank")]
    public class BankUpdateTest : BankBaseTest, IBankUpdateTest
    {
        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var bankFactory =
                 _bankFactory.Create().WithName(_name)
                     .Persist();
            Assert.Throws<NullObjectException>(() => _bankService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var bankFactory =
                _bankFactory.Create().WithName(_name)
                    .Persist();
            Bank bankObj = _bankService.GetBank(bankFactory.Object.Id);
            bankObj.Name = bankObj.Name + " DEF GHI";
            bankObj.ShortName = bankObj.ShortName + " FGH";
            var success = _bankService.SaveOrUpdate(bankObj);
            Assert.True(success);
        }


        [Theory]
        [InlineData("up")]
        [InlineData("down")]
        public void Should_Update_Rank_Successfully(string action)
        {
            var organizationFac = organizationFactory.Create().Persist();
            var organization = organizationFac.Object;
            var success = _bankService.UpdateRank(organization.Id, 1, action);
            Assert.Equal(true, success);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void Should_Not_Update_Empty_Field(int index)
        {
            _bankFactory.Create().Persist();
            Bank bank = _bankFactory.Object;
            if (index == 1)
            {

                bank.Name = "";
            }
            else if (index == 2)
            {
                bank.ShortName = "";
            }
            Assert.Throws<InvalidDataException>(() => _bankService.SaveOrUpdate(bank));
        }

        #endregion

        #region Business Logic Test
        [Fact]
        public void Update_Should_Check_Duplicate_Name_Entry()
        {
            var bankFactory = _bankFactory.CreateMore(2).Persist();
            var bank1 = bankFactory.GetLastCreatedObjectList()[0];
            var bank2 = bankFactory.GetLastCreatedObjectList()[1];
            bank2.Name = bank1.Name;
            Assert.Throws<DuplicateEntryException>(() => _bankService.SaveOrUpdate(bank2));
        }

        [Fact]
        public void Update_Should_Check_Duplicate_ShortName_Entry()
        {
            var bankFactory = _bankFactory.CreateMore(2).Persist();
            var bank1 = bankFactory.GetLastCreatedObjectList()[0];
            var bank2 = bankFactory.GetLastCreatedObjectList()[1];
            bank2.ShortName = bank1.ShortName;
            Assert.Throws<DuplicateEntryException>(() => _bankService.SaveOrUpdate(bank2));
        }
        #endregion
    }
}
