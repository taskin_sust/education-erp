﻿using System;
using System.Configuration;
using System.Drawing;
using System.Text;

namespace UdvashERP.Services.Test 
{
    public static class AppConfigHelper
    {
        public static bool GetBoolean(string key)
        {
            return GetBoolean(key, false);
        }

        public static bool GetBoolean(string key, bool defaultValue)
        {
            bool result = defaultValue;
            if (bool.TryParse(GetString(key), out result) == false)
                result = defaultValue;
            return result;
        }


        public static int GetInt32(string key)
        {
            return GetInt32(key, 0);
        }

        public static int GetInt32(string key, int defaultValue)
        {
            int result = defaultValue;
            if (int.TryParse(GetString(key), out result) == false)
                result = defaultValue;
            return result;
        }


        public static decimal GetDecimal(string key)
        {
            return GetDecimal(key, 0);
        }

        public static decimal GetDecimal(string key, decimal defaultValue)
        {
            decimal result = defaultValue;
            if (decimal.TryParse(GetString(key), out result) == false)
                result = defaultValue;
            return result;
        }


        public static object GetEnum(string key, Type enumType, object defaultValue)
        {
            object result = null;
            try
            {
                result = Enum.Parse(enumType, GetString(key), true);
            }
            catch { }
            if (result == null)
                result = defaultValue;
            return result;
        }


        public static string GetString(string key)
        {
                return GetString(key, "");
        }

        public static string GetString(string key, string defaultValue)
        {
            if (ConfigurationManager.AppSettings[key] == null)
                return defaultValue;
            else
                return ConfigurationManager.AppSettings[key];
        }

       
    }
}
