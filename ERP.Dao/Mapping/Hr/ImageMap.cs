using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class ImageMap : BaseClassMap<Image,long> {
        
        public ImageMap() {
            Table("HR_Image");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			References(x => x.NomineeInfo).Column("NomineeInfoId");
			Map(x => x.Name).Column("Name");
			Map(x => x.Hash).Column("Hash");
        }
    }
}
