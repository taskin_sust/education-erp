﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.AttendanceDeviceTest
{
    public class AttendanceDeviceBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper CommonHelper;
        internal readonly AttendanceDeviceService _attendanceDeviceService;
        internal readonly AttendanceSynchronizerService _attendanceSynchronizerService;
        internal readonly CampusService _campusService;
        internal readonly List<long> IdList = new List<long>();

        private ISession _session;
        internal readonly AttendanceDeviceFactory _attendanceDeviceFactory;
        internal readonly AttendanceSynchronizerFactory _attendanceSynchronizerFactory;
        internal readonly CampusRoomFactory _campusRoomFactory;
        internal readonly CampusFactory _campusFactory;
        internal readonly AttendanceDeviceSynchronizationFactory _attendanceDeviceSynchronizationFactory;

        internal readonly string _name = DateTime.Now.ToString("yyyyMMddHHmmss") + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal readonly int _deviceMachineNo = GenerateNumaricGuid();

        #endregion

        public AttendanceDeviceBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _attendanceSynchronizerFactory = new AttendanceSynchronizerFactory(null, _session);
            _attendanceSynchronizerService = new AttendanceSynchronizerService(_session);
            _attendanceDeviceFactory = new AttendanceDeviceFactory(null, _session);
            _attendanceDeviceService = new AttendanceDeviceService(_session);
            _campusService = new CampusService(_session);
            _campusRoomFactory = new CampusRoomFactory(null, _session);
            _campusFactory = new CampusFactory(null, _session);
            _attendanceDeviceSynchronizationFactory = new AttendanceDeviceSynchronizationFactory(null, _session);
        }

        public void Dispose()
        {
            _attendanceDeviceSynchronizationFactory.CleanUp();
            _attendanceDeviceFactory.CleanUp();
            _campusRoomFactory.CleanUp();
            _campusFactory.Cleanup();
            branchFactory.Cleanup();
            base.Dispose();
        }
        private static int GenerateNumaricGuid()
        {
            byte[] buffer = Guid.NewGuid().ToByteArray();
            return BitConverter.ToInt32(buffer, 0);
        }
    }
}
