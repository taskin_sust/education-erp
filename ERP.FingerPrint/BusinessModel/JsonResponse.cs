﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.FingerPrint.BusinessModel
{
    public class JsonResponse
    {
        public bool IsSuccess { get; set; }
        public MemberInfo Data { get; set; }
    }
}
