﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Microsoft.Ajax.Utilities;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;

using UdvashERP.Services.Teachers;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class PreviousStudentAdmissionController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly IProgramService _programService;
        private readonly IBranchService _branchService;
        private readonly IBatchService _batchService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ISubjectService _subjectService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IStudentClassAttendanceService _classAttendanceService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ICampusService _campusService;
        private readonly IExamsService _examService;
        private readonly IStudentExamAttendanceService _studentExamAttendanceService;
        private readonly IStudentClassAttendanceService _studentClassAttendanceService;
        private readonly IOrganizationService _organizationService;
        private readonly ILectureService _lectureService;
        private readonly ITeacherService _teacherService;
        private readonly IInformationService _informationService;
        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _userMenu;
        public PreviousStudentAdmissionController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();

                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _batchService = new BatchService(session);
                _sessionService = new SessionService(session);
                _classAttendanceService = new StudentClassAttendanceService(session);
                _courseService = new CourseService(session);
                _subjectService = new SubjectService(session);
                _studentProgramService = new StudentProgramService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _campusService = new CampusService(session);
                _examService = new ExamsService(session);
                _studentExamAttendanceService = new StudentExamAttendanceService(session);
                _studentClassAttendanceService = new StudentClassAttendanceService(session);
                _organizationService = new OrganizationService(session);
                _lectureService = new LectureService(session);
                _teacherService = new TeacherService(session);
                _informationService = new InformationService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;

                _logger.Error(ex);
            }

        }
        #endregion

        #region Prev Student Admission Summary reportPreviousStudentAdmissionSummaryList
        private void ViewBagDataInitialization(PreviousStudentAdmissionReportViewModel previousStudentAdmissionInfo)
        {

            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            ViewBag.BranchFromName = "All";
            ViewBag.BranchToName = "All";
            ViewBag.OrganizationFromName = _organizationService.LoadById(previousStudentAdmissionInfo.OrganizationFromId).Name;
            ViewBag.OrganizationFromShortName = _organizationService.LoadById(previousStudentAdmissionInfo.OrganizationFromId).ShortName;
            ViewBag.ProgramFromName = _programService.GetProgram(previousStudentAdmissionInfo.ProgramFromId).Name;
            ViewBag.SessionFromName = _sessionService.LoadById(previousStudentAdmissionInfo.SessionFromId).Name;
            if (!previousStudentAdmissionInfo.BranchFromId.Contains(SelectionType.SelelectAll))
            {
                ViewBag.BranchFromName = string.Join(", ", _branchService.LoadBranch(null, null, null, previousStudentAdmissionInfo.BranchFromId.ToList()).Select(x => x.Name).ToList());
            }
            ViewBag.OrganizationToName = _organizationService.LoadById(previousStudentAdmissionInfo.OrganizationToId).Name;
            ViewBag.OrganizationToShortName = _organizationService.LoadById(previousStudentAdmissionInfo.OrganizationToId).ShortName;
            ViewBag.ProgramToName = _programService.GetProgram(previousStudentAdmissionInfo.ProgramToId).Name;
            ViewBag.SessionToName = _sessionService.LoadById(previousStudentAdmissionInfo.SessionToId).Name;
            ViewBag.stdType = previousStudentAdmissionInfo.StudentType;
            if (!previousStudentAdmissionInfo.BranchToId.Contains(SelectionType.SelelectAll))
            {
                ViewBag.BranchToName = string.Join(", ", _branchService.LoadBranch(null, null, null, previousStudentAdmissionInfo.BranchToId.ToList()).Select(x => x.Name).ToList());
            }
            ViewBag.previousStudentAdmissionInfo = previousStudentAdmissionInfo;
        }
        [HttpPost]
        public ActionResult PreviousStudentAdmissionSummaryReport(PreviousStudentAdmissionReportViewModel previousStudentAdmissionInfo)
        {
            ViewBagDataInitialization(previousStudentAdmissionInfo);
            return View();
        }

        private IList<PrevStudentSummary> getPrevStudentSummaries(IList<dynamic> queryResult)
        {
            try
            {
                IList<PrevStudentSummary> prevStudentAdmissionSummary = new List<PrevStudentSummary>();
                IList<PrevStudentSummary> retprevStudentAdmissionSummary = new List<PrevStudentSummary>();
                foreach (var obj in queryResult)
                {
                    IDictionary<string, object> dic;
                    var prevstdSummary = new PrevStudentSummary();
                    foreach (KeyValuePair<string, object> entry in dic = obj)
                    {
                        string defaultValue = entry.Key;
                        switch (defaultValue)
                        {
                            case "BranchName":
                                {
                                    prevstdSummary.Branch = (entry.Value != null) ?entry.Value.ToString(): "N/A";
                                    break;
                                }
                            case "Admitted":
                                {
                                    prevstdSummary.Admitted = (entry.Value != null) ? entry.Value.ToString() : "0";
                                    break;
                                }
                            case "Total":
                                {
                                    prevstdSummary.TotalStudent = (entry.Value != null) ? entry.Value.ToString() : "0";
                                    break;
                                }
                            default: { break; }

                        }
                    }
                    prevStudentAdmissionSummary.Add(prevstdSummary);
                }

                //var totalAdmittedStds = prevStudentAdmissionSummary.Sum(x => Convert.ToInt32(x.Admitted));
                foreach (var prevStudentSummary in prevStudentAdmissionSummary)
                {
                    prevStudentSummary.NotAdmitted = (Convert.ToInt64(prevStudentSummary.TotalStudent) - Convert.ToInt64(prevStudentSummary.Admitted)).ToString();
                    double admittedPercentage = Convert.ToInt64(prevStudentSummary.Admitted) * 100.0 / Convert.ToInt32(prevStudentSummary.TotalStudent);
                    admittedPercentage = Math.Round(admittedPercentage, 2);
                    prevStudentSummary.AdmittedInPercentage = admittedPercentage.ToString();
                    prevStudentSummary.NotAdmittedInPercentage = (100.0 - admittedPercentage).ToString();
                    retprevStudentAdmissionSummary.Add(prevStudentSummary);
                }

                //if (retprevStudentAdmissionSummary.Any())
                //    retprevStudentAdmissionSummary = retprevStudentAdmissionSummary.OrderByDescending(x => x.AdmittedInPercentage).ToList();
                return retprevStudentAdmissionSummary;
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public ActionResult PreviousStudentAdmissionSummaryList(int draw, int start, int length, PreviousStudentAdmissionReportViewModel previousStudentAdmissionInfo)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentProgramList = _studentProgramService.LoadStudentProgramListByCriteriaForPreviousStudentSummaryReport(start, length, _userMenu,
                    previousStudentAdmissionInfo.ProgramFromId, previousStudentAdmissionInfo.SessionFromId, previousStudentAdmissionInfo.BranchFromId, previousStudentAdmissionInfo.ProgramToId
                    , previousStudentAdmissionInfo.SessionToId
                    , previousStudentAdmissionInfo.BranchToId,
                    previousStudentAdmissionInfo.StudentType);
                IList<PrevStudentSummary> prevStudentSummaries = getPrevStudentSummaries(studentProgramList);
                //var recordsTotal =
                //    _studentProgramService.GetStudentsCountForPreviousStudentSummaryReport(_userMenu,
                //        previousStudentAdmissionInfo.ProgramFromId, previousStudentAdmissionInfo.SessionFromId,
                //        previousStudentAdmissionInfo.BranchFromId, previousStudentAdmissionInfo.ProgramToId
                //        , previousStudentAdmissionInfo.SessionToId
                //        , previousStudentAdmissionInfo.BranchToId,
                //        previousStudentAdmissionInfo.StudentType);
                var recordsTotal = 50;
                var recordsFiltered = 50;
                var data = new List<object>();
                int sl = start + 1;
                foreach (var sp in prevStudentSummaries)
                {
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    str.Add(sp.Branch);
                    str.Add(sp.TotalStudent);
                    if (previousStudentAdmissionInfo.StudentType == StudentType.Admitted)
                    {
                        str.Add(sp.Admitted);
                        str.Add(sp.AdmittedInPercentage);
                    }
                    else
                    {
                        str.Add(sp.NotAdmitted);
                        str.Add(sp.NotAdmittedInPercentage);
                    }

                    sl++;
                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = prevStudentSummaries.Count,
                    recordsFiltered = prevStudentSummaries.Count,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                var data = new List<object>();
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });
            }
        }

        #endregion

        #region Previous Student Admission Report
        public ActionResult PreviousStudentAdmissionReport()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //ViewBag.OrganizationId = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                ViewBag.studentType = new SelectList(new List<SelectListItem>()
                                                               {
                                                                   new SelectListItem() {Value = StudentType.Admitted.ToString(), Text = "Admitted"},
                                                                   new SelectListItem() {Value =StudentType.Nonadmitted.ToString() , Text = "Not Admitted"}
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
                ViewBag.InformationViewList = new SelectList(new List<SelectListItem>()
                                                               {
                                                                   new SelectListItem() {Value = "Program Roll", Text = "Program Roll"},
                                                                   new SelectListItem() {Value ="Student's Name" , Text = "Student's Name"},
                                                                    new SelectListItem() {Value = "Nick Name", Text = "Nick Name"},
                                                                   new SelectListItem() {Value ="Mobile Number (Student)" , Text = "Mobile Number (Student)"},
                                                                    new SelectListItem() {Value = "Mobile Number (Father)", Text = "Mobile Number (Father)"},
                                                                   new SelectListItem() {Value ="Mobile Number (Mother)" , Text = "Mobile Number (Mother)"},
                                                                    new SelectListItem() {Value = "Father's Name", Text = "Father's Name"},
                                                                   new SelectListItem() {Value ="Institute Name" , Text = "Institute Name"},
                                                                    new SelectListItem() {Value = "Email", Text = "Email"},
                                                                   new SelectListItem() {Value ="Branch" , Text = "Branch"},
                                                                    new SelectListItem() {Value = "Batch", Text = "Batch"},
                                                                   new SelectListItem() {Value ="Version of Study" , Text = "Version of Study"},
                                                                    new SelectListItem() {Value = "Gender", Text = "Gender"},
                                                                   new SelectListItem() {Value ="Religion" , Text = "Religion"},
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }

            return View();
        }
        [HttpPost]
        public ActionResult GeneratePreviousStudentAdmissionReport(PreviousStudentAdmissionReportViewModel previousStudentAdmissionInfo)
        {
            try
            {
                ViewBagDataInitialization(previousStudentAdmissionInfo);
                //ViewBag.PageSize = Constants.PageSize;
                //ViewBag.CurrentPage = 1;
                //ViewBag.BranchFromName = "All";
                //ViewBag.BranchToName = "All";
                //ViewBag.OrganizationFromName = _organizationService.LoadById(previousStudentAdmissionInfo.OrganizationFromId).Name;
                //ViewBag.OrganizationFromShortName = _organizationService.LoadById(previousStudentAdmissionInfo.OrganizationFromId).ShortName;
                //ViewBag.ProgramFromName = _programService.GetProgram(previousStudentAdmissionInfo.ProgramFromId).Name;
                //ViewBag.SessionFromName = _sessionService.LoadById(previousStudentAdmissionInfo.SessionFromId).Name;
                //if (!previousStudentAdmissionInfo.BranchFromId.Contains(SelectionType.SelelectAll))
                //{
                //    ViewBag.BranchFromName = string.Join(", ", _branchService.LoadBranch(null,null,null,previousStudentAdmissionInfo.BranchFromId.ToList()).Select(x => x.Name).ToList());
                //}
                //ViewBag.OrganizationToName = _organizationService.LoadById(previousStudentAdmissionInfo.OrganizationToId).Name;
                //ViewBag.OrganizationToShortName = _organizationService.LoadById(previousStudentAdmissionInfo.OrganizationToId).ShortName;
                //ViewBag.ProgramToName = _programService.GetProgram(previousStudentAdmissionInfo.ProgramToId).Name;
                //ViewBag.SessionToName = _sessionService.LoadById(previousStudentAdmissionInfo.SessionToId).Name;
                //if (!previousStudentAdmissionInfo.BranchToId.Contains(SelectionType.SelelectAll))
                //{
                //    ViewBag.BranchToName = string.Join(", ", _branchService.LoadBranch(null,null,null,previousStudentAdmissionInfo.BranchToId.ToList()).Select(x => x.Name).ToList());
                //}
                //ViewBag.previousStudentAdmissionInfo = previousStudentAdmissionInfo;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return View();
        }


        #region Render Data Table
        [HttpPost]
        public ActionResult PreviousStudentAdmissionList(int draw, int start, int length, PreviousStudentAdmissionReportViewModel previousStudentAdmissionInfo)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentProgramList = _studentProgramService.LoadStudentProgramListByCriteriaForPreviousStudentReport(start, length, _userMenu,
                    previousStudentAdmissionInfo.ProgramFromId, previousStudentAdmissionInfo.SessionFromId, previousStudentAdmissionInfo.BranchFromId, previousStudentAdmissionInfo.ProgramToId
                    , previousStudentAdmissionInfo.SessionToId
                    , previousStudentAdmissionInfo.BranchToId,
                    previousStudentAdmissionInfo.StudentType);
                var recordsTotal =
                    _studentProgramService.GetStudentsCountForPreviousStudentReport(_userMenu,
                        previousStudentAdmissionInfo.ProgramFromId, previousStudentAdmissionInfo.SessionFromId,
                        previousStudentAdmissionInfo.BranchFromId, previousStudentAdmissionInfo.ProgramToId
                        , previousStudentAdmissionInfo.SessionToId
                        , previousStudentAdmissionInfo.BranchToId,
                        previousStudentAdmissionInfo.StudentType);
                var recordsFiltered = recordsTotal;
                var data = new List<object>();
                int sl = start + 1;
                //studentProgramList = studentProgramList.OrderBy(x => x.PrnNo).ToList();
                foreach (var sp in studentProgramList)
                {
                    var studentProgram = _studentProgramService.GetStudentProgram(sp.Id);

                    var str = new List<string>();
                    str.Add(sl.ToString());

                    if (previousStudentAdmissionInfo.InformationViewList != null)
                    {
                        foreach (string informationView in previousStudentAdmissionInfo.InformationViewList)
                        {

                            switch (informationView)
                            {
                                case "Program Roll":
                                    str.Add(studentProgram.PrnNo);
                                    break;
                                case "Student's Name":
                                    if (studentProgram.Student != null && studentProgram.Student.FullName != null)
                                    {
                                        str.Add(studentProgram.Student.FullName);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Nick Name":
                                    if (studentProgram.Student != null && studentProgram.Student.NickName != null)
                                    {
                                        str.Add(studentProgram.Student.NickName);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Mobile Number (Student)":
                                    if (studentProgram.Student != null && studentProgram.Student.Mobile != null)
                                    {
                                        str.Add(studentProgram.Student.Mobile);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Mobile Number (Father)":
                                    if (studentProgram.Student != null && studentProgram.Student.GuardiansMobile1 != null)
                                    {
                                        str.Add(studentProgram.Student.GuardiansMobile1);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Mobile Number (Mother)":
                                    if (studentProgram.Student != null && studentProgram.Student.GuardiansMobile2 != null)
                                    {
                                        str.Add(studentProgram.Student.GuardiansMobile2);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Father's Name":
                                    if (studentProgram.Student != null && studentProgram.Student.FatherName != null)
                                    {
                                        str.Add(studentProgram.Student.FatherName);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Institute Name":
                                    if (studentProgram.Student.StudentPrograms[0].Institute != null)
                                    {
                                        str.Add(studentProgram.Student.StudentPrograms[0].Institute.Name);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Email":
                                    if (studentProgram.Student != null && studentProgram.Student.Email != null)
                                    {
                                        str.Add(studentProgram.Student.Email);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Branch":
                                    if (studentProgram.Batch != null && studentProgram.Batch.Branch != null)
                                    {
                                        str.Add(studentProgram.Batch.Branch.Name);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Batch":
                                    if (studentProgram.Batch != null && studentProgram.Batch.Name != null)
                                    {
                                        str.Add(studentProgram.Batch.Name);
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Version of Study":
                                    if (studentProgram.VersionOfStudy != null)
                                    {
                                        str.Add(((VersionOfStudy)studentProgram.VersionOfStudy).ToString());
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Gender":
                                    if (studentProgram.Student != null && studentProgram.Student.Gender != null)
                                    {
                                        str.Add(((Gender)studentProgram.Student.Gender).ToString());
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                case "Religion":
                                    if (studentProgram.Student != null && studentProgram.Student.Religion != null)
                                    {
                                        str.Add(((Religion)studentProgram.Student.Religion).ToString());
                                        break;
                                    }
                                    else
                                    {
                                        str.Add("");
                                        break;
                                    }
                                default:
                                    break;
                            }
                        }
                    }
                    sl++;
                    data.Add(str);
                }



                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                var data = new List<object>();
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });
            }

        }
        #endregion
        #endregion

        #region Export Previous Student Admission Report To Excel
        public ActionResult ExportPreviousStudentAdmissionReport(string organizationFromHeader, string programFromHeader, string sessionFromHeader, string branchFromHeader, string organizationToHeader, string programToHeader, string sessionToHeader, string branchToHeader, long programIdFrom,
           long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo,
           int studentType, string[] informationViewList, bool showError = false)
        {

            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentProgramList = _studentProgramService.LoadStudentProgramListByCriteriaForPreviousStudentReport(0, 0, _userMenu,
                    programIdFrom, sessionIdFrom,
                    branchIdFrom, programIdTo
                    , sessionIdTo
                    , branchIdTo,
                    studentType);
                //studentProgramList = studentProgramList.OrderBy(x => x.PrnNo).ToList();
                var headerList = new List<string>();
                var str = "                                                            ";
                headerList.Add("Previous Student Admission Report");
                headerList.Add("From:");
                headerList.Add("Organiztion : " + organizationFromHeader);
                headerList.Add("Program : " + programFromHeader);
                headerList.Add("Session : " + sessionFromHeader);
                headerList.Add("Branch : " + branchFromHeader);
                headerList.Add("");
                // headerList.Add("");
                headerList.Add("To:");
                headerList.Add("Organiztion : " + organizationToHeader);
                headerList.Add("Program : " + programToHeader);
                headerList.Add("Session : " + sessionToHeader);
                headerList.Add("Branch : " + branchToHeader);
                var footerList = new List<string>();
                footerList.Add("");
                //footerList.Add("Developed By OnnoRokom Software Ltd.");

                var columnList = new List<string>();
                if (informationViewList != null)
                {
                    if (informationViewList.Contains("Program Roll"))
                    {
                        columnList.Add("Program Roll");
                    }
                    if (informationViewList.Contains("Student's Name"))
                    {
                        columnList.Add("Full Name");
                    }
                    if (informationViewList.Contains("Nick Name"))
                    {
                        columnList.Add("Nick Name");
                    }
                    if (informationViewList.Contains("Mobile Number (Student)"))
                    {
                        columnList.Add("Mobile Number (Student)");
                    }
                    if (informationViewList.Contains("Mobile Number (Father)"))
                    {
                        columnList.Add("Mobile Number (Father)");
                    }
                    if (informationViewList.Contains("Mobile Number (Mother)"))
                    {
                        columnList.Add("Mobile Number (Mother)");
                    }
                    if (informationViewList.Contains("Father's Name"))
                    {
                        columnList.Add("Father's Name");
                    }
                    if (informationViewList.Contains("Institute Name"))
                    {
                        columnList.Add("Institute Name");
                    }
                    if (informationViewList.Contains("Email"))
                    {
                        columnList.Add("Email");
                    }
                    if (informationViewList.Contains("Branch"))
                    {
                        columnList.Add("Branch");
                    }
                    if (informationViewList.Contains("Batch"))
                    {
                        columnList.Add("Batch");
                    }
                    if (informationViewList.Contains("Version of Study"))
                    {
                        columnList.Add("Version of Study");
                    }
                    if (informationViewList.Contains("Gender"))
                    {
                        columnList.Add("Gender");

                    }
                    if (informationViewList.Contains("Religion"))
                    {
                        columnList.Add("Religion");
                    }

                }

                var studentExcelList = new List<List<object>>();
                foreach (var x in studentProgramList)
                {
                    var xSes = _studentProgramService.GetStudentProgram(x.Id);
                    var xlsRow = new List<object>();
                    if (informationViewList != null)
                    {
                        if (informationViewList.Contains("Program Roll"))
                        {
                            if (xSes.PrnNo != null)
                                xlsRow.Add(xSes.PrnNo);
                        }
                        if (informationViewList.Contains("Student's Name"))
                        {
                            xlsRow.Add(xSes.Student.FullName ?? "");
                        }
                        if (informationViewList.Contains("Nick Name"))
                        {
                            xlsRow.Add(xSes.Student.NickName ?? "");
                        }
                        if (informationViewList.Contains("Mobile Number (Student)"))
                        {
                            xlsRow.Add(xSes.Student.Mobile ?? "");
                        }
                        if (informationViewList.Contains("Mobile Number (Father)"))
                        {
                            xlsRow.Add(xSes.Student.GuardiansMobile1 ?? "");
                        }
                        if (informationViewList.Contains("Mobile Number (Mother)"))
                        {
                            xlsRow.Add(xSes.Student.GuardiansMobile2 ?? "");
                        }
                        if (informationViewList.Contains("Father's Name"))
                        {
                            xlsRow.Add(xSes.Student.FatherName ?? "");
                        }
                        if (informationViewList.Contains("Institute Name"))
                        {
                            if (xSes.Institute != null)
                                xlsRow.Add(xSes.Institute.Name ?? "");
                            else
                                xlsRow.Add("N/A");
                        }
                        if (informationViewList.Contains("Email"))
                        {
                            xlsRow.Add(xSes.Student.Email ?? "");
                        }
                        if (informationViewList.Contains("Branch"))
                        {
                            if (xSes.Batch != null)
                                xlsRow.Add(xSes.Batch.Branch.Name ?? "");
                        }
                        if (informationViewList.Contains("Batch"))
                        {
                            if (xSes.Batch != null)
                                xlsRow.Add(xSes.Batch.Name ?? "");
                        }
                        if (informationViewList.Contains("Version of Study"))
                        {
                            xlsRow.Add(xSes.VersionOfStudy != null ? ((VersionOfStudy)xSes.VersionOfStudy).ToString() : "--");
                        }
                        if (informationViewList.Contains("Gender"))
                        {
                            if (xSes.Student != null && xSes.Student.Gender != null)
                            {
                                xlsRow.Add(((Gender)xSes.Student.Gender).ToString());
                            }
                            else
                            {
                                xlsRow.Add("--");
                            }
                        }
                        if (informationViewList.Contains("Religion"))
                        {
                            if (xSes.Student != null && xSes.Student.Religion != null)
                            {
                                xlsRow.Add(((Religion)xSes.Student.Religion).ToString());
                            }
                            else
                            {
                                xlsRow.Add("--");
                            }
                        }
                    }
                    studentExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "previous-student-admission-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (showError)
                    ViewBag.AutoCloseMsg = ex.ToString();
                return View("AutoClose");
            }
        }

        #endregion

        #region Ajax Request Function
        [HttpPost]
        public ActionResult GetStudentsCountForPreviousStudentReport(long programIdFrom, long sessionIdFrom, long[] branchIdFrom, long programIdTo, long sessionIdTo, long[] branchIdTo,
            int studentType)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var totalStudentCount =
                       _studentProgramService.GetStudentsCountForPreviousStudentReport(_userMenu,
                           programIdFrom, sessionIdFrom,
                           branchIdFrom, programIdTo
                           , sessionIdTo
                           , branchIdTo,
                           studentType);
                return Json(new { studentCount = totalStudentCount, isSuccess = true });
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new { isSuccess = false });
            }

        }
        [HttpPost]
        public JsonResult GetProgramByOrg(string organizatonId, bool isAuthorized = false)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<SelectListItem> programSelectList = new List<SelectListItem>();
                if (isAuthorized)
                {
                    programSelectList = new SelectList(_programService.LoadAuthorizedProgram(userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(organizatonId))), "Id", "Name").ToList();
                }
                else
                {
                    programSelectList = new SelectList(_programService.LoadProgram(_commonHelper.ConvertIdToList(Convert.ToInt64(organizatonId))), "Id", "Name").ToList();
                }
                return Json(new { programList = programSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Program load failed";
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult GetSession(long programId, bool isAuthorized = false)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (isAuthorized)
                {
                    ViewBag.SessionList = _sessionService.LoadAuthorizedSession(userMenu, null, _commonHelper.ConvertIdToList(programId));
                }
                else
                {
                    ViewBag.SessionList = _sessionService.LoadSession(null, _commonHelper.ConvertIdToList(programId));
                }
                return View("Partial/_SessionList");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult GetBranch(long programId, long sessionId, long organizationId, bool isAuthorized = false)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var branchList = new List<SelectListItem>();
                if (isAuthorized)
                {
                    branchList = (new SelectList(_branchService.LoadAuthorizedBranch(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId)).ToList(), "Id", "Name")).ToList();
                }
                else
                {
                    //branchList =(new SelectList(_branchService.LoadBranchByProgramSession(programId,sessionId).ToList(),"Id", "Name")).ToList();
                    branchList = (new SelectList(_branchService.LoadBranch(null, _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId)).ToList(), "Id", "Name")).ToList();
                }
                return Json(new { branchList = branchList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

    }
}
