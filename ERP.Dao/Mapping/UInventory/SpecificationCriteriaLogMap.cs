using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class SpecificationCriteriaLogMap : BaseClassMap<SpecificationCriteriaLog, long>
    {
        public SpecificationCriteriaLogMap()
        {
            Table("UINV_SpecificationCriteriaLog");
            LazyLoad();
            References(x => x.SpecificationCriteria).Column("SpecificationCriteriaId");
            Map(x => x.Rank).Column("Rank").Not.Nullable();
            Map(x => x.Name).Column("Name").Not.Nullable();
            Map(x => x.Controltype).Column("ControlType").Not.Nullable();
            Map(x => x.Criteriaoptions).Column("CriteriaOptions");
        }
    }
}
