using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.UInventory
{
    public interface IItemGroupDao : IBaseDao<ItemGroup, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        IList<ItemGroup> LoadItemGroup(int start, int length, string orderBy, string orderDir, string name, string status);
        IList<ItemGroup> LoadItemGroup(List<long> itemGroupIdList = null);

        #endregion

        #region Others Function

        bool IsDuplicateItemGroup(out string fieldName, string name, long id);
        int LoadItemGroupCount(string orderBy, string orderDir, string name, string status);

        #endregion

    }

    public class ItemGroupDao : BaseDao<ItemGroup, long>, IItemGroupDao
    {

        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<ItemGroup> LoadItemGroup(int start, int length, string orderBy, string orderDir, string name, string status)
        {
            ICriteria criteria = GetItemGroupCriteria(name, status);
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }
            return criteria.SetFirstResult(start).SetMaxResults(length).List<ItemGroup>();
        }

        public IList<ItemGroup> LoadItemGroup(List<long> itemGroupIdList = null)
        {
            ICriteria criteria = Session.CreateCriteria<ItemGroup>().Add(Restrictions.Eq("Status", ItemGroup.EntityStatus.Active));
            if (itemGroupIdList != null && itemGroupIdList.Any() && !itemGroupIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("Id", itemGroupIdList));
            }
            return criteria.List<ItemGroup>();
        }

        #endregion

        #region Others Function

        public bool IsDuplicateItemGroup(out string fieldName, string name, long id)
        {
            fieldName = "";
            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<ItemGroup>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", ItemGroup.EntityStatus.Delete)));

            if (id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (name != null)
            {
                criteria.Add(Restrictions.Eq("Name", name));
                fieldName = "Name";
            }
            var itemGroupList = criteria.List<ItemGroup>();
            if (itemGroupList != null && itemGroupList.Count > 0)
            {
                checkValue = true;
            }
            return checkValue;
        }

        public int LoadItemGroupCount(string orderBy, string orderDir, string name, string status)
        {
            ICriteria criteria = GetItemGroupCriteria(name, status);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function

        private ICriteria GetItemGroupCriteria(string name, string status)
        {
            ICriteria criteria = Session.CreateCriteria<ItemGroup>().Add(Restrictions.Not(Restrictions.Eq("Status", ItemGroup.EntityStatus.Delete)));
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            return criteria;
        }

        #endregion

    }
}
