﻿using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.LectureSettingTest
{
    interface ILectureSettingDeleteTest
    {
        #region Basic Test

        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "LectureSetting")]
    public class LectureSettingDeleteTest : LectureSettingBaseTest, ILectureSettingDeleteTest
    {
        #region Basic Test
        [Fact]
        public void Should_Delete_Successfully()
        {
            var lectureSettingFactory =
                _lectureSettingFactory.Create().WithCourseSubject()
                    .Persist();
            LectureSettings lectureSettings = _lectureSettingService.LoadById(lectureSettingFactory.Object.Id);
            var lectureSettingId = lectureSettings.Id;
            lectureSettings.Status = Lecture.EntityStatus.Delete;
            _lectureService.Delete(lectureSettings);
            lectureSettings = _lectureSettingService.LoadById(lectureSettingFactory.Object.Id);
            IList<Lecture> lectures = _lectureService.LoadLecture(lectureSettingId);
            Assert.Null(lectureSettings);
            //Assert.Null(lectures);
            _TestBaseService.DeleteProgramSessionSubject(lectureSettingFactory.Object.Course.Program.Id, lectureSettingFactory.Object.Course.RefSession.Id);
            _TestBaseService.DeleteLecture(lectures);
        }

        //[Fact]
        //public void LectureSetting_Delete_Should_Throw_Dependency_Exception()
        //{
        //    var lectureSettingFactory =
        //        _lectureSettingFactory.Create().WithCourseSubject()
        //            .Persist();
        //    LectureSettings lectureSettings = _lectureSettingService.LoadById(lectureSettingFactory.Object.Id);
        //    var lectureSettingId = lectureSettings.Id;
        //    var lectures = _lectureService.LoadLecture(lectureSettingId);
        //    lectureSettings.Lectures = lectures;
        //    lectureSettings.Lectures[0].StudentClassAttendence = new List<StudentClassAttendence>()
        //    {
        //        new StudentClassAttendence()
        //    };
        //    Assert.Throws<DependencyException>(() => _lectureService.Delete(lectureSettings));
        //    lectures = _lectureService.LoadLecture(lectureSettingId);
        //    _TestBaseService.DeleteProgramSessionSubject(lectureSettingFactory.Object.Course.Program.Id, lectureSettingFactory.Object.Course.RefSession.Id);
        //    _TestBaseService.DeleteLecture(lectures);
        //}
        
        #endregion

    }
}
