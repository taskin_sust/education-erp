﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.AssignSupplierTest
{
    public interface ISupplierItemSaveOrUpdateTest
    {
        #region Basic Test

        void Should_Return_Save_Null_Exception();
        void Should_Return_Save_Successfully();

        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "SupplierItem")]
    public class SupplierItemSaveOrUpdateTest : SupplierItemBaseTest, ISupplierItemSaveOrUpdateTest
    {
        [Fact]
        public void Should_Return_Save_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _SupplierItemService.SaveOrUpdate(null, null));
        }

        [Fact]
        public void Should_Return_Save_Successfully()
        {
            var si = _SupplierItemFactory.Create().WithItem().WithSupplier().Object;
            var itemAry = new string[] { si.Item.Id.ToString() };
            var supplierAry = new string[] { si.Supplier.Id.ToString() };
            Assert.True(_SupplierItemService.SaveOrUpdate(itemAry, supplierAry));
        }
    }
}
