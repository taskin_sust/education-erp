﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.AreaControllersTest
{
    interface IAreaControllersDeleteTest
    {
        #region Basic Test
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "AreaControllers")]
    public class AreaControllersDeleteTest : AreaControllersBaseTest, IAreaControllersDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            AreaControllersFactory areaControllersFactory =
                    _areaControllersFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithArea(_areaName)
                    .Persist();
            AreaControllers areaControllersObj = _areaControllersService.LoadById(areaControllersFactory.Object.Id);
            var success = _areaControllersService.Delete(areaControllersObj.Id, areaControllersObj);
            Assert.True(success);
            areaControllersObj = _areaControllersService.LoadById(areaControllersFactory.Object.Id);
            Assert.Null(areaControllersObj);

        }

        #endregion
    }
}
