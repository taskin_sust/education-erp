﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.HolidaySettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "HolidaySetting")]
    public class HolidaySettingUpdateTest : HolidaySettingBaseTest
    {

        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var factoryObj = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            factoryObj.Object.HolidayType = (int)HolidayTypeEnum.Management;
            factoryObj.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            factoryObj.Persist();
            var holidaySetting = _holidaySettingService.GetHolidaySetting(factoryObj.Object.Id);
            Assert.Throws<NullObjectException>(() => _holidaySettingService.Edit(holidaySetting.Id, null));
        }

        [Fact]
        public void Update_Null_Object()
        {
            var hrHolidaySetting = new HolidaySetting();
            Assert.Throws<NullObjectException>(() => _holidaySettingService.Edit(hrHolidaySetting.Id, hrHolidaySetting));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var factoryObj = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            factoryObj.Object.HolidayType = (int)HolidayTypeEnum.Management;
            factoryObj.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            factoryObj.Persist();
            factoryObj.Object.Organization = _organizationService.LoadById(2);
            factoryObj.Object.Name = _name + "  DEF";
            factoryObj.Object.HolidayType = (int)HolidayTypeEnum.Gazetted;
            Assert.True(_holidaySettingService.Edit(factoryObj.Object.Id, factoryObj.Object));
        }
        #endregion

        #region Business Logic Test

        [Fact]
        public void Update_Should_Check_UpdateDate()
        {
            var factoryObj = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            factoryObj.Object.HolidayType = (int)HolidayTypeEnum.Management;
            factoryObj.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            factoryObj.Persist();
            var holidaySetting = _holidaySettingService.GetHolidaySetting(factoryObj.Object.Id);
            var newHoliday = new HolidaySetting()
            {
                Name = holidaySetting.Name + "  DEF  IJK",
                Organization = _organizationService.LoadById(Convert.ToInt64(1)),
                DateFrom = DateTime.Now.AddDays(-1),
                DateTo = DateTime.Now,
                HolidayType = (int)HolidayTypeEnum.Gazetted,
                RepeatationType = (int)HolidayRepeatationType.Once
            };
            Assert.Throws<InvalidDataException>(() => _holidaySettingService.Edit(holidaySetting.Id, newHoliday));
        }

        [Fact]
        public void Yearly_To_Once_Update_Check()
        {
            var holidaySettingFactory = _holidaySettingFactory.Create().WithOrganization();
            holidaySettingFactory.Object.HolidayType = (int)HolidayTypeEnum.Management;
            holidaySettingFactory.Object.RepeatationType = (int)HolidayRepeatationType.Yearly;
            _holidaySettingService.Save(holidaySettingFactory.Object);
            var holidaySetting = _holidaySettingService.GetHolidaySetting(holidaySettingFactory.Object.Id);
            holidaySetting.RepeatationType = (int)HolidayRepeatationType.Once;
            holidaySetting.RepeatationType = (int)HolidayTypeEnum.Gazetted;
            holidaySetting.CreateBy = 1;
            bool isSuccess = _holidaySettingService.Edit(holidaySetting.Id, holidaySetting);
            var yearlyHoliday = _YearlyHolidayService.GetYearlyHoliday(holidaySettingFactory.Object.Name, holidaySettingFactory.Object.Organization.Id);
            _yearlyHolidayFactory.Cleanup(yearlyHoliday);
            Assert.True(isSuccess);

        }

        [Fact]
        public void Once_To_Yearly_Update_Check()
        {
            var holidaySettingFactory = _holidaySettingFactory.Create().WithOrganization();
            holidaySettingFactory.Object.HolidayType = (int)HolidayTypeEnum.Gazetted;
            holidaySettingFactory.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            _holidaySettingService.Save(holidaySettingFactory.Object);
            var holidaySetting = _holidaySettingService.GetHolidaySetting(holidaySettingFactory.Object.Id);
            holidaySetting.RepeatationType = (int)HolidayRepeatationType.Yearly;
            holidaySetting.RepeatationType = (int)HolidayTypeEnum.Management;
            holidaySetting.CreateBy = 1;
            bool isSuccess = _holidaySettingService.Edit(holidaySetting.Id, holidaySetting);
            var yearlyHoliday = _YearlyHolidayService.GetYearlyHoliday(holidaySettingFactory.Object.Name, holidaySettingFactory.Object.Organization.Id, false);
            _yearlyHolidayFactory.Cleanup(yearlyHoliday);
            Assert.True(isSuccess);

        }

        [Fact]
        public void Update_Should_Check_Property()
        {
            var dFactory1 = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            dFactory1.Object.HolidayType = (int)HolidayTypeEnum.Management;
            dFactory1.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            dFactory1.Persist();
            dFactory1.Object.RepeatationType = 0;
            Assert.Throws<ValueNotAssignedException>(() => _holidaySettingService.Edit(dFactory1.Object.Id, dFactory1.Object));
        }

        [Fact]
        public void ValueNotAssignedException_For_Organization_Null()
        {
            var dFactory1 = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            dFactory1.Object.HolidayType = (int)HolidayTypeEnum.Management;
            dFactory1.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            dFactory1.Persist();
            dFactory1.Object.Organization = null;
            Assert.Throws<ValueNotAssignedException>(() => _holidaySettingService.Edit(dFactory1.Object.Id, dFactory1.Object));
        }
        
        [Fact]
        public void ValueNotAssignedException_For_HolidayType_Invalid()
        {
            var dFactory1 = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            dFactory1.Object.HolidayType = (int)HolidayTypeEnum.Management;
            dFactory1.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            dFactory1.Persist();
            dFactory1.Object.HolidayType = 0;
            Assert.Throws<ValueNotAssignedException>(() => _holidaySettingService.Edit(dFactory1.Object.Id, dFactory1.Object));
        }


        [Fact]
        public void ValueNotAssignedException_For_Name_Empty()
        {
            var dFactory1 = _holidaySettingFactory.Create().WithOrganization().WithNameAndRank(_name, 1);
            dFactory1.Object.HolidayType = (int)HolidayTypeEnum.Management;
            dFactory1.Object.RepeatationType = (int)HolidayRepeatationType.Once;
            dFactory1.Persist();
            dFactory1.Object.Name = string.Empty;
            Assert.Throws<ValueNotAssignedException>(() => _holidaySettingService.Edit(dFactory1.Object.Id, dFactory1.Object));
        }


        #endregion

    }
}
