﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MaterialTest
{
    public interface IMaterialSaveTest
    {
        #region basic test
        [Fact]
        void Save_Should_Return_Null_Exception();

        [Fact]
        void Should_Save_Successfully();

        [Theory]
        [MemberData("MaterialIndex", MemberType = typeof(MaterialSaveTest))]
        void Save_Should_Check_Invalid_Entry(int i);
        #endregion

        #region business logic test
        [Fact]
        void Save_Should_Check_Duplicate_Name_Entry();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Material")]
    public class MaterialSaveTest : MaterialBaseTest, IMaterialSaveTest
    {
        #region Test Related Properties

        public static IEnumerable<object[]> MaterialIndex
        {
            get
            {
                var indexList = new List<object[]>();
                for (int i = 0; i < 5; i++)
                    indexList.Add(new object[] { i });
                return indexList;
            }
        }
        #endregion

        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _materialService.MaterialSaveOrUpdate(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var materialFactory =
                _materialFactory.Create().Persist();
            Material material = _materialService.GetMaterial(materialFactory.Object.Id);
            Assert.NotNull(material);
            Assert.True(material.Id > 0);
        }

        [Theory]
        [MemberData("MaterialIndex", MemberType = typeof(MaterialSaveTest))]
        public void Save_Should_Check_Invalid_Entry(int i)
        {
            MaterialFactory materialFactory =
                _materialFactory.Create();
            var oldMaterial = materialFactory.Object;
            if (i == 0)
            {
                oldMaterial.Name = "";
            }
            if (i == 1)
            {
                oldMaterial.ShortName = "";
            }
            if (i == 2)
            {
                oldMaterial.Program = null;
            }
            if (i == 3)
            {
                oldMaterial.Session = null;
            }
            if (i == 4)
            {
                oldMaterial.MaterialType = null;
            }
            Assert.Throws<InvalidDataException>(() => materialFactory.Persist());
        }
        #endregion

        #region Business logic test

        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            var materialFactory =
                _materialFactory.Create().Persist();
            var material1 = materialFactory.Object;
            var singleMaterial = _materialFactory.Object;
            var organization = singleMaterial.Organization;
            var program = singleMaterial.Program;
            var sessionObj = singleMaterial.Session;
            var materialType = singleMaterial.MaterialType;
            var materialFactory2 =
                _materialFactory.Create(organization,program,sessionObj,materialType);
            materialFactory2.Object.Name = material1.Name;
            materialFactory2.Object.ShortName = material1.ShortName;
            Assert.Throws<DuplicateEntryException>(() => materialFactory2.Persist());
        }
        #endregion
    }
}
