﻿namespace Synchronizer.Updater.BusinessRule
{
    public static class SoftwareInfo
    {
        public static string ApplicationName = "Synchronizer Updater";
        public static string ApplicationVersion = "3.0.0";

        public const string DeviceSynchronizerAppName = "Attendance Synchronizer";
        public static string DeviceSynchronizerExeName = "DeviceSynchronizer.exe";
        public static string DeviceSynchronizerProcessName = "DeviceSynchronizer";

        public static bool AutomaticExit = false;
    }
}
