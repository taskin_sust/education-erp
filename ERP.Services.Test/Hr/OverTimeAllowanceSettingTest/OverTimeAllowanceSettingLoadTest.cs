﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.OverTimeAllowanceSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "OverTimeAllowanceSetting")]
    public class OverTimeAllowanceSettingLoadTest : OverTimeAllowanceSettingBaseTest
    {
        [Fact]
        public void Should_Return_Expected_Data_Successfully()
        {
            const int count = 1;
            var orgFactory = _organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            _overtimeAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[0]).Persist();
            var returnList = _overtimeAllowanceSettingService.LoadOvertimeAllowanceSetting(userMenu, 0, 10, "Organization", "DESC", 0, "", "");
            Assert.Equal(returnList.Count, count);
        }

        [Fact]
        public void Should_Return_Expected_Count()
        {
            const int count = 1;
            var orgFactory = _organizationFactory.CreateMore(2).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            _overtimeAllowanceSettingFactory.CreateMore(count, orgFactory.ObjectList[0]).Persist();
            int returnValue = _overtimeAllowanceSettingService.GetOvertimeAllowanceSettingCount(userMenu,"Organization", "DESC",0,"","");
            Assert.Equal(returnValue, count);
        }

        [Fact]
        public void Should_Return_Selected_Data()
        {
            _organizationFactory.Create().Persist();
            var userMenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object));
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var entity = _overtimeAllowanceSettingService.GetOvertimeAllowanceSetting(userMenu, _overtimeAllowanceSettingFactory.Object.Id);
            Assert.NotNull(entity);
            Assert.Equal(entity.Id, _overtimeAllowanceSettingFactory.Object.Id);
        }

        #region Business Logic Test

        [Fact]
        public void Should_Return_Null_Object_Exception_From_Invalid_Data()
        {
           _organizationFactory.Create().Persist();
           var userMenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object));
           _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            Assert.Throws<NullObjectException>(() => _overtimeAllowanceSettingService.GetOvertimeAllowanceSetting(userMenu, 0));
        }

        [Fact]
        public void InvalidDataException_Organization_Mismatch_Overtime_Allowance_Settings_Load_By_Id()
        {
            var organizationOne = _organizationFactory.CreateMore(1).Persist();
            var organizationTwo = _organizationFactory.Create().Persist();
            var userMenu = BuildUserMenu(organizationTwo.SingleObjectList);
            var objFactory = _overtimeAllowanceSettingFactory.Create().WithOrganization(organizationOne.Object).Persist();
            Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.GetOvertimeAllowanceSetting(userMenu, objFactory.Object.Id));
        }

        //[Fact]
        //public void InvalidDataException_Overtime_Allowance_Setting_Count_Test()
        //{
        //    Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.CountSettingsList(null, 0, "", ""));
        //}

        //[Fact]
        //public void AuthorizationException_Extra_Day_Setting_Count_Test()
        //{
        //    Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.CountSettingsList(new List<UserMenu>(), 0, "", ""));
        //}

        #endregion
    }
}
