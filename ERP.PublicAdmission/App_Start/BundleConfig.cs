﻿using System.Web.Optimization;

namespace UdvashERP.PublicAdmission
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/Result").Include("~/Scripts/result.js"));
            bundles.Add(new ScriptBundle("~/bundles/Registration").Include("~/Scripts/registration.js"));
            bundles.Add(new ScriptBundle("~/bundles/RegistrationSecond").Include("~/Scripts/registrationSecond.js"));
            bundles.Add(new ScriptBundle("~/bundles/CourseCalculator").Include("~/Scripts/courseCalculator.js"));

            bundles.Add(new ScriptBundle("~/bundles/typeaheadAutoComplete").Include("~/Content/typeahead/bootstrap3-typeahead.js"));
            bundles.Add(new ScriptBundle("~/bundles/AutoComplete").Include("~/Scripts/AutoComplete.js"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap-datetimepicker").Include("~/Scripts/bootstrap-datetimepicker.min.js"));
            bundles.Add(new StyleBundle("~/Content/bootstrap-datetimepicker").Include("~/Content/bootstrap-datetimepicker.min.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
