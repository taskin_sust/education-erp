﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Hr
{
    public class MemberFingerPrintMap : BaseClassMap<MemberFingerPrint, long>
    {
        public MemberFingerPrintMap()
        {
            Table("HR_MemberFingerPrint");
            LazyLoad();
            Map(x => x.Index0).Column("Index0");
            Map(x => x.Index1).Column("Index1");
            Map(x => x.Index2).Column("Index2");
            Map(x => x.Index3).Column("Index3");
            Map(x => x.Index4).Column("Index4");
            Map(x => x.Index5).Column("Index5");
            Map(x => x.Index6).Column("Index6");
            Map(x => x.Index7).Column("Index7");
            Map(x => x.Index8).Column("Index8");
            Map(x => x.Index9).Column("Index9");
            References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.AttendanceDeviceType).Column("AttendanceDeviceTypeId");
        }
    }
}
