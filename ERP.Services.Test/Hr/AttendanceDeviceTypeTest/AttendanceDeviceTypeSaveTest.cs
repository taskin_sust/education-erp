﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceDeviceTypeTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceDeviceType")]
    public class AttendanceDeviceTypeSaveTest : AttendanceDeviceTypeBaseTest
    {
        #region Basic

        [Fact]
        public void Should_Return_DuplicateEntry_Exception()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.Create();
            AttendanceDeviceTypeFactory.SingleObjectList[1].Code = AttendanceDeviceTypeFactory.SingleObjectList[0].Code;
            AttendanceDeviceTypeFactory.SingleObjectList[1].Name = AttendanceDeviceTypeFactory.SingleObjectList[0].Name;
            Assert.Throws<DuplicateEntryException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[1]));
        }
        
        [Fact]
        public void Should_Save_Successfully()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            Assert.True(AttendanceDeviceTypeFactory.Object.Id > 0);
        }

        #endregion

        #region Validation Check Test

        [Fact]
        public void Should_Return_EmptyName_Exception()
        {
            AttendanceDeviceTypeFactory.Create();
            AttendanceDeviceTypeFactory.Object.Code = "";
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.Object));
        }

        [Fact]
        public void Should_Return_EmptyFirmwareVersion_Exception()
        {
            AttendanceDeviceTypeFactory.Create();
            AttendanceDeviceTypeFactory.Object.FirmwareVersion = -1;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.Object));
        }

        [Fact]
        public void Should_Return_EmptyAlgorithmVersion_Exception()
        {
            AttendanceDeviceTypeFactory.Create();
            AttendanceDeviceTypeFactory.Object.AlgorithmVersion = -1;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.Object));
        }

        #endregion

        #region Invalid Input Check Test

        [Fact]
        public void Should_Return_Invalid_Attendance_LogCapacity_LessThanLowerRange()
        {
            AttendanceDeviceTypeFactory.Create();
            AttendanceDeviceTypeFactory.Object.AttendanceLogCapacity = -1;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_AttendanceLogCapacity_GreaterThanRange()
        {
            AttendanceDeviceTypeFactory.Create();
            AttendanceDeviceTypeFactory.Object.AttendanceLogCapacity = 100001;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_User_Capacity_LessThanLowerRange()
        {
            AttendanceDeviceTypeFactory.Create();
            AttendanceDeviceTypeFactory.Object.UserCapacity = -1;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_User_Capacity_GreaterThanRange()
        {
            AttendanceDeviceTypeFactory.Create();
            AttendanceDeviceTypeFactory.Object.UserCapacity = 100001;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_Template_Capacity_LessThanLowerRange()
        {
            AttendanceDeviceTypeFactory.Create();
            AttendanceDeviceTypeFactory.Object.UserCapacity = -1;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_Template_Capacity_GreaterThanRange()
        {
            AttendanceDeviceTypeFactory.Create();
            AttendanceDeviceTypeFactory.Object.UserCapacity = 100001;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.Object));
        }

        #endregion
    }
}
