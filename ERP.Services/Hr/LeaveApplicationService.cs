﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Data;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Sms;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;
using UdvashERP.MessageExceptions;
using System.Text.RegularExpressions;

namespace UdvashERP.Services.Hr
{
    public interface ILeaveApplicationService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(List<LeaveApplication> leaveApplicationList, bool isMentor = false, bool isHr = false);

        #endregion

        #region Single Instances Loading Function

        LeaveApplication GetById(long leaveappsId);

        DateTime? GetTeamMemberLeaveApplicationIsPostDateTime(long? teamMemberId, int? pin);

        #endregion

        #region List Loading Function

        IList<LeaveApplication> LoadTeamMemberLeaveApplication(int start, int length, DateTime dateTime, long memId, string leaveTypeId = "", string leaveStatus = "");

        IList<LeaveApplicationViewModel> LoadMentorLeave(long mentorId, DateTime? dateTime = null, LeaveStatus? leaveStatus = null, List<int> memberPinList = null, int start = 0, int length = 0);

        IList<LeaveApplicationViewModel> LoadHrLeave(List<UserMenu> userMenu, string datetime = "", List<long> organizationIdList = null, List<long> departmentIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<int> memberPinList = null, LeaveStatus? leaveStatus = null, int start = 0, int length = 0);

        #endregion

        #region Others Function

        int GetTeamMemberLeaveApplicationCount(DateTime dateTime, long memId, string leaveTypeId = "", string leaveStatus = "");

        int GetHrLeaveCount(List<UserMenu> userMenu, string datetime = "", List<long> organizationIdList = null, List<long> departmentIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<int> memberPinList = null, LeaveStatus? leaveStatus = null);

        int GetMentorLeaveCount(long mentorId, DateTime? dateTime = null, LeaveStatus? leaveStatus = null, List<int> memberPinList = null);

        #endregion

        #region Helper Function

        #endregion

    }

    public class LeaveApplicationService : BaseService, ILeaveApplicationService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly ITeamMemberDao _hrTeamMemberDao;
        private readonly IMembersLeaveSummaryDao _hrMembersLeaveSummaryDao;
        private readonly ILeaveApplicationDao _hrLeaveApplicationDao;
        private readonly IShiftWeekendHistoryDao _hrShiftWeekendHistoryDao;
        private readonly ILeaveApplicationLogDao _leaveApplicationLogDao;
        private readonly IHolidaySettingDao _holidaySettingDao;
        private readonly ISmsSettingsDao _smsSettingsDao;
        private readonly IMentorHistoryDao _mentorHistoryDao;
        private readonly IEmploymentHistoryDao _hrEmploymentHistoryDao;
        private readonly ISmsHistoryDao _hrHistoryDao;
        private readonly ILeaveApplicationDetailsDao _leaveApplicationDetailsDao;
        private readonly ISalarySheetDao _salarySheetDao;
        private readonly ICommonHelper _commonHelper;

        public LeaveApplicationService(ISession session)
        {
            Session = session;
            _hrTeamMemberDao = new TeamMemberDao() { Session = session };
            _hrMembersLeaveSummaryDao = new MembersLeaveSummaryDao() { Session = session };
            _hrLeaveApplicationDao = new LeaveApplicationDao() { Session = session };
            _hrShiftWeekendHistoryDao = new ShiftWeekendHistoryDao() { Session = session };
            _leaveApplicationLogDao = new LeaveApplicationLogDao() { Session = session };
            _holidaySettingDao = new HolidaySettingDao() { Session = session };
            _smsSettingsDao = new SmsSettingsDao() { Session = session };
            _mentorHistoryDao = new MentorHistoryDao() { Session = session };
            _hrEmploymentHistoryDao = new EmploymentHistoryDao() { Session = session };
            _hrHistoryDao = new SmsHistoryDao() { Session = session };
            _leaveApplicationDetailsDao = new LeaveApplicationDetailsDao() { Session = session };
            _salarySheetDao = new SalarySheetDao { Session = session };
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Functions

        /*
         * For Leave Allpication Save, update, Approve, Reject, cancel
         */
        public void SaveOrUpdate(List<LeaveApplication> leaveApplicationList, bool isMentor = false, bool isHr = false)
        {
            string message = "";
            string message2 = "";

            if (leaveApplicationList == null)
                throw new InvalidDataException("Leave application can't be empty");

            #region Overlaping or duplicate leave application for same team member check

            if (leaveApplicationList.Where(l => l.Id == 0).Count() > 1)
            {
                List<LeaveApplication> tempLaList = new List<LeaveApplication>();
                foreach (var la in leaveApplicationList.Where(l => l.Id == 0).ToList())
                {
                    tempLaList.Add((LeaveApplication)la.Clone());
                }

                for (int i = 0; i < tempLaList.Count(); i++)
                {
                    tempLaList[i].Id = i * -1;
                }
                foreach (var la in tempLaList)
                {
                    LeaveApplication tempLa = tempLaList.Where(l => l.Id != la.Id
                                                && l.TeamMember.Id == la.TeamMember.Id
                                                && (
                                                    (la.DateFrom >= l.DateFrom && la.DateFrom <= l.DateTo)
                                                    || (la.DateTo >= l.DateFrom && la.DateTo <= l.DateTo)
                                                )
                                                ).FirstOrDefault();
                    if (tempLa != null && tempLa.TeamMember.Id > 0)
                    {
                        message = "Multiple Leave Application Found for the same date";
                        message2 = " for PIN " + la.TeamMember.Pin + " (" + la.DateFrom.ToString("dd MMM") + "-" + la.DateTo.ToString("dd MMM, yyyy") + ")";
                        throw new DuplicateEntryException(message + message2);
                    }
                }
            }

            #endregion

            List<LeaveApplication> validLeaveApplicationList = new List<LeaveApplication>();

            #region Leave Application List Validation
            //Valdate all application first
            //If any application has error data return message using exception
            foreach (var la in leaveApplicationList)
            {
                Session.Evict(la);
                validLeaveApplicationList.Add(LeaveApplicationValidation(la, isMentor, isHr));
            }

            #endregion

            //TRANSECTON 
            ITransaction trans = null;
            try
            {
                //TRANSECTON START
                using (trans = Session.BeginTransaction())
                {
                    //FOREACH START
                    foreach (var la in validLeaveApplicationList)
                    {
                        if (isMentor || isHr)
                        {
                            message2 = " for PIN " + la.TeamMember.Pin + " (" + la.DateFrom.ToString("dd MMM") + "-" + la.DateTo.ToString("dd MMM, yyyy") + ")";
                        }
                        Organization org = _hrTeamMemberDao.GetTeamMemberOrganization(la.CreationDate, la.TeamMember.Id, null);
                        //Pull Member Summary
                        MemberLeaveSummary currentLeaveSummary = _hrMembersLeaveSummaryDao.GetLeaveSummary(org.Id, la.TeamMember.Id, la.Leave.Id);
                        //QUICK FIX Start
                        if (currentLeaveSummary == null)
                        {
                            currentLeaveSummary = new MemberLeaveSummary();
                            currentLeaveSummary.Leave = la.Leave;
                            currentLeaveSummary.CarryForwardLeave = 0;
                            currentLeaveSummary.CurrentYearLeave = 0;
                            currentLeaveSummary.EncashLeave = 0;
                            currentLeaveSummary.VoidLeave = 0;
                            currentLeaveSummary.TotalLeaveBalance = 0;
                            currentLeaveSummary.ApprovedLeave = 0;
                            currentLeaveSummary.PendingLeave = 0;
                            currentLeaveSummary.AvailableBalance = 0;
                            currentLeaveSummary.Year = la.DateFrom.Year;
                            currentLeaveSummary.TeamMember = la.TeamMember;
                            currentLeaveSummary.Organization = org;
                            currentLeaveSummary.Rank = 10;
                        }

                        //QUICK FIX End
                        la.TotalLeaveDay = (la.DateTo - la.DateFrom).Days + 1;

                        #region New Leave Application Details Calculation

                        //New Leave Application Detail Calculation
                        //la.LeaveApplicationDetail = null;

                        la.LeaveApplicationDetail = new List<LeaveApplicationDetails>();
                        int totalMonth = la.DateTo.Month - la.DateFrom.Month;
                        if (totalMonth == 0)
                        {
                            LeaveApplicationDetails tld = new LeaveApplicationDetails();
                            tld.DateFrom = la.DateFrom;
                            tld.DateTo = la.DateTo;
                            tld.IsPost = false;
                            tld.CreateBy = GetCurrentUserId();
                            tld.ModifyBy = GetCurrentUserId();
                            tld.Status = LeaveApplicationDetails.EntityStatus.Active;
                            //tld.LeaveApplication = la;
                            la.LeaveApplicationDetail.Add(tld);
                        }
                        else
                        {
                            for (int i = 0; i <= totalMonth; i++)
                            {
                                LeaveApplicationDetails tld = new LeaveApplicationDetails();
                                if (i == 0)
                                {
                                    tld.DateFrom = la.DateFrom;
                                    tld.DateTo = new DateTime(la.DateFrom.Year, la.DateFrom.Month, DateTime.DaysInMonth(la.DateFrom.Year, la.DateFrom.Month));
                                }
                                else if (i == totalMonth)
                                {
                                    tld.DateFrom = new DateTime(la.DateTo.Year, la.DateTo.Month, 1);
                                    tld.DateTo = la.DateTo;
                                }
                                else
                                {
                                    tld.DateFrom = new DateTime(la.DateFrom.Year, la.DateFrom.Month + i, 1);
                                    tld.DateTo = new DateTime(la.DateFrom.Year, la.DateFrom.Month + i, DateTime.DaysInMonth(la.DateFrom.Year, la.DateFrom.Month + i));
                                }
                                tld.IsPost = false;
                                tld.CreateBy = GetCurrentUserId();
                                tld.ModifyBy = GetCurrentUserId();
                                tld.Status = LeaveApplicationDetails.EntityStatus.Active;
                                la.LeaveApplicationDetail.Add(tld);
                            }
                        }

                        #endregion

                        if (la.Id < 1)
                        {
                            #region New Leave Application

                            if (la.Leave.IsPublic == false)
                            {
                                #region Private Leave
                                if (isHr == false)
                                {
                                    message = "Unauthorized Leave Type";
                                    throw new InvalidDataException(message + message2);
                                }
                                else
                                {
                                    currentLeaveSummary.ApprovedLeave += la.TotalLeaveDay;
                                    currentLeaveSummary.TotalLeaveBalance = currentLeaveSummary.ApprovedLeave;
                                    la.LeaveStatus = (int)LeaveStatus.Approved;
                                }

                                #endregion
                            }
                            else if (la.Leave.PayType == (int)PayType.WithoutPay)
                            {
                                #region Leave Without Pay

                                if (isHr == false && isMentor == false)
                                {
                                    currentLeaveSummary.PendingLeave += la.TotalLeaveDay;
                                    la.LeaveStatus = (int)LeaveStatus.Pending;
                                }
                                else
                                {
                                    currentLeaveSummary.ApprovedLeave += la.TotalLeaveDay;
                                    currentLeaveSummary.TotalLeaveBalance = currentLeaveSummary.ApprovedLeave;
                                    la.LeaveStatus = (int)LeaveStatus.Approved;
                                }
                                #endregion
                            }
                            else
                            {
                                if (currentLeaveSummary.AvailableBalance < la.TotalLeaveDay)
                                {
                                    #region Has leave balance
                                    message = "You do not have " + ((la.DateTo - la.DateFrom).Days + 1) + " days leave in your '" + la.Leave.Name + "' balance";
                                    throw new InvalidDataException(message + message2);
                                    #endregion
                                }
                                else
                                {
                                    if (isMentor == true || isHr == true)
                                    {
                                        la.Status = LeaveApplication.EntityStatus.Active;
                                        //status always approved
                                        la.LeaveStatus = (int)LeaveStatus.Approved;

                                        currentLeaveSummary.ApprovedLeave += la.TotalLeaveDay;
                                        currentLeaveSummary.AvailableBalance -= la.TotalLeaveDay;
                                    }
                                    else
                                    {
                                        la.Status = LeaveApplication.EntityStatus.Active;
                                        //status always pending
                                        la.LeaveStatus = (int)LeaveStatus.Pending;

                                        currentLeaveSummary.PendingLeave += la.TotalLeaveDay;
                                        currentLeaveSummary.AvailableBalance -= la.TotalLeaveDay;
                                    }
                                }
                            }
                            // new Leave Application Save
                            _hrLeaveApplicationDao.Save(la);
                            //Leave Summary Save or update
                            if (currentLeaveSummary.Id > 0)
                                _hrMembersLeaveSummaryDao.Update(currentLeaveSummary);
                            else
                                _hrMembersLeaveSummaryDao.Save(currentLeaveSummary);
                            // Leave Application Log Save
                            _leaveApplicationLogDao.Save(LeaveApplicationToLeaveApplicationLog(la));

                            //Send SMS to responsible person
                            //var responsibleperson = la.ResponsiblePerson
                            //Save
                            var smsTypes = new List<string>() { "Leave Application(To Responsible Person)" };
                            if (isMentor == false && isHr == false)
                            {
                                smsTypes.Add("Leave Application(To Mentor)");
                            }

                            if (isMentor == true || isHr == true)
                            {
                                smsTypes.Add("Leave Approve(To Applicant)");
                            }

                            // var smsTypes = new List<string>() { "Leave Application(To Mentor)", "Leave Application(To Responsible Person)" };
                            SaveSmsHistory(la, smsTypes);

                            #endregion
                        }
                        else
                        {
                            #region Update Leave Application
                            LeaveApplication pla = _hrLeaveApplicationDao.LoadById(la.Id); //previous Leave Application
                            bool isLeaveTypeChanged = false;
                            //Pull Member Summary
                            MemberLeaveSummary previousLeaveSummary = _hrMembersLeaveSummaryDao.GetLeaveSummary(org.Id, pla.TeamMember.Id, pla.Leave.Id);

                            #region Is Team Member Changed

                            if (la.TeamMember.Id != pla.TeamMember.Id)
                            {
                                message = "You can't change team member in a leave application";
                                throw new InvalidDataException(message + message2);
                            }
                            #endregion

                            #region Is Leave type changed from Public to Private OR WithPay to WithoutPay
                            if (la.Leave.IsPublic != pla.Leave.IsPublic || la.Leave.PayType != pla.Leave.PayType)
                            {
                                message = "You can't change Leave Type from public to private or payment type or vice versa";

                                throw new InvalidDataException(message + message2);
                            }
                            #endregion

                            #region Full Posted Leave
                            if (pla.LeaveApplicationDetail.Min(x => x.IsPost) == true)
                            {
                                message = "This leave application is already posted";
                                throw new InvalidDataException(message + message2);

                            }
                            #endregion

                            if (la.LeaveStatus == (int)LeaveStatus.Rejected || la.LeaveStatus == (int)LeaveStatus.Cancelled)
                            {
                                #region Leave Cancel or Reject
                                if (pla.LeaveApplicationDetail.Max(x => x.IsPost) == true)
                                {
                                    #region Partially Posted Leave
                                    message = "This leave is posted, you can't cancel or reject this leave";
                                    throw new InvalidDataException(message + message2);
                                    #endregion
                                }
                                else
                                {
                                    if (pla.LeaveStatus == (int)LeaveStatus.Pending && la.LeaveStatus == (int)LeaveStatus.Rejected && (isHr == true || isMentor == true))
                                    {
                                        #region Reject Leave From approved
                                        //Reject Leave From approved
                                        pla.LeaveStatus = (int)LeaveStatus.Rejected;
                                        if (pla.Leave.IsPublic == false || pla.Leave.PayType == (int)PayType.WithoutPay) //For Special and without pay leave
                                        {
                                            previousLeaveSummary.PendingLeave -= pla.TotalLeaveDay;
                                        }
                                        else  // For Regular leave
                                        {
                                            previousLeaveSummary.PendingLeave -= pla.TotalLeaveDay;
                                            previousLeaveSummary.AvailableBalance += pla.TotalLeaveDay;
                                        }
                                        #endregion
                                    }
                                    else if (pla.LeaveStatus == (int)LeaveStatus.Approved && la.LeaveStatus == (int)LeaveStatus.Rejected && (isHr == true || isMentor == true))
                                    {
                                        #region Reject Leave From approved
                                        //Reject Leave From approved
                                        pla.LeaveStatus = (int)LeaveStatus.Rejected;
                                        if (pla.Leave.IsPublic == false || pla.Leave.PayType == (int)PayType.WithoutPay) //For Special and without pay leave
                                        {
                                            previousLeaveSummary.ApprovedLeave -= pla.TotalLeaveDay;
                                            previousLeaveSummary.TotalLeaveBalance = previousLeaveSummary.ApprovedLeave;
                                        }
                                        else  // For Regular leave
                                        {
                                            previousLeaveSummary.ApprovedLeave -= pla.TotalLeaveDay;
                                            previousLeaveSummary.AvailableBalance += pla.TotalLeaveDay;
                                        }
                                        #endregion
                                    }
                                    else if (pla.LeaveStatus == (int)LeaveStatus.Pending && la.LeaveStatus == (int)LeaveStatus.Cancelled && isHr == false && isMentor == false)
                                    {
                                        #region Cancel Leave From approved
                                        if (pla.DateFrom < DateTime.Today)
                                        {
                                            throw new InvalidDataException("You already enjoyed this leave");
                                        }
                                        //Cancel Leave From approved
                                        if (pla.Leave.IsPublic == false || pla.Leave.PayType == (int)PayType.WithoutPay) //For Special and without pay leave
                                        {
                                            previousLeaveSummary.PendingLeave -= pla.TotalLeaveDay;
                                        }
                                        else  // For Regular leave
                                        {
                                            pla.LeaveStatus = (int)LeaveStatus.Cancelled;
                                            previousLeaveSummary.PendingLeave -= pla.TotalLeaveDay;
                                            previousLeaveSummary.AvailableBalance += pla.TotalLeaveDay;
                                        }
                                        #endregion
                                    }
                                    else if (pla.LeaveStatus == (int)LeaveStatus.Approved && la.LeaveStatus == (int)LeaveStatus.Cancelled && isHr == false && isMentor == false)
                                    {
                                        #region Cancel Leave From approved
                                        if (pla.DateFrom < DateTime.Today)
                                        {
                                            throw new InvalidDataException("You already enjoyed this leave");
                                        }
                                        //Cancel Leave From approved
                                        pla.LeaveStatus = (int)LeaveStatus.Cancelled;

                                        if (pla.Leave.IsPublic == false || pla.Leave.PayType == (int)PayType.WithoutPay) //For Special and without pay leave
                                        {
                                            previousLeaveSummary.ApprovedLeave -= pla.TotalLeaveDay;
                                            previousLeaveSummary.TotalLeaveBalance = previousLeaveSummary.ApprovedLeave;
                                        }
                                        else // For Regular leave
                                        {
                                            previousLeaveSummary.ApprovedLeave -= pla.TotalLeaveDay;
                                            previousLeaveSummary.AvailableBalance += pla.TotalLeaveDay;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        message = "01 Invalid combination of leave";
                                        throw new InvalidDataException(message + message2);
                                    }
                                }
                                #endregion
                            }
                            else if (la.LeaveStatus == (int)LeaveStatus.Approved && (isHr == true || isMentor == true))
                            {
                                //Leave Type and DateFrom DateTo Edit implementation Start

                                #region Leave Type Change

                                if (la.Leave.Id != pla.Leave.Id)
                                {
                                    if (currentLeaveSummary.AvailableBalance < la.TotalLeaveDay)
                                    {
                                        #region Has leave balance
                                        message = "You do not have " + ((la.DateTo - la.DateFrom).Days + 1) + " days leave in your '" + la.Leave.Name + "' balance";
                                        throw new InvalidDataException(message + message2);
                                        #endregion
                                    }

                                    //Previous Leave Return Start
                                    if (pla.LeaveStatus == (int)LeaveStatus.Pending)
                                    {
                                        previousLeaveSummary.PendingLeave -= pla.TotalLeaveDay;
                                        previousLeaveSummary.AvailableBalance += pla.TotalLeaveDay;
                                    }
                                    else if (pla.LeaveStatus == (int)LeaveStatus.Approved)
                                    {
                                        previousLeaveSummary.ApprovedLeave -= pla.TotalLeaveDay;
                                        previousLeaveSummary.AvailableBalance += pla.TotalLeaveDay;
                                    }
                                    else if (pla.LeaveStatus == (int)LeaveStatus.Rejected)
                                    {
                                        //Nothing to return
                                    }
                                    else
                                    {
                                        message = "02 Invalid combination of leave application";
                                        throw new InvalidDataException(message + message2);
                                    }
                                    //Previous Leave Return End

                                    //New Leave Cutting Start                                    
                                    if (la.LeaveStatus == (int)LeaveStatus.Approved)
                                    {
                                        currentLeaveSummary.ApprovedLeave += la.TotalLeaveDay;
                                        currentLeaveSummary.AvailableBalance -= la.TotalLeaveDay;
                                    }
                                    else
                                    {
                                        message = "03 Invalid combination of leave application";
                                        throw new InvalidDataException(message + message2);
                                    }
                                    //New Leave Cutting End
                                    isLeaveTypeChanged = true;
                                    pla.Leave = la.Leave;
                                }
                                #endregion

                                #region DateFrom DateTo Edit for Leave Application Details

                                if (la.DateFrom != pla.DateFrom || la.DateTo != pla.DateTo)
                                {
                                    int newDetailsCount = la.LeaveApplicationDetail.Count;
                                    int prevDetailsCount = pla.LeaveApplicationDetail.Count;

                                    int loop = newDetailsCount < prevDetailsCount ? newDetailsCount : prevDetailsCount;
                                    for (int i = 0; i < loop; i++)
                                    {
                                        if (pla.LeaveApplicationDetail[i].IsPost == true)
                                        {
                                            message = "In this leave from " + pla.LeaveApplicationDetail[i].DateFrom.ToString("dd MMM") + "-" + pla.LeaveApplicationDetail[i].DateTo.ToString("dd MMM, yyyy") + " is already posted";
                                            throw new InvalidDataException(message + message2);
                                        }
                                        pla.LeaveApplicationDetail[i].ModificationDate = DateTime.Now;
                                        pla.LeaveApplicationDetail[i].ModifyBy = GetCurrentUserId();
                                        pla.LeaveApplicationDetail[i].DateFrom = la.LeaveApplicationDetail[i].DateFrom;
                                        pla.LeaveApplicationDetail[i].DateTo = la.LeaveApplicationDetail[i].DateTo;
                                    }
                                    if (newDetailsCount < prevDetailsCount)
                                    {
                                        for (int i = loop; i < prevDetailsCount; i++)
                                        {
                                            if (pla.LeaveApplicationDetail[i].IsPost == true)
                                            {
                                                message = "In this leave from " + pla.LeaveApplicationDetail[i].DateFrom.ToString("dd MMM") + "-" + pla.LeaveApplicationDetail[i].DateTo.ToString("dd MMM, yyyy") + " is already posted";
                                                throw new InvalidDataException(message + message2);
                                            }
                                            pla.LeaveApplicationDetail.RemoveAt(i);
                                        }
                                    }
                                    else
                                    {
                                        for (int i = loop; i < newDetailsCount; i++)
                                        {
                                            pla.LeaveApplicationDetail.Add(la.LeaveApplicationDetail[i]);
                                        }
                                    }
                                    //pla.DateFrom = la.DateFrom;
                                    //pla.DateTo = la.DateTo;
                                    //pla.TotalLeaveDay = (pla.DateTo - pla.DateFrom).Days + 1;
                                }
                                #endregion

                                //Leave Type and DateFrom DateTo Edit implementation End

                                #region Leave Type Not Changed Summary Calculation

                                if (isLeaveTypeChanged == false)
                                {
                                    if (pla.Leave.IsPublic == false || pla.Leave.PayType == (int)PayType.WithoutPay) //For Special and without pay leave
                                    {
                                        if (pla.LeaveStatus == (int)LeaveStatus.Pending)
                                        {
                                            previousLeaveSummary.PendingLeave -= la.TotalLeaveDay;
                                            previousLeaveSummary.ApprovedLeave += la.TotalLeaveDay;
                                            previousLeaveSummary.TotalLeaveBalance = previousLeaveSummary.ApprovedLeave;
                                        }
                                        else if (pla.LeaveStatus == (int)LeaveStatus.Rejected)
                                        {
                                            previousLeaveSummary.ApprovedLeave += la.TotalLeaveDay;
                                            previousLeaveSummary.TotalLeaveBalance = previousLeaveSummary.ApprovedLeave;
                                        }
                                        else if (pla.LeaveStatus == (int)LeaveStatus.Approved)
                                        {
                                            previousLeaveSummary.ApprovedLeave -= pla.TotalLeaveDay;
                                            previousLeaveSummary.ApprovedLeave += la.TotalLeaveDay;
                                            previousLeaveSummary.TotalLeaveBalance = previousLeaveSummary.ApprovedLeave;
                                        }
                                        else
                                        {
                                            message = "04 Invalid combination of leave application";
                                            throw new InvalidDataException(message + message2);
                                        }
                                    }
                                    else  // For Regular leave
                                    {
                                        if (previousLeaveSummary.AvailableBalance + previousLeaveSummary.PendingLeave < la.TotalLeaveDay)
                                        {
                                            #region Has leave balance
                                            message = "You do not have " + ((la.DateTo - la.DateFrom).Days + 1) + " days leave in your '" + la.Leave.Name + "' balance";
                                            throw new InvalidDataException(message + message2);
                                            #endregion
                                        }
                                        if (pla.LeaveStatus == (int)LeaveStatus.Pending)
                                        {
                                            previousLeaveSummary.ApprovedLeave += la.TotalLeaveDay;
                                            previousLeaveSummary.PendingLeave -= pla.TotalLeaveDay;
                                            //previousLeaveSummary.AvailableBalance -= pla.TotalLeaveDay;
                                        }
                                        else if (pla.LeaveStatus == (int)LeaveStatus.Rejected)
                                        {
                                            previousLeaveSummary.ApprovedLeave += la.TotalLeaveDay;
                                            previousLeaveSummary.AvailableBalance -= la.TotalLeaveDay;
                                        }
                                        else if (pla.LeaveStatus == (int)LeaveStatus.Approved)
                                        {
                                            previousLeaveSummary.ApprovedLeave -= pla.TotalLeaveDay;
                                            previousLeaveSummary.AvailableBalance += pla.TotalLeaveDay;

                                            previousLeaveSummary.ApprovedLeave += la.TotalLeaveDay;
                                            previousLeaveSummary.AvailableBalance -= la.TotalLeaveDay;
                                        }
                                        else
                                        {
                                            message = "05 Invalid combination of leave application";
                                            throw new InvalidDataException(message + message2);
                                        }
                                    }
                                }
                                #endregion

                                #region DateFrom DateTo Edit For Leave Application

                                if (la.DateFrom != pla.DateFrom || la.DateTo != pla.DateTo)
                                {
                                    pla.DateFrom = la.DateFrom;
                                    pla.DateTo = la.DateTo;
                                    pla.TotalLeaveDay = (pla.DateTo - pla.DateFrom).Days + 1;
                                }
                                #endregion

                                pla.LeaveStatus = la.LeaveStatus;
                                pla.LeaveNote = la.LeaveNote;
                                pla.ResponsiblePerson = la.ResponsiblePerson;
                                pla.Remarks = la.Remarks;
                                pla.ModifyBy = GetCurrentUserId();
                            }
                            else
                            {
                                message = "06 Invalid combination of leave application";
                                throw new InvalidDataException(message + message2);
                            }


                            if (previousLeaveSummary.ApprovedLeave < 0 || previousLeaveSummary.PendingLeave < 0 || previousLeaveSummary.AvailableBalance < 0 ||
                                currentLeaveSummary.ApprovedLeave < 0 || currentLeaveSummary.PendingLeave < 0 || currentLeaveSummary.AvailableBalance < 0)
                            {
                                message = "Summery calculation causes an error. Please contact with your HR.";
                                throw new InvalidDataException(message + message2);
                            }
                            //Update Leave Application
                            _hrLeaveApplicationDao.Update(pla);

                            if (la.Leave.IsPublic == true && la.Leave.PayType == (int)PayType.WithPay)
                            {
                                previousLeaveSummary.AvailableBalance = previousLeaveSummary.TotalLeaveBalance - (previousLeaveSummary.ApprovedLeave + previousLeaveSummary.PendingLeave);
                            }
                            //previous Leave Summary Update
                            _hrMembersLeaveSummaryDao.Update(previousLeaveSummary);
                            if (isLeaveTypeChanged == true)
                            {
                                currentLeaveSummary.AvailableBalance = currentLeaveSummary.TotalLeaveBalance - (currentLeaveSummary.ApprovedLeave + currentLeaveSummary.PendingLeave);
                                //Current Leave Summary Update
                                _hrMembersLeaveSummaryDao.Update(currentLeaveSummary);
                            }

                            //var smsTypes = new List<string>() { "Leave Approve(To Applicant)", "Leave Reject(To Applicant)" };
                            if (la.LeaveStatus == (int)LeaveStatus.Approved)
                            {
                                SaveSmsHistory(la, new List<string>() { "Leave Approve(To Applicant)" });
                            }
                            else if (la.LeaveStatus == (int)LeaveStatus.Rejected)
                            {
                                SaveSmsHistory(la, new List<string>() { "Leave Reject(To Applicant)" });
                            }
                            // Leave Application Log Save
                            _leaveApplicationLogDao.Save(LeaveApplicationToLeaveApplicationLog(pla));

                            #endregion
                        }
                    }
                    //FOREACH END
                    //COMMIT 
                    trans.Commit();
                }
            }
            catch (InvalidDataException)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public LeaveApplication GetById(long leaveappsId)
        {
            try
            {
                if (leaveappsId <= 0) throw new InvalidDataException("Invalid id");
                return _hrLeaveApplicationDao.LoadById(leaveappsId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<LeaveApplication> LoadTeamMemberLeaveApplication(int start, int length, DateTime dateTime, long memId, string leaveTypeId = "", string leaveStatus = "")
        {
            try
            {

                return _hrLeaveApplicationDao.LoadTeamMemberLeaveApplication(start, length, dateTime, memId, leaveTypeId, leaveStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }

        }


        public IList<LeaveApplicationViewModel> LoadMentorLeave(long mentorId, DateTime? dateTime = null, LeaveStatus? leaveStatus = null, List<int> memberPinList = null, int start = 0, int length = 0)
        {
            try
            {
                List<long> mentorMemberIdList = _mentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentorId), dateTime ?? DateTime.Now, null, memberPinList);
                if (!mentorMemberIdList.Any())
                {
                    throw new InvalidDataException("There is no team member under this mentor");
                }

                return _hrLeaveApplicationDao.LoadMentorLeave(dateTime, leaveStatus, mentorMemberIdList, start, length);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<LeaveApplicationViewModel> LoadHrLeave(List<UserMenu> userMenu, string datetime = "", List<long> organizationIdList = null, List<long> departmentIdList = null,
            List<long> branchIdList = null, List<long> campusIdList = null, List<int> memberPinList = null, LeaveStatus? leaveStatus = null, int start = 0, int length = 0)
        {
            try
            {
                DateTime? searchingDateTime = null;
                if (!String.IsNullOrEmpty(datetime))
                {
                    searchingDateTime = Convert.ToDateTime(datetime);
                }

                List<long> authorganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationIdList != null && !organizationIdList.Contains(SelectionType.SelelectAll)) ? organizationIdList : null);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authorganizationIdList, null, (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll)) ? branchIdList : null);

                if (!authBranchIdList.Any())
                {
                    throw new InvalidDataException("You have no authorized Branch to access Leave Information");
                }

                return _hrLeaveApplicationDao.LoadHrLeave(searchingDateTime, authBranchIdList, campusIdList, departmentIdList, memberPinList, leaveStatus, start, length);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        #endregion

        #region Others Function

        public int GetTeamMemberLeaveApplicationCount(DateTime dateTime, long memId, string leaveTypeId = "", string leaveStatus = "")
        {
            try
            {
                return _hrLeaveApplicationDao.GetTeamMemberLeaveApplicationCount(dateTime, memId, leaveTypeId, leaveStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }

        }

        public int GetMentorLeaveCount(long mentorId, DateTime? dateTime = null, LeaveStatus? leaveStatus = null, List<int> memberPinList = null)
        {
            try
            {
                List<long> mentorMemberIdList = _mentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentorId), dateTime ?? DateTime.Now, null, memberPinList);
                if (!mentorMemberIdList.Any())
                {
                    throw new InvalidDataException("You have no TeamMember to Mentor.");
                }
                return _hrLeaveApplicationDao.GetMentorLeaveCount(dateTime, leaveStatus, mentorMemberIdList);
                //return _hrLeaveApplicationDao.GetMentorLeaveCount(mentorId, dateTime, leaveStatus, memberPinList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetHrLeaveCount(List<UserMenu> userMenu, string datetime = "", List<long> organizationIdList = null, List<long> departmentIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<int> memberPinList = null, LeaveStatus? leaveStatus = null)
        {
            try
            {
                DateTime? searchingDateTime = null;
                if (!String.IsNullOrEmpty(datetime))
                {
                    searchingDateTime = Convert.ToDateTime(datetime);
                }

                List<long> authorganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationIdList != null && !organizationIdList.Contains(SelectionType.SelelectAll)) ? organizationIdList : null);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authorganizationIdList, null, (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll)) ? branchIdList : null);

                if (!authBranchIdList.Any())
                {
                    throw new InvalidDataException("You have no authorized Branch to access Leave Information");
                }

                return _hrLeaveApplicationDao.GetHrLeaveCount(searchingDateTime, authBranchIdList, campusIdList, departmentIdList, memberPinList, leaveStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public DateTime? GetTeamMemberLeaveApplicationIsPostDateTime(long? teamMemberId, int? pin = null)
        {
            DateTime? postDateTime = null;
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(teamMemberId, pin);
            LeaveApplicationDetails leaveApplicationDetails = _leaveApplicationDetailsDao.GetTeamMemberLastIsPostLeaveApplicationDetails(teamMemberId, pin);

            if (salarySheet != null && leaveApplicationDetails != null)
            {
                postDateTime = salarySheet.EndDate;
                if (leaveApplicationDetails.DateTo > postDateTime)
                    postDateTime = leaveApplicationDetails.DateTo;
            }
            else if (salarySheet != null)
            {
                postDateTime = salarySheet.EndDate;
            }
            else if (leaveApplicationDetails != null)
            {
                postDateTime = leaveApplicationDetails.DateTo;
            }
            return postDateTime;
        }


        #endregion

        #region Helper function

        private LeaveApplication LeaveApplicationValidation(LeaveApplication leaveApplication, bool isMentor = false, bool isHr = false)
        {
            string message = "";
            string message2 = "";
            leaveApplication.TotalLeaveDay = (leaveApplication.DateTo - leaveApplication.DateFrom).Days + 1;
            if (leaveApplication.TeamMember == null || leaveApplication.TeamMember.Id <= 0)
                throw new EmptyFieldException("Team member is not selected");

            if (isMentor || isHr)
            {
                message2 = " for PIN " + leaveApplication.TeamMember.Pin + " (" + leaveApplication.DateFrom.ToString("dd MMM") + "-" + leaveApplication.DateTo.ToString("dd MMM, yyyy") + ")";
            }
            try
            {
                #region Form Validation

                if (leaveApplication.Leave == null || leaveApplication.Leave.Id <= 0)
                    throw new EmptyFieldException("Leave can't empty");
                //Must have Team Member
                if (leaveApplication.TeamMember == null || leaveApplication.TeamMember.Id <= 0)
                    throw new EmptyFieldException("Team member is not selected");

                //Must have Date From and To
                if (leaveApplication.DateFrom == null || leaveApplication.DateTo == null)
                {
                    message = "You must select Date From & Date To";
                    if (isMentor || isHr)
                    {
                        message += " for PIN " + leaveApplication.TeamMember.Pin;
                    }
                    throw new EmptyFieldException(message);
                }

                //Date From can't be greater then date to
                if (leaveApplication.DateFrom > leaveApplication.DateTo)
                {
                    message = "Date From must be less then or equal Date To";
                    throw new InvalidDataException(message + message2);
                }

                //Leave Note can't be empty
                if (leaveApplication.LeaveNote.Trim() == "")
                {
                    message = "Reason can't be empty";
                    throw new EmptyFieldException(message + message2);
                }

                //Team Member can't be his own responsible person
                if (leaveApplication.ResponsiblePerson != null && leaveApplication.ResponsiblePerson.Id == leaveApplication.TeamMember.Id)
                {
                    message = "You can't be your responsible person for taking leave";
                    throw new InvalidDataException(message + message2);
                }

                #endregion

                //IS POST CHECK
                #region Is Post Check

                //Leave date can't be grater then Team Member's salary receive date
                DateTime? postDateTime = GetTeamMemberLeaveApplicationIsPostDateTime(leaveApplication.TeamMember.Id);
                if (postDateTime != null && leaveApplication.DateTo.Date <= postDateTime.Value.Date)
                {
                    message = "You can only apply for leave after Date (" + postDateTime.Value.ToString("dd MMM, yyyy")+") ";
                    throw new InvalidDataException(message + message2);
                }

                #endregion

                #region LeaveOrganizationCheck

                if (leaveApplication.LeaveStatus == (int) LeaveStatus.Approved || leaveApplication.LeaveStatus == (int) LeaveStatus.Pending)
                {
                    Organization memApplyOrg = _hrTeamMemberDao.GetTeamMemberOrganization(leaveApplication.CreationDate, leaveApplication.TeamMember.Id, null);
                    Organization memStartOrg = _hrTeamMemberDao.GetTeamMemberOrganization(leaveApplication.DateFrom, leaveApplication.TeamMember.Id, null);
                    Organization memEndOrg = _hrTeamMemberDao.GetTeamMemberOrganization(leaveApplication.DateTo, leaveApplication.TeamMember.Id, null);
                    //if (memEndOrg == null) throw new InvalidDataException("Member Id Changed !! invalid Data manupulation");
                    if (memStartOrg == null) throw new InvalidDataException("Invalid Organization ");
                    if (memApplyOrg == null) throw new InvalidDataException("You have to take from your current organization");
                    if (memApplyOrg.Id == memStartOrg.Id && memStartOrg.Id == memEndOrg.Id)
                    {
                        /*Everything os OK*/
                    }
                    else
                    {
                        message = "You have to take from your current organization";
                        throw new InvalidDataException(message + message2);
                    }
                }

                #endregion

                #region Current year check

                //Current year check
                //For Maternity and Paternal Leave Year check wil not be applicable
                double currentYear = DateTime.Now.Year;
                //For Private leave and Special Leave Repeat type is not set 
                //that's why we have to add two seperate type of check
                if (leaveApplication.Leave.IsPublic == false || leaveApplication.Leave.PayType == (int)PayType.WithoutPay)
                {
                    if ((currentYear != leaveApplication.CreationDate.Year || currentYear != leaveApplication.DateFrom.Year || currentYear != leaveApplication.DateTo.Year))
                    {
                        message = "You have to take leave in current year";
                        throw new InvalidDataException(message + message2);
                    }
                }
                else
                {
                    if ((leaveApplication.Leave.RepeatType.Value != (int)RepeatType.Once && leaveApplication.Leave.RepeatType.Value != (int)RepeatType.MaxTwo) && (currentYear != leaveApplication.CreationDate.Year || currentYear != leaveApplication.DateFrom.Year || currentYear != leaveApplication.DateTo.Year))
                    {
                        message = "You have to take leave in current year";
                        throw new InvalidDataException(message + message2);
                    }
                }

                #endregion

                #region Pre Post [Responsible Person Check]

                //Check Responsible Person for Pre and Post Application
                if (leaveApplication.Leave.Applicationtype == null || leaveApplication.Leave.Applicationtype == 1)
                    // null=Private leave & Without Pay Leave, 1 = Pre, 2 = Post
                {
                    if (leaveApplication.ResponsiblePerson == null)
                    {
                        message = "Responsible person can't be empty";
                        throw new EmptyFieldException(message + message2);
                    }
                    if (isHr == false)
                    {
                        //Is responsible person already take or apply for leave
                        MemberLeaveDates(leaveApplication.ResponsiblePerson.Id, leaveApplication.DateFrom, leaveApplication.DateTo, message2);

                        //Is team member already responsible person for someone else's leave
                        MemberResponsibleLeaveDates(leaveApplication.TeamMember.Id, leaveApplication.DateFrom, leaveApplication.DateTo, message2);
                    }
                }
                else
                {
                    if (leaveApplication.DateFrom > leaveApplication.CreationDate && leaveApplication.ResponsiblePerson == null)
                    {
                        message = "Responsible person can't be empty for future dated leave";
                        throw new EmptyFieldException(message + message2);
                    }
                    else if (leaveApplication.DateFrom > leaveApplication.CreationDate && isHr == false)
                    {
                        //Is responsible person already take or apply for leave
                        MemberLeaveDates(leaveApplication.ResponsiblePerson.Id, leaveApplication.DateFrom, leaveApplication.DateTo, message2);

                        //Is team member already responsible person for someone else's leave
                        MemberResponsibleLeaveDates(leaveApplication.TeamMember.Id, leaveApplication.DateFrom, leaveApplication.DateTo, message2);
                    }
                }

                #endregion

                #region Duplicate Leave Check

                //Duplicate Leave Check
                for (DateTime d = leaveApplication.DateFrom; d <= leaveApplication.DateTo; d = d.AddDays(1))
                {
                    if (_hrLeaveApplicationDao.IsDuplicateLeaveApplicationByTeamMemberAndDate(leaveApplication.TeamMember.Id, d, leaveApplication.Id) == true)
                    {
                        message = "Previous leave exists for the date " + d.ToString("dd MMM, yyyy");
                        throw new DuplicateEntryException(message + message2);
                    }
                }

                #endregion

                if (isHr == false)
                {
                    #region Leave application in holidays

                    if (leaveApplication.Leave.RepeatType != null && leaveApplication.Leave.RepeatType == (int)RepeatType.EveryYear)
                    {
                        //If all leave application days fall in holiday or weekend then through exception

                        Organization memberOrg = _hrTeamMemberDao.GetTeamMemberOrganization(leaveApplication.DateFrom, leaveApplication.TeamMember.Id, null);

                        int memberWeekend = _hrShiftWeekendHistoryDao.GetLastShiftWeekendHistory(leaveApplication.TeamMember, leaveApplication.CreationDate).Weekend.Value;

                        bool IsFirst = false;
                        if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), leaveApplication.DateFrom.DayOfWeek.ToString()))
                            IsFirst = true;
                        else if (_holidaySettingDao.IsHolidayByOrganizationAndDate(memberOrg.Id, leaveApplication.DateFrom) == true)
                            IsFirst = true;

                        bool IsLast = false;
                        if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), leaveApplication.DateTo.DayOfWeek.ToString()))
                            IsLast = true;
                        else if (_holidaySettingDao.IsHolidayByOrganizationAndDate(memberOrg.Id, leaveApplication.DateTo) == true)
                            IsLast = true;

                        if (IsFirst || IsLast)
                        {
                            if (IsLast)
                                message = "Last day of your leave application is in holiday or weekend";
                            if (IsFirst)
                                message = "First day of your leave application is in holiday or weekend";

                            if (leaveApplication.TotalLeaveDay == 1)
                                message = "You are appling leave for holiday or weekend";
                            throw new InvalidDataException(message + message2);
                        }
                    }

                    #endregion
                }

                if (isMentor == false && isHr == false)
                {
                    #region Max Taking day Check

                    //Max Leave Check from Leave
                    if (leaveApplication.Leave.IsTakingLimit == true &&
                        leaveApplication.TotalLeaveDay > leaveApplication.Leave.MaxTakingLimit)
                    {
                        message = "You can't apply more then " + leaveApplication.Leave.MaxTakingLimit + " days for " +
                                  leaveApplication.Leave.Name;
                        throw new InvalidDataException(message);
                    }

                    #endregion

                    #region Pre Post [Working Days calculation]

                    //Check Pre Post Working Days calculation 
                    //this check is for Team member application only
                    //IsPost field checking should be added for Mentor and HR

                    if (leaveApplication.Leave.Applicationtype == null || leaveApplication.Leave.Applicationtype == 1)
                        // null=Private leave & Without Pay Leave, 1 = Pre, 2 = Post
                    {
                        int workingDays = GetMemberWorkingDateDifference(leaveApplication.TeamMember,
                            leaveApplication.CreationDate, leaveApplication.DateFrom);
                        if (leaveApplication.Leave.ApplicationDays >= workingDays)
                        {
                            message = "You have to apply before " + leaveApplication.Leave.ApplicationDays +
                                      " working days";
                            throw new InvalidDataException(message);
                        }
                    }
                    else
                    {
                        int workingDays = GetMemberWorkingDateDifference(leaveApplication.TeamMember,
                            leaveApplication.CreationDate, leaveApplication.DateTo);
                        if ((leaveApplication.Leave.ApplicationDays + 1)*-1 > workingDays)
                        {
                            message = "You have to apply within " + leaveApplication.Leave.ApplicationDays +
                                      " working days";
                            throw new InvalidDataException(message);
                        }
                    }

                    #endregion
                }
                //Minimum Leave Check
                //Need to add extra field in leave settings 
            }
            catch (EmptyFieldException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return leaveApplication;
        }

        private int GetMemberWorkingDateDifference(TeamMember member, DateTime createdDateTime, DateTime applicationDateTime)
        {
            int memWeekEnd = 0;
            createdDateTime = new DateTime(createdDateTime.Year, createdDateTime.Month, createdDateTime.Day);
            applicationDateTime = new DateTime(applicationDateTime.Year, applicationDateTime.Month, applicationDateTime.Day);

            //Select Member org by application date
            Organization memberOrg = _hrTeamMemberDao.GetTeamMemberOrganization(applicationDateTime, member.Id, null);

            #region Select Weekend
            //Select Member weekday by application date
            var shiftWeekendHistory = _hrShiftWeekendHistoryDao.GetLastShiftWeekendHistory(member, applicationDateTime);
            if (shiftWeekendHistory != null)
            {
                memWeekEnd = shiftWeekendHistory.Weekend.Value;
            }
            #endregion

            DateTime startDate = createdDateTime;
            DateTime endDate = applicationDateTime;
            DateTime tempDate;
            //If leave is poste dated then user can apply for leave after enjoying the leave. That's why date is swapped.
            if (createdDateTime > applicationDateTime)
            {
                tempDate = startDate;
                startDate = endDate;
                endDate = tempDate;
            }
            tempDate = startDate;

            int workingDaysCount = 0;
            //int length = (int)(endDate - startDate).TotalDays + 1;

            #region Woking day Count
            for (DateTime td = startDate; td <= endDate; td = td.AddDays(1))
            {
                if ((int)Enum.Parse(typeof(WeekDay), td.DayOfWeek.ToString()) == memWeekEnd)
                    continue;
                if (_holidaySettingDao.IsHolidayByOrganizationAndDate(memberOrg.Id, td) == true)
                    continue;

                //Working days increment
                workingDaysCount++;

                //tempDate = tempDate.AddDays(1); //Get next day to check
            }
            #endregion

            //If Postdated leave then return negtive value
            if (createdDateTime > applicationDateTime)
            {
                workingDaysCount = workingDaysCount * -1;
            }

            return workingDaysCount;
        }

        private LeaveApplicationLog LeaveApplicationToLeaveApplicationLog(LeaveApplication leaveApplication)
        {
            LeaveApplicationLog nlal = new LeaveApplicationLog();
            nlal.LeaveApplicationId = leaveApplication.Id;
            nlal.TeamMemberId = leaveApplication.TeamMember.Id;
            nlal.LeaveId = leaveApplication.Leave.Id;
            nlal.DateFrom = leaveApplication.DateFrom;
            nlal.DateTo = leaveApplication.DateTo;
            nlal.LeaveNote = leaveApplication.LeaveNote;
            if (leaveApplication.ResponsiblePerson != null)
            {
                nlal.ResponsiblePersonId = leaveApplication.ResponsiblePerson.Id;
            }
            nlal.LeaveStatus = leaveApplication.LeaveStatus;
            nlal.Remarks = leaveApplication.Remarks;
            //nlal.BusinessId = "Added & Approved By Hr";
            return nlal;
        }

        private void MemberLeaveDates(long responsiblePersonId, DateTime dateFrom, DateTime dateTo, string message2)
        {
            for (DateTime d = dateFrom; d <= dateTo; d = d.AddDays(1))
            {
                if (_hrLeaveApplicationDao.MemberLeaveDates(responsiblePersonId, d) == true)
                {
                    throw new DuplicateEntryException("Responsible person already take leave for the date " + d.ToString("dd MMM, yyyy") + message2);
                }
            }
        }

        private void MemberResponsibleLeaveDates(long teamMemberId, DateTime dateFrom, DateTime dateTo, string message2)
        {
            for (DateTime d = dateFrom; d <= dateTo; d = d.AddDays(1))
            {
                LeaveApplication la = _hrLeaveApplicationDao.MemberResponsibleLeaveApplication(teamMemberId, d);
                if (la != null)
                {
                    throw new DuplicateEntryException("You are responsible person for " + la.TeamMember.Name + "'s leave for date " + d.ToString("dd MMM, yyyy") + message2);
                }
            }
        }

        private void SaveSmsHistory(LeaveApplication la, List<String> smsTypes)
        {
            var teamMember = la.TeamMember;
            object memberDes, memberDep, memberOrg;
            _hrEmploymentHistoryDao.GetCurrentInformations(teamMember.Id, out memberDes, out memberDep, out memberOrg, la.DateFrom);

            if (memberDes == null || memberDep == null || memberOrg == null)
                throw new InvalidDataException("Member designation or department or organization not found.");

            var responsiblePerson = la.ResponsiblePerson;

            object responsiblePersonDes, responsiblePersonDep, responsiblePersonOrg;
            if (responsiblePerson != null)
            {
                _hrEmploymentHistoryDao.GetCurrentInformations(responsiblePerson.Id, out responsiblePersonDes,
                    out responsiblePersonDep,
                    out responsiblePersonOrg, la.DateFrom);

                if (responsiblePersonDes == null || responsiblePersonDep == null || responsiblePersonOrg == null)
                    throw new InvalidDataException("Member designation or department or organization not found.");
            }
            else
            {
                responsiblePersonDes = new Designation();
                responsiblePersonDep = new Department();
                responsiblePersonOrg = new Organization();
            }


            var mentorHistory = _mentorHistoryDao.GetTeamMemberMentorHistory(la.DateFrom, teamMember.Id, teamMember.Pin);

            if (mentorHistory == null)
                throw new InvalidDataException("Member Mentor not found.");

            var mentor = mentorHistory.Mentor;
            object mentorDes, mentorDep, mentorOrg;
            _hrEmploymentHistoryDao.GetCurrentInformations(mentor.Id, out mentorDes, out mentorDep,
                out mentorOrg, la.DateFrom);

            if (mentorDes == null || mentorDep == null || mentorOrg == null)
                throw new InvalidDataException("Mentor designation or department or organization not found.");

            var smsSettingses = _smsSettingsDao.SmsSettingsList(smsTypes);

            var leavApplicationtoMentor = smsSettingses.Where(x => x.SmsType.Name == "Leave Application(To Mentor)" && x.Organization == (Organization)mentorOrg)
                .ToList().FirstOrDefault();
            if (leavApplicationtoMentor != null)
            {
                var smsHistoryList = GenerateSmsHistoryTemplate(leavApplicationtoMentor, teamMember, responsiblePerson,
                    mentor, (Designation)memberDes, (Department)memberDep, (Organization)memberOrg,
                    (Designation)responsiblePersonDes, (Department)responsiblePersonDep, (Organization)responsiblePersonOrg,
                    (Designation)mentorDes, (Department)mentorDep, (Organization)mentorOrg,
                    la);

                if (smsHistoryList.Any())
                {
                    foreach (var smsHistory in smsHistoryList)
                    {
                        _hrHistoryDao.Save(smsHistory);
                    }
                }
            }

            var leavApplicationtoResponsiblePerson = smsSettingses.Where(x => x.SmsType.Name == "Leave Application(To Responsible Person)" && x.Organization == (Organization)responsiblePersonOrg)
                .ToList().FirstOrDefault();
            if (leavApplicationtoResponsiblePerson != null && la.ResponsiblePerson != null)
            {
                var smsHistoryList = GenerateSmsHistoryTemplate(leavApplicationtoResponsiblePerson, teamMember, responsiblePerson,
                    mentor, (Designation)memberDes, (Department)memberDep, (Organization)memberOrg,
                    (Designation)responsiblePersonDes, (Department)responsiblePersonDep, (Organization)responsiblePersonOrg,
                    (Designation)mentorDes, (Department)mentorDep, (Organization)mentorOrg,
                    la);

                if (smsHistoryList.Any())
                {
                    foreach (var smsHistory in smsHistoryList)
                    {
                        _hrHistoryDao.Save(smsHistory);
                    }
                }
            }

            var leavApproveApplicant = smsSettingses.Where(x => x.SmsType.Name == "Leave Approve(To Applicant)" && x.Organization == (Organization)memberOrg)
                .ToList().FirstOrDefault();
            if (leavApproveApplicant != null)
            {
                var smsHistoryList = GenerateSmsHistoryTemplate(leavApproveApplicant, teamMember, responsiblePerson,
                    mentor, (Designation)memberDes, (Department)memberDep, (Organization)memberOrg,
                    (Designation)responsiblePersonDes, (Department)responsiblePersonDep, (Organization)responsiblePersonOrg,
                    (Designation)mentorDes, (Department)mentorDep, (Organization)mentorOrg,
                    la);

                if (smsHistoryList.Any())
                {
                    foreach (var smsHistory in smsHistoryList)
                    {
                        _hrHistoryDao.Save(smsHistory);
                    }
                }
            }

            var leavRejectApplicant = smsSettingses.Where(x => x.SmsType.Name == "Leave Reject(To Applicant)" && x.Organization == (Organization)memberOrg)
                .ToList().FirstOrDefault();
            if (leavRejectApplicant != null)
            {
                var smsHistoryList = GenerateSmsHistoryTemplate(leavRejectApplicant, teamMember, responsiblePerson,
                    mentor, (Designation)memberDes, (Department)memberDep, (Organization)memberOrg,
                    (Designation)responsiblePersonDes, (Department)responsiblePersonDep, (Organization)responsiblePersonOrg,
                    (Designation)mentorDes, (Department)mentorDep, (Organization)mentorOrg,
                    la);

                if (smsHistoryList.Any())
                {
                    foreach (var smsHistory in smsHistoryList)
                    {
                        _hrHistoryDao.Save(smsHistory);
                    }
                }
            }

        }

        private List<SmsHistory> GenerateSmsHistoryTemplate(SmsSettings smsSettings, TeamMember teamMember, TeamMember responsiblePerson, TeamMember mentor,
            Designation memberDes, Department memberDep, Organization memberOrg,
            Designation responsiblePersonDes, Department responsiblePersonDep, Organization responsiblePersonOrg,
            Designation mentorDes, Department mentorDep, Organization mentorOrg, LeaveApplication la
            )
        {
            // long smsUserId = Convert.ToInt64(ConfigurationManager.AppSettings["SmsUserId"]);
            long smsUserId = 1;
            var smsHistoryList = new List<SmsHistory>();
            var options = smsSettings.SmsType.DynamicOptions;
            var message = smsSettings.Template;
            var mask = smsSettings.MaskName;

            var memberOfficialDetalis = teamMember.MemberOfficialDetails.FirstOrDefault();
            var teamMemberMobile = teamMember.PersonalContact;

            if (memberOfficialDetalis != null && !String.IsNullOrEmpty(memberOfficialDetalis.OfficialContact))
            {
                teamMemberMobile = memberOfficialDetalis.OfficialContact;
            }

            var responsiblePersonMobile = "";
            if (responsiblePerson != null)
            {
                var responsiblePersonOfficialDetalis = responsiblePerson.MemberOfficialDetails.FirstOrDefault();
                responsiblePersonMobile = responsiblePerson.PersonalContact;
                if (responsiblePersonOfficialDetalis != null && !String.IsNullOrEmpty(responsiblePersonOfficialDetalis.OfficialContact))
                {
                    responsiblePersonMobile = responsiblePersonOfficialDetalis.OfficialContact;
                }
            }



            var mentorOfficialDetalis = mentor.MemberOfficialDetails.FirstOrDefault();
            var mentorMobile = mentor.PersonalContact;
            if (mentorOfficialDetalis != null && !String.IsNullOrEmpty(mentorOfficialDetalis.OfficialContact))
            {
                mentorMobile = mentorOfficialDetalis.OfficialContact;
            }


            //var leaveTypeShortName = la.le

            //  var smsToBeSend = new List<SendSmsViewModel>();

            #region Generate SMS from Template
            if (options.Any())
            {
                foreach (var o in options)
                {
                    string optionField = "[[{{" + o.Name + "}}]]";
                    string optionValue = "";

                    switch (o.Name)
                    {
                        case "Member Full Name":
                            optionValue = teamMember.FullNameEng;
                            break;
                        case "Member Nick Name":
                            optionValue = teamMember.Name;
                            break;
                        case "Member Pin":
                            optionValue = teamMember.Pin.ToString();
                            break;
                        case "Responsible Person Full Name":
                            if (responsiblePerson != null)
                            {
                                optionValue = responsiblePerson.FullNameEng;
                            }

                            break;
                        case "Responsible Person Nick Name":
                            if (responsiblePerson != null)
                            {
                                optionValue = responsiblePerson.Name;
                            }

                            break;
                        case "Responsible Person Pin":
                            if (responsiblePerson != null)
                            {
                                optionValue = responsiblePerson.Pin.ToString();
                            }

                            break;
                        case "Responsible Person Mobile":
                            if (responsiblePersonMobile != null && !String.IsNullOrEmpty(responsiblePersonMobile))
                            {
                                optionValue = responsiblePersonMobile;
                            }

                            break;
                        case "Mentor Full Name":
                            optionValue = mentor.FullNameEng;
                            break;
                        case "Mentor Nick Name":
                            optionValue = mentor.Name;
                            break;
                        case "Mentor Pin":
                            optionValue = mentor.Pin.ToString();
                            break;
                        case "Mentor Mobile":
                            if (mentorMobile != null && !String.IsNullOrEmpty(mentorMobile))
                            {
                                optionValue = mentorMobile;
                            }
                            break;
                        case "Leave Type(Full Name)":
                            optionValue = la.Leave.Name;
                            break;
                        case "Leave Date":
                            if (la.TotalLeaveDay > 1)
                            {
                                optionValue = " from " + la.DateFrom.ToString("dd MMM yyyy") + " to " + la.DateTo.ToString("dd MMM yyyy");
                            }
                            else
                            {
                                optionValue = " on " + la.DateFrom.ToString("dd MMM yyyy");
                            }

                            break;
                        case "Leave Type(Short Name)":
                            optionValue = la.Leave.Name;
                            break;
                        default:
                            optionValue = "";
                            break;
                    }
                    message = message.Replace(optionField, optionValue);
                }
            }

            #endregion

            #region Listing Receivers

            var smsToBeSend = new List<SendHrSmsViewModel>();
            foreach (var sr in smsSettings.SmsReceivers)
            {
                var svm = new SendHrSmsViewModel();
                switch (sr.Name)
                {
                    case "Mobile Number (Responsible Person)":
                        responsiblePersonMobile = CheckMobileNumber(responsiblePersonMobile);
                        if (responsiblePersonMobile != null && responsiblePerson != null && responsiblePersonOrg != null)
                        {
                            svm.SmsReceiverId = sr.Id;
                            svm.ReceiverNumber = "88" + responsiblePersonMobile;
                            svm.TeamMemberId = responsiblePerson.Id;
                            svm.OrganizationId = responsiblePersonOrg.Id;
                            svm.Organization = responsiblePersonOrg;
                        }
                        break;
                    case "Mobile Number (Applicant)":
                        teamMemberMobile = CheckMobileNumber(teamMemberMobile);
                        if (teamMemberMobile != null)
                        {
                            svm.SmsReceiverId = sr.Id;
                            svm.ReceiverNumber = "88" + teamMemberMobile;
                            svm.TeamMemberId = teamMember.Id;
                            svm.OrganizationId = memberOrg.Id;
                            svm.Organization = memberOrg;
                        }
                        break;
                    case "Mobile Number (Mentor)":
                        mentorMobile = CheckMobileNumber(mentorMobile);
                        if (mentorMobile != null)
                        {
                            svm.SmsReceiverId = sr.Id;
                            svm.ReceiverNumber = "88" + mentorMobile;
                            svm.TeamMemberId = mentor.Id;
                            svm.OrganizationId = mentorOrg.Id;
                            svm.Organization = mentorOrg;
                        }
                        break;


                    default:
                        break;
                }

                if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                {
                    var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                    if (!alreadyExist.Any())
                    {
                        smsToBeSend.Add(svm);
                    }
                }
            }
            #endregion

            #region Generate Sms History List
            if (smsToBeSend.Any())
            {
                if (String.IsNullOrEmpty(mask))
                {
                    mask = "";
                }
                foreach (var stbs in smsToBeSend)
                {

                    var smsHistory = new SmsHistory();
                    var currentNumber = stbs;
                    smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                    smsHistory.SmsReceiver = smsSettings.SmsReceivers.FirstOrDefault(x => x.Id == currentNumber.SmsReceiverId);
                    smsHistory.Sms = message;

                    smsHistory.Organization = stbs.Organization;
                    smsHistory.SmsSettings = smsSettings;
                    smsHistory.Mask = mask;
                    smsHistory.Type = Convert.ToInt32(smsSettings.SmsType.Id);
                    smsHistory.Status = SmsHistory.EntityStatus.Pending;
                    smsHistory.CreateBy = smsUserId;
                    smsHistory.ModifyBy = smsUserId;
                    smsHistory.Priority = 4;
                    smsHistoryList.Add(smsHistory);
                }
            }

            #endregion

            return smsHistoryList;
        }

        public static string CheckMobileNumber(string mobile)
        {
            if (mobile != null)
            {
                var regex = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
                var match = regex.Match(mobile);
                if (match.Success == true)
                {
                    switch (mobile.Length)
                    {
                        case 14:
                            mobile = mobile.Substring(3);
                            break;
                        case 13:
                            mobile = mobile.Substring(2);
                            break;
                        case 10:
                            mobile = "0" + mobile;
                            break;
                    }
                }
                else { mobile = null; }
            }
            return mobile;
        }

        #endregion

    }
}