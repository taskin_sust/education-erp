﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net.Config;
using DeviceSynchronizer.Helper;

namespace DeviceSynchronizer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //prevent run duplicate copy
            bool newCopy;
            string appName = BusinessRules.AppName;
            System.Threading.Mutex mutex = new System.Threading.Mutex(true, appName, out newCopy);
            if (newCopy == false)
            {
                MessageBox.Show("\"" + appName + "\" application is already running", "Duplicate instance", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            //handle arg
            if (args.Length > 1 && args[0].Equals("-dev", StringComparison.CurrentCultureIgnoreCase))
            {
                bool startDev = false;
                Boolean.TryParse(args[1], out startDev);
                BusinessRules.DevMode =  startDev;
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "log4net.config"));
            //Application.Run(new AttendanceSynchronizer());

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.ThreadException += Application_ThreadException;
            Application.Run(new MainForm());
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Console.WriteLine(e.ToString());
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ToString());
        }

    }
}
