﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.ObjectFactory.UserAuth
{
    public class UserProfileFactory : ObjectFactoryBase<UserProfile>
    {
        #region Object Initialization

        private CampusFactory _campusFactory;
        private AspNetUserFactory _aspNetUserFactory;
        public readonly IUserService _userService;
        private ISession mediaSession;
        public UserProfileFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            mediaSession = NHibernateMediaSessionFactory.OpenSession();
            _campusFactory = new CampusFactory(this, session);
            _aspNetUserFactory = new AspNetUserFactory(this, session);
            _userService = new UserService(session);
        }

        #endregion

        #region Create

        public UserProfileFactory WithName(string name)
        {
            Object.Name = name;
            return this;
        }

        public UserProfileFactory Create()
        {
            Object = new UserProfile()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                NickName = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = UserProfile.EntityStatus.Active,
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public UserProfileFactory WithCampus()
        {
            _campusFactory.Create().WithBranch().Persist();
            Object.Campus = _campusFactory.Object;
            return this;
        }

        public UserProfileFactory WithCampus(Campus campus)
        {
            Object.Campus = campus;
            return this;
        }
        public UserProfileFactory WithBranch(Branch branch)
        {
            Object.Branch = branch;
            return this;
        }
        public UserProfileFactory WithAspNetUser(AspNetUser aspNetUser)
        {
            Object.AspNetUser = aspNetUser;
            return this;
        }

        public UserProfileFactory Persist()
        {
            Images imgObj = new Images();
            imgObj.Hash = "";
            imgObj.UserImages = Encoding.ASCII.GetBytes("hahaha");
            imgObj.Name = "";
            imgObj.Name = "test";

            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    _userService.Save(x, imgObj);
                }
            });           

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _userService.Save(item, imgObj);
                    }
                }
            }
            return this;
        }

        public UserProfileFactory CreateMore(int numberOfUser)
        {
            var camNamePrefix = Guid.NewGuid().ToString();
            _campusFactory.Create().WithNameAndRank(camNamePrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"), 1).WithBranch().Persist();
            for (int i = 0; i < numberOfUser; i++)
            {
                var usr = new UserProfile() { Id = 0, NickName = Name + "_" + i, CreateBy = 1, ModifyBy = 1, Status = UserProfile.EntityStatus.Active };
                usr.Campus = _campusFactory.Object;
                usr.Branch = _campusFactory.Object.Branch;
                usr.AspNetUser = _aspNetUserFactory.GetAspNetUser().Object;
                ObjectList.Add(usr);
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup

        internal void Cleanup()
        {
            DeleteAll();
            _campusFactory.Cleanup();
        }
        #endregion

        internal void UserProgfileWithImageCleanup(UserProfile userProfile)
        {
            DeleteTableValue(userProfile);
        }
        internal void UserProgfileWithImageCleanup(TeamMember teamMember)
        {
            if (teamMember!=null && teamMember.UserProfile!=null)
                DeleteTableValue(teamMember.UserProfile);
        }

        private void DeleteTableValue(UserProfile userProfile)
        {
            if (userProfile != null)
            {
                var userProfileId = userProfile.Id;
                IEmployeeImageMediaService employeeImageMediaService = new EmployeeImageMediaService();
                var userImage =
                    _session.QueryOver<Images>().Where(x => x.User.Id == userProfileId).Take(1).SingleOrDefault<Images>();

                if (userImage != null)
                {
                    EmployeeMediaImage mediaEmployeeImages = employeeImageMediaService.GetEmployeeImageMediaByRefId(userImage.Id);
                    var tableName = GetEntityTableName(typeof(EmployeeMediaImage));
                    string a = "DELETE FROM " + tableName + " WHERE [Id] =" + mediaEmployeeImages.Id + ";";
                    mediaSession.CreateSQLQuery(a).ExecuteUpdate();
                    mediaSession.Flush();
                }
                if (userProfileId > 0)
                {
                    var tableName = GetEntityTableName(typeof(Images));
                    string a = "DELETE FROM "+tableName+" WHERE [User_id] =" + userProfileId + ";";
                    _session.CreateSQLQuery(a).ExecuteUpdate();
                    _session.Flush();

                    var tableName2 = GetEntityTableName(typeof(UserProfile));
                    string b = "DELETE FROM "+tableName2+" WHERE [Id] =" + userProfileId + ";";
                    _session.CreateSQLQuery(b).ExecuteUpdate();
                    _session.Flush();
                }
            }
        }
    }
}
