﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Teachers;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class ExamProgressController : Controller
    {
        #region Logger

        private ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IProgramService _programService;
        private readonly IBranchService _branchService;
        private readonly IBatchService _batchService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ISubjectService _subjectService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IStudentClassAttendanceService _classAttendanceService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ICampusService _campusService;
        private readonly IExamsService _examsService;
        private readonly IExamsStudentMarksService _examsStudentMarksService;
        private readonly IExamsDetailsService _examsDetailsService;
        private List<UserMenu> _userMenu;
        private readonly IOrganizationService _organizationService;
        private readonly ITeacherService _teacherService;
        private readonly IStudentClassAttendanceService _studentClassAttendanceService;
        private readonly ILectureService _lectureService;
        private readonly IStudentExamAttendanceService _studentExamAttendanceService;
        private readonly ICourseSubjectService _courseSubjectService;
        private readonly ICourseProgressService _courseProgressService;
        private readonly CommonHelper _commonHelper;
        private readonly IUserService _userService;
        public ExamProgressController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _teacherService = new TeacherService(session);
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _batchService = new BatchService(session);
                _sessionService = new SessionService(session);
                _classAttendanceService = new StudentClassAttendanceService(session);
                _courseService = new CourseService(session);
                _subjectService = new SubjectService(session);
                _studentProgramService = new StudentProgramService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _campusService = new CampusService(session);
                _examsService = new ExamsService(session);
                _examsStudentMarksService = new ExamsStudentMarksService(session);
                _examsDetailsService = new ExamsDetailsService(session);
                _organizationService = new OrganizationService(session);
                _studentClassAttendanceService = new StudentClassAttendanceService(session);
                _studentExamAttendanceService = new StudentExamAttendanceService(session);
                _lectureService = new LectureService(session);
                _courseSubjectService = new CourseSubjectService(session);
                _courseProgressService = new CourseProgressService(session);
                _userService = new UserService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                if (ex.InnerException != null)
                {
                    _logger.Error("Course Progress Controller Constructor Can't be Initialize  due to " +
                                  ex.InnerException);
                }
                else
                {
                    _logger.Error("Course Progress Controller Constructor Can't be Initialize  due to " + ex.Message);
                }
                Console.WriteLine("{0}", ex.Message);
            }
        }

        #endregion

        #region Operational Function

        #region Save Operation
        public ActionResult EnterExamProgress(string successMessage = "", string errorMessage = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(successMessage))
                {
                    ViewBag.SuccessMessage = successMessage;
                }
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    ViewBag.ErrorMessage = errorMessage;
                }
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                SelectList selectList = new SelectList(organizations, "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.orgList = selectList;
                ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
                
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult EnterExamProgress(ExamProgressViewModel examProgressObj, string date)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                SelectList selectList = new SelectList(organizations, "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.orgList = selectList;
                ViewBag.SelectedExam = new SelectList(new List<Subject>(), "Id", "Name");
                List<Batch> batchList = new List<Batch>();
                if (examProgressObj.SelectedBatch.Contains(SelectionType.SelelectAll))
                {
                    batchList =
                        _batchService.LoadAuthorizeBatch(_userMenu,
                            _commonHelper.ConvertIdToList(examProgressObj.OrganizationId),
                            _commonHelper.ConvertIdToList(examProgressObj.ProgramId),
                            examProgressObj.SelectedBranch.ToList(),
                            _commonHelper.ConvertIdToList(examProgressObj.SessionId),
                            examProgressObj.SelectedCampus.ToList(), examProgressObj.SelectedBatchDays,
                            examProgressObj.SelectedBatchTime).ToList();
                }
                else
                {
                    batchList = _batchService.LoadBatch(null, null, null, null, null, examProgressObj.SelectedBatch.ToList()).ToList();
                }
                IList<Exams> examList = new List<Exams>();
                if (examProgressObj.SelectedExam.Contains(SelectionType.SelelectAll))
                {
                    examList = _examsService.LoadExamByProgramAndSessionAndBranchsAndCampusesAndBatches(_userMenu, examProgressObj.ProgramId,
                        examProgressObj.SessionId, examProgressObj.SelectedCourse,
                        examProgressObj.SelectedBranch, examProgressObj.SelectedCampus, examProgressObj.SelectedBatch, null, null, null);

                }
                else
                {
                    examList = _examsService.LoadExams(examProgressObj.SelectedExam.ToList());
                }
                var success = _courseProgressService.SaveExamProgress(examList.ToList(), batchList, Convert.ToDateTime(date));
                if (success)
                {
                    return RedirectToAction("EnterExamProgress", new { successMessage = "Exam progress saved successfully." });
                }
                else
                {
                    return RedirectToAction("EnterExamProgress", new { errorMessage = WebHelper.CommonErrorMessage });
                }
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                return RedirectToAction("EnterExamProgress", new { errorMessage = WebHelper.CommonErrorMessage });
            }
            return null;
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isSuccess = _courseProgressService.DeleteExamProgress(id);
                return Json(isSuccess ? new Response(true, "Exam progress sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Exam progress Delete Fail !"));
            }
        }
        #endregion

        #region Details Operation

        public ActionResult Details(int id)
        {
            var examProgressObj = _courseProgressService.GetExamProgress(id);
            if (examProgressObj != null)
            {
                examProgressObj.CreateByText = _userService.GetUserNameByAspNetUserId(examProgressObj.CreateBy);
                examProgressObj.ModifyByText = _userService.GetUserNameByAspNetUserId(examProgressObj.ModifyBy);
            }
            return View(examProgressObj);
        }
        #endregion
        #endregion

        #region Index/Manage
        public ActionResult ManageExamProgress()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                SelectList selectList = new SelectList(organizations, "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.orgList = selectList;
                ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult ManageExamProgress(ExamProgressViewModel examProgressObj)
        {
            try
            {
                var json = new JavaScriptSerializer().Serialize(examProgressObj);
                ViewData["examProgressObj"] = json;
                return View("GenerateExamProgressReport");
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return View();
        }

        #region Render Data Table
        public JsonResult GenerateExamProgressReport(int draw, int start, int length, string branch, string campus,
            string batch,string subject, string exam, string examProgressObjString)
        {
            var data = new List<object>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var examProgressObj =
                    new JavaScriptSerializer().Deserialize<ExamProgressViewModel>(
                        examProgressObjString);

                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(examProgressObj.OrganizationId), _commonHelper.ConvertIdToList(examProgressObj.ProgramId), examProgressObj.SelectedBranch.ToList(), _commonHelper.ConvertIdToList(examProgressObj.SessionId), examProgressObj.SelectedCampus.ToList(), examProgressObj.SelectedBatchDays, examProgressObj.SelectedBatchTime, null, null).ToList();
                if (examProgressObj.SelectedBatch[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(examProgressObj.SelectedBatch)).ToList();
                }
                List<Branch> branchList = _branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(examProgressObj.OrganizationId), _commonHelper.ConvertIdToList(examProgressObj.ProgramId), _commonHelper.ConvertIdToList(examProgressObj.SessionId)).ToList();
                if (examProgressObj.SelectedBranch[0] != 0)
                {
                    branchList = branchList.Where(b => b.Id.In(examProgressObj.SelectedBranch)).ToList();
                }
                IList<Exams> examList = _examsService.LoadExams(examProgressObj.ProgramId, examProgressObj.SessionId, examProgressObj.SelectedCourse, batchList, branchList, null,true);
                
                int recordsTotal = _courseProgressService.GetExamProgressCount(branch, campus, batch,
                    subject, exam, examList, batchList);
                int recordsFiltered = recordsTotal;
                var examProgressReportList = new List<ExamProgress>();
                examProgressReportList = _courseProgressService.LoadExamProgress(start, length, branch, campus,
                    batch,
                    subject, exam, examList, batchList);
                int sl = start;
                foreach (var examprogress in examProgressReportList)
                {
                    sl++;
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    str.Add(examprogress.Batch.Branch.Name);
                    str.Add(examprogress.Batch.Campus.Name);
                    str.Add(examprogress.Batch.Name);
                    if (examprogress.Exam.ExamDetails[0].SubjectType != 3)
                    {
                        var subjects = string.Join(", ", examprogress.Exam.ExamDetails.Select(x => x.Subject.Name).ToList());
                        str.Add(subjects);
                    }
                    else
                    {
                        str.Add("Combined");
                    }
                    str.Add(examprogress.Exam.Name);
                    if (examprogress.HeldDate != null)
                    {
                        str.Add(examprogress.HeldDate.ToString("dd-MM-yyyy"));
                    }
                    else
                    {
                        str.Add("");
                    }
                    var detailsLink = LinkGenerator.GetDetailsLink("Details", "ExamProgress", examprogress.Id);
                    var deleteLink = LinkGenerator.GetDeleteLinkForModal(examprogress.Id, examprogress.Exam.Name + " in batch: " + examprogress.Batch.Name);
                    str.Add(detailsLink + deleteLink);
                    data.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data,
                    isSuccess = false
                });
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = data,
                isSuccess = false
            });
        }
        #endregion
        #endregion

        #region Helper Function
        [HttpPost]
        public ActionResult GetLecture(List<long> organizationIds, List<long> programIds, List<long> branchIds,
            List<long> sessionIds, List<long> campusIds, string[] batchDays, string[] batchTimes, List<long> batchIds,
            List<long> courseIds, List<long> subjectIds)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = new List<Batch>();
                if (batchIds.Contains(SelectionType.SelelectAll))
                {
                    batchList =
                        _batchService.LoadAuthorizeBatch(_userMenu, organizationIds, programIds, branchIds, sessionIds,
                            campusIds, batchDays, batchTimes).ToList();
                }
                else
                {
                    //batchList = _batchService.LoadBatch(batchIds).ToList();//remove
                    batchList = _batchService.LoadBatch(null, null, null, null, null, batchIds).ToList();//remove
                }
                var lectureList = _lectureService.LoadLecture(programIds, sessionIds, courseIds, subjectIds, batchList,
                    false);

                var lectureSelectList = new SelectList(lectureList.DistinctBy(x => x.Id), "Id", "LectureName").ToList();
                //lectureSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Lecture" });
                return Json(new { lectureList = lectureSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult GetSubject(long programId, long sessionId, List<long> courseIdList)
        {
            try
            {
                var courseSubjectList = _courseSubjectService.LoadCourseSubject(programId, sessionId, courseIdList);
                var subjectSelectList = new List<SelectListItem>();
                //subjectSelectList.Add(new SelectListItem() { Text = "Select Subject", Value = "" });
                if (courseSubjectList != null && courseSubjectList.Count > 0)
                {
                    //subjectSelectList.Add(new SelectListItem() {Text = "Select subject", Value = ""});
                    foreach (var courseSubject in courseSubjectList)
                    {
                        if (courseSubject != null)
                        {
                            subjectSelectList.Add(new SelectListItem()
                            {
                                Text = courseSubject.Subject.Name,
                                Value = courseSubject.Subject.Id.ToString()
                            });
                        }

                    }
                    if (courseSubjectList.Contains(null))
                    {
                        subjectSelectList.Add(new SelectListItem() { Text = "Combined", Value = "-1" });
                    }
                    subjectSelectList = subjectSelectList.DistinctBy(x => x.Value).ToList();
                }
                return Json(new { subjectList = subjectSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult GetExamForEnterExamProgress(long programId, long sessionId, long[] courseId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //IList<Exams> examList =
                //    _examsService.LoadExams(_userMenu, programId,
                //        sessionId, courseId,
                //         batchId);
                IList<Exams> examList = _examsService.GetExamByProgramAndSessionAndBranchsAndCampusesAndBatches(programId, sessionId, courseId, branchId, campusId, batchId);
                List<SelectListItem> selectexamList = (new SelectList(examList, "Id", "Name")).ToList();
                if (examList.Any())
                {
                    selectexamList.Insert(0, new SelectListItem() { Value = "", Text = "Select Exam" });
                    return Json(new { examList = selectexamList, IsSuccess = true });
                }
                return Json(new { examList = new List<Exams>(), IsSuccess = true });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return Json(new { examList = new List<Exams>(), IsSuccess = false });

            }
            
        }
        public ActionResult LoadExams(long organizationId,long programId, long sessionId, long[] courseId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime, null, null).ToList();
                if (batchId[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(batchId)).ToList();
                }
                List<Branch> branchList = _branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId)).ToList();
                if (branchId[0] != 0)
                {
                    branchList = branchList.Where(b => b.Id.In(branchId)).ToList();
                }
                IList<Exams> examList = _examsService.LoadExams(programId, sessionId, courseId, batchList, branchList,true);
                List<SelectListItem> selectexamList = (new SelectList(examList, "Id", "Name")).ToList();
                if (examList.Any())
                {
                    selectexamList.Insert(0, new SelectListItem() { Value = "", Text = "Select Exam" });
                    return Json(new { examList = selectexamList, IsSuccess = true });
                }
                return Json(new { examList = new List<Exams>(), IsSuccess = true });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return Json(new { examList = new List<Exams>(), IsSuccess = false });

            }

        }
       
        #endregion
    }
} ;