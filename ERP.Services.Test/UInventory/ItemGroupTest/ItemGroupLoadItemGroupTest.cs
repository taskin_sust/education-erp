﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.ItemGroupTest
{
    interface IItemGroupLoadItemGroupTest
    {
        #region Basic Test
        void Should_Return_LoadActive_List();
        void should_Return_Load_All_ItemGroup_List();
        void Should_Return_ItemGroup_List();
        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "ItemGroup")]
    public class ItemGroupLoadItemGroupTest : ItemGroupBaseTest, IItemGroupLoadItemGroupTest
    {
        [Fact]
        public void Should_Return_LoadActive_List()
        {
            const int numOfItemGroup = 10;
            var itemGroupFactory = _itemGroupFactory.CreateMore(numOfItemGroup).Persist();
            var listCount = _itemGroupService.LoadItemGroup(itemGroupFactory.ObjectList.Select(y => y.Id).ToList()).Count;
            Assert.StrictEqual(listCount, numOfItemGroup);
        }

        [Fact]
        public void should_Return_Load_All_ItemGroup_List()
        {
            const int numOfItemGroup = 10;
            _itemGroupFactory.CreateMore(numOfItemGroup).Persist();
            var list = _itemGroupService.LoadItemGroup();
            Assert.NotEmpty(list);
        }
        
        [Fact]
        public void Should_Return_ItemGroup_List()
        {
            const int numOfItemGroup = 10;
            var itemGroupFactory = _itemGroupFactory.CreateMore(numOfItemGroup).Persist();
            var list = _itemGroupService.LoadItemGroup(0, numOfItemGroup, null, null, itemGroupFactory.ObjectList.Select(x=>x.Name).FirstOrDefault(), null);
            Assert.StrictEqual(list.Count,1);

            Assert.StrictEqual(itemGroupFactory.ObjectList.Count, numOfItemGroup);
        }
    }
}
