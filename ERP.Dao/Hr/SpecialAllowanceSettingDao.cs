using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface ISpecialAllowanceSettingDao : IBaseDao<SpecialAllowanceSetting, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        IList<SpecialAllowanceSetting> LoadSpecialAllowanceSettings(int start, int length, string orderBy, string orderDir, List<long> authorizeTeamMember, int? pin, string name, long? paymentType);

        List<SpecialAllowanceSetting> LoadIndividualTeamMemberSpecialAllowanceSetting(long teamMemberId, DateTime effectiveDate);
        #endregion

        #region Others Function

        int GetSpecialAllowanceSettingCount(string orderBy, string orderDir, List<long> authorizeTeamMember, int? pin, string name, long? paymentType);

        #endregion

        #region Helper Function

        bool HasSpecialSettings(SpecialAllowanceSetting specialAllowanceSetting);

        #endregion


        bool HasDuplicate(SpecialAllowanceSetting entity, bool isUpdate);
    }

    public class SpecialAllowanceSettingDao : BaseDao<SpecialAllowanceSetting, long>, ISpecialAllowanceSettingDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<SpecialAllowanceSetting> LoadSpecialAllowanceSettings(int start, int length, string orderBy, string orderDir, List<long> authorizeTeamMember, int? pin, string name, long? paymentType)
        {
            ICriteria criteria = GetSpecialAllowanceSettingsCriteria(orderBy, orderDir, authorizeTeamMember, pin, name, paymentType);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<SpecialAllowanceSetting>();
        }

        private ICriteria GetSpecialAllowanceSettingsCriteria(string orderBy, string orderDir, List<long> authorizeTeamMember, int? pin, string name, long? paymentType, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<SpecialAllowanceSetting>().Add(Restrictions.Not(Restrictions.Eq("Status", SpecialAllowanceSetting.EntityStatus.Delete)));
            criteria.CreateAlias("TeamMember", "teamMember").Add(Restrictions.Not(Restrictions.Eq("teamMember.Status", TeamMember.EntityStatus.Delete)));

            //List<long> teamMemberIds = authorizeTeamMember.Select(x => x.Id).ToList();
            criteria.Add(Restrictions.In("teamMember.Id", authorizeTeamMember));


            if (pin != null)
            {
                criteria.Add(Restrictions.Eq("teamMember.Pin", Convert.ToInt32(pin)));
            }

            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("teamMember.Name", name, MatchMode.Anywhere));
            }

            if (paymentType != null)
            {
                criteria.Add(Restrictions.Eq("PaymentType", Convert.ToInt32(paymentType)));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()):Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }

        public List<SpecialAllowanceSetting> LoadIndividualTeamMemberSpecialAllowanceSetting(long teamMemberId, DateTime effectiveDate)
        {
            ICriteria criteria = Session.CreateCriteria<SpecialAllowanceSetting>().Add(Restrictions.Eq("Status", SpecialAllowanceSetting.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "teamMember");

            criteria.Add(Restrictions.Eq("teamMember.Id", teamMemberId));

            criteria.Add(Restrictions.Le("EffectiveDate", effectiveDate));

            // criteria.Add(Restrictions.Ge("ClosingDate", effectiveDate));

            var disjunction = Restrictions.Disjunction(); // for OR statement 
            disjunction.Add(Restrictions.IsNull("ClosingDate") || Restrictions.Ge("ClosingDate", effectiveDate));
            criteria.Add(disjunction);

            return criteria.List<SpecialAllowanceSetting>().ToList();
        }


        #endregion

        #region Others Function

        public int GetSpecialAllowanceSettingCount(string orderBy, string orderDir, List<long> authorizeTeamMember, int? pin, string name, long? paymentType)
        {
            ICriteria criteria = GetSpecialAllowanceSettingsCriteria(orderBy, orderDir, authorizeTeamMember, pin, name, paymentType, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function

        public bool HasSpecialSettings(SpecialAllowanceSetting specialAllowanceSetting)
        {
            string query = string.Format(@"
                        SELECT COUNT(*) FROM HR_AllowanceSheetSecondDetails ash
                        INNER JOIN HR_SpecialAllowanceSetting sas ON sas.Id=ash.SpecialAllowanceSettingId
                        WHERE sas.Id={0}", specialAllowanceSetting.Id);
            IQuery iQuery = Session.CreateSQLQuery(query);
            var count = Convert.ToInt32(iQuery.UniqueResult());
            return count > 0;
        }
        #endregion



        public bool HasDuplicate(SpecialAllowanceSetting entity, bool isUpdate)
        {
            ICriteria criteria = Session.CreateCriteria<SpecialAllowanceSetting>().Add(Restrictions.Eq("Status", SpecialAllowanceSetting.EntityStatus.Active));
            criteria.SetTimeout(3000);
            criteria.SetProjection(Projections.RowCount());
            bool returnValue = false;
            int count = Convert.ToInt32(criteria.UniqueResult());
            return returnValue = count > 0 ? true : false;
        }
    }
}
