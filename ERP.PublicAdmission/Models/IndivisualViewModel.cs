﻿using System.Collections.Generic;

namespace UdvashERP.PublicAdmission.Models
{
    public class IndivisualViewModel 
    {
        public virtual string Program { get; set; }
        public virtual string Session { get; set; }
        public virtual string PrnNo { get; set; } 
        public virtual string StudentName { get; set; }
        public virtual string ExamName { get; set; }
        public virtual string McqMarks { get; set; } 
        public virtual string WrittenMarks { get; set; }
        public virtual string TotalMarks { get; set; }
        public virtual string HighestMarks { get; set; } 
        public virtual string FullMarks { get; set; }
        public virtual string Bmp { get; set; }
        public virtual string Cmp { get; set; }   

    }   
}