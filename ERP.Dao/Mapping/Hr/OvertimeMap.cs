using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class OvertimeMap : BaseClassMap<Overtime,long> {
        
        public OvertimeMap() {
			Table("HR_Overtime");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.OverTimeDate).Column("OverTimeDate");
            Map(x => x.ApprovedTime).Column("ApprovedTime");
            Map(x => x.IsPost).Column("IsPost");
			HasMany(x => x.OvertimeLog).KeyColumn("OvertimeId");
        }
    }
}
