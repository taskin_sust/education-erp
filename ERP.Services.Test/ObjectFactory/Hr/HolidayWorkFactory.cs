﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class HolidayWorkFactory : ObjectFactoryBase<HolidayWork>
    {
        private TestBaseService<HolidayWorkLog> _holidayWorkLogService;
        private readonly IHolidayWorkService _holidayWorkService;
        private TeamMemberFactory _teamMemberFactory;
        public HolidayWorkFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _holidayWorkLogService = new TestBaseService<HolidayWorkLog>(session);
            _holidayWorkService = new HolidayWorkService(session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
        }

        #region Create

        public HolidayWorkFactory Create()
        {
            Object = new HolidayWork()
            {
                HolidayWorkDate = DateTime.Now,
                ApprovalType = 1,
                Reason = "Test Reason",
                Rank = 10,
                BusinessId = GetPcUserName(),
                Status = HolidayWork.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public HolidayWorkFactory CreateWith(DateTime holidayWorkDate, int approvalType)
        {
            Object = new HolidayWork()
            {
                HolidayWorkDate = holidayWorkDate,
                ApprovalType = approvalType,
                BusinessId = GetPcUserName(),
                Rank = 10,
                Reason = "Test Reason",
                Status = HolidayWork.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public HolidayWorkFactory CreateMore(int count, IList<DateTime> holidayWorkDate, IList<int> approvalTypes,
            IList<TeamMember> teamMembers)
        {
            for (int i = count; i < count; i++)
            {
                var obj = new HolidayWork()
                {
                    HolidayWorkDate = holidayWorkDate.Count <= count ? holidayWorkDate[i] : holidayWorkDate.Last(),
                    ApprovalType = approvalTypes.Count <= count ? approvalTypes[i] : approvalTypes.Last(),
                    TeamMember = teamMembers.Count <= count ? teamMembers[i] : teamMembers.Last(),
                    Reason = "Reason_" + i.ToString(CultureInfo.InvariantCulture),
                    Rank = 10,
                    BusinessId = GetPcUserName(),
                    Status = HolidayWork.EntityStatus.Active
                };
                ObjectList.Add(obj);
            }
            return this;
        }

        public HolidayWorkFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }

        public HolidayWorkFactory WithTeamMember(TeamMember teamMember)
        {
            if (Object != null)
            {
                Object.TeamMember = teamMember;
            }
            if (ObjectList.Count > 1)
            {
                foreach (var hwObject in ObjectList)
                {
                    hwObject.TeamMember = teamMember;
                }
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region Persist

        
        public HolidayWorkFactory Persist(TeamMember tm, bool isMentor = false, bool isHr = false, List<UserMenu> userMenus = null)
        {
            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    _holidayWorkService.SaveHolidayWorkApproval(new List<HolidayWork>() { x }, tm, isMentor, isHr, userMenus);
                }
            });

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _holidayWorkService.SaveHolidayWorkApproval(new List<HolidayWork>() { item }, tm, isMentor, isHr, userMenus);
                    }
                }
            }
            return this;
        }
        

        #endregion


        #region CleanUp
        public HolidayWorkFactory CleanUp()
        {
            var adIdList = _session.QueryOver<HolidayWork>()
                                .Select(x => x.Id)
                                .Where(x => x.BusinessId == GetPcUserName().ToString(CultureInfo.InvariantCulture))
                                .List<long>();
            var ids = adIdList.Select(hId => _session.QueryOver<HolidayWorkLog>().Where(x => x.HolidayWorkId == hId).SingleOrDefault()).Select(entity => entity.Id).ToList();
            if (adIdList.Count > 0)
                _holidayWorkLogService.DeleteByIdList(ids.ToList());

            DeleteAll();
            _teamMemberFactory.Cleanup();
            return this;
        }

        public HolidayWorkFactory CleanUpFactory()
        {
            DeleteAll();
            return this;
        }

        public HolidayWorkFactory LogCleanUp(HolidayWorkFactory holiDayWorkFactory)
        {
            if (holiDayWorkFactory.ObjectList.Any())
            {
                DeleteLog(holiDayWorkFactory.ObjectList);
            }
            if (holiDayWorkFactory.SingleObjectList.Any())
            {
                DeleteLog(holiDayWorkFactory.SingleObjectList);
            }
            return this;
        }

        private void DeleteLog(IEnumerable<HolidayWork> list)
        {
            foreach (var holidayWork in list)
            {
                var tableName = GetEntityTableName(typeof(HolidayWorkLog));
                string swData = "DELETE FROM " + tableName + " WHERE [HolidayWorkId] = " + holidayWork.Id + ";";
                _session.CreateSQLQuery(swData).ExecuteUpdate();
            }
        }

        #endregion
    }
}
