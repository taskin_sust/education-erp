﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Engine;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class BankBranchFactory : ObjectFactoryBase<BankBranch>
    {
        protected readonly IBankBranchService _bankBranchService;
        protected readonly BankFactory _bankFactory;
        public BankBranchFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _bankBranchService = new BankBranchService(_session);
            _bankFactory = new BankFactory(this, _session);
        }

        #region Create

        public BankBranchFactory Create()
        {
            Object = new BankBranch()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString().Substring(0,10),
                Rank = 1,
                CreationDate = DateTime.Now,
                ModificationDate = DateTime.Now,
                CreateBy = 1,
                ModifyBy = 1,
                Status = BankBranch.EntityStatus.Active,
                ShortName = Guid.NewGuid().ToString().Substring(0,10),
                ContactPerson = Guid.NewGuid().ToString().Substring(0,10),
                ContactPersonMobile = "8801727665912",
                RoutingNo = Guid.NewGuid().ToString().Substring(0, 10),
                SwiftCode = Guid.NewGuid().ToString().Substring(0, 10)
               // BankId = Bank.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public BankBranchFactory WithBank()
        {
            _bankFactory.Create().Persist();
            Object.Bank = _bankFactory.Object;
            return this;
        }
        public BankBranchFactory WithBank(Bank bank)
        {
            Object.Bank = bank;
            return this;
        }

        public BankBranchFactory With_Routing_No(string routingNo)
        {
            Object.RoutingNo = routingNo;
            return this;
        }

        public BankBranchFactory With_Swift_Code(string swiftCode)
        {
            Object.SwiftCode = swiftCode;
            return this;
        }

        public BankBranchFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _bankBranchService.IsSave(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _bankBranchService.IsSave(item);
                    }
                }
            }
            return this;
        }


        public BankBranchFactory CreateMore(int numberOfBankBranch)
        {
            CreateBankBranchList(numberOfBankBranch);
            return this;
        }

        private List<BankBranch> CreateBankBranchList(int numberOfBankBranch)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<BankBranch>();
            var banks = _bankFactory.Create().Persist();
            for (int i = 0; i < numberOfBankBranch; i++)
            {
                var bankBranch = new BankBranch()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = Guid.NewGuid().ToString().Substring(0,10)+ "_" + i,
                    Rank = 1,
                    CreationDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = BankBranch.EntityStatus.Active,
                    ShortName = Guid.NewGuid().ToString().Substring(0,10)+ "_" + i,
                    ContactPerson = Guid.NewGuid().ToString().Substring(0,10)+ "_" + i,
                    ContactPersonMobile = "8801727665912",
                    RoutingNo = Guid.NewGuid().ToString().Substring(0, 10),
                    SwiftCode = Guid.NewGuid().ToString().Substring(0, 10),
                    //BankId = banks.Object.Id
                    Bank = banks.Object
                };
                list.Add(bankBranch);
            }
            ObjectList.AddRange(list);
            return list;
        }

        #endregion

        #region Cleanup

        public void Cleanup()
        {
            DeleteAll();
            _bankFactory.Cleanup();
        }

        #endregion
        
    }
}
