﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.Hr;
namespace UdvashERP.Services.Hr
{
    public interface ILeaveService : IBaseService
    {

        #region Operational Function

        void Save(Leave leave);
        void LeaveSettingCheckAndSave(long org, string leaveName, string isPub, string payType, string repeatType, string noOfDays, string startFrom,
            string effectiveDate, string[] genAry, string[] empStatus, string[] maritalStatus, string isForward, string maxForward, string isTakingLimit,
            string maxTaking, string appType, string appTypeDays, string isEncash, string maxEncash, string p);
        Leave GetLeave(long id);
        bool Delete(long id);
        bool Update(long id, long org, string leaveName, string isPub, string payType, string repeatType,
            string noOfDays, string startFrom, string effectiveDate, string[] genAry, string[] empStatus,
            string[] maritalStatus, string isForward, string maxForward, string isTakingLimit, string maxTaking,
            string appType, string appTypeDays, string isEncash, string maxEncash, string closingDate = "");

        #endregion

        #region Single Instances Loading Function

        Leave GetById(long hRLeaveId);
        
        #endregion

        #region List Loading Function

        IList<Leave> LoadLeave(List<long> organizationIdList = null);
        IList<Leave> LoadLeave(List<UserMenu> userMenu, long organizationId, int draw = 0, int start = 0, int length = 0);
        IList<Leave> LoadOrganizationLeaveByDate(List<UserMenu> userMenu, long organizationId, DateTime searchDate);
        IList<Leave> LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(long orgId, int? gender, int employeStatus, int maritalStatus);

        #endregion

        #region Others Function

        int GetLeaveCount(List<UserMenu> userMenu, long organizationId);

        #endregion

        #region Helper Function

        #endregion
        
    }

    public class LeaveService : BaseService, ILeaveService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly ILeaveDao _hrLeaveDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly ILeaveApplicationDao _leaveApplicationDao;
        private Dictionary<string, string> messDictionary = null;

        public LeaveService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrLeaveDao = new LeaveDao() { Session = Session };
            _organizationDao = new OrganizationDao() { Session = session };
            _leaveApplicationDao = new LeaveApplicationDao() { Session = session };
            messDictionary = new Dictionary<string, string>();
            //GenerateMessage();
        }

        private void GenerateMessage()
        {
            messDictionary.Add("EmptyLeave", "Leave name empty");
            messDictionary.Add("EmptyEffective", "Effective date empty");
            messDictionary.Add("EmptyGender", "Not selected any gender");
            messDictionary.Add("EmptyEmployeeStatus", "Not selected any employeement status");
            messDictionary.Add("EmptyMaritalStatus", "Not selected any marital status");
            messDictionary.Add("EmptyMaxCarryDays", "MaxCarryDays empty");
            messDictionary.Add("EmptymaxTaking", "maxTaking empty");
            messDictionary.Add("EmptyMinReserveDays", "Min.Encash Reserve Days empty");
            messDictionary.Add("EmptyOrg", "Organization missing");

        }

        #endregion

        #region Operational Functions

        public void Save(Leave leave)
        {

            if (leave == null)
            {
                throw new NullObjectException("Null Object!!");
            }
            if (leave is Leave)
            {
                _hrLeaveDao.Save(leave);
            }
            else
            {
                throw new TypeMismatchException("Invalid Type!!");
            }
        }

        public void LeaveSettingCheckAndSave(long org, string leaveName, string isPub, string payType, string repeatType,
            string noOfDays, string startFrom, string effectiveDate, string[] genAry, string[] empStatus,
            string[] maritalStatus,
            string isForward, string maxForward, string isTakingLimit, string maxTaking, string appType,
            string appTypeDays,
            string isEncash, string maxEncash, string closingDate = "")
        {
            try
            {
                var leave = new Leave();
                if (org <= 0)
                {
                    throw new InvalidDataException("Organization missing");
                }
                var organization = _organizationDao.LoadById(Convert.ToInt64(org));
                leave.Organization = organization;
                if (String.IsNullOrEmpty(leaveName))
                {
                    throw new InvalidDataException("Leave name empty");
                }
                leave.Name = leaveName.Trim();
                if (Convert.ToInt32(isPub) == (int)PublicLeave.Yes)
                {
                    if (String.IsNullOrEmpty(effectiveDate))
                    {
                        throw new InvalidDataException("Effective date empty");
                    }
                    leave.EffectiveDate = Convert.ToDateTime(effectiveDate.Trim());
                    if (genAry.ToList().Count <= 0)
                    {
                        throw new InvalidDataException("Not selected any gender");
                    }
                    foreach (var gender in genAry)
                    {
                        if (gender == Gender.Male.ToString())
                        {
                            leave.IsMale = true;
                        }
                        else if (gender == Gender.Female.ToString())
                        {
                            leave.IsFemale = true;
                        }
                    }
                    if (empStatus.ToList().Count <= 0)
                    {
                        throw new InvalidDataException("Not selected any employeement status");
                    }
                    foreach (var empsts in empStatus)
                    {
                        if (empsts == MemberEmploymentStatus.Probation.ToString())
                        {
                            leave.IsProbation = true;
                        }
                        else if (empsts == MemberEmploymentStatus.Permanent.ToString())
                        {
                            leave.IsPermanent = true;
                        }
                        else if (empsts == MemberEmploymentStatus.PartTime.ToString())
                        {
                            leave.IsPartTime = true;
                        }
                        else if (empsts == MemberEmploymentStatus.Contractual.ToString())
                        {
                            leave.IsContractual = true;
                        }
                        else if (empsts == MemberEmploymentStatus.Intern.ToString())
                        {
                            leave.IsIntern = true;
                        }
                    }
                    if (maritalStatus.ToList().Count <= 0)
                    {
                        throw new InvalidDataException("Not selected any marital status");
                    }
                    foreach (var maritalSts in maritalStatus)
                    {
                        if (maritalSts == MaritalType.Single.ToString())
                        {
                            leave.IsSingle = true;
                        }
                        else if (maritalSts == MaritalType.Married.ToString())
                        {
                            leave.IsMarried = true;
                        }
                        else if (maritalSts == MaritalType.Widow.ToString())
                        {
                            leave.IsWidow = true;
                        }
                        else if (maritalSts == MaritalType.Widower.ToString())
                        {
                            leave.IsWidower = true;
                        }
                        else if (maritalSts == MaritalType.Divorced.ToString())
                        {
                            leave.IsDevorced = true;
                        }
                    }
                    if (isForward != "0")
                    {
                        leave.IsCarryforward = isForward.Equals("1");
                        if (!String.IsNullOrEmpty(maxForward))
                            leave.MaxCarryDays = Convert.ToInt32(maxForward.Trim());
                        else
                        {
                            throw new InvalidDataException("MaxCarryDays empty");
                        }
                    }
                    if (isTakingLimit != "0")
                    {
                        leave.IsTakingLimit = isTakingLimit.Equals("1");
                        if (!String.IsNullOrEmpty(maxTaking))
                            leave.MaxTakingLimit = Convert.ToInt32(maxTaking.Trim());
                        else
                        {
                            throw new InvalidDataException("MaxTaking empty");
                        }
                    }
                    if (appType != "0")
                    {
                        leave.Applicationtype = Convert.ToInt32(appType);
                        if (!String.IsNullOrEmpty(appTypeDays))
                            leave.ApplicationDays = Convert.ToInt32(appTypeDays.Trim());
                    }
                    if (String.IsNullOrEmpty(isEncash))
                    {
                    }
                    else
                    {
                        if (isEncash != "0")
                        {
                            leave.IsEncash = isEncash.Equals("1");
                            if (!String.IsNullOrEmpty(maxEncash))
                                leave.MinEncashReserveDays = Convert.ToInt32(maxEncash.Trim());
                            else
                            {
                                throw new InvalidDataException("Min.Encash Reserve Days empty");
                            }
                        }

                    }

                    if (!String.IsNullOrEmpty(closingDate))
                    {
                        leave.ClosingDate = Convert.ToDateTime(closingDate);
                    }
                    leave.IsPublic = isPub.Equals("1");
                    leave.PayType = (int?)(payType.Equals("1") ? PayType.WithPay : PayType.WithoutPay);
                    if (repeatType.Equals("1"))
                    {
                        leave.RepeatType = (int?)RepeatType.EveryYear;
                    }
                    else if (repeatType.Equals("2"))
                    {
                        leave.RepeatType = (int?)RepeatType.Once;
                    }
                    else if (repeatType.Equals("3"))
                    {
                        leave.RepeatType = (int?)RepeatType.MaxTwo;
                    }
                    int numOfdDays;
                    bool res = int.TryParse(noOfDays, out numOfdDays);
                    if (res)
                    {
                        leave.NoOfDays = Convert.ToInt32(noOfDays);
                    }
                    if (startFrom.Equals("1"))
                    {
                        leave.StartFrom = (int?)StartingPoint.Joining;
                    }
                    else if (startFrom.Equals("2"))
                    {
                        leave.StartFrom = (int?)StartingPoint.Permanent;
                    }
                    else if (startFrom.Equals("3"))
                    {
                        leave.StartFrom = (int?)StartingPoint.One_Year_After_Permanent;
                    }
                }
                _hrLeaveDao.Save(leave);
                
            }
            catch (Exception e)
            {
                throw e;
                //return e.ToString();
            }
        }

        public bool Delete(long id)
        {
            ITransaction trans = null;
            try
            {
                var dbLeave = _hrLeaveDao.LoadById(id);
                DoBeforeDelete(dbLeave);
                using (trans = Session.BeginTransaction())
                {
                    dbLeave.Status = Leave.EntityStatus.Delete;
                    _hrLeaveDao.Update(dbLeave);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool Update(long id, long org, string leaveName, string isPub, string payType, string repeatType,
            string noOfDays, string startFrom, string effectiveDate, string[] genAry, string[] empStatus,
            string[] maritalStatus, string isForward, string maxForward, string isTakingLimit, string maxTaking,
            string appType, string appTypeDays, string isEncash, string maxEncash, string closingDate = "")
        {
            ITransaction trans = null;
            try
            {
                if (id <= 0) throw new NullObjectException();
                var leave = _hrLeaveDao.LoadById(id);
                if(leave==null)
                    throw new InvalidDataException("Invalid Leave Setting");

                var prevLeave = new Leave();
                CopyHRLeavePropertyValues(leave, prevLeave);
                bool isEffectiveDatePass = false;
                using (trans = Session.BeginTransaction())
                {
                    if (org > 0)
                    {
                        var organization = _organizationDao.LoadById(Convert.ToInt64(org));
                        if (organization == null)
                            throw new InvalidDataException("Invalid organization");
                        
                        leave.Organization = organization;
                        if (!String.IsNullOrEmpty(leaveName))
                        {
                            leave.Name = leaveName.Trim();
                        }
                        else throw new InvalidDataException("Leave name can't empty");

                        var prevIsPublic = leave.IsPublic;
                        leave.IsPublic = isPub.Equals("1");

                        #region if isPublic is false

                        if (prevIsPublic != Convert.ToBoolean(Convert.ToInt32(isPub)) && !isPub.Equals("1"))
                        {
                            if (prevLeave.EffectiveDate != null &&
                                DateTime.Now.Date > prevLeave.EffectiveDate.Value.Date)
                            {
                                _hrLeaveDao.Update(leave);
                                trans.Commit();
                                isEffectiveDatePass = true;
                                throw new MessageException(
                                    "Leave's effective date already passed. so you can only update Leave name");
                            }
                        }
                        if (leave.PayType.ToString().Trim() != payType.Trim() && !payType.Equals("1"))
                        {
                            if (prevLeave.EffectiveDate != null &&
                                DateTime.Now.Date > prevLeave.EffectiveDate.Value.Date)
                            {
                                _hrLeaveDao.Update(leave);
                                trans.Commit();
                                throw new MessageException(
                                    "Leave's effective date already passed. so you can only update Leave name");
                            }
                        }
                        if (!isPub.Equals("1"))
                        {
                            leave.EffectiveDate = null;
                            leave.IsMale = null;
                            leave.IsFemale = null;
                            leave.IsContractual = null;
                            leave.IsIntern = null;
                            leave.IsPartTime = null;
                            leave.IsPermanent = null;
                            leave.IsProbation = null;
                            leave.IsSingle = null;
                            leave.IsMarried = null;
                            leave.IsWidow = null;
                            leave.IsWidower = null;
                            leave.IsDevorced = null;
                            leave.IsCarryforward = null;
                            leave.MaxCarryDays = null;
                            leave.IsTakingLimit = null;
                            leave.MaxTakingLimit = null;
                            leave.Applicationtype = null;
                            leave.ApplicationDays = null;
                            leave.IsEncash = null;
                            leave.MinEncashReserveDays = null;
                            leave.ClosingDate = null;
                            leave.PayType = null;
                            leave.RepeatType = null;
                            leave.NoOfDays = null;
                            leave.StartFrom = null;
                            _hrLeaveDao.Update(leave);
                            trans.Commit();
                            return true;
                        }

                        #endregion

                        #region if pay type is without pay

                        //if (leave.PayType.ToString().Trim() != payType.Trim() && !payType.Equals("1"))
                        //{
                        //    if (prevLeave.EffectiveDate != null && DateTime.Now.Date > prevLeave.EffectiveDate.Value.Date)
                        //    {
                        //        throw new MessageException("Leave's effective date already passed.");
                        //    }
                        //}
                        if (!payType.Equals("1"))
                        {

                            leave.PayType = payType.Equals("1") ? 1 : 0;
                            leave.IsMale = null;
                            leave.IsFemale = null;
                            if (!String.IsNullOrEmpty(appType))
                            {
                                leave.Applicationtype = Convert.ToInt32(appType);
                                if (!String.IsNullOrEmpty(appTypeDays))
                                    leave.ApplicationDays = Convert.ToInt32(appTypeDays.Trim());
                                else
                                {
                                    leave.ApplicationDays = 0;
                                }
                            }

                            leave.EffectiveDate = null;
                            leave.IsContractual = null;
                            leave.IsIntern = null;
                            leave.IsPartTime = null;
                            leave.IsPermanent = null;
                            leave.IsProbation = null;
                            leave.IsSingle = null;
                            leave.IsMarried = null;
                            leave.IsWidow = null;
                            leave.IsWidower = null;
                            leave.IsDevorced = null;
                            leave.IsCarryforward = null;
                            leave.MaxCarryDays = null;
                            leave.IsTakingLimit = null;
                            leave.MaxTakingLimit = null;
                            leave.IsEncash = null;
                            leave.MinEncashReserveDays = null;
                            leave.ClosingDate = null;
                            leave.RepeatType = null;
                            leave.NoOfDays = null;
                            leave.StartFrom = null;
                            _hrLeaveDao.Update(leave);
                            trans.Commit();
                            return true;
                        }

                        #endregion

                        // #region if isPublic is true and pay type is with pay

                        leave.PayType = payType.Equals("1") ? 1 : 0;
                        if (!String.IsNullOrEmpty(effectiveDate))
                        {
                            leave.EffectiveDate = Convert.ToDateTime(effectiveDate.Trim());
                        }
                        else throw new InvalidDataException("effective date can't empty");

                        if (genAry.ToList().Count > 0)
                        {
                            if (genAry.Contains(Gender.Male.ToString()))
                            {
                                leave.IsMale = true;
                            }
                            else
                            {
                                leave.IsMale = false;
                            }
                            if (genAry.Contains(Gender.Female.ToString()))
                            {
                                leave.IsFemale = true;
                            }
                            else
                            {
                                leave.IsFemale = false;
                            }
                        }
                        else throw new InvalidDataException("Invalid gender");
                        if (empStatus.ToList().Count > 0)
                        {

                            if (empStatus.Contains(MemberEmploymentStatus.Probation.ToString()))
                            {
                                leave.IsProbation = true;
                            }
                            else
                            {
                                leave.IsProbation = false;
                            }
                            if (empStatus.Contains(MemberEmploymentStatus.Permanent.ToString()))
                            {
                                leave.IsPermanent = true;
                            }
                            else
                            {
                                leave.IsPermanent = false;
                            }
                            if (empStatus.Contains(MemberEmploymentStatus.PartTime.ToString()))
                            {
                                leave.IsPartTime = true;
                            }
                            else
                            {
                                leave.IsPartTime = false;
                            }
                            if (empStatus.Contains(MemberEmploymentStatus.Contractual.ToString()))
                            {
                                leave.IsContractual = true;
                            }
                            else
                            {
                                leave.IsContractual = false;
                            }
                            if (empStatus.Contains(MemberEmploymentStatus.Intern.ToString()))
                            {
                                leave.IsIntern = true;
                            }
                            else
                            {
                                leave.IsIntern = false;
                            }
                        }
                        else throw new InvalidDataException("Invalid employment status");

                        if (maritalStatus.ToList().Count > 0)
                        {
                            if (maritalStatus.Contains(MaritalType.Single.ToString()))
                            {
                                leave.IsSingle = true;
                            }
                            else
                            {
                                leave.IsSingle = false;
                            }
                            if (maritalStatus.Contains(MaritalType.Married.ToString()))
                            {
                                leave.IsMarried = true;
                            }
                            else
                            {
                                leave.IsMarried = false;
                            }
                            if (maritalStatus.Contains(MaritalType.Widow.ToString()))
                            {
                                leave.IsWidow = true;
                            }
                            else
                            {
                                leave.IsWidow = false;
                            }
                            if (maritalStatus.Contains(MaritalType.Widower.ToString()))
                            {
                                leave.IsWidower = true;
                            }
                            else
                            {
                                leave.IsWidower = false;
                            }
                            if (maritalStatus.Contains(MaritalType.Divorced.ToString()))
                            {
                                leave.IsDevorced = true;
                            }
                            else
                            {
                                leave.IsDevorced = false;
                            }
                        }
                        else throw new InvalidDataException("Invalid marital status");

                        if (!String.IsNullOrEmpty(isForward))
                        {
                            leave.IsCarryforward = isForward.Equals("1");
                            if (!String.IsNullOrEmpty(maxForward))
                                leave.MaxCarryDays = Convert.ToInt32(maxForward.Trim());
                            else
                            {
                                leave.MaxCarryDays = 0;
                            }
                        }
                        else throw new InvalidDataException("Carry forward can't empty");

                        if (!String.IsNullOrEmpty(isTakingLimit))
                        {
                            leave.IsTakingLimit = isTakingLimit.Equals("1");
                            if (!String.IsNullOrEmpty(maxTaking))
                                leave.MaxTakingLimit = Convert.ToInt32(maxTaking.Trim());
                            else
                            {
                                leave.MaxTakingLimit = 0;
                            }
                        }
                        else throw new InvalidDataException("Taking limit can't empty");

                        if (!String.IsNullOrEmpty(appType))
                        {
                            leave.Applicationtype = Convert.ToInt32(appType);
                            if (!String.IsNullOrEmpty(appTypeDays))
                                leave.ApplicationDays = Convert.ToInt32(appTypeDays.Trim());
                            else
                            {
                                leave.ApplicationDays = 0;
                            }
                        }
                        else throw new InvalidDataException("Application type can't empty");

                        if (!String.IsNullOrEmpty(isEncash))
                        {
                            leave.IsEncash = isEncash.Equals("1");
                            if (!String.IsNullOrEmpty(maxEncash))
                                leave.MinEncashReserveDays = Convert.ToInt32(maxEncash.Trim());
                            else
                            {
                                leave.MinEncashReserveDays = 0;
                            }
                        }
                        else throw new InvalidDataException("Encash ability can't ");

                        if (!String.IsNullOrEmpty(closingDate))
                        {
                            leave.ClosingDate = Convert.ToDateTime(closingDate);
                        }
                        
                        if(String.IsNullOrEmpty(repeatType))
                            throw new InvalidDataException("Invalid Repeat type");

                        else if (repeatType.Equals("1"))
                        {
                            leave.RepeatType = 1;
                        }
                        else if (repeatType.Equals("2"))
                        {
                            leave.RepeatType = 2;
                        }
                        else if (repeatType.Equals("3"))
                        {
                            leave.RepeatType = 3;
                        }
                        else throw new InvalidDataException("Invalid Repeat type");

                        int numOfdDays;
                        bool res = int.TryParse(noOfDays, out numOfdDays);
                        if (res)
                        {
                            leave.NoOfDays = Convert.ToInt32(noOfDays);
                        }
                        else
                        {
                            leave.NoOfDays = 0;
                        }
                        if(String.IsNullOrEmpty(startFrom))
                            throw new InvalidDataException("Start from can't empty");

                        else if (startFrom.Equals("1"))
                        {
                            leave.StartFrom = 1;
                        }
                        else if (startFrom.Equals("2"))
                        {
                            leave.StartFrom = 2;
                        }
                        else if (startFrom.Equals("3"))
                        {
                            leave.StartFrom = 3;
                        }
                        else throw new InvalidDataException("Invalid start from");
                    }
                    DoBeforeEdit(leave, prevLeave);
                    _hrLeaveDao.Update(leave);
                    trans.Commit();
                    return true;

                    //#endregion
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        
        #endregion

        #region Single Instances Loading Function
        
        public Leave GetLeave(long id)
        {
            try
            {
                return _hrLeaveDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Leave GetById(long hRLeaveId)
        {
            try
            {
                return _hrLeaveDao.LoadById(hRLeaveId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        #endregion

        #region List Loading Function

        public IList<Leave> LoadLeave(List<long> organizationIdList = null)
        {
            try
            {
                return _hrLeaveDao.LoadLeave(organizationIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Leave> LoadOrganizationLeaveByDate(List<UserMenu> userMenu, long organizationId, DateTime searchDate)
        {
            try
            {
                long? convertedOrganizationId = organizationId == 0 ? (long?)null : Convert.ToInt32(organizationId);

                if (userMenu == null || !userMenu.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);

                if (organizationIdList == null || !organizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                return _hrLeaveDao.LoadOrganizationLeaveByDate(organizationIdList, searchDate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Leave> LoadLeave(List<UserMenu> userMenu, long organizationId, int draw = 0, int start = 0, int length = 0)
        {
            try
            {
                long? convertedOrganizationId = organizationId == 0 ? (long?)null : Convert.ToInt32(organizationId);

                if (userMenu == null || !userMenu.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);

                if (organizationIdList == null || !organizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                return _hrLeaveDao.LoadLeave(organizationIdList, draw, start, length);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Leave> LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(long orgId, int? gender, int employeStatus,
            int maritalStatus)
        {
            try
            {
                return _hrLeaveDao.LoadLeave(orgId, gender, employeStatus, maritalStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int GetLeaveCount(List<UserMenu> userMenu, long organizationId)
        {
            try
            {
                if (userMenu == null || !userMenu.Any())
                    throw new InvalidDataException("Invalid User menu");

                long? convertedOrganizationId = organizationId == 0 ? (long?) null : Convert.ToInt32(organizationId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu,
                    (convertedOrganizationId != null)
                        ? _commonHelper.ConvertIdToList((long) convertedOrganizationId)
                        : null);

                if (organizationIdList == null || !organizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                return _hrLeaveDao.GetLeaveCount(organizationIdList);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public static string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            return (propertyExpression.Body as MemberExpression).Member.Name;
        }

        public static void CopyHRLeavePropertyValues(object source, object destination)
        {
            try
            {
                var destProperties = destination.GetType().GetProperties();
                foreach (var sourceProperty in source.GetType().GetProperties())
                {
                    foreach (var destProperty in destProperties)
                    {
                        if (destProperty.Name == sourceProperty.Name &&
                    destProperty.PropertyType.IsAssignableFrom(sourceProperty.PropertyType))
                        {
                            destProperty.SetValue(destination, sourceProperty.GetValue(
                              source, new object[] { }), new object[] { });
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {

                throw;
            }

        }

        #endregion

        #region Helper function

        private void DoBeforeDelete(Leave hrLeave)
        {
            if (hrLeave == null)
                throw new ValueNotAssignedException("Leave is not found.");
            bool dependencyFound = _hrLeaveDao.HasDependency(hrLeave);
            if (dependencyFound)
                throw new DependencyException("Leave is already taken.");
            if (hrLeave.EffectiveDate != null && DateTime.Now.Date > hrLeave.EffectiveDate.Value.Date)
                throw new DependencyException("Leave's effective date already passed.");
        }

        private void DoBeforeEdit(Leave newLeave, Leave prevLeave)
        {

            try
            {
                var leaveNameProperty = GetPropertyName(() => newLeave.Name);
                var closingDateProperty = GetPropertyName(() => newLeave.ClosingDate);
                var differences = new List<PropertyInfo>();
                if (prevLeave.EffectiveDate != null && DateTime.Now.Date > prevLeave.EffectiveDate.Value.Date)
                {
                    foreach (PropertyInfo property in newLeave.GetType().GetProperties())
                    {
                        if (property.Name.Trim() != leaveNameProperty.Trim() && property.Name.Trim() != "ClosingDate" && property.Name.Trim() != "BusinessId"
                            && property.Name.Trim() != "CreationDate" && property.Name.Trim() != "ModificationDate" && property.Name.Trim() != "CreateBy" && property.Name.Trim() != "CreateByText"
                            && property.Name.Trim() != "ModifyBy" && property.Name.Trim() != "ModifyByText" && property.Name.Trim() != "VersionNumber")
                        {
                            object value1 = property.GetValue(newLeave, null);
                            object value2 = property.GetValue(prevLeave, null);
                            var typeJudge1 = value1.GetType().ToString();
                            if (typeJudge1 == "System.Boolean")
                            {
                                if (value2 == null)
                                {
                                    value2 = false;
                                }
                            }
                            if (typeJudge1 == "System.Int32")
                            {
                                if (value2 == null)
                                {
                                    value2 = 0;
                                }
                            }
                            if (!value1.Equals(value2))
                            {
                                differences.Add(property);
                            }
                        }
                    }
                }
                if (differences.Count > 0)
                {
                    throw new MessageException("Leave's effective date already passed.");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

    }
}
