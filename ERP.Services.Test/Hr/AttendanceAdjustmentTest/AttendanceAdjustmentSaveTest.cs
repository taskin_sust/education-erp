﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceAdjustmentTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceAdjustment")]
    public class AttendanceAdjustmentSaveTest : AttendanceAdjustmentBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_AttendanceAdjustment_Successfully()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            _attendanceAdjustmentFactory.CreateMore(startDate, tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);

            Assert.True(_attendanceAdjustmentFactory.ObjectList.Any());
        }

        [Fact]
        public void Should_Return_AttendanceAdjustment_InvalidException_For_Empty_Adjustment()
        {
            Assert.Throws<InvalidDataException>(
                () =>
                    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { }));

        }
        
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_AttendanceAdjustment_InvalidException_For_Reason()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016,12,5),
                new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 12, 5, 11, 10, 10), "",(int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm);
            Assert.Throws<InvalidDataException>(
                () =>
                    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
                    {
                        _attendanceAdjustmentFactory.Object
                    }));
        }

        [Fact]
        public void Should_Return_AttendanceAdjustment_InvalidException_For_StartTimeIsGreaterThanEndTime()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016, 12, 5),new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 12, 5, 8, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0)
                .WithTeamMember(tm);
            Assert.Throws<InvalidDataException>(
                () =>
                    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
                    {
                        _attendanceAdjustmentFactory.Object
                    }));
        }

        [Fact]
        public void Should_Return_AttendanceAdjustment_Duplicate_Exception()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            _attendanceAdjustmentFactory.CreateWith("Test", DateTime.Now.AddDays(-1),
                new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 10, 10, 10),
                new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
            {
                _attendanceAdjustmentFactory.Object
            });

            var att = _attendanceAdjustmentFactory.CreateWith("Test", DateTime.Now.AddDays(-1),
                new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 10, 10, 10),
                new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm).Object;

            Assert.Throws<DuplicateEntryException>(() => _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { att }));
        }
        
        #endregion
        
    }
}
