﻿$(document).ready(function () {

    //organization change functionalities
    $("#organizationId").change(function () {
        
        var organizatonId = $(this).val();
        $("#ProgramId option").remove();
        $("#SelectedCourse option").remove();
        $("#SelectedBranch option").remove();
        $("#SelectedCampus option").remove();
        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();
        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select Session</option>");
        //$('#SelectedCourse').append("<option value=''>Select Course</option>");
        //$('#SelectedBranch').append("<option value=''>Select Branch</option>");
        //$('#SelectedCampus').append("<option value=''>Select Campus</option>");
        //$('#SelectedBatchDays').append("<option value=''>Select Batch Days</option>");
        //$('#SelectedBatchTime').append("<option value=''>Select Batch Time</option>");
        //$('#SelectedBatch').append("<option value=''>Select Batch</option>");
        if (organizatonId != "") {
            $.ajax({
                //url: '/Student/Attendance/GetProgramTeacherByOrganization',
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                type: "POST",
                //data: { "organizatonIds": organizatonId },
                data: { organizationIds: organizatonId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    if (response.IsSuccess) {
                        //load programs
                        $('#ProgramId').append('<option value="">Select Program</option>');
                        if (response.returnProgramList != null && response.returnProgramList.length > 0) {
                            $.each(response.returnProgramList, function (i, v) {
                                $('#ProgramId').append($('<option>').text(v.Text).attr('value', v.Value));
                            });
                            $("#ProgramId").find('option:eq(0)').prop('selected', true);
                        }
                       
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error:
                    function (response) {
                        // alert("Session load failed");
                        var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a> Program and Branch load failed</div>';
                        $('.customMessage').append(errorMessage);
                        }
            });

        } else {

            $('#ProgramId').append('<option value="">Select Program</option>');
        }
    });
    //Program Change Functionalities
    $(document).on('change', '#ProgramId', function () {

        $("#missingImageCount").html("...");

        //remove all dependant dropdownlist & selection option
        $("#SelectedCourse option").remove();
        $("#SelectedBranch option").remove();
        $("#SelectedCampus option").remove();

        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
         $("#SelectedBatch option").remove();

        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select session</option>");

        var organizatonId = $("#organizationId").val();
        var programId = $("#ProgramId").val();

        if (organizatonId != null && organizatonId != "" && programId != null && programId != "") {
            $.ajax({
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",

                //url: "/Student/Attendance/AjaxRequestForSession",
                cache: false,
                async: true,
                //data: { programId: programId },
                data: { organizationIds: organizatonId, programIds: programId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    //$.unblockUI();
                    //$("#SessionId").html(result);
                    ////console.log("Success -----");
                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$('#SessionId').append('<option value="">Select Session</option>');
                        //$.each(response.sessionList, function (i, v) {
                        //    $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        $.each(response.returnSessionList, function (i, v) {
                            $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        //// $("#SessionId").find('option:eq(0)').prop('selected', true);
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                }, complete: function (response) {
                    $.unblockUI();
                    //console.log("Complete -----");
                    //console.log(result);
                },
                error: function (response) {
                    $.unblockUI();
                    //console.log("Failed -----");
                    //console.log(result);
                }
            });
        }
    });


    //Session Change Functionalities
    $(document).on('change', '#SessionId', function () {

        $("#missingImageCount").html("...");

        //remove all dependant dropdownlist & selection option
        $("#SelectedCourse option").remove();
        $("#SelectedBranch option").remove();
        $("#SelectedCampus option").remove();

        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
         $("#SelectedBatch option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var organizationId = $("#organizationId").val();
        if (organizationId != null && organizationId != "" && organizationId != 0 && programId != null && programId != "" && programId != 0 && sessionId != null && sessionId != "" && sessionId != 0) {
             
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForBranch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                cache: false,
                async: true,
                //data: { programId: programId, sessionId: sessionId, organizationId: organizationId },
                data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    if (response.IsSuccess) {
                        //$.each(result.branchList, function (i, v) {
                        //    $('#SelectedBranch').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        $('#SelectedBranch').append('<option value="0">All Branch</option>');
                        $.each(response.returnBranchList, function (i, v) {
                            $('#SelectedBranch').append($('<option>').text(v.Text).attr('value', v.Value));
                        });

                        ///// $("#SelectedBranch").find('option:eq(0)').prop('selected', true);
                        //console.log("Success -----");

                        $.ajax({
                            //url: 'GetCourseList',
                            //url: '/Student/Attendance/GetCourseList',
                            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
                            type: "POST",
                            //data: { programId: programId, sessionId: sessionId },
                            data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (responseForCourse) {
                                $.unblockUI();
                                if (responseForCourse.IsSuccess) {
                                    //$.each(result.courseList, function (i, v) {
                                    //    $('#SelectedCourse').append($('<option>').text(v.Text).attr('value', v.Value));
                                    //});
                                    //$("#SelectedCourse").find('option:eq(0)').prop('selected', true);
                                    //console.log("Success ------");
                                    $('#SelectedCourse').append('<option value="0">All Course</option>');
                                    $.each(responseForCourse.returnCourse, function (i, v) {
                                        $('#SelectedCourse').append($('<option>').text(v.Text).attr('value', v.Value));
                                    });

                                } else {
                                    console.log("Error -----");
                                    //console.log(responseForCourse);
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                                console.log("Complete -----");
                                //console.log(result);
                            },
                            error: function () {
                                $.unblockUI();
                                console.log("Failed -----");
                                //console.log(result);
                            }
                        });
                    } else {
                        $.unblockUI();
                        console.log("Error -----");
                        //console.log(result);
                    }

                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    //console.log(result);
                },
                error: function () {
                    $.unblockUI();
                    console.log("Failed -----");
                    //console.log(result);
                }
            });
        }
    });

    //Branch Change Functionalities
    $(document).on('change', '#SelectedBranch', function () {

        $("#missingImageCount").html("...");

        //remove all dependant dropdownlist & selection option 
        $("#SelectedCampus option").remove();

        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
         $("#SelectedBatch option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();
        var organizationId = $("#organizationId").val();

        if (organizationId != null && organizationId != "" && programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "") {
            $.ajax({
                type: "POST",
                //url: "/Student/Attendance/AjaxRequestForCampus",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                cache: false,
                async: true,
                //data: { programId: programId, sessionId: sessionId, branchId: branchId },
                data: { organizationIds: organizationId, programIds: programId, branchIds: branchId, sessionIds: sessionId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$.each(result.campusList, function (i, v) {
                        //    $('#SelectedCampus').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        ///// $("#SelectedCampus").find('option:eq(0)').prop('selected', true); 
                        //console.log("Success ------");
                        $('#SelectedCampus').append('<option value="0">All Campus</option>');
                        $.each(response.returnCampusList, function (i, v) {
                            $('#SelectedCampus').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        console.log("Error -----");
                        //console.log(result);
                    }

                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    //console.log(result);
                },
                error: function () {
                    $.unblockUI();
                    console.log("Failed -----");
                    //console.log(result);
                }
            });
        }
    });


    //Campus Change Functionalities
    $(document).on('change', '#SelectedCampus', function () {

        $("#missingImageCount").html("...");

        //remove all dependant dropdownlist & selection option  
        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
         $("#SelectedBatch option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var organizationId = $("#organizationId").val();

        if (organizationId != null && organizationId != "" && programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "" && campusId != null && campusId != "") {
            $.ajax({
                type: "POST",
                //url: "/Student/Attendance/AjaxRequestForBatchDay",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchDay",
                cache: false,
                async: true,
                //data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId },
                data: { organizationIds: organizationId, programIds: programId, branchIds: branchId, sessionIds: sessionId, campusIds: campusId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$.each(result.batchDays, function (i, v) {
                        //    if (v.Value == "0") {
                        //        $('#SelectedBatchDays').append($('<option>').text(v.Text).attr('value', v.Value));
                        //    } else {
                        //        $('#SelectedBatchDays').append($('<option>').text(v.Text).attr('value', v.Text));
                        //    }
                            
                        //});
                        ///// $("#SelectedBatchDays").find('option:eq(0)').prop('selected', true);
                        //console.log("Success ------");
                        $('#SelectedBatchDays').append('<option value="0">All Batch Day</option>');
                        $.each(response.returnBatchDays, function (i, v) {
                            if (v.Value == 0)
                                $('#SelectedBatchDays').append($('<option>').text(v.Text).attr('value', v.Value));
                            else
                                $('#SelectedBatchDays').append($('<option>').text(v.Text).attr('value', v.Text));
                        });
                    } else {
                        console.log("Error -----");
                        //console.log(result);
                    }

                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    //console.log(result);
                },
                error: function () {
                    $.unblockUI();
                    console.log("Failed -----");
                    //console.log(result);
                }
            });
        }
    });


    //Batch Day Change Functionalities
    $(document).on('change', '#SelectedBatchDays', function () {

        $("#missingImageCount").html("...");

        //remove all dependant dropdownlist & selection option  
        $("#SelectedBatchTime option").remove();
         $("#SelectedBatch option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchDays = $("#SelectedBatchDays").val();
        var organizationId = $("#organizationId").val();


        if (organizationId != null && organizationId != "" && programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "" && campusId != null && campusId != "" && batchDays != null && batchDays != "") {
            $.ajax({
                type: "POST",
                //url: "/Student/Attendance/AjaxRequestForBatchTime",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchTime",
                cache: false,
                async: true,
                //data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId, batchDays: batchDays },
                data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, batchDays: batchDays, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$.each(result.batchTime, function (i, v) {
                        //    $('#SelectedBatchTime').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        ///// $("#SelectedBatchTime").find('option:eq(0)').prop('selected', true);
                        //console.log("Success ------");
                        $('#SelectedBatchTime').append('<option value="0">All Batch Time</option>');
                        $.each(response.returnBatchTime, function (i, v) {
                            $('#SelectedBatchTime').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        console.log("Error -----");
                        //console.log(result);
                    }
                },
                complete: function () {
                    $.unblockUI();
                    //console.log("Complete -----");
                    //console.log(result);
                },
                error: function () {
                    $.unblockUI();
                    //console.log("Failed -----");
                    //console.log(result);
                }
            });
        }
    });


    //Batch Time Change Functionalities
    $(document).on('change', '#SelectedBatchTime', function () {

        $("#missingImageCount").html("...");

        //remove all dependant dropdownlist & selection option  
         $("#SelectedBatch option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchDays = $("#SelectedBatchDays").val();
        var batchTime = $("#SelectedBatchTime").val();
        var organizationId = $("#organizationId").val();
         
        var batchTimeText = [];
        $("#SelectedBatchTime option:selected").each(function () {
            var $this = $(this);
            if ($this.length) {
                batchTimeText.push($this.text());
            }
        });

        if (organizationId != null && organizationId != "" && programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "" && campusId != null && campusId != "" && batchDays != null && batchDays != "" && batchTime != null && batchTime != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForBatch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatch",
                cache: false,
                async: true,
                //data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId, batchDays: batchDays, batchTime: batchTime, batchTimeText: batchTimeText },
                data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId, campusIds: campusId, branchIds: branchId, batchDays: batchDays, batchTimes: batchTime, batchTimeText: batchTimeText, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        //$.each(result.batchName, function (i, v) {
                        //    $('#SelectedBatch').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        ///// $("#batchName").find('option:eq(0)').prop('selected', true);
                        //console.log("Success ------");
                        $('#SelectedBatch').append('<option value="0">All Batch</option>');
                        $.each(result.returnBatch, function (i, v) {
                            $('#SelectedBatch').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        console.log("Error -----");
                        //console.log(result);
                    }
                },
                complete: function () {
                    $.unblockUI();
                    //console.log("Complete -----");
                    //console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    //console.log(result);
                }
            });
        }
    });
    $(document).on('change', '#EndDate', function () {
        //$("#SelectedExam option").remove();
        $('#countBtn').attr('disabled', 'disabled');
        $('#countValue').val('0');
        $("#SelectedLecture option").remove();
        $("#SelectedTeacher option").remove();
        var orgId = $("#organizationId").val();
        var batchDays = $("#SelectedBatchDays").val();
        var batchTime = $("#SelectedBatchTime").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var courseId = $("#SelectedCourse").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchId = $("#SelectedBatch").val();
        //var subjectId = $("#SelectedSubject").val();
        var dateFrom = $("#StartDate").val();
        var dateTo = $("#EndDate").val();
        var lectureIds = [];
        if (programId.length > 0 && sessionId.length > 0 & branchId.length > 0 && campusId.length > 0 && batchId.length > 0  && dateFrom.length > 0 && dateTo.length > 0) {

            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Attendance/LoadLectureForClassAttendanceReport",
                cache: false,
                async: true,
                //(userMenu,courseIdList,subjectIdList, batchIdList,dateFrom,  dateTo,  organizationIdList , programIdList,  branchIdList, sessionIdList, campusIdList ,  batchDaysList,  batchTimeList)
                data: { courseIdList: courseId, batchIdList: batchId, dateFrom: dateFrom, dateTo: dateTo, organizationIdList: orgId, programIdList: programId, branchIdList: branchId, sessionIdList: sessionId, campusIdList: campusId, batchDaysList: batchDays, batchTimeList: batchTime },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.lectureList.length > 0) {
                        $('#SelectedLecture').append("<option value='0'>ALL LECTURE</option>");
                    }
                    if (result.IsSuccess) {
                        console.log('xxyyzz:'+result.lectureList);
                        $.each(result.lectureList, function (i, v) {
                            $('#SelectedLecture').append($('<option>').text(v.Text).attr('value', v.Value));
                            lectureIds.push(v.Value);
                        });
                        $("#lectureIds").val(lectureIds.join());
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");

                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");

                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");

                }
            });
        }
    });

    $('#informationViewList').bootstrapDualListbox({
        nonselectedlistlabel: 'Non-selected',
        selectedlistlabel: 'Selected',
        preserveselectiononmove: 'moved',
        moveOnSelect: false,
        showFilterInputs: false,
        infoText: '',
    });
});