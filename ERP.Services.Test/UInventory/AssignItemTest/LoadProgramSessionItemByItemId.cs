﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.AssignItemTest
{
    interface ILoadProgramSessionItemByItemId
    {
        void Should_Return_Load_item();
    }

    [Collection("Collection #2")]
    [Trait("Area", "UInventory")]
    [Trait("Service", "ProgramSessionItem")]
    public class LoadProgramSessionItemByItemId:ProgramSessionItemBaseTest,ILoadProgramSessionItemByItemId
    {
        [Fact]
        public void Should_Return_Load_item()
        {
            organizationFactory.Create().Persist();
            _ProgramSessionItemFactory.Create().WithItem(organizationFactory.Object).WithProgramAndSession(null,null).Persist();
            Assert.NotNull(_ProgramSessionItemFactory.Object.Item);
            Assert.NotEmpty(_ProgramSessionItemService.LoadProgramSessionItemByItemId(_ProgramSessionItemFactory.Object.Item.Id));
        }
    }
}
