﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.RequisitionTest
{
    public class RequisitionBaseTest : TestBase, IDisposable
    {
        #region Obj initialization

        internal int RequiredQuantity = 100;
        internal readonly CommonHelper CommonHelper;
        private ISession _session;

        internal RequisitionFactory _requisitionFactory;
        internal RequisitionDetailsFactory _requisitionDetailsFactory;
        internal GoodsIssueFactory _goodsIssueFactory;
        internal GoodsIssueDetailsFactory _goodsIssueDetailsFactory;
        internal GoodsReceiveFactory _goodsReceiveFactory;
        internal GoodsReceiveDetailsFactory _goodsReceiveDetailsFactory;

        internal BranchFactory _branchFactory;
        internal ItemFactory _ItemFactory;
        internal ProgramFactory _programFactory;
        internal SessionFactory _sessionFactory;
        internal ProgramSessionItemFactory _programSessionItemFactory;

        internal CurrentStockSummaryService _currentStockSummaryService;

        internal IRequisitionService _requisitionService;
        internal IGoodsIssueService _goodsIssueService;
        internal IGoodsReceiveService _goodsReceiveService;
        internal IProgramSessionItemService _programSessionItemService;


        #endregion

        public RequisitionBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _requisitionFactory = new RequisitionFactory(null, _session);
            _requisitionDetailsFactory = new RequisitionDetailsFactory(null, _session);

            _goodsIssueFactory = new GoodsIssueFactory(null, _session);
            _goodsIssueDetailsFactory = new GoodsIssueDetailsFactory(null, _session);
            _goodsReceiveFactory = new GoodsReceiveFactory(null, _session);
            _goodsReceiveDetailsFactory = new GoodsReceiveDetailsFactory(null, _session);

            _branchFactory = new BranchFactory(null, _session);
            _ItemFactory = new ItemFactory(null, _session);
            _programFactory = new ProgramFactory(null, _session);
            _sessionFactory = new SessionFactory(null, _session);
            _programSessionItemFactory = new ProgramSessionItemFactory(null, _session);

            _requisitionService = new RequisitionService(_session);
            _goodsIssueService = new GoodsIssueService(_session);
            _goodsReceiveService = new GoodsReceiveService(_session);
            _programSessionItemService = new ProgramSessionItemService(_session);
            CommonHelper = new CommonHelper();
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }
        public void Dispose()
        {
            CleanUpStockSummary(_ItemFactory.Object);
            _programSessionItemFactory.CleanUp();
            _goodsIssueDetailsFactory.CleanUp();
            _goodsIssueFactory.CleanUp();
            _goodsReceiveDetailsFactory.CleanUp();
            _goodsReceiveFactory.CleanUp();
            _requisitionDetailsFactory.CleanUp();
            _requisitionFactory.CleanUp();
            _ItemFactory.CleanUp();
            _branchFactory.Cleanup();
            _programFactory.Cleanup();
            _sessionFactory.Cleanup();
            base.Dispose();

        }

        private void CleanUpStockSummary(Item item)
        {
            if (item != null)
            {
                var sql = String.Format("DELETE FROM {0} WHERE "
                                            + "ItemId = {1} "
                                            , NHibernateSessionFactory.GetTableName(typeof(CurrentStockSummary))
                                            , item.Id
                            );
                var query = _session.CreateSQLQuery(sql);
                query.ExecuteUpdate();
            }
        }
    }
}
