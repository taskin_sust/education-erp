using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.Hr
{
    public interface IExtradayAllowanceSettingDao : IBaseDao<ExtradayAllowanceSetting, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<ExtradayAllowanceSetting> LoadExtradayAllowanceSetting(int start, int length, string orderBy, string orderDir, List<long> organizationId
                                                        , List<AllowanceSettingHolidayType> holidayTypes, List<MemberEmploymentStatus> employmentTypes);

        #endregion

        #region Others Function

        int GetExtradayAllowanceSettingsCount(List<long> organizationList, List<AllowanceSettingHolidayType> holidayTypes, List<MemberEmploymentStatus> employmentTypes);

        #endregion


        bool CheckForDuplicate(ExtradayAllowanceSetting entity);
    }

    public class ExtradayAllowanceSettingDao : BaseDao<ExtradayAllowanceSetting, long>, IExtradayAllowanceSettingDao
    {
        #region Properties & Object & Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<ExtradayAllowanceSetting> LoadExtradayAllowanceSetting(int start, int length, string orderBy, string orderDir, List<long> organizationIds,
                                                                    List<AllowanceSettingHolidayType> holidayTypes, List<MemberEmploymentStatus> employmentTypes)
        {
            var criteria = GetExtradayAllowanceSettingCriteria(orderBy, orderDir, organizationIds, holidayTypes, employmentTypes);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<ExtradayAllowanceSetting>();
        }

        public int GetExtradayAllowanceSettingsCount(List<long> organizationIds, List<AllowanceSettingHolidayType> holidayTypes, List<MemberEmploymentStatus> employmentTypes)
        {
            var criteria = GetExtradayAllowanceSettingCriteria("", "", organizationIds, holidayTypes, employmentTypes);
            criteria.SetProjection(Projections.RowCount());
            int conut = Convert.ToInt32(criteria.UniqueResult());
            return conut;
        }
        
        #endregion

        #region Others Function

        public bool CheckForDuplicate(ExtradayAllowanceSetting entity)
        {
            ICriteria criteria = Session.CreateCriteria<ExtradayAllowanceSetting>().Add(Restrictions.Eq("Status", ExtradayAllowanceSetting.EntityStatus.Active));
            criteria.CreateAlias("Organization", "organization")
                .Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active))
                .Add(Restrictions.Eq("organization.Id", entity.Organization.Id));
            criteria.Add(Restrictions.Le("EffectiveDate", entity.EffectiveDate));

            var disjunction = Restrictions.Disjunction();
            disjunction.Add(Restrictions.IsNull("ClosingDate") || Restrictions.Ge("ClosingDate", entity.EffectiveDate));
            criteria.Add(disjunction);

            var employeeStatusDj = Restrictions.Disjunction();
            if (entity.IsPermanent)
                employeeStatusDj.Add(Restrictions.Eq("IsPermanent", entity.IsPermanent));
            if (entity.IsProbation)
                employeeStatusDj.Add(Restrictions.Eq("IsProbation", entity.IsProbation));
            if (entity.IsPartTime)
                employeeStatusDj.Add(Restrictions.Eq("IsPartTime", entity.IsPartTime));
            if (entity.IsContractual)
                employeeStatusDj.Add(Restrictions.Eq("IsContractual", entity.IsContractual));
            if (entity.IsIntern)
                employeeStatusDj.Add(Restrictions.Eq("IsIntern", entity.IsIntern));
            if (employeeStatusDj != null)
            {
                criteria.Add(employeeStatusDj);
            }

            var holidayTypesDj = Restrictions.Disjunction();
            if (entity.IsGazetted)
                holidayTypesDj.Add(Restrictions.Eq("IsGazetted", entity.IsGazetted));
            if (entity.IsManagement)
                holidayTypesDj.Add(Restrictions.Eq("IsManagement", entity.IsManagement));
            if (entity.IsWeekend)
                holidayTypesDj.Add(Restrictions.Eq("IsWeekend", entity.IsWeekend));
            if (holidayTypesDj != null)
            {
                criteria.Add(holidayTypesDj);
            }

            if (entity.Id > 0)
                criteria.Add(!Restrictions.Eq("Id", entity.Id));

            criteria.SetTimeout(3000);
            criteria.SetProjection(Projections.RowCount());
            int count = Convert.ToInt32(criteria.UniqueResult());
            return count > 0;
        }
        
        #endregion

        #region Helper Function

        private ICriteria GetExtradayAllowanceSettingCriteria(string orderBy, string orderDir, List<long> organizationIds
                                                                , IEnumerable<AllowanceSettingHolidayType> holidayTypes
                                                                , IEnumerable<MemberEmploymentStatus> employmentTypes, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<ExtradayAllowanceSetting>().Add(Restrictions.Not(Restrictions.Eq("Status", ExtradayAllowanceSetting.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Not(Restrictions.Eq("organization.Status", Organization.EntityStatus.Delete)));

            if (organizationIds.Count > 0)
            {
                criteria.Add(Restrictions.In("organization.Id", organizationIds));
            }
            foreach (var type in holidayTypes)
            {
                switch (type)
                {
                    case AllowanceSettingHolidayType.Gazetted:
                        criteria.Add(Restrictions.Eq("IsGazetted", true));
                        break;
                    case AllowanceSettingHolidayType.Management:
                        criteria.Add(Restrictions.Eq("IsManagement", true));
                        break;
                    case AllowanceSettingHolidayType.Weekend:
                        criteria.Add(Restrictions.Eq("IsWeekend", true));
                        break;
                }
            }
            foreach (var type in employmentTypes)
            {
                switch (type)
                {
                    case MemberEmploymentStatus.Contractual:
                        criteria.Add(Restrictions.Eq("IsContractual", true));
                        break;
                    case MemberEmploymentStatus.Intern:
                        criteria.Add(Restrictions.Eq("IsIntern", true));
                        break;
                    case MemberEmploymentStatus.PartTime:
                        criteria.Add(Restrictions.Eq("IsPartTime", true));
                        break;
                    case MemberEmploymentStatus.Permanent:
                        criteria.Add(Restrictions.Eq("IsPermanent", true));
                        break;
                    case MemberEmploymentStatus.Probation:
                        criteria.Add(Restrictions.Eq("IsProbation", true));
                        break;
                }
            }

            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }
        

        #endregion

    }
}
