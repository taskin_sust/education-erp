﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DesignationTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Designation")]
    public class DesignationSaveTest:DesignationBaseTest
    {
        #region Basic Test
        
        /*[Fact]
        public void Designation_Save_Should_Return_True_With_Authrizetion()
        {
            var objFactory = DesignationFactory.Create().WithOrganization().WithRank(TestRank);
            var userMenu = BuildUserMenu(_commonHelper.ConvertIdToList(objFactory.Object.Organization));
            bool isSuccess = DesignationService.DesignationSaveUpdate(userMenu, objFactory.Object);
            Assert.True(isSuccess);
        }*/
        
        [Fact]
        public void Designation_Save_Should_Return_True()
        {
            var objFactory = _designationFactory.Create().WithOrganization().WithRank(TestRank);
            bool isSuccess = _designationService.DesignationSaveUpdate(objFactory.Object);
            Assert.True(isSuccess);
        }
        [Fact]
        public void Designation_Save_Should_Return_Null()
        {
            Assert.Throws<NullObjectException>(() => _designationService.DesignationSaveUpdate(null));
        }
        #endregion

        #region Business Logic Test

        [Fact]
        public void Designation_Save_Should_Return_Organization_Null()
        {
            var objFactory = _designationFactory.Create();
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }

        [Fact]
        public void Validation_Error_For_Name_Null()
        {
            var objFactory = _designationFactory.Create().WithOrganization().WithNameAndRank(null, ShortName, TestRank);
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }

        [Fact]
        public void Validation_Error_For_Name_Empty()
        {
            var objFactory = _designationFactory.Create().WithOrganization().WithNameAndRank(string.Empty, ShortName, TestRank);
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }

        [Fact]
        public void Duplicate_Entry_Exception_For_Name()
        {
            var objFactory1 = _designationFactory.Create().WithOrganization().WithNameAndRank(TestName, ShortName, TestRank).Persist();
            var org = objFactory1.Object.Organization;
            var objFactory2 = _designationFactory.Create()
                .WithOrganization(org)
                .WithNameAndRank(TestName, UpdatingShortName, UpdatingTestRank);
            Assert.Throws<DuplicateEntryException>(() => _designationService.DesignationSaveUpdate(objFactory2.Object));
        }

        [Fact]
        public void Validation_Error_For_ShortName_Null()
        {
            var objFactory = _designationFactory.Create().WithOrganization().WithNameAndRank(TestName, null, TestRank);
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }

        [Fact]
        public void Validation_Error_For_ShortName_Empty()
        {
            var objFactory = _designationFactory.Create().WithOrganization().WithNameAndRank(TestName, "", TestRank);
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }

        [Fact]
        public void Designation_Save_Should_Return_Rank_Invalid()
        {
            var objFactory = _designationFactory.Create().WithOrganization().WithNameAndRank(TestName, ShortName, 0);
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }

        [Fact]
        public void Validation_Error_For_Rank_Null()
        {
            var objFactory = _designationFactory.Create().WithOrganization().WithNameAndRank(TestName, ShortName, null);
            Assert.Throws<InvalidDataException>(() => _designationService.DesignationSaveUpdate(objFactory.Object));
        }

        [Fact]
        public void Validation_Error_For_Rank_Duplicate()
        {
            var objFactory1 = _designationFactory.Create().WithOrganization().WithNameAndRank(TestName, ShortName, TestRank).Persist();
            var org = objFactory1.Object.Organization;
            var objFactory2 = _designationFactory.Create()
                .WithOrganization(org)
                .WithNameAndRank(UpdatingTestName, UpdatingShortName, TestRank);
            Assert.Throws<DuplicateEntryException>(() => _designationService.DesignationSaveUpdate(objFactory2.Object));
        }

        #endregion
    }
}
