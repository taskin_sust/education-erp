﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SurveyQuestionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "SurveyQuestion")]
   public class SurveyQuestionSaveTest : SurveyQuestionBaseTest
    {
       #region Basic test
       [Fact]
       public void Save_Should_Return_Null_Exception()
       {
           Assert.Throws<NullObjectException>(() => _surveyQuestionService.Save(null));
       }

       [Fact]
       public void Save_Should_Return_Model_inValid()
       {
           _surveyQuestionAnswerFactory.CreateMore(1, null);
           _surveyQuestionFactory.Create().WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList());
           _surveyQuestionFactory.Object.Question = "";
           Assert.Throws<MessageException>(() => _surveyQuestionFactory.Persist());
       }

       [Fact]
       public void Save_Should_Check_Empty_Answer() 
       {
           _surveyQuestionAnswerFactory.CreateMore(1, null);
           _surveyQuestionFactory.Create().WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList());
           Assert.Throws<EmptyFieldException>(() => _surveyQuestionFactory.Persist());
       }

       [Fact]
       public void Save_Should_Check_Duplicate() 
       {
           
           _surveyQuestionFactory.Create().WithQuestionAndRank(_question,10);
           _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
           _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList()).Persist();

           _surveyQuestionFactory.Create().WithQuestionAndRank(_question,10);
           _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
           _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList());

           Assert.Throws<DuplicateEntryException>(() => _surveyQuestionFactory.Persist());
       }

       [Fact]
       public void Save_Should_Save_Successfully()  
       {
           _surveyQuestionFactory.Create().WithQuestionAndRank(_question, 10);
           _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
           _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList()).Persist();
           Assert.NotEqual(_surveyQuestionFactory.Object.Id, 0);
           Assert.NotNull(_surveyQuestionFactory.Object.Id);
           var obj = _surveyQuestionService.GetSurveyQuestion(_surveyQuestionFactory.Object.Id);
           Assert.NotEmpty(obj.SurveyQuestionAnswers);
       }

       #endregion
    }
}
