using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class QuotationCriteriaFactory : ObjectFactoryBase<QuotationCriteria>
   {

       private readonly IQuotationCriteriaService _quotationCriteriaService;

       public QuotationCriteriaFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
       {

        _quotationCriteriaService = new QuotationCriteriaService(session);

       }

       #region Create 

       public QuotationCriteriaFactory Create()
       {

           Object = null;
           SingleObjectList.Add(Object);
           return this;
       }

       public QuotationCriteriaFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public QuotationCriteriaFactory Persist()       
       {
          return this;
       }

       public QuotationCriteriaFactory CreateMore(int num)
       {

           for (int i = 0; i < num; i++){
           var obj = new QuotationCriteria(){};
           ObjectList.Add(obj);}
           return this;
       }

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public QuotationCriteriaFactory CleanUp()
       {
           DeleteAll();
          return this;
       }

       #endregion

    }
}
