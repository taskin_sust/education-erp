﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ProgramTest
{
    interface IProgramGetTest
    {
        #region Basic Test
        void GetProgram_Should_Be_Return_Valid_Object();
        void GetProgram_Should_Be_Fail_For_Invalid_Program();

        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Program")]
    public class ProgramGetTest : ProgramBaseTest, IProgramGetTest
    {
        [Fact]
        public void GetProgram_Should_Be_Return_Valid_Object()
        {
           var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
           var program=_programService.GetProgram(programFactory.Object.Id);
           Assert.NotNull(program);
        }

         [Fact]
        public void GetProgram_Should_Be_Fail_For_Invalid_Program()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            var program = _programService.GetProgram(programFactory.Object.Id+100000);
            Assert.Null(program);
        }
        


       
    }
}
