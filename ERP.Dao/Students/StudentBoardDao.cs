﻿using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentBoardDao : IBaseDao<StudentBoard, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        StudentBoard GetByBoardName(string boardName, bool exactMatch = false);

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion
    }
    public class StudentBoardDao : BaseDao<StudentBoard, long>, IStudentBoardDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        public StudentBoard GetByBoardName(string boardName, bool exactMatch = false)
        {
            //return Session.QueryOver<StudentBoard>()
            //             .Where(
            //                 Restrictions.Eq(
            //                     Projections.SqlFunction("lower", NHibernateUtil.String,
            //                         Projections.Property<StudentBoard>(x => x.Name)), boardName.ToLower())).SingleOrDefault<StudentBoard>();

            if (exactMatch)
                return Session.QueryOver<StudentBoard>().Where(x => x.Name == boardName).SingleOrDefault<StudentBoard>();
            //if not exact match
            return Session.QueryOver<StudentBoard>().Where(x => x.Name.IsLike(boardName.ToLower(), MatchMode.Start)).List<StudentBoard>().FirstOrDefault();
        }
    }
}
