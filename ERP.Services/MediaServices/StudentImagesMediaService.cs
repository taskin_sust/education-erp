﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Dao.MediaDao;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.MediaServices
{
    public interface IStudentImagesMediaService
    {

        BusinessModel.Entity.MediaDb.StudentMediaImage GetStudentImageMediaByRefId(long id);

        void DeleteMediaImage(long refId);

        void Delete(BusinessModel.Entity.Students.StudentProgram studentProgram);
    }
    public class StudentImagesMediaService : IStudentImagesMediaService
    {
        private readonly IStudentImageMediaDao _studentImageMediaDao;
        private ISession session;
        public StudentImagesMediaService()
        {
            var sessionMedia = NHibernateMediaSessionFactory.OpenSession();
            session = sessionMedia;
            _studentImageMediaDao = new StudentImageMediaDao(sessionMedia);
        }

        //
        public StudentMediaImage GetStudentImageMediaByRefId(long id)
        {
            return _studentImageMediaDao.GetStudentImageMediaByRefId(id);
        }

        public void DeleteMediaImage(long refId)
        {
             _studentImageMediaDao.DeleteMediaImage(refId);
        }

        public void Delete(StudentProgram studentProgram)
        {
            var mediaTranacition = session.BeginTransaction();
            foreach (var imageObj in studentProgram.StudentImage)
            {
                _studentImageMediaDao.DeleteMediaImage(imageObj.Id);
            }
            mediaTranacition.Commit();
        }
    }
}
