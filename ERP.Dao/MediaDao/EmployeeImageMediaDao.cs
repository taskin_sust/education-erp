﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.MediaDb;

namespace UdvashERP.Dao.MediaDao
{
    public interface IEmployeeImageMediaDao
    {

        BusinessModel.Entity.MediaDb.EmployeeMediaImage GetEmployeeImageMediaByRefId(long refId);
    }
    public class EmployeeImageMediaDao : IEmployeeImageMediaDao
    {
        private ISession _session;
        public EmployeeImageMediaDao(ISession session)
        {
            _session = session;
        }

        public EmployeeMediaImage GetEmployeeImageMediaByRefId(long refId)
        {
            return
                _session.QueryOver<EmployeeMediaImage>().Where(x => x.ImageRef == refId).SingleOrDefault<EmployeeMediaImage>();
        }
    }
}
