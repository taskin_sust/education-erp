﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.UserAuth
{
    public interface IUserProfileDao : IBaseDao<UserProfile, long>
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        UserProfile LoadByAspNetUserWithoutStatus(AspNetUser basicUser);
        UserProfile LoadByAspNetUser(AspNetUser userObj,bool loadActive=true);
        UserProfile GetByAspNetUser(long aspnetUserId);
        #endregion

        #region List Loading Function
        IList<UserProfile> LoadUser(long[] branchIds = null, long[] campusIds = null);
        IList<UserProfile> LoadActiveAuthorizedUserByBranch(List<long> branchList);
        IList<UserProfile> LoadActiveAuthorizedUserByBranch(List<long> programIdList, List<long> branchIdList);
        IList<UserProfile> LoadUsersWithAuthorizedBranchPermission(long[] authorizedBranches);
        IList<UserProfile> LoadAuthorizedUser(bool? userStatus, List<long> authBranchIdList, List<long> campusIdList);
        IList<UserProfile> LoadLike(string email);
        IList<UserProfile> LoadActive(IList<long> idList = null, bool eagerLoadAspnetUser = false);
        IList<AspNetUser> LoadUsers(List<long> programIdList, long[] programIds, long[] sessionIds, long[] courseIds, long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo);
        IList<AspNetUser> LoadUsers(List<long> authProgramIdList, long[] programIds, long[] sessionIds, long[] courseIds, long[] examIdList, DateTime dateFrom, DateTime dateTo);
        List<UserProfile> LoadUserProfileListByDepartmentBranch(List<long> authorizedBranchIdList, List<long> userProfileIdList, List<long> campusIdList = null);
        #endregion

        #region Others Function
       
        bool HasDuplicateByEmail(string email, long id);
     
        #endregion

        //need to transfer
        IList<Branch> LoadAuthorizedBranchByProgram(long[] authorizedProgramLists, bool allProgram, long[] authorizedBranchLists, bool allBranch);

        //no need
        //new IList<UserProfile> LoadByEmail(string email);
        //bool HasDuplicateByEmail(string email);
        //int UserRowCount(string userName, string contactNo, string status, string rank);
    }
    public class UserProfileDao : BaseDao<UserProfile, long>, IUserProfileDao
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public UserProfile LoadByAspNetUser(AspNetUser userObj,bool loadActive=true)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(UserProfile));
            criteria.Add(Expression.Eq("AspNetUser", userObj));
            if (loadActive)
            {
                criteria.Add(Expression.Eq("Status", UserProfile.EntityStatus.Active)); 
            }
            return criteria.UniqueResult<UserProfile>();
        }
        public UserProfile GetByAspNetUser(long aspnetUserId)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(UserProfile));
            criteria.Add(Expression.Eq("AspNetUser.Id", aspnetUserId));
            criteria.Add(Expression.Eq("Status", UserProfile.EntityStatus.Active));
            criteria.SetFetchMode("AspNetUser", FetchMode.Eager);
            return criteria.UniqueResult<UserProfile>();
        }
        public UserProfile LoadByAspNetUserWithoutStatus(AspNetUser basicUser)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(UserProfile));
            criteria.Add(Expression.Eq("AspNetUser", basicUser));
            return criteria.UniqueResult<UserProfile>();
        }
        #endregion

        #region List Loading Function
        public IList<UserProfile> LoadUser(long[] branchIds = null, long[] campusIds = null)
        {
           ICriteria criteria = Session.CreateCriteria<UserProfile>().Add(Restrictions.Not(Restrictions.Eq("Status", UserProfile.EntityStatus.Delete)));
           if (branchIds != null && !branchIds.Contains(SelectionType.SelelectAll))
           {
               criteria.CreateCriteria("Branch", "branch")
               .Add(Restrictions.Eq("branch.Status", Campus.EntityStatus.Active))
               .Add(Restrictions.In("branch.Id", branchIds));
           }
            if (campusIds != null && !campusIds.Contains(SelectionType.SelelectAll))
            {
                criteria.CreateCriteria("Campus", "campus")
                .Add(Restrictions.Eq("campus.Status", Campus.EntityStatus.Active))
                .Add(Restrictions.In("campus.Id",campusIds));
            }
            return criteria.List<UserProfile>();
        }
        public IList<UserProfile> LoadAuthorizedUser(bool? userStatus, List<long> authBranchIdList, List<long> campusIdList)
        {
            ICriteria criteria = Session.CreateCriteria<UserProfile>().Add(Restrictions.Not(Restrictions.Eq("Status", UserProfile.EntityStatus.Delete)));
            if (userStatus != null)
            {
                if (userStatus == true)
                    criteria.Add(Restrictions.Eq("Status", UserProfile.EntityStatus.Active));
                else if (userStatus == false)
                    criteria.Add(Restrictions.Eq("Status", UserProfile.EntityStatus.Inactive));    
            }

            criteria.CreateAlias("Branch", "br", JoinType.LeftOuterJoin);
            criteria.CreateAlias("Campus", "ca", JoinType.LeftOuterJoin);
            criteria.SetFetchMode("AspNetUser", FetchMode.Eager);
            if (authBranchIdList != null && !authBranchIdList.Contains((int)SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("br.Id", authBranchIdList));
            }
            if (campusIdList != null && !campusIdList.Contains((int)SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("ca.Id", campusIdList));
            }
            return criteria.List<UserProfile>();
        }
        public IList<UserProfile> LoadActiveAuthorizedUserByBranch(List<long> branchList)
        {
            ICriteria criteria = Session.CreateCriteria<UserProfile>().Add(Restrictions.Eq("Status", UserProfile.EntityStatus.Active));
            criteria.CreateAlias("Branch", "br", JoinType.LeftOuterJoin);
            criteria.SetFetchMode("AspNetUser", FetchMode.Eager);
            if (branchList != null)
            {
                criteria.Add(Restrictions.In("br.Id", branchList));
            }
            else
            {
                criteria.Add(Restrictions.IsNull("Branch"));
            }
            return criteria.List<UserProfile>();
        }

        public IList<Branch> LoadAuthorizedBranchByProgram(long[] authorizedProgramLists, bool allProgram, long[] authorizedBranchLists, bool allBranch)
        {
            ICriteria criteria = Session.CreateCriteria<Branch>().Add(Restrictions.Eq("Status", Branch.EntityStatus.Active));
            DetachedCriteria branchListByProgram = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id"))).CreateCriteria("Program", "p");
            DetachedCriteria branchListBySession = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id"))).CreateCriteria("Session", "s");

            if (!allProgram)
            {
                branchListByProgram.Add(Restrictions.In("p.Id", authorizedProgramLists));
            }
            if (!allBranch)
            {
                criteria.Add(Restrictions.In("Id", authorizedBranchLists));
            }
            branchListByProgram.Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            branchListBySession.Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.Add(Subqueries.PropertyIn("Id", branchListByProgram));
            return criteria.List<Branch>();
        }
        public IList<UserProfile> LoadUsersWithAuthorizedBranchPermission(long[] authorizedBranches)
        {
            IList<UserProfile> userBySpecificBranch =
                Session.QueryOver<UserProfile>()
                    .Where(u => u.Status == UserProfile.EntityStatus.Active)
                    .Where(u => u.Branch != null)
                    .Where(u => u.Branch.Id.IsIn(authorizedBranches))
                    .List<UserProfile>();
            IList<UserProfile> userByAllBranch =
                Session.QueryOver<UserProfile>()
                    .Where(u => u.Status == UserProfile.EntityStatus.Active)
                    .Where(u => u.Branch == null)
                    .List<UserProfile>();
            return userBySpecificBranch.Concat(userByAllBranch).Distinct().ToList();
        }
        public IList<UserProfile> LoadActive(IList<long> idList = null, bool eagerLoadAspnetUser = false)
        {

            ICriteria criteria = Session.CreateCriteria<UserProfile>();
            criteria.Add(Restrictions.Eq("Status", UserProfile.EntityStatus.Active));
            if (idList != null && idList.Any())
                criteria.Add(Restrictions.In("Id", idList.ToArray()));
            if (eagerLoadAspnetUser)
                criteria.SetFetchMode("AspNetUser", FetchMode.Eager);

            return criteria.List<UserProfile>();
        }

        public IList<UserProfile> LoadActiveAuthorizedUserByBranch(List<long> programIdList, List<long> branchIdList)
        {
            ICriteria criteria = Session.CreateCriteria<UserProfile>().Add(Restrictions.Eq("Status", UserProfile.EntityStatus.Active));
            criteria.CreateAlias("Branch", "br", JoinType.LeftOuterJoin);
            criteria.SetFetchMode("AspNetUser", FetchMode.Eager);
            if (branchIdList!=null)
            {
                criteria.Add(Restrictions.In("br.Id", branchIdList));
            }
            return criteria.List<UserProfile>();
        }
        public IList<UserProfile> LoadLike(string email)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(UserProfile));
            criteria.Add(Restrictions.Like("Email", email, MatchMode.Anywhere));
            criteria.Add(Expression.Eq("Status", UserProfile.EntityStatus.Active));
            return criteria.List<UserProfile>();
        }

        public IList<AspNetUser> LoadUsers(List<long> programIdList, long[] programIds, long[] sessionIds, long[] courseIds,
            long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo)
        {
            var query = @"select distinct anu.Id,anu.Email from StudentClassAttendence sca inner join Lecture l on sca.LectureId=l.Id
                                    inner join LectureSettings ls on l.LectureSettingsId=ls.Id
                                    inner join Course c on c.Id=ls.CourseId
                                    inner join UserProfile up on up.AspNetUserId=sca.CreateBy
									inner join AspNetUsers anu on anu.Id=up.AspNetUserId                                      
                                    where c.ProgramId in({0}) and c.SessionId in({1})
                                    --and anu.status=1 
                                    and sca.status=1 and l.status=1 and ls.status=1 and c.status=1                                    
                                    and c.ProgramId in ({2})
                                    and sca.LectureId in ({3})
                                    and sca.TeacherId in ({4})
                                    and sca.HeldDate>='{5}' and sca.HeldDate<'{6}'";
            if (!courseIds.Contains(0))
            {
                query += " and c.Id in ({7}) ";
            }
            query = string.Format(query, string.Join(",", programIds), string.Join(",", sessionIds),
                string.Join(",", programIdList), string.Join(",", lectureIdList), string.Join(",", teacherIdList),
                dateFrom, dateTo.AddDays(1), string.Join(",", courseIds));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2000);
            iQuery.SetResultTransformer(Transformers.AliasToBean<AspNetUser>());
            return iQuery.List<AspNetUser>().ToList();
        }

        public IList<AspNetUser> LoadUsers(List<long> authProgramIdList, long[] programIds, long[] sessionIds, long[] courseIds,
            long[] examIdList, DateTime dateFrom, DateTime dateTo)
        {
            var query = @"select distinct anu.Id,anu.Email from StudentExamAttendance sea inner join exams e on sea.examid=e.Id
                                    inner join UserProfile up on up.AspNetUserId=sea.CreateBy
									inner join AspNetUsers anu on anu.Id=up.AspNetUserId                                     
                                    where e.ProgramId in({0}) and e.SessionId in({1})
                                    and sea.status=1 and  e.status=1                                    
                                    and e.ProgramId in ({2})
                                    and sea.examid in ({3})
                                    and sea.HeldDate>='{4}' and sea.HeldDate<'{5}'";
            if (!courseIds.Contains(0))
            {
                query += " and e.courseId in ({6}) ";
            }
            query = string.Format(query, string.Join(",", programIds), string.Join(",", sessionIds),
                string.Join(",", authProgramIdList), string.Join(",", examIdList),
                dateFrom, dateTo.AddDays(1), string.Join(",", courseIds));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2000);
            iQuery.SetResultTransformer(Transformers.AliasToBean<AspNetUser>());
            return iQuery.List<AspNetUser>().ToList();
        }

        public List<UserProfile> LoadUserProfileListByDepartmentBranch(List<long> authorizedBranchIdList, List<long> userProfileIdList, List<long> campusIdList = null)
        {
            ICriteria criteria = Session.CreateCriteria<UserProfile>().Add(Restrictions.Eq("Status", UserProfile.EntityStatus.Active));
            criteria.CreateAlias("Branch", "br", JoinType.LeftOuterJoin);
            criteria.CreateAlias("Campus", "cam", JoinType.LeftOuterJoin);
            criteria.SetFetchMode("AspNetUser", FetchMode.Eager);
            if (authorizedBranchIdList != null)
            {
                criteria.Add(Restrictions.In("br.Id", authorizedBranchIdList));
            }
            if (userProfileIdList != null && userProfileIdList.Any())
            {
                criteria.Add(Restrictions.In("Id", userProfileIdList));
            }
            if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("cam.Id", campusIdList));
            }
            return criteria.List<UserProfile>().ToList();
        }

        #endregion

        #region Others Function
      
        public bool HasDuplicateByEmail(string email, long Id)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(AspNetUser));
            criteria.Add(Restrictions.Not(Restrictions.Eq("Id", Id)));
            criteria.Add(Restrictions.Eq("Email", email)).Add(Restrictions.Not(Restrictions.Eq("Status", AspNetUser.EntityStatus.Delete)));
            IList<AspNetUser> rowList = criteria.List<AspNetUser>();
            if (rowList == null || rowList.Count < 1)
                return true;
            return false;
        }
       
        #endregion

        //no need

        //public new IList<UserProfile> LoadByEmail(string email)
        //{
        //    IQuery query = Session.CreateQuery("from User c where c.Email=:email and c.Status=:status");
        //    query.SetString("email", email);
        //    query.SetInt32("status", UserProfile.EntityStatus.Active);
        //    IList<UserProfile> rowList = query.List<UserProfile>();
        //    if (rowList == null || rowList.Count < 1)
        //        return null;
        //    return rowList;
        //}

        //public bool HasDuplicateByEmail(string email)
        //{
        //    IQuery query = Session.CreateQuery("from AspNetUser c where c.Email =:email");
        //    query.SetParameter("email", email);
        //    IList<AspNetUser> rowList = query.List<AspNetUser>();
        //    if (rowList == null || rowList.Count < 1)
        //        return true;
        //    return false;
        //}

        //public int UserRowCount(string username, string contactNo, string status, string rank)
        //{
        //    ICriteria criteria = Session.CreateCriteria<AspNetUser>()
        //            .Add(Restrictions.Not(Restrictions.Eq("UserName", "superadmin")));

        //    if (!String.IsNullOrEmpty(username))
        //    {
        //        criteria.Add(Restrictions.Like("UserName", username, MatchMode.Anywhere));
        //    }
        //    if (!String.IsNullOrEmpty(contactNo))
        //    {
        //        criteria.Add(Restrictions.Like("PhoneNumber", contactNo, MatchMode.Anywhere));
        //    }
        //    if (!String.IsNullOrEmpty(status))
        //    {
        //        criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
        //    }
        //    if (!String.IsNullOrEmpty(rank))
        //    {
        //        criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
        //    }
        //    IList list = criteria.Add(Restrictions.Not(Restrictions.Eq("Status", AspNetUser.EntityStatus.Delete))).List();
        //    return list.Count;
        //}
    }
}