using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class SessionMap : BaseClassMap<Session, long>
    {
        public SessionMap()
        {
            Table("Session");
            LazyLoad();

            Map(x => x.Rank);
            Map(x => x.Name);
            Map(x => x.Code).Column("Code");

            HasMany(x => x.Batches).KeyColumn("SessionId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.Discounts).KeyColumn("SessionId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.Materials).KeyColumn("SessionId").Cascade.SaveUpdate();

            HasMany(x => x.Courses).KeyColumn("SessionId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ProgramBranchSessions).KeyColumn("SessionId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ExamList).KeyColumn("SessionId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ServiceBlocks).KeyColumn("SessionId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            HasMany(x => x.SmsStudentRegistrationSettingList).KeyColumn("SessionId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
