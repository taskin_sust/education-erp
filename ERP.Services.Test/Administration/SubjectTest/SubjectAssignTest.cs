﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;
using SubjectAssign = UdvashERP.BusinessModel.Dto.SubjectAssign;

namespace UdvashERP.Services.Test.AdminisTration.SubjectTest
{
    public interface ISubjectAssignTest
    {
        #region basic test
        [Fact]
        void Should_Assign_Subject_Successfully();

        [Fact]
        void Should_Update_Assign_Subject_Successfully(); 
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "ProgramSessionSubject")]
    public class SubjectAssignTest : SubjectBaseTest, ISubjectAssignTest
    {
        #region basic test
        
        [Fact]
        public void Should_Assign_Subject_Successfully()
        {
            const int noOfSubject = 10;
            var organzationFactory = _organizationFactory.Create().WithRequiredProperties().Persist();
            var programCode = _programFactory.GetRandomNumber();
            var programFactory =
                _programFactory.Create().WithOrganization(organzationFactory.Object).WithNameAndRank(_name, 1).WithProperties(_name, programCode, 1)
                    .Persist();
            var sessionCode = _sessionFactory.GetRandomNumber();
            var sessionFactory = _sessionFactory.Create().WithNameAndRank(Guid.NewGuid() + "test session name", 1).WithCode(sessionCode).Persist();
            var subjectFactory =
                _subjectFactory.CreateMore(noOfSubject)
                    .Persist();
            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectFactory.GetLastCreatedObjectList().Select(x => Convert.ToInt32(x.Id)).ToArray()
            };
            var programId = programFactory.Object.Id;
            var sessionId = sessionFactory.Object.Id;
            var subjectIdList = subjectFactory.GetLastCreatedObjectList().Select(x => x.Id).ToList();
            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, programId,
                sessionId);
            var programSessionSubject = _programSessionSubjectService.LoadProgramSessionSubject(programId, sessionId);
            Assert.NotNull(programSessionSubject);
            Assert.True(programSessionSubject.Count > 0);
            CleanUpProgramSessionSubject(programId, sessionId, subjectIdList);
        }

        [Fact]
        public void Should_Update_Assign_Subject_Successfully()
        {
            const int noOfSubject = 10;
            var organzationFactory = _organizationFactory.Create().WithRequiredProperties().Persist();
            var programCode = _programFactory.GetRandomNumber();
            var programFactory =
                _programFactory.Create().WithOrganization(organzationFactory.Object).WithNameAndRank(_name, 1)
                .WithProperties(_name, programCode, 1)
                    .Persist();
            var sessionCode = _sessionFactory.GetRandomNumber();
            var sessionFactory = _sessionFactory.Create().WithNameAndRank(Guid.NewGuid() + "test session name", 1)
                .WithCode(sessionCode).Persist();
            var subjectFactory =
                _subjectFactory.CreateMore(noOfSubject)
                    .Persist();
            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectFactory.GetLastCreatedObjectList().Select(x => Convert.ToInt32(x.Id)).Skip(2).ToArray()
            };
            var programId = programFactory.Object.Id;
            var sessionId = sessionFactory.Object.Id;
            var subjectIdList = subjectFactory.GetLastCreatedObjectList().Select(x => x.Id).ToList();
            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, programId,
                sessionId);
            subjectAssign.SelectedSubjects[0] = (int)subjectIdList[0];
            subjectAssign.SelectedSubjects[1] = (int)subjectIdList[1];
            var returnMsg = _programSessionSubjectService.SaveUpdateSubject(subjectAssign, programId,
                sessionId);
            Assert.NotNull(returnMsg.Trim());
            Assert.True(returnMsg.Trim() != "");
            CleanUpProgramSessionSubject(programId, sessionId, subjectIdList);
        }
  
        #endregion
    }
}
