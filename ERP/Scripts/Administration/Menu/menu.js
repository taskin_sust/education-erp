$(document).ready(function () {

    function dataTableRender() {
            var pageSize = 10;
            if ($('#pageSize').val() != "" && /^\d+$/.test($('#pageSize').val())) {
                pageSize = parseInt($('#pageSize').val());
            }
            else {
                $('#pageSize').val(pageSize);
            }
            $('#DataGrid').dataTable({
                destroy: true,
                "processing": true,
                searching: false,
                serverSide: true,
                "scrollX": true,
                "bLengthChange": false,
                "iDisplayLength": pageSize,
                //stateSave: true,
                order: [[0, "desc"]],
                _pageSetter: function (val) {
                    return isFinite(+val) ? +val : null;
                },

                page: {
                    value: 1,
                    setter: '_pageSetter'
                },
                ajax: {
                    url: 'MenuListResult',
                    type: 'POST',
                    data: function (d) {
                        d.menuGroupId = $('#MenuGroupFiter').val();
                        d.parentId = $('#MenuListFiter').val();
                        d.name = $('#name').val();
                        d.controllerName = $('#controllerName').val();
                        d.actionName = $('#actionName').val();
                        d.status = $('#status').val();
                        d.rank = $('#rank').val();
                    }
                }
            });
        }

        dataTableRender();

        $(document).on("click", "#search", function () {
            dataTableRender();
        });

        $(document).on("change", "#MenuGroupFiter", function () {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/Menu/GetMenuForDropDownList",
                cache: false,
                async: true,
                data: { "groupId": $(this).val() },
                success: function (result) {
                    if (result.hasOwnProperty('IsSuccess')) {
                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    else {
                        $("#MenuListFiter").html(result);
                    }                              
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        });

        $(document).on("click", ".glyphicon-arrow-up, .glyphicon-arrow-down", function () {
            var id = $(this).attr("id");
            var current = $(this).parent().text().trim();
            var action = $(this).attr("data-action");
            //glyphicon glyphicon-arrow-up
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/Menu/RankChange",
                cache: false,
                async: true,
                data: { "id": id, "current": current, "action": action },
                success: function (result) {
                    if (result.IsSuccess) {
                        dataTableRender();
                    }
                    else {
                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });

        });

        $(document).on("click", ".glyphicon-trash", function () {
            var id = $(this).attr("id");
            var name = $(this).attr("data-name");
            if (id != "") {
                bootbox.deleteConfirm("<h3>Are you sure you want to delete this <span class='confirm-message'>" + name + "</span> Menu?</h3>", function (result) {
                    if (result) {
                        $.ajax({
                            type: "post",
                            url: $("body").attr("data-project-root") + "Administration/Menu/Delete",
                            cache: false,
                            async: true,
                            data: { "id": id },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (result) {
                                $.unblockUI();

                                if (result.IsSuccess) {
                                    $.fn.customMessage({
                                        displayMessage: "Menu Delete Successfully",
                                        displayMessageType: "success",
                                    });
                                    dataTableRender();
                                }
                                else {
                                    $.fn.customMessage({
                                        displayMessage: result.Message,
                                        displayMessageType: "error"
                                    });
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (result) {
                                $.fn.customMessage({
                                    displayMessage: "Menu Delete Failed",
                                    displayMessageType: "error",
                                });
                            }
                        });

                    }
                }).css({ 'margin-top': (($(window).height() / 4)) });
            }
            return false;
        });
    });



    /*
    $("#MenuGroupId").change(function () {
        var MenuGroupId = $("#MenuGroupId").val();
        if (MenuGroupId != "") {
            var data = { "MenuGroupId": MenuGroupId };
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Menu/getRootMenu",
                contenttype: "application/json; charset=utf-8",
                data: data,
                success: function (result) {
                    $("#ParentId option").remove();
                    if (!($.isEmptyObject(result))) {
                        $("#ParentId").append('<option value="">---Select---</option>');
                        $.each(result, function (index, menu) {
                            $("#ParentId").append('<option value="' + menu.value + '">' + menu.text + '</option>');
                        });
                    }
                    else { }                  
                },
                error: function (result) {
                    alert("Unable to update status.");
                }
            });  
        }
    });
    */


    
