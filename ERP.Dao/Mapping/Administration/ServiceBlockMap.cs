﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class ServiceBlockMap: BaseClassMap<ServiceBlock, long>
    {
        public ServiceBlockMap()
        {
            Table("ServiceBlock");
            LazyLoad();

            Map(x => x.ServiceType).Column("ServiceType");
            Map(x => x.ConditionType).Column("ConditionType");


            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
        }
    }
}
