﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.FestivalBonusSheetTest
{
    public class FestivalBonusSheetBaseTest : TestBase, IDisposable
    {
        internal readonly FestivalBonusSheetFactory _festivalBonusSheetFactory;
        internal readonly IFestivalBonusSheetService _festivalBonusSheetService;
        internal readonly FestivalBonusSettingFactory _festivalBonusSettingFactory;
        public FestivalBonusSheetBaseTest()
        {
            _festivalBonusSheetFactory = new FestivalBonusSheetFactory(null,Session);
            _festivalBonusSheetService = new FestivalBonusSheetService(Session);
            _festivalBonusSettingFactory = new FestivalBonusSettingFactory(null,Session);
        }

        public void Dispose()
        {
            _festivalBonusSheetFactory.CleanUp();
            salaryHistoryFactory.LogCleanUp(salaryHistoryFactory);
            salaryHistoryFactory.CleanUp();
            _festivalBonusSettingFactory.FestivalBonusSettingCalculationCleanUp(_festivalBonusSettingFactory);
            _festivalBonusSettingFactory.CleanUp();

            base.Dispose();
        }
    }
}
