﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.TdsHisotryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "TdsHistory")]
    public class TdsHistoryUpdateTest : TdsHistoryBaseTest
    {
        [Fact]
        public void TdsHistory_Update_Old_Object_UpdateReturnTrue()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            var objFactory = _tdsHistoryFactory.Create().WithTeamMember(teamMemberObj);
            _tdsHistoryService.AddTdsHistory(teamMemberObj, objFactory.Object.TdsAmount.ToString(),
                objFactory.Object.EffectiveDate.ToString());
            var oldEnity = _tdsHistoryService.GetTdsHistory(teamMemberObj.Id);
            oldEnity.TdsAmount = 1500;
            bool saveOldEnity = _tdsHistoryService.UpdateTdsHistory(oldEnity, oldEnity.TdsAmount.ToString(), oldEnity.EffectiveDate.ToString());
            Assert.True(saveOldEnity);

        }

        [Fact]
        public void InvalidDataExpection_UpdateTdsHistory_For_Amount_Null()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            var objFactory = _tdsHistoryFactory.Create().WithTeamMember(teamMemberObj);
            _tdsHistoryService.AddTdsHistory(teamMemberObj, objFactory.Object.TdsAmount.ToString(),
                objFactory.Object.EffectiveDate.ToString());
            var oldEnity = _tdsHistoryService.GetTdsHistory(teamMemberObj.Id);
            oldEnity.TdsAmount = 1500;
            Assert.Throws<InvalidDataException>(() => _tdsHistoryService.UpdateTdsHistory(oldEnity, null, oldEnity.EffectiveDate.ToString()));
        }

        [Fact]
        public void InvalidDataExpection_UpdateTdsHistory_For_EffectiveDate_Null()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            var objFactory = _tdsHistoryFactory.Create().WithTeamMember(teamMemberObj);
            _tdsHistoryService.AddTdsHistory(teamMemberObj, objFactory.Object.TdsAmount.ToString(),
                objFactory.Object.EffectiveDate.ToString());
            var oldEnity = _tdsHistoryService.GetTdsHistory(teamMemberObj.Id);
            oldEnity.TdsAmount = 1500;
            Assert.Throws<InvalidDataException>(() => _tdsHistoryService.UpdateTdsHistory(oldEnity, oldEnity.TdsAmount.ToString(), null));
        }
    }
}
