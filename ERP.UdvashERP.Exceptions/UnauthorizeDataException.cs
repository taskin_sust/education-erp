﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.MessageExceptions
{
    public class UnauthorizeDataException : Exception
    {
        public UnauthorizeDataException()
            : base("Not Found")
        {

        }

        public UnauthorizeDataException(string message)
            : base(message)
        {

        }

    }


}
