using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IChildrenAllowanceSettingDao : IBaseDao<ChildrenAllowanceSetting, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        IList<ChildrenAllowanceSetting> LoadChildrenAllowanceSetting(int draw, int start, int length, List<long> authOrgList, int? empStatus, int? payType);
        IList<ChildrenAllowanceSetting> LoadByOrganization(long orgId);
        IList<ChildrenAllowanceSetting> LoadByOrgAndEmpStatus(long orgId, int employmentStatus);
        IList<ChildrenAllowanceSetting> GetTeamMemberChildrenEducationAllowance(long orgId, int empStatus, System.DateTime dateTime);
        IList<ChildrenAllowanceSetting> LoadByOrgAndEmpStatus(long orgId, IList<MemberEmploymentStatus> list, int paymentType, DateTime effectiveDate, DateTime? closingDate, long id = 0);

        #endregion

        #region Others Function

        int CountAllowance(List<long> authOrgList, int? empStatus, int? payType);
        DateTime GetEligibleClosingDateForPayrollSetting(long id);
        bool IsChildrenAllowanceSettingStillValid(long id);

        #endregion
    }

    public class ChildrenAllowanceSettingDao : BaseDao<ChildrenAllowanceSetting, long>, IChildrenAllowanceSettingDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<ChildrenAllowanceSetting> LoadChildrenAllowanceSetting(int draw, int start, int length, List<long> authOrgList, int? empStatus, int? payType)
        {
            var criteria = Session.CreateCriteria<ChildrenAllowanceSetting>();
            criteria = GetQueryCriteria(criteria, authOrgList, empStatus, payType);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<ChildrenAllowanceSetting>();
        }

        public IList<ChildrenAllowanceSetting> LoadByOrganization(long orgId)
        {
            var criteria = Session.CreateCriteria<ChildrenAllowanceSetting>();
            criteria = GetQueryCriteria(criteria, new List<long>() { orgId }, null, null);
            return criteria.List<ChildrenAllowanceSetting>();
        }

        public IList<ChildrenAllowanceSetting> LoadByOrgAndEmpStatus(long orgId, int empStatus)
        {
            var criteria = Session.CreateCriteria<ChildrenAllowanceSetting>();
            criteria = GetQueryCriteria(criteria, new List<long>() { orgId }, empStatus, null);
            return criteria.List<ChildrenAllowanceSetting>();
        }

        public IList<ChildrenAllowanceSetting> GetTeamMemberChildrenEducationAllowance(long organizationId, int empStatus, DateTime dateTime)
        {
            var query = Session.QueryOver<ChildrenAllowanceSetting>();
            switch (empStatus)
            {
                case (int)AllowanceSettingEmployeeStatus.Contractual: query.Where(x => x.IsContractual);
                    break;
                case (int)AllowanceSettingEmployeeStatus.PartTime: query.Where(x => x.IsPartTime);
                    break;
                case (int)AllowanceSettingEmployeeStatus.Permanent: query.Where(x => x.IsPermanent);
                    break;
                case (int)AllowanceSettingEmployeeStatus.Probation: query.Where(x => x.IsProbation);
                    break;
                case (int)AllowanceSettingEmployeeStatus.Intern: query.Where(x => x.IsIntern);
                    break;
            }
            query.Where(x => x.Organization.Id == organizationId);
            query.Where(x => x.EffectiveDate <= dateTime);
            query = query.Where(x => x.ClosingDate == null || x.ClosingDate >= dateTime);
            return query.List<ChildrenAllowanceSetting>();
        }

        public IList<ChildrenAllowanceSetting> LoadByOrgAndEmpStatus(long orgId, IList<MemberEmploymentStatus> empStatus, int paymentType, DateTime effectiveDate, DateTime? closingDate, long id = 0)
        {
            var criteria = Session.CreateCriteria<ChildrenAllowanceSetting>();
            criteria = GetQueryCriteria(criteria, new List<long>() { orgId }, empStatus);
            criteria.Add(Restrictions.Eq("PaymentType", paymentType));
            if (id > 0)
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            if (closingDate != null)
                criteria.Add(Restrictions.And(Restrictions.Le("EffectiveDate", effectiveDate), Restrictions.Ge("ClosingDate", closingDate)));
            var list = criteria.List<ChildrenAllowanceSetting>();
            return list.ToList();
        }

        #endregion

        #region Others Function

        public int CountAllowance(List<long> authOrgList, int? empStatus, int? payType)
        {
            var criteria = Session.CreateCriteria<ChildrenAllowanceSetting>();
            criteria = GetQueryCriteria(criteria, authOrgList, empStatus, payType);

            return criteria.List<ChildrenAllowanceSetting>().Count;
        }

        private NHibernate.ICriteria GetQueryCriteria(NHibernate.ICriteria criteria, List<long> authOrgList, IList<MemberEmploymentStatus> empStatusList)
        {
            criteria.Add(Restrictions.Eq("Status", ChildrenAllowanceSetting.EntityStatus.Active));
            criteria.CreateAlias("Organization", "org");
            if (authOrgList.Count > 0)
                criteria.Add(Restrictions.In("org.Id", authOrgList));
            var or = Restrictions.Disjunction();
            foreach (var memberEmploymentStatus in empStatusList)
            {
                var memberEmploymentStatuse = (int)memberEmploymentStatus;
                switch (memberEmploymentStatuse)
                {
                    case (int)MemberEmploymentStatus.Permanent:
                        var resF = Restrictions.Eq("IsPermanent", true);
                        or.Add(resF);
                        break;
                    case (int)MemberEmploymentStatus.Probation:
                        var resS = Restrictions.Eq("IsProbation", true);
                        or.Add(resS);
                        break;
                    case (int)MemberEmploymentStatus.PartTime:
                        var resT = Restrictions.Eq("IsPartTime", true);
                        or.Add(resT);
                        break;
                    case (int)MemberEmploymentStatus.Contractual:
                        var resFo = Restrictions.Eq("IsContractual", true);
                        or.Add(resFo);
                        break;
                    case (int)MemberEmploymentStatus.Intern:
                        var resFiv = Restrictions.Eq("IsIntern", true);
                        or.Add(resFiv);
                        break;
                    default: break;
                }
            }
            criteria.Add(or);
            return criteria;
        }

        private NHibernate.ICriteria GetQueryCriteria(NHibernate.ICriteria criteria, List<long> authOrgList, int? empStatus, int? payType)
        {
            criteria.Add(Restrictions.Eq("Status", ChildrenAllowanceSetting.EntityStatus.Active));
            if (authOrgList.Count > 0)
                criteria.Add(Restrictions.In("Organization", authOrgList));
            if (empStatus != null)
            {
                switch (empStatus)
                {
                    case (int)MemberEmploymentStatus.Permanent:
                        criteria.Add(Restrictions.Eq("IsPermanent", true));
                        break;
                    case (int)MemberEmploymentStatus.Probation:
                        criteria.Add(Restrictions.Eq("IsProbation", true));
                        break;

                    case (int)MemberEmploymentStatus.PartTime:
                        criteria.Add(Restrictions.Eq("IsPartTime", true));
                        break;
                    case (int)MemberEmploymentStatus.Contractual:
                        criteria.Add(Restrictions.Eq("IsContractual", true));
                        break;
                    case (int)MemberEmploymentStatus.Intern:
                        criteria.Add(Restrictions.Eq("IsIntern", true));
                        break;
                    default: break;
                }
            }
            if (payType != null)
            {
                switch (payType)
                {
                    case (int)PaymentType.Monthly:
                        criteria.Add(Restrictions.Eq("PaymentType", payType));
                        break;
                    case (int)PaymentType.Yearly:
                        criteria.Add(Restrictions.Eq("PaymentType", payType));
                        break;
                }
            }
            return criteria;
        }

        public DateTime GetEligibleClosingDateForPayrollSetting(long id)
        {
            var query = @"select top(1) aSheet.DateTo from(
                    select * from [HR_AllowanceSheetSecondDetails] where ChildrenAllowanceSettingId=" + id;
            query += @") as aSheetSecDetail
                    left join HR_AllowanceSheet as aSheet on aSheet.Id= aSheetSecDetail.AllowanceSheetId and aSheet.IsFinalSubmit='true'
                    order by aSheet.DateTo desc ";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            return iQuery.UniqueResult<DateTime>();
        }

        public bool IsChildrenAllowanceSettingStillValid(long id)
        {
            var closingDate = this.GetEligibleClosingDateForPayrollSetting(id);
            if (closingDate == DateTime.MinValue) return false;
            if (closingDate != DateTime.MinValue && closingDate < DateTime.Now) return false;
            return true;
        }

        #endregion

        #region Helper Function

        #endregion
    }
}
