﻿/*Written By Taskin*/

var programId = "";
var sessionId = "";
var branchId = "";
var campusId = "";
var batchDay = "";
var batchTime = "";
var batchName = "";
var studentName = "";
var mobileNumber = "";

function StudentViewModel() {
    this.Id = 0;
    this.Name = 0;
    this.MobNumber = 0;
    this.Program = 0;
    this.Session = "";
    this.Branch = "";
    this.Campus = "";
    this.BatchDays = "";
    this.BatchTime = "";
    this.Batch = "";
    this.CourseViewModels = [];
    this.StudentPayment = {};
    this.StudentViewModel = function () { };
};

function CourseViewModel() {
    this.Id = 0;
    this.Name = 0;
    this.ProgramId = "";
    this.SessionId = "";
    this.IsComplementaryCourse = false;
    this.IsTaken = false;
    this.IsNewlyTakenCourse = false;
    this.SubjectViewModels = [];
    this.CourseViewModel = function () { };
};

function SubjectViewModel() {
    this.Id = 0;
    this.Name = 0;
    this.Payment = 0;
    this.IsTaken = false;
    this.IsNewlyTakenSubject = false;
    this.SubjectViewModel = function () { };
};

function StudentPaymentModel() {

    this.OfferedDiscount = 0;
    this.NetReceivable = 0;
    this.CashBackAmount = 0;
    this.ConsiderationAmount = 0;
    this.CourseFees = 0;
    /*this.CourseSubjectList = [];*/
    this.DiscountAmount = 0;
    this.DueAmount = 0;
    this.NextReceivedDate = "";
    this.PayableAmount = 0;
    this.PaymentMethod = 0;
    this.PaymentType = 0;
    this.ReceiptNo = "";
    this.ReceivedAmount = 0;
    this.ReceivableAmount = 0;
    this.ReceivedDate = "";
    this.ReferrerId = 0;
    this.SpDiscountAmount = 0;
    this.Remarks = "";
    this.SpReferenceNote = "";
    this.StudentPaymentModel = function () { };
};

function CourseSubject() {
    this.Course = new Object();
    this.Subject = new Object();
};

function valueCalculation() {
    studentName = $('#StudentName').val();
    mobileNumber = $('#MobileNumber').val();
    programId = $('#Program').val();
    sessionId = $('#Session').val();
    branchId = $('#Branch').val();
    campusId = $('#Campus').val();
    batchDay = $('#BatchDays').val();
    batchTime = $('#BatchTime').val();
    batchName = $('#Batch').val();
}

$(document).ready(function () {

    validateTextField("referrerenceNote", "Please Enter Referance Note.");
    validateDropDownField("REFERRERLIST", "Please Select Referere.");

    $(document).on("change", "#nextRecDate", function (event) {
        var val = $("#nextRecDate").val();
        console.log(val);
        if (val.length > 0) {
            showErrorMessageBelowCtrl("nextRecDate", "", false);
        }
    });

    $(document).on('input propertychanges', '.numbersOnly', function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g, '');
        var consideratAmount = $('#considerationAmount').val();
        consideratAmount = Number(consideratAmount);
        if (consideratAmount < 0) {
            $('#considerationAmount').val(0);
        }
    });


    $(document).on("input propertychange", "#considerationAmount", function (event) {

        $('#duePaymentErrorMessageContainer').empty();
        $('#considerationAmountErrorMessageContainer').empty();
        $('#considerationAmount').removeClass('highlight');
        $('#dueAmount').removeClass('highlight');
        $('#nextRecDate').removeClass('highlight');
        $('#nextDateErrorMessageContainer').empty();
        $('#nextRecDate').removeClass('highlight');

        var dueAmount = $('#dueAmount').val();
        var considerationAmount = $('#considerationAmount').val();
        var cancellationAmount = $('#cancellationAmount').val();
        cancellationAmount = Number(cancellationAmount);
        considerationAmount = Number(considerationAmount);

        dueAmount = Number(dueAmount);

        if (dueAmount < 0) {
            showErrorMessageBelowCtrl("dueAmount", "", false);
            showErrorMessageBelowCtrl("dueAmount", " Manually Data tempered !! Serious Crime", true);
        }
        if (cancellationAmount < considerationAmount) {
            showErrorMessageBelowCtrl("considerationAmount", "", false);
            showErrorMessageBelowCtrl("considerationAmount", "Consideration amount can't be higher than your Cancellation amount", true);
        } else {
            if (dueAmount - considerationAmount > 0) {

                $('#cashBack').val(0);
                $('#currentDue').val(dueAmount - considerationAmount);
                var nextDate = $('#nextRecDate').val();
                var currentDue = $('#currentDue').val();
                if (Number(currentDue) > 0 && nextDate.length <= 0) {
                    showErrorMessageBelowCtrl("nextRecDate", "", false);
                    showErrorMessageBelowCtrl("nextRecDate", "Next Receiving date required", true);
                }

            }
            else if (dueAmount - considerationAmount == 0) {
                
                $('#cashBack').val(0);
                $('#currentDue').val(dueAmount - considerationAmount);
                showErrorMessageBelowCtrl("nextRecDate", "Next Receiving date required", false);
            }
            else {
                if (considerationAmount >= 0) {
                    $('#cashBack').val(considerationAmount - dueAmount);
                    $('#currentDue').val(0);
                    showErrorMessageBelowCtrl("nextRecDate", "Next Receiving date required", false);
                } else {
                    $('#cashBack').val(0);
                }
            }
        }
    });

    /*Cancel Course Payment Part */
    $(document).on('click', '#courseCancelPayment', function () {
        valueCalculation();

        $('#referrerenceNote').removeClass('highlight');
        $('#cancellationAmount').removeClass('highlight');
        $('#REFERRERLIST').removeClass('highlight');
        $('#nextRecDate').removeClass('highlight');

        var isReqSendToServer = true;
        var student = new StudentViewModel();
        $('.panel-heading input:checkbox').each(function (index, value) {
            var courseDetail = new CourseViewModel();
            var currentCheckboxClassParent = $(this).parent();
            var courseName = currentCheckboxClassParent.next().attr('data-bodyname');

            /* CourseViewModel object form */
            courseDetail.Name = courseName.trim();
            courseDetail.ProgramId = programId;
            courseDetail.SessionId = sessionId;
            courseDetail.Id = $(this).attr('data-course-id');
            var innerCheckboxClassName = currentCheckboxClassParent.next().attr('class');
            var spil = innerCheckboxClassName.split(" ");

            console.log("FLOG " + spil[1]);

            $('.' + spil[1] + ' input[type=checkbox]').each(function (index, value) {

                if ($(this).is(':checked')) {
                    console.log("Entered WRONG");
                    /*Subject Object Form*/
                } else {
                    console.log("Entered RIGHT");
                    var subject = new SubjectViewModel();
                    subject.Id = $(this).attr('data-course-subject-id');
                    subject.Name = $(this).attr('data-course-subject-name');
                    subject.Payment = $(this).attr('data-course-subject-payment');
                    courseDetail.SubjectViewModels.push(subject);
                }
            });
            student.CourseViewModels.push(courseDetail);
        });
        console.log(student);
        var studentObj = JSON.stringify(student);
        var stdPId = $('#stdProRoll').val();

        var dueAmount = $('#dueAmount').val();
        var cancleAmount = $('#cancellationAmount').val();
        var considerAmount = $('#considerationAmount').val();
        var cashBack = $('#cashBack').val();
        var currentDue = $('#currentDue').val();
        var nextDate = $('#nextRecDate').val();

        var referrer = $('#REFERRERLIST').val();
        var referrerenceNote = $('#referrerenceNote').val();

        if (referrer.length <= 0) { $('#REFERRERLIST').addClass('highlight'); isReqSendToServer = false; }
        if (referrerenceNote.length <= 0) { $('#referrerenceNote').addClass('highlight'); isReqSendToServer = false; }
        if (cancleAmount.length <= 0) { $('#cancellationAmount').addClass('highlight'); isReqSendToServer = false; }
        if (considerAmount.length <= 0) { $('#considerationAmount').val(0); }
        if (Number(considerAmount) <= 0) { $('#considerationAmount').addClass('highlight'); isReqSendToServer = false; }
        if (cashBack.length <= 0) { $('#cashBack').val(0); }
        if (currentDue.length <= 0) { $('#currentDue').val(0); }
        if (Number(cancleAmount) <= 0) { $('#cancellationAmount').addClass('highlight'); isReqSendToServer = false; }
        considerAmount = $('#considerationAmount').val();
        cashBack = $('#cashBack').val();
        currentDue = $('#currentDue').val();
        if (Number(currentDue) > 0 && nextDate.length <= 0) { $('#nextRecDate').addClass('highlight'); isReqSendToServer = false; }
        if (isReqSendToServer) {
            $.ajax({
                url: "/Student/CancelCourse/CourseCanceledAndPaymentCalculation",
                type: "POST",
                data: { studentObj: studentObj, stdPId: stdPId, dueAmount: dueAmount, cancleAmount: cancleAmount, considerAmount: considerAmount, cashBack: cashBack, currentDue: currentDue, nextDate: nextDate, referrer: referrer, referrerenceNote: referrerenceNote },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {

                    if (data.IsSuccess) {
                        $.unblockUI();
                        var link = "/Student/Payment/GenerateMoneyReciept?id=" + data.Message + "";
                        console.log(link);
                        window.location.href = link;

                    } else {
                        $.fn.customMessage({
                            displayMessage: data.Message,
                            displayMessageType: "e",
                            //displayTime: 5000
                        });
                        //$('.customMessage').append(data.Message);
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                }
            });
        } else {
            //$.fn.customMessage({
            //    displayMessage: "Input Field Value Missing",
            //    displayMessageType: "e",
            //});
        }

    });


});