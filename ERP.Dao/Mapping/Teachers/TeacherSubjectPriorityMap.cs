﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherSubjectPriorityMap : BaseClassMap<TeacherSubjectPriority, long>
    {
        public TeacherSubjectPriorityMap()
        {
            Table("TeacherSubjectPriority");
            LazyLoad();
            //Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Priority);
            References(x => x.Teacher).Column("TeacherId");
            References(x => x.Subject).Column("SubjectId");
        }
    }
}
