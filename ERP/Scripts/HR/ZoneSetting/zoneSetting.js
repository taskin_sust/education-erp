﻿
$(document).ready(function () {

    function addOperation(e,button) {
        $('.field-validation-error').remove();
        var organizationId = $('#OrganizationId').val();
        console.log(organizationId);
        var name = $('#Name').val();
        var toleranceTime = $('#ToleranceTime').val();
        var toleranceDay = $('#ToleranceDay').val();
        var deductionMultiplier = $('#DeductionMultiplier').val();
        var isSuccess = true;
        var customError = false;
        if (organizationId == null || organizationId <= 0) {
            window.showErrorMessageBelowCtrl("OrganizationId", "", false);
            window.showErrorMessageBelowCtrl("OrganizationId", "Please Select Organization.", true);
            isSuccess = false;
        }
        if (toleranceTime == null || toleranceTime.length <= 0) {
            window.showErrorMessageBelowCtrl("ToleranceTime", "", false);
            window.showErrorMessageBelowCtrl("ToleranceTime", "Please select Tolerance Time.", true);
            isSuccess = false;
        }
        if (toleranceDay == null || toleranceDay.length <= 0) {
            window.showErrorMessageBelowCtrl("ToleranceDay", "", false);
            window.showErrorMessageBelowCtrl("ToleranceDay", "Please select Tolerance Day.", true);
            isSuccess = false;
        }
        if (deductionMultiplier == null || deductionMultiplier.length <= 0) {
            window.showErrorMessageBelowCtrl("DeductionMultiplier", "", false);
            window.showErrorMessageBelowCtrl("DeductionMultiplier", "Please select Deduction Multiplier.", true);
            isSuccess = false;
        }
        if (name == null || name.length <= 0) {
            window.showErrorMessageBelowCtrl("Name", "", false);
            window.showErrorMessageBelowCtrl("Name", "Please enter valid Shift Name.", true);
            isSuccess = false;
        }
        if (isSuccess == true) {

            $.ajax({
                url: $("body").attr("data-project-root") + "Hr/ZoneSetting/Create",
                type: "POST",
                data: {
                    name: name, organizationId: organizationId, toleranceTime: toleranceTime, toleranceDay: toleranceDay, deductionMultiplier: deductionMultiplier
                },
                success: function (data) {
                    if (data.IsSuccess) {
                        if (button == 1 && customError == false) {
                            window.location = $("body").attr("data-project-root") + "Hr/ZoneSetting/ManageZoneSetting?message=" + data.Message;
                        } else {
                                $('#Name').val("");
                                $('#ToleranceTime').val("");
                                var organizationIdSelect = $('#OrganizationId');
                                organizationIdSelect[0].selectedIndex = 0;
                                var toleranceDayIdSelect = $('#ToleranceDay');
                                toleranceDayIdSelect[0].selectedIndex = 0;
                                var deductionMultiplierIdSelect = $('#DeductionMultiplier');
                                deductionMultiplierIdSelect[0].selectedIndex = 0;
                            $.fn.customMessage({
                                displayMessage: data.Message,
                                displayMessageType: "s",
                                //displayTime: 5000
                            });
                        }

                    } else {
                        $.fn.customMessage({
                            //displayMessage: ERROR_MESS,
                            displayMessage: data.Message,
                            displayMessageType: "e",
                            //displayTime: 5000
                        });
                    }
                },
                error: function () {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
            e.preventDefault();
        } else {
            e.preventDefault();
        }
    }

    $(document).on('click', '#saveAndExit', function (e) {
        addOperation(e,1);
    });
    $(document).on('click', '#saveAndNew', function (e) {
        addOperation(e, 2);
    });

    $(document).on('click', '#update', function (e) {
        $('.field-validation-error').remove();
        var organizationId = $('#OrganizationId').val();
        var id = $('#Id').val();
        console.log(id);
        var name = $('#Name').val();
        var toleranceTime = $('#ToleranceTime').val();
        var toleranceDay = $('#ToleranceDay').val();
        var deductionMultiplier = $('#DeductionMultiplier').val();
        var status = $('#Status').val();
        var isSuccess = true;
        var customError = false;
        if (organizationId == null || organizationId <= 0) {
            window.showErrorMessageBelowCtrl("OrganizationId", "", false);
            window.showErrorMessageBelowCtrl("OrganizationId", "Please Select Organization.", true);
            isSuccess = false;
        }
        if (toleranceTime == null || toleranceTime.length <= 0) {
            window.showErrorMessageBelowCtrl("ToleranceTime", "", false);
            window.showErrorMessageBelowCtrl("ToleranceTime", "Please select Tolerance Time.", true);
            isSuccess = false;
        }
        if (toleranceDay == null || toleranceDay.length <= 0) {
            window.showErrorMessageBelowCtrl("ToleranceDay", "", false);
            window.showErrorMessageBelowCtrl("ToleranceDay", "Please select Tolerance Day.", true);
            isSuccess = false;
        }
        if (deductionMultiplier == null || deductionMultiplier.length <= 0) {
            window.showErrorMessageBelowCtrl("DeductionMultiplier", "", false);
            window.showErrorMessageBelowCtrl("DeductionMultiplier", "Please select Deduction Multiplier.", true);
            isSuccess = false;
        }
        if (name == null || name.length <= 0) {
            window.showErrorMessageBelowCtrl("Name", "", false);
            window.showErrorMessageBelowCtrl("Name", "Please enter valid Shift Name.", true);
            isSuccess = false;
        }
        if (isSuccess == true) {

            $.ajax({
                url: $("body").attr("data-project-root") + "Hr/ZoneSetting/Edit",
                type: "POST",
                data: {
                    //string name, string organizationId, string toleranceTime, string toleranceDay, string deductionMultiplier, string status
                    id: id, name: name, organizationId: organizationId, toleranceTime: toleranceTime, toleranceDay: toleranceDay, deductionMultiplier: deductionMultiplier, status: status
                },
                success: function (data) {
                    if (data.IsSuccess) {
                        //if (button == 1 && customError == false) {
                        //    window.location = $("body").attr("data-project-root") + "HR/ShiftSetting/ManageShiftSetting?message=" + data.Message;
                        //} else {
                            $.fn.customMessage({
                                displayMessage: data.Message,
                                displayMessageType: "s",
                                //displayTime: 5000
                            });
                        //}

                    } else {
                        $.fn.customMessage({
                            //displayMessage: ERROR_MESS,
                            displayMessage: data.Message,
                            displayMessageType: "e",
                            //displayTime: 5000
                        });
                    }
                },
                error: function () {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
            e.preventDefault();
        } else {
            e.preventDefault();
        }
    });

   
});