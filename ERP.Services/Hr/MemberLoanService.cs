using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Util;
using Remotion.Linq.Parsing;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.Hr
{
    public interface IMemberLoanService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(MemberLoan prMemberLoan);
        void SaveOrUpdate(List<MemberLoan> memberLoans);

        #endregion

        #region Single Instances Loading Function
        MemberLoanDetailsDto GenerateLoanDetailByMemberPin(long id);
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        decimal GetCurrentLoan(int pin);
        decimal GetCurrentMonthlyRefund(int pin);

        #endregion

        #region Helper Function

        #endregion

        
    }
    public class MemberLoanService : BaseService, IMemberLoanService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("PayrollService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IMemberLoanDao _memberLoanDao;
        private readonly IMemberLoanApplicationDao _memberLoanApplicationDao;
        public MemberLoanService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _memberLoanDao = new MemberLoanDao { Session = session };
            _memberLoanApplicationDao = new MemberLoanApplicationDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(MemberLoan prMemberLoan)
        {
            return true;
        }

        public void SaveOrUpdate(List<MemberLoan> memberLoans)
        {
            ITransaction trans = null;
            try
            {
                foreach (var memberLoan in memberLoans)
                {
                    ModelValidationCheck(memberLoan);
                    CheckBeforSave(memberLoan);
                }
                using (trans = Session.BeginTransaction())
                {
                    foreach (var memberLoan in memberLoans)
                    {
                        _memberLoanApplicationDao.Save(memberLoan.MemberLoanApplication);
                        _memberLoanDao.Save(memberLoan);
                    }
                    trans.Commit();
                }
            }
            catch (InvalidDataException ide)
            {
                if (trans != null) { trans.Rollback(); }
                _logger.Error(ide);
                throw ide;
            }
            catch (Exception ex)
            {
                if (trans != null) { trans.Rollback(); }
                _logger.Error(ex);
                throw ex;
            }
        }

       

        private void CheckBeforSave(MemberLoan memberLoan)
        {
            IList<MemberLoanApplication> canAddOpeningBalance = _memberLoanApplicationDao.LoadByMemberId(memberLoan.MemberLoanApplication.TeamMember.Id);
            if (canAddOpeningBalance.Count > 0) throw new InvalidDataException(memberLoan.MemberLoanApplication.TeamMember.Name + " had applied for loan! invalid loan application ");
        }

        public void ModelValidationCheck(MemberLoan memberLoan)
        {
            string errorMessage = "";
            var memberLoanApplicationValidationResult =
                ValidationHelper.ValidateEntity<MemberLoan, MemberLoan>(memberLoan);
            if (memberLoanApplicationValidationResult.HasError)
            {
                memberLoanApplicationValidationResult.Errors.ForEach(
                    r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
            }
        }

        #endregion

        #region Single Instances Loading Function
        public MemberLoanDetailsDto GenerateLoanDetailByMemberPin(long id)
        {
            if(id<=0) throw new InvalidDataException("Invalid Pin found");
            return _memberLoanDao.GenerateLoanDetailByMemberPin(id);
        }
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        public decimal GetCurrentLoan(int pin)
        {
            try
            {
                decimal currentLoan = _memberLoanDao.GetCurrentLoan(pin);
                return currentLoan;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public decimal GetCurrentMonthlyRefund(int pin)
        {
            try
            {
                decimal currentLoan = _memberLoanDao.GetCurrentMonthlyRefund(pin);
                return currentLoan;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }



        #endregion

        #region Helper function



        #endregion
    }
}

