﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface IMemberFingerPrintLogDao : IBaseDao<MemberFingerPrintLog, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function



        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class MemberFingerPrintLogDao : BaseDao<MemberFingerPrintLog, long>, IMemberFingerPrintLogDao
    {
        #region Propertise & Object Initialization

        public MemberFingerPrintLogDao()
        {

        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function



        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
