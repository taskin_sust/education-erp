﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class JobExperienceFactory : ObjectFactoryBase<JobExperience>
    {
        private readonly IJobExperienceService _jobExperienceService;
        private readonly TeamMemberFactory _teamMemberFactory;
        public JobExperienceFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _jobExperienceService = new JobExperienceService(session);
        }

        #region Create

        public JobExperienceFactory Create()
        {
            Object = new JobExperience()
            {
                CompanyName = "OnnoRokom Group",
                CompanyType = 1,
                CompanyLocation = "Karwan Bazar, Dhaka",
                Position = "Software Engineer",
                ExperienceArea = "ASP.NET MVC, ANGULAR.JS",
                Duration = "3",
                DepartmentName = "Software Development",
                FromDate = DateTime.Now,
                ToDate = DateTime.Now.AddDays(100),
                BusinessId = GetPcUserName()
            };
            SingleObjectList.Add(Object);

            return this;
        }

        public JobExperienceFactory CreateWith(string companyName, int companyType, string companyLocation, string position, string experienceArea,
            string duration, string departmentName, DateTime fromDate, DateTime toDate)
        {
            Object = new JobExperience()
            {
                CompanyName = companyName,
                CompanyType = companyType,
                CompanyLocation = companyLocation,
                Position = position,
                ExperienceArea = experienceArea,
                Duration = duration,
                DepartmentName = departmentName,
                FromDate = fromDate,
                ToDate = toDate,
                BusinessId = GetPcUserName(),
            };

            SingleObjectList.Add(Object);

            return this;
        }
        
        public JobExperienceFactory CreateMore(int num)
        {
            _teamMemberFactory.Create().Persist();
            for (int i = 0; i < num; i++)
            {
                var obj = new JobExperience()
                {
                    CompanyName = "OnnoRokom Group" + i,
                    CompanyType = 1,
                    CompanyLocation = "Karwan Bazar, Dhaka",
                    Position = "Software Engineer",
                    ExperienceArea = "ASP.NET MVC, ANGULAR.JS",
                    Duration = "3",
                    DepartmentName = "Software Development",
                    FromDate = DateTime.Now.AddYears(-(num - i)),
                    ToDate = DateTime.Now.AddYears(-(num - i + 1)),
                    BusinessId = GetPcUserName(),
                };
                obj.TeamMember = _teamMemberFactory.Object;
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region CleanUp
        public JobExperienceFactory CleanUp()
        {
            DeleteAll();
            //_teamMemberFactory.Cleanup();
            return this;
        }
        #endregion

    }

}
