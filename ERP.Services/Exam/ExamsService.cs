﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Exam;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;

using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Exam
{
    public interface IExamsService : IBaseService
    {
        #region Operational Function
        bool Save(Exams exam, string examNamePrefix,
            int numberOfExam, string examShortNamePrefix, string mcqFullMark, string writtenFullMark,
            bool isTakeAnswerSheetWhenUploadMarks, int isAcceptPartialAnswer, long[] mcqSubject,
            int mcqMaxSubCount, int[] mcqTotalQuestions, long[] writtenSubject, int writtenMaxSubCount,
            decimal[] writtenFullMarks, decimal[] mcqMarksPerQuestion, decimal[] mcqNegativeMark, decimal[] mcqPassMark,
            int[] mcqQuestionFrom, int[] mcqQuestionTo, int[] mcqIsCompulsory, decimal[] writtenPassMark,
            int[] writtenIsCompulsory, int mcqsubType, int writtensubType, out string message);

        //bool Update(Exams examExams, long examId, int isAcceptPartialAnswer, long[] mcqSubject, int mcqMaxSubCount, int[] mcqTotalQuestions, long[] writtenSubject, int writtenMaxSubCount, decimal[] writtenFullMarks, decimal[] mcqMarksPerQuestion, decimal[] mcqNegativeMark, decimal[] mcqPassMark, int[] mcqQuestionFrom, int[] mcqQuestionTo, int[] mcqIsCompulsory, decimal[] writtenPassMark, int[] writtenIsCompulsory, int mcqsubType, int writtensubType, out string message);

        void Update(UpdateExamViewModel data);
        void Delete(long examParentId);
        bool UpdateRank(Exams examOldObj, Exams examUpdateObj);

        void UpdateIndividualExam(UpdateExamViewModel data);

        #endregion

        #region Single Instances Loading Function
        Exams LoadById(long id);
        Exams GetExamById(long id);
        Exams LoadByRankNextOrPrevious(int rank, string action);
        IList<Exams> GetByExamCode(string examCode);
        Exams GetExams(string examCode, long courseId);
        ExamParent GetExamParentById(long id);
        #endregion

        #region List Loading Function
        IList<Exams> GetExamByProgramSessionAndCourse(long programId, long sessionId, long courseId);

        IList<Exams> LoadExams(List<long> examIds);
        List<Exams> GetExamList(int start, int length, string orderBy, string direction, string organization, string program, string session, string course, string name, string shortName, string code, string status, string rank);
        IList<Exams> GetExamByProgramSessionCourseAutoComplete(string query, long programId, long sessionId, long? courseId, bool? isMcq = null, bool isQuestion = false);
        IList<Exams> GetExamCodeByAutoComplete(string query, long programId, long sessionId, long[] courseId);
        IList<Exams> GetExamByProgramAndSessionAndBranchsAndCampusesAndBatches(long programId, long sessionId, long[] courseId, long[] branchId, long[] campusId, long[] batchId);
        IList<Exams> LoadExamByProgramAndSessionAndBranchsAndCampusesAndBatches(List<UserMenu> userMenus, long programId, long sessionId, long[] courseId,
            long[] branchId, long[] campusId, long[] batchId, long[] subjectIds, DateTime? dateFrom, DateTime? dateTo, bool? returnDoneExams = null);

        IList<Exams> LoadExam(string query, long courseId);
        IList<Exams> LoadExam(long courseId);
        IList<Exams> LoadExams(List<UserMenu> _userMenu, long programId, long sessionId, long[] courseId, bool returnDoneExams = false);
        IList<Exams> LoadExams(List<UserMenu> _userMenu, long programId, long sessionId, long[] courseId, long[] batchId, bool returnDoneExams = false);
        IList<Exams> LoadExams(long programId, long sessionId, long[] courseId, List<Batch> batchList, List<Branch> branchList, bool? enterProgress = null, bool? manageProgress = null, bool? attendanceGiven = null, long[] subjectIdList = null, DateTime? dateFrom = null, DateTime? dateTo = null, bool? clearExamAttendance = null, long[] userIdList = null);
        IList<Exams> LoadExams(long programId, long sessionId, List<long> courseList, DateTime dateFrom, DateTime dateTo);
        IList<Exams> LoadExam(long programId, long sessionId, long branchId, long batchId, long courseId, string examName, long[] mcqSubject, long[] writtenSubject);
        IList<ExamParent> LoadExamParentList(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, int start, int length, string orderBy, string orderDirection, string organization, string program, string session, string course, string examNamePrefix);
        #endregion

        #region Other Functions
        int GetMaximumRank(Exams e);

        int GetExamParentRowCount(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, string organization, string program, string session, string course, string examNamePrefix);

        int GetAnswerScriptCount(long id);

        int GetStudentMarksCount(long id);

        bool IsChildrenExamModified(ExamParent examParent);

        #endregion

    }

    public class ExamsService : BaseService, IExamsService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("ExamService");

        #endregion

        #region Propertise & Object Initialization

        private readonly IExamsDetailsDao _examsDetailsDao;
        private readonly IExamsDao _examsDao;
        private readonly IExamParentDao _examParentDao;
        private readonly ISubjectDao _subjectDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        private readonly IBatchDao _batchDao;
        private readonly ICourseDao _courseDao;
        private readonly IBranchDao _branchDao;
        private readonly ICommonHelper _commonHelper;

        public ExamsService(ISession session)
        {
            Session = session;
            _examsDao = new ExamsDao() { Session = session };
            _examParentDao = new ExamParentDao() { Session = session };
            _programDao = new ProgramDao() { Session = session };
            _sessionDao = new SessionDao() { Session = session };
            _courseDao = new CourseDao() { Session = session };
            _batchDao = new BatchDao() { Session = session };
            _branchDao = new BranchDao() { Session = session };
            _examsDetailsDao = new ExamsDetailsDao() { Session = session };
            _subjectDao = new SubjectDao() { Session = session };
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Function

        public bool Save(Exams exam, string examNamePrefix,
            int numberOfExam, string examShortNamePrefix, string mcqFullMark, string writtenFullMark,
            bool isTakeAnswerSheetWhenUploadMarks, int isAcceptPartialAnswer, long[] mcqSubject,
            int mcqMaxSubCount, int[] mcqTotalQuestions, long[] writtenSubject, int writtenMaxSubCount,
            decimal[] writtenFullMarks, decimal[] mcqMarksPerQuestion, decimal[] mcqNegativeMark, decimal[] mcqPassMark,
            int[] mcqQuestionFrom, int[] mcqQuestionTo, int[] mcqIsCompulsory, decimal[] writtenPassMark,
            int[] writtenIsCompulsory, int mcqsubType, int writtensubType, out string message)
        {
            ITransaction transaction = null;
            try
            {
                long programId = exam.Program.Id;
                long sessionId = exam.Session.Id;
                long branchId = exam.Branch.Id;
                long batchId = exam.Batch.Id;
                long courseId = exam.Course.Id;
                int totalUniqueset = exam.TotalUniqueSet;
                string durationHour = exam.DurationHour;
                string durationMinute = exam.DurationMinute;
                bool isBanglaVersion = exam.IsBanglaVersion;
                bool isEnglishVersion = exam.IsEnglishversion;

                #region Duplicate Check

                bool checkDuplicateName = CheckDuplicateExamPrefix(programId, sessionId, courseId, branchId, batchId,
                    examNamePrefix);
                bool checkDuplicateSubject = CheckDuplicateSubject(mcqSubject, writtenSubject);
                if (checkDuplicateName)
                {
                    message = "Duplicate name is not allowed";
                    return false;
                }

                if (!checkDuplicateSubject)
                {
                    message = "Duplicate subject is not allowed";
                    return false;
                }
                var startExamCode = 101;
                var oldExamList = _examsDao.LoadExam(courseId);
                if (oldExamList.Any())
                {
                    var lastExamCodeObj = oldExamList.OrderByDescending(x => x.Code).FirstOrDefault();
                    if (lastExamCodeObj != null)
                    {
                        var lastExamCode = lastExamCodeObj.Code;
                        startExamCode = Convert.ToInt32(lastExamCode) + 1;
                    }
                }

                #endregion

                using (transaction = Session.BeginTransaction())
                {
                    #region Initialization

                    Program program = _programDao.LoadById(programId);
                    Session session = _sessionDao.LoadById(sessionId);
                    Course course = _courseDao.LoadById(courseId);
                    Branch branch = _branchDao.LoadById(branchId);
                    Batch batch = _batchDao.LoadById(batchId);
                    //decimal mcqFullMarksDecimal =

                    #endregion

                    #region Save exam Parent

                    var examParent = new ExamParent
                    {
                        Course = course,
                        NumberOfExam = numberOfExam,
                        ExamNamePrefix = examNamePrefix,
                        ExamShortNamePrefix = examShortNamePrefix,
                        Batch = batch,
                        Branch = branch,
                        IsTakeAnswerSheetWhenUploadMarks = isTakeAnswerSheetWhenUploadMarks

                    };
                    _examParentDao.Save(examParent);

                    #endregion

                    #region Loop numberof exams and save each exam

                    for (int j = 0; j < numberOfExam; j++)
                    {
                        string examSuffix = "";
                        if ((j + 1) < 10)
                        {
                            examSuffix = "0" + (j + 1);
                        }
                        else
                        {
                            examSuffix = (j + 1).ToString();
                        }

                        #region Exam Save

                        var createExam = new Exams
                        {
                            Program = program,
                            Session = session,
                            Course = course,
                            Branch = branch,
                            Batch = batch,
                            ExamParent = examParent,
                            Name = examNamePrefix + examSuffix,
                            Code = startExamCode.ToString(CultureInfo.InvariantCulture),
                            ShortName = examShortNamePrefix + examSuffix,
                            IsTakeAnswerSheetWhenUploadMarks = isTakeAnswerSheetWhenUploadMarks,
                            TotalUniqueSet = totalUniqueset,
                            DurationHour = durationHour,
                            DurationMinute = durationMinute,
                            IsBanglaVersion = isBanglaVersion,
                            IsEnglishversion = isEnglishVersion
                            //McqFullMarks = mcqFullMark,
                            //WrittenFullMark = writtenFullMark
                        };

                        if (mcqFullMark != "") createExam.McqFullMarks = Convert.ToDecimal(mcqFullMark);
                        if (writtenFullMark != "") createExam.WrittenFullMark = Convert.ToDecimal(writtenFullMark);

                        if (mcqSubject != null)
                        {
                            createExam.IsMcq = true;
                        }

                        if (writtenSubject != null)
                        {
                            createExam.IsWritten = true;
                        }
                        if (mcqSubject == null && mcqTotalQuestions != null)
                        {
                            createExam.IsMcq = true;
                        }
                        if (writtenSubject == null && writtenFullMarks != null)
                        {
                            createExam.IsWritten = true;
                        }
                        if (createExam.IsMcq)
                        {
                            createExam.McqMaxSubjectCount = mcqMaxSubCount > 0 ? mcqMaxSubCount : 1;
                        }
                        if (createExam.IsWritten)
                        {
                            createExam.WrittenMaxSubjectCount = writtenMaxSubCount > 0 ? writtenMaxSubCount : 1;
                        }
                        createExam.Rank = _examsDao.GetMaximumRank(createExam) + 1;
                        createExam.ExamDate = DateTime.Now;
                        createExam.Status = Exams.EntityStatus.Active;
                        if (createExam.IsMcq)
                        {
                            createExam.IsAcceptPartialAnswer = isAcceptPartialAnswer == 1;
                        }

                        _examsDao.Save(createExam);

                        #endregion

                        #region Exam Details Save

                        if (mcqSubject != null)
                        {
                            for (int i = 0; i < mcqSubject.Length; i++)
                            {
                                var mcqExamsDetails = new ExamsDetails
                                {
                                    Exams = createExam,
                                    ExamType = ExamType.Mcq,
                                    SubjectType = mcqsubType,
                                    Subject = _subjectDao.LoadById(mcqSubject[i]),
                                    QuestionFrom = mcqQuestionFrom != null ? mcqQuestionFrom[i] : 1,
                                    QuestionTo = mcqQuestionTo != null ? mcqQuestionTo[i] : mcqTotalQuestions[i]
                                };

                                if (mcqTotalQuestions != null)
                                {
                                    mcqExamsDetails.TotalQuestion = mcqTotalQuestions[i];
                                }

                                if (mcqMarksPerQuestion != null)
                                {
                                    mcqExamsDetails.MarksPerQuestion = mcqMarksPerQuestion[i];
                                }

                                if (mcqNegativeMark != null)
                                {
                                    mcqExamsDetails.NegativeMark = mcqNegativeMark[i];
                                }

                                if (mcqPassMark != null)
                                {
                                    mcqExamsDetails.PassMark = mcqPassMark[i];
                                }

                                if (mcqIsCompulsory != null)
                                {
                                    mcqExamsDetails.IsCompulsary = Convert.ToBoolean(mcqIsCompulsory[i]);
                                }

                                if (mcqIsCompulsory == null && mcqSubject.Length == 1)
                                {
                                    mcqExamsDetails.IsCompulsary = true;
                                }

                                mcqExamsDetails.Status = ExamsDetails.EntityStatus.Active;
                                _examsDetailsDao.Save(mcqExamsDetails);

                            }
                        }
                        if (mcqSubject == null)
                        {
                            if (mcqTotalQuestions != null)
                            {
                                var mcqExamsDetails = new ExamsDetails
                                {
                                    Exams = createExam,
                                    ExamType = ExamType.Mcq,
                                    SubjectType = mcqsubType,
                                    QuestionFrom = 1
                                };
                                if (mcqTotalQuestions != null)
                                {
                                    mcqExamsDetails.TotalQuestion = mcqTotalQuestions[0];
                                    mcqExamsDetails.QuestionTo = mcqTotalQuestions[0];
                                }
                                if (mcqMarksPerQuestion != null)
                                {
                                    mcqExamsDetails.MarksPerQuestion = mcqMarksPerQuestion[0];
                                }
                                if (mcqNegativeMark != null)
                                {
                                    mcqExamsDetails.NegativeMark = mcqNegativeMark[0];
                                }
                                if (mcqPassMark != null)
                                {
                                    mcqExamsDetails.PassMark = mcqPassMark[0];
                                }
                                mcqExamsDetails.IsCompulsary = true;
                                mcqExamsDetails.Status = ExamsDetails.EntityStatus.Active;
                                _examsDetailsDao.Save(mcqExamsDetails);
                            }
                        }
                        if (writtenSubject != null)
                        {
                            for (int i = 0; i < writtenSubject.Length; i++)
                            {
                                var writtenExamsDetails = new ExamsDetails
                                {
                                    Exams = createExam,
                                    ExamType = ExamType.Written,
                                    SubjectType = writtensubType,
                                    Subject = _subjectDao.LoadById(writtenSubject[i])
                                };
                                if (writtenFullMarks != null)
                                {
                                    writtenExamsDetails.WrittenFullMark = writtenFullMarks[i];
                                }
                                if (writtenPassMark != null)
                                {
                                    writtenExamsDetails.PassMark = writtenPassMark[i];
                                }
                                if (writtenIsCompulsory != null)
                                {
                                    writtenExamsDetails.IsCompulsary = Convert.ToBoolean(writtenIsCompulsory[i]);
                                }
                                if (writtenSubject.Length == 1 && writtensubType == SubjectType.Single)
                                {
                                    writtenExamsDetails.IsCompulsary = true;
                                }
                                _examsDetailsDao.Save(writtenExamsDetails);

                            }
                        }

                        if (writtenSubject == null)
                        {
                            if (writtenFullMarks != null)
                            {
                                var writtenExamsDetails = new ExamsDetails
                                {
                                    Exams = createExam,
                                    ExamType = ExamType.Written,
                                    SubjectType = writtensubType,
                                    WrittenFullMark = writtenFullMarks[0]
                                };
                                if (writtenPassMark != null)
                                {
                                    writtenExamsDetails.PassMark = writtenPassMark[0];
                                }
                                writtenExamsDetails.IsCompulsary = true;
                                _examsDetailsDao.Save(writtenExamsDetails);
                            }
                        }

                        #endregion


                        startExamCode++;
                    }

                    #endregion

                    transaction.Commit();
                    message = "";
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        public void Update(UpdateExamViewModel data)
        {
            ITransaction transaction = null;
            try
            {
                var totalUniqueSet = data.TotalUniqueSet;
                var durationHour = data.DurationHour.ToString();
                var durationMinute = data.DurationMinute.ToString();
                var isBanglaVersion = data.IsBanglaVersion;
                var isEnglishversion = data.IsEnglishversion;
                var examParent = _examParentDao.LoadById(data.ExamParentId);
                bool isChildrenExamModified = IsChildrenExamModified(examParent);
                if (isChildrenExamModified)
                {
                    throw new DependencyException("One/more child exam modified already.");
                }
                if (examParent == null)
                {
                    throw new MessageException("Invalid Exam found");
                }
                if (data.NumberOfExam < 0)
                {
                    throw new MessageException("Number of exam must be a positive integer.");
                }
                if (String.IsNullOrEmpty(data.ExamNamePrefix))
                {
                    throw new MessageException("Exam name prefix is required.");
                }

                if (String.IsNullOrEmpty(data.ExamShortNamePrefix))
                {
                    throw new MessageException("Exam Short name prefix is required.");
                }

                var examUnderThisParent = new Exams();
                if (examParent.Examses.Any())
                {
                    examUnderThisParent = examParent.Examses.OrderByDescending(x => x.Id).FirstOrDefault();
                }

                if (examUnderThisParent == null)
                {
                    throw new MessageException("Exam List is empty.");
                }


                long prevProgramId = 0;
                long prevSessionId = 0;
                long prevCourseId = 0;
                long prevBranchId = 0;
                long prevBatchId = 0;

                if (examUnderThisParent.Program != null) prevProgramId = examUnderThisParent.Program.Id;
                if (examUnderThisParent.Session != null) prevSessionId = examUnderThisParent.Session.Id;
                if (examUnderThisParent.Course != null) prevCourseId = examUnderThisParent.Course.Id;
                if (examUnderThisParent.Branch != null) prevBranchId = examUnderThisParent.Branch.Id;
                if (examUnderThisParent.Batch != null) prevBatchId = examUnderThisParent.Batch.Id;


                bool checkDuplicateName = CheckDuplicateExamPrefix(prevProgramId, prevSessionId,
                    prevCourseId, prevBranchId, prevBatchId, data.ExamNamePrefix, examParent.Id);

                if (checkDuplicateName)
                {
                    throw new MessageException("Duplicate name is not allowed.");
                }
                long[] mcqSubject = null;
                long[] writtenSubject = null;

                IList<ExamsDetails> mcqExamsDetailses = new List<ExamsDetails>();
                IList<ExamsDetails> writtenExamsDetailses = new List<ExamsDetails>();

                if (examUnderThisParent.IsMcq)
                {
                    mcqExamsDetailses = examUnderThisParent.ExamDetails.Where(x => x.ExamType == ExamType.Mcq).ToList();
                    if (mcqExamsDetailses.Count > 1)
                    {
                        mcqSubject = new long[mcqExamsDetailses.Count];
                    }
                }
                if (examUnderThisParent.IsWritten)
                {
                    writtenExamsDetailses =
                        examUnderThisParent.ExamDetails.Where(x => x.ExamType == ExamType.Written).ToList();
                    if (writtenExamsDetailses.Count > 1)
                    {
                        writtenSubject = new long[writtenExamsDetailses.Count];
                    }
                }

                int msi = 0;
                int wsi = 0;
                if (data.ExamDetails.Any())
                {
                    foreach (var d in data.ExamDetails)
                    {
                        if (d.ExamType == ExamType.Mcq && d.SubjectType == SubjectType.Multiple)
                        {
                            mcqSubject[msi++] = d.SubjectId;
                        }
                        else if (d.ExamType == ExamType.Written && d.SubjectType == SubjectType.Multiple)
                        {
                            writtenSubject[wsi++] = d.SubjectId;
                        }
                    }

                }
                bool checkDuplicateSubject = CheckDuplicateSubject(mcqSubject, writtenSubject);


                if (!checkDuplicateSubject)
                {
                    throw new MessageException("Duplicate subject is not allowed.");
                }
                var examesOld = examParent.Examses.OrderBy(x => x.Id).ToList();

                int answerScript = 0;
                int studentMarks = 0;

                answerScript = _examParentDao.GetAnswerScriptCount(examParent.Id);
                studentMarks = _examParentDao.GetStudentMarksCount(examParent.Id);

                using (transaction = Session.BeginTransaction())
                {
                    string examSuffix = "";
                    if (examesOld.Count > data.NumberOfExam)
                    {
                        #region Decrease No Of Exam

                        string lastUploadedStudentMarksExamName = "";
                        //string lastUploadedStudentMarksExamCode = "";

                        string lastUploadedAnswerSheetExamName = "";
                        //string lastUploadedAnswerSheetExamCode = "";



                        foreach (var e in examesOld)
                        {
                            foreach (var ed in e.ExamDetails)
                            {
                                if (ed.ExamsStudentMarks.Any())
                                {
                                    lastUploadedStudentMarksExamName = e.Name;
                                    //lastUploadedStudentMarksExamCode = e.Code;
                                    break;
                                }
                            }
                            foreach (var ed in e.ExamDetails)
                            {
                                if (ed.ExamsStudentAnswerScripts.Any())
                                {
                                    lastUploadedAnswerSheetExamName = e.Name;
                                    //lastUploadedAnswerSheetExamCode = e.Code;
                                    break;
                                }
                            }
                        }



                        int highestNotEditableExamStudentMarks = 0;
                        if (!String.IsNullOrEmpty(lastUploadedStudentMarksExamName))
                        {
                            highestNotEditableExamStudentMarks =
                                Convert.ToInt32(lastUploadedStudentMarksExamName.Replace(examParent.ExamNamePrefix, ""));

                            // int index = lastUploadedStudentMarksExamName.IndexOf(examParent.ExamNamePrefix, System.StringComparison.Ordinal);
                            //string cleanPath = (index < 0) ? lastUploadedStudentMarksExamName : lastUploadedStudentMarksExamName.Remove(index, examParent.ExamNamePrefix.Length);
                        }

                        int highestNotEditableExamAnswer = 0;
                        if (!String.IsNullOrEmpty(lastUploadedAnswerSheetExamName))
                        {
                            highestNotEditableExamAnswer =
                                Convert.ToInt32(lastUploadedAnswerSheetExamName.Replace(examParent.ExamNamePrefix, ""));

                            // int index = lastUploadedStudentMarksExamName.IndexOf(examParent.ExamNamePrefix, System.StringComparison.Ordinal);
                            //string cleanPath = (index < 0) ? lastUploadedStudentMarksExamName : lastUploadedStudentMarksExamName.Remove(index, examParent.ExamNamePrefix.Length);
                        }

                        if (highestNotEditableExamStudentMarks >= data.NumberOfExam ||
                            highestNotEditableExamAnswer >= data.NumberOfExam)
                        {
                            int h = (highestNotEditableExamStudentMarks > highestNotEditableExamAnswer)
                                ? highestNotEditableExamStudentMarks
                                : highestNotEditableExamAnswer;
                            throw new DependencyException(
                                "This Exams has one or more dependant data. Please change your number of exam gerater than : " +
                                h);
                        }


                        examParent.ExamNamePrefix = data.ExamNamePrefix;
                        examParent.ExamShortNamePrefix = data.ExamShortNamePrefix;
                        examParent.NumberOfExam = data.NumberOfExam;
                        examParent.IsTakeAnswerSheetWhenUploadMarks = data.IsTakeAnswerSheetWhenUploadMarks;
                        int remIndex = -1;
                        int j = 0;
                        int total = examParent.Examses.Count;

                        foreach (var e in examParent.Examses)
                        {
                            if ((j + 1) > data.NumberOfExam)
                            {
                                remIndex = j;
                                break;
                            }
                            else
                            {
                                if ((j + 1) < 10)
                                {
                                    examSuffix = "0" + (j + 1);
                                }
                                else
                                {
                                    examSuffix = (j + 1).ToString();
                                }
                                e.IsAcceptPartialAnswer = data.IsAcceptPartialAnswer;
                                e.IsTakeAnswerSheetWhenUploadMarks = data.IsTakeAnswerSheetWhenUploadMarks;
                                e.Name = data.ExamNamePrefix + examSuffix;
                                e.ShortName = data.ExamShortNamePrefix + examSuffix;
                                var examDetailsNewList = data.ExamDetails;
                                if (e.IsMcq)
                                {
                                    e.McqMaxSubjectCount = data.McqMaxSubjectCount;
                                    e.McqFullMarks = data.McqFullMarks;
                                    foreach (var ed in e.ExamDetails.Where(x => x.ExamType == ExamType.Mcq))
                                    {
                                        var subjectType = ed.SubjectType;
                                        var examType = ed.ExamType;
                                        if (subjectType == SubjectType.Combined)
                                        {
                                            var examDetailsNew =
                                                examDetailsNewList.FirstOrDefault(
                                                    x => x.SubjectId == 0 && x.ExamType == examType);
                                            ed.TotalQuestion = examDetailsNew.TotalQuestion;
                                            ed.MarksPerQuestion = examDetailsNew.MarksPerQuestion;
                                            ed.NegativeMark = examDetailsNew.NegativeMark;
                                            ed.PassMark = examDetailsNew.PassMark;
                                        }
                                        else if (subjectType == SubjectType.Single)
                                        {
                                            var subjectId = ed.Subject.Id;
                                            var examDetailsNew =
                                                examDetailsNewList.FirstOrDefault(
                                                    x => x.SubjectId == subjectId && x.ExamType == examType);
                                            ed.Subject = _subjectDao.LoadById(examDetailsNew.SubjectId);
                                            //set question to from
                                            ed.QuestionFrom = 1;
                                            ed.QuestionTo = examDetailsNew.TotalQuestion;

                                            ed.TotalQuestion = examDetailsNew.TotalQuestion;
                                            ed.MarksPerQuestion = examDetailsNew.MarksPerQuestion;
                                            ed.NegativeMark = examDetailsNew.NegativeMark;
                                            ed.PassMark = examDetailsNew.PassMark;
                                        }
                                        else if (subjectType == SubjectType.Multiple)
                                        {
                                            var subjectId = ed.Subject.Id;
                                            var examDetailsNew =
                                                examDetailsNewList.FirstOrDefault(
                                                    x => x.SubjectId == subjectId && x.ExamType == examType);
                                            ed.Subject = _subjectDao.LoadById(examDetailsNew.SubjectId);
                                            ed.QuestionFrom = examDetailsNew.QuestionFrom;
                                            ed.QuestionTo = examDetailsNew.QuestionTo;
                                            ed.TotalQuestion = examDetailsNew.TotalQuestion;
                                            ed.MarksPerQuestion = examDetailsNew.MarksPerQuestion;
                                            ed.NegativeMark = examDetailsNew.NegativeMark;
                                            ed.PassMark = examDetailsNew.PassMark;
                                            ed.IsCompulsary = examDetailsNew.IsCompulsary;
                                        }
                                    }
                                }
                                if (e.IsWritten)
                                {
                                    e.WrittenMaxSubjectCount = data.WrittenMaxSubjectCount;
                                    e.WrittenFullMark = data.WrittenFullMark;
                                    foreach (var ed in e.ExamDetails.Where(x => x.ExamType == ExamType.Written))
                                    {
                                        var subjectType = ed.SubjectType;
                                        var examType = ed.ExamType;
                                        if (subjectType == SubjectType.Combined)
                                        {
                                            var examDetailsNew =
                                                examDetailsNewList.FirstOrDefault(
                                                    x => x.SubjectId == 0 && x.ExamType == examType);
                                            ed.WrittenFullMark = examDetailsNew.WrittenFullMark;
                                            ed.PassMark = examDetailsNew.PassMark;
                                        }
                                        else if (subjectType == SubjectType.Single)
                                        {
                                            var subjectId = ed.Subject.Id;
                                            var examDetailsNew =
                                                examDetailsNewList.FirstOrDefault(
                                                    x => x.SubjectId == subjectId && x.ExamType == examType);
                                            ed.Subject = _subjectDao.LoadById(examDetailsNew.SubjectId);
                                            ed.WrittenFullMark = examDetailsNew.WrittenFullMark;
                                            ed.PassMark = examDetailsNew.PassMark;
                                        }
                                        else if (subjectType == SubjectType.Multiple)
                                        {
                                            var subjectId = ed.Subject.Id;
                                            var examDetailsNew =
                                                examDetailsNewList.FirstOrDefault(
                                                    x => x.SubjectId == subjectId && x.ExamType == examType);
                                            ed.Subject = _subjectDao.LoadById(examDetailsNew.SubjectId);
                                            ed.WrittenFullMark = examDetailsNew.WrittenFullMark;
                                            ed.PassMark = examDetailsNew.PassMark;
                                            ed.IsCompulsary = examDetailsNew.IsCompulsary;
                                        }
                                    }
                                }
                            }
                            j++;
                        }

                        if (remIndex != -1)
                        {
                            for (int i = total - 1; i >= remIndex; i--)
                            {
                                examParent.Examses.RemoveAt(i);
                            }
                        }
                        _examParentDao.SaveOrUpdate(examParent);

                        #endregion
                    }
                    else
                    {
                        #region Increase No of exam

                        examParent.ExamNamePrefix = data.ExamNamePrefix;
                        examParent.ExamShortNamePrefix = data.ExamShortNamePrefix;
                        examParent.NumberOfExam = data.NumberOfExam;
                        examParent.IsTakeAnswerSheetWhenUploadMarks = data.IsTakeAnswerSheetWhenUploadMarks;
                        //_examParentDao.Update(examParent);
                        int j = 0;
                        foreach (var e in examParent.Examses)
                        {
                            //examSuffix = "";
                            if ((j + 1) < 10)
                            {
                                examSuffix = "0" + (j + 1);
                            }
                            else
                            {
                                examSuffix = (j + 1).ToString();
                            }

                            e.IsAcceptPartialAnswer = data.IsAcceptPartialAnswer;
                            e.IsTakeAnswerSheetWhenUploadMarks = data.IsTakeAnswerSheetWhenUploadMarks;
                            e.Name = data.ExamNamePrefix + examSuffix;
                            e.ShortName = data.ExamShortNamePrefix + examSuffix;
                            e.TotalUniqueSet = totalUniqueSet;
                            e.DurationHour = durationHour;
                            e.DurationMinute = durationMinute;
                            e.IsBanglaVersion = isBanglaVersion;
                            e.IsEnglishversion = isEnglishversion;
                            var examDetailsNewList = data.ExamDetails;
                            if (e.IsMcq)
                            {
                                e.McqMaxSubjectCount = data.McqMaxSubjectCount;
                                e.McqFullMarks = data.McqFullMarks;
                                foreach (var ed in e.ExamDetails.Where(x => x.ExamType == ExamType.Mcq))
                                {
                                    var subjectType = ed.SubjectType;
                                    var examType = ed.ExamType;
                                    if (subjectType == SubjectType.Combined)
                                    {
                                        var examDetailsNew =
                                            examDetailsNewList.FirstOrDefault(
                                                x => x.SubjectId == 0 && x.ExamType == examType);
                                        ed.QuestionFrom = 1;
                                        ed.QuestionTo = examDetailsNew.TotalQuestion;
                                        ed.TotalQuestion = examDetailsNew.TotalQuestion;
                                        ed.MarksPerQuestion = examDetailsNew.MarksPerQuestion;
                                        ed.NegativeMark = examDetailsNew.NegativeMark;
                                        ed.PassMark = examDetailsNew.PassMark;
                                    }
                                    else if (subjectType == SubjectType.Single)
                                    {
                                        var subjectId = ed.Subject.Id;
                                        var examDetailsNew =
                                            examDetailsNewList.FirstOrDefault(
                                                x => x.SubjectId == subjectId && x.ExamType == examType);
                                        ed.Subject = _subjectDao.LoadById(examDetailsNew.SubjectId);
                                        //set question to from 
                                        ed.QuestionFrom = 1;
                                        ed.QuestionTo = examDetailsNew.TotalQuestion;

                                        ed.TotalQuestion = examDetailsNew.TotalQuestion;
                                        ed.MarksPerQuestion = examDetailsNew.MarksPerQuestion;
                                        ed.NegativeMark = examDetailsNew.NegativeMark;
                                        ed.PassMark = examDetailsNew.PassMark;
                                    }
                                    else if (subjectType == SubjectType.Multiple)
                                    {
                                        var subjectId = ed.Subject.Id;
                                        var examDetailsNew =
                                            examDetailsNewList.FirstOrDefault(
                                                x => x.SubjectId == subjectId && x.ExamType == examType);
                                        ed.Subject = _subjectDao.LoadById(examDetailsNew.SubjectId);
                                        ed.QuestionFrom = examDetailsNew.QuestionFrom;
                                        ed.QuestionTo = examDetailsNew.QuestionTo;
                                        ed.TotalQuestion = examDetailsNew.TotalQuestion;
                                        ed.MarksPerQuestion = examDetailsNew.MarksPerQuestion;
                                        ed.NegativeMark = examDetailsNew.NegativeMark;
                                        ed.PassMark = examDetailsNew.PassMark;
                                        ed.IsCompulsary = examDetailsNew.IsCompulsary;
                                    }
                                }
                            }
                            if (e.IsWritten)
                            {
                                e.WrittenMaxSubjectCount = data.WrittenMaxSubjectCount;
                                e.WrittenFullMark = data.WrittenFullMark;
                                foreach (var ed in e.ExamDetails.Where(x => x.ExamType == ExamType.Written))
                                {
                                    var subjectType = ed.SubjectType;
                                    var examType = ed.ExamType;
                                    if (subjectType == SubjectType.Combined)
                                    {
                                        var examDetailsNew =
                                            examDetailsNewList.FirstOrDefault(
                                                x => x.SubjectId == 0 && x.ExamType == examType);
                                        ed.WrittenFullMark = examDetailsNew.WrittenFullMark;
                                        ed.PassMark = examDetailsNew.PassMark;
                                    }
                                    else if (subjectType == SubjectType.Single)
                                    {
                                        var subjectId = ed.Subject.Id;
                                        var examDetailsNew =
                                            examDetailsNewList.FirstOrDefault(
                                                x => x.SubjectId == subjectId && x.ExamType == examType);
                                        ed.Subject = _subjectDao.LoadById(examDetailsNew.SubjectId);
                                        ed.WrittenFullMark = examDetailsNew.WrittenFullMark;
                                        ed.PassMark = examDetailsNew.PassMark;
                                    }
                                    else if (subjectType == SubjectType.Multiple)
                                    {
                                        var subjectId = ed.Subject.Id;
                                        var examDetailsNew =
                                            examDetailsNewList.FirstOrDefault(
                                                x => x.SubjectId == subjectId && x.ExamType == examType);
                                        ed.Subject = _subjectDao.LoadById(examDetailsNew.SubjectId);
                                        ed.WrittenFullMark = examDetailsNew.WrittenFullMark;
                                        ed.PassMark = examDetailsNew.PassMark;
                                        ed.IsCompulsary = examDetailsNew.IsCompulsary;
                                    }
                                }
                            }
                            j++;
                        }
                        _examParentDao.SaveOrUpdate(examParent);
                        int startExamCode = 101;
                        var oldExamList = _examsDao.LoadExam(examParent.Course.Id);
                        if (oldExamList.Any())
                        {
                            var lastExamCodeObj = oldExamList.OrderByDescending(x => x.Code).FirstOrDefault();
                            if (lastExamCodeObj != null)
                            {
                                var lastExamCode = lastExamCodeObj.Code;
                                startExamCode = Convert.ToInt32(lastExamCode) + 1;
                            }
                        }

                        for (int i = (examParent.Examses.Count + 1); i <= data.NumberOfExam; i++)
                        {
                            if ((j + 1) < 10)
                            {
                                examSuffix = "0" + (j++ + 1);
                            }
                            else
                            {
                                examSuffix = (j++ + 1).ToString();
                            }
                            var createExam = new Exams
                            {
                                Program = examUnderThisParent.Program,
                                Session = examUnderThisParent.Session,
                                Course = examUnderThisParent.Course,
                                Branch = examParent.Branch,
                                Batch = examParent.Batch,
                                ExamParent = examParent,
                                Name = examParent.ExamNamePrefix + examSuffix,
                                Code = startExamCode.ToString(CultureInfo.InvariantCulture),
                                ShortName = examParent.ExamShortNamePrefix + examSuffix,
                                IsTakeAnswerSheetWhenUploadMarks = data.IsTakeAnswerSheetWhenUploadMarks,
                                ExamDate = DateTime.Now
                                //McqFullMarks = mcqFullMark,
                                //WrittenFullMark = writtenFullMark
                            };
                            createExam.Rank = _examsDao.GetMaximumRank(createExam) + 1;
                            if (data.IsMcq)
                            {
                                createExam.McqFullMarks = data.McqFullMarks;
                                createExam.IsMcq = data.IsMcq;
                                createExam.McqMaxSubjectCount = data.McqMaxSubjectCount;
                                createExam.IsAcceptPartialAnswer = data.IsAcceptPartialAnswer;
                            }
                            if (data.IsWritten)
                            {
                                createExam.WrittenFullMark = data.WrittenFullMark;
                                createExam.IsWritten = data.IsWritten;
                                createExam.WrittenMaxSubjectCount = data.WrittenMaxSubjectCount;
                                createExam.IsAcceptPartialAnswer = data.IsAcceptPartialAnswer;

                            }
                            createExam.TotalUniqueSet = totalUniqueSet;
                            createExam.DurationHour = durationHour;
                            createExam.DurationMinute = durationMinute;
                            createExam.IsBanglaVersion = isBanglaVersion;
                            createExam.IsEnglishversion = isEnglishversion;

                            _examsDao.Save(createExam);
                            if (data.IsMcq)
                            {
                                var tempMcqExamDetailsList =
                                    data.ExamDetails.Where(s => s.ExamType == ExamType.Mcq).ToList();
                                foreach (var tmed in tempMcqExamDetailsList)
                                {
                                    var mcqExamsDetails = new ExamsDetails
                                    {
                                        Exams = createExam,
                                        ExamType = ExamType.Mcq,
                                        SubjectType = tmed.SubjectType
                                        // QuestionFrom = tmed.QuestionFrom
                                    };

                                    if (tmed.SubjectType == SubjectType.Combined)
                                    {
                                        mcqExamsDetails.TotalQuestion = tmed.TotalQuestion;
                                        mcqExamsDetails.MarksPerQuestion = tmed.MarksPerQuestion;
                                        mcqExamsDetails.NegativeMark = tmed.NegativeMark;
                                        mcqExamsDetails.PassMark = tmed.PassMark;
                                        mcqExamsDetails.IsCompulsary = true;
                                    }
                                    else if (tmed.SubjectType == SubjectType.Single)
                                    {
                                        var subjectId = tmed.SubjectId;
                                        mcqExamsDetails.Subject = _subjectDao.LoadById(subjectId);
                                        mcqExamsDetails.TotalQuestion = tmed.TotalQuestion;
                                        mcqExamsDetails.MarksPerQuestion = tmed.MarksPerQuestion;
                                        mcqExamsDetails.NegativeMark = tmed.NegativeMark;
                                        mcqExamsDetails.PassMark = tmed.PassMark;
                                        mcqExamsDetails.IsCompulsary = true;
                                    }
                                    else if (tmed.SubjectType == SubjectType.Multiple)
                                    {
                                        var subjectId = tmed.SubjectId;
                                        mcqExamsDetails.Subject = _subjectDao.LoadById(tmed.SubjectId);
                                        mcqExamsDetails.QuestionFrom = tmed.QuestionFrom;
                                        mcqExamsDetails.QuestionTo = tmed.QuestionTo;
                                        mcqExamsDetails.TotalQuestion = tmed.TotalQuestion;
                                        mcqExamsDetails.MarksPerQuestion = tmed.MarksPerQuestion;
                                        mcqExamsDetails.NegativeMark = tmed.NegativeMark;
                                        mcqExamsDetails.PassMark = tmed.PassMark;
                                        mcqExamsDetails.IsCompulsary = tmed.IsCompulsary;
                                    }
                                    _examsDetailsDao.Save(mcqExamsDetails);
                                }
                            }
                            if (data.IsWritten)
                            {
                                var tempWrittenExamDetailsList =
                                    data.ExamDetails.Where(s => s.ExamType == ExamType.Written).ToList();
                                foreach (var twed in tempWrittenExamDetailsList)
                                {
                                    var writtenExamsDetails = new ExamsDetails
                                    {
                                        Exams = createExam,
                                        ExamType = ExamType.Written,
                                        SubjectType = twed.SubjectType,
                                    };

                                    if (twed.SubjectType == SubjectType.Combined)
                                    {
                                        writtenExamsDetails.WrittenFullMark = twed.WrittenFullMark;
                                        writtenExamsDetails.PassMark = twed.PassMark;
                                        writtenExamsDetails.IsCompulsary = true;
                                    }
                                    else if (twed.SubjectType == SubjectType.Single)
                                    {
                                        var subjectId = twed.SubjectId;
                                        writtenExamsDetails.Subject = _subjectDao.LoadById(subjectId);
                                        writtenExamsDetails.WrittenFullMark = twed.WrittenFullMark;
                                        writtenExamsDetails.PassMark = twed.PassMark;
                                        writtenExamsDetails.IsCompulsary = true;
                                    }
                                    else if (twed.SubjectType == SubjectType.Multiple)
                                    {
                                        var subjectId = twed.SubjectId;
                                        writtenExamsDetails.Subject = _subjectDao.LoadById(subjectId);
                                        writtenExamsDetails.WrittenFullMark = twed.WrittenFullMark;
                                        writtenExamsDetails.PassMark = twed.PassMark;
                                        writtenExamsDetails.IsCompulsary = twed.IsCompulsary;
                                    }

                                    _examsDetailsDao.Save(writtenExamsDetails);
                                }
                            }

                            startExamCode++;
                        }

                        #endregion
                    }
                    transaction.Commit();
                }
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                throw new DependencyException(ex.Message);
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                throw new MessageException(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        public void UpdateIndividualExam(UpdateExamViewModel data)
        {
            ITransaction transaction = null;
            try
            {
                var durationHour = data.DurationHour.ToString();
                var durationMinute = data.DurationMinute.ToString();
                var isBanglaVersion = data.IsBanglaVersion;
                var isEnglishversion = data.IsEnglishversion;
                var examName = data.ExamName;
                var exam = _examsDao.LoadById(data.ExamId);
                bool checkDuplicateIndividualExamName = CheckDuplicateIndividualExamName(exam.ExamParent.Id, exam.Id,
                    examName);
                if (checkDuplicateIndividualExamName)
                {
                    throw new MessageException("Duplicate name is not allowed.");
                }
                exam.Name = examName;
                exam.DurationHour = durationHour;
                exam.DurationMinute = durationMinute;
                exam.IsBanglaVersion = isBanglaVersion;
                exam.IsEnglishversion = isEnglishversion;
                using (transaction = Session.BeginTransaction())
                {
                    _examsDao.Update(exam);
                    transaction.Commit();
                }
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                throw new DependencyException(ex.Message);
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                throw new MessageException(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }

        private bool CheckDuplicateIndividualExamName(long parentExamId, long examId, string examName)
        {
            bool isDuplicate;
            try
            {
                isDuplicate = _examsDao.CheckDuplicateIndividualExamName(parentExamId, examId, examName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isDuplicate;
        }

        private bool CheckDuplicateExamPrefix(long programId, long sessionId, long courseId, long branchId, long batchId,
            string examNamePrefix, long id = 0)
        {
            var examParent = _examParentDao.GetExamParent(programId, sessionId, courseId, branchId, batchId,
                examNamePrefix, id);
            if (examParent != null)
            {
                return true;
            }
            return false;
        }

        public void Delete(long examParentId)
        {
            ITransaction transaction = null;
            try
            {
                int answerScript = 0;
                int studentMarks = 0;
                var examParent = _examParentDao.LoadById(examParentId);
                answerScript = _examParentDao.GetAnswerScriptCount(examParent.Id);
                studentMarks = _examParentDao.GetStudentMarksCount(examParent.Id);
                if (answerScript > 0)
                {
                    throw new DependencyException("This exam has Answer Script.");
                }
                if (studentMarks > 0)
                {
                    throw new DependencyException("This exam has one or more Student Marks.");
                }
                using (transaction = Session.BeginTransaction())
                {
                    examParent.Status = ExamParent.EntityStatus.Delete;
                    if (examParent.Examses.Any())
                    {
                        foreach (var e in examParent.Examses)
                        {
                            e.Status = Exams.EntityStatus.Delete;
                        }
                    }
                    _examParentDao.SaveOrUpdate(examParent);
                    transaction.Commit();
                }

            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                throw new DependencyException(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                transaction.Rollback();
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        //public bool Delete(Exams deleteExam)
        //{
        //    bool deleteSuccess = true;
        //    try
        //    {
        //        var examDetails = deleteExam.ExamDetails.ToList();
        //        bool hasStudentMarks = false;
        //        bool hasStudentAnswerScript = false;
        //        foreach (var examsDetailse in examDetails)
        //        {
        //            if (examsDetailse.ExamsStudentMarks.Count > 0)
        //            {
        //                hasStudentMarks = true;
        //                break;

        //            }
        //        }

        //        foreach (var examsDetailse in examDetails)
        //        {
        //            if (examsDetailse.ExamsStudentAnswerScripts.Count > 0)
        //            {
        //                hasStudentAnswerScript = true;
        //                break;

        //            }
        //        }

        //        if (hasStudentMarks)
        //        {
        //            throw new DependencyException("This exam has one or more Student Marks.");

        //        }
        //        else if (hasStudentAnswerScript)
        //        {
        //            throw new DependencyException("This exam has Answer Script.");
        //        }
        //        else
        //        {
        //            using (var trans = Session.BeginTransaction())
        //            {
        //                try
        //                {
        //                    _examsDao.Update(deleteExam);
        //                    trans.Commit();

        //                }
        //                catch (Exception e)
        //                {
        //                    trans.Rollback();
        //                    deleteSuccess = false;
        //                }
        //            }
        //            // }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw ex;
        //    }
        //    return deleteSuccess;
        //}



        public bool UpdateRank(Exams referrerOldObj, Exams referrerUpdateObj)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        _examsDao.Update(referrerOldObj);
                        _examsDao.Update(referrerUpdateObj);
                        trans.Commit();
                        return true;
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public Exams LoadById(long id)
        {
            try
            {
                return _examsDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }


        public Exams GetExamById(long id)
        {
            try
            {
                return _examsDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public Exams LoadByRankNextOrPrevious(int rank, string action)
        {
            try
            {
                return _examsDao.LoadByRankDirection(rank, action);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Exams> GetByExamCode(string examCode)
        {
            IList<Exams> exams = new List<Exams>();
            try
            {
                exams = _examsDao.GetByExamCode(examCode);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;

            }
            return exams;
        }

        public Exams GetExams(string examCode, long courseId)
        {
            try
            {
                return _examsDao.GetExams(examCode, courseId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;

            }
        }

        public ExamParent GetExamParentById(long id)
        {
            try
            {
                return _examParentDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function

        public IList<Exams> LoadExams(List<long> examIds)
        {
            return _examsDao.LoadExams(examIds);
        }

        public IList<Exams> GetExamByProgramSessionAndCourse(long programId, long sessionId, long courseId)
        {
            try
            {
                return _examsDao.GetExamByProgramSessionAndCourse(programId, sessionId, courseId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public List<Exams> GetExamList(int start, int length, string orderBy, string direction, string organization,
            string program,
            string session,
            string course, string name, string shortName, string code, string status, string rank)
        {

            try
            {
                return _examsDao.GetExamList(start, length, orderBy, direction, organization, program, session, course,
                    name, shortName, code, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Exams> GetExamByProgramSessionCourseAutoComplete(string query, long programId, long sessionId,
            long? courseId, bool? isMcq = null, bool isQuestion = false)
        {
            try
            {
                return _examsDao.GetExamByProgramSessionCourseAutoComplete(query, programId, sessionId, courseId, isMcq,
                    isQuestion);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Exams> GetExamCodeByAutoComplete(string query, long programId, long sessionId, long[] courseId)
        {
            try
            {
                return _examsDao.GetExamCodeByAutoComplete(query, programId, sessionId, courseId);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Exams> LoadExamByProgramAndSessionAndBranchsAndCampusesAndBatches(List<UserMenu> userMenus,
            long programId, long sessionId, long[] courseId,
            long[] branchId, long[] campusId, long[] batchId, long[] subjectIds, DateTime? dateFrom, DateTime? dateTo,
            bool? returnDoneExams = null)
        {
            try
            {
                List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(userMenus, null,
                    CommonHelper.ConvertSelectedAllIdList(branchId.ToList()), _commonHelper.ConvertIdToList(programId));
                List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenus, null,
                    _commonHelper.ConvertIdToList(programId), CommonHelper.ConvertSelectedAllIdList(branchId.ToList()));
                return
                    _examsDao.LoadExamByProgramAndSessionAndBranchsAndCampusesAndBatches(authorizedProgramIdList,
                            sessionId, courseId.ToList(), authorizedBranchIdList,
                            campusId.ToList(), batchId.ToList(), subjectIds, dateFrom, dateTo, returnDoneExams)
                        .OrderBy(x => x.Name.Trim())
                        .ToList();
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;
            }
        }



        public IList<Exams> GetExamByProgramAndSessionAndBranchsAndCampusesAndBatches(long programId, long sessionId,
            long[] courseId, long[] branchId,
            long[] campusId, long[] batchId)
        {
            try
            {

                return _examsDao.GetExamByProgramAndSessionAndBranchsAndCampusesAndBatches(programId, sessionId,
                    courseId, branchId,
                    campusId, batchId);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Exams> LoadExam(string query, long courseId)
        {
            try
            {
                return _examsDao.LoadExam(query, courseId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Exams> LoadExam(long courseId)
        {
            try
            {
                return _examsDao.LoadExam(courseId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Exams> LoadExams(List<UserMenu> _userMenu, long programId, long sessionId, long[] courseId,
            bool returnDoneExams = false)
        {
            //Session.QueryOver<Exams>().Where(x=>x.Program.Id==progra)
            List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(_userMenu, null, null, null);
            var examList =
                Session.QueryOver<Exams>()
                    .Where(
                        x =>
                            x.Status == Exams.EntityStatus.Active &&
                            x.Program.Id.IsIn(authorizedProgramIdList.ToArray()) && x.Session.Id == sessionId &&
                            x.Course.Id.IsIn(courseId));
            var examIdsInCourseProgress =
                Session.QueryOver<ExamProgress>()
                    .Where(x => x.Status == 1)
                    .Select(x => x.Exam.Id)
                    .List<long>()
                    .Distinct()
                    .ToArray();
            if (returnDoneExams)
            {
                examList = examList.Where(x => x.Id.IsIn(examIdsInCourseProgress));
            }
            //.List<Exams>();
            return examList.List<Exams>().OrderBy(x => x.Name.Trim()).ToList();
            ;
        }

        public IList<Exams> LoadExams(List<UserMenu> _userMenu, long programId, long sessionId, long[] courseId,
            long[] batchId, bool returnDoneExams = false)
        {
            List<long> authorizedProgramIdList = AuthHelper.LoadProgramIdList(_userMenu, null, null, null);
            var examList = new List<Exams>();

            var exams = Session.QueryOver<Exams>()
                .Where(
                    x =>
                        x.Program.Id.IsIn(authorizedProgramIdList.ToArray()) && x.Session.Id == sessionId &&
                        x.Program.Id == programId &&
                        x.Course.Id.IsIn(courseId));


            var examIdsInCourseProgress = new List<long>();
            if (!batchId.Contains(0))
            {
                examIdsInCourseProgress = Session.QueryOver<ExamProgress>()
                    .Where(x => x.Status == 1)
                    .Where(x => x.Batch.Id.IsIn(batchId))
                    .Select(x => x.Exam.Id)
                    .List<long>()
                    .Distinct().ToList();

            }
            else
            {
                examIdsInCourseProgress = Session.QueryOver<ExamProgress>()
                    .Where(x => x.Status == 1)
                    .Select(x => x.Exam.Id)
                    .List<long>()
                    .Distinct().ToList();
            }
            if (returnDoneExams)
            {
                examList = exams.Where(x => x.Id.IsIn(examIdsInCourseProgress)).List<Exams>().ToList();
            }
            else
            {
                examList = exams.Where(x => !x.Id.IsIn(examIdsInCourseProgress)).List<Exams>().ToList();
            }
            //.List<Exams>();
            return examList.ToList();
        }


        public IList<Exams> LoadExams(long programId, long sessionId, long[] courseId, List<Batch> batchList,
            List<Branch> branchList, bool? enterProgress = null, bool? manageProgress = null,
            bool? attendanceGiven = null, long[] subjectIdList = null, DateTime? dateFrom = null,
            DateTime? dateTo = null, bool? clearExamAttendance = null, long[] userIdList = null)
        {
            return _examsDao.LoadExams(programId, sessionId, courseId, batchList, branchList, enterProgress,
                manageProgress, attendanceGiven, subjectIdList, dateFrom, dateTo, clearExamAttendance, userIdList);
        }

        public IList<Exams> LoadExams(long programId, long sessionId, List<long> courseList, DateTime dateFrom,
            DateTime dateTo)
        {
            return
                _examsDao.LoadExams(programId, sessionId, courseList, dateFrom, dateTo)
                    .OrderBy(x => x.Name.Trim())
                    .ToList();
        }




        public IList<Exams> LoadExam(long programId, long sessionId, long branchId, long batchId, long courseId,
            string examName,
            long[] mcqSubject, long[] writtenSubject)
        {
            try
            {
                return _examsDao.LoadExam(programId, sessionId, branchId, batchId, courseId, examName, mcqSubject,
                    writtenSubject);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }

        }


        public IList<ExamParent> LoadExamParentList(long[] authorizeOrgIds, long[] authorizeProgramIds,
            long[] authorizeSessionIds, int start, int length, string orderBy, string orderDirection,
            string organization,
            string program, string session, string course, string examNamePrefix)
        {
            try
            {




                return _examParentDao.LoadExamParentList(authorizeOrgIds, authorizeProgramIds, authorizeSessionIds,
                    start, length, orderBy, orderDirection, organization, program, session, course, examNamePrefix);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Other Functions

        public bool CheckDuplicateSubject(long[] mcqSubject, long[] writtenSubject)
        {
            try
            {
                bool isDuplicateExam = true;
                if (mcqSubject != null)
                {
                    if (mcqSubject.Distinct().Count() < mcqSubject.Length)
                    {
                        isDuplicateExam = false;
                    }
                }
                if (writtenSubject != null)
                {
                    if (writtenSubject.Distinct().Count() < writtenSubject.Length)
                    {
                        isDuplicateExam = false;
                    }

                }

                return isDuplicateExam;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;
            }
        }

        public bool CheckDuplicateExamCode(Exams createExams, long batchId, long branchId)
        {
            try
            {

                bool isDuplicateExamCode = true;
                IList<Exams> exams = _examsDao.CheckDuplicateExamCode(createExams, batchId, branchId);
                if (exams != null && exams.Count > 0)
                {
                    isDuplicateExamCode = false;
                }
                return isDuplicateExamCode;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;
            }
        }

        public bool CheckDuplicateName(Exams exam, long? examId = null)
        {
            try
            {

                bool isDuplicateExamName = true;
                IList<Exams> exams = _examsDao.CheckDuplicateExamName(exam, examId);
                if (exams != null && exams.Count > 0)
                {
                    isDuplicateExamName = false;
                }
                return isDuplicateExamName;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;
            }
        }

        public bool CheckDuplicateShortName(Exams exam, long? examId = null)
        {
            try
            {

                bool isDuplicateExamShortName = true;
                IList<Exams> exams = _examsDao.CheckDuplicateExamShortName(exam, examId);
                if (exams != null && exams.Count > 0)
                {
                    isDuplicateExamShortName = false;
                }
                return isDuplicateExamShortName;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;
            }
        }

        public bool CheckDuplicateExamCodeForEdit(Exams editExams, long examId)
        {
            try
            {
                bool isDuplicateExamCode = true;
                IList<Exams> exams = _examsDao.CheckDuplicateExamCodeForEdit(editExams, examId);
                if (exams != null && exams.Count > 0)
                {
                    isDuplicateExamCode = false;
                }
                return isDuplicateExamCode;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetMaximumRank(Exams e)
        {
            try
            {
                return _examsDao.GetMaximumRank(e);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetExamParentRowCount(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds,
            string organization, string program, string session, string course, string examNamePrefix)
        {
            try
            {
                return _examParentDao.GetExamParentRowCount(authorizeOrgIds, authorizeProgramIds, authorizeSessionIds,
                    organization, program, session, course, examNamePrefix);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetAnswerScriptCount(long id)
        {
            try
            {
                return _examParentDao.GetAnswerScriptCount(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetStudentMarksCount(long id)
        {
            try
            {
                return _examParentDao.GetStudentMarksCount(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        public bool IsChildrenExamModified(ExamParent examParent)
        {
            try
            {
                var firstExam = examParent.Examses.FirstOrDefault();
                if (firstExam != null)
                {
                    var mcqFullMarks = firstExam.McqFullMarks;
                    var durationHour = firstExam.DurationHour;
                    var durationMinute = firstExam.DurationMinute;
                    var isBangla = firstExam.IsBanglaVersion;
                    var isEnglish = firstExam.IsEnglishversion;
                    var parentExamPrefix = examParent.ExamNamePrefix;
                    var dependencyException = new DependencyException("Child exam modified already.");
                    int serial = 1;
                    foreach (var exam in examParent.Examses)
                    {
                        if (exam.McqFullMarks != mcqFullMarks || exam.DurationMinute != durationMinute ||
                            exam.DurationHour != durationHour || exam.IsBanglaVersion != isBangla ||
                            exam.IsEnglishversion != isEnglish)
                        {
                            throw dependencyException;
                        }
                        if (!exam.Name.Contains(parentExamPrefix))
                        {
                            throw dependencyException;
                        }
                        var examSuffix = exam.Name.Remove(0, parentExamPrefix.Length);
                        int numericSuffix;
                        bool isNumeric = int.TryParse(examSuffix, out numericSuffix);
                        if (!isNumeric)
                        {
                            throw dependencyException;
                        }
                        if (numericSuffix != serial)
                        {
                            throw dependencyException;
                        }
                        serial++;
                    }
                }
                return false;
            }
            catch (DependencyException ex)
            {
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Helper Function

        #endregion
    }
}


