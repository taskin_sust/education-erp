﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.MaterialTypeTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "MaterialType")]
    public class MaterialTypeUpdateTest : MaterialTypeBaseTest
    {
        #region Basic Test
        [Fact]
        public void MaterialType_Save_Should_Return_True()
        {
            _materialTypeFactory.Create().WithOrganization().Persist();
            _materialTypeFactory.Object.Name = "Updated_Material_Type";
            var isUpdated = _materialTypeService.Update(_materialTypeFactory.Object.Id, _materialTypeFactory.Object);
            Assert.True(isUpdated);
        }
        [Fact]
        public void MaterialType_Update_Should_Return_Object_Null_Exception()
        {
            _materialTypeFactory.Create().WithOrganization().Persist();
            Assert.Throws<NullObjectException>(() => _materialTypeService.Update(_materialTypeFactory.Object.Id, null));
        }
        [Fact]
        public void MaterialType_Update_Should_Return_Invalid_Id()
        {
            _materialTypeFactory.Create().WithOrganization().Persist();
            Assert.Throws<NullObjectException>(() => _materialTypeService.Update(0, _materialTypeFactory.Object));
        }
        #endregion

        #region Business Logic Test
        [Fact]
        public void Validation_Error_For_Name_Empty()
        {
            _materialTypeFactory.Create().WithOrganization();
            _materialTypeService.Save(_materialTypeFactory.Object);
            _materialTypeFactory.Object.Name = string.Empty;
            Assert.Throws<InvalidDataException>(() => _materialTypeService.Update(_materialTypeFactory.Object.Id, _materialTypeFactory.Object));

        }
        [Fact]
        public void Validation_Error_For_ShortName_Empty()
        {
            _materialTypeFactory.Create().WithOrganization();
            _materialTypeService.Save(_materialTypeFactory.Object);
            _materialTypeFactory.Object.ShortName = string.Empty;
            Assert.Throws<InvalidDataException>(() => _materialTypeService.Update(_materialTypeFactory.Object.Id, _materialTypeFactory.Object));
        }
        [Fact]
        public void Validation_Error_For_Name_Null()
        {
            _materialTypeFactory.Create().WithOrganization();
            _materialTypeService.Save(_materialTypeFactory.Object);
            _materialTypeFactory.Object.Name = null;
            Assert.Throws<InvalidDataException>(() => _materialTypeService.Update(_materialTypeFactory.Object.Id, _materialTypeFactory.Object));

        }
        [Fact]
        public void Validation_Error_For_ShortName_Null()
        {
            _materialTypeFactory.Create().WithOrganization();
            _materialTypeService.Save(_materialTypeFactory.Object);
            _materialTypeFactory.Object.ShortName = null;
            Assert.Throws<InvalidDataException>(() => _materialTypeService.Update(_materialTypeFactory.Object.Id, _materialTypeFactory.Object));
        }
        [Fact]
        public void MaterialType_Update_Should_Return_Validation_Error_For_OrganizationRef()
        {
            _materialTypeFactory.Create().WithOrganization();
            _materialTypeService.Save(_materialTypeFactory.Object);
            _materialTypeFactory.Object.Organization = null;
            Assert.Throws<InvalidDataException>(() => _materialTypeService.Update(_materialTypeFactory.Object.Id, _materialTypeFactory.Object));
        }
        [Fact]
        public void MaterialType_Update_Should_Return_Name_Duplicate()
        {
            organizationFactory.Create().Persist();
            _materialTypeFactory.CreateMore(2, organizationFactory.Object);
            _materialTypeService.Save(_materialTypeFactory.ObjectList[0]);
            _materialTypeService.Save(_materialTypeFactory.ObjectList[1]);
            _materialTypeFactory.ObjectList[1].Name = _materialTypeFactory.ObjectList[0].Name;
            Assert.Throws<DuplicateEntryException>(() =>_materialTypeService.Update(_materialTypeFactory.ObjectList[1].Id, _materialTypeFactory.ObjectList[1]));

        }
        [Fact]
        public void MaterialType_Update_Should_Return_ShortName_Duplicate()
        {
            organizationFactory.Create().Persist();
            _materialTypeFactory.CreateMore(2, organizationFactory.Object);
            _materialTypeService.Save(_materialTypeFactory.ObjectList[0]);
            _materialTypeService.Save(_materialTypeFactory.ObjectList[1]);
            _materialTypeFactory.ObjectList[1].ShortName = _materialTypeFactory.ObjectList[0].ShortName;
            Assert.Throws<DuplicateEntryException>(() =>_materialTypeService.Update(_materialTypeFactory.ObjectList[1].Id, _materialTypeFactory.ObjectList[1]));

        }
        #endregion

    }
}
