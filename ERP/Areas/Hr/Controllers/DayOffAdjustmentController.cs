﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using NHibernate;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;
using UdvashERP.Services.Hr.AttendanceSummaryServices;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class DayOffAdjustmentController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        private readonly ITeamMemberService _teamMemberService;
        private readonly IDayOffAdjustmentService _dayOffAdjustmentService;
        private readonly IAttendanceSummaryService _attendanceSummaryService;
        private readonly IUserService _userService;
        private readonly IOrganizationService _organizationService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IDepartmentService _hrDepartmentService;

        private List<UserMenu> _userMenus;

        public DayOffAdjustmentController()
        {
            ISession session = NHibernateSessionFactory.OpenSession();
            _teamMemberService = new TeamMemberService(session);
            _dayOffAdjustmentService = new DayOffAdjustmentService(session);
            _attendanceSummaryService = new AttendanceSummaryService(session);
            _organizationService = new OrganizationService(session);
            _branchService = new BranchService(session);
            _campusService = new CampusService(session);
            _hrDepartmentService = new DepartmentService(session);
            _userService = new UserService(session);
        }

        #region Mentor Adjustment

        public ActionResult MentorDayOffAdjustmentApproval()
        {
            ViewBag.dateFromField = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
            ViewBag.dateToField = DateTime.Now.ToString("yyyy-MM-dd");
            return View();
        }

        [HttpPost]
        public ActionResult MentorApproveAllDayOffAdjustment(string dayOffObjectList)
        {
            try
            {
                if (!String.IsNullOrEmpty(dayOffObjectList))
                {
                    var dayOffAdjustments = new List<DayOffAdjustment>();
                    var listModels = new JavaScriptSerializer().Deserialize<List<DayOffAdjustmentViewModel>>(dayOffObjectList);
                    foreach (var model in listModels)
                    {
                        var dayOffAdjustment = new DayOffAdjustment();
                        dayOffAdjustment.TeamMember = _teamMemberService.LoadById(model.MemberId);
                        dayOffAdjustment.DayOffDate = Convert.ToDateTime(model.Date);
                        dayOffAdjustment.InsteadOfDate = Convert.ToDateTime(model.InsteadOfDate);
                        dayOffAdjustment.Reason = model.ReasonForAdjustment;
                        dayOffAdjustment.DayOffStatus = (int?) DayOffAdjustmentStatus.Approved;
                        dayOffAdjustments.Add(dayOffAdjustment);
                    }
                    bool isSuccess = _dayOffAdjustmentService.Save(dayOffAdjustments);
                    if (isSuccess) return Json(new { IsSuccess = true, returnSuccess="Successfully Added" });
                }
                return Json(new Response(false,WebHelper.CommonErrorMessage));
            }
            catch (InvalidDataException ex)
            {
                return Json(new { IsSuccess = false, returnSuccess = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, returnSuccess = WebHelper.CommonErrorMessage });
                //return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult MentorDayOffRecentAdjustment(int draw, int start, int length, string pin, string dateFrom, string dateTo)
        {
            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            int recordsFiltered = 0;
            int recordsTotal = 0;
            bool isSuccess = false;
            NameValueCollection nvc = Request.Form;
            TeamMember mentor = _teamMemberService.GetCurrentMember();

         
            if (mentor == null)
            {
                throw new InvalidDataException("This TeamMember Has No User.");
            }
            try
            {
                DateTime dateFromDateTime = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).Date;
                DateTime dateToDateTime = DateTime.Today.Date;
                if (!String.IsNullOrEmpty(dateFrom))
                {
                    dateFromDateTime = Convert.ToDateTime(dateFrom).Date;
                }
                if (!String.IsNullOrEmpty(dateTo))
                {
                    dateToDateTime = Convert.ToDateTime(dateTo).Date;
                }

                if (dateFromDateTime > dateToDateTime)
                {
                    DateTime temp = dateToDateTime;
                    dateToDateTime = dateFromDateTime;
                    dateFromDateTime = temp;
                }
                #region Order By
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "TeamMemberPin";
                            break;
                        case "1":
                            orderBy = "TeamMemberName";
                            break;
                        case "2":
                            orderBy = "DayOffDate";
                            break;
                        case "3":
                            orderBy = "InsteadOfDate";
                            break;
                        default:
                            orderBy = "DayOffDate";
                            break;
                    }
                }
                #endregion

                int totalRow = _dayOffAdjustmentService.CountDayOffRecentAdjustmentForMentor(mentor, pin, dateFromDateTime, dateToDateTime);
                List<DayOffAdjustmentMentorDto> rowList = _dayOffAdjustmentService.LoadDayOffRecentAdjustmentForMentor(mentor, start, length, orderBy, orderDir.ToUpper(), pin, dateFromDateTime, dateToDateTime).ToList();
                recordsFiltered = totalRow;
                recordsTotal = recordsFiltered;
                
                #region DataTable
                foreach (var row in rowList)
                {
                    var str = new List<string>();
                    str.Add(row.TeamMemberPin.ToString());
                    str.Add(row.TeamMemberName);
                    str.Add(row.DayOffDate.ToString("yyyy-MM-dd"));
                    str.Add(row.InsteadOfDate.ToString("yyyy-MM-dd"));
                    str.Add(row.Reason);
                    switch (row.DayOffStatus)
                    {
                        case (int) DayOffAdjustmentStatus.Approved:
                            str.Add("<span class='glyphicon glyphicon-ok'></span>");
                            break;
                        case (int) DayOffAdjustmentStatus.Rejected:
                            str.Add("<span class='glyphicon glyphicon-remove'></span>");
                            break;
                        case (int)DayOffAdjustmentStatus.Cancel:
                            str.Add("<span class='glyphicon glyphicon-remove'></span>");
                            break;
                        case (int)DayOffAdjustmentStatus.Pending:
                            str.Add("<span class='glyphicon glyphicon-remove'></span>");
                            break;

                    }
                    if (row.CanEdit == 1)
                        str.Add("<a href='" + Url.Action("EditRecentAdjustmentByMentor", "DayOffAdjustment") + "?Id=" + row.Id + "' class='glyphicon glyphicon-edit updateHistory' id='" + row.Id + "'></a>");
                    else str.Add("");
                    getData.Add(str);
                }
                #endregion
                
                isSuccess = true;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new { draw = draw, recordsTotal = recordsTotal, recordsFiltered = recordsFiltered, start = start, length = length, data = getData, isSuccess = isSuccess });
        }

        [HttpGet]
        public ActionResult EditRecentAdjustmentByMentor(string id)
        {
            try
            {
                if (id != "")
                {
                    var dayOffAdjustment = _dayOffAdjustmentService.LoadById(Convert.ToInt64(id));
                    if (dayOffAdjustment != null)
                    {
                        //IList<AttendanceSummary> list = _attendanceSummaryService.ListAttendanceSummary(dayOffAdjustment.TeamMember);
                        IList<AttendanceSummary> list = _attendanceSummaryService.LoadTeamMemberDayOffAttendanceSummary(dayOffAdjustment.TeamMember, dayOffAdjustment.Id);
                        var insdteadOfDateList = new SelectList(list.ToList(), "ModifiedInsteadOf", "ModifiedInsteadOf", dayOffAdjustment.InsteadOfDate.Value.ToString("yyyy-MM-dd"));
                        ViewBag.InsdteadOfDateList = insdteadOfDateList.ToList();
                        ViewBag.DayOffAdjustment = dayOffAdjustment;
                    }
                    else
                    {
                        return RedirectToAction("MentorDayOffAdjustmentApproval");
                    }
                    
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public ActionResult ApproveRecentAdjustmentByMentor(string dataObject)
        {
            string message = "";
            bool isSuccess = false;
            try
            {
                if (dataObject != "")
                {
                    dynamic stuff = JsonConvert.DeserializeObject(dataObject);
                    DayOffAdjustment dayOffAdjustment = _dayOffAdjustmentService.LoadById(Convert.ToInt64(stuff.Id));
                    dayOffAdjustment.DayOffDate = Convert.ToDateTime(stuff.DateOff);
                    dayOffAdjustment.InsteadOfDate = Convert.ToDateTime(stuff.InsteadOf);
                    dayOffAdjustment.Reason = stuff.Reason;
                    List<DayOffAdjustment> savingDayOffAdjustmentList = new List<DayOffAdjustment> {dayOffAdjustment};
                    isSuccess = _dayOffAdjustmentService.Save(savingDayOffAdjustmentList);
                    if (isSuccess)
                    {
                        message = "Successfully Approved.";
                    }
                    //bool isSuccess = _dayOffAdjustmentService.Update(dayOffAdjustment);
                    //if (isSuccess)
                    //    return Json(new {IsSuccess = true, returnSuccess = "Successfully Added"});
                }
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = isSuccess, returnSuccess = message });
        }

        [HttpPost]
        public ActionResult DeleteRecentAdjustmentByMentor(string dataObject)
        {
            string message = "";
            bool isSuccess = true;
            try
            {
                if (dataObject == "")
                    throw new InvalidDataException("Invalid Object");

                dynamic stuff = JsonConvert.DeserializeObject(dataObject);
                DayOffAdjustment dayOffAdjustment = _dayOffAdjustmentService.LoadById(Convert.ToInt64(stuff.Id));
                dayOffAdjustment.DayOffDate = Convert.ToDateTime(stuff.DateOff);
                dayOffAdjustment.InsteadOfDate = Convert.ToDateTime(stuff.InsteadOf);
                dayOffAdjustment.Reason = stuff.Reason;
                dayOffAdjustment.DayOffStatus = (int) DayOffAdjustmentStatus.Rejected;
                List<DayOffAdjustment> savingDayOffAdjustmentList = new List<DayOffAdjustment> { dayOffAdjustment };
                isSuccess = _dayOffAdjustmentService.Save(savingDayOffAdjustmentList);
              //  isSuccess = _dayOffAdjustmentService.Update(dayOffAdjustment);
                if (isSuccess)
                {
                    message = "Successfully Deleted.";
                    isSuccess = true;
                }
                //if(isSuccess)
                //    return Json(new {IsSuccess = true, returnSuccess = "Successfully updated"});
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
                isSuccess = false;
                //return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
                isSuccess = false;
               // return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return Json(new { IsSuccess = isSuccess, returnSuccess = message });
           // return Json(new Response(false, WebHelper.CommonErrorMessage));
        }

        #endregion

        #region HR Adjustment
        [HttpGet]
        public ActionResult HrDayOffAdjustment()
        {
            _userMenus = (List<UserMenu>)ViewBag.UserMenu;
            var orgList = _organizationService.LoadAuthorizedOrganization(_userMenus).ToList();
            
            ViewBag.OrgList = new SelectList(orgList, "Id", "ShortName");
            ViewBag.DeptList = new SelectList(new List<Department>(), "Id", "Name");
            ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.dateFromField = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
            ViewBag.dateToField = DateTime.Now.ToString("yyyy-MM-dd");

            return View();
        }

        [HttpPost]
        public ActionResult HrDayOffAdjustmentEntry(string dataObject)
        {
            string message = "";
            bool isSucces = false;
            try
            {
                if (dataObject != "")
                {
                    dynamic stuffList  = JsonConvert.DeserializeObject(dataObject);
                    var dayOffAdjustments = new List<DayOffAdjustment>();
                    foreach (var stuff in stuffList)
                    {
                        var model = new DayOffAdjustment
                        {
                            DayOffDate = Convert.ToDateTime(stuff.DayOffDate),
                            InsteadOfDate = Convert.ToDateTime(stuff.InsteadOfDate),
                            Reason = stuff.ReasonForAdjustment,
                            TeamMember = _teamMemberService.LoadById(Convert.ToInt64(stuff.MemberId)),
                            DayOffStatus = (int) DayOffAdjustmentStatus.Approved
                        };
                        dayOffAdjustments.Add(model);
                    }
                    bool isSuccess = _dayOffAdjustmentService.Save(dayOffAdjustments);
                    if (isSuccess)
                    {
                        isSucces = true;
                        message = "Successfully Added";
                    }
                        //return Json(new { IsSuccess = true, returnSuccess = "Successfully Added" });
                }
            }
            catch (InvalidDataException ex)
            {
                isSucces = false;
                message = ex.Message;
                //_logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
            }
            return Json(new { IsSuccess = isSucces, returnSuccess = message });
            //return Json(new Response(false, WebHelper.CommonErrorMessage));
        }

        [HttpPost]
        public ActionResult HrDayOffAdjustmentHistory(int draw, int start, int length, long? org, long? branch, long? campus, long? department,string pin, string dateFrom, string dateTo)
        {
            NameValueCollection nvc = Request.Form;

            _userMenus = (List<UserMenu>)ViewBag.UserMenu;
            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Date";
                            break;
                        default:
                            orderBy = "Date";
                            break;
                    }
                }

                DateTime dateFromDateTime = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).Date;
                DateTime dateToDateTime = DateTime.Today.Date;
                if (!String.IsNullOrEmpty(dateFrom))
                {
                    dateFromDateTime = Convert.ToDateTime(dateFrom).Date;
                }
                if (!String.IsNullOrEmpty(dateTo))
                {
                    dateToDateTime = Convert.ToDateTime(dateTo).Date;
                }

                if (dateFromDateTime > dateToDateTime)
                {
                    DateTime temp = dateToDateTime;
                    dateToDateTime = dateFromDateTime;
                    dateFromDateTime = temp;
                }

                int totalRow = _dayOffAdjustmentService.CountHrDayOffAdjustmentHistory(_userMenus, pin, dateFromDateTime,
                    dateToDateTime, org, branch, campus, department);
                IList<DayOffAdjustmentHrDto> rowList =
                    _dayOffAdjustmentService.LoadAllHrDayOffAdjustmentHistory(_userMenus, start, length, orderBy,
                        orderDir.ToUpper(), pin, dateFromDateTime, dateToDateTime, org, branch, campus, department)
                        .ToList();

                long recordsFiltered = totalRow;
                long recordsTotal = recordsFiltered;

                foreach (var row in rowList)
                {
                    var str = new List<string>();
                    string status = "-";
                    str.Add(row.Pin.ToString());
                    str.Add(row.Name);
                    str.Add(row.Department);
                    str.Add(row.Organization);
                    str.Add(row.Branch);
                    str.Add(row.Campus);
                    str.Add(row.Designation);
                    str.Add(row.DayOffDate.ToString("yyyy-MM-dd"));
                    str.Add(row.InsteadOfDate.ToString("yyyy-MM-dd"));
                    str.Add(row.Reason);
                    if (row.DayOffStatus == (int) DayOffAdjustmentStatus.Approved)
                        status = "Approved";
                    else if (row.DayOffStatus == (int) DayOffAdjustmentStatus.Rejected)
                        status = "Rejected";
                    str.Add(status);
                    str.Add(row.ModifiedBy != null ? _userService.GetUserNameByAspNetUserId((long) row.ModifiedBy) : "-");
                    str.Add(CommonHelper.GetReportCommonDateTimeFormat(row.LastModificationDate));
                    if (row.CanEdit == 1)
                    {
                        str.Add("<a href='" + Url.Action("EditDayOffHistoryByHr", "DayOffAdjustment") + "?Id=" + row.Id +
                                "' class='glyphicon glyphicon-edit changeHistory' id='" + row.Id + "'></a>");
                    }
                    else
                    {
                        str.Add("-");
                    }
                    getData.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData

                });
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }
        [HttpGet]
        public ActionResult EditDayOffHistoryByHr(string id)
        {
            try
            {
                if (id == "")
                    throw new InvalidDataException("Id not found");
                DayOffAdjustment model = _dayOffAdjustmentService.LoadById(Convert.ToInt64(id));
                if(model==null)
                    throw new InvalidDataException("Data is invalid");

                EmploymentHistory emplomentHistory = model.TeamMember.EmploymentHistory.Where(x=>x.EffectiveDate <= model.DayOffDate).OrderByDescending(x => x.EffectiveDate).ThenByDescending(x=>x.Id).FirstOrDefault();
                if (model.DayOffDate != null) ViewBag.DateOffDate = model.DayOffDate.Value.ToString("yyyy-MM-dd");
                if (model.InsteadOfDate != null)
                {
                    var insateadOfDate = model.InsteadOfDate;
                }
                //IList<AttendanceSummary> list = _attendanceSummaryService.ListAttendanceSummary(model.TeamMember);
                IList<AttendanceSummary> list = _attendanceSummaryService.LoadTeamMemberDayOffAttendanceSummary(model.TeamMember, model.Id);
                var insdteadOfDateList = new SelectList(list.ToList(), "ModifiedInsteadOf", "ModifiedInsteadOf", model.InsteadOfDate.Value.ToString("yyyy-MM-dd"));
                ViewBag.InsdteadOfDateList = insdteadOfDateList.ToList();
                //ViewBag.Org = emplomentHistory.Select(x => x.Campus.Branch.Organization.Name).FirstOrDefault();
                //ViewBag.Branch = emplomentHistory.Select(x => x.Campus.Branch.Name).FirstOrDefault();
                //ViewBag.Campus = emplomentHistory.Select(x => x.Campus.Name).FirstOrDefault();
                //ViewBag.Department = emplomentHistory.Select(x => x.Department.Name).FirstOrDefault();
                ViewBag.Org = (emplomentHistory != null) ? emplomentHistory.Department.Organization.ShortName : "-";
                ViewBag.Branch = (emplomentHistory != null) ? emplomentHistory.Campus.Branch.Name : "-";
                ViewBag.Campus = (emplomentHistory != null) ? emplomentHistory.Campus.Name : "-";
                ViewBag.Department = (emplomentHistory != null) ? emplomentHistory.Department.Name : "-";
                ViewBag.Pin = model.TeamMember.Pin;
                ViewBag.Name = model.TeamMember.Name;
                ViewBag.Reason = model.Reason;
                ViewBag.Model = model;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public ActionResult ApproveDayOffAdjustmentByHr(string dataObject)
        {
            string message = "";
            bool isSucces = false;
            try
            {
                if (dataObject == "")
                    throw new InvalidDataException("Invalid Object");
                
                dynamic stuff = JsonConvert.DeserializeObject(dataObject);
                DayOffAdjustment dayOffAdjustment = _dayOffAdjustmentService.LoadById(Convert.ToInt64(stuff.Id));
                dayOffAdjustment.DayOffDate = Convert.ToDateTime(stuff.DateOff);
                dayOffAdjustment.InsteadOfDate = Convert.ToDateTime(stuff.InsteadOf);
                dayOffAdjustment.Reason = stuff.Reason;
                dayOffAdjustment.DayOffStatus = (int)DayOffAdjustmentStatus.Approved;
                List<DayOffAdjustment> savingDayOffAdjustmentList = new List<DayOffAdjustment> { dayOffAdjustment };
                isSucces = _dayOffAdjustmentService.Save(savingDayOffAdjustmentList);
                if (isSucces)
                {
                    message = "Successfully Approved.";    
                }
                //bool isSuccess = _dayOffAdjustmentService.Update(dayOffAdjustment);
                //if (isSuccess)
                //    return Json(new { IsSuccess = true, returnSuccess = "Successfully Approved." });

            }
            catch (InvalidDataException ex)
            {
                isSucces = false;
                message = ex.Message;
                //_logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
            }
            return Json(new { IsSuccess = isSucces, returnSuccess = message });
        }

        [HttpPost]
        public ActionResult DeleteDayOffAdjustmentByHr(string dataObject)
        {
            string message = "";
            bool isSucces = false;
            try
            {
                if (dataObject == "")
                    throw new InvalidDataException("Invalid Object");

                dynamic stuff = JsonConvert.DeserializeObject(dataObject);
                DayOffAdjustment dayOffAdjustment = _dayOffAdjustmentService.LoadById(Convert.ToInt64(stuff.Id));
                dayOffAdjustment.DayOffDate = Convert.ToDateTime(stuff.DateOff);
                dayOffAdjustment.InsteadOfDate = Convert.ToDateTime(stuff.InsteadOf);
                dayOffAdjustment.Reason = stuff.Reason;
                dayOffAdjustment.DayOffStatus = (int)DayOffAdjustmentStatus.Rejected;
                List<DayOffAdjustment> savingDayOffAdjustmentList = new List<DayOffAdjustment> { dayOffAdjustment };
                isSucces = _dayOffAdjustmentService.Save(savingDayOffAdjustmentList);
                if (isSucces)
                {
                    message = "Successfully Rejected.";
                }
                //bool isSuccess = _dayOffAdjustmentService.Update(dayOffAdjustment);
                //if (isSuccess)
                //    return Json(new { IsSuccess = true, returnSuccess = "Successfully Rejected." });
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
            }
            return Json(new { IsSuccess = isSucces, returnSuccess = message });
        }
        #endregion
    }
}