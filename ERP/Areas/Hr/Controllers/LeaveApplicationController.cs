﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Script.Serialization;
using FluentNHibernate;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Linq.Functions;
using NHibernate.Type;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]

    public class LeaveApplicationController : Controller
    {
        // GET: HR/HRLeaveApplication
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        const string DropDownOptionView = "Partial/_leaveDetails";
        private readonly ILeaveService _hrLeaveService;
        private ISession _session;
        private readonly ICommonHelper _commonHelper;
        private readonly ILeaveApplicationService _hrLeaveApplicationService;
        private readonly ITeamMemberService _hrMemberService;
        private readonly IMembersLeaveSummaryService _hrMembersLeaveSummaryService;
        private readonly IMentorHistoryService _mentorHistoryService;
        private List<UserMenu> _userMenu;
        public LeaveApplicationController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _hrLeaveService = new LeaveService(_session);
            _hrLeaveApplicationService = new LeaveApplicationService(_session);
            _hrMemberService = new TeamMemberService(_session);
            _hrMembersLeaveSummaryService = new MembersLeaveSummaryService(_session);
            _commonHelper = new CommonHelper();
            _mentorHistoryService = new MentorHistoryService(_session);
        }

        public ActionResult Index()
        {
            return  RedirectToAction("LeaveApplication");
        }

        #region Leave Application

        public ActionResult LeaveApplication()
        {
            try
            {
                TeamMember member = _hrMemberService.GetCurrentMember();
                if (member == null)
                    throw new InvalidDataException("This User has no TeamMember");

                Organization teamMemberOrganization = _hrMemberService.GetTeamMemberOrganization(null, member.Id);
                IList<MemberLeaveSummary> leaveSummaries = _hrMembersLeaveSummaryService.GetLeaveSummary(teamMemberOrganization.Id, member.Id, DateTime.Now.Year);

                List<SelectListItem> sList = new List<SelectListItem>();
                sList.Insert(0, (new SelectListItem { Text = "Select a Leave Type", Value = "" }));
                if (leaveSummaries.Any())
                {
                    leaveSummaries = leaveSummaries.Where(x => x.Leave.IsPublic == true && x.Leave.PayType == (int)PayType.WithPay && x.Status == MemberLeaveSummary.EntityStatus.Active).ToList();
                    if (leaveSummaries.Any())
                    {
                        foreach (var leaveSummary in leaveSummaries)
                        {
                            SelectListItem item = new SelectListItem();
                            item.Text = leaveSummary.Leave.Name + " (" + leaveSummary.AvailableBalance.ToString() + ") ";
                            item.Value = leaveSummary.Leave.Id.ToString();
                            sList.Add(item);
                        }
                    }
                }
               
                List<Leave> withOutPayLeave = _hrLeaveService.LoadLeave(_commonHelper.ConvertIdToList(teamMemberOrganization.Id)).ToList();
                if (withOutPayLeave.Any())
                {
                    withOutPayLeave = withOutPayLeave.Where(x => x.PayType == 0 && x.Status == Leave.EntityStatus.Active && x.IsPublic == true).ToList();
                    foreach (var leave in withOutPayLeave)
                    {
                        SelectListItem item = new SelectListItem();
                        item.Text = leave.Name + " (-) ";
                        item.Value = leave.Id.ToString();
                        sList.Add(item);
                    }
                }
                ViewBag.LeaveType = sList;

            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            return View();
        }

        [HttpGet]
        public PartialViewResult RecentLeaveApplication()
        {
            List<SelectListItem> sList = new List<SelectListItem>();
            sList.Insert(0, (new SelectListItem { Text = "Select All", Value = "" }));
            ViewBag.LeaveType = sList;
            try
            {
                TeamMember member = _hrMemberService.GetCurrentMember();
                if (member != null)
                {
                    Organization organization = _hrMemberService.GetTeamMemberOrganization(null, member.Id);
                    ViewBag.LeaveType = new SelectList(_hrLeaveService.LoadLeave(_commonHelper.ConvertIdToList(organization.Id)), "Id", "Name");
                }
                ViewBag.LeaveStatus = new SelectList(_commonHelper.LoadEmumToDictionary<LeaveStatus>(), "Key", "Value");

                return PartialView("Partial/_RecentLeaveApplication");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #region Datatable 

        [HttpPost]
        public JsonResult RecentLeaveApplicationPost(int draw, int start, int length, string leaveTypeId = "", string leaveStatus = "")
        {
            try
            {
                var data = new List<object>();
                var recordsTotal = 0;
                var recordsFiltered = 0;
                TeamMember member = _hrMemberService.GetCurrentMember();
                if (member == null)
                {
                    var str = new List<string>();
                    int i = 0;
                    for (i = 1; i <= 6; i++)
                    {
                        str.Add("N/A");
                    }
                    data.Add(str);
                }
                else
                {
                    IList<LeaveApplication> hrLeaveApplications = _hrLeaveApplicationService.LoadTeamMemberLeaveApplication(start, length, DateTime.Now, member.Id, leaveTypeId, leaveStatus);
                    recordsTotal = _hrLeaveApplicationService.GetTeamMemberLeaveApplicationCount(DateTime.Now, member.Id, leaveTypeId, leaveStatus);
                    recordsFiltered = recordsTotal;

                    foreach (var leaveappObj in hrLeaveApplications)
                    {
                        DateTime leaveExpairTime = GetLeaveExpaireDate(leaveappObj);
                        string appliedBy = "-";
                        TeamMember appliedByTeamMember = _hrMemberService.GetTeamMemberByAspNetUserId(leaveappObj.CreateBy);
                        if (appliedByTeamMember != null)
                        {
                            appliedBy = appliedByTeamMember.Name + " (" + appliedByTeamMember.Pin + ") ";
                        }
                        var str = new List<string>();
                        str.Add(!String.IsNullOrEmpty(leaveappObj.Leave.Name) ? leaveappObj.Leave.Name : "");
                        str.Add(leaveappObj.DateFrom.ToString("dd MMM yyyy") + " to " + leaveappObj.DateTo.ToString("dd MMM yyyy"));
                        str.Add(leaveappObj.TotalLeaveDay.ToString());
                        str.Add(leaveappObj.CreationDate.ToString("dd MMM yyyy"));
                        str.Add(appliedBy);
                        if (leaveappObj.LeaveStatus == (int)LeaveStatus.Approved)
                        {
                            str.Add("<span id='status-" + leaveappObj.Id + "'>Approved</span>");
                        }
                        else if (leaveappObj.LeaveStatus == (int)LeaveStatus.Pending)
                        {
                            str.Add("<span id='status-" + leaveappObj.Id + "'>Pending</span>");
                        }
                        else if (leaveappObj.LeaveStatus == (int)LeaveStatus.Cancelled)
                        {
                            str.Add("<span id='status-" + leaveappObj.Id + "'>Cancelled</span>");
                        }
                        else if (leaveappObj.LeaveStatus == (int)LeaveStatus.Rejected)
                        {
                            str.Add("<span id='status-" + leaveappObj.Id + "'>Rejected</span>");
                        }
                        else
                        {
                            str.Add("-");
                        }

                        if ((leaveappObj.LeaveStatus == (int)LeaveStatus.Pending || leaveappObj.LeaveStatus == (int)LeaveStatus.Approved) && leaveExpairTime >= DateTime.Now)
                        {
                            str.Add("<a href='#' data-id='" + leaveappObj.Id + "' class='glyphicon glyphicon-trash'> </a>");
                        }
                        else
                        {
                            str.Add("");
                        }
                        data.Add(str);
                    }
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        
        #endregion

        [HttpPost]
        public JsonResult LeaveApplication(LeaveApplicationViewModel leaveApplicationViewModel)
        {
            string message = "";
            bool successState = false;
            try
            {
                LeaveApplication leaveApplication = new LeaveApplication();
                TeamMember member = _hrMemberService.GetCurrentMember();
                
                leaveApplicationViewModel.FillLeaveApplicationValue(leaveApplication);
                Leave leave = _hrLeaveService.GetById(leaveApplicationViewModel.LeaveId);
                leaveApplication.Leave = leave;
                leaveApplication.TeamMember = member;
                leaveApplication.Status = BusinessModel.Entity.Hr.LeaveApplication.EntityStatus.Active;
                leaveApplication.LeaveStatus = (int)LeaveStatus.Pending;
                if (leaveApplicationViewModel.ResponsibleMemberPin != null)
                {
                    TeamMember responsiblePerson = _hrMemberService.GetByPin(leaveApplicationViewModel.ResponsibleMemberPin.Value);
                    leaveApplication.ResponsiblePerson = responsiblePerson;
                }
                List<LeaveApplication> leaveApplicationList = new List<LeaveApplication>();
                leaveApplicationList.Add(leaveApplication);
                _hrLeaveApplicationService.SaveOrUpdate(leaveApplicationList);
                message = "Your Leave Application Successfully Submitted";
                successState = true;
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        [HttpPost]
        public JsonResult CancelLeave(long id)
        {
            string message = "";
            string statusText = "";
            bool successState = false;
            try
            {
                TeamMember member = _hrMemberService.GetCurrentMember();
                LeaveApplication leaveApplication = _hrLeaveApplicationService.GetById(id);
                if (leaveApplication.TeamMember != member)
                    throw new InvalidDataException("Please select your Leave Application.");
                DateTime leaveExpairTime = GetLeaveExpaireDate(leaveApplication);
                if (leaveExpairTime < DateTime.Now)
                    throw new InvalidDataException("You already Enjoy this Leave.");
                leaveApplication.LeaveStatus = (int) LeaveStatus.Cancelled;
                statusText = "Cancelled";
                List<LeaveApplication> leaveApplicationList = new List<LeaveApplication>();
                leaveApplicationList.Add(leaveApplication);
                _hrLeaveApplicationService.SaveOrUpdate(leaveApplicationList);
                message = "Your Leave Application Successfully Cancelled";
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message, statusText });
        }

        #endregion

        #region Helper

        //[HttpPost]
        //public JsonResult GetTeamMemberInfo(int pin = 0, bool isHr = false, bool isMentor = false,  bool isLeaveSummaryInfo = false, bool isOrganization = false,  bool isBranch = false,  bool isCampus = false,  bool isDepartment = false)
        //{
        //    string message = "";
        //    string teamMemberName = "";
        //    bool successState = false;
        //    var leaveTypelist = "";
        //    string organizationName = "";
        //    string branchName = "";
        //    string campusName = "";
        //    string departmentName = "";
        //    _userMenu =  (List<UserMenu>)ViewBag.UserMenu;
        //    try
        //    {
        //        if(pin <= 0)
        //            throw new InvalidDataException("Invalid PIN");

        //        TeamMember member = null;
        //        List<TeamMember> authorizedTeamMemberList;
        //        if (isHr)
        //        {
        //            authorizedTeamMemberList = _hrMemberService.LoadHrAuthorizedTeamMember(_userMenu, null, null, null,null, null, null, _commonHelper.ConvertIdToList(pin)).ToList();
        //            if (authorizedTeamMemberList.Any())
        //            {
        //                member = authorizedTeamMemberList[0];
        //            }
                    
        //        } else if (isMentor)
        //        {
        //            TeamMember mentor = _hrMemberService.GetCurrentMember();
        //            authorizedTeamMemberList =
        //                _mentorHistoryService.LoadMentorTeamMember(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now, null, _commonHelper.ConvertIdToList(pin));
        //            if (authorizedTeamMemberList.Any())
        //            {
        //                member = authorizedTeamMemberList[0];
        //            }
        //        }
        //        else
        //        {
        //            member = _hrMemberService.GetByPin(pin);
        //        }
               
        //        if (member != null)
        //        {
        //            teamMemberName = member.Name;


        //            if (isLeaveSummaryInfo == true)
        //            {
        //                IList<LeaveTypeDtoList> leavePendingListDtos = _hrMemberService.LoadMembersPendingLeave(member.Id);
        //                leaveTypelist = this.RenderRazorViewToOptions(DropDownOptionView, leavePendingListDtos, 0, "Select Leave Type");
        //            }

        //            if (isOrganization)
        //            {
        //                Organization org = _hrMemberService.GetTeamMemberOrganization(null, member.Id);
        //                if (org != null)
        //                    organizationName = org.ShortName;
        //            }

        //            if (isBranch)
        //            {
        //                Branch branch = _hrMemberService.GetTeamMemberBranch(null, member.Id);
        //                if (branch != null)
        //                    branchName = branch.Name;
        //            }

        //            if (isCampus)
        //            {
        //                Campus campus = _hrMemberService.GetTeamMemberCampus(null, member.Id);
        //                if (campus != null)
        //                    campusName = campus.Name;
        //            }

        //            if (isDepartment)
        //            {
        //                Department department = _hrMemberService.GetTeamMemberDepartment(null, member.Id);
        //                if (department != null)
        //                    departmentName = department.Name;
        //            }
        //            successState = true;
        //        }
                
        //    }
        //    catch (InvalidDataException ex)
        //    {
        //        message = ex.Message;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        message = ex.Message;
        //    }
        //    return Json(new { IsSuccess = successState, Message = message, teamMemberName = teamMemberName, leaveTypelist = leaveTypelist, organizationName = organizationName, branchName = branchName, campusName = campusName, departmentName = departmentName });
        //}

        private DateTime GetLeaveExpaireDate(LeaveApplication leaveApplication)
        {
            Organization leaveOrganization = leaveApplication.Leave.Organization;
            DateTime organizationEndTime = leaveOrganization.AttendanceStartTime.AddSeconds(-1);
            DateTime leaveExpairTime = leaveApplication.DateFrom.Date.AddHours(organizationEndTime.Hour).AddMinutes(organizationEndTime.Minute).AddSeconds(organizationEndTime.Second);

            return leaveExpairTime;
        }

        #endregion

    }


}