﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UdvashERP.Api
{
    public class JsonResultResponse
    {
        public bool IsAuthenticated { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public dynamic Data { get; set; }

        public JsonResultResponse()
        {
        }

        public JsonResultResponse(bool isAuthenticated, bool isSuccess, string message, dynamic data)
        {
            this.IsAuthenticated = isAuthenticated;
            this.IsSuccess = isSuccess;
            this.Message = message;
            this.Data = data;
        }
    }
}