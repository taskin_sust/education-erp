﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceDeviceTypeTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceDeviceType")]
    public class AttendanceDeviceTypeLoadTest : AttendanceDeviceTypeBaseTest
    {
        #region Basic
        #endregion

        #region Single Object Test

        public void Should_Return_Invalid_Id()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.GetAttendanceDeviceType(0));
        }

        public void Should_Return_Single_ObjectSuccessfully()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            Assert.Equal(AttendanceDeviceTypeFactory.Object.Id, _attendanceDeviceTypeService.GetAttendanceDeviceType(AttendanceDeviceTypeFactory.Object.Id).Id);
        }

        #endregion

        #region Multiple Object Test
        #endregion
    }
}
