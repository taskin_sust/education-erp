﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentExamDao : IBaseDao<StudentExam, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        StudentExam GetByExamName(string examName);
        #endregion

        #region List Loading Function
        IList<StudentExam> LoadStudentExam(bool isBoard);
        #endregion

        #region Others Function

        #endregion
    }
    public class StudentExamDao : BaseDao<StudentExam, long>, IStudentExamDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<StudentExam> LoadStudentExam(bool isBoard)
        {
            ICriteria criteria = Session.CreateCriteria<StudentExam>()
               .Add(Restrictions.Eq("Status", StudentExam.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("IsBoard", isBoard));
            return criteria.List<StudentExam>();
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion


        public StudentExam GetByExamName(string examName)
        {
            return
                     Session.QueryOver<StudentExam>()
                         .Where(
                             Restrictions.Eq(
                                 Projections.SqlFunction("lower", NHibernateUtil.String,
                                     Projections.Property<StudentExam>(x => x.Name)), examName.ToLower())).SingleOrDefault<StudentExam>();

            //return
            //    Session.QueryOver<StudentExam>()
            //        .Where(x => x.Name.IsLike(examName.ToLower(),MatchMode.Start))
            //        .SingleOrDefault<StudentExam>();

            //Session.QueryOver<StudentExam>()
            //    .Where(x => x.Name.Substring(0, 3).ToLower() == examName.Trim().ToLower())
            //    .SingleOrDefault<StudentExam>();
        }
    }
}
