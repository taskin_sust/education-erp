﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using log4net;
using NHibernate;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr.AttendanceSummaryServices;

namespace UdvashERP.Services.Hr
{
    public interface IAttendanceDeviceRawDataService : IBaseService
    {
        #region Operational Function

        bool AttendanceDeviceSynchronizations(List<AttendanceDeviceRawDataViewModel> attendanceSynViewModel, long userId, string requestIp);
        bool AttendanceDeviceErrorPinUpdate(List<AttendanceDeviceErrorPinViewModel> attendanceErrorPinViewModels, long userId);
        bool IsDelete(long id, string remarks, int status);
        void Save(IList<AttendanceDeviceRawData> rowList);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        List<AttendanceDeviceRawData> AttendanceReportByPinDate(int pin, DateTime orgStartTime, DateTime orgEndTime, bool status = true);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion


    }
    public class AttendanceDeviceRawDataService : BaseService, IAttendanceDeviceRawDataService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IAttendanceDeviceSynchronizationDao _attendanceDeviceSynchronizationDao;
        private readonly IAttendanceDeviceErrorPinDao _attendanceDeviceErrorPinDao;
        private readonly IAttendanceDeviceDao _attendanceDeviceDao;
        private readonly IMemberOfficialDetailDao _officialDetailDao;
        private readonly AttendanceSummaryServiceHelper _attendanceSummaryServiceHelper;
        private readonly ITeamMemberDao _teamMemberDao;

        public AttendanceDeviceRawDataService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _attendanceDeviceSynchronizationDao = new AttendanceDeviceSynchronizationDao() { Session = session };
            _attendanceDeviceErrorPinDao = new AttendanceDeviceErrorPinDao() { Session = session };
            _attendanceDeviceDao = new AttendanceDeviceDao() { Session = session };
            _officialDetailDao = new MemberOfficialDetailDaoDao() { Session = session };
            _attendanceSummaryServiceHelper = new AttendanceSummaryServiceHelper(session);
            _teamMemberDao = new TeamMemberDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public void Save(AttendanceDeviceRawData obj)
        {
            ITransaction transaction = null;
            try
            {
                if (obj == null)
                {
                    throw new NullObjectException("Attendance Device Synchronization can not be null");
                }
                var validationContext = new ValidationContext(obj, null, null);
                var validationResults = new List<ValidationResult>();
                var isValid = false;
                isValid = Validator.TryValidateObject(obj, validationContext, validationResults, true);

                if (!isValid)
                {
                    string errorMessage = "";
                    validationResults.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                    throw new MessageException(errorMessage);
                }
                using (transaction = Session.BeginTransaction())
                {
                    _attendanceDeviceSynchronizationDao.Save(obj);
                    transaction.Commit();
                }
            }

            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }

        }

        public bool AttendanceDeviceSynchronizations(List<AttendanceDeviceRawDataViewModel> attendanceSynViewModels, long userId, string requestIp)
        {
            ITransaction transaction = null;
            //Dictionary<int, DateTime> userList = new Dictionary<int, DateTime>();
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var listModel in attendanceSynViewModels)
                    {
                        //AttendanceDevice attendanceDevice = _attendanceDeviceDao.GetDevice("", Convert.ToInt64(listModel.DeviceId));
                        AttendanceDevice attendanceDevice = _attendanceDeviceDao.LoadById(Convert.ToInt64(listModel.DeviceId));
                        //MemberOfficialDetail memberOfficialDetail = _officialDetailDao.GetMemberByCardNumber(listModel.CardNumber);
                        //if (memberOfficialDetail != null)
                        //{
                        //bool checkDuplicate = _attendanceDeviceSynchronizationDao.CheckDuplicate(memberOfficialDetail.TeamMember.Pin, listModel.PunchTime);
                        bool checkDuplicate = _attendanceDeviceSynchronizationDao.CheckDuplicate(Convert.ToInt32(listModel.Pin), listModel.PunchTime);
                        if (!checkDuplicate)
                        {
                            var model = new AttendanceDeviceRawData
                            {
                                //Pin = memberOfficialDetail.TeamMember.Pin,
                                Pin = Convert.ToInt32(listModel.Pin),
                                PunchTime = listModel.PunchTime,
                                //SynchonizerKey = attendanceDevice.AttendanceSynchronizer.SynchronizerKey,
                                //Din = listModel.DeviceNo,
                                //CardNumber = listModel.CardNumber,
                                RequestIp = requestIp,
                                AttendanceDevice = attendanceDevice,
                                //DeviceCallingInterval = attendanceDevice.AttendanceSynchronizer.DataCallingInterval,
                                CreateBy = userId,
                                ModifyBy = userId,
                                Status = AttendanceDeviceRawData.EntityStatus.Active
                            };
                            _attendanceDeviceSynchronizationDao.Save(model);
                            bool res = _attendanceSummaryServiceHelper.UpdateAttendenceSummeryByPinAndDateTime(Convert.ToInt32(listModel.Pin), listModel.PunchTime);
                        }
                        //}
                    }

                    transaction.Commit();
                    //foreach (var i in userList)
                    //{
                    //    bool res = _attendanceSummaryServiceHelper.UpdateAttendenceSummeryByPinAndDateTime(i.Key, i.Value);
                    //}
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

        }

        public bool AttendanceDeviceErrorPinUpdate(List<AttendanceDeviceErrorPinViewModel> attendanceErrorPinViewModels, long userId)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var listModel in attendanceErrorPinViewModels)
                    {
                        AttendanceDevice attendanceDevice = _attendanceDeviceDao.LoadById(Convert.ToInt64(listModel.DeviceId));
                        bool checkDuplicate = _attendanceDeviceErrorPinDao.CheckDuplicate(Convert.ToInt32(listModel.Pin), Convert.ToInt64(listModel.DeviceId));
                        if (!checkDuplicate)
                        {
                            var model = new AttendanceDeviceErrorPin
                            {
                                Pin = Convert.ToInt32(listModel.Pin),
                                AttendanceDevice = attendanceDevice,
                                CreateBy = userId,
                                ModifyBy = userId,
                                Status = AttendanceDeviceRawData.EntityStatus.Active
                            };
                            _attendanceDeviceErrorPinDao.Save(model);
                        }
                    }
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return false;
        }

        public bool IsDelete(long id, string remarks, int status)
        {
            ITransaction transaction = null;
            try
            {
                var obj =
                    Session.QueryOver<AttendanceDeviceRawData>()
                        .Where(x => x.Id == id)
                        .SingleOrDefault<AttendanceDeviceRawData>();
                ValidationAttendanceDelete(obj);
                using (transaction = Session.BeginTransaction())
                {

                    if (obj == null)
                        throw new InvalidDataException("Wrong attendance data");
                    if (status == AttendanceDeviceRawData.EntityStatus.Delete)
                        obj.Status = AttendanceDeviceRawData.EntityStatus.Active;
                    else if (status == AttendanceDeviceRawData.EntityStatus.Active)
                        obj.Status = AttendanceDeviceRawData.EntityStatus.Delete;
                    obj.Remarks = remarks;
                    obj.ModifyBy = GetCurrentUserId();
                    obj.ModificationDate = DateTime.Now;
                    Session.SaveOrUpdate(obj);
                    _attendanceSummaryServiceHelper.UpdateAttendenceSummeryByPinAndDateTime(obj.Pin, obj.PunchTime);
                    transaction.Commit();
                }
                return true;
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public List<AttendanceDeviceRawData> AttendanceReportByPinDate(int pin, DateTime orgStartTime, DateTime orgEndTime, bool status = true)
        {
            try
            {
                return (List<AttendanceDeviceRawData>)_attendanceDeviceSynchronizationDao.LoadByPinAndDateRange(pin, orgStartTime, orgEndTime, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public void Save(IList<AttendanceDeviceRawData> logList)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var attendanceDeviceLogData in logList)
                    {
                        _attendanceDeviceSynchronizationDao.Save(attendanceDeviceLogData);
                    }
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        #endregion

        #region Others Function


        #endregion

        #region Helper function
        private void ValidationAttendanceDelete(AttendanceDeviceRawData attendanceDeviceRawData)
        {
            DateTime attendanceDate = attendanceDeviceRawData.PunchTime;
            DateTime punchTime = attendanceDeviceRawData.PunchTime;

            var organization = _teamMemberDao.GetTeamMemberOrganization(punchTime, null, attendanceDeviceRawData.Pin);
            DateTime orgStartTime =
                             Convert.ToDateTime(punchTime.ToString("yyyy-MM-dd") + " " +
                                                organization.AttendanceStartTime.ToString("HH:mm"));
            //DateTime orgEndTime =
            //    Convert.ToDateTime(
            //        punchTime.AddDays(1).ToString("yyyy-MM-dd") + " " +
            //        organization.AttendanceStartTime.AddMinutes(-1).ToString("HH:mm"));


            attendanceDate = Convert.ToDateTime(punchTime < orgStartTime ? attendanceDate.AddDays(-1).ToString("yyyy-MM-dd") : attendanceDate.ToString("yyyy-MM-dd"));

            var teamMmberAttendanceAdjustmentInfo = _attendanceDeviceSynchronizationDao.GetTeamMemberAttendanceAdjustmentInfo(attendanceDate, attendanceDeviceRawData.Pin);

            if (teamMmberAttendanceAdjustmentInfo == null) return;
            if (teamMmberAttendanceAdjustmentInfo.DayOffStatus >= 0)
                throw new DependencyException("Attendence Data can't delete. Dayoff adjustment already approve for this date.");
            if (teamMmberAttendanceAdjustmentInfo.HolyDayApprovalType >= 0)
                throw new DependencyException("Attendence Data can't delete. Holyday work already approve for this date.");
            if (teamMmberAttendanceAdjustmentInfo.NightWorkApprovalType >= 0)
                throw new DependencyException("Attendence Data can't delete. Nightwork already approve for this date.");
            if (teamMmberAttendanceAdjustmentInfo.OverTimeApproveTime >= 0)
                throw new DependencyException("Attendence Data can't delete. Overtime already approve for this date.");
        }
        #endregion

    }
}
