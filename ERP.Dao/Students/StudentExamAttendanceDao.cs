﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Dto;

namespace UdvashERP.Dao.Students
{
    public interface IStudentExamAttendanceDao : IBaseDao<StudentExamAttendance, long>
    {
        #region Operational Function
        bool ClearExamAttendace(string examIdList,string userIds, DateTime dateFrom, DateTime dateTo);
        #endregion

        #region Single Instances Loading Function
        List<StudentExamAttendance> LoadExamAttendance(long examId, DateTime heldDate);
        #endregion

        #region List Loading Function

        IList<ExamEvaluationReportDto> LoadExamEvaluationDto(int start, int length, string orderby, string orderDir, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList );
        IList<StudentExamAttendanceDetails> LoadExamEvaluationForIndividualReport(int start, int length, List<long> authorizedProgramIdList, long sessionId, List<long> authorizedBranchIdList, List<long> campusIdList, List<long> batchIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, string batchIds);
        
        #endregion

        #region Others Function
        //int LoadExamAttendanceByDateAndBatchAndStudentProgram(DateTime heldDate, long batchId, long studentProgramId, long examId);
        //int GetExamEvaluationCountDto(int start, int length, string orderby, string orderDir, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList);
        int CountRowCountForIndividualExamEvaluationReport(List<long> authorizedProgramIdList, long sessionId, List<long> authorizedBranchIdList, List<long> campusIdList, List<long> batchIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, string batchIds, string userIds=null);
        int ExamAttendanceCount(long examId, long studentProgramId);
        IList<ExamAttendanceReportDto> LoadExamAttendanceReport(int start, int length, long programId, long sessionId, List<long> programIdList, List<long> branchIdList, long[] courseId, DateTime startDate, DateTime endDate, string batchIds, long[] paymentStatus, long attendanceStatus,long[]examIds);
        int GetExamAttendanceCount(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus, long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds, long[] courseIds,long[]examIds);
        int GetStudentCount(List<long> authorizedProgramIdList, long programId, long sessionId, DateTime dateFrom, DateTime dateTo, List<long> examIdlist, string userIds);
        bool IsRegisteredExam(long studentProgramId, long examId,DateTime heldDate);
        #endregion

        //removed Code
        //IList<IGrouping<long, StudentExamAttendance>> LoadExamEvaluationByExamId(long[] selectedExam, int start, int length);
        //IList<IGrouping<long, StudentExamAttendance>> LoadExamEvaluation(int start, int length, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList);
        //IList<StudentExamAttendance> LoadExamEvaluationByExamIdForIndividualReport(long[] selectedExam, int start,int length);
        //int CountRowCountForExamEvaluationReport(long[] examIds);
        //int CountRowCountForIndividualExamEvaluationReport(long[] examIds);







        int GetExamEvaluationCountDto(int start, int length, string orderby, string orderDir, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIds, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList);
        int GetCountOfAttendExam(long subjectId, long stdProgramId, long courseId);
    }

    public class StudentExamAttendanceDao : BaseDao<StudentExamAttendance, long>, IStudentExamAttendanceDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        public bool ClearExamAttendace(string examIdList,string userIds, DateTime dateFrom, DateTime dateTo)
        {
            ITransaction trans = null;
            try
            {

                using (trans = Session.BeginTransaction())
                {
                    var deleteQuery =
                        "delete  from StudentExamAttendanceDetails where StudentExamAttendanceId in(select distinct id from StudentExamAttendance where ExamId in(" +
                         examIdList + ") and createby in(" +
                        userIds + ") and HeldDate>='" + dateFrom + "' and HeldDate<'" +
                        dateTo.AddDays(1) + "' );delete from studentExamAttendance where ExamId in(" +
                         examIdList + ") and  createby in(" +
                        userIds + ") and HeldDate>='" + dateFrom + "' and HeldDate<'" +
                        dateTo.AddDays(1) + "'";
                    Session.CreateSQLQuery(deleteQuery).SetTimeout(5000)
                    .UniqueResult();
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                return false;
            } 
        }
        #endregion

        #region Single Instances Loading Function

        public List<StudentExamAttendance> LoadExamAttendance(long examId, DateTime heldDate)
        {
            var examAttendance =
                Session.QueryOver<StudentExamAttendance>()
                    .Where(x => x.Exams.Id == examId && x.HeldDate == heldDate && x.Status==1)
                    .List<StudentExamAttendance>();
            return examAttendance.ToList();
        }
        #endregion

        #region List Loading Function
        public IList<ExamEvaluationReportDto> LoadExamEvaluationDto(int start, int length, string orderby, string orderDir, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList)
        {
            string query = GetExamEvaluationQuery(start, length, orderby,orderDir, programId, sessionId, courseIdList, branchIdList, campusIdList, batchIdList, subjectIdList, fromDate, toDate, examIdList);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ExamEvaluationReportDto>());
            return iQuery.List<ExamEvaluationReportDto>().ToList();
        }
        public IList<StudentExamAttendanceDetails> LoadExamEvaluationForIndividualReport(int start, int length, List<long> authorizedProgramId, long sessionId, List<long> authorizedBranchIdList, List<long> campusIdList, List<long> batchIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, string batchIds)
        {
            ICriteria criteria = GetIndividualExamEvaluationReportIcriteria(authorizedProgramId, sessionId, authorizedBranchIdList, campusIdList, batchIdList, fromDate, toDate, examIdList,batchIds);
            if (length >= 0)
            {
                return criteria.SetFirstResult(start).SetMaxResults(length).List<StudentExamAttendanceDetails>();
            }
            return criteria.List<StudentExamAttendanceDetails>();
        }

        //public int LoadExamAttendanceByDateAndBatchAndStudentProgram(DateTime heldDate, long batchId, long studentProgramId, long examId)
        //{
        //    return
        //        Session.QueryOver<StudentExamAttendance>()
        //            .Where(x => x.HeldDate == heldDate && x.Batch.Id == batchId && x.StudentProgram.Id == studentProgramId && x.Exams.Id == examId && x.Status == StudentExamAttendance.EntityStatus.Active)
        //            .List<StudentExamAttendance>().Count;
        //}
        #endregion

        #region Others Function

        public int GetExamEvaluationCountDto(int start, int length, string orderby, string orderDir, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIds, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList)
        {
            string query = GetExamEvaluationQuery(start, length, orderby, orderDir, programId, sessionId, courseIdList, branchIdList, campusIdList, batchIds, subjectIdList, fromDate, toDate, examIdList, true);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int GetCountOfAttendExam(long subjectId, long stdProgramId, long courseId)
        {
            var query = @"select count(*) from [dbo].[StudentExamAttendanceDetails]  as sead
                            inner join [dbo].[StudentExamAttendance] as sea on sead.StudentExamAttendanceId = sea.Id
                            inner join [dbo].[Exams] as e on e.id=sea.ExamId
                            inner join [dbo].[ExamsDetails] as ed on ed.ExamId = e.Id
                            where sead.StudentProgramId=" + stdProgramId + " and e.CourseId=" + courseId + " and ed.SubjectId=" + subjectId + "";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int CountRowCountForIndividualExamEvaluationReport(List<long> authorizedProgramIdList, long sessionId, List<long> authorizedBranchIdList, List<long> campusIdList, List<long> batchIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, string batchIds, string userIds=null)
        {
            ICriteria criteria = GetIndividualExamEvaluationReportIcriteria(authorizedProgramIdList, sessionId, authorizedBranchIdList, campusIdList, batchIdList, fromDate, toDate, examIdList,batchIds,userIds);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public int ExamAttendanceCount(long examId, long studentProgramId)
        {
            var classAttendance = Session.Query<StudentExamAttendanceDetails>()
                    .Where(
                        x =>
                            x.StudentProgram.Id == studentProgramId &&
                            x.StudentExamAttendance.Exams.Id == examId);
            return classAttendance.Count();
        }

        public IList<ExamAttendanceReportDto> LoadExamAttendanceReport(int start, int length, long programId, long sessionId,
            List<long> programIdList, List<long> branchIdList, long[] courseId, DateTime startDate, DateTime endDate,
            string batchIds, long[] paymentStatus, long attendanceStatus, long[] examIds)
        {
            var query = @"with studentProgramCTEE as ( select
   distinct Id,
   prnNo,
   batchid 
from
   studentprogram 
where
   id in(select sp.id from studentprogram sp inner join  batch b on sp.batchid=b.id where

sp.programid={0} and sp.programid in({1}) and b.sessionid={2} and b.branchid in({3}) and b.Id in ({6})";
            if (!paymentStatus.Contains(0))
            {
                if (paymentStatus.Contains(1))
                {
                    query += " and dueamount=0";
                }
                if (paymentStatus.Contains(2))
                {
                    query += " and dueamount>0";
                }
            }

            query += @" )) select
   x.id as StdProId,
   x.PrnNo,
   LectureHeld,
   CAST(COALESCE(RegisteredPresent,
   0) as int)+CAST(COALESCE(UnregisteredPresent,
   0) as int) LectureAttend,
   COALESCE(RegisteredPresent,
   0)RegisteredPresent,
   COALESCE(UnregisteredPresent,
   0)UnregisteredPresent,
   (COALESCE(RegisteredAbsent,
      0)) +
	  (COALESCE(RegisteredAbsent2,
      0))+(COALESCE(complementaryAbsent,
      0))
	  RegisteredAbsent
from
   (select
      swcp.prnNo,
      swcp.id,
      count(distinct cp.examid) as RegisteredAbsent 
   from
      studentProgramCTEE swcp 
   left join
      examprogress cp 
         on swcp.batchid=cp.batchid 
--and cp.batchid  =(select batchid from studentprogram where id=swcp.BatchId) 
 and cp.batchid  in(select BatchId from studentprogram as sp where sp.BatchId=swcp.BatchId)  
         and cp.heldDate>='{4}' 
         and cp.heldDate<'{5}' 
         and cp.examid not in (
            select
               sca.examid 
         from
            [dbo].[StudentExamAttendanceDetails] scad 
         inner join
            [dbo].[StudentExamAttendance] sca 
               on scad.[StudentExamAttendanceId]=sca.id 
               and scad.studentprogramid=swcp.id 
         where
            sca.heldDate>='{4}'
            and sca.heldDate<'{5}'
and sca.Status=1 and scad.Status=1
      ) ";
//      and exists (select 1 from StudentExamAttendance sca inner join StudentExamAttendanceDetails scad on sca.Id=scad.StudentExamAttendanceId
//inner join Exams e on sca.ExamId=e.Id inner join ExamsDetails ed on ed.ExamId=e.Id inner join StudentCourseDetails scd on scd.StudentProgramId=scad.StudentProgramId
//--inner join CourseSubject cs on scd.CourseSubjectId=cs.id and cs.CourseId=e.CourseId and cs.SubjectId=ed.SubjectId
//where scad.StudentProgramId=swcp.Id and scd.StudentProgramId=swcp.Id and sca.Status=1 and scad.Status=1)";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.courseid in (" + string.Join(", ", courseId) + ")";
            }
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @" group by
      swcp.id,
      swcp.prnNo) x 
   

left join
            (
               select
         swcp.prnNo,
         swcp.id,
         count(distinct cp.examid) as RegisteredAbsent2      
      from
         studentProgramCTEE swcp      
      left join
         examprogress cp            
            on swcp.batchid=cp.batchid 
                    
            and cp.heldDate>='{4}'           
            and cp.heldDate<'{5}'         
            and cp.examid not in (
               select
                  sca.examid            
            from
               [dbo].[StudentExamAttendanceDetails] scad            
            inner join
               [dbo].[StudentExamAttendance] sca                  
                  on scad.[StudentExamAttendanceId]=sca.id                  
                  and scad.studentprogramid=swcp.id            
            where
               sca.heldDate>='{4}'              
               and sca.heldDate<'{5}'  
and sca.Status=1 and scad.Status=1      
         )         
         and exists(select 1 from ExamsDetails ed where ed.ExamId=cp.ExamId and ed.SubjectType=3)";

            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.courseid in (" + string.Join(", ", courseId) + ")";
            }
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @" group by
            swcp.id,
            swcp.prnNo
			   )zyx            
                  on x.id=zyx.id
 --complementary absent

				  left join
            (
               select
         swcp.prnNo,
         swcp.id,
         count(distinct cp.examid) as complementaryAbsent   
      from
         studentProgramCTEE swcp      
      left join
         examprogress cp            
            on swcp.batchid=cp.batchid            
            and cp.heldDate>='{4}'            
            and cp.heldDate<'{5}'            
            and cp.examid not in (
               select
                  sca.examid            
            from
               [dbo].[StudentExamAttendanceDetails] scad            
            inner join
               [dbo].[StudentExamAttendance] sca                  
                  on scad.[StudentExamAttendanceId]=sca.id                  
                  and scad.studentprogramid=swcp.id            
            where
               sca.heldDate>='{4}'              
               and sca.heldDate<'{5}' 
and sca.Status=1 and scad.Status=1       
         )         
         and cp.courseid in (
            select compc.CompCourseId from studentcoursedetails scd inner join CourseSubject cs on scd.CourseSubjectId=cs.Id inner join Course c on cs.CourseId=c.Id 
			left join ComplementaryCourse compc on c.Id=compc.CourseId
			where scd.studentprogramid=swcp.id and scd.status=1 and cs.Status=1 and c.Status=1 and compc.Status=1
			 ) ";

            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += "and cp.courseid in (" + string.Join(", ", courseId) + ")";
            }
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @" group by
            swcp.id,
            swcp.prnNo
			   )zyxx            
                  on x.id=zyxx.id

--complementary absent






left join
      (
         select
            scad.studentprogramid,
            count(*) as RegisteredPresent 
         from
            [dbo].[StudentExamAttendanceDetails] scad 
         inner join
            [dbo].[StudentExamAttendance] sca 
               on scad.[StudentExamAttendanceId]=sca.id ";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " inner join exams e on e.id=sca.examid and e.courseid in (" + string.Join(", ", courseId) + ")";
            }
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and sca.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @" where
            scad.isregistered=1 
and sca.Status=1 and scad.Status=1            
and sca.heldDate>='{4}'
            and sca.heldDate<'{5}'  
            and exists (select 1 from ExamProgress where ExamId=sca.ExamId and Status=1)           
            group by
            scad.studentprogramid
      )y 
         on x.id=y.studentprogramid 
   left join
      (
         select
            scad.studentprogramid,
            count(*) as UnregisteredPresent 
         from
            [dbo].[StudentExamAttendanceDetails] scad 
         inner join
            [dbo].[StudentExamAttendance] sca 
               on scad.[StudentExamAttendanceId]=sca.id ";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " inner join exams e on e.id=sca.examid and e.courseid in (" + string.Join(", ", courseId) + ")";
            }
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and sca.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @" where
            scad.isregistered=0 
and sca.Status=1 and scad.Status=1
            and sca.heldDate>='{4}'
            and sca.heldDate<'{5}'
            and exists (select 1 from ExamProgress where ExamId=sca.ExamId and Status=1)           
            group by
            scad.studentprogramid
      )z 
         on x.id=z.studentprogramid 
   left join
      (
         select
            sp.id spid,
            COALESCE(classHeld,
            0) as LectureHeld 
         from
            studentProgramCTEE sp 
         left join
            (
               select
                  cp.batchid,
                  count(*) classHeld 
               from
                  examprogress cp  ";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " inner join exams e on e.id=cp.examid and e.courseid in (" + string.Join(", ", courseId) + ")";
            }

            query += @" where
                  cp.heldDate>='{4}' 
                  and cp.heldDate<'{5}'";
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @"   group by
                  cp.batchid
            )x 
               on sp.batchid=x.batchid 
            )p 
               on p.spid=x.id where 1=1";

            if (!paymentStatus.Contains(0))
            {
                if (paymentStatus.Contains(3))
                {
                    query += " and unregisteredpresent>0";
                }

            }
            if (attendanceStatus != 0)
            {
                if (attendanceStatus == 1)
                {
                    query += " and registeredpresent>0";
                }
                if (attendanceStatus == 2)
                {
                    query += " and registeredabsent>0";
                }
                if (attendanceStatus == 3)
                {
                    query += " and unregisteredpresent>0";
                }
            }
            query += @" order by
            prnNo";
            if (length > 0)
            {
                query += " offset " + start + " rows fetch next " + length +
                        "rows only ;";
            }

            var attendanceReport = new List<ExamAttendanceReportDto>();
            string endQuery = String.Format(query, programId, string.Join(", ", programIdList), sessionId, string.Join(", ", branchIdList), startDate, endDate.AddDays(1), batchIds);
            IQuery iQuery = Session.CreateSQLQuery(endQuery);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ExamAttendanceReportDto>()).SetTimeout(2700);
            attendanceReport = iQuery.List<ExamAttendanceReportDto>().ToList();

            return attendanceReport;
        }

        public int GetExamAttendanceCount(List<long> programIdList, List<long> branchIdList, long programId,
            long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchList,
            long[] paymentStatus, long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds,
            long[] courseIds, long[] examIds)
        {
            var query = @"with studentProgramCTEE as ( select
   distinct Id,
   prnNo,
   batchid 
from
   studentprogram 
where
   id in(select sp.id from studentprogram sp inner join  batch b on sp.batchid=b.id where

sp.programid={0} and sp.programid in({1}) and b.sessionid={2} and b.branchid in({3}) and b.Id in ({6})";
            if (!paymentStatus.Contains(0))
            {
                if (paymentStatus.Contains(1))
                {
                    query += " and dueamount=0";
                }
                if (paymentStatus.Contains(2))
                {
                    query += " and dueamount>0";
                }
            }

            query += @" )) select
   count(x.id) 
from
   (select
      swcp.prnNo,
      swcp.id,
      count(distinct cp.examid) as RegisteredAbsent 
   from
      studentProgramCTEE swcp 
   left join
      examprogress cp 
         on swcp.batchid=cp.batchid 
--and cp.batchid  =(select batchid from studentprogram where id=swcp.BatchId) 
and cp.batchid  in(select BatchId from studentprogram as sp where sp.BatchId=swcp.BatchId) 
         and cp.heldDate>='{4}' 
         and cp.heldDate<'{5}' 
         and cp.examid not in (
             
            select
               sca.examid 
         from
            [dbo].[StudentExamAttendanceDetails] scad 
         inner join
            [dbo].[StudentExamAttendance] sca 
               on scad.[StudentExamAttendanceId]=sca.id 
               and scad.studentprogramid=swcp.id 
         where
           sca.heldDate>='{4}'
            and sca.heldDate<'{5}'
and sca.Status=1 and scad.Status=1
     ) ";
//      and exists (select 1 from StudentExamAttendance sca inner join StudentExamAttendanceDetails scad on sca.Id=scad.StudentExamAttendanceId
//inner join Exams e on sca.ExamId=e.Id inner join ExamsDetails ed on ed.ExamId=e.Id inner join StudentCourseDetails scd on scd.StudentProgramId=scad.StudentProgramId
//--inner join CourseSubject cs on scd.CourseSubjectId=cs.id and cs.CourseId=e.CourseId and cs.SubjectId=ed.SubjectId
//where scad.StudentProgramId=swcp.Id and scd.StudentProgramId=swcp.Id and sca.Status=1 and scad.Status=1)";
            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @" group by
      swcp.id,
      swcp.prnNo) x 
   

left join
            (
               select
         swcp.prnNo,
         swcp.id,
         count(distinct cp.examid) as RegisteredAbsent2      
      from
         studentProgramCTEE swcp      
      left join
         examprogress cp            
            on swcp.batchid=cp.batchid 
                    
            and cp.heldDate>='{4}'           
            and cp.heldDate<'{5}'         
            and cp.examid not in (
               select
                  sca.examid            
            from
               [dbo].[StudentExamAttendanceDetails] scad            
            inner join
               [dbo].[StudentExamAttendance] sca                  
                  on scad.[StudentExamAttendanceId]=sca.id                  
                  and scad.studentprogramid=swcp.id            
            where
               sca.heldDate>='{4}'              
               and sca.heldDate<'{5}' 
and sca.Status=1 and scad.Status=1       
         )         
         and exists(select 1 from ExamsDetails ed where ed.ExamId=cp.ExamId and ed.SubjectType=3)";

            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @" group by
            swcp.id,
            swcp.prnNo
			   )zyx            
                  on x.id=zyx.id
 --complementary absent

				  left join
            (
               select
         swcp.prnNo,
         swcp.id,
         count(distinct cp.examid) as complementaryAbsent   
      from
         studentProgramCTEE swcp      
      left join
         examprogress cp            
            on swcp.batchid=cp.batchid            
            and cp.heldDate>='{4}'            
            and cp.heldDate<'{5}'            
            and cp.examid not in (
               select
                  sca.examid            
            from
               [dbo].[StudentExamAttendanceDetails] scad            
            inner join
               [dbo].[StudentExamAttendance] sca                  
                  on scad.[StudentExamAttendanceId]=sca.id                  
                  and scad.studentprogramid=swcp.id            
            where
               sca.heldDate>='{4}'              
               and sca.heldDate<'{5}'
and sca.Status=1 and scad.Status=1        
         )         
         and cp.courseid in (
            select compc.CompCourseId from studentcoursedetails scd inner join CourseSubject cs on scd.CourseSubjectId=cs.Id inner join Course c on cs.CourseId=c.Id 
			left join ComplementaryCourse compc on c.Id=compc.CourseId
			where scd.studentprogramid=swcp.id and scd.status=1 and cs.Status=1 and c.Status=1 and compc.Status=1
			 ) ";

            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += "and cp.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @" group by
            swcp.id,
            swcp.prnNo
			   )zyxx            
                  on x.id=zyxx.id

--complementary absent






left join
      (
         select
            scad.studentprogramid,
            count(*) as RegisteredPresent 
         from
            [dbo].[StudentExamAttendanceDetails] scad 
         inner join
            [dbo].[StudentExamAttendance] sca 
               on scad.[StudentExamAttendanceId]=sca.id ";
            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += " inner join exams e on e.id=sca.examid and e.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and sca.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @" where
            scad.isregistered=1 
and sca.Status=1 and scad.Status=1            
and sca.heldDate>='{4}'
            and sca.heldDate<'{5}'  
and exists (select 1 from ExamProgress where ExamId=sca.ExamId and Status=1)           
group by
            scad.studentprogramid
      )y 
         on x.id=y.studentprogramid 
   left join
      (
         select
            scad.studentprogramid,
            count(*) as UnregisteredPresent 
         from
            [dbo].[StudentExamAttendanceDetails] scad 
         inner join
            [dbo].[StudentExamAttendance] sca 
               on scad.[StudentExamAttendanceId]=sca.id ";
            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += " inner join exams e on e.id=sca.examid and e.courseid in (" + string.Join(", ", courseIds) + ")";
            }
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and sca.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @" where
           
scad.isregistered=0 
and sca.Status=1 and scad.Status=1 
            and sca.heldDate>='{4}'
            and sca.heldDate<'{5}'
and exists (select 1 from ExamProgress where ExamId=sca.ExamId and Status=1)          
group by
            scad.studentprogramid
      )z 
         on x.id=z.studentprogramid 
   left join
      (
         select
            sp.id spid,
            COALESCE(classHeld,
            0) as LectureHeld 
         from
            studentProgramCTEE sp 
         left join
            (
               select
                  cp.batchid,
                  count(*) classHeld 
               from
                  examprogress cp  ";
            if (!courseIds.Contains(SelectionType.SelelectAll))
            {
                query += " inner join exams e on e.id=cp.examid and e.courseid in (" + string.Join(", ", courseIds) + ")";
            }

            query += @" where
                  cp.heldDate>='{4}' 
                  and cp.heldDate<'{5}'";
            if (!examIds.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.examid in (" + string.Join(", ", examIds) + ")";
            }
            query += @"   group by
                  cp.batchid
            )x 
               on sp.batchid=x.batchid 
            )p 
               on p.spid=x.id where 1=1";

            if (!paymentStatus.Contains(0))
            {
                if (paymentStatus.Contains(3))
                {
                    query += " and unregisteredpresent>0";
                }

            }
            if (attendanceStatus != 0)
            {
                if (attendanceStatus == 1)
                {
                    query += " and registeredpresent>0";
                }
                if (attendanceStatus == 2)
                {
                    query += " and registeredabsent>0";
                }
                if (attendanceStatus == 3)
                {
                    query += " and unregisteredpresent>0";
                }
            }
            string endQuery = String.Format(query, programId, string.Join(", ", programIdList), sessionId, string.Join(", ", branchIdList), startDate, endDate.AddDays(1), batchIds);
            IQuery iQuery = Session.CreateSQLQuery(endQuery);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int GetStudentCount(List<long> authorizedProgramIdList, long programId, long sessionId, DateTime dateFrom,
            DateTime dateTo, List<long> examIdlist, string userIds)
        {
            var query = @"select  count(distinct sead.StudentProgramId) from Exams e inner join StudentExamAttendance sea on e.Id=sea.ExamId
inner join StudentExamAttendanceDetails sead on sea.Id=sead.StudentExamAttendanceId                          
where e.ProgramId={0} and e.ProgramId in({5}) and e.id in({6}) and e.SessionId={1} 
--and e.CourseId in({2})
                            and e.Status=1 and sea.Status=1 
                            and sea.HeldDate>='{2}' and sea.HeldDate<'{3}' and sea.createby in ({4})";
            query = string.Format(query, programId, sessionId, dateFrom, dateTo.AddDays(1), userIds, string.Join(",", authorizedProgramIdList), string.Join(",", examIdlist));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public bool IsRegisteredExam(long studentProgramId, long examId,DateTime heldDate)
        {
            var query = @"declare @c1 int;set @c1=(select 1 where exists(select 1 from Exams e inner join ExamsDetails ed on ed.ExamId=e.Id 
                        inner join CourseSubject cs on cs.CourseId=e.CourseId and cs.SubjectId=ed.SubjectId
                        inner join StudentCourseDetails scd on scd.CourseSubjectId=cs.Id
                        and scd.StudentProgramId={0}
                        and scd.Status=1
                        and e.Id={1}
                        ))
                        declare @c2 int;set @c2=(select 1 where exists(select 1 from Exams e inner join ExamsDetails ed on ed.ExamId=e.Id 
                        and e.Id={1}
                        and e.CourseId in(
                        select   compc.CompCourseId from studentcoursedetails scd 
                        inner join
                           CourseSubject cs 
                              on scd.CourseSubjectId=cs.Id 
                        inner join
                           Course c 
                              on cs.CourseId=c.Id      
                        left join
                           ComplementaryCourse compc 
                              on c.Id=compc.CourseId     
                        where
                           scd.studentprogramid={0}
                           and scd.status=1 
                           and cs.Status=1 
                           and c.Status=1 
                           and compc.Status=1
                        )
                        ))
                        declare @c3 int;set @c3=(select 1 where exists( select 1 from Exams e inner join ExamsDetails ed on ed.ExamId=e.Id and ed.SubjectType=3 and e.Id={1}))
                        select coalesce(@c1,0)+coalesce(@c2,0)+coalesce(@c3,0)";
            query = string.Format(query, studentProgramId, examId);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Helper Function
        private string GetExamEvaluationQuery(int start, int length, string orderby, string orderDir, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, bool countQuery = false)
        {
            string orderByString;
            switch (orderby)
            {
                case "Total":
                    orderByString = "(CAST(a.Easy AS bigint) + CAST(a.Standard AS bigint) + CAST(a.Hard AS bigint))";
                    break;
                case "ExamName":
                    orderByString = "a.ExamName";
                    break;
                case "SubjectName":
                    orderByString = "a.SubjectName";
                    break;
                case "EasyPercentage":
                    orderByString = "CAST(((NULLIF(CAST(a.Easy as float),0)/(NULLIF(CAST(a.Easy AS bigint),0) + NULLIF(CAST(a.Standard AS bigint),0) + NULLIF(CAST(a.Hard AS bigint),0)))*100) as decimal(19,2))";
                    break;
                case "StandardPercentage":
                    orderByString = "CAST(((NULLIF(CAST(a.Standard as float),0)/(NULLIF(CAST(a.Easy AS bigint),0) + NULLIF(CAST(a.Standard AS bigint),0) + NULLIF(CAST(a.Hard AS bigint),0)))*100) as decimal(19,2))";
                    break;
                case "HardPercentage":
                    orderByString = "CAST(((NULLIF(CAST(a.Hard as float),0)/(NULLIF(CAST(a.Easy AS bigint),0) + NULLIF(CAST(a.Standard AS bigint),0) + NULLIF(CAST(a.Hard AS bigint),0)))*100) as decimal(19,2))";
                    break;
                default:
                    orderByString = "(NULLIF((CAST(a.Standard as float),0)/NULLIF((CAST(a.Easy AS bigint),0) + NULLIF(CAST(a.Standard AS bigint),0) + NULLIF(CAST(a.Hard AS bigint),0)))*100)";
                    break;
            }

            string selectedOrderByQuery = " RANK() OVER(PARTITION BY 1 ORDER BY  " + orderByString + "  " + orderDir + ", a.ExamId ASC) AS Id ";

            var mainQuery = "SELECT studentexamAttendance.ExamId " +
                ", COUNT(sead.StudentFeedback) as total " +
                ", SUM(CASE WHEN sead.StudentFeedback=3 THEN 1 ELSE 0 END) Easy " +
                ", SUM(CASE WHEN sead.StudentFeedback=1 THEN 1 ELSE 0 END) Standard " +
                ", SUM(CASE WHEN sead.StudentFeedback=2 THEN 1 ELSE 0 END) Hard " +
                ", STUFF(( SELECT DISTINCT ', ' + (CASE WHEN examDetails.SubjectId IS NOT NULL THEN  CAST(subject.Name AS VARCHAR(255))  END) [text()] FROM [dbo].[Exams] AS exam INNER JOIN [dbo].[ExamsDetails] AS examDetails ON examDetails.ExamId = exam.Id LEFT JOIN Subject AS subject ON examDetails.SubjectId = subject.Id WHERE 1=1  AND exam.Id = studentexamAttendance.ExamId FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') Subject " +
                ", MAX(exam.Name) ExamName " +
                " From [dbo].[StudentExamAttendance] as studentexamAttendance ";

            mainQuery += @" INNER JOIN
	   StudentExamAttendanceDetails sead
	  on sead.StudentExamAttendanceId=studentexamAttendance.Id
	  inner join [dbo].[Exams] as exam ON exam.Id = studentexamAttendance.ExamId 
            INNER JOIN
         [dbo].[StudentProgram] as studentProgram 
            ON studentProgram.Id = sead.StudentProgramId  
 inner join Batch b on b.id=studentProgram.BatchId
       ";

            var whereCluse = " where 1=1 and studentexamAttendance.Status=1 and sead.Status=1 " +
                             " AND (sead.StudentFeedback IS NOT NULL or sead.StudentFeedback !=0) ";
            whereCluse += " AND studentProgram.ProgramId = " + programId;
            whereCluse += " AND b.sessionId = " + sessionId;
            whereCluse += " AND studentProgram.batchId in ( " + string.Join(",",batchIdList)+")";
            
            if (!campusIdList.Contains(SelectionType.SelelectAll))
                whereCluse += " AND b.CampusId IN ( " + string.Join(", ", campusIdList) + " )";
            if (!branchIdList.Contains(SelectionType.SelelectAll))
                whereCluse += " AND b.BranchId IN ( " + string.Join(", ", branchIdList) + " )  ";
            if (!courseIdList.Contains(SelectionType.SelelectAll))
                whereCluse += " AND exam.CourseId IN ( " + string.Join(", ", courseIdList) + " )  ";
            if (!subjectIdList.Contains(SelectionType.SelelectAll))
                whereCluse += " AND EXISTS(SELECT 1 FROM [dbo].[ExamsDetails] WHERE ExamId = exam.Id AND (SubjectId in (" + string.Join(", ", subjectIdList) + ") OR SubjectId IS NULL)) ";
                //whereCluse += " AND ( examDetails.SubjectId IN (" + string.Join(", ", subjectIdList) + ")  or examDetails.SubjectId IS NULL) ";
            if (!examIdList.Contains(SelectionType.SelelectAll))
                whereCluse += "and studentexamAttendance.ExamId IN (" + string.Join(", ", examIdList) + ")";
            whereCluse += "AND (studentexamAttendance.HeldDate >= '" + fromDate.Date + "' AND studentexamAttendance.HeldDate <= '" + toDate.Date+"' ) ";

            string fullQuery = countQuery ? "SELECT COUNT(*) FROM(" : "SELECT * FROM(";

            fullQuery += "Select " +
                         selectedOrderByQuery +
                         ",a.ExamId as ExamId " +
                         ",a.ExamName as ExamName " +
                         " ,CAST (a.Easy as bigint) +"+
                         "CAST (a.Standard as bigint)+"+
                         "CAST (a.Hard as bigint) as Total" +
                         ",CAST (a.Easy as bigint) as Easy " +
                         " ,CAST (a.Standard as bigint) as Standard " +
                         ",CAST (a.Hard as bigint) as Hard " +
                         ", CASE WHEN (CAST(a.Easy AS bigint) + CAST(a.Standard AS bigint) + CAST(a.Hard AS bigint)) > 0 THEN CAST(((CAST(a.Easy as float)/(CAST(a.Easy AS bigint) + CAST(a.Standard AS bigint) + CAST(a.Hard AS bigint)))*100) as decimal(19,2)) ELSE CAST( 0 as decimal(19, 2)) END AS EasyPercentage " +
                         ", CASE WHEN (CAST(a.Easy AS bigint) + CAST(a.Standard AS bigint) + CAST(a.Hard AS bigint)) > 0 THEN CAST(((CAST(a.Standard as float)/(CAST(a.Easy AS bigint) + CAST(a.Standard AS bigint) + CAST(a.Hard AS bigint)))*100) as decimal(19,2)) ELSE CAST( 0 as decimal(19, 2)) END AS StandardPercentage " +
                         ", CASE WHEN (CAST(a.Easy AS bigint) + CAST(a.Standard AS bigint) + CAST(a.Hard AS bigint)) > 0 THEN CAST(((CAST(a.Hard as float)/(CAST(a.Easy AS bigint) + CAST(a.Standard AS bigint) + CAST(a.Hard AS bigint)))*100) as decimal(19,2)) ELSE CAST( 0 as decimal(19, 2)) END AS HardPercentage " +
                         //",a.Subject as SubjectName " +
                         " ,(CASE WHEN a.Subject IS NOT NULL THEN  CAST(a.Subject AS VARCHAR(255)) ELSE CAST('Combined' AS VARCHAR(255))  END) AS SubjectName " +
                         " from ( " +
                         mainQuery +
                         whereCluse +
                         " group by studentexamAttendance.ExamId " +
                         " ) as a " +
                         ") as a";
            if (!countQuery)
            {
                fullQuery += " WHERE Id Between " + (start + 1) + " AND " + (start + length) + " " + " ORDER BY Id ASC" + " ";
            }

            return fullQuery;
        }

        private ICriteria GetIndividualExamEvaluationReportIcriteria(List<long> authorizedProgramIdList, long sessionId, List<long> authorizedBranchIdList, List<long> campusIdList, List<long> batchIdList, DateTime fromDate, DateTime toDate, List<long> examIdList, string batchIds, string userIds=null)
        {
            ICriteria criteria = Session.CreateCriteria<StudentExamAttendanceDetails>().Add(Restrictions.Eq("Status", StudentExamAttendance.EntityStatus.Active));
            //criteria.CreateAlias("Batch", "ba");//.Add(Restrictions.Eq("ba.Status", Batch.EntityStatus.Active));
            criteria.CreateAlias("StudentProgram", "sp");//.Add(Restrictions.Eq("sp.Status", StudentProgram.EntityStatus.Active));
            criteria.CreateAlias("StudentExamAttendance", "sea").Add(Restrictions.Eq("ex.Status", Exams.EntityStatus.Active));
            criteria.CreateAlias("sea.Exams", "ex").Add(Restrictions.Eq("ex.Status", Exams.EntityStatus.Active));
            criteria.CreateAlias("sp.Batch", "ba").Add(Restrictions.Eq("ba.Status", Batch.EntityStatus.Active));
            if (!examIdList.Contains(0))
                criteria.Add(Restrictions.In("ex.Id", examIdList));

            if (!authorizedProgramIdList.Contains(0))
                criteria.Add(Restrictions.In("sp.Program.Id", authorizedProgramIdList));
            //criteria.Add(Restrictions.Eq("sp.Program.Id", programid));
            if (!batchIdList.Contains(0))
                criteria.Add(Restrictions.In("sp.Batch.Id", batchIds.Split(',').ToList()));
            criteria.Add(Restrictions.Eq("ba.Session.Id", sessionId));

            if (!authorizedBranchIdList.Contains(0))
                criteria.Add(Restrictions.Or(Restrictions.IsNull("ba.Branch"), Restrictions.In("ba.Branch.Id", authorizedBranchIdList)));
            if (!campusIdList.Contains(0))
                criteria.Add(Restrictions.Or(Restrictions.IsNull("ba.Campus"), Restrictions.In("ba.Campus.Id", campusIdList)));

           // if (!batchIdList.Contains(0))
               // criteria.Add(Restrictions.In("ba.Id", batchIdList));

            criteria.Add(Restrictions.Ge("sea.HeldDate", fromDate));
            criteria.Add(Restrictions.Le("sea.HeldDate", toDate));
            if (!string.IsNullOrEmpty(userIds))
            {
                criteria.Add(Restrictions.In("sea.CreateBy", userIds.Split(',').ToList())); 
            }
            criteria.Add(Restrictions.IsNotNull("StudentFeedback"));
            criteria.Add(Restrictions.Not((Restrictions.Eq("StudentFeedback",0))));

            return criteria;
        }

        #endregion

        //removed Code
        //public IList<StudentExamAttendance> LoadExamEvaluationByExamIdForIndividualReport(long[] selectedExam, int start, int length)
        //{
        //    var query = Session.QueryOver<StudentExamAttendance>();
        //    if (selectedExam.Length == 1 && selectedExam[0] == 0)
        //    {
        //    }
        //    else
        //    {
        //        query.AndRestrictionOn(x => x.Exams.Id).IsIn(selectedExam);
        //    }
        //    if (length > 0)
        //    {
        //        return query.Where(x => x.Status == StudentExamAttendance.EntityStatus.Active)
        //            .List<StudentExamAttendance>().Skip(start).Take(length).ToList();
        //    }
        //    else
        //    {
        //        return query.Where(x => x.Status == StudentExamAttendance.EntityStatus.Active)
        //            .List<StudentExamAttendance>().ToList();
        //    }
        //}
        //public IList<IGrouping<long, StudentExamAttendance>> LoadExamEvaluationByExamId(long[] selectedExam, int start, int length)
        //{
        //    var query = Session.QueryOver<StudentExamAttendance>();
        //    if (selectedExam.Length == 1 && selectedExam[0] == 0)
        //    {
        //    }
        //    else
        //    {
        //        query.AndRestrictionOn(x => x.Exams.Id).IsIn(selectedExam);
        //    }
        //    var ss = query.Where(x => x.Status == StudentExamAttendance.EntityStatus.Active)
        //     .List<StudentExamAttendance>().GroupBy(x => x.Exams.Id).Skip(start).Take(length).ToList();
        //    return ss.ToList();
        //}
        //public IList<IGrouping<long, StudentExamAttendance>> LoadExamEvaluation(int start, int length, long programId, long sessionId, List<long> courseIdList, List<long> branchIdList, List<long> campusIdList, List<long> batchIdList, List<long> subjectIdList, DateTime fromDate, DateTime toDate, List<long> examIdList)
        //{
        //    ICriteria criteria = Session.CreateCriteria<StudentExamAttendance>().Add(Restrictions.Eq("Status", StudentExamAttendance.EntityStatus.Active));
        //    criteria.CreateAlias("StudentProgram", "sp").Add(Restrictions.Eq("sp.Status", StudentProgram.EntityStatus.Active));
        //    criteria.CreateAlias("Exams", "ex").Add(Restrictions.Eq("ex.Status", Exams.EntityStatus.Active));
        //    criteria.CreateAlias("Batch", "ba",JoinType.LeftOuterJoin).Add(Restrictions.Eq("ba.Status", Batch.EntityStatus.Active));

        //    criteria.Add(Restrictions.Eq("ba.Program.Id", programId));
        //    criteria.Add(Restrictions.Eq("ba.Session.Id", sessionId));
        //    if (!branchIdList.Contains(0))
        //        criteria.Add(Restrictions.Or(Restrictions.IsNull("ba.Branch"), Restrictions.In("ba.Branch.Id", branchIdList)));
        //    if (!campusIdList.Contains(0))
        //        criteria.Add(Restrictions.Or(Restrictions.IsNull("ba.Campus"), Restrictions.In("ba.Campus.Id", campusIdList)));
        //    if (!batchIdList.Contains(0))
        //        criteria.Add(Restrictions.In("ba.Id", batchIdList));
        //    criteria.Add(Restrictions.Ge("HeldDate", fromDate));
        //    criteria.Add(Restrictions.Le("HeldDate", toDate));
        //    if (!examIdList.Contains(0))
        //        criteria.Add(Restrictions.In("ex.Id", examIdList));
        //    if (!courseIdList.Contains(0))
        //        criteria.Add(Restrictions.In("ex.Course.Id", courseIdList));
        //    criteria.Add(Restrictions.IsNotNull("StudentFeedback"));
        //    //criteria.Add(Gr)
        //    var ss = criteria.List<StudentExamAttendance>().GroupBy(x => x.Exams.Id).Skip(start).Take(length).ToList();
        //    //ss = ss.GroupBy(x => x.Exams.Id).Skip(start).Take(length);
        //    return ss.ToList();
        //}
        //public int CountRowCountForExamEvaluationReport(long[] examIds)
        //{
        //    var query = Session.QueryOver<StudentExamAttendance>();
        //    if (examIds.Length == 1 && examIds[0] == 0)
        //    {
        //    }
        //    else
        //    {
        //        query.AndRestrictionOn(x => x.Exams.Id).IsIn(examIds);
        //    }
        //    return
        //        query.Where(x => x.Status == StudentExamAttendance.EntityStatus.Active)
        //            .List<StudentExamAttendance>()
        //            .GroupBy(x => x.Exams.Id)
        //            .ToList()
        //            .Count;
        //}
        //public int CountRowCountForIndividualExamEvaluationReport(long[] examIds)
        //{
        //    var query = Session.QueryOver<StudentExamAttendance>();
        //    if (examIds.Length == 1 && examIds[0] == 0)
        //    {
        //    }
        //    else
        //    {
        //        query.AndRestrictionOn(x => x.Exams.Id).IsIn(examIds);
        //    }
        //    return
        //        query.Where(x => x.Status == StudentExamAttendance.EntityStatus.Active)
        //            .RowCount();
        //}


    }
}
