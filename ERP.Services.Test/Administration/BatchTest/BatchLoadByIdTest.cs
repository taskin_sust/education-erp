﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BatchTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Batch")]
    public class BatchLoadByIdTest : BatchBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _batchService.GetBatch(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            var batchFactory =_batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            var obj = _batchService.GetBatch(batchFactory.Object.Id);
            Assert.NotNull(obj);
            
        }

    }
}
