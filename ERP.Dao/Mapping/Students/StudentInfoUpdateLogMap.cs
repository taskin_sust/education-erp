﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentInfoUpdateLogMap : ClassMap<StudentInfoUpdateLog>
    {
        public StudentInfoUpdateLogMap()
        {
            Table("StudentInfoUpdateLog");
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.CreateBy).Column("CreateBy");
            Map(x => x.CreationDate).Column("CreationDate");
            Map(x => x.FullName).Column("FullName");
            Map(x => x.NickName).Column("NickName");
            Map(x => x.Mobile).Column("Mobile");
            Map(x => x.Gender).Column("Gender");
            Map(x => x.Religion).Column("Religion");
            Map(x => x.DateOfBirth).Column("DateOfBirth");
            Map(x => x.FatherName).Column("FatherName");
            Map(x => x.GuardiansMobile1).Column("GuardiansMobile1");
            Map(x => x.GuardiansMobile2).Column("GuardiansMobile2");
            Map(x => x.RegistrationNo).Column("RegistrationNo");
            Map(x => x.Email).Column("Email");
            Map(x => x.District).Column("District");
            Map(x => x.LastMeritPosition).Column("LastMeritPosition");
            Map(x => x.Gpa).Column("Gpa");
            Map(x => x.Exam).Column("Exam");
            Map(x => x.Section).Column("Section");
            Map(x => x.Institute).Column("Institute");
            Map(x => x.StudentProgramId).Column("StudentProgramId");
            Map(x => x.StudentId).Column("StudentId");
            Map(x => x.InfoStatus).Column("InfoStatus");
        }

    }
}
