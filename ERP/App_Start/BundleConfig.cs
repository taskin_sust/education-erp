﻿using System.Web;
using System.Web.Optimization;

namespace UdvashERP
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/CommonJs").Include("~/Scripts/Common/Common.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/"));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            //"~/Scripts/jquery.validate.min.js",
            //    "~/Scripts/jquery.validate.unobtrusive.min.js"
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js", "~/Scripts/respond.js", "~/Scripts/bootbox.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap-datetimepicker-moment").Include("~/Content/bootstrap-datetimepicker/js/moment.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap-datepicker").Include("~/Content/bootstrap-datepicker-master/js/bootstrap-datepicker.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap-datetimepicker").Include("~/Scripts/bootstrap-datetimepicker.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/datatable").Include("~/Content/DataTables-1.10.2/js/jquery.dataTables.js"));
            bundles.Add(new ScriptBundle("~/bundles/tabletools").Include("~/Content/TableTools-2.2.3/js/dataTables.tableTools.js"));
            bundles.Add(new ScriptBundle("~/bundles/MenuGenerate").Include("~/Scripts/menu.js"));
            bundles.Add(new ScriptBundle("~/bundles/PageinationView").Include("~/Scripts/pagination.js"));

           // bundles.Add(new ScriptBundle("~/bundles/qrcode").Include("~/Content/jquery-qrcode-master/jquery.qrcode.js", "~/Content/jquery-qrcode-master/qrcode.js"));
            bundles.Add(new ScriptBundle("~/bundles/qrcode").Include("~/Content/jquery-qrcode-master/dist/jquery.qrcode.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/typeaheadAutoComplete").Include("~/Content/typeahead/bootstrap3-typeahead.js"));
            bundles.Add(new ScriptBundle("~/bundles/AutoComplete").Include("~/Scripts/Student/AutoComplete.js"));




            bundles.Add(new ScriptBundle("~/bundles/MenuGroup").Include("~/Scripts/Administration/Menu/menuGroup.js"));
            bundles.Add(new ScriptBundle("~/bundles/Menu").Include("~/Scripts/Administration/Menu/menu.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/dualListBox").Include("~/Scripts/jquery.bootstrap-duallistbox.js"));
            bundles.Add(new ScriptBundle("~/bundles/generateIdCard").Include("~/Scripts/Student/IdCard/GenerateIdCard.js"));
            bundles.Add(new ScriptBundle("~/bundles/MissingImage").Include("~/Scripts/Student/Image/MissingImage.js"));
            bundles.Add(new ScriptBundle("~/bundles/generateIdCardReport").Include("~/Scripts/Student/IdCard/GenerateIdCardReport.js"));
            bundles.Add(new ScriptBundle("~/bundles/ClassAttendance").Include("~/Scripts/Student/Attendance/ClassAttendance.js"));
            bundles.Add(new ScriptBundle("~/bundles/IndividualReport").Include("~/Scripts/Student/Attendance/IndividualReport.js"));
            bundles.Add(new ScriptBundle("~/bundles/PrintIdCard").Include("~/Scripts/Student/IdCard/PrintIdCard.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryPrint").Include("~/Scripts/jQuery.print.js"));
            bundles.Add(new ScriptBundle("~/bundles/distributeIdCard").Include("~/Scripts/Student/IdCard/distributeIdCard.js"));
            bundles.Add(new ScriptBundle("~/bundles/IdCardDistributionList").Include("~/Scripts/Student/IdCard/IdCardDistributionList.js"));

            bundles.Add(new ScriptBundle("~/bundles/SmsSetting").Include("~/Scripts/Sms/SmsSettings.js"));
            bundles.Add(new ScriptBundle("~/bundles/ManageSmsSetting").Include("~/Scripts/Sms/SmsManage.js"));
            bundles.Add(new ScriptBundle("~/bundles/UpdateSmsSetting").Include("~/Scripts/Sms/SmsSettingsUpdate.js"));

            bundles.Add(new ScriptBundle("~/bundles/MarksEntry").Include("~/Scripts/Exam/MarksEntry.js"));
            bundles.Add(new ScriptBundle("~/bundles/ManualMarksEntry").Include("~/Scripts/Exam/ManualMarksEntry.js"));
            bundles.Add(new ScriptBundle("~/bundles/ManageMarksEntry").Include("~/Scripts/Exam/ManageMarksEntry.js"));
            bundles.Add(new ScriptBundle("~/bundles/QuestionEntry").Include("~/Scripts/Exam/CreateQuestion.js"));

            bundles.Add(new ScriptBundle("~/bundles/LeaveApplication").Include("~/Scripts/Hr/Leave/LeaveApplication.js"));
            bundles.Add(new ScriptBundle("~/bundles/LeaveApplicationHrMentor").Include("~/Scripts/Hr/Leave/LeaveApplicationHrMentor.js"));

            bundles.Add(new ScriptBundle("~/bundles/MentorAttendanceAdjustment").Include("~/Scripts/Hr/AttendanceAdjustment/MentorAttendanceAdjustment.js"));
            bundles.Add(new ScriptBundle("~/bundles/HrAttendanceAdjustment").Include("~/Scripts/Hr/AttendanceAdjustment/HrAttendanceAdjustment.js"));

            bundles.Add(new ScriptBundle("~/bundles/MentorDayOffAdjustment").Include("~/Scripts/Hr/DayOffAdjustment/MentorDayOffAdjustment.js"));
            bundles.Add(new ScriptBundle("~/bundles/HrDayOffAdjustment").Include("~/Scripts/Hr/DayOffAdjustment/HrDayOffAdjustment.js"));

            bundles.Add(new ScriptBundle("~/bundles/MentorHolidayWorkApproval").Include("~/Scripts/Hr/HolidayWorkApproval/MentorHolidayWorkApproval.js"));
            bundles.Add(new ScriptBundle("~/bundles/HrHolidayWorkApproval").Include("~/Scripts/Hr/HolidayWorkApproval/HrHolidayWorkApproval.js"));
            bundles.Add(new ScriptBundle("~/bundles/SaveBankHistory").Include("~/Scripts/Hr/BankHistory/SaveBankHistory.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap.css", "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap-datepicker").Include("~/Content/bootstrap-datepicker-master/css/bootstrap-datepicker.min.css"));
            bundles.Add(new StyleBundle("~/Content/bootstrap-datetimepicker").Include("~/Content/bootstrap-datetimepicker.min.css"));
            bundles.Add(new StyleBundle("~/Content/styledatatable").Include("~/Content/DataTables-1.10.2/css/jquery.dataTables.css"));
            bundles.Add(new StyleBundle("~/Content/styletabletools").Include("~/Content/TableTools-2.2.3/css/dataTables.tableTools.css"));
            bundles.Add(new StyleBundle("~/Content/css/bootstrap-duallistbox").Include("~/Content/bootstrap-duallistbox.css"));
            bundles.Add(new StyleBundle("~/Content/fixedColumns.dataTables.min").Include("~/Content/fixedColumns.dataTables.min.css"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap-multiselect").Include("~/Content/bootstrap-multiselect/js/bootstrap-multiselect.js"));
            bundles.Add(new StyleBundle("~/Content/bootstrap-multiselect").Include("~/Content/bootstrap-multiselect/css/bootstrap-multiselect.css"));
           
        }
    }
}