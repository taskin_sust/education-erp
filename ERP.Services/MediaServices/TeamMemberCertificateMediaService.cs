﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.MediaDao;
using UdvashERP.Services.Helper;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Services.MediaServices
{
    public interface ITeamMemberCertificateMediaService
    {
        TeamMemberCertificateMediaImage GetTeamMemberCertificateMediaImageByRefId(long aId, long tId);

        TeamMemberCertificateMediaImage FileUpload(TeamMember teamMember, long academicInfoId, HttpPostedFileBase file, bool overwirte);

        TeamMemberTrainingCertificateMediaImage TrainingCertificateFileUpload(TeamMember teamMember, TrainingInfo trainingEntity, HttpPostedFileBase file, bool overwirte);

        TeamMemberTrainingCertificateMediaImage GetTeamMemberTrainingCertificateMediaImageByRefId(long trId, long teamMemberId);
    }
    public class TeamMemberCertificateMediaService : ITeamMemberCertificateMediaService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion


        private readonly ITeamMemberCertificateImageMediaDao _teamMemberCertificateMediaImageDao;
        private readonly ITeamMemberTrainingCertificateImageMediaDao _teamMemberTrainingCertificateMediaImageDao;
        private ISession session;
        public TeamMemberCertificateMediaService(ISession sessionParam = null)
        {
            if (sessionParam == null)
            {
                var sessionMedia = NHibernateMediaSessionFactory.OpenSession();
                session = sessionMedia;
            }
            else
            {
                session = sessionParam;
            }
            _teamMemberCertificateMediaImageDao = new TeamMemberCertificateImageMediaDao() { Session = session };
            _teamMemberTrainingCertificateMediaImageDao = new TeamMemberTrainingCertificateImageMediaDao() { Session = session };
        }

        public TeamMemberCertificateMediaImage GetTeamMemberCertificateMediaImageByRefId(long aId, long tId)
        {
            try
            {
                return _teamMemberCertificateMediaImageDao.GetTeamMemberCertificateImageMediaByRefId(aId, tId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public TeamMemberCertificateMediaImage FileUpload(TeamMember teamMember, long academicInfoId, HttpPostedFileBase file, bool p)
        {
            try
            {
                if (file != null)
                {
                    string sizeCheckerMessage = CheckFileSettingsForTypeAndSize(file.FileName, file.ContentType, file.ContentLength);
                    if (sizeCheckerMessage != null && sizeCheckerMessage.Length > 1)
                        throw new InvalidDataException(sizeCheckerMessage);
                    //return sizeCheckerMessage;

                    var contentType = file.ContentType;
                    var imageFile = ConvertFileInByteArray(file.InputStream, file.ContentLength);
                    var teamMemberCertificateMediaImages = _teamMemberCertificateMediaImageDao.GetTeamMemberCertificateImageMediaByRefId(academicInfoId, teamMember.Id);

                    //Check contentType for image
                    if (contentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                    {
                        using (var transicationMedia = session.BeginTransaction())
                        {
                            if (teamMemberCertificateMediaImages == null)
                                teamMemberCertificateMediaImages = new TeamMemberCertificateMediaImage();
                            teamMemberCertificateMediaImages.ImageGuid = Guid.NewGuid();
                            teamMemberCertificateMediaImages.Images = imageFile;
                            teamMemberCertificateMediaImages.Status = TeamMemberCertificateMediaImage.EntityStatus.Active;
                            teamMemberCertificateMediaImages.TeamMemberId = teamMember.Id;
                            teamMemberCertificateMediaImages.AcademicInfoId = academicInfoId;

                            _teamMemberCertificateMediaImageDao.SaveOrUpdate(teamMemberCertificateMediaImages);
                            transicationMedia.Commit();
                            return teamMemberCertificateMediaImages;
                        }
                    }
                    else
                    {
                        throw new InvalidDataException("This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension");
                    }
                }
                throw new InvalidDataException("File is missing");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public TeamMemberTrainingCertificateMediaImage TrainingCertificateFileUpload(TeamMember teamMember, TrainingInfo trainingEntity, HttpPostedFileBase file, bool overwirte)
        {
            try
            {
                if (file != null)
                {
                    string sizeCheckerMessage = CheckFileSettingsForTypeAndSize(file.FileName, file.ContentType, file.ContentLength);
                    if (sizeCheckerMessage != null && sizeCheckerMessage.Length > 1)
                        throw new InvalidDataException(sizeCheckerMessage);
                    //return sizeCheckerMessage;

                    var contentType = file.ContentType;
                    var imageFile = ConvertFileInByteArray(file.InputStream, file.ContentLength);
                    TeamMemberTrainingCertificateMediaImage teamMemberTrainingCertificateMediaImages = _teamMemberTrainingCertificateMediaImageDao.GetTrainingCertificateImageMediaByRefId(trainingEntity.Id, teamMember.Id);

                    //Check contentType for image
                    if (contentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                    {
                        using (var transicationMedia = session.BeginTransaction())
                        {
                            if (teamMemberTrainingCertificateMediaImages == null)
                                teamMemberTrainingCertificateMediaImages = new TeamMemberTrainingCertificateMediaImage();
                            teamMemberTrainingCertificateMediaImages.ImageGuid = Guid.NewGuid();
                            teamMemberTrainingCertificateMediaImages.Images = imageFile;
                            teamMemberTrainingCertificateMediaImages.Status = TeamMemberCertificateMediaImage.EntityStatus.Active;
                            teamMemberTrainingCertificateMediaImages.TeamMemberId = teamMember.Id;
                            teamMemberTrainingCertificateMediaImages.TrainingInfoId = trainingEntity.Id;

                            _teamMemberTrainingCertificateMediaImageDao.SaveOrUpdate(teamMemberTrainingCertificateMediaImages);
                            transicationMedia.Commit();
                            return teamMemberTrainingCertificateMediaImages;
                        }
                    }
                    else
                    {
                        throw new InvalidDataException("This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension");
                    }
                }
                throw new InvalidDataException("File is missing");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public TeamMemberTrainingCertificateMediaImage GetTeamMemberTrainingCertificateMediaImageByRefId(long trId, long teamMemberId)
        {
            try
            {
                return _teamMemberTrainingCertificateMediaImageDao.GetTrainingCertificateImageMediaByRefId(trId, teamMemberId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public string CheckFileSettingsForTypeAndSize(string name, string contentType, int size)
        {
            try
            {
                if (name == null || contentType == null || size < 1)
                    return "This is not valid file. Check the file extension.";

                if (contentType.ToUpper() == "JPG" || contentType.ToUpper() == "JPEG" || contentType.ToUpper() == "IMAGE/JPEG")
                {
                    string message = "";
                    var imageSizeKB = TeamMemberCertificateImage.CalculateSize(Convert.ToDouble(size));

                    if (imageSizeKB < TeamMemberCertificateImage.MinimumSizeKB)
                        message = "The image size is too small.";

                    if (imageSizeKB > TeamMemberCertificateImage.MaximumSizeKB)
                        message = "The image size is too large. Max valid size is up to " + TeamMemberCertificateImage.MaximumSizeKB + "KB";

                    return message;
                }

                return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
            }
        }
        private byte[] ConvertFileInByteArray(Stream inputStream, int contentLength)
        {
            try
            {
                byte[] file = null;

                using (var binaryReader = new BinaryReader(inputStream))
                {
                    file = binaryReader.ReadBytes(contentLength);
                }

                return file;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        private string ComputeHash(byte[] file)
        {
            MD5 md5 = MD5.Create();

            byte[] hashAraay = md5.ComputeHash(file);

            var builder = new StringBuilder();

            foreach (byte b in hashAraay)
            {
                builder.AppendFormat("{0:x2}", b);
            }

            return builder.ToString();
        }
    }
}
