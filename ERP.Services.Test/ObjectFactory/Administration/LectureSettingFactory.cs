﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using Xunit;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class LectureSettingFactory : ObjectFactoryBase<LectureSettings>
    {
        #region Object Initialization

        protected readonly ILectureService _lectureService;
        protected readonly ProgramSessionSubjectService _programSessionSubjectService;
        private ProgramFactory _programFactory;
        private SessionFactory _sessionFactory;
        private OrganizationFactory _organizationFactory;
        private SubjectFactory _subjectFactory;
        private CourseFactory _courseFactory;
        private CourseSubjectFactory _courseSubjectFactory;
        public LectureSettingFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _lectureService = new LectureService(session);
            _programSessionSubjectService = new ProgramSessionSubjectService(session);
            _programFactory = new ProgramFactory(this, session);
            _sessionFactory = new SessionFactory(this, session);
            _organizationFactory = new OrganizationFactory(this, session);
            _subjectFactory = new SubjectFactory(this, session);
            _courseFactory = new CourseFactory(this, session);
            _courseSubjectFactory = new CourseSubjectFactory(this, session);
        }

        #endregion

        #region Create

        public LectureSettingFactory Create()
        {
            Object = new LectureSettings()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Batch.EntityStatus.Active,
                ClassNamePrefix = Guid.NewGuid() + "_test class name prefix",
                NumberOfClasses = 100
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public LectureSettingFactory WithCourseSubject()
        {
            string courseName = Guid.NewGuid() + "test course name";
            Organization organization = _organizationFactory.Create().Persist().Object;
            Program program = _programFactory.Create()
                .WithOrganization(organization)
                .Persist()
                .Object;
            Session sessionObj = _sessionFactory.Create().Persist().Object;
            List<Subject> subjectList = _subjectFactory.CreateMore(5).Persist().GetLastCreatedObjectList();

            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectList.Select(x => Convert.ToInt32(x.Id)).ToArray()
            };

            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, program.Id, sessionObj.Id);
            IList<ProgramSessionSubject> assignSubjects = _programSessionSubjectService.LoadProgramSessionSubject(program.Id, sessionObj.Id);

            _courseFactory.Create().WithName(courseName).WithStartDateAndEndDate(DateTime.Now.AddDays(5), DateTime.Now.AddDays(35))
                .WithOrganization(organization).WithProgram(program).WithSession(sessionObj)
                .WithMaxMinSubject(4, 2, 2);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList()).Persist();
            Object.Course = _courseFactory.Object;
            Object.CourseSubject = _courseSubjectFactory.GetLastCreatedObjectList()[0];
            return this;
        }

        public LectureSettingFactory CreateMore(int numberOfLecturSetting)
        {
            CreateLectureSettingList(numberOfLecturSetting);
            return this;
        }

        private List<LectureSettings> CreateLectureSettingList(int numberOfLecturSetting)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<LectureSettings>();
            string courseName = Guid.NewGuid() + "test course name";
            Organization organization = _organizationFactory.Create().Persist().Object;
            Program program = _programFactory.Create()
                .WithOrganization(organization)
                .Persist()
                .Object;
            Session sessionObj = _sessionFactory.Create().Persist().Object;
            List<Subject> subjectList = _subjectFactory.CreateMore(numberOfLecturSetting).Persist().GetLastCreatedObjectList();

            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectList.Select(x => Convert.ToInt32(x.Id)).ToArray()
            };

            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, program.Id, sessionObj.Id);
            IList<ProgramSessionSubject> assignSubjects = _programSessionSubjectService.LoadProgramSessionSubject(program.Id,
                sessionObj.Id);

            _courseFactory.Create()
                .WithName(courseName)
                .WithStartDateAndEndDate(DateTime.Now.AddDays(5), DateTime.Now.AddDays(35))
                .WithOrganization(organization).WithProgram(program).WithSession(sessionObj)
                .WithMaxMinSubject(4, 2, 2);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList()).Persist();
            for (int i = 0; i < numberOfLecturSetting; i++)
            {
                var lecturSetting = new LectureSettings()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = null,
                    Rank = 1,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Batch.EntityStatus.Active,
                    ClassNamePrefix = Guid.NewGuid() + "test class name prifix",
                    NumberOfClasses = 10,
                    Course = _courseFactory.Object,
                    CourseSubject = _courseSubjectFactory.GetLastCreatedObjectList()[i]
                };
                list.Add(lecturSetting);
            }
            ObjectList.AddRange(list);
            return list;
        }

        public LectureSettingFactory Persist()
        {
            var lectureSettingList = new List<LectureSettings>();
            if (ObjectList != null)
            {

                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        lectureSettingList.Add(item);
                        _lectureService.Save(lectureSettingList);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        lectureSettingList.Add(item);
                        _lectureService.Save(lectureSettingList);
                    }
                }
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void Cleanup()
        {
            //do inside test case
            //_TestBaseService.DeleteProgramSessionSubject(_programFactory.Object.Id, _sessionFactory.Object.Id);
            //_TestBaseService.DeleteLecture();
            
            DeleteAll();
            _courseSubjectFactory.Cleanup();
            _courseFactory.Cleanup();
            _subjectFactory.Cleanup();
            _programFactory.Cleanup();
            _sessionFactory.Cleanup();
            _organizationFactory.Cleanup();
            
        }
        #endregion

    }
}
