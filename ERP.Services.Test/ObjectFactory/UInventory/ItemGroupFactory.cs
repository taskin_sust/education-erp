using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Base;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class ItemGroupFactory : ObjectFactoryBase<ItemGroup>
   {

       private readonly IItemGroupService _itemGroupService;

       public ItemGroupFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
       {
            _itemGroupService = new ItemGroupService(session);
       }

       #region Create 

       public ItemGroupFactory Create()
       {
           Object = new ItemGroup()
           {
               Id = 0,
               BusinessId = GetPcUserName(),
               Name = Guid.NewGuid().ToString(),
               Rank = 1,
               CreateBy = 1,
               ModifyBy = 1,
               Status = ItemGroup.EntityStatus.Active
           };
       SingleObjectList.Add(Object);
       return this;
       }

       public ItemGroupFactory CreateWith(long id,string name,int status)
       {
           Object = new ItemGroup()
           {
               Id = id,
               BusinessId = GetPcUserName(),
               Name = name,
               Rank = 1,
               CreateBy = 1,
               ModifyBy = 1,
               Status = status
           };
           SingleObjectList.Add(Object);
          return this;
       }

       public ItemGroupFactory Persist()
       {

          if (SingleObjectList != null && SingleObjectList.Count > 0)
          {
              SingleObjectList.ForEach(x =>
              {
                  if (x.Id == 0) _itemGroupService.SaveOrUpdate(x);
              });
          }

          if (ObjectList != null && ObjectList.Count > 0)
          {
              ObjectList.ForEach(x =>
              {
                  if (x.Id == 0)_itemGroupService.SaveOrUpdate(x);
              });
          }
          return this;
       }

       public ItemGroupFactory CreateMore(int num)
       {
           CreateItemGroupList(num);
            return this;
       }

       private List<ItemGroup> CreateItemGroupList(int numberOfItemGroup)
       {
           ListStartIndex = ObjectList.Count;
           var list = new List<ItemGroup>();
           for (int i = 0; i < numberOfItemGroup; i++)
           {
               var itemGroupObj = new ItemGroup()
               {
                   Id = 0,
                   Name = Guid.NewGuid() + "_" + i,
                   BusinessId = GetPcUserName(),
                   Rank = i,
                   CreateBy = 1,
                   ModifyBy = 1,
                   Status = ItemGroup.EntityStatus.Active
               };
               list.Add(itemGroupObj);
           }
           ObjectList.AddRange(list);
           return list;
       }

       #endregion

       #region other
       #endregion

       #region CleanUp 
       public void CleanupItem(List<long> itemIdList)
       {
           if (itemIdList.Count > 0)
           {

               var tableName = GetEntityTableName(typeof(Item));
               _session.CreateSQLQuery("Delete from " + tableName + " where id in(" + string.Join(", ", itemIdList) + ")").ExecuteUpdate();
           }
       }
       public ItemGroupFactory CleanUp()
       {
          DeleteAll();
          return this;
       }

       #endregion

    }
}
