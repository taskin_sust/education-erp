using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class MentorHistoryLogMap : ClassMap<MentorHistoryLog> 
    {
        
        public MentorHistoryLogMap() {
			Table("HR_MentorHistoryLog");
			LazyLoad();
            //References(x => x.Designation).Cascade.All().Column("MemberDesignationId");//old
            //References(x => x.Department).Cascade.All().Column("MemberDepartmentId");//old
            //References(x => x.Organization).Cascade.All().Column("MemberOrganizationId");//old
            //References(x => x.TeamMember).Column("TeamMemberId");//old
            //References(x => x.Mentor).Column("MentorId");//old
            //References(x => x.MentorDesignation).Cascade.All().Column("MentorDesignationId");//old
            //References(x => x.MentorDepartment).Cascade.All().Column("MentorDepartmentId");//old
            //References(x => x.MentorOrganization).Cascade.All().Column("MentorOrganizationId");//old
            //References(x => x.MentorHistory).Column("MentorHistoryId");//old
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
            Map(x => x.MemberDesignationId);
            Map(x => x.MemberDepartmentId);
            Map(x => x.MemberOrganizationId);
            Map(x => x.TeamMemberId);
            Map(x => x.MentorId);
            Map(x => x.MentorDesignationId);
            Map(x => x.MentorDepartmentId);
            Map(x => x.MentorOrganizationId);
            Map(x => x.MentorHistoryId);
            Map(x => x.Pin).Column("Pin");
            //Map(x => x.MentorName).Column("MentorName");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
        }
    }
}
