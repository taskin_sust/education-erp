﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.AdminisTration.SurveyQuestionTest;
using Xunit;

namespace UdvashERP.Services.Test.Administration.SurveyQuestionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "SurveyQuestion")]
    public class SurveyQuestionUpdateTest : SurveyQuestionBaseTest
    {
        [Fact]
        public void Update_Should_Return_Null_Exception() 
        {
            _surveyQuestionAnswerFactory.CreateMore(1, null);
            _surveyQuestionFactory.Create().WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList());
            Assert.Throws<NullObjectException>(() => _surveyQuestionService.Update(null));
            Assert.Throws<NullObjectException>(() => _surveyQuestionService.Update(_surveyQuestionFactory.Object));
        }

        [Fact]
        public void Update_Should_Return_Model_inValid()
        {
            _surveyQuestionFactory.Create().WithQuestionAndRank(_question, 10);
            _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
           var obj1 = _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList()).Persist().Object;
            obj1.Question = "";
            Assert.Throws<MessageException>(() => _surveyQuestionService.Update(obj1));
        }

        [Fact]
        public void Update_Should_Check_Duplicate()
        {
            _surveyQuestionFactory.Create().WithQuestionAndRank(_question, 10);
            _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
            var obj1 = _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList()).Persist().Object;

            _surveyQuestionFactory.Create().WithQuestionAndRank(_question+"1", 10);
            _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
            var obj2 = _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList()).Persist().Object;

            var objectUpdate = _surveyQuestionService.GetSurveyQuestion(obj2.Id);
            objectUpdate.Question = obj1.Question;

            Assert.Throws<DuplicateEntryException>(() => _surveyQuestionService.Update(objectUpdate));
        }

        [Fact]
        public void Update_Should_Successfully()
        {
            _surveyQuestionFactory.Create().WithQuestionAndRank(_question, 10);
            _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
            var obj1 = _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList()).Persist().Object;
            var objectUpdate = _surveyQuestionService.GetSurveyQuestion(obj1.Id);
            var oldQuestion = obj1.Question;
            var newQuestion = oldQuestion + " Update Check";
            objectUpdate.Question = newQuestion;
            _surveyQuestionService.Update(objectUpdate);
            var objUpdated = _surveyQuestionService.GetSurveyQuestion(obj1.Id);
            Assert.Equal(objUpdated.Question, newQuestion);
        }
    }
}
