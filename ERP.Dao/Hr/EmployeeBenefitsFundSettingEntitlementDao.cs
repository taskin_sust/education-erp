using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IEmployeeBenefitsFundSettingEntitlementDao : IBaseDao<EmployeeBenefitsFundSettingEntitlement, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion
    }

    public class EmployeeBenefitsFundSettingEntitlementDao : BaseDao<EmployeeBenefitsFundSettingEntitlement, long>, IEmployeeBenefitsFundSettingEntitlementDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion
    }
}
