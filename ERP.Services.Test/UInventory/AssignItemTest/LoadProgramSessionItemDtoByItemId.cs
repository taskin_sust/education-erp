﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.AssignItemTest
{
    internal interface ILoadProgramSessionItemDtoByItemId
    {
        void Should_Return_Load_item();
        void Should_Return_Null_Item_Exception();
    }

    [Collection("Collection #1")]
    [Trait("Area", "UInventory")]
    [Trait("Service", "ProgramSessionItem")]
    public class LoadProgramSessionItemDtoByItemId : ProgramSessionItemBaseTest, ILoadProgramSessionItemDtoByItemId
    {
        [Fact]
        public void Should_Return_Load_item()
        {
            organizationFactory.Create().Persist();
            _ProgramSessionItemFactory.Create().WithItem(_name,(int)ItemType.ProgramItem,(int) ItemUnit.Kg,(int)CostBearer.Individual
                ,organizationFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            
            Assert.NotNull(_ProgramSessionItemFactory.Object.Item);
            Assert.Equal(1, _ProgramSessionItemService.LoadProgramSessionItemDtoByItemId(new long[] { _ProgramSessionItemFactory.Object.Item.Id }).Count());
           // Assert.NotEmpty(_ProgramSessionItemService.LoadProgramSessionItemDtoByItemId(new long[]{_ProgramSessionItemFactory.Object.Item.Id}));
        }

        [Fact]
        public void Should_Return_Null_Item_Exception()
        {
            Assert.Throws<NullObjectException>(() => _ProgramSessionItemService.LoadProgramSessionItemDtoByItemId(null));
        }
    }
}
