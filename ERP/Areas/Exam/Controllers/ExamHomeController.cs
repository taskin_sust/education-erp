﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;

namespace UdvashERP.Areas.Exam.Controllers
{
    [UerpArea("Exam")]
    [Authorize]
    [AuthorizeAccess]
    public class ExamHomeController : Controller
    {
        // GET: Exam/ExamsHome
        [AuthorizeRequired]
        public ActionResult Index()
        {
            return View();
        }
    }
}