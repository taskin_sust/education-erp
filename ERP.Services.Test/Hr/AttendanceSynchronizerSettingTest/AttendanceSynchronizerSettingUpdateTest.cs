﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSynchronizerSettingTest
{

    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSynchronizerSettingService")]
    public class AttendanceSynchronizerSettingUpdateTest : AttendanceSynchronizerSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_DuplicateEntry_Exception()
        {
            _attendanceSynchronizerSettingFactory.Create().Persist();
          Assert.Throws<DuplicateEntryException>(
                () =>
                    _attendanceSynchronizerSettingService.Update(_attendanceSynchronizerSettingFactory.Object,
                        10));
        }
        
        [Fact]
        public void Should_Update_Successfully()
        {
            _attendanceSynchronizerSettingFactory.Create().Persist();
            _attendanceSynchronizerSettingFactory.Object.ReleaseDate = DateTime.Now.AddDays(7);
            Assert.Equal(_attendanceSynchronizerSettingService.GetAttendanceSynchronizerSetting(_attendanceSynchronizerSettingFactory.Object.Id).ReleaseDate, _attendanceSynchronizerSettingFactory.Object.ReleaseDate);
        }

        [Fact]
        public void Should_Return_InvalidData_Exception()
        {
            _attendanceSynchronizerSettingFactory.Create();
            _attendanceSynchronizerSettingFactory.Object.Status = AttendanceSynchronizerSetting.EntityStatus.Delete;
            Assert.Throws<InvalidDataException>(
                () => _attendanceSynchronizerSettingService.Update(_attendanceSynchronizerSettingFactory.Object,_attendanceSynchronizerSettingFactory.Object.Id));
        }

        #endregion

    }
}
