using System.Collections.Generic;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class SupplierItemFactory : ObjectFactoryBase<SupplierItem>
    {

        private readonly ISupplierItemService _supplierItemService;
        private OrganizationFactory _organizationFactory;
        private ItemFactory _itemFactory;
        private SupplierFactory _supplierFactory;

        public SupplierItemFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _supplierItemService = new SupplierItemService(session);
            _organizationFactory = new OrganizationFactory(this, session);
            _itemFactory = new ItemFactory(this, session);
            _supplierFactory = new SupplierFactory(this, session);
        }

        #region Create

        public SupplierItemFactory Create()
        {
            Object = new SupplierItem()
            {
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = 1
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public SupplierItemFactory CreateWith(long id)
        {
            Object = null;
            SingleObjectList.Add(Object);
            return this;
        }

        public SupplierItemFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x =>
                {
                    if (x.Id == 0) _supplierItemService.Save(new List<SupplierItem>() {x});
                });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                _supplierItemService.Save(ObjectList.Where(x => x.Id == 0).ToList());
            }
            return this;
        }

        public SupplierItemFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new SupplierItem() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        public SupplierItemFactory WithItem()
        {
            Object.Item = _itemFactory.Create().WithOrganization().WithItemGroup().Persist().Object;
            return this;
        }
        public SupplierItemFactory WithItem(Item item)
        {
            Object.Item = item;
            return this;
        }

        public SupplierItemFactory WithSupplier()
        {
            Object.Supplier = _supplierFactory.Create().Persist().Object;
            return this;
        }

        public SupplierItemFactory WithSupplier(Supplier supplier)
        {
            Object.Supplier = supplier;
            return this;
        }
        
        #endregion

        #region other

        #endregion

        #region CleanUp

        public void CleanUp()
        {
            DeleteAll();
        }

        #endregion

    }
}
