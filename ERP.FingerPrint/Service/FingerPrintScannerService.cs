﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UdvashERP.FingerPrint.BusinessModel;
using UdvashERP.FingerPrint.BusinessRule;
using UdvashERP.FingerPrint.FormUI;
using UdvashERP.FingerPrint.Helper;

namespace UdvashERP.FingerPrint.Service
{
    public class FingerPrintScannerService
    {
        ZktecoColorDriver zktecoDriver;
        public FingerPrintScannerService()
        {
            zktecoDriver = new ZktecoColorDriver();
        }

        internal JsonResponse Scanning(JsonResponse memberInfo, AttendanceDeviceApiModel onlineDevice, Device offlineDevice, int fingerIndex)
        {
            ScanClearFinger dlg = new ScanClearFinger();
            CheckingFingerPrintIndex(onlineDevice, fingerIndex, dlg);
            if (dlg.IsRescanClick || !dlg.IsClear)
            {
                SaveOrUpdateTeamMemberFingerTemplate(DeviceHelper.MachineNo, memberInfo, onlineDevice, offlineDevice, fingerIndex);
            }

            if (dlg.IsClear)
            {
                SaveOrUpdateTeamMemberFingerTemplate(DeviceHelper.MachineNo, memberInfo, onlineDevice, offlineDevice, fingerIndex, true);
            }
            //SaveOrUpdateTeamMemberFingerTemplate(DeviceHelper.MachineNo, memberInfo, onlineDevice, offlineDevice, fingerIndex);

            return memberInfo;
        }

        private void CheckingFingerPrintIndex(AttendanceDeviceApiModel onlineDevice, int fingerIndex, ScanClearFinger dlg)
        {
            if (fingerIndex == 0 && !String.IsNullOrEmpty(onlineDevice.Index0))
            {
                dlg.ShowDialog();
            }
            if (fingerIndex == 1 && !String.IsNullOrEmpty(onlineDevice.Index1))
            {
                dlg.ShowDialog();
            }
            if (fingerIndex == 2 && !String.IsNullOrEmpty(onlineDevice.Index2))
            {
                dlg.ShowDialog();
            }
            if (fingerIndex == 3 && !String.IsNullOrEmpty(onlineDevice.Index3))
            {
                dlg.ShowDialog();
            }
            if (fingerIndex == 4 && !String.IsNullOrEmpty(onlineDevice.Index4))
            {
                dlg.ShowDialog();
            }
            if (fingerIndex == 5 && !String.IsNullOrEmpty(onlineDevice.Index5))
            {
                dlg.ShowDialog();
            }
            if (fingerIndex == 6 && !String.IsNullOrEmpty(onlineDevice.Index6))
            {
                dlg.ShowDialog();
            }
            if (fingerIndex == 7 && !String.IsNullOrEmpty(onlineDevice.Index7))
            {
                dlg.ShowDialog();
            }
            if (fingerIndex == 8 && !String.IsNullOrEmpty(onlineDevice.Index8))
            {
                dlg.ShowDialog();
            }
            if (fingerIndex == 9 && !String.IsNullOrEmpty(onlineDevice.Index9))
            {
                dlg.ShowDialog();
            }
        }

        private JsonResponse SaveOrUpdateTeamMemberFingerTemplate(int iMachineNumber, JsonResponse memberInfo, AttendanceDeviceApiModel onlineDevice, Device offlineDevice, int fingerIndex, bool isClear = false)
        {
            bool isConnected = zktecoDriver.Connect(offlineDevice);

            if (isConnected == false)
            {
                MessageBox.Show("Device Not Connected.", "Not Connected");
                return memberInfo;
            }

            //onlineDevice = memberInfo.Data.AttendanceDeviceApiModel.Where(m => m.ModelId == onlineDevice.ModelId).FirstOrDefault();

            int flag = 1;
            int iTmpLength = 0;
            byte[] byTmpData = new byte[2000];

            if (isClear == false)
            {
                bool isScan = zktecoDriver.SetUserFingerTemplate(memberInfo.Data.Pin.ToString(), fingerIndex, flag);
            }

            // i need to refactoring

            zktecoDriver.GetUserFingerTemplate(offlineDevice.MachineNo, memberInfo.Data.Pin.ToString(), fingerIndex, out  flag, out  byTmpData, out  iTmpLength);

            string hexString = "";
            if (iTmpLength > 0)
            {
                StringBuilder sb = new StringBuilder();
                hexString = "";
                foreach (byte b in byTmpData)
                {
                    sb.Append(b.ToString("X2"));
                }
                hexString = sb.ToString();
            }

            #region Assign Finger

            foreach (var device in memberInfo.Data.AttendanceDeviceApiModel)
            {
                if (device.ModelId != onlineDevice.ModelId)
                {
                    continue;
                }

                if (fingerIndex == FingerPrintConstant.FingerIndex0)
                {
                    if (!isClear)
                        onlineDevice.Index0 = hexString;
                    else
                        onlineDevice.Index0 = null;
                }
                if (fingerIndex == FingerPrintConstant.FingerIndex1)
                {
                    if (!isClear)
                        onlineDevice.Index1 = hexString;
                    else
                        onlineDevice.Index1 = null;
                }
                if (fingerIndex == FingerPrintConstant.FingerIndex2)
                {
                    if (!isClear)
                        onlineDevice.Index2 = hexString;
                    else
                        onlineDevice.Index2 = null;
                }
                if (fingerIndex == FingerPrintConstant.FingerIndex3)
                {
                    if (!isClear)
                        onlineDevice.Index3 = hexString;
                    else
                        onlineDevice.Index3 = null;
                }
                if (fingerIndex == FingerPrintConstant.FingerIndex4)
                {
                    if (!isClear)
                        onlineDevice.Index4 = hexString;
                    else
                        onlineDevice.Index4 = null;
                }
                if (fingerIndex == FingerPrintConstant.FingerIndex5)
                {
                    if (!isClear)
                        onlineDevice.Index5 = hexString;
                    else
                        onlineDevice.Index5 = null;
                }
                if (fingerIndex == FingerPrintConstant.FingerIndex6)
                {
                    if (!isClear)
                        onlineDevice.Index6 = hexString;
                    else
                        onlineDevice.Index6 = null;
                }
                if (fingerIndex == FingerPrintConstant.FingerIndex7)
                {
                    if (!isClear)
                        onlineDevice.Index7 = hexString;
                    else
                        onlineDevice.Index7 = null;
                }
                if (fingerIndex == FingerPrintConstant.FingerIndex8)
                {
                    if (!isClear)
                        onlineDevice.Index8 = hexString;
                    else
                        onlineDevice.Index8 = null;
                }
                if (fingerIndex == FingerPrintConstant.FingerIndex9)
                {
                    if (!isClear)
                        onlineDevice.Index9 = hexString;
                    else
                        onlineDevice.Index9 = null;
                }
            }

            #endregion

            return memberInfo;
        }


        internal bool ConnectionTest(Device device)
        {
            Device deviceInfo = DeviceHelper.GetDeviceInfo(device.Name);
            return zktecoDriver.Connect(deviceInfo);

        }

        internal void GetUserInfo()
        {
            Device deviceInfo = DeviceHelper.GetDeviceInfo("ZK Color");
            bool isConnected = zktecoDriver.Connect(deviceInfo);

            if (isConnected == false)
            {
                MessageBox.Show("Device Not Connected.", "Not Connected");
                return;
            }

            zktecoDriver.GetUserInfo();

        }

        internal bool DeleteData(Device device)
        {
            throw new NotImplementedException();
        }
    }
}
