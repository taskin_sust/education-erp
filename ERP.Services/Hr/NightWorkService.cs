﻿using System;
using System.Collections.Generic;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
//using Enumerable = System.Linq.Enumerable;
using System.Linq;
using System.Linq.Expressions;

namespace UdvashERP.Services.Hr
{

    public interface INightWorkService : IBaseService
    {
        #region Operational Function

        void Save(string date, string pinList, string fullValues, string halfValues, string noValues, string inValues, string outValues);

        #endregion

        #region Single Instances Loading Function

        NightWork LoadById(long id);

        #endregion

        #region List Loading Function

        IList<NightWorkDto> LoadNightWork(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList = null, List<UserMenu> userMenu = null, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, int start = 0, int length = 0, string orderBy = null, string orderDir = null, bool isHistory = false);

        IList<NightWork> LoadNightWork(DateTime dateFrom, DateTime dateTo, long teamMemberId);

        #endregion

        #region Others Function

        int LoadNightWorkCount(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, bool isHistory = false);
        DateTime? GetTeamMemberNightWorkIsPostDateTime(long? teamMemberId = null, int? pin = null);

        #endregion

        #region Helper Function
        #endregion


    }
    public class NightWorkService : BaseService, INightWorkService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Properties & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly INightWorkDao _nightWorkDao;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;
        private readonly IAttendanceSummaryDao _attendanceSummaryDao;

        public NightWorkService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _nightWorkDao = new NightWorkDao() { Session = session };
            _teamMemberDao = new TeamMemberDao { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao { Session = session };
            _attendanceSummaryDao = new AttendanceSummaryDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public void Save(string date, string pinList, string fullValues, string halfValues, string noValues, string inValues, string outValues)
        {
            if (pinList == null)
            {
                throw new NullObjectException("Night object can not be null");
            }
            ITransaction trans = null;
            try
            {
                var pinArray = pinList.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                var fullValuesArray = fullValues.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                var halfValuesArray = halfValues.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                var noValuesArray = noValues.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                var inValuesArray = inValues.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                var outValuesArray = outValues.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                if (string.IsNullOrEmpty(date) || !pinArray.Any())
                {
                    throw new InvalidDataException("Please Enter Night work date and Pin");
                }

                #region valid object

                List<NightWork> vaildNightworkList = new List<NightWork>();
                DateTime nightWorkDate = Convert.ToDateTime(date).Date;

                List<string> nightWorkTeamMemberPinLsit = new List<string>();
                if (noValuesArray.Any())
                    nightWorkTeamMemberPinLsit.AddRange(noValuesArray);
                if (halfValuesArray.Any())
                    nightWorkTeamMemberPinLsit.AddRange(halfValuesArray);
                if (fullValuesArray.Any())
                    nightWorkTeamMemberPinLsit.AddRange(fullValuesArray);

                if (!nightWorkTeamMemberPinLsit.Any())
                    throw new InvalidDataException("No TeamMember found for applying night work.");

                var dublicate = nightWorkTeamMemberPinLsit.GroupBy(x => x)
                        .Select(x => new { teammemberPin = x, count = x.Count() })
                        .Where(x => x.count > 1)
                        .ToList();
                if (dublicate.Any())
                    throw new InvalidDataException("Duplicate entry for TeamMember with pin (" + dublicate[0].teammemberPin.Key + ").");

                #region NightWork Object

                if (nightWorkTeamMemberPinLsit.Any())
                {
                    foreach (string teamMemberPin in nightWorkTeamMemberPinLsit)
                    {
                        AttendanceSummary attendanceSummary = _attendanceSummaryDao.GetByPinAndDate(Convert.ToInt32(teamMemberPin.Trim()), Convert.ToDateTime(date));
                        if (attendanceSummary == null)
                            throw new InvalidDataException("Can't allow pin no " + teamMemberPin + " for this allowance because of his absence! on date " + date);
                        int? approvalType = null;
                        bool inHouseWorkType = false;
                        TeamMember member = _teamMemberDao.GetMember(Convert.ToInt32(teamMemberPin));

                        if (member == null)
                            throw new InvalidDataException("No TeamMember found for Pin (" + teamMemberPin + ") .");

                        DateTime? postDateTime = GetTeamMemberNightWorkIsPostDateTime(member.Id);
                        if (postDateTime != null && postDateTime.Value.Date >= nightWorkDate.Date)
                            throw new InvalidDataException("TeamMember can only applicable for Night work after Date (" + postDateTime.Value.ToString("d MMM, yyyy") + ") for Pin (" + teamMemberPin + ") .");

                        if (halfValuesArray.Any() && halfValuesArray.Any(x => x == teamMemberPin))
                            approvalType = Convert.ToInt32(HolidayWorkApprovalStatus.Half);
                        else if (fullValuesArray.Any() && fullValuesArray.Any(x => x == teamMemberPin))
                            approvalType = Convert.ToInt32(HolidayWorkApprovalStatus.Full);

                        if (inValuesArray.Any() && inValuesArray.Any(x => x == teamMemberPin))
                            inHouseWorkType = true;


                        NightWork nightWork = _nightWorkDao.GetNightWork(teamMemberPin, nightWorkDate);
                        if (nightWork == null)
                        {
                            nightWork = new NightWork
                            {
                                NightWorkDate = nightWorkDate,
                                TeamMember = member,
                                Rank = 10
                            };
                        }

                        nightWork.ApprovalType = approvalType;
                        nightWork.InHouseWork = inHouseWorkType;
                        vaildNightworkList.Add(nightWork);
                    }
                }

                #endregion

                #endregion

                #region Save Night Work

                using (trans = Session.BeginTransaction())
                {
                    if (vaildNightworkList.Any())
                    {
                        foreach (NightWork nightWork in vaildNightworkList)
                        {
                            _nightWorkDao.SaveOrUpdate(nightWork);
                            SaveNewNightWorkLog(nightWorkDate, nightWork, nightWork.ApprovalType);
                        }
                        trans.Commit();
                    }
                }

                #endregion

                #region old code
                //using (trans = Session.BeginTransaction())
                //{
                //    DateTime nightWorkDate = Convert.ToDateTime(date).Date;

                //    #region No NightWork 

                //    if (noValues.Any())
                //    {
                //        int? approvalType = null;
                //        //No Night Work
                //        foreach (var teamMemberPin in noValuesArray)
                //        {
                //            NightWork nightWork = _nightWorkDao.GetNightWork(teamMemberPin, nightWorkDate);
                //            if (nightWork != null)
                //            {
                //                nightWork.ApprovalType = approvalType;
                //            }
                //            else
                //            {
                //                TeamMember member = _teamMemberDao.GetMember(Convert.ToInt32(teamMemberPin));
                //                if (member == null)
                //                {
                //                    throw new InvalidDataException("No TeamMember found for Pin (" + teamMemberPin + ") .");
                //                }
                //                nightWork = new NightWork
                //                {
                //                    NightWorkDate = nightWorkDate,
                //                    TeamMember = member,
                //                    Rank = 10,
                //                    ApprovalType = approvalType
                //                };
                //            }

                //            if (inValuesArray.Any())
                //            {
                //                foreach (var invalMemberTeam in inValuesArray)
                //                {
                //                    if (teamMemberPin == invalMemberTeam)
                //                    {
                //                        nightWork.InHouseWork = true;
                //                        break;
                //                    }
                //                }
                //            }
                //            if (outValuesArray.Any())
                //            {
                //                foreach (var invalMemberTeam in outValuesArray)
                //                {
                //                    if (teamMemberPin == invalMemberTeam)
                //                    {
                //                        nightWork.InHouseWork = false;
                //                        break;
                //                    }
                //                }
                //            }
                //            _nightWorkDao.Save(nightWork);
                //            SaveNewNightWorkLog(nightWorkDate, nightWork, approvalType);
                //        }
                //    }

                //    #endregion

                //    #region Half NightWork

                //    if (halfValuesArray.Any())
                //    {
                //        int? approvalType = Convert.ToInt32(HolidayWorkApprovalStatus.Half);
                //        //No Night Work
                //        foreach (var teamMemberPin in halfValuesArray)
                //        {
                //            var nightWork = _nightWorkDao.GetNightWork(teamMemberPin, nightWorkDate);

                //            if (nightWork != null)
                //            {
                //                nightWork.ApprovalType = approvalType;
                //                //_nightWorkDao.Save(nightWork);
                //            }
                //            else
                //            {
                //                TeamMember member = _teamMemberDao.GetMember(Convert.ToInt32(teamMemberPin));
                //                nightWork = new NightWork
                //                {
                //                    NightWorkDate = nightWorkDate,
                //                    TeamMember = member,
                //                    Rank = 10,
                //                    ApprovalType = approvalType
                //                };
                //                //_nightWorkDao.Save(nightWork);
                //                //SaveNewNightWorkApproval(nightWorkDate, teamMemberPin, Convert.ToInt32(HolidayWorkApprovalStatus.Half));
                //            }
                //            if (inValuesArray.Any())
                //            {
                //                foreach (var invalMemberTeam in inValuesArray)
                //                {
                //                    if (teamMemberPin == invalMemberTeam)
                //                    {
                //                        nightWork.InHouseWork = true;
                //                        break;
                //                    }
                //                }
                //            }
                //            if (outValuesArray.Any())
                //            {
                //                foreach (var invalMemberTeam in outValuesArray)
                //                {
                //                    if (teamMemberPin == invalMemberTeam)
                //                    {
                //                        nightWork.InHouseWork = false;
                //                        break;
                //                    }
                //                }
                //            }
                //            _nightWorkDao.Save(nightWork);
                //            SaveNewNightWorkLog(nightWorkDate, nightWork, approvalType);
                //        }
                //    }

                //    #endregion

                //    #region full NightWork

                //    if (fullValuesArray.Any())
                //    {
                //        int? approvalType = Convert.ToInt32(HolidayWorkApprovalStatus.Full);
                //        //No Night Work
                //        foreach (var teamMemberPin in fullValuesArray)
                //        {
                //            var nightWork = _nightWorkDao.GetNightWork(teamMemberPin, nightWorkDate);
                //            if (nightWork != null)
                //            {
                //                nightWork.ApprovalType = approvalType;
                //                _nightWorkDao.Save(nightWork);
                //            }
                //            else
                //            {
                //                //SaveNewNightWorkApproval(nightWorkDate, teamMemberPin, Convert.ToInt32(HolidayWorkApprovalStatus.Full));
                //                TeamMember member = _teamMemberDao.GetMember(Convert.ToInt32(teamMemberPin));
                //                nightWork = new NightWork
                //                {
                //                    NightWorkDate = nightWorkDate,
                //                    TeamMember = member,
                //                    Rank = 10,
                //                    ApprovalType = approvalType
                //                };
                //                _nightWorkDao.Save(nightWork);
                //            }
                //            if (inValuesArray.Any())
                //            {
                //                foreach (var invalMemberTeam in inValuesArray)
                //                {
                //                    if (teamMemberPin == invalMemberTeam)
                //                    {
                //                        nightWork.InHouseWork = true;
                //                        break;
                //                    }
                //                }
                //            }
                //            if (outValuesArray.Any())
                //            {
                //                foreach (var invalMemberTeam in outValuesArray)
                //                {
                //                    if (teamMemberPin == invalMemberTeam)
                //                    {
                //                        nightWork.InHouseWork = false;
                //                        break;
                //                    }
                //                }
                //            }
                //            _nightWorkDao.Save(nightWork);

                //            SaveNewNightWorkLog(nightWorkDate, nightWork, approvalType);
                //        }
                //    }

                //    #endregion

                //    trans.Commit();
                //}
                #endregion

            }
            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        public NightWork LoadById(long id)
        {
            try
            {
                return _nightWorkDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<NightWorkDto> LoadNightWork(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList = null, List<UserMenu> userMenu = null, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, int start = 0, int length = 0, string orderBy = null, string orderDir = null, bool isHistory = false)
        {
            try
            {
                List<long> organizationIdList = new List<long>();
                List<long> branchIdList = new List<long>();
                if (userMenu != null)
                {
                    organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                    branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);
                }
                return _nightWorkDao.LoadNightWork(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdList, campusId, departmentId, start, length, orderBy, orderDir, isHistory);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<NightWork> LoadNightWork(DateTime dateFrom, DateTime dateTo, long teamMemberId)
        {
            try
            {
                return _nightWorkDao.LoadNightWork(dateFrom, dateTo, teamMemberId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int LoadNightWorkCount(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, bool isHistory = false)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);

                return _nightWorkDao.LoadNightWorkCount(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdList, campusId, departmentId, isHistory);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public DateTime? GetTeamMemberNightWorkIsPostDateTime(long? teamMemberId = null, int? pin = null)
        {
            DateTime? postDateTime = null;
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            AllowanceSheet allowanceSheet = _allowanceSheetDao.GetTeamMemberLastIsSubmittedAllowanceSheet(teamMemberId, pin);
            NightWork nightWork = _nightWorkDao.GetTeamMemberLastIsPostNightWork(teamMemberId, pin);

            if (allowanceSheet != null && nightWork != null)
            {
                postDateTime = allowanceSheet.DateTo;
                if (nightWork.NightWorkDate > postDateTime)
                    postDateTime = nightWork.NightWorkDate;
            }
            else if (allowanceSheet != null)
            {
                postDateTime = allowanceSheet.DateTo;
            }
            else if (nightWork != null)
            {
                postDateTime = nightWork.NightWorkDate;
            }
            return postDateTime;
        }

        #endregion

        #region Helper function

        private void SaveNewNightWorkLog(DateTime nightWorkDate, NightWork nightWork, int? approvalType = null)
        {
            try
            {
                NightWorkLog nightWorkLog = new NightWorkLog();
                nightWorkLog.NightWorkDate = nightWorkDate;
                nightWorkLog.NightWork = nightWork;
                nightWorkLog.ApprovalType = approvalType;
                nightWorkLog.CreationDate = DateTime.Now;
                nightWorkLog.CreateBy = GetCurrentUserId();
                Session.Save(nightWorkLog);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion


    }
}
