﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.InstituteTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Institute")]
    public class InstituteUpdateTest : InstituteBaseTest
    {

        #region Basic Test
        [Fact]
        public void Should_Update_Institute()
        {
            InstituteFactory instituteFactory =
                _InstituteFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithInstituteCategory().Persist();
            instituteFactory.Object.Name = instituteFactory.Object.Name+" hgjgjhj";
            bool success=_InstituteService.Update(instituteFactory.Object, instituteFactory.Object.InstituteCategory.Id);
            Assert.True(success);
        }

        [Fact]
        public void Should_Not_Update_Duplicate_Institute()
        {
            InstituteFactory instituteFactory =
                _InstituteFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithInstituteCategory().Persist();
            var institute2=new Institute();
            foreach (PropertyInfo property in typeof(Institute).GetProperties())
            {
                property.SetValue(institute2, property.GetValue(instituteFactory.Object, null), null);
            }
            institute2.Id = 0;
            institute2.Name = instituteFactory.Object.Name + " HJKHJKJK";
            _InstituteService.Save(institute2);
            institute2.Name = instituteFactory.Object.Name;
            Assert.Throws<DuplicateEntryException>(() => _InstituteService.Update(institute2, instituteFactory.Object.InstituteCategory.Id));
            _InstituteFactory.CleanupInstitute(new List<long>() { institute2 .Id});
        }
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Not_Update_Empty_Name()
        {
            InstituteFactory instituteFactory =
                _InstituteFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithInstituteCategory().Persist();
            instituteFactory.Object.Name = "";
            Assert.Throws<EmptyFieldException>(() => _InstituteService.Update(instituteFactory.Object, instituteFactory.Object.InstituteCategory.Id));
        }
        
        #endregion
    }
}
