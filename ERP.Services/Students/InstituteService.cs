﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface INstituteService : IBaseService
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        Institute GetInstitute(long id);
        #endregion

        #region List Loading Function
        IList<Institute> LoadInstituteByQuery(string query, bool isUniversity);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }


    public class InstituteService : BaseService, INstituteService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly InstituteDao _instituteDao;

        public InstituteService(ISession session)
        {
            Session = session;
            _instituteDao = new InstituteDao() { Session = Session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public Institute GetInstitute(long id)
        {
            return Session.QueryOver<Institute>().Where(x => x.Id == id).SingleOrDefault<Institute>();
        }
        #endregion

        #region List Loading Function
        public IList<Institute> LoadInstituteByQuery(string query, bool isUniversity)
        {
            try
            {
                return _instituteDao.LoadInstituteByQuery(query, isUniversity);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }          
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
