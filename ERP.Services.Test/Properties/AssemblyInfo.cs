﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Xunit;
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("UdvashERP.Services.Test")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("UdvashERP.Services.Test")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dbb11c4b-2329-41be-9f8e-362ccd5d6e40")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

 //Put all test classes into a single test collection by default:
//[assembly: CollectionBehavior(CollectionBehavior.CollectionPerAssembly)]
//Default: CollectionBehavior.CollectionPerClass

//Set the maximum number of threads to use when running test in parallel:
  [assembly: CollectionBehavior(MaxParallelThreads = 1)]
//Default: number of virtual CPUs in the PC

//Turn off parallelism inside the assembly:
//[assembly: CollectionBehavior(DisableTestParallelization = true)]
//Default: false

