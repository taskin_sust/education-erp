﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.DirectGoodsIssueTest
{

    [Trait("Area", "UInventory")]
    [Trait("Service", "DirectGoodsIssue")]

    public class DirectGoodsIssueSaveTest : DirectGoodsIssueBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Save_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _goodsIssueService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Return_Save_Successfully()
        {
            var goodsIssueFacotry =  _goodsIssueFactory.Create().WithBranch();
            goodsIssueFacotry.WithGoodsIssueDetails(_goodsIssueFactory.Object.Branch.Organization).Persist();
            var goodsIssue = _goodsIssueService.GetGoodsIssue(_goodsIssueFactory.Object.Id);
            Assert.NotNull(goodsIssue);
            Assert.True(goodsIssue.Id > 0);

        } 
        #endregion

        #region Business Logic

        [Fact]
        public void Should_Return_Branch_Null_Exception()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(organizationFactory.Object, commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), branchFactory.Object);

            _goodsIssueFactory.Create().WithBranch(branchFactory.Object);
            _goodsIssueDetailsFactory.Create()
                .WithProgramAndSession(_programSessionItemFactory.Object.Program,
                    _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object)
                .WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsIssue(_goodsIssueFactory.Object);
            _goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Object);
            _goodsIssueFactory.Object.Branch = null;
            Assert.Throws<NullObjectException>(() => _goodsIssueService.SaveOrUpdate(_goodsIssueFactory.Object));
        }

        [Fact]
        public void Should_Return_Item_Null_Exception()
        {
            _goodsIssueFactory.Create().WithBranch();
            _goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Create().WithProgram().WithSession().WithPurpose().WithGoodsIssue(_goodsIssueFactory.Object).Object);
            Assert.Throws<NullObjectException>(() => _goodsIssueService.SaveOrUpdate(_goodsIssueFactory.Object));

        }

        [Fact]
        public void Should_Return_Issue_Quantity_Exception()
        {
            //_goodsIssueFactory.Create().WithBranch();
            //_goodsIssueDetailsFactory.Create()
            //    .WithProgram()
            //    .WithSession()
            //    .WithItem()
            //    .WithGoodsIssue(_goodsIssueFactory.Object);
            //_goodsIssueDetailsFactory.Object.IssuedQuantity = 0;
            //_goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Object);
            //Assert.Throws<InvalidDataException>(() => _goodsIssueService.SaveOrUpdate(_goodsIssueFactory.Object));
        }

        [Fact]
        public void Should_Successfully_Check_ProgramItem_With_AnyPurpose()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            _goodsIssueFactory.Create().WithBranch(branchFactory.Object);
            _goodsIssueDetailsFactory.Create()
                .WithProgramAndSession(_programSessionItemFactory.Object.Program,
                    _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object)
                .WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsIssue(_goodsIssueFactory.Object);
            _goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Object);
            _goodsIssueService.SaveOrUpdate(_goodsIssueFactory.Object);
            Assert.Equal(_goodsIssueDetailsFactory.Object.PurposeProgramId.Id, _goodsIssueDetailsFactory.Object.Program.Id);
            Assert.Equal(_goodsIssueDetailsFactory.Object.PurposeSessionId.Id, _goodsIssueDetailsFactory.Object.Session.Id);
        }

        [Fact]
        public void Should_Successfully_Check_CommonItem_With_NotApplicablePurpose()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            _goodsIssueFactory.Create().WithBranch(branchFactory.Object);
            _goodsIssueDetailsFactory.Create()
                .WithProgramAndSession(_programSessionItemFactory.Object.Program,
                    _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object)
                .WithPurpose(SelectionType.NotApplicable)
                .WithGoodsIssue(_goodsIssueFactory.Object);
            _goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Object);

            Assert.Equal(_goodsIssueDetailsFactory.Object.PurposeProgramId, null);
            Assert.Equal(_goodsIssueDetailsFactory.Object.PurposeSessionId, null);
        }

        [Fact]
        public void Should_Successfully_Check_CommonItem_With_FirstThreePurpose()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            _goodsIssueFactory.Create().WithBranch(branchFactory.Object);
            _goodsIssueDetailsFactory.Create()
                .WithProgramAndSession(_programSessionItemFactory.Object.Program,
                    _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object)
                .WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsIssue(_goodsIssueFactory.Object);
            _goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Object);
            Assert.Equal(_goodsIssueDetailsFactory.Object.PurposeProgramId, _goodsIssueDetailsFactory.Object.PurposeProgramId);
            Assert.Equal(_goodsIssueDetailsFactory.Object.PurposeSessionId, _goodsIssueDetailsFactory.Object.PurposeSessionId);
        }

        [Fact]
        public void Should_Successfully_Check_CommonItem_With_LastTwoPurpose()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.CommonItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).Persist();
            _goodsIssueFactory.Create().WithBranch(branchFactory.Object);
            _goodsIssueDetailsFactory.Create().WithProgram().WithSession().WithItem(_itemFactory.Object).WithPurpose(Purpose.AdministrativePurpose).WithGoodsIssue(_goodsIssueFactory.Object);
            _goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Object);
            _goodsIssueService.SaveOrUpdate(_goodsIssueFactory.Object);
            Assert.Equal(_goodsIssueDetailsFactory.Object.PurposeProgramId, _goodsIssueDetailsFactory.Object.PurposeProgramId);
            Assert.Equal(_goodsIssueDetailsFactory.Object.PurposeSessionId, _goodsIssueDetailsFactory.Object.PurposeSessionId);
        }

        #endregion
        
    }
}
