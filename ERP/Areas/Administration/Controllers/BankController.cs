﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class BankController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly IBankService _bankService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserService _userService;

        public BankController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _bankService = new BankService(nHsession);
            _userService = new UserService(nHsession);
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Index/Manage Page
        public ActionResult ManageBank()
        {
            try
            {
                GetStatusText();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                GetStatusText();
                return View();
            }
        }

        #region Render Data Table
        public JsonResult BankList(int draw, int start, int length, string name, string shortname, string rank, string status)
        {
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "ShortName";
                            break;
                        case "2":
                            orderBy = "Rank";
                            break;
                        case "3":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                int recordsTotal = _bankService.BankRowCount(name, shortname, rank, status);
                long recordsFiltered = recordsTotal;
                IList<Bank> bankList = _bankService.LoadBankList(start, length, orderBy, orderDir.ToUpper(), name, shortname, rank, status).ToList();
                var obj = new Bank();
                var maxMinRank = new int[2];
                maxMinRank[0] = _bankService.GetMinimumRank(obj);
                maxMinRank[1] = _bankService.GetMaximumRank(obj);
                foreach (var bank in bankList)
                {
                    var str = new List<string> { bank.Name, bank.ShortName };
                    var rankbtn = bank.Rank.ToString();
                    if (bank.Rank > maxMinRank[0])
                        rankbtn += "<a href='#'  id='" + bank.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    if (bank.Rank < maxMinRank[1])
                        rankbtn += "<a href='#'  id='" + bank.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    str.Add(rankbtn);
                    str.Add(StatusTypeText.GetStatusText(bank.Status));
                    str.Add(LinkGenerator.GetGeneratedLink("BankDetails", "EditBank", "DeleteBank", "Bank", bank.Id, bank.Name));
                    getData.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData,
                    isSuccess = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }

        #endregion
        #endregion

        #region Operational Function
        #region Details Operation
        public ActionResult BankDetails(int id)
        {
            var bank = _bankService.GetBank(id);
            if (bank != null)
            {
                bank.CreateByText = _userService.GetUserNameByAspNetUserId(bank.CreateBy);
                bank.ModifyByText = _userService.GetUserNameByAspNetUserId(bank.ModifyBy);
            }
            return View(bank);
        }
        #endregion

        #region Save Operation
        public ActionResult CreateBank(string message)
        {
            ViewBag.SuccessMessage = message;
            return View();
        }

        [HttpPost]
        public ActionResult CreateBank(Bank bank)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool isSuccess = _bankService.SaveOrUpdate(bank);
                    if (isSuccess)
                        return RedirectToAction("CreateBank", new { message = "Bank successfully saved." });
                }
                else
                {
                    return View(bank);
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error("UnknownException", ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                return View();
            }
            return View();
        }
        #endregion

        #region Edit Operation
        public ActionResult EditBank(long id)
        {
            try
            {
                var bank = _bankService.GetBank(id);
                GetStatusText();
                return View(bank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                GetStatusText();
                return View();
            }
        }

        [HttpPost]
        public ActionResult EditBank(long id, Bank bank)
        {
            try
            {
                GetStatusText();
                if (ModelState.IsValid)
                {
                    bool isSuccess = _bankService.SaveOrUpdate(bank);
                    if (isSuccess)
                        ViewBag.SuccessMessage = "Bank successfully updated.";
                    else
                        ViewBag.ErrorMessage = "Problem Occurred, during session update.";
                    return View(bank);
                }
                return View(bank);
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
            }
            return View(bank);
        }

        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult DeleteBank(long id)
        {
            try
            {
                bool isSuccess = _bankService.Delete(id);
                return Json(isSuccess ? new Response(true, "Bank sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Bank Delete Fail !"));
            }
        }
        #endregion
        #endregion

        #region Helper Function
        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }
        #endregion

        #region Rank Operation
        public JsonResult RankChange(long id, int current, string action)
        {
            try
            {
                var isSuccess = _bankService.UpdateRank(id, current, action);
                return Json(isSuccess ? "Rank Changes" : "Something Wrong Occured!!");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Rank Changes, Something Wrong Occured!!"));
            }

        }
        #endregion

    }
}
