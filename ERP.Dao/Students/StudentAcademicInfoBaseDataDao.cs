﻿using System;
using NHibernate;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentAcademicInfoBaseDataDao : IBaseDao<StudentAcademicInfoBaseData, long>
    {
        #region Operational Function
        bool SaveInfoBaseData(StudentAcademicInfoBaseData infoBaseData);
        bool UpdateInfoBaseData(StudentAcademicInfoBaseData existInfoBaseData);
        #endregion

        #region Single Instances Loading Function
        StudentAcademicInfoBaseData LoadByAcademicInfoId(long id);
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion
    }

    public class StudentAcademicInfoBaseDataDao : BaseDao<StudentAcademicInfoBaseData, long>, IStudentAcademicInfoBaseDataDao
    {        
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function
        public bool SaveInfoBaseData(StudentAcademicInfoBaseData infoBaseData)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Session.Save(infoBaseData);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                return false;
            }

        }
        public bool UpdateInfoBaseData(StudentAcademicInfoBaseData existInfoBaseData)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Session.Update(existInfoBaseData);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                return false;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public StudentAcademicInfoBaseData LoadByAcademicInfoId(long id)
        {
            StudentAcademicInfoBaseData studentAcademicInfoBaseData;            
            studentAcademicInfoBaseData=Session.QueryOver<StudentAcademicInfoBaseData>()
                .Where(x => x.StudentAcademicInfo.Id == id).SingleOrDefault<StudentAcademicInfoBaseData>();          
            return studentAcademicInfoBaseData;
        }
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
