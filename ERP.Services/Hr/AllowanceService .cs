﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using Enumerable = System.Linq.Enumerable;

namespace UdvashERP.Services.Hr
{
    public interface IAllowanceService : IBaseService
    {
        #region Operational Function

        //void Save(List<UserMenu> userMenu, string date, string pinList, string allowValues, string disallowValues, bool isMentor = false);
        void Save(string date, string pinList, string allowValues, string disallowValues);
        //void Update(List<UserMenu> userMenu, Allowance hrAllowance, bool isMentor = false);
        void Update(Allowance hrAllowance);

        #endregion

        #region Single Instances Loading Function

        Allowance GetAllowance(string pin, DateTime date);
        Allowance GetAllowance(long id);

        #endregion

        #region List Loading Function

        IList<DailyAllowanceDto> LoadDailyAllowance(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, List<UserMenu> userMenu = null, string pinList = null, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, int start = 0, int length = 0, string orderBy = null, string orderDir = null, bool isHistory = false);

        #endregion

        #region Others Function

        int DailyAllowanceRowCount(DateTime dateFrom, DateTime dateTo, List<UserMenu> userMenu, List<long> mentorTeamMemberIdList, string pinList, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, bool isHistory = false);

        #endregion

        #region Helper Function
        #endregion


    }
    public class AllowanceService : BaseService, IAllowanceService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private const int DefaultMentorPin = 2;
        private readonly CommonHelper _commonHelper;
        private readonly IAllowanceDao _hrAllowanceDao;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly IMentorHistoryDao _hrMentorHistoryDao;
        private readonly IUserProfileDao _userProfileDao;
        private readonly IAttendanceSummaryDao _attendanceSummaryDao;
        public AllowanceService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrAllowanceDao = new AllowanceDao() { Session = session };
            _teamMemberDao = new TeamMemberDao { Session = session };
            _hrMentorHistoryDao = new MentorHistoryDao() { Session = session };
            _userProfileDao = new UserProfileDao() { Session = session };
            _attendanceSummaryDao = new AttendanceSummaryDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public void Save(string date, string pinList, string allowValues, string disallowValues)
        {
            ITransaction trans = null;
            try
            {
                var pinArray = pinList.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                var allowValuesArray = allowValues.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                var disallowValuesArray = disallowValues.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                if (string.IsNullOrEmpty(date) || !pinArray.Any())
                {
                    throw new InvalidDataException("Please Enter Night work date and Pin");
                }
                using (trans = Session.BeginTransaction())
                {
                    DateTime dailyAllowanceDate = Convert.ToDateTime(date).Date;
                    if (disallowValues.Any())
                    {
                        //No daily allowance
                        foreach (var teamMemberPin in disallowValuesArray)
                        {
                            var allowance = _hrAllowanceDao.GetAllowance(teamMemberPin, dailyAllowanceDate);
                            if (allowance != null)
                            {
                                allowance.IsApproved = false;
                                _hrAllowanceDao.Save(allowance);
                            }
                            else
                            {
                                TeamMember member = _teamMemberDao.GetMember(Convert.ToInt32(teamMemberPin));
                                allowance = new Allowance
                                {
                                    DailyAllowanceDate = dailyAllowanceDate,
                                    TeamMember = member,
                                    Rank = 10,
                                    IsApproved = false
                                };
                                _hrAllowanceDao.Save(allowance);
                            }
                            SaveAllowanceLog(dailyAllowanceDate, allowance, false);
                        }
                    }
                    if (allowValues.Any())
                    {
                        //daily allowance
                        foreach (var teamMemberPin in allowValuesArray)
                        {
                            var allowance = _hrAllowanceDao.GetAllowance(teamMemberPin, dailyAllowanceDate);
                            if (allowance != null)
                            {
                                allowance.IsApproved = true;
                                _hrAllowanceDao.Save(allowance);
                            }
                            else
                            {
                                TeamMember member = _teamMemberDao.GetMember(Convert.ToInt32(teamMemberPin));
                                allowance = new Allowance
                                {
                                    DailyAllowanceDate = dailyAllowanceDate,
                                    TeamMember = member,
                                    Rank = 10,
                                    IsApproved = true
                                };
                                _hrAllowanceDao.Save(allowance);
                            }
                            SaveAllowanceLog(dailyAllowanceDate, allowance, true);
                        }
                    }
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public void Update(Allowance hrAllowance)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    _hrAllowanceDao.Save(hrAllowance);

                    AllowanceLog hrAllowanceLog = new AllowanceLog();
                    hrAllowanceLog.DailyAllowanceDate = hrAllowance.DailyAllowanceDate;
                    hrAllowanceLog.Allowance = hrAllowance;
                    hrAllowanceLog.IsApproved = hrAllowance.IsApproved;
                    hrAllowanceLog.CreationDate = DateTime.Now;
                    hrAllowanceLog.CreateBy = hrAllowanceLog.CreateBy != 0 ? hrAllowanceLog.CreateBy : Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                    Session.Save(hrAllowanceLog);
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        //public void Save(List<UserMenu> userMenu, string date, string pinList, string allowValues, string disallowValues, bool isMentor = false)
        //{
        //    ITransaction trans = null;
        //    try
        //    {
        //        if (userMenu == null || userMenu.Count <= 0)
        //        {
        //            throw new ArgumentNullException("userMenu", "Authentication failed");
        //        }
        //        var pinArray = pinList.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
        //        if (string.IsNullOrEmpty(date) || !pinArray.Any()
        //            || (Enumerable.Count(allowValues) <= 0 && Enumerable.Count(disallowValues) <= 0))
        //        {
        //            throw new InvalidDataException("Please Enter Allowance date and Pin");
        //        }
        //        var allowValuesArray = allowValues.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
        //        var disallowValuesArray = disallowValues.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);

        //        var identicalList = Enumerable.ToList(allowValuesArray.Except(disallowValuesArray));
        //        if (identicalList.Count != Enumerable.Count(allowValuesArray))
        //        {
        //            throw new InvalidDataException("One or many pin found both allowing and disallowing allowance!");
        //        }
        //        var memberPinList = new List<int>();
        //        memberPinList.AddRange(Enumerable.ToList(Enumerable.Select(allowValuesArray, av => Convert.ToInt32(av))));
        //        memberPinList.AddRange(Enumerable.ToList(Enumerable.Select(disallowValuesArray, dv => Convert.ToInt32(dv))));

        //        //Validate selected pin's Mentor
        //        if (isMentor)
        //        {
        //            long currentUserId = GetCurrentUserId();

        //            //Used default mentor Id as 128 for service test.

        //            long userId = currentUserId > 0 ? currentUserId : 128;
                    
        //            UserProfile user = _userProfileDao.GetByAspNetUser(userId);
        //            TeamMember mentor = _teamMemberDao.LoadByUserProfile(user);

        //            if (mentor == null)
        //            {
        //                throw new InvalidDataException("You don't have a \"Team Member\" profile!");
        //            }

        //            var authPinList = _hrMentorHistoryDao.LoadMentorTeamMemberPinList(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now, null, memberPinList);
        //            if (memberPinList.Count != authPinList.Count)
        //            {
        //                throw new InvalidDataException("You don't have enough permission!");
        //            }
        //        }

        //        //Validate logged user branch and organization's authorization

        //        var authorizeBranchs = AuthHelper.LoadBranchIdList(userMenu);
        //        foreach (var pin in memberPinList)
        //        {
        //            var branch = _teamMemberDao.GetTeamMemberBranch(Convert.ToDateTime(date), null, pin);
        //            if (branch == null || !authorizeBranchs.Contains(branch.Id))
        //            {
        //                throw new InvalidDataException("You don't have enough permission!");
        //            }
        //            var att = _attendanceSummaryDao.GetByPinAndDate(pin, Convert.ToDateTime(date));
        //            if (att == null)
        //            {
        //                throw new InvalidDataException("One or many pin don't have attendence in selected date!");
        //            }
        //        }

        //        using (trans = Session.BeginTransaction())
        //        {
        //            DateTime dailyAllowanceDate = Convert.ToDateTime(date).Date;
        //            if (disallowValues.Any())
        //            {
        //                //No daily allowance
        //                foreach (var teamMemberPin in disallowValuesArray)
        //                {
        //                    var allowance = _hrAllowanceDao.GetAllowance(teamMemberPin, dailyAllowanceDate);
        //                    if (allowance != null)
        //                    {
        //                        allowance.IsApproved = false;
        //                        _hrAllowanceDao.Save(allowance);
        //                    }
        //                    else
        //                    {
        //                        TeamMember member = _teamMemberDao.GetMember(Convert.ToInt32(teamMemberPin));
        //                        allowance = new Allowance
        //                        {
        //                            DailyAllowanceDate = dailyAllowanceDate,
        //                            TeamMember = member,
        //                            Rank = 10,
        //                            IsApproved = false
        //                        };
        //                        _hrAllowanceDao.Save(allowance);
        //                    }
        //                    SaveAllowanceLog(dailyAllowanceDate, allowance, false);
        //                }
        //            }
        //            if (allowValues.Any())
        //            {
        //                //daily allowance
        //                foreach (var teamMemberPin in allowValuesArray)
        //                {
        //                    var allowance = _hrAllowanceDao.GetAllowance(teamMemberPin, dailyAllowanceDate);
        //                    if (allowance != null)
        //                    {
        //                        allowance.IsApproved = true;
        //                        _hrAllowanceDao.Save(allowance);
        //                    }
        //                    else
        //                    {
        //                        TeamMember member = _teamMemberDao.GetMember(Convert.ToInt32(teamMemberPin));
        //                        allowance = new Allowance
        //                        {
        //                            DailyAllowanceDate = dailyAllowanceDate,
        //                            TeamMember = member,
        //                            Rank = 10,
        //                            IsApproved = true
        //                        };
        //                        _hrAllowanceDao.Save(allowance);
        //                    }
        //                    SaveAllowanceLog(dailyAllowanceDate, allowance, true);
        //                }
        //            }
        //            trans.Commit();
        //        }
        //    }
        //    catch (ArgumentNullException ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    catch (InvalidDataException ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    finally
        //    {
        //        if (trans != null && trans.IsActive)
        //            trans.Rollback();
        //    }
        //}

        //public void Update(List<UserMenu> userMenu, Allowance entity, bool isMentor = false)
        //{
        //    ITransaction trans = null;
        //    try
        //    {
        //        if (userMenu == null || userMenu.Count <= 0)
        //        {
        //            throw new ArgumentNullException("userMenu", "Authentication failed");
        //        }
        //        CheckValidation(entity);
        //        if (isMentor)
        //        {
        //            long currentUserId = GetCurrentUserId();
                    
        //            //Used default mentor Id as 128 for service test.
                    
        //            long userId = currentUserId > 0 ? currentUserId : 128;
        //            UserProfile user = _userProfileDao.GetByAspNetUser(userId);
        //            TeamMember mentor = _teamMemberDao.LoadByUserProfile(user);

        //            if (mentor == null)
        //            {
        //                throw new InvalidDataException("You don't have a \"Team Member\" profile!");
        //            }
        //            var authIds = _hrMentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now, null, new List<int>() { entity.TeamMember.Pin });
        //            if (authIds == null || authIds.Count <= 0 || entity.TeamMember.Id != authIds[0])
        //            {
        //                throw new InvalidDataException("You don't have enough permission!");
        //            }
        //        }
        //        var authorizeBranchs = AuthHelper.LoadBranchIdList(userMenu);
        //        var branch = _teamMemberDao.GetTeamMemberBranch(Convert.ToDateTime(entity.DailyAllowanceDate), null, entity.TeamMember.Pin);
        //        if (branch == null || !authorizeBranchs.Contains(branch.Id))
        //        {
        //            throw new InvalidDataException("You don't have enough permission!");
        //        }

        //        using (trans = Session.BeginTransaction())
        //        {
        //            _hrAllowanceDao.Save(entity);

        //            var hrAllowanceLog = new AllowanceLog();
        //            hrAllowanceLog.DailyAllowanceDate = entity.DailyAllowanceDate;
        //            hrAllowanceLog.Allowance = entity;
        //            hrAllowanceLog.IsApproved = entity.IsApproved;
        //            hrAllowanceLog.CreationDate = DateTime.Now;
        //            hrAllowanceLog.CreateBy = GetCurrentUserId();
        //            if (!string.IsNullOrEmpty(entity.BusinessId))
        //                hrAllowanceLog.BusinessId = entity.BusinessId;
        //            Session.Save(hrAllowanceLog);
        //            trans.Commit();
        //        }
        //    }

        //    catch (ArgumentNullException ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    catch (InvalidDataException ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    finally
        //    {
        //        if (trans != null && trans.IsActive)
        //            trans.Rollback();
        //    }
        //}

        private void SaveAllowanceLog(DateTime dailyAllowanceDate, Allowance allowance, bool isApproved)
        {
            try
            {
                var hrAllowanceLog = new AllowanceLog();
                hrAllowanceLog.DailyAllowanceDate = dailyAllowanceDate;
                hrAllowanceLog.Allowance = allowance;
                hrAllowanceLog.IsApproved = isApproved;
                hrAllowanceLog.CreationDate = DateTime.Now;
                hrAllowanceLog.CreateBy = GetCurrentUserId();
                Session.Save(hrAllowanceLog);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public Allowance GetAllowance(string pin, DateTime date)
        {
            try
            {
                return _hrAllowanceDao.GetAllowance(pin, date);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Allowance GetAllowance(long id)
        {
            try
            {
                return _hrAllowanceDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<DailyAllowanceDto> LoadDailyAllowance(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, List<UserMenu> userMenu = null, string pinList = null, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, int start = 0, int length = 0, string orderBy = null, string orderDir = null,
            bool isHistory = false)
        {
            try
            {
                //if (userMenu == null || userMenu.Count <= 0)
                //{
                //    throw new ArgumentNullException();
                //}
                //var organizationIdList = new List<long>();
                //var branchIdList = new List<long>();

                //organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                //branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);

                //if (branchIdList.Count <= 0)
                //{
                //    throw new InvalidDataException();
                //}

                //if (mentorTeamMemberIdList.Count > 0 && mentorTeamMemberIdList[0] > 0)
                //{
                //    long mentorId = GetCurrentUserId();
                //    if (mentorId <= 0)
                //    {
                //        mentorId = _teamMemberDao.GetMember(DefaultMentorPin).Id;
                //    }
                //    //Used default mentor Id as 128 below for service test.
                //    var authPinList = _hrMentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentorId), DateTime.Now, mentorTeamMemberIdList);
                //    if (authPinList.Count <= 0)
                //    {
                //        throw new InvalidDataException("Requested for unauthorized team member information!");
                //    }
                //}
                List<long> organizationIdList = new List<long>();
                List<long> branchIdList = new List<long>();
                if (userMenu != null)
                {
                    organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                    branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);
                }

                return _hrAllowanceDao.LoadDailyAllowance(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdList, campusId, departmentId, start, length, orderBy, orderDir, isHistory);
            }
            catch (ArgumentNullException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int DailyAllowanceRowCount(DateTime dateFrom, DateTime dateTo, List<UserMenu> userMenu, List<long> mentorTeamMemberIdList, string pinList, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, bool isHistory = false)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);

                return _hrAllowanceDao.DailyAllowanceRowCount(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdList, campusId, departmentId, isHistory);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        private static void CheckValidation(Allowance entity)
        {
            var validationResult = ValidationHelper.ValidateEntity<Allowance, Allowance>(entity);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
            if (entity.DailyAllowanceDate == null)
            {
                throw new InvalidDataException("Please Enter Night work date and Pin");
            }
        }

        #endregion
    }
}
