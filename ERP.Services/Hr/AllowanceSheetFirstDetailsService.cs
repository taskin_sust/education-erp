﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Dao.Hr;
using UdvashERP.Services.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Services.Hr
{
    public interface IAllowanceSheetFirstDetailsService : IBaseService
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
       
        #endregion

        #region List Loading Function

        IList<AllowanceSheetFirstDetails> GetAllowanceSheetFirstDetailByOvertimeAllowanceSheet(long overtimeAllowanceSheetId);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }

    public class AllowanceSheetFirstDetailsService : BaseService, IAllowanceSheetFirstDetailsService
    {
        #region Propertise & Object Initialization

        private readonly IAllowanceSheetFirstDetailsDao _allowanceSheetFirstDetailsDao;
        public AllowanceSheetFirstDetailsService(ISession session)
        {
            Session = session;
            _allowanceSheetFirstDetailsDao = new AllowanceSheetFirstDetailsDao() { Session = session };
        }

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public IList<AllowanceSheetFirstDetails> GetAllowanceSheetFirstDetailByOvertimeAllowanceSheet(long overtimeAllowanceSheetId)
        {
            return _allowanceSheetFirstDetailsDao.GetAllowanceSheetFirstDetailByOvertimeAllowanceSheet(overtimeAllowanceSheetId);
        }

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

       
    }
}
