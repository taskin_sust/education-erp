﻿using System;
using System.Security.Authentication;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ChildrenAllowanceBlockTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ChildrenAllowanceBlockService")]
    public class ChildrenAllowanceBlockSaveTest : ChildrenAllowanceBlockBaseTest
    {
        #region validation and business logic

        [Fact]
        public void Should_Return_Invalid_Object()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;
            Assert.Throws<InvalidDataException>(() => _allowanceBlockService.SaveOrUpdate(menu, null));
        }

        [Fact]
        public void Should_Return_Invalid_Permission()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;
            Assert.Throws<AuthenticationException>(() => _allowanceBlockService.SaveOrUpdate(null, _allowanceBlockFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_DateTime()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;

            _allowanceBlockFactory.Object.EffectiveDate = DateTime.Now.AddDays(5);
            _allowanceBlockFactory.Object.ClosingDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(() => _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;
            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);
            Assert.True(_allowanceBlockFactory.Object.Id > 0);
        }

        [Fact]
        public void Should_Return_Invalid_Edit_Object()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;
            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);

            Assert.Throws<InvalidDataException>(
                () => _allowanceBlockService.SaveOrUpdate(menu, null, true));

        }

        [Fact]
        public void Should_Return_Invalid_Edit_Permission()
        {
            Assert.Throws<InvalidDataException>(() => _allowanceBlockService.SaveOrUpdate(null, _allowanceBlockFactory.Object, true));
            }

        [Fact]
        public void Should_Return_Invalid_Edit_DateTime()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _allowanceBlockFactory.Create().WithTeamMember(teamMember).WithCkAllowance(_childrenAllowanceSettingFactory.Object);
            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);

            
            var abService = _allowanceBlockService.GetById(_allowanceBlockFactory.Object.Id);
            Session.Evict(abService);
            abService.EffectiveDate = DateTime.Now.AddDays(5);
            abService.ClosingDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(() => _allowanceBlockService.SaveOrUpdate(menu, abService, true));
        }

        [Fact]
        public void Should_Edit_Successfully()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;
            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);

            _allowanceBlockFactory.Object.EffectiveDate = DateTime.Now;
            _allowanceBlockFactory.Object.ClosingDate = DateTime.Now.AddDays(5);

            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object, true);
        }

        [Fact]
        public void Should_Return_Duplicate_Entry_Exception()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;
            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);
            var entity = new ChildrenAllowanceBlock
            {
                BusinessId = organizationFactory.Object.BusinessId,
                ChildrenAllowanceSetting = _allowanceBlockFactory.Object.ChildrenAllowanceSetting,
                TeamMember = _allowanceBlockFactory.Object.TeamMember,
                EffectiveDate = _allowanceBlockFactory.Object.EffectiveDate,
                ClosingDate = _allowanceBlockFactory.Object.ClosingDate
            };

            Assert.Throws<DuplicateEntryException>(() => _allowanceBlockService.SaveOrUpdate(menu, entity));
        }

        #endregion
    }
}
