using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class HolidayWorkLogMap : ClassMap<HolidayWorkLog> {
        
        public HolidayWorkLogMap() {
			Table("HR_HolidayWorkLog");
			LazyLoad();
            
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
            Map(x => x.HolidayWorkDate).Column("HolidayWorkDate");
            Map(x => x.ApprovalType).Column("ApprovalType");
            Map(x => x.Reason).Column("Reason");
            Map(x => x.HolidayWorkId).Column("HolidayWorkId");

            //References(x => x.HolidayWork).Column("HolidayWorkId");
        }
    }
}
