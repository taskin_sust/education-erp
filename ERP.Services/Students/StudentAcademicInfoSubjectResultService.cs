﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentAcademicInfoSubjectResultService : IBaseService
    {
        #region Operational Function
        bool Save(CommonHelper.BoardSubjectResult subjectResult, StudentAcademicInfoExamSubject infoExamSubject, StudentAcademicInfoBaseData infoBaseData);
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
    public class StudentAcademicInfoSubjectResultService : BaseService, IStudentAcademicInfoSubjectResultService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentAcademicInfoSubjectResultDao _studentAcademicInfoSubjectResultDao;
        private readonly IStudentDao _studentDao;
        public StudentAcademicInfoSubjectResultService(ISession session)
        {
            Session = session;
            _studentAcademicInfoSubjectResultDao = new StudentAcademicInfoSubjectResultDao() { Session = Session };
            _studentDao = new StudentDao() { Session = Session };
        }

        #endregion

        #region Operational Function
        public bool Save(CommonHelper.BoardSubjectResult subjectResult, StudentAcademicInfoExamSubject infoExamSubject, StudentAcademicInfoBaseData infoBaseData)
        {
            try
            {
                StudentAcademicInfoSubjectResult subjectResultObj = new StudentAcademicInfoSubjectResult();
                subjectResultObj.CreationDate = DateTime.Now;
                subjectResultObj.Grade = subjectResult.Grade;
                subjectResultObj.StudentAcademicInfoExamSubject = infoExamSubject;
                subjectResultObj.StudentAcademicInfoBaseData = infoBaseData;
                _studentAcademicInfoSubjectResultDao.SaveBoardResult(subjectResultObj);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

        }
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion


    }
}
