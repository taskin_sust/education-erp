﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IAttendanceDeviceDao : IBaseDao<AttendanceDevice, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        AttendanceDevice GetDevice(int machineNo, long synchronizerId, string ipAddress, long id);
        AttendanceDevice GetAttendanceDeviceByMachineNo(long deviceId);

        #endregion

        #region List Loading Function

        List<AttendanceDevice> LoadDevices(int start, int length, string orderBy, string orderDir, string organizationId, string branchId, string campusId, string status, string iPAddress);
        #endregion

        #region Others Function

        int DeviceRowCount(string organizationId, string branchId, string campusId, string status, string iPAddress);

        #endregion

        #region Helper Function

        #endregion



    }
    public class AttendanceDeviceDao : BaseDao<AttendanceDevice, long>, IAttendanceDeviceDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public AttendanceDevice GetDevice(int machineNo, long synchronizerId, string ipAddress, long id)
        {
            if (id > 0)
            {
                return Session.QueryOver<AttendanceDevice>().Where(x =>
                    x.Status != AttendanceDevice.EntityStatus.Delete &&
                    x.MachineNo == machineNo &&
                    x.AttendanceSynchronizer.Id == synchronizerId &&
                    x.IpAddress == ipAddress &&
                    x.Id != id
               ).SingleOrDefault<AttendanceDevice>();
            }
            else
            {
                return Session.QueryOver<AttendanceDevice>().Where(x =>
                    x.Status != AttendanceDevice.EntityStatus.Delete &&
                    x.MachineNo == machineNo &&
                    x.AttendanceSynchronizer.Id == synchronizerId &&
                    x.IpAddress == ipAddress &&
                    x.Id != id
                ).SingleOrDefault<AttendanceDevice>();
            }

        }

        public AttendanceDevice GetAttendanceDeviceByMachineNo(long deviceId)
        {
            var criteria = Session.CreateCriteria<AttendanceDevice>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", AttendanceDevice.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("Id", deviceId));
            return criteria.UniqueResult<AttendanceDevice>();
        }

        #endregion

        #region List Loading Function
        public List<AttendanceDevice> LoadDevices(int start, int length, string orderBy, string orderDir, string organizationId, string branchId, string campusId, string status, string iPAddress)
        {
            ICriteria criteria = GetDeviceCriteria(organizationId, branchId, campusId, status, iPAddress);
            if (orderDir == "ASC")
            {
                criteria.AddOrder(Order.Asc(orderBy));
            }
            else
            {
                criteria.AddOrder(Order.Desc(orderBy));
            }

            return (List<AttendanceDevice>)criteria.SetFirstResult(start).SetMaxResults(length).List<AttendanceDevice>();
        }
        #endregion

        #region Others Function
        public int DeviceRowCount(string organizationId, string branchId, string campusId, string status, string iPAddress)
        {
            ICriteria criteria = GetDeviceCriteria(organizationId, branchId, campusId, status, iPAddress);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
        #endregion

        #region Helper Function

        private ICriteria GetDeviceCriteria(string organizationId, string branchId, string campusId, string status, string iPAddress)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceDevice>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", AttendanceDevice.EntityStatus.Delete)));
            criteria.CreateAlias("Campus", "c").Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));
            criteria.CreateAlias("c.Branch", "b").Add(Restrictions.Eq("b.Status", Branch.EntityStatus.Active));

            if (!String.IsNullOrEmpty(organizationId))
            {
                criteria.CreateAlias("b.Organization", "o").Add(Restrictions.Eq("o.Status", Branch.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("o.Id", Convert.ToInt64(organizationId)));
            }
            if (!String.IsNullOrEmpty(branchId))
            {
                criteria.Add(Restrictions.Eq("b.Id", Convert.ToInt64(branchId)));
            }
            if (!String.IsNullOrEmpty(campusId))
            {
                criteria.Add(Restrictions.Eq("c.Id", Convert.ToInt64(campusId)));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!String.IsNullOrEmpty(iPAddress))
            {
                criteria.Add(Restrictions.Like("IpAddress", iPAddress, MatchMode.Anywhere));
            }
            return criteria;
        }

        #endregion


    }
}
