using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using log4net;
using NHibernate;
using NHibernate.Event;
using NHibernate.Util;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface ISupplierItemService : IBaseService
    {
        #region Operational Function

        bool Save(IList<SupplierItem> supplierItemList);

        bool SaveOrUpdate(string[] itemIds, string[] supplierIds);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion


    }
    public class SupplierItemService : BaseService, ISupplierItemService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly ISupplierItemDao _supplierItemDao;
        private readonly IItemDao _itemDao;
        private readonly ISupplierDao _supplierDao;
        public SupplierItemService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _supplierItemDao = new SupplierItemDao { Session = session };
            _itemDao = new ItemDao() { Session = session };
            _supplierDao = new SupplierDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool Save(IList<SupplierItem> supplierItemList)
        {
            //ITransaction trans = null;
            try
            {
                foreach (var asi in supplierItemList)
                {
                    _supplierItemDao.Save(asi);
                }
                //using (trans = Session.BeginTransaction())
                //{
                //    foreach (var asi in supplierItemList)
                //    {
                //        _supplierItemDao.Save(asi);
                //    }
                //    trans.Commit();
                //    return true;
                //}
                return true;
            }
            catch (Exception ex)
            {
                //if (trans != null) trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }


        public bool SaveOrUpdate(string[] itemIds, string[] supplierIds)
        {
            ITransaction trans = null;
            try
            {
                if (itemIds == null) throw new NullObjectException("ItemList can't be Empty");
                IList<Item> items = _itemDao.LoadByIds(itemIds);
                IList<Supplier> suppliers = null;
                IList<SupplierItem> addsupplierItems = new List<SupplierItem>();
                IList<SupplierItem> removableSupplierItemList = new List<SupplierItem>();
                using (trans = Session.BeginTransaction())
                {
                    if (supplierIds != null)
                    {
                        suppliers = _supplierDao.LoadByIds(supplierIds); //10,12 
                    }
                    foreach (var itemObj in items)
                    {
                        IList<SupplierItem> removableSupplierItem = _supplierItemDao.AssignedThisItemToSupplier(itemObj.Id);
                        if (suppliers == null)
                        {
                            foreach (var supplierItem in removableSupplierItem)
                            {
                                removableSupplierItemList.Add(supplierItem);
                            }
                        }
                        else
                        {
                            foreach (var rs in removableSupplierItem) //5,10
                            {
                                var isRemove = suppliers.FirstOrDefault(x => x.Id == rs.Id);
                                if (isRemove == null)
                                {
                                    removableSupplierItemList.Add(rs);
                                }
                            }
                            foreach (var supplierObj in suppliers)
                            {
                                var supplierItem = new SupplierItem();
                                supplierItem.Item = itemObj;
                                supplierItem.Supplier = supplierObj;
                                addsupplierItems.Add(supplierItem);
                            }
                        }
                    }
                    foreach (var rsi in removableSupplierItemList)
                    {
                        _supplierItemDao.Delete(rsi);
                    }
                    Save(addsupplierItems);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }

        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}

