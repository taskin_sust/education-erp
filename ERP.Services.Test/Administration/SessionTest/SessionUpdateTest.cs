﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SessionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Session")]
    public class SessionUpdateTest : SessionBaseTest
    {
        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var sessionFactory =
                 _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                     .Persist();
            Assert.Throws<NullObjectException>(() => _sessionService.Update(sessionFactory.Object.Id, null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var sessionFactory =
                _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            Session sessionObj = _sessionService.LoadById(sessionFactory.Object.Id);
            sessionObj.Name = sessionObj.Name + " DEF GHI";
            var success = _sessionService.Update(sessionObj.Id, sessionObj);
            Assert.True(success);
        }
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Not_Update_Empty_Name()
        {
            var sessionFactory =
                _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            sessionFactory.Object.Name = "";
            Assert.Throws<EmptyFieldException>(() => _sessionService.Update(sessionFactory.Object.Id, sessionFactory.Object));
        }
        [Fact]
        public void Should_Not_Update_Empty_Code()
        {
            var sessionFactory =
                 _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                     .Persist();
            sessionFactory.Object.Code = "";
            Assert.Throws<EmptyFieldException>(() => _sessionService.Update(sessionFactory.Object.Id, sessionFactory.Object));
        }
        [Fact]
        public void Update_Should_Check_Name_Duplicate_Entry()
        {
            _sessionFactory.CreateMore(2).Persist();
            _sessionFactory.ObjectList[1].Name = _sessionFactory.ObjectList[0].Name;
            Assert.Throws<DuplicateEntryException>(() => _sessionService.Update(_sessionFactory.ObjectList[1].Id, _sessionFactory.ObjectList[1]));
            _sessionFactory.CleanupSession(new List<long>() { _sessionFactory.ObjectList[1].Id });
        }
        [Fact]
        public void Update_Should_Check_Code_Duplicate_Entry()
        {
            _sessionFactory.CreateMore(2).Persist();
            _sessionFactory.ObjectList[1].Code = _sessionFactory.ObjectList[0].Code;
            Assert.Throws<DuplicateEntryException>(() => _sessionService.Update(_sessionFactory.ObjectList[1].Id, _sessionFactory.ObjectList[1]));
            _sessionFactory.CleanupSession(new List<long>() { _sessionFactory.ObjectList[1].Id });
        }
        [Fact]
        public void Model_Validation_For_Invalid_Code_Max_Lenght()
        {
            var sessionFactory =
                _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            Session sessionObj = _sessionService.LoadById(sessionFactory.Object.Id);
            sessionObj.Code = "111";
            Assert.Throws<InvalidDataException>(() => _sessionService.Update(sessionObj.Id, sessionObj));
        }
        [Fact]
        public void Model_Validation_For_Invalid_Code_Min_Lenght()
        {
            var sessionFactory =
                _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            Session sessionObj = _sessionService.LoadById(sessionFactory.Object.Id);
            sessionObj.Code = "1";
            Assert.Throws<InvalidDataException>(() => _sessionService.Update(sessionObj.Id, sessionObj));
        }
        [Fact]
        public void Model_Validation_For_Invalid_Code_Format()
        {
            var sessionFactory =
                _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            Session sessionObj = _sessionService.LoadById(sessionFactory.Object.Id);
            sessionObj.Code = "QQ";
            Assert.Throws<InvalidDataException>(() => _sessionService.Update(sessionObj.Id, sessionObj));
        }
        [Fact]
        public void Model_Validation_Invalid_Code_Format()
        {
            var sessionFactory =
                _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            Session sessionObj = _sessionService.LoadById(sessionFactory.Object.Id);
            sessionObj.Code = "00";
            Assert.Throws<InvalidDataException>(() => _sessionService.Update(sessionObj.Id, sessionObj));
        }
        #endregion
    }
}
