﻿
$(document).ready(function () {
    $('#countBtn').attr('disabled', 'disabled');
    var teacherIds = [];
    var lectureIds = [];
    console.log("EXAM EVALUATION INITIALIZEDs");
    //organization change functionalities
    $("#OrganizationId").change(function () {
        var organizatonId = $(this).val();
        $("#ProgramId option").remove();
        $("#SelectedCourse option").remove();
        $("#SelectedBranch option").remove();
        $("#SelectedCampus option").remove();
        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();
        $('#SessionId').empty();
        $("FromTime").val('');
        $("#ToTime").val('');
        $("#SelectedLecture option").remove();
        $("#SelectedTeacher option").remove();
        $("#SelectedSubject option").remove();
        $('#SessionId').append("<option value=''>Select Session</option>");
        //$('#SelectedCourse').append("<option value=''>Select Course</option>");
        //$('#SelectedBranch').append("<option value=''>Select Branch</option>");
        //$('#SelectedCampus').append("<option value=''>Select Campus</option>");
        //$('#SelectedBatchDays').append("<option value=''>Select Batch Days</option>");
        //$('#SelectedBatchTime').append("<option value=''>Select Batch Time</option>");
        //$('#SelectedBatch').append("<option value=''>Select Batch</option>");
        if (organizatonId != "") {
            $.ajax({
                //url: 'GetProgramTeacherByOrganization',
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                type: "POST",
                data: { organizationIds: organizatonId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    if (response.IsSuccess) {
                        //load programs
                        $('#ProgramId').append('<option value="">Select Program</option>');
                        if (response.returnProgramList != null && response.returnProgramList.length > 0) {
                            $.each(response.returnProgramList, function (i, v) {
                                $('#ProgramId').append($('<option>').text(v.Text).attr('value', v.Value));
                            });

                            $("#ProgramId").find('option:eq(0)').prop('selected', true);
                        }

                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error:
                    function (response) {
                        var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a> Program and Branch load failed</div>';
                        $('.customMessage').append(errorMessage);

                    }
            });

        } else {

            $('#ProgramId').append('<option value="">Select Program</option>');
        }
    });
    //Program Change Functionalities
    $(document).on('change', '#ProgramId', function () {

        $("#missingImageCount").html("...");

        //remove all dependant dropdownlist & selection option
        $("#SelectedCourse option").remove();
        $("#SelectedBranch option").remove();
        $("#SelectedCampus option").remove();

        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();
        $("#SelectedLecture option").remove();
        $("#SelectedTeacher option").remove();
        $("#SelectedSubject option").remove();
        $("FromTime").val('');
        $("#ToTime").val('');

        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select session</option>");


        var programId = $("#ProgramId").val();

        if (programId != null && programId != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForSession",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                cache: false,
                async: true,
                data: { programIds: programId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $.each(result.returnSessionList, function (i, v) {
                        $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                    //$("#SessionId").html(result);
                    console.log("Success -----");
                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    //
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    
                }
            });
        }
    });
    //Session Change Functionalities
    $(document).on('change', '#SessionId', function () {

        $("#missingImageCount").html("...");

        //remove all dependant dropdownlist & selection option
        $("#SelectedCourse option").remove();
        $("#SelectedBranch option").remove();
        $("#SelectedCampus option").remove();

        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();
        $("#SelectedLecture option").remove();
        $("#SelectedTeacher option").remove();
        $("FromTime").val('');
        $("#ToTime").val('');

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var organizationId = $("#OrganizationId").val();
        if (programId != null && programId != "" && sessionId != null && sessionId != "" && sessionId!='0') {

            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForBranch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, organizationIds: organizationId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $('#SelectedBranch').append("<option value='0'>All Branch</option>");
                    if (result.IsSuccess) {
                        $.each(result.returnBranchList, function (i, v) {
                            $('#SelectedBranch').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        console.log("Success -----");

                        $.ajax({
                            //url: 'GetCourseList',
                            //url: '/Student/Attendance/GetCourseList',
                            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
                            type: "POST",
                            data: { programIds: programId, sessionIds: sessionId },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (result1) {
                                $.unblockUI();
                                $('#SelectedCourse').append("<option value='0'>All Course</option>");
                                if (result1.IsSuccess) {
                                    $.each(result1.returnCourse, function (i, v) {
                                        $('#SelectedCourse').append($('<option>').text(v.Text).attr('value', v.Value));
                                    });
                                    //$("#SelectedCourse").find('option:eq(0)').prop('selected', true);

                                    $("#SelectedSubject option").remove();
                                    $("FromTime").val('');
                                    $("#ToTime").val('');

                                    var organizatonId = $("#OrganizationId").val();
                                    var programIdForCourse = $("#ProgramId").val();
                                    var sessionIdForSession = $("#SessionId").val();
                                    var courseId = $("#SelectedCourse").val();
                                    //if (courseId == "") courseId = 0;
                                    if (organizatonId.length > 0 && programId.length > 0 && sessionId.length > 0) {
                                        $.ajax({
                                            //url: 'GetCourseList',
                                            url: '/Student/StudentClassEvaluationReport/AjaxRequestForSubject',
                                            type: "POST",
                                            data: { organizatonId: organizatonId, programId: programIdForCourse, sessionId: sessionIdForSession, courseId: courseId },
                                            beforeSend: function () {
                                                $.blockUI({
                                                    timeout: 0,
                                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                                });
                                            },
                                            success: function (result2) {
                                                $.unblockUI();
                                                if (result2.IsSuccess) {
                                                    $.each(result2.subjectList, function (i, v) {
                                                        $('#SelectedSubject').append($('<option>').text(v.Text).attr('value', v.Value));
                                                    });
                                                    $("#SelectedSubject").find('option:eq(0)').prop('selected', true);
                                                    console.log("Success ------");
                                                } else {
                                                    console.log("Error -----");
                                                    
                                                }
                                            },
                                            complete: function () {
                                                $.unblockUI();
                                                console.log("Complete -----");
                                                //
                                            },
                                            error: function (result) {
                                                $.unblockUI();
                                                console.log("Failed -----");
                                                
                                            }
                                        });
                                    }
                                    /*call back end */
                                    console.log("Success ------");
                                } else {
                                    console.log("Error -----");
                                    
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                                console.log("Complete -----");
                                
                            },
                            error: function (result) {
                                $.unblockUI();
                                console.log("Failed -----");
                                
                            }
                        });
                    } else {
                        $.unblockUI();
                        console.log("Error -----");
                        
                    }

                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    
                }
            });
        }
    });
    $(document).on('change', '#SelectedCourse', function () {
        $("#SelectedLecture option").remove();
        $("#SelectedTeacher option").remove();
        $("#SelectedSubject option").remove();
        $("FromTime").val('');
        $("#ToTime").val('');

        var organizatonId = $("#OrganizationId").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var courseId = $("#SelectedCourse").val();

        if (organizatonId.length > 0 && programId.length > 0 && sessionId.length > 0 && courseId.length > 0) {
            $.ajax({
                //url: 'GetCourseList',
                url: '/Student/StudentClassEvaluationReport/AjaxRequestForSubject',
                type: "POST",
                data: { organizatonId: organizatonId, programId: programId, sessionId: sessionId, courseId: courseId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result2) {
                    $.unblockUI();
                    if (result2.IsSuccess) {
                        $.each(result2.subjectList, function (i, v) {
                            $('#SelectedSubject').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        //$("#SelectedSubject").find('option:eq(0)').prop('selected', true);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    
                }
            });
        }

    });
    //Branch Change Functionalities
    $(document).on('change', '#SelectedBranch', function () {

        $("#missingImageCount").html("...");
        //$("#SelectedLecture option").remove();
        //$("#SelectedTeacher option").remove();
        //$("#SelectedSubject option").remove();
        $("FromTime").val('');
        $("#ToTime").val('');
        //remove all dependant dropdownlist & selection option 
        $("#SelectedCampus option").remove();

        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();

        if (programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForCampus",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#SelectedCampus').append("<option value='0'>All campus</option>");
                    if (result.IsSuccess) {
                        $.each(result.returnCampusList, function (i, v) {
                            $('#SelectedCampus').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        /// $("#SelectedCampus").find('option:eq(0)').prop('selected', true); 
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        
                    }

                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    
                }
            });
        }
    });
    //Campus Change Functionalities
    $(document).on('change', '#SelectedCampus', function () {

        $("#missingImageCount").html("...");
        //$("#SelectedLecture option").remove();
        //$("#SelectedTeacher option").remove();
        //$("#SelectedSubject option").remove();
        $("FromTime").val('');
        $("#ToTime").val('');
        //remove all dependant dropdownlist & selection option  
        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();

        if (programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "" && campusId != null && campusId != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForBatchDay",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchDay",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#SelectedBatchDays').append("<option value='0'>All Batch Days</option>");
                    if (result.IsSuccess) {
                        $.each(result.returnBatchDays, function (i, v) {
                            //if (v.Value == 0)
                            //    $('#SelectedBatchDays').append($('<option>').text(v.Text).attr('value', v.Value));
                            //else
                            $('#SelectedBatchDays').append($('<option>').text(v.Text).attr('value', v.Text));
                        });
                        /// $("#SelectedBatchDays").find('option:eq(0)').prop('selected', true);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        
                    }

                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    
                }
            });
        }
    });
    //Batch Day Change Functionalities
    $(document).on('change', '#SelectedBatchDays', function () {

        $("#missingImageCount").html("...");
        //$("#SelectedLecture option").remove();
        //$("#SelectedTeacher option").remove();
        //$("#SelectedSubject option").remove();
        $("FromTime").val('');
        $("#ToTime").val('');
        //remove all dependant dropdownlist & selection option  
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();
        $("#SelectedExam option").remove();
        $('#countBtn').attr('disabled', 'disabled');
        $('#countValue').val('0');

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchDays = $("#SelectedBatchDays").val();

        if (programId != null && programId != ""
            && sessionId != null && sessionId != ""
            && branchId != null && branchId != ""
            && campusId != null && campusId != ""
            && batchDays != null && batchDays != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForBatchTime",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchTime",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, batchDays: batchDays, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#SelectedBatchTime').append("<option value='0'>All Batch Times</option>");
                    if (result.IsSuccess) {
                        $.each(result.returnBatchTime, function (i, v) {
                            $('#SelectedBatchTime').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        /// $("#SelectedBatchTime").find('option:eq(0)').prop('selected', true);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    
                }
            });
        }
    });
    //Batch Time Change Functionalities
    $(document).on('change', '#SelectedBatchTime', function () {

        $("#missingImageCount").html("...");
        //$("#SelectedLecture option").remove();
        //$("#SelectedTeacher option").remove();
        //$("#SelectedSubject option").remove();
        $("FromTime").val('');
        $("#ToTime").val('');
        //remove all dependant dropdownlist & selection option  
        $("#SelectedBatch option").remove();
        $("#SelectedExam option").remove();
        $('#countBtn').attr('disabled', 'disabled');
        $('#countValue').val('0');

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchDays = $("#SelectedBatchDays").val();
        var batchTime = $("#SelectedBatchTime").val();

        var batchTimeText = [];
        $("#SelectedBatchTime option:selected").each(function () {
            var $this = $(this);
            if ($this.length) {
                batchTimeText.push($this.text());
            }
        });

        if (programId != null && programId != ""
            && sessionId != null && sessionId != ""
            && branchId != null && branchId != ""
            && campusId != null && campusId != ""
            && batchDays != null && batchDays != ""
            && batchTime != null && batchTime != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForBatch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatch",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, batchDays: batchDays, batchTimes: batchTime, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#SelectedBatch').append("<option value='0'>All Batch</option>");
                    if (result.IsSuccess) {
                        $.each(result.returnBatch, function (i, v) {
                            $('#SelectedBatch').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        /// $("#batchName").find('option:eq(0)').prop('selected', true);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    
                }
            });
        }
    });
    $("#SelectedSubject").change(function () {

        //remove all dependant dropdownlist & selection option
        $("#SelectedLecture option").remove();
        $("#SelectedTeacher option").remove();
        var courseId = $("#SelectedCourse").val();
        var subjectId = $("#SelectedSubject").val();
        var batchId = $("#SelectedBatch").val();
        var dateFrom = $("#FromTime").val();
        var dateTo = $("#ToTime").val();
        if (subjectId != null && subjectId != "" && batchId != null && batchId != "" && dateFrom != null && dateTo != null && dateFrom != "" && dateTo!="") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "StudentClassEvaluationReport/GetLecture",
                cache: false,
                async: true,
                data: { batchIdList: batchId, courseIdList: courseId, subjectIdList: subjectId, dateFrom: dateFrom, dateTo: dateTo },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.lectureList.length) {
                        $('#SelectedLecture').append("<option value='0'>ALL LECTURE</option>");
                    }
                    if (result.IsSuccess) {
                        lectureIds = [];
                        $.each(result.lectureList, function (i, v) {
                            $('#SelectedLecture').append($('<option>').text(v.Text).attr('value', v.Value));
                            lectureIds.push(v.Value);
                        });
                        $("#lectureIds").val(lectureIds.join());
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");

                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }

    });
    $(document).on('change', '#ToTime', function () {
        //$("#SelectedExam option").remove();
        $('#countBtn').attr('disabled', 'disabled');
        $('#countValue').val('0');
        $("#SelectedLecture option").remove();
        $("#SelectedTeacher option").remove();
        var orgId = $("#OrganizationId").val();
        var batchDays = $("#SelectedBatchDays").val();
        var batchTime = $("#SelectedBatchTime").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var courseId = $("#SelectedCourse").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchId = $("#SelectedBatch").val();
        var subjectId = $("#SelectedSubject").val();
        var dateFrom = $("#FromTime").val();
        var dateTo = $("#ToTime").val();
        lectureIds = [];
        if (programId.length > 0 && sessionId.length > 0 & branchId.length > 0 && campusId.length > 0 && batchId.length > 0 && subjectId.length > 0 && dateFrom.length > 0 && dateTo.length > 0) {

            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "StudentClassEvaluationReport/GetLecture",
                cache: false,
                async: true,
                //(userMenu,courseIdList,subjectIdList, batchIdList,dateFrom,  dateTo,  organizationIdList , programIdList,  branchIdList, sessionIdList, campusIdList ,  batchDaysList,  batchTimeList)
                data: { courseIdList: courseId, batchIdList: batchId, subjectIdList: subjectId, dateFrom: dateFrom, dateTo: dateTo, organizationIdList: orgId, programIdList: programId, branchIdList: branchId, sessionIdList: sessionId, campusIdList: campusId, batchDaysList: batchDays, batchTimeList: batchTime },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.lectureList.length > 0) {
                        $('#SelectedLecture').append("<option value='0'>ALL LECTURE</option>");
                    }
                    if (result.IsSuccess) {

                        $.each(result.lectureList, function (i, v) {
                            $('#SelectedLecture').append($('<option>').text(v.Text).attr('value', v.Value));
                            lectureIds.push(v.Value);
                        });
                        $("#lectureIds").val(lectureIds.join());
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    
                }
            });
        }
    });
    $(document).on('change', '#FromTime', function () {
        $("#SelectedLecture option").remove();
        $("#SelectedTeacher option").remove();
        $('#countBtn').attr('disabled', 'disabled');
        $('#countValue').val('0');
        $("#ToTime").val('');
    });
    $(document).on('change', '#SelectedLecture', function () {
        //$("#SelectedExam option").remove();
        teacherIds = [];
        $('#countBtn').attr('disabled', 'disabled');
        $('#countValue').val('0');
        $("#SelectedTeacher option").remove();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var courseId = $("#SelectedCourse").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchId = $("#SelectedBatch").val();
        var subjectId = $("#SelectedSubject").val();
        var dateFrom = $("#FromTime").val();
        var dateTo = $("#ToTime").val();
        var lectureId = $("#SelectedLecture").val();
        //var lectureIds = $.map($('#SelectedLecture option'), function (e) { return e.value; });
        //var batchIds = $.map($('#SelectedBatch option'), function (e) { return e.value; });
        if ($("#SelectedLecture").val().indexOf('0') == -1) {
            lectureIds = $("#SelectedLecture").val();
        } else {
            lectureIds = $.map($('#SelectedLecture option'), function (e) { return e.value; });
            lectureIds = jQuery.grep(lectureIds, function (value) {
                return value != 0;
            });
        }
        var batchIds = [];
        if ($("#SelectedBatch").val().indexOf('0') == -1) {
            batchIds = $("#SelectedBatch").val();
        } else {
            batchIds = $.map($('#SelectedBatch option'), function (e) { return e.value; });
            batchIds = jQuery.grep(batchIds, function (value) {
                return value != 0;
            });
        }
        if (programId.length > 0 && sessionId.length > 0 & branchId.length > 0 && campusId.length > 0 && batchId.length > 0 && subjectId.length > 0 && dateFrom.length > 0 && dateTo.length > 0 && lectureId.length > 0) {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "StudentClassEvaluationReport/GetTeacher",
                cache: false,
                async: true,
                data: { lectureIdList: lectureIds, batchIdList: batchIds, dateFrom: dateFrom, dateTo: dateTo },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.teacherList.length > 0) {
                        $('#SelectedTeacher').append("<option value='0'>ALL TEACHER</option>");
                    }
                    if (result.IsSuccess) {
                        $.each(result.teacherList, function (i, v) {
                            teacherIds.push(v.Value);
                            $('#SelectedTeacher').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        $("#teacherIds").val(teacherIds.join());
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    
                }
            });
        }
    });
    $(document).on('change', '#SelectedTeacher', function () {
        $('#countBtn').removeAttr('disabled', 'disabled');
    });
    $('#informationViewList').bootstrapDualListbox({
        nonselectedlistlabel: 'Non-selected',
        selectedlistlabel: 'Selected',
        preserveselectiononmove: 'moved',
        moveOnSelect: false,
        showFilterInputs: false,
        infoText: '',
    });
    $(document).on('click', '#countBtn', function () {
        //var batchId = $("#SelectedBatch").val();
        var batchIds = [];
        var dateFrom = $("#FromTime").val();
        var dateTo = $("#ToTime").val();
        //var lectureId = $("#SelectedLecture").val();
        //var teacherIds = $('#SelectedTeacher').val();
        if ($("#SelectedBatch").val().indexOf('0') == -1) {
            batchIds = $("#SelectedBatch").val();
        } else {
            batchIds = $.map($('#SelectedBatch option'), function (e) { return e.value; });
            batchIds = jQuery.grep(batchIds, function (value) {
                return value != 0;
            });
        }
        if ($("#SelectedLecture").val().indexOf('0') == -1) {
            lectureIds = $("#SelectedLecture").val();
        } else {
            lectureIds = $.map($('#SelectedLecture option'), function (e) { return e.value; });
            lectureIds = jQuery.grep(lectureIds, function (value) {
                return value != 0;
            });
        }
        if ($("#SelectedTeacher").val().indexOf('0') == -1) {
            teacherIds = $("#SelectedTeacher").val();
        } else {
            teacherIds = $.map($('#SelectedTeacher option'), function (e) { return e.value; });
            teacherIds = jQuery.grep(teacherIds, function (value) {
                return value != 0;
            });
        }

        if (teacherIds.length > 0) {

            $.ajax({
                type: "post",
                url: "/Student/StudentClassEvaluationReport/GetStudentCount",
                cache: false,
                async: true,
                data: { batchIdList: batchIds, lectureIdList: lectureIds, teacherIdList: teacherIds, dateFrom: dateFrom, dateTo: dateTo },

                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $('#countValue').val(result.rowCount);
                    } else {
                        $('#countValue').val(0);
                    }


                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    
                }
            });
        }
    });
});