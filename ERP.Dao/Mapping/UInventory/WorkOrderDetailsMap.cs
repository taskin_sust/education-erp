using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class WorkOrderDetailsMap : BaseClassMap<WorkOrderDetails, long>
    {
        public WorkOrderDetailsMap()
        {
            Table("UINV_WorkOrderDetails");
            LazyLoad();
            References(x => x.Item).Column("ItemId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.WorkOrder).Column("WorkOrderId");
            References(x => x.PurposeProgram).Column("PurposeProgramId");
            References(x => x.PurposeSession).Column("PurposeSessionId");
            Map(x => x.Purpose).Column("Purpose");
            Map(x => x.OrderQuantity).Column("OrderQuantity").Not.Nullable();
            Map(x => x.UnitCost).Column("UnitCost").Not.Nullable();
            Map(x => x.TotalCost).Column("TotalCost").Not.Nullable();
            HasMany(x => x.GoodsReceiveDetails).KeyColumn("WorkOrderDetailId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.WorkOrderCriteriaList).KeyColumn("WorkOrderDetailId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
