﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceDeviceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceDevice")]
    public class AttendanceDeviceLoadTest : AttendanceDeviceBaseTest
    {
        #region Basic test

        [Fact]
        public void Shud_Return_Zero()
        {
            int start = 0;
            int length = 10;
            string orderBy = "Id";
            string orderDir = "Desc";
            string organizationId = "-1";
            string branchId = "-1";
            string campusId = "-1";
            string iPAddress = "192.168.0.2";
            var obj = _attendanceDeviceService.LoadDevices(start, length, orderBy, orderDir, organizationId, branchId, campusId, "", iPAddress);
            Assert.Equal(obj.Count, 0);
        }

        [Fact]
        public void Shud_Return_One()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();

            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object)
                .Persist();

            int start = 0;
            int length = 10;
            string orderBy = "Id";
            string orderDir = "Desc";
            string organizationId = _campusFactory.Object.Branch.Organization.Id.ToString();
            string branchId = _campusFactory.Object.Branch.Id.ToString();
            string campusId = _campusFactory.Object.Id.ToString();
            string iPAddress = _attendanceDeviceFactory.Object.IpAddress;

            var obj = _attendanceDeviceService.LoadDevices(start, length, orderBy, orderDir, organizationId, branchId, campusId, "", iPAddress);
            Assert.Equal(obj.Count, 1);
        }

        [Fact]
        public void Shud_Return_Top_Ten()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.CreateMore(15, _campusFactory.Object).Persist();
            int start = 0;
            int length = 10;
            string orderBy = "Id";
            string orderDir = "Desc";
            string organizationId = _campusFactory.Object.Branch.Organization.Id.ToString();
            string branchId = _campusFactory.Object.Branch.Id.ToString();
            string campusId = _campusFactory.Object.Id.ToString();
            string iPAddress = "";
            var obj = _attendanceDeviceService.LoadDevices(start, length, orderBy, orderDir, organizationId, branchId, campusId, "", iPAddress);
            Assert.Equal(obj.Count, 10);
        }

        #endregion

        #region Single Object Load Test

        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _attendanceDeviceService.GetAttendanceDevice(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();

            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object)
                .Persist();
            var obj = _attendanceDeviceService.GetAttendanceDevice(_attendanceDeviceFactory.Object.Id);
            Assert.NotNull(obj);
        }

        #endregion

        #region Row Count Test

        [Fact]
        public void DeviceRowCount_Should_Return_Zero()
        {
            var count = _attendanceDeviceService.DeviceRowCount("0", "0", "0", "", "");
            Assert.Equal(0, count);
        }

        [Fact]
        public void DeviceRowCount_Should_Return_One()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object).Persist();
            var count = _attendanceDeviceService.DeviceRowCount(_campusFactory.Object.Branch.Organization.Id.ToString(),
                _campusFactory.Object.Branch.Id.ToString(), _campusFactory.Object.Id.ToString(), "", "192.168.0.28");
            Assert.Equal(1, count);
        }

        [Fact]
        public void DeviceRowCount_Should_Return_Valid()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.CreateMore(7, _campusFactory.Object).Persist();
            var count = _attendanceDeviceService.DeviceRowCount(_campusFactory.Object.Branch.Organization.Id.ToString(),
               _campusFactory.Object.Branch.Id.ToString(), _campusFactory.Object.Id.ToString(), "", "192.168.0.28");
            Assert.Equal(7, count);
        }

        //TODO: write more test
        //1. Load active devices
        //2. Load inactive devices
        //3. Load device by ip
        //4. Load devices by synchronazer
        //5. Load devices by campus

        #endregion


    }
}