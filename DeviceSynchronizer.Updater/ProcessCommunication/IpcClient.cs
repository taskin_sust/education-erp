﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.Threading;
using log4net;
using Synchronizer.Updater.Services;

namespace Synchronizer.Updater.ProcessCommunication
{
    class IpcClient
    {
        #region Objects/Properties/Services/Dao & Initialization

        private static readonly ILog Log = LogManager.GetLogger("IpcLogger");
        private readonly SynchronizerUpdater.ViewMessageDelegate _viewMessage;
        public static IIpcOperation RemotingObject { get; set; }
        public ChannelFactory<IIpcOperation> FactoryChannel { get; set; }
        public IpcClient(SynchronizerUpdater.ViewMessageDelegate currentMessage)
        {
            _viewMessage = currentMessage;
        }

        #endregion

        #region Delegate & Function

        private void OnViewMessage(string message)
        {
            if (_viewMessage != null)
            {
                _viewMessage(message);
            }
        }

        #endregion

        #region Operational Function

        public bool Connect()
        {
            bool returnValue = false;
            try
            {
                Thread.Sleep(1000*20);
                returnValue = Helper.AppHelper.IsSynchronizerRunning();
                if (returnValue == false)
                {
                    OnViewMessage("Syncronizer software is not running.");
                }
                else
                {
                    //server address
                    string ipcAddress = ConfigurationManager.AppSettings["IpcAddress"];
                    NetNamedPipeBinding namedPipeBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);
                    FactoryChannel = new ChannelFactory<IIpcOperation>(namedPipeBinding, ipcAddress);
                    RemotingObject = FactoryChannel.CreateChannel();
                    ((IClientChannel)RemotingObject).Open();

                    OnViewMessage("Connected to the Synchronizer Software");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                OnViewMessage("Unable to connect Synchronizer Software.");
            }
            return returnValue;
        }

        public string GetVersion()
        {
            string version =null;
            try
            {
                version = RemotingObject.GetSoftwareVersion();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                version = null;
                OnViewMessage("Unable to get information about synchronizer software version.");
            }
            return version;
        }

        public bool IsServiceRunning()
        {
            bool isRunning = false;
            try
            {
                isRunning = RemotingObject.IsServiceRunning();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
               // OnViewMessage("Unable to get info about running software.");
                OnViewMessage("Attendance synchronizer software is not running");
            }
            return isRunning;
        }

        public void StopService(bool exitApplication = false)
        {
            bool isRunning = false;
            try
            {
                isRunning = Helper.AppHelper.IsSynchronizerRunning();
                if (isRunning)
                    RemotingObject.StopService(exitApplication);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
               // OnViewMessage("Could not stop this service.");
                OnViewMessage("Attendance Synchronizer service Stopped");
            }
        }

        #endregion
    }
}
