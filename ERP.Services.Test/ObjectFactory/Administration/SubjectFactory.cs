﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class SubjectFactory : ObjectFactoryBase<Subject>
    {
        #region Object Initialization

        protected readonly ISubjectService _subjectService;
        public SubjectFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _subjectService = new SubjectService(_session);
        }

        #endregion

        #region Create

        public SubjectFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }
        public SubjectFactory WithProperties(string shortName)
        {
            Object.ShortName = shortName;
            return this;
        }

        public SubjectFactory Create()
        {
            Object = new Subject()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Branch.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public SubjectFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _subjectService.SubjectSaveOrUpdate(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _subjectService.SubjectSaveOrUpdate(item);
                    }
                }
            }
            return this;
        }

        public SubjectFactory CreateMore(int numberOfSubject)
        {
            CreateSubjectList(numberOfSubject);
            return this;
        }

        private List<Subject> CreateSubjectList(int numberOfSubject)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Subject>();
            for (int i = 0; i < numberOfSubject; i++)
            {
                var subject = new Subject()
                {
                    Id = 0,
                    Name = Guid.NewGuid() + "_" + i,
                    ShortName = Guid.NewGuid() + "abcd_" + i,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Subject.EntityStatus.Active
                };
                list.Add(subject);
            }
            ObjectList.AddRange(list);
            return list;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void CleanupSubject(List<long> subjectIdList)
        {
            _session.CreateSQLQuery("Delete from subject where id in(" + string.Join(", ", subjectIdList) + ")").ExecuteUpdate();
        }
        public void Cleanup()
        {
            DeleteAll();
        }

        #endregion

    }
}
