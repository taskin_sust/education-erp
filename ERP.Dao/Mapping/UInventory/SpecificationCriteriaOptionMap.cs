using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory {


    public class SpecificationCriteriaOptionMap : BaseClassMap<SpecificationCriteriaOption, long>
    {
        
        public SpecificationCriteriaOptionMap() {
			Table("UINV_SpecificationCriteriaOption");
			LazyLoad();
			References(x => x.SpecificationCriteria).Column("SpecificationCriteriaId");
			Map(x => x.Rank).Column("Rank").Not.Nullable();
			Map(x => x.Name).Column("Name").Not.Nullable();
        }
    }
}
