﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class HomeController : Controller
    {
        // GET: UInventory/Home
        [AuthorizeRequired]
        public ActionResult Index()
        {
            return View();
        }
    }
}