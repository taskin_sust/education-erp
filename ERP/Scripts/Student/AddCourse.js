﻿/*Written By Taskin*/

$(document).ready(function () {
    /*Class Declaration Start*/

    function StudentViewModel() {
        this.Id = 0;
        this.Name = 0;
        this.MobNumber = 0;
        this.Program = 0;
        this.Session = "";
        this.Branch = "";
        this.Campus = "";
        this.BatchDays = "";
        this.BatchTime = "";
        this.Batch = "";
        this.CourseViewModels = [];
        this.StudentPayment = {};
        this.StudentViewModel = function () { };
    };

    function CourseViewModel() {
        this.Id = 0;
        this.Name = 0;
        this.ProgramId = "";
        this.SessionId = "";
        this.IsComplementaryCourse = false;
        this.IsTaken = false;
        this.IsNewlyTakenCourse = false;
        this.SubjectViewModels = [];
        this.CourseViewModel = function () { };
    };

    function SubjectViewModel() {
        this.Id = 0;
        this.Name = 0;
        this.Payment = 0;
        this.IsTaken = false;
        this.IsNewlyTakenSubject = false;
        this.SubjectViewModel = function () { };
    };

    function StudentPaymentModel() {

        this.OfferedDiscount = 0;
        this.NetReceivable = 0;
        this.CashBackAmount = 0;
        this.ConsiderationAmount = 0;
        this.CourseFees = 0;
        this.DiscountAmount = 0;
        this.DueAmount = 0;
        this.NextReceivedDate = "";
        this.PayableAmount = 0;
        this.PaymentMethod = 0;
        this.PaymentType = 0;
        this.ReceiptNo = "";
        this.ReceivedAmount = 0;
        this.ReceivableAmount = 0;
        this.ReceivedDate = "";
        this.ReferrerId = 0;
        this.SpDiscountAmount = 0;
        this.Remarks = "";
        this.SpReferenceNote = "";
        this.StudentPaymentModel = function () { };
    };

    function CourseSubject() {
        this.Course = new Object();
        this.Subject = new Object();
    };

    var MESSAGE_SHOW_INTERVAL = 10000;
    function showErrorMessage(msg) {
        $("#messageContainer").append('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><strong>Error!</strong> ' + msg + '.</div>');
        scrollToElement("#messageContainer");
        setTimeout(function () {
            $("#messageContainer").html("");
        }, MESSAGE_SHOW_INTERVAL);
    }

    validateTextField("referrerenceNote", "Please Enter Referance Note.");
    validateDropDownField("RefererList", "Please Select Referer.");
    validateDropDownField("spDiscountAmount", "Please Enter Special Discount.");

    /*End*/

    /** Edit  **/
    $(document).on('click', '#paymentPaidCourseEditBtn', function () {
        $('.course-name-check').each(function (index, value) {
            var courseId = $(this).attr('data-course-id');

            if ($(this).is(':checked')) {
                var courseDetail = new CourseViewModel();
                courseDetail.Name = $(this).attr("data-course-name");
                courseDetail.ProgramId = $(this).attr('data-program-id');
                courseDetail.SessionId = $(this).attr('data-session-id');
                courseDetail.Id = courseId;
                var subCount = 0;
                $('.course-' + courseId + '-subjects').each(function (index, value) {
                    subCount++;
                    if ($(this).is(':checked') && $(this).attr('data-newly-added') == 'new') {
                        $(this).attr('data-newlyEdit', '1');
                        $(this).prop('disabled', false);
                    }
                    else if ($(this).is(':checked') && $(this).attr('disabled') == 'disabled') {
                        /*No action Needed*/
                    }
                    else if ($(this).attr('disabled') != 'disabled' && $(this).is(':checked') == false) {
                        var subjectId = $(this).attr('data-course-subject-id');
                        $(".course-" + courseId + "-subject-" + subjectId + "-container").show();
                    }

                });
                // $(this).prop('disabled', false);
                if ($(this).attr('data-newlytakencourse') == 'new') {
                    //$(this).attr("disabled", "disabled");
                    //$(this).attr("readonly", "readonly");
                    $(this).prop('disabled', false);
                } else {
                    $(this).attr("disabled", "disabled");
                    $(this).attr("readonly", "readonly");
                }

            }
            else {
                $("#course-" + courseId + "-container").show();
                $(".course-" + courseId + "-subject-container").show();
            }
        });
        $(".paymentDue").html('');
        $("#AddCourseToStudent").show();
    });

    /** edit end **/

    /* Add New Courses To a Student --- "NEXT" Button action */
    $(document).on('click', '#AddCourseToStudent', function () {

        var stdProRoll = $('#stdProRoll').val();
        var student = new StudentViewModel();
        var xx = 0;
        var isReqSendToServer1 = true;
        var tSubCount = 0;
        $('.course-name-check').each(function (index, value) {

            var courseId = $(this).attr('data-course-id');

            if ($(this).is(':checked')) {
                xx++;
                console.log("Entered into course-->" + xx);
                var courseDetail = new CourseViewModel();
                courseDetail.Name = $(this).attr("data-course-name");
                courseDetail.ProgramId = $(this).attr('data-program-id');
                courseDetail.SessionId = $(this).attr('data-session-id');
                courseDetail.Id = courseId;
                var takenSubjectCount = 0;
                var prevTakenSubject = 0;
                var subCount = 0;
                $('.course-' + courseId + '-subjects').each(function (index, value) {
                    subCount++;
                    console.log("Entered into subjects -->" + subCount);
                    if ($(this).is(':checked') && $(this).attr('disabled') != 'disabled') {

                        var subject = new SubjectViewModel();
                        subject.Id = $(this).attr('data-course-subject-id');
                        subject.Name = $(this).attr('data-course-subject-name');
                        subject.Payment = $(this).attr('data-course-subject-payment');
                        subject.IsNewlyTakenSubject = true;
                        courseDetail.SubjectViewModels.push(subject);
                        $(this).attr('data-newlyaddedsubject', '1');

                        $(this).attr("disabled", "disabled");
                        $(this).attr("data-newly-added", "new");
                        console.log($(this).parent().find(".new").length);
                        if ($(this).parent().find(".new").length < 1) {
                            $(this).parent().append('<span class="new" style="color:green"><b>*new</b></span>');
                        }
                        //if (!$(this).attr('data-newlyEdit')) {
                        //    $(this).parent().append('<span class="new" style="color:green"><b> *new</b></span>');
                        //}
                        takenSubjectCount++;
                        tSubCount++;
                        console.log("Entered into New Assigned subjects -->" + takenSubjectCount);
                    }
                    else if ($(this).is(':checked') && $(this).attr('disabled') == 'disabled') {
                        prevTakenSubject++;
                        var subject1 = new SubjectViewModel();
                        subject1.Id = $(this).attr('data-course-subject-id');
                        subject1.Name = $(this).attr('data-course-subject-name');
                        subject1.Payment = $(this).attr('data-course-subject-payment');
                        subject1.IsNewlyTakenSubject = false;
                        courseDetail.SubjectViewModels.push(subject1);
                        /*No action Needed*/
                    }
                    else if ($(this).attr('disabled') != 'disabled' && $(this).is(':checked') == false) {
                        var subjectId = $(this).attr('data-course-subject-id');
                        $(".course-" + courseId + "-subject-" + subjectId + "-container").hide();
                    }
                });

                $(this).attr("disabled", "disabled");
                if (takenSubjectCount == 0 && prevTakenSubject == 0) {
                    $("#course-" + courseId + "-container").hide();
                    $(".course-" + courseId + "-subject-container").hide();
                }
                if (takenSubjectCount > 0 || prevTakenSubject > 0) {
                    student.CourseViewModels.push(courseDetail);
                }
                if (subCount == takenSubjectCount) {
                    courseDetail.IsNewlyTakenCourse = true;
                }
            }
            else {
                $("#course-" + courseId + "-container").hide();
                $(".course-" + courseId + "-subject-container").hide();
            }
        });
        if (tSubCount <= 0) {
            isReqSendToServer1 = false;
        }
        var studentObj = JSON.stringify(student);
        //if (isReqSendToServer1 == true) {
        $.ajax({
            url: "/Student/AddCourse/NewCourseAssigned",
            type: "POST",
            data: { studentObj: studentObj, stdProRoll: stdProRoll },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {

                if (data.IsSuccess == false && data.message != "") {
                    var link = "/Student/AddCourse/CourseOrSubjectAdd?stdProRoll=" + stdProRoll + "&" + "optionalErrorMessage=" + data.Message;
                    console.log(link);
                    window.location.href = link;

                    //$("#nextButtonContainer").hide();
                    //var stdProgramId = data.Message;
                } else {
                    $('.paymentDue').html(data);
                    $.unblockUI();
                    $("#AddCourseToStudent").hide();
                    //bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                }
            },
            complete: function () {
                $.unblockUI();
            },

            error: function (data) {
                bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
            }
        });
        //} 
        /*else {
            $.fn.customMessage({
                displayMessage: "Please Select at least One Subject",
                displayMessageType: "error",
                displayTime: 5000
            });
        }*/
    });

    /*Special Discount and Received amount property changing  */

    /*#receivedAmount,#spDiscountAmount */

    function GetAmount(receivable, spDiscount) {
        var val = parseFloat("0" + receivable) - parseFloat("0" + spDiscount);
        return val;
    }


    //$(document).on("input propertychange", ".course-subject-check", function (event) {
    //    if (!($(this).is(':checked'))) {
    //        //$(this).parent().append('<span class="new" style="color:green"><b> *new</b></span>');
    //        $(this).parent().find(".new").remove();
    //    }
    //});

    $(document).on("input propertychange", "#receivedAmount", function (event) {

        var receivableAmount = $('#receivableAmount').val();
        var netReceivable = $('#netReceivable').val();
        var receivedAmount = $('#receivedAmount').val();

        if (receivableAmount.length > 0 && netReceivable.length > 0) {

            if (receivedAmount.length > 0) {
                var dAmount = GetAmount(netReceivable, receivedAmount);
                $('#dueAmount').val(dAmount);

            } else {
                $('#dueAmount').val(netReceivable);
            }

            checkDueAmount();
            console.log("netReceivable: " + netReceivable);
        }
    });

    function checkDueAmount() {

        var val = $("#dueAmount").val().trim();
        var dueAmount = parseInt(val);
        console.log("due " + val);
        if (!isNaN(dueAmount)) {
            if (dueAmount < 0) {
                showErrorMessageBelowCtrl("dueAmount", "", false);
                showErrorMessageBelowCtrl("dueAmount", "Due amount cannot be negative.", true);
            } else {
                showErrorMessageBelowCtrl("dueAmount", "", false);
            }
        }
    }

    $(document).on("input propertychange", "#spDiscountAmount", function (event) {

        var receivableAmount = $('#receivableAmount').val();
        var netReceivable = $('#netReceivable').val();
        var receivedAmount = $('#receivedAmount').val();
        var spDiscountAmount = $('#spDiscountAmount').val();
        var dAmount = 0;

        if (receivableAmount.length > 0 && netReceivable.length > 0) {

            var sDis = parseInt(spDiscountAmount);
            var receiveableAmountI = parseInt("0" + receivableAmount);

            if (!isNaN(sDis) && sDis > 0) {

                $('#RefererList').removeAttr('disabled');
                $('#referrerenceNote').removeAttr('disabled');
                showErrorMessageBelowCtrl("spDiscountAmount", "", false);

                if (sDis >= 0 && sDis <= receiveableAmountI) {
                    netReceivable = GetAmount(receivableAmount, spDiscountAmount);
                    $('#netReceivable').val(netReceivable);
                    dAmount = GetAmount(netReceivable, receivedAmount);
                    $('#dueAmount').val(dAmount);

                } else {
                    showErrorMessageBelowCtrl("spDiscountAmount", "Special discount can not be negative or higher than receivable amount.", true);
                }

            } else {

                $('#netReceivable').val(receivableAmount);
                receivedAmount = $('#receivedAmount').val();
                netReceivable = $('#netReceivable').val();
                var due = GetAmount(netReceivable, receivedAmount);
                $('#dueAmount').val(due);
                $('#RefererList').prop('disabled', true);
                $('#referrerenceNote').prop('disabled', true);
            }

            checkDueAmount();
        } else {
            showErrorMessageBelowCtrl("spDiscountAmount", "", false);
        }
    });

    function validateAddCoursePaymentForm() {

        var receivedAmount = $('#receivedAmount').val().trim();
        var receivedAmountI = parseInt(receivedAmount);

        if (isNaN(receivedAmountI) || receivedAmountI < 0) {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
            showErrorMessageBelowCtrl("receivedAmount", "Please enter valid amount", true);
            return false;
        } else {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
        }

        var nextRecDate = $('#nextRecDate').val();
        var dueAmount = $('#dueAmount').val();

        if (nextRecDate.length <= 0 && parseInt("0" + dueAmount) > 0) {
            showErrorMessageBelowCtrl("nextRecDate", "", false);
            showErrorMessageBelowCtrl("nextRecDate", "Please select next payment date", true);
            console.log("date error is added");
            return false;
        } else {
            showErrorMessageBelowCtrl("nextRecDate", "", false);
        }

        return true;
    }

    /* studentview model generate here */
    function courseListGenerateForAddCourse() {

        var student = new StudentViewModel();

        $('.course-name-check').each(function (index, value) {

            if ($(this).is(':checked')) {
                var courseDetail = new CourseViewModel();
                courseDetail.Name = $(this).attr("data-course-name");
                courseDetail.ProgramId = $(this).attr('data-program-id');
                courseDetail.SessionId = $(this).attr('data-session-id');
                courseDetail.Id = $(this).attr('data-course-id');

                $('.course-' + courseDetail.Id + '-subjects').each(function (index, value) {
                    //console.log("Entered");
                    if ($(this).is(':checked') && $(this).attr('disabled') == 'disabled' && $(this).attr('data-newlyaddedsubject') == "1") {
                        var subject = new SubjectViewModel();
                        subject.Id = $(this).attr('data-course-subject-id');
                        subject.Name = $(this).attr('data-course-subject-name');
                        subject.Payment = $(this).attr('data-course-subject-payment');
                        subject.IsNewlyTakenSubject = true;
                        courseDetail.SubjectViewModels.push(subject);
                    }
                });
                student.CourseViewModels.push(courseDetail);
            }
        });
        var studentObj = JSON.stringify(student);
        //console.log(studentObj);
        return studentObj;
    }

    /*Finally submit to server  */
    $(document).on('click', '#paymentPaidCourseAddBtn', function () {
        $(this).prop("disabled", true);
        var receivableAmount = $('#receivableAmount').val().trim();
        var netReceivable = $('#netReceivable').val();
        var receivedAmount = $('#receivedAmount').val();
        var spDiscountAmount = $('#spDiscountAmount').val().trim();
        var dueAmount = $('#dueAmount').val();
        //console.log(dueAmount);
        var paymentMethod = $('#PaymentMethods').val();
        var referrer = $('#RefererList').val();
        var referrerenceNote = $('#referrerenceNote').val();
        var nextRecDate = $('#nextRecDate').val();
        var stdPId = $('#stdProRoll').val();

        receivableAmount = Number(receivableAmount);
        netReceivable = Number(netReceivable);
        dueAmount = Number(dueAmount);
        receivedAmount = Number(receivedAmount);
        spDiscountAmount = Number(spDiscountAmount);
        //console.log(dueAmount);

        var nRec = parseFloat(netReceivable);
        var sDis = parseFloat(spDiscountAmount);
        var recvdAmount = parseFloat(receivedAmount);
        var recavailAmount = parseFloat(receivableAmount);
        var dAmount = nRec - recvdAmount;/*dAmount : Due Amount*/

        console.log(dueAmount);

        if (!validateAddCoursePaymentForm()) {
            $(this).prop("disabled", false);
            return;
        }

        if (dAmount >= 0) {
            if (recavailAmount > 0 && nRec >= 0) {
                if (sDis > 0) {
                    if (referrer.length > 0 && referrerenceNote.length > 0) {

                        var studentObj = courseListGenerateForAddCourse();

                        $.ajax({
                            url: "/Student/AddCourse/NewCourseAddPaymentAndMoneyReceiptGeneration",
                            type: "POST",
                            data: { studentObj: studentObj, stdPId: stdPId, recavailAmount: recavailAmount, nRec: nRec, sDis: sDis, recvdAmount: recvdAmount, paymentMethod: paymentMethod, referrerenceNote: referrerenceNote, nextRecDate: nextRecDate, referrer: referrer },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data) {

                                $.unblockUI();
                                if (data.IsSuccess) {
                                    window.location = "/Student/Payment/GenerateMoneyReciept?id=" + data.AdditionalValue;
                                } else {
                                    showErrorMessage(data.Message);
                                    $(this).prop("disabled", false);
                                }
                            },

                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (data) {
                                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                                $(this).prop("disabled", false);
                            }
                        });

                    } else {
                        if (referrer.length <= 0)
                            $('#RefererList').addClass('highlight');

                        if (referrerenceNote.length <= 0)
                            $('#referrerenceNote').addClass('highlight');
                        $(this).prop("disabled", false);
                    }

                } else {

                    sDis = 0;
                    if (nRec < 0) {
                        $('#netReceivable').addClass('highlight');
                        $('#netReceivable').after("<p style ='color:red'>Net Receivable Amount 0 Not accepted</p>");
                        $(this).prop("disabled", false);
                        return;
                    }

                    studentObj = courseListGenerateForAddCourse();

                    $.ajax({
                        url: "/Student/AddCourse/NewCourseAddPaymentAndMoneyReceiptGeneration",
                        type: "POST",
                        data: { studentObj: studentObj, stdPId: stdPId, recavailAmount: recavailAmount, nRec: nRec, sDis: sDis, recvdAmount: recvdAmount, paymentMethod: paymentMethod, referrerenceNote: referrerenceNote, nextRecDate: nextRecDate, referrer: referrer, },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (data) {

                            if (data.IsSuccess) {
                                window.location = "/Student/Payment/GenerateMoneyReciept?id=" + data.AdditionalValue;
                            } else {
                                showErrorMessage(data.Message);
                                $(this).prop("disabled", false);
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                            $(this).prop("disabled", false);
                        }
                    });
                }
            }
        } else {
            showErrorMessageBelowCtrl("dueAmount", "", false);
            showErrorMessageBelowCtrl("dueAmount", "Due amount Negative Not acceptable", true);
            $(this).prop("disabled", false);
        }
    });
});