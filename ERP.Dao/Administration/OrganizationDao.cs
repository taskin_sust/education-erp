﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Event.Default;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Administration
{
    public interface IOrganizationDao : IBaseDao<Organization, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        Organization GetByBusinessId(string businessId);
        Organization GetActiveOrganization();
        Organization GetOrganization(string shortName);
        #endregion

        #region List Loading Function
        IList<Organization> LoadOrganization(int start, int length, string orderBy, string orderDir, string name, string shortName, string website, string status);
        IList<Organization> LoadOrganization(bool eagerLoadOrgBranch = false);
        IList<Organization> LoadAuthorizedOrganization(List<long> organizationIdList, bool eagerLoadOrgBranch = false);
        #endregion

        #region Others Function
        bool CheckDuplicateFields(long id, string name = "", string shortName = "");
        int GetOrganizationCount(string name, string shortName, string website, string status);
        #endregion

    }

    public class OrganizationDao : BaseDao<Organization, long>, IOrganizationDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function


        #endregion

        #region Single Instances Loading Function

        public Organization GetByBusinessId(string businessId)
        {
            Organization organization = Session.QueryOver<Organization>()
                .Where(x => x.Status == Organization.EntityStatus.Active && x.BusinessId == businessId)
                .Take(1)
                .SingleOrDefault();
            return organization;
        }

        public Organization GetActiveOrganization()
        {
            var entity = Session.QueryOver<Organization>()
                            .Where(x => x.Status == Organization.EntityStatus.Active)
                            .List<Organization>();
            if (entity.Count > 0)
                return entity[0];
            return null;
        }

        public Organization GetOrganization(string shortName)
        {
            return Session.QueryOver<Organization>().Where(x => x.ShortName == shortName).SingleOrDefault<Organization>();
        }
        #endregion

        #region List Loading Function

        public IList<Organization> LoadOrganization(int start, int length, string orderBy, string orderDir, string name, string shortName, string website, string status)
        {
            ICriteria criteria = GetOrganizationCriteria(name, shortName, website, status);
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Organization>();
        }

        public IList<Organization> LoadOrganization(bool eagerLoadOrgBranch = false)
        {
            var criteria = Session.CreateCriteria<Organization>();
            criteria.Add(Restrictions.Eq("Status", Organization.EntityStatus.Active));

            /*if (eagerLoadOrgBranch)
            {
                criteria.SetFetchMode("Programs", FetchMode.Eager);
                criteria.SetFetchMode("Branches", FetchMode.Eager);                
            }*/

            criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            var dataList = criteria.List<Organization>().ToList();
            if (eagerLoadOrgBranch)
            {
                dataList.Select(x => x.Programs);
                dataList.Select(x => x.Branches);
            }
            return dataList;
        }

        public IList<Organization> LoadAuthorizedOrganization(List<long> organizationIdList, bool eagerLoadOrgBranch = false)
        {
            var criteria = Session.CreateCriteria<Organization>();
            criteria.Add(Restrictions.Eq("Status", Organization.EntityStatus.Active));
            if (organizationIdList != null)
            {
                if (!organizationIdList.Contains(SelectionType.SelelectAll))
                    criteria.Add(Restrictions.In("Id", organizationIdList));
            }
            /*if (eagerLoadOrgBranch)
            {
                criteria.SetFetchMode("Programs", FetchMode.Eager);
                criteria.SetFetchMode("Branches", FetchMode.Eager);
            }*/
            criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            var dataList = criteria.List<Organization>().OrderBy(x => x.Rank).ToList();
            if (eagerLoadOrgBranch)
            {
                dataList.Select(x => x.Programs);
                dataList.Select(x => x.Branches);
            }
            return dataList;
        }
        #endregion

        #region Others Function
        public bool CheckDuplicateFields(long id, string name = "", string shortName = "")
        {
            var organizationQueryOver =
                Session.QueryOver<Organization>().Where(x => x.Status == Organization.EntityStatus.Active);
            if (id > 0)
            {
                organizationQueryOver = organizationQueryOver.Where(x => x.Id != id);
            }
            if (!string.IsNullOrEmpty(name.Trim()))
            {
                organizationQueryOver = organizationQueryOver.Where(x => x.Name == name.ToString());
            }
            if (!string.IsNullOrEmpty(shortName.Trim()))
            {
                organizationQueryOver.Where(x => x.ShortName == shortName.ToString());
            }
            var organizationList = organizationQueryOver.List<Organization>();
            if (organizationList.Count > 0)
            {
                return true;
            }
            return false;
        }
        public int GetOrganizationCount(string name, string shortName, string website, string status)
        {
            ICriteria criteria = GetOrganizationCriteria(name, shortName, website, status);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function
        private ICriteria GetOrganizationCriteria(string name, string shortName, string website, string status)
        {
            var criteria = Session.CreateCriteria<Organization>().Add(Restrictions.Not(Restrictions.Eq("Status", Organization.EntityStatus.Delete)));

            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.InsensitiveLike("Name", name.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(shortName))
            {
                criteria.Add(Restrictions.InsensitiveLike("ShortName", shortName.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(website))
            {
                criteria.Add(Restrictions.InsensitiveLike("Website", website.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status.Trim())));
            }
            return criteria;
        }
        #endregion
    }
}
