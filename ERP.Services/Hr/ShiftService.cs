﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;

namespace UdvashERP.Services.Hr
{
    public interface IShiftService : IBaseService
    {

        #region Operational Function

        bool Save(Shift shiftObj);
        bool Update(Shift shiftObj);
        bool Delete(long id);
        
        #endregion

        #region Single Instances Loading Function

        Shift LoadById(long id);

        #endregion

        #region List Loading Function

        IList<Shift> LoadShift(List<UserMenu> userMenu ,string orderBy, string direction, long? organizationId);
        IList<Shift> LoadShift(List<long> organizationIds);
        
        #endregion

        #region Others Function

        int GetShiftRowCount(List<UserMenu> usermenu ,string orderBy, string direction, long? organizationId);

        #endregion

        #region Helper Function

        #endregion

    }
    public class ShiftService : BaseService, IShiftService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IShiftDao _hrShiftDao;
        private readonly IOrganizationDao _organizationDao;
        public ShiftService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrShiftDao = new ShiftDao { Session = session };
            _organizationDao = new OrganizationDao{Session =  session};
        }

        #endregion

        #region Operational Functions

        public bool Save(Shift shiftObj)
        {
            if (shiftObj == null)
            {
                throw new NullObjectException("shift object can not be null");
            }
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    //check before save
                    int sDay = 01;
                    int eDay = 01;
                    CheckShiftObjectBeforeSave(shiftObj);
                    DateTime start = Convert.ToDateTime(shiftObj.StartTime.Value.ToString("H:mm:ss"));
                    DateTime end = Convert.ToDateTime(shiftObj.EndTime.Value.ToString("H:mm:ss"));

                    var organizationOfficeStartTimeString =
                        Convert.ToDateTime("2000-01-01 " + shiftObj.Organization.AttendanceStartTime.ToString("H:mm:ss"));
                    var organizationOfficeEndTimeString =
                        Convert.ToDateTime(organizationOfficeStartTimeString)
                            .AddHours(23)
                            .AddMinutes(59)
                            .AddSeconds(59)
                            .ToString("yyyy-MM-dd H:mm:ss");

                    if (Convert.ToDateTime("2000-01-01 " + shiftObj.StartTime.Value.ToString("H:mm:ss")) <
                        organizationOfficeStartTimeString)
                    {
                        sDay = 02;
                    }
                    if (Convert.ToDateTime("2000-01-01 " + shiftObj.StartTime.Value.ToString("H:mm:ss")) >
                        Convert.ToDateTime("2000-01-01 " + shiftObj.EndTime.Value.ToString("H:mm:ss")))
                    {
                        eDay = 02;
                    }
                    shiftObj.Rank = 10;
                    shiftObj.StartTime = new DateTime(2000, 01, sDay, start.Hour, start.Minute, start.Second);
                    shiftObj.EndTime = new DateTime(2000, 01, eDay, end.Hour, end.Minute, end.Second);

                    _hrShiftDao.Save(shiftObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public bool Update(Shift shiftObj)
        {
            if (shiftObj == null)
            {
                throw new NullObjectException("shift object can not be null");
            }
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    int sDay = 01;
                    int eDay = 01;
                    //check before save
                    CheckShiftObjectBeforeSave(shiftObj);
                    var organization = shiftObj.Organization;
                    var organizationOfficeStartTimeString = Convert.ToDateTime("2000-01-01 " + organization.AttendanceStartTime.ToString("H:mm:ss"));
                    var organizationOfficeEndTimeString = Convert.ToDateTime(organizationOfficeStartTimeString).AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy-MM-dd H:mm:ss");

                    DateTime start = shiftObj.StartTime.Value;
                    DateTime end = shiftObj.EndTime.Value;

                    if (Convert.ToDateTime("2000-01-01 " + shiftObj.StartTime.Value.ToString("H:mm:ss")) < organizationOfficeStartTimeString)
                    {
                        sDay = 02;
                    }
                    if (Convert.ToDateTime("2000-01-01 " + shiftObj.StartTime.Value.ToString("H:mm:ss")) > Convert.ToDateTime("2000-01-01 " + shiftObj.EndTime.Value.ToString("H:mm:ss")))
                    {
                        eDay = 02;
                    }
                    shiftObj.StartTime = new DateTime(2000, 01, sDay, start.Hour, start.Minute, start.Second);
                    shiftObj.EndTime = new DateTime(2000, 01, eDay, end.Hour, end.Minute, end.Second);


                    // TODO only inactive if this shift has no active member
                    if (shiftObj.Status == 1)
                    {
                        shiftObj.Status = Shift.EntityStatus.Active;
                    }
                    else if (shiftObj.Status == -1)
                    {
                        shiftObj.Status = Shift.EntityStatus.Inactive;
                    }
                    _hrShiftDao.Update(shiftObj);

                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool Delete(long id)
        {
            if (id <= 0)
            {
                throw new InvalidDataException("id is not valid");
            }
            ITransaction transaction = null;
            try
            {
                Shift tempShiftSettingObj = _hrShiftDao.LoadById(id);
                // TODO check if any member has this shift can't delete it

                int totalHrTeamMemberHasThisShift = tempShiftSettingObj
                    .ShiftWeekendHistory
                    .Where(x => x.Status != ShiftWeekendHistory.EntityStatus.Delete)
                    .ToList()
                    .Count();
                if (totalHrTeamMemberHasThisShift > 0)
                {
                    throw new DependencyException("This Shift has total " + totalHrTeamMemberHasThisShift.ToString() + " record(s). Please delete those then you can delete this Shift.");
                }

                using (transaction = Session.BeginTransaction())
                {
                    tempShiftSettingObj.Status = Shift.EntityStatus.Delete;
                    _hrShiftDao.Update(tempShiftSettingObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        
        #endregion

        #region Single Instances Loading Function

        public Shift LoadById(long id)
        {
            Shift hrShift;
            try
            {
                hrShift = _hrShiftDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return hrShift;
        }

        #endregion

        #region List Loading Function

        public IList<Shift> LoadShift(List<UserMenu> userMenu,string orderBy, string direction, long? organizationId)
        {
            IList<Shift> shiftList;
            try
            {
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                shiftList = _hrShiftDao.LoadShift(orderBy, direction, organizationIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return shiftList;
        }

        public IList<Shift> LoadShift(List<long> organizationIds)
        {
           var shiftList= _hrShiftDao.LoadShift(organizationIds);
           return shiftList;
        }

        #endregion

        #region Others Function

        public int GetShiftRowCount(List<UserMenu> usermenu ,string orderBy, string direction, long? organizationId)
        {
            int shiftCount;
            try
            {
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(usermenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                shiftCount = _hrShiftDao.GetShiftRowCount(orderBy, direction, organizationIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return shiftCount;
        }

        #endregion

        #region Helper function

        private void CheckShiftObjectBeforeSave(Shift shiftObj)
        {
            Organization org = shiftObj.Organization;
            if (org == null)
                throw new EmptyFieldException("Invalid Organization or No organization found!");
            if (string.IsNullOrEmpty(shiftObj.Name.Trim()))
                throw new EmptyFieldException("Name can not be empty!");
            var organizationOfficeStartTimeString = Convert.ToDateTime("2000-01-01 " + org.AttendanceStartTime.ToString("H:mm:ss"));
            var organizationOfficeEndTimeString = Convert.ToDateTime(organizationOfficeStartTimeString).AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy-MM-dd H:mm:ss");

            if (shiftObj.StartTime == null || shiftObj.EndTime == null)
                throw new EmptyFieldException("Either Start Time or End Time is empty!");

            var convertedStartTime = Convert.ToDateTime("2000-01-01 " + shiftObj.StartTime.Value.ToString("H:mm:ss"));
            var convertedEndTime = Convert.ToDateTime("2000-01-01 " + shiftObj.EndTime.Value.ToString("H:mm:ss"));
            var nameExists = _hrShiftDao.CheckDublicateName(shiftObj.Name, org.Id, shiftObj.Id);
            if (nameExists)
                throw new DuplicateEntryException("Shift Setting name already remain");

            if (convertedStartTime < organizationOfficeStartTimeString)
            {
                convertedStartTime = convertedStartTime.AddDays(1);
            }
            if (convertedStartTime > convertedEndTime)
            {
                convertedEndTime = convertedEndTime.AddDays(1);
            }
            if (convertedStartTime < Convert.ToDateTime(organizationOfficeStartTimeString) || convertedStartTime > Convert.ToDateTime(organizationOfficeEndTimeString))
                throw new EmptyFieldException("Shift Setting Start time not maching according to organization Attendance Start Time");

            if (convertedEndTime < Convert.ToDateTime(organizationOfficeStartTimeString) || convertedEndTime > Convert.ToDateTime(organizationOfficeEndTimeString))
                throw new EmptyFieldException("Shift Setting End time not maching according to organization Attendance Start Time");

            var diffHour = (convertedEndTime - convertedStartTime).TotalHours;
            if (diffHour <= 0)
            {
                throw new InvalidDataException("Shift must have a duration to work.");
            }

            if (diffHour >= 24)
            {
                throw new EmptyFieldException("Shift must have a less than 1 day duration to work.");
            }

            var nameStartEndTimeExists = _hrShiftDao.CheckDublicateStartEndTime(convertedStartTime, convertedEndTime, org.Id, shiftObj.Id);
            if (nameStartEndTimeExists)
                throw new DuplicateEntryException("Shift Start & End Time already remain");

        }

        #endregion
        
    }
}
