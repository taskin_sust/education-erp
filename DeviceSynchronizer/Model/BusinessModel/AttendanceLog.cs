﻿using System;

namespace DeviceSynchronizer.Model.BusinessModel
{
    public class AttendanceLog
    {
        public virtual int ID { get; set; }
        public virtual int Pin { get; set; }
        public virtual int PunchType { get; set; }
        public virtual string PunchTime { get; set; }
        public virtual int DeviceId { get; set; }
        public virtual int EnrollNumber { get; set; }   //DTP
    }
}
