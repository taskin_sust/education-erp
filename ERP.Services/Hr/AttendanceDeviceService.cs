﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;

namespace UdvashERP.Services.Hr
{
    public interface IAttendanceDeviceService : IBaseService
    {
        #region Operational Function
        void Save(AttendanceDevice hrAttendanceDeviceObj);
        void Delete(AttendanceDevice deleteHrAttendanceDevice);
        void Update(AttendanceDevice hrAttendanceDevice);
        void DeleteParmanent(IList<AttendanceDevice> hrAttendanceDeviceList);
        #endregion

        #region Single Instances Loading Function
        AttendanceDevice GetAttendanceDevice(long id);
        AttendanceDevice GetAttendanceDeviceByMachineNo(long deviceId);

        #endregion

        #region List Loading Function
        List<AttendanceDevice> LoadDevices(int start, int length, string orderBy, string orderDir, string organizationId, string branchId, string campusId, string status, string iPAddress);
        #endregion

        #region Others Function
        int DeviceRowCount(string organizationId, string branchId, string campusId, string status, string iPAddress);
        #endregion

        #region Helper Function

        #endregion

        void InitialEntry();
    }
    public class AttendanceDeviceService : BaseService, IAttendanceDeviceService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IAttendanceDeviceDao _hrAttendanceDeviceDao;
        ITeamMemberDao _teamMemberDao;
        IAttendanceDeviceMemberDao _attendanceDeviceMemberDao;

        public AttendanceDeviceService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrAttendanceDeviceDao = new AttendanceDeviceDao() { Session = session };
            _teamMemberDao = new TeamMemberDao() { Session = session };
            _attendanceDeviceMemberDao = new AttendanceDeviceMemberDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public void Save(AttendanceDevice hrAttendanceDeviceObj)
        {
            ITransaction transaction = null;
            try
            {
                if (hrAttendanceDeviceObj == null)
                {
                    throw new NullObjectException("Attendance Device can not be null");
                }

                var validationContext = new ValidationContext(hrAttendanceDeviceObj, null, null);
                var validationResults = new List<ValidationResult>();
                var isValid = false;
                isValid = Validator.TryValidateObject(hrAttendanceDeviceObj, validationContext, validationResults, true);

                if (!isValid)
                {
                    string errorMessage = "";
                    validationResults.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                    throw new MessageException(errorMessage);
                }

                DuplicatationCheck(hrAttendanceDeviceObj.MachineNo, hrAttendanceDeviceObj.AttendanceSynchronizer.Id, hrAttendanceDeviceObj.IpAddress);

                //attendance device member
                //IList<AttendanceDeviceMember> deviceMembers = new List<AttendanceDeviceMember>();
                IList<TeamMember> teamMembers = _teamMemberDao.LoadAllOk();
                DateTime lastUpdateTime = DateTime.Now;
                using (transaction = Session.BeginTransaction())
                {
                    _hrAttendanceDeviceDao.Save(hrAttendanceDeviceObj);
                    if (hrAttendanceDeviceObj.Id > 0)
                    {
                        AttendanceDevice attendanceDevice = new AttendanceDevice();
                        attendanceDevice = _hrAttendanceDeviceDao.LoadById(hrAttendanceDeviceObj.Id);
                        foreach (TeamMember member in teamMembers)
                        {
                            AttendanceDeviceMember newDeviceMember = new AttendanceDeviceMember();
                            newDeviceMember.AttendanceDevice = attendanceDevice;
                            newDeviceMember.TeamMember = member;
                            newDeviceMember.EnrollNo = _attendanceDeviceMemberDao.GetMaxEnrollNo(hrAttendanceDeviceObj.Id);
                            newDeviceMember.LastUpdateDateTime = lastUpdateTime;
                            newDeviceMember.IsSynced = 0;
                            newDeviceMember.LastSyncedDateTime = null;
                            Session.Save(newDeviceMember);
                        }
                    }
                    transaction.Commit();
                }

            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }

        }

        public void Delete(AttendanceDevice deleteHrAttendanceDevice)
        {
            ITransaction transaction = null;
            try
            {

                if (deleteHrAttendanceDevice == null)
                {
                    throw new NullObjectException("Attendance Device can not be null");
                }
                var newObj = _hrAttendanceDeviceDao.LoadById(deleteHrAttendanceDevice.Id);
                if (newObj == null)
                {
                    throw new NullObjectException("Attendance Device can not be null");
                }
                if (newObj.AttendanceDeviceRawDatas.Any())
                {
                    throw new DependencyException("This Attendance Device have one or more child data.");
                }

                using (transaction = Session.BeginTransaction())
                {
                    _hrAttendanceDeviceDao.Update(deleteHrAttendanceDevice);
                    transaction.Commit();
                }

            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void Update(AttendanceDevice hrAttendanceDevice)
        {
            ITransaction transaction = null;
            try
            {

                if (hrAttendanceDevice == null || hrAttendanceDevice.Id == 0)
                {
                    throw new NullObjectException("Attendance Device can not be null");
                }

                var newObj = _hrAttendanceDeviceDao.LoadById(hrAttendanceDevice.Id);

                if (newObj == null)
                {
                    throw new NullObjectException("Attendance Device can not be null");
                }

                var validationContext = new ValidationContext(hrAttendanceDevice, null, null);
                var validationResults = new List<ValidationResult>();
                var isValid = false;
                isValid = Validator.TryValidateObject(hrAttendanceDevice, validationContext, validationResults, true);

                if (!isValid)
                {
                    string errorMessage = "";
                    validationResults.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                    throw new MessageException(errorMessage);
                }

                DuplicatationCheck(hrAttendanceDevice.MachineNo, hrAttendanceDevice.AttendanceSynchronizer.Id, hrAttendanceDevice.IpAddress, hrAttendanceDevice.Id);
                //if (hrAttendanceDevice.AttendanceDeviceSynchronizations.Any())
                //{
                //    throw new DependencyException("This Attendance Device have one or more child data.");
                //}
                //else
                //{
                using (transaction = Session.BeginTransaction())
                {
                    _hrAttendanceDeviceDao.Update(hrAttendanceDevice);

                    AttendanceDevice attendanceDevice = new AttendanceDevice();
                    var attendanceDeviceMembers = _attendanceDeviceMemberDao.LoadDeviceMember(hrAttendanceDevice.Id);
                    var lastUpdateTime = DateTime.Now;
                    foreach (AttendanceDeviceMember deviceMember in attendanceDeviceMembers)
                    {
                        deviceMember.LastUpdateDateTime = lastUpdateTime;
                        Session.Update(deviceMember);
                    }
                    transaction.Commit();
                }
                //}

            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void DeleteParmanent(IList<AttendanceDevice> hrAttendanceDeviceList)
        {
            ITransaction transaction = null;
            try
            {
                if (hrAttendanceDeviceList.Any())
                {
                    using (transaction = Session.BeginTransaction())
                    {
                        foreach (var hrAttendanceDevice in hrAttendanceDeviceList)
                        {
                            _hrAttendanceDeviceDao.Delete(hrAttendanceDevice);
                        }
                        transaction.Commit();
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }


        public List<AttendanceDevice> LoadDevices(int start, int length, string orderBy, string orderDir, string organizationId, string branchId, string campusId, string status, string iPAddress)
        {
            try
            {
                return _hrAttendanceDeviceDao.LoadDevices(start, length, orderBy, orderDir, organizationId, branchId, campusId, status, iPAddress);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public AttendanceDevice GetAttendanceDevice(long id)
        {
            try
            {
                return _hrAttendanceDeviceDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public AttendanceDevice GetAttendanceDeviceByMachineNo(long deviceId)
        {
            try
            {
                return _hrAttendanceDeviceDao.GetAttendanceDeviceByMachineNo(deviceId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function
        public int DeviceRowCount(string organizationId, string branchId, string campusId, string status, string iPAddress)
        {
            try
            {
                return _hrAttendanceDeviceDao.DeviceRowCount(organizationId, branchId, campusId, status, iPAddress);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Helper function

        private void DuplicatationCheck(int machineNo, long synchronizerId, string ipAddress, long id = 0)
        {
            var data = _hrAttendanceDeviceDao.GetDevice(machineNo, synchronizerId, ipAddress, id);
            if (data != null)
                throw new DuplicateEntryException("Duplicate Device found");
        }

        #endregion

        public void InitialEntry()
        {
            ITransaction transaction = null;
            try
            {
                IList<AttendanceDevice> attendanceDevices = _hrAttendanceDeviceDao.LoadAllOk();
                IList<TeamMember> teamMembers = _teamMemberDao.LoadAllOk();
                DateTime lastUpdateTime = DateTime.Now;
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var attenDevice in attendanceDevices.Where(x => x.Status == AttendanceDevice.EntityStatus.Active))
                    {
                        AttendanceDevice attendanceDevice = _hrAttendanceDeviceDao.LoadById(attenDevice.Id);
                        //checking
                        var attendanceDeviceMember = _attendanceDeviceMemberDao.HasDeviceMember(attenDevice.Id);

                        if (attendanceDeviceMember!=true)
                        {
                            foreach (TeamMember member in teamMembers)
                            {
                                AttendanceDeviceMember newDeviceMember = new AttendanceDeviceMember();
                                newDeviceMember.AttendanceDevice = attendanceDevice;
                                newDeviceMember.TeamMember = member;
                                newDeviceMember.EnrollNo = _attendanceDeviceMemberDao.GetMaxEnrollNo(attenDevice.Id);
                                newDeviceMember.LastUpdateDateTime = lastUpdateTime;
                                newDeviceMember.IsSynced = 0;
                                newDeviceMember.LastSyncedDateTime = null;
                                Session.Save(newDeviceMember);
                            }
                        }
                       
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }


        }
    }
}
