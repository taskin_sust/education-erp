﻿$(document).ready(function () {
    $("#answers").find(".ansClass").each(function () {
        if ($(this).val() == "") {
            $(this).parent().remove();
        }

        if ($("#answers").find(".answerDiv").length <= 0) {

            var newTrId = '<div style="margin-bottom: 5px;"  class="answerDiv">';
            newTrId += '<input type="text" name="answer[]" placeholder="বাংলায় উত্তর লিখুন" class="form-control ansClass" style="float: left; margin-right: 5px;" />';
            newTrId += '<button type="button" class="btn btn-default plus">';
            newTrId += '<span class="glyphicon glyphicon-plus-sign"></span>';
            newTrId += '</button>';
            newTrId += '<div class="clearfix"></div>';
            newTrId += '</div>';

            $("#answers").find(".plus").removeClass("plus").addClass("minus");
            $("#answers").find("span").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
            $("#answers").append(newTrId);
        }       
    });

    $(document).on("click", ".plus", function () {
        var newTrId = '<div style="margin-bottom: 5px;"  class="answerDiv">';
        newTrId += '<input type="text" name="answer[]" placeholder="বাংলায় উত্তর লিখুন" class="form-control ansClass" style="float: left; margin-right: 5px;" />';
        newTrId += '<button type="button" class="btn btn-default plus">';
        newTrId += '<span class="glyphicon glyphicon-plus-sign"></span>';
        newTrId += '</button>';
        newTrId += '<div class="clearfix"></div>';
        newTrId += '</div>';

        $("#answers").find(".plus").removeClass("plus").addClass("minus");
        $("#answers").find("span").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
        $("#answers").append(newTrId);
    });

    $(document).on("click", ".minus", function () {
        $(this).parent().remove();        
    });

    $("#answers .plus").addClass("minus").removeClass("plus").find(".glyphicon-plus-sign").addClass("glyphicon-minus-sign").removeClass("glyphicon-plus-sign");
    $("#answers .minus:last").addClass("plus").removeClass("minus").find(".glyphicon-minus-sign").addClass("glyphicon-plus-sign").removeClass("glyphicon-minus-sign"); 
});

$(document).on('click', '#surveyQuestionSave', function (event) {
    surveyQuestion = {};
    var isSuccess = 0;
    var question = $("#Question").val();
    if (question == "") {
        $("#Question").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("<span>Question is required </span>");
        $("#Question").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Question").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Question").removeClass("highlight");
    }
    
    var surveyQuestionsAnswers = [];
    var totalAns = 0;
    $("#answers").find(".ansClass").each(function() {
        var ans = $(this).val();
        if (ans != "") {
            totalAns++;
            surveyQuestionsAnswer = {}
            surveyQuestionsAnswer["Answer"] = ans;
            surveyQuestionsAnswer["SurveyQuestion"] = null;
            surveyQuestionsAnswer["StudentSurveyAnswers"] = null;
            surveyQuestionsAnswers.push(surveyQuestionsAnswer);

        }
    });

    if (totalAns == 0) {
        isSuccess = isSuccess + 1;
        console.log(isSuccess);
        console.log(totalAns);
        $.fn.customMessage({
            displayMessage: "You must give at least one answer.",
            displayMessageType: "error",
        });
    }

    console.log();
   
    if (isSuccess==0) {
        surveyQuestion["Rank"] = 0; 
        surveyQuestion["Question"] = question; 
        surveyQuestion["SurveyQuestionAnswers"] = surveyQuestionsAnswers;
        surveyQuestion["StudentSurveyAnswers"] = null; 
        surveyQuestion["ProgramSessionSurveyQuestions"] = null;

        console.log(surveyQuestion);

        var stringify = JSON.stringify(surveyQuestion);
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/Survey/SaveSurveyQuestion",
            cache: false,
            async: true,
            dataType: "json",
            contentType: "application/json",
            data: stringify,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {                                      
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "s",
                    });

                    $("#Question").val("");
                    $("#answers").find(".ansClass").each(function () {
                        $(this).parent().remove();                        
                    });
                    if ($("#answers").find(".answerDiv").length <= 0) {
                        var newTrId = '<div style="margin-bottom: 5px;"  class="answerDiv">';
                        newTrId += '<input type="text" name="answer[]" placeholder="বাংলায় উত্তর লিখুন" class="form-control ansClass" style="float: left; margin-right: 5px;" />';
                        newTrId += '<button type="button" class="btn btn-default plus">';
                        newTrId += '<span class="glyphicon glyphicon-plus-sign"></span>';
                        newTrId += '</button>';
                        newTrId += '<div class="clearfix"></div>';
                        newTrId += '</div>';
                        $("#answers").find(".plus").removeClass("plus").addClass("minus");
                        $("#answers").find("span").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
                        $("#answers").append(newTrId);
                    }

                }
                else {
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "error",
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});


$(document).on("input propertychange", "#Question ", function (event) {
    var question = $("#Question").val();
    if (question == "") {
        $("#Question").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("<span>Question is required </span>");
        $("#Question").addClass("highlight");       
    }
    else {
        $("#Question").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Question").removeClass("highlight");
    }
});






//$(document).on("input propertychange", ".tridclass ", function (event) {
//    var input = $(this),
//    text = input.val().replace(/[^0-9.]/g, "");
//    input.val(text);
//});