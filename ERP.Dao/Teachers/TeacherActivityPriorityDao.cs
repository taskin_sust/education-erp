﻿using System.Collections.Generic;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Teachers
{
    public interface ITeacherActivityPriorityDao : IBaseDao<TeacherActivityPriority, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<TeacherActivityPriority> LoadTeacherActivityPriority(long id);
        #endregion

        #region Others Function

        #endregion
    }

    public class TeacherActivityPriorityDao : BaseDao<TeacherActivityPriority, long>, ITeacherActivityPriorityDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<TeacherActivityPriority> LoadTeacherActivityPriority(long id)
        {
            var criteria = Session.CreateCriteria<TeacherActivityPriority>();
            criteria.CreateCriteria("Teacher", "teacher").Add(Restrictions.Eq("teacher.Status", Teacher.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Status", TeacherActivityPriority.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("teacher.Id",id));
            return criteria.List<TeacherActivityPriority>();
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
