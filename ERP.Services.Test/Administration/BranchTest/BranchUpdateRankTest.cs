﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.AdminisTration.BranchTest;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BranchTest
{
    interface IBranchUpdateRankTest
    {
        void Should_Return_Successfully_UpdateRank_Up();
        void Should_Return_Successfully_UpdateRank_Down();
        void Should_Return_Null_Object_Exception();
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Branch")]
    public class BranchUpdateRankTest : BranchBaseTest, IBranchUpdateRankTest
    {
        [Fact]
        public void Should_Return_Successfully_UpdateRank_Up()
        {
            string up = "up";
            var branchFactory =_branchFactory.Create().WithOrganization().Persist();
            Assert.True(_branchService.UpdateRank(branchFactory.Object, up));
        }
        [Fact]
        public void Should_Return_Successfully_UpdateRank_Down()
        {
            string up = "";
            var branchFactory = _branchFactory.Create().WithOrganization().Persist();
            Assert.True(_branchService.UpdateRank(branchFactory.Object, up));
        }
        [Fact]
        public void Should_Return_Null_Object_Exception()
        {
            Assert.Throws<NullObjectException>(() => _branchService.UpdateRank(null, ""));
        }
    }
}
