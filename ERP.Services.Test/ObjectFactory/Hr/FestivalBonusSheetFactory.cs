﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class FestivalBonusSheetFactory:ObjectFactoryBase<FestivalBonusSheet>
    {
        public FestivalBonusSheetFactory(IObjectFactoryBase caller,ISession session):base(caller,session)
        {

        }

        public FestivalBonusSheetFactory Create()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            Object = new FestivalBonusSheet()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = 1,
                Name = Guid.NewGuid().ToString(),
                BankAccount = 100,
                CashAccount = 20,
                Remarks = "test",
                IsSubmit = true,
                StartDate = new DateTime(DateTime.Now.Year,DateTime.Now.Month,1),
                EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, lastDayOfMonth),
                Year = DateTime.Now.Year,
                Month = DateTime.Now.Month
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public FestivalBonusSheetFactory CreateWith(long id,int bankAccount,int cashAccount,bool isSubmit,int month,int year)
        {
            var lastDayOfMonth = DateTime.DaysInMonth(year, month);
            Object = new FestivalBonusSheet()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = 1,
                Name = Guid.NewGuid().ToString(),
                BankAccount = bankAccount,
                CashAccount = cashAccount,
                Remarks = "test",
                IsSubmit = isSubmit,
                StartDate = new DateTime(year, month, 1),
                EndDate = new DateTime(year, month, lastDayOfMonth),
                Year = year,
                Month = month
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public FestivalBonusSheetFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public FestivalBonusSheetFactory WithFestivalSetting(FestivalBonusSetting festivalBonusSetting)
        {
            Object.FestivalBonusSetting = festivalBonusSetting;
            return this;
        }

        public FestivalBonusSheetFactory WithSalaryOrganization(Organization salaryOrganization)
        {
            Object.SalaryOrganization = salaryOrganization;
            return this;
        }
        public FestivalBonusSheetFactory WithSalaryBranch(Branch salaryBranch)
        {
            Object.SalaryBranch = salaryBranch;
            return this;
        }
        public FestivalBonusSheetFactory WithSalaryCampus(Campus salaryCampus)
        {
            Object.SalaryCampus = salaryCampus;
            return this;
        }
        public FestivalBonusSheetFactory WithSalaryDepartment(Department salaryDepartment)
        {
            Object.SalaryDepartment = salaryDepartment;
            return this;
        }
        public FestivalBonusSheetFactory WithSalaryDesignation(Designation salaryDesignation)
        {
            Object.SalaryDesignation = salaryDesignation;
            return this;
        }

        public FestivalBonusSheetFactory WithJobOrganization(Organization jobOrganization)
        {
            Object.JobOrganization = jobOrganization;
            return this;
        }
        public FestivalBonusSheetFactory WithJobBranch(Branch jobBranch)
        {
            Object.JobBranch = jobBranch;
            return this;
        }
        public FestivalBonusSheetFactory WithJobCampus(Campus jobCampus)
        {
            Object.JobCampus = jobCampus;
            return this;
        }
        public FestivalBonusSheetFactory WithJobDepartment(Department jobDepartment)
        {
            Object.JobDepartment = jobDepartment;
            return this;
        }
        public FestivalBonusSheetFactory WithJobDesignation(Designation jobDesignation)
        {
            Object.JobDesignation = jobDesignation;
            return this;
        }

        public FestivalBonusSheetFactory CleanUp()
        {
            DeleteAll();
            return this;
        }
    }
}