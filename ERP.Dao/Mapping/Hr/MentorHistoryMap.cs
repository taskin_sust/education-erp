using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class MentorHistoryMap : BaseClassMap<MentorHistory,long> {
        
        public MentorHistoryMap() {
			Table("HR_MentorHistory");
			LazyLoad();
            References(x => x.Designation).Column("MemberDesignationId");//new
            References(x => x.Department).Column("MemberDepartmentId");//new
            //References(x => x.Designation).Cascade.All().Column("MemberDesignationId");//old
            //References(x => x.Department).Cascade.All().Column("MemberDepartmentId");//old
			References(x => x.Organization).Column("MemberOrganizationId");
			References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.Mentor).Column("MentorId");
            References(x => x.MentorDesignation).Column("MentorDesignationId");//new
            References(x => x.MentorDepartment).Column("MentorDepartmentId");//new
            References(x => x.MentorOrganization).Column("MentorOrganizationId");//new
            //References(x => x.MentorDesignation).Cascade.All().Column("MentorDesignationId");//old
            //References(x => x.MentorDepartment).Cascade.All().Column("MentorDepartmentId");//old
            //References(x => x.MentorOrganization).Cascade.All().Column("MentorOrganizationId");//old
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.Pin).Column("Pin");
            //Map(x => x.MentorName).Column("MentorName");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
			//HasMany(x => x.MentorHistoryLog).KeyColumn("MentorHistoryId");
        }
    }
}
