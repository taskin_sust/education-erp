﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentExamMap:BaseClassMap<StudentExam,long>
    {
        public StudentExamMap()
        {
            Table("StudentExam");
            LazyLoad();
            Map(x => x.Name);
            Map(x => x.IsBoard);
            
             //References(x => x.Students).Column("StudentId");
            //HasMany(x => x.StudentCurrentAcademicInfos).KeyColumn("StudentCurrentAccademicId");
            HasMany(x => x.StudentAcademicInfoExamSubjects).KeyColumn("StudentAcademicInfoExamSubjectId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SmsStudentRegistrationSettingList).KeyColumn("StudentExamId").Cascade.AllDeleteOrphan().Inverse();

        }
    }
}
