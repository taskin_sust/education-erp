﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ShiftWeekendHistoryTest
{
    public interface IShiftWeekendHistorySaveTest
    {
        #region basic Save Test

        void Should_Return_Invalid_Object_Exception();
        void Should_Return_Invalid_Object_Id();
        void Should_Return_Save_Successfully();

        #endregion

        #region Business logic and validation Test

        void Should_Return_Invalid_Member_Object_Exception();
        void Should_Return_Invalid_Organization_Object_exception();
        void Should_Return_Invalid_Shift_Object_Exception();
        void Should_Return_Invalid_WeekDays_Exception();
        void Should_Return_NoMemberEmploymentHistory_Exception();
        void Should_Return_Already_Active_ShiftWeekend_For_TeamMember_Exception();
        void Should_Return_Manupulated_Organization_Exception();

        //void Should_Return_LastActivated_Shift_For_Individual_profile_Exception();
        //void Should_Return_Future_Shift_For_Individual_profile_Exceotion();

        void Should_Return_Invalid_ShiftWeekendHistory();
        void Should_Return_SaveBatchWeekendHistory_Successfully();


        #endregion
    }

    [Trait("Area", "Hr")]
    [Trait("Service", "ShiftWeekendHistoryService")]
    public class ShiftWeekendHistorySaveTest : ShiftWeekendHistoryBaseTest, IShiftWeekendHistorySaveTest
    {
        #region basic Save Test
        public void Should_Return_Invalid_Object_Exception()
        {
            _shiftWeekendHistoryFactory.Create();
            Assert.Throws<InvalidDataException>(
                () => _shiftWeekendHistoryService.Save(_shiftWeekendHistoryFactory.Object));
        }

        public void Should_Return_Invalid_Object_Id()
        {
            _shiftWeekendHistoryFactory.Create().Persist();
            Assert.Throws<InvalidDataException>(
                () => _shiftWeekendHistoryService.Save(_shiftWeekendHistoryFactory.Object));
        }

        public void Should_Return_Save_Successfully()
        {
            _shiftWeekendHistoryFactory.Create();
            _shiftWeekendHistoryService.Save(_shiftWeekendHistoryFactory.Object);
            Assert.True(_shiftWeekendHistoryFactory.Object.Id > 0);
        }

        #endregion

        #region Business logic and validation Test

        [Fact]
        public void Should_Return_Invalid_Member_Object_Exception()
        {
            var teamMember = PersistTeamMember();
            var organizationId = teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id;
            var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
            var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
            var effectiveDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(
                () =>
                    _shiftWeekendHistoryService.AddShiftWeekendHistory(null, organizationId, shiftId, weekend,
                        effectiveDate.ToString()));
        }

        [Fact]
        public void Should_Return_Invalid_Organization_Object_exception()
        {
            var teamMember = PersistTeamMember();
            var organizationId = teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id;
            var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
            var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
            var effectiveDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(
                () =>
                    _shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, 0, shiftId, weekend,
                        effectiveDate.ToString()));
        }

        [Fact]
        public void Should_Return_Invalid_Shift_Object_Exception()
        {
            var teamMember = PersistTeamMember();
            var organizationId = teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id;
            var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
            var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
            var effectiveDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(
                () =>
                    _shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, organizationId, 0, weekend,
                        effectiveDate.ToString()));
        }

        [Fact]
        public void Should_Return_Invalid_WeekDays_Exception()
        {
            var teamMember = PersistTeamMember();
            var organizationId = teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id;
            var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
            var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
            var effectiveDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(
                () =>
                    _shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, organizationId, shiftId, "0",
                        effectiveDate.ToString()));
        }

        [Fact]
        public void Should_Return_NoMemberEmploymentHistory_Exception()
        {
            var teamMember = PersistTeamMember();
            var organizationId = teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id;
            var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
            var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
            var effectiveDate = DateTime.Now;
            teamMember.EmploymentHistory = new List<EmploymentHistory>();
            Assert.Throws<InvalidDataException>(
                () =>
                    _shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, organizationId, shiftId, weekend,
                        effectiveDate.ToString()));
        }

        [Fact]
        public void Should_Return_Already_Active_ShiftWeekend_For_TeamMember_Exception()
        {
            var teamMember = PersistTeamMemberForHistory();
            var organizationId = teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id;
            var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
            var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
            var effectiveDate = DateTime.Now;
            //_shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, organizationId, shiftId, weekend,
            //    effectiveDate.ToString());
            Assert.Throws<InvalidDataException>(
                () =>
                    _shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, organizationId, shiftId, weekend,
                        effectiveDate.ToString()));
        }

        [Fact]
        public void Should_Return_Manupulated_Organization_Exception()
        {
            var teamMember = PersistTeamMember();
            var organizationId = organizationFactory.Create().Persist().Object.Id;
            var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
            var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
            var effectiveDate = DateTime.Now;
            teamMember.EmploymentHistory = new List<EmploymentHistory>();
            Assert.Throws<InvalidDataException>(
                () =>
                    _shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, organizationId, shiftId, weekend,
                        effectiveDate.ToString()));
        }

        //[Fact]
        //public void Should_Return_LastActivated_Shift_For_Individual_profile_Exception()
        //{
        //    var teamMember = PersistTeamMember();
        //    var organizationId = teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id;
        //    var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
        //    var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
        //    var effectiveDate = DateTime.Now;
        //    _shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, organizationId, shiftId, weekend,
        //        effectiveDate.ToString());
        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, organizationId, shiftId, weekend,
        //                DateTime.Now.AddDays(-5).ToString()));
        //}

        //[Fact]
        //public void Should_Return_Future_Shift_For_Individual_profile_Exceotion()
        //{
        //    var teamMember = PersistTeamMember();
        //    var organizationId = teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id;
        //    var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
        //    var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
        //    var effectiveDate = DateTime.Now;
        //    _shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, organizationId, shiftId, weekend,
        //        effectiveDate.ToString());
        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _shiftWeekendHistoryService.AddShiftWeekendHistory(teamMember, organizationId, shiftId, weekend,
        //                effectiveDate.ToString()));
        //}

        [Fact]
        public void Should_Return_Invalid_ShiftWeekendHistory()
        {
            var teamMember = PersistTeamMember();
            _shiftWeekendHistoryFactory.Create().WithOrganization(teamMember.EmploymentHistory[0].Campus.Branch.Organization).WithShift(teamMember.ShiftWeekendHistory[0].Shift).WithTeamMember(teamMember);
            Assert.Throws<InvalidDataException>(
                () => _shiftWeekendHistoryService.SaveBatchShiftWeekendHistory(new List<ShiftWeekendHistory>()));
        }

        [Fact]
        public void Should_Return_SaveBatchWeekendHistory_Successfully()
        {
            var teamMember = PersistTeamMemberForHistory();
            //_shiftWeekendHistoryFactory.CreateMore(1, teamMember.EmploymentHistory[0].Campus.Branch.Organization);
            _shiftWeekendHistoryFactory.CreateWith(0, 1, DateTime.Now.AddDays(1))
                .WithOrganization(teamMember.EmploymentHistory[0].Campus.Branch.Organization)
                 .WithShift(teamMember.EmploymentHistory[0].Campus.Branch.Organization).WithTeamMember(teamMember);

            _shiftWeekendHistoryService.SaveBatchShiftWeekendHistory(new List<ShiftWeekendHistory>() { _shiftWeekendHistoryFactory.Object });
            Assert.True(_shiftWeekendHistoryFactory.Object.Id > 0);
        }

        #endregion
    }
}
