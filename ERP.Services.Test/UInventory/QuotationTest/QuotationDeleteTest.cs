﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.QuotationTest
{
    interface IQuotationDeleteTest
    {
        #region Basic Test

        void Should_Return_True();
        void Should_Return_Null_Exception();

        #endregion 

        #region Business logic

        void Should_Return_Dependancy_Exception();

        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "Quotation")]
    public class QuotationDeleteTest : QuotationBaseTest, IQuotationDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_True()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate)
                .WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program, _quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();
            Assert.True(_quotationService.IsDelete(_quotationFactory.Object.Id));
        }

        [Fact]
        public void Should_Return_Null_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate)
                .WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(organizationFactory.Object)
                .WithPurposeprogramAndPurposeSession(_quotationFactory.Object.Program, _quotationFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();
            Assert.Throws<NullObjectException>(() => _quotationService.IsDelete(_quotationFactory.Object.Id + 1));
        }

        [Fact]
        public void Should_Return_Dependancy_Exception()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.PertialIssued, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithPurposeprogramAndPurposeSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();


            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object)
                .Persist();

            Assert.Throws<DependencyException>(() => _quotationService.IsDelete(_quotationFactory.Object.Id));
        }

        #endregion
    }
}
