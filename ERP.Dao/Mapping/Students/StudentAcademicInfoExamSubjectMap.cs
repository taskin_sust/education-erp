﻿using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentAcdemicInfoExamSubjectMap : ClassMap<StudentAcademicInfoExamSubject>
    {
        public StudentAcdemicInfoExamSubjectMap()
        {
            Table("StudentAcademicInfoExamSubject");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.CreationDate).Column("CreationDate");
            Map(x => x.Name).Column("Name");
            Map(x => x.Code).Column("Code");

            References(x => x.StudentExam).Column("StudentExamId");

            HasMany(x => x.StudentAcademicInfoSubjectResult).KeyColumn("StudentAcademicInfoSubjectResultId").Cascade.AllDeleteOrphan().Inverse();
          


        }
    }
}
