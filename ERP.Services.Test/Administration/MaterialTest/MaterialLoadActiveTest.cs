﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Remotion.Linq.Parsing.Structure.IntermediateModel;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MaterialTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Material")]
    public class MaterialLoadActiveTest : MaterialBaseTest
    {
        #region Basic Test

        //[Fact]
        //public void LoadActive_should_return_list()
        //{
        //    _materialFactory.CreateMore(100).Persist();
        //    var singleMaterial = _materialFactory.GetLastCreatedObjectList()[0];
        //    var organizationIdString = singleMaterial.Organization.Id.ToString();
        //    var programIdString = singleMaterial.Program.Id.ToString();
        //    var sessionName = singleMaterial.Session.Name;
        //    var materialTypeName = singleMaterial.MaterialType.Name;
        //    var materialList = _materialService.LoadMaterial(new List<UserMenu>(), 0, 100, "", "", organizationIdString, programIdString, sessionName, materialTypeName, null, "1");
        //    Assert.Equal(100, materialList.Count);
        //}

        [Fact]
        public void LoadMaterial_should_return_list()
        {
            _materialFactory.CreateMore(100).Persist();
            var singleMaterial = _materialFactory.GetLastCreatedObjectList()[0];
            var programId = singleMaterial.Program.Id;
            var sessionId = singleMaterial.Session.Id;
            var materialList = _materialService.LoadMaterial(sessionId, programId);
            Assert.True(materialList.Count > 0);
        }

        [Fact]
        public void LoadMaterialByMaterialType_should_return_list()
        {
            _materialFactory.CreateMore(100).Persist();
            var singleMaterial = _materialFactory.GetLastCreatedObjectList()[0];
            var programId = singleMaterial.Program.Id;
            var sessionId = singleMaterial.Session.Id;
            var materialTypes = new[] { singleMaterial.MaterialType.Id };
            var materialList = _materialService.LoadMaterial(materialTypes, programId, sessionId);
            Assert.True(materialList.Count > 0);
        }
        #endregion
    }
}
