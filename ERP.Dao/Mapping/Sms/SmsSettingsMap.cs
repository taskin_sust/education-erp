﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Mapping.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public class SmsSettingsMap : BaseClassMap<SmsSettings, long>
    {
        public SmsSettingsMap()
        {
            Table("SmsSettings");
            LazyLoad();

            Map(x => x.Rank);
            Map(x => x.Template);
            Map(x => x.MaskName);
            Map(x => x.SmsTime);
            Map(x => x.IsRepeat);
            Map(x => x.DayBefore);
            Map(x => x.DayAfter);
            Map(x => x.RepeatDuration);
            References(x => x.Program).Column("ProgramId");
            References(x => x.SmsType).Column("SmsTypeId");
            References(x => x.Organization).Column("OrganizationId");

            HasManyToMany(x => x.SmsReceivers).Cascade.All().Table("SmsReceiverSettings");
            //HasManyToMany(x => x.SmsReceivers)
            //    .Table("SmsReceiverSettings")
            //    .Cascade.All()
            //    .Inverse();            
        }
    }
}
