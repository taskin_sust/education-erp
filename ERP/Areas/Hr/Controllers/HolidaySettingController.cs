﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.App_Start;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.UserAuth;
using UdvashERP.BusinessRules;
using Constants = UdvashERP.BusinessRules.Constants;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class HolidaySettingController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly IOrganizationService _organizationService;
        private readonly IHolidaySettingService _hrHolidaySettingService;
        private readonly ICommonHelper _commonHelper;
        //authorization
        private List<UserMenu> _userMenu;
        public HolidaySettingController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _organizationService = new OrganizationService(session);
                _hrHolidaySettingService = new HolidaySettingService(session); 
                _commonHelper = new CommonHelper();
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        #endregion

        #region Operational Function

        #region Save operation
        public ActionResult Create(string message = "")
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //ViewBag.OrganizationId = new SelectList(_organizationService.LoadOrganization().ToList(), "Id", "ShortName");
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName");
                var holidayTypeList = Enum.GetValues(typeof(HolidayTypeEnum)).Cast<HolidayTypeEnum>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
                ViewBag.holidayTypeSelectList = new SelectList(holidayTypeList, "Value", "Text");
                var holidayTypeRepeatationList = Enum.GetValues(typeof(HolidayRepeatationType)).Cast<HolidayRepeatationType>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
                ViewBag.holidayTypeRepeatationSelectList = new SelectList(holidayTypeRepeatationList, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return View();
            }
        }
        [HttpPost]
        public ActionResult Create(HolidaySetting hrHolidaySetting, string saveAndExit, string saveAndAddNew)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //ViewBag.OrganizationId = new SelectList(_organizationService.LoadOrganization().ToList(), "Id", "ShortName");
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName");
                var holidayTypeList = Enum.GetValues(typeof(HolidayTypeEnum)).Cast<HolidayTypeEnum>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
                ViewBag.holidayTypeSelectList = new SelectList(holidayTypeList, "Value", "Text");
                var holidayTypeRepeatationList = Enum.GetValues(typeof(HolidayRepeatationType)).Cast<HolidayRepeatationType>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
                ViewBag.holidayTypeRepeatationSelectList = new SelectList(holidayTypeRepeatationList, "Value", "Text");
                var success = _hrHolidaySettingService.Save(hrHolidaySetting);
                var message = "Holiday saved successfully.";
                if (saveAndAddNew != null)
                {
                    ViewBag.SuccessMessage = message;
                    return View();
                }
                else if (saveAndExit != null)
                {
                    return RedirectToAction("Index", new { successMessage = message });
                }
                    
                else
                    return View();
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (ServiceException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return View();
            }
        } 
        #endregion
        
        #region Update Operation
        [HttpGet]
        public ActionResult Edit(long id)
        {
            try
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var holidaySetting = _hrHolidaySettingService.GetHolidaySetting(id);
                    //ViewBag.OrganizationId = new SelectList(_organizationService.LoadOrganization().ToList(), "Id", "ShortName", holidaySetting.Organization.Id);
                    ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName", holidaySetting.Organization.Id);
                    var holidayTypeList = Enum.GetValues(typeof(HolidayTypeEnum)).Cast<HolidayTypeEnum>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
                    ViewBag.holidayTypeSelectList = new SelectList(holidayTypeList, "Value", "Text", holidaySetting.HolidayType);
                    var holidayTypeRepeatationList = Enum.GetValues(typeof(HolidayRepeatationType)).Cast<HolidayRepeatationType>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
                    ViewBag.holidayTypeRepeatationSelectList = new SelectList(holidayTypeRepeatationList, "Value", "Text", holidaySetting.RepeatationType);
                    ViewBag.OrgId = holidaySetting.Organization.Id;
                    return View(holidaySetting);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    return View();
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            } 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(long id,HolidaySetting hrHolidaySetting)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var holidaySetting = _hrHolidaySettingService.GetHolidaySetting(hrHolidaySetting.Id);
                //ViewBag.OrganizationId = new SelectList(_organizationService.LoadOrganization().ToList(), "Id", "ShortName");
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName");
                var holidayTypeList = Enum.GetValues(typeof(HolidayTypeEnum)).Cast<HolidayTypeEnum>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
                ViewBag.holidayTypeSelectList = new SelectList(holidayTypeList, "Value", "Text");
                var holidayTypeRepeatationList = Enum.GetValues(typeof(HolidayRepeatationType)).Cast<HolidayRepeatationType>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
                ViewBag.holidayTypeRepeatationSelectList = new SelectList(holidayTypeRepeatationList, "Value", "Text");
                var success = _hrHolidaySettingService.Edit(id,hrHolidaySetting);
                ViewBag.SuccessMessage = "Holiday edited successfully.";
                ViewBag.OrgId = "";
                return View();
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (ServiceException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (DuplicateNameException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return View();
            }
        }
        #endregion
       
        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                var holidaySetting = _hrHolidaySettingService.GetHolidaySetting(id);
                bool isSuccess = _hrHolidaySettingService.Delete(id);

                if (isSuccess)
                {
                    ViewBag.SuccessMessage = "Holiday deleted Successfully";
                    return Json(new Response(true, "Holiday sucessfully deleted."));
                }
                else
                {
                    ViewBag.ErrorMessage = "Holiday delete fail";
                    return Json(new Response(false, "Holiday delete failed"));
                }
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Holiday Delete Failed";
                return Json(new Response(false, "Holiday Delete Failed !"));
            }
        }
        #endregion
        #endregion

        #region Others Function

        #endregion

        #region Ajax Request Function

        #endregion

        #region Index/Manage Page
        [HttpGet]
        public ActionResult Index(string successMessage="")
        {
            
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //var organization = new SelectList(_organizationService.LoadOrganization(), "Id", "ShortName");
                var organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                var monthList = DateTimeFormatInfo
               .InvariantInfo
               .MonthNames
               .Select((monthName, index) => new SelectListItem
                   {
                       Value = (index + 1).ToString(),
                       Text = monthName
                   }).ToList();
                monthList.RemoveAt(12);
                var yearList =new SelectList(Enumerable.Range(DateTime.Now.Year, 10)) ;
                ViewBag.Organization = organization;
                ViewBag.Month = monthList;
                ViewBag.year = yearList;
                ViewBag.PageSize = Constants.PageSize;
                if (!string.IsNullOrEmpty(successMessage))
                {
                    ViewBag.SuccessMessage = successMessage; 
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
            
            return View();
        }

        #region Render Data Table
        [HttpPost]
        public ActionResult HolidaySettingList(int draw, int start, int length, string organizationId, string month, string year)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                    var convertedOrganizationId = String.IsNullOrEmpty(organizationId)
                        ? (long?) null
                        : Convert.ToInt32(organizationId);
                    var convertedMonth = String.IsNullOrEmpty(month) ? (int?) null : Convert.ToInt32(month);
                    var convertedYear = String.IsNullOrEmpty(year) ? (int?) null : Convert.ToInt32(year);
                    NameValueCollection nvc = Request.Form;
                    var orderBy = "";
                    var orderDir = "";

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                                orderBy = "Organization.Name";
                                break;
                            case "1":
                                orderBy = "Name";
                                break;
                            case "2":
                                orderBy = "DateFrom";
                                break;
                            case "3":
                                orderBy = "DateTo";
                                break;
                            case "4":
                                orderBy = "HolidayType";
                                break;
                            case "5":
                                orderBy = "RepeatationType";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }

                    #endregion

                    List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(_userMenu,
                        (convertedOrganizationId != null)
                            ? _commonHelper.ConvertIdToList((long) convertedOrganizationId)
                            : null);

                    //var recordsTotal = _hrHolidaySettingService.GetHolidayCount(convertedOrganizationId, convertedMonth, convertedYear);
                    //var holidayList = _hrHolidaySettingService.LoadHoliday(start, length, orderBy, orderDir.ToUpper(), convertedOrganizationId, convertedMonth, convertedYear).ToList();
                    var recordsTotal = _hrHolidaySettingService.GetHolidayCount(_userMenu, organizationIdList,
                        convertedMonth, convertedYear);
                    var holidayList =
                        _hrHolidaySettingService.LoadHoliday(start, length, orderBy, orderDir.ToUpper(),
                            organizationIdList, convertedMonth, convertedYear).ToList();
                    if (holidayList.Count < 1)
                    {
                        recordsTotal = 0;
                    }
                    long recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    var sl = start;
                    foreach (var holiday in holidayList)
                    {
                        var str = new List<string>();
                        str.Add((++sl).ToString());
                        str.Add(holiday.Organization.ShortName);
                        str.Add(holiday.Name);
                        str.Add(holiday.DateFrom.ToString("dd MMM yyyy"));
                        str.Add(holiday.DateTo.ToString("dd MMM yyyy"));
                        str.Add(((holiday.DateTo - holiday.DateFrom).Days + 1).ToString());
                        str.Add((holiday.HolidayType != null)
                            ? Enum.GetName(typeof (HolidayTypeEnum), holiday.HolidayType)
                            : "-");
                        str.Add(Enum.GetName(typeof (HolidayRepeatationType), holiday.RepeatationType));
                        str.Add("<a href='" + Url.Action("Edit", "HolidaySetting") + "?Id=" + holiday.Id + "' data-id='" +
                                holiday.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; " +
                                " <a id='" + holiday.Id + "'   href='#' data-name='" + holiday.Name +
                                "'  class='glyphicon glyphicon-trash'></a>");
                        data.Add(str);
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (InvalidDataException ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View();
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    return View();
                }
            }
            return HttpNotFound();
        }

        #endregion

        #endregion

        #region Helper Function
        [HttpGet]
        public ActionResult ExportHoliday(string organizationId, string month, string year)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var convertedOrganizationId = String.IsNullOrEmpty(organizationId) ? (long?)null : Convert.ToInt32(organizationId);
                var convertedMonth = String.IsNullOrEmpty(month) ? (int?)null : Convert.ToInt32(month);
                var convertedYear = String.IsNullOrEmpty(year) ? (int?)null : Convert.ToInt32(year);
                var headerList = new List<string>();
                var columnList = new List<string>();
                var footerList = new List<string>();
                headerList.Add("Holiday List");
                columnList.Add("Organization");
                columnList.Add("Name");
                columnList.Add("Date From");
                columnList.Add("Date To");
                columnList.Add("Duration(Day)");
                columnList.Add("Holiday Type");
                columnList.Add("Repeatation Type");
                footerList.Add("");
                var holidayXlsList = new List<List<object>>();


                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(_userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);

                var holidayList = _hrHolidaySettingService.LoadHoliday(0, 0, "", "", organizationIdList, convertedMonth, convertedYear, true).ToList();
                if (holidayList.Count > 0)
                {
                    foreach (var c in holidayList)
                    {
                        string organization = "",
                            name = "",
                            dateFrom = "",
                            dateTo = "",
                            holidayType = "",
                            duration="",
                            repeatationType = "";

                        if (c.Organization!=null && c.Organization.ShortName != null) organization = c.Organization.ShortName;
                        if (c.Name != null) name = c.Name;
                        if (c.DateFrom != null) dateFrom = c.DateFrom.ToString("dd-MM-yyyy");
                        if (c.DateTo != null) dateTo = c.DateTo.ToString("dd-MM-yyyy");
                        if (c.DateFrom != null && c.DateTo != null) duration = (c.DateTo - c.DateFrom).Days + 1.ToString();
                        if (c.HolidayType != null) holidayType = Enum.GetName(typeof(HolidayTypeEnum), c.HolidayType);
                        if (c.RepeatationType != null)
                        {
                            repeatationType = Enum.GetName(typeof(HolidayRepeatationType), c.RepeatationType);
                        }

                        var xlsRow = new List<object>
                        {
                            organization,
                            name,
                            dateFrom,
                            dateTo,
                            duration,
                            holidayType,
                            repeatationType
                        };
                        holidayXlsList.Add(xlsRow);
                    }
                }

                ExcelGenerator.GenerateExcel(headerList, columnList, holidayXlsList, footerList, "holiday-list__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }
        }
        
        #endregion
    }
}