﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Cache;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.TrainingInfoTest
{
    public class TrainingInfoBaseTest:TestBase, IDisposable
    {
        #region Propertise & Object Initialization
        protected readonly ITrainingInfoService TrainingInfoService;
        protected readonly ITeamMemberService TeamMemberService;
        protected List<long> IdList = new List<long>();

        internal TrainingInfoFactory TrainingInfoFactory;
        internal InstituteCategoryFactory InstituteCategoryFactory;
        internal InstituteFactory InstituteFactory;
        internal TeamMemberFactory TeamMemberFactory;
        internal readonly OrganizationFactory OrganizationFactory;
        internal readonly DepartmentFactory DepartmentFactory;
        internal readonly EmploymentHistoryFactory EmploymentHistoryFactory;
        internal readonly MaritalInfoFactory MaritalInfoFactory;
        internal readonly ShiftWeekendHistoryFactory ShiftWeekendHistoryFactory;
        internal readonly MentorHistoryFactory MentorHistoryFactory;


        private ISession _session;

        #endregion

        public TrainingInfoBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            TrainingInfoService = new TrainingInfoService(_session);
            TeamMemberService = new TeamMemberService(_session);

            TrainingInfoFactory = new TrainingInfoFactory(null, _session);
            InstituteCategoryFactory = new InstituteCategoryFactory(null, _session);
            InstituteFactory = new InstituteFactory(null, _session);

            TeamMemberFactory = new TeamMemberFactory(null, _session);
            OrganizationFactory = new OrganizationFactory(null, _session);
            DepartmentFactory = new DepartmentFactory(null, _session);
            EmploymentHistoryFactory = new EmploymentHistoryFactory(null, _session);
            MaritalInfoFactory = new MaritalInfoFactory(null, _session);
            ShiftWeekendHistoryFactory = new ShiftWeekendHistoryFactory(null, _session);
            MentorHistoryFactory = new MentorHistoryFactory(null, _session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        #region Custom Error Text

        public const string MemberEntityNotFound = "MemberEntityNotFound";
        public const string NullObjectError = "NullObject";
        public const string CompanyNameNullError = "Company Name Not Found!";
        public const string CompanyTypeNullError = "Company Type Not Found!";
        public const string CompanyLocationNullError = "Company Location Not Found!";
        public const string DepartmentNameNullError = "Department Name Not Found!";
        public const string ExperienceAreaNullError = "Experience Area Not Found!";
        public const string JobPositionNullError = "Job Position Not Found!";
        public const string JobDurationNullError = "Job Duration Not Found!";

        #endregion

        internal Institute GetInstituteObject()
        {
            var instituteObj = InstituteFactory.Create().Persist();
            return instituteObj.Object;
        }
        public void Dispose()
        {
            TrainingInfoFactory.CleanUp();
            InstituteFactory.Cleanup();
            InstituteCategoryFactory.Cleanup();
            base.Dispose();
        }
    }
}
