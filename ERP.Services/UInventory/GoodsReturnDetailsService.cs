using log4net;
using NHibernate;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
public interface IGoodsReturnDetailService : IBaseService
{
    #region Operational Function

    bool SaveOrUpdate(GoodsReturnDetails goodsReturnDetail);

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function

    #endregion

    #region Helper Function

    #endregion
}
public class GoodsReturnDetailsService : BaseService, IGoodsReturnDetailService {
    #region Logger
    private readonly ILog _logger = LogManager.GetLogger("InventoryService");
    #endregion

    #region Propertise & Object Initialization
    private readonly CommonHelper _commonHelper;
    private readonly IGoodsReturnDetailsDao _goodsReturnDetailDao;
    public GoodsReturnDetailsService(ISession session)
    {
        Session = session;
        _commonHelper = new CommonHelper();
        _goodsReturnDetailDao=new GoodsReturnDetailsDao{Session = session};}
    #endregion

    #region Operational Functions

    public bool SaveOrUpdate(GoodsReturnDetails goodsReturnDetail)
    {

        return true;
    }

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function


    #endregion

    #region Helper function



    #endregion
}
}
                                    
