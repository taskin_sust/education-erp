﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Mapping.UserAuth
{
    public partial class AspNetUserMap: BaseClassMap<AspNetUser,long>
    {
        public AspNetUserMap()
        {
            LazyLoad();
            Table("AspNetUsers");
            Id(x => x.Id);
            Map(x => x.Rank);
            Map(x => x.FullName).Column("FullName");
            Map(x => x.Email).Column("Email");
            Map(x => x.EmailConfirmed);
            Map(x => x.PasswordHash);
            Map(x => x.SecurityStamp);
            Map(x => x.PhoneNumber).Column("PhoneNumber");
            Map(x => x.PhoneNumberConfirmed);
            Map(x => x.TwoFactorEnabled);
            Map(x => x.LockoutEndDateUtc);
            Map(x => x.LockoutEnabled);
            Map(x => x.AccessFailedCount);
            Map(x => x.UserName).Column("UserName");
        }
    }
}
