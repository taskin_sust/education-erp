﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.ItemTest
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "Item")]
    public class ItemUpdateTest : ItemBaseTest
    {
        [Fact]
        public void Should_Return_Update_Successfully()
        {
            var itemFactory = _itemFactory.Create().WithItemGroup().WithOrganization().Persist();
            var itemObj = _itemService.LoadById(itemFactory.Object.Id);
            itemObj.Name = _name + "Update";
            var itemUpdate = _itemService.SaveOrUpdate(new List<Item>() { itemObj });
            Assert.True(itemUpdate);
        }
    }
}
