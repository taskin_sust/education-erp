﻿using System;

namespace UdvashERP.MessageExceptions
{
    public class OverLimitException:Exception
    {
        public OverLimitException():base("Over Limit")
        {

        }
        public OverLimitException(string message):base(message)
        {

        }

    }
}
