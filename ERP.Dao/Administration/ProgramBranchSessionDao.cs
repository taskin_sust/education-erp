﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.UInventory;

namespace UdvashERP.Dao.Administration
{
    public interface IProgramBranchSessionDao : IBaseDao<ProgramBranchSession, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        ProgramBranchSession GetAllProgramBranchSessionByBranchIdSessionIdProgramId(long branchId, long sessionId, long programId, int?[] selectedAdmissionTypeList = null, bool filterByAdmissionType = true);

        #endregion

        #region List Loading Function

        IList<ProgramBranchSession> LoadProgramBranchSession(long programId, long? sessionId = null);
        IList<ProgramBranchSession> LoadProgramBranchSessionB(long branchId, long? sessionId = null);
        IList<ProgramSessionDto> LoadProgramSession(List<long> organizationIds, List<long> branchIds = null, List<long> authProIdList = null, List<long> itemId = null, bool? withOutCurrentProgramSession = false, bool isReport = false);
        IList<ProgramSessionDto> LoadProgramSessionDto(List<long> authProgramList, long branchFromId, long branchToId, long itemId);
        #endregion

        #region Others Function
        long HasManyProgramObject(long sessionId);
        long HasManyBranchObjectBySession(long sessionId);
        long HasManyBranchObject(long branchId);


        #endregion


    }
    public class ProgramBranchSessionDao : BaseDao<ProgramBranchSession, long>, IProgramBranchSessionDao
    {
        #region Propertise & Object Initialization

        private readonly IItemDao _itemDao;

        public ProgramBranchSessionDao()
        {
            _itemDao = new ItemDao() { Session = Session };
        }

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public ProgramBranchSession GetAllProgramBranchSessionByBranchIdSessionIdProgramId(long branchId, long sessionId, long programId, int?[] selectedAdmissionTypeList = null, bool filterByAdmissionType = true)
        {

            var returnObj = Session.QueryOver<ProgramBranchSession>()
                .Where(x => x.Branch.Id == branchId && x.Session.Id == sessionId && x.Program.Id == programId && x.Status == ProgramBranchSession.EntityStatus.Active);
            if (filterByAdmissionType == false)
            {
                returnObj = returnObj.Where(x => x.IsOffice == false && x.IsPublic == false);
            }
            else
            {
                if (selectedAdmissionTypeList != null)
                {
                    if (selectedAdmissionTypeList.Contains(1))
                    {
                        returnObj = returnObj.Where(x => x.IsOffice == true);
                    }
                    if (selectedAdmissionTypeList.Contains(2))
                    {
                        returnObj = returnObj.Where(x => x.IsPublic == true);
                    }
                }
            }
            return returnObj.List<ProgramBranchSession>().FirstOrDefault();

        }
        #endregion

        #region List Loading Function

        public IList<ProgramBranchSession> LoadProgramBranchSession(long programId, long? sessionId = null)
        {
            IList<ProgramBranchSession> returnProgramBranchSessions;
            if (sessionId == null)
            {
                ICriteria criteria = Session.CreateCriteria<ProgramBranchSession>();
                criteria.Add(Restrictions.Eq("Status", ProgramBranchSession.EntityStatus.Active));
                criteria.CreateAlias("Session", "s", JoinType.InnerJoin);
                criteria.Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("Program.Id", programId));
                // criteria.SetProjection(Projections.Distinct(Projections.Property("s.Id")));
                criteria.SetResultTransformer(Transformers.DistinctRootEntity);
                returnProgramBranchSessions = criteria.List<ProgramBranchSession>();
                return returnProgramBranchSessions;
            }
            returnProgramBranchSessions = Session.QueryOver<ProgramBranchSession>()
                     .Where(x => x.Program.Id == programId && x.Session.Id == sessionId && x.Status == ProgramBranchSession.EntityStatus.Active)
                     .List<ProgramBranchSession>();
            return returnProgramBranchSessions;

        }


        public IList<ProgramBranchSession> LoadProgramBranchSessionB(long branchId, long? sessionId = null)
        {
            if (sessionId == null)
            {
                return Session.QueryOver<ProgramBranchSession>()
                              .Where(x => x.Status == ProgramBranchSession.EntityStatus.Active && x.Branch.Id == branchId && (x.IsOffice || x.IsPublic)).List<ProgramBranchSession>();

            }
            return Session.QueryOver<ProgramBranchSession>().Where(x => x.Branch.Id == branchId
               && x.Session.Id == sessionId && x.Status == ProgramBranchSession.EntityStatus.Active).List<ProgramBranchSession>();
        }


        // called from BuildAuthProgramSession
        public IList<ProgramSessionDto> LoadProgramSession(List<long> organizationIds, List<long> branchIds = null, List<long> authProIdList = null, List<long> itemIds = null, bool? withOutCurrentProgramSession = false, bool isReport = false)
        {
            string condition = "";

            if (organizationIds != null)
            {
                condition = " AND p.OrganizationId IN(" + (string.Join(",", organizationIds)) + ")";
            }

            string itemSearchCondition = "";
            if (itemIds != null && itemIds.Any() && isReport)
            {
                itemSearchCondition = string.Format(@"INNER JOIN UINV_ProgramSessionItem psi ON psi.programid=p.id AND psi.sessionid=s.id AND psi.itemid in({0}) ", string.Join(", ", itemIds));

            }
            else if (itemIds != null && itemIds.Any() && isReport == false)
            {
                //itemIds.FirstOrDefault()
                var itemObj = Session.QueryOver<Item>().Where(x => x.Id == itemIds.FirstOrDefault()).SingleOrDefault();//_itemDao.LoadById(11);
                if (itemObj.ItemType != (int)ItemType.CommonItem)
                    itemSearchCondition = string.Format(@"INNER JOIN UINV_ProgramSessionItem psi ON psi.programid=p.id AND psi.sessionid=s.id AND psi.itemid in({0}) ", string.Join(", ", itemIds));
            }

            if (branchIds != null && branchIds.Count > 0)
            {
                condition += " AND pbs.BranchId IN(" + string.Join(",", branchIds) + " )";
            }
            if (authProIdList != null && authProIdList.Count > 0)
            {
                condition += " AND p.Id IN(" + string.Join(",", authProIdList) + " )";
            }
            string withOutProgramSession = "";
            if (withOutCurrentProgramSession != true)
            {
                withOutProgramSession += " AND (pbs.IsOffice=1 OR pbs.IsPublic=1) ";
            }

            string query = string.Format(@"SELECT  pbs.ProgramId ,
                                                    pbs.SessionId ,
                                                    p.Name AS ProgramName ,
                                                    p.ShortName as ProgramShortName,
                                                    s.Name AS SessionName
                                            FROM    Program p
                                                    INNER JOIN ProgramBranchSession pbs ON p.Id = pbs.ProgramId AND pbs.Status={0} " +
                                                    " INNER JOIN Branch b ON b.Id = pbs.BranchId  AND b.Status={1} " +
                                                    " INNER JOIN Session s ON s.Id = pbs.SessionId AND s.Status={2} " +
                                                    " {3} " + @" WHERE 1 = 1 " + "{4} " +
                                                    withOutProgramSession +
                                                    " AND p.Status=1 " +
                                                    " GROUP BY pbs.ProgramId ,pbs.SessionId ,p.Name, p.ShortName ,s.Name"
                                                   , ProgramBranchSession.EntityStatus.Active
                                                   , Branch.EntityStatus.Active
                                                   , BusinessModel.Entity.Administration.Session.EntityStatus.Active
                                                   , itemSearchCondition
                                                   , condition);

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ProgramSessionDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<ProgramSessionDto>().ToList();
            return list;
        }

        public IList<ProgramSessionDto> LoadProgramSessionDto(List<long> authProgramList, long branchFromId, long branchToId, long itemId)
        {
            string query = string.Format(@"
                                            SELECT  pbs.ProgramId ,
                                                    p.Name AS ProgramName,
                                                    pbs.SessionId ,
                                                    s.Name AS SessionName
                                            FROM    [dbo].[ProgramBranchSession] pbs
                                                    INNER JOIN Program p ON p.Id = pbs.ProgramId AND p.Status=" + Program.EntityStatus.Active + @"
                                                    INNER JOIN Branch b ON b.Id = pbs.BranchId  AND b.Status=" + Branch.EntityStatus.Active + @"
                                                    INNER JOIN Session s ON s.Id = pbs.SessionId AND s.Status=" + BusinessModel.Entity.Administration.Session.EntityStatus.Active + @"
                                                    INNER JOIN [dbo].[UINV_ProgramSessionItem] psi ON psi.ProgramId = p.Id AND psi.SessionId = s.Id
                                                    INNER JOIN UINV_Item it ON it.Id = psi.ItemId  AND it.Status=" + Item.EntityStatus.Active + @"
                                            WHERE   
                                                    1=1
                                                    AND psi.ItemId = {0}
                                                    AND ( p.Id IN ({1}) OR p.Id IS NULL )
                                            GROUP BY pbs.ProgramId ,
                                                    p.Name ,
                                                    pbs.SessionId ,
                                                    s.Name", itemId, string.Join(",", authProgramList));

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ProgramSessionDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<ProgramSessionDto>().ToList();
            return list;
        }

        #endregion

        #region Others Function
        public long HasManyProgramObject(long sessionId)
        {
            var returnObj = Session.QueryOver<ProgramBranchSession>().Where(x => x.Session.Id == sessionId && x.Status != Program.EntityStatus.Delete).List<ProgramBranchSession>();
            return returnObj.Count;
        }
        public long HasManyBranchObject(long branchId)
        {
            var returnObj = Session.QueryOver<ProgramBranchSession>().Where(x => x.Branch.Id == branchId && x.Status != Branch.EntityStatus.Delete).List<ProgramBranchSession>();
            return returnObj.Count;
        }
        public long HasManyBranchObjectBySession(long sessionId)
        {
            var returnObj = Session.QueryOver<ProgramBranchSession>().Where(x => x.Session.Id == sessionId && x.Status != BusinessModel.Entity.Administration.Session.EntityStatus.Delete).List<ProgramBranchSession>();
            return returnObj.Count;
        }
        #endregion



    }
}
