﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UdvashERP.PublicAdmission.Models;

namespace UdvashERP.PublicAdmission.Controllers
{
    public class TeacherController : Controller
    {

        private string _key = ConfigurationManager.AppSettings["apiKey"];
        private string _org = ConfigurationManager.AppSettings["orgBussinessId"];
        private string _baseUrl = ConfigurationManager.AppSettings["teacherRegistrationBaseApiUrl"];
        private const string ReligionUrl = "GetReligion";
        private const string GenderUrl = "GetGender";
        private const string VersionUrl = "GetVersionPriority";
        private const string TeacherActivityUrl = "GetTeacherActivity";
        private const string SubjectUrl = "GetSubject";
        private const string OrganizationUrl = "GetOrganizationByBusinessId";
        private const string RegistrationUrl = "Registration";

        //
        // GET: /Teacher/
        public ActionResult Index()
        {
            return View();
        }

        // 
        // GET: /Teacher/Registration/
        public ActionResult Registration()
        {
            ViewBag.ReligionSelectList = new SelectList(new List<CommonObjectApi>(), "Id", "Name");
            ViewBag.GenderSelectList = new SelectList(new List<CommonObjectApi>(), "Id", "Name");
            ViewBag.TeacherActivitySelectList = new SelectList(new List<CommonObjectApi>(), "Id", "Name");
            ViewBag.SubjectSelectList = new SelectList(new List<CommonObjectApi>(), "Id", "Name");
            ViewBag.VersionSelectList = new SelectList(new List<CommonObjectApi>(), "Id", "Name");
            try
            {
                string regligionJsonString = WebHelper.GetRequest(_baseUrl + ReligionUrl + "?key=" + _key);
                JsonResponseViewModel regligionJsonList = new JavaScriptSerializer().Deserialize<JsonResponseViewModel>(regligionJsonString);
                List<CommonObjectApi> regligionList = regligionJsonList.Data;

                string genderJsonString = WebHelper.GetRequest(_baseUrl + GenderUrl + "?key=" + _key);
                JsonResponseViewModel genderJsonList = new JavaScriptSerializer().Deserialize<JsonResponseViewModel>(genderJsonString);
                List<CommonObjectApi> genderList = genderJsonList.Data;

                string versionJsonString = WebHelper.GetRequest(_baseUrl + VersionUrl + "?key=" + _key);
                JsonResponseViewModel versionJsonList = new JavaScriptSerializer().Deserialize<JsonResponseViewModel>(versionJsonString);
                List<CommonObjectApi> versionList = versionJsonList.Data;

                string teacherActivityJsonString = WebHelper.GetRequest(_baseUrl + TeacherActivityUrl + "?key=" + _key);
                JsonResponseViewModel teacherActivityJsonList = new JavaScriptSerializer().Deserialize<JsonResponseViewModel>(teacherActivityJsonString);
                List<CommonObjectApi> teacherActivityList = teacherActivityJsonList.Data;

                string subjectJsonString = WebHelper.GetRequest(_baseUrl + SubjectUrl + "?key=" + _key);
                JsonResponseViewModel subjectJsonList = new JavaScriptSerializer().Deserialize<JsonResponseViewModel>(subjectJsonString);
                List<CommonObjectApi> subjectList = subjectJsonList.Data;

                //set viewbags value
                ViewBag.ReligionSelectList = new SelectList(regligionList, "Id", "Name");
                ViewBag.GenderSelectList = new SelectList(genderList, "Id", "Name");
                ViewBag.TeacherActivitySelectList = new SelectList(teacherActivityList, "Id", "Name");
                ViewBag.SubjectSelectList = new SelectList(subjectList, "Id", "Name");
                ViewBag.VersionSelectList = new SelectList(versionList, "Id", "Name");
            }
            catch (Exception ex)
            {

            }
            return View();
        }
        // 
        // Post
        [HttpPost]
        public JsonResult Registration(TeacherRegistrationViewModel teacherRegistrationViewModel)
        {
            bool successState = false;
            string message = "";
            try
            {
                string organizationJsonString = WebHelper.GetRequest(_baseUrl + OrganizationUrl + "?key=" + _key + "&businessId=" + _org);
                JsonResponseViewModel organizationJsonList = new JavaScriptSerializer().Deserialize<JsonResponseViewModel>(organizationJsonString);
                List<CommonObjectApi> organizationList = organizationJsonList.Data;
                if (organizationList.Any())
                {
                    teacherRegistrationViewModel.OrganizationId = organizationList[0].Id.ToString();
                }
                string teacherRegistrationViewModelString = new JavaScriptSerializer().Serialize(teacherRegistrationViewModel);
                RegistrationJsonData registrationJsonData = new RegistrationJsonData
                {
                    Key = _key,
                    TeacherRegistrationViewModelString = teacherRegistrationViewModelString
                };
                //send registration Data to API
                string registrationJsonDataString = new JavaScriptSerializer().Serialize(registrationJsonData);
                string registrationJsonString = WebHelper.PostRequest(_baseUrl + RegistrationUrl, registrationJsonDataString);
                JsonResponseViewModel registrationJson = new JavaScriptSerializer().Deserialize<JsonResponseViewModel>(registrationJsonString);
                successState = registrationJson.IsSuccess;
                message = registrationJson.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = WebHelper.CommonErrorMessage;
            }
            return Json(new { IsSuccess = successState, Message = message });
            
        }
	}

    public class RegistrationJsonData
    {
        public virtual string Key { get; set; }
        public virtual string TeacherRegistrationViewModelString { get; set; }
    }
}