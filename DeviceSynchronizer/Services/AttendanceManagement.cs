﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceSynchronizer.ApiReferences;
using DeviceSynchronizer.Dao;
using DeviceSynchronizer.DeviceHelper;
using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model;
using DeviceSynchronizer.Model.ApiModel;
using DeviceSynchronizer.Model.BusinessModel;
using log4net;
using Newtonsoft.Json;

namespace DeviceSynchronizer.Services
{
    public class AttendanceManagement
    {
        #region Declaration

        private static readonly ILog Log = LogManager.GetLogger("ServiceLogger");
        private const int SendingDataAmount = 10;

        private readonly AttendanceLogDao _attendanceLogDao;
        public MessagePass ThrowMessage;
        private IAttendanceDeviceDriver _attendanceDeviceDriver;
        private AttendanceDeviceModel _attendanceDevice;

        #endregion

        #region Constructor
        public AttendanceManagement(AttendanceDeviceModel attendanceDevice, IAttendanceDeviceDriver attendanceDeviceDriver, SQLiteConnection sqLiteConnection, MessagePass throwMessage)
        {
            this._attendanceDevice = attendanceDevice;
            this._attendanceDeviceDriver = attendanceDeviceDriver;
            _attendanceLogDao = new AttendanceLogDao(sqLiteConnection);
            this.ThrowMessage = throwMessage;
        }

        #endregion

        #region Helper

        protected virtual void OnThrowingMessage(string message, MessageType messageType)
        {
            if (ThrowMessage != null)
            {
                ThrowMessage(message, messageType);
            }
        }

        #endregion

        #region Operation

        internal void UpdateAttendance()
        {
            try
            {
                if (_attendanceDeviceDriver.IsAttendanceLogExits() == false)
                {
                    OnThrowingMessage("No new data exits on device [" + _attendanceDevice.IPAddress + "]",
                        MessageType.Info);
                }
                else
                {
                    List<AttendanceLog> attdLogList = _attendanceDeviceDriver.LoadLogList();
                    bool isTrue = _attendanceLogDao.Insert(attdLogList);

                    //Clear device data
                    if (isTrue)
                    {
                        OnThrowingMessage("Clearing attendance data ", MessageType.Info);
                        if (_attendanceDeviceDriver.ClearLog())
                            _attendanceDeviceDriver.RefreshData();
                        else
                            OnThrowingMessage("Unable to clear attendance data ", MessageType.Error);
                    }
                }
                PushAttendanceLogToServer();
                OnThrowingMessage("Attendance sync successfully completed ", MessageType.Success);
            }
            catch (Exception e)
            {
                OnThrowingMessage("Unable to sync attendance data ", MessageType.Error);
                Log.Error(e);
            }
        }

        //Push attendance data to server 
        private void PushAttendanceLogToServer()
        {
            try
            {
                var request = new SyncAttendanceReceiveApiRequest();
                List<AttendanceLog> logList = _attendanceLogDao.LoadByDeviceId(_attendanceDevice.Id);
                if (logList == null || logList.Count <= 0) return;
                int start = 0;
                while (logList.Count / SendingDataAmount >= start)
                {
                    var serverLogList = logList.Skip(start * SendingDataAmount).Take(SendingDataAmount).ToList();
                    var isSuccess = request.UploadAttendanceData(serverLogList);
                    if (isSuccess)
                    {
                        var ids = serverLogList.Select(x => x.ID).ToList();
                        OnThrowingMessage(ids.Count + " log data have been successfully sent to server ", MessageType.Info);
                        //delete data 
                        _attendanceLogDao.Delete(ids);
                        OnThrowingMessage(ids.Count + " log data have been cleared from database", MessageType.Info);
                    }
                    start++;
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        #endregion
    }
}
