﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.FestivalBonusSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "FestivalBonusSetting")]
    public class FestivalBonusSettingUpdateTest : FestivalBonusSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Save_Successfully()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            var obj = _festivalBonusSettingService.LoadById(_festivalBonusSettingFactory.Object.Id);
            obj.IsBuddhism = false;
            _festivalBonusSettingService.SaveOrUpdate(obj);

            Assert.Equal(_festivalBonusSettingFactory.Object.IsBuddhism, obj.IsBuddhism);
        }

        [Fact]
        public void Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _festivalBonusSettingService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Return_Null_Exception_Empty_Organization()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            var obj = _festivalBonusSettingService.LoadById(_festivalBonusSettingFactory.Object.Id);
            obj.Organization = null;
            Assert.Throws<NullObjectException>(
                () => _festivalBonusSettingService.SaveOrUpdate(_festivalBonusSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_BonusCalculation()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            var obj = _festivalBonusSettingService.LoadById(_festivalBonusSettingFactory.Object.Id);

            obj.PrFestivalBonusCalculation = null;
            Assert.Throws<InvalidDataException>(
                () => _festivalBonusSettingService.SaveOrUpdate(_festivalBonusSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_Property_Exception_For_Bonus_Calculation()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            var obj = _festivalBonusSettingService.LoadById(_festivalBonusSettingFactory.Object.Id);

            obj.PrFestivalBonusCalculation[0].PrFestivalBonusSetting = null;
            Assert.Throws<MessageException>(() => _festivalBonusSettingService.SaveOrUpdate(obj));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_InvalidDataException_For_Date()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            var obj = _festivalBonusSettingService.LoadById(_festivalBonusSettingFactory.Object.Id);
            obj.EffectiveDate = DateTime.Now.AddDays(5);
            obj.ClosingDate = DateTime.Now.AddDays(3);
            Assert.Throws<InvalidDataException>(() => _festivalBonusSettingService.SaveOrUpdate(obj));
        }

        [Fact]
        public void Save_Should_Return_InvalidDataException_Empty_Religion()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            var obj = _festivalBonusSettingService.LoadById(_festivalBonusSettingFactory.Object.Id);
            obj.IsBuddhism = false;
            obj.IsChristianity = false;
            obj.IsHinduism = false;
            obj.IsIslam = false;
            obj.IsOthers = false;
            
            Assert.Throws<InvalidDataException>(
                () => _festivalBonusSettingService.SaveOrUpdate(obj));
        }

        #endregion
    }
}
