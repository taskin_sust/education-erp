﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Qmaster.Print.Forms;
using Qmaster.Print.Model;

namespace Qmaster.Print.Helper
{
    public delegate void AlertHandlerThreadExamMcqQuestionSetPrint(object sender, ExamMcqQuestionSetPrintThread.AlertEventArg e);

    public class ExamMcqQuestionSetPrintThread
    {
        public bool IsRunning { get; set; }
        public event AlertHandlerThreadExamMcqQuestionSetPrint OnNewExamMcqQuestionSetPrint;

        private List<FileInfo> allPdfFiles;
        private List<ExamMcqQuestionSetPrint> examMcqQuestionSetPrintList;
        private List<MargePdfSartEndCompare> margePdfList;
        private PrintConfigInfo printConfigInfo;
        private PrinterSettings printerSettings;
        private Thread examMcqQuestionSetPrintSaveTHD;
        private string fileConcatNameString;

        public List<FileInfo> AllPdfFiles { get { return allPdfFiles; } set { allPdfFiles = value; } }
        public List<ExamMcqQuestionSetPrint> ExamMcqQuestionSetPrintList
        {
            get { return examMcqQuestionSetPrintList; }
            set { examMcqQuestionSetPrintList = value; }
        }
        public PrintConfigInfo PrintConfigInfo { get { return printConfigInfo; } set { printConfigInfo = value; } }
        public PrinterSettings PrinterSettings { get { return printerSettings; } set { printerSettings = value; } }
        public List<MargePdfSartEndCompare> MargePdfList { get { return margePdfList; } set { margePdfList = value; } }
        public string FileConcatNameString { get { return fileConcatNameString; } set { fileConcatNameString = value; } }

        public ExamMcqQuestionSetPrintThread()
        {
            examMcqQuestionSetPrintSaveTHD = new Thread(ExamMcqQuestionSetPrintStart);
        }

        private void ExamMcqQuestionSetPrintStart()
        {
            try
            {
                if (examMcqQuestionSetPrintList == null)
                    examMcqQuestionSetPrintList = new List<ExamMcqQuestionSetPrint>();

                int totalPrintCount = 0;
                if (examMcqQuestionSetPrintList != null)
                    totalPrintCount = examMcqQuestionSetPrintList.Count;

                int alreadyPrinted = 0;
                double totalProgressD = 100.0;
                double totalPrinted = totalPrintCount;
                double singleProgressValue = totalProgressD / totalPrinted;
                double currentProgressValue = 0;               
                int printCount = 0;
                
                //send one by one
                foreach (ExamMcqQuestionSetPrint ss in examMcqQuestionSetPrintList)
                {
                    printCount++;
                    //lastExamQuestionPrintId++;

                    //set total upload progress
                    currentProgressValue += singleProgressValue;
                    ss.Progress = int.Parse(Math.Round(currentProgressValue).ToString());
                    ss.ProgressString = ss.Progress.ToString();
                    //upload counter
                    alreadyPrinted++;

                    //if user wants to stop during uploading
                    if (IsRunning == false)
                        break;
                    
                    if (alreadyPrinted >= totalPrintCount)
                        ss.IsAllUploaded = true;

                    string setName = "QuestionSet_" + ss.SetId.ToString();
                    //print Auto
                    FileInfo file = allPdfFiles.SingleOrDefault(x => x.Name.Remove(x.Name.Length - 4) == setName);
                    if (file == null)
                        throw new InvalidDataException("No pdf file find to print question");
                    MargePdfSartEndCompare tempMargePdf = MargePdfList.SingleOrDefault(x => x.Compare == printCount);
                    PrintPdfHelper.PrintPdf(ss, file, printConfigInfo, printCount, printerSettings, tempMargePdf, fileConcatNameString);
                    ss.Message = "Print Successfully";
                    
                    //throw an event 
                    OnNewExamMcqQuestionSetPrint(this, new ExamMcqQuestionSetPrintThread.AlertEventArg() { LastExamMcqQuestionSetPrintUploaded = ss });
                }

                //upload is complete so stop it now 
                Stop();
            }
            catch (InvalidDataException ex)
            {
                //stop and show message
                Stop();
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                OnNewExamMcqQuestionSetPrint(this, new ExamMcqQuestionSetPrintThread.AlertEventArg() { LastExamMcqQuestionSetPrintUploaded = null });
            }
            catch (Exception ex)
            {
                //stop and show message
                Stop();
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                OnNewExamMcqQuestionSetPrint(this, new ExamMcqQuestionSetPrintThread.AlertEventArg() { LastExamMcqQuestionSetPrintUploaded = null });
            }
            finally
            {

            }
        }

        public void Start()
        {
            IsRunning = true;
            examMcqQuestionSetPrintSaveTHD.Start();
        }

        public void Stop()
        {
            IsRunning = false;
            if (examMcqQuestionSetPrintSaveTHD.IsAlive)
                examMcqQuestionSetPrintSaveTHD.Join(10);
        }

        public class AlertEventArg
        {
            public ExamMcqQuestionSetPrint LastExamMcqQuestionSetPrintUploaded { get; set; }
        }
    }

}
