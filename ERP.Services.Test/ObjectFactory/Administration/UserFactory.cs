﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class UserFactory : ObjectFactoryBase<UserProfile>
    {
        #region Object Initialization

        protected readonly IUserService _userService;
        private OrganizationFactory _organizationFactory;
        private CampusFactory _campusFactory;
        private BranchFactory _branchFactory;
        private TestBaseService<UserProfile> _userTestBaseService;
        public UserFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _userService = new UserService(session);
            _organizationFactory = new OrganizationFactory(this, session);
            _campusFactory = new CampusFactory(this, session);
            _branchFactory = new BranchFactory(this, session);
            _userTestBaseService=new TestBaseService<UserProfile>(session);
        }

        #endregion

        #region Create

        public UserFactory Create()
        {
            Object = new UserProfile()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid()+"test user name",
                CreateBy = 1,
                ModifyBy = 1,
                Status = UserProfile.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        #region with properties old code
        //public UserFactory WithProperties(Organization organization, Program program, Session session, Branch branch, Campus campus, string batchDays, DateTime? startTime = null, DateTime? endTime = null, int? capacity = null, int? versionOfStudy = null, int? gender = null)
        //{
        //    if (organization != null)
        //        Object.Organization = organization;
        //    if (program != null)
        //        Object.Program = program;
        //    if (session != null)
        //        Object.Session = session;
        //    if (branch != null)
        //        Object.Branch = branch;
        //    if (campus != null)
        //        Object.Campus = campus;
        //    Object.Days = batchDays;
        //    Object.StartTime = startTime;
        //    Object.EndTime = endTime;
        //    Object.Capacity = capacity;
        //    if (gender != null)
        //    {
        //        Object.Gender = gender.Value;
        //    }
        //    if (versionOfStudy != null)
        //    {
        //        Object.VersionOfStudy = versionOfStudy.Value;
        //    }
        //    return this;
        //} 
        #endregion
        public UserFactory WithBranch()
        {
            var organizationFactory =
                _organizationFactory.Create().WithRequiredProperties()
                    .Persist();
            var branchFactory =
                _branchFactory.Create().WithOrganization(organizationFactory.Object).WithNameAndRank(Guid.NewGuid() + "testBranchName", 1).WithCode(Guid.NewGuid() + "testBranchCode")
                    .Persist();
            Object.Branch = branchFactory.Object;
            return this;
        }

        public UserFactory WithCampus()
        {
            var campusFactory =
                _campusFactory.Create()
                    .WithBranch(Object.Branch)
                    .WithNameAndRank(Guid.NewGuid() + "testCampusName", 1)
                    .WithCampusRoomList(new List<CampusRoom>()
                    {
                        new CampusRoom()
                        {
                            RoomNo = "101",
                            ClassCapacity = 100,
                            ExamCapacity = 100
                        }
                    })
                    .Persist();
            Object.Campus = campusFactory.Object;
            return this;
        }

        public UserFactory WithBranch(Branch branch)
        {
            Object.Branch = branch;
            return this;
        }

        public UserFactory WithCampus(Campus campus)
        {
            Object.Campus = campus;
            return this;
        }

        //public BatchFactory CreateMore(int numberOfcampus, IList<Branch> branchList, List<CampusRoom> objList)
        //{
        //    foreach (var br in branchList)
        //    {
        //        for (int i = 0; i < numberOfcampus; i++)
        //        {
        //            var campus = new Batch()
        //            {
        //                Id = 0,
        //                BusinessId = GetPcUserName(),
        //                Name = "_" + i,
        //                Rank = i,
        //                CreateBy = 1,
        //                ModifyBy = 1,
        //                Status = Batch.EntityStatus.Active
        //            };
        //            ObjectList.Add(campus);
        //        }
        //    }
        //    return this;
        //}
        
        public UserFactory CreateMore(int numberOfuser)
        {
            var organizationFactory =
                _organizationFactory.Create().WithRequiredProperties()
                    .Persist();
            var branchFactory =
                _branchFactory.Create().WithOrganization(organizationFactory.Object).WithNameAndRank(Guid.NewGuid() + "testBranchName", 1).WithCode(Guid.NewGuid() + "testBranchCode")
                    .Persist();
            var campusFactory =
                _campusFactory.Create()
                    .WithBranch(branchFactory.Object)
                    .WithNameAndRank(Guid.NewGuid() + "testCampusName", 1)
                    .WithCampusRoomList(new List<CampusRoom>()
                    {
                        new CampusRoom()
                        {
                            RoomNo = "101",
                            ClassCapacity = 100,
                            ExamCapacity = 100
                        }
                    })
                    .Persist();
            for (int i = 0; i < numberOfuser; i++)
            {
                var user = new UserProfile()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = "_" + i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Batch.EntityStatus.Active,
                    Branch = branchFactory.Object,
                    Campus = campusFactory.Object
                };
                ObjectList.Add(user);
            }
            return this;
        }

        public UserFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _userService.Save(item, new Images());
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _userService.Save(item, new Images());
                    }
                }
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void Cleanup()
        {
            DeleteAll();
            _organizationFactory.Cleanup();
            if (_campusFactory.Object != null && _campusFactory.Object.CampusRooms != null && _campusFactory.Object.CampusRooms.Count > 0)
                _userTestBaseService.DeleteCampusRoom(_campusFactory.Object.CampusRooms.Select(x => x.Id).ToList());
            _campusFactory.Cleanup();
            _branchFactory.Cleanup();

        }
        #endregion

    }
}
