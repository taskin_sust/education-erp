﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.ItemTest
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "Item")]
    public class ItemSaveTest : ItemBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Save_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _itemService.SaveOrUpdate(null));

            Assert.Throws<NullObjectException>(() => _itemService.SaveOrUpdate(new List<Item>()));
        }

        [Fact]
        public void Should_Return_Save_Successfully()
        {
            _itemFactory.Create().WithItemGroup().WithOrganization().Persist();
            Item item = _itemService.LoadById(_itemFactory.Object.Id);
            Assert.NotNull(item);
            Assert.True(item.Id > 0);
        }

        #endregion

        #region Business Logic

        [Theory]
        [InlineData("Test_Name", 0, ItemUnit.Feet, CostBearer.Corporate)]
        [InlineData("Test_Name", ItemType.CommonItem, 0, CostBearer.Corporate)]
        [InlineData("Test_Name", ItemType.CommonItem, ItemUnit.Feet, 0)]
        public void Should_be_assigned_different_values(string name, int itemType, int itemUnit, int costBearer)
        {
            _itemFactory.CreateWith(0, name, itemType, itemUnit, costBearer).WithItemGroup().WithOrganization();
            Assert.Throws<InvalidDataException>(() => _itemService.SaveOrUpdate(new List<Item>() { _itemFactory.Object }));
        }

        [Fact]
        public void Should_Return_Save_Check_Duplicate_Name_Entry()
        {
            var itemFactory1 = _itemFactory.Create().WithItemGroup().WithOrganization().Persist();

            var itemFactory2 = _itemFactory.CreateWith(0, itemFactory1.Object.Name, itemFactory1.Object.ItemType, itemFactory1.Object.ItemUnit,
                itemFactory1.Object.CostBearer).WithOrganization().WithItemGroup();

            Assert.Throws<DuplicateEntryException>(() => _itemService.SaveOrUpdate(new List<Item>() { itemFactory2.Object }));
        }

        [Fact]
        public void Should_Return_Null_Exception_For_Empty_ItemGroup()
        {
            _itemFactory.Create().WithOrganization();
            Assert.Throws<InvalidDataException>(() => _itemService.SaveOrUpdate(new List<Item>() { _itemFactory.Object }));
        }

        [Fact]
        public void Should_Return_Message_Exception_For_Empty_Item()
        {
            _itemFactory.CreateWith(0, "", (int)ItemType.CommonItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization();
            Assert.Throws<NullObjectException>(() => _itemService.SaveOrUpdate(_itemFactory.ObjectList));
        }

        #endregion
    }
}
