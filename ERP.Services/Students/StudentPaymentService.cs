﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Sms;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentPaymentService : IBaseService
    {
        #region Operational Function

        /// <summary>
        /// 19-01-2015 Current Batch Added
        /// </summary>
        /// <param name="studentPayment"></param>
        /// <param name="stdProId"></param>
        /// <returns></returns>
        bool IsPaymentProcessSuccess(StudentPayment studentPayment, string stdProId);
        bool IsCourseCancelledSuccess(IList<StudentCourseDetail> studentCourseDetails, StudentPayment studentPayment);
        bool IsPaymentProcessSuccessForAddingCourse(List<StudentCourseDetail> studentCourseDetailList, StudentPayment studentPayment, string stdProRoll, IList<StudentCourseDetail> removalList);

        void Update(IList<long> paymentIdList, long userCampusId, long userId, long currentUserId, long rcampusId, DateTime date, decimal totalNowReceived, decimal totalNowCashBack, decimal cashReceive, decimal bKashReceive, decimal voucherReceive, decimal iouReceive);
        #endregion

        #region Single Instances Loading Function
        StudentPayment LoadLastOrDefaultPayment(long studentProgramId);
        StudentPayment LoadByStudentProgram(StudentProgram studentProgram);
        StudentPayment OrganizeObjectForDuePayment(decimal recavailAmount, decimal recvdAmount, decimal sDis, decimal nRec, string referrerenceNote, string paymentMethod, Referrer referrerObj, string[] recAmountDisAmount, string p);
        StudentPayment LoadById(long id);
        StudentPayment OrganizeObjectUsedAddCourse(decimal recavailAmount, decimal recvdAmount, decimal sDis, decimal nRec, string referrerenceNote, string paymentMethod, Referrer referrerObj, string[] recAmountDisAmount
            , string p, List<StudentCourseDetail> studentCourseDetailList);
        StudentPayment OrganizeObjectForNewAdmission(decimal recavailAmount, decimal recvdAmount, decimal sDis, decimal nRec, string referrerenceNote, string paymentMethod, Referrer referrerObj, string[] recAmountDisAmount, string stdPId);
        StudentPayment OrganizeObjectForNewAdmissionForFirstTimeOnly(decimal recavailAmount, decimal recvdAmount, decimal sDis, decimal nRec, string referrerenceNote, string paymentMethod, Referrer referrerObj, string[] recAmountDisAmount);
        StudentPayment GetLastPaymentWithInBlockingTime(StudentPayment studentPayment, string studentProgramId, int duplicatePaymentBlockingTimeInMinute);
        #endregion

        #region List Loading Function
        IList<StudentPayment> GetByStudentProgram(StudentProgram studentProgram);
        IList<StudentDueListDto> LoadDueList(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, DateTime tillDate, string[] batchDays, string[] batchTime, int start, int length, string orderBy, string orderDir);
        //x
        IList<StudentPayment> GetTransactionReport(List<UserMenu> userMenu, long[] organizationId, long[] programId, long[] branchId, long[] sessionId, long[] campusId, IList<UserProfile> users, long[] userIds, DateTime datef, 
            DateTime datet, int paymentMethod,int start, int length);

        IList<StudentPayment> GetMyTransactionReport(long currentUserId, long[] branchId, List<long> authorizedBranches, long[] campusId, long[] authorizedCampuses, DateTime date, int? start = null, int? length = null);
        IList<StudentPayment> GetTransaction(long userId, DateTime toDate, bool isPrev = true);   
        
        IList<StudentPaymentReceiveByCampus> LoadTransactionReceiveReportList(IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet, int start, int length);
        List<CollectionSummaryReportDto> LoadCollectionSummaryReport(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string dateFrom, string dateTo);
        List<DueSummaryReportDto> LoadDueSummaryReport(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string dateFrom, string dateTo);
        List<CollectionVDueReportDto> LoadCollectionVDueReport(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string dateTill);
        List<BranchWiseTransactionReportDto> BranchWiseTransactionReportDtoList(List<UserMenu> userMenu, long organizationId, long branchId, List<long> campusIdList, int programBranchWiseType, int dailySummaryType, string dateFrom, string dateTo, int reportType);
        List<BranchWiseTransactionReportDueDto> BranchWiseTransactionReportDueDtoList(List<UserMenu> userMenu, long organizationId, long branchId, List<long> campusIds, int programBranchWiseType, int dailySummaryType, string dateFrom, string dateTo, int reportType);
        List<SettledComparisonReportDto> SettledComparisonReport(List<UserMenu> userMenu, long organizationId, long programId, string dateFrom, string dateTo);

        List<TransactionSummaryReportDto> TransactioSummarynReportDtoList(List<UserMenu> userMenu, long organizationId, long branchId, List<long> programIds, List<long> campusIds, int method, int dailySummaryType, string dateFrom, string dateTo);
        
        #endregion

        #region Others Function
        bool isDuplicateTransicationFound(string[] tranArray);
        int GetAllStudentPaymentListCountBeforeAParticularPayment(StudentPayment studentPayment);
        int GetTransactionReportCount(List<UserMenu> userMenu, long[] organizationId, long[] programId, long[] branchId, long[] sessionId, long[] campusId, IList<UserProfile> users, long[] userIds, DateTime datef, 
            DateTime datet, int paymentMethod);

        //int GetTransactionReportCount(long[] sessionId, long[] programId, long[] branchId, long[] authorizedPrograms, long[] authorizedSessions, List<long> authorizedBranches, long[] authorizedCampuses, long[] campusId,
        //     IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet, int paymentMethod);
        int GetTransactionReceiveReportCount(IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet);
        int GetMyTransactionReportCount(long currentUserId, long[] branchId, List<long> authorizedBranchIdList, long[] campusId, long[] authorizedCampusIdList, DateTime date);
        int LoadDueListCount(long programId, long sessionId, long[] brances, long[] campusId, long[] batchName, long[] course, DateTime tillDate, string[] batchDays, string[] batchTime);
        int TotalpayAbleAmount(long stProgramId);
        decimal GetTotalCollectionAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo);
        decimal GetTotalRefundAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo);
        #endregion

       
    }
    public class StudentPaymentService : BaseService, IStudentPaymentService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentPaymentDao _studentPaymentDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IStudentCourseDetailDao _studentCourseDetailDao;
        private readonly ICampusDao _campusDao;
        private readonly IUserProfileDao _userProfileDao;
        private readonly IStudentPaymentReceiveByCampusDao _studentPaymentReceiveByCampusDao;
        private readonly ISmsTypeDao _smsTypeDao;
        private readonly ISmsHistoryDao _smsHistoryDao;
        private readonly ISmsMaskDao _smsMaskDao;
        private readonly ICommonHelper _commonHelper;

        public StudentPaymentService(ISession session)
        {
            Session = session;
            _studentPaymentDao = new StudentPaymentDao() { Session = Session };
            _studentProgramDao = new StudentProgramDao() { Session = Session };
            _studentCourseDetailDao = new StudentCourseDetailDao() { Session = Session };
            _campusDao = new CampusDao() { Session = Session };
            _userProfileDao = new UserProfileDao() { Session = Session };
            _studentPaymentReceiveByCampusDao = new StudentPaymentReceiveByCampusDao() { Session = Session };
            _smsTypeDao = new SmsTypeDao() { Session = Session };
            _smsHistoryDao = new SmsHistoryDao(){Session = Session};
            _smsMaskDao = new SmsMaskDao() { Session = Session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentPayment"></param>
        /// <param name="stdProId"></param>
        /// <returns></returns>
        public bool IsPaymentProcessSuccess(StudentPayment studentPayment, string stdProId)
        {
            try
            {
                var stdProgram = _studentProgramDao.GetStudentProgram(stdProId);
                var batch = stdProgram.Batch;
                var mrNo = GetMrNumber(stdProgram.Batch.Branch.Code, stdProgram.Batch.Session.Code, studentPayment.PaymentType);
                //studentPayment.CourseFees = 
                if (!String.IsNullOrEmpty(mrNo))
                {
                    studentPayment.ReceiptNo = mrNo;
                    studentPayment.StudentProgram = stdProgram;
                    studentPayment.Batch = batch;
                }
                using (var trans = Session.BeginTransaction())


                {
                    try
                    {
                        _studentPaymentDao.Save(studentPayment);
                        trans.Commit();
                        Session.Clear();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        _logger.Error(ex);
                        return false;
                        //throw;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
                //throw;
            }
        }

        public bool IsCourseCancelledSuccess(IList<StudentCourseDetail> studentCourseDetails, StudentPayment studentPayment)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        var crNo = GetCrNumber(studentPayment.StudentProgram.Batch.Branch.Code, studentPayment.StudentProgram.Batch.Session.Code, studentPayment.PaymentType);

                        var batch = studentPayment.StudentProgram.Batch;
                        if (crNo != "")
                        {
                            studentPayment.ReceiptNo = crNo;
                            studentPayment.Batch = batch;
                            //studentPayment.PaymentType = PaymentType.CancellationReceipt;
                            Session.Save(studentPayment);
                            foreach (var studentCourseDetail in studentCourseDetails)
                            {
                                studentCourseDetail.Status = StudentCourseDetail.EntityStatus.Delete;
                                Session.Update(studentCourseDetail);
                            }
                            trans.Commit();
                            Session.Clear();
                            return true;
                        }
                        return false;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        _logger.Error(ex);
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;

            }
        }

        public bool IsPaymentProcessSuccessForAddingCourse(List<StudentCourseDetail> studentCourseDetailList, StudentPayment studentPayment, string stdProRoll, IList<StudentCourseDetail> removalList)
        {
            try
            {
                var stdProgram = _studentProgramDao.GetStudentProgram(stdProRoll, true);
                var batch = stdProgram.Batch;
                var mrNo = GetMrNumber(stdProgram.Batch.Branch.Code, stdProgram.Batch.Session.Code, studentPayment.PaymentType);
                if (!String.IsNullOrEmpty(mrNo))
                {
                    studentPayment.ReceiptNo = mrNo;
                    studentPayment.StudentProgram = stdProgram;
                    studentPayment.Batch = batch;
                }
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        if (stdProgram.IsPolitical == false && studentPayment.Referrer != null && studentPayment.Referrer.Code == "04")/*FOR POLITICAL HARD CODED*/
                        {
                            stdProgram.IsPolitical = true;
                            _studentProgramDao.Update(stdProgram);
                        }
                        foreach (var studentCourseDetail in studentCourseDetailList)
                        {
                            _studentCourseDetailDao.Save(studentCourseDetail);
                        }
                        if (removalList.Count > 0)
                        {
                            foreach (var studentCourseDetail in removalList)
                            {
                                studentCourseDetail.Status = StudentCourseDetail.EntityStatus.Delete;
                                Session.Update(studentCourseDetail);
                            }
                        }                      
                        //SetCourseFeeAndNoteFromCourseSubject(studentPayment, studentCourseDetailList);
                        _studentPaymentDao.Save(studentPayment);
                        trans.Commit();
                        Session.Clear();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        _logger.Error(ex);
                        return false;
                        //throw;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
                //throw;
            }
        }

        public void Update(IList<long> paymentIdList, long userCampusId, long userId, long currentUserId, long rcampusId, DateTime date, decimal totalNowReceived, decimal totalNowCashBack, decimal cashReceive, decimal bKashReceive, decimal voucherReceive, decimal iouReceive)
        {
            ITransaction transaction = null;
            try
            {
                Campus rcampus = _campusDao.LoadById(rcampusId);
                Campus fcampus = _campusDao.LoadById(userCampusId);

                UserProfile user = _userProfileDao.LoadById(userId);
                UserProfile currentUser = _userProfileDao.GetByAspNetUser(currentUserId);

                List<StudentPayment> spayList = new List<StudentPayment>();

                if (paymentIdList.Any() && rcampus != null)
                {
                    foreach (var payId in paymentIdList)
                    {
                        StudentPayment spay = _studentPaymentDao.LoadById(payId);
                        spay.CampusReceiveFrom = rcampus;
                        spay.CampusReceiveBy = currentUserId;
                        spay.CampusReceiveDate = DateTime.Now;
                        spayList.Add(spay);
                        //_studentPaymentDao.Update(spay);
                    }
                }

                using (transaction = Session.BeginTransaction())
                {

                    //StudentPayment spay = _studentPaymentDao.LoadById();

                    foreach (StudentPayment spay in spayList)
                    {
                        _studentPaymentDao.Update(spay);
                    }

                    var studentPaymentReceiveByCampus = new StudentPaymentReceiveByCampus();
                    studentPaymentReceiveByCampus.NetReceive = (totalNowReceived - totalNowCashBack);
                    studentPaymentReceiveByCampus.ReceiveAmount = totalNowReceived;
                    studentPaymentReceiveByCampus.CashBackAmount = totalNowCashBack;
                    studentPaymentReceiveByCampus.ReceiveFromUser = user;
                    studentPaymentReceiveByCampus.ReceiveByUser = currentUser;
                    studentPaymentReceiveByCampus.ReceiveFromCampus = fcampus;
                    studentPaymentReceiveByCampus.ReceiveByCampus = rcampus;
                    studentPaymentReceiveByCampus.ReceiveFromDate = date;
                    studentPaymentReceiveByCampus.ReceiveDate = DateTime.Now;
                    studentPaymentReceiveByCampus.CashReceive = cashReceive;
                    studentPaymentReceiveByCampus.BKashReceive = bKashReceive;
                    studentPaymentReceiveByCampus.VoucherReceive = voucherReceive;
                    studentPaymentReceiveByCampus.IouReceive = iouReceive;

                    _studentPaymentReceiveByCampusDao.Save(studentPaymentReceiveByCampus);
                    var smsTypeId = 12;
                    var smsType = _smsTypeDao.LoadByTypeName("Branch Receive");
                    if (smsType != null)
                    {
                        smsTypeId = Convert.ToInt32(smsType.Id);
                    }
                    var userPhone = CheckMobileNumber(user.AspNetUser.PhoneNumber);

                    var o = user.Branch.Organization;
                    var mask = "";
                    if (o.SmsMasks.Any())
                    {
                        var smsMask = o.SmsMasks.FirstOrDefault();
                        mask = smsMask.Name;
                    }
                    if (userPhone != null)
                    {
                        // currentUser
                        var userNickName = user.NickName;
                        var netReceive = (totalNowReceived - totalNowCashBack);
                        var message = "Received Tk. " + String.Format("{0:0}", (totalNowReceived - totalNowCashBack)) + " (" + LongToWords(Convert.ToInt64(totalNowReceived - totalNowCashBack)) + ") Only On " + DateTime.Now.ToString("dd.MM.yyyy at h:mm:ss tt") + " By " + currentUser.NickName;
                        //var message = "Received Tk. " + (totalNowReceived - totalNowCashBack) + " () Only On " + DateTime.Now.ToShortDateString();
                        var smsHistory = new SmsHistory();

                        smsHistory.ReceiverNumber = "88" + userPhone;
                        smsHistory.Sms = message;
                        smsHistory.Organization = user.Branch.Organization;
                        smsHistory.Mask = mask;
                        smsHistory.Type = smsTypeId;
                        smsHistory.Status = SmsHistory.EntityStatus.Pending;
                        smsHistory.CreateBy = currentUser.Id;
                        smsHistory.ModifyBy = currentUser.Id;
                        smsHistory.Priority = 0;
                        _smsHistoryDao.Save(smsHistory);
                    }


                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public StudentPayment OrganizeObjectForNewAdmission(decimal recavailAmount, decimal recvdAmount, decimal sDis, decimal nRec, string referrerenceNote, string paymentMethod
          , Referrer referrerObj, string[] recAmountDisAmount, string stdPId)
        {
            try
            {
                var studentPayment = new StudentPayment();
                studentPayment.PayableAmount = recavailAmount;
                studentPayment.ReceivedAmount = recvdAmount;
                var studentProgram = _studentProgramDao.GetStudentProgram(stdPId.Trim());
                StudentPayment lastStudentPaymentTransication = _studentPaymentDao.LoadByStudentProgram(studentProgram);
                if (lastStudentPaymentTransication == null)
                {
                    studentPayment.DiscountAmount = Convert.ToDecimal(recAmountDisAmount[1]);
                }

                studentPayment.SpDiscountAmount = sDis;
                studentPayment.SpReferenceNote = String.IsNullOrEmpty(referrerenceNote) ? "" : referrerenceNote;
                studentPayment.DueAmount = nRec - recvdAmount;
                studentPayment.PaymentMethod = Convert.ToInt32(paymentMethod.Trim());
                studentPayment.ReceivedDate = DateTime.Now;
                //studentPayment.NextReceivedDate = nextRDate;
                studentPayment.Referrer = referrerObj;
                //studentPayment.StudentProgram = stdProgram;
                studentPayment.Status = StudentPayment.EntityStatus.Active;
                //studentPayment.PaymentStatus = studentPayment.DueAmount == 0 ? PaymentStatus.Paid: PaymentStatus.Due; //TODO: have to check

                SetCourseFeeAndNoteForNewAdmission(studentPayment, stdPId);

                return studentPayment;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public StudentPayment LoadByStudentProgram(StudentProgram studentProgram)
        {
            try
            {
                return _studentPaymentDao.LoadByStudentProgram(studentProgram);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public StudentPayment OrganizeObjectForDuePayment(decimal recavailAmount, decimal recvdAmount, decimal sDis, decimal nRec,
         string referrerenceNote,
         string paymentMethod, Referrer referrerObj, string[] recAmountDisAmount, string stdPId)
        {
            try
            {
                var studentPayment = new StudentPayment();
                //var stdProgram = _studentProgramService.LoadById(Convert.ToInt64(stdPId));
                studentPayment.PayableAmount = recavailAmount;
                studentPayment.ReceivedAmount = recvdAmount;


                var studentProgram = _studentProgramDao.GetStudentProgram(stdPId.Trim());
                StudentPayment lastStudentPaymentTransication = _studentPaymentDao.LoadByStudentProgram(studentProgram);
                if (lastStudentPaymentTransication == null)
                {
                    studentPayment.DiscountAmount = Convert.ToDecimal(recAmountDisAmount[1]);
                }

                studentPayment.SpDiscountAmount = sDis;
                studentPayment.SpReferenceNote = String.IsNullOrEmpty(referrerenceNote) ? "" : referrerenceNote;
                studentPayment.DueAmount = nRec - recvdAmount;
                studentPayment.PaymentMethod = Convert.ToInt32(paymentMethod.Trim());
                studentPayment.ReceivedDate = DateTime.Now;
                //studentPayment.NextReceivedDate = nextRDate;
                studentPayment.Referrer = referrerObj;
                //studentPayment.StudentProgram = stdProgram;
                studentPayment.Status = StudentPayment.EntityStatus.Active;
                //studentPayment.PaymentStatus = studentPayment.DueAmount == 0 ? PaymentStatus.Paid: PaymentStatus.Due; //TODO: have to check

                SetCourseFeeAndNoteForDuePayment(studentPayment, stdPId);

                return studentPayment;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public StudentPayment LoadById(long id)
        {
            try
            {
                return _studentPaymentDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public StudentPayment LoadLastOrDefaultPayment(long studentProgramId)
        {
            try
            {
                return _studentPaymentDao.LoadLastOrDefaultPayment(studentProgramId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public StudentPayment OrganizeObjectUsedAddCourse(decimal recavailAmount, decimal recvdAmount, decimal sDis, decimal nRec,
        string referrerenceNote, string paymentMethod, Referrer referrerObj, string[] recAmountDisAmount, string stdPId, List<StudentCourseDetail> studentCourseDetailList)
        {
            try
            {
                var studentPayment = new StudentPayment();
                //var stdProgram = _studentProgramService.LoadById(Convert.ToInt64(stdPId));
                studentPayment.PayableAmount = recavailAmount;
                studentPayment.ReceivedAmount = recvdAmount;


                var studentProgram = _studentProgramDao.GetStudentProgram(stdPId.Trim());
                //I (gmnazmul) did not understand why this checking was applied
                //For this checking no discount (offer discount) was saved to the payment row from the second time.
                //StudentPayment lastStudentPaymentTransication = _studentPaymentDao.LoadByStudentProgram(studentProgram);
                //if (lastStudentPaymentTransication == null)
                //{
                if (string.IsNullOrEmpty(recAmountDisAmount[1]) == false)
                {
                    studentPayment.DiscountAmount = Convert.ToDecimal(recAmountDisAmount[1]);
                }
                //}

                foreach (var cs in studentCourseDetailList)
                {
                    studentPayment.CourseSubjectList.Add(cs.CourseSubject);
                }
                studentPayment.PaymentType = PaymentType.MoneyReceipt;
                studentPayment.SpDiscountAmount = sDis;
                studentPayment.SpReferenceNote = String.IsNullOrEmpty(referrerenceNote) ? "" : referrerenceNote;
                studentPayment.DueAmount = nRec - recvdAmount;
                studentPayment.PaymentMethod = Convert.ToInt32(paymentMethod.Trim());
                studentPayment.ReceivedDate = DateTime.Now;
                //studentPayment.NextReceivedDate = nextRDate;
                studentPayment.Referrer = referrerObj;
                //studentPayment.StudentProgram = stdProgram;
                studentPayment.Status = StudentPayment.EntityStatus.Active;
                //studentPayment.PaymentStatus = studentPayment.DueAmount == 0 ? PaymentStatus.Paid: PaymentStatus.Due; //TODO: have to check

                SetCourseFeeAndNoteUsedAddCourse(studentPayment, studentCourseDetailList, stdPId);

                return studentPayment;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        //x
        public IList<StudentPayment> GetTransactionReport(List<UserMenu> userMenu, long[] organizationId, long[] programId, long[] branchId, long[] sessionId,
            long[] campusId, IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet, int paymentMethod, int start,int length)
        {
            try
            {
                //var aspnetUsers = users.Select(u => u.AspNetUser).ToList();

                //var organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationId.Contains(SelectionType.SelelectAll) ? null : organizationId.ToList());
                //var programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, programId.Contains(SelectionType.SelelectAll) ? null : programId.ToList());
                //var branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchId.Contains(SelectionType.SelelectAll) ? null : branchId.ToList());

                return _studentPaymentDao.GetTransactionReport(
                    organizationId.ToList(), programId.ToList(), branchId.ToList(), sessionId, campusId, null, userIds, datef, datet, paymentMethod, start, length);
                //return _studentPaymentDao.GetTransactionReport(sessionId, programId, branchId, authorizedPrograms, authorizedSessions, authorizedBranches, authorizedCampuses, campusId, aspnetUsers, userIds, datef, datet, paymentMethod, start, length);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

       

        public IList<StudentPaymentReceiveByCampus> LoadTransactionReceiveReportList(IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet, int start, int length)
        {
            try
            {
                return _studentPaymentReceiveByCampusDao.LoadTransactionReceiveReportList(users, userIds, datef, datet, start, length);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        //x
        

        public IList<StudentPayment> GetMyTransactionReport(long currentUserId, long[] branchId, List<long> authorizedBranches, long[] campusId, long[] authorizedCampuses, DateTime date, int? start = null, int? length = null)
        {
            try
            {
                return _studentPaymentDao.GetMyTransactionReport(currentUserId, branchId, authorizedBranches, campusId, authorizedCampuses, date, start, length);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public IList<StudentPayment> GetTransaction(long userId, DateTime toDate, bool isPrev = true)
        {
            try
            {
                return _studentPaymentDao.GetTransaction(userId, toDate, isPrev);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        } 
        public List<CollectionSummaryReportDto> LoadCollectionSummaryReport(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string dateFrom,
            string dateTo)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null);


                var collectionSummaryReport = _studentPaymentDao.LoadCollectionSummaryReport(programIdList, branchIdList, sessionId, dateFrom, dateTo);
                return collectionSummaryReport;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<DueSummaryReportDto> LoadDueSummaryReport(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string dateFrom,
           string dateTo)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null);


                List<DueSummaryReportDto> dueSummaryReport = _studentPaymentDao.LoadDueSummaryReport(programIdList, branchIdList, sessionId, dateFrom, dateTo);
                return dueSummaryReport;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<CollectionVDueReportDto> LoadCollectionVDueReport(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string dateTill)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null);


                List<CollectionVDueReportDto> collectionVDueReport = _studentPaymentDao.LoadCollectionVDueReport(programIdList, branchIdList, sessionId, dateTill);
                return collectionVDueReport;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<BranchWiseTransactionReportDto> BranchWiseTransactionReportDtoList(List<UserMenu> userMenu, long organizationId, long branchId, List<long> campusIdList,
            int programBranchWiseType, int dailySummaryType, string dateFrom, string dateTo, int reportType)
        {

            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, _commonHelper.ConvertIdToList(branchId));

                //List<long> authCampusIdList = new List<long>();
                //if (campusIdList.Contains(SelectionType.SelelectAll))
                //{
                //    List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, _commonHelper.ConvertIdToList(branchId));
                //    List<Campus> campusList = _campusDao.LoadAuthorizeCampus(programIdList, branchIdList).ToList();
                //    if (campusList.Any())
                //    {
                //        foreach (Campus campus in campusList)
                //        {
                            
                //        }   
                //    }
                //}
                    
                    //AuthHelper.LoadBranchIdList(userMenu, organizationIdList, _commonHelper.ConvertIdToList(programId));


                List<BranchWiseTransactionReportDto> branchWiseTransactionReportList = _studentPaymentDao.BranchWiseTransactionReportDtoList(branchIdList, campusIdList, programBranchWiseType, dailySummaryType, dateFrom, dateTo, reportType);
                return branchWiseTransactionReportList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public List<BranchWiseTransactionReportDueDto> BranchWiseTransactionReportDueDtoList(List<UserMenu> userMenu, long organizationId, long branchId, List<long> campusIdList,
            int programBranchWiseType, int dailySummaryType, string dateFrom, string dateTo, int reportType)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, _commonHelper.ConvertIdToList(branchId));

                //List<long> authCampusIdList = new List<long>();
                //if (campusIdList.Contains(SelectionType.SelelectAll))
                //{
                //    List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, _commonHelper.ConvertIdToList(branchId));
                //    List<Campus> campusList = _campusDao.LoadAuthorizeCampus(programIdList, branchIdList).ToList();
                //    if (campusList.Any())
                //    {
                //        foreach (Campus campus in campusList)
                //        {

                //        }   
                //    }
                //}

                //AuthHelper.LoadBranchIdList(userMenu, organizationIdList, _commonHelper.ConvertIdToList(programId));


                List<BranchWiseTransactionReportDueDto> branchWiseTransactionReportDueList = _studentPaymentDao.BranchWiseTransactionReportDueDtoList(branchIdList, campusIdList, programBranchWiseType, dailySummaryType, dateFrom, dateTo, reportType);
                return branchWiseTransactionReportDueList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public List<SettledComparisonReportDto> SettledComparisonReport(List<UserMenu> userMenu, long organizationId, long programId, string dateFrom, string dateTo)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, (programId != 0) ? _commonHelper.ConvertIdToList(programId) : null);


                List<SettledComparisonReportDto> settledComparisonReport = _studentPaymentDao.SettledComparisonReport(programIdList, branchIdList, dateFrom, dateTo);
                return settledComparisonReport;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<TransactionSummaryReportDto> TransactioSummarynReportDtoList(List<UserMenu> userMenu, long organizationId, long branchId, List<long> programIds, List<long> campusIds, int method, int dailySummaryType, string dateFrom, string dateTo)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, _commonHelper.ConvertIdToList(branchId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, branchIdList, (!programIds.Contains(SelectionType.SelelectAll)) ? programIds : null);

                List<TransactionSummaryReportDto> transactionSummaryReportList = _studentPaymentDao.TransactionSummaryReportDtoList(branchIdList, programIdList, campusIds, method, dailySummaryType, dateFrom, dateTo);
                return transactionSummaryReportList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public StudentPayment OrganizeObjectForNewAdmissionForFirstTimeOnly(decimal recavailAmount, decimal recvdAmount, decimal sDis,
            decimal nRec, string referrerenceNote, string paymentMethod, Referrer referrerObj, string[] recAmountDisAmount)
        {
            try
            {
                var studentPayment = new StudentPayment();
                studentPayment.PayableAmount = recavailAmount;
                studentPayment.ReceivedAmount = recvdAmount;
                studentPayment.DiscountAmount = Convert.ToDecimal(recAmountDisAmount[1]);
                studentPayment.SpDiscountAmount = sDis;
                studentPayment.SpReferenceNote = String.IsNullOrEmpty(referrerenceNote) ? "" : referrerenceNote;
                studentPayment.DueAmount = nRec - recvdAmount;
                studentPayment.PaymentMethod = Convert.ToInt32(paymentMethod.Trim());
                studentPayment.ReceivedDate = DateTime.Now;
                //studentPayment.NextReceivedDate = nextRDate;
                studentPayment.Referrer = referrerObj;
                //studentPayment.StudentProgram = stdProgram;
                studentPayment.Status = StudentPayment.EntityStatus.Active;
                //studentPayment.PaymentStatus = studentPayment.DueAmount == 0 ? PaymentStatus.Paid: PaymentStatus.Due; //TODO: have to check

                //SetCourseFeeAndNoteForNewAdmission(studentPayment, stdPId);

                return studentPayment;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public StudentPayment GetLastPaymentWithInBlockingTime(StudentPayment studentPayment, string studentProgramId,
            int duplicatePaymentBlockingTimeInMinute)
        {
            var payment = _studentPaymentDao.GetLastPaymentWithInBlockingTime(
                studentPayment,
                studentProgramId,
                duplicatePaymentBlockingTimeInMinute
            );
            return payment;
        }

        public bool isDuplicateTransicationFound(string[] tranArray)
        {
            try
            {
                return _studentPaymentDao.isDuplicateTransicationFound(tranArray);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<StudentDueListDto> LoadDueList(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, DateTime tillDate, string[] batchDays, string[] batchTime, int start, int length, string orderBy, string orderDir)
        {
            try
            {
                return _studentPaymentDao.LoadDueList(programId, sessionId, branchId, campusId, batchName, course, tillDate, batchDays, batchTime, start, length, orderBy, orderDir);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<StudentPayment> GetByStudentProgram(StudentProgram studentProgram)
        {
            try
            {
                return _studentPaymentDao.GetByStudentProgram(studentProgram);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function
        private string GetMrNumber(string brCode, string sCode, int paymentType)
        {
            try
            {
                if (!String.IsNullOrEmpty(brCode) && !String.IsNullOrEmpty(sCode))
                {
                    var firstMrCode = "000001";
                    var lastMrCode = brCode + sCode;
                    StudentPayment studentPaymentObj = _studentPaymentDao.LoadBybrCodeAndsCode(lastMrCode, paymentType);
                    if (studentPaymentObj == null)
                    {
                        return lastMrCode + firstMrCode;
                    }

                    var preMrNo = studentPaymentObj.ReceiptNo;
                    var preMrNoLastPart = preMrNo.Substring(4);

                    return lastMrCode + (Convert.ToInt64(preMrNoLastPart) + 1).ToString().PadLeft(6, '0');
                }
                return "";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "";

                //throw;
            }

        }
        private string GetCrNumber(string brCode, string sCode, int paymentType)
        {
            try
            {
                if (!String.IsNullOrEmpty(brCode) && !String.IsNullOrEmpty(sCode))
                {
                    var firstMrCode = "00001";
                    var lastMrCode = brCode + sCode;
                    StudentPayment studentPaymentObj = _studentPaymentDao.LoadBybrCodeAndsCode(lastMrCode, paymentType);
                    if (studentPaymentObj == null)
                    {
                        return lastMrCode + firstMrCode;
                    }

                    var preMrNo = studentPaymentObj.ReceiptNo;
                    var preMrNoLastPart = preMrNo.Substring(4);

                    return lastMrCode + (Convert.ToInt64(preMrNoLastPart) + 1).ToString().PadLeft(5, '0');
                }
                return "";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "";
                //throw;
            }

        }
        public int GetAllStudentPaymentListCountBeforeAParticularPayment(StudentPayment studentPayment)
        {
            try
            {
                return _studentPaymentDao.GetAllStudentPaymentListCountBeforeAParticularPayment(studentPayment);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public int GetTransactionReportCount(List<UserMenu> userMenu, long[] organizationId, long[] programId, long[] branchId, long[] sessionId, long[] campusId, IList<UserProfile> users, long[] userIds, DateTime datef,
            DateTime datet, int paymentMethod)
        {

            //if (!organizationId.Contains(SelectionType.SelelectAll))
            //{
                
            //}
            //var organizationIdList = new List<long>();
            //var branchIdList = new List<long>();
            //if (userMenu != null)
            //{
            //    organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
            //    branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);
            //}
            //var aspnetUsers = users.Select(u => u.AspNetUser).ToList();
          


            //var organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationId.Contains(SelectionType.SelelectAll)?null: organizationId.ToList());
            //var programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, programId.Contains(SelectionType.SelelectAll)?null: programId.ToList());
            //var branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchId.Contains(SelectionType.SelelectAll)?null: branchId.ToList());

            return _studentPaymentDao.GetTransactionReportCount(
                organizationId.ToList(), programId.ToList(), branchId.ToList(), sessionId, campusId, null, userIds, datef, datet, paymentMethod);

        }
        public int GetTransactionReceiveReportCount(IList<UserProfile> users, long[] userIds, DateTime datef, DateTime datet)
        {
            try
            {
                // List<AspNetUser> aspnetUsers = users.Select(u => u.AspNetUser).ToList();
                return _studentPaymentReceiveByCampusDao.GetTransactionReceiveReportCount(users, userIds, datef, datet);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public int GetMyTransactionReportCount(long currentUserId, long[] branchId, List<long> authorizedBranchIdList, long[] campusId, long[] authorizedCampusIdList, DateTime date)
        {
            return _studentPaymentDao.GetMyTransactionReportCount(currentUserId, branchId, authorizedBranchIdList, campusId, authorizedCampusIdList, date);
        }

        public int LoadDueListCount(long programId, long sessionId, long[] brances, long[] campusId, long[] batchName, long[] course,
            DateTime tillDate, string[] batchDays, string[] batchTime)
        {
            try
            {
                return _studentPaymentDao.LoadDueListCount(programId, sessionId, brances, campusId, batchName, course, tillDate, batchDays, batchTime);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int TotalpayAbleAmount(long stProgramId)
        {
            try
            {
                return _studentPaymentDao.TotalpayAbleAmount(stProgramId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public decimal GetTotalCollectionAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return _studentPaymentDao.GetTotalCollectionAmount(sessionId, programId, branchId, campusId, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public decimal GetTotalRefundAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return _studentPaymentDao.GetTotalRefundAmount(sessionId, programId, branchId, campusId, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper Function
        public string LoadPaymentStatusByStudentProgram(long stdProId)
        {
            return _studentPaymentDao.LoadPaymentStatusByStudentProgram(stdProId);
        }
        private void SetCourseFeeAndNoteForNewAdmission(StudentPayment studentPayment, string stdPId)
        {
            Session.Clear();
            var studentProgram = _studentProgramDao.GetStudentProgram(stdPId);
            var count = studentProgram.StudentCourseDetails.Count;

            /*if (studentProgram.DueAmount == null)
            {*/
            decimal totalAmount = 0;
            string refNote = "";

            studentProgram.StudentCourseDetails.ForEach(x =>
            {
                totalAmount += x.CourseSubject.Payment;
            });

            var cd = studentProgram.StudentCourseDetails.ToList();
            var cdGroup = cd.GroupBy(x => x.CourseSubject.Course, (key, group) => new { Key = key, Group = group.Distinct().ToList() });

            foreach (var cg in cdGroup)
            {
                refNote += cg.Key.Name + " : ( ";
                foreach (var cs in cg.Group)
                {
                    refNote += cs.CourseSubject.Subject.ShortName + ", ";
                }
                if (!string.IsNullOrEmpty(refNote))
                {
                    int lastIndex = refNote.LastIndexOf(",");
                    if (lastIndex >= 0)
                        refNote = refNote.Remove(lastIndex);
                }
                refNote += " ), ";
            }

            if (!string.IsNullOrEmpty(refNote))
            {
                int lastIndex = refNote.LastIndexOf(",");
                if (lastIndex >= 0)
                    refNote = refNote.Remove(lastIndex, 1);
            }

            studentPayment.CourseFees = totalAmount;
            studentPayment.Remarks = refNote + " New Admitted.";
            /*}*/

        }
        private void SetCourseFeeAndNoteForDuePayment(StudentPayment studentPayment, string stdPId)
        {
            Session.Clear();
            var studentProgram = _studentProgramDao.GetStudentProgram(stdPId);
            var cdList = studentProgram.StudentCourseDetails.ToList();
            studentPayment.CourseSubjectList = new List<CourseSubject>();
            foreach (var cd in cdList)
            {
                if (cd.Status == StudentCourseDetail.EntityStatus.Active)
                {
                    studentPayment.CourseSubjectList.Add(cd.CourseSubject);
                }
            }
        }
        private void SetCourseFeeAndNoteUsedAddCourse(StudentPayment studentPayment, List<StudentCourseDetail> courseDetails, string stdPId)
        {
            decimal totalAmount = 0;
            string refNote = "";

            courseDetails.ForEach(x =>
            {
                totalAmount += x.CourseSubject.Payment;
            });

            var cdGroup = courseDetails.GroupBy(x => x.CourseSubject.Course, (key, group) => new { Key = key, Group = group.Distinct().ToList() });

            foreach (var cg in cdGroup)
            {
                refNote += cg.Key.Name + " : ( ";
                foreach (var cs in cg.Group)
                {
                    refNote += cs.CourseSubject.Subject.ShortName + ", ";
                }

                if (!string.IsNullOrEmpty(refNote))
                {
                    int lastIndex = refNote.LastIndexOf(",");
                    if (lastIndex >= 0)
                        refNote = refNote.Remove(lastIndex, 1);
                }

                refNote += " ), ";
            }

            if (!string.IsNullOrEmpty(refNote)) { refNote = refNote.Remove(refNote.LastIndexOf(","), 1); }

            studentPayment.CourseFees = totalAmount;
            studentPayment.Remarks = refNote + " Added.";

            var studentProgram = _studentProgramDao.GetStudentProgram(stdPId, true);
            studentProgram.StudentCourseDetails.ForEach(x =>
            {
                if (x.Status == StudentCourseDetail.EntityStatus.Active)
                    studentPayment.CourseSubjectList.Add(x.CourseSubject);
            });
        }

        public static string CheckMobileNumber(string mobile)
        {
            if (mobile != null)
            {
                var regex = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
                var match = regex.Match(mobile);
                if (match.Success == true)
                {
                    switch (mobile.Length)
                    {
                        case 14:
                            mobile = mobile.Substring(3);
                            break;
                        case 13:
                            mobile = mobile.Substring(2);
                            break;
                        case 10:
                            mobile = "0" + mobile;
                            break;
                    }
                }
                else { mobile = null; }
            }
            return mobile;
        }
        private string LongToWords(long inputNum)
        {
            int dig1, dig2, dig3, level = 0, lasttwo, threeDigits;

            string retval = "";
            string x = "";
            string[] ones ={
                "zero",
                "one",
                "two",
                "three",
                "four",
                "five",
                "six",
                "seven",
                "eight",
                "nine",
                "ten",
                "eleven",
                "twelve",
                "thirteen",
                "fourteen",
                "fifteen",
                "sixteen",
                "seventeen",
                "eighteen",
                "nineteen"
              };
            string[] tens ={
                "zero",
                "ten",
                "twenty",
                "thirty",
                "forty",
                "fifty",
                "sixty",
                "seventy",
                "eighty",
                "ninety"
              };
            string[] thou ={
                "",
                "thousand",
                "million",
                "billion",
                "trillion",
                "quadrillion",
                "quintillion"
              };

            bool isNegative = false;
            if (inputNum < 0)
            {
                isNegative = true;
                inputNum *= -1;
            }

            if (inputNum == 0)
                return ("zero");

            string s = inputNum.ToString();

            while (s.Length > 0)
            {
                // Get the three rightmost characters
                x = (s.Length < 3) ? s : s.Substring(s.Length - 3, 3);

                // Separate the three digits
                threeDigits = int.Parse(x);
                lasttwo = threeDigits % 100;
                dig1 = threeDigits / 100;
                dig2 = lasttwo / 10;
                dig3 = (threeDigits % 10);

                // append a "thousand" where appropriate
                if (level > 0 && dig1 + dig2 + dig3 > 0)
                {
                    retval = thou[level] + " " + retval;
                    retval = retval.Trim();
                }

                // check that the last two digits is not a zero
                if (lasttwo > 0)
                {
                    if (lasttwo < 20) // if less than 20, use "ones" only
                        retval = ones[lasttwo] + " " + retval;
                    else // otherwise, use both "tens" and "ones" array
                        retval = tens[dig2] + " " + ones[dig3] + " " + retval;
                }

                // if a hundreds part is there, translate it
                if (dig1 > 0)
                    retval = ones[dig1] + " hundred " + retval;

                s = (s.Length - 3) > 0 ? s.Substring(0, s.Length - 3) : "";
                level++;
            }

            while (retval.IndexOf("  ") > 0)
                retval = retval.Replace("  ", " ");

            retval = retval.Trim();

            if (isNegative)
                retval = "negative " + retval;

            return (retval);
        }
        #endregion
    }
}

