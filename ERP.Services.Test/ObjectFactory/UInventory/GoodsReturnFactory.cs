using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Engine;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class GoodsReturnFactory : ObjectFactoryBase<GoodsReturn>
    {
        #region Object Initialization

        private readonly ItemFactory _itemFactory;
        private readonly GoodsReceiveFactory _goodsReceiveFactory;
        private readonly GoodsReceiveDetailsFactory _goodsReceiveDetailsFactory;
        private readonly GoodsReturnDetailsFactory _goodsReturnDetailsFactory;
        private readonly SupplierItemFactory _supplierItemFactory;
        private readonly IGoodsReturnService _goodsReturnService;
        

        public GoodsReturnFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _itemFactory = new ItemFactory(this, _session);
            _goodsReceiveFactory = new GoodsReceiveFactory(this, _session);
            _goodsReturnDetailsFactory = new GoodsReturnDetailsFactory(this, _session);
            _goodsReceiveDetailsFactory = new GoodsReceiveDetailsFactory(this, _session);
            _supplierItemFactory = new SupplierItemFactory(this, _session);
            _goodsReturnService = new GoodsReturnService(session);
           
        }

        #endregion

        #region Create

        public GoodsReturnFactory Create()
        {
            Object = new GoodsReturn
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = "",
                CreateBy = 1,
                ModifyBy = 1,
                Status = WorkOrder.EntityStatus.Active,
                ReturnedQuantity = 10,
                Remarks = "this is a goods return",
                ReturnStatus = (int)GoodsReturnStatus.Returned,
                GoodsReturnNo = "ORGBRAGRT-000001"
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsReturnFactory WithGoodsReceive()
        {
            //var goodsReceive = _goodsReceiveFactory.Create().WithBranch().WithSupplier().Object;
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Persist(false);
            Object.GoodsReceive = _goodsReceiveFactory.Object;
            return this;
        }

        public GoodsReturnFactory WithGoodsReceive(GoodsReceive goodsReceive)
        {
            Object.GoodsReceive = goodsReceive;
            return this;
        }

        public GoodsReturnFactory WithGoodsReturnDetailsAndGoodsReceive()
        {
            var item=_itemFactory.Create().WithItemGroup().WithOrganization().Persist().Object;
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithPurpose().WithProgram().WithSession().WithItem(item).WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Persist(false);
            Object.GoodsReceive = _goodsReceiveFactory.Object;
            _goodsReturnDetailsFactory.Create(item, Object);
            Object.GoodsReturnDetails = new List<GoodsReturnDetails>()
            {
                _goodsReturnDetailsFactory.Object
            };
            return this;
        }

        public GoodsReturnFactory CreateMore(int num)
        {
            CreateGoodsReturnList(num);
            return this;
        }

        private GoodsReturnFactory CreateGoodsReturnList(int number)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<GoodsReturn>();
            var goodsReceive = _goodsReceiveFactory.Create().WithBranch().WithSupplier().Object;
            var item = _itemFactory.Create().WithItemGroup().WithOrganization(goodsReceive.Branch.Organization).Persist().Object;
            _goodsReturnDetailsFactory.CreateMore(item, number);
            _goodsReceiveDetailsFactory.CreateMore(5, goodsReceive, item);
            goodsReceive.GoodsReceiveDetails=_goodsReceiveDetailsFactory.GetLastCreatedObjectList();
            _goodsReceiveFactory.Persist(false);
            _supplierItemFactory.Create().WithItem(item).WithSupplier(goodsReceive.Supplier).Persist();
            for (int i = 0; i < number; i++)
            {
                var obj = new GoodsReturn()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = "",
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = WorkOrder.EntityStatus.Active,
                    ReturnedQuantity = 1,
                    Remarks = "this is a goods return",
                    ReturnStatus = (int)GoodsReturnStatus.Returned,
                    GoodsReceive = goodsReceive,
                    GoodsReturnNo= "ORGBRAGRT-000001",
                    CreationDate = DateTime.Now,
                    ModificationDate = DateTime.Now
                };
                _goodsReturnDetailsFactory.GetLastCreatedObjectList()[i].GoodsReturn = obj;
                obj.GoodsReturnDetails = new List<GoodsReturnDetails>()
                {
                    _goodsReturnDetailsFactory.GetLastCreatedObjectList()[i]
                };
                //goodsReceive.GoodsReturnList =new List<GoodsReturn>(){obj};
               // obj.GoodsReceive = goodsReceive;
                list.Add(obj);
            }
            ObjectList.AddRange(list);
            return this;
        }


        public GoodsReturnFactory Persist()
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x =>
                {
                    if (x.Id == 0) _goodsReturnService.SaveOrUpdate(x);
                });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x =>
                {
                    if (x.Id == 0) _goodsReturnService.SaveOrUpdate(x);
                });
            }
            return this;
        }

        #endregion

        #region CleanUp

        public void CleanUp()
        {
            DeleteAll();
            _goodsReturnDetailsFactory.CleanUp();
            _supplierItemFactory.CleanUp();
            _itemFactory.CleanUp();
            _goodsReceiveDetailsFactory.CleanUp();
            _goodsReceiveFactory.CleanUp();
            
        }
        
        #endregion
    }
}
