﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BankBranchTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "BankBranch")]
    public class BankBranchCountTest : BankBranchBaseTest
    {
        [Fact]
        public void Bank_Branch_Count_Greater_Than_Zero_Check()
        {
            _bankBranchFactory.CreateMore(100).Persist();
            var bankBranchCount = _bankBranchService.GetBankBranchCount("","","","","","","","","","",BankBranch.EntityStatus.Active.ToString());
            Assert.True(bankBranchCount > 0);
        }

    }
}
