﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Exam
{
    public interface IExamMcqQuestionDao : IBaseDao<ExamMcqQuestion, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        List<ExamMcqQuestion> LoadExamMcqQuestionList(Exams exam, List<int> uniqueSetList);

        IList<ExamMcqQuestion> LoadExamMcqQuestion(long examId, int uniqueSet, int version, int? questionSerial = null);

        IList<ExamMcqQuestion> GetExamMcqQuestionList(int start, int length, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, int? examVersion, string keyword, string uniqueSetNo);

        List<ExamMcqQuestion> GetExamMcqQuestion(long examId, int uniqueSet, long subjectId, int questionSerial);

        #endregion

        #region Others Function

        int GetExamMcqQuestionCount(List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, int? examVersion, string keyword, string uniqueSetNo);

        int GetLastQuestionSerial(long examId, int uniqueSet);

        #endregion
    }
    public class ExamMcqQuestionDao : BaseDao<ExamMcqQuestion, long>, IExamMcqQuestionDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public List<ExamMcqQuestion> GetExamMcqQuestion(long examId, int uniqueSet, long subjectId, int questionSerial)
        {
            ICriteria criteria =
                Session.CreateCriteria<ExamMcqQuestion>()
                    .Add(Restrictions.Not(Restrictions.Eq("Status", ExamMcqQuestion.EntityStatus.Delete)));
            criteria.CreateAlias("Exams", "ex");
            criteria.Add(Restrictions.Eq("ex.Id", examId));
            criteria.Add(Restrictions.Eq("UniqueSet", uniqueSet));
            criteria.Add(Restrictions.Eq("Subject.Id", subjectId));
            criteria.Add(Restrictions.Eq("QuestionSerial", questionSerial));
            return criteria.List<ExamMcqQuestion>().ToList();
        }

        #endregion

        #region List Loading Function

        public IList<ExamMcqQuestion> LoadExamMcqQuestion(long examId, int uniqueSet, int version, int? questionSerial = null)
        {
            ICriteria criteria = GetExamMcqQuestionCriteria(examId, uniqueSet, version, questionSerial);
            return criteria.List<ExamMcqQuestion>();
        }

        public List<ExamMcqQuestion> LoadExamMcqQuestionList(Exams exam, List<int> uniqueSetList)
        {
            ICriteria criteria =
                    Session.CreateCriteria<ExamMcqQuestion>()
                        .Add(Restrictions.Eq("Status", ExamMcqQuestion.EntityStatus.Active));
            criteria.CreateAlias("Exams", "xm").Add(Restrictions.Eq("xm.Status", Organization.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("xm.Id", exam.Id));
            if (exam.IsBanglaVersion)
            {
                criteria.Add(Restrictions.Eq("Version", 1));
            }
            else
            {
                criteria.Add(Restrictions.Eq("Version", 2));
            }
            criteria.Add(Restrictions.In("UniqueSet", uniqueSetList.ToArray()));
            return criteria.List<ExamMcqQuestion>().ToList();
        }

        public IList<ExamMcqQuestion> GetExamMcqQuestionList(int start, int length, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, int? examVersion, string keyword, string uniqueSetNo)
        {
            var transactionReportCriteria = GetTransactionReportCriteria(programIdList, sessionIdList, courseIdList, examVersion, keyword, uniqueSetNo);
            transactionReportCriteria.AddOrder(Order.Desc("Id"));
            return transactionReportCriteria.SetFirstResult(start).SetMaxResults(length).List<ExamMcqQuestion>();
        }
        #endregion

        #region Others Function
        public int GetLastQuestionSerial(long examId, int uniqueSet)
        {
            ICriteria criteria = GetExamMcqQuestionCriteria(examId, uniqueSet);
            criteria.SetProjection(Projections.Max("QuestionSerial"));
            return criteria.UniqueResult<int>();
        }

        public int GetExamMcqQuestionCount(List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, int? examVersion, string keyword, string uniqueSetNo)
        {
            var transactionReportCriteria = GetTransactionReportCriteria(programIdList, sessionIdList, courseIdList, examVersion, keyword, uniqueSetNo);
            var rowCount = transactionReportCriteria.SetProjection(Projections.RowCount()).UniqueResult<int>();
            return rowCount;
        }

        #endregion

        #region Helper Function
        private ICriteria GetExamMcqQuestionCriteria(long examId, int uniqueSet, int? version = null, int? questionSerial = null)
        {
            ICriteria criteria = Session.CreateCriteria<ExamMcqQuestion>().Add(
                Restrictions.Not(Restrictions.Eq("Status", ExamMcqQuestion.EntityStatus.Delete)));
            criteria.CreateAlias("Exams", "e").Add(Restrictions.Eq("e.Status", Exams.EntityStatus.Active)).Add(Restrictions.Eq("e.Id", examId));

            criteria.Add(Restrictions.Eq("UniqueSet", uniqueSet));
            if (version != null && version > 0)
            {
                criteria.Add(Restrictions.Eq("Version", version));
            }
            if (questionSerial != null)
            {
                criteria.Add(Restrictions.Eq("QuestionSerial", questionSerial));
            }
            return criteria;
        }

        private ICriteria GetTransactionReportCriteria(List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, int? examVersion, string keyword, string uniqueSetNo)
        {
            ICriteria criteria = Session.CreateCriteria<ExamMcqQuestion>().Add(Restrictions.Not(Restrictions.Eq("Status", ExamMcqQuestion.EntityStatus.Delete)));
            criteria.CreateAlias("Exams", "ex");
            criteria.CreateAlias("ex.Program", "p");
            criteria.CreateAlias("ex.Session", "s");
            criteria.CreateAlias("ex.Course", "c");
            int uniqueSet;
            if (!String.IsNullOrEmpty(uniqueSetNo) && Int32.TryParse(uniqueSetNo, out uniqueSet))
            {
                if (uniqueSet > 0)
                {
                    criteria.Add(Restrictions.Eq("UniqueSet", uniqueSet));
                }
            }
            if (programIdList != null && programIdList.Any())
            {
                criteria.Add(Restrictions.In("p.Id", programIdList)).Add(Restrictions.Eq("p.Status", BusinessModel.Entity.Administration.Program.EntityStatus.Active));
            }
            if (sessionIdList != null && sessionIdList.Any())
            {
                criteria.Add(Restrictions.In("s.Id", sessionIdList)).Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            }
            if (courseIdList != null && courseIdList.Any())
            {
                criteria.Add(Restrictions.In("c.Id", courseIdList)).Add(Restrictions.Eq("c.Status", BusinessModel.Entity.Administration.Course.EntityStatus.Active));
            }
            if (examVersion != null)
            {
                criteria.Add(Restrictions.Eq("Version", examVersion));
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                var or = Restrictions.Disjunction();
                or.Add(Restrictions.Like("ex.Name", keyword, MatchMode.Anywhere))
                    .Add(Restrictions.Like("QuestionText", keyword, MatchMode.Anywhere))
                    .Add(Restrictions.Like("OptionA", keyword, MatchMode.Anywhere))
                    .Add(Restrictions.Like("OptionB", keyword, MatchMode.Anywhere))
                    .Add(Restrictions.Like("OptionC", keyword, MatchMode.Anywhere))
                    .Add(Restrictions.Like("OptionD", keyword, MatchMode.Anywhere))
                    .Add(Restrictions.Like("OptionE", keyword, MatchMode.Anywhere));
                criteria.Add(or);
            }
            return criteria;
        }

        #endregion


    }
}
