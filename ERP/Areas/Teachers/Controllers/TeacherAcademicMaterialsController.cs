﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using Microsoft.AspNet.Identity;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Teachers;
using UdvashERP.App_code;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Teachers.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Teachers")]
    public class TeacherAcademicMaterialsController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeachersArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly ITeacherService _teacherService;
        private readonly IBranchService _branchService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ITeacherAcademicMaterialService _teacherAcademicMaterialService;
        private readonly ITeacherAcademicMaterialDetailsService _teacherAcademicMaterialDetailsService;
        private readonly IUserService _userService;
        private List<UserMenu> _userMenu;
        public TeacherAcademicMaterialsController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _organizationService = new OrganizationService(session);
                _teacherService = new TeacherService(session);
                _branchService = new BranchService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _teacherAcademicMaterialService = new TeacherAcademicMaterialService(session);
                _teacherAcademicMaterialDetailsService = new TeacherAcademicMaterialDetailsService(session);
                _userService = new UserService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Save operation
        public ActionResult Create()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.CourseId = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.BranchId = new SelectList(new List<Branch>(), "Id", "Name");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(TeacherAcademicMaterialsViewModel teacherAcademicMViewModel)
        {

            try
            {
                if (teacherAcademicMViewModel == null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                if (teacherAcademicMViewModel.TeacherAcademicMaterialDetailsViewModel == null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                Organization organization = _organizationService.LoadById(teacherAcademicMViewModel.OrganizationId);
                if (organization == null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));

                Program program;
                if (teacherAcademicMViewModel.ProgramId == -1)
                {
                    program = null;
                }
                else
                {
                    program = _programService.GetProgram(teacherAcademicMViewModel.ProgramId);
                    if (program == null)
                        return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
                Session session;
                if (teacherAcademicMViewModel.SessionId == -1)
                {
                    session = null;
                }
                else
                {
                    session = _sessionService.LoadById(teacherAcademicMViewModel.SessionId);
                    if (session == null)
                        return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
                var teacherAcademicMaterial = new TeacherAcademicMaterials();
                teacherAcademicMaterial.Organization = organization;
                teacherAcademicMaterial.Program = program;
                teacherAcademicMaterial.Session = session;
                teacherAcademicMaterial.SerialNumber = _teacherAcademicMaterialService.GetSerialNumber();
                teacherAcademicMaterial.SubmissionDate = teacherAcademicMViewModel.SubmissionDate;

                foreach (var detailsViewModel in teacherAcademicMViewModel.TeacherAcademicMaterialDetailsViewModel)
                {
                    var details = new TeacherAcademicMaterialsDetails();
                    details.TeacherAcademicMaterials = teacherAcademicMaterial;

                    details.Status = TeacherAcademicMaterialsDetails.EntityStatus.Active;
                    details.CreateBy =
                        Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    details.ModifyBy =
                        Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());

                    Teacher teacher = _teacherService.GetTeacher(detailsViewModel.TeacherId);
                    if (teacher == null)
                        return Json(new Response(false, "'" + detailsViewModel.TeacherName + "' invalid teacher.Please correct teacher name."));

                    details.Teacher = teacher;//_teacherService.GetTeacher(detailsViewModel.TeacherId);
                    if (detailsViewModel.Branch != -1)
                    {
                        Branch branch = _branchService.GetBranch(detailsViewModel.Branch);
                        if (branch == null)
                            return Json(new Response(false, WebHelper.CommonErrorMessage));
                        details.ForBranch = branch;//_branchService.GetBranch(detailsViewModel.Branch);
                    }
                    else if (detailsViewModel.Branch == -1)
                    {
                        details.ForBranch = null;
                    }
                    else
                    {
                        return Json(new Response(false, WebHelper.CommonErrorMessage));
                    }

                    details.Description = detailsViewModel.Description;
                    if (detailsViewModel.Amount > 99999999)
                        return Json(new Response(false, "Amount must be 8 Digit"));
                    details.Amount = detailsViewModel.Amount;
                    details.PaymentStatus = TeacherPaymentStatus.Due;

                    teacherAcademicMaterial.TeacherAcademicMaterialsDetails.Add(details);
                }
                bool isSuccess = _teacherAcademicMaterialService.Save(teacherAcademicMaterial);
                if (isSuccess)
                    return Json(new { returnSuccess = "Successfully Save. Tracking Id: M" + teacherAcademicMaterial.Id, IsSuccess = true });

                else return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #region Manage Operation

        public ActionResult Index()
        {
            InitializeIndex();
            return View();
        }

        [HttpPost]
        public ActionResult Index(TeacherAcademicMaterialsManageViewModel teacherAcademicMaterialsManageViewModel)
        {
            InitializeIndex();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (teacherAcademicMaterialsManageViewModel != null)
                {
                    var bList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(teacherAcademicMaterialsManageViewModel.OrganizationId), _commonHelper.ConvertIdToList(teacherAcademicMaterialsManageViewModel.ProgramId), _commonHelper.ConvertIdToList(teacherAcademicMaterialsManageViewModel.SessionId)), "Id", "Name");

                    List<SelectListItem> branchList = bList.ToList();
                    branchList.Insert(0, new SelectListItem() { Value = "-1", Text = "Common" });
                    ViewData["BranchIds"] = new SelectList(branchList, "Value", "Text");
                    var json = new JavaScriptSerializer().Serialize(teacherAcademicMaterialsManageViewModel);
                    ViewData["teacherAcademicMaterialsManageViewModel"] = json;
                }
                return View("teacherAcademicMaterialsReport");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        private void InitializeIndex()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            var paymentStatus = new SelectList(TeacherPaymentStatus.GetTeacherPaymentStatus(), "Key", "Value");

            ViewBag.PaymentStatus = new SelectList(paymentStatus, "Value", "Text");
        }

        #endregion

        #region Details Operation

        public ActionResult Details(int id)
        {
            TeacherAcademicMaterialsDetails teacherAmDetail = _teacherAcademicMaterialDetailsService.GetTeacherAcademicMaterialsDetailById(Convert.ToInt64(id));
            if (teacherAmDetail != null)
            {
                teacherAmDetail.CreateByText = _userService.GetUserNameByAspNetUserId(teacherAmDetail.CreateBy);
                teacherAmDetail.ModifyByText = _userService.GetUserNameByAspNetUserId(teacherAmDetail.ModifyBy);
            }
            return View(teacherAmDetail);
        }

        #endregion

        #region Delete Operation

        public ActionResult Delete(int id)
        {
            try
            {
                bool isSuccess = _teacherAcademicMaterialDetailsService.Delete(id);
                return Json(isSuccess ? new Response(true, "Academic Material sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Teacher Academic Materials Delete Fail !"));
            }
        }

        #endregion

        #region Update Operation

        public ActionResult Edit(int id)
        {
            try
            {
                long cId;
                bool isNum = long.TryParse(id.ToString(), out cId);
                if (!isNum)
                    return HttpNotFound();

                var teacherAcademicMaterialDetails =
                    _teacherAcademicMaterialDetailsService.GetTeacherAcademicMaterialsDetailById(Convert.ToInt64(id));
                return View(teacherAcademicMaterialDetails);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Edit(long id, string description, string amount)
        {
            long cId;
            bool isNum = long.TryParse(id.ToString(), out cId);
            if (!isNum)
            {
                return HttpNotFound();
            }
            var teacherAcademicMaterilDetails = _teacherAcademicMaterialDetailsService.GetTeacherAcademicMaterialsDetailById(Convert.ToInt64(id));
            try
            {
                if (!String.IsNullOrEmpty(description) && !String.IsNullOrEmpty(amount))
                {
                    teacherAcademicMaterilDetails.Description = description;
                    teacherAcademicMaterilDetails.Amount = Convert.ToDecimal(amount);
                    bool isSuccess = _teacherAcademicMaterialDetailsService.Update(teacherAcademicMaterilDetails);
                    if (isSuccess)
                        ViewBag.SuccessMessage = "Successfully updated.";
                    else
                        ViewBag.ErrorMessage = "Problem Occurred, during update.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Problem Occurred, during update.";
                }

            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
            }
            return View(teacherAcademicMaterilDetails);
        }

        #endregion

        #region Ajax Operation

        [HttpPost]
        public ActionResult AjaxRequestForTeacherAcademicEvaluationReport(int draw, int start, int length, string teacherAcademicMaterialsManageViewModel, string date, string teacherName = "", long? forBranchId = null, string trackingId = "", string tpin = "")
        {

            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (!String.IsNullOrEmpty(teacherAcademicMaterialsManageViewModel))
                {
                    TeacherAcademicMaterialsManageViewModel teacherAcademicMaterialsManageViewModels = new JavaScriptSerializer().Deserialize<TeacherAcademicMaterialsManageViewModel>(teacherAcademicMaterialsManageViewModel);


                    long totalScriptEvaluation = _teacherAcademicMaterialDetailsService.CountTeacherAcademicMaterials(_userMenu, teacherAcademicMaterialsManageViewModels.OrganizationId, teacherAcademicMaterialsManageViewModels.ProgramId, teacherAcademicMaterialsManageViewModels.SessionId, teacherAcademicMaterialsManageViewModels.PaymentStatus, date, teacherName, forBranchId, trackingId, tpin);

                    long recordsTotal = totalScriptEvaluation;
                    long recordsFiltered = recordsTotal;

                    IList<TeacherAcademicMaterialsDetails> teacherAcdemicMaterialDetails = _teacherAcademicMaterialDetailsService.TeacherAcademicMaterialsList(start, length, _userMenu, teacherAcademicMaterialsManageViewModels.OrganizationId, teacherAcademicMaterialsManageViewModels.ProgramId, teacherAcademicMaterialsManageViewModels.SessionId, teacherAcademicMaterialsManageViewModels.PaymentStatus, date, teacherName, forBranchId, trackingId, tpin);

                    var data = new List<object>();
                    int sl = start + 1;

                    #region datatable

                    foreach (var value in teacherAcdemicMaterialDetails)
                    {
                        var str = new List<string>();
                        //str.Add(value.CreationDate.ToString("dd/MM/yyyy"));
                        str.Add((value.TeacherAcademicMaterials.SubmissionDate != null) ? value.TeacherAcademicMaterials.SubmissionDate.Value.ToString("dd/MM/yyy") : "");
                        str.Add(value.Teacher.Tpin.ToString());
                        str.Add(value.Teacher.DisplayTeacherName);
                        if (value.ForBranch != null)
                            str.Add(value.ForBranch.Name);
                        else
                            str.Add("Common");
                        str.Add(value.Description);
                        str.Add(value.Amount.ToString());
                        str.Add(value.PaymentStatus == TeacherPaymentStatus.Due ? "Due" : "Paid");
                        str.Add("M" + value.TeacherAcademicMaterials.Id);
                        if (value.PaymentStatus == TeacherPaymentStatus.Paid)
                        {
                            str.Add("<a href='" + Url.Action("Details", "TeacherAcademicMaterials") + "?Id=" + value.Id + "' data-id='" + value.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;");
                        }
                        else
                        {
                            str.Add("<a href='" + Url.Action("Details", "TeacherAcademicMaterials") + "?Id=" + value.Id + "' data-id='" + value.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;" +
                              "<a href='" + Url.Action("Edit", "TeacherAcademicMaterials") + "?Id=" + value.Id + "' data-id='" + value.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; "
                             + "<a href='#' Id='" + value.Id + "' data-id='" + value.Id + "' class='glyphicon glyphicon-trash'> </a>&nbsp;&nbsp; ");
                        }

                        sl++;
                        data.Add(str);
                    }

                    #endregion

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                else
                {
                    return Json(HttpNotFound());
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

    }
}