﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.OrganizationTest
{
    public class OrganizationBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal OrganizationService _organizationService;
        internal List<long> IdList = new List<long>();
        private ISession _session;
        internal OrganizationFactory _organizationFactory;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal readonly string _shortName = Guid.NewGuid() + "_(Short_ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";

        internal readonly string _website = "https://" + Guid.NewGuid() +
                                            "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)" + ".com";
        internal readonly string _email = DateTime.Now.ToString("yyyyMMddHHmmss") +
                                            "ABCCDEFGHIJKLMNOPQRSTUVWXYZ" + "@gmail.com";

        internal readonly string _contactNo = "8801911234567";
        internal readonly string _logoImageUrl = "logoImageUrl/" + Guid.NewGuid() +
                                            "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)" + "@gmail.com";
        internal readonly string _reportImageUrl = "reportImageUrl/" + Guid.NewGuid() +
                                            "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)" + "@gmail.com";
        internal readonly string _idFooterImageUrl = "idFooterImageUrl/" + Guid.NewGuid() +
                                            "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)" + "@gmail.com";
        internal const long OrgId = 1;
        internal DateTime _startTime = DateTime.Now.AddDays(1).AddHours(1);
        internal DateTime _endTime = DateTime.Now.AddDays(1).AddHours(10);
        #endregion

        public OrganizationBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _organizationFactory = new OrganizationFactory(null, _session);
            _organizationService = new OrganizationService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _organizationFactory.Cleanup();
            base.Dispose();
        }
    }   
}
