﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface  IStudentAdmissionBranchTarget:IBaseService
    {
        #region Operational Function

        #endregion

        #region List Loading Functions
        
        IList<StudentBranchTarget> LoadByAdmissionTarget(long studentAdmissionTarget); 
        #endregion
        
        #region Single Instances Loading Functions
        
        #endregion

        #region Others Function

        #endregion

        #region Clean
        //IList<StudentBranchTarget> LoadByBranchList(long[] branches);
        //IList<StudentBranchTarget> LoadByBranchList(long branchId);
        //StudentBranchTarget GetByAdmissionTarget(long studentAdmissionTarget, long branchId); 
        #endregion

    }
    public class StudentAdmissionBranchTarget:BaseService,IStudentAdmissionBranchTarget
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion
       
        #region Propertise & Object Initialization
        private readonly IStudentDao _studentDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IStudentCourseDetailDao _studentCourseDetailDao;
        private readonly IStudentPaymentDao _studentPaymentDao;
        private readonly IStudentBranchTargetDao _studentBranchTargetDao;
        public StudentAdmissionBranchTarget(ISession session)
        {
            Session = session;
            _studentDao = new StudentDao() { Session = Session };
            _studentProgramDao = new StudentProgramDao() { Session = Session };
            _studentCourseDetailDao = new StudentCourseDetailDao() { Session = Session };
            _studentPaymentDao = new StudentPaymentDao() { Session = Session };
            _studentBranchTargetDao = new StudentBranchTargetDao() { Session = Session };
        }
        
        #endregion

        #region Operational Function

        #endregion

        #region List Loading Functions
        

        public IList<StudentBranchTarget> LoadByAdmissionTarget(long studentAdmissionTarget)
        {
            try
            {
                return _studentBranchTargetDao.LoadByAdmissionTarget(studentAdmissionTarget);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        } 
        #endregion
       
        #region Single Instances Loading Functions
        
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        #region Clean
        //public IList<StudentBranchTarget> LoadByBranchList(long[] branches)
        //{
        //    try
        //    {
        //        return _studentBranchTargetDao.LoadByBranchList(branches);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw ex;
        //    }
        //}
        //public IList<StudentBranchTarget> LoadByBranchList(long branchId)
        //{
        //    try
        //    {
        //        return _studentBranchTargetDao.LoadByBranchList(branchId);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw ex;
        //    }
        //}
        //public StudentBranchTarget GetByAdmissionTarget(long studentAdmissionTarget, long branchId)
        //{
        //    try
        //    {
        //        return _studentBranchTargetDao.GetByAdmissionTarget(studentAdmissionTarget, branchId);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw ex;
        //    }
        //}
        #endregion
    }
}
