﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class CertificateFactory : ObjectFactoryBase<Certificate>
    {
        public CertificateFactory(IObjectFactoryBase caller, ISession session)
            : base(caller,session)
        {

        }
    }
}
