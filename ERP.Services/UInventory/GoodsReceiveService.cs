using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Conventions;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.UInventory;

namespace UdvashERP.Services.UInventory
{
    public interface IGoodsReceiveService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(GoodsReceive goodsReceive, bool isOpenning = false);

        #endregion

        #region Single Instances Loading Function

        GoodsReceive GetGoodsReceive(long id);
        GoodsReceive GetGoodsReceive(string goodsReceiveNo);

        #endregion

        #region List Loading Function

        List<ItemOpeningBalanceDetailsViewModel> LoadOpeningBalanceItem(long organizationId, string programSessionId, string goodsReceiveNo, string itemGroupId, string itemType);
        List<ItemOpeningBalanceDetailsViewModel> LoadOpeningBalanceItem(List<UserMenu> userMenus, long organizationId, string programSessionId, string goodsReceiveNo, long? itemGroupId, long itemType);
        List<OrderedGoodsRecViewModel> LoadGoodsReceived(int draw, int start, int length, List<UserMenu> authorizeMenu, long organizationId, long branchId, string itemName, string receivedDate, string workOrderNo, string workOrderDate, string goodStatus);
        IList<SupplierGoodsSummaryReportDto> LoadSuppliersReceivedGoodsSummaryList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long organizationId
                                                                                    , long[] branchs, long[] itemGroups, long[] items, long[] suppliers
                                                                                    , int[] purposes, string[] psArray, DateTime dateFrom, DateTime dateTo);

        IList<GoodsReceive> LoadGoodsReceiveByWorkOrder(long workOrderId);

        #endregion

        #region Others Function

        int CountGoodsReceive(List<UserMenu> authorizeMenu, long organizationId, long branchId, string itemName, string receivedDate, string workOrderNo, string workOrderDate, string goodStatus);
        string GetCurrentGoodsReceivedNoByBranchAndOrg(Branch branch);

        int LoadSuppliersReceivedGoodsSummaryCount(int start, int length, string orderBy, string orderDir, List<UserMenu> list, long organizationId, long[] branchIds, long[] itemGroupIds, long[] itemIds, long[] supplierIds, int[] purposeIds, string[] programSession, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Helper Function

        //GoodsReceive CalculateDirectGoodsRec(WorkOrder workOrder, long currentlyRecQuantity, long prevrecQuantity, string remark = "");

        #endregion

    }
    public class GoodsReceiveService : BaseService, IGoodsReceiveService
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("InventoryService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IGoodsReceiveDao _goodsReceiveDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        private readonly IBranchDao _branchDao;
        private readonly IItemDao _itemDao;
        private readonly ICurrentStockSummaryDao _currentStockSummaryDao;
        private readonly IWorkOrderDao _workOrderDao;
        private readonly IProgramBranchSessionDao _programBranchSessionDao;
        public GoodsReceiveService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _goodsReceiveDao = new GoodsReceiveDao { Session = session };
            _programDao = new ProgramDao() { Session = session };
            _sessionDao = new SessionDao() { Session = session };
            _branchDao = new BranchDao() { Session = session };
            _itemDao = new ItemDao() { Session = session };
            _currentStockSummaryDao = new CurrentStockSummaryDao() { Session = session };
            _workOrderDao = new WorkOrderDao() { Session = session };
            _programBranchSessionDao = new ProgramBranchSessionDao() { Session = session };

        }
        #endregion

        #region Operational Functions

        public void SaveOrUpdate(GoodsReceive goodsReceive, bool isOpenning = false)
        {
            ITransaction trans = null;
            try
            {
                IList<CurrentStockSummary> summaries = new List<CurrentStockSummary>();
                IList<GoodsReceiveDetails> receiveDetailses = new List<GoodsReceiveDetails>();
                ServiceValidationCheckGoodRec(goodsReceive, isOpenning);
                MakeSummariesAndRecDetail(goodsReceive, isOpenning, out summaries, out receiveDetailses);

                using (trans = Session.BeginTransaction())
                {
                    //if (!isOpenning)
                    goodsReceive.GoodsReceivedNo = GetCurrentGoodsReceivedNoByBranchAndOrg(goodsReceive.Branch);
                    goodsReceive.GoodsReceiveDetails = receiveDetailses;
                    CheckValidation(goodsReceive);
                    _goodsReceiveDao.Save(goodsReceive);
                    if (goodsReceive.Workorder != null)
                    {
                        //update workorder status here 
                        var totalOrderedQuantity = goodsReceive.Workorder.WorkOrderDetails.Sum(wod => wod.OrderQuantity);
                        IList<GoodsReceive> gdList = this.LoadGoodsReceiveByWorkOrder(goodsReceive.Workorder.Id);
                        long totalRecQuantity = gdList.Aggregate<GoodsReceive, long>(0, (current, grd) => current + grd.GoodsReceiveDetails.Sum(wod => wod.ReceivedQuantity));
                        goodsReceive.Workorder.WorkOrderStatus = totalOrderedQuantity == totalRecQuantity ? WorkOrderStatus.Received : WorkOrderStatus.PartiallyReceived;
                        _workOrderDao.Update(goodsReceive.Workorder);
                    }
                    foreach (var currentStockSummary in summaries)
                    {
                        var program = currentStockSummary.Program;
                        var session = currentStockSummary.Session;
                        long? programId = null;
                        long? sessionId = null;
                        if (program != null && session != null)
                        {
                            programId = program.Id;
                            sessionId = session.Id;
                        }
                        CurrentStockSummary prevStockSummary = _currentStockSummaryDao.GetCurrentStockSummary(currentStockSummary.Item.Id, currentStockSummary.Branch.Id, programId, sessionId);
                        if (prevStockSummary != null)
                        {
                            prevStockSummary.StockQuantity += currentStockSummary.StockQuantity;
                            _currentStockSummaryDao.Update(prevStockSummary);
                        }
                        else
                            _currentStockSummaryDao.Save(currentStockSummary);
                    }
                    trans.Commit();
                }
            }
            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public GoodsReceive GetGoodsReceive(long id)
        {
            if (id <= 0) throw new InvalidDataException("Invalid receive Id");
            return _goodsReceiveDao.LoadById(id);
        }

        public GoodsReceive GetGoodsReceive(string goodsReceiveNo)
        {
            try
            {
                if (String.IsNullOrEmpty(goodsReceiveNo)) throw new InvalidDataException("invalid goods receive no.");
                if (goodsReceiveNo.Length > 13 || goodsReceiveNo.Length < 13) throw new InvalidDataException("invalid length of goods receive no.");
                var goodsReceiveObj = _goodsReceiveDao.GetGoodsReceive(goodsReceiveNo.Trim());
                return goodsReceiveObj;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<GoodsReceive> LoadGoodsReceiveByWorkOrder(long workOrderId)
        {
            try
            {
                if (workOrderId <= 0) throw new InvalidDataException("invalid workorder no");
                return _goodsReceiveDao.LoadGoodsReceiveByWorkOrder(workOrderId);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        public List<ItemOpeningBalanceDetailsViewModel> LoadOpeningBalanceItem(List<UserMenu> userMenus, long organizationId, string programSessionId,
            string goodsReceiveNo, long? itemGroupId, long itemType)
        {
            try
            {
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                List<long> authProgramIdList;
                List<long> programIdList = new List<long>();
                List<long> sessionIdList = new List<long>();

                if (String.IsNullOrEmpty(programSessionId) || programSessionId == "0")
                {
                    sessionIdList = null;
                    authProgramIdList = null;
                }
                else
                {
                    var spltValue = programSessionId.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                    programIdList.Add(Convert.ToInt64(spltValue[0]));
                    sessionIdList.Add(Convert.ToInt64(spltValue[1]));
                    authProgramIdList = AuthHelper.LoadProgramIdList(userMenus, authOrganizationIdList, null, programIdList);
                    if (authProgramIdList.Count == 0)
                        throw new MessageException("You are not authorize.");
                }
                return _goodsReceiveDao.LoadOpeningBalanceItem(authOrganizationIdList, authProgramIdList, sessionIdList,
                    goodsReceiveNo, itemGroupId, itemType);
            }
            catch (MessageException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<OrderedGoodsRecViewModel> LoadGoodsReceived(int draw, int start, int length, List<UserMenu> authorizeMenu, long organizationId, long branchId,
            string itemName, string receivedDate, string workOrderNo, string workOrderDate, string goodStatus)
        {
            try
            {
                if (authorizeMenu == null) throw new InvalidDataException("invalid user permission");
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authorizeMenu);
                List<long> authBrList = AuthHelper.LoadBranchIdList(authorizeMenu);
                List<long> authProgramList = AuthHelper.LoadProgramIdList(authorizeMenu, authOrgList, authBrList);
                return _goodsReceiveDao.LoadGoodsReceived(draw, start, length, authOrgList, authProgramList, authBrList, organizationId, branchId, itemName, receivedDate, workOrderNo, workOrderDate, goodStatus);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public List<ItemOpeningBalanceDetailsViewModel> LoadOpeningBalanceItem(long organizationId, string programSessionIdList, string goodsReceiveNo, string itemGroupId, string itemType)
        {
            try
            {
                return _goodsReceiveDao.LoadOpeningBalanceItem(programSessionIdList, goodsReceiveNo, itemGroupId, itemType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<SupplierGoodsSummaryReportDto> LoadSuppliersReceivedGoodsSummaryList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus, long organizationId, long[] branchs,
            long[] itemGroups, long[] items, long[] suppliers, int[] purposes, string[] psArray, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                branchs = branchs.Where(val => val != 0).ToArray();
                long[] bIds = { };
                if (branchs.Count() > 0 && branchs[0] != 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, authOrgList, null, branchs.ToList()).ToArray();
                }
                else if (branchs.Length <= 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, _commonHelper.ConvertIdToList(organizationId)).ToArray();
                }
                if (bIds.Length <= 0)
                {
                    throw new InvalidDataException("Authorize branch not found");
                }
                items = items.Where(val => val != 0).ToArray();
                if (items.Length <= 0 && itemGroups.Length > 0)
                {
                    itemGroups = itemGroups.Where(val => val != 0).ToArray();
                    items = _itemDao.LoadItem(itemGroups, organizationId).Select(x => x.Id).ToArray();
                }
                psArray = FilterProgramSession(userMenus, authOrgList, bIds, items, psArray);
                var list = _goodsReceiveDao.LoadSuppliersReceivedGoodsSummary(start, length, orderBy, orderDir, bIds, purposes, itemGroups, items, psArray, suppliers, dateFrom, dateTo);
                return list;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int CountGoodsReceive(List<UserMenu> authorizeMenu, long organizationId, long branchId, string itemName, string receivedDate, string workOrderNo,
            string workOrderDate, string goodStatus)
        {
            try
            {
                if (authorizeMenu == null) throw new InvalidDataException("invalid user permission");
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authorizeMenu);
                List<long> authBrList = AuthHelper.LoadBranchIdList(authorizeMenu);
                List<long> authProgramList = AuthHelper.LoadProgramIdList(authorizeMenu, authOrgList, authBrList);
                return _goodsReceiveDao.CountGoodsReceive(authOrgList, authProgramList, authBrList, organizationId, branchId, itemName, receivedDate, workOrderNo, workOrderDate, goodStatus);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public string GetCurrentGoodsReceivedNoByBranchAndOrg(Branch branch)
        {
            if (branch == null) throw new NullObjectException("Branch can't be null");
            var list = Session.QueryOver<GoodsReceive>().Where(x => x.Branch.Id == branch.Id).List<GoodsReceive>();
            if (list == null || list.Count == 0) return branch.Organization.ShortName.Substring(0, 3) + branch.Code + "GR" + "00000" + 1;
            GoodsReceive obj = list.OrderByDescending(x => x.Id).Take(1).SingleOrDefault();
            if (obj == null) return branch.Organization.ShortName.Substring(0, 3) + branch.Code + "GR" + "00000" + 1;
            return branch.Organization.ShortName.Substring(0, 3) + branch.Code + "GR" + (Convert.ToInt64(obj.GoodsReceivedNo.Split(new string[] { "GR" }, StringSplitOptions.None)[1]) + 1).ToString("000000");
        }

        public int LoadSuppliersReceivedGoodsSummaryCount(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus,
            long organizationId, long[] branchIds, long[] itemGroups, long[] items, long[] supplierIds, int[] purposeIds,
            string[] psArray, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                branchIds = branchIds.Where(val => val != 0).ToArray();
                long[] bIds = { };
                if (branchIds.Count() > 0 && branchIds[0] != 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, authOrgList, null, branchIds.ToList()).ToArray();
                }
                else if (branchIds.Length <= 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, _commonHelper.ConvertIdToList(organizationId)).ToArray();
                }
                if (bIds.Length <= 0)
                {
                    throw new InvalidDataException("Authorize branch not found");
                }
                items = items.Where(val => val != 0).ToArray();
                if (items.Length <= 0 && itemGroups.Length > 0)
                {
                    itemGroups = itemGroups.Where(val => val != 0).ToArray();
                    items = _itemDao.LoadItem(itemGroups, organizationId).Select(x => x.Id).ToArray();
                }
                psArray = FilterProgramSession(userMenus, authOrgList, bIds, items, psArray);
                return _goodsReceiveDao.CountSuppliersReceivedGoodsCount(bIds, purposeIds, itemGroups, items, psArray, supplierIds, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        private void MakeSummariesAndRecDetail(GoodsReceive goodsReceive, bool isOpenning, out IList<CurrentStockSummary> summaries, out IList<GoodsReceiveDetails> receiveDetailses)
        {
            try
            {
                const int pPurpose = (int)Purpose.ProgramPurpose;
                const int mPurpose = (int)Purpose.MarketingPurpose;
                const int gPurpose = (int)Purpose.GiftAndPromotionPurpose;
                const int aPurpose = (int)Purpose.AdministrativePurpose;
                const int bPurpose = (int)Purpose.BrandingPurpose;
                summaries = new List<CurrentStockSummary>();
                receiveDetailses = new List<GoodsReceiveDetails>();
                foreach (var goodsReceiveDetailse in goodsReceive.GoodsReceiveDetails)
                {
                    ServiceValidationCheckGoodRecDetail(goodsReceiveDetailse, isOpenning);

                    //if (goodsReceiveDetailse.Item.ItemType == (int)ItemType.ProgramItem && goodsReceiveDetailse.PurposeProgramId == null) throw new InvalidDataException("Invalid purpose program because program item selected");
                    //if (goodsReceiveDetailse.Item.ItemType == (int)ItemType.ProgramItem && goodsReceiveDetailse.PurposeSessionId == null) throw new InvalidDataException("Invalid purpose Session because program item selected");
                    //if (goodsReceiveDetailse.Item.ItemType == (int)ItemType.ProgramItem && goodsReceiveDetailse.Purpose == null && goodsReceiveDetailse.PurposeProgramId != null) throw new InvalidDataException("Invalid purpose program because program item selected");
                    //if (goodsReceiveDetailse.Item.ItemType == (int)ItemType.ProgramItem && goodsReceiveDetailse.Purpose == null && goodsReceiveDetailse.PurposeSessionId != null) throw new InvalidDataException("Invalid purpose program because program item selected");

                    /* n/a check */
                    if (goodsReceiveDetailse.Item.ItemType == (int)ItemType.ProgramItem && goodsReceiveDetailse.Purpose != null)
                    {
                        goodsReceiveDetailse.PurposeProgramId = goodsReceiveDetailse.Program;
                        goodsReceiveDetailse.PurposeSessionId = goodsReceiveDetailse.Session;
                    }
                    else if ((goodsReceiveDetailse.Item.ItemType == (int)ItemType.ProgramItem && goodsReceiveDetailse.Purpose == null) ||
                        (goodsReceiveDetailse.Item.ItemType == (int)ItemType.CommonItem && goodsReceiveDetailse.Purpose == null))
                    {
                        goodsReceiveDetailse.PurposeProgramId = null;
                        goodsReceiveDetailse.PurposeSessionId = null;
                    }
                    else if (goodsReceiveDetailse.Item.ItemType == (int)ItemType.CommonItem &&
                         (goodsReceiveDetailse.Purpose.Equals(pPurpose) || goodsReceiveDetailse.Purpose.Equals(mPurpose) || goodsReceiveDetailse.Purpose.Equals(gPurpose)))
                    {
                        if (goodsReceiveDetailse.Program != null)
                        {
                            goodsReceiveDetailse.PurposeProgramId = _programDao.LoadById(goodsReceiveDetailse.Program.Id);
                            if (goodsReceiveDetailse.Session != null)
                                goodsReceiveDetailse.PurposeSessionId = _sessionDao.LoadById(goodsReceiveDetailse.Session.Id);
                        }
                        goodsReceiveDetailse.Program = null;
                        goodsReceiveDetailse.Session = null;
                    }
                    else if (goodsReceiveDetailse.Item.ItemType == (int)ItemType.CommonItem && (goodsReceiveDetailse.Purpose.Equals(aPurpose) || goodsReceiveDetailse.Purpose.Equals(bPurpose)))
                    {
                        if (goodsReceiveDetailse.Program != null)
                        {
                            goodsReceiveDetailse.PurposeProgramId = _programDao.LoadById(goodsReceiveDetailse.Program.Id);
                            if (goodsReceiveDetailse.Session != null)
                                goodsReceiveDetailse.PurposeSessionId = _sessionDao.LoadById(goodsReceiveDetailse.Session.Id);
                        }
                        goodsReceiveDetailse.Program = null;
                        goodsReceiveDetailse.Session = null;
                    }

                    

                    //receive details
                    goodsReceiveDetailse.GoodsReceive = goodsReceive;
                    if (HttpContext.Current != null)
                    {
                        goodsReceiveDetailse.CreateBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                        goodsReceiveDetailse.ModifyBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                    }
                    goodsReceiveDetailse.Status = GoodsReceive.EntityStatus.Active;
                    receiveDetailses.Add(goodsReceiveDetailse);
                    var currentStockSummary = new CurrentStockSummary().SetValue(goodsReceiveDetailse.Item, goodsReceiveDetailse.Program, goodsReceiveDetailse.Session, goodsReceive.Branch, goodsReceiveDetailse.ReceivedQuantity);
                    summaries.Add(currentStockSummary);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        private void ServiceValidationCheckGoodRecDetail(GoodsReceiveDetails goodsReceiveDetailse, bool isOpenning)
        {
            try
            {
                if (goodsReceiveDetailse.Item == null)
                    throw new NullObjectException("Item can't be null !");
                if (isOpenning == false)
                {
                    if (goodsReceiveDetailse.ReceivedQuantity <= 0)
                        throw new InvalidDataException("Received quantity can't be empty !");
                    if (goodsReceiveDetailse.Rate <= 0)
                        throw new InvalidDataException("Rate can't be empty or negative !");
                    if (goodsReceiveDetailse.TotalCost <= 0)
                        throw new InvalidDataException("Total cost can't be empty or negative !");
                }
                else
                {
                    if (goodsReceiveDetailse.ReceivedQuantity < 0)
                        throw new InvalidDataException("Received quantity can't be empty !");
                    if (goodsReceiveDetailse.Rate < 0)
                        throw new InvalidDataException("Rate can't be empty or negative !");
                    if (goodsReceiveDetailse.TotalCost < 0)
                        throw new InvalidDataException("Total cost can't be empty or negative !");
                }
                var notapplicablePurposeAry = new[] { (int)Purpose.ProgramPurpose, (int)Purpose.MarketingPurpose, (int)Purpose.GiftAndPromotionPurpose };
                if (goodsReceiveDetailse.Purpose == 0)
                    goodsReceiveDetailse.Purpose = null;

                if (isOpenning) return;
                if (goodsReceiveDetailse.Purpose == null)
                {
                    // do nothing 
                }
                else if (notapplicablePurposeAry.Contains((int)goodsReceiveDetailse.Purpose))
                {
                    if (goodsReceiveDetailse.Item.ItemType == (int)ItemType.ProgramItem)
                    {
                        if (goodsReceiveDetailse.Program == null && goodsReceiveDetailse.Session == null)
                            throw new InvalidDataException("Invalid selection! program item must contain a particular program and session if purpose [ProgramPurpose or MarketingPurpose or GiftAndPromotionPurpose] ");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        private void ServiceValidationCheckGoodRec(GoodsReceive goodsReceive, bool isOpenning)
        {
            try
            {
                if (goodsReceive.Branch == null)
                    throw new NullObjectException("Branch can't be null !");
                if (goodsReceive.Supplier == null && !isOpenning)
                    throw new NullObjectException("Supplier can't be null !");
                if (!isOpenning)
                {
                    if (goodsReceive.TotalCost <= 0)
                        throw new InvalidDataException("Total cost can't be empty or negative !");
                }
                else
                {
                    if (goodsReceive.TotalCost < 0)
                        throw new InvalidDataException("Total cost can't be empty or negative !");
                }
                if (String.IsNullOrEmpty(goodsReceive.GoodsReceivedDate.ToString()))
                    throw new InvalidDataException("receive date is empty !");
                if ((goodsReceive.GoodsReceiveDetails == null || goodsReceive.GoodsReceiveDetails.Count == 0))
                    throw new NullObjectException("Goods receive details can not be null");

            }
            catch (NullObjectException noe)
            {
                throw noe;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        private static void CheckValidation(GoodsReceive goodsReceive)
        {
            var validationResult = ValidationHelper.ValidateEntity<GoodsReceive, GoodsReceive>(goodsReceive);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private string[] FilterProgramSession(List<UserMenu> userMenus, List<long> authOrgList, long[] bIds, long[] itemIds, string[] programSession)
        {
            if (programSession[0].Equals(string.Empty))
            {
                var authProIdList = AuthHelper.LoadProgramIdList(userMenus, authOrgList, bIds.ToList());
                var psList = _programBranchSessionDao.LoadProgramSession(authOrgList, bIds.ToList<long>(), authProIdList, itemIds.ToList(), false);
                IList<string> li = psList.Select(programSessionDto => programSessionDto.ProgramAndSessionId).ToList();
                li.Add("N/A");
                programSession = li.ToArray();
            }
            else if (programSession[0] == "0")
            {
                var ps = programSession.ToList();
                ps.RemoveAt(0);
                programSession = ps.ToArray();
            }
            return programSession;
        }

        #endregion

        #region Suppliers Goods Summary Report

        #endregion

    }
}

