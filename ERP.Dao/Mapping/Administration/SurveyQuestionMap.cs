using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class SurveyQuestionMap : BaseClassMap<SurveyQuestion, long>
    {

        public SurveyQuestionMap()
        {
            Table("SurveyQuestion");
			LazyLoad();
            Map(x => x.Question).Column("Question");
            Map(x => x.Rank);

            HasMany(x => x.ProgramSessionSurveyQuestions).KeyColumn("SurveyQuestionId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentSurveyAnswers).KeyColumn("SurveyQuestionId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SurveyQuestionAnswers).KeyColumn("SurveyQuestionId").Cascade.All().Inverse().Where(ss => ss.Status != SurveyQuestionAnswer.EntityStatus.Delete).Not.LazyLoad(); 
        }
    }
}
