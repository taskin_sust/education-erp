﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MenuGroupTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "MenuGroup")]
    public class MenuGroupLoadActiveTest : MenuGroupBaseTest
    {
        #region Basic Test
        
        [Fact]
        public void LoadActive_should_return_list()
        {

            _menugroupFactory.CreateMore(100).Persist();
            var menuGroupList = _menuService.GetMenuGroupList(0, 100, "", "", "", "1", "");
            Assert.Equal(100, menuGroupList.Count);
        }

        #endregion
    }
}
