﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IShiftDao : IBaseDao<Shift, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<Shift> LoadShift(string orderBy, string direction, List<long> organizationId);
        IList<Shift> LoadShift(List<long> organizationIds);
        #endregion

        #region Others Function
        int GetShiftRowCount(string orderBy, string direction, List<long> organizationId);
        #endregion

        #region Helper Function

        #endregion

        bool CheckDublicateName(string name, long organizationId, long? id);
        bool CheckDublicateStartEndTime(DateTime startTime, DateTime endTime, long organizationId, long? id);

        
    }
    public class ShiftDao : BaseDao<Shift, long>, IShiftDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<Shift> LoadShift(string orderBy, string direction, List<long> organizationId)
        {
            ICriteria criteria = GetShiftListCriteria(orderBy, direction, organizationId);
            //return criteria.SetFirstResult(start).SetMaxResults(length).List<HRShift>();
            return criteria.List<Shift>();
        }

        public IList<Shift> LoadShift(List<long> organizationIds)
        {
            var shiftList =
              Session.QueryOver<Shift>().Where(x => x.Organization.Id.IsIn(organizationIds.ToArray()) && x.Status==Shift.EntityStatus.Active).OrderBy(x=>x.Rank).Asc.List<Shift>();
            return shiftList;
        }
        #endregion

        #region Others Function



        public int GetShiftRowCount(string orderBy, string direction, List<long> organizationId)
        {
            ICriteria criteria = GetShiftListCriteria(orderBy, direction, organizationId, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
        #endregion

        #region Helper Function
        private ICriteria GetShiftListCriteria(string orderBy, string direction, List<long> organizationIds, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<Shift>().Add(Restrictions.Not(Restrictions.Eq("Status", BusinessModel.Entity.Administration.Session.EntityStatus.Delete)));

            if (organizationIds != null)
            {
                criteria.Add(Restrictions.In("Organization.Id", organizationIds));
            }

            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    if (orderBy == "Organization.ShortName")
                    {
                        criteria.CreateAlias("Organization", "Organization");
                    }
                    if (orderBy == "ShoftDuration")
                    {
                        criteria.AddOrder(direction == "ASC" ? Order.Asc("StartTime") : Order.Desc("EndTime"));
                    }
                    else
                    {
                        criteria.AddOrder(direction == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                    }
                }
            }

            return criteria;
        }

        public bool CheckDublicateName(string name, long organizationId, long? id)
        {
            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<Shift>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Shift.EntityStatus.Delete)));
            if (id != null && id > 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (name != null)
            {
                criteria.Add(Restrictions.Eq("Name", name));
            }
            criteria.Add(Restrictions.Eq("Organization.Id", organizationId));
            
            var hrShiftSettingList = criteria.List<Shift>();
            if (hrShiftSettingList != null && hrShiftSettingList.Count > 0)
            {

                checkValue = true;
            }
            return checkValue;
        }

        public bool CheckDublicateStartEndTime(DateTime startTime, DateTime endTime, long organizationId, long? id)
        {
            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<Shift>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Shift.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("StartTime", startTime));
            criteria.Add(Restrictions.Eq("EndTime", endTime));
            criteria.Add(Restrictions.Eq("Organization.Id", organizationId));
            if (id != null && id > 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            var hrShiftSettingList = criteria.List<Shift>();
            if (hrShiftSettingList != null && hrShiftSettingList.Count > 0)
            {

                checkValue = true;
            }
            return checkValue;
        }

        #endregion

       
    }

}
