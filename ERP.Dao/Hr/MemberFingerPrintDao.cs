﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface IMemberFingerPrintDao : IBaseDao<MemberFingerPrint, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        MemberFingerPrint GetByTeamMember(long teammemberid);
        MemberFingerPrint GetByTeamMemberAndModelId(long memberId, long modelId);
        #endregion

        #region List Loading Function
        IList<MemberInfoApi> LoadMemberInfoForDeviceApiByPin(int pin);
        IList<MemberFingerPrint> LoadByFinderIndex(List<string> fingerList, long id);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion


    }

    public class MemberFingerPrintDao : BaseDao<MemberFingerPrint, long>, IMemberFingerPrintDao
    {
        #region Properties & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public MemberFingerPrint GetByTeamMember(long teammemberid)
        {
            return Session.QueryOver<MemberFingerPrint>().Where(x => x.TeamMember.Id == teammemberid && x.Status == MemberFingerPrint.EntityStatus.Active).SingleOrDefault<MemberFingerPrint>();
        }

        public MemberFingerPrint GetByTeamMemberAndModelId(long memberId = 0, long modelId = 0)
        {
            var criteria = Session.CreateCriteria<MemberFingerPrint>();
            criteria.Add(Restrictions.Eq("Status", MemberFingerPrint.EntityStatus.Active));
            if (modelId > 0)
            {
                criteria.CreateAlias("AttendanceDeviceType", "adt");
                criteria.Add(Restrictions.Eq("adt.Id", modelId));
            }
            if (memberId > 0)
            {
                criteria.CreateAlias("TeamMember", "tm");
                criteria.Add(Restrictions.Eq("tm.Id", memberId));
            }
            return criteria.UniqueResult<MemberFingerPrint>();
            //   return Session.QueryOver<MemberFingerPrint>().Where(x => x.TeamMember.Id == memberId && x.AttendanceDeviceType.Id == modelId && x.Status == MemberFingerPrint.EntityStatus.Active).SingleOrDefault<MemberFingerPrint>();
        }

        #endregion

        #region List Loading Function

        public IList<MemberInfoApi> LoadMemberInfoForDeviceApiByPin(int pin)
        {
            var query = @"Select a.*
                ,mfp.Index0,mfp.Index1,mfp.Index2,mfp.Index3,mfp.Index4,mfp.Index5,mfp.Index6,mfp.Index7,mfp.Index8,mfp.Index9
                 from (
	                Select a.*,b.* from (
		                select adt.Id as ModelId,adt.Name as ModelName,adt.IsFinger  
		                from HR_AttendanceDeviceType as adt where Status=1
	                ) as a,(
		                Select tm.FullNameEng as Name, tm.Pin, tm.Id as TeamMemberId, org.ShortName Organization ,bra.Name as Branch, cam.Name as Campus , dept.Name as Department, desg.Name as Designation
		                from (
			                SELECT * FROM (
					                SELECT
					                  [CampusId],
					                  [DesignationId],
					                  [DepartmentId],
					                  [TeamMemberId],
					                  [EffectiveDate],
					                  [EmploymentStatus],
					                  RANK() OVER (PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC, Id DESC) AS R
					                FROM [HR_EmploymentHistory]
					                WHERE Status = 1
					                AND EffectiveDate <= GETDATE()
				                ) AS a
			                WHERE a.R = 1
			                AND a.EmploymentStatus != 6
			                AND a.TeamMemberId = ( select Id from HR_TeamMember where Pin =" + pin;
            query += @" )
		                ) as fPart 
		                left join Campus as cam on fPart.CampusId = cam.Id 
		                left join HR_Designation as desg on fPart.DesignationId = desg.Id
		                left join HR_Department as dept on fPart.DepartmentId = dept.Id
		                left join Branch as bra on cam.BranchId = bra.Id
		                left join Organization as org on bra.OrganizationId = org.Id
		                left join HR_TeamMember as tm on fPart.TeamMemberId = tm.Id
	                ) as b
                ) as a
                LEFT JOIN (
	                Select * from (
		                Select tm.Id as TeamMemberId
		                ,mfp.Index0,mfp.Index1,mfp.Index2,mfp.Index3,mfp.Index4,mfp.Index5,mfp.Index6,mfp.Index7,mfp.Index8,mfp.Index9,  adty.Name as adtnames
		                from  HR_TeamMember as tm 
		                left join HR_MemberFingerPrint as mfp on tm.Id = mfp.TeamMemberId 
		                left join HR_AttendanceDeviceType as adty on adty.id = mfp.AttendanceDeviceTypeId
		                Where mfp.TeamMemberId = " + pin;
            query += @" ) as a
                ) as mfp ON a.TeamMemberId = mfp.TeamMemberId
                  AND a.ModelName = mfp.adtnames";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<MemberInfoApi>());
            var result = iQuery.List<MemberInfoApi>();
            return result;

        }

        public IList<MemberFingerPrint> LoadByFinderIndex(List<string> fingerList, long id)
        {
            var critera = Session.CreateCriteria<MemberFingerPrint>();
            if (id > 0)
                critera.Add(Restrictions.Not(Restrictions.Eq("Id", id)));

            var dis = Restrictions.Disjunction();
            dis.Add(Restrictions.In("Index0", fingerList));
            dis.Add(Restrictions.In("Index1", fingerList));
            dis.Add(Restrictions.In("Index2", fingerList));
            dis.Add(Restrictions.In("Index3", fingerList));
            dis.Add(Restrictions.In("Index4", fingerList));
            dis.Add(Restrictions.In("Index5", fingerList));
            dis.Add(Restrictions.In("Index6", fingerList));
            dis.Add(Restrictions.In("Index7", fingerList));
            dis.Add(Restrictions.In("Index8", fingerList));
            dis.Add(Restrictions.In("Index9", fingerList));
            critera.Add(dis);
            return critera.List<MemberFingerPrint>();

            //var conF = Restrictions.Conjunction();
            //conF.Add(Restrictions.In("Index0", fingerList));

            //var conS = Restrictions.Conjunction();
            //conS.Add(Restrictions.In("Index1", fingerList));

            //var conT = Restrictions.Conjunction();
            //conT.Add(Restrictions.In("Index2", fingerList));

            //var conFo = Restrictions.Conjunction();
            //conFo.Add(Restrictions.In("Index3", fingerList));

            //var conFi = Restrictions.Conjunction();
            //conFi.Add(Restrictions.In("Index4", fingerList));

            //var conSi = Restrictions.Conjunction();
            //conSi.Add(Restrictions.In("Index5", fingerList));

            //var conSe = Restrictions.Conjunction();
            //conSe.Add(Restrictions.In("Index6", fingerList));

            //var conE = Restrictions.Conjunction();
            //conE.Add(Restrictions.In("Index7", fingerList));

            //var conN = Restrictions.Conjunction();
            //conN.Add(Restrictions.In("Index8", fingerList));

            //var conTe = Restrictions.Conjunction();
            //conTe.Add(Restrictions.In("Index9", fingerList));
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
}
