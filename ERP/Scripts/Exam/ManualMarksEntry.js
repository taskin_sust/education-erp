function autoCompleteFunc() {     
    $('.autoComplete').each(function () {
        var $this = $(this);       
            $this.typeahead({
                source: function (query, process) {
                    var programId = $("#Program").val();
                    var sessionId = $("#Session").val();
                    var courseId = $("#Course").val();
                    var data = { query: query, programId: programId, sessionId: sessionId, courseId: courseId }
                    $.ajax({
                        type: "POST",
                        url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForExamCoad",
                        cache: false,
                        async: true,
                        data: data,

                        success: function(result) {
                            if (result.IsSuccess) {
                                process(result.returnList);
                            }
                        },
                        complete: function() {
                        },
                        error: function(result) {
                        }
                    });
                },
                updater: function(item) {
                    $("#ExamId").val(item.Value);
                    $("#ExamType option").remove();
                    $("#ExamCode").val("");
                    $("#ExamName").val("");
                    $.ajax({
                        type: "post",
                        url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForExamInformation",
                        cache: false,
                        async: true,
                        data: { "id": item.Value },
                        beforeSend: function() {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function(result) {
                            $.unblockUI();
                            if (result.IsSuccess) {
                                console.log(result.returnList);
                                if (result.returnList.hasOwnProperty("Name")) {
                                    $("#ExamName").val(result.returnList.Name);
                                    //  $("#ExamCode").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").text("");
                                    showErrorMessageBelowCtrl("ExamCode", "", false);
                                }
                                    
                                if (result.returnList.IsMcq == "True" && result.returnList.IsWritten == "True") {
                                    // $('#ExamType').parent().find("span").removeClass(".field-validation-error").addClass("field-validation-valid").text("");
                                    showErrorMessageBelowCtrl("ExamType", "", false);
                                    $('#ExamType').append("<option value='" + result.returnList.IsMcqValue + "'>MCQ</option>");
                                    $('#ExamType').append("<option value='" + result.returnList.IsWrittenValue + "'>Written</option>");
                                }
                                else if (result.returnList.IsMcq == "True" && result.returnList.IsWritten == "False") {
                                    // $('#ExamType').parent().find("span").removeClass(".field-validation-error").addClass("field-validation-valid").text("");
                                    showErrorMessageBelowCtrl("ExamType", "", false);
                                    $('#ExamType').append("<option value='" + result.returnList.IsMcqValue + "'>MCQ</option>");
                                }
                                else if (result.returnList.IsMcq == "False" && result.returnList.IsWritten == "True") {
                                    //$('#ExamType').parent().find("span").removeClass(".field-validation-error").addClass("field-validation-valid").text("");
                                    showErrorMessageBelowCtrl("ExamType", "", false);
                                    $('#ExamType').append("<option value='" + result.returnList.IsWrittenValue + "'>Written</option>");
                                }
                                else {
                                    showErrorMessageBelowCtrl("ExamType", "", false);
                                    //$('#ExamType').append("<option value=''>--Select Exam Types--</option>");
                                    //$("#ExamType").parent().find("span").removeClass(".field-validation-error").addClass("field-validation-valid").text("The Exam Type Field is required.");
                                }
                            }
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        error: function(result) {
                            $.unblockUI();
                            console.log("Failed -----");
                        }
                    });
                    return item.Text;
                },
                matcher: function(item) {
                    if (item.Text.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                        return true;
                    }
                    return false;
                },
                sorter: function(items) {
                    return items.sort();
                },
                highlighter: function(item) {
                    var regex = new RegExp('(' + this.query + ')', 'gi');
                    return item.Text.replace(regex, "<strong>$1</strong>");
                },
                onselect: function(obj) {
                    alert('Selected ' + obj);
                }
            });                
    });
}
$(document).on("input propertychange", ".autoComplete", function (event) {
    $("#manualMarksEntry").empty();
    $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria.</th></tr></thead>");
    autoCompleteFunc();
    if ($(this).val() == "") {
        //  $("#ExamCode").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").text("The Exam Code field is required.");
        showErrorMessageBelowCtrl("ExamCode", "", false);
    }
});
function responseReturn() {
    $("#manualMarksEntry").empty();
    $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria.</th></tr></thead>");
    var check = true;



    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var course = $("#Course").val();
    var examCode = $("#ExamCode").val();
    var examType = $("#ExamType").val();
    var totalRows = $("#TotalRows").val();
    var commonSetCode = $("#CommonSetCode").val();
    if (organizationId == "" || organizationId == null) {
        showErrorMessageBelowCtrl("Organization", "", false);
        showErrorMessageBelowCtrl("Organization", "The Organization field is required.", true);
        check = false;
    }

    if (programId == "" || programId == null) {
        showErrorMessageBelowCtrl("Program", "", false);
        showErrorMessageBelowCtrl("Program", "The Program field is required.", true);
        check = false;
    }

    if (sessionId == "" || sessionId == null) {
        showErrorMessageBelowCtrl("Session", "", false);
        showErrorMessageBelowCtrl("Session", "The Session field is required.", true);
        check = false;
    }

    if (course == "" || course == null) {
        showErrorMessageBelowCtrl("Course", "", false);
        showErrorMessageBelowCtrl("Course", "The Course field is required.", true);
        check = false;
    }


    if (examCode == "" || examCode == null) {
        showErrorMessageBelowCtrl("ExamCode", "", false);
        showErrorMessageBelowCtrl("ExamCode", "The Exam Code field is required.", true);
        check = false;
    }


    if (examType == "" || examType == null) {
        showErrorMessageBelowCtrl("ExamType", "", false);
        showErrorMessageBelowCtrl("ExamType", "The Exam Type field is required.", true);
        check = false;
    }

    if (totalRows == "") {
        showErrorMessageBelowCtrl("TotalRows", "", false);
        showErrorMessageBelowCtrl("TotalRows", "The Total Rows field is required.", true);
       // $("#TotalRows").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Total Rows field is required.");
        check = false;
    }
    else if (totalRows < 1) {
        showErrorMessageBelowCtrl("TotalRows", "", false);
        showErrorMessageBelowCtrl("TotalRows", "Please enter a value greater than or equal to 1.", true);
        check = false;
    }
    if (commonSetCode == "") {
        showErrorMessageBelowCtrl("CommonSetCode", "", false);
        showErrorMessageBelowCtrl("CommonSetCode", "The Set Code field is required.", true);
        check = false;
    }

    if (check) {
        var examId = $("#ExamId").val();
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForDrawingManualEntryTable",
            cache: false,
            async: true,
            data: { "id": examId, "totalRows": totalRows, "examType": examType, "commonSetCode": commonSetCode },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (!result.hasOwnProperty("IsSuccess")) {
                    $("#manualMarksEntry").html(result);
                    $('#marksPanel').keydown(function (e) {
                        if (e.keyCode == 13) {
                            // responseReturn();
                            return false;
                        }
                    });
                    $('[data-toggle="tooltip"]').tooltip();                   
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
}

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $("#manualMarksEntryForm").submit(function () {
        return false;
    });
    $('body').keydown(function (e) {
        if (e.keyCode == 13) {
            responseReturn();
        }
    });
    
    
    $("#displayPrnNo").click(function () {
        responseReturn();
    });
    $("#TotalRows").val("40");

    $("#Organization").change(function () {
        $("#manualMarksEntry").empty();
        $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria.</th></tr></thead>");
        $("#Program option").remove();
        $('#Program').append("<option value=''>--Select Program--</option>");

        $("#Session option").remove();
        $('#Session').append("<option value=''>--Select Session--</option>");

        $("#Course option").remove();
        $('#Course').append("<option value=''>--Select Course--</option>");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");
        if ($(this).val() != "") {
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForGetProgram",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                cache: false,
                async: true,
                data: { "organizationIds": $(this).val(), isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$.each(result.returnList, function (i, v) {
                        //    $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        $.each(response.returnProgramList, function (i, v) {
                            $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
    });
    $("#Program").change(function () {
        $("#manualMarksEntry").empty();
        $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria.</th></tr></thead>");
        $("#Session option").remove();
        $('#Session').append("<option value=''>--Select Session--</option>");
        $("#Course option").remove();
        $('#Course').append("<option value=''>--Select Course--</option>");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");
        var organizationId = $("#Organization").val();
        var programId = $("#Program").val();

        if (organizationId != "" && programId!="") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                cache: false,
                async: true,
                data: { organizationIds: organizationId, programIds: programId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$.each(result.returnList, function (i, v) {
                        //    $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        $.each(response.returnSessionList, function (i, v) {
                            $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }    
    });
    $("#Session").change(function () {
        $("#manualMarksEntry").empty();
        $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria.</th></tr></thead>");
        $("#Course option").remove();
        $('#Course').append("<option value=''>--Select Course--</option>");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");
        var organizationId = $("#Organization").val();
        var programId = $("#Program").val();
        var sessionId = $("#Session").val();
        if (organizationId != "" && organizationId != null && programId != "" && programId != null && sessionId != "" && sessionId != null) {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
                cache: false,
                async: true,
                data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId },
                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {

                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$.each(result.returnList, function(i, v) {
                        //    $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        $.each(response.returnCourse, function (i, v) {
                            $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function(result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
        
    });
    $("#Course").change(function () {
        $("#manualMarksEntry").empty();
        $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria.</th></tr></thead>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
    });

});

$(document).on("input propertychange", "#TotalRows", function (event) {
    $("#manualMarksEntry").empty();
    $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria.</th></tr></thead>");
});


function checkRowValidity(placeHolder) {
    
    var prnNo = placeHolder.find(".prnNoClass").val();
    var validPrnNo = placeHolder.find(".prnNoClass").attr("isSuccess");
    var setCode = placeHolder.find(".setCodeClass").val();
    var maxSubject = placeHolder.attr("data-max-subject");
    var totalCompulsary = 0;
    var tatalCompulsaryFillup = 0;
    var totalField = 0;
    var totalFillup = 0;
    var isError = 0;
    

    placeHolder.find(".marks").each(function () {
        var currentField = $(this);
        var currentVal = currentField.val();
        

        totalField = totalField + 1;
        var isCompusary = currentField.attr("data-is-compulsary");
        var maxMarks = currentField.attr("data-max-marks");
        currentField.removeClass("highlight");
        if (isCompusary == "True") {          
            totalCompulsary = totalCompulsary + 1;
            if (currentVal != "" && /^(?=.*\d)\d*(?:\.\d+)?$/.test(currentVal) && parseFloat(currentVal) <= parseFloat(maxMarks)) {
                
                tatalCompulsaryFillup = tatalCompulsaryFillup + 1;
                totalFillup = totalFillup + 1;
            }
            else {
                currentField.addClass("highlight");
                //isError = isError + 1;
            }
        }
        else {
            if (currentVal != "" && /^(?=.*\d)\d*(?:\.\d+)?$/.test(currentVal) && parseFloat(currentVal) <= parseFloat(maxMarks)) {
                totalFillup = totalFillup + 1;
            }
            else {
                
                if (currentVal != "") {
                    if (parseFloat(currentVal) > parseFloat(maxMarks)) {
                        currentField.addClass("highlight");
                        console.log(currentVal + " " + maxMarks + " " + (parseFloat(currentVal) <= parseFloat(maxMarks)));
                        isError = isError + 1;
                    }
                }
            }
        }

        if (totalFillup > parseInt(maxSubject)) {
            currentField.addClass("highlight");
        }
        //console.log(/^(?=.*\d)\d*(?:\.\d+)?$/.test(currentVal));

    });

    console.log("prnNo: "+validPrnNo);
    console.log("setCode: " + setCode);
    console.log("totalFillup: " + totalFillup);
    console.log("maxSubject: " + maxSubject);
    console.log("totalCompulsary: " + totalCompulsary);
    console.log("tatalCompulsaryFillup: " + tatalCompulsaryFillup);
    console.log("isError: " + isError);

    if (validPrnNo == "True" && setCode != "" && totalFillup <= parseInt(maxSubject) && totalCompulsary == tatalCompulsaryFillup && isError <= 0 && totalFillup>0) {
        placeHolder.find(".prnNoClass").removeClass("highlight");
        placeHolder.find(".setCodeClass").removeClass("highlight");
        placeHolder.find(".marks").removeClass("highlight");
        placeHolder.removeClass("danger");
        placeHolder.addClass("info");
        return 1;
    }
    //else if (validPrnNo == "" && totalFillup == 0 && isError <= 0) {
    //        placeHolder.find(".marks").removeClass("highlight");
    //        placeHolder.removeClass("danger");
    //        placeHolder.addClass("info");
    //}
    else if (validPrnNo == "" && totalFillup == 0 && isError <= 0) {
        if (prnNo != "" && totalFillup == 0) {
            placeHolder.removeClass("info");
            placeHolder.addClass("danger");

            return 3;
        }
        placeHolder.find(".prnNoClass").removeClass("highlight");
        placeHolder.find(".setCodeClass").removeClass("highlight");
        placeHolder.find(".marks").removeClass("highlight");
        placeHolder.removeClass("danger");
        placeHolder.removeClass("info");
        return 2;
    } 
    
    else {
        
        if (prnNo == "") {
            placeHolder.find(".prnNoClass").addClass("highlight");
        }

        

        if (setCode == "") {
            placeHolder.find(".setCodeClass").addClass("highlight");
        }
        else {
            placeHolder.find(".setCodeClass").removeClass("highlight");
        }

        placeHolder.removeClass("info");
        placeHolder.addClass("danger");

        return 3;
    }
}

$(document).on("blur", ".marks,.setCodeClass ", function (event) {
    checkRowValidity($(this).parent().parent());
});

$(document).on("input propertychange", ".marks ", function (event) {
    var input = $(this),
    text = input.val().replace(/[^0-9.]/g, "");    
    input.val(text);
});

$(document).on("input propertychange", ".setCodeClass ", function (event) {
    if (!(/^[A-Ja-j]+$/.test($(this).val()))) {
        $(this).val($(this).val().slice(0, -1));
    }
    else {
        console.log($(this).val());
        if ((/^[a-j]+$/.test($(this).val()))) {
            $(this).val( $(this).val().toUpperCase());
        }
    }
});


$(document).on("click", "#saveMarks", function (event) {
    var validRow = 0;
    var inValidRow = 0;
    var blankRow = 0;
    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var courseId = $("#Course").val();
    var examId = $("#ExamId").val();
    var examType = $("#ExamType").val();

    var dataArray = [];

    var isValidRow;

    $("#manualMarksEntry tbody tr").not(':last').each(function () {
        
        isValidRow = checkRowValidity($(this));
        //console.log(isValidRow);

        if (isValidRow == 1) {
            var marks = [];
            validRow = validRow + 1;
            obj = {};
            
            obj["Sl"] = $(this).attr("id").slice(6);
            obj["PrnNo"] = $(this).find(".prnNoClass").val();  
            obj["SetCode"] = $(this).find(".setCodeClass").val();

            $(this).find(".marks").each(function() {
                obj1 = {};
                
                obj1["ExamDetailsId"] = $(this).attr("data-id");
                obj1["Marks"] = $(this).val();
                marks.push(obj1);
            });
            obj["MarksList"] = marks;
            dataArray.push(obj);
        }
        else if (isValidRow == 3) {
            inValidRow = inValidRow + 1;
        }
        else {
            blankRow = blankRow + 1;
        }
    });
    
    if (organizationId!="" && programId != "" && sessionId != "" && courseId != "" && examId != "" && examType != "" && dataArray.length > 0) {
        var data = JSON.stringify({ "Program": programId, "Session": sessionId, "Course": courseId, "ExamId": examId, "ExamType": examType, "StudentMarksList": dataArray });
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForSubmitStudentMarks",
            cache: false,
            async: true,
            dataType: "json",
            contentType: "application/json",
            data: data,
            beforeSend: function() {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function(result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    var successRollNumbers = "";
                    var warningRollNumbers = "";
                    var errorRollNumbers = "";
                    $.each(result.returnList, function(i, v) {
                        var rowId = "rowId_" + v.Key;
                        $.each(v.Group, function (ii, vv) {
                            console.log(vv);
                            if (vv.mesageType == 0) {
                                $("#" + rowId).removeClass("info").addClass("success");
                            }
                            else if (vv.mesageType == 1) {
                                $("#" + rowId).removeClass("info").addClass("warning");
                                //$("#" + rowId).attr('errorMessage', 'Marks Already Exist for Roll Number: ' + $("#" + rowId).find(".prnNoClass").val());
                                ///$("#" + rowId).find("td:first").append("<>");

                            }
                            //else if (vv.mesageType == 2) {
                            //    $("#" + rowId).removeClass("info").addClass("danger");
                            //    $("#" + rowId).attr('errorMessage', 'Something went wrong, possible reason slow internet connection! for Roll Number: ' + $("#" + rowId).find(".prnNoClass").val());
                            //}
                            //else if (vv.mesageType == 3) {
                            //    $("#" + rowId).removeClass("info").addClass("danger");
                            //    $("#" + rowId).attr('errorMessage', 'Something went wrong, possible reason slow internet connection! for Roll Number: ' + $("#" + rowId).find(".prnNoClass").val());

                            //}
                            else {
                                $("#" + rowId).removeClass("info").addClass("danger");
                                //$("#" + rowId).attr('errorMessage', 'Something went wrong, possible reason slow internet connection! for Roll Number: ' + $("#" + rowId).find(".prnNoClass").val());
                            }
                        });
                        if ($("#" + rowId).hasClass("success")) {
                            $("#" + rowId).removeClass("success").addClass("active");
                            successRollNumbers = successRollNumbers + $("#" + rowId).find(".prnNoClass").val() + ", ";
                            $("#" + rowId).find(".prnNoClass").val("");
                            //$("#" + rowId).find(".setCodeClass").val("");
                            $("#" + rowId).find(".marks").val("");
                        }
                        if ($("#" + rowId).hasClass("danger")) {
                            errorRollNumbers = errorRollNumbers+$("#" + rowId).find(".prnNoClass").val()+", ";
                            
                            //$.fn.customMessage({
                            //    displayMessage: $("#" + rowId).attr("errorMessage"),
                            //    displayMessageType: "error",
                            //    displayTime: 50000
                            //});
                        }
                        if ($("#" + rowId).hasClass("warning")) {
                            warningRollNumbers = warningRollNumbers + $("#" + rowId).find(".prnNoClass").val() + ", ";

                            //$.fn.customMessage({
                            //    displayMessage: $("#" + rowId).attr("errorMessage"),
                            //    displayMessageType: "error",
                            //    displayTime: 50000
                            //});
                        }
                    });

                    successRollNumbers = successRollNumbers.slice(0, -2);
                    errorRollNumbers = errorRollNumbers.slice(0, -2);
                    warningRollNumbers = warningRollNumbers.slice(0, -2);

                    if (successRollNumbers != "") {
                        $.fn.customMessage({
                            displayMessage: "Marks Entry Successful for Roll No: " + successRollNumbers,
                            displayMessageType: "success",
                            displayTime: 100000
                        });
                    }
                    if (warningRollNumbers != "") {
                        $.fn.customMessage({
                            displayMessage: "Marks Already Exist for Roll No: " + warningRollNumbers,
                            displayMessageType: "w",
                            displayTime: 100000
                        });
                    }
                    if (errorRollNumbers != "") {
                        $.fn.customMessage({
                            displayMessage: "Something went wrong for Roll No: " + successRollNumbers,
                            displayMessageType: "e",
                            displayTime: 100000
                        });
                    }



                } else {
                    $.fn.customMessage({
                        displayMessage: "Something went wrong, possible reason double entry!",
                        displayMessageType: "error",
                        displayTime: 50000
                    });
                }
                $("body, html").animate({
                    scrollTop: $("body").offset().top
                }, 600);
            },
            complete: function() {
                $.unblockUI();
            },
            error: function(result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
    else {
        var firstErrorId = $("#manualMarksEntry tbody .danger:first").attr("id");
       
        if (firstErrorId == undefined) {
            firstErrorId = "marksPanel";
        }
        else {
            if (parseInt(firstErrorId.slice(6)) < 3) {
                firstErrorId = "marksPanel";
            }
            else {
                firstErrorId = "rowId_" + (parseInt(firstErrorId.slice(6))-3);
            }
        }
       // console.log(firstErrorId);
        $("body, html").animate({
            scrollTop: $("#" + firstErrorId).offset().top
        }, 600);
    }
    if (dataArray.length == 0) {
        $.fn.customMessage({
            displayMessage: "Please entry atleast one student marks",
            displayMessageType: "error",
            displayTime: 50000
        });
        $("body, html").animate({
            scrollTop: $("body").offset().top
        }, 600);
    }
});

$(document).on("blur", ".prnNoClass", function (event) {
    var placeHolder = $(this);
    var prnNo = $(this).val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    $(this).removeClass("highlight");
    if (/^[0-9]{11}$/.test(prnNo)) {      
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForStudentCheck",
            cache: false,
            async: true,
            data: { "prnNo": prnNo, "programId": programId, "sessionId": sessionId },
            beforeSend: function () {
                //$.blockUI({
                //    timeout: 0,
                //    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                //});
            },
            success: function (result) {
                //$.unblockUI();
                if (result.IsSuccess) {
                    placeHolder.attr("isSuccess","True");
                }
                else {
                    placeHolder.addClass("highlight");
                    placeHolder.attr("isSuccess", "False");
                }
            },
            complete: function () {
                //$.unblockUI();
            },
            error: function (result) {
                //$.unblockUI();
                console.log("Failed -----");
            }
        });       
    }
    else if (prnNo=="") {
        placeHolder.attr("isSuccess", "");
    }
    else {
        $(this).addClass("highlight");
        placeHolder.attr("isSuccess", "False");
    }
    checkRowValidity(placeHolder.parent().parent());
});