﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IEmploymentHistoryDao : IBaseDao<EmploymentHistory, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        EmploymentHistory GetTeamMemberEmploymentHistory(DateTime? searchingDateTime, long? teamMemberId, int? teamMemberPin);
        EmploymentHistory GetTeamMemberJoiningEmploymentHistory(long teamMemberId);
        EmploymentHistory GetTeamMemberPermanentEmploymentHistory(long teamMemberId);
        EmploymentHistory GetTeamMemberFirstMonth(long tmId, int currentMonth, int currentYear);
        #endregion

        #region List Loading Function

        IList<EmploymentHistory> LoadByMemberId(long memId, DateTime? searchingDate = null);

        #endregion

        #region Others Function

        void GetCurrentInformations(long id, out object hrDesignation, out object hrDepartment, out object organization, DateTime? searchingDate = null);

        void GetCurrentInformations(long id, out int empStatus, out object hrDesignation, out object hrDepartment, out object organization, DateTime? searchingDate = null);

        #endregion

        #region Helper Function

        #endregion


    }
    public class EmploymentHistoryDao : BaseDao<EmploymentHistory, long>, IEmploymentHistoryDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public EmploymentHistory GetTeamMemberFirstMonth(long tmId, int currentMonth, int currentYear)
        {
            string query = @"select eh.* from(
                                Select a.TeamMemberId,a.EffectiveDate,a.Id from (
		                                Select 
			                                o.[Id] as OrganizationId
			                                ,b.[Id] as BranchId
			                                ,eh.[CampusId]
			                                ,eh.[DesignationId]
			                                ,eh.[DepartmentId]
			                                ,eh.[TeamMemberId]
			                                ,eh.[EffectiveDate]
			                                ,eh.[EmploymentStatus]
                                            ,eh.[Id]
			                                ,RANK() OVER(PARTITION BY eh.TeamMemberId ORDER BY eh.EffectiveDate ASC, eh.Id ASC) AS R  
			                                from [HR_EmploymentHistory] as eh
			                                inner join Campus as c on c.Id = eh.CampusId  and c.Status = 1
			                                inner join Branch as b on b.Id = c.BranchId and b.Status = 1
			                                inner join Organization as o on o.Id = b.OrganizationId and o.Status = 1 
			                                Where eh.Status = 1
	                                ) as a where a.R=1  
	                                and a.EmploymentStatus != 6 
	                                and Month(a.EffectiveDate) = " + currentMonth + @" and YEAR(a.EffectiveDate) = " + currentYear + @"
	                                and a.TeamMemberId = " + tmId + @"
                                ) as b inner join [HR_EmploymentHistory] as eh on eh.Id = b.Id";
            IQuery iQuery = Session.CreateSQLQuery(query).AddEntity("Eh", typeof(EmploymentHistory));

            iQuery.SetResultTransformer(Transformers.AliasToBean<EmploymentHistory.EmploymentHistoryQueryTransfer>());
            var res = iQuery.List<EmploymentHistory.EmploymentHistoryQueryTransfer>().ToList();
            return res.Select(employmentHistoryDto => employmentHistoryDto.Eh).FirstOrDefault();

        }
        public EmploymentHistory GetTeamMemberEmploymentHistory(DateTime? searchingDateTime, long? teamMemberId, int? teamMemberPin)
        {
            if (teamMemberId != null)
                return Session.QueryOver<EmploymentHistory>().Where(x => x.TeamMember.Id == teamMemberId
                    && x.EffectiveDate <= searchingDateTime
                    && x.Status == EmploymentHistory.EntityStatus.Active
                ).OrderBy(x => x.EffectiveDate).Desc.List<EmploymentHistory>().FirstOrDefault();

            return Session.QueryOver<EmploymentHistory>().Where(x => x.TeamMember.Pin == teamMemberPin
                    && x.EffectiveDate <= searchingDateTime
                    && x.Status == EmploymentHistory.EntityStatus.Active
                ).OrderBy(x => x.EffectiveDate).Desc.List<EmploymentHistory>().FirstOrDefault();
        }

        public EmploymentHistory GetTeamMemberJoiningEmploymentHistory(long teamMemberId)
        {
            return Session.QueryOver<EmploymentHistory>().Where(x => x.TeamMember.Id == teamMemberId && x.Status == EmploymentHistory.EntityStatus.Active).OrderBy(x => x.EffectiveDate).Asc.ThenBy(x => x.Id).Asc.List<EmploymentHistory>().FirstOrDefault();
        }

        public EmploymentHistory GetTeamMemberPermanentEmploymentHistory(long teamMemberId)
        {
            return Session.QueryOver<EmploymentHistory>().Where(x => x.TeamMember.Id == teamMemberId && x.Status == EmploymentHistory.EntityStatus.Active && x.EmploymentStatus == (int)MemberEmploymentStatus.Permanent).OrderBy(x => x.EffectiveDate).Asc.ThenBy(x => x.Id).Asc.List<EmploymentHistory>().FirstOrDefault();
        }

        #endregion

        #region List Loading Function
        public IList<EmploymentHistory> LoadByMemberId(long memId, DateTime? searchingDate)
        {
            if (searchingDate != null)
                return Session.QueryOver<EmploymentHistory>().Where(x => x.TeamMember.Id == memId && x.EffectiveDate <= searchingDate && x.Status == EmploymentHistory.EntityStatus.Active).OrderBy(x => x.EffectiveDate).Desc.List<EmploymentHistory>();
            return Session.QueryOver<EmploymentHistory>().Where(x => x.TeamMember.Id == memId && x.Status == EmploymentHistory.EntityStatus.Active).OrderBy(x => x.EffectiveDate).Desc.List<EmploymentHistory>();

        }

        #endregion

        #region Others Function

        public void GetCurrentInformations(long id, out object hrDesignation, out object hrDepartment, out object organization, DateTime? searchingDate = null)
        {
            hrDesignation = null;
            hrDepartment = null;
            organization = null;
            int i = 0;
            var currentDesignation = new Designation();
            EmploymentHistory employmentHistory = null;
            if (searchingDate == null)
            {
                employmentHistory =
                    Session.QueryOver<EmploymentHistory>()
                        .Where(x => x.TeamMember.Id == id && x.EffectiveDate <= DateTime.Now.Date)
                        .OrderBy(x => x.EffectiveDate)
                        .Desc.Take(1).SingleOrDefault<EmploymentHistory>();

            }
            else
            {
                employmentHistory =
                   Session.QueryOver<EmploymentHistory>()
                       .Where(x => x.TeamMember.Id == id && x.EffectiveDate <= searchingDate.Value.Date)
                       .OrderBy(x => x.EffectiveDate)
                       .Desc.Take(1).SingleOrDefault<EmploymentHistory>();
            }
            if (employmentHistory != null)
            {
                hrDesignation = employmentHistory.Designation;
                hrDepartment = employmentHistory.Department;
                organization = employmentHistory.Campus.Branch.Organization;
            }
            //var employmentHistory = 
            //    Session.QueryOver<EmploymentHistory>()
            //        .Where(x => x.TeamMember.Id == id)
            //        .OrderBy(x => x.EffectiveDate)
            //        .Desc.List<EmploymentHistory>();
            //foreach (var hrEmploymentHistory in employmentHistory)
            //{
            //    if (hrEmploymentHistory.EffectiveDate.Date >= DateTime.Now.Date)
            //    {
            //        i++;
            //    }
            //    else
            //    {
            //        var curreHistory = employmentHistory.Skip(i).Take(1).FirstOrDefault();
            //        if (curreHistory != null)
            //        {
            //            hrDesignation = curreHistory.Designation;
            //            hrDepartment = curreHistory.Department;
            //            organization = curreHistory.Campus.Branch.Organization;
            //        }
            //        break;
            //    }
            //}
        }

        public void GetCurrentInformations(long id, out int empStatus, out object hrDesignation, out object hrDepartment,
            out object organization, DateTime? searchingDate = null)
        {
            hrDesignation = null;
            hrDepartment = null;
            organization = null;
            empStatus = 0;

            EmploymentHistory employmentHistory = null;
            if (searchingDate == null)
            {
                employmentHistory =
                    Session.QueryOver<EmploymentHistory>()
                        .Where(x => x.TeamMember.Id == id && x.EffectiveDate <= DateTime.Now.Date)
                        .OrderBy(x => x.EffectiveDate)
                        .Desc.Take(1).SingleOrDefault<EmploymentHistory>();

            }
            else
            {
                employmentHistory =
                   Session.QueryOver<EmploymentHistory>()
                       .Where(x => x.TeamMember.Id == id && x.EffectiveDate <= searchingDate.Value.Date)
                       .OrderBy(x => x.EffectiveDate)
                       .Desc.Take(1).SingleOrDefault<EmploymentHistory>();
            }
            if (employmentHistory != null)
            {
                hrDesignation = employmentHistory.Designation;
                hrDepartment = employmentHistory.Department;
                organization = employmentHistory.Campus.Branch.Organization;
                empStatus = employmentHistory.EmploymentStatus;
            }
        }

        #endregion

        #region Helper Function



        #endregion
    }

}
