﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Security.Policy;
using UdvashERP.Api;

namespace UdvashERP.Api.Controllers
{
    public class AttendanceUpdaterController : Controller
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AttendanceUpdater");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IAttendanceSynchronizerService _hrAttendanceSynchronizerService;
        private readonly IAttendanceSynchronizerSettingService _hrAttendanceSynchronizerSettingService;
        public AttendanceUpdaterController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _hrAttendanceSynchronizerService = new AttendanceSynchronizerService(session);
                _hrAttendanceSynchronizerSettingService = new AttendanceSynchronizerSettingService(session);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.Message);
            }
        }
        #endregion

        [HttpGet]
        public JsonResult GetSoftwareUpdate(string key, double versionNumber)
        {
            //string basePath = "https://dev.umsbd.net/Uploads/DeviceSyncUpdate/";
            string retUrl = "";
            var attendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(key);

            if (attendanceSynchronizer == null)
                return Json(new JsonResponse(false, "Invalid Key"), JsonRequestBehavior.AllowGet);

            var attendanceSynchronizerSettings = _hrAttendanceSynchronizerSettingService.GetNextAttendanceSynchronizerSetting(versionNumber);
            if (attendanceSynchronizerSettings != null)
            {
                retUrl = attendanceSynchronizerSettings.UpdateUrl;
            }
            else
            {
                return Json(new JsonResponse(false, "Your software is updated"), JsonRequestBehavior.AllowGet);
            }

            return Json(new JsonResponse(true, retUrl), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult LogSoftwareUpdate(string key, double versionNumber, bool isSuccess, string message)
        {
            var attendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(key);
            if (attendanceSynchronizer == null)
                return Json(new JsonResponse(false, "Invalid Key"), JsonRequestBehavior.AllowGet);

            return Json(new JsonResponse(true, "Loge saved"), JsonRequestBehavior.AllowGet);
        }
    }
}