﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Exam;
using UdvashERP.MessageExceptions;

using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Exam.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Exam")]
    public class ExamMcqQuestionSetPrintController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly IOrganizationService _organizationService;
        private readonly IExamMcqQuestionSetPrintService _examMcqQuestionSetPrintService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _userMenu;
        public ExamMcqQuestionSetPrintController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _organizationService = new OrganizationService(session);
                _examMcqQuestionSetPrintService = new ExamMcqQuestionSetPrintService(session);
                _userService = new UserService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Manage

        [HttpGet]
        public ActionResult PrintReport()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                organizations.Insert(0, new Organization() { ShortName = "All", Id = 0 });
                ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");
                var printStatusDictonary = _commonHelper.LoadEmumToDictionary<PrintStatus>();
                ViewBag.PrintStatusList = new SelectList(printStatusDictonary, "Key", "Value");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            return View();
        }

        [HttpPost]
        public ActionResult PrintReportResult(int draw, int start, int length, string organizationId, string programId, string sessionId, string courseId, string exam, string uniqueResult, string codeNo, string printStatus)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    string selectAll = SelectionType.SelelectAll.ToString();
                    var convertedOrganizationIdList = String.IsNullOrEmpty(organizationId) || organizationId == selectAll ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId));
                    var convertedProgramIdList = String.IsNullOrEmpty(programId) || programId == selectAll ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(programId));
                    var convertedSessionIdList = String.IsNullOrEmpty(sessionId) || sessionId == selectAll ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(sessionId));
                    var convertedCourseIdList = String.IsNullOrEmpty(courseId) || courseId == selectAll ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(courseId));
                    int? uniqueRes = null;
                    if (!string.IsNullOrEmpty(uniqueResult))
                    {
                        uniqueRes = Convert.ToInt32(uniqueResult);
                    }
                    int? printStat = null;
                    if (!string.IsNullOrEmpty(printStatus) && printStatus != "-1")
                    {
                        printStat = Convert.ToInt32(printStatus);
                    }
                    int recordsTotal = _examMcqQuestionSetPrintService.GetExamMcqQuestionSetPrintReportCount(_userMenu, convertedOrganizationIdList, convertedProgramIdList, convertedSessionIdList, convertedCourseIdList, exam, uniqueRes, codeNo, printStat);
                    List<ExamMcqQuestionSetPrint> examMcqQuestionSetPrintList =
                            _examMcqQuestionSetPrintService.GetExamMcqQuestionSetPrintReportList(start, length, _userMenu, convertedOrganizationIdList, convertedProgramIdList, convertedSessionIdList, convertedCourseIdList, exam, uniqueRes, codeNo, printStat).ToList();

                    int recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    foreach (var c in examMcqQuestionSetPrintList)
                    {
                        var str = new List<string>();
                        str.Add(c.Exam.Name + " (" + c.Exam.Code + ")");
                        str.Add(c.UniqueSet.ToString());
                        str.Add(c.SetNo);
                        str.Add(c.CodeNo.ToString());
                        str.Add(_commonHelper.GetEmumIdToValue<PrintStatus>(c.PrintStatus));
                        var printedBy = _userService.LoadAspNetUserById(c.PrintBy);
                        str.Add(printedBy.Email);
                        str.Add(c.PrintDate.ToString("dd MMM, yyyy HH:mm:ss tt"));
                        str.Add(c.Exam.Program.Organization.ShortName);
                        str.Add(c.Exam.Program.Name);
                        str.Add(c.Exam.Session.Name);
                        data.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        #endregion
    }
}
