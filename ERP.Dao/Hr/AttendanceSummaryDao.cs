﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using NHibernate.Criterion;

namespace UdvashERP.Dao.Hr
{
    public interface IAttendanceSummaryDao : IBaseDao<AttendanceSummary, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        AttendanceSummary GetByPinAndDate(int pin, DateTime attendenceDate);
        AttendanceSummary GetTeamMemberLastIsPostAttendanceSummary(long? teamMemberId, int? pin = null);

        #endregion

        #region List Loading Function

        IList<AttendanceSummary> LoadTeamMemberDayOffAttendanceSummary(TeamMember teamMember, long id = 0);
        //IList<AttendanceSummary> ListAttendanceSummary(TeamMember teamMember);
        IList<AttendanceSummary> ListAttendanceSummary(TeamMember teamMember, DateTime startDate, DateTime endDate);
        #endregion

        #region Others Function
        //int GetAttendanceSummeryReportCount(List<long> authorizedBranchIdList, string organization, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin);
        int GetAttendanceSummeryReportCount(List<long> organizationIdList, List<long> branchIdList, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin);
        #endregion

        #region Helper Function

        #endregion

        //List<AttendanceSummeryReportDto> LoadAttendanceSummeryReport(List<long> authorizedBranchIdList, string organization, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin, int start, int length, string orderBy, string orderDir);
        List<AttendanceSummeryReportDto> LoadAttendanceSummeryReport(List<long> organizationIdList, List<long> branchIdList, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin, int start, int length, string orderBy, string orderDir);

    }

    public class AttendanceSummaryDao : BaseDao<AttendanceSummary, long>, IAttendanceSummaryDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Functions

        #endregion

        #region Single Instances Loading Function

        public AttendanceSummary GetByPinAndDate(int pin, DateTime attendenceDate)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceSummary>()
                //.Add(Restrictions.Eq("Status", AttendanceSummary.EntityStatus.Active))
                                .Add(Restrictions.Eq("AttendanceDate", attendenceDate));

            criteria.CreateAlias("TeamMember", "member")
                                .Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
                                .Add(Restrictions.Eq("member.Pin", pin));
            criteria.SetTimeout(3000);
            var list = criteria.List<AttendanceSummary>().FirstOrDefault();
            return list;
        }

        public AttendanceSummary GetTeamMemberLastIsPostAttendanceSummary(long? teamMemberId, int? pin = null)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<AttendanceSummary>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", AttendanceSummary.EntityStatus.Delete))); //Add(Restrictions.Eq("Status", NightWork.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("IsPosted", true));
            criteria.CreateAlias("TeamMember", "member");//.Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("AttendanceDate"));
            return criteria.List<AttendanceSummary>().Take(1).FirstOrDefault();
        }

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        public IList<AttendanceSummary> ListAttendanceSummary(TeamMember teamMember, DateTime startDate, DateTime endDate)
        {
            var list = Session.QueryOver<AttendanceSummary>()
                            .Where(x => x.Status == AttendanceSummary.EntityStatus.Active)
                            .Where(x => x.TeamMember.Id == teamMember.Id)
                            .Where(x => x.AttendanceDate >= startDate)
                            .Where(x => x.AttendanceDate <= endDate)
                            .OrderBy(x => x.AttendanceDate).Asc
                            .List<AttendanceSummary>();
            return list.Count > 0 ? list : null;
        }

        //public int GetAttendanceSummeryReportCount(List<long> authorizedBranchIdList, string organization, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin)
        public int GetAttendanceSummeryReportCount(List<long> organizationIdList, List<long> branchIdList, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin)
        {
            //string innerQuery = GetInnerQueryAttendanceSummeryReport(authorizedBranchIdList, organization, department, branch, informationViewList, campus, dateFrom, dateTo,pin);
            string innerQuery = GetInnerQueryAttendanceSummeryReport(organizationIdList, branchIdList, department, branch, informationViewList, campus, dateFrom, dateTo, pin);
            string query = "SELECT count(A.TeamMemberId) as total FROM(" + innerQuery + ") as A";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2000);
            var a = Convert.ToInt32(iQuery.UniqueResult());
            return a;
        }

        //public List<AttendanceSummeryReportDto> LoadAttendanceSummeryReport(List<long> authorizedBranchIdList, string organization, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin, int start, int length, string orderBy, string orderDir)
        public List<AttendanceSummeryReportDto> LoadAttendanceSummeryReport(List<long> organizationIdList, List<long> branchIdList, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin, int start, int length, string orderBy, string orderDir)
        {
            //string innerQuery = GetInnerQueryAttendanceSummeryReport(authorizedBranchIdList, organization, department, branch, informationViewList, campus, dateFrom, dateTo,pin);
            string innerQuery = GetInnerQueryAttendanceSummeryReport(organizationIdList, branchIdList, department, branch, informationViewList, campus, dateFrom, dateTo, pin);

            string pagination = " ";
            if (length > 0)
            {
                pagination = " where pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }

            string query = @"SET ANSI_WARNINGS OFF
                            DECLARE @tempdate AS TABLE ([Date] datetime);
                            DECLARE  @StartDate DATETIME
                                    ,@EndDate DATETIME
		                            ,@TempStartDate DATETIME;

                            DECLARE @rowsperpage INT 
                            DECLARE @start INT 
                            SET @start = " + (start + 1) + @"
                            SET @rowsperpage = " + length + @"

                            SET DATEFIRST 6;
                            SELECT   @EndDate = '" + dateTo + @"'
                                    ,@StartDate = '" + dateFrom + @"'
		                            ,@TempStartDate = '" + dateFrom + @"';

                            while (@TempStartDate <= @EndDate)
                            BEGIN
                                INSERT INTO @tempdate([Date]) VALUES (@TempStartDate);
                                SET @TempStartDate = dateadd(DAY, 1, @TempStartDate)
                            END
                            
                            DECLARE @ZoneWithCountTable TABLE (
							TeamMemberId bigint,
							zonenme nvarchar(255),
							ZoneId bigint,
							zonecount int,
							toleranceTime int
							);
							insert @ZoneWithCountTable
							select asum.TeamMemberId,zone.Name zonenme, ZoneId,COUNT(*) zonecount,zone.ToleranceTime toleranceTime from HR_AttendanceSummary asum 
							left join HR_ZoneSetting zone on zone.Id = asum.ZoneId and  AttendanceDate>=@StartDate and AttendanceDate<=@EndDate
                            LEFT JOIN (
					                Select [TeamMemberId],[DateFrom],[DateTo],[LeaveStatus] from [dbo].[HR_LeaveApplication] Where [LeaveStatus] = " + (int)LeaveStatus.Approved + @"
				                ) as l  ON l.[TeamMemberId] = asum.TeamMemberId AND asum.[AttendanceDate]>=l.DateFrom AND asum.[AttendanceDate]<=l.DateTo 						
							where  l.TeamMemberId is null and IsWeekend<=0 and HolidaySettingId is null
							group by asum.TeamMemberId,zone.Name,ZoneId,zone.ToleranceTime
                            
                            Select * INTO #FilterTeamMemberTemp From (" + innerQuery + @") as a

                            DECLARE @leaveDetailsTable TABLE (
								TeamMemberId bigint,								
								leaveDetails nvarchar(MAX)
							);
							Insert @leaveDetailsTable
							Select a.TeamMemberId, CONCAT(l.Name,'(',a.leaveCount,')') as leaveDetails  from (
								Select a.[TeamMemberId],a.LeaveId, count(LeaveId) as leaveCount from (
									Select a.TeamMemberId,
									l.[LeaveId]
									from(
									 Select * from  @tempdate,#FilterTeamMemberTemp ) as a
									 LEFT JOIN (
										Select [TeamMemberId],[DateFrom],[DateTo],[LeaveStatus],[LeaveId] from [dbo].[HR_LeaveApplication] Where [LeaveStatus] = " + (int)LeaveStatus.Approved + @"
									) as l  ON l.[TeamMemberId] = a.TeamMemberId AND a.Date>=l.DateFrom AND a.Date<=l.DateTo 
									where l.LeaveId is not NULL
								) as a Group By [TeamMemberId],[LeaveId]
							) as a 
							left join [dbo].[HR_Leave] as l on l.Id = a.LeaveId 

                            Select * FROM (
	                            SELECT row_number() OVER (ORDER BY A.TeamMemberId asc) AS RowNum, A.*,z.Name as Zone, zonewithcount.zonenamewithcount  ZoneNameWithCount,ld.leaveDetails as LeaveDetails FROM (
		                            Select a.TeamMemberId 
			                            ,a.OrganizationId as OrganizationId
			                            ,a.Pin as Pin
			                            ,a.Name as Name
			                            ,a.MemberType as MemberType
			                            ,a.Department as Department
			                            ,a.Designation as Designation
			                            ,SUM(
				                            (DATEPART(hh,a.EarlyEntry)*3600)+
				                            (DATEPART(mi,a.EarlyEntry)*60)+
				                            DATEPART(ss,a.EarlyEntry)
				                            )  as EarlyEntry
			                            ,SUM(
				                            (DATEPART(hh,a.LateLeave)*3600)+
				                            (DATEPART(mi,a.LateLeave)*60)+
				                            DATEPART(ss,a.LateLeave)
				                            )  as LateLeave
			                            ,SUM(
				                            (DATEPART(hh,a.TotalOvertime)*3600)+
				                            (DATEPART(mi,a.TotalOvertime)*60)+
				                            DATEPART(ss,a.TotalOvertime)
				                            )  as TotalOvertime
			                            ,SUM(
				                            (DATEPART(hh,a.ApprovedOvertime)*3600)+
				                            (DATEPART(mi,a.ApprovedOvertime)*60)+
				                            DATEPART(ss,a.ApprovedOvertime)
				                            )  as ApprovedOvertime
			                            ,SUM(
				                            (DATEPART(hh,a.LateEntry)*3600)+
				                            (DATEPART(mi,a.LateEntry)*60)+
				                            DATEPART(ss,a.LateEntry)
				                            )  as TotalLateEntry
			                             ,SUM(case when a.LateEntry is not null then 1 else 0 end) NoofLateEntry

			                            ,SUM(
				                            (DATEPART(hh,a.EarlyLeave)*3600)+
				                            (DATEPART(mi,a.EarlyLeave)*60)+
				                            DATEPART(ss,a.EarlyLeave)
				                            )  as TotalEarlyLeave
			                             ,SUM(case when a.EarlyLeave is not null then 1 else 0 end) NoofEarlyLeave
			                            ,SUM(a.absentCount) as  AbsentCount
			                            ,SUM(a.leave) as  LeaveCount
			                            ,SUM(a.noofWorkingDay+a.noofExtraDay) as  NoofWorkingDay
			                            ,SUM(a.noofExtraDay) as  NoofExtraDay
                                        ,CONCAT(
											CASE WHEN SUM(a.WeekendHolidayExtraDay) >0
												THEN CONCAT(' Weekend (',SUM(a.WeekendHolidayExtraDay),')')
												ELSE ''
											END
											,
											CASE WHEN SUM(a.GazettedHolidayExtraDay) >0
												THEN CONCAT(' Gazetted Holiday (',SUM(a.GazettedHolidayExtraDay),')')
												ELSE ''
											END
											,
											CASE WHEN SUM(a.ManagementHolidayExtraDay) >0
												THEN CONCAT(' Management Holiday (',SUM(a.ManagementHolidayExtraDay),')')
												ELSE ''
											END
										) as ExtraDayDetails

			                            ,SUM(a.holiday) as  NoofCalendarLeave
			                            ,SUM(a.dailyAllowance) as  TotalDailyAllowance
			                            ,SUM(a.nightWork) as  TotalNightwork
			                            ,Max(a.ToleranceTime) as MaxToleranceTime
		                            from (
			                            Select a.*
			                            ,ats.AttendanceDate 
			                            ,ats.ZoneId

			                            ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
				                            THEN null
				                            ELSE ats.EarlyEntry
			                            END) AS EarlyEntry

										 ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
				                            THEN null
				                            ELSE ats.LateEntry
			                            END) AS LateEntry

										 ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
				                            THEN null
				                            ELSE ats.EarlyLeave
			                            END) AS EarlyLeave

										 ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
				                            THEN null
				                            ELSE ats.LateLeave
			                            END) AS LateLeave
                                        ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
				                            THEN null
				                            ELSE ats.TotalOvertime
			                            END) AS TotalOvertime
			                           
			                            ,ot.ApprovedTime as ApprovedOvertime
			                            ,ats.InTime
			                            ,ats.OutTime
			                            ,ats.IsPosted
			                            ,ats.IsWeekend
			                            ,(CASE 
											WHEN nw.ApprovalType IS NULL THEN 0
											WHEN nw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Half + @" THEN 0.5
											WHEN nw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Full + @" THEN 1
				                            ELSE 0
			                            END) AS nightWork
			                            ,(CASE WHEN da.IsApproved IS NULL
				                            THEN 0
				                            ELSE da.IsApproved
			                            END) AS dailyAllowance
			                            ,z.Name AS Zone

			                            ,(CASE  WHEN ats.AttendanceDate IS NOT NULL AND a.leave=1 THEN null
                                            WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1) THEN 0
				                            ELSE z.ToleranceTime
			                            END) AS ToleranceTime

			                            ,(CASE WHEN ats.AttendanceDate IS NULL AND a.holiday=0 AND a.Weekend=0 AND a.leave=0 AND (do.DayOffStatus != 1 or  do.DayOffStatus IS NULL)
				                            THEN 1
				                            ELSE 0
			                            END) AS absentCount

			                             ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND a.leave=0 AND hw.ApprovalType IS NULL
				                            THEN 1
				                            ELSE 0
			                            END) AS noofWorkingDay										

                                        ,(CASE 
											WHEN hw.ApprovalType IS NULL THEN 0
											WHEN hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Half + @" THEN 0.5
											WHEN hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Full + @" THEN 1
				                            ELSE 0
			                            END) AS noofExtraDay

										,(CASE 
											WHEN hw.ApprovalType IS NOT NULL AND a.GazettedHoliday = 1 AND hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Half + @" THEN 0.5
											WHEN hw.ApprovalType IS NOT NULL AND a.GazettedHoliday = 1 AND hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Full + @" THEN 1
				                            ELSE 0
			                            END) AS GazettedHolidayExtraDay

			                           ,(CASE 
											WHEN hw.ApprovalType IS NOT NULL AND a.ManagementHoliday = 1  AND a.GazettedHoliday != 1 AND hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Half + @" THEN 0.5
				                            WHEN hw.ApprovalType IS NOT NULL AND a.ManagementHoliday = 1  AND a.GazettedHoliday != 1 AND hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Full + @" THEN 1
				                            ELSE 0
			                            END) AS ManagementHolidayExtraDay										

										,(CASE 
											WHEN hw.ApprovalType IS NOT NULL AND a.Weekend = 1  AND a.GazettedHoliday != 1  AND a.ManagementHoliday != 1 AND hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Half + @" THEN 0.5
				                            WHEN hw.ApprovalType IS NOT NULL AND a.Weekend = 1  AND a.GazettedHoliday != 1  AND a.ManagementHoliday != 1 AND hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Full + @" THEN 1
				                            ELSE 0
			                            END) AS WeekendHolidayExtraDay


			                            from (
				                            select a.*
					                            --,h.Name as holiday
					                            ,(CASE WHEN h.Name IS NULL
						                              THEN 0
						                              ELSE 1
					                             END) AS holiday
                                                 ,(CASE WHEN h.HolidayType = 1
													THEN 1
													ELSE 0
												END) AS ManagementHoliday
												,(CASE WHEN h.HolidayType = 2
													THEN 1
													ELSE 0
												END) AS GazettedHoliday
					                             ,(CASE WHEN w.Weekend IS NULL THEN 0 ELSE 
						                            CASE WHEN w.Weekend=datepart(dw,a.Date) THEN 1 ELSE 0 END
					                             END) AS Weekend
					                             ,(CASE WHEN l.LeaveStatus IS NULL
						                              THEN 0
						                              ELSE 1
					                             END) AS leave
					                            --,w.Weekend,datepart(dw,a.Date) as WeekDays 
				                            from (
					                            Select * from  @tempdate,#FilterTeamMemberTemp
				                            ) as a
				                            Left JOIN [dbo].[HR_HolidaySetting] as h ON a.OrganizationId = h.OrganizationId AND a.Date>=h.DateFrom AND a.Date<=h.DateTo AND h.Status = " + HolidaySetting.EntityStatus.Active + @"
				                            LEFT JOIN (
					                           select * from (
													Select [TeamMemberId], Date
																	,EffectiveDate
																	,[Weekend],
																	RANK() OVER(PARTITION BY TeamMemberId,date ORDER BY EffectiveDate DESC) AS R   from (
															Select [TeamMemberId]
																	,Date
																	,EffectiveDate
																	,[Weekend]
																from @tempdate as a,[dbo].[HR_ShiftWeekendHistory] as b 			
																Where [Status] = 1 
																AND a.Date>=EffectiveDate
														) as a 
													) as a WHERE a.R = 1
				                            ) AS w on w.TeamMemberId = a.TeamMemberId and w.Date = a.Date
				                            LEFT JOIN (
					                            Select [TeamMemberId],[DateFrom],[DateTo],[LeaveStatus] from [dbo].[HR_LeaveApplication] Where [LeaveStatus] = " + (int)LeaveStatus.Approved + @"
				                            ) as l  ON l.[TeamMemberId] = a.TeamMemberId AND a.Date>=l.DateFrom AND a.Date<=l.DateTo 
			                            )  as a
			                            left join (
			                            Select * from [dbo].[HR_AttendanceSummary] 
				                            Where AttendanceDate>=@StartDate and AttendanceDate<=@EndDate
			                            ) as ats ON ats.TeamMemberId  = a.TeamMemberId AND a.Date = ats.AttendanceDate
			                            left join (
			                            Select [ApprovedTime],[TeamMemberId],[OverTimeDate] from [dbo].[HR_Overtime]
				                            Where OverTimeDate>=@StartDate and OverTimeDate<=@EndDate
			                            ) as ot ON ot.TeamMemberId  = a.TeamMemberId AND a.Date = ot.OverTimeDate
			                            left join (
			                            Select [ApprovalType],[TeamMemberId],NightWorkDate from [dbo].[HR_NightWork]
				                            Where NightWorkDate>=@StartDate and NightWorkDate<=@EndDate
			                            ) as nw ON nw.TeamMemberId  = a.TeamMemberId AND a.Date = nw.NightWorkDate
			                            left join (
			                            Select [IsApproved],[TeamMemberId],[DailyAllowanceDate] from [dbo].[HR_Allowance]
				                            Where [DailyAllowanceDate]>=@StartDate and [DailyAllowanceDate]<=@EndDate 
			                            ) as da ON da.TeamMemberId  = a.TeamMemberId AND a.Date = da.DailyAllowanceDate

                                        left join (
			                            Select [ApprovalType],[TeamMemberId],[HolidayWorkDate] from [dbo].[HR_HolidayWork]
				                            Where [HolidayWorkDate]>=@StartDate and [HolidayWorkDate]<=@EndDate AND [Status] = " + HolidayWork.EntityStatus.Active + @"
			                            ) as hw ON hw.TeamMemberId  = a.TeamMemberId AND a.Date = hw.HolidayWorkDate

                                        left join (
			                            Select [DayOffStatus],[TeamMemberId],[DayOffDate],[InsteadOfDate] from [dbo].[HR_DayOffAdjustment]
				                            Where [DayOffDate]>=@StartDate and [DayOffDate]<=@EndDate AND [DayOffStatus] = " + (int)DayOffAdjustmentStatus.Approved + @"
			                            ) as do ON do.TeamMemberId  = a.TeamMemberId AND a.Date = do.DayOffDate

			                            left join [dbo].[HR_ZoneSetting] as z ON z.Id = ats.ZoneId AND z.Status = " + ZoneSetting.EntityStatus.Active + @"
			                            --WHERE a.TeamMemberId = 111
			                            --order by a.Date asc
		                            ) as a 
		                            group by a.TeamMemberId
				                            ,a.Pin
				                            ,a.Name
				                            ,a.MemberType
				                            ,a.Department 
				                            ,a.Designation
			                            ,a.OrganizationId
	                            ) as A
                                left join [dbo].[HR_ZoneSetting] as z ON z.OrganizationId = A.OrganizationId AND z.Status = 1 AND A.MaxToleranceTime = z.ToleranceTime
                                left join  (SELECT
                                     TeamMemberId,
                                     STUFF(
                                         (SELECT  ', ' + zonenme+' ('+cast(zonecount as varchar)+')'
                                          FROM @ZoneWithCountTable x
                                          WHERE TeamMemberId= a.TeamMemberId
                                          order by toleranceTime                                          
                                          FOR XML PATH (''))
                                          , 1, 1, '')  AS ZoneNameWithCount
                                FROM @ZoneWithCountTable a
                                GROUP BY TeamMemberId) zoneWithCount on zoneWithCount.teammemberid=a.teammemberid

                                left join (
									Select TeamMemberId,
									STUFF((Select ', '+ leaveDetails from @leaveDetailsTable WHERE TeamMemberId = a.TeamMemberId FOR XML PATH ('')),1,1,' ') AS leaveDetails						   
									From @leaveDetailsTable as a Group By TeamMemberId
								) as ld ON ld.TeamMemberId = a.TeamMemberId

                            ) as pagination" + pagination + @"
                             
                              IF EXISTS(SELECT * FROM #FilterTeamMemberTemp) DROP TABLE #FilterTeamMemberTemp;";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<AttendanceSummeryReportDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<AttendanceSummeryReportDto>().ToList();
        }

        #endregion

        #region Helper function
        //private string GetInnerQueryAttendanceSummeryReport(List<long> authorizedBranchIdList, string organization, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin)
        private string GetInnerQueryAttendanceSummeryReport(List<long> organizationIdList, List<long> branchIdList, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin)
        {

            string endDateFilter = " AND EffectiveDate <='" + dateTo + "'";
            string filter = "";

            //if (!String.IsNullOrEmpty(organization))
            //{
            //    filter += " AND o.Id ="+organization;
            //}
            if (organizationIdList != null && organizationIdList.Any() && !organizationIdList.Contains(SelectionType.SelelectAll))
            {
                filter += " AND o.Id  IN (" + string.Join(",", organizationIdList) + ") ";
            }

            if (!String.IsNullOrEmpty(department))
            {
                filter += " AND currentPosition.DepartmentId =" + department;
            }
            if (!String.IsNullOrEmpty(branch))
            {
                filter += " AND br.Id =" + branch;
            }
            if (!String.IsNullOrEmpty(campus))
            {
                filter += " AND c.Id =" + campus;
            }


            //#region filterBranch
            //string filterBranch = "";
            //if (authorizedBranchIdList.Any())
            //{
            //    foreach (var e in authorizedBranchIdList)
            //    {
            //        if (filterBranch == "")
            //        {
            //            filterBranch += e.ToString();
            //        }
            //        else
            //        {
            //            filterBranch += ", " + e.ToString();
            //        }
            //    }
            //    if (filterBranch != "")
            //    {
            //        filterBranch = " AND br.Id IN (" + filterBranch + ") ";
            //    }
            //}
            //else
            //{
            //    filterBranch = " AND br.Id =0 ";
            //}
            //#endregion
            string filterBranch = "";

            if (branchIdList != null && branchIdList.Any() && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                filter += " AND br.Id IN (" + string.Join(",", branchIdList) + ") ";
            }
            filter = filter + filterBranch;

            if (!String.IsNullOrEmpty(pin))
            {
                filter += " AND t.pin ='" + pin + "'";
            }

            string query = "";
            query = @"Select 
		        t.Id as TeamMemberId
		        ,t.Pin as Pin
		        ,t.Name as Name
		        ,currentPosition.[EmploymentStatus] as MemberType
		        ,dp.Name as Department
		        ,dg.Name as Designation
		        ,o.Id as OrganizationId
		        from (
		        Select * from (
			        Select [CampusId]
				        ,[DesignationId]
				        ,[DepartmentId]
				        ,[TeamMemberId]
				        ,[EffectiveDate]
				        ,[EmploymentStatus]
				        ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC) AS R  
			        from [dbo].[HR_EmploymentHistory] Where [Status] = " + EmploymentHistory.EntityStatus.Active + endDateFilter + @"
		        ) as a WHERE a.R = 1
		        ) as currentPosition
		        left join [dbo].[HR_TeamMember] AS t ON t.Id = currentPosition.TeamMemberId  AND t.Status = " + TeamMember.EntityStatus.Active + @"
		        left join [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status = " + Campus.EntityStatus.Active + @"
		        left join [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status = " + Branch.EntityStatus.Active + @"
		        Left Join [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =" + Organization.EntityStatus.Active + @"
		        Left Join [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status = " + Department.EntityStatus.Active + @"
		        Left Join [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status = " + Designation.EntityStatus.Active + @"
		        WHere 	1=1  AND EmploymentStatus!=" + (int)MemberEmploymentStatus.Retired + " " + filter;
            return query;
        }
        #endregion

        public IList<AttendanceSummary> LoadTeamMemberDayOffAttendanceSummary(TeamMember teamMember, long id = 0)
        {
            string idEqualString = "";
            if (id > 0)
            {
                idEqualString = " OR do.Id = " + id;
            }
            var today = DateTime.Today;
            var startOfMonth = new DateTime(today.Year, today.Month, 1);
            var endOfMonth = startOfMonth.AddMonths(1).AddDays(-1);
            var query = @"Select distinct {AttSummary.*} from(
                           select Ats.* FROM [dbo].[HR_AttendanceSummary] as ats   
                            where (ats.HolidaySettingId is not null or ats.IsWeekend=1) ";
            if (teamMember != null)
            {
                query += " and ats.TeamMemberId='" + teamMember.Id + "'";
            }
            query += " and ats.AttendanceDate>='" + startOfMonth.ToString("yyyy-MM-dd") + " 00:00:00.000' and ats.AttendanceDate<='" + endOfMonth.ToString("yyyy-MM-dd") + " 00:00:00.000'";
            query += @"  ) as AttSummary 
                        left join 
                        ( SELECT * FROM 
                        [dbo].[HR_DayOffAdjustment] 
                        WHERE DayOffStatus = " + (int)DayOffAdjustmentStatus.Approved + @" 
                        AND  Status = " + DayOffAdjustment.EntityStatus.Active + @" ) as do 
                        on do.TeamMemberId = AttSummary.TeamMemberId AND AttSummary.AttendanceDate = do.InsteadOfDate
                        left join (SELECT * FROM   [dbo].[HR_HolidayWork] WHERE  ApprovalType > 0 AND Status = 1) as hdw on hdw.TeamMemberId = AttSummary.TeamMemberId AND AttSummary.AttendanceDate = hdw.HolidayWorkDate
                        where 1=1  AND ( do.InsteadOfDate is null " + idEqualString + @" ) AND (hdw.id is null) 
                        ";

            IQuery iQuery = Session.CreateSQLQuery(query).AddEntity("AttSummary", typeof(AttendanceSummary));
            iQuery.SetResultTransformer(Transformers.AliasToBean<AttendanceSummaryQueryTransfer>());
            var res = iQuery.List<AttendanceSummaryQueryTransfer>().ToList();
            return res.Select(attendanceSummary => attendanceSummary.AttSummary).ToList();
        }

//        public IList<AttendanceSummary> ListAttendanceSummary(TeamMember teamMember)
//        {
//            var today = DateTime.Today;
//            var startOfMonth = new DateTime(today.Year, today.Month, 1);
//            var endOfMonth = startOfMonth.AddMonths(1).AddDays(-1);
//            var query = @"Select distinct {AttSummary.*} from(
//                           select Ats.* FROM [dbo].[HR_AttendanceSummary] as ats   
//                            where (ats.HolidaySettingId is not null or ats.IsWeekend=1) ";
//            if (teamMember != null)
//            {
//                query += " and ats.TeamMemberId='" + teamMember.Id + "'";
//            }
//            query += " and ats.AttendanceDate>='" + startOfMonth.ToString("yyyy-MM-dd") + " 00:00:00.000' and ats.AttendanceDate<='" + endOfMonth.ToString("yyyy-MM-dd") + " 00:00:00.000'";
//            query += @"  ) as AttSummary 
//                        left join [dbo].[HR_DayOffAdjustment] as do on do.TeamMemberId = AttSummary.TeamMemberId
//                        where AttSummary.AttendanceDate != do.InsteadOfDate or do.InsteadOfDate is null";

//            IQuery iQuery = Session.CreateSQLQuery(query).AddEntity("AttSummary", typeof(AttendanceSummary));
//            iQuery.SetResultTransformer(Transformers.AliasToBean<AttendanceSummaryQueryTransfer>());
//            var res = iQuery.List<AttendanceSummaryQueryTransfer>().ToList();
//            return res.Select(attendanceSummary => attendanceSummary.AttSummary).ToList();
//        }


    }

}
