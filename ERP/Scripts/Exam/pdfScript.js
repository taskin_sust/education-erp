﻿function whiteSpaceWraping(element) {
    var li = element.parent();
    var ol = li.parent();

    var liWidth = li.width();
    var olWidth = ol.width();

    if (((liWidth / olWidth) * 100) > 50) {
        element.css("white-space", "normal");
    }
}

function adjustOverflowing(element, q) {

    var olElement = element.parent();
    var index = element.parent().find(".list-group-item").index(element);

    if (((element.width() / olElement.width()) * 100) > 23 && ((element.width() / olElement.width()) * 100) < 25) {
        olElement.find(".list-group-item").each(function () {
            $(this).css('width', "48%");            
            $(this).find(".optionNumbering").css('width', "12%");
            $(this).find(".option").css('width', "88%");
        });

    } else if (((element.width() / olElement.width()) * 100) > 45 && ((element.width() / olElement.width()) * 100) < 51) {
        olElement.find(".list-group-item").each(function () {
            $(this).css('width', "96%");
            $(this).find(".optionNumbering").css('width', "6%");
            $(this).find(".option").css('width', "94%");
        });
    }
}

function checkOverflowing() {
    $('.options .list-group-item').each(function () {
        if ($(this).prop('scrollWidth') > $(this).width()) {
            adjustOverflowing($(this), 1);
        }
    });

    $('.options .list-group-item').each(function () {
        if ($(this).prop('scrollWidth') > $(this).width()) {
            adjustOverflowing($(this), 2);

            var mathTexCurrentSize = $("body").attr("data-eqfontsize");
            while (mathTexCurrentSize > 0) {
                if ($(this).prop('scrollWidth') > $(this).width()) {
                    $(this).find(".math-tex").css("font-size", mathTexCurrentSize + "mm");
                }
                else {
                    break;
                }
                mathTexCurrentSize--;
            }

        }
    });
}

MathJax.Hub.Register.StartupHook("End", function () {

    checkOverflowing();
    $('.optionNumbering').each(function () {
        $(this).css('padding-top', (($(this).parent().find(".option").height() - $(this).height()) / 2));
    });

    $('.option').each(function () {
        whiteSpaceWraping($(this));
    });

    columnDivider();


   var textFontSize = $("body").attr("data-textfontsize");
   var eqFontSize = $("body").attr("data-eqfontsize");
   var targetPage = $("body").attr("data-target-page");

   var examId = $("body").attr("data-examid");
   var setid = $("body").attr("data-setid");
   var version = $("body").attr("data-version");
   var uniqueSet = $("body").attr("data-unique-set");
   var attr = $("body").attr('data-show-correct-answer');
   var showCorrectAnswer = "";
   if (typeof attr !== typeof undefined && attr !== false) {
       showCorrectAnswer = "&showCorrectAnswer=" + $("body").attr("data-show-correct-answer");
   }
   
   
   if ($(".nextPage").length > (targetPage - 1) && !(textFontSize == 1 || eqFontSize == 1)) {

       var pathname = window.location.pathname;
       
       var res = pathname.match(/previewQuestion/gi);
     
      
       if (res!=null) {
           window.location.href = "PreviewQuestion?examId=" + examId + "&setId=" + setid + "&version=" + version + "&textFontSize=" + textFontSize + "&eqFontSize=" + eqFontSize + "&targetPage=" + targetPage + showCorrectAnswer;

       } else {
           window.location.href = "DisplayQuestion?examId=" + examId + "&uniqueSet=" + uniqueSet + "&version=" + version + "&textFontSize=" + textFontSize + "&eqFontSize=" + eqFontSize + "&targetPage=" + targetPage + showCorrectAnswer;

       }
   }

   questionEqFontResize();

   $('.pageCount').each(function (index, val) {
       $(this).text("Page: " + (index + 1) + "/" + $('.pageCount').length);
   });

   //manual trigger
   //if (typeof wnvPdfConverter != "undefined") {
   //    wnvPdfConverter.startConversion();
   //}

});

function columnDivider() {
    var isOverfolow = false;
    var firstPage = $(".firsPage");
    var firstPagefirstColumn = $(".firsPage .columnCustomFirst");
    var question = [];
    var nextIndex = 0;
    var nextPageHtml = '<div style="" class="nextPage"><div style="" class="columnCustomFirst"></div><div style="" class="columnCustomSecoond"></div><div style="clear: both;"></div></div><div style="text-align: right; height: 4mm; padding-right:5mm; font-weight:bold; font-size:2mm;" class="pageCount">1</div>';
    var currentPage = $(".firsPage");
    var currentColumn = $(".firsPage .columnCustomFirst");
    var pageCount = 0;

    if (firstPagefirstColumn.prop('scrollHeight') > firstPagefirstColumn.height()) {

        $('.question').each(function () {
            question.push($(this)[0].outerHTML);
        });

        $(".firsPage .columnCustomFirst").empty();
        isOverfolow = true;
    }


    var s = "";
    while (isOverfolow) {
        MathJax.Hub.Config({ skipStartupTypeset: true });
        for (var i = nextIndex; i < question.length; i++) {
            s = s + question[i];

            currentColumn.append(question[i]);

            if (currentColumn.prop('scrollHeight') > currentColumn.height()) {
                //   alert(currentColumn.find(".question").length);
                currentColumn.find(".question:eq(" + (currentColumn.find(".question").length - 1) + ")").remove();
                if (currentPage.attr("class") == "firsPage") {
                    if (currentColumn.attr("class") == "columnCustomFirst") {
                        currentColumn = $(".firsPage .columnCustomSecoond");
                    } else {
                        $(".questionPages").append(nextPageHtml);
                        currentPage = $(".questionPages").find(".nextPage:eq(" + pageCount + ")");
                        currentColumn = currentPage.find(".columnCustomFirst");
                        pageCount = pageCount + 1;
                    }
                } else {
                    if (currentColumn.attr("class") == "columnCustomFirst") {
                        currentColumn = currentPage.find(".columnCustomSecoond");
                    } else {
                        $(".questionPages").append(nextPageHtml);
                        currentPage = $(".questionPages").find(".nextPage:eq(" + pageCount + ")");
                        currentColumn = currentPage.find(".columnCustomFirst");
                        pageCount = pageCount + 1;
                    }
                }
                break;
            }
            else {
                nextIndex++;
            }
        }
        if (nextIndex == question.length) {
            isOverfolow = false;
        }
    }

    var lastpage = $(".firsPage").last();
   
    if ($(".nextPage").length > 0) {
        lastpage = $(".nextPage").last();
    }

    lastpage.css("overflow", "visible");

    var lastPageFirstColumn = lastpage.find(".columnCustomFirst");
    var lastPageSecondColumn = lastpage.find(".columnCustomSecoond");


    while (1) {
        var a = 0;
        var b = 0;
        var c = 0;
        var lastChild = "";
        var mf = 0;
        var ms = 0;
        lastPageFirstColumn.find(".question").each(function(index,value) {
            a = a + $(this).height();
            
            if (index == (lastPageFirstColumn.find(".question").length-1)) {
                c = $(this).height();
                lastChild = $(this);
            }
            
            mf = mf + Math.ceil(parseFloat($(this).css("marginBottom")));
           // console.log("QHF: " + $(this).height());
            
        });
        lastPageSecondColumn.find(".question").each(function () {
            b = b + $(this).height();
            ms = ms + Math.ceil(parseFloat($(this).css("marginBottom")));
            //console.log("QH: " + $(this).height());
            //console.log("QM: " + parseFloat($(this).css("marginBottom")) + ", QMT: " + parseFloat($(this).css("marginTop")));
            //console.log("QP: " + parseFloat($(this).css("paddingBottom")) + ", QPT: " + parseFloat($(this).css("paddingTop")));
        });

       

        if ((b + c) > (a - c)) {
             break;
        }
        else {
            lastChild.prependTo(lastPageSecondColumn);
        }
    }


  //  console.log("F:" + a + ", FM:" + mf + ", FPH:" + lastPageFirstColumn.height() + ", F+FM:", (a + mf));
  //  console.log("S:" + b + ", SM:" + ms + ", PH:" + lastPageSecondColumn.height() + ", F+FM:", (b + ms));

    if ((a + mf) > (b + ms)) {
        var firstCHeight = lastPageFirstColumn.height();
        lastPageFirstColumn.height(a + mf);
        lastPageSecondColumn.height(a + mf);
        lastpage.height(a + mf + Math.ceil(parseFloat(lastPageFirstColumn.css("marginTop"))));
        $(".pageCount").last().css("margin-top", (firstCHeight - (a + mf + Math.ceil(parseFloat(lastPageFirstColumn.css("marginTop"))))) + "px");
    }
    else {
      
        var secondCHeight = lastPageSecondColumn.height();
        //alert(b);
        //alert(ms);
        //alert(secondCHeight);
        //alert(b + ms);
        
        

        lastPageFirstColumn.height(b + ms);
        lastPageSecondColumn.height(b + ms);
        lastpage.height(b + ms + Math.ceil(parseFloat(lastPageSecondColumn.css("marginTop"))));
        $(".pageCount").last().css("margin-top", (secondCHeight - (b + ms + Math.ceil(parseFloat(lastPageSecondColumn.css("marginTop"))))) + "px");
    }
    


    // var dpm = 291 / lastPageFirstColumn.height();

    //alert(lastPageFirstColumn.height());
    //alert(a);
    //alert(lastPageSecondColumn.height());
    //alert(b);

    //  $(this).find(".option").css('width', "90%");
}

function questionEqFontResize() {
    $('.mainQuestion').each(function () {
        //console.log($(this).prop('scrollWidth'));
        //console.log($(this).width());
        var mathTexCurrentSize = $("body").attr("data-eqfontsize");
        while (mathTexCurrentSize > 1) {
            if ($(this).prop('scrollWidth') > $(this).width()) {
                $(this).find(".math-tex").css("font-size", mathTexCurrentSize + "mm");
            }
            else {
                break;
            }
            mathTexCurrentSize--;
        }
    });
}