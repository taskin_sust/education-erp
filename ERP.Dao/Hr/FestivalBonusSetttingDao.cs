using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IFestivalBonusSettingDao : IBaseDao<FestivalBonusSetting, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        IList<FestivalBonusSetting> LoadFestivalBonusList(string orderBy, string orderDir, List<long> authOrganizationIdList, string employmentStatus, string religion, string calculationOn);

        IList<FestivalBonusSetting> LoadFestivalBonusSettingList(List<long> organizationIdList,DateTime startDate); 

        #endregion

        #region Others Function
        int LoadFestivalBonusListCount(List<long> authOrganizationIdList, string employmentStatus, string religion, string calculationOn);
        bool IsDuplicateBonusTitle(string name, long id,DateTime effectiveDate,Organization organization);
        bool HasDuplicateByFbTitle(string name, long organizationId, long? id, DateTime effectiveDate, DateTime? closingDate);

        #endregion
    }

    public class FestivalBonusSetttingDao : BaseDao<FestivalBonusSetting, long>, IFestivalBonusSettingDao
    {

        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<FestivalBonusSetting> LoadFestivalBonusList(string orderBy, string orderDir, List<long> authOrganizationIdList,
            string employmentStatus, string religion, string calculationOn)
        {
            ICriteria criteria = GetFestivalBonusCriteria(authOrganizationIdList, employmentStatus, religion, calculationOn);
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }
            else
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc("Organization") : Order.Desc("Organization"));
            }
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            return criteria.List<FestivalBonusSetting>();
        }

        public IList<FestivalBonusSetting> LoadFestivalBonusSettingList(List<long> organizationIdList,DateTime startDate)
        {
            //return Session.QueryOver<FestivalBonusSetting>()
            //    .Where(x => x.Organization.Id.IsIn(organizationIdList.ToArray()) && x.Status == FestivalBonusSetting.EntityStatus.Active)
            //    .OrderBy(x => x.Id)
            //    .Asc
            //    .List<FestivalBonusSetting>();           
            
            ICriteria criteria = Session.CreateCriteria<FestivalBonusSetting>();
            criteria.CreateAlias("Organization", "org").Add(Restrictions.Eq("org.Status", Organization.EntityStatus.Active));
            criteria.Add(Restrictions.In("org.Id", organizationIdList));
            criteria.Add(Restrictions.Eq("Status", FestivalBonusSetting.EntityStatus.Active));
            criteria.Add(Restrictions.Le("EffectiveDate", startDate));
            
            var disjunction = Restrictions.Disjunction(); // for OR statement 
            disjunction.Add(Restrictions.IsNull("ClosingDate") || Restrictions.Ge("ClosingDate", startDate));
            criteria.Add(disjunction);

            IList<FestivalBonusSetting> rowList = criteria.List<FestivalBonusSetting>();
            return rowList;
        }

        #endregion

        #region Others Function

        public int LoadFestivalBonusListCount(List<long> authOrganizationIdList, string employmentStatus, string religion,
           string calculationOn)
        {
            ICriteria criteria = GetFestivalBonusCriteria(authOrganizationIdList, employmentStatus, religion, calculationOn);
            //criteria.SetResultTransformer(Transformers.AliasToBean(typeof(FestivalBonusSetting)));
            //criteria.SetProjection(Projections.RowCount());
            criteria.SetProjection(Projections.CountDistinct("Id"));
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public bool IsDuplicateBonusTitle(string name, long id, DateTime effectiveDate,Organization organization)
        {
            var month = effectiveDate.Month;
            ICriteria criteria = Session.CreateCriteria<FestivalBonusSetting>();
            criteria.CreateAlias("Organization", "org").Add(Restrictions.Not(Restrictions.Eq("org.Status",Organization.EntityStatus.Delete)));
            if (id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Eq("Name", name));
            }
            criteria.Add(Restrictions.Eq("org.Id", organization.Id));
            var calculatedY = Projections.SqlFunction("month", NHibernateUtil.Int32, Projections.Property("EffectiveDate"));
            criteria.Add(Restrictions.Eq(calculatedY, month));
            var list = criteria.List<FestivalBonusSetting>();
            return list != null && list.Count > 0;
        }

        public bool HasDuplicateByFbTitle(string name, long organizationId, long? id, DateTime effectiveDate, DateTime? closingDate)
        {
            ICriteria criteria = Session.CreateCriteria<FestivalBonusSetting>();
            criteria.CreateAlias("Organization", "org").Add(Restrictions.Eq("org.Status", Organization.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Name", name));
            criteria.Add(Restrictions.Eq("org.Id", organizationId));
            criteria.Add(Restrictions.Eq("Status", FestivalBonusSetting.EntityStatus.Active));
            criteria.Add(Restrictions.Le("EffectiveDate", effectiveDate));

            //criteria.Add(Restrictions.Ge("ClosingDate", effectiveDate));
            var disjunction = Restrictions.Disjunction(); // for OR statement 
            disjunction.Add(Restrictions.IsNull("ClosingDate") || Restrictions.Ge("ClosingDate", effectiveDate));
            criteria.Add(disjunction);

            if (id != null && id>0)
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            IList<FestivalBonusSetting> rowList = criteria.List<FestivalBonusSetting>();
            return rowList == null || rowList.Count < 1;
        }

        #endregion

        #region Helper Function

        private ICriteria GetFestivalBonusCriteria(List<long> authOrgList, string employmentStatus, string religion, string calculationOn)
        {
            ICriteria criteria = Session.CreateCriteria<FestivalBonusSetting>().Add(Restrictions.Not(Restrictions.Eq("Status", FestivalBonusSetting.EntityStatus.Delete)));
            criteria.CreateAlias("PrFestivalBonusCalculation", "fbc").Add(Restrictions.Not(Restrictions.Eq("fbc.Status", FestivalBonusSettingCalculation.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "org").Add(Restrictions.Not(Restrictions.Eq("org.Status", Organization.EntityStatus.Delete)));
            if (authOrgList != null && authOrgList.Any())
            {
                criteria.Add(Restrictions.In("org.Id", authOrgList));
            }
            if (!String.IsNullOrEmpty(employmentStatus))
            {
                criteria.Add(Restrictions.Eq("fbc.EmploymentStatus", Convert.ToInt32(employmentStatus)));
            }
            if (!String.IsNullOrEmpty(religion))
            {
                switch (Convert.ToInt32(religion))
                {
                    case 1:
                        criteria.Add(Restrictions.Eq("IsIslam", true));
                        break;
                    case 2:
                        criteria.Add(Restrictions.Eq("IsHinduism", true));
                        break;
                    case 3:
                        criteria.Add(Restrictions.Eq("IsChristianity", true));
                        break;
                    case 4:
                        criteria.Add(Restrictions.Eq("IsBuddhism", true));
                        break;
                    case 5:
                        criteria.Add(Restrictions.Eq("IsOthers", true));
                        break;
                    default:
                        break;
                }
            }
            if (!String.IsNullOrEmpty(calculationOn))
            {
                criteria.Add(Restrictions.Eq("fbc.CalculationOn", Convert.ToInt32(calculationOn)));
            }
           
            return criteria;
        }

        #endregion

    }
}
