using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory {
    
    
    public partial class GoodsIssueMap : BaseClassMap<GoodsIssue, long> {
        
        public GoodsIssueMap() {
			Table("UINV_GoodsIssue");
			LazyLoad();
			References(x => x.Branch).Column("BranchId");
			References(x => x.Requisition).Column("RequisitionId");
			Map(x => x.GoodsIssueNo).Column("GoodsIssueNo").Not.Nullable();
			Map(x => x.Remarks).Column("Remarks");
            HasMany(x => x.GoodsIssueDetails).KeyColumn("GoodsIssueId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
