using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class GoodsReceiveDetailFactory : ObjectFactoryBase<GoodsReceiveDetail>
   {

       private readonly IGoodsReceiveDetailService _goodsReceiveDetailService;

       public GoodsReceiveDetailFactory(IObjectFactoryBase caller, ISession session): base(caller, session){

       _goodsReceiveDetailService = new GoodsReceiveDetailService(session);

       }

       #region Create 

       public GoodsReceiveDetailFactory Create(){

       Object = null;
       SingleObjectList.Add(Object);
       return this;
       }

       public GoodsReceiveDetailFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public GoodsReceiveDetailFactory Persist()       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _goodsReceiveDetailService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x =>{if (x.Id == 0)_goodsReceiveDetailService.SaveOrUpdate(x);});

          }
          return this;
       }

       public GoodsReceiveDetailFactory CreateMore(int num){

       for (int i = 0; i < num; i++){
       var obj = new GoodsReceiveDetail(){};
       ObjectList.Add(obj);}
       return this;}

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public GoodsReceiveDetailFactory CleanUp()
       {

          DeleteAll();
          return this;
       }

       #endregion

    }
}
