﻿/*Written By Taskin */

$(document).ready(function () {

    var successMessage = '<div style="text-align: center;"class="alert alert-success"><a class="close" data-dismiss="alert">×</a><strong>Success!</strong> Course Add Successfully</div>';
    var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Failed </div>';
    var paymentErrorMsg = '<p class="errorMsg" style="color: #F00"> Field Value Missing </p>';

    $(document).on('click', 'input:checkbox', function (data) {
        var chkBoxId = $(this).attr('id');
        var firstTdWhichContainsCheckbox = $(this).parent();
        //var firstTdVal = firstTd.attr('id');
        var secondTdWhichContainsTextbox = firstTdWhichContainsCheckbox.next();
        //var secTdVal = secondTdWhichContainsTextbox.attr('class');
        var textboxId = secondTdWhichContainsTextbox.find('input[type=number]').attr('id');

        /*Check is Checkbox is checked */
        if ($(this).is(':checked')) {
            $(this).prop('checked', true);

            /* Compare checkbox Id and TextBox Id if same then enable checkbox */
            if (chkBoxId === textboxId) {
                secondTdWhichContainsTextbox.find('input[type=number]').removeAttr('disabled');
            }
        } else {
            /* Checkbox disable */
            $(this).prop('checked', false);
            /*If textbox is not empty and disable the checkbox then clear textbox value and disable text box */
            var textValue = secondTdWhichContainsTextbox.find('input[type=number]').val();
            if (textValue.length > 0) {
                secondTdWhichContainsTextbox.find('input[type=number]').val('');
            }
            secondTdWhichContainsTextbox.find('input[type=number]').attr('disabled', 'disabled');
        }
    });

    $(document).on('click', '#courseAdd', function (data) {

        $('#Name').removeClass('highlight');
        $('#Program').removeClass('highlight');
        $('#RefSession').removeClass('highlight');
        $('input').removeClass('highlight');
        $('.errorMsg').remove();

        var programId = $('#Program').val();
        var sessionId = $('#RefSession').val();
        var courseName = $('#Name').val();

        var courseFeesObj = [];
        var isPaymentFieldNotEmpty = false;
        if (programId.length > 0) {

            if (sessionId.length > 0) {
                if (courseName.length > 0) {

                    /*Get SubjectId and Subject corresponding payment */
                    $('#courseFees tr').each(function (index, value) {
                        var courseFees = {};
                        if ($(this).find('input[type=checkbox]').is(':checked')) {
                            /*Empty textbox check */
                            if ($(this).find('input[type=number]').val().length > 0) {
                                isPaymentFieldNotEmpty = true;
                                courseFees[$(this).find('input[type=checkbox]').attr('id')] = $(this).find('input[type=number]').val();
                                courseFeesObj.push(JSON.stringify(courseFees));
                            } else {

                                $(this).find('input[type=number]').addClass('highlight');
                                $(this).find('input[type=number]').after(paymentErrorMsg);
                                isPaymentFieldNotEmpty = false;
                                return false;
                            }
                        }
                    });
                    if (courseFeesObj.length > 0) {
                        if (isPaymentFieldNotEmpty == true) {
                            $.ajax({
                                url: "/Administration/Course/AddCourse",
                                type: "POST",
                                data: { courseFeesObjArray: courseFeesObj, programId: programId, sessionId: sessionId, courseName: courseName },
                                success: function (data) {
                                    if ($.trim(data) === "Course Add Successfully") {

                                        $('.customMessage').append(successMessage);
                                    } else {

                                        $('.customMessage').append(errorMessage);
                                    }
                                },
                                error: function (data) {
                                    $('.customMessage').append(errorMessage);
                                }
                            });
                        } else {
                            $('.customMessage').append('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Failed!! Missing Payment </div>');
                        }

                    } else {
                        $('.customMessage').append('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Failed!! Select atleast One Subject </div>');
                    }

                } else {

                    $('#Name').addClass('highlight');
                }
            } else {
                $('#RefSession').addClass('highlight');
            }
        } else {
            $('#Program').addClass('highlight');
        }
    });
});