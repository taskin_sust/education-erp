﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.GoodsTransferTest
{
    public class GoodsTransferBaseTest : TestBase, IDisposable
    {
        #region  Object Initialization

        private ISession _session;
        internal ProgramFactory ProgramFactory;
        internal SessionFactory SessionFactory;
        internal BranchFactory BranchFactory;
        internal ItemFactory ItemFactory;
        internal ProgramSessionItemFactory ProgramSessionItemFactory;
        internal GoodsTransferDetailsFactory GoodsTransferDetailsFactory;
        internal GoodsTransferFactory GoodsTransferFactory;
        internal GoodsReceiveFactory GoodsReceiveFactory;
        internal GoodsReceiveDetailsFactory GoodsReceiveDetailsFactory;

        internal GoodsTransferService _goodsTransferService;

        public GoodsTransferBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            GoodsTransferFactory = new GoodsTransferFactory(null, _session);
            GoodsTransferDetailsFactory=new GoodsTransferDetailsFactory(null,_session);
            ProgramFactory = new ProgramFactory(null, _session);
            SessionFactory = new SessionFactory(null, _session);
            BranchFactory = new BranchFactory(null, _session);
            ItemFactory = new ItemFactory(null, _session);
            ProgramSessionItemFactory = new ProgramSessionItemFactory(null, _session);
            GoodsReceiveFactory=new GoodsReceiveFactory(null,_session);
            GoodsReceiveDetailsFactory = new GoodsReceiveDetailsFactory(null, _session);

            _goodsTransferService=new GoodsTransferService(_session);
        }

        #endregion

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }


        #region CleanUp

        public void Dispose()
        {
            ProgramSessionItemFactory.CleanUp();
            ItemFactory.CleanUp();
            SessionFactory.Cleanup();
            ProgramFactory.Cleanup();
            BranchFactory.Cleanup();
            
            base.Dispose();
        }


        #endregion
    }
}
