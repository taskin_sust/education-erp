using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Data;
using log4net;
using NHibernate;
using NHibernate.Util;
using Remotion.Linq.Parsing;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.Hr
{
    public interface IDailySupportAllowanceSettingService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(List<UserMenu> userMenus, DailySupportAllowanceSetting dailySupportAllowance);
        void Delete(DailySupportAllowanceSetting entity);

        #endregion

        #region Single Instances Loading Function

        DailySupportAllowanceSetting GetDailySupportAllowanceSetting(long id);
        DailySupportAllowanceSetting GetDailySupportAllowanceSetting(long memberId, DateTime dateTime);

        #endregion

        #region List Loading Function

        IList<DailySupportAllowanceSetting> LoadDailySupportAllowanceSetting(int draw, int start, int length, List<UserMenu> authorizeMenu, long? organizationId);

        #endregion

        #region Others Function

        int GetDailySupportAllowanceCount(List<UserMenu> authorizeMenu, long? organizationId);

        #endregion

        #region Helper Function

        #endregion
    }

    public class DailySupportAllowanceSettingService : BaseService, IDailySupportAllowanceSettingService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("PayrollService");

        #endregion

        #region Properties & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IDailySupportAllowanceSettingDao _dailySupportAllowanceDao;
        private readonly IAllowanceSheetFirstDetailsDao _allowanceSheetFirstDetailsDao;
        private readonly IEmploymentHistoryDao _employmentHistoryDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;
        public DailySupportAllowanceSettingService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _dailySupportAllowanceDao = new DailySupportAllowanceSettingDao { Session = session };
            _allowanceSheetFirstDetailsDao = new AllowanceSheetFirstDetailsDao() { Session = session };
            _employmentHistoryDao = new EmploymentHistoryDao() { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public void SaveOrUpdate(List<UserMenu> userMenus, DailySupportAllowanceSetting dailySupportAllowance)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSave(userMenus, dailySupportAllowance);
                CheckValidation(dailySupportAllowance);
                using (transaction = Session.BeginTransaction())
                {
                    if (dailySupportAllowance.Id == 0)
                    {
                        EligibleForSave(dailySupportAllowance.Organization.Id, dailySupportAllowance.EffectiveDate, dailySupportAllowance.Designation.Id, dailySupportAllowance.ClosingDate);
                        _dailySupportAllowanceDao.Save(dailySupportAllowance);
                    }
                    else
                    {
                        var obj = EligibleForUpdate(dailySupportAllowance);
                        _dailySupportAllowanceDao.Update(obj);
                    }
                    transaction.Commit();
                }
            }
            catch (NullObjectException nullObjectException)
            {
                throw;
            }
            catch (InvalidDataException invalidDataException)
            {
                throw;
            }
            catch (DuplicateEntryException duplicateEntryException)
            {
                throw;
            }
            catch (MessageException me)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void Delete(DailySupportAllowanceSetting entity)
        {
            ITransaction transaction = null;
            try
            {
                IList<AllowanceSheetFirstDetails> list = _allowanceSheetFirstDetailsDao.LoadByDailySupportAllowanceId(entity.Id);
                if (list.Count > 0) throw new InvalidDataException("You can't delete this allowance setting because Allowance sheet has already been prepared");
                using (transaction = Session.BeginTransaction())
                {
                    entity.Status = DailySupportAllowanceSetting.EntityStatus.Delete;
                    _dailySupportAllowanceDao.Update(entity);
                    transaction.Commit();
                }
            }
            catch (NullObjectException nullObjectException)
            {
                throw;
            }
            catch (InvalidDataException invalidDataException)
            {
                throw;
            }
            catch (DuplicateEntryException duplicateEntryException)
            {
                throw;
            }
            catch (MessageException me)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public DailySupportAllowanceSetting GetDailySupportAllowanceSetting(long id)
        {
            try
            {
                if (id <= 0) throw new InvalidDataException("invalid data");
                return _dailySupportAllowanceDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public DailySupportAllowanceSetting GetDailySupportAllowanceSetting(long memberId, DateTime dateTime)
        {
            try
            {
                object responsiblePersonDes, responsiblePersonDep, responsiblePersonOrg;
                int empStatus;
                if (memberId <= 0) throw new InvalidDataException("invalid memberid");
                if (dateTime <= DateTime.MinValue || dateTime > DateTime.MaxValue) throw new InvalidDataException("Invalid datetime");
                _employmentHistoryDao.GetCurrentInformations(memberId, out empStatus, out responsiblePersonDes, out responsiblePersonDep, out responsiblePersonOrg, dateTime);
                if (responsiblePersonDes == null) throw new InvalidDataException("Invalid Designation");
                if (responsiblePersonDep == null) throw new InvalidDataException("Invalid Department");
                if (responsiblePersonOrg == null) throw new InvalidDataException("Invalid Organization");
                if (empStatus == 0) throw new InvalidDataException("Invalid Emp Status");
                var org = (Organization)responsiblePersonOrg;
                var des = (Designation)responsiblePersonDes;
                return _dailySupportAllowanceDao.GetDailySupportAllowanceSetting(org.Id, des.Id, dateTime);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function

        public IList<DailySupportAllowanceSetting> LoadDailySupportAllowanceSetting(int draw, int start, int length, List<UserMenu> authorizeMenu, long? organizationId)
        {
            try
            {
                if (authorizeMenu == null) throw new InvalidDataException("invalid user permission");
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authorizeMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                if (authOrgList.Count <= 0) throw new UnauthorizeDataException("Unauthorized data exception");
                return _dailySupportAllowanceDao.LoadDailySupportAllowanceSetting(draw, start, length, authOrgList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int GetDailySupportAllowanceCount(List<UserMenu> authorizeMenu, long? organizationId)
        {
            try
            {
                if (authorizeMenu == null) throw new InvalidDataException("invalid user permission");
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authorizeMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                return _dailySupportAllowanceDao.GetDailySupportAllowanceSettingCount(authOrgList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        private void CheckBeforeSave(List<UserMenu> userMenus, DailySupportAllowanceSetting dailySupportAllowance)
        {
            if (userMenus == null || userMenus.Count <= 0) throw new InvalidDataException("Invalid user permission");
            if (dailySupportAllowance == null) throw new NullObjectException("Null object exception");
            if (dailySupportAllowance.Organization == null) throw new InvalidDataException("Invalid organization");
            if (dailySupportAllowance.Designation == null) throw new InvalidDataException("Invalid designation");
            var authOrg = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(dailySupportAllowance.Organization.Id));
            if (authOrg == null) throw new InvalidDataException("Unauthorized organization");
            if (dailySupportAllowance.Amount <= 0) throw new InvalidDataException("Invalid designation amount");
            if (dailySupportAllowance.ClosingDate.ToString().Length > 0)
                if (dailySupportAllowance.EffectiveDate > dailySupportAllowance.ClosingDate)
                    throw new InvalidDataException("Closing date must be bigger than effective date");
        }

        private void CheckValidation(DailySupportAllowanceSetting dailySupportAllowance)
        {
            var validationResult = ValidationHelper.ValidateEntity<DailySupportAllowanceSetting, DailySupportAllowanceSetting>(dailySupportAllowance);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private IList ShouldContinue(DailySupportAllowanceSetting updateObj)
        {
            Session.Clear();
            var prevObj = _dailySupportAllowanceDao.LoadById(updateObj.Id);
            IList diffProperties = new ArrayList();
            foreach (var item in updateObj.GetType().GetProperties())
            {
                if (!Object.Equals(item.GetValue(updateObj, null),
                         prevObj.GetType().GetProperty(item.Name).GetValue(prevObj, null)))
                {
                    diffProperties.Add(item.Name);
                }
            }
            return diffProperties;
            //if (diffProperties.Contains("Amount") || diffProperties.Contains("EffectiveDate")) throw new InvalidDataException("you can't change anything except closing date");
            //return true;
        }

        private DailySupportAllowanceSetting EligibleForUpdate(DailySupportAllowanceSetting dailySupportAllowanceSetting)
        {
            Session.Clear();
            DateTime eligibledateTime = _dailySupportAllowanceDao.GetEligibleClosingDateForPayrollSetting(dailySupportAllowanceSetting.Id);
            //check if any allowance sheet generate if generate then can change only closing date
            if (eligibledateTime != DateTime.MinValue)
            {
                var difProperties = ShouldContinue(dailySupportAllowanceSetting);
                if (difProperties.Contains("Amount") || difProperties.Contains("EffectiveDate")) throw new InvalidDataException("you can't change anything except closing date");
                if (eligibledateTime > dailySupportAllowanceSetting.ClosingDate) throw new InvalidDataException("closing date must be greater than " + eligibledateTime.ToString("yyyy MMMM dd"));
            }
            CheckDuplicateEntry(dailySupportAllowanceSetting.Organization.Id, dailySupportAllowanceSetting.Designation.Id, dailySupportAllowanceSetting.EffectiveDate, dailySupportAllowanceSetting.Id);
            //build object
            var prevObj = _dailySupportAllowanceDao.LoadById(dailySupportAllowanceSetting.Id);
            prevObj.Amount = dailySupportAllowanceSetting.Amount;
            prevObj.ClosingDate = dailySupportAllowanceSetting.ClosingDate;
            prevObj.Designation = dailySupportAllowanceSetting.Designation;
            prevObj.EffectiveDate = dailySupportAllowanceSetting.EffectiveDate;
            prevObj.Organization = dailySupportAllowanceSetting.Organization;
            prevObj.Status = dailySupportAllowanceSetting.Status;
            return prevObj;
        }

        private void EligibleForSave(long orgId, DateTime effectiveDateTime, long designationId, DateTime? closingDate)
        {
            var allObj = _allowanceSheetDao.GetEligibleAllowanceSheetByOrganizationId(orgId);
            if (allObj != null)
                if (allObj.DateTo >= effectiveDateTime)
                    throw new MessageException("You can't save this allowance from this effective date because " +
                                               "allowance sheet has already been generated for this month!! " +
                                               "try another effective date after " + allObj.DateTo.ToString("yyyy MMMM dd"));
            // check duplicate allowance for save
            CheckDuplicateEntry(orgId, designationId, effectiveDateTime);
        }

        private void CheckDuplicateEntry(long orgId, long designationId, DateTime effectiveDateTime, long id = 0)
        {
            if (id == 0)
            {
                //from save 
                IList<DailySupportAllowanceSetting> list = _dailySupportAllowanceDao.LoadDailySupportAllowanceSetting(orgId, designationId, 0);
                if (list != null && list.Any())
                {
                    DailySupportAllowanceSetting supportAllowanceSetting = list.OrderByDescending(x => x.EffectiveDate).Take(1).SingleOrDefault();
                    if (supportAllowanceSetting.ClosingDate == null) throw new InvalidDataException("You can't add this allowance because an allowance has already been declared from " + supportAllowanceSetting.EffectiveDate.ToString("yyyy MMMM dd"));
                    if (supportAllowanceSetting.ClosingDate >= effectiveDateTime) throw new InvalidDataException("You can't add this allowance because an allowance has already been declared from " + supportAllowanceSetting.EffectiveDate.ToString("yyyy MMMM dd") + " to " + supportAllowanceSetting.ClosingDate.Value.ToString("yyyy MMMM dd") + "Try another date which will be greater than " + supportAllowanceSetting.ClosingDate.Value.ToString("yyyy MMMM dd"));
                }
            }
            else
            {
                //from update
                IList<DailySupportAllowanceSetting> list = _dailySupportAllowanceDao.LoadDailySupportAllowanceSetting(orgId, designationId, id);
                if (list != null && list.Any())
                {
                    DailySupportAllowanceSetting supportAllowanceSetting = list.OrderBy(x => x.EffectiveDate).Take(1).SingleOrDefault();
                    if (supportAllowanceSetting.ClosingDate == null) throw new InvalidDataException("You can't update this allowance because an allowance has already been declared from " + supportAllowanceSetting.EffectiveDate.ToString("yyyy MMMM dd"));
                    if (supportAllowanceSetting.ClosingDate >= effectiveDateTime) throw new InvalidDataException("You can't update this allowance because an allowance has already been declared from " + supportAllowanceSetting.EffectiveDate.ToString("yyyy MMMM dd") + " to " + supportAllowanceSetting.ClosingDate.Value.ToString("yyyy MMMM dd") + "Try another date which will be greater than " + supportAllowanceSetting.ClosingDate.Value.ToString("yyyy MMMM dd"));
                }
            }

        }

        #endregion
    }
}

