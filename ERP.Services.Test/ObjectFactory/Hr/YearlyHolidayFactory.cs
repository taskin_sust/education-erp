﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class YearlyHolidayFactory : ObjectFactoryBase<YearlyHoliday>
    {
        public IYearlyHolidayService _yearlyHolidayService;
        //protected OrganizationFactory _organizationFactory;
        
        public YearlyHolidayFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {            
            //ObjectList = new List<HolidaySetting>();         
            _yearlyHolidayService = new YearlyHolidayService(session);
            //_organizationFactory = new OrganizationFactory(this, _session);
            
        }

        #region Create
        public YearlyHolidayFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        public YearlyHolidayFactory Create(string name,Organization organization)
        {
            Object = new YearlyHoliday()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = name,
                Rank = 1,
                DateFrom = DateTime.Now.AddDays(2),
                DateTo = DateTime.Now.AddDays(3),
                CreateBy = 1,
                ModifyBy = 1,
                Status = YearlyHoliday.EntityStatus.Active,
                Organization = organization
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public YearlyHolidayFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public YearlyHolidayFactory Persist()
        {
            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    _yearlyHolidayService.Save(x);
                }
            });
            
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _yearlyHolidayService.Save(Object);
                    }
                }
            }
            return this;
        }
        #endregion

        #region Others
        #endregion

        #region CleanUp

        public void Cleanup(YearlyHoliday yearlyHoliday)
        {
            DeleteObject(yearlyHoliday);
            //  _organizationFactory.Cleanup();
        }
        public void Cleanup()
        {
            DeleteAll();
          //  _organizationFactory.Cleanup();
        }
        #endregion


       
    }
}
