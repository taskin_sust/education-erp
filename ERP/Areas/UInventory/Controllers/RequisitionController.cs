﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using log4net;
using Microsoft.Ajax.Utilities;
using NHibernate.Dialect.Function;
using NHibernate.Engine;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("UInventory")]

    public class RequisitionController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IItemGroupService _itemGroupService;
        private readonly IItemService _itemService;
        private readonly IProgramSessionItemService _itemProgramSessionService;
        private readonly IBranchService _branchService;
        private readonly IRequisitionService _requisitionService;
        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly IList<string> _assignRsNameList = new List<string>();
        public RequisitionController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _organizationService = new OrganizationService(session);
                _branchService = new BranchService(session);
                _itemGroupService = new ItemGroupService(session);
                _itemService = new ItemService(session);
                _itemProgramSessionService = new ProgramSessionItemService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _requisitionService = new RequisitionService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        #endregion

        #region Details/Manage Page

        public ActionResult Index(string message = "", string type = "")
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                if (type.Equals("s"))
                {
                    ViewBag.SuccessMessage = message;
                }
                else if (type.Equals("i"))
                {
                    ViewBag.InfoMessage = message;
                }
                else if (type.Equals("e"))
                {
                    ViewBag.ErrorMessage = message;
                }
            }
            InitializeIndex(false);
            return View();
        }

        [HttpGet]
        public ActionResult Details(long id)
        {
            try
            {
                if (id <= 0)
                {
                    return HttpNotFound();
                }
                var requisitionObj = _requisitionService.LoadById(id);
                if (requisitionObj == null)
                    return RedirectToAction("Index", new { message = "Requisition Not Found! Please try for another!", type = "i" });
                var viewModel = InitialRequisitionSlipDetailsViewModel(requisitionObj);
                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("Index", new { message = WebHelper.CommonErrorMessage, type = "e" });
            }
        }

        #region Requisition Goods Issue Index

        [HttpGet]
        public ActionResult RequisitionGoodsIssue(string message = "", string type = "")
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                if (type.Equals("s"))
                {
                    ViewBag.SuccessMessage = message;
                }
                else if (type.Equals("i"))
                {
                    ViewBag.InfoMessage = message;
                }
                else if (type.Equals("e"))
                {
                    ViewBag.ErrorMessage = message;
                }
            }
            InitializeIndex(true);
            return View();
        }

        #endregion

        #endregion

        #region Operational functions

        #region Create

        public ActionResult Create(string message = "", string type = "")
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                if (type.Equals("s"))
                {
                    ViewBag.SuccessMessage = message;
                }
                else if (type.Equals("e"))
                {
                    ViewBag.ErrorMessage = message;
                }
            }
            InitializeView(new RequisitionForm());
            return View(new RequisitionForm());
        }

        [HttpPost]
        public ActionResult Create(RequisitionForm objForm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    InitializeView(objForm);
                    return View(objForm);
                }
                if (objForm.RequiredDate < DateTime.Today)
                {
                    ViewBag.ErrorMessage = "Required date cannot be less then today.";
                    InitializeView(objForm);
                    return View(objForm);
                }
                var slipDetailsList = new List<RequisitionDetails>();
                var branch = _branchService.GetBranch(objForm.BranchId);
                var model = new Requisition()
                {
                    RequisitionNo = _requisitionService.GetRequisitionSlipName(objForm.OrganizationId, branch),
                    Branch = branch,
                    Remarks = objForm.Remarks,
                    RequiredDate = objForm.RequiredDate,
                    RequisionStatus = Convert.ToInt32(RequisitionStatus.Pending),
                    Status = Requisition.EntityStatus.Active
                };
                foreach (var row in objForm.RequisitionDetails)
                {
                    var item = _itemService.LoadById(Convert.ToInt64(row.Item));
                    int purpose = Convert.ToInt32(row.Purpose);
                    Program program = null;
                    Session session = null;
                    if (!String.IsNullOrEmpty(row.ProgramSession))
                    {
                        string[] psIds = row.ProgramSession.Split(new string[] { "::" }, StringSplitOptions.None);
                        if (psIds.Length == 2)
                        {
                            program = _programService.GetProgram(Convert.ToInt64(psIds[0]));
                            session = _sessionService.LoadById(Convert.ToInt64(psIds[1]));
                        }
                    }
                    if (program == null || session == null)
                    {
                        ViewBag.ErrorMessage = "One or many row found without program & session.";
                        InitializeView(objForm);
                        return View(objForm);
                    }
                    var itemDetailsModel = new RequisitionDetails()
                    {
                        Session = session,
                        Program = program,
                        PurposeProgram = program,
                        PurposeSession = session,
                        Purpose = purpose,
                        Item = item,
                        RequiredQuantity = row.Quantity,
                        RequisitionStatus = (int)RequisitionStatus.Pending,
                        Status = RequisitionDetails.EntityStatus.Active,
                        CreateBy = _requisitionService.GetCurrentUserId(),
                        ModifyBy = _requisitionService.GetCurrentUserId(),
                        CreationDate = DateTime.Now,
                        ModificationDate = DateTime.Now,
                        Requisition = model
                    };
                    slipDetailsList.Add(itemDetailsModel);
                }
                model.RequisitionDetails = slipDetailsList;
                bool isSuccess = _requisitionService.SaveRequisition(model, (List<UserMenu>)ViewBag.UserMenu);
                if (isSuccess)
                {
                    return RedirectToAction("Details", new { id = model.Id });
                }
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
            }
            InitializeView(objForm);
            return View(objForm);
        }

        #endregion

        #region Cancel Requisition

        [HttpPost]
        public ActionResult CancelRequisition(long id)
        {
            try
            {
                if (id <= 0)
                {
                    return HttpNotFound();
                }
                var requisitionObj = _requisitionService.LoadById(id);
                if (requisitionObj == null)
                {
                    throw new NullObjectException("Requsition not found! Please try again!");
                }
                if (requisitionObj.RequisionStatus != (int)RequisitionStatus.Pending)
                {
                    throw new InvalidDataException("Requsition can't be cancel now! Only pending requsition can be cancel!");
                }
                _requisitionService.CancelRequisition(requisitionObj);
                return Json(new { isSuccess = true, Text = "RequisitionNo: \'" + requisitionObj.RequisitionNo + "\' successfully canceled!" });
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                return Json(new { isSuccess = false, Text = ex.Message });
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                return Json(new { isSuccess = false, Text = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { isSuccess = false, Text = WebHelper.CommonErrorMessage });
            }
        }

        #endregion

        #endregion

        #region Ajax function

        public JsonResult ItemListByItemGroup(string organizationId, string itemGroupId)
        {
            try
            {
                if (organizationId == "") organizationId = null;
                if (itemGroupId == "") itemGroupId = null;
                var itemList = _itemService.LoadItemForOrganizationOrGroup((List<UserMenu>)ViewBag.UserMenu, Convert.ToInt64(organizationId),
                                                                            _commonHelper.ConvertIdToList(Convert.ToInt64(itemGroupId)).ToArray());
                var itemSelectList = new SelectList(itemList.ToList(), "Id", "Name");
                return Json(new { returnList = itemSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load item";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        /*
        public JsonResult GetProgramAndSessionItem(long organizationId, List<long> branchIdList, List<long> itemIdList = null,
            int purposeId = 0, bool isReport = true, bool? isCurrentProgramSession = false)
        {
            if (Request.IsAjaxRequest())
            {
                var programSessionList = new List<ProgramSessionDto>();
                var authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                try
                {
                    if (!isReport)
                    {
                        if (itemIdList != null)
                        {
                            var itemType = _itemService.LoadById(itemIdList.FirstOrDefault()).ItemType;
                            if (itemType == (int)ItemType.CommonItem && purposeId == SelectionType.NotApplicable)
                                return Json(new { programSessionList = (MultiSelectList)null, isSuccess = true });

                            if ((itemType == (int)ItemType.CommonItem && (purposeId > 0 && purposeId <= 3)) || itemType == (int)ItemType.ProgramItem)
                            {
                                programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, itemIdList).ToList();
                            }
                            else if (itemType == (int)ItemType.CommonItem && (purposeId > 3 && purposeId <= 5))
                            {
                                programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, itemIdList).ToList();
                                var lists = new SelectList(programSessionList, "ProgramAndSessionId", "ProgramAndSessionName").ToList();
                                //lists.Insert(0, new SelectListItem() { Value = SelectionType.NotApplicable.ToString(CultureInfo.InvariantCulture), Text = "N/A" });
                                return Json(new { programSessionList = lists, isSuccess = true });
                            }
                        }
                        else
                        {
                            programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, null, isCurrentProgramSession).ToList();
                        }
                    }
                    else
                    {
                        programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, itemIdList).ToList();
                    }
                    var list = new MultiSelectList(programSessionList, "ProgramAndSessionId", "ProgramAndSessionName");
                    return Json(new { programSessionList = list, isSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    throw;
                }
            }
            return null;
        }
        */

        [HttpPost]
        public ActionResult AddRequisitionItemDetailsRow(int index, long? organizationId, long? branchId)
        {
            try
            {
                ViewBagForItemDetailsRow();
                ViewBag.Index = index + 1;
                return PartialView("_partial/_AddRowItemDetails");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return RedirectToAction("Create");
            }
        }

        #region Render Requisition Slip Data Table

        [HttpPost]
        public ActionResult RenderRequisitionSlipList(int draw, int start, int length, string organizationId, string branchId
                                                                , string itemName, string requiredDate, string requisitionStatus, bool isListForIssue)
        {
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Organization";
                            break;
                        case "1":
                            orderBy = "Branch";
                            break;
                        case "2":
                            orderBy = "ItemName";
                            break;
                        case "3":
                            orderBy = "CreationDate";
                            break;
                        case "4":
                            orderBy = "RequiredDate";
                            break;
                        case "5":
                            orderBy = "RequisitionNo";
                            break;
                        case "6":
                            orderBy = "RequisionStatus";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                #endregion

                long organization, branch; int rqStatus;
                GetParamsForList(out organization, out branch, out rqStatus, organizationId, branchId, requisitionStatus);

                var requisitionSlipList = _requisitionService.LoadRequisitionSlipList(start, length, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu, organization,
                                                                           branch, itemName, requiredDate, rqStatus, isListForIssue);
                var recordsTotal = _requisitionService.CountRequisitionSlip(orderBy.ToUpper(), orderDir, (List<UserMenu>)ViewBag.UserMenu, organization,
                                                                        branch, itemName, requiredDate, rqStatus, isListForIssue);
                long recordsFiltered = recordsTotal;

                foreach (var requisition in requisitionSlipList)
                {
                    var itemNameString = "";
                    var statusString = "";
                    var actionString = "";

                    var itemNameStringList = requisition.RequisitionDetails.Select(rd => rd.Item.Name).ToList();
                    var uniqueItemNameStringList = new HashSet<string>(itemNameStringList);
                    itemNameString = string.Join(",", uniqueItemNameStringList);

                    var rStatus = requisition.RequisionStatus;
                    switch (rStatus)
                    {
                        case (int)RequisitionStatus.Pending:
                            statusString = isListForIssue ? "Not Issued" : "Submitted";
                            break;
                        case (int)RequisitionStatus.Partially:
                            statusString = isListForIssue ? "Partially Issued" : "Received";
                            break;
                        case (int)RequisitionStatus.Completed:
                            statusString = isListForIssue ? "Issued" : "Received";
                            break;
                        case (int)RequisitionStatus.Cancelled:
                            statusString = "Cancelled";
                            break;
                        default:
                            statusString = "";
                            break;
                    }

                    actionString += LinkGenerator.GetDynamicLink("Details", "Requisition", requisition.Id, "glyphicon-th-list");

                    if (isListForIssue)
                    {

                        if (rStatus == (int)RequisitionStatus.Pending || rStatus == (int)RequisitionStatus.Partially)
                        {
                            actionString += LinkGenerator.GetDynamicLink("RequisitionGoodsIssue", "RequisitionGoodsIssue", requisition.Id, "glyphicon-new-window");
                        }
                        if (rStatus == (int)RequisitionStatus.Partially || rStatus == (int)RequisitionStatus.Completed)
                        {
                            actionString += LinkGenerator.GetDynamicLink("GoodsIssueLog", "RequisitionGoodsIssue", requisition.Id, "glyphicon-time");
                        }
                    }
                    else
                    {
                        if (rStatus == (int)RequisitionStatus.Pending)
                        {
                            actionString += LinkGenerator.GetDynamicLink("CancelRequisition", "Requisition", requisition.Id, "glyphicon-remove");
                        }
                    }
                    var str = new List<string>();
                    str.Add(requisition.Branch.Organization != null ? requisition.Branch.Organization.ShortName : "");
                    str.Add(requisition.Branch != null ? requisition.Branch.Name : "");
                    str.Add(itemNameString);
                    str.Add(requisition.CreationDate.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture));
                    str.Add(requisition.RequiredDate.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture));
                    str.Add(requisition.RequisitionNo);
                    str.Add(statusString);
                    str.Add(actionString);
                    getData.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData,
                    isSuccess = true
                });
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }

        #endregion

        #endregion

        #region Helper functions

        private RequisitionViewModel InitialRequisitionSlipDetailsViewModel(Requisition requisitionObj)
        {
            var model = new RequisitionViewModel();
            model.Id = requisitionObj.Id;
            model.Organization = requisitionObj.Branch.Organization.Name;
            model.Branch = requisitionObj.Branch.Name;
            model.RequiredDate = requisitionObj.RequiredDate.ToString("MMM dd, yyyy");
            model.RequisitionNo = requisitionObj.RequisitionNo;
            model.RequisionStatus = GetEnumDescription.Description((RequisitionStatus)requisitionObj.RequisionStatus);

            model.Remarks = requisitionObj.Remarks;

            model.RequisitionDetails = new List<RequisitionItemListViewModel>();
            foreach (var row in requisitionObj.RequisitionDetails)
            {
                var details = new RequisitionItemListViewModel();
                string purpose = "N/A";
                string programSession = "N/A";
                if (row.Purpose != null && row.Purpose > 0)
                {
                    purpose = GetEnumDescription.Description((Purpose)row.Purpose);
                }
                if (row.Program != null && row.Session != null)
                    programSession = _programService.GetProgram(row.Program.Id).ShortName + " " + _sessionService.LoadById(row.Session.Id).Name;

                details.Purpose = purpose;
                details.ProgramSession = programSession;
                details.Item = row.Item.Name;
                details.ItemGroup = row.Item.ItemGroup.Name;
                details.Quantity = row.RequiredQuantity.ToString();
                model.RequisitionDetails.Add(details);
            }
            return model;
        }

        private static void GetParamsForList(out long organization, out long branch, out int rqStatus, string organizationId, string branchId, string requisitionStatus)
        {
            branch = !string.IsNullOrWhiteSpace(branchId.Trim()) ? Convert.ToInt64(branchId) : 0;
            organization = !string.IsNullOrWhiteSpace(organizationId.Trim()) ? Convert.ToInt64(organizationId) : 0;
            rqStatus = !string.IsNullOrWhiteSpace(requisitionStatus) ? Convert.ToInt32(requisitionStatus) : 0;
        }

        #endregion

        #region Other's Function

        private void InitializeIndex(bool isGoodsIssue)
        {
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization((List<UserMenu>)ViewBag.UserMenu), "Id", "ShortName");
            ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
            if (isGoodsIssue)
            {
                ViewBag.RequisitionStatus = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem {Text = "Not Issued", Value = "1"},
                    new SelectListItem {Text = "Issued", Value = "2"},
                    new SelectListItem {Text = "Partially Issued", Value = "3"}
                }, "Value", "Text");
            }
            else
            {
                ViewBag.RequisitionStatus = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem {Text = "Submitted", Value = "1"},
                    new SelectListItem {Text = "Received", Value = "2"},
                    new SelectListItem {Text = "Canceled", Value = "3"}
                }, "Value", "Text");
            }

        }

        private void InitializeView(RequisitionForm objForm)
        {
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization((List<UserMenu>)ViewBag.UserMenu), "Id", "ShortName");
            ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
            if (objForm.RequisitionDetails != null && objForm.RequisitionDetails.Count > 0)
            {
                ViewBagForListOfItemDetailsRow(objForm);
            }
            else
            {
                objForm.RequisitionDetails = null;
                ViewBagForItemDetailsRow();
            }
        }

        private void ViewBagForListOfItemDetailsRow(RequisitionForm objForm)
        {
            var purposeList = _commonHelper.LoadEmumToDictionary<Purpose>().ToList();
            ViewBag.PurposeList = new SelectList(purposeList.OrderBy(x => x.Key), "Key", "Value");
            ViewBag.ItemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
            ViewBag.ProgramSessionIdlist = new SelectList(string.Empty, "Value", "Text");

            long organizationId = objForm.OrganizationId;
            var branchList = _branchService.LoadAuthorizedBranch((List<UserMenu>)ViewBag.UserMenu,
                                    _commonHelper.ConvertIdToList(organizationId), null, null, false, false);
            //var branchList = _branchService.LoadBranch(organizationIds, null, null, null, null, false);
            ViewBag.BranchList = new SelectList(branchList, "Id", "Name", objForm.BranchId);

            IList<SelectList> itemNameListArray = new List<SelectList>();
            IList<SelectList> itemProgramSessionListArray = new List<SelectList>();
            foreach (var row in objForm.RequisitionDetails)
            {
                long itemGroupId = row.ItemGroup;
                long itemId = row.Item;

                var itemList = _itemService.LoadItemForOrganizationOrGroup((List<UserMenu>)ViewBag.UserMenu, Convert.ToInt64(organizationId),
                                                                            _commonHelper.ConvertIdToList(Convert.ToInt64(itemGroupId)).ToArray());
                //var itemList = _itemService.LoadItem(Convert.ToInt64(organizationId), Convert.ToInt64(itemGroupId));
                var itemSelectList = new SelectList(itemList.ToList(), "Id", "Name");
                itemNameListArray.Add(itemSelectList);


                var itemProgramSessionList = _itemProgramSessionService.LoadProgramSessionItemDtoByItemId(new long[] { itemId });
                var list = new SelectList(itemProgramSessionList, "ProgramSessionAndItemId", "ProgramAndSessionName");
                if (row.Purpose > 3)
                {
                    var pList = new SelectListItem() { Text = "N/A", Value = "0" };
                    List<SelectListItem> newList = list.ToList();
                    newList.Insert(0, pList);
                    list = new SelectList(newList, "Value", "Text", row.Purpose);
                }
                itemProgramSessionListArray.Add(list);
            }
            ViewBag.ItemNameListArray = itemNameListArray;
            ViewBag.ItemProgramSessionListArray = itemProgramSessionListArray;
        }

        private void ViewBagForItemDetailsRow()
        {
            var purposeList = _commonHelper.LoadEmumToDictionary<Purpose>().ToList();
            ViewBag.PurposeList = new SelectList(purposeList.OrderBy(x => x.Key), "Key", "Value");
            ViewBag.ItemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
            ViewBag.ProgramSessionIdlist = new SelectList(string.Empty, "Value", "Text");
        }

        #endregion

        #region Report

        #region Requisition Report/Export/Print

        public ActionResult RequisitionReport(string message = "")
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                ViewBag.ErrorMessage = message;
            }
            InitialReportView();
            return View();
        }

        [HttpPost]
        public ActionResult RequisitionReport(RequisitionReportForm model)
        {
            string message = "";
            if (ModelState.IsValid)
            {
                try
                {
                    var authMenu = (List<UserMenu>)ViewBag.UserMenu;
                    InitialReportHeaderDiv(model);
                    if (model.ReportType == (int)RequisitionReportType.ListWise)
                    {
                        return PartialView("_partial/_RequisitionListViewReport", model);
                    }

                    long[] bIds;
                    var branchIds = model.BranchId;
                    if (branchIds != null && branchIds.Any() && branchIds[0] > 0)
                    {
                        bIds = AuthHelper.LoadBranchIdList(authMenu, null, null, branchIds.ToList()).ToArray();
                    }
                    else
                    {
                        bIds = AuthHelper.LoadBranchIdList(authMenu, _commonHelper.ConvertIdToList(model.OrganizationId)).ToArray();
                    }

                    if (model.ReportType == (int)RequisitionReportType.BranchWise)
                    {
                        IList<string> branchNameList = _branchService.LoadBranch(bIds).Select(x => x.Name).ToList();
                        ViewBag.BranchNameList = branchNameList;
                        return PartialView("_partial/_RequisitionReportByBranch", model);
                    }
                    if (model.ReportType == (int)RequisitionReportType.ProgramWise)
                    {
                        if (model.ProgramSession != null && model.ProgramSession[0] != "")
                        {
                            IList<string> programSessionList = GetProgramSessionNameList(model.ProgramSession);
                            ViewBag.ProgramSessionList = programSessionList;
                        }
                        else
                        {
                            if (model.ItemId != null)
                            {
                                model.ItemId = model.ItemId.Where(val => val != 0).ToArray();
                            }

                            if (model.ItemId == null)
                            {
                                if (model.ItemGroupId != null)
                                {
                                    model.ItemGroupId = model.ItemGroupId.Where(x => x != 0).ToArray();
                                }
                                model.ItemId = _itemService.LoadItem(model.ItemGroupId, model.OrganizationId).Select(x => x.Id).ToArray();
                            }

                            model.ProgramSession = _requisitionService.FilterProgramSession((List<UserMenu>)ViewBag.UserMenu, _commonHelper.ConvertIdToList(model.OrganizationId)
                                                                                                , bIds, model.ItemId, model.ProgramSession);
                            ViewBag.ProgramSessionList = GetProgramSessionNameList(model.ProgramSession);
                        }
                        return PartialView("_partial/_RequisitionReportByProgramSession", model);
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    _logger.Error(ex);
                }
            }
            else
            {
                message = WebHelper.CommonErrorMessage;
            }
            return View(model);
        }

        #region Print/Export

        public ActionResult ExportListViewReport(long organizationId, long[] branchIds, int reportType, int[] purposeIds, long[] itemGroupIds, long[] itemIds,
                                    string[] programSession, int[] requisitionStatus, DateTime fromDate, DateTime toDate,
                                    string branchlbl, string purposelbl, string programSessionlbl, string statuslbl)
        {

            var excelList = new List<List<object>>();
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime startDate = Convert.ToDateTime(fromDate);
                DateTime endDate = Convert.ToDateTime(toDate);

                var organization = _organizationService.LoadById(organizationId);

                var headerList = new List<string>()
                {
                    organization.Name,
                    GetEnumDescription.Description((RequisitionReportType)reportType) + " Requisition Report",
                    fromDate.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture) 
                        + " To " + toDate.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture)
                };

                var headerClList = new List<List<object>>
                {
                    new List<object>
                    {
                        "Branch", "Propose", "Program & Session", "Status"
                    },
                    new List<object>
                    {
                        branchlbl, purposelbl, programSessionlbl, statuslbl
                    }
                };


                var footerList = new List<string>();
                footerList.Add("");

                var columnList = new List<string> { "Branch", "Requisition Date", "Requisition No", "Status" };

                int recordsTotal = _requisitionService.GetRequisitionReportByListCount((List<UserMenu>)ViewBag.UserMenu, organizationId,
                                    branchIds, itemGroupIds, itemIds, programSession, purposeIds, requisitionStatus, startDate, endDate);
                IList<RequisitionListReportDto> requisitionList = _requisitionService.RequisitionReportByList(0, recordsTotal, "CreationDate", "desc", (List<UserMenu>)ViewBag.UserMenu, organizationId,
                                    branchIds, itemGroupIds, itemIds, programSession, purposeIds, requisitionStatus, startDate, endDate).ToList();

                foreach (var row in requisitionList)
                {
                    var xlsRow = new List<object>();
                    xlsRow.Add(row.Branch);
                    xlsRow.Add(row.RequisitionDate);
                    xlsRow.Add(row.RequisitionNo);

                    string statusString = "";
                    var rStatus = row.RequisitionStatus;
                    switch (rStatus)
                    {
                        case (int)RequisitionStatus.Pending:
                            statusString = "Submitted";
                            break;
                        case (int)RequisitionStatus.Partially:
                            statusString = "Received";
                            break;
                        case (int)RequisitionStatus.Completed:
                            statusString = "Received";
                            break;
                        case (int)RequisitionStatus.Cancelled:
                            statusString = "Cancelled";
                            break;
                        default:
                            statusString = "None";
                            break;
                    }
                    xlsRow.Add(statusString);
                    excelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, headerClList, columnList, excelList, footerList, "Requisition_List_Report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }
            catch (Exception ex)
            {
                return View("AutoClose");
            }
        }

        public ActionResult ExportBranchViewReport(long organizationId, long[] branchIds, int reportType, int[] purposeIds, long[] itemGroupIds, long[] itemIds,
                                                    string[] programSession, int[] requisitionStatus, DateTime fromDate, DateTime toDate, string branchlbl, string purposelbl,
                                                        string programSessionlbl, string statuslbl, string itemlbl, string itemGrouplbl)
        {

            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime startDate = Convert.ToDateTime(fromDate);
                DateTime endDate = Convert.ToDateTime(toDate);

                var organization = _organizationService.LoadById(organizationId);

                var headerList = new List<string>()
                {
                    organization.Name,
                    GetEnumDescription.Description((RequisitionReportType)reportType) + " Requisition Report",
                    fromDate.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture) 
                        + " To " + toDate.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture),
                    "Branch(s): " + branchlbl,
                    "Purpose(s): " + purposelbl,
                    "Item Group(s): " + itemGrouplbl,
                    "Item(s): " + itemlbl,
                    "Status: " + statuslbl
                };

                var footerList = new List<string>();
                footerList.Add("");
                string columnString = "Item Group,Item Name,Program & Session,";

                int recordsTotal = _requisitionService.RequisitionReportByBranchCount((List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds, purposeIds
                                                                                        , programSession, itemGroupIds, itemIds, requisitionStatus, fromDate, toDate);
                if (recordsTotal < 1)
                {
                    return View("AutoClose");
                }
                IList<dynamic> objectList = _requisitionService.RequisitionReportByBranch(0, recordsTotal, "", "", (List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds, purposeIds
                                                                                        , programSession, itemGroupIds, itemIds, requisitionStatus, fromDate, toDate).ToList();


                var excelList = new List<List<object>>();
                bool firstCall = true;
                foreach (var obj in objectList)
                {
                    int index = 0;
                    var xlsRow = new List<object>();
                    foreach (KeyValuePair<string, object> entry in obj)
                    {
                        if (index > 2)
                        {
                            if (firstCall)
                            {
                                columnString += entry.Key + ",";
                            }
                            xlsRow.Add(entry.Value ?? 0);
                        }
                        else
                        {
                            xlsRow.Add((entry.Value != null) ? entry.Value.ToString() : "-");
                        }
                        index++;
                    }
                    excelList.Add(xlsRow);
                    firstCall = false;
                }
                var columnList = new List<string>();
                columnList.AddRange(columnString.Split(',').ToList());
                ExcelGenerator.GenerateExcel(headerList, columnList, excelList, footerList, "BranchWiseRequisition_Report_" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View("AutoClose");
        }

        public ActionResult ExportProgramSessionViewReport(long organizationId, long[] branchIds, int reportType, int[] purposeIds, long[] itemGroupIds, long[] itemIds,
                                                    string[] programSession, int[] requisitionStatus, DateTime fromDate, DateTime toDate, string branchlbl, string purposelbl,
                                                        string programSessionlbl, string statuslbl, string itemlbl, string itemGrouplbl)
        {

            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime startDate = Convert.ToDateTime(fromDate);
                DateTime endDate = Convert.ToDateTime(toDate);

                var organization = _organizationService.LoadById(organizationId);

                var headerList = new List<string>()
                {
                    organization.Name,
                    GetEnumDescription.Description((RequisitionReportType)reportType) + " Requisition Report",
                    fromDate.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture) 
                        + " To " + toDate.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture),
                    "Branch(s): " + branchlbl,
                    "Purpose(s): " + purposelbl,
                    "Item Group(s): " + itemGrouplbl,
                    "Item(s): " + itemlbl,
                    "Status: " + statuslbl
                };

                var footerList = new List<string>();
                footerList.Add("");

                var columnList = new List<string>()
                {
                    "Item Group", "Item Name"
                };

                int recordsTotal = _requisitionService.RequisitionReportByProgramSessionCount((List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds, purposeIds
                                                                                        , programSession, itemGroupIds, itemIds, requisitionStatus, startDate, endDate);
                if (recordsTotal < 1)
                {
                    return View("AutoClose");
                }

                IList<dynamic> objectList = _requisitionService.RequisitionReportByProgramSession(0, recordsTotal, "", "", (List<UserMenu>)ViewBag.UserMenu, organizationId
                                                                                                , branchIds, purposeIds, itemIds, itemGroupIds, programSession
                                                                                                , requisitionStatus, startDate, endDate).ToList();

                var excelList = new List<List<object>>();
                bool firstCall = true;
                foreach (var obj in objectList)
                {
                    int index = 0;
                    var xlsRow = new List<object>();
                    foreach (KeyValuePair<string, object> entry in obj)
                    {
                        if (index > 1)
                        {
                            xlsRow.Add(entry.Value ?? 0);
                            if (firstCall)
                                columnList.Add(entry.Key);
                        }
                        else
                        {
                            xlsRow.Add((entry.Value != null) ? entry.Value.ToString() : "-");
                        }
                        index++;
                    }
                    firstCall = false;
                    excelList.Add(xlsRow);
                }
                string fileName = "Requisition_Report_" + DateTime.Now.ToString("yyyyMMdd-HHmmss");
                ExcelGenerator.GenerateExcel(headerList, columnList, excelList, footerList, fileName);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View("AutoClose");
        }

        #endregion

        #endregion

        #region Ajax Functions


        [HttpPost]
        public ActionResult RequisitionListViewReport(int draw, int start, int length, long organizationId, long[] branchIds
                                                        , int[] purposeIds, long[] itemGroupIds, long[] itemIds, string[] programSession
                                                        , int[] status, DateTime dateFrom, DateTime dateTo)
        {
            int recordsFiltered = 0;
            int recordsTotal = 0;
            bool isSuccess = true;

            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Branch";
                            break;
                        case "1":
                            orderBy = "CreationDate";
                            break;
                        case "2":
                            orderBy = "RequisitionNo";
                            break;
                        case "3":
                            orderBy = "RequisionStatus";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                #endregion


                IList<RequisitionListReportDto> list = _requisitionService.RequisitionReportByList(start, length, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu, organizationId,
                                    branchIds, itemGroupIds, itemIds, programSession, purposeIds, status, dateFrom, dateTo).ToList<RequisitionListReportDto>();
                recordsTotal = _requisitionService.GetRequisitionReportByListCount((List<UserMenu>)ViewBag.UserMenu, organizationId,
                                    branchIds, itemGroupIds, itemIds, programSession, purposeIds, status, dateFrom, dateTo);
                recordsFiltered = recordsTotal;

                int sl = 1;
                foreach (var row in list)
                {
                    string statusString = "";
                    var rStatus = row.RequisitionStatus;
                    switch (rStatus)
                    {
                        case (int)RequisitionStatus.Pending:
                            statusString = "Submitted";
                            break;
                        case (int)RequisitionStatus.Partially:
                            statusString = "Received";
                            break;
                        case (int)RequisitionStatus.Completed:
                            statusString = "Received";
                            break;
                        case (int)RequisitionStatus.Cancelled:
                            statusString = "Cancelled";
                            break;
                        default:
                            statusString = "";
                            break;
                    }

                    string actionString = LinkGenerator.GetDynamicLink("Details", "Requisition", row.Id, "glyphicon-th-list");
                    var str = new List<string>
                    {
                        sl.ToString(CultureInfo.CurrentCulture),
                        !string.IsNullOrWhiteSpace(row.Branch) ? row.Branch : "---",
                        row.RequisitionDate.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture),
                        row.RequisitionNo,
                        statusString,
                        actionString
                    };
                    getData.Add(str);
                    sl++;
                }
            }
            catch (NullObjectException ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        [HttpPost]
        public ActionResult RequisitionReportByBranch(int draw, int start, int length, long organizationId, long[] branchIds
                                                        , int[] purposeIds, long[] itemGroupIds, long[] itemIds, string[] programSession
                                                        , int[] status, DateTime dateFrom, DateTime dateTo)
        {
            int recordsFiltered = 0;
            int recordsTotal = 0;
            bool isSuccess = true;

            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "ItemGroup";
                            break;
                        case "1":
                            orderBy = "ItemName";
                            break;
                        default:
                            orderBy = "ItemGroup";
                            break;
                    }
                }
                else
                {
                    orderBy = "ItemGroup";
                    orderDir = "desc";
                }
                #endregion

                IList<dynamic> queryResult = _requisitionService.RequisitionReportByBranch(start, length, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds, purposeIds
                                                                                        , programSession, itemGroupIds, itemIds, status, dateFrom, dateTo).ToList();
                recordsTotal = _requisitionService.RequisitionReportByBranchCount((List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds, purposeIds
                                                                                        , programSession, itemGroupIds, itemIds, status, dateFrom, dateTo);

                int sl = start + 1;
                foreach (var obj in queryResult)
                {
                    int index = 1;
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    IDictionary<string, object> dic;
                    foreach (KeyValuePair<string, object> entry in dic = obj)
                    {
                        if (index > 3)
                        {
                            str.Add((entry.Value != null) ? entry.Value.ToString() : "0");
                        }
                        else
                        {
                            str.Add((entry.Value != null) ? entry.Value.ToString() : "-");
                        }
                        index++;
                    }
                    getData.Add(str);
                    sl++;
                }

                recordsFiltered = recordsTotal;
            }
            catch (NullObjectException ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        [HttpPost]
        public ActionResult RequisitionReportByProgramSession(int draw, int start, int length, long organizationId, long[] branchIds
                                                        , int[] purposeIds, long[] itemGroupIds, long[] itemIds, string[] programSession
                                                        , int[] status, DateTime dateFrom, DateTime dateTo)
        {
            int recordsFiltered = 0;
            int recordsTotal = 0;
            bool isSuccess = true;

            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "ItemGroup";
                            break;
                        case "1":
                            orderBy = "ItemName";
                            break;
                        default:
                            orderBy = "ItemGroup";
                            break;
                    }
                }
                else
                {
                    orderBy = "ItemGroup";
                    orderDir = "desc";
                }
                #endregion

                IList<dynamic> queryResult = _requisitionService.RequisitionReportByProgramSession(start, length, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu, organizationId
                                                                                                , branchIds, purposeIds, itemIds, itemGroupIds, programSession
                                                                                                , status, dateFrom, dateTo).ToList();
                recordsTotal = _requisitionService.RequisitionReportByProgramSessionCount((List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds, purposeIds
                                                                                        , programSession, itemGroupIds, itemIds, status, dateFrom, dateTo);

                int sl = start + 1;
                foreach (var obj in queryResult)
                {
                    int index = 1;
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    IDictionary<string, object> dic;
                    foreach (KeyValuePair<string, object> entry in dic = obj)
                    {
                        if (index > 2)
                        {
                            str.Add((entry.Value != null) ? entry.Value.ToString() : "0");
                        }
                        else
                        {
                            str.Add((entry.Value != null) ? entry.Value.ToString() : "N/A");
                        }
                        index++;
                    }
                    getData.Add(str);
                    sl++;
                }
                recordsFiltered = recordsTotal;
            }
            catch (NullObjectException ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }


        #endregion

        #region Helper Functions

        private void InitialReportView()
        {
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization((List<UserMenu>)ViewBag.UserMenu), "Id", "ShortName");
            ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
            var purposeList = (new Dictionary<int, string> { { 0, "Select All" } }).Concat(_commonHelper.LoadEmumToDictionary<Purpose>()).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.PurposeList = new SelectList(purposeList.ToList().OrderBy(x => x.Key), "Key", "Value");

            ViewBag.ItemGroupList = new SelectList((new Dictionary<long, string> { { 0, "Select All" } })
                                                    .Concat(_itemGroupService.LoadItemGroup().ToDictionary(ig => ig.Id, ig => ig.Name))
                                                    .ToDictionary(k => k.Key, v => v.Value)
                                                  .OrderBy(x => x.Key).ToList(), "Key", "Value");
            ViewBag.ReportTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<RequisitionReportType>().ToList().OrderBy(x => x.Key), "Key", "Value");
            ViewBag.ProgramSessionIdlist = new SelectList(string.Empty, "Value", "Text");
            ViewBag.Itemlist = new SelectList(string.Empty, "Value", "Text");

            var statusList = (new Dictionary<int, string> { { 0, "Select All" } }).Concat(_commonHelper.LoadEmumToDictionary<RequisitionStatus>()).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.RequisitionStatus = new SelectList(statusList.ToList().OrderBy(x => x.Key), "Key", "Value");
        }

        private void InitialReportHeaderDiv(RequisitionReportForm model)
        {
            #region Report Title

            string reportTypeText = "";
            switch (model.ReportType)
            {
                case (int)RequisitionReportType.ListWise:
                    reportTypeText = "List Wise Requisition Report";
                    break;

                case (int)RequisitionReportType.BranchWise:
                    reportTypeText = "Branch Wise Requisition Report";
                    break;

                case (int)RequisitionReportType.ProgramWise:
                    reportTypeText = "Program Wise Requisition Report";
                    break;
                default:
                    reportTypeText = "";
                    break;
            }

            var organization = _organizationService.LoadById(model.OrganizationId);
            var reportTitle = new string[3];
            reportTitle[0] = organization.Name;
            reportTitle[1] = reportTypeText;
            //.ToString("MMM d, yyyy", CultureInfo.CreateSpecificCulture("en-US")); Display as Aug 10, 2016
            reportTitle[2] = model.DateFrom.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture) + " To " + model.DateTo.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture);

            #endregion

            #region Program & Session List

            var programSession = "";
            if (model.ProgramSession != null && model.ProgramSession[0] != "0")
            {
                var psNamelist = GetProgramSessionNameList(model.ProgramSession);
                programSession = string.Join(",", psNamelist);
            }
            else if (model.ProgramSession == null || model.ProgramSession[0] == "0")
            {
                programSession = "All";
            }
            #endregion

            #region Purpose List

            var purpose = "All";
            if (model.PurposeId != null && model.PurposeId[0] > 0)
            {
                purpose = model.PurposeId.Aggregate("", (current, purposeId) => current + (GetEnumDescription.Description((Purpose)purposeId) + ", "));
                purpose = purpose.TrimEnd(',');
            }

            #endregion

            #region Status

            var status = "All";
            if (model.RequisitionStatus != null && model.RequisitionStatus[0] > 0 && model.RequisitionStatus.Count() < 4)
            {
                status = model.RequisitionStatus.Aggregate("", (current, statusId) => current + (GetEnumDescription.Description((RequisitionStatus)statusId) + ", "));
                status = status.TrimEnd(',');
            }

            #endregion

            var branchName = "";
            if (model.BranchId != null && model.BranchId[0] > 0)
            {
                branchName = string.Join(", ", _branchService.LoadBranch(model.BranchId).Select(x => x.Name).ToList());
            }
            else if (model.BranchId == null || model.BranchId[0] == 0)
            {
                branchName = "All";
            }

            var itemGroupString = "";
            if (model.ItemGroupId != null && model.ItemGroupId[0] > 0)
            {
                itemGroupString = string.Join(", ", _itemGroupService.LoadItemGroup(model.ItemGroupId.ToList()).Select(x => x.Name).ToList());
            }
            else if (model.ItemGroupId == null || model.ItemGroupId[0] == 0)
            {
                itemGroupString = "All";
            }
            var itemString = "";
            if (model.ItemId != null && model.ItemId[0] > 0)
            {
                itemString = string.Join(", ", string.Join(", ", _itemService.LoadItemList(model.ItemId).Select(x => x.Name).ToList()));
            }
            else if (model.ItemId == null || model.ItemId[0] == 0)
            {
                itemString = "All";
            }

            ViewBag.ReportTitle = reportTitle;
            ViewBag.Organizationlbl = organization.ShortName;
            ViewBag.Branchlbl = branchName;
            ViewBag.Purposelbl = purpose;
            ViewBag.ProgramSessionlbl = programSession;
            ViewBag.ItemGrouplbl = itemGroupString;
            ViewBag.Itemlbl = itemString;
            ViewBag.Statuslbl = status;
        }

        private IList<string> GetProgramSessionNameList(IEnumerable<string> programSession)
        {
            IList<string> programSessionNameList = new List<string>();

            foreach (var ps in programSession)
            {
                string[] psIds = ps.Split(new string[] { "::" }, StringSplitOptions.None);
                if (psIds.Length == 2)
                {
                    string programName = _programService.GetProgram(Convert.ToInt64(psIds[0])).ShortName;
                    string sessionName = _sessionService.LoadById(Convert.ToInt64(psIds[1])).Name;
                    if (!string.IsNullOrWhiteSpace(programName) && !string.IsNullOrWhiteSpace(sessionName))
                    {
                        programSessionNameList.Add(programName + " " + sessionName);
                    }
                }
                else
                {
                    programSessionNameList.Add("N/A");
                }
            }
            return programSessionNameList.Distinct().ToList();
        }

        #endregion

        #endregion
    }
}