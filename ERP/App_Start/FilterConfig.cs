﻿using System.Web;
using System.Web.Mvc;
using UdvashERP.App_Start;

namespace UdvashERP
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new AuthorizeAccess());
        }
    }
}
