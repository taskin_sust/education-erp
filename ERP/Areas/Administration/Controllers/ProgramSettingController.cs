﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class ProgramSettingController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly IOrganizationService _organizationService;
        private readonly IProgramSettingService _programSettingService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private CommonHelper _commonHelper;
        public ProgramSettingController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(session);
            _programSettingService = new ProgramSettingService(session);
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _commonHelper=new CommonHelper();
        }
        #endregion
        
        #region Operational Function
        [HttpGet]
        public ActionResult ManageProgramSetting()
        {
            ViewBag.ShowResult = false;
            ViewBag.ProgramSetting = new List<ProgramSettings>();
            InitializeManageProgramSetting();
            return View();
        }
        [HttpPost]
        public JsonResult LoadProgramSettingList(int draw, int start, int length, string organizationId, string programId, string sessionId)
        {
            var getData = new List<object>();
            try
            {
                if (String.IsNullOrEmpty(organizationId) || String.IsNullOrEmpty(programId) ||String.IsNullOrEmpty(sessionId))
                {
                    return Json(new{draw,recordsTotal = 0,recordsFiltered = 0,start,length,data = getData,isSuccess = false});
                }
                var programSettingList = _programSettingService.LoadProgramSetting(Convert.ToInt64(programId), Convert.ToInt64(sessionId));
                int recordsTotal = _programSettingService.GetProgramSettingCount(Convert.ToInt64(programId), Convert.ToInt64(sessionId));
                long recordsFiltered = recordsTotal;
                foreach (var programSetting in programSettingList)
                {
                    var str = new List<string>();
                    str.Add(programSetting.NextProgram.Organization.ShortName ?? "");
                    str.Add(programSetting.NextProgram.Name);
                    str.Add(programSetting.NextSession.Name);

                    str.Add(LinkGenerator.GetGeneratedLink("Edit", "Delete", "ProgramSetting", programSetting.Id, programSetting.NextProgram.Name));
                    getData.Add(str);
                }
                return Json(new
                {
                    draw,
                    recordsTotal,
                    recordsFiltered,
                    start,
                    length,
                    data = getData,
                    isSuccess = true
                });

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                //ViewBag.ProgramSetting = new List<ProgramSettings>();
                //InitializeManageProgramSetting();
            }
            return Json(new
            {
                 draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start,
                length,
                data = getData,
                isSuccess = false
            });
        }

        public ActionResult Create(string message)
        {
            try
            {
                ViewBag.SuccessMessage = message;
                InitializeView();
            }
            catch (Exception ex)
            {
                InitializeView();
                _logger.Error(ex);
            }
            return View();
        }

   
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(long organizationId, long programId, long sessionId, long nextOrganizationId, long nextProgramId, long nextSessionId)
        {
            try
            {
                var program = _programService.GetProgram(Convert.ToInt32(programId));
                var session = _sessionService.LoadById(Convert.ToInt32(sessionId));
                var nextProgram = _programService.GetProgram(Convert.ToInt32(nextProgramId));
                var nextSession = _sessionService.LoadById(Convert.ToInt32(nextSessionId));

                var programSetting = new ProgramSettings
                {
                    Program = program,
                    Session = session,
                    NextProgram = nextProgram,
                    NextSession = nextSession
                };
                var isSuccess = _programSettingService.Save(programSetting);
                if (isSuccess)
                    return RedirectToAction("Create", new { message = "Program Setting successfully saved." });
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
            }
            InitializeView(organizationId, programId, sessionId, nextOrganizationId, nextProgramId, nextSessionId);
            return View();
        }


        [HttpGet]
        public ActionResult Edit(int id, string message = "")
        {
            try
            {
                if (!String.IsNullOrEmpty(message))
                    ViewBag.SuccessMessage = message;
                var programSetting = _programSettingService.GetProgramSetting(id);
                InitializeView(programSetting.Program.Organization.Id, programSetting.Program.Id, programSetting.Session.Id, programSetting.NextProgram.Organization.Id, programSetting.NextProgram.Id, programSetting.NextSession.Id);
            }
            catch (Exception ex)
            {
                InitializeView();
            }

            return View();
        }

   
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(int id, long organizationId, long programId, long sessionId, long nextOrganizationId, long nextProgramId, long nextSessionId)
        {
            try
            {
                var programSettingObj = _programSettingService.GetProgramSetting(id);
                var program = _programService.GetProgram(Convert.ToInt32(programId));
                var session = _sessionService.LoadById(Convert.ToInt32(sessionId));
                var nextProgram = _programService.GetProgram(Convert.ToInt32(nextProgramId));
                var nextSession = _sessionService.LoadById(Convert.ToInt32(nextSessionId));

                var isSuccess = _programSettingService.Update(programSettingObj, program, session, nextProgram, nextSession);
                if (isSuccess)
                    return RedirectToAction("Edit", new { id, Area = "Administration", message = "Program Setting successfully saved." });
            }
            catch (EmptyFieldException ex)
            {
                InitializeView();
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                InitializeView();
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                InitializeView();
                ViewBag.ErrorMessage = ex.Message;
            }
            InitializeView(organizationId, programId, sessionId, nextOrganizationId, nextProgramId, nextSessionId);
            return View();
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                var programSetting = _programSettingService.GetProgramSetting(Convert.ToInt32(id));
                bool isSuccess = _programSettingService.Delete(programSetting);
                return Json(isSuccess ? new Response(true, "Program Setting sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, "Program Setting Delete Fail !"));
            }
            catch (NullObjectException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, "Program Setting Delete Fail !"));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Program Setting Delete Fail !"));
            }
        }



        #endregion

        #region Helper Function
        private void InitializeView(long organization = 0, long program = 0, long session = 0, long nextOrganization = 0, long nextProgram = 0, long nextSession = 0)
        {
            var organizations = _organizationService.LoadOrganization(Organization.EntityStatus.Active);
            ViewBag.Organization = new SelectList(organizations.ToList(), "Id", "ShortName", organization);
            ViewBag.NextOrganization = new SelectList(organizations.ToList(), "Id", "ShortName", nextOrganization);

            if (organization != 0)
                ViewBag.Program = new SelectList(_programService.LoadProgram(_commonHelper.ConvertIdToList(organization)), "Id", "Name", program);
            else
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name", program);

            if (program != 0)
                ViewBag.Session = new SelectList(_sessionService.LoadSession(null, _commonHelper.ConvertIdToList(program)), "Id", "Name", session);
            else
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name", session);

            if (nextOrganization != 0)
                ViewBag.NextProgram = new SelectList(_programService.LoadProgram(_commonHelper.ConvertIdToList(nextOrganization)), "Id", "Name", nextProgram);
            else
                ViewBag.NextProgram = new SelectList(new List<Program>(), "Id", "Name", nextProgram);
            if (nextProgram != 0)
                ViewBag.NextSession = new SelectList(_sessionService.LoadSession(null,_commonHelper.ConvertIdToList(nextProgram)), "Id", "Name", nextSession);
            else
                ViewBag.NextSession = new SelectList(new List<Session>(), "Id", "Name", nextSession);

        }
        private void InitializeManageProgramSetting(long organizationId = 0, long programId = 0, long sessionId = 0)
        {
            var organizations = _organizationService.LoadOrganization(Organization.EntityStatus.Active);
            ViewBag.Organization = new SelectList(organizations.ToList(), "Id", "ShortName", organizationId);

            if (organizationId != 0)
                ViewBag.Program = new SelectList(_programService.LoadProgram(_commonHelper.ConvertIdToList(organizationId)), "Id", "Name", programId);
            else
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name", programId);

            if (programId != 0)
                ViewBag.Session = new SelectList(_sessionService.LoadSession(null, _commonHelper.ConvertIdToList(programId)), "Id", "Name", sessionId);
            else
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name", sessionId);
        }
        #endregion

    }
}
