﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.Administration
{
    public interface IProgramSessionSurveyQuestionDao : IBaseDao<ProgramSessionSurveyQuestion, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<ProgramSessionSurveyQuestion> LoadSurveyQuestionByProgramSession(long programId, long sessionId, int surveyType);
        IList<ProgramSessionSurveyQuestion> LoadSurveyQuestionByProgramSessionQuestionIds(long sessionId, long programId, int surveyType, long[] selectedQuestion);
        IList<ProgramSessionSurveyQuestion> LoadSurveyQuestionByProgramSessionAnswerIds(long sessionId, long programId, int surveyType, string[] selectedAnswer, long[] questionIds = null);
        #endregion

        #region Others Function
        bool IsExistCheckStudentByProgramSessionAndSurveyQuestion(long programId, long sessionId, long surveyQuestionId);
        #endregion

    }


    public class ProgramSessionSurveyQuestionDao : BaseDao<ProgramSessionSurveyQuestion, long>, IProgramSessionSurveyQuestionDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<ProgramSessionSurveyQuestion> LoadSurveyQuestionByProgramSession(long programId, long sessionId, int surveyType)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(ProgramSessionSurveyQuestion));
            criteria.CreateAlias("SurveyQuestion", "q").Add(Restrictions.Eq("q.Status", SurveyQuestion.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            criteria.Add(Restrictions.Eq("SurveyType", surveyType));
            criteria.AddOrder(Order.Asc("q.Rank"));
            var returnObj = (List<ProgramSessionSurveyQuestion>)criteria.List<ProgramSessionSurveyQuestion>();
            return returnObj;
        }
        public IList<ProgramSessionSurveyQuestion> LoadSurveyQuestionByProgramSessionQuestionIds(long sessionId, long programId, int surveyType, long[] selectedQuestion)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(ProgramSessionSurveyQuestion));
            criteria.CreateAlias("SurveyQuestion", "sq").Add(Restrictions.Eq("sq.Status", SurveyQuestion.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            criteria.Add(Restrictions.Eq("SurveyType", surveyType));
            if (!(Array.Exists(selectedQuestion, item => item == 0)))
            {
                criteria.Add(Restrictions.In("sq.Id", selectedQuestion));
            }

            var returnObj = (List<ProgramSessionSurveyQuestion>)criteria.List<ProgramSessionSurveyQuestion>();
            return returnObj;
        }

        public IList<ProgramSessionSurveyQuestion> LoadSurveyQuestionByProgramSessionAnswerIds(long sessionId, long programId, int surveyType,
            string[] selectedAnswer, long[] questionIds = null)
        {

            ICriteria criteria = Session.CreateCriteria(typeof(ProgramSessionSurveyQuestion));
            criteria.CreateAlias("SurveyQuestion", "sq")
                .Add(Restrictions.Eq("sq.Status", SurveyQuestion.EntityStatus.Active));
            criteria.CreateAlias("sq.SurveyQuestionAnswers", "sqa");
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            criteria.Add(Restrictions.Eq("SurveyType", surveyType));


            if (!(Array.Exists(selectedAnswer, item => item == "0")))
            {
                criteria.Add(Restrictions.In("sqa.Answer", selectedAnswer));
            }
            if (questionIds!=null && !(Array.Exists(questionIds, item => item == SelectionType.SelelectAll)))
            {
                criteria.Add(Restrictions.In("sq.Id", questionIds));
            }
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            var returnObj = (List<ProgramSessionSurveyQuestion>)criteria.List<ProgramSessionSurveyQuestion>();
            return returnObj;


        }
        #endregion

        #region Others Function
        public bool IsExistCheckStudentByProgramSessionAndSurveyQuestion(long programId, long sessionId, long surveyQuestionId)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(StudentSurveyAnswer)).Add(Restrictions.Eq("Status", StudentSurveyAnswer.EntityStatus.Active));
            criteria.CreateAlias("SurveyQuestion", "sq");
            criteria.Add(Restrictions.Eq("sq.Id", surveyQuestionId));
            criteria.CreateAlias("StudentProgram", "sp").Add(Restrictions.Eq("sp.Program.Id", programId));
            criteria.CreateAlias("sp.Batch", "ba").Add(Restrictions.Eq("ba.Session.Id", sessionId));
            // criteria.Add(Restrictions.Eq("sq.Program.Id", programId)).Add(Restrictions.Eq("sq.Session.Id", sessionId));
            var data = (List<StudentSurveyAnswer>)criteria.List<StudentSurveyAnswer>();
            if (data.Count > 0)
                return true;
            return false;
        }
        #endregion

        #region Helper Function

        #endregion

    }
}
