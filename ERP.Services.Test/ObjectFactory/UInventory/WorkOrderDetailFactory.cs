using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class WorkOrderDetailFactory : ObjectFactoryBase<WorkOrderDetail>
   {

       private readonly IWorkOrderDetailService _workOrderDetailService;

       public WorkOrderDetailFactory(IObjectFactoryBase caller, ISession session): base(caller, session){

       _workOrderDetailService = new WorkOrderDetailService(session);

       }

       #region Create 

       public WorkOrderDetailFactory Create(){

       Object = null;
       SingleObjectList.Add(Object);
       return this;
       }

       public WorkOrderDetailFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public WorkOrderDetailFactory Persist()       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _workOrderDetailService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x =>{if (x.Id == 0)_workOrderDetailService.SaveOrUpdate(x);});

          }
          return this;
       }

       public WorkOrderDetailFactory CreateMore(int num){

       for (int i = 0; i < num; i++){
       var obj = new WorkOrderDetail(){};
       ObjectList.Add(obj);}
       return this;}

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public WorkOrderDetailFactory CleanUp()
       {

          DeleteAll();
          return this;
       }

       #endregion

    }
}
