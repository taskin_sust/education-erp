﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UdvashERP.Areas.Student.Models
{
    public class BoardInformation
    {
        public long OrganizationId { get; set; }
        [Required(ErrorMessage = "You need to select program")]
        public long ProgramId { get; set; }

        [Required(ErrorMessage = "You need to select session")]
        public long SessionId { get; set; }

        [Required(ErrorMessage = "You need to select branch")]
        public long[] SelectedBranch { get; set; }

        [Required(ErrorMessage = "You need to select campus")]
        public long[] SelectedCampus { get; set; }

        [Required(ErrorMessage = "You need to select batch day")]
        public string[] SelectedBatchDays { get; set; }

        [Required(ErrorMessage = "You need to select batch time")]
        public string[] SelectedBatchTime { get; set; }

        [Required(ErrorMessage = "You need to select batch")]
        public long[] SelectedBatch { get; set; }

        [Required(ErrorMessage = "You need to select exam")]
        public long StudentExamId { get; set; }

        [DisplayName("Number Of Student")]
        public long NumberOfStudent { get; set; }
        public string[] InformationViewList { get; set; }
        public int SelectGrade { get; set; }
        public int SelectReportType { get; set; }
        public int? SelectStatus { get; set; }

        [Required(ErrorMessage = "You need to select exam year")]
        public virtual string ExamYear { get; set; }

        public virtual string[] InfoOption { get; set; }
        [DisplayName("Status")]
        public virtual int? SyncStatus { get; set; }


    }
}