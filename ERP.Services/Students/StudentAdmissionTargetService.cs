﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Exceptions;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentAdmissionTargetService : IBaseService
    {
        #region Operational Function
        void Save(StudentAdmissionTarget studentAdmissionTarget);
        bool Delete(long programId, long sessionId, DateTime deadLine, string deadLineTitle);
       
        bool Update(long programId, long sessionId, DateTime deadLine, string deadLineTitle, string deadLineTitleModified, DateTime deadLineModified, long[] brancheIds, int[] targets);

        #endregion

        #region Single Instances Loading Function
        DateTime LoadDeadLine(long programId, long sessionId, string deadLineTitle);
        
        StudentAdmissionTarget GetById(long id);
        StudentAdmissionTarget GetByProgramAndSessionId(long programId, long sessionId, DateTime? deadLineDate = null);
        #endregion

        #region List Loading Function
        IList<StudentBranchTarget> LoadBranchTargets(List<UserMenu> userMenu, long programId, long sessionId, string deadLineTitle, bool? isAuthorized = null);
        IList<string> LoadDeadLineTitles(long programId, long sessionId);
        
        IList<StudentAdmissionTarget> LoadDeadLine(long organizatonId, long? programId = null, long? SessionId = null);
        IList<dynamic> LoadProgramwiseStudentTarget(long targetId, long programId, long sessionId, DateTime deadTime, int currentSessionrank);
        IList<dynamic> LoadBranchwiseStudentTarget(long organizationId, long branchId, long? studentAdmissionProgramId);
        #endregion

        #region Others Function

        #endregion     
   
        #region Clean
        //IList<StudentAdmissionTarget> LoadOk();
        //StudentAdmissionTarget GetstudentAdmissionTarget(long programId, long sessionId, DateTime deadLine, string deadLineTitle);
        #endregion
    }

    public class StudentAdmissionTargetService : BaseService, IStudentAdmissionTargetService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion
       
        #region Propertise & Object Initialization
        private readonly IProgramDao _programDao;
        private readonly IBranchDao _branchDao;
        private readonly ISessionDao _sessionDao;
        private readonly IStudentAdmissionTargetDao _studentAdmissionTargetDao;
        private readonly IStudentBranchTargetDao _studentBranchTargetDao;
        private readonly IProgramSettingDao _programSettingDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private CommonHelper _commonHelper;
        public StudentAdmissionTargetService(ISession session)
        {
            Session = session;
            _programDao = new ProgramDao() { Session = Session };
            _sessionDao = new SessionDao() { Session = Session };
            _branchDao = new BranchDao() { Session = Session };
            _studentAdmissionTargetDao = new StudentAdmissionTargetDao() { Session = Session };
            _studentBranchTargetDao = new StudentBranchTargetDao() { Session = Session };
            _programSettingDao = new ProgramSettingDao() { Session = Session };
            _studentProgramDao = new StudentProgramDao() { Session = Session };
            _commonHelper=new CommonHelper();
        }
        #endregion

        #region Operational Function
        private void CheckDuplicateName(StudentAdmissionTarget studentAdmissionTarget)
        {
            if (_studentAdmissionTargetDao.HasDuplicateByName(studentAdmissionTarget.DeadLineTitle, studentAdmissionTarget.Program.Id, studentAdmissionTarget.Session.Id) == true)
                throw new DuplicateEntryException("Duplicate Student Admission Target found");
        }
        public void Save(StudentAdmissionTarget studentAdmissionTarget)
        {
            using (var trans = Session.BeginTransaction())
            {
                try
                {
                    CheckDuplicateName(studentAdmissionTarget);
                  //  CheckPreviousProgramSettings(studentAdmissionTarget.Program.Id, studentAdmissionTarget.Session.Id);
                    _studentAdmissionTargetDao.Save(studentAdmissionTarget);
                    trans.Commit();
                }
                catch (Exception ex)
                {
                   _logger.Error(ex);
                    if (trans != null && trans.IsActive)
                        trans.Rollback();
                    throw ex;
                }
            }
        }

        public bool Delete(long programId, long sessionId, DateTime deadLine, string deadLineTitle)
        {
            ITransaction transaction = null;
            try
            {
                var studentAdmissionTarget = _studentAdmissionTargetDao.GetstudentAdmissionTarget(programId, sessionId, deadLine, deadLineTitle);
                //CheckBeforeDelete(tempSessionObj);
                if (studentAdmissionTarget.DeadTime.AddDays(1) < DateTime.Now)
                    throw new DependencyException("You can't delete already expired Deadline target!");
                using (transaction = Session.BeginTransaction())
                {
                    studentAdmissionTarget.Status = BusinessModel.Entity.Administration.Session.EntityStatus.Delete;
                    _studentAdmissionTargetDao.Update(studentAdmissionTarget);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }
        private void CheckDuplicateNameForManage(string deadLineTitleModified, long programId, long sessionId, long studentAdmissionTargetId)
        {
            if (_studentAdmissionTargetDao.HasDuplicateByName(deadLineTitleModified, programId, sessionId, studentAdmissionTargetId))
                throw new DuplicateEntryException("Duplicate Student Admission Target found");
        }
        private void CheckPreviousProgramSettings(long programId, long sessionId)
        {
            var previousProgramSettings = _programSettingDao.LoadPreviousProgramSettings(programId, sessionId);
            if (previousProgramSettings == null || previousProgramSettings.Count < 1)
                throw new DependencyException("No previous program settings found.");
        }
        public bool Update(long programId, long sessionId, DateTime deadLine, string deadLineTitle, string deadLineTitleModified, DateTime deadLineModified, long[]  brancheIds, int[] targets)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Program program = _programDao.LoadById(programId);
                    Session session = _sessionDao.LoadById(sessionId);
                    var studentAdmissionTarget = _studentAdmissionTargetDao.GetstudentAdmissionTarget(programId,sessionId,deadLine, deadLineTitle);
                    //CheckPreviousProgramSettings(programId, sessionId);
                    if (studentAdmissionTarget == null)
                        throw new DependencyException("Can't find any Target for this dealline!");
                    CheckDuplicateNameForManage(deadLineTitleModified, programId, sessionId, studentAdmissionTarget.Id);
                    if (studentAdmissionTarget.DeadTime.AddDays(1) < DateTime.Now)
                        throw new DependencyException("You can't edit already expired Deadline target!");
                    if (deadLineModified.AddHours(23).AddSeconds(59) < studentAdmissionTarget.CreationDate)
                        throw new DependencyException("You can't give deadline before when it was created!");
                    studentAdmissionTarget.Program = program;
                    studentAdmissionTarget.Session = session;
                    studentAdmissionTarget.DeadTime = deadLineModified;
                    studentAdmissionTarget.DeadLineTitle = deadLineTitleModified;
                    _studentAdmissionTargetDao.Save(studentAdmissionTarget);
                    if (brancheIds != null && brancheIds.Length > 0)
                    {
                        for (int i = 0; i < brancheIds.Length; i++)
                        {
                            if (targets[i] > 0)
                            {
                                /*This Check is close by the instruction of Liton Bhi Start*/
                                //var studentCount = _studentProgramDao.GetAdmittedStudentCountByBranchProgramSession(brancheIds[i], programId, sessionId);
                                //if (studentCount > targets[i])
                                //{
                                //    throw new DependencyException(_branchDao.LoadById(brancheIds[i]).Name + " has less Target(" + targets[i] + ") then current student count(" + studentCount + ").");
                                //}
                                /*This Check is close by the instruction of Liton Bhi End*/
                                var studentBranchTarget =
                                    studentAdmissionTarget.StudentBranchTargets.Where(x => x.Branch.Id == brancheIds[i])
                                        .ToList()
                                        .FirstOrDefault();
                                if (studentBranchTarget != null)
                                {
                                    studentBranchTarget.Target = targets[i];
                                    _studentBranchTargetDao.Update(studentBranchTarget);
                                }
                                else
                                {
                                    studentBranchTarget = new StudentBranchTarget();
                                    studentBranchTarget.StudentAdmissionTarget = _studentAdmissionTargetDao.LoadById(studentAdmissionTarget.Id);
                                    studentBranchTarget.Branch = _branchDao.LoadById(brancheIds[i]);
                                    studentBranchTarget.Target = targets[i];
                                    _studentBranchTargetDao.Save(studentBranchTarget);
                                }
                            }
                        }
                    }
                    
                    transaction.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public DateTime LoadDeadLine(long programId, long sessionId, string deadLineTitle)
        {
            try
            {
                return _studentAdmissionTargetDao.LoadDeadLine(programId, sessionId, deadLineTitle);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        
        public StudentAdmissionTarget GetById(long id)
        {
            try
            {
                return _studentAdmissionTargetDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public StudentAdmissionTarget GetByProgramAndSessionId(long programId, long sessionId, DateTime? deadLineDate = null)
        {
            try
            {
                return _studentAdmissionTargetDao.GetByProgramAndSessionId(programId, sessionId, deadLineDate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region List Loading Function
        public IList<string> LoadDeadLineTitles(long programId, long sessionId)
        {
            try
            {
                return _studentAdmissionTargetDao.LoadDeadLineTitles(programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public IList<StudentBranchTarget> LoadBranchTargets(List<UserMenu> userMenu, long programId, long sessionId,
           string deadLineTitle, bool? loadAuthorizedTargets = null)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                return _studentBranchTargetDao.LoadBranchTargets(programIdList, branchIdList, programId, sessionId, deadLineTitle, loadAuthorizedTargets);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

       

        public IList<StudentAdmissionTarget> LoadDeadLine(long organizatonId, long? programId = null, long? sessionId = null)
        {
            return _studentAdmissionTargetDao.LoadDeadLine(organizatonId, programId, sessionId);
        }
        public IList<dynamic> LoadProgramwiseStudentTarget(long targetId, long programId, long sessionId, DateTime deadTime, int currentSessionrank)
        {
            try
            {
                return _studentAdmissionTargetDao.LoadProgramwiseStudentTarget(targetId, programId, sessionId, deadTime, currentSessionrank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<dynamic> LoadBranchwiseStudentTarget(long organizationId, long branchId, long? studentAdmissionProgramId)
        {
            try
            {
                return _studentAdmissionTargetDao.LoadBranchwiseStudentTarget(organizationId, branchId, studentAdmissionProgramId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function
       
        #endregion        

        #region Helper Function

        #endregion

        #region Clean
        //public IList<StudentAdmissionTarget> LoadOk()
        //{
        //    try
        //    {
        //        return _studentAdmissionTargetDao.LoadOk();
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw ex;
        //    }
        //}
        //public StudentAdmissionTarget GetstudentAdmissionTarget(long programId, long sessionId, DateTime deadLine,
        //   string deadLineTitle)
        //{
        //    try
        //    {
        //        return _studentAdmissionTargetDao.GetstudentAdmissionTarget(programId, sessionId, deadLine,
        //             deadLineTitle);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw ex;
        //    }
        //}
        #endregion
    }
}
