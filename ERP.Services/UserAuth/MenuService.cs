﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.UserAuth
{
    public interface IMenuService : IBaseService
    {
        #region Operational Function
        void Save(MenuGroup menuGroup);
        bool Update(MenuGroup menuGroup);
        bool UpdateRank(MenuGroup menuGroupOldObj, MenuGroup menuGroupUpdateObj);
        bool Delete(MenuGroup menuGroup);
        void Save(Menu menu);
        bool UpdateRank(Menu menuOldObj, Menu menuUpdateObj);
        bool Update(Menu menu);
        bool Delete(Menu menu); 
        #endregion

        #region Single Instances Loading Function
        MenuGroup LoadById(long id);
        MenuGroup LoadByRank(int rank);
        Menu MenuLoadById(long id);
        Menu MenuLoadByRank(int rank, long parentId);
        Menu MenuLoadByIdEgarly(long id);
        Menu LoadByRankNextOrPrevious(int rank, string action);
        #endregion

        #region List Loading Function
        IList<MenuGroup> LoadAllMenuGroup();
        List<MenuGroup> GetMenuGroupList(int start, int length, string orderBy, string direction, string name, string status, string rank);
        IList<Menu> LoadActive(bool eagerLoadMenuAccess = false);
        IList<Menu> LoadAllMenu();
        IList<Menu> LoadByMenuGroupId(int menuGroupId);   
        List<Menu> LoadUserMenuByUser(UserProfile userProfile);
        List<Menu> GetMenuList(int start, int length, string orderBy, string direction, string menuGroupId, string parentId, string name, string controllerName, string actionName, string status, string rank);
        List<Menu> LoadMenuForUserPermission(long userProfileId, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null);
        List<Menu> LoadMenuForUserObservePermission(long userProfileId);
        #endregion

        #region Others Function
        int GetMaximumRank(MenuGroup m);
        int GetMenuMaximumRank(long parentId);
        int MenuGroupRowCount(string name, string status, string rank);
        int MenuRowCount(string menuGroupId, string parentId, string name, string controllerName, string actionName, string status, string rank);
        bool DuplicationCheckMenuGroup(string name, long? id);
        bool DuplicationCheck(string name, long? menuGroupId, long? parentId, long? id); 
        #endregion

    }
    public class MenuService : BaseService, IMenuService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IMenuDao _menuDao;
        private readonly IMenuGroupDao _menuGroupDao;
        public MenuService(ISession _session)
        {
            Session = _session;
            _menuDao = new MenuDao() { Session = _session };
            _menuGroupDao = new MenuGroupDao() { Session = Session };
        }
        #endregion

        #region Operational Function
        
        public bool DuplicationCheckMenuGroup(string name, long? Id)
        {
            try
            {
                return _menuGroupDao.DuplicationCheck(name, Id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public void Save(MenuGroup menuGroup)
        {
            try
            {
                CheckBeforeSaveUpdate(menuGroup);
                int maxRank = _menuGroupDao.GetMaximumRank(menuGroup);
                menuGroup.Rank = maxRank + 1;
                _menuGroupDao.Save(menuGroup);
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool DuplicationCheck(string name, long? menuGroupId, long? parentId, long? Id)
        {
            try
            {
                return _menuDao.DuplicationCheck(name, menuGroupId, parentId, Id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }

        public void Save(Menu menu)
        {
            try
            {
                if (menu == null)
                {
                    throw new NullObjectException("menu can not be null");
                }
                long parentId = 0;
                if (menu.MenuSelfMenu != null)
                    parentId = menu.MenuSelfMenu.Id;

                int maxRank = _menuDao.GetMenuMaximumRank(parentId);
                menu.Rank = maxRank + 1;

                _menuDao.Save(menu);
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        
        public bool UpdateRank(MenuGroup menuGroupOldObj, MenuGroup menuGroupUpdateObj)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        _menuGroupDao.Update(menuGroupOldObj);
                        _menuGroupDao.Update(menuGroupUpdateObj);
                        trans.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        trans.Rollback();
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

        }
        
        public bool UpdateRank(Menu menuOldObj, Menu menuUpdateObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    _menuDao.Update(menuOldObj);
                    _menuDao.Update(menuUpdateObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                return false;
            }
        }
        
        public bool Update(MenuGroup menuGroup)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    CheckBeforeSaveUpdate(menuGroup);
                    _menuGroupDao.Update(menuGroup);
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }
        public bool Update(Menu menu)
        {
            ITransaction transaction = null;
            try
            {
                if (menu == null)
                {
                    throw new NullObjectException("menu can not be null");
                }
                using (transaction = Session.BeginTransaction())
                {
                    _menuDao.Update(menu);
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }
        public bool Delete(MenuGroup menuGroup)
        {
            ITransaction transaction = null;
            try
            {
                if (menuGroup.Menus.Count() > 0)
                {
                    throw new DependencyException("This menu group have one or more child menu.");
                }
                else
                {
                    using (transaction = Session.BeginTransaction())
                    {
                        menuGroup.Status = MenuGroup.EntityStatus.Delete;
                        _menuGroupDao.Update(menuGroup);
                        transaction.Commit();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ;
            }
        }
        public bool Delete(Menu menu)
        {
            ITransaction transaction = null;
            try
            {
                if (menu.Menus.Count() > 0)
                {
                    throw new DependencyException("This menu have one or more child menu.");
                }
                else
                {
                    using (transaction = Session.BeginTransaction())
                    {
                        menu.Status = Menu.EntityStatus.Delete;
                        _menuDao.Update(menu);
                        transaction.Commit();
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }
        public void DeleteMenuPermanently(List<long> menuIdList)
        {
             _menuDao.DeleteMenuPermanently(menuIdList);
        }
        #endregion

        #region Single Instances Loading Function
        public MenuGroup LoadById(long id)
        {
            try
            {
                return _menuGroupDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public Menu MenuLoadById(long id)
        {
            try
            {
                return _menuDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }

        public MenuGroup LoadByRank(int rank)
        {
            try
            {
                return _menuGroupDao.LoadByRank(rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public Menu MenuLoadByRank(int rank, long parentId)
        {
            try
            {
                return _menuDao.LoadByRank(rank, parentId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Menu LoadByRankNextOrPrevious(int rank, string action)
        {
            try
            {
                return _menuDao.LoadByRankDirection(rank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public Menu MenuLoadByIdEgarly(long id)
        {
            try
            {
                return _menuDao.LoadByIdEgarly(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }

        #endregion

        #region List Loading Function
        public IList<MenuGroup> LoadAllMenuGroup()
        {
            try
            {
                var menuGroupList = _menuGroupDao.LoadAll();
                return menuGroupList.OrderBy(menugroup => menugroup.Rank).Cast<MenuGroup>().ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }

        public IList<Menu> LoadActive(bool eagerLoadMenuAccess = false)
        {
            return _menuDao.LoadActive(eagerLoadMenuAccess);
        }

        public IList<Menu> LoadAllMenu()
        {
            try
            {
                var menuList = _menuDao.LoadAll();
                return menuList.OrderBy(menu => menu.Rank).Cast<Menu>().ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public IList<Menu> LoadByMenuGroupId(int menuGroupId)
        {
            try
            {
                var menuList = _menuDao.LoadByMenuGroupId(menuGroupId);
                return menuList.OrderBy(menu => menu.Rank).Cast<Menu>().ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }  
        public List<Menu> LoadUserMenuByUser(UserProfile userProfile)
        {
            try
            {
                return _menuDao.LoadUserMenuByUser(userProfile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public List<MenuGroup> GetMenuGroupList(int start, int length, string orderBy, string direction, string name, string status, string rank)
        {
            try
            {
                return _menuGroupDao.GetMenuGroupList(start, length, orderBy, direction, name, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public List<Menu> GetMenuList(int start, int length, string orderBy, string direction, string menuGroupId, string parentId, string name
            , string controllerName, string actionName, string status, string rank)
        {
            try
            {
                controllerName = controllerName.Trim();
                actionName = actionName.Trim();
                return _menuDao.GetMenuList(start, length, orderBy, direction, menuGroupId, parentId, name, controllerName, actionName, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<Menu> LoadMenuForUserPermission(long userProfileId, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null)
        {
            try
            {
                List<Menu> menuList = _menuDao.LoadMenuForUserPermission(userProfileId, (organizationIdList != null && (!organizationIdList.Contains(-1)) ? organizationIdList : null), (programIdList != null && (!programIdList.Contains(-1)) ? programIdList : null), (branchIdList != null && (!branchIdList.Contains(-1)) ? branchIdList : null));

                return menuList;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<Menu> LoadMenuForUserObservePermission(long userProfileId)
        {
            try
            {
                List<Menu> menuList = _menuDao.LoadMenuForUserObservePermission(userProfileId);

                return menuList;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        private static void CheckBeforeSaveUpdate(MenuGroup menuGroup)
        {
            if (menuGroup == null)
            {
                throw new NullObjectException("menu group can not be null");
            }
            if (string.IsNullOrEmpty(menuGroup.Name.Trim()))
            {
                throw new EmptyFieldException("menu group name can not be null");
            }
            if (string.IsNullOrEmpty(menuGroup.DisplayText.Trim()))
            {
                throw new EmptyFieldException("menu group display text can not be null");
            }
        }

        public int GetMaximumRank(MenuGroup m)
        {
            try
            {
                return _menuGroupDao.GetMaximumRank(m);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public int GetMenuMaximumRank(long parentId)
        {
            try
            {
                return _menuDao.GetMenuMaximumRank(parentId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public int MenuGroupRowCount(string Name, string Status, string Rank)
        {
            try
            {
                return _menuGroupDao.MenuGroupRowCount(Name, Status, Rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public int MenuRowCount(string menuGroupId, string parentId, string name, string controllerName, string actionName, string status, string rank)
        {
            try
            {
                controllerName = controllerName.Trim();
                actionName = actionName.Trim();
                return _menuDao.MenuRowCount(menuGroupId, parentId, name, controllerName, actionName, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion
      
    }
}
