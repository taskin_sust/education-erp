using System;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Hr;
namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class MemberLoanCsrFactory : ObjectFactoryBase<MemberLoanCsr>
    {

        private readonly IMemberLoanCsrService _prMemberLoanCsrService;
        private readonly MemberLoanApplicationFactory _memberLoanApplicationFactory;

        public MemberLoanCsrFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _prMemberLoanCsrService = new MemberLoanCsrService(session);
            _memberLoanApplicationFactory = new MemberLoanApplicationFactory(null, session);
        }

        #region Create

        public MemberLoanCsrFactory Create()
        {
            Object = new MemberLoanCsr()
            {
                Amount = 5000,
                ApprovedDate = DateTime.Now,
                PaymentType = (int)PaymentType.Monthly,
                TotalAmount = 2000,
                EffectiveDate = DateTime.Now,
                ClosingDate = DateTime.Now.AddMonths(5)
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanCsrFactory WithMemberLoanApplication(MemberLoanApplication memberLoanApplication)
        {
            Object.MemberLoanApplication = memberLoanApplication;
            return this;
        }

        public MemberLoanCsrFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public MemberLoanCsrFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _prMemberLoanCsrService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_prMemberLoanCsrService.SaveOrUpdate(x); });

            }
            return this;
        }

        public MemberLoanCsrFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new MemberLoanCsr() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public MemberLoanCsrFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion



    }
}
