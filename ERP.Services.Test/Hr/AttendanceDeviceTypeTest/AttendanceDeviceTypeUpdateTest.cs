﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceDeviceTypeTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceDeviceType")]
    public class AttendanceDeviceTypeUpdateTest : AttendanceDeviceTypeBaseTest
    {
        #region Basic

        [Fact]
        public void Should_Update_Successfully()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].Code = "single code";
            _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]);
            var lObj = _attendanceDeviceTypeService.GetAttendanceDeviceType(AttendanceDeviceTypeFactory.SingleObjectList[0].Id);
            Assert.Equal(lObj.Code, "single code");
        }

        #endregion

        #region Validation Check Test

        [Fact]
        public void Should_Return_EmptyName_Exception()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].Code = "";
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]));
        }

        [Fact]
        public void Should_Return_EmptyFirmwareVersion_Exception()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].FirmwareVersion = -1;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]));
        }

        [Fact]
        public void Should_Return_EmptyAlgorithmVersion_Exception()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].AlgorithmVersion = -1;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]));
        }



        #endregion

        #region Invalid Input Check Test

        [Fact]
        public void Should_Return_Invalid_Attendance_LogCapacity_LessThanLowerRange()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].AttendanceLogCapacity = -1;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]));
        }

        [Fact]
        public void Should_Return_Invalid_AttendanceLogCapacity_GreaterThanRange()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].AttendanceLogCapacity = 100001;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]));
        }

        [Fact]
        public void Should_Return_Invalid_User_Capacity_LessThanLowerRange()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].UserCapacity = -1;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]));
        }

        [Fact]
        public void Should_Return_Invalid_User_Capacity_GreaterThanRange()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].UserCapacity = 100001;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]));
        }

        [Fact]
        public void Should_Return_Invalid_Template_Capacity_LessThanLowerRange()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].UserCapacity = -1;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]));
        }

        [Fact]
        public void Should_Return_Invalid_Template_Capacity_GreaterThanRange()
        {
            AttendanceDeviceTypeFactory.Create().Persist();
            AttendanceDeviceTypeFactory.SingleObjectList[0].UserCapacity = 100001;
            Assert.Throws<InvalidDataException>(() => _attendanceDeviceTypeService.SaveOrUpdate(AttendanceDeviceTypeFactory.SingleObjectList[0]));
        }

        #endregion
    }
}
