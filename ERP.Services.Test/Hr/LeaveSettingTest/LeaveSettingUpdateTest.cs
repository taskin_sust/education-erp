﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.LeaveSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "LeaveSettings")]
    public class LeaveSettingUpdateTest : LeaveSettingBaseTest
    {

        public static IEnumerable<object[]> LeaveSettingsIndex
        {
            get
            {
                var indexList = new List<object[]>();
                for (int i = 1; i < 7; i++)
                    indexList.Add(new object[] { i });
                return indexList;
            }
        }

        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            Leave leaveSavedObj = leaveFactory.Create().WithOrganization().Persist().Object;

            var leave = _leaveService.GetById(leaveSavedObj.Id);
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;

            Assert.Throws<NullObjectException>(
                () => _leaveService.Update(0, leave.Organization.Id, leave.Name, isPub.ToString(),
                    leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                    leave.StartFrom.ToString(),
                    leave.EffectiveDate.ToString(), new string[] { "1", "2" }, new string[] { "1", "2" },
                    new string[] { "1", "2" }, isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                    isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                    leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(), ""));
        }

        [Theory]
        [InlineData(new string[] { "1", "2" }, new string[] { "1", "2" }, new string[] { })]
        [InlineData(new string[] { "1", "2" }, new string[] { }, new string[] { "1", "2" })]
        [InlineData(new string[] { }, new string[] { "1", "2" }, new string[] { "1", "2" })]
        public void Update_Should_Check_Property(string[] genderArray, string[] empStatus, string[] maritalStatus)
        {
            Leave leaveSavedObj = leaveFactory.Create().WithOrganization().Persist().Object;

            var leave = _leaveService.GetById(leaveSavedObj.Id);
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;
            long orgId = leave.Organization.Id;
            string name = "Changed Name";
            string effectiveDate = DateTime.Now.Subtract(new TimeSpan(5, 5, 5, 5)).ToString();

            Assert.Throws<InvalidDataException>(
                () => _leaveService.Update(leave.Id, orgId, name, isPub.ToString(),
                    leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                    leave.StartFrom.ToString(), effectiveDate, genderArray, empStatus, maritalStatus, isCarryforward.ToString(),
                    leave.MaxCarryDays.ToString(), isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                    leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(), ""));

        }

        [Theory]
        [MemberData("LeaveSettingsIndex", MemberType = typeof(LeaveSettingUpdateTest))]
        public void Update_Should_Check_Property2(int counter)
        {
            Leave leaveSavedObj = leaveFactory.Create().WithOrganization().Persist().Object;

            var leave = _leaveService.GetById(leaveSavedObj.Id);
            long id = (leave.Id + 1);
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int? isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int? isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int? isEncash = (bool)leave.IsEncash ? 1 : 0;
            long orgId = leave.Organization.Id;
            string name = "Changed Name";
            var repeatType = leave.RepeatType.ToString();
            var startFrom = leave.StartFrom.ToString();
            var appType = leave.Applicationtype.ToString();
            string effectiveDate = DateTime.Now.Subtract(new TimeSpan(5, 5, 5, 5)).ToString();
            string[] genderArray = new string[] { "1", "2" };
            string[] empStatus = new string[] { "1", "2" };
            string[] maritalStatus = new string[] { "1", "2" };

            if (counter == 1) { id = id + 1; }

            if (counter == 2) { orgId = orgId + 1; }

            if (counter == 3) { name = ""; }

            if (counter == 4) { repeatType = ""; }

            if (counter == 5) { repeatType = "4"; }

            if (counter == 6) { startFrom = ""; }

            if (counter == 7) { startFrom = "4"; }

            if (counter == 8) { effectiveDate = ""; }

            if (counter == 9) { isCarryforward = null; }

            if (counter == 10) { isTakingLimit = null; }

            if (counter == 10) { appType = ""; }

            if (counter == 11) { isEncash = null; }

            Assert.Throws<InvalidDataException>(
                () => _leaveService.Update(id, orgId, name, isPub.ToString(),
                    leave.PayType.ToString(), repeatType, leave.NoOfDays.ToString(),
                    startFrom, effectiveDate, genderArray, empStatus, maritalStatus, isCarryforward.ToString(),
                    leave.MaxCarryDays.ToString(), isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), appType,
                    leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(), ""));

        }

        [Fact]
        public void Should_Update_Successfully()
        {
            Leave leaveSavedObj = leaveFactory.CreateMore(1).Persist().ObjectList.FirstOrDefault();
            var leave = _leaveService.GetById(leaveSavedObj.Id);
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;

            Assert.True(_leaveService.Update(leave.Id, leave.Organization.Id, "Changed Name", isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                DateTime.Now.Subtract(new TimeSpan(5, 5, 5, 5)).ToString(), new string[] { "1", "2" }, new string[] { "1", "2" },
                new string[] { "1", "2" }, isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(), ""));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Update_Should_Check_IsPublic()
        {
            Leave leaveSavedObj = leaveFactory.Create().WithEffectiveDate(DateTime.Now.AddDays(5)).WithOrganization().Persist().Object;

            var leave = _leaveService.GetById(leaveSavedObj.Id);
            long id = leave.Id;
            int isPub = 0;//(bool)leave.IsPublic ? 1 : 0;
            int? isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int? isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int? isEncash = (bool)leave.IsEncash ? 1 : 0;
            long orgId = leave.Organization.Id;
            string name = "Changed Name";
            var repeatType = leave.RepeatType.ToString();
            var startFrom = leave.StartFrom.ToString();
            var appType = leave.Applicationtype.ToString();
            string effectiveDate = DateTime.Now.AddDays(5).ToString();
            string[] genderArray = new string[] { "1", "2" };
            string[] empStatus = new string[] { "1", "2" };
            string[] maritalStatus = new string[] { "1", "2" };
            
            Assert.True(_leaveService.Update(id, orgId, name, isPub.ToString(),
                    leave.PayType.ToString(), repeatType, leave.NoOfDays.ToString(),
                    startFrom, effectiveDate, genderArray, empStatus, maritalStatus, isCarryforward.ToString(),
                    leave.MaxCarryDays.ToString(), isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), appType,
                    leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(), ""));
        }

        [Fact]
        public void Update_Should_Check_PayType()
        {
            Leave leaveSavedObj = leaveFactory.Create().WithEffectiveDate(DateTime.Now.AddDays(5)).WithOrganization().Persist().Object;

            var leave = _leaveService.GetById(leaveSavedObj.Id);
            long id = leave.Id;
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int? isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int? isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int? isEncash = (bool)leave.IsEncash ? 1 : 0;
            string payType = "0";
            long orgId = leave.Organization.Id;
            string name = "Changed Name";
            var repeatType = leave.RepeatType.ToString();
            var startFrom = leave.StartFrom.ToString();
            var appType = leave.Applicationtype.ToString();
            string effectiveDate = DateTime.Now.AddDays(5).ToString();
            string[] genderArray = new string[] { "1", "2" };
            string[] empStatus = new string[] { "1", "2" };
            string[] maritalStatus = new string[] { "1", "2" };
            
            Assert.True(_leaveService.Update(id, orgId, name, isPub.ToString(),payType, repeatType, leave.NoOfDays.ToString(),
                    startFrom, effectiveDate, genderArray, empStatus, maritalStatus, isCarryforward.ToString(),
                    leave.MaxCarryDays.ToString(), isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), appType,
                    leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(), ""));

        }
        
        [Fact]
        public void Update_Should_Check_Dependency()
        {
            //GenerateLeave();
            //var leave = _leaveService.GetById(idList[0]);
            Leave leaveSavedObj = leaveFactory.CreateMore(1).Persist().ObjectList.FirstOrDefault();

            var leave = _leaveService.GetById(leaveSavedObj.Id);
            int isPub = (bool)leave.IsPublic ? 1 : 0;
            int isCarryforward = (bool)leave.IsCarryforward ? 1 : 0;
            int isTakingLimit = (bool)leave.IsTakingLimit ? 1 : 0;
            int isEncash = (bool)leave.IsEncash ? 1 : 0;

            Assert.True(_leaveService.Update(leave.Id, leave.Organization.Id, leave.Name, isPub.ToString(),
                leave.PayType.ToString(), leave.RepeatType.ToString(), leave.NoOfDays.ToString(),
                leave.StartFrom.ToString(),
                leave.EffectiveDate.ToString(), new string[] { "1", "2" }, new string[] { "1", "2" },
                new string[] { "1", "2" }, isCarryforward.ToString(), leave.MaxCarryDays.ToString(),
                isTakingLimit.ToString(), leave.MaxTakingLimit.ToString(), leave.Applicationtype.ToString(),
                leave.ApplicationDays.ToString(), isEncash.ToString(), leave.MinEncashReserveDays.ToString(), ""));
        }

        #endregion
        
    }
}
