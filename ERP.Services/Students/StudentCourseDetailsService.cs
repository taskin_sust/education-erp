﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IStudentCourseDetailsService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        StudentCourseDetail LoadStudentCourseDetailByCourseSubjectAndStudentProgram(CourseSubject courseSubject, StudentProgram studentProgram);
        #endregion

        #region List Loading Function
        IList<StudentCourseDetail> LoadStudentCourseDetailsListByStudentProgramId(long stdProId);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        StudentCourseDetail GetByCourseSubject(long courseSubId);
    }
    public class StudentCourseDetailsService : BaseService, IStudentCourseDetailsService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentCourseDetailDao _studentCourseDetailDao;
        public StudentCourseDetailsService(ISession session)
        {
            Session = session;
            _studentCourseDetailDao = new StudentCourseDetailDao() { Session = Session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public StudentCourseDetail LoadStudentCourseDetailByCourseSubjectAndStudentProgram(CourseSubject courseSubject, StudentProgram studentProgram)
        {
            try
            {
                return _studentCourseDetailDao.LoadStudentCourseDetailByCourseSubjectAndStudentProgram(courseSubject, studentProgram);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        #endregion

        #region List Loading Function
        public IList<StudentCourseDetail> LoadStudentCourseDetailsListByStudentProgramId(long stdProId)
        {
            try
            {
                return _studentCourseDetailDao.LoadStudentCourseDetailsListByStudentProgramId(stdProId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public StudentCourseDetail GetByCourseSubject(long courseSubId)
        {
            return _studentCourseDetailDao.GetByCourseSubject(courseSubId);
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
