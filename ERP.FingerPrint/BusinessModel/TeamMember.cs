﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.FingerPrint.BusinessModel
{
    public class TeamMember
    {
        public string Name { get; set; }
        public string Pin { get; set; }
        public string Organization { get; set; }
        public string Branch { get; set; }
        public string Campus { get; set; }
        public string Department { get; set; }
        public byte[] Image { get; set; }
        public string ZeroIndex { get; set; }
        public string OneIndex { get; set; }
        public string TwoIndex { get; set; }
        public string ThreeIndex { get; set; }
        public string FourIndex { get; set; }
        public string FiveIndex { get; set; }
        public string SixIndex { get; set; }
        public string SevenIndex { get; set; }
        public string EightIndex { get; set; }     
        public string NineIndex { get; set; }
    }
}
