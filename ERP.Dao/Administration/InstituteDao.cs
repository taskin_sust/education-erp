﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IInstituteDao : IBaseDao<Institute, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        Institute LoadByRank(int rank);
        Institute LoadByInstituteName(string institute);
        Institute GetInstitute(string eiin);
        Institute GetInstituteByName(string institute);
        #endregion

        #region List Loading Function
        IList<Institute> GetOnlyPageSizedInstituteList(int page, int pageSize, Institute i);
        IList<Institute> LoadInstituteByQuery(string query, bool isUniversity);
        IList<Institute> LoadInstituteByQuery(string name);
        List<Institute> GetInstituteList(int start, int length, string orderBy, string direction, string name, string category, string district, string eiin, string status, string rank);
        IList<Institute> LoadInstituteInstituteByEiin(string eiin, long id);
        IList<Institute> LoadInstituteByName(long id, string name, string shortName, long categoryId, string district, string eiin);
        List<Institute> GetInstituteList(int start, int length, string orderBy, string direction, bool isUniversity);
        IList<Institute> GetInstituteByNameLike(string name);

        #endregion

        #region Others Function
        bool HasDuplicateByName(string name, long id);
        int InstituteRowCount(string name, string category, string district, string eiin, string status, string rank);

        #endregion
    }
    public class InstituteDao : BaseDao<Institute, long>, IInstituteDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public Institute LoadByRank(int rank)
        {
            return Session.QueryOver<Institute>().Where(x => x.Rank == rank).SingleOrDefault<Institute>();
        }
        public Institute LoadByInstituteName(string institute)
        {
            ICriteria criteria = Session.CreateCriteria<Institute>();
            if (!String.IsNullOrEmpty(institute))
            {
                criteria.Add(Restrictions.Like("Name", institute, MatchMode.Anywhere));
            }
            return criteria.UniqueResult<Institute>();
        }

        public Institute GetInstitute(string eiin)
        {
            ICriteria criteria = Session.CreateCriteria<Institute>().Add(Restrictions.Eq("Status", Institute.EntityStatus.Active));
            if (!String.IsNullOrEmpty(eiin))
            {
                criteria.Add(Restrictions.Eq("Eiin", eiin));
            }
            var listInstitute = criteria.List<Institute>();
            return listInstitute.Take(1).FirstOrDefault();
            //return criteria.UniqueResult<Institute>();
        }
        public Institute GetInstituteByName(string institute)
        {
            ICriteria criteria = Session.CreateCriteria<Institute>().Add(Restrictions.Eq("Status", Institute.EntityStatus.Active));
            if (!String.IsNullOrEmpty(institute))
            {
                criteria.Add(Restrictions.Eq("Name", institute));
            }
            return criteria.UniqueResult<Institute>();
        }
        #endregion

        #region List Loading Function
        public List<Institute> GetInstituteList(int start, int length, string orderBy, string direction, string name, string category, string district, string eiin, string status, string rank)
        {
            ICriteria criteria = Session.CreateCriteria<Institute>().Add(Restrictions.Not(Restrictions.Eq("Status", Institute.EntityStatus.Delete)));
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(district))
            {
                criteria.Add(Restrictions.Like("District", district, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(eiin))
            {
                criteria.Add(Restrictions.Like("Eiin", eiin, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }
            if (!string.IsNullOrEmpty(orderBy))
            {
                if (orderBy != "Category")
                {
                    if (direction == "ASC")
                    {
                        criteria.AddOrder(Order.Asc(orderBy));
                    }
                    else
                    {
                        criteria.AddOrder(Order.Desc(orderBy));
                    }
                } 
            }
            return (List<Institute>)criteria
                //.SetFirstResult(start)
                //.SetMaxResults(length)
                .List<Institute>();
        }
        public IList<Institute> LoadInstituteInstituteByEiin(string eiin, long id)
        {
            return
                //from Institute r where r.Name=:name and r.Status!=:status and r.Id!=:id
                    Session.CreateCriteria<Institute>()
                        .Add(Restrictions.Eq("Eiin", eiin))
                        .Add(Restrictions.Not(Restrictions.Eq("Status", Institute.EntityStatus.Delete)))
                        .Add(Restrictions.Not(Restrictions.Eq("Id", id)))
                        .List<Institute>();
        }
        public IList<Institute> LoadInstituteByName(long id, string name, string shortName, long categoryId, string district, string eiin)
        {
            if (id<=0)
            {
                return
                Session.QueryOver<Institute>()
                    .Where(
                        x =>
                            x.Name == name && x.ShortName == shortName && x.InstituteCategory.Id == categoryId &&
                            x.District == district &&
                            x.Eiin == eiin && x.Status != Institute.EntityStatus.Delete).List<Institute>();
            }
            return
                Session.QueryOver<Institute>()
                    .Where(
                        x =>
                            x.Name == name && x.ShortName == shortName && x.InstituteCategory.Id == categoryId &&
                            x.District == district &&
                            x.Eiin == eiin && x.Status != Institute.EntityStatus.Delete && x.Id != id).List<Institute>();
        }
        public List<Institute> GetInstituteList(int start, int length, string orderBy, string direction, bool isUniversity)
        {
            ICriteria criteria = Session.CreateCriteria<Institute>().Add(Restrictions.Not(Restrictions.Eq("Status", Institute.EntityStatus.Delete))).CreateAlias("InstituteCategory", "ic")
                        .Add(Restrictions.Eq("ic.IsUniversity", isUniversity));
            if (direction == "ASC")
            {
                criteria.AddOrder(Order.Asc(orderBy));
            }
            else
            {
                criteria.AddOrder(Order.Desc(orderBy));
            }
            return (List<Institute>)criteria.SetFirstResult(start).SetMaxResults(length).List<Institute>();
        }
        public IList<Institute> LoadInstituteByQuery(string query, bool isUniversity)
        {
            ICriteria criteria = Session.CreateCriteria<Institute>().Add(Restrictions.Eq("Status", Institute.EntityStatus.Active));
            if (!String.IsNullOrEmpty(query))
            {
                criteria.Add(Restrictions.Like("Name", query, MatchMode.Anywhere));
            }
            criteria.CreateAlias("InstituteCategory", "ic").Add(Restrictions.Eq("ic.IsUniversity", isUniversity));
            criteria.SetFirstResult(0).SetMaxResults(10);
            return criteria.List<Institute>();
        }
        public IList<Institute> LoadInstituteByQuery(string query)
        {
            ICriteria criteria =
                   Session.CreateCriteria<Institute>()
                       .Add(Restrictions.Eq("Status", Institute.EntityStatus.Active));
            if (!String.IsNullOrEmpty(query))
            {
                criteria.Add(Restrictions.Like("Name", query, MatchMode.Anywhere));
            }
            criteria.SetFirstResult(0).SetMaxResults(10);
            return criteria.List<Institute>();
        }
        public IList<Institute> GetOnlyPageSizedInstituteList(int page, int pageSize, Institute i)
        {
            string strvalue = "";
            int intValue;
            ICriteria criteria =
                Session.CreateCriteria<Institute>()
                    .Add(Restrictions.Not(Restrictions.Eq("Status", Institute.EntityStatus.Delete)));
            foreach (var data in i.GetType().GetProperties())
            {
                object proval = data.GetValue(i);
                if (data.GetValue(i) != null && data.GetValue(i) != "" && proval.ToString() != "0")
                {
                    var type = data.PropertyType.Name;
                    if (type == "String")
                    {
                        strvalue = (string)data.GetValue(i);
                        criteria.Add(Restrictions.Like(data.Name, strvalue, MatchMode.Anywhere));
                    }
                    if (type == "Int32")
                    {
                        intValue = Convert.ToInt32(data.GetValue(i));
                        criteria.Add(Restrictions.Eq(data.Name, intValue));
                    }
                }
            }
            return criteria.AddOrder(Order.Asc("Rank")).SetFirstResult((page - 1) * pageSize).SetMaxResults(pageSize).List<Institute>();

        }

        public IList<Institute> GetInstituteByNameLike(string name)
        {
            var list = Session.QueryOver<Institute>().Where(x => x.Status == Institute.EntityStatus.Active)
                .WhereRestrictionOn(x=>x.Name).IsLike("%" + name + "%")
                .Take(12).List<Institute>();
            return list;
        } 
        #endregion

        #region Others Function
        public bool HasDuplicateByName(string name, long id)
        {
            IQuery query = Session.CreateQuery("from Institute r where r.Name=:name and r.Status!=:status and r.Id!=:id");
            query.SetString("name", name);
            query.SetInt32("status", Institute.EntityStatus.Delete);
            query.SetInt64("id", id);
            IList<Institute> catList = query.List<Institute>();
            if (catList == null || catList.Count < 1)
                return false;
            else
                return true;
        }
        public int InstituteRowCount(string name, string category, string district, string eiin, string status, string rank)
        {

            ICriteria criteria =
               Session.CreateCriteria<Institute>();
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(category))
            {
                criteria.Add(Restrictions.Like("Category", category, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(district))
            {
                criteria.Add(Restrictions.Like("District", district, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(category))
            {
                criteria.Add(Restrictions.Like("Eiin", eiin, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }
            IList list = criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Institute.EntityStatus.Delete))).List();
            return list.Count;
        }
        #endregion

        
    }
}