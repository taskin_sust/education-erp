using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface IMemberLoanApplicationDao : IBaseDao<MemberLoanApplication, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        IList<MemberLoanApplicationHistoryDto> LoadMemberLoanApplication(int start, int length, System.Collections.Generic.List<long> organizationIdList, System.Collections.Generic.List<long> branchIdList, long? departmentId, int? pin, int? status);
        IList<MemberLoanApplication> LoadByMemberId(long memberId);
        IList<MemberLoanDetailsDto> GetLoadDetails(int pin);
        #endregion

        #region Others Function

        int GetMemberLoanApplicationCount(List<long> organizationIdList, List<long> branchIdList, long? departmentId, int? pin, int? status);

        #endregion





    }

    public class MemberLoanApplicationDao : BaseDao<MemberLoanApplication, long>, IMemberLoanApplicationDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<MemberLoanApplicationHistoryDto> LoadMemberLoanApplication(int start, int length, List<long> organizationIdList, List<long> branchIdList, long? departmentId,
            int? pin, int? status)
        {
            string queryStr = GetMemberLoanApplicationSqlQuery(organizationIdList, branchIdList, departmentId, pin, status);
            queryStr += " order by  finalSelection.Pin OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            IQuery iQuery = Session.CreateSQLQuery(queryStr);
            iQuery.SetResultTransformer(Transformers.AliasToBean<MemberLoanApplicationHistoryDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<MemberLoanApplicationHistoryDto>();
            return list;
        }
        public IList<MemberLoanApplication> LoadByMemberId(long memberId)
        {
            return Session.QueryOver<MemberLoanApplication>().Where(x => x.TeamMember.Id == memberId).List<MemberLoanApplication>();
        }

        public IList<MemberLoanDetailsDto> GetLoadDetails(int pin)
        {
            return new List<MemberLoanDetailsDto>();
        }
        #endregion

        #region Others Function

        public int GetMemberLoanApplicationCount(List<long> organizationIdList, List<long> branchIdList, long? departmentId, int? pin, int? status)
        {
            string queryStr = "select Count(countNum.Pin) from ( ";
            queryStr += GetMemberLoanApplicationSqlQuery(organizationIdList, branchIdList, departmentId, pin, status);
            queryStr += " ) as countNum ";
            IQuery iQuery = Session.CreateSQLQuery(queryStr);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function

        public string GetMemberLoanApplicationSqlQuery(List<long> organizationIdList, List<long> branchIdList, long? departmentId, int? pin, int? status)
        {
            string filterQuery = "";

            if (organizationIdList != null && organizationIdList.Any() && !organizationIdList.Contains(SelectionType.SelelectAll))
            {
                filterQuery += @" AND finalSelection.orgId IN (" + string.Join(",", organizationIdList) + ") ";
            }
            if (branchIdList != null && branchIdList.Any() && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                filterQuery += @" AND finalSelection.brId IN (" + string.Join(",", branchIdList) + ") ";
            }
            if (pin != null)
            {
                filterQuery += @" AND finalSelection.Pin =" + pin;
            }
            if (departmentId != null)
            {
                filterQuery += @" AND  finalSelection.dpId =" + departmentId;
            }
            if (status != null)
            {
                filterQuery += @" AND  finalSelection.LoanStatus =" + status;
            }

            string query = @"select finalSelection.Pin, finalSelection.Name, finalSelection.Department,
                            finalSelection.Branch, finalSelection.Organization, finalSelection.RequestedAmount ,
                            finalSelection.LoanApprovedAmount,finalSelection.MonthlyRefund , finalSelection.RefundStart, finalSelection.Reason, finalSelection.LastModifiedBy, finalSelection.modDate as ModificationDate, finalSelection.mlaid as MemberLeaveApplicationId from (         
                            select fselection.mlaid ,fselection.Pin as Pin, fselection.Name as Name, sselection.deptName as Department,
                            sselection.brName as Branch, sselection.ShortName as Organization, fselection.RequestedAmount ,
                            fselection.LoanApprovedAmount, fselection.MonthlyRefund , fselection.RefundStart, fselection.Remarks as Reason, fselection.md as modDate, up.Email as LastModifiedBy, sselection.orgId, sselection.brId, sselection.dpId, fselection.LoanStatus from(
                            select mla.ModifyBy as modBy, mla.Id as mlaid, mla.LoanStatus, tm.pin, tm.Name, mla.TeamMemberId, mla.RequestedAmount, ml.LoanApprovedAmount, ml.[MonthlyRefund], ml.RefundStart, mla.Remarks, mla.ModificationDate as md from [dbo].[HR_MemberLoanApplication] mla 
                            left join [dbo].[HR_MemberLoan] ml on mla.Id = ml.MemberLoanApplicationId
                            left join [dbo].[HR_TeamMember] tm on mla.TeamMemberId = tm.Id ) as fselection 
                            left join
                            (Select o.Id orgId, br.Id brId, dp.Id dpId, o.ShortName, dp.Name as deptName, br.Name as brName, currentPosition.TeamMemberId as teammemberId from 
                                                [HR_TeamMember] AS hrm 
                                                Left Join (
	                                                Select * from (
		                                                Select [CampusId]
			                                                ,[DesignationId]
			                                                ,[DepartmentId]
			                                                ,[TeamMemberId]
			                                                ,[EffectiveDate]
			                                                ,[EmploymentStatus]
			                                                ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC, Id DESC) AS R  
			                                                from [HR_EmploymentHistory] 
			                                                Where Status = 1
                                                            and EffectiveDate <= getDate()
			                                                ) AS A 
                                                Where A.R = 1) AS currentPosition ON currentPosition.TeamMemberId  = hrm.Id and hrm.status=1 
					                            Left Join [Campus] AS c ON c.Id = currentPosition.CampusId and c.Status = 1
                                                Left Join [Branch] AS br ON br.Id = c.BranchId and br.Status =  1
                                                Left Join [Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  1
                                                Left Join [HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  1
                                                Left Join [HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  1
                                               
                    ) as sselection on fselection.TeamMemberId = sselection.teammemberId 
                        left join [dbo].[AspNetUsers] as up on up.Id = fselection.modBy) as finalSelection 
                     Where 1 = 1
                    " + filterQuery + @"";
            return query;
        }


        #endregion

    }
}
