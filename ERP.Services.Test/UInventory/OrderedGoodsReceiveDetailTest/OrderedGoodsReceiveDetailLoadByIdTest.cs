﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.OrderedGoodsReceiveDetailTest
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReceiveDetail")]
    public class OrderedGoodsReceiveDetailLoadByIdTest : OrderedGoodsReceiveDetailBaseTest
    {
        [Fact]
        public void Should_Return_Invalid_Id_For_GetPrevReceiveAmountByReceiveDetailsId()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<InvalidDataException>(() => _GoodsReceiveDetailService.GetPrevReceiveAmountByReceiveDetailsId(-1));
        }

        [Fact]
        public void Should_Return_Success_For_GetPrevReceiveAmountByReceiveDetailsId()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Equal(2, _GoodsReceiveDetailService.GetPrevReceiveAmountByReceiveDetailsId(_goodsReceiveFactory.Object.GoodsReceiveDetails[0].Id));
        }

        [Fact]
        public void Should_Return_Invalid_ReceivedNo_For_GetPrevReceivedAmountFromSelectedReceivedNo()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<InvalidDataException>(
                () => _GoodsReceiveDetailService.GetPrevReceivedAmountFromSelectedReceivedNo("1"));
        }

        [Fact]
        public void Should_Return_Success_For_GetPrevReceivedAmountFromSelectedReceivedNo()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Equal(0, _GoodsReceiveDetailService.GetPrevReceivedAmountFromSelectedReceivedNo(_goodsReceiveFactory.Object.GoodsReceivedNo));
        }

        [Fact]
        public void Should_Return_Invalid_Id_For_GetPrevSumReceiveAmountByGoodsReceiveDetailsId()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<InvalidDataException>(
                 () => _GoodsReceiveDetailService.GetPrevReceiveAmountByReceiveDetailsId(-1));
        }

        [Fact]
        public void Should_Return_Invalid_Id_For_GetLastReceivedAmountByReceiveDetailsId()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<InvalidDataException>(
                 () => _GoodsReceiveDetailService.GetPrevReceiveAmountByReceiveDetailsId(-1));
        }


    }
}
