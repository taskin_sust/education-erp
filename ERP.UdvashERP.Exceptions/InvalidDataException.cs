﻿using System;

namespace UdvashERP.MessageExceptions
{
    public class InvalidDataException: Exception
    {
        public InvalidDataException():base("Invalid Data")
        {

        }

        public InvalidDataException(string message):base(message)
        {

        }

    }
}
