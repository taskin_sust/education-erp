﻿using System.Collections.Generic;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Teachers
{
    public interface ITeacherVersionOfStudyPriorityDao : IBaseDao<TeacherVersionOfStudyPriority, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<TeacherVersionOfStudyPriority> LoadTeacher(long teacherId);
        #endregion

        #region Others Function

        #endregion
    }

    public class TeacherVersionOfStudyPriorityDao : BaseDao<TeacherVersionOfStudyPriority, long>, ITeacherVersionOfStudyPriorityDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<TeacherVersionOfStudyPriority> LoadTeacher(long teacherId)
        {
            var criteria = Session.CreateCriteria<TeacherVersionOfStudyPriority>();
            criteria.CreateCriteria("Teacher", "teacher").Add(Restrictions.Eq("teacher.Status", Teacher.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Status", TeacherVersionOfStudyPriority.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("teacher.Id", teacherId));
            return criteria.List<TeacherVersionOfStudyPriority>();
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
