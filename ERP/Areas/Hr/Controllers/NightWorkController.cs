﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate.Linq.Functions;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class NightWorkController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly INightWorkService _nightWorkService; 
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IMentorHistoryService _mentorHistoryService;
        private readonly IUserService _userService;

        public NightWorkController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _nightWorkService = new NightWorkService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _teamMemberService = new TeamMemberService(session);
            _mentorHistoryService = new MentorHistoryService(session);
            _commonHelper = new CommonHelper();
            _userService = new UserService(session);
        }

        #endregion

        #region Night Work Approval By Mentor

        public ActionResult NightWork() 
        {
            return View();
        }

        public ActionResult NightWorkApproval()
        {
            ViewBag.ShowNoData = false;
            return View();
        }

        [HttpPost]
        public ActionResult NightWork(NightWorkFormViewModel data)
        {
            ViewBag.ShowNodata = true;
            try
            {
                if (ModelState.IsValid)
                {

                    var pinList = (!String.IsNullOrEmpty(data.PinList)) ? data.PinList.Trim() : "";
                    var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");
                    var mentor = _teamMemberService.GetCurrentMember();
                    var memberPinList = pinList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList();
                    var mentorTeamMemberIdList = _mentorHistoryService.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now, null, memberPinList);
                    if (!mentorTeamMemberIdList.Any())
                    {
                        mentorTeamMemberIdList.Add(0);
                    }
                    IList<NightWorkDto> nightWorkList = _nightWorkService.LoadNightWork(data.Date, data.Date, mentorTeamMemberIdList, pinList);
                    ViewBag.NightWorkList = nightWorkList;
                }
                else
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }

            return View();
        }

        public ActionResult NightWorkSaveByMentor(string date, string pinList, string fullValues, string halfValues, string noValues, string inValues, string outValues) 
        {
            try
            {
                _nightWorkService.Save(date, pinList.Trim(), fullValues, halfValues, noValues,  inValues,  outValues);
                return Json(new Response(true, "Night Work Approval Successful"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Night Work By HR

        public ActionResult NightWorkByHr() 
        {
            return View();
        }

        public ActionResult NightWorkbyHrApproval()
        {
            ViewBag.ShowNoData = false;
            return View();
        }

        [HttpPost]
        public ActionResult NightWorkByHr(NightWorkFormViewModel data) 
        {
            ViewBag.ShowNodata = true;
            try
            {
                if (ModelState.IsValid)
                {
                    var pinList = (!String.IsNullOrEmpty(data.PinList)) ? data.PinList.Trim() : "";
                    var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var mentorTeamMemberIdList = new List<long>();
                    IList<NightWorkDto> nightWorkList = _nightWorkService.LoadNightWork(data.Date, data.Date, mentorTeamMemberIdList, pinList, userMenu);
                    ViewBag.NightWorkList = nightWorkList;
                }
                else
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }

            return View();
        }

        public ActionResult NightWorkSaveByHr(string date, string pinList, string fullValues, string halfValues, string noValues, string inValues, string outValues) 
        {
            try
            {
                _nightWorkService.Save(date, pinList, fullValues, halfValues, noValues, inValues, outValues);
                return Json(new Response(true, "Night Work Approval Successful"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        #endregion

        #region Night Work History

        public ActionResult NightWorkHistory()
        {
            try
            {
                SelectList branch = null, organization = null, campus = null;
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.organizationlist = organization;
                ViewBag.BranchId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.CampusId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.DepartmentId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.dateFromField = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
                ViewBag.dateToField = DateTime.Now.ToString("yyyy-MM-dd");
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return PartialView("_NightWorkHistory");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }

        }

        [HttpPost]
        public ActionResult LoadNightWorkHistory(int draw, int start, int length, long? organizationId, long? departmentId, long? branchId, long? campusId, string pin, string dateFrom, string dateTo)
        { 
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    var mentorTeamMemberIdList = new List<long>();
                    DateTime dateFromDateTime = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).Date;
                    DateTime dateToDateTime = DateTime.Today.Date;
                    if (!String.IsNullOrEmpty(dateFrom))
                    {
                        dateFromDateTime = Convert.ToDateTime(dateFrom).Date;
                    }
                    if (!String.IsNullOrEmpty(dateTo))
                    {
                        dateToDateTime = Convert.ToDateTime(dateTo).Date;
                    }

                    if (dateFromDateTime > dateToDateTime)
                    {
                        DateTime temp = dateToDateTime;
                        dateToDateTime = dateFromDateTime;
                        dateFromDateTime = temp;
                    }

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        // orderBy = nvc["order[0][column]"];
                        orderBy = "Pin";
                        orderDir = nvc["order[0][dir]"];
                    }
                    #endregion 

                    int recordsTotal = _nightWorkService.LoadNightWorkCount(dateFromDateTime, dateToDateTime, mentorTeamMemberIdList, pin.Trim(), userMenu, organizationId, branchId, campusId, departmentId, true);
                    long recordsFiltered = recordsTotal;
                    IList<NightWorkDto> dataList = _nightWorkService.LoadNightWork(dateFromDateTime, dateToDateTime, mentorTeamMemberIdList, pin, userMenu, organizationId, branchId, campusId, departmentId, start, length, orderBy, orderDir, true);
                    var data = new List<object>();
                    foreach (var c in dataList)
                    {
                        var str = new List<string>();
                        str.Add(c.Pin.ToString());
                        str.Add(c.Name);
                        str.Add(c.Department);
                        str.Add(c.Organization);
                        str.Add(c.Branch);
                        str.Add(c.Campus);
                        str.Add(c.NightWorkDate.ToString("yyyy-MM-dd"));
                        str.Add(c.TimeFrom.ToString("hh:mm tt"));
                        str.Add(c.TimeTo.ToString("hh:mm tt"));
                        str.Add(c.ModifyBy != null ? _userService.GetUserNameByAspNetUserId((long)c.ModifyBy) : "");
                        str.Add(CommonHelper.GetReportCommonDateTimeFormat(c.LastModificationDate));
                        if (c.ApprovalType == null || c.ApprovalType==0)
                        {
                            str.Add("No");
                        }
                        else if (c.ApprovalType != null)
                        {
                            str.Add(_commonHelper.GetEmumIdToValue<HolidayWorkApprovalStatus>((int)c.ApprovalType)); 
                        }

                        data.Add(str);
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });

                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                    _logger.Error(ex);
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return HttpNotFound();
        }

        #endregion

        // GET: Hr/NightWork
        public ActionResult Index()
        {
            return View();
        }
    }
}