﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceDeviceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceDevice")]
    public class AttendanceDeviceDeleteTest : AttendanceDeviceBaseTest
    {
        #region Basic test

        [Fact]
        public void Delete_Should_Return_Null_Exception()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _campusFactory.Create()
                .WithBranch(branchFactory.Object)
                .WithCampusRoomList(new List<CampusRoom>() {_campusRoomFactory.Create().Object})
                .Persist();
            //_campusRoomFactory.CreateMore(1, null);
            //_campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object);
            Assert.Throws<NullObjectException>(() => _attendanceDeviceService.Delete(null));
            Assert.Throws<NullObjectException>(() => _attendanceDeviceService.Delete(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Should_Check_Dependency()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();

            var obj1 = _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object).Persist().Object;
            _attendanceDeviceSynchronizationFactory.Create().WithAttendanceDevice(obj1).Persist();
            _attendanceDeviceFactory.Evict(obj1);
            var obj2 = _attendanceDeviceService.GetAttendanceDevice(obj1.Id);
            var asynch = obj2.AttendanceDeviceRawDatas;
            obj2.Status = AttendanceDevice.EntityStatus.Delete;
            Assert.Throws<DependencyException>(() => _attendanceDeviceService.Delete(obj2));
        }

        [Fact]
        public void Should_Delete_Successfully()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            var obj1 = _attendanceDeviceFactory.Create()
                 .WithCampus(_campusFactory.Object)
                 .WithAttendanceSyncronizer(_campusFactory.Object).Persist().Object;
            var objUpdate = _attendanceDeviceService.GetAttendanceDevice(obj1.Id);
            objUpdate.Status = AttendanceDevice.EntityStatus.Delete;
            _attendanceDeviceService.Delete(objUpdate);
            var objUpdate2 = _attendanceDeviceService.GetAttendanceDevice(obj1.Id);
            Assert.Null(objUpdate2);
        }

        #endregion
    }
}
