﻿$(document).ready(function () {
    $("#MenuGroupId").change(function () {
        var menuGroupId = $(this).val();
        if (menuGroupId != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/MenuAccess/GetMenuForDropDownList",
                cache: false,
                async: true,
                data: { "MenuGroupId": menuGroupId },
                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function(result) {
                    if (result.hasOwnProperty('IsSuccess')) {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "s"
                        });
                    } else {
                        $("#MenuId").html(result);
                        $("#RenderActions").html("");
                    }
                    $.unblockUI();
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function(result) {
                    $.fn.customMessage({
                        displayMessage: "Unable to load data.",
                        displayMessageType: "e"
                    });
                }
            });
        } else {
            $('#MenuId').html("<option value=''>Select Menu</option>");
            $('#ControllersId').html("<option value=''>Select Controller</option>");
            $("#RenderActions").html("");
            $("#Area").val("");
        }
    });
    $("#MenuId").change(function () {
        var controllerId = $("#ControllersId").val();
        var menuId = $(this).val();
        if (menuId != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/MenuAccess/GetMenuAccessData",
                cache: false,
                async: true,
                data: { "menu": menuId, "controllerId": controllerId },
                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function(result) {
                    if (result.hasOwnProperty('IsSuccess')) {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "s"
                        });
                    } else {
                        $("#RenderActions").html(result);
                    }
                    $.unblockUI();
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function(result) {
                    $.fn.customMessage({
                        displayMessage: "Unable to load data.",
                        displayMessageType: "e"
                    });
                }
            });
        } else {
            $("#Area").val("");
            $('#ControllersId').html("<option value=''>Select Controller</option>");
            $("#RenderActions").html("");
        }
    });

    $("#Area").change(function () {
        var area = $(this).val();
        if (area != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/MenuAccess/GetControllersForDropDownList",
                cache: false,
                async: true,
                data: { "area": $(this).val() },
                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function(result) {
                    if (result.hasOwnProperty('IsSuccess')) {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "s"
                        });
                    } else {
                        $("#ControllersId").html(result);
                        var menu = $("#MenuId").val();
                        if (menu != "") {
                            getMenuAccessData();
                        } else {
                            $("#RenderActions").html("");
                        }
                    }
                    $.unblockUI();
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function(result) {
                    $.fn.customMessage({
                        displayMessage: "Unable to update Controllers.",
                        displayMessageType: "e"
                    });
                }
            });
        } else {
            $('#ControllersId').html("<option value=''>Select Controller</option>");
            var menuId = $("#MenuId").val();
            if (menuId != "") {
                getMenuAccessData();
            }
        }
    });
    
    $("#ControllersId").change(function () {
        var menuId = $("#MenuId").val();
        if (menuId != "") {
            getMenuAccessData();
        } else {
            $.fn.customMessage({
                displayMessage: "Please select menu.",
                displayMessageType: "e"
            });
        }

    });

    function getMenuAccessData() {
        var menuId = $("#MenuId").val();
        var controllerId = $('#ControllersId').val();
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/MenuAccess/GetMenuAccessData",
            cache: false,
            async: true,
            data: { "menu": menuId, "controllerId": controllerId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                if (result.hasOwnProperty('IsSuccess')) {
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "s"
                    });

                } else {
                    $("#RenderActions").html(result);
                }
                $.unblockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.fn.customMessage({
                    displayMessage: "Unable to display actions.",
                    displayMessageType: "e"
                });

            }
        });
    }
});