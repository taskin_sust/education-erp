using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class SupplierBankDetailsMap : BaseClassMap<SupplierBankDetails, long>
    {
        public SupplierBankDetailsMap()
        {
            Table("UINV_SupplierBankDetails");
            LazyLoad();
            References(x => x.Supplier).Column("SupplierId");
            References(x => x.BankBranch).Column("BankBranchId");
            //Map(x => x.Name).Column("Name").Not.Nullable();
            Map(x => x.Rank).Column("Rank").Not.Nullable();
            Map(x => x.AccountTitle).Column("AccountTitle").Not.Nullable();
            Map(x => x.AccountNo).Column("AccountNo").Not.Nullable();
        }
    }
}
