using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using log4net;
using NHibernate;
using NHibernate.Hql.Ast.ANTLR.Tree;
using NHibernate.Transaction;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface ISupplierService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(Supplier supplier);

        bool Delete(Supplier entity);

        #endregion

        #region Single Instances Loading Function

        Supplier LoadById(long id);

        #endregion

        #region List Loading Function

        IList<Supplier> LoadSupplierByItemIds(string[] itemIds);
        IList<Supplier> LoadSupplierList(int start, int length, string orderBy, string toUpper, string name, string status);
        IList<Supplier> LoadOnlyCommonSuppliersByItemIds(string[] itemIds);
        IList<Supplier> LoadAll();
        IList<Supplier> LoadSupplierByIds(long[] supplierIds);

        #endregion

        #region Others Function

        int GetSupplierCount(string name, string status);

        bool IsBankAccountExistsByBankBranch(long id, string accountNumber, long bankBranchId);

        #endregion

        #region Helper Function

        bool IsSupplierNameExist(string supplierName, long? id);
        bool IsTlnIncExist(string tlnInc, long? id);
        bool IsTinExist(string trim, long? id);
        bool IsVatRegistrationNoExist(string vatRegNumber, long? id);
        #endregion


    }
    public class SupplierService : BaseService, ISupplierService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly ISupplierDao _supplierDao;
        private readonly IBankBranchDao _bankBranchDao;
        private readonly ISupplierBankDetailsDao _supplierBankDetailsDao;
        private readonly IWorkOrderDao _workOrderDao;
        private readonly IItemDao _itemDao;
        private readonly ISupplierItemDao _supplierItemDao;
        public SupplierService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _supplierDao = new SupplierDao { Session = session };
            _supplierBankDetailsDao = new SupplierBankDetailsDao { Session = session };
            _workOrderDao = new WorkOrderDao() { Session = session };
            _bankBranchDao = new BankBranchDao() { Session = session };
            _itemDao = new ItemDao() { Session = session };
            _supplierItemDao = new SupplierItemDao() { Session = session };
        }
        #endregion

        #region Operational Functions
        
        public bool SaveOrUpdate(Supplier supplier)
        {
            bool returnStatus = false;
            ITransaction trans = null;
            try
            {
                if (supplier == null)
                {
                    throw new NullObjectException("Supplier form not submitted!");
                }
                if (supplier.MobileNo != null && !supplier.MobileNo.Equals(""))
                {
                    if (!(_commonHelper.CheckMobileNumberValidation(new string[] { supplier.MobileNo })))
                    {
                        throw new InvalidDataException("Mobile Or Telephone number not in valid format!");
                    }
                }
                CheckValidation(supplier);
                CheckBankInfoValidation(supplier);
                UniquePropertyCheck(supplier);

                using (trans = Session.BeginTransaction())
                {
                    if (supplier.MobileNo != null && supplier.MobileNo.Length == 11)
                        supplier.MobileNo = _commonHelper.AddCountryCodeWithMobileNumber(supplier.MobileNo);

                    int rank = 1;
                    foreach (var sd in supplier.SupplierBankDetails)
                    {
                        sd.Id = 0;
                        sd.CreateBy = GetCurrentUserId();
                        sd.ModifyBy = GetCurrentUserId();
                        sd.CreationDate = DateTime.Now;
                        sd.ModificationDate = DateTime.Now;
                        sd.Status = SupplierBankDetails.EntityStatus.Active;
                        sd.Rank = rank;
                        sd.Supplier = supplier;
                        rank++;
                    }

                    if (supplier.Id == 0)
                    {
                        supplier.Status = Supplier.EntityStatus.Active;
                        supplier.Rank = GetMaximumRank() + 1;
                        _supplierDao.Save(supplier);
                    }
                    else if (supplier.Id > 0)
                    {
                       _supplierDao.SaveOrUpdate(supplier);
                    }
                    trans.Commit();
                    returnStatus = true;
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                {
                    trans.Rollback();
                }
            }
            return returnStatus;
        }
        
        public bool Delete(Supplier entity)
        {
            ITransaction transaction = null;
            var workOrderList = entity.WorkOrderList.Where(x => x.Status != SupplierBankDetails.EntityStatus.Delete).ToList<WorkOrder>();
            var priceQuoteList = entity.SupplierPriceQuoteList.Where(x => x.Status != SupplierBankDetails.EntityStatus.Delete).ToList<SupplierPriceQuote>();
            try
            {
                if (workOrderList.Count > 0 || priceQuoteList.Count > 0)
                {
                    throw new DependencyException("Action failed! This supplier has one or more work order or quotation or ledger.");
                }
                else
                {
                    using (transaction = Session.BeginTransaction())
                    {
                        entity.Status = Supplier.EntityStatus.Delete;
                        _supplierDao.Update(entity);
                        transaction.Commit();
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public Supplier LoadById(long id)
        {
            try
            {
                return _supplierDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Supplier> LoadSupplierByItemIds(string[] itemIds)
        {
            try
            {
                var supplierIdsList = new List<string>();
                if (itemIds != null && itemIds.Any())
                {
                    supplierIdsList = _supplierItemDao.LoadSupplierByItemIds(itemIds).Select(x => x.Supplier.Id.ToString()).ToList();
                }
                if (supplierIdsList.Count <= 0) return new List<Supplier>();
                return _supplierDao.LoadByIds(supplierIdsList.ToArray());
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        #endregion

        #region List Loading Function

        public IList<Supplier> LoadSupplierList(int start, int length, string orderBy, string toUpper, string name, string status)
        {
            try
            {
                return _supplierDao.LoadSupplier(start, length, orderBy, toUpper, name, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Supplier> LoadOnlyCommonSuppliersByItemIds(string[] itemIds)
        {
            try
            {
                if (itemIds.ToList().Count <= 0) return _supplierDao.LoadAll();
                List<string> supplierIdsList = LoadCommonSupplier(itemIds);
                IList<Supplier> supplierList = _supplierDao.LoadByIds(supplierIdsList.ToArray());
                return supplierList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        private List<string> LoadCommonSupplier(string[] itemIds)
        {
            try
            {
                var resultList = new List<string>();
                int counter = 0;
                foreach (string itemId in itemIds)
                {
                    var idary = new[] { itemId };
                    IList<SupplierItem> supplierItems = _supplierItemDao.LoadSupplierByItemIds(idary).ToList();
                    if (counter == 0)
                        resultList = supplierItems.Select(x => x.Supplier.Id.ToString()).ToList();
                    else
                        if (resultList.Count == 0) break;
                        else if (supplierItems.Count > 0)
                        {
                            List<string> newsuppliers = supplierItems.Select(x => x.Supplier.Id.ToString()).ToList();
                            resultList = resultList.Intersect((IEnumerable<string>)newsuppliers).ToList();
                        }
                        else return new List<string>();
                    counter++;
                }
                return resultList;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public IList<Supplier> LoadAll()
        {
            try
            {
                IList<Supplier> list = _supplierDao.LoadAll(Supplier.EntityStatus.Active).ToList<Supplier>();
                return list;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Supplier> LoadSupplierByIds(long[] supplierIds)
        {
            try
            {
                return _supplierDao.LoadSupplierByIds(supplierIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int GetSupplierCount(string name, string status)
        {
            try
            {
                return _supplierDao.GetSupplierCount(name, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Helper function

        public int GetMaximumRank()
        {
            try
            {
                return _supplierDao.GetMaximumRank(new Supplier());
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        private static void CheckValidation(Supplier supplier)
        {
            var validationResult = ValidationHelper.ValidateEntity<Supplier, Supplier.SupplierMetaData>(supplier);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }
        
        private void CheckBankInfoValidation(Supplier supplier)
        {
            bool isAccountMandatory = !string.IsNullOrWhiteSpace(supplier.Tin) || !string.IsNullOrWhiteSpace(supplier.TlnInc) ||
                                      !string.IsNullOrWhiteSpace(supplier.VatRegistrationNo);

            var blist = new List<SupplierBankDetails>();
            foreach (SupplierBankDetails bankDetail in supplier.SupplierBankDetails)
            {
                if (string.IsNullOrWhiteSpace(bankDetail.AccountNo) ||
                    string.IsNullOrWhiteSpace(bankDetail.AccountTitle) || bankDetail.BankBranch == null) continue;
                blist.Add(new SupplierBankDetails()
                {
                    Id = bankDetail.Id, BankBranch = bankDetail.BankBranch, AccountTitle = bankDetail.AccountTitle, AccountNo = bankDetail.AccountNo
                });
            }

            if (blist.Count != supplier.SupplierBankDetails.Count)
            {
                throw new InvalidDataException("Supplier bank branch list not in valid format!");
            }

            var distinctList = new List<SupplierBankDetails>();
            foreach (var bd in supplier.SupplierBankDetails)
            {
                if (!(distinctList.Any(x => x.AccountNo == bd.AccountNo && x.BankBranch.Id == bd.BankBranch.Id)))
                {
                    distinctList.Add(bd);
                }
            }

            if (distinctList.Count != blist.Count)
            {
                throw new InvalidDataException("Supplier bank branch duplicated found!");
            }

            if (isAccountMandatory && distinctList.Count <= 0)
            {
                throw new InvalidDataException("Supplier form submitted without bank branch!");
            }
            if (distinctList.Select(sdb => IsBankAccountExistsByBankBranch(sdb.Id, sdb.AccountNo, sdb.BankBranch.Id)).Any(isExists => isExists))
            {
                throw new InvalidDataException("Supplier bank branch list not in valid format!");
            }
        }

        private void UniquePropertyCheck(Supplier supplier)
        {
            string fieldName = "";
            if (supplier.Name != null)
            {
                bool isExistsByName = _supplierDao.CheckDuplicateSupplier(out fieldName, supplier.Name, null, null, null, supplier.Id);
                if (isExistsByName)
                {
                    throw new DuplicateEntryException("Duplicate supplier found for" + fieldName + "! Please try for another " + fieldName + "!");
                }
            }
            if (supplier.Tin != null && !supplier.Tin.Equals(""))
            {
                bool isExistsByTin = _supplierDao.CheckDuplicateSupplier(out fieldName, null, supplier.Tin, null, null, supplier.Id);
                if (isExistsByTin)
                {
                    throw new DuplicateEntryException("Duplicate supplier found for" + fieldName + "! Please try for another " + fieldName + "!");
                }
            }
            if (supplier.TlnInc != null && !supplier.TlnInc.Equals(""))
            {
                bool isExistsByTlnInc = _supplierDao.CheckDuplicateSupplier(out fieldName, null, null, supplier.TlnInc, null, supplier.Id);
                if (isExistsByTlnInc)
                {
                    throw new DuplicateEntryException("Duplicate supplier found for" + fieldName + "! Please try for another " + fieldName + "!");
                }
            }
            if (supplier.VatRegistrationNo != null && !supplier.VatRegistrationNo.Equals(""))
            {
                bool isExistsByTVatRegNo = _supplierDao.CheckDuplicateSupplier(out fieldName, null, null, null, supplier.VatRegistrationNo, supplier.Id);
                if (isExistsByTVatRegNo)
                {
                    throw new DuplicateEntryException("Duplicate supplier found for" + fieldName + "! Please try for another " + fieldName + "!");
                }
            }
        }

        public bool IsSupplierNameExist(string supplierName, long? id)
        {
            bool isExists = false;
            try
            {
                var entity = _supplierDao.SupplierByName(supplierName, id);
                if (entity == null)
                {
                    isExists = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return isExists;
        }

        public bool IsTlnIncExist(string tlnInc, long? id)
        {
            bool isExists = false;

            try
            {
                var entity = _supplierDao.LoadByTlnInc(tlnInc, id);
                if (entity == null)
                {
                    isExists = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return isExists;
        }

        public bool IsTinExist(string tin, long? id)
        {
            bool isExists = false;

            try
            {
                var entity = _supplierDao.LoadByTin(tin, id);
                if (entity == null)
                {
                    isExists = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return isExists;
        }

        public bool IsVatRegistrationNoExist(string vatRegNumber, long? id)
        {
            bool isExists = false;

            try
            {
                var entity = _supplierDao.LoadByVatRegNo(vatRegNumber, id);
                if (entity == null)
                {
                    isExists = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return isExists;
        }

        public bool IsBankAccountExistsByBankBranch(long id, string accountNo, long bankBranchId)
        {
            bool isExist = true;
            try
            {
                isExist = _supplierBankDetailsDao.IsBankAccountExistsByBankBranch(id, accountNo, bankBranchId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return isExist;
        }

        private static string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            return (propertyExpression.Body as MemberExpression).Member.Name;
        }

        #endregion
    }
}

