﻿using System;
using System.Collections;
using UdvashERP.BusinessModel.Entity.Base;
namespace UdvashERP.Services.Test.ObjectFactory
{
    public interface IObjectFactoryBase
    {        
        void DeleteAll();       
        string GetPcUserName();        
        int Level { get; set; }
        string Name { get; set; }
        int ListStartIndex { get; set; }

    }
}
