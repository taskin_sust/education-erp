using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using log4net;
using NHibernate;
using NHibernate.Hql.Ast.ANTLR.Tree;
using NHibernate.Transaction;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface IStockReportService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<dynamic> StockSummaryReport(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu
                                            , long organizationId, long branchId, int[] purposeIds, string[] programSession
                                            , long[] itemGroupIds, long[] itemIds, DateTime dateFrom, DateTime dateTo);

        IList<dynamic> StockSummaryDetailsReport(int start, int length, List<UserMenu> userMenu, long organizationId, long branchId
                                            , int[] purposeIds, string[] programSession, long[] itemGroupIds, long[] itemIds
                                            , DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Others Function

        int StockSummaryReportRowCount(List<UserMenu> userMenu, long organizationId, long branchId, int[] purposeIds, string[] programSession
                                                , long[] itemGroupIds, long[] itemIds, DateTime dateFrom, DateTime dateTo);

        int StockSummaryDetailsReportRowCount(List<UserMenu> userMenu, long organizationId, long branchId, int[] purposeIds, string[] programSession
                                                , long[] itemGroupIds, long[] itemIds, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Helper Function

        #endregion
    }
    public class StockReportService : BaseService, IStockReportService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IStockReportDao _stockReportDao;
        private readonly IItemDao _itemDao;
        private readonly IProgramBranchSessionDao _programBranchSessionDao;
        public StockReportService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _stockReportDao = new StockReportDao { Session = session };
            _itemDao = new ItemDao() { Session = session };
            _programBranchSessionDao = new ProgramBranchSessionDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function


        public IList<dynamic> StockSummaryReport(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long organizationId,
            long branchId, int[] purposeIds, string[] programSession, long[] itemGroupIds, long[] itemIds, DateTime dateFrom,
            DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }

                if (branchId <= 0)
                {
                    throw new InvalidDataException("Branch is not defined");
                }

                long[] bIds = { };
                var organizationIdList = _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId));
                bIds = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, new List<long>() { branchId }).ToArray();
                if (bIds.Length <= 0 || bIds[0] <= 0)
                {
                    throw new InvalidDataException("Permission denied for selected branch!");
                }
                itemIds = itemIds.Where(val => val != 0).ToArray();
                if (itemIds.Length <= 0)
                {
                    itemGroupIds = itemGroupIds.Where(val => val != 0).ToArray();
                    itemIds = _itemDao.LoadItem(itemGroupIds.Length == 0 ? null : itemGroupIds, organizationId).Select(x => x.Id).ToArray();
                }
                if (itemIds.Length == 0)
                {
                    IList<long> gIds = new List<long>();
                    gIds.Add(0);
                    itemIds = gIds.ToArray();
                }

                programSession = FilterProgramSession(userMenu, authOrgList, bIds, itemIds, programSession);
                IList<dynamic> list = _stockReportDao.StockSummaryReport(start, length, orderBy, orderDir, branchId, itemGroupIds
                                                                        , itemIds, purposeIds, programSession, dateFrom, dateTo);
                return list;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public IList<dynamic> StockSummaryDetailsReport(int start, int length, List<UserMenu> userMenu, long organizationId
                                                        , long branchId, int[] purposeIds, string[] programSession
                                                        , long[] itemGroupIds, long[] itemIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                if (branchId <= 0)
                {
                    throw new InvalidDataException("Branch is not defined");
                }
                long[] bIds = { };
                var organizationIdList = _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId));
                bIds = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, new List<long>() { branchId }).ToArray();
                if (bIds.Length <= 0 || bIds[0] <= 0)
                {
                    throw new InvalidDataException("Permission denied for selected branch!");
                }
                itemIds = itemIds.Where(val => val != 0).ToArray();
                if (itemIds.Length <= 0 && itemGroupIds.Length > 0)
                {
                    itemGroupIds = itemGroupIds.Where(val => val != 0).ToArray();
                    itemIds = _itemDao.LoadItem(itemGroupIds, organizationId).Select(x => x.Id).ToArray();
                }
                programSession = FilterProgramSession(userMenu, authOrgList, bIds, itemIds, programSession);

                IList<dynamic> list = _stockReportDao.StockSummaryDetailsReport(start, length, branchId, itemIds, purposeIds, programSession, dateFrom, dateTo);
                return list;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion

        #region Others Function

        private string[] FilterProgramSession(List<UserMenu> userMenu, List<long> authOrgList, long[] bIds, IEnumerable<long> itemIds, string[] programSession)
        {
            if (programSession[0].Equals(string.Empty))
            {
                var authProIdList = AuthHelper.LoadProgramIdList(userMenu, authOrgList, bIds.ToList());
                var psList = _programBranchSessionDao.LoadProgramSession(authOrgList, bIds.ToList<long>(), authProIdList, itemIds.ToList(), false);
                IList<string> li = psList.Select(programSessionDto => programSessionDto.ProgramAndSessionId).ToList();
                li.Add("N/A");
                programSession = li.ToArray();
            }
            else if (programSession[0] == "0")
            {
                var ps = programSession.ToList();
                ps.RemoveAt(0);
                programSession = ps.ToArray();
            }
            return programSession;
        }

        public int StockSummaryReportRowCount(List<UserMenu> userMenu, long organizationId, long branchId, int[] purposeIds, string[] programSession, long[] itemGroupIds
                                                            , long[] itemIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                if (branchId <= 0)
                {
                    throw new InvalidDataException("Branch is not defined");
                }
                long[] bIds = { };
                var organizationIdList = _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId));
                bIds = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, new List<long>() { branchId }).ToArray();
                if (bIds.Length <= 0 || bIds[0] <= 0)
                {
                    throw new InvalidDataException("Permission denied for selected branch!");
                }
                itemIds = itemIds.Where(val => val != 0).ToArray();
                if (itemIds.Length <= 0 && itemGroupIds.Length > 0)
                {
                    itemGroupIds = itemGroupIds.Where(val => val != 0).ToArray();
                    itemIds = _itemDao.LoadItem(itemGroupIds, organizationId).Select(x => x.Id).ToArray();
                }
                programSession = FilterProgramSession(userMenu, authOrgList, bIds, itemIds, programSession);
                return _stockReportDao.StockSummaryReportRowCount(branchId, itemGroupIds, itemIds, purposeIds, programSession, dateFrom, dateTo);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public int StockSummaryDetailsReportRowCount(List<UserMenu> userMenu, long organizationId, long branchId, int[] purposeIds,
            string[] programSession, long[] itemGroupIds, long[] itemIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                if (branchId <= 0)
                {
                    throw new InvalidDataException("Branch is not defined");
                }
                long[] bIds = { };
                var organizationIdList = _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId));
                bIds = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, new List<long>() { branchId }).ToArray();
                if (bIds.Length <= 0 || bIds[0] <= 0)
                {
                    throw new InvalidDataException("Permission denied for selected branch!");
                }
                itemIds = itemIds.Where(val => val != 0).ToArray();
                if (itemIds.Length <= 0 && itemGroupIds.Length > 0)
                {
                    itemIds = _itemDao.LoadItem(itemGroupIds, organizationId).Select(x => x.Id).ToArray();
                }
                programSession = FilterProgramSession(userMenu, authOrgList, bIds, itemIds, programSession);
                return _stockReportDao.StockSummaryDetailsReportRowCount(branchId, itemIds, purposeIds, programSession, dateFrom, dateTo);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion

        #region Helper function

        #endregion
    }
}

