﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Qmaster.Print.Forms
{
    public partial class LoginForm : BaseForm
    {

        #region object\initial\load
        
        public bool isLogin = false;
        
        public LoginForm()
        {
            InitializeComponent();
            txtUserName.Text = "Admin";
            txtPassword.Text = "password";
        }

        #endregion

        #region Login Button

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUserName.Text;
            string password = txtPassword.Text;

            if (username == "Admin" && password == "password")
            {
                isLogin = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Try Again!!");
            }
        }

        #endregion

    }
}
