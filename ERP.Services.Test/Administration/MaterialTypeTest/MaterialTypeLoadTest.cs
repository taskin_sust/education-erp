﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UdvashERP.Services.Test.Administration.MaterialTypeTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "MaterialType")]
    public class MaterialTypeLoadTest : MaterialTypeBaseTest
    {
        [Fact]
        public void MaterialType_LoadById()
        {
            _materialTypeFactory.Create().WithOrganization();
            _materialTypeService.Save(_materialTypeFactory.Object);
            var obj = _materialTypeService.GetMaterialType(_materialTypeFactory.Object.Id);
            Assert.NotNull(obj);
        }
        [Fact]
        public void MaterialTypeList_ByOrganizationId()
        {
            organizationFactory.Create().Persist();
            _materialTypeFactory.CreateMore(5, organizationFactory.Object).Persist();
            var objList = _materialTypeService.LoadMaterialType(organizationFactory.Object.Id);
            Assert.Equal(objList.Count, 5);
        }
        [Fact]
        public void LoadMaterialTypeList()
        {
            organizationFactory.Create().Persist();
            _materialTypeFactory.CreateMore(2, organizationFactory.Object).Persist();
            var objList = _materialTypeService.LoadMaterialType();
            Assert.NotEqual(objList.Count, 0);
        }
        [Fact]
        public void Get_MaterialType_By_DownRank_Return_True()
        {
            organizationFactory.Create().Persist();
            _materialTypeFactory.CreateMore(2, organizationFactory.Object).Persist();
            int newRank = 0;
            newRank = _materialTypeFactory.ObjectList[0].Rank + 1;
            var materialOldObj = _materialTypeService.GetMaterialTypeByRankNextOrPrevious(newRank, "down");
            Assert.Equal(materialOldObj.Id, _materialTypeFactory.ObjectList[1].Id);
        }
        [Fact]
        public void Get_MaterialType_By_UpRank_Return_True()
        {
            organizationFactory.Create().Persist();
            _materialTypeFactory.CreateMore(2, organizationFactory.Object).Persist();
            int newRank = 0;
            newRank = _materialTypeFactory.ObjectList[1].Rank - 1;
            var materialOldObj = _materialTypeService.GetMaterialTypeByRankNextOrPrevious(newRank, "up");
            Assert.Equal(materialOldObj.Id, _materialTypeFactory.ObjectList[0].Id);
        }
        [Fact]
        public void Get_MaterialType_Maximum_Rank()
        {
            _materialTypeFactory.Create().WithOrganization().Persist();
            int maxRank = _materialTypeService.GetMaximumRank(_materialTypeFactory.Object);
            var lastObject = _materialTypeService.LoadMaterialType().Last();
            Assert.Equal(lastObject.Rank, maxRank);
        }
        [Fact]
        public void Get_MaterialType_Minimum_Rank()
        {
            _materialTypeFactory.Create().WithOrganization().Persist();
            int minRank = _materialTypeService.GetMinimumRank(_materialTypeFactory.Object);
            var list = _materialTypeService.LoadMaterialType();
            int checkWith = _materialTypeFactory.Object.Rank - (list.Count) + 1;
            Assert.Equal(checkWith, minRank);
        }


    }
}
