﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class InstituteCategoryFactory : ObjectFactoryBase<InstituteCategory>
    {
        IInstitutionCategoryService _institutionCategoryService;
        public InstituteCategoryFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {           
            _institutionCategoryService = new InstitutionCategoryService(_session);
        }

        public InstituteCategoryFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        public InstituteCategoryFactory Create()
        {
            Object = CreateInstituteCategory(Name, 1,true);
            SingleObjectList.Add(Object);
            return this;
        }

        public InstituteCategoryFactory CreateWith(string instituteCategoryName,
            int rank,
            bool isUniversity)
        {
            Object = CreateInstituteCategory(instituteCategoryName, rank, isUniversity);
            SingleObjectList.Add(Object);
            return this;
        }

        public InstituteCategoryFactory CreateMore(int numberOfInstituteCategory)
        {
            CreatInstituteCategoryList(numberOfInstituteCategory);
            return this;
        }

        private List<InstituteCategory> CreatInstituteCategoryList(int numberOfInstituteCategory)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<InstituteCategory>();
            var numberOfInstituteCategoryPrefix = Guid.NewGuid().ToString().Substring(0, 6);

            for (int i = 0; i < numberOfInstituteCategory; i++)
            {
                var name = numberOfInstituteCategoryPrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                bool isUniversity = true;
                var instituteCategory = CreateInstituteCategory(name, i, isUniversity);
                list.Add(instituteCategory);
            }
            ObjectList.AddRange(list);
            return list;
        }


        private InstituteCategory CreateInstituteCategory(string instituteCategoryName, int rank, bool isUniversity)
        {
            var instituteCategory = new InstituteCategory()
            {
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Name = instituteCategoryName,
                Rank = rank,
                IsUniversity = isUniversity,
                Status = InstituteCategory.EntityStatus.Active,
            };

            return instituteCategory;
        }


        public IList<InstituteCategory> LoadAll()
        {
            return _institutionCategoryService.GetInstituteCategoryList();
        }

        public InstituteCategoryFactory GetRandomInstituteCategory() 
        {
            IList<InstituteCategory> instituteCategoryList = LoadAll();
            
            if (instituteCategoryList.Any())
            {
                var random = new Random();
                Object = instituteCategoryList[random.Next(instituteCategoryList.Count())];
            }
            return this;
        } 
        

        public void Cleanup()
        {
            DeleteAll();
        }
    }
}
