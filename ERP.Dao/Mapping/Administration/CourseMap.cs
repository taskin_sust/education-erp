using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{


    public partial class CourseMap : BaseClassMap<Course, long>
    {

        public CourseMap()
        {
            Table("Course");
            LazyLoad();
            Map(x => x.Rank);
            Map(x => x.Name);
            Map(x => x.MaxSubject);
            Map(x => x.OfficeMinSubject);
            Map(x => x.PublicMinSubject);
            Map(x => x.OfficeMinPayment);
            Map(x => x.PublicMinPayment);
            Map(x => x.OfficeCompulsory);
            Map(x => x.PublicCompulsory);
            Map(x => x.StartDate);
            Map(x => x.EndDate);
            References(x => x.Program).Column("ProgramId");
            References(x => x.RefSession).Column("SessionId");
            HasMany(x => x.CourseSubjects).KeyColumn("CourseId").Cascade.All();
            HasMany(x => x.StudentClassAttendences).KeyColumn("CourseId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.DiscountDetails).KeyColumn("CourseId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ExamList).KeyColumn("CourseId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.LectureSettings).KeyColumn("CourseId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ComplementaryCourses).KeyColumn("CourseId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SmsStudentRegistrationSettingList).KeyColumn("CourseId").Cascade.AllDeleteOrphan().Inverse();

        }
    }
}
