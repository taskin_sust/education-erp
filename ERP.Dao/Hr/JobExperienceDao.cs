﻿using System.Collections.Generic;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IJobExperienceDao : IBaseDao<JobExperience, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<JobExperience> GetTeamMemberJobExperiences(long teamMemberId);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
    public class JobExperienceDao : BaseDao<JobExperience, long>, IJobExperienceDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<JobExperience> GetTeamMemberJobExperiences(long teamMemberId)
        {
            return Session.QueryOver<JobExperience>().Where(x => x.TeamMember.Id == teamMemberId && x.Status != JobExperience.EntityStatus.Delete).List<JobExperience>();
        }
        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion

        
    }

}
