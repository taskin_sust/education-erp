﻿$(document).ready(function () {

    $("#mentorAttendanceAdjustmentTableList").tableHeadFixer({ "left": 3, "z-index": 2 });
    setAttendanceAdjustmentRowCount();

    $('.mentorDatepicker').datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: false,
        showMeridian: true,
        initialDate: new Date(),
        startView: 2,
        minView: 2,
        maxView: 4
    });



    $(document).on("focus", "input.historyDate", function () {
        var today = new Date();
        var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: today,
            //startDate: startDate,
            endDate: lastDay,
            startView: 2,
            minView: 2,
            maxView: 4
        });
    });
    
    $(document).on("input propertychange", ".reasonForAdjustment", function (event) {
        var missing = $(this).val() === "";
        $(this).toggleClass('highlight', missing);
    });


    $(document).on("click", ".approveTeamMemberAAd", function () {
    //$(".approveTeamMemberAAd").click(function () {
        var row = $(this).parent().parent();
        var checkRow = checkIndividualRowValidity(row);
        if (checkRow > 0) {
            notValidRow(row);
        } else {
            bootbox.confirm("<h3>Are you sure you want to approve attendance?</h3>", function (result) {
                if (result) {
                    var mentorAttendanceAdjustmentAllRowArray = [];
                    var mentorAttendanceAdjustmentRowViewModel = {};
                    mentorAttendanceAdjustmentRowViewModel["Id"] = row.find(".tableId").val();
                    mentorAttendanceAdjustmentRowViewModel["MemberId"] = row.find(".employeeId").val();
                    mentorAttendanceAdjustmentRowViewModel["Date"] = row.find(".datepicker").val();
                    mentorAttendanceAdjustmentRowViewModel["ReasonForAdjustment"] = row.find(".reasonForAdjustment").val();
                    mentorAttendanceAdjustmentRowViewModel["TimeFrom"] = row.find(".timeFrom").val();
                    mentorAttendanceAdjustmentRowViewModel["TimeTo"] = row.find(".timeTo").val();
                    mentorAttendanceAdjustmentAllRowArray.push(mentorAttendanceAdjustmentRowViewModel);
                    var data = JSON.stringify({ "MentorAttendanceAdjustments": mentorAttendanceAdjustmentAllRowArray });
                    $.ajax({
                        type: "post",
                        url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/MentorApproveAllAttendanceAdjustment",
                        cache: false,
                        async: true,
                        dataType: "json",
                        contentType: "application/json",
                        data: data,
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (result2) {
                            $.unblockUI();
                            if (result2.IsSuccess) {
                                $.fn.customMessage({
                                    displayTime: 120000,
                                    displayMessage: result2.returnSuccess,
                                    displayMessageType: "success"
                                });
                                window.setTimeout(function () { window.location.reload(true); }, 5000);
                            } else {
                                $.fn.customMessage({
                                    displayMessage: result2.returnSuccess,
                                    displayMessageType: "error"
                                });
                            }
                            setAttendanceAdjustmentRowCount();
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function () {
                            $.unblockUI();
                        }
                    });
                }
            }).css({ 'margin-top': (($(window).height() / 4)) });
        }
    });

    //$(".rejectTeammemberAAd").click(function () {
    $(document).on("click", ".rejectTeammemberAAd", function () {

        var id = $(this).parent().parent().find('input.tableId').val();
        bootbox.confirm("<h3>Are you sure you want to reject attendance?</h3>", function (result) {
            if (result) {
                $.ajax({
                    type: "post",
                    url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/MentorRejectedAttendanceAdjustment",
                    cache: false,
                    async: true,
                    data: { "id": id },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (result2) {
                        $.unblockUI();
                        if (result2.IsSuccess) {
                            $.fn.customMessage({
                                displayMessage: result2.returnSuccess,
                                displayMessageType: "success",
                            });
                            window.location.reload(true);
                        } else {
                            $.fn.customMessage({
                                displayMessage: result2.returnSuccess,
                                displayMessageType: "error",
                            });
                        }
                        setAttendanceAdjustmentRowCount();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function () {
                        $.unblockUI();
                    }
                });
            }
        }).css({ 'margin-top': (($(window).height() / 4)) });
    });

    //$(".mentorRecentHistoryTab, #showForm").click(function () {
    $(document).on("click", ".mentorRecentHistoryTab, #showForm", function () {
        var dateFrom = $("#dateFrom").val();
        var dateTo = $("#dateTo").val();
        if (dateFrom > dateTo) {

            $.fn.customMessage({
                displayMessage: "From Datetime must be less than or equal to To Datetime",
                displayMessageType: "error",
            });
        }
        else {
            dataTableRender();
        }
    });

    //$("#submitForm").click(function () {
    $(document).on("click", "#submitForm", function () {
        var checkValidRow;
        var e = 0;
        var mentorAttendanceAdjustmentAllRowArray = [];
        bootbox.confirm("<h3>Are you sure you want to  APPROVE ALL adjustment?</h3>", function (confirmResult) {
            if (confirmResult) {

                if ($("#mentorAttendanceAdjustmentTableList tbody tr").length > 50) {
                    e = e + 1;
                    $.fn.customMessage({
                        displayMessage: "You can approve maximum 50 attendance adjustment at a time. Please filter again.",
                        displayMessageType: "error",
                    });
                }

                $("#mentorAttendanceAdjustmentTableList tbody tr").each(function () {

                    checkValidRow = checkIndividualRowValidity($(this));
                    if (checkValidRow > 0) {
                        e = e + 1;
                    } else {
                        var mentorAttendanceAdjustmentRowViewModel = {};
                        mentorAttendanceAdjustmentRowViewModel["Id"] = $(this).find(".tableId").val();
                        mentorAttendanceAdjustmentRowViewModel["MemberId"] = $(this).find(".employeeId").val();
                        mentorAttendanceAdjustmentRowViewModel["Date"] = $(this).find(".datepicker").val();
                        mentorAttendanceAdjustmentRowViewModel["ReasonForAdjustment"] = $(this).find(".reasonForAdjustment").val();
                        mentorAttendanceAdjustmentRowViewModel["TimeFrom"] = $(this).find(".timeFrom").val();
                        mentorAttendanceAdjustmentRowViewModel["TimeTo"] = $(this).find(".timeTo").val();

                        var checkObject = containsObject(mentorAttendanceAdjustmentRowViewModel, mentorAttendanceAdjustmentAllRowArray);
                        if (checkObject == true) {
                            e = e + 1;
                            notValidRow($(this));
                        } else {
                            mentorAttendanceAdjustmentAllRowArray.push(mentorAttendanceAdjustmentRowViewModel);
                        }
                    }
                });

                if (e == 0) {
                    var data = JSON.stringify({ "MentorAttendanceAdjustments": mentorAttendanceAdjustmentAllRowArray });

                    $.ajax({
                        type: "post",
                        url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/MentorApproveAllAttendanceAdjustment",
                        cache: false,
                        async: true,
                        dataType: "json",
                        contentType: "application/json",
                        data: data,
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (result) {
                            $.unblockUI();
                            if (result.IsSuccess) {
                                $.fn.customMessage({
                                    displayTime: 120000,
                                    displayMessage: result.returnSuccess,
                                    displayMessageType: "success",
                                });
                                window.setTimeout(function () { location.reload(); }, 1000);
                            } else {
                                $.fn.customMessage({
                                    displayMessage: result.returnSuccess,
                                    displayMessageType: "error",
                                });
                            }
                            // window.location.reload(true);
                            //$.unblockUI();

                        },
                        complete: function () {

                            $.unblockUI();
                        },
                        error: function (result) {
                            $.unblockUI();
                            console.log("Failed -----");
                        }
                    });

                } else {

                }
            }
        }).css({ 'margin-top': (($(window).height() / 4)) });
    });


    var itemCount = $(".totalCount").val(); //1;
    var output = "";
    $(document).on("click", ".dynamicRowGenerate", function (event) {
        var html = "";

        // dynamically create rows in the table
        html = "<tr id='tr" + itemCount + "' >" +
            "<td><input type='text' name='date" + itemCount + "' id='date_" + itemCount + "'  class='form-control text-box single-line datepicker' placeholder='Date' readonly='readonly'/></td>" +
            "<td><input type='hidden' id='employeeId" + itemCount + "' value='' class='employeeId'/><input type='text' name='pinName" + itemCount + "' id='pinName_" + itemCount + "' class='form-control text-box single-line autoComplete memberPinClass' value='' rowindex='" + itemCount + "'/></td>" +
            "<td style='width:200px;'><input type='text' name='employeeName" + itemCount + "' id='employeeName_" + itemCount + "'  value='' class='employeeName form-control text-box single-line' readonly='readonly'/></td>" +
            "<td><input type='text' name='reasonforad" + itemCount + "' id='reasonforad_" + itemCount + "' class='form-control text-box single-line reasonForAdjustment' placeholder='Reason for Adjustment'> </td>" +
            "<td><input type='text' name='timefrom" + itemCount + "' id='timefrom_" + itemCount + "' class='form-control text-box single-line timeFrom' placeholder='Time From' readonly='readonly'/> </td>" +
            "<td><input type='text' name='timeto" + itemCount + "' id='timeto_" + itemCount + "' class='form-control text-box single-line timeTo' placeholder='Time To' readonly='readonly'/></td>" +
            "<td><input type='button'  id='" + itemCount + "' value='-' class='btn btn-primary'></td> " +
            "</tr>";
        $(".mentorattendanceAdjustmentTableNew_table").append(html);
        $("#" + itemCount).click(function () {
            var buttonId = $(this).attr("id");
            $("#tr" + buttonId).remove();
            setAttendanceAdjustmentRowCount();
        });
        itemCount++;
        $(".displayData").animate({ scrollTop: $('.displayData').prop("scrollHeight") }, 1000);
        setAttendanceAdjustmentRowCount();
    });

    $(document).on("focus", "input.datepicker", function () {
        var today = new Date();
        var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();

        var pastMonth = new Date(today.setMonth(today.getMonth() - 1, 1));
        var startDate = pastMonth.getFullYear().toString() + "-" + (pastMonth.getMonth() + 1) + "-" + "01";

        var endDate = initialDate;
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: initialDate,
            startDate: startDate,
            endDate: endDate,
            startView: 2,
            minView: 2,
            maxView: 4
        }).on("changeDate", function (e) {
            var missing = $(this).val() === "";
            $(this).toggleClass('highlight', missing);
        });
    });

    $(document).on("focus", "input.timeFrom", function () {
        $(this).datetimepicker({
            format: 'HH:ii p',
            showMeridian: true,
            startView: 1,
            pickDate: false,
            minuteStep: 5,
            autoclose: 1,
            timeOnly: true,
        }).on("changeDate", function (e) {
            var missing = $(this).val() === "";
            $(this).toggleClass('highlight', missing);
        });
    });

    $(document).on("focus", "input.timeTo", function () {
        $(this).datetimepicker({
            format: 'HH:ii p',
            showMeridian: true,
            startView: 1,
            pickDate: false,
            minuteStep: 5,
            autoclose: 1,
            timeOnly: true,
        }).on("changeDate", function (e) {
            var missing = $(this).val() === "";
            $(this).toggleClass('highlight', missing);
        });
    });

    $(document).on("blur", ".memberPinClass", function (event) {
        console.log("In");
        var _this = $(this);
        var memberPin = parseInt(_this.val().trim());
        console.log(memberPin);
        //var AdjDate = _this.parent().prev('td').find('input.datepicker').val();
        //console.log(AdjDate);
        if (memberPin !== "" && memberPin > 0) {
            //if (memberPin > 0) {
                $.ajax({
                    type: "POST",
                    //url: $("body").attr("data-project-root") + "HR/AttendanceAdjustment/AjaxRequestMentorForMemberInformation",
                    url: $("body").attr("data-project-root") + "Hr/CommonAjaxHr/GetTeamMemberInformation",
                    cache: false,
                    async: true,
                    data: { pin: memberPin, isMentor: true },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (result) {
                        $.unblockUI();
                        if (result.IsSuccess) {
                            _this.prev('input.employeeId').val(result.teamMemberId);
                            _this.parent().next().find('input.employeeName').val(result.teamMemberName);
                            //console.log($this.parent().parent());
                            //_this.parent().parent().find('input.employeedept').val(result.memDept);
                            //_this.parent().parent().find('input.employeeOrg').val(result.memOrg);
                            //_this.parent().parent().find('input.employeeBr').val(result.memBr);
                            //_this.parent().parent().find('input.employeeCam').val(result.memCam);
                        }
                        else {
                            _this.prev('input.employeeId').val(0);
                            _this.parent().next().find('input.employeeName').val("");
                        }
                        //$.unblockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function () {
                        $.unblockUI();
                    }
                });
            //}
        }
    });
    
});

function dataTableRender() {
    var pin = $("#pin").val();
    var dateFrom = $("#dateFrom").val();
    var dateTo = $("#dateTo").val();

    var displayNo = 100;
    if ($('#pageSize').val() != "" && /^\d+$/.test($('#pageSize').val())) {
        displayNo = parseInt($('#pageSize').val());
    } else {
        $('#pageSize').val(displayNo);
    }
    $('#MentorAttendanceApplicationRecentHistory').dataTable({
        destroy: true,
        "processing": true,
        searching: false,
        serverSide: true,
        "scrollX": true,
        "scrollCollapse": true,
        "scrollY": "60vh",
        "bLengthChange": false,
        "iDisplayLength": displayNo,
        //"pageLength": displayNo,
        //stateSave: true,
        order: [[0, "Asc"]],
        "aoColumnDefs": [{ "bSortable": false, "aTargets": ['_all'] }],
        fixedColumns: {
            leftColumns: 3
        },
        ajax: {
            //url: '@Url.Action("MentorAttendanceAdjustmentRecentHistory", "AttendanceAdjustment")',
            url: $("body").attr("data-project-root") + "Hr/AttendanceAdjustment/MentorAttendanceAdjustmentRecentHistory",
            type: 'POST',
            data: function (d) {
                d.pin = pin;
                d.dateFrom = dateFrom;
                d.dateTo = dateTo;
            },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (response) {
                ShowErrorMessage("data load error. Please try again.");
            }
        }
    });
}

function checkIndividualRowValidity(row) {
    var error = 0;
    var date = row.find(".datepicker").val();
    var reasonForAdjustment = row.find(".reasonForAdjustment").val();
    var timeFrom = row.find(".timeFrom").val();
    var timeTo = row.find(".timeTo").val();
    //alert(date + "//" + reasonForAdjustment + "//" + timeFrom + "//" + timeTo);
    if (date == "") {
        error = error + 1;
        row.find(".datepicker").addClass("highlight");
    } else {
        row.find(".datepicker").removeClass("highlight");
    }
    if (reasonForAdjustment == "") {
        error = error + 1;
        row.find(".reasonForAdjustment").addClass("highlight");
    } else {
        row.find(".reasonForAdjustment").removeClass("highlight");
    }
    if (timeFrom == "") {
        error = error + 1;
        row.find(".timeFrom").addClass("highlight");
    } else {
        row.find(".timeFrom").removeClass("highlight");
    }
    if (timeTo == "") {
        error = error + 1;
        row.find(".timeTo").addClass("highlight");
    } else {
        row.find(".timeTo").removeClass("highlight");
    }
    if (error == 0) {
        row.removeClass("danger");
        row.addClass("info");
    } else {
        row.addClass("danger");
        row.removeClass("info");
    }
    // alert(error);
    return error;
}

function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (JSON.stringify(list[i]) === JSON.stringify(obj)) {
            return true;
        }
    }
    return false;
}

function notValidRow(row) {
    try {
        row.find(".datepicker").addClass("highlight");
        row.find(".reasonForAdjustment").addClass("highlight");
        row.find(".timeFrom").addClass("highlight");
        row.find(".timeTo").addClass("highlight");
        row.find(".timeTo").addClass("highlight");
        return 1;
    } catch (e) {
        return 0;
    }

}

function setAttendanceAdjustmentRowCount() {
    var attendanceAdjustmentCount = $('#mentorAttendanceAdjustmentTableList >tbody >tr').length;
    $("#adjustmentCount").html(attendanceAdjustmentCount);
}