using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class SupplierBankDetailFactory : ObjectFactoryBase<SupplierBankDetail>
   {

       private readonly ISupplierBankDetailService _supplierBankDetailService;

       public SupplierBankDetailFactory(IObjectFactoryBase caller, ISession session): base(caller, session){

       _supplierBankDetailService = new SupplierBankDetailService(session);

       }

       #region Create 

       public SupplierBankDetailFactory Create(){

       Object = null;
       SingleObjectList.Add(Object);
       return this;
       }

       public SupplierBankDetailFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public SupplierBankDetailFactory Persist()       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _supplierBankDetailService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x =>{if (x.Id == 0)_supplierBankDetailService.SaveOrUpdate(x);});

          }
          return this;
       }

       public SupplierBankDetailFactory CreateMore(int num){

       for (int i = 0; i < num; i++){
       var obj = new SupplierBankDetail(){};
       ObjectList.Add(obj);}
       return this;}

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public SupplierBankDetailFactory CleanUp()
       {

          DeleteAll();
          return this;
       }

       #endregion

    }
}
