using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.UI.WebControls;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Engine;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.UInventory
{

    public interface IGoodsIssueDao : IBaseDao<GoodsIssue, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        GoodsIssue GetGoodsIssueByRequisitionSlipId(long requisitionId);

        #endregion

        #region List Loading Function

        IList<GoodsIssueDto> LoadGoodsIssue(int start, int length, string orderBy, string orderDir, List<long> branchIdList = null, string item = "", DateTime? issuedDate = null, string goodsIssueNo = "", int? status = null);
        int GetGoodsIssueCount(List<long> branchIdList = null, string item = "", DateTime? issuedDate = null, string goodsIssueNo = "", int? status = null);
        int GetCurrentStockQuantity(long branchId, long itemId, int? status = null);
        IList<dynamic> LoadGoodsIssueBranchWiseReport(int start, int length, List<long> organizationList, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, string branchNameStr, List<string> programsession, bool isOnlyNa = false);
        IList<dynamic> LoadGoodsIssueProgramWiseReport(int start, int length, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, string programSessionNameStr, List<string> programsession, bool isOnlyNa = false);
        int LoadGoodsIssueBranchWiseReportCount(List<long> organizationList, List<long> authBranchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, string branchNameStr, List<string> programsession, bool isOnlyNa = false);
        int LoadGoodsIssueProgramWiseReportCount(List<long> organizationList, List<long> authBranchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, string programSessionNameStr, List<string> programsession, bool isOnlyNa = false);
        
        #endregion

        #region Others Function

        string GetGoodsIssueNo(long branchId);
        GoodsIssue GetGoodsIssue(string goodsIssueNo);

        #endregion

    }

    public class GoodsIssueDao : BaseDao<GoodsIssue, long>, IGoodsIssueDao
    {

        #region Properties & Object & Initialization

        public static IResultTransformer ExpandoObjects;

        #endregion

        #region Operational Function

        public GoodsIssueDao()
        {
            ExpandoObjects = new ExpandObjectResultSetTransformers();
        }

        #endregion

        #region Single Instances Loading Function

        public GoodsIssue GetGoodsIssue(string goodsIssueNo)
        {
            var goodsIssue =
                Session.QueryOver<GoodsIssue>()
                    .Where(x => x.GoodsIssueNo == goodsIssueNo)
                    .Take(1)
                    .List<GoodsIssue>()
                    .FirstOrDefault();
            return goodsIssue;
        }

        #endregion

        #region List Loading Function

        public GoodsIssue GetGoodsIssueByRequisitionSlipId(long requisitionId)
        {
            return
                Session.QueryOver<GoodsIssue>()
                    .Where(x => x.Requisition.Id == requisitionId)
                    .WhereNot(x => x.Status == GoodsIssue.EntityStatus.Delete)
                    .SingleOrDefault<GoodsIssue>();
        }
        public IList<GoodsIssueDto> LoadGoodsIssue(int start, int length, string orderBy, string orderDir, List<long> branchIdList = null, string item = "", DateTime? issuedDate = null, string goodsIssueNo = "", int? goodsIssueStatus = null)
        {
            string query = CommonQueryString(orderBy, orderDir, branchIdList, item, issuedDate, goodsIssueNo, goodsIssueStatus);
            IQuery iQuery = Session.CreateSQLQuery(query);
            if (length > 0)
            {
                iQuery.SetFirstResult(start).SetMaxResults(length);
            }
            iQuery.SetResultTransformer(Transformers.AliasToBean<GoodsIssueDto>());
            var list = iQuery.List<GoodsIssueDto>().ToList();
            return list;
        }

        #endregion

        #region Others Function

        public int GetGoodsIssueCount(List<long> branchIdList = null, string item = "", DateTime? issuedDate = null, string goodsIssueNo = "", int? goodsIssueStatus = null)
        {
            string query = "select count(DISTINCT Id) from (" + CommonQueryString("", "", branchIdList, item, issuedDate, goodsIssueNo, goodsIssueStatus) + ")x";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }
        public int GetCurrentStockQuantity(long branchId, long itemId, int? stockQuantityStaus = null)
        {
            var result =
                Session.QueryOver<CurrentStockSummary>().Where(x => (x.Item.Id == itemId && x.Branch.Id == branchId)).Select(x => x.StockQuantity).List<int>().Sum(x=>x);
            return result;
        }
        public string CommonQueryString(string orderBy = "", string orderDir = "", List<long> branchIdList = null, string item = "", DateTime? issuedDate = null, string goodsIssueNo = "", int? goodsIssueStatus = null)
        {
            string whereClause = "";
            if (branchIdList != null && branchIdList.Count > 0) whereClause += " and b.Id in(" + string.Join(", ", branchIdList) + ")";
            if (!string.IsNullOrEmpty(item))
            {
                whereClause += " and i.Name LIKE '%" + item + "%'";
            }
            if (issuedDate != null)
            {
                whereClause += " and gi.CreationDate >='" + issuedDate + "' and gi.CreationDate <'" + issuedDate.Value.AddDays(1) + "'";
            }
            if (!string.IsNullOrEmpty(goodsIssueNo))
            {
                whereClause += " and gi.GoodsIssueNo LIKE '%" + goodsIssueNo + "%'";
            }
            if (goodsIssueStatus != null)
            {
                whereClause += " and gi.status=" + goodsIssueStatus;
            }
            string query = @"select gi.Id, o.ShortName Organization, b.Name Branch, i.Name ItemName
		                        ,gi.CreationDate CreateDate, gi.GoodsIssueNo, gi.status 'Status' from UINV_GoodsIssue gi 
		                        inner join UINV_GoodsIssueDetails gis on gis.GoodsIssueId=gi.Id and gis.status = "+ GoodsIssueDetails.EntityStatus.Active +@"
		                        inner join Branch b on b.Id=gi.BranchId and b.status = "+ Branch.EntityStatus.Active +@"
		                        inner join Organization o on b.OrganizationId=o.Id and o.status = "+ Organization.EntityStatus.Active +@"
		                        inner join UINV_Item i on gis.ItemId=i.Id and i.status = "+ Item.EntityStatus.Active +@"
		                        where gi.[status]=1 " + whereClause;

            return query;
        }
        public int LoadGoodsIssueBranchWiseReportCount(List<long> organizationList, List<long> authBranchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, 
            List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, string branchNameStr, List<string> programsession,bool isOnlyNa = false)
        {
            string query = "SELECT COUNT(piv.itemgroup) from ( ";
            query += GenerateGoodIssueBranchWiseQuery(organizationList, authBranchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, dateFrom, dateTo, branchNameStr, programsession,isOnlyNa);
            query += @") as piv ";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }
        public int LoadGoodsIssueProgramWiseReportCount(List<long> organizationList, List<long> authBranchIdList, int goodsIssueType,
            List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType,
            DateTime dateFrom, DateTime dateTo, string programSessionNameStr, List<string> programsession, bool isOnlyNa = false)
        {
            string query = "SELECT COUNT(piv.[Item]) from ( ";
            query += GenerateGoodIssueProgramWiseQuery(authBranchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, dateFrom, dateTo, programSessionNameStr, programsession,isOnlyNa);
            query += @") as piv "; ;
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }
        public IList<dynamic> LoadGoodsIssueBranchWiseReport(int start, int length, List<long> organizationList, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList,
            List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, string branchNameStr, List<string> programsession, bool isOnlyNa = false)
        {
            string[] brAry = branchNameStr.Split(',');

            string query = @" SELECT piv.* , ";
            int index = 0;
            foreach (var brName in brAry)
            {
                if (index == 0)
                    query += @" ((case when piv." + brName + " is null then 0 else piv." + brName + " end) +";
                else
                    query += @" (case when piv." + brName + " is null then 0 else piv." + brName + " end) +";
                index++;
                if (index == brAry.Length)
                {
                    query += ") ";
                }
            }
            query = query.Trim().Remove(query.Length - 4,1);
            query += @" as Total  
                              FROM ( ";
            query += GenerateGoodIssueBranchWiseQuery(organizationList, branchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, dateFrom, dateTo, branchNameStr, programsession,isOnlyNa);
            query += @") as piv ";
            IQuery queryI = Session.CreateSQLQuery(query);
            IList<dynamic> result = DynamicList(queryI);
            return result;

        }
        public IList<dynamic> LoadGoodsIssueProgramWiseReport(int start, int length, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList,
            List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo,
            string programSessionNameStr, List<string> programsession, bool isOnlyNa = false)
        {
           string[] prSessionAry = programSessionNameStr.Split(',');

            string query = @" SELECT piv.* , ";
            int index = 0;
            foreach (var psObj in prSessionAry)
            {
                if (psObj != "")
                {
                    if (index == 0)
                        query += @" ((case when piv." + psObj + " is null then 0 else piv." + psObj + " end) +";
                    else
                        query += @" (case when piv." + psObj + " is null then 0 else piv." + psObj + " end) +";
                    
                }
                index++;
                if (index == prSessionAry.Length)
                {
                    query += ") ";
                }
            }
            query = query.Trim().Remove(query.Length - 4,1);
            query += @" as Total  
                              FROM ( ";
            query += GenerateGoodIssueProgramWiseQuery(branchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, dateFrom, dateTo, programSessionNameStr, programsession);
            query += @") as piv ";
            IQuery queryI = Session.CreateSQLQuery(query);
            IList<dynamic> result = DynamicList(queryI);
            return result;
        }
        private string GenerateGoodIssueBranchWiseQuery(List<long> organizationList, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, string branchNameStr, List<string> programsessionList, bool isOnlyNa = false)
        {
            string where = "";
            if (programsessionList != null && programsessionList.Any() && !programsessionList.Contains(SelectionType.SelelectAll.ToString()))
            {
                where += " WHERE";
                if (programsessionList.Contains("-101") && isOnlyNa)
                {
                    programsessionList = programsessionList.Skip(1).Take(programsessionList.Count - 1).ToList();
                    where += " (PR.Id is null and SE.Id is null ) or ";
                }
                else if (programsessionList.Contains("-101"))
                {
                    programsessionList = programsessionList.Skip(1).Take(programsessionList.Count - 1).ToList();
                    where += " (PR.Id is null and SE.Id is null ) or ";
                }
                if (!isOnlyNa)
                {
                    foreach (var programsession in programsessionList)
                    {
                        var programId = programsession.Split(new string[] { "::" }, StringSplitOptions.None)[0];
                        var sessionId = programsession.Split(new string[] { "::" }, StringSplitOptions.None)[1];
                        where += "(PR.Id=" + programId + " and SE.Id=" + sessionId + ") or ";
                    }
                }
                where = where.Trim();
                where = where.Remove(where.Length - 2);
            }
            string query = @" SELECT ig.Name itemgroup, i.Name item, ISNULL(PR.ShortName + ' ' + SE.Name, 'N/A') AS [Program & Session], b.Name branch, gid.IssuedQuantity Quantity
                                FROM UINV_GoodsIssue gi INNER JOIN UINV_GoodsIssueDetails gid 
                                ON gi.Id = gid.GoodsIssueId ";

            if (goodsIssueType == (int)GoodsIssueType.RequistionBasedIssue)
            {
                query += @"AND RequisitionId IS NOT NULL ";
            }
            if (goodsIssueType == (int)GoodsIssueType.DirectGoodsIssue)
            {
                query += @"AND RequisitionId IS NULL ";
            }
            if (!purposeList.Contains(SelectionType.SelelectAll))
            {
                if (purposeList.Contains(-101))
                {
                    query += @" AND (gid.Purpose IS NULL OR ";
                }
                if (query.Contains("OR"))
                {
                    query += @" gid.Purpose IN (" + string.Join(", ", purposeList) + ") ) ";
                }
                else { query += @" AND gid.Purpose IN (" + string.Join(", ", purposeList) + ") "; }
            }
            if (itemGroupIdList.Count > 0 && !itemGroupIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND i.ItemGroupId IN (" + string.Join(", ", itemGroupIdList) + ") ";
            }
            if (itemIdList.Count > 0 && !itemIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND gid.ItemId IN (" + string.Join(", ", itemIdList) + ") ";
            }
            query += @" INNER JOIN [dbo].[Branch] AS b ON gi.BranchId = b.Id ";

            query += @" LEFT JOIN [dbo].[Program] AS PR ON PR.Id = gid.ProgramId ";
            query += @" LEFT JOIN [dbo].[Session] AS SE ON SE.Id = gid.SessionId ";
            query += @" INNER JOIN [dbo].[UINV_Item] AS i ON i.Id = gid.ItemId ";
            query += @" INNER JOIN [dbo].[UINV_ItemGroup] AS IG ON IG.Id = i.ItemGroupId ";
            if (itemGroupIdList.Count > 0 && !itemGroupIdList.Contains(SelectionType.SelelectAll))
                query += @" AND IG.ID IN (" + string.Join(", ", itemGroupIdList) + ") ";
            query += @" INNER JOIN [dbo].[Branch] AS BR ON BR.ID = gi.BranchId ";
            if (branchIdList.Count > 0 && !branchIdList.Contains(SelectionType.SelelectAll))
                query += @" AND BR.Id IN (" + string.Join(", ", branchIdList) + ") ";
            query += where;
            query += " and gi.CreationDate>='" + dateFrom.Date + "'";
            query += " and gi.CreationDate < '" + dateTo.AddDays(1).Date + "'";
            query += @") AS FINAL
	                             PIVOT
	                             (
	                             SUM(Quantity)
	                             FOR Branch IN( " + branchNameStr;
            query += @" )";
            return query;
        }
        private string GenerateGoodIssueProgramWiseQuery(List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, string programSessionNameStr, List<string> programsessionList, bool isOnlyNa = false)
        {
            string where = "";
            if (programsessionList != null && programsessionList.Any() && !programsessionList.Contains(SelectionType.SelelectAll.ToString())
                )
            {
                where += " WHERE";
                if (programsessionList.Contains("-101") && isOnlyNa)
                {
                    programsessionList = programsessionList.Skip(1).Take(programsessionList.Count - 1).ToList();
                    where += " (PR.Id is null and SE.Id is null ) or ";
                }
                else if (programsessionList.Contains("-101"))
                {
                    programsessionList = programsessionList.Skip(1).Take(programsessionList.Count - 1).ToList();
                    where += " (PR.Id is null and SE.Id is null ) or ";
                }
                if (!isOnlyNa)
                {
                    foreach (var programsession in programsessionList)
                    {
                        var programId = programsession.Split(new string[] { "::" }, StringSplitOptions.None)[0];
                        var sessionId = programsession.Split(new string[] { "::" }, StringSplitOptions.None)[1];
                        where += "(PR.Id=" + programId + " and SE.Id=" + sessionId + ") or ";
                    }
                }
                where = where.Trim();
                where = where.Remove(where.Length - 2);
            }

            string query = @" select i.Name item, isnull(p.ShortName+' & '+s.Name,'N/A') programSession, gid.IssuedQuantity Quantity
						from UINV_GoodsIssue gi inner join UINV_GoodsIssueDetails gid 
						on gi.Id = gid.GoodsIssueId
						left join Program p on gid.ProgramId=p.Id
						left join Session s on gid.SessionId=s.Id ";
            if (goodsIssueType == (int)GoodsIssueType.RequistionBasedIssue)
            {
                query += @"AND RequisitionId IS NOT NULL ";
            }
            else if (goodsIssueType == (int)GoodsIssueType.DirectGoodsIssue)
            {
                query += @"AND RequisitionId IS NULL ";
            }
            query += @" INNER JOIN [dbo].[UINV_Item] AS i ON i.Id = gid.ItemId ";
            if (!purposeList.Contains(SelectionType.SelelectAll))
            {
                if (purposeList.Contains(-101))
                {
                    query += @" AND (gid.Purpose IS NULL OR ";
                }
                if (query.Contains("OR"))
                {
                    query += @" gid.Purpose IN (" + string.Join(", ", purposeList) + ") ) ";
                }
                else { query += @" AND gid.Purpose IN (" + string.Join(", ", purposeList) + ") "; }
            }
            if (programIdList != null && programIdList.Count > 0)
            {
                query += @" AND gid.ProgramId IN (" + string.Join(", ", programIdList) + ") ";
            }
            if (sessionIdList != null && sessionIdList.Count > 0)
            {
                query += @" AND gid.SessionId IN (" + string.Join(", ", sessionIdList) + ") ";
            }
            if (itemGroupIdList.Count > 0 && !itemGroupIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND i.ItemGroupId IN (" + string.Join(", ", itemGroupIdList) + ") ";
            }
            if (itemIdList.Count > 0 && !itemIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND gid.ItemId IN (" + string.Join(", ", itemIdList) + ") ";
            }

            query += @" LEFT JOIN [dbo].[Program] AS PR ON PR.Id = gid.ProgramId
                        LEFT JOIN [dbo].[Session] AS SE ON SE.Id = gid.SessionId
                              INNER JOIN [dbo].[UINV_Item] AS IT ON IT.Id = gid.ItemId ";

            query += @" INNER JOIN [dbo].[UINV_ItemGroup] AS IG ON IG.Id = IT.ItemGroupId ";

            query += @" INNER JOIN [dbo].[Branch] AS b ON gi.BranchId = b.Id ";
            if (branchIdList.Count > 0 && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND b.Id IN (" + string.Join(", ", branchIdList) + ") ";
            }
            query += where;
            query += " and gi.CreationDate>='" + dateFrom.Date + "'";
            query += " and gi.CreationDate < '" + dateTo.AddDays(1).Date + "'";
            query += @") AS FINAL
	                             PIVOT
	                             (
	                             SUM(Quantity)
	                             FOR ProgramSession IN( " + programSessionNameStr.TrimEnd(',');
            query += @" )";
            return query;
        }
        #endregion

        #region Helper Function
        public IList<dynamic> DynamicList(IQuery query)
        {
            var result = query.SetResultTransformer(ExpandoObjects)
                        .List<dynamic>();
            return result;
        }

        public string GetGoodsIssueNo(long branchId)
        {
            var obj =
                    Session.QueryOver<GoodsIssue>()
                        .Where(x => x.Branch.Id == branchId)
                        .List<GoodsIssue>()
                        .OrderByDescending(x => x.Id)
                        .Take(1)
                        .SingleOrDefault();
            return obj == null ? "" : obj.GoodsIssueNo;
        }

        #endregion

       
    }
}
