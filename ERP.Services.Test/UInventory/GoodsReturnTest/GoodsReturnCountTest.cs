﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.GoodsReturnTest
{
    public interface IGoodsReturnCountTest
    {
        void WorkOrder_Count_Should_Be_Greater_Than_Zero();
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReturn")]
    public class GoodsReturnCountTest : GoodsReturnBaseTest, IGoodsReturnCountTest
    {
        #region Basic Test

        [Fact]
        public void WorkOrder_Count_Should_Be_Greater_Than_Zero()
        {
            _goodsReturnFactory.CreateMore(5).Persist();
            var goodsReturn = _goodsReturnFactory.GetLastCreatedObjectList()[0];
            var branch = goodsReturn.GoodsReceive.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var usermenu = BuildUserMenu(branch.Organization, goodsReturn.GoodsReceive.GoodsReceiveDetails[0].Program, branch);
            var count = _goodsReturnService.GetGoodsReturnCount(usermenu, organizationId, branchId, "", null, null);
            Assert.True(count >= 5);
        }

        #endregion
    }
}
