﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Areas.Student.Models
{
    public class StudentTransferSlip
    {

        public string PrnNo { get; set; }

        public string TsN { get; set; }
        public string StudentName { get; set; }

        public string MobileNo { get; set; }

        public string Courses { get; set; }

        public string FromBranch { get; set; }
        public string FromCampus { get; set; }


        public string FromBatchDays { get; set; }
        public string FromBatchTime { get; set; }

        public string FromBatchName { get; set; }

        public string Branch { get; set; }
        public string Campus { get; set; }


        public string BatchDays { get; set; }
        public string BatchTime { get; set; }

        public string BatchName { get; set; }

        public string ProgramName { get; set; }
        public string SessionName { get; set; }

        public string RollNo { get; set; }
        public string CreateDate { get; set; }

        public string CampusAddress { get; set; }

        public string AuthorityFullName { get; set; }
        public string PhoneNo { get; set; }

        public virtual StudentProgram StudentProgram { get; set; }



    }

   
}