﻿using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MaterialTest
{
    public interface IMaterialUpdateTest
    {
        #region basic test
        [Fact]
        void Update_Should_Return_Null_Exception();

        [Fact]
        void Should_Update_Successfully();

        [Fact]
        void Update_Should_Check_Duplicate_Entry(); 
        #endregion

        #region business logic test
        [Theory]
        [MemberData("MaterialIndex", MemberType = typeof(MaterialSaveTest))]
        void Save_Should_Check_Invalid_Entry(int i); 
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Material")]
    public class MaterialUpdateTest : MaterialBaseTest, IMaterialUpdateTest
    {
        #region Test Related Properties
        public static IEnumerable<object[]> MaterialIndex
        {
            get
            {
                var indexList = new List<object[]>();
                for (int i = 0; i < 5; i++)
                    indexList.Add(new object[] { i });
                return indexList;
            }
        }
        #endregion
       
        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var materialFactory =
                   _materialFactory.Create()
                       .Persist();
            Assert.Throws<NullObjectException>(() => _materialService.MaterialSaveOrUpdate(null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var materialFactory =
                   _materialFactory.Create()
                       .Persist();
            var material = materialFactory.Object;
            material.Name = material.Name + " gfghfghfhg";
            material.ShortName = material.ShortName + " gfghfghfhg";
            var success = _materialService.MaterialSaveOrUpdate(new List<Material>() { material });
            Assert.True(success);
        }
       
        [Fact]
        public void Should_Update_Rank_Successfully()
        {
            var materialFactory =
                _materialFactory.CreateMore(2)
                    .Persist();
            var material1 = materialFactory.GetLastCreatedObjectList()[0];
            var material2 = materialFactory.GetLastCreatedObjectList()[1];
            var tempRank = material1.Rank;
            material1.Rank = material2.Rank;
            material2.Rank = tempRank;
            _materialService.UpdateRank(material1, material2);
            Assert.True(material1.Rank > material2.Rank);
        }
        #endregion

        #region Business logic test
        [Fact]
        public void Update_Should_Check_Duplicate_Entry()
        {
            var materialFactory =
                _materialFactory.CreateMore(2)
                    .Persist();
            var material1 = materialFactory.GetLastCreatedObjectList()[0];
            var material2 = materialFactory.GetLastCreatedObjectList()[1];
            material2.Name = material1.Name;
            material2.ShortName = material1.ShortName;
            Assert.Throws<DuplicateEntryException>(() => _materialService.MaterialSaveOrUpdate(new List<Material>() { material2 }));
        }

        [Theory]
        [MemberData("MaterialIndex", MemberType = typeof(MaterialSaveTest))]
        public void Save_Should_Check_Invalid_Entry(int i)
        {
            MaterialFactory materialFactory =
                _materialFactory.Create().Persist();
            var oldMaterial = materialFactory.Object;
            if (i == 0)
            {
                oldMaterial.Name = "";
            }
            if (i == 1)
            {
                oldMaterial.ShortName = "";
            }
            if (i == 2)
            {
                oldMaterial.Program = null;
            }
            if (i == 3)
            {
                oldMaterial.Session = null;
            }
            if (i == 4)
            {
                oldMaterial.MaterialType = null;
            }
            Assert.Throws<InvalidDataException>(() => _materialService.MaterialSaveOrUpdate(new List<Material>() { oldMaterial }));
        }
        #endregion
    }
}
