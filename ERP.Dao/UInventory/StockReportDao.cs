using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{

    public interface IStockReportDao : IBaseDao<Supplier, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        IList<dynamic> StockSummaryReport(int start, int length, string orderBy, string orderDir
                                            , long branchId, long[] itemGroupIds, long[] itemIds
                                            , int[] purposeIds, string[] programSession
                                            , DateTime dateFrom, DateTime dateTo);

        IList<dynamic> StockSummaryDetailsReport(int start, int length, long branchId, long[] itemIds
                                            , int[] purposeIds, string[] programSession, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Others Function

        int StockSummaryReportRowCount(long branchId, long[] itemGroupIds, long[] itemIds, int[] purposeIds, string[] programSession, DateTime dateFrom, DateTime dateTo);

        int StockSummaryDetailsReportRowCount(long branchId, long[] itemIds, int[] purposeIds, string[] programSession, DateTime dateFrom, DateTime dateTo);

        #endregion
    }

    public class StockReportDao : BaseDao<Supplier, long>, IStockReportDao
    {

        #region Properties & Object & Initialization

        public static IResultTransformer ExpandoObject;
        const string DefaultCls = "_dCls_";

        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function



        #endregion

        #region List Loading Function

        public IList<dynamic> StockSummaryReport(int start, int length, string orderBy, string orderDir, long branchId, long[] itemGroupIds,
            long[] itemIds, int[] purposeIds, string[] programSession, DateTime dateFrom, DateTime dateTo)
        {
            var query = GenerateStockSummaryReportQuery(start, length, orderBy, orderDir, branchId, purposeIds, programSession, itemGroupIds, itemIds, dateFrom, dateTo);
            IList<dynamic> list = GenereteReportByDynamicQuery(query);
            return list;
        }

        public IList<dynamic> StockSummaryDetailsReport(int start, int length, long branchId, long[] itemIds
                                                        , int[] purposeIds, string[] programSession, DateTime dateFrom, DateTime dateTo)
        {
            var query = GenerateStockScheduleDetailsReportQuery(start, length, branchId, purposeIds, programSession, itemIds, dateFrom, dateTo);
            IList<dynamic> list = GenereteReportByDynamicQuery(query);
            return list;
        }

        #endregion

        #region Others Function
        public int StockSummaryReportRowCount(long branchId, long[] itemGroupIds, long[] itemIds, int[] purposeIds,
            string[] programSession, DateTime dateFrom, DateTime dateTo)
        {
            var query = GenerateStockSummaryReportQuery(0, 0, "", "", branchId, purposeIds, programSession, itemGroupIds, itemIds, dateFrom, dateTo, true);
            var q = Session.CreateSQLQuery(query);
            q.SetTimeout(2700);
            var count = q.UniqueResult<int>();
            return count;
        }

        public int StockSummaryDetailsReportRowCount(long branchId, long[] itemIds, int[] purposeIds,
                                                    string[] programSession, DateTime dateFrom, DateTime dateTo)
        {
            var query = GenerateStockScheduleDetailsReportQuery(0, 0, branchId, purposeIds, programSession, itemIds, dateFrom, dateTo, true);
            var q = Session.CreateSQLQuery(query);
            q.SetTimeout(2700);
            var count = q.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function

        public IList<dynamic> GenereteReportByDynamicQuery(string query)
        {
            IQuery iQuery = Session.CreateSQLQuery(query);
            IList<dynamic> result = DynamicList(iQuery);
            return result;
        }
        public IList<dynamic> DynamicList(IQuery query)
        {
            var result = query.SetResultTransformer(ExpandoObject)
                        .List<dynamic>();
            return result;
        }

        #region Stock Schedule Report

        private static string GenerateStockSummaryReportQuery(int start, int length, string orderBy, string orderDir, long branchId
                                                        , int[] purposeIds, string[] programSession, long[] itemGroupIds, IEnumerable<long> itemIds
                                                        , DateTime dateFrom, DateTime dateTo, bool countQuery = false)
        {
            #region Where Clause

            string purposeCls = purposeIds.Any() && purposeIds[0] > 0 ? GeneratePurposeClause(purposeIds, DefaultCls) : String.Empty;
            string programSessionCls = programSession.Any() && programSession[0] != "" ? GenerateProgramSessionClause(programSession, DefaultCls) : String.Empty;

            string dateTimeStart = "'" + dateFrom.ToString("yyyy-MM-dd") + " 00:00:00.000'";
            string dateTimeEnd = "'" + dateTo.ToString("yyyy-MM-dd") + " 23:59:59.999'";

            string itemIdsStr = string.Join(", ", itemIds);

            #endregion
            string query = "";


            string mainQuery = GetStockScheduleInnerSelectQuery(dateTimeStart, dateTimeEnd, itemIdsStr, purposeCls, programSessionCls, branchId);
            const string selectQuery = @"   SELECT 
                                                itmG.Name AS ItemGroup, itm.Name AS Item
                                                , CASE WHEN query.PRId IS NULL THEN 'N/A' ELSE (prm.ShortName + ' ' + ses.Name) END AS ProgramSession
                                                , OpeningQuantity, OpeningRate, CAST((OpeningQuantity*OpeningRate) AS DECIMAL(18, 2)) AS OpeningValue
                                                , ReceivedQuantity,  ReceivedRate, CAST((ReceivedQuantity*ReceivedRate) AS DECIMAL(18, 2)) AS ReceivedValue
                                                , query.IssuedQuantity
                                                , CAST((CASE WHEN IssuedQuantity = 0 THEN 0
                                                        WHEN OpeningRate > 0 AND ReceivedRate = 0 THEN OpeningRate
		                                                WHEN OpeningRate = 0 AND ReceivedRate > 0 THEN ReceivedRate
		                                                ELSE (OpeningRate + ReceivedRate)/2 END) AS DECIMAL(18,2)) AS IssuedRate

                                                , CAST((CASE WHEN IssuedQuantity IS NULL  THEN 0 ELSE IssuedQuantity END)*
		                                                (CASE WHEN IssuedQuantity = 0 THEN 0
                                                            WHEN OpeningRate > 0 AND ReceivedRate = 0 THEN OpeningRate
		                                                    WHEN OpeningRate = 0 AND ReceivedRate > 0 THEN ReceivedRate
		                                                    ELSE (OpeningRate + ReceivedRate)/2 END) AS DECIMAL(18, 2)) AS IssuedValue
                                                , ((OpeningQuantity + ReceivedQuantity) - IssuedQuantity) AS ClosingQuantity
                                                , CAST((CASE WHEN OpeningRate > 0 AND ReceivedRate = 0 THEN OpeningRate
		                                                WHEN OpeningRate = 0 AND ReceivedRate > 0 THEN ReceivedRate
		                                                ELSE (OpeningRate + ReceivedRate)/2 END) AS DECIMAL(18,2)) AS ClosingRate
                                                , CAST(((OpeningQuantity + ReceivedQuantity) - IssuedQuantity)*
		                                                (CASE WHEN OpeningRate > 0 AND ReceivedRate = 0 THEN OpeningRate
		                                                WHEN OpeningRate = 0 AND ReceivedRate > 0 THEN ReceivedRate
		                                                ELSE (OpeningRate + ReceivedRate)/2 END) AS DECIMAL(18, 2)) AS ClosingValue";

            string relationQuery = @" INNER JOIN UINV_Item itm ON query.ItemId = itm.Id
                                                INNER JOIN UINV_ItemGroup itmG ON itmG.Id = itm.ItemGroupId
                                                LEFT OUTER JOIN Program prm ON query.PRID = prm.Id
                                                LEFT OUTER JOIN Session ses ON query.SESSID = ses.Id ";
            relationQuery  += GenerateProgramSessionClause(programSession, new List<string>() { "prm", "ses" });
            if (countQuery)
            {
                query = @"SELECT COUNT(CountQuery.Item) FROM (";
                query += selectQuery + mainQuery + relationQuery + @") AS CountQuery";
            }
            else
            {
                string fQuery = " ORDER BY ItemGroup DESC OFFSET " + start + " ROWS FETCH FIRST " + length + " ROWS ONLY";
                query = selectQuery + mainQuery + relationQuery + fQuery;
            }
            return query;
        }

        private static string GetStockScheduleInnerSelectQuery(string dateTimeStart, string dateTimeEnd, string itemIdsStr, string purposeCls, string programSessionCls, long branchId)
        {
            string qq = @"  FROM 
                            (
	                            SELECT 
		                            mainQuery.PRID, mainQuery.SESSID, mainQuery.ItemId
		                            , sum(mainQuery.OpeningQuantity) AS OpeningQuantity
		                            , sum(mainQuery.OpeningRate) AS OpeningRate
		                            , sum(mainQuery.ReceivedQuantity) AS ReceivedQuantity
		                            , sum(mainQuery.ReceivedRate) AS ReceivedRate
		                            , sum(mainQuery.IssuedQuantity) AS IssuedQuantity
	                            FROM
	                            (
		                            SELECT 
			                            OpeningIn.BranchId, OpeningIn.PRID, OpeningIn.SESSID, OpeningIn.ItemId, 
			                            CASE WHEN OpeningOut.IssuedQuantity IS NULL AND OpeningIn.ReceivedQuantity IS NULL THEN 0
				                                WHEN OpeningOut.IssuedQuantity IS NULL AND OpeningIn.ReceivedQuantity IS NOT NULL THEN OpeningIn.ReceivedQuantity
				                                ELSE (OpeningIn.ReceivedQuantity - OpeningOut.IssuedQuantity) END AS OpeningQuantity,
			                            dbo.getrate(" + dateTimeStart +
                        @", OpeningIn.ItemId, 1, 1, OpeningIn.PRID, OpeningIn.SESSID, OpeningIn.BranchId ) as OpeningRate
			                            , 0 AS ReceivedQuantity , 0.00 AS ReceivedRate, 0 AS IssuedQuantity
		                            FROM
		                            (
			                            SELECT 
				                            ReceivePart.BranchId, ReceivePart.PRID, ReceivePart.SESSID, ReceivePart.ItemId, SUM(ReceivePart.Quantity) AS ReceivedQuantity 
			                            FROM
			                            ( 
				                            SELECT 
					                            GR.BranchId, GRD.ProgramId as PRID, GRD.SessionId AS SESSID, GRD.ItemId AS ItemId, GRD.ReceivedQuantity AS Quantity, GRD.Rate AS UnitPrize 
				                            FROM UINV_GoodsReceive AS GR 
				                            INNER JOIN UINV_GoodsReceiveDetails AS GRD ON GR.ID = GRD.GoodsRecieveId 
				                            WHERE GR.BranchId = " + branchId + @" 
				                            AND GR.CreationDate < " + dateTimeStart;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                    AND GRD.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                    " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                    " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            qq += @"                        UNION ALL 
				                            SELECT 
						                            GT.BranchToId AS BranchId, GTD.ProgramId as PRID, GTD.SessionId AS SESSID, GTD.ItemId AS ItemId, GTD.TransferQuantity AS Quantity, 0.0 AS UnitPrize 
				                            FROM UINV_GoodsTransfer AS GT 
				                            INNER JOIN UINV_GoodsTransferDetails AS GTD ON GT.ID= GTD.GoodsTransferId 
				                            WHERE GT.BranchToId = " + branchId + @" AND GT.CreationDate < " + dateTimeStart;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                    AND GTD.ItemId IN(" + itemIdsStr + @") ";
            }
            qq += @"                        UNION ALL 

				                            SELECT 
					                            GIssueReq.BranchId , GIssue.PRID , GIssue.SESSID , GIssue.ItemId , GIssue.IssuedQuantity as Quantity , 0.0 AS UnitPrize 
				                            FROM
				                            ( 
					                            SELECT 
						                            GI.RequisitionId , GID.ProgramId as PRID , GID.SessionId as SESSID , GID.ItemId AS ItemId , GID.IssuedQuantity 
					                            FROM UINV_GoodsIssue AS GI INNER JOIN UINV_GoodsIssueDetails GID ON GI.Id = GID.GoodsIssueId 
					                            INNER JOIN UINV_Requisition RR ON GI.RequisitionId = RR.Id 
					                            WHERE 
						                            GI.RequisitionId 
							                            IN 
							                            (
								                            SELECT REQ.Id FROM UINV_Requisition AS REQ  
								                            INNER JOIN UINV_RequisitionDetails AS REQD ON REQ.Id = REQD.RequisitionId 
								                            WHERE GI.RequisitionId IS NOT NULL
                                                            AND GI.CreationDate < " + dateTimeStart + @" 
								                            AND REQ.BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                                    AND REQD.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                                    " + ReplaceClasueStringByGivenString(purposeCls, "REQD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                                    " + ReplaceClasueStringByGivenString(programSessionCls, "REQD");
            }
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                                    AND GID.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                                    " + ReplaceClasueStringByGivenString(purposeCls, "GID");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                                    " + ReplaceClasueStringByGivenString(programSessionCls, "GID");
            }
            qq += @"                                    )
				                            ) AS GIssue 
				                            LEFT JOIN 
				                            (SELECT Id, RR.BranchId FROM UINV_Requisition AS RR )
				                            AS GIssueReq
				                            ON GIssue.RequisitionId = GIssueReq.Id
			                            ) AS ReceivePart
			                            GROUP BY ReceivePart.BranchId, ReceivePart.PRID, ReceivePart.SESSID, ReceivePart.ItemId
		                            ) AS OpeningIn 
		                            LEFT JOIN
		                            (
			                            SELECT 
				                            IssuePart.BranchId , IssuePart.PRID , IssuePart.SESSID , IssuePart.ItemId , sum(IssuePart.Quantity) AS IssuedQuantity 
			                            FROM
			                            (
				                            SELECT 
					                            GIPr_RET.BranchId , GIPr_RET.PRID , GIPr_RET.SESSID , GIPr_GR.ItemId , GIPr_GR.ReturnedQuantity as Quantity 
				                            FROM 
				                            ( 
					                            SELECT 
						                            GRET.GoodsReceivedId , GRETD.ItemId , GRETD.ReturnedQuantity 
					                            FROM UINV_GoodsReturn AS GRET 
					                            INNER JOIN UINV_GoodsReturnDetails GRETD ON GRET.Id = GRETD.GoodsReturnId 
					                            WHERE GRET.CreationDate < " + dateTimeStart + @" 
					                            AND GRET.GoodsReceivedId 
										                            IN
										                            (
											                            SELECT GR.ID FROM UINV_GoodsReceive AS GR
											                            INNER JOIN UINV_GoodsReceiveDetails GRD ON GR.Id = GRD.GoodsRecieveId
											                            WHERE GR.BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                                                AND GRD.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                                                " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                                                " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            qq += @"                                                )";

            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @" AND GRETD.ItemId IN(" + itemIdsStr + @") ";
            }
            qq += @"
                    ) AS GIPr_GR 
				                            LEFT JOIN
				                            (
					                            SELECT 
						                            GR.BranchId , GR.ID , GRD.ProgramId as PRID , GRD.SessionId AS SESSID , GRD.ItemId , GRD.ReceivedQuantity AS GRQ 
					                            FROM UINV_GoodsReceive AS GR 
					                            INNER JOIN UINV_GoodsReceiveDetails AS GRD ON GR.ID= GRD.GoodsRecieveId 
					                            WHERE BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"
                         AND GRD.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                        " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                        " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            qq += @"                        ) AS GIPr_RET ON GIPr_GR.GoodsReceivedId = GIPr_RET.ID 
				
				                            UNION ALL 
				                            SELECT 
					                            GT.BranchFromId as BranchId , GTD.ProgramId as PRID , GTD.SessionId AS SESSID , GTD.ItemId , GTD.TransferQuantity AS Quantity 
				                            FROM UINV_GoodsTransfer AS GT 
				                            INNER JOIN UINV_GoodsTransferDetails AS GTD ON GT.ID= GTD.GoodsTransferId 
				                            WHERE GT.CreationDate < " + dateTimeStart + @" 
                                            AND GT.BranchFromId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                    AND GTD.ItemId IN(" + itemIdsStr + @") ";
            }
            qq += @"                        UNION ALL 
				                            SELECT 
					                            GI.BranchId , GID.ProgramId as PRID , GID.SessionId AS SESSID , GID.ItemId , GID.IssuedQuantity AS Quantity 
				                            FROM UINV_GoodsIssue AS GI 
				                            INNER JOIN UINV_GoodsIssueDetails AS GID ON GI.ID= GID.GoodsIssueId 
				                            WHERE GI.CreationDate < " + dateTimeStart + @" 
				                            AND GI.BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                    AND GID.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                    " + ReplaceClasueStringByGivenString(purposeCls, "GID");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                    " + ReplaceClasueStringByGivenString(programSessionCls, "GID");
            }
            qq += @"	                ) AS IssuePart 
			                            GROUP BY IssuePart.BranchId, IssuePart.PRID, IssuePart.SESSID, IssuePart.ItemId
		                            ) AS OpeningOut 
		                            ON OpeningIn.BranchId = OpeningOut.BranchId AND OpeningIn.ItemId = OpeningOut.ItemId 
		                            AND (OpeningIn.PRID=OpeningOut.PRID OR (OpeningIn.PRID IS NULL AND OpeningOut.PRID IS NULL)) 
		                            AND (OpeningIn.SESSID=OpeningOut.SESSID OR (OpeningIn.SESSID IS NULL AND OpeningOut.SESSID IS NULL))
		                            UNION ALL
		                            --Opening End
		                            SELECT 
			                            ReceivePart.BranchId,ReceivePart.PRID, ReceivePart.SESSID, ReceivePart.ItemId
			                            , 0 AS OpeningQuantity, 0.00 AS OpeningRate, ReceivePart.ReceivedQuantity
			                            , dbo.get_rate_by_daterange( " + dateTimeStart + @", " + dateTimeEnd + @", ReceivePart.ItemId, 1, 1, ReceivePart.PRID, ReceivePart.SESSID, ReceivePart.BranchId) as ReceiveRate
			                            , 0 AS IssuedQuantity
		                            FROM
		                            (
			                            SELECT 
				                            ReceiveQuery.BranchId, ReceiveQuery.PRID, ReceiveQuery.SESSID, ReceiveQuery.ItemId, SUM(ReceiveQuery.Quantity) AS ReceivedQuantity 
			                            FROM
			                            (
				                            SELECT 
					                            GR.BranchId, GRD.ProgramId as PRID, GRD.SessionId AS SESSID, GRD.ItemId AS ItemId, GRD.ReceivedQuantity AS Quantity, GRD.Rate AS UnitPrize 
				                            FROM UINV_GoodsReceive AS GR 
				                            INNER JOIN UINV_GoodsReceiveDetails AS GRD ON GR.ID= GRD.GoodsRecieveId 
				                            WHERE GR.CreationDate BETWEEN " + dateTimeStart + @" AND " + dateTimeEnd + @"
				                            AND GR.BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                    AND GRD.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                    " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                    " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            qq += @"                        UNION ALL 
				                            SELECT 
					                            GT.BranchToId AS BranchId, GTD.ProgramId as PRID, GTD.SessionId AS SESSID, GTD.ItemId AS ItemId, GTD.TransferQuantity AS Quantity, 0.0 AS UnitPrize 
				                            FROM UINV_GoodsTransfer AS GT 
				                            INNER JOIN UINV_GoodsTransferDetails AS GTD ON GT.ID= GTD.GoodsTransferId 
				                            WHERE GT.CreationDate BETWEEN " + dateTimeStart + @" AND " + dateTimeEnd + @"
				                            AND GT.BranchToId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                    AND GTD.ItemId IN(" + itemIdsStr + @") ";
            }
            qq += @"                        UNION ALL 
				                            SELECT GIssueReq.BranchId , GIssue.PRID , GIssue.SESSID , GIssue.ItemId , GIssue.IssuedQuantity as Quantity , 0.0 AS UnitPrize 
				                            FROM
				                            ( 
					                            SELECT GI.RequisitionId , GID.ProgramId as PRID , GID.SessionId as SESSID , GID.ItemId AS ItemId , GID.IssuedQuantity 
					                            FROM UINV_GoodsIssue AS GI INNER JOIN UINV_GoodsIssueDetails GID ON GI.Id = GID.GoodsIssueId 
					                            INNER JOIN UINV_Requisition RR ON GI.RequisitionId = RR.Id 
					                            WHERE GI.RequisitionId 
								                            IN 
								                            (
									                            SELECT REQ.Id 
									                            FROM UINV_Requisition AS REQ 
									                            WHERE req.BranchId = " + branchId + @"
								                            ) 
					                            AND GI.RequisitionId IS NOT NULL";
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                        AND GID.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                        " + ReplaceClasueStringByGivenString(purposeCls, "GID");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                        " + ReplaceClasueStringByGivenString(programSessionCls, "GID");
            }
            qq += @"                        ) AS GIssue
				                            LEFT JOIN
				                            (
					                            SELECT Id, RR.BranchId FROM UINV_Requisition AS RR
				                            ) 
				                            AS GIssueReq 
				                            ON GIssue.RequisitionId = GIssueReq.Id
			                            ) AS ReceiveQuery
			                            GROUP BY ReceiveQuery.BranchId, ReceiveQuery.PRID, ReceiveQuery.SESSID, ReceiveQuery.ItemId
		                            ) ReceivePart
		                            --Receive Part
		                            UNION ALL
		                            SELECT 
			                            IssuePart.BranchId , IssuePart.PRID , IssuePart.SESSID , IssuePart.ItemId
			                            , 0 As OpeningQuantity, 0.00 AS OpeningRate, 0 AS ReceivedQuantity, 0.00 AS ReceivedRate
			                            , sum(IssuePart.Quantity) AS IssuedQuantity 
		                            FROM 
		                            ( 
			                            SELECT 
				                            GIPr_RET.BranchId , GIPr_RET.PRID , GIPr_RET.SESSID , GIPr_GR.ItemId , GIPr_GR.ReturnedQuantity as Quantity 
			                            FROM 
			                            ( 
				                            SELECT 
					                            GRET.GoodsReceivedId , GRETD.ItemId , GRETD.ReturnedQuantity 
				                            FROM UINV_GoodsReturn AS GRET 
				                            INNER JOIN UINV_GoodsReturnDetails GRETD ON GRET.Id = GRETD.GoodsReturnId 
				                            WHERE GRET.CreationDate BETWEEN " + dateTimeStart + @" AND " + dateTimeEnd + @" 
				                            AND GRET.GoodsReceivedId IN
											                            (
												                            SELECT GR.ID FROM UINV_GoodsReceive AS GR
												                            INNER JOIN UINV_GoodsReceiveDetails GRD ON GR.Id = GRD.GoodsRecieveId
												                            WHERE GR.BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                                                    AND GRD.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                                                    " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                                                    " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            qq += @"                                                    )";
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                    AND GRETD.ItemId IN(" + itemIdsStr + @") ";
            }
            qq += @"                    ) AS GIPr_GR 
			                            LEFT JOIN 
			                            (
				                            SELECT 
					                            GR.BranchId , GR.ID , GRD.ProgramId as PRID , GRD.SessionId AS SESSID , GRD.ItemId , GRD.ReceivedQuantity AS GRQ 
				                            FROM UINV_GoodsReceive AS GR 
				                            INNER JOIN UINV_GoodsReceiveDetails AS GRD ON GR.ID = GRD.GoodsRecieveId 
				                            WHERE GR.CreationDate BETWEEN " + dateTimeStart + @" AND " + dateTimeEnd + @" 
				                            AND BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                    AND GRD.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                    " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                    " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            qq += @"                    ) AS GIPr_RET 
			                            ON GIPr_GR.GoodsReceivedId = GIPr_RET.ID 
			                            UNION ALL 
			                            SELECT GT.BranchFromId as BranchId, GTD.ProgramId as PRID, GTD.SessionId AS SESSID, GTD.ItemId, GTD.TransferQuantity AS Quantity 
			                            FROM UINV_GoodsTransfer AS GT 
			                            INNER JOIN UINV_GoodsTransferDetails AS GTD ON GT.ID= GTD.GoodsTransferId 
			                            WHERE GT.CreationDate BETWEEN " + dateTimeStart + @" AND " + dateTimeEnd + @"
			                            AND GT.BranchFromId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                AND GTD.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                " + ReplaceClasueStringByGivenString(programSessionCls, "GTD");
            }
            qq += @"                    UNION ALL 
			                            SELECT 
				                            GI.BranchId, GID.ProgramId as PRID , GID.SessionId AS SESSID , GID.ItemId , GID.IssuedQuantity AS Quantity 
			                            FROM UINV_GoodsIssue AS GI 
			                            INNER JOIN UINV_GoodsIssueDetails AS GID ON GI.ID= GID.GoodsIssueId 
			                            WHERE GI.CreationDate BETWEEN " + dateTimeStart + @" AND " + dateTimeEnd + @"
                                        AND GI.BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                qq += @"                    AND GID.ItemId IN(" + itemIdsStr + @") ";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                qq += @"                    " + ReplaceClasueStringByGivenString(purposeCls, "GID");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                qq += @"                    " + ReplaceClasueStringByGivenString(programSessionCls, "GID");
            }
            qq += @"                ) AS IssuePart
		                            GROUP BY IssuePart.BranchId, IssuePart.PRID, IssuePart.SESSID, IssuePart.ItemId
	                            ) AS mainQuery
	                            GROUP BY mainQuery.PRID, mainQuery.SESSID, mainQuery.ItemId
                            ) as query";
            return qq;
        }

        #endregion

        #region Stock Schedule Details Summary Report

        private static string GenerateStockScheduleDetailsReportQuery(int start, int length, long branchId, int[] purposeIds
                                                                    , string[] programSession, long[] itemIds
                                                                    , DateTime dateFrom, DateTime dateTo, bool countQuery = false)
        {
            #region Where Clause

            string purposeCls = purposeIds.Any() && purposeIds[0] > 0 ? GeneratePurposeClause(purposeIds, DefaultCls) : String.Empty;
            string programSessionCls = programSession.Any() && programSession[0] != "0" ? GenerateProgramSessionClause(programSession, DefaultCls) : String.Empty;

            string dateTimeStart = "'" + dateFrom.ToString("yyyy-MM-dd") + " 00:00:00.000'";
            string dateTimeEnd = "'" + dateTo.ToString("yyyy-MM-dd") + " 23:59:59.999'";

            string itemIdsStr = "";

            if (itemIds.Count() > 1)
            {
                itemIdsStr = string.Join(", ", itemIds);
            }

            #endregion

            string query = "";
            string mainQuery = GetDetailsStockScheduleInnerSelectQuery(dateTimeStart, dateTimeEnd, itemIdsStr, purposeCls, programSessionCls, branchId);
            
            #region SELECT Query
            
            const string selectQuery = @"SELECT
	itmG.Name AS ItemGroup
	, itm.Name AS ItemName
	, CASE WHEN expCls.PRID IS NULL THEN 'N/A' ELSE (prm.ShortName +' '+ ses.Name) END AS ProgramSession 
	, expCls.OpeningQty
	, expCls.OpeningRate
	, CAST((expCls.OpeningQty*expCls.OpeningRate) AS DECIMAL(18, 2)) AS OpeningValue
	, expCls.ReceivedQty
	, expCls.ReceivedRate
	, CAST((expCls.ReceivedQty*expCls.ReceivedRate) AS DECIMAL(18, 2)) AS ReceivedValue
	, expCls.TrnsInQty
	, CAST((CASE WHEN expCls.TrnsInQty = 0 THEN 0
			WHEN expCls.OpeningRate = 0 AND expCls.ReceivedRate > 0 THEN expCls.ReceivedRate
			WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate = 0 THEN expCls.OpeningRate
			WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate > 0 THEN ((expCls.OpeningRate+expCls.ReceivedRate)/2)
			ELSE 0 END) AS DECIMAL(18, 2)) AS TrnsInRate
	, CAST((expCls.TrnsInQty*
			CASE WHEN expCls.TrnsInQty = 0 THEN 0
				WHEN expCls.OpeningRate = 0 AND expCls.ReceivedRate > 0 THEN expCls.ReceivedRate
				WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate = 0 THEN expCls.OpeningRate
				WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate > 0 THEN ((expCls.OpeningRate+expCls.ReceivedRate)/2)
				ELSE 0 END)
			 AS DECIMAL(18, 2)) AS TrnsInValue
	, expCls.IssuedQty
	, CAST((CASE WHEN expCls.IssuedQty = 0 THEN 0
			WHEN expCls.OpeningRate = 0 AND expCls.ReceivedRate > 0 THEN expCls.ReceivedRate
			WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate = 0 THEN expCls.OpeningRate
			WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate > 0 THEN ((expCls.OpeningRate+expCls.ReceivedRate)/2)
			ELSE 0 END) AS DECIMAL(18, 2)) AS IssuedRate
	, CAST((expCls.IssuedQty*
			CASE WHEN expCls.IssuedQty = 0 THEN 0
				WHEN expCls.OpeningRate = 0 AND expCls.ReceivedRate > 0 THEN expCls.ReceivedRate
				WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate = 0 THEN expCls.OpeningRate
				WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate > 0 THEN ((expCls.OpeningRate+expCls.ReceivedRate)/2)
				ELSE 0 END)
			 AS DECIMAL(18, 2)) AS IssuedValue
	, expCls.ReturnQty
	, CAST((CASE WHEN expCls.ReturnQty = 0 THEN 0
			WHEN expCls.OpeningRate = 0 AND expCls.ReceivedRate > 0 THEN expCls.ReceivedRate
			WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate = 0 THEN expCls.OpeningRate
			WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate > 0 THEN ((expCls.OpeningRate+expCls.ReceivedRate)/2)
			ELSE 0 END) AS DECIMAL(18, 2)) AS ReturnRate
	, CAST((expCls.ReturnQty*
			CASE WHEN expCls.ReturnQty = 0 THEN 0
				WHEN expCls.OpeningRate = 0 AND expCls.ReceivedRate > 0 THEN expCls.ReceivedRate
				WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate = 0 THEN expCls.OpeningRate
				WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate > 0 THEN ((expCls.OpeningRate+expCls.ReceivedRate)/2)
				ELSE 0 END)
			 AS DECIMAL(18, 2)) AS ReturnValue
	, expCls.TrnsOutQty
	, CAST((CASE WHEN expCls.TrnsOutQty = 0 THEN 0
			WHEN expCls.OpeningRate = 0 AND expCls.ReceivedRate > 0 THEN expCls.ReceivedRate
			WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate = 0 THEN expCls.OpeningRate
			WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate > 0 THEN ((expCls.OpeningRate+expCls.ReceivedRate)/2)
			ELSE 0 END) AS DECIMAL(18, 2)) AS TrnsOutRate
	, CAST((expCls.TrnsOutQty*
			CASE WHEN expCls.TrnsOutQty = 0 THEN 0
				WHEN expCls.OpeningRate = 0 AND expCls.ReceivedRate > 0 THEN expCls.ReceivedRate
				WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate = 0 THEN expCls.OpeningRate
				WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate > 0 THEN ((expCls.OpeningRate+expCls.ReceivedRate)/2)
				ELSE 0 END)
			 AS DECIMAL(18, 2)) AS TrnsOutValue
	, ((expCls.OpeningQty+expCls.ReceivedQty+expCls.TrnsInQty) - (expCls.IssuedQty+expCls.ReturnQty+expCls.TrnsOutQty)) AS ClosingQty
	, CAST((CASE WHEN (expCls.OpeningQty+expCls.ReceivedQty+expCls.TrnsInQty) - (expCls.IssuedQty+expCls.ReturnQty+expCls.TrnsOutQty) = 0 THEN 0
			WHEN expCls.OpeningRate = 0 AND expCls.ReceivedRate > 0 THEN expCls.ReceivedRate
			WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate = 0 THEN expCls.OpeningRate
			WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate > 0 THEN ((expCls.OpeningRate+expCls.ReceivedRate)/2)
			ELSE 0 END) AS DECIMAL(18, 2)) AS ClosingRate
	, CAST((((expCls.OpeningQty+expCls.ReceivedQty+expCls.TrnsInQty) - (expCls.IssuedQty+expCls.ReturnQty+expCls.TrnsOutQty))*
			CASE WHEN (expCls.OpeningQty+expCls.ReceivedQty+expCls.TrnsInQty) - (expCls.IssuedQty+expCls.ReturnQty+expCls.TrnsOutQty) = 0 THEN 0
				WHEN expCls.OpeningRate = 0 AND expCls.ReceivedRate > 0 THEN expCls.ReceivedRate
				WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate = 0 THEN expCls.OpeningRate
				WHEN expCls.OpeningRate > 0 AND expCls.ReceivedRate > 0 THEN ((expCls.OpeningRate+expCls.ReceivedRate)/2)
				ELSE 0 END)
			 AS DECIMAL(18, 2)) AS ClosingValue
                ";
#endregion

            string relationQuery = @"
INNER JOIN UINV_Item itm ON expCls.ItemId = itm.Id
INNER JOIN UINV_ItemGroup itmG ON itmG.Id = itm.ItemGroupId
LEFT OUTER JOIN Program prm ON expCls.PRID = prm.Id
LEFT OUTER JOIN Session ses ON expCls.SESSID = ses.Id";
            relationQuery += GenerateProgramSessionClause(programSession, new List<string>() { "prm", "ses" });
            if (countQuery)
            {
                query = @"SELECT COUNT(CountQuery.ItemName) FROM ( ";
                query += selectQuery + mainQuery + relationQuery;
                query += @") AS CountQuery ";
            }
            else
            {
                query = selectQuery + mainQuery + relationQuery + " ORDER BY ItemName OFFSET " + start + " ROWS FETCH FIRST " + length + " ROWS ONLY";
            }

            

            return query;
        }

        private static string GetDetailsStockScheduleInnerSelectQuery(string dateTimeStart, string dateTimeEnd, string itemIdsStr, string purposeCls, string programSessionCls, long branchId)
        {
            string query = @"
            FROM
            (
	            SELECT 
	            mainQuery.BranchId, mainQuery.PRID, mainQuery.SESSID, mainQuery.ItemId
	            , SUM(mainQuery.OpeningQty) AS OpeningQty
	            , dbo.getrate(" + dateTimeStart +
                           @", mainQuery.ItemId, 1, 1, mainQuery.PRID, mainQuery.SESSID, mainQuery.BranchId ) as OpeningRate
	            , SUM(mainQuery.ReceivedQty) AS ReceivedQty
	            , dbo.get_rate_by_daterange(" + dateTimeStart + @", " + dateTimeEnd +
                           @", mainQuery.ItemId, 1, 1, mainQuery.PRID, mainQuery.SESSID, mainQuery.BranchId ) as ReceivedRate
	            , SUM(mainQuery.TrnsInQty) AS TrnsInQty
	            , SUM(mainQuery.IssuedQty) AS IssuedQty
	            , SUM(mainQuery.ReturnQty) AS ReturnQty
	            , SUM(mainQuery.TrnsOutQty) AS TrnsOutQty 
	            FROM
	            (
		            SELECT 
			            OpeningIn.BranchId, OpeningIn.PRID, OpeningIn.SESSID, OpeningIn.ItemId, 
		                SUM(CASE WHEN OpeningOut.IssuedQuantity IS NULL AND OpeningIn.ReceivedQuantity IS NULL THEN 0
		                        WHEN OpeningOut.IssuedQuantity IS NULL AND OpeningIn.ReceivedQuantity IS NOT NULL THEN OpeningIn.ReceivedQuantity
		                        ELSE (OpeningIn.ReceivedQuantity - OpeningOut.IssuedQuantity) END) AS OpeningQty
		                , 0 AS ReceivedQty , 0 AS TrnsInQty, 0 AS IssuedQty, 0 AS ReturnQty, 0 AS TrnsOutQty
		            FROM
		            (
			            SELECT 
				            ReceivePart.BranchId, ReceivePart.PRID, ReceivePart.SESSID, ReceivePart.ItemId, SUM(ReceivePart.Quantity) AS ReceivedQuantity 
			            FROM
			            ( 
				            SELECT 
					            GR.BranchId, GRD.ProgramId as PRID, GRD.SessionId AS SESSID, GRD.ItemId AS ItemId, GRD.ReceivedQuantity AS Quantity, GRD.Rate AS UnitPrize 
				            FROM UINV_GoodsReceive AS GR 
				            INNER JOIN UINV_GoodsReceiveDetails AS GRD ON GR.ID = GRD.GoodsRecieveId 
				            WHERE 
				            GR.BranchId = " + branchId + @" 
				            AND GR.CreationDate < " + dateTimeStart;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GRD.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            query += @"	
				            UNION ALL 
				            SELECT 
					            GT.BranchToId AS BranchId, GTD.ProgramId as PRID, GTD.SessionId AS SESSID, GTD.ItemId AS ItemId, GTD.TransferQuantity AS Quantity, 0.0 AS UnitPrize 
				            FROM UINV_GoodsTransfer AS GT 
				            INNER JOIN UINV_GoodsTransferDetails AS GTD ON GT.ID= GTD.GoodsTransferId 
				            WHERE 
				            GT.BranchToId = " + branchId + @" 
				            AND GT.CreationDate < " + dateTimeStart;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GTD.ItemId IN(" + itemIdsStr + ")";
            }
            query += @"

				            UNION ALL 
				            SELECT 
					            GIssueReq.BranchId , GIssue.PRID , GIssue.SESSID , GIssue.ItemId , GIssue.IssuedQuantity as Quantity , 0.0 AS UnitPrize 
				            FROM
				            ( 
					            SELECT 
						            GI.RequisitionId , GID.ProgramId as PRID , GID.SessionId as SESSID , GID.ItemId AS ItemId , GID.IssuedQuantity 
					            FROM UINV_GoodsIssue AS GI INNER JOIN UINV_GoodsIssueDetails GID ON GI.Id = GID.GoodsIssueId 
					            INNER JOIN UINV_Requisition RR ON GI.RequisitionId = RR.Id 
					            WHERE 
					            GI.RequisitionId 
					            IN 
					            (
						            SELECT REQ.Id FROM UINV_Requisition AS REQ  
						            INNER JOIN UINV_RequisitionDetails AS REQD ON REQ.Id = REQD.RequisitionId 
						            WHERE GI.RequisitionId IS NOT NULL
						            AND REQ.BranchId = " + branchId + @"                                    
						            AND GI.CreationDate < " + dateTimeStart;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND REQD.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "REQD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "REQD");
            }
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GID.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "GID");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "GID");
            }
            query += @" 
					            )
				            )
				            AS GIssue
				            LEFT JOIN 
				            (
					            SELECT 
						            Id, RR.BranchId 
						            FROM UINV_Requisition AS RR 
				            ) 
				            AS GIssueReq ON GIssue.RequisitionId = GIssueReq.Id
			            )
			            AS ReceivePart
			            GROUP BY ReceivePart.BranchId, ReceivePart.PRID, ReceivePart.SESSID, ReceivePart.ItemId
		            )
		            AS OpeningIn 
		            LEFT JOIN
		            (
			            SELECT 
				            IssuePart.BranchId , IssuePart.PRID , IssuePart.SESSID , IssuePart.ItemId , sum(IssuePart.Quantity) AS IssuedQuantity 
			            FROM
			            (
				            SELECT 
					            GIPr_RET.BranchId , GIPr_RET.PRID , GIPr_RET.SESSID , GIPr_GR.ItemId , GIPr_GR.ReturnedQuantity as Quantity 
				            FROM 
				            ( 
					            SELECT 
						            GRET.GoodsReceivedId , GRETD.ItemId , GRETD.ReturnedQuantity 
					            FROM UINV_GoodsReturn AS GRET 
					            INNER JOIN UINV_GoodsReturnDetails GRETD ON GRET.Id = GRETD.GoodsReturnId 
					            WHERE GRET.CreationDate < " + dateTimeStart + @" 
					            AND 
					            GRET.GoodsReceivedId 
						            IN
						            (
							            SELECT GR.ID FROM UINV_GoodsReceive AS GR
							            INNER JOIN UINV_GoodsReceiveDetails GRD ON GR.Id = GRD.GoodsRecieveId
							            WHERE GR.BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GRD.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            query += @"
						            )";
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GRETD.ItemId IN(" + itemIdsStr + ")";
            }
            query += @"
				            )
				            AS GIPr_GR 
				            LEFT JOIN
				            (
					            SELECT 
						            GR.BranchId , GR.ID , GRD.ProgramId as PRID , GRD.SessionId AS SESSID , GRD.ItemId , GRD.ReceivedQuantity AS GRQ 
					            FROM UINV_GoodsReceive AS GR 
					            INNER JOIN UINV_GoodsReceiveDetails AS GRD ON GR.ID= GRD.GoodsRecieveId 
					            WHERE
					            BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GRD.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            query += @"
				            )
				            AS GIPr_RET ON GIPr_GR.GoodsReceivedId = GIPr_RET.ID
		
				            UNION ALL 
		
				            SELECT 
					            GT.BranchFromId as BranchId , GTD.ProgramId as PRID , GTD.SessionId AS SESSID , GTD.ItemId , GTD.TransferQuantity AS Quantity
				            FROM UINV_GoodsTransfer AS GT 
				            INNER JOIN UINV_GoodsTransferDetails AS GTD ON GT.ID= GTD.GoodsTransferId 
				            WHERE
				            GT.BranchFromId = " + branchId + @" 
				            AND GT.CreationDate < " + dateTimeStart;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GTD.ItemId IN(" + itemIdsStr + ")";
            }
            query += @"
				            UNION ALL 
		
				            SELECT 
					            GI.BranchId , GID.ProgramId as PRID , GID.SessionId AS SESSID , GID.ItemId , GID.IssuedQuantity AS Quantity 
				            FROM UINV_GoodsIssue AS GI 
				            INNER JOIN UINV_GoodsIssueDetails AS GID ON GI.ID= GID.GoodsIssueId 
				            WHERE 
				            GI.BranchId = " + branchId + @"
				            AND GI.CreationDate < " + dateTimeStart;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GID.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "GID");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "GID");
            }
            query += @" 
			            )
			            AS IssuePart 
			            GROUP BY IssuePart.BranchId, IssuePart.PRID, IssuePart.SESSID, IssuePart.ItemId
		            )
		            AS OpeningOut 
		            ON OpeningIn.BranchId = OpeningOut.BranchId AND OpeningIn.ItemId = OpeningOut.ItemId  
		            AND (OpeningIn.PRID = OpeningOut.PRID OR (OpeningIn.PRID IS NULL AND OpeningOut.PRID IS NULL)) 
		            AND (OpeningIn.SESSID = OpeningOut.SESSID OR (OpeningIn.SESSID IS NULL AND OpeningOut.SESSID IS NULL))
		            GROUP BY OpeningIn.BranchId, OpeningIn.PRID, OpeningIn.SESSID, OpeningIn.ItemId
		            UNION ALL

		            --Received Part
		            SELECT 
			            ReceivedPart.BranchId, ReceivedPart.PRID as PRID, ReceivedPart.SESSID AS SESSID, ReceivedPart.ItemId AS ItemId
			            , 0 AS OpeningQty
			            , SUM(ReceivedPart.Quantity) AS ReceivedQty
			            , 0 AS TrnsInQty, 0 AS IssuedQty, 0 AS ReturnQty, 0 AS TrnsOutQty
		            FROM
		            (
			            SELECT 
				            GR.BranchId, GRD.ProgramId as PRID, GRD.SessionId AS SESSID, GRD.ItemId AS ItemId, GRD.ReceivedQuantity AS Quantity 
			            FROM UINV_GoodsReceive AS GR 
			            INNER JOIN UINV_GoodsReceiveDetails AS GRD ON GR.ID = GRD.GoodsRecieveId 
			            WHERE 
			            GR.BranchId = " + branchId + @" 
			            AND GR.CreationDate BETWEEN " + dateTimeStart + @"  AND " + dateTimeEnd;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GRD.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            query += @"
		
			            UNION ALL 
			            SELECT 
				            GT.BranchToId AS BranchId, GTD.ProgramId as PRID, GTD.SessionId AS SESSID, GTD.ItemId AS ItemId, GTD.TransferQuantity AS Quantity 
			            FROM UINV_GoodsTransfer AS GT 
			            INNER JOIN UINV_GoodsTransferDetails AS GTD ON GT.ID= GTD.GoodsTransferId 
			            WHERE 
			            GT.BranchToId = " + branchId + @" 
			            AND GT.CreationDate BETWEEN " + dateTimeStart + @"  AND " + dateTimeEnd;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GTD.ItemId IN(" + itemIdsStr + ")";
            }

            query += @"
			            UNION ALL 
			            SELECT 
				            GIssueReq.BranchId , GIssue.PRID , GIssue.SESSID , GIssue.ItemId , GIssue.IssuedQuantity as Quantity 
			            FROM
			            ( 
				            SELECT 
					            GI.RequisitionId , GID.ProgramId as PRID , GID.SessionId as SESSID , GID.ItemId AS ItemId , GID.IssuedQuantity 
				            FROM UINV_GoodsIssue AS GI INNER JOIN UINV_GoodsIssueDetails GID ON GI.Id = GID.GoodsIssueId 
				            INNER JOIN UINV_Requisition RR ON GI.RequisitionId = RR.Id 
				            WHERE 
				            GI.RequisitionId 
				            IN 
				            (
					            SELECT REQ.Id FROM UINV_Requisition AS REQ  
					            INNER JOIN UINV_RequisitionDetails AS REQD ON REQ.Id = REQD.RequisitionId 
					            WHERE GI.RequisitionId IS NOT NULL
					            AND REQ.BranchId = " + branchId + @"                                    
					            AND GI.CreationDate BETWEEN " + dateTimeStart + @"  AND " + dateTimeEnd;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND REQD.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "REQD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "REQD");
            }
            query += @"
				            )";
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GID.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "GID");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "GID");
            }
            query += @"
			            )
			            AS GIssue
			            LEFT JOIN 
			            (
				            SELECT 
					            Id, RR.BranchId 
					            FROM UINV_Requisition AS RR 
			            ) 
			            AS GIssueReq ON GIssue.RequisitionId = GIssueReq.Id
		            ) AS ReceivedPart
		            GROUP BY ReceivedPart.BranchId, ReceivedPart.PRID, ReceivedPart.SESSID, ReceivedPart.ItemId
		            UNION ALL
		            --Transfer IN Part
		            SELECT 
			            GT.BranchToId AS BranchId, GTD.ProgramId as PRID, GTD.SessionId AS SESSID, GTD.ItemId AS ItemId
			            , 0 AS OpeningQty, 0 AS ReceivedQty, GTD.TransferQuantity AS TrnsInQty
			            , 0 AS IssuedQty, 0 AS ReturnQty, 0 AS TrnsOutQty
		            FROM UINV_GoodsTransfer AS GT 
		            INNER JOIN UINV_GoodsTransferDetails AS GTD ON GT.ID= GTD.GoodsTransferId 
		            WHERE 
		            GT.BranchToId = " + branchId + @" 
		            AND GT.CreationDate BETWEEN " + dateTimeStart + @"  AND " + dateTimeEnd;

            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GTD.ItemId IN(" + itemIdsStr + ")";
            }
            query += @"                    
		            UNION ALL
		            --Issue Part
		            SELECT 
			            GI.BranchId, GID.ProgramId as PRID, GID.SessionId AS SESSID, GID.ItemId
			            , 0 AS OpeningQty, 0 AS ReceivedQty, 0 AS TrnsInQty
			            , SUM(GID.IssuedQuantity) AS IssuedQty , 0 AS ReturnQty, 0 AS TrnsOutQty

		            FROM UINV_GoodsIssue AS GI 
		            INNER JOIN UINV_GoodsIssueDetails AS GID ON GI.ID= GID.GoodsIssueId 
		            WHERE 
		            GI.BranchId = " + branchId + @"
		            AND GI.CreationDate BETWEEN " + dateTimeStart + @"  AND " + dateTimeEnd;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GID.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "GID");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "GID");
            }
            query += @"
		            GROUP BY GI.BranchId, GID.ProgramId, GID.SessionId, GID.ItemId
		            
                    UNION ALL
		            --Return Part
		            SELECT 
			            GIPr_RET.BranchId , GIPr_RET.PRID , GIPr_RET.SESSID , GIPr_GR.ItemId
			            , 0 AS OpeningQty, 0 AS ReceivedQty, 0 AS TrnsInQty, 0 AS IssuedQty 
			            , GIPr_GR.ReturnedQuantity AS ReturnQty, 0 AS TrnsOutQty
		            FROM 
		            ( 
			            SELECT 
				            GRET.GoodsReceivedId , GRETD.ItemId , GRETD.ReturnedQuantity 
			            FROM UINV_GoodsReturn AS GRET 
			            INNER JOIN UINV_GoodsReturnDetails GRETD ON GRET.Id = GRETD.GoodsReturnId 
			            WHERE GRET.CreationDate BETWEEN " + dateTimeStart + @"  AND " + dateTimeEnd + @"
			            AND 
			            GRET.GoodsReceivedId 
				            IN
				            (
					            SELECT GR.ID FROM UINV_GoodsReceive AS GR
					            INNER JOIN UINV_GoodsReceiveDetails GRD ON GR.Id = GRD.GoodsRecieveId
					            WHERE GR.BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GRD.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            query += @"
				            )";
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GRETD.ItemId IN(" + itemIdsStr + ")";
            }
            query += @"
		            )
		            AS GIPr_GR 
		            LEFT JOIN
		            (
			            SELECT 
				            GR.BranchId , GR.ID , GRD.ProgramId as PRID , GRD.SessionId AS SESSID , GRD.ItemId , GRD.ReceivedQuantity AS GRQ 
			            FROM UINV_GoodsReceive AS GR 
			            INNER JOIN UINV_GoodsReceiveDetails AS GRD ON GR.ID= GRD.GoodsRecieveId 
			            WHERE
			            BranchId = " + branchId;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GRD.ItemId IN(" + itemIdsStr + ")";
            }
            if (!string.IsNullOrEmpty(purposeCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(purposeCls, "GRD");
            }
            if (!string.IsNullOrEmpty(programSessionCls))
            {
                query += @" " + ReplaceClasueStringByGivenString(programSessionCls, "GRD");
            }
            query += @"
		            )
		            AS GIPr_RET ON GIPr_GR.GoodsReceivedId = GIPr_RET.ID

		            UNION ALL
		            --Transfer Out Part
		            SELECT 
			            GT.BranchFromId as BranchId, GTD.ProgramId as PRID, GTD.SessionId AS SESSID, GTD.ItemId
			            , 0 AS OpeningQty, 0 AS ReceivedQty, 0 AS TrnsInQty, 0 AS IssuedQty, 0 AS ReturnQty
			            , GTD.TransferQuantity AS TrnsOutQty
		            FROM UINV_GoodsTransfer AS GT 
		            INNER JOIN UINV_GoodsTransferDetails AS GTD ON GT.ID= GTD.GoodsTransferId 
		            WHERE
		            GT.BranchFromId = " + branchId + @" 
		            AND GT.CreationDate BETWEEN " + dateTimeStart + @"  AND " + dateTimeEnd;
            if (!string.IsNullOrEmpty(itemIdsStr))
            {
                query += @" AND GTD.ItemId IN(" + itemIdsStr + ")";
            }
            query += @"
	            )
	            AS mainQuery
	            GROUP BY mainQuery.BranchId, mainQuery.PRID, mainQuery.SESSID, mainQuery.ItemId
            ) AS expCls";
            return query;
        }


        #endregion

        #region Other Function
        
        private static string ReplaceClasueStringByGivenString(string input, string replaceBy)
        {
            string output = "";
            output = input.Replace(DefaultCls, replaceBy);
            return output;
        }

        private static string GeneratePurposeClause(int[] purposeIds, string cls)
        {
            string whereClause = "";
            if (purposeIds.Any() && !purposeIds.Contains(0))
            {
                if (purposeIds.Contains(-1))
                {
                    var list = purposeIds.ToList<int>();
                    list.RemoveAt(list.IndexOf(-1));
                    purposeIds = list.ToArray();
                    whereClause += purposeIds.Length > 0
                        ? " AND (" + cls + ".Purpose IN( " + string.Join(", ", purposeIds) + ") OR " + cls + ".Purpose IS NULL)"
                        : " AND " + cls + ".Purpose IS NULL ";
                }
                else
                {
                    whereClause += " AND " + cls + ".Purpose IN( " + string.Join(", ", purposeIds) + ")";
                }
            }
            return whereClause;
        }

        private static string GenerateProgramSessionClause(string[] psArray, string defaultCls)
        {
            string whereClause = "";
            if (psArray.Any() && psArray[0] != "0")
            {
                string psConditionsStr = "";

                foreach (var ps in psArray)
                {
                    if (string.IsNullOrEmpty(ps.Trim()))
                        continue;
                    string[] psIds = ps.Split(new string[] { "::" }, StringSplitOptions.None);
                    if (psIds.Length == 2)
                    {
                        psConditionsStr += "(" + defaultCls + ".ProgramId = " + psIds[0] + " AND " + defaultCls + ".SessionId = " + psIds[1] + ")";
                    }
                    else
                    {
                        psConditionsStr += "(" + defaultCls + ".ProgramId IS NULL AND " + defaultCls + ".SessionId IS NULL)";
                    }
                    psConditionsStr += " OR ";
                }
                psConditionsStr = psConditionsStr.TrimEnd();
                if (!string.IsNullOrEmpty(psConditionsStr))
                {
                    psConditionsStr = psConditionsStr.Substring(0, psConditionsStr.Length - 2);
                    whereClause += " AND (" + psConditionsStr + ") ";
                }
            }
            return whereClause;
        }
        
        private static string GenerateProgramSessionClause(string[] psArray, List<string> defaultCls)
        {
            string whereClause = "";
            if (psArray.Any() && psArray[0] != "0")
            {
                string psConditionsStr = "";

                foreach (var ps in psArray)
                {
                    if (string.IsNullOrEmpty(ps.Trim()))
                        continue;
                    string[] psIds = ps.Split(new string[] { "::" }, StringSplitOptions.None);
                    if (psIds.Length == 2)
                    {
                        psConditionsStr += "(" + defaultCls[0] + ".Id = " + psIds[0] + " AND " + defaultCls[1] + ".Id = " + psIds[1] + ")";
                    }
                    else
                    {
                        psConditionsStr += "(" + defaultCls[0] + ".Id IS NULL AND " + defaultCls[1] + ".Id IS NULL)";
                    }
                    psConditionsStr += " OR ";
                }
                psConditionsStr = psConditionsStr.TrimEnd();
                if (!string.IsNullOrEmpty(psConditionsStr))
                {
                    psConditionsStr = psConditionsStr.Substring(0, psConditionsStr.Length - 2);
                    whereClause += " WHERE " + psConditionsStr + " ";
                }
            }
            return whereClause;
        }
        
        #endregion
        
        #endregion
    }
}
