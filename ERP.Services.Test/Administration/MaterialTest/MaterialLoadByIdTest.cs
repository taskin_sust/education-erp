﻿using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MaterialTest
{
    public interface IMaterialLoadByIdTest
    {
        #region basic test
        [Fact]
        void LoadById_Null_Check();

        [Fact]
        void LoadById_NotNull_Check();

        [Theory]
        [InlineData(49, "down")]
        [InlineData(51, "up")]
        void GetMaterialByRankNextOrPrevious_NotNull_Check(int rank, string action); 
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Material")]
    public class MaterialLoadByIdTest : MaterialBaseTest, IMaterialLoadByIdTest
    {
        #region basic test
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _materialService.GetMaterial(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            var materialFactory =
                _materialFactory.Create().Persist();
            var obj = _materialService.GetMaterial(materialFactory.Object.Id);
            Assert.NotNull(obj);

        }

        [Theory]
        [InlineData(49, "down")]
        [InlineData(51, "up")]
        public void GetMaterialByRankNextOrPrevious_NotNull_Check(int rank, string action)
        {
            _materialFactory.Create().WithRank(50).Persist();
            var campusByRank = _materialService.GetMaterialByRankNextOrPrevious(rank, action);
            Assert.NotNull(campusByRank);
        } 
        #endregion
    }
}
