﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.ShiftWeekendHistoryTest
{
    public class ShiftWeekendHistoryBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper _commonHelper;
        private ISession _session;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal const long OrgId = 1;
        internal DateTime _startTime = DateTime.Now.AddDays(1).AddHours(1);
        internal DateTime _endTime = DateTime.Now.AddDays(1).AddHours(10);

        internal IShiftWeekendHistoryService _shiftWeekendHistoryService;
        internal ShiftWeekendHistoryFactory _shiftWeekendHistoryFactory;

        #endregion

        public ShiftWeekendHistoryBaseTest()
        {
            _commonHelper = new CommonHelper();
            _shiftWeekendHistoryService = new ShiftWeekendHistoryService(Session);
            _shiftWeekendHistoryFactory = new ShiftWeekendHistoryFactory(null, Session);
        }
        public void Dispose()
        {
            _shiftWeekendHistoryFactory.CleanUp();
            base.Dispose();
        }
    }
}
