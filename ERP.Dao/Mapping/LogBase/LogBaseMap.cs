﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.LogBase;

namespace UdvashERP.Dao.Mapping.LogBase
{
    public class LogBaseMap<TEntityT, TIdT> : ClassMap<TEntityT> where TEntityT : LogBaseEntity<TIdT>
    {
        public LogBaseMap()
        {
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
            Map(x => x.Remarks);
        }
    }
}
