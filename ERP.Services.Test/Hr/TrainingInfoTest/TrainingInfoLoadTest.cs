﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.TrainingInfoTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "TrainingInfo")]
    public class TrainingInfoLoadTest : TrainingInfoBaseTest
    {
        [Fact]
        public void LoadById_Not_Null_Check()
        {
            var instituteObj = GetInstituteObject();
            var teamMemberObj = PersistTeamMember();
            var objFactory = TrainingInfoFactory.Create();
            
            var infoArray = new TeamMemberTrainingInfoArray
            {
                PassingYear = Convert.ToInt32(objFactory.Object.Year),
                TrainingDuration = objFactory.Object.Duration,
                TrainingInstitute = instituteObj.Id.ToString(CultureInfo.CurrentCulture),
                TrainingLocation = objFactory.Object.Location,
                TrainingTitle = objFactory.Object.TrainingTitle,
                TrainingTopic = objFactory.Object.TopicCovere
            };
            var arrayList = new List<TeamMemberTrainingInfoArray> {infoArray};
            var ids = TrainingInfoService.UpdateTrainingInfo(teamMemberObj, arrayList);
            objFactory.Object.Id = ids[0];
            IList<TrainingInfo> traningInfoObj = TrainingInfoService.LoadMemberTraningInfo(teamMemberObj.Id);
            Assert.NotNull(traningInfoObj);
            Assert.NotEqual(traningInfoObj.Count, 0);
        }
    }
}
