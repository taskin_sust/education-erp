﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Hr
{
    public interface ILeaveDao : IBaseDao<Leave, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        //IList<Leave> GetLeaves();
        //IList<Leave> GetLeaves(List<long> organizationId);
        //IList<Leave> LoadLeave(List<long> organizationId = null);
        //IList<Leave> GetLeaves(int draw, int start, int length, List<long> organizationIds);
        IList<Leave> LoadLeave(List<long> organizationIds = null, int draw = 0, int start = 0, int length = 0); 
        IList<Leave> LoadOrganizationLeaveByDate(List<long> organizationIdList, DateTime searchDate);

        #endregion

        #region Others Function

        int GetLeaveCount(List<long> organizationIds = null);
        #endregion

        #region Helper Function

        #endregion


        #region by sajjad
        bool HasDependency(Leave hrLeave);
        //IList<Leave> LoadLeave(long organizationId, int? gender, int employmentStatus, int maritalStatus);
        IList<Leave> LoadLeave(long organizationId, int? gender, int employmentStatus, int maritalStatus, DateTime? searchDate = null);
        #endregion

        Leave GetByName(string name);
    }
    public class LeaveDao : BaseDao<Leave, long>, ILeaveDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        //public IList<Leave> GetLeaves()
        //{
        //    return Session.QueryOver<Leave>().Where(x => x.Status == Leave.EntityStatus.Active).List<Leave>();
        //}

        //public IList<Leave> GetLeaves(List<long> organizationIds)
        //{
        //    var criteria = Session.CreateCriteria<Leave>();
        //    criteria.CreateAlias("Organization", "org");
        //    criteria.Add(Restrictions.Eq("Status", Leave.EntityStatus.Active));
        //    if (organizationIds.Count > 0)
        //    {
        //        criteria.Add(Restrictions.In("org.Id", organizationIds));
        //    }

        //    return criteria.List<Leave>();
        //}
        public IList<Leave> LoadLeave(List<long> organizationIds = null, int draw = 0, int start = 0, int length = 0)
        {
            ICriteria criteria = GetLoadLeaveQuery(organizationIds);
            if (length > 0)
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Leave>();

            return criteria.List<Leave>();
        }

        public IList<Leave> LoadOrganizationLeaveByDate(List<long> organizationIdList, DateTime searchDate)
        {
            var query = Session.QueryOver<Leave>();
            if (organizationIdList != null && organizationIdList.Any() && !organizationIdList.Contains(SelectionType.SelelectAll))
                query.AndRestrictionOn(x => x.Organization.Id).IsIn(organizationIdList);

            query.Where(x => x.EffectiveDate <= searchDate);
            query = query.Where(x => x.ClosingDate == null || x.ClosingDate >= searchDate);
            return query.List<Leave>();
        }

        //public IList<Leave> GetLeaves(int draw, int start, int length, List<long> organizationIds)
        //public IList<Leave> LoadLeave(int draw, int start, int length, List<long> organizationIds)
        //{
        //    //var criteria = Session.CreateCriteria<Leave>().Add(Restrictions.Eq("Status", Leave.EntityStatus.Active));
        //    //if (organizationIds.Count > 0)
        //    //{
        //    //    criteria.CreateAlias("Organization", "org");
        //    //    criteria.Add(Restrictions.In("org.Id", organizationIds));
        //    //}
        //    ICriteria criteria = GetLoadLeaveQuery(organizationIds);
            
        //    return criteria.SetFirstResult(start).SetMaxResults(length).List<Leave>();
        //}

        #endregion

        #region Others Function

        public int GetLeaveCount(List<long> organizationIds = null)
        {
            ICriteria criteria = GetLoadLeaveQuery(organizationIds);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function

        ICriteria GetLoadLeaveQuery(List<long> organizationIds = null)
        {
            ICriteria criteria = Session.CreateCriteria<Leave>();
            criteria.Add(Restrictions.Eq("Status", Leave.EntityStatus.Active));

            if (organizationIds != null && organizationIds.Any() && !organizationIds.Contains(SelectionType.SelelectAll))
            {
                criteria.CreateAlias("Organization", "org").Add(Restrictions.Eq("org.Status", Organization.EntityStatus.Active));
                criteria.Add(Restrictions.In("org.Id", organizationIds));
            }
            return criteria;
        }
        #endregion

        #region by sajjad
        public bool HasDependency(Leave hrLeave)
        {
            var leaveCount = hrLeave.LeaveApplication.Count + hrLeave.MembersLeaveSummary.Count;
            if (leaveCount > 0)
            {
                return true;
            }
            return false;
        }

        public IList<Leave> LoadLeave(long organizationId, int? gender, int employmentStatus, int maritalStatus, DateTime? searchDate = null)
        {
            if (searchDate == null)
                searchDate = DateTime.Now.Date;

            var hrLeave = Session.Query<Leave>().Where(x => x.Status == Leave.EntityStatus.Active && x.Organization.Id == organizationId);
            if (gender == (int)Gender.Male)
            {
                hrLeave = hrLeave.Where(x => x.IsMale == true);
            }
            if (gender == (int)Gender.Female)
            {
                hrLeave = hrLeave.Where(x => x.IsFemale == true);
            }
            if (employmentStatus == (int)MemberEmploymentStatus.Contractual)
            {
                hrLeave = hrLeave.Where(x => x.IsContractual == true);
            }
            if (employmentStatus == (int)MemberEmploymentStatus.Intern)
            {
                hrLeave = hrLeave.Where(x => x.IsIntern == true);
            }
            if (employmentStatus == (int)MemberEmploymentStatus.PartTime)
            {
                hrLeave = hrLeave.Where(x => x.IsPartTime == true);
            }
            if (employmentStatus == (int)MemberEmploymentStatus.Permanent)
            {
                hrLeave = hrLeave.Where(x => x.IsPermanent == true);
            }
            if (employmentStatus == (int)MemberEmploymentStatus.Probation)
            {
                hrLeave = hrLeave.Where(x => x.IsProbation == true);
            }
            if (employmentStatus == (int)MemberEmploymentStatus.Retired)
            {
                hrLeave = hrLeave.Where(x => x.IsContractual == false && x.IsIntern == false && x.IsPartTime == false && x.IsPermanent == false && x.IsProbation == false);
            }
            if (maritalStatus == (int)MaritalType.Divorced)
            {
                hrLeave = hrLeave.Where(x => x.IsDevorced == true);
            }
            if (maritalStatus == (int)MaritalType.Married)
            {
                hrLeave = hrLeave.Where(x => x.IsMarried == true);
            }
            if (maritalStatus == (int)MaritalType.Single)
            {
                hrLeave = hrLeave.Where(x => x.IsSingle == true);
            }
            if (maritalStatus == (int)MaritalType.Widow)
            {
                hrLeave = hrLeave.Where(x => x.IsWidow == true);
            }
            if (maritalStatus == (int)MaritalType.Widower)
            {
                hrLeave = hrLeave.Where(x => x.IsWidower == true);
            }

            //hrLeave = hrLeave.Where(x => x.EffectiveDate.Value.Date <= searchDate.Value.Date);
            //hrLeave = hrLeave.Where(x => x.ClosingDate.Value.Date == null || x.ClosingDate >= searchDate.Value.Date);

            return hrLeave.ToList();
        }

        public Leave GetByName(string name)
        {
            return
                Session.QueryOver<Leave>()
                    .Where(x => x.Name == name && x.Status == Leave.EntityStatus.Active)
                    .SingleOrDefault<Leave>();
        }

        #endregion
    }

}