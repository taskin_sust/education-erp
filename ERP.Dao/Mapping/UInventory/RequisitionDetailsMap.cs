using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class RequisitionDetailsMap : BaseClassMap<RequisitionDetails, long>
    {
        public RequisitionDetailsMap()
        {
            Table("UINV_RequisitionDetails");
            LazyLoad();
            References(x => x.Item).Column("ItemId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.Requisition).Column("RequisitionId");
            References(x => x.PurposeProgram).Column("PurposeProgramId");
            References(x => x.PurposeSession).Column("PurposeSessionId");
            Map(x => x.Purpose).Column("Purpose");
            Map(x => x.RequiredQuantity).Column("RequiredQuantity");
            Map(x => x.RequisitionStatus).Column("RequisitionStatus").Not.Nullable();
        }
    }
}
