﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Students
{

    //test  Class programId and BranchId pair 
    //public class ProgramBranch
    //{
    //    public long programId;
    //    public long branchId;

    //    public ProgramBranch(long programId, long branchId)
    //    {
    //        this.programId = programId;
    //        this.branchId = branchId;
    //    }
    //}
    //
    public interface IStudentPaymentDao : IBaseDao<StudentPayment, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        StudentPayment LoadBybrCodeAndsCode(string lastPrCode, int paymentType);
        StudentPayment LoadByStudentProgram(StudentProgram studentProgram);
        StudentPayment LoadLastOrDefaultPayment(long studentProgramId);
        StudentPayment GetLastPaymentWithInBlockingTime(StudentPayment studentPayment, string studentProgramId, int duplicatePaymentBlockingTimeInMinute);
        #endregion

        #region List Loading Function
        IList<StudentPayment> GetByStudentProgram(StudentProgram studentProgram);
        IList<StudentPayment> LoadAllActive();
        IList<StudentPayment> LoadWithoutDue();
        IList<StudentDueListDto> LoadDueList(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, DateTime tillDate, string[] batchDays, string[] batchTime, int start, int length, string orderBy, string orderDir);
        IList<StudentPayment> GetTransactionReport(
            List<long> organizationIdList, List<long> programIdList, List<long> branchIdList, 
            long[] sessionId,  long[] campusId,
            List<AspNetUser> aspnetUsers, long[] userIds, DateTime datef, DateTime datet, int paymentMethod, int start, int length            
            );
        List<CollectionSummaryReportDto> LoadCollectionSummaryReport(List<long> programIdList, List<long> branchIdList, long sessionId, string dateFrom, string dateTo);
        List<DueSummaryReportDto> LoadDueSummaryReport(List<long> programIdList, List<long> branchIdList, long sessionId, string dateFrom, string dateTo);
        List<CollectionVDueReportDto> LoadCollectionVDueReport(List<long> programIdList, List<long> branchIdList, long sessionId, string dateTill);
        List<BranchWiseTransactionReportDto> BranchWiseTransactionReportDtoList(List<long> branchIdList, List<long> campusIdList, int programBranchWiseType, int dailySummaryType, string dateFrom, string dateTo, int reportType);
        List<BranchWiseTransactionReportDueDto> BranchWiseTransactionReportDueDtoList(List<long> branchIdList, List<long> campusIdList, int programBranchWiseType, int dailySummaryType, string dateFrom, string dateTo, int reportType);
        List<SettledComparisonReportDto> SettledComparisonReport(List<long> programIdList, List<long> branchIdList, string dateFrom, string dateTo);
        List<TransactionSummaryReportDto> TransactionSummaryReportDtoList(List<long> branchIdList, List<long> programIdList, List<long> campusIds, int method, int dailySummaryType, string dateFrom, string dateTo);
        //x
        IList<StudentPayment> GetMyTransactionReport(long currentUserId, long[] branchId, List<long> authorizedBranches, long[] campusId, long[] authorizedCampuses, DateTime date, int? start = null, int? length = null);

        IList<StudentPayment> GetTransaction(long userId, DateTime toDate,  bool isPrev);
        #endregion

        #region Others Function
        string LoadPaymentStatusByStudentProgram(long stdProId);
        int GetAllStudentPaymentListCountBeforeAParticularPayment(StudentPayment studentPayment);
        bool isDuplicateTransicationFound(string[] tranArray);

        int GetTransactionReportCount(
            List<long> organizationIdList, List<long> programIdList, List<long> branchIdList, 
            long[] sessionId,  long[] campusId,
            List<AspNetUser> aspnetUsers, long[] userIds, DateTime datef, DateTime datet, int paymentMethod
        );
        int GetMyTransactionReportCount(long currentUserId, long[] branchId, List<long> authorizedBranchIdList, long[] campusId, long[] authorizedCampusIdList, DateTime date);

        int LoadDueListCount(long programId, long sessionId, long[] brances, long[] campusId, long[] batchName, long[] course, DateTime tillDate, string[] batchDays, string[] batchTime);

        int TotalpayAbleAmount(long stProgramId);
        decimal GetTotalCollectionAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo);
        decimal GetTotalRefundAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo);

        #endregion

    }

    public class StudentPaymentDao : BaseDao<StudentPayment, long>, IStudentPaymentDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public StudentPayment LoadBybrCodeAndsCode(string lastPrCode, int paymentType)
        {
            var criteria =
                Session.CreateCriteria<StudentPayment>();
            criteria.Add(Restrictions.Eq("Status", StudentPayment.EntityStatus.Active));
            criteria.Add(Restrictions.Like("ReceiptNo", lastPrCode, MatchMode.Anywhere));
            criteria.Add(Restrictions.Eq("PaymentType", paymentType));

            var sPayment = criteria.AddOrder(Order.Desc("Id")).SetMaxResults(1).UniqueResult<StudentPayment>();
            //    .Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active))
            //    .Add(Restrictions.Like("MrNo", lastPrCode, MatchMode.Start))
            //    .AddOrder(Order.Desc("Id")).SetMaxResults(1).UniqueResult<StudentPayment>();
            ////Session.QueryOver<StudentPayment>()
            //        .Where(x => x.Status == StudentPayment.EntityStatus.Active).Where(Restrictions.On<StudentPayment>(x => x.MrNo).IsLike(lastPrCode))
            //        .OrderBy(x => x.Id)
            //        .Desc.Take(1).SingleOrDefault<StudentPayment>();
            return sPayment;
        }
        public StudentPayment LoadByStudentProgram(StudentProgram studentProgram)
        {
            return Session.QueryOver<StudentPayment>()
            .Where(x => x.StudentProgram == studentProgram && x.Status == StudentPayment.EntityStatus.Active)
            .OrderBy(x => x.Id)
            .Desc.Take(1)
            .SingleOrDefault<StudentPayment>();
        }

        public StudentPayment LoadLastOrDefaultPayment(long studentProgramId)
        {
            var criteria = Session.CreateCriteria<StudentPayment>();
            criteria.Add(Restrictions.Eq("Status", StudentPayment.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("StudentProgram.Id", studentProgramId));
            return criteria.AddOrder(Order.Desc("Id")).SetMaxResults(1).UniqueResult<StudentPayment>();
        }

        public StudentPayment GetLastPaymentWithInBlockingTime(StudentPayment studentPayment, string studentProgramId, int duplicatePaymentBlockingTimeInMinute)
        {
            var creationDate = DateTime.Now.AddMinutes((-1) * duplicatePaymentBlockingTimeInMinute);
            var criteria = Session.CreateCriteria<StudentPayment>();
            criteria.AddOrder(Order.Desc(Projections.Id()));
            criteria.SetMaxResults(1);
            criteria.CreateAlias("StudentProgram", "sp");

            criteria.Add(Restrictions.Eq(Projections.Property<StudentPayment>(x => x.ReceivedAmount), studentPayment.ReceivedAmount));
            if (studentPayment.NextReceivedDate != Constants.MaxDateTime)
                criteria.Add(Restrictions.Eq(Projections.Property<StudentPayment>(x => x.NextReceivedDate), studentPayment.NextReceivedDate));
            //criteria.Add(Restrictions.Eq(Projections.Property<StudentPayment>(x => x.DueAmount), studentPayment.DueAmount));
            criteria.Add(Restrictions.Eq(Projections.Property<StudentPayment>(x => x.SpDiscountAmount), studentPayment.SpDiscountAmount));
            criteria.Add(Restrictions.Eq("sp.PrnNo", studentProgramId));

            criteria.Add(
                Restrictions.Or(
                    Restrictions.Ge("CreationDate", creationDate),
                    Restrictions.Eq("CreationDate", creationDate)
                )
            );

            return criteria.UniqueResult<StudentPayment>();
        }

        #endregion

        #region List Loading Function
        public IList<StudentPayment> GetByStudentProgram(StudentProgram studentProgram)
        {
            return
                Session.QueryOver<StudentPayment>()
                    .Where(x => x.StudentProgram == studentProgram && x.Status == StudentPayment.EntityStatus.Active).List<StudentPayment>();
        }
        public IList<StudentPayment> LoadAllActive()
        {
            return
                Session.CreateCriteria<StudentPayment>()
                    .Add(Restrictions.Eq("Status", StudentPayment.EntityStatus.Active))
                    .List<StudentPayment>();
        }
        public IList<StudentPayment> LoadWithoutDue()
        {
            return
                Session.CreateCriteria<StudentPayment>()
                    .Add(Restrictions.Eq("Status", StudentPayment.EntityStatus.Active))
                    .Add(Restrictions.Not(Restrictions.Eq("DueAmount", Convert.ToDecimal(0))))
                    .List<StudentPayment>();
        }
        public IList<StudentDueListDto> LoadDueList(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, DateTime tillDate, string[] batchDays, string[] batchTime, int start, int length, string orderBy, string orderDir)
        {

            string innerQuery = GetInnerQueryDueList(programId, sessionId, branchId, campusId, batchName, course, tillDate, batchDays, batchTime);
            //string query = "DECLARE @rowsperpage INT DECLARE @start INT SET @start = " + (start + 1) + " SET @rowsperpage = " + length +
            //                " select * from ( SELECT row_number() OVER (ORDER BY " + orderBy + " " + orderDir + ") AS RowNum,  A.Id,A.DueAmount,A.NextReceivedDate,A.PrnNo,A.Batch,A.Branch,s1_.NickName,s1_.Mobile,s1_.GuardiansMobile1,i_.Name as Institute FROM(" + innerQuery + ") AS A" +
            //               " left join Student s1_ on s1_.Id = A.StudentId " +
            //               " left join Institute i_ on i_.Id = A.InstituteId " +
            //               "WHERE A.R=1 AND A.DueAmount>0) as pagination";

            var filterPagination = "";
            if (length > 0)
            {
                filterPagination = " where pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            string query = @"DECLARE @rowsperpage INT 
		                    DECLARE @start INT 
		                    SET @start = " + (start + 1) + @" 
		                    SET @rowsperpage = " + length + @"
                            Select  [StudentProgramId],[TransferDate],[FromBatchId],[ToBatchId]
                            INTO #batchTemp
                            From (
	                            Select *, RANK() OVER(PARTITION BY StudentProgramId ORDER BY [TransferDate] DESC) as R  From [dbo].[StudentBatchLog]
	                            Where [TransferDate]<='" + tillDate + @"' 
	                            AND [Status]=" + StudentProgram.EntityStatus.Active + @"
                            ) AS a where a.R=1

                            SELECT * FROM ( 
	                            SELECT row_number() OVER (ORDER BY A.PrnNo asc) AS RowNum, A.Id,A.DueAmount,A.NextReceivedDate,A.PrnNo,A.Batch,A.Branch,s1_.NickName,s1_.Mobile,s1_.GuardiansMobile1,i_.Name as Institute 
	                            FROM(
		                            " + innerQuery + @"
	                            ) A
	                            left join Student s1_ on s1_.Id = A.StudentId  
	                            left join Institute i_ on i_.Id = A.InstituteId 
                            ) as pagination 
                            " + filterPagination + @"	
                            IF EXISTS(SELECT * FROM #batchTemp) DROP TABLE #batchTemp;";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentDueListDto>());
            return iQuery.List<StudentDueListDto>().ToList();
            //return new List<StudentDueListDto>();
        }

        public IList<StudentPayment> GetTransactionReport(
            List<long> organizationIdList, List<long> programIdList, List<long> branchIdList,
            long[] sessionId, long[] campusId,
            List<AspNetUser> aspnetUsers, long[] userIds, DateTime datef, DateTime datet, int paymentMethod, int start, int length  
            )
        {
            var criteria = GetTransactionReportCriteria(organizationIdList, programIdList, branchIdList, sessionId, campusId, aspnetUsers, userIds, datef, datet, paymentMethod);
            if (length > 0)
            {
                criteria = criteria.SetFirstResult(start).SetMaxResults(length);
            }
            IList<StudentPayment> studentPaymentsList = criteria.List<StudentPayment>();
            return studentPaymentsList;
        }

        public List<CollectionSummaryReportDto> LoadCollectionSummaryReport(List<long> programIdList, List<long> branchIdList, long sessionId, string dateFrom, string dateTo)
        {
            string queryString = "";

            string bSessionFilter = (sessionId != 0) ? " AND b.SessionId = " + sessionId + @" " : " ";
            string pbsSessionFilter = (sessionId != 0) ? " AND pbs.SessionId = " + sessionId + @"  " : " ";

            queryString = @"SELECT MAX(br.Name) AS BranchName
                             , ISNULL(MAX(sp.CollectionAmount),0.0) AS CollectionAmount
                             , ISNULL(MAX(sp.CashBack),0.0) AS CashBack
                             , ISNULL(MAX(sp.NetCollection),0.0) AS NetCollection
                            FROM Branch AS br 
                            INNER JOIN ProgramBranchSession AS pbs ON pbs.BranchId = br.Id
                            LEFT JOIN (
                             SELECT b.BranchId
                              , ISNULL(SUM(sp.ReceivedAmount),0.0) AS CollectionAmount
                              , ISNULL(SUM(sp.CashBackAmount),0.0) AS CashBack
                              , ISNULL(SUM(sp.ReceivedAmount),0.0) - ISNULL(SUM(sp.CashBackAmount),0.0) AS NetCollection
                             FROM StudentPayment sp
                             INNER JOIN Batch AS b ON b.Id = sp.CurrentBatchId
                             WHERE sp.Status = 1 AND b.Status = 1
                              AND b.ProgramId IN (" + string.Join(",", programIdList) + @") 
                              " + bSessionFilter + @"  
                              AND sp.receivedDate BETWEEN '" + dateFrom + @" 00:00:00' AND '" + dateTo + @" 23:59:59'
                             GROUP BY b.BranchId
                            ) AS sp ON sp.BranchId = br.id
                            WHERE br.Status = 1
                             AND pbs.ProgramId IN (" + string.Join(",", programIdList) + @") 
                             " + pbsSessionFilter + @"  
                             AND br.Id IN (" + string.Join(",", branchIdList) + @")  
                            GROUP BY br.Id
                            ORDER BY MAX(br.Rank) ASC";
            IQuery query = Session.CreateSQLQuery(queryString);
            query.SetResultTransformer(Transformers.AliasToBean<CollectionSummaryReportDto>());
            return query.List<CollectionSummaryReportDto>().ToList();
        }

        public List<DueSummaryReportDto> LoadDueSummaryReport(List<long> programIdList, List<long> branchIdList, long sessionId, string dateFrom, string dateTo)
        {
            string queryString = "";
            string bSessionFilter = (sessionId != 0) ? " AND b.SessionId = " + sessionId + @" " : " ";
            string pbsSessionFilter = (sessionId != 0) ? " AND pbs.SessionId = " + sessionId + @"  " : " ";
            queryString = @"SELECT MAX(br.Name) AS BranchName
                             , ISNULL(SUM(sp.OpeningDue),0.0) AS OpeningDue
                             , ISNULL(SUM(sp.ClosingDue),0.0) AS ClosingDue
                            FROM Branch AS br 
                            INNER JOIN ProgramBranchSession AS pbs ON pbs.BranchId = br.Id
                            LEFT JOIN (
                             SELECT a.*, b.BranchId FROM (
                              SELECT a.CurrentBatchId, SUM(a.DueAmount) AS OpeningDue, 0 AS ClosingDue FROM (
                               SELECT sp.DueAmount, sp.CurrentBatchId, RANK() OVER(PARTITION BY sp.StudentProgramId ORDER BY sp.ReceivedDate DESC) AS r
                               FROM StudentPayment AS sp 
                               WHERE sp.Status = 1 
                                AND sp.receivedDate <= '" + dateFrom + @" 00:00:00' 
                              ) AS a
                              WHERE r= 1
                              GROUP BY a.CurrentBatchId
                              UNION ALL
                              SELECT a.CurrentBatchId, 0 AS OpeningDue, SUM(a.DueAmount) AS ClosingDue FROM (
                               SELECT sp.DueAmount, sp.CurrentBatchId, RANK() OVER(PARTITION BY sp.StudentProgramId ORDER BY sp.ReceivedDate DESC) AS r
                               FROM StudentPayment AS sp 
                               WHERE sp.Status = 1 
                                AND sp.receivedDate <= '" + dateTo + @" 23:59:59' 
                              ) AS a
                              WHERE r= 1
                              GROUP BY a.CurrentBatchId
                             ) AS a
                             INNER JOIN Batch as b ON b.Id = a.CurrentBatchId
                             AND b.ProgramId IN (" + string.Join(",", programIdList) + @") 
                              " + bSessionFilter + @"  
                             WHERE b.Status = 1
                            ) AS sp ON sp.BranchId = br.Id
                            WHERE br.Status = 1 
                             AND pbs.ProgramId IN (" + string.Join(",", programIdList) + @") 
                              " + pbsSessionFilter + @"  
                             AND br.Id IN (" + string.Join(",", branchIdList) + @")  
                            GROUP BY br.Id
                            ORDER BY MAX(br.Rank) ASC";

            IQuery query = Session.CreateSQLQuery(queryString);
            query.SetResultTransformer(Transformers.AliasToBean<DueSummaryReportDto>());
            return query.List<DueSummaryReportDto>().ToList();
        }

        public List<CollectionVDueReportDto> LoadCollectionVDueReport(List<long> programIdList, List<long> branchIdList, long sessionId, string dateTill)
        {
            string queryString = "";
            string bSessionFilter = (sessionId != 0) ? " AND b.SessionId = " + sessionId + @" " : " ";
            string pbsSessionFilter = (sessionId != 0) ? " AND SessionId = " + sessionId + @"  " : " ";
            queryString = @"SELECT MAX(br.Name) AS BranchName
                             , ISNULL(SUM(sp.TotalStudent),0.0) AS TotalStudent
                             , ISNULL(MAX(sc.NetCollection),0.0) AS NetCollection
                             , ISNULL(MAX(sc.NetCollection),0.0) / CASE WHEN ISNULL(SUM(sp.TotalStudent),1.0) = 0 THEN 1 ELSE ISNULL(SUM(sp.TotalStudent),1.0) END AverageCollection
                             , ISNULL(SUM(sp.DueAmount),0.0) AS DueAmount
                             , ROUND((ISNULL(SUM(sp.DueAmount),0.0) / CASE WHEN ISNULL(MAX(sc.NetCollection),1.0) = 0 THEN 1 ELSE ISNULL(MAX(sc.NetCollection),1.0) END ) * 100,2) AS DuePercent
                            FROM Branch AS br 
                            INNER JOIN (
                                SELECT DISTINCT BranchId 
                                FROM ProgramBranchSession 
                                WHERE ProgramId IN (" + string.Join(",", programIdList) + @")  
                                    " + pbsSessionFilter + @"                                      
                            ) AS pbs ON pbs.BranchId = br.Id
                            LEFT JOIN (
                             SELECT a.*, b.BranchId FROM (
                              SELECT a.CurrentBatchId, SUM(a.DueAmount) AS DueAmount, COUNT(*) AS TotalStudent FROM (
                               SELECT sp.DueAmount, sp.CurrentBatchId, RANK() OVER(PARTITION BY sp.StudentProgramId ORDER BY sp.ReceivedDate DESC) AS r
                               FROM StudentPayment AS sp 
                               WHERE sp.Status = 1 
                                AND sp.receivedDate <= '" + dateTill + @" 23:59:59' 
                              ) AS a
                              WHERE r= 1
                              GROUP BY a.CurrentBatchId
                             ) AS a
                             INNER JOIN Batch as b ON b.Id = a.CurrentBatchId
                             AND b.ProgramId IN (" + string.Join(",", programIdList) + @") 
                              " + bSessionFilter + @"  
                             WHERE b.Status = 1
                            ) AS sp ON sp.BranchId = br.Id
                            LEFT JOIN (
                             SELECT b.BranchId
                              , ISNULL(SUM(sp.ReceivedAmount),0.0) AS CollectionAmount
                              , ISNULL(SUM(sp.CashBackAmount),0.0) AS CashBack
                              , ISNULL(SUM(sp.ReceivedAmount),0.0) - ISNULL(SUM(sp.CashBackAmount),0.0) AS NetCollection
                             FROM StudentPayment sp
                             INNER JOIN Batch AS b ON b.Id = sp.CurrentBatchId
                             WHERE sp.Status = 1 AND b.Status = 1
                              AND b.ProgramId IN (" + string.Join(",", programIdList) + @") 
                               " + bSessionFilter + @"  
                              AND sp.receivedDate <= '" + dateTill + @"  23:59:59'
                             GROUP BY b.BranchId
                            ) AS sc ON sc.BranchId = br.id
                            WHERE br.Status = 1 
                             AND br.Id IN  (" + string.Join(",", branchIdList) + @")
                            GROUP BY br.Id
                            ORDER BY MAX(br.Rank) ASC ";

            IQuery query = Session.CreateSQLQuery(queryString);
            query.SetResultTransformer(Transformers.AliasToBean<CollectionVDueReportDto>());
            return query.List<CollectionVDueReportDto>().ToList();
        }

        public List<BranchWiseTransactionReportDto> BranchWiseTransactionReportDtoList(List<long> branchIdList, List<long> campusIdList, int programBranchWiseType, int dailySummaryType, string dateFrom, string dateTo, int reportType)
        {
            string queryString = "";

            string dailySummarySelect = " '' AS ReceivedDate ";
            string dailySummaryGroupBy = "";
            string programWiseSelect = " '' AS ProgramSession ";
            string programWiseGroupBy = "";
            string selectedCampus = "";
            string selectedCampusId = "";
            string selectedBranch = "";
            string selectedBranchCampusId = "";
            string campusIdListFilter = "";

            if (reportType == 1)
            {
                selectedCampus = "c";
                selectedCampusId = "c.Id";
                selectedBranch = "b";
                selectedBranchCampusId = "b.CampusId";
            }
            else
            {
                selectedCampus = "b";
                selectedCampusId = "b.CampusId";
                selectedBranch = "c";
                selectedBranchCampusId = "c.Id";
            }

            if (!campusIdList.Contains(SelectionType.SelelectAll))
            {
                campusIdListFilter = " AND " + selectedBranchCampusId + @" IN (" + string.Join(",", campusIdList) + @") ";
            }
            if (dailySummaryType == 1)
            {
                dailySummarySelect = " a.ReceivedDate AS ReceivedDate ";
                dailySummaryGroupBy = " a.ReceivedDate, ";
            }
            if (programBranchWiseType == 2)
            {
                programWiseSelect = " MAX(p.ShortName) + '-' + MAX(s.Name) AS ProgramSession ";
                programWiseGroupBy = " , a.ProgramId, a.SessionId ";
            }

            queryString = @"  SELECT 
                             " + dailySummarySelect + @"
                             , MAX(br.Name) AS Branch
                             , MAX(c.Name) AS Campus
                             , " + programWiseSelect + @"
                             , SUM(a.ReceivedAmount) AS ReceivedAmount 
                             , SUM(a.CashBackAmount) AS CashBackAmount 
                             , SUM(a.NetReceivedAmount) AS NetReceivedAmount 
                            FROM (
                             SELECT CONVERT(VARCHAR,sp.ReceivedDate,106) ReceivedDate, " + selectedCampus + @".BranchId, " + selectedCampusId + @" AS CampusId, b.ProgramId, b.SessionId
                              , SUM(sp.ReceivedAmount) AS ReceivedAmount
                              , ISNULL(SUM(sp.CashBackAmount),0) AS CashBackAmount
                              , ISNULL(SUM(sp.ReceivedAmount),0) - ISNULL(SUM(sp.CashBackAmount),0) AS NetReceivedAmount
                             FROM [dbo].[StudentPayment] AS sp
                             INNER JOIN Batch AS b ON b.Id = sp.CurrentBatchId
                             INNER JOIN Campus AS c ON c.Id = sp.CampusIdReceiveFrom
                             WHERE sp.Status = 1 -- AND c.Status = 1  AND b.Status = 1 
                              AND " + selectedBranch + @".BranchId IN (" + string.Join(",", branchIdList) + @") 
                               " + campusIdListFilter + @"
                              AND sp.ReceivedDate BETWEEN '" + dateFrom + @" 00:00:00.000' AND '" + dateTo + @" 23:59:59'
                             GROUP BY CONVERT(VARCHAR,sp.ReceivedDate,106), " + selectedCampus + @".BranchId, " + selectedCampusId + @", b.ProgramId, b.SessionId
                            ) AS a
                            LEFT JOIN Branch AS br ON br.Id = a.BranchId
                            LEFT JOIN Campus AS c ON c.Id = a.CampusId
                            LEFT JOIN Program AS p ON p.Id = a.ProgramId
                            LEFT JOIN [Session] AS s ON s.Id = a.SessionId
                            WHERE br.Status = 1 AND c.Status = 1 AND p.Status = 1 AND s.Status = 1 --AND NetReceivedAmount > 0
                            GROUP BY 
                             " + dailySummaryGroupBy + @"
                             a.BranchId, a.CampusId
                             " + programWiseGroupBy + @" ";

            IQuery query = Session.CreateSQLQuery(queryString);
            query.SetResultTransformer(Transformers.AliasToBean<BranchWiseTransactionReportDto>());
            return query.List<BranchWiseTransactionReportDto>().ToList();
        }

        public List<BranchWiseTransactionReportDueDto> BranchWiseTransactionReportDueDtoList(List<long> branchIdList, List<long> campusIdList, int programBranchWiseType,
            int dailySummaryType, string dateFrom, string dateTo, int reportType)
        {
            string queryString = "";

            string dailySummarySelect = " '' AS ReceivedDate, ";
            string dailySummaryGroupBy = "";
            string dailySummaryOrderBy = "";
            string programWiseSelect = " ,'' AS ProgramSession ";
            string programWiseGroupBy = "";
            string programOrderBy = "";
            string campusIdListFilter = "";
            if (!campusIdList.Contains(SelectionType.SelelectAll))
                campusIdListFilter = " AND b.CampusId IN  (" + string.Join(",", campusIdList) + @")  ";
            if (dailySummaryType == 1)
            {
                dailySummarySelect = " CONVERT(varchar,sp.ReceivedDate,106) AS ReceivedDate,  ";
                dailySummaryGroupBy = " , CONVERT(varchar,sp.ReceivedDate,106) ";
                dailySummaryOrderBy = " MAX(sp.ReceivedDate),  ";
            }
            if (programBranchWiseType == 2)
            {
                programWiseSelect = " , MAX(p.ShortName) + '-' + MAX(s.Name) ProgramSession ";
                programWiseGroupBy = " , b.ProgramId, b.SessionId ";
                programOrderBy = " , MAX(p.Rank), MAX(s.Rank)   ";
            }

            queryString = @"  SELECT " + dailySummarySelect + @"
                             MAX(anu.userName) UserEmail
                             " + programWiseSelect + @"
                             , ISNULL(SUM(ReceivedAmount),0) - ISNULL(SUM(CashBackAmount),0)
                                AS NotReceivedAmount
                            FROM StudentPayment AS sp
                            INNER JOIN Batch AS b ON b.Id = sp.CurrentBatchId
                            LEFT JOIN Program AS p ON p.Id = b.ProgramId
                            LEFT JOIN [Session] AS s ON s.Id = b.SessionId
                            LEFT JOIN AspNetUsers AS anu ON anu.Id = sp.CreateBy
                            WHERE sp.Status = 1 AND sp.CampusReceiveBy IS NULL
                             AND b.BranchId IN  (" + string.Join(",", branchIdList) + @") 
                             " + campusIdListFilter + @"
                             AND sp.ReceivedDate BETWEEN '" + dateFrom + @" 00:00:00' AND '" + dateTo + @" 23:59:59'
                            GROUP BY sp.CreateBy
                             " + dailySummaryGroupBy + @"
                             " + programWiseGroupBy + @"
                            ORDER BY " + dailySummaryOrderBy + @"
                             MAX(anu.userName)
                             " + programOrderBy + @"
                             ASC ";

            IQuery query = Session.CreateSQLQuery(queryString);
            query.SetResultTransformer(Transformers.AliasToBean<BranchWiseTransactionReportDueDto>());
            return query.List<BranchWiseTransactionReportDueDto>().ToList();
        }

        public List<SettledComparisonReportDto> SettledComparisonReport(List<long> programIdList, List<long> branchIdList, string dateFrom, string dateTo)
        {
            string queryString = "";

            string oneYearSubFrom = Convert.ToDateTime(dateFrom).Date.AddYears(-1).ToString("yyyy-MM-dd");
            string oneYearSubTo = Convert.ToDateTime(dateTo).Date.AddYears(-1).ToString("yyyy-MM-dd");

            queryString = @"  SELECT br.name  AS BranchName
                             , ISNULL(a.ThisPeriodAlreadySettledAmount,0) AS ThisPeriodAlreadySettledAmount
                             , ISNULL(b.ThisPeriodLastYearSettledAmount,0) AS ThisPeriodLastYearSettledAmount 
                            FROM Branch as br
                            LEFT JOIN (
                             SELECT BranchId,
                              SUM(ReceivedAmount) as ThisPeriodAlreadySettledAmount
                             FROM StudentPaymentTransections
                             WHERE Status = 1
                              AND IsSubmitted = 1
                              AND ProgramId IN  (" + string.Join(",", programIdList) + @") 
                              AND BranchId IN  (" + string.Join(",", branchIdList) + @") 
                              AND ReceivedDate BETWEEN '" + dateFrom + @" 00:00:00' AND '" + dateTo + @" 23:59:59'
                             GROUP BY BranchId
                            ) AS a ON a.BranchId = br.Id
                            LEFT JOIN (
                             SELECT BranchId,
                              SUM(ReceivedAmount) as ThisPeriodLastYearSettledAmount
                             FROM StudentPaymentTransections
                             WHERE Status = 1
                              AND IsSubmitted = 1
                              AND ProgramId IN  (" + string.Join(",", programIdList) + @") 
                              AND BranchId IN   (" + string.Join(",", branchIdList) + @") 
                              AND ReceivedDate BETWEEN '" + oneYearSubFrom + @" 00:00:00' AND '" + oneYearSubTo + @" 23:59:59'
                             GROUP BY BranchId
                            ) AS b ON b.BranchId = br.Id
                            WHERE br.Status = 1
                             AND br.Id IN   (" + string.Join(",", branchIdList) + @") 
                            ORDER BY br.Rank ASC ";



            IQuery query = Session.CreateSQLQuery(queryString);
            query.SetResultTransformer(Transformers.AliasToBean<SettledComparisonReportDto>());
            return query.List<SettledComparisonReportDto>().ToList();
        }

        public List<TransactionSummaryReportDto> TransactionSummaryReportDtoList(List<long> branchIdList, List<long> programIdList, List<long> campusIds, int method,
            int dailySummaryType, string dateFrom, string dateTo)
        {
            string queryString = "";
            string selectDateWise = " '' AS TransactionDate, ";
            string groupDateWise = "";
            string orderDateWise = "";
            if (dailySummaryType == 1)
            {
                selectDateWise = " CONVERT(varchar,sp.ReceivedDate,106) AS TransactionDate, ";
                groupDateWise = " , CONVERT(varchar,sp.ReceivedDate,106) ";
                orderDateWise = " MAX(sp.ReceivedDate), ";
            }
            string campusInQuery = "";
            if (campusIds !=null && !campusIds.Contains(SelectionType.SelelectAll))
                campusInQuery = " AND b.CampusId IN (" + string.Join(",", campusIds) + @")  ";

            string methodQuery = "";
            if (method != 0)
                methodQuery = " AND sp.PaymentMethod =  " + method +" ";


            queryString = @"  SELECT
                            " + selectDateWise + @" 
                             MAX(p.ShortName) + '-' + MAX(s.Name) ProgramSession
                             , CASE WHEN PaymentMethod=1 THEN 'Cash' ELSE 'bKash' END AS Method
                             , SUM(ISNULL(CourseFees,0)-ISNULL(DiscountAmount,0)) AS Income
                             , ISNULL(SUM(ConsiderationAmount),0) AS Consideration
                             , ISNULL(SUM(SpDiscountAmount),0) AS Discount
                             , ISNULL(SUM(ReceivedAmount),0) AS Received
                             , ISNULL(SUM(CashBackAmount),0) AS CashBack
                            FROM StudentPayment AS sp
                            INNER JOIN Batch AS b ON b.Id = sp.CurrentBatchId
                            LEFT JOIN Program AS p ON p.Id = b.ProgramId
                            LEFT JOIN [Session] AS s ON s.Id = b.SessionId
                            WHERE sp.Status = 1
                             AND b.BranchId IN (" + string.Join(",", branchIdList) + @") 
                             " + campusInQuery + @"
                             AND b.ProgramId IN (" + string.Join(",", programIdList) + @") 
                             --AND b.SessionId IN ()
                             " + methodQuery + @"
                             AND sp.ReceivedDate BETWEEN '" + dateFrom + @" 00:00:00' AND '" + dateTo + @" 23:59:59'
                            GROUP BY b.ProgramId, b.SessionId, sp.PaymentMethod
                            " + groupDateWise + @" 
                            ORDER BY " + orderDateWise + @"  
                             MAX(p.Rank), MAX(s.Rank), sp.PaymentMethod ASC ";

            IQuery query = Session.CreateSQLQuery(queryString);
            query.SetResultTransformer(Transformers.AliasToBean<TransactionSummaryReportDto>());
            return query.List<TransactionSummaryReportDto>().ToList();
        }


        public IList<StudentPayment> GetMyTransactionReport(long currentUserId, long[] branchId, List<long> authorizedBranches, long[] campusId, long[] authorizedCampuses, DateTime date, int? start = null, int? length = null)
        {
            var criteria = GetMyTransactionReportCriteria(currentUserId, branchId, authorizedBranches, campusId, authorizedCampuses, date);
            if (start != null && length != null)
            {
                return criteria.SetFirstResult(Convert.ToInt32(start)).SetMaxResults(Convert.ToInt32(length)).List<StudentPayment>();
            }
            else
            {
                return criteria.List<StudentPayment>();
            }
        }

        public IList<StudentPayment> GetTransaction(long userId, DateTime toDate, bool isPrev)
        {
            ICriteria criteria =
               Session.CreateCriteria<StudentPayment>()
                   .Add(Restrictions.Not(Restrictions.Eq("Status", StudentPayment.EntityStatus.Delete)));
            criteria.Add(Restrictions.Le("CreationDate", toDate.AddDays(1)));
            criteria.Add(Restrictions.Eq("CreateBy", userId));
            if (isPrev)
            {
                criteria.Add(Restrictions.IsNotNull("CampusReceiveBy"));
                criteria.Add(Restrictions.Ge("CreationDate", toDate));
                criteria.Add(Restrictions.Le("CreationDate", toDate.AddDays(1)));
            }
            else
            {
                criteria.Add(Restrictions.IsNull("CampusReceiveBy"));
            }
            
            return criteria.List<StudentPayment>();
        }

        #endregion

        #region Others Function
        public string LoadPaymentStatusByStudentProgram(long stdProId)
        {
            var stdPayment =
                Session.QueryOver<StudentPayment>()
                    .Where(x => x.StudentProgram.Id == stdProId && x.Status == StudentPayment.EntityStatus.Active)
                    .OrderBy(x => x.Id)
                    .Desc.Take(1).SingleOrDefault<StudentPayment>();
            if (stdPayment == null)
            {
                return "PaymentDue";
            }
            //if (stdPayment.PaymentStatus == PaymentStatus.Paid) return "PaymentClear"; //TODO: Have to check
            return "PaymentDue";
        }
        public int GetAllStudentPaymentListCountBeforeAParticularPayment(StudentPayment studentPayment)
        {
            return
                Session.QueryOver<StudentPayment>()
                    .Where(
                        x => x.CreationDate < studentPayment.CreationDate && x.StudentProgram.Id == studentPayment.StudentProgram.Id && x.PaymentType == studentPayment.PaymentType && x.Status == StudentPayment.EntityStatus.Active)
                    .RowCount();
        }
        public bool isDuplicateTransicationFound(string[] tranArray)
        {
            var criteria = Session.CreateCriteria<StudentPaymentOnlineTransactions>();
            criteria.Add(Restrictions.In("TransactionId", tranArray));
            criteria.Add(Restrictions.Eq("Status", StudentPaymentOnlineTransactions.EntityStatus.Active));
            var list = criteria.List<StudentPaymentOnlineTransactions>();
            if (list.Count > 0)
            {
                return true;
            }
            return false;
        }
        public int GetTransactionReportCount(List<long> organizationIdList, List<long> programIdList, List<long> branchIdList, 
            long[] sessionId, long[] campusId,
            List<AspNetUser> aspnetUsers, long[] userIds, DateTime datef, DateTime datet, int paymentMethod)
        {
            var transactionReportCriteria = GetTransactionReportCriteria(organizationIdList, programIdList, branchIdList, sessionId, campusId, aspnetUsers, userIds, datef, datet, paymentMethod);
            var rowCount = transactionReportCriteria.SetProjection(Projections.RowCount()).UniqueResult<int>();
            return rowCount;
        }

        public int GetMyTransactionReportCount(long currentUserId, long[] branchId, List<long> authorizedBranchIdList, long[] campusId, long[] authorizedCampusIdList, DateTime date)
        {
            var criteria = GetMyTransactionReportCriteria(currentUserId, branchId, authorizedBranchIdList, campusId, authorizedCampusIdList, date);
            return criteria.SetProjection(Projections.RowCount()).UniqueResult<int>();
        }

        public int LoadDueListCount(long programId, long sessionId, long[] brances, long[] campusId, long[] batchName, long[] course,
            DateTime tillDate, string[] batchDays, string[] batchTime)
        {
            string innerQuery = GetInnerQueryDueList(programId, sessionId, brances, campusId, batchName, course, tillDate, batchDays, batchTime);
            string query = @"Select [StudentProgramId],[TransferDate],[FromBatchId],[ToBatchId]
                            INTO #batchTemp
                            From (
	                            Select *, RANK() OVER(PARTITION BY StudentProgramId ORDER BY [TransferDate] DESC) as R  From [dbo].[StudentBatchLog]
	                            Where [TransferDate]<='" + tillDate + @"' 
	                            AND [Status]=" + StudentProgram.EntityStatus.Active + @"
                            ) AS a where a.R=1

                            SELECT count(A.Id) as total FROM ( 
		                            " + innerQuery + @"
	                            ) A 
                            IF EXISTS(SELECT * FROM #batchTemp) DROP TABLE #batchTemp;";
            // string query = "SELECT count(A.Id) as total FROM(" + innerQuery + ") as A WHERE A.R=1 AND A.DueAmount>0";
            IQuery iQuery = Session.CreateSQLQuery(query);
            return Convert.ToInt32(iQuery.UniqueResult());
        }

        public int TotalpayAbleAmount(long stProgramId)
        {
            string query = "select sum(COALESCE([CourseFees],0))-sum(COALESCE([DiscountAmount],0))-sum(COALESCE([SpDiscountAmount],0)) as total from [dbo].[StudentPayment] where [StudentProgramId]=" + stProgramId + "";
            IQuery iQuery = Session.CreateSQLQuery(query);
            return Convert.ToInt32(iQuery.UniqueResult());
        }

        public decimal GetTotalCollectionAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo)
        {
            ICriteria criteria = Session.CreateCriteria<StudentPayment>()
                                    .Add(Restrictions.Eq("Status", StudentPayment.EntityStatus.Active))
                                    .Add(Restrictions.IsNotNull("ReceivedAmount"))
                                    .CreateAlias("StudentProgram", "sp")
                                    .CreateAlias("Batch", "b")
                                    .CreateAlias("b.Branch", "br")
                                    .CreateAlias("b.Program", "p")
                                    .CreateAlias("b.Session", "s")
                                    .CreateAlias("b.Campus", "c")
                                    .Add(Restrictions.Eq("p.Id", programId))
                                    .Add(Restrictions.Eq("s.Id", sessionId))
                                    .Add(Restrictions.Eq("br.Id", branchId))
                                    .Add(Restrictions.Eq("c.Id", campusId));
            criteria.Add(Restrictions.Ge("ReceivedDate", dateFrom));
            criteria.Add(Restrictions.Le("ReceivedDate", dateTo));

            return criteria.SetProjection(Projections.Sum("ReceivedAmount")).UniqueResult<decimal>();
        }

        public decimal GetTotalRefundAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo)
        {
            ICriteria criteria = Session.CreateCriteria<StudentPayment>()
                                    .Add(Restrictions.Eq("Status", StudentPayment.EntityStatus.Active))
                                    .Add(Restrictions.IsNotNull("CashBackAmount"))
                                    .CreateAlias("StudentProgram", "sp")
                                    .CreateAlias("Batch", "b")
                                    .CreateAlias("b.Branch", "br")
                                    .CreateAlias("b.Program", "p")
                                    .CreateAlias("b.Session", "s")
                                    .CreateAlias("b.Campus", "c")
                                    .Add(Restrictions.Eq("p.Id", programId))
                                    .Add(Restrictions.Eq("s.Id", sessionId))
                                    .Add(Restrictions.Eq("br.Id", branchId))
                                    .Add(Restrictions.Eq("c.Id", campusId));
            criteria.Add(Restrictions.Ge("ReceivedDate", dateFrom));
            criteria.Add(Restrictions.Le("ReceivedDate", dateTo));

            return criteria.SetProjection(Projections.Sum("CashBackAmount")).UniqueResult<decimal>();
        }

        #endregion

        #region Helper Function
        private ICriteria GetTransactionReportCriteria(
            List<long> organizationIdList, List<long> programIdList, List<long> branchIdList, 
            long[] sessionId, long[] campusId,
            List<AspNetUser> aspnetUsers, long[] userIds, DateTime datef, DateTime datet, int paymentMethod
           )
        {
            ICriteria criteria =
                Session.CreateCriteria<StudentPayment>()
                    .Add(Restrictions.Not(Restrictions.Eq("Status", StudentPayment.EntityStatus.Delete)));
            criteria.CreateAlias("StudentProgram", "sp");
            criteria.CreateAlias("Batch", "b");
            criteria.CreateAlias("b.Branch", "br");
            criteria.CreateAlias("b.Program", "p");
            criteria.CreateAlias("p.Organization", "o");
            criteria.CreateAlias("b.Session", "s");
            criteria.CreateAlias("b.Campus", "c");
            criteria.Add(Restrictions.In("o.Id", organizationIdList)).Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active));
            criteria.Add(Restrictions.In("p.Id", programIdList)).Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.Add(Restrictions.In("br.Id", branchIdList)).Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));

            if (!sessionId.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("s.Id", sessionId))
                    .Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            }
            if (!campusId.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("c.Id", campusId)).Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));
            }
            
         
            datet = datet.AddDays(1);
            criteria.Add(Restrictions.Ge("CreationDate", datef));
            criteria.Add(Restrictions.Le("CreationDate", datet));
            //criteria.Add(userIds.Contains(SelectionType.SelelectAll)
              //  ? Restrictions.In("CreateBy", aspnetUsers.Select(x => x.Id).ToArray())
               // : Restrictions.In("CreateBy", userIds));
            criteria.Add(Restrictions.In("CreateBy", userIds));
            if (paymentMethod > 0)
            {
                criteria.Add(Restrictions.Eq("PaymentMethod", paymentMethod));
            }
            return criteria;
        }
        private ICriteria GetMyTransactionReportCriteria(long currentUserId, long[] branchId, List<long> authorizedBranches,
            long[] campusId, long[] authorizedCampuses, DateTime date)
        {
            ICriteria criteria =
                Session.CreateCriteria<StudentPayment>()
                    .Add(Restrictions.Not(Restrictions.Eq("Status", StudentPayment.EntityStatus.Delete)));
            //criteria.CreateAlias("StudentProgram", "sp");
            criteria.CreateAlias("Batch", "b");
            criteria.CreateAlias("b.Branch", "br");
            // criteria.CreateAlias("b.Program", "p");
            // criteria.CreateAlias("b.Session", "s");
            criteria.CreateAlias("b.Campus", "c");
            if (!branchId.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("br.Id", branchId)).Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            }
            else
            {
                criteria.Add(Restrictions.In("br.Id", authorizedBranches))
                    .Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            }
            if (!campusId.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("c.Id", campusId)).Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));
            }
            else
            {
                criteria.Add(Restrictions.In("c.Id", authorizedCampuses))
                    .Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));
            }
            criteria.Add(Restrictions.Ge("CreationDate", date));
            criteria.Add(Restrictions.Le("CreationDate", date.AddDays(1)));
            
            criteria.Add(Restrictions.Eq("CreateBy", currentUserId));
            return criteria;
        }

        private string GetInnerQueryDueList(long programId, long sessionId, long[] brances, long[] campusId, long[] batchName, long[] course, DateTime tillDate, string[] batchDays, string[] batchTime)
        {
            string query = "";



            //query +=
            //    "SELECT this_.Id, this_.DueAmount, this_.ReceivedDate, this_.NextReceivedDate, this_.StudentProgramId, " +
            //    " sp1_.PrnNo,sp1_.StudentId, sp1_.InstituteId,b2_.Name as Batch,br3_.Name as Branch," +
            //    " RANK() OVER(PARTITION BY this_.StudentProgramId ORDER BY this_.ReceivedDate DESC) as R" +
            //    " FROM   StudentPayment this_ " +
            //    " inner join StudentProgram sp1_ on this_.StudentProgramId = sp1_.Id"+
            //    " inner join Batch b2_ on this_.CurrentBatchId = b2_.Id" +
            //   // " inner join Batch b2_ on sp1_.BatchId = b2_.Id" +
            //    " inner join Program p4_ on b2_.ProgramId = p4_.Id" +
            //    " inner join Session s5_ on b2_.SessionId = s5_.Id" +  
            //    " inner join Branch br3_ on b2_.BranchId = br3_.Id";

            //string filter = " WHERE not (this_.Status = -404) " +
            //                " and  p4_.Id = " + programId + " and p4_.Status = "+Program.EntityStatus.Active+
            //                " and br3_.Id in (" + string.Join(",", brances) + ") and br3_.Status = " + Branch.EntityStatus.Active +
            //                " and s5_.Id = " + sessionId + " and s5_.Status = " + Model.Entity.Administration.Session.EntityStatus.Active +
            //                " and b2_.Status = "+Batch.EntityStatus.Active;
            //if (!batchName.Contains(SelectionType.SelelectAll))
            //{
            //    filter += " and b2_.Id in (" + string.Join(",", batchName) + ")";
            //}

            //if (!campusId.Contains(SelectionType.SelelectAll))
            //{
            //    query += " inner join Campus c6_ on b2_.CampusId = c6_.Id";
            //    filter += " and c6_.Status = " + Campus.EntityStatus.Active + " and c6_.Id in (" + string.Join(",", campusId) + ")"; 
            //}

            //if (!course.Contains(SelectionType.SelelectAll))
            //{
            //    query += " inner join StudentCourseDetails sc1_ on sp1_.Id = sc1_.StudentProgramId" +
            //             " inner join CourseSubject cs1_ on cs1_.Id = sc1_.CourseSubjectId";
            //    filter += " and sc1_.Status = " + StudentCourseDetail.EntityStatus.Active + " and cs1_.CourseId in (" + string.Join(",", course) + ")";
            //}

            //if (!batchDays.Contains(SelectionType.SelelectAll.ToString()))
            //{
            //    filter += " and b2_.Days in ('" + string.Join("','", batchDays) + "')";
            //}
            //if (batchTime != null && !batchTime.Contains(SelectionType.SelelectAll.ToString()))
            //{
            //    string btQ = "";
            //    int iteration = 0;
            //    foreach (string bt in batchTime)
            //    {
            //        var batchArrays = bt.Replace("To", ",");
            //        var batchArray = batchArrays.Split(',');
            //        var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
            //        var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
            //        if (iteration == 0)
            //        {
            //            btQ += "(b2_.StartTime >= '" + startTime + "' and b2_.EndTime <= '" + endTime + "')";
            //        }
            //        else
            //        {
            //            btQ += " or (b2_.StartTime >= '" + startTime + "' and b2_.EndTime <= '" + endTime + "')";
            //        }
            //        iteration++;
            //    }
            //    filter += " and (" + btQ + ")";
            //}

            //filter += " and this_.ReceivedDate <= '" + tillDate + "'";

            //query += filter +
            //         " group by this_.Id,this_.DueAmount, this_.ReceivedDate, this_.NextReceivedDate, this_.StudentProgramId,sp1_.PrnNo,sp1_.StudentId, sp1_.InstituteId,b2_.Name,br3_.Name";

            #region Filtering and extra INNER JOIN
            string innerJoin = "";
            string filter = @"AND p.Id = " + programId + " AND p.Status = " + Program.EntityStatus.Active + @" 
		                    AND br.Id IN (" + string.Join(",", brances) + ") AND br.Status = " + Branch.EntityStatus.Active + @" 
		                    AND s.Id = " + sessionId + " AND s.Status = " + BusinessModel.Entity.Administration.Session.EntityStatus.Active + @"  
		                    /* AND b.Status = " + Batch.EntityStatus.Active + @" -- Due from Deleted batch need to be checked, so this filter is off */ 
                            ";
            if (!batchName.Contains(SelectionType.SelelectAll))
            {
                filter += " AND b.Id IN (" + string.Join(",", batchName) + ")";
            }
            if (!campusId.Contains(SelectionType.SelelectAll))
            {
                innerJoin += " INNER join Campus c on b.CampusId = c.Id";
                filter += " AND c.Status = " + Campus.EntityStatus.Active + " AND c.Id IN (" + string.Join(",", campusId) + ")";
            }
            if (!course.Contains(SelectionType.SelelectAll))
            {
                query += " INNER join StudentCourseDetails sc on sp.Id = sc.StudentProgramId" +
                         " INNER join CourseSubject cs on cs.Id = sc.CourseSubjectId";
                filter += " AND sc.Status = " + StudentCourseDetail.EntityStatus.Active + " AND cs.CourseId IN (" + string.Join(",", course) + ")";
            }
            if (!batchDays.Contains(SelectionType.SelelectAll.ToString()))
            {
                filter += " AND b.Days IN ('" + string.Join("','", batchDays) + "')";
            }
            if (batchTime != null && !batchTime.Contains(SelectionType.SelelectAll.ToString()))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' AND b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " OR (b.StartTime >= '" + startTime + "' AND b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                filter += " AND (" + btQ + ")";
            }
            #endregion

            var queryString = @"Select A.Id, A.DueAmount, A.ReceivedDate, A.NextReceivedDate, A.StudentProgramId 
		                            ,sp.PrnNo,sp.StudentId, sp.InstituteId,b.Name as Batch,br.Name as Branch
		                            from (
			                            Select A.Id, A.DueAmount, A.ReceivedDate, A.NextReceivedDate, A.StudentProgramId
			                            ,CASE 
				                             WHEN bt.ToBatchId IS NULL THEN A.CurrentBatchId
				                             WHEN bt.TransferDate >= A.ReceivedDate THEN bt.ToBatchId
				                             ELSE A.CurrentBatchId
			                              END
			                              AS BatchId  
			                            from (
				                            Select * from (
					                            Select spay.Id, spay.DueAmount, spay.ReceivedDate, spay.NextReceivedDate, spay.StudentProgramId, spay.CurrentBatchId, RANK() OVER(PARTITION BY spay.StudentProgramId ORDER BY spay.ReceivedDate DESC) as R 
					                            from [dbo].[StudentPayment] spay
					                            Where  not (Status = -404) 
					                            AND ReceivedDate <= '" + tillDate + @"' 
				                            ) as A Where A.R = 1 AND A.DueAmount>0 
			                            ) AS A
			                            Left Join #batchTemp bt ON bt.StudentProgramId  = A.StudentProgramId
		                            ) A
		                            INNER join StudentProgram sp on A.StudentProgramId = sp.Id
		                            INNER join Batch b on A.BatchId = b.Id 
		                            INNER join Program p on b.ProgramId = p.Id 
		                            INNER join [Session] s on b.SessionId = s.Id 
		                            INNER join Branch br on b.BranchId = br.Id 
		                            " + innerJoin + @"
		                            WHERE 1=1
		                            " + filter;
            return queryString;
        }

        #endregion
    }
}