﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IProgramSettingDao : IBaseDao<ProgramSettings, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<ProgramSettings> LoadProgramSetting(long programId, long sessionId);
        IList<ProgramSettings> LoadByNextProgramIdAndNextSessionId(long programId, long sessionId);
        IList<ProgramSettings> LoadPreviousProgramSettings(long programId, long sessionId);
        #endregion

        #region Others Function
        int GetProgramSettingCount(long programId, long sessionId);
        bool CheckDuplicateEntry(long programId, long sessionId, long nextProgramId, long nextSessionId, long programSettingId);
        bool CheckReverseEntry(long programId, long sessionId, long nextProgramId, long nextSessionId);

        #endregion


    }
    public class ProgramSettingDao : BaseDao<ProgramSettings, long>, IProgramSettingDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function


        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<ProgramSettings> LoadProgramSetting(long programId, long sessionId)
        {
            var criteria = GetProgramSettingQuery(programId, sessionId);
            return criteria.List<ProgramSettings>().ToList();
        }

        public IList<ProgramSettings> LoadByNextProgramIdAndNextSessionId(long programId, long sessionId)
        {
            var criteria = Session.CreateCriteria<ProgramSettings>().Add(Restrictions.Not(Restrictions.Eq("Status", ProgramSettings.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("NextProgram.Id", programId));
            criteria.Add(Restrictions.Eq("NextSession.Id", sessionId));

            return criteria.List<ProgramSettings>().ToList();
        }
        public IList<ProgramSettings> LoadPreviousProgramSettings(long programId, long sessionId)
        {
            return Session.QueryOver<ProgramSettings>().Where(x =>x.Status == ProgramSettings.EntityStatus.Active && 
                    x.NextProgram.Id == programId &&x.NextSession.Id == sessionId).List<ProgramSettings>();
        }

        #endregion

        #region Others Function
     
        public int GetProgramSettingCount(long programId, long sessionId)
        {
            var criteria = GetProgramSettingQuery(programId, sessionId);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public bool CheckDuplicateEntry(long programId, long sessionId, long nextProgramId, long nextSessionId, long programSettingId)
        {
            var criteria = Session.CreateCriteria<ProgramSettings>().Add(Restrictions.Not(Restrictions.Eq("Status", ProgramSettings.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            criteria.Add(Restrictions.Eq("NextProgram.Id", nextProgramId));
            criteria.Add(Restrictions.Eq("NextSession.Id", nextSessionId));
            if (programSettingId != 0)
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", programSettingId)));
            criteria.SetProjection(Projections.RowCount());
            if (Convert.ToInt32(criteria.UniqueResult()) > 0)
                return true;
            return false;
        }

        public bool CheckReverseEntry(long programId, long sessionId, long nextProgramId, long nextSessionId)
        {
            var criteria = Session.CreateCriteria<ProgramSettings>().Add(Restrictions.Not(Restrictions.Eq("Status", ProgramSettings.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("Program.Id", nextProgramId));
            criteria.Add(Restrictions.Eq("Session.Id", nextSessionId));
            criteria.Add(Restrictions.Eq("NextProgram.Id", programId));
            criteria.Add(Restrictions.Eq("NextSession.Id", sessionId));
            criteria.SetProjection(Projections.RowCount());
            if (Convert.ToInt32(criteria.UniqueResult()) > 0)
                return true;
            return false;
        }

        #endregion

        #region Helper Function
        private ICriteria GetProgramSettingQuery(long programId, long sessionId)
        {
            var criteria = Session.CreateCriteria<ProgramSettings>().Add(Restrictions.Not(Restrictions.Eq("Status", ProgramSettings.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            return criteria;
        }
        #endregion

    }
}
