﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qmaster.Print.Model
{
    public class PrintConfigInfo
    {
        public virtual bool IsBarCode { get; set; }
        public virtual bool IsQrCode { get; set; }
        public virtual bool IsPrint { get; set; }
        public virtual bool DownloadNewFile { get; set; }
        public virtual bool IsDeleteAllGeneratedFile { get; set; }
        public virtual bool IsDeleteAllMargeFile { get; set; }
        public virtual bool IsDeleteUnzipFile { get; set; }
        public virtual bool IsDeleteZipFile { get; set; }
        public virtual bool ShowBarNo { get; set; }
        public virtual int BarWidth { get; set; }
        public virtual int BarHeight { get; set; }
        public virtual int QrWidth { get; set; }
        public virtual int QrHeight { get; set; }
        public virtual int BarDipX { get; set; }
        public virtual int BarDipY { get; set; }
        public virtual int QrDipX { get; set; }
        public virtual int QrDipY { get; set; }
        public virtual int QrAxis { get; set; }
        public virtual int QrYaxis { get; set; }
        public virtual int BarAxis { get; set; }
        public virtual int BarYaxis { get; set; }
        public virtual int MinPages { get; set; }
        public virtual int MaxPages { get; set; }
        public virtual int MargePdf { get; set; }
        public virtual int ShowBarQrCodeOnPage { get; set; }
        public virtual bool IsOsl { get; set; }
        public virtual bool IsAlpha { get; set; }
        public virtual bool ShowCodeNumber { get; set; }
        public virtual bool ShowCodeNumberWithExamNo { get; set; }
        public virtual int ShowCodeNumberOnPage { get; set; }
        public virtual int CodeNumberX { get; set; }
        public virtual int CodeNumberY { get; set; }
        public virtual int ShowCodeNumberOnPage2Nd { get; set; }
        public virtual int CodeNumberX2Nd { get; set; }
        public virtual int CodeNumberY2Nd{ get; set; }
    }

    public class MargePdfSartEndCompare
    {
        public virtual int Start { get; set; }
        public virtual int End { get; set; }
        public virtual int Compare { get; set; }
    }
}
