﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.SpecialAllowanceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "SpecialAllowanceSetting")]
    public class SpecialAllowanceSaveTest : SpecialAllowanceBaseTest
    {
        #region Basic Test

        [Fact]
        public void Save_SpecialAllowance_Successfully()
        {
            const string effectiveDate = "FEB 2017";

            var teamMember = PersistTeamMember(MentorId);
            Branch branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).SingleOrDefault();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, branch);

            _specialAllowanceSettingFactory.Create(teamMember.Pin).WithAmount(2000)
                .WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate)
                .WithRemarks("TEST REMARK");
            _specialAllowanceSettingService.SaveOrUpdate(userMenu, _specialAllowanceSettingFactory.Object);
            Assert.True(_specialAllowanceSettingFactory.Object.Id > 0);
        }

        #endregion

        #region Business logic test

        [Fact]
        public void SpecialAllowance_Should_Failed_WithOut_UserMenu()
        {
            const string effectiveDate = "Oct 2016";
            var teamMember = PersistTeamMember(MentorId);
            _specialAllowanceSettingFactory.Create(teamMember.Pin).WithAmount(2000)
                .WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate)
                .WithRemarks("TEST REMARK");
            Assert.Throws<InvalidDataException>(() => _specialAllowanceSettingService.SaveOrUpdate(new List<UserMenu>(), _specialAllowanceSettingFactory.Object));
        }

        [Fact]
        public void SpecialAllowance_Should_Be_Failed_Date_Not_In_Right_Order()
        {
            const string effectiveDate = "Oct 2016";

            var teamMember = PersistTeamMember(MentorId);
            Branch branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).SingleOrDefault();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, branch);

            _specialAllowanceSettingFactory.Create(teamMember.Pin).WithAmount(2000)
                .WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate, "Sep 2016")
                .WithRemarks("TEST REMARK");
            Assert.Throws<InvalidDataException>(() => _specialAllowanceSettingService.SaveOrUpdate(userMenu, _specialAllowanceSettingFactory.Object));
        }

        [Fact]
        public void SpecialAllowance_Should_Be_Failed_WithOut_Pin()
        {
            var organization = organizationFactory.Create().Persist().Object;
            var branch = branchFactory.Create().WithOrganization(organization).Persist().Object;
            var userMenu = BuildUserMenu(organization, (Program)null, branch);
            const string effectiveDate = "Oct 2016";
            _specialAllowanceSettingFactory.Create(0)
                .WithAmount(2000)
                .WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate)
                .WithRemarks("ss");
            Assert.Throws<NullObjectException>(() => _specialAllowanceSettingService.SaveOrUpdate(userMenu, _specialAllowanceSettingFactory.Object));
        }

        [Fact]
        public void SpecialAllowance_Should_Be_Failed_WithOut_Amount()
        {
            const string effectiveDate = "Oct 2016";
            var teamMember = PersistTeamMember(MentorId);
            Branch branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).SingleOrDefault();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, branch);
            _specialAllowanceSettingFactory.Create(teamMember.Pin)
                .WithAmount(0)
                .WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate)
                .WithRemarks("ss");
            Assert.Throws<InvalidDataException>(() => _specialAllowanceSettingService.SaveOrUpdate(userMenu, _specialAllowanceSettingFactory.Object));
        }

        #endregion
    }
}
