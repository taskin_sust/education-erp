using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface ISupplierItemDao : IBaseDao<SupplierItem, long>
    {
        #region Operational Function
        void Delete(IList<SupplierItem> removableSupplierItem);

        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        IList<SupplierItem> AssignedThisItemToSupplier(long itemId);

        IList<SupplierItem> LoadSupplierByItemIds(string[] itemIds);


        #endregion

        #region Others Function
        #endregion











    }

    public class SupplierItemDao : BaseDao<SupplierItem, long>, ISupplierItemDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public void Delete(IList<SupplierItem> removableSupplierItem)
        {
            foreach (var removableSupplier in removableSupplierItem)
            {
                Delete(removableSupplier);
            }
        }

        public IList<SupplierItem> AssignedThisItemToSupplier(long itemId)
        {
            return Session.QueryOver<SupplierItem>().Where(x => x.Item.Id == itemId).List<SupplierItem>();
        }

        public IList<SupplierItem> LoadSupplierByItemIds(string[] itemIds)
        {
            ICriteria criteria = Session.CreateCriteria<SupplierItem>();
            criteria.Add(Restrictions.In("Item.Id", itemIds));
            return criteria.List<SupplierItem>();
        }



        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion


    }
}
