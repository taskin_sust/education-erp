using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class ReferrerMap : BaseClassMap<Referrer, long>
    {
        
        public ReferrerMap() {
			Table("Referrer");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name);
			Map(x => x.Code).Column("Code");
			Map(x => x.Rank).Column("Rank");
            Map(x => x.IsEditable).Column("IsEditable");


            HasMany(x => x.StudentPayments).KeyColumn("SpReferenceId").Cascade.All();
        }
    }
}
