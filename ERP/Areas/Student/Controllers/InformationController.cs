﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using FluentNHibernate.Testing.Values;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_code;
using UdvashERP.Areas.Student.Models;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.com.onnorokomsms.api2;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services;
using UdvashERP.Services.Administration;
using UdvashERP.App_Start;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Sms;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using Constants = UdvashERP.BusinessRules.Constants;
using InstituteService = UdvashERP.Services.Students.InstituteService;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessRules.Student;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class InformationController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("StudentArea");
        private void LogError(string message, Exception exception)
        {
            if (exception != null)
            {
                _logger.Error(message, exception);
            }
            else
            {
                _logger.Error(message);
            }
        }

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly StudentProgramView _studentProgramView;
        private readonly StudentInformationView _studentInformationView;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IInformationService _informationService;
        private readonly IProgramService _programService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly IBatchService _batchService;
        private IList<ProgramBranchSession> _programBranchSessionList;
        private readonly IStudentService _studentService;
        private readonly ISessionService _sessionService;
        private IStudentImageService _studentImageService;
        private readonly ICourseService _courseService;
        private readonly ICampusService _campusService;
        private readonly IBranchService _branchService;
        private readonly IMaterialDistributionService _materialDistributionService;
        private readonly INstituteService _instituteService;
        private readonly AddressService _districtService;
        private readonly IStudentExamService _studentExamService;
        private readonly IStudentBoardService _studentBoardService;
        private readonly IStudentUniversitySubjectService _studentUniversitySubjectService;
        private readonly IOrganizationService _organizationService;
        private readonly IStudentInfoUpdateLogService _studentInfoUpdateLog;
        private ICommonHelper _commonHelper;
        private readonly ISmsService _smsService;
        private readonly IUserService _userService;
        private readonly IStudentImagesMediaService _studentImagesMediaService;
        private readonly IStudentAcademicInfoService _studentAcademicInfoService;
        private readonly ISmsSenderService _smsSenderService;
        private readonly ISmsHistoryServices _smsHistoryService;
        private readonly IStudentBatchLogService _studentBatchLogService;
        private readonly IInstitutionService _institutionService;
        //authorization
        private List<UserMenu> _userMenu;

        public InformationController()
        {
            //open nHibernate session
            var nHSession = NHibernateSessionFactory.OpenSession();
            _studentProgramService = new StudentProgramService(nHSession);
            _informationService = new InformationService(nHSession);
            _programService = new ProgramService(nHSession);
            _studentProgramView = new StudentProgramView();
            _studentInformationView = new StudentInformationView();
            _programBranchSessionService = new ProgramBranchSessionService(nHSession);
            _batchService = new BatchService(nHSession);
            _campusService = new CampusService(nHSession);
            _programBranchSessionList = new List<ProgramBranchSession>();
            _courseService = new CourseService(nHSession);
            _studentService = new StudentService(nHSession);
            _studentImageService = new StudentImageService(nHSession);
            _sessionService = new SessionService(nHSession);
            _branchService = new BranchService(nHSession);
            _sessionService = new SessionService(nHSession);
            _instituteService = new InstituteService(nHSession);
            _districtService = new AddressService(nHSession);
            _materialDistributionService = new MaterialDistributionService(nHSession);
            _studentExamService = new StudentExamService(nHSession);
            _studentBoardService = new StudentBoardService(nHSession);
            _studentUniversitySubjectService = new StudentUniversitySubjectService(nHSession);
            _organizationService = new OrganizationService(nHSession);
            _studentInfoUpdateLog = new StudentInfoUpdateLogService(nHSession);
            _smsService = new SmsService(nHSession);
            _userService = new UserService(nHSession);
            _commonHelper = new CommonHelper();
            _studentImagesMediaService = new StudentImagesMediaService();
            _studentAcademicInfoService = new StudentAcademicInfoService(nHSession);
            _smsSenderService = new SmsSenderService();
            _smsHistoryService = new SmsHistoryServices();
            _studentBatchLogService = new StudentBatchLogService(nHSession);
             _institutionService = new UdvashERP.Services.Administration.InstituteService(nHSession);

        }
        #endregion

        #region Information Search
        private bool PrnNoValidityCheck(string PrnNo)
        {
            return PrnNo.Length.ToString() == Constants.PrnNoLength;
        }

        [HttpGet]
        public ActionResult InformationSearch()
        {
            return View();
        }
        private void InformationCommon(string PrnNo)
        {
            if (PrnNo == null)
            {
                throw new MessageException("You need to insert student program roll no.");
            }
            if (PrnNoValidityCheck(PrnNo) == false)
            {
                throw new MessageException("Insert valid program roll no");
            }
            //load student program by prn no
            _studentProgramView.StudentProgram = _studentProgramService.GetStudentProgram(PrnNo);
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            if (_studentProgramView.StudentProgram == null)
            {
                throw new MessageException("Program roll number is not matching with any student");
            }

            #region Authorization Check
            //check authorization
            var message = _studentProgramService.CheckAuthorizationForStudentProgram(_userMenu, _studentProgramView.StudentProgram);
            if (message != "")
                throw new MessageException("You are not authorize person to search this student");

            #endregion

            #region Assign Data For View

            if (_studentProgramView.StudentProgram.Student.DateOfBirth != null)
            {
                DateTime? dateOfBirth = _studentProgramView.StudentProgram.Student.DateOfBirth;
                var dateOfBirthString = dateOfBirth.Value.Day + "/" + dateOfBirth.Value.Month + "/" + dateOfBirth.Value.Year;
                _studentInformationView.DateOfBirth = dateOfBirthString;
            }

            _studentInformationView.PrnNo = PrnNo;
            //casting student admission & sif information
            _studentInformationView.Student = _studentProgramView.StudentProgram.Student;
            _studentInformationView.StudentProgram = _studentProgramView.StudentProgram;
            _studentInformationView.Session = _studentProgramView.StudentProgram.Batch.Session;
            _studentInformationView.Branch = _studentProgramView.StudentProgram.Batch.Branch;
            _studentInformationView.Campus = _studentProgramView.StudentProgram.Batch.Campus;
            _studentInformationView.Program = _studentProgramView.StudentProgram.Program;
            _studentInformationView.Batch = _studentProgramView.StudentProgram.Batch;

            #region Working With Batch

            var organizationId = _studentInformationView.Program.Organization.Id;
            var programId = _studentInformationView.Program.Id;
            var sessionId = _studentInformationView.Session.Id;

            var branchIds = _commonHelper.ConvertIdToList(_studentInformationView.Branch.Id);
            var campusIds = _commonHelper.ConvertIdToList(_studentInformationView.Campus.Id);
            IList<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), branchIds, _commonHelper.ConvertIdToList(sessionId), campusIds);

            ViewBag.BatchDays = batchList.DistinctBy(x => x.Days).OrderBy(y => y.Rank).ToList();
            ViewBag.BatchTimes = batchList.Where(x => x.Days == _studentInformationView.Batch.Days).DistinctBy(x => x.FormatedBatchTime).OrderBy(y => y.Rank).ToList();
            ViewBag.BatchList = batchList.Where(x => x.Days == _studentInformationView.Batch.Days && x.BatchTime == _studentInformationView.Batch.BatchTime).DistinctBy(x => x.Name).OrderBy(y => y.Rank).ToList();



            //end complete

            #endregion

            //get information about previous academic result
            _studentInformationView.StudentAcademicInfos = _studentProgramView.StudentProgram.Student.StudentAcademicInfos.Where(x => x.Status == StudentAcademicInfo.EntityStatus.Active).ToList();
            //getting student admission information 
            _studentInformationView.StudentUniversityInfos = _studentProgramView.StudentProgram.Student.StudentUniversityInfos.ToList();
            #endregion

            #region Initialize Student Update

            InitializeStudentUpdate();

            #endregion
        }
        [HttpPost]
        public ActionResult InformationSearch(string PrnNo)
        {
            try
            {
                InformationCommon(PrnNo);
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem occurred during search student" + ex.Message;
                return View();
            }
            return View("InformationUpdate", _studentInformationView);
        }
        [HttpGet]
        public ActionResult InformationView(string PrnNo)
        {
            try
            {
                InformationCommon(PrnNo);
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                TempData["InformationSearchError"] = ex.Message;
                return RedirectToAction("InformationSearch");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem occurred during search student" + ex.Message;
                TempData["InformationSearchError"] = "Problem occurred during search student" + ex.Message;
                return RedirectToAction("InformationSearch");
            }
            return View("InformationUpdate", _studentInformationView);
        }
        //initialize student update
        private void InitializeStudentUpdate()
        {

            #region Student Exam List
            IList<StudentExam> studentExamList = _studentExamService.LoadActive();
            if (studentExamList != null)
            {
                ViewBag.StudentExams = studentExamList;
            }
            else
            {
                ViewBag.StudentExams = new SelectList(null, "Id", "Name");
            }
            #endregion

            #region Student Board List
            IList<StudentBoard> studentBoardList = _studentBoardService.LoadActive();
            if (studentBoardList != null)
            {
                ViewBag.StudentBoard = studentBoardList;
            }
            else
            {
                ViewBag.StudentBoard = new SelectList(null, "Id", "Name");
            }
            #endregion

            #region Student University Subject
            IList<StudentUniversitySubject> studentUniversitySubjectList = _studentUniversitySubjectService.LoadActive();

            if (studentUniversitySubjectList != null)
            {
                ViewBag.StudentAdmissionSubject = studentUniversitySubjectList;
            }
            else
            {
                ViewBag.StudentAdmissionSubject = new SelectList(null, "Id", "Name");
            }
            #endregion
        }

        #endregion

        #region Information Update

        [HttpGet]
        public ActionResult InformationUpdate()
        {
            return RedirectToAction("InformationSearch");
            //return View();
        }

        [HttpPost]
        public ActionResult InformationUpdate(StudentInformationView studentInformation)
        {
            InitializeStudentUpdate();
            try
            {
                StudentProgram oldStudentProgram = (StudentProgram)_studentProgramService.GetStudentProgram(studentInformation.PrnNo).Clone();
                var oldBatch = oldStudentProgram.Batch;
                var oldNickName = oldStudentProgram.Student.NickName;
                var oldMobile = oldStudentProgram.Student.Mobile;
                var oldGender = oldStudentProgram.Student.Gender;
                var oldReligion = oldStudentProgram.Student.Religion;
                var oldFullName = oldStudentProgram.Student.FullName;
                var oldFatherName = oldStudentProgram.Student.FatherName;
                var oldDateOfBirth = oldStudentProgram.Student.DateOfBirth;
                var oldGuardiansMobile1 = oldStudentProgram.Student.GuardiansMobile1;
                var oldGuardiansMobile2 = oldStudentProgram.Student.GuardiansMobile2;
                var oldBloodGroup = oldStudentProgram.Student.BloodGroup;
                var oldEmail = oldStudentProgram.Student.Email;
                var oldLastMeritPosition = oldStudentProgram.Student.LastMeritPosition;
                var oldGpa = oldStudentProgram.Student.Gpa;
                var oldExam = oldStudentProgram.Student.Exam;
                var oldSection = oldStudentProgram.Student.Section;
                var oldRegistrationNo = oldStudentProgram.Student.RegistrationNo;  //Maybe Useless Check                            
                var oldDistrict = oldStudentProgram.Student.District;
                var oldInstitute = oldStudentProgram.Institute;
                BusinessModel.Entity.Students.Student studentObj = studentInformation.Student;

                if (studentInformation.DateOfBirth.IsNullOrWhiteSpace() != true)
                {
                    string[] splitDate = studentInformation.DateOfBirth.Split('/');
                    if (splitDate.Length == 3 && Convert.ToInt16(splitDate[2]) > 1980 &&
                        Convert.ToInt16(splitDate[1]) > 0 && Convert.ToInt16(splitDate[2]) > 0)
                    {
                        DateTime birthDay = new DateTime(Convert.ToInt16(splitDate[2]), Convert.ToInt16(splitDate[1]),
                            Convert.ToInt16(splitDate[0]));
                        studentInformation.Student.DateOfBirth = birthDay;
                    }
                }
                else
                {
                    studentInformation.Student.DateOfBirth = null;
                }

                studentInformation.StudentProgram.PrnNo = studentInformation.PrnNo;
                studentInformation.StudentProgram.Batch = studentInformation.Batch;
                studentObj.StudentPrograms.Add(studentInformation.StudentProgram);
                //StudentProgram studentProgramObj = studentInformation.StudentProgram;
                if (studentInformation.StudentAcademicInfos != null && studentInformation.StudentAcademicInfos.Count > 0)
                {
                    IList<StudentAcademicInfo> studentAcademicInfos = studentInformation.StudentAcademicInfos.Where(x => x.StudentExam.Id > 0 && x.StudentBoard.Id > 0 && !String.IsNullOrEmpty(x.Year)).ToList();
                    studentObj.StudentAcademicInfos = studentAcademicInfos;
                }
                if (studentInformation.StudentUniversityInfos != null && studentInformation.StudentUniversityInfos.Count > 0)
                {
                    IList<StudentUniversityInfo> studentUniversityInfos = studentInformation.StudentUniversityInfos;
                    studentObj.StudentUniversityInfos = studentUniversityInfos;
                }
                bool isSuccess = _informationService.Update(studentObj);
                var updateStudentProgramView = new StudentProgramView();

                #region After Update Student

                if (isSuccess)
                {

                    //initialize student update
                    InitializeStudentUpdate();
                    updateStudentProgramView.StudentProgram = _studentProgramService.GetStudentProgram(studentInformation.PrnNo);
                    if (_studentProgramView.StudentProgram != null)
                    {
                        if (studentObj.District.Id == 0) { studentObj.District = null; }
                        //if (studentObj.District.Id == 0) { studentObj.District = null; }
                        if (studentObj.NickName != oldNickName
                            || studentObj.Mobile != oldMobile
                            || studentObj.Gender != oldGender
                            || studentObj.Religion != oldReligion
                            || studentObj.FullName != oldFullName
                            || studentObj.FatherName != oldFatherName
                            || studentObj.DateOfBirth != oldDateOfBirth
                            || studentObj.GuardiansMobile1 != oldGuardiansMobile1
                            || studentObj.GuardiansMobile2 != oldGuardiansMobile2
                          //  || studentObj.BloodGroup != oldBloodGroup
                            || updateStudentProgramView.StudentProgram.Student.Email != oldEmail
                            || studentObj.LastMeritPosition != oldLastMeritPosition
                            || studentObj.Exam != oldExam
                            || studentObj.Section != oldSection
                            || updateStudentProgramView.StudentProgram.Student.RegistrationNo != oldRegistrationNo
                            || (studentObj.District != null && studentObj.District.Id != oldDistrict.Id)
                            || updateStudentProgramView.StudentProgram.Institute != oldInstitute
                        )
                        {
                            // student Info Update Log
                            StudentInfoUpdateLog studentInfoUpdateLog = new StudentInfoUpdateLog();
                            studentInfoUpdateLog.CreationDate = DateTime.Now;
                            studentInfoUpdateLog.CreateBy = Convert.ToInt64(IdentityExtensions.GetUserId(User.Identity));

                            studentInfoUpdateLog.NickName = studentObj.NickName;
                            studentInfoUpdateLog.Mobile = studentObj.Mobile;
                            studentInfoUpdateLog.Gender = studentObj.Gender;
                            studentInfoUpdateLog.Religion = studentObj.Religion;
                            studentInfoUpdateLog.FullName = studentObj.FullName;
                            studentInfoUpdateLog.FatherName = studentObj.FatherName;
                            studentInfoUpdateLog.DateOfBirth = studentObj.DateOfBirth;
                            studentInfoUpdateLog.GuardiansMobile1 = studentObj.GuardiansMobile1;
                            studentInfoUpdateLog.GuardiansMobile2 = studentObj.GuardiansMobile2;
                            studentInfoUpdateLog.BloodGroup = studentObj.BloodGroup;
                            studentInfoUpdateLog.Email = updateStudentProgramView.StudentProgram.Student.Email;//studentObj.Email;
                            studentInfoUpdateLog.LastMeritPosition = studentObj.LastMeritPosition;
                            studentInfoUpdateLog.Gpa = studentObj.Gpa;
                            studentInfoUpdateLog.Exam = studentObj.Exam;
                            studentInfoUpdateLog.Section = studentObj.Section;
                            studentInfoUpdateLog.StudentProgramId = updateStudentProgramView.StudentProgram.Id;
                            studentInfoUpdateLog.RegistrationNo = updateStudentProgramView.StudentProgram.Student.RegistrationNo;
                            if (studentObj.District != null)
                                studentInfoUpdateLog.District = Convert.ToInt32(studentObj.District.Id);

                            //NEW FIELDS
                            studentInfoUpdateLog.StudentId = studentObj.Id;
                            if (updateStudentProgramView.StudentProgram.Institute != null)
                                studentInfoUpdateLog.Institute = updateStudentProgramView.StudentProgram.Institute.Name;
                            studentInfoUpdateLog.InfoStatus = 2; //FOR UPDATE = 2, FOR Admission = 1

                            _studentInfoUpdateLog.SaveLog(studentInfoUpdateLog);
                        }

                        if (oldBatch.Id != updateStudentProgramView.StudentProgram.Batch.Id)
                        {
                            StudentBatchLog studentBatchLog = new StudentBatchLog();
                            studentBatchLog.FromBatch = oldBatch;
                            studentBatchLog.ToBatch = updateStudentProgramView.StudentProgram.Batch;
                            studentBatchLog.StudentProgram = updateStudentProgramView.StudentProgram;
                            studentBatchLog.TransferDate = DateTime.Now;
                            _studentBatchLogService.Save(studentBatchLog);
                        }

                        //set student prn no
                        _studentInformationView.PrnNo = studentInformation.PrnNo;
                        //casting student admission & sif information
                        _studentInformationView.Student = updateStudentProgramView.StudentProgram.Student;
                        _studentInformationView.StudentProgram = updateStudentProgramView.StudentProgram;
                        _studentInformationView.Session = updateStudentProgramView.StudentProgram.Batch.Session;
                        _studentInformationView.Program = updateStudentProgramView.StudentProgram.Program;
                        _studentInformationView.Branch = updateStudentProgramView.StudentProgram.Batch.Branch;
                        _studentInformationView.Campus = updateStudentProgramView.StudentProgram.Batch.Campus;
                        _studentInformationView.Batch = updateStudentProgramView.StudentProgram.Batch;

                        #region Working With Batch

                        var organizationId = _studentInformationView.Program.Organization.Id;
                        var programId = _studentInformationView.Program.Id;
                        var sessionId = _studentInformationView.Session.Id;
                        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                        var branchIds = _commonHelper.ConvertIdToList(_studentInformationView.Branch.Id);
                        var campusIds = _commonHelper.ConvertIdToList(_studentInformationView.Campus.Id);
                        IList<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), branchIds, _commonHelper.ConvertIdToList(sessionId), campusIds);
                        ViewBag.BatchDays = batchList.DistinctBy(x => x.Days).OrderBy(y => y.Rank).ToList();
                        ViewBag.BatchTimes = batchList.Where(x => x.Days == _studentInformationView.Batch.Days).DistinctBy(x => x.FormatedBatchTime).OrderBy(y => y.Rank).ToList();
                        ViewBag.BatchList = batchList.Where(x => x.Days == _studentInformationView.Batch.Days && x.BatchTime == _studentInformationView.Batch.BatchTime).DistinctBy(x => x.Name).OrderBy(y => y.Rank).ToList();


                        #endregion

                        //getting student admission information 
                        _studentInformationView.StudentUniversityInfos =
                            updateStudentProgramView.StudentProgram.Student.StudentUniversityInfos.ToList();
                        //get information about previous academic result
                        _studentInformationView.StudentAcademicInfos =
                            updateStudentProgramView.StudentProgram.Student.StudentAcademicInfos.Where(x => x.Status == StudentAcademicInfo.EntityStatus.Active).ToList();
                    }
                    ViewBag.SuccessMessage = "Student information successfully updated.";
                }
                #endregion

                //InitializeStudentUpdate();
                //return View(_studentInformationView);
            }
            catch (EmptyFieldException ex)
            {
                InformationCommon(studentInformation.PrnNo);
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception)
            {
                InformationCommon(studentInformation.PrnNo);
                ViewBag.ErrorMessage = "Problem Occurred during update student information";
                //LogError("Error creating session", ex);
                //return View(studentInformation);
            }
            return View(_studentInformationView);
        }
        #endregion

        #region Ajax Request Function
        [HttpPost]
        public ActionResult GetInstitute(string query, bool isUniversity = false, string isEiin = "")
        {
            try
            {
                if (String.IsNullOrEmpty(isEiin))
                {
                    SelectList instituteSelectList = new SelectList(_instituteService.LoadInstituteByQuery(query, isUniversity), "Id", "Name");
                    List<SelectListItem> instituteList = instituteSelectList.ToList();
                    return Json(new { returnList = instituteList, IsSuccess = true });
                }
                else
                {
                    if (isEiin == "Id")
                    {
                        var institute = _institutionService.LoadById(Convert.ToInt64(query));
                        if (institute != null)
                        {
                            return Json(new Response(true, institute.Eiin));
                        }
                    }
                    else
                    {
                        var institute = _institutionService.GetInstitute(query);
                        Dictionary<string, string> instituteInfo = new Dictionary<string, string>(); 
                        if (institute != null)
                        {
                            instituteInfo.Add("Name", institute.Name);
                            instituteInfo.Add("Id", institute.Id.ToString());
                            return Json(new { returnList = instituteInfo, IsSuccess = true });
                        }
                    }

                    return Json(new Response(false, "Institute Not Found!!"));
                }
               
            }
            catch (Exception e)
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult GetDistrict(string query)
        {
            try
            {
                // IList<District> districtList = _districtService
                SelectList districtSelectList = new SelectList(_districtService.LoadDistrictByQuery(query), "Id", "Name");
                List<SelectListItem> districtList = districtSelectList.ToList();
                return Json(new { returnList = districtList, IsSuccess = true });
            }
            catch (Exception e)
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult GetProgram(long organizationId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.ProgramList = _programService.LoadAuthorizedProgram(userMenu, _commonHelper.ConvertIdToList(organizationId));
                return View("Partial/_ProgramList");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult GetSession(long programId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.SessionList = _sessionService.LoadAuthorizedSession(userMenu, null, _commonHelper.ConvertIdToList(programId));
                return View("Partial/_SessionList");
            }
            catch (Exception ex)
            {
                _logger.Error("Batch. AjaxRequestForBranch.", ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public JsonResult GetCourse(long programId, long sessionId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var courseSelectList = new SelectList(_courseService.LoadCourse(programId, sessionId), "Id", "Name");
                    List<SelectListItem> courseList = courseSelectList.ToList();
                    if (courseList.Count > 0)
                        courseList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "ALL COURSE" });
                    return Json(new { courseList = courseList, IsSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error("Information. GetCourse", ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult GetBranch(long programId, long sessionId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<SelectListItem> branchList = (new SelectList(_branchService.LoadAuthorizedBranch(userMenu, null, _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId)).ToList(), "Id", "Name")).ToList();
                if (branchList.Any())
                    branchList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Branch" });
                return Json(new { branchList = branchList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error("Batch. AjaxRequestForBranch.", ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult GetCampus(long programId, long sessionId, long[] branchId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                var campusList = _campusService.LoadAuthorizeCampus(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId));
                List<SelectListItem> campusSelecteList = (new SelectList(campusList.OrderBy(x => x.Rank).ToList(), "Id", "Name")).ToList();
                if (campusList.Any())
                    campusSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All CAMPUS" });

                return Json(new { campusList = campusSelecteList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error("Information. GetCampus.", ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public ActionResult GetBatchDaysForGender(long programId, long sessionId, long[] branchId, long[] campusId, int genderId, int versionOfStudy)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var batchList = _batchService.LoadAuthorizeBatchGroupByDays(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), _commonHelper.ConvertIdToList(versionOfStudy), _commonHelper.ConvertIdToList(genderId));

                var batchDaysSelectList = new SelectList(batchList);
                List<SelectListItem> batchdays = batchDaysSelectList.ToList();
                if (batchdays.Any())
                    batchdays.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Days" });
                return Json(new { batchdays = batchdays, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error("Batch. AjaxRequestForBatchDays.", ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult GetBatchDay(long programId, long sessionId, long[] branchId, long[] campusId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var batchList = _batchService.LoadAuthorizeBatchGroupByDays(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList());
                //Check by Batch Day
                //IList<string> batchDayList = (from x in batchList select x.Days).Distinct().ToList();
                List<SelectListItem> batchSelecteList = (new SelectList(batchList)).ToList();
                if (batchSelecteList.Any())
                    batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Day" });

                return Json(new { batchDays = batchSelecteList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error("Batch. AjaxRequestForBatchDay.", ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]

        public ActionResult GetBatchTime(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var batchList = _batchService.LoadAuthorizeBatchGroupByTimes(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays);
                var batchTimeSelectList = new SelectList(batchList);
                List<SelectListItem> batchTimes = batchTimeSelectList.ToList();
                if (batchTimes.Any())
                    batchTimes.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Time" });

                return Json(new { batchTime = batchTimes, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error("Batch. AjaxRequestForBatchTime.", ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]

        public ActionResult GetBatch(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, string[] batchTimeText, List<int> selectedVersions = null, List<int> selectedGenders = null)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var batchList = new List<Batch>();

                if (selectedGenders != null && selectedVersions != null)
                {
                    batchList = _batchService.LoadAuthorizeBatch(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime, selectedVersions, selectedGenders).OrderBy(x => x.Rank).ToList();
                }
                else
                {
                    batchList = _batchService.LoadAuthorizeBatch(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime).OrderBy(x => x.Rank).ToList();
                }
                List<SelectListItem> batchSelecteList = (new SelectList(batchList.ToList(), "Id", "Name")).ToList();
                if (batchSelecteList.Any())
                    batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch" });

                return Json(new { batchName = batchSelecteList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error("Batch. AjaxRequestForBatch.", ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Reports

        #region Student List Report

        [HttpPost]
        public JsonResult GetStudentCount(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus, int[] smsReciever, string prnNo, string nickName, string mobile)
        {
            try
            {
                #region Authorization Check
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                int studentCountResult = _studentProgramService.GetAuthorizedStudentsCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, prnNo, nickName, mobile);
                var mobileCountResult = 0;
                if (smsReciever != null)
                {
                    mobileCountResult = _studentService.GetMobileCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, smsReciever, null, null, null);
                }
                #endregion
                return Json(new { studentCount = studentCountResult, mobileCount = mobileCountResult, IsSuccess = true });
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _logger.Error("Information. AjaxRequestForStudentCount." + ex.InnerException);
                else
                    _logger.Error("Information. AjaxRequestForStudentCount." + ex.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpGet]
        public ActionResult GetStudentList()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
            ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
            ViewBag.InformationViewSelectList = new SelectList(StudentListConstant.GetStudentListInformationStringList().Select(x => new SelectListItem() { Text = x, Value = x }), "Text", "Value").ToList();

            List<SelectListItem> verPrioritySelectList = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int> { (int)VersionOfStudy.Combined }), "Key", "Value").ToList();
            verPrioritySelectList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All" });
            ViewBag.VersionOfStudyList = verPrioritySelectList;

            List<SelectListItem> genderselectList = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined }), "Key", "Value").ToList();
            genderselectList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All" });
            ViewBag.GenderList = genderselectList;

            List<SelectListItem> religionSelectList = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value").ToList();
            religionSelectList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All" });
            ViewBag.ReligionList = religionSelectList;

            StudentInfoSearch studentInfoObj = new StudentInfoSearch();
            return View(studentInfoObj);
        }

        [HttpPost]
        public ActionResult GetStudentList(StudentInfoSearch studentListReportObj)
        {
            bool isError = true;
            try
            {
                if (studentListReportObj != null)
                {
                    if (studentListReportObj.InformationViewList == null)
                    {
                        var infoView = new List<string>(new[] { StudentListConstant.ProgramRoll, StudentListConstant.StudentsName, StudentListConstant.MobileNumberStudent });
                        studentListReportObj.InformationViewList = infoView.ToArray();
                    }
                    ViewBag.PageSize = Constants.PageSize;
                    ViewBag.Version = studentListReportObj.SelectedVersion;
                    ViewBag.Gender = studentListReportObj.SelectedGender;
                    ViewBag.Religion = studentListReportObj.SelectedReligion;
                    
                    #region Report Head Info

                    IList<string> reportHeadInfo = new List<string>();
                    List<string> resultList = new List<string>();

                    #region Authorization Check
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    #endregion
                    //Program Name 
                    try
                    {
                        if (studentListReportObj != null)
                        {
                            var p = _programService.GetProgram(studentListReportObj.ProgramId);
                            if (p != null && p.Id > 0)
                                reportHeadInfo.Add(p.Name);
                            else
                                reportHeadInfo.Add("");
                        }
                    }
                    catch { reportHeadInfo.Add(""); }

                    //Session Name
                    try
                    {
                        if (studentListReportObj != null)
                        {
                            var s = _sessionService.LoadById(studentListReportObj.SessionId);
                            if (s != null && s.Id > 0)
                                reportHeadInfo.Add(s.Name);
                            else
                                reportHeadInfo.Add("");
                        }
                    }
                    catch { reportHeadInfo.Add(""); }


                    //Branch Name 
                    try
                    {
                        if (studentListReportObj != null && studentListReportObj.SelectedBranch.Contains(0))
                        {
                            reportHeadInfo.Add("All Branch");
                        }
                        else
                        {
                            var selectedBranch = _branchService.LoadBranch(studentListReportObj.SelectedBranch);
                            resultList = (from xx in selectedBranch select xx.Name).ToList();
                            reportHeadInfo.Add(string.Join(", ", resultList));
                        }
                    }
                    catch { reportHeadInfo.Add(""); }


                    //Campus Name
                    try
                    {
                        if (studentListReportObj != null && studentListReportObj.SelectedCampus.Contains(0))
                        {
                            reportHeadInfo.Add("All Campus");
                        }
                        else
                        {
                            var selectedCampus = _campusService.LoadCampus(studentListReportObj.SelectedCampus);
                            resultList = (from xx in selectedCampus select xx.Name).ToList();
                            reportHeadInfo.Add(string.Join(", ", resultList));
                        }
                    }
                    catch { reportHeadInfo.Add(""); }
                    //Batch Days    
                    try
                    {
                        if (studentListReportObj != null && studentListReportObj.SelectedBatchDays.Contains("0"))
                        {
                            reportHeadInfo.Add("All Batch Days");
                        }
                        else
                        {
                            var reportHeadInfoBatchDays = string.Join("; ", studentListReportObj.SelectedBatchDays);
                            reportHeadInfo.Add(reportHeadInfoBatchDays);
                        }
                    }
                    catch { reportHeadInfo.Add(""); }
                    //Batch Name
                    try
                    {
                        if (studentListReportObj != null && studentListReportObj.SelectedBatch.Contains(0))
                        {
                            reportHeadInfo.Add("All Batch");
                        }
                        else
                        {
                            var selectedBatch = _batchService.LoadBatch(studentListReportObj.SelectedBatch);
                            resultList = (from xx in selectedBatch select xx.Name).ToList();
                            reportHeadInfo.Add(string.Join(", ", resultList));
                        }
                    }
                    catch { reportHeadInfo.Add(""); }
                    //Course Name
                    try
                    {
                        if (Array.Exists(studentListReportObj.SelectedCourse, item => item == 0))
                        {
                            reportHeadInfo.Add("All Course");
                        }
                        else
                        {
                            var courseList = _courseService.LoadCourse(studentListReportObj.SelectedCourse);
                            resultList = (from x in courseList select x.Name).ToList();
                            reportHeadInfo.Add(string.Join(", ", resultList));
                        }
                    }
                    catch { reportHeadInfo.Add(""); }
                    ViewBag.ReportHeadInfo = reportHeadInfo;
                    #endregion

                    ViewBag.InformationViewList = studentListReportObj.InformationViewList;
                    isError = false;
                }
                else
                {
                    ViewBag.ErrorMessage = "Student Info Search Option Not Found .";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
               // return View();
            }
            ViewBag.IsSuccess = isError;
            return PartialView("Partial/_GetStudentList");
            //return View("GetStudentInformationListReport", studentListReportObj);
        }

        #region Render Data Table Student Information List

        [HttpPost]
        public JsonResult StudentInformationAjaxRequest(int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId , long[] batchName, long[] course, string[] informationViewList, string[] batchDays, string[] batchTime, long[] courseId, string prnNo, string name , string mobile, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus)
        {
            if (!String.IsNullOrEmpty(startDate))
            {
                string[] sd = startDate.Split(' ');
                startDate = sd[0].ToString();

            }
            if (!String.IsNullOrEmpty(endDate))
            {
                string[] ed = endDate.Split(' ');
                endDate = ed[0].ToString();

            }
            if (Request.IsAjaxRequest())
            {
                try
                {
                    #region OrderBy and Direction
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];

                        if (String.IsNullOrEmpty(informationViewList[0]) && informationViewList.Length == 1)
                        {
                            informationViewList = new[] { StudentListConstant.ProgramRoll, StudentListConstant.StudentsName, StudentListConstant.MobileNumberStudent };
                        }

                        int informationViewListLength = informationViewList.Length;
                        if (Convert.ToInt32(orderBy) == 0)
                            orderBy = "A.Id";
                        else
                        {//A.NextReceivedDate,A.PrnNo,A.Batch,A.Branch,s1_.NickName,s1_.Mobile,s1_.GuardiansMobile1,i_.Name as Institute
                            switch (informationViewList[Convert.ToInt32(orderBy) - 1])
                            {
                                //case "Program Roll":
                                case StudentListConstant.ProgramRoll:
                                    orderBy = "A.PrnNo";
                                    break;
                                case StudentListConstant.RegistrationNumber:
                                    orderBy = "A.RegistrationNo";
                                    break;
                                case StudentListConstant.StudentsName:
                                    orderBy = "A.FullName";
                                    break;
                                case StudentListConstant.NickName:
                                    orderBy = "A.NickName";
                                    break;
                                case StudentListConstant.MobileNumberStudent:
                                    orderBy = "A.Mobile";
                                    break;
                                case StudentListConstant.MobileNumberFather:
                                    orderBy = "A.GuardiansMobile1";
                                    break;
                                case StudentListConstant.MobileNumberMother:
                                    orderBy = "A.GuardiansMobile2";
                                    break;
                                case StudentListConstant.FathersName:
                                    orderBy = "A.FatherName";
                                    break;
                                case StudentListConstant.InstituteName:
                                    orderBy = "I.Institute";
                                    break;
                                case StudentListConstant.Email:
                                    orderBy = "A.Email";
                                    break;
                                case StudentListConstant.Branch:
                                    orderBy = "A.Branch";
                                    break;
                                case StudentListConstant.Campus:
                                    orderBy = "A.Campus";
                                    break;
                                case StudentListConstant.BatchDays:
                                    orderBy = "A.BatchDays";
                                    break;
                                case StudentListConstant.BatchTime:
                                    orderBy = "A.BatchTime";
                                    break;
                                case StudentListConstant.Batch:
                                    orderBy = "A.Batch";
                                    break;
                                case StudentListConstant.VersionOfStudy:
                                    orderBy = "A.VersionOfStudy";
                                    break;
                                case StudentListConstant.Gender:
                                    orderBy = "A.Gender";
                                    break;
                                case StudentListConstant.Religion:
                                    orderBy = "A.Religion";
                                    break;
                                default:
                                    orderBy = "A.Id";
                                    break;
                            }
                        }
                    }

                    #endregion

                    #region Authorization Check
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    bool isDisplayAcademicInfo = informationViewList.Contains("Academic Info");

                    int recordsTotal = _studentProgramService.GetAuthorizedStudentsCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, courseId, version, gender, religion, startDate, endDate, selectedStatus, prnNo, name, mobile);
                    IList<StudentListDto> studentProgramList = _studentProgramService.LoadAuthorizedStudents(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, courseId, version, gender, religion, orderBy, orderDir.ToUpper(), start, length, startDate, endDate, selectedStatus, isDisplayAcademicInfo, prnNo, name, mobile).ToList();

                    #endregion

                    long recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var c in studentProgramList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case StudentListConstant.ProgramRoll:
                                        str.Add(c.PrnNo);
                                        break;
                                    case StudentListConstant.RegistrationNumber:
                                        str.Add(c.RegistrationNo);
                                        break;
                                    case StudentListConstant.StudentsName:
                                        str.Add(c.FullName);
                                        break;
                                    case StudentListConstant.NickName:
                                        str.Add(c.NickName);
                                        break;
                                    case StudentListConstant.MobileNumberStudent:
                                        str.Add(c.Mobile);
                                        break;
                                    case StudentListConstant.MobileNumberFather:
                                        str.Add(c.GuardiansMobile1);
                                        break;
                                    case StudentListConstant.MobileNumberMother:
                                        str.Add(c.GuardiansMobile2);
                                        break;
                                    case StudentListConstant.FathersName:
                                        str.Add(c.FatherName);
                                        break;
                                    case StudentListConstant.InstituteName:
                                        str.Add(c.Institute);
                                        break;
                                    case StudentListConstant.Email:
                                        str.Add(c.Email);
                                        break;
                                    case StudentListConstant.Branch:
                                        str.Add(c.Branch);
                                        break;
                                    case StudentListConstant.Campus:
                                        str.Add(c.Campus);
                                        break;
                                    case StudentListConstant.BatchDays:
                                        str.Add(c.BatchDays);
                                        break;
                                    case StudentListConstant.BatchTime:
                                        str.Add(c.BatchTime);
                                        break;
                                    case StudentListConstant.Batch:
                                        str.Add(c.Batch);
                                        break;
                                    case StudentListConstant.VersionOfStudy:
                                        if (c.VersionOfStudy != null)
                                        {
                                            str.Add(((VersionOfStudy)c.VersionOfStudy).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case StudentListConstant.Gender:
                                        if (c.Gender != null)
                                        {
                                            str.Add(((Gender)c.Gender).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case StudentListConstant.Religion:
                                        if (c.Religion != null)
                                        {
                                            str.Add(((Religion)c.Religion).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case StudentListConstant.AcademicInfo:
                                        str.Add(c.PSC ?? "");
                                        str.Add(c.JSC ?? "");
                                        str.Add(c.SSC ?? "");
                                        str.Add(c.HSC ?? "");
                                        str.Add(c.OLevel ?? "");
                                        str.Add(c.ALevel ?? "");
                                        break;
                                    default:
                                        break;
                                }

                            }
                            if (selectedStatus != 2)
                                str.Add("<a target='_blank' href='" + Url.Action("InformationView", "Information") + "?PrnNo=" + c.PrnNo + "'  class='glyphicon glyphicon-th-list'></a>");
                            else
                                str.Add("");
                        }
                        sl++;
                        data.Add(str);

                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                    //return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return Json(HttpNotFound()); }

        }

        #endregion

        public ActionResult ExportFiles(string programHeader, string branchHeader, string campusHeader, string batchDayHeader, string batchHeader, string courseHeader, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, string[] batchDays, string[] batchTime, string startDate, string endDate, string[] informationViewList, string prnNo, string name, string mobile, int version, int gender, int[] religion, int selectedStatus, bool showError = false)
        {
            try
            {
                if (!String.IsNullOrEmpty(startDate))
                {
                    startDate = Convert.ToDateTime(startDate).ToString("yyyy-MM-dd");
                }
                if (!String.IsNullOrEmpty(endDate))
                {
                    endDate = Convert.ToDateTime(endDate).ToString("yyyy-MM-dd");
                }

                if (String.IsNullOrEmpty(informationViewList[0]) && informationViewList.Length == 1)
                {
                    informationViewList = new[] { StudentListConstant.ProgramRoll, StudentListConstant.StudentsName, StudentListConstant.MobileNumberStudent };
                }

                bool isDisplayAcademicInfo = informationViewList.Contains("Academic Info");
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<StudentListDto> studentProgramList = _studentProgramService.LoadAuthorizedStudents(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, course, version, gender, religion, "A.PrnNo", "ASC", 0, 0, startDate, endDate, selectedStatus, isDisplayAcademicInfo, prnNo, name, mobile).ToList();
                List<string> headerList = new List<string>();
                headerList.Add("Student List Report");
                headerList.Add(programHeader);
                headerList.Add("Branch : " + branchHeader);
                headerList.Add("Campus : " + campusHeader);
                headerList.Add("Batch : " + batchHeader);
                headerList.Add("Course : " + courseHeader);
                headerList.Add("");

                List<string> footerList = new List<string>();
                footerList.Add("");

                List<string> columnList = new List<string>();

                if (informationViewList != null)
                {
                    foreach (string information in informationViewList)
                    {
                        if (information == StudentListConstant.AcademicInfo)
                        {
                            columnList.Add("PSC");
                            columnList.Add("JSC");
                            columnList.Add("SSC");
                            columnList.Add("HSC");
                            columnList.Add("OLevel");
                            columnList.Add("ALevel");
                        }
                        else if (information == StudentListConstant.StudentsName)
                        {
                            columnList.Add("Full Name");
                        }
                        else
                        {
                            columnList.Add(information);
                        }
                    }
                }
                var studentExcelList = new List<List<object>>();
                foreach (var xSes in studentProgramList)
                {
                    var xlsRow = new List<object>();
                    if (informationViewList != null)
                    {
                        if (informationViewList.Contains(StudentListConstant.ProgramRoll))
                        {
                            if (xSes.PrnNo != null)
                                xlsRow.Add(xSes.PrnNo);
                        }

                        if (informationViewList.Contains(StudentListConstant.RegistrationNumber))
                        {
                            xlsRow.Add(xSes.RegistrationNo ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.StudentsName))
                        {
                            xlsRow.Add(xSes.FullName ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.NickName))
                        {
                            xlsRow.Add(xSes.NickName ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.MobileNumberStudent))
                        {
                            xlsRow.Add(xSes.Mobile ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.MobileNumberFather))
                        {
                            xlsRow.Add(xSes.GuardiansMobile1 ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.MobileNumberMother))
                        {
                            xlsRow.Add(xSes.GuardiansMobile2 ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.FathersName))
                        {
                            xlsRow.Add(xSes.FatherName ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.InstituteName))
                        {
                            if (xSes.Institute != null)
                                xlsRow.Add(xSes.Institute ?? "");
                            else
                                xlsRow.Add("N/A");
                        }
                        if (informationViewList.Contains(StudentListConstant.Email))
                        {
                            xlsRow.Add(xSes.Email ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.Branch))
                        {
                            xlsRow.Add(xSes.Branch ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.Campus))
                        {
                            xlsRow.Add(xSes.Campus ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.BatchDays))
                        {
                            xlsRow.Add(xSes.BatchDays ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.BatchTime))
                        {
                            xlsRow.Add(xSes.BatchTime ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.Batch))
                        {
                            xlsRow.Add(xSes.Batch ?? "");
                        }
                        if (informationViewList.Contains(StudentListConstant.VersionOfStudy))
                        {
                            xlsRow.Add(xSes.VersionOfStudy != null ? ((VersionOfStudy)xSes.VersionOfStudy).ToString() : "--");
                        }
                        if (informationViewList.Contains(StudentListConstant.Gender))
                        {
                            if (xSes.Gender != null)
                            {
                                xlsRow.Add(((Gender)xSes.Gender).ToString());
                            }
                            else
                            {
                                xlsRow.Add("--");
                            }
                        }
                        if (informationViewList.Contains(StudentListConstant.Religion))
                        {
                            if (xSes.Religion != null)
                            {
                                xlsRow.Add(((Religion)xSes.Religion).ToString());
                            }
                            else
                            {
                                xlsRow.Add("--");
                            }
                        }
                        if (informationViewList.Contains(StudentListConstant.AcademicInfo))
                        {
                            xlsRow.Add(xSes.PSC ?? "--");
                            xlsRow.Add(xSes.JSC ?? "--");
                            xlsRow.Add(xSes.SSC ?? "--");
                            xlsRow.Add(xSes.HSC ?? "--");
                            xlsRow.Add(xSes.OLevel ?? "--");
                            xlsRow.Add(xSes.ALevel ?? "--");
                        }
                    }
                    studentExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "student-list-report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (showError)
                    ViewBag.AutoCloseMsg = ex.ToString();
                return View("AutoClose");
            }
        }

        #endregion

        #region Student Information Search

        [HttpGet]
        public ActionResult StudentInfoSearch()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var organizationList = _organizationService.LoadAuthorizedOrganization(userMenu);
            organizationList.Insert(0, new Organization() { Id = 0, ShortName = "Select Organization" });
            ViewBag.OrganizationId = new SelectList(organizationList, "Id", "Shortname");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.InformationViewSelectList = new SelectList(StudentListConstant.GetStudentSearchStringList().Select(x => new SelectListItem() { Text = x, Value = x }), "Text", "Value").ToList();
            return View();
        }

        [HttpPost]
        public ActionResult StudentInfoSearch(StudentInfoSearch studentInfoSearchObj)
        {
            try
            {
                if (studentInfoSearchObj != null)
                {
                    if (studentInfoSearchObj.InformationViewList == null)
                    {
                        //var infoView = new List<string>(new[] { "Program Roll", "Student's Name", "Student's Mobile" });
                        var infoView = new List<string>(new[] { StudentListConstant.ProgramRoll, StudentListConstant.StudentsName, StudentListConstant.MobileNumberStudent });
                        studentInfoSearchObj.InformationViewList = infoView.ToArray();
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Student Info Search Option Not Found .";
                }
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Problem occurred during student information search";
            }
            return View("StudentInfoSearchReport", studentInfoSearchObj);
        }


        #region Render Data Table Student Info Search

        [HttpPost]
        public JsonResult StudentInfoSearchAjaxRequest(int draw, int start, int length, long programId, long sessionId, string searchKey, string[] informationViewList, string prnNo, string name, string mobile)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    #region OrderBy and Direction
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                    }

                    #endregion

                    #region Authorization

                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    #endregion
                    int recordsTotal = _studentProgramService.GetAuthorizedStudentsCount(_userMenu, programId, sessionId, searchKey, informationViewList, prnNo, name, mobile);
                    long recordsFiltered = recordsTotal;
                    IList<StudentListDto> studentProgramList = _studentProgramService.LoadAuthorizedStudents(_userMenu, programId, sessionId, searchKey, informationViewList, start, length, orderBy, orderDir.ToUpper(), prnNo, name, mobile).ToList();
                    //studentProgramList = studentProgramList.Skip(start).Take(length).ToList();
                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var c in studentProgramList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case StudentListConstant.ProgramRoll:
                                        str.Add(c.PrnNo);
                                        break;
                                    case StudentListConstant.RegistrationNumber:
                                        str.Add(c.RegistrationNo);
                                        break;
                                    case StudentListConstant.StudentsName:
                                        str.Add(c.FullName);
                                        break;
                                    case StudentListConstant.NickName:
                                        str.Add(c.NickName);
                                        break;
                                    case StudentListConstant.MobileNumberStudent:
                                        str.Add(c.Mobile);
                                        break;
                                    case StudentListConstant.MobileNumberFather:
                                        str.Add(c.GuardiansMobile1);
                                        break;
                                    case StudentListConstant.MobileNumberMother:
                                        str.Add(c.GuardiansMobile2);
                                        break;
                                    case StudentListConstant.FathersName:
                                        str.Add(c.FatherName);
                                        break;
                                    case StudentListConstant.InstituteName:
                                        str.Add(c.Institute);
                                        break;
                                    case StudentListConstant.Email:
                                        str.Add(c.Email);
                                        break;
                                    case StudentListConstant.Branch:
                                        str.Add(c.Branch);
                                        break;
                                    case StudentListConstant.Batch:
                                        str.Add(c.Batch);
                                        break;
                                    case StudentListConstant.District:
                                        str.Add(c.District);
                                        break;

                                    case StudentListConstant.VersionOfStudy:
                                        if (c.VersionOfStudy != null)
                                        {
                                            str.Add(((VersionOfStudy)c.VersionOfStudy).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case StudentListConstant.Gender:
                                        if (c.Gender != null)
                                        {
                                            str.Add(((Gender)c.Gender).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }
                                    case StudentListConstant.Religion:
                                        if (c.Religion != null)
                                        {
                                            str.Add(((Religion)c.Religion).ToString());
                                            break;
                                        }
                                        else
                                        {
                                            str.Add("");
                                            break;
                                        }

                                    default:
                                        break;
                                }
                            }
                            str.Add("<a target='_blank' href='" + Url.Action("InformationView", "Information") + "?PrnNo=" + c.PrnNo + "'  class='glyphicon glyphicon-th-list'></a>");
                        }
                        sl++;
                        data.Add(str);
                    }
                    return Json(
                            new
                            {
                                draw = draw,
                                recordsTotal = recordsTotal,
                                recordsFiltered = recordsFiltered,
                                start = start,
                                length = length,
                                data = data
                            });
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occured during search student", ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }

            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #endregion

        #region Batch Wise Admission Report

        public ActionResult BatchWiseAdmissionReport()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //ViewBag.OrganizationId = new SelectList(_organizationService.LoadOrganization(), "Id", "ShortName");
            ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");

            StudentInfoSearch studentInfoObj = new StudentInfoSearch();
            return View(studentInfoObj);
        }
        [HttpPost]
        public ActionResult BatchWiseAdmissionReport(StudentInfoSearch studentListReportObj)
        {
            //var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var json = new JavaScriptSerializer().Serialize(studentListReportObj);
            ViewData["studentListReportObjJson"] = json;
            ViewBag.studentListReportObj = studentListReportObj;
            var organization = _organizationService.LoadById(studentListReportObj.OrganizationId);
            ViewBag.organizationName = organization.Name;
            ViewBag.organizationShortName = organization.ShortName;
            ViewBag.programName = _programService.GetProgram(studentListReportObj.ProgramId).Name;
            ViewBag.sessionName = _sessionService.LoadById(studentListReportObj.SessionId).Name;
            return View("GenerateBatchWiseAdmissionReport");
        }
        [HttpPost]
        public JsonResult BatchWiseAdmissionAjaxRequest(int draw, int start, int length, StudentInfoSearch studentSearchInfo)
        {

            if (Request.IsAjaxRequest())
            {
                try
                {
                    var data = new List<object>();
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    // _userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime
                    int recordsTotal = _informationService.GetAuthorizedBatchCount(_userMenu, studentSearchInfo.ProgramId, studentSearchInfo.SessionId, studentSearchInfo.SelectedBranch, studentSearchInfo.SelectedCampus, studentSearchInfo.SelectedBatchDays, studentSearchInfo.SelectedBatchTime, studentSearchInfo.SelectedVersions, studentSearchInfo.SelectedGenders, studentSearchInfo.SelectedBatch);
                    int recordsFiltered = recordsTotal;
                    IList<BatchWiseAdmissionReportDto> batchWiseAdmissionReportDto =
                        _informationService.LoadAuthorizedBatchAdmissionReport(_userMenu, _commonHelper.ConvertIdToList(studentSearchInfo.ProgramId), _commonHelper.ConvertIdToList(studentSearchInfo.SessionId), studentSearchInfo.SelectedBranch.ToList(), studentSearchInfo.SelectedCampus.ToList(), studentSearchInfo.SelectedBatchDays, studentSearchInfo.SelectedBatchTime, studentSearchInfo.SelectedVersions, studentSearchInfo.SelectedGenders, studentSearchInfo.SelectedBatch, start, length).OrderBy(x => x.BatchRank).ToList();
                    // BatchWiseAdmissionReportDto
                    int sl = start + 1;
                    foreach (var c in batchWiseAdmissionReportDto)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        if (studentSearchInfo.InformationViewList != null && !studentSearchInfo.InformationViewList.Contains(""))
                        {
                            foreach (string informationView in studentSearchInfo.InformationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Branch":
                                        str.Add(c.Branch);
                                        break;
                                    case "Campus":
                                        str.Add(c.Campus);
                                        break;
                                    case "Batch Days":
                                        str.Add(c.BatchDays);
                                        break;
                                    case "Batch Time":
                                        string startTime = Convert.ToDateTime(c.BatchStartTime).ToString("hh:mm tt");
                                        string endTime = Convert.ToDateTime(c.BatchEndTime).ToString("hh:mm tt");
                                        str.Add(startTime + " To " + endTime);
                                        break;
                                    case "Version":
                                        var versionEnumValue = (VersionOfStudy)c.Version;
                                        string versionEnumName = versionEnumValue.ToString();
                                        str.Add(versionEnumName);
                                        break;

                                    case "Gender":
                                        var genderEnumValue = (Gender)c.Gender;
                                        string genderEnumName = genderEnumValue.ToString();
                                        str.Add(genderEnumName);
                                        break;

                                    case "Batch Name":

                                        str.Add(c.Batch);
                                        break;

                                    case "Total Capacity":
                                        str.Add(c.TotalCapacity.ToString());
                                        break;

                                    case "Student Admit":

                                        str.Add(c.StudentAdmit.ToString());
                                        break;

                                    case "Available Capacity":
                                        str.Add(c.AvailableCapacity.ToString());
                                        break;


                                    default:
                                        break;
                                }

                            }
                        }
                        else
                        {
                            str.Add(c.Branch);
                            str.Add(c.Campus);
                            var versionEnumValue = (VersionOfStudy)c.Version;
                            string versionEnumName = versionEnumValue.ToString();
                            str.Add(versionEnumName);
                            var genderEnumValue = (Gender)c.Gender;
                            string genderEnumName = genderEnumValue.ToString();
                            str.Add(genderEnumName);
                            str.Add(c.Batch);
                            str.Add(c.TotalCapacity.ToString());
                            str.Add(c.StudentAdmit.ToString());
                            str.Add(c.AvailableCapacity.ToString());
                        }
                        sl++;
                        data.Add(str);

                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return Json(HttpNotFound());
            }
        }

        #endregion

        #region Update Reports

        [HttpPost]
        public JsonResult GetStudentInformationEditLogCount(long organizationId, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus)
        {
            try
            {
                #region Authorization Check
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime, _commonHelper.ConvertIdToList(version), _commonHelper.ConvertIdToList(gender)).ToList();
                if (batchId[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(batchId)).ToList();
                }
                /*DateTime dateFrom = DateTime.ParseExact(startDate.Trim(), "dd-MM-yyyy",
                                  CultureInfo.InvariantCulture);
                DateTime dateTo = DateTime.ParseExact(endDate.Trim(), "dd-MM-yyyy",
                                   CultureInfo.InvariantCulture);*/
                int studentCount = _informationService.GetEditedStudentsCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), selectedStatus, batchList.Select(x => x.Id).ToList(), null, null, null);
                #endregion
                return Json(new { missingImageCount = studentCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpGet]
        public ActionResult InformationEditLog()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                var studentInfoObj = new StudentInfoSearch();
                return View(studentInfoObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();

            }

        }
        [HttpPost]
        public ActionResult InformationEditLog(StudentInfoSearch studentListReportObj)
        {
            try
            {
                var program = _programService.GetProgram(studentListReportObj.ProgramId);
                var session = _sessionService.LoadById(studentListReportObj.SessionId);
                var batchName = "";
                var branchName = "";
                var batchDays = "";
                var campusName = "";
                var batchTime = "";
                var courseName = "";
                var subjectName = "";
                if (!studentListReportObj.SelectedBatchDays.Contains("0"))
                {
                    string batchDaysString = "";
                    foreach (var bd in studentListReportObj.SelectedBatchDays)
                    {
                        batchDaysString += bd + "; ";
                    }
                    batchDaysString = batchDaysString.Remove(batchDaysString.Length - 2);
                    batchDays = batchDaysString;
                }
                else
                {
                    batchDays = "ALL";
                }
                if (studentListReportObj.SelectedBatch[0] == 0)
                {
                    batchName = "ALL";
                }
                else
                {
                    IList<Batch> batches = _batchService.LoadBatch(null, null, null, null, null, studentListReportObj.SelectedBatch.ToList());
                    batchName = string.Join(", ", batches.Select(x => x.Name.Trim()).Distinct().ToList());
                }
                if (studentListReportObj.SelectedBatchTime[0] == "0")
                {
                    batchTime = "ALL";
                }
                else
                {
                    var tempBatchTime = new string[studentListReportObj.SelectedBatchTime.Length];

                    for (int i = 0; i < studentListReportObj.SelectedBatchTime.Length; i++)
                    {
                        var spl = new[] { "To" };
                        var dateFrom = Convert.ToDateTime(studentListReportObj.SelectedBatchTime[i].Split(spl, StringSplitOptions.None)[0]).ToString("hh:mm tt");
                        var dateTo = Convert.ToDateTime(studentListReportObj.SelectedBatchTime[i].Split(spl, StringSplitOptions.None)[1]).ToString("hh:mm tt");
                        tempBatchTime[i] = dateFrom + " to " + dateTo;
                    }
                    batchTime = string.Join(", ", tempBatchTime.Select(x => x.Trim()).ToArray().Distinct());
                }

                if (studentListReportObj.SelectedBranch[0] == 0)
                {
                    branchName = "ALL";
                }
                else
                {
                    IList<Branch> branches = _branchService.LoadBranch(null, null, null, studentListReportObj.SelectedBranch.ToList());
                    branchName = string.Join(", ", branches.Select(x => x.Name.Trim()).Distinct().ToList());
                }

                if (studentListReportObj.SelectedCampus[0] == 0)
                {
                    campusName = "ALL";
                }
                else
                {
                    IList<Campus> campuses = _campusService.LoadCampus(null, null, null, null, studentListReportObj.SelectedCampus.ToList());
                    campusName = string.Join(", ", campuses.Select(x => x.Name.Trim()).Distinct().ToList());
                }

                if (studentListReportObj.SelectedCourse[0] == 0)
                {
                    courseName = "ALL";
                }
                else
                {
                    IList<Course> courses = _courseService.LoadCourse(studentListReportObj.SelectedCourse);
                    courseName = string.Join(", ", courses.Select(x => x.Name.Trim()).Distinct().ToList());
                }
                var json = new JavaScriptSerializer().Serialize(studentListReportObj);
                ViewData["studentInfoEditLog"] = json;
                ViewBag.ProgramSession = program.Name + "-" + session.Name;
                ViewBag.Branch = branchName;
                ViewBag.BatchName = batchName;
                ViewBag.BatchDays = batchDays;
                ViewBag.CampusName = campusName;
                ViewBag.BatchTime = batchTime;
                if (studentListReportObj.StartDate != null)
                {
                    ViewBag.StartDate = studentListReportObj.StartDate.Value.ToString("dd-MM-yyyy");
                }
                else
                {
                    ViewBag.StartDate = new DateTime(2000, 1, 1).ToString("dd-MM-yyyy");
                    //new .ToString("dd-MM-yyyy");
                }
                if (studentListReportObj.EndDate != null)
                {
                    ViewBag.EndDate = studentListReportObj.EndDate.Value.ToString("dd-MM-yyyy");
                }
                else
                {
                    ViewBag.EndDate = DateTime.Now.ToString("dd-MM-yyyy");
                    //new .ToString("dd-MM-yyyy");
                }

                ViewBag.Course = courseName;
                ViewBag.informationViewList = studentListReportObj.InformationViewList;
                return View("GenerateStudentInfoEditLog");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult InformationEditLogReport(int draw, int start, int length, string studentInfoEditLogSelection, string startDate, string endDate)
        {
            try
            {
                #region Authorization Check
                var studentInformationEditLog =
                            new JavaScriptSerializer().Deserialize<StudentInfoSearch>(
                                studentInfoEditLogSelection);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(studentInformationEditLog.OrganizationId), _commonHelper.ConvertIdToList(studentInformationEditLog.ProgramId), studentInformationEditLog.SelectedBranch.ToList(), _commonHelper.ConvertIdToList(studentInformationEditLog.SessionId), studentInformationEditLog.SelectedCampus.ToList(), studentInformationEditLog.SelectedBatchDays, studentInformationEditLog.SelectedBatchTime, _commonHelper.ConvertIdToList(studentInformationEditLog.SelectedVersion), _commonHelper.ConvertIdToList(studentInformationEditLog.SelectedGender)).ToList();
                if (studentInformationEditLog.SelectedBatch[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(studentInformationEditLog.SelectedBatch)).ToList();
                }
                DateTime dateFrom = DateTime.ParseExact(startDate.Trim(), "dd-MM-yyyy",
                                  CultureInfo.InvariantCulture);
                DateTime dateTo = DateTime.ParseExact(endDate.Trim(), "dd-MM-yyyy",
                                   CultureInfo.InvariantCulture);
                int recordsTotal = _informationService.GetEditedStudentsCount(_userMenu, studentInformationEditLog.ProgramId, studentInformationEditLog.SessionId, studentInformationEditLog.SelectedBranch, studentInformationEditLog.SelectedCampus, studentInformationEditLog.SelectedBatchDays, studentInformationEditLog.SelectedBatchTime, studentInformationEditLog.SelectedBatch, studentInformationEditLog.SelectedCourse, studentInformationEditLog.SelectedVersion, studentInformationEditLog.SelectedGender, studentInformationEditLog.SelectedReligion, dateFrom, dateTo, studentInformationEditLog.SelectedStatus, batchList.Select(x => x.Id).ToList(), null, null, null);

                var studentInfo = _informationService.LoadEditedStudentsInfo(start, length, _userMenu, studentInformationEditLog.ProgramId, studentInformationEditLog.SessionId, studentInformationEditLog.SelectedBranch, studentInformationEditLog.SelectedCampus, studentInformationEditLog.SelectedBatchDays, studentInformationEditLog.SelectedBatchTime, studentInformationEditLog.SelectedBatch, studentInformationEditLog.SelectedCourse, studentInformationEditLog.SelectedVersion, studentInformationEditLog.SelectedGender, studentInformationEditLog.SelectedReligion, dateFrom, dateTo, studentInformationEditLog.SelectedStatus, batchList.Select(x => x.Id).ToList(), null, null, null);
                #endregion
                //int recordsTotal = _informationService.GetEditedStudentsCount(_userMenu, studentInformationEditLog.ProgramId, studentInformationEditLog.SessionId, studentInformationEditLog.SelectedBranch, studentInformationEditLog.SelectedCampus, studentInformationEditLog.SelectedBatchDays, studentInformationEditLog.SelectedBatchTime, studentInformationEditLog.SelectedBatch, studentInformationEditLog.SelectedCourse, studentInformationEditLog.SelectedVersion, studentInformationEditLog.SelectedGender, studentInformationEditLog.SelectedReligion, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate).Add(TimeSpan.Parse("23:59:59")), studentInformationEditLog.SelectedStatus, null, null, null);
                long recordsFiltered = recordsTotal;
                var data = new List<object>();
                int sl = start + 1;
                foreach (var c in studentInfo)
                {
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    if (studentInformationEditLog.InformationViewList != null)
                    {
                        foreach (string informationView in studentInformationEditLog.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case "Program Roll":
                                    str.Add(c.PrnNo);
                                    break;
                                case "Nick Name":
                                    str.Add(!string.IsNullOrEmpty(c.PrevNickName) ? c.PrevNickName : "");
                                    str.Add(!string.IsNullOrEmpty(c.NickName) ? c.NickName : "");
                                    break;
                                case "Registration Number":
                                    str.Add(!string.IsNullOrEmpty(c.PrevRegistrationNo) ? c.PrevRegistrationNo : "");
                                    str.Add(!string.IsNullOrEmpty(c.RegistrationNo) ? c.RegistrationNo : "");
                                    break;
                                case "Student's Name":
                                    str.Add(!string.IsNullOrEmpty(c.PrevFullName) ? c.PrevFullName : "");
                                    str.Add(!string.IsNullOrEmpty(c.FullName) ? c.FullName : "");
                                    break;
                                case "Father's Name":

                                    str.Add(!string.IsNullOrEmpty(c.PrevFatherName) ? c.PrevFatherName : "");
                                    str.Add(!string.IsNullOrEmpty(c.FatherName) ? c.FatherName : "");
                                    break;
                                case "Mobile Number (Student)":
                                    str.Add(!string.IsNullOrEmpty(c.PrevMobile) ? c.PrevMobile : "");
                                    str.Add(!string.IsNullOrEmpty(c.Mobile) ? c.Mobile : "");

                                    break;
                                case "Mobile Number (Father)":
                                    str.Add(!string.IsNullOrEmpty(c.PrevGuardiansMobile1) ? c.PrevGuardiansMobile1 : "");
                                    str.Add(!string.IsNullOrEmpty(c.GuardiansMobile1) ? c.GuardiansMobile1 : "");

                                    break;
                                case "Mobile Number (Mother)":

                                    str.Add(!string.IsNullOrEmpty(c.PrevGuardiansMobile2) ? c.PrevGuardiansMobile2 : "");
                                    str.Add(!string.IsNullOrEmpty(c.GuardiansMobile2) ? c.GuardiansMobile2 : "");

                                    break;
                                case "Institute Name":

                                    //str.Add(c.PrevInstitute != null ?_instituteService.GetInstitute(Convert.ToInt64(c.PrevInstitute)).Name : "");
                                    if (c.PrevInstitute != null)
                                    {
                                        str.Add(_instituteService.GetInstitute(Convert.ToInt64(c.PrevInstitute)).Name);

                                    }
                                    else
                                    {
                                        str.Add("");
                                    }
                                    //str.Add(c.InstituteId != null ? _instituteService.GetInstitute(Convert.ToInt64(c.InstituteId)).Name : "");
                                    if (c.InstituteId != null)
                                    {
                                        var institute = _instituteService.GetInstitute(Convert.ToInt64(c.InstituteId));
                                        str.Add(institute.Name);

                                    }
                                    else
                                    {
                                        str.Add("");
                                    }
                                    break;
                                case "Email":
                                    str.Add(!string.IsNullOrEmpty(c.PrevEmail) ? c.PrevEmail : "");
                                    str.Add(!string.IsNullOrEmpty(c.Email) ? c.Email : "");

                                    break;

                                default:
                                    break;
                            }

                        }
                        str.Add(c.totalEdited.ToString());
                        str.Add("<a target='_blank' href='" + Url.Action("IndividualInformationEditLogReport", "Information") + "?studentIdString=" + c.StudentId + "&prnNo=" + c.PrnNo + "&studentName=" + c.FullName + "&editLogInfoSelection=" + studentInfoEditLogSelection + "&startDate=" + startDate + "&endDate=" + endDate + "'  class='glyphicon glyphicon-th-list'></a>");

                    }
                    sl++;
                    data.Add(str);

                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                var data = new List<object>();
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });

            }

        }
        public ActionResult ExportInformationEditLogReport(string studentInfoEditLogSelection, string startDate, string endDate, string programSessionHeader, string branchHeader, string campusHeader, string batchDaysHeader, string batchTimeHeader, string batchHeader, string courseHeader)
        {
            try
            {
                DateTime dateFrom = DateTime.ParseExact(startDate.Trim(), "dd-MM-yyyy",
                                  CultureInfo.InvariantCulture);
                DateTime dateTo = DateTime.ParseExact(endDate.Trim(), "dd-MM-yyyy",
                                   CultureInfo.InvariantCulture);
                #region Authorization Check
                var studentInformationEditLog =
                            new JavaScriptSerializer().Deserialize<StudentInfoSearch>(
                                studentInfoEditLogSelection);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(studentInformationEditLog.OrganizationId), _commonHelper.ConvertIdToList(studentInformationEditLog.ProgramId), studentInformationEditLog.SelectedBranch.ToList(), _commonHelper.ConvertIdToList(studentInformationEditLog.SessionId), studentInformationEditLog.SelectedCampus.ToList(), studentInformationEditLog.SelectedBatchDays, studentInformationEditLog.SelectedBatchTime, _commonHelper.ConvertIdToList(studentInformationEditLog.SelectedVersion), _commonHelper.ConvertIdToList(studentInformationEditLog.SelectedGender)).ToList();
                if (studentInformationEditLog.SelectedBatch[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(studentInformationEditLog.SelectedBatch)).ToList();
                }

                var studentInfo = _informationService.LoadEditedStudentsInfo(0, 0, _userMenu, studentInformationEditLog.ProgramId, studentInformationEditLog.SessionId, studentInformationEditLog.SelectedBranch, studentInformationEditLog.SelectedCampus, studentInformationEditLog.SelectedBatchDays, studentInformationEditLog.SelectedBatchTime, studentInformationEditLog.SelectedBatch, studentInformationEditLog.SelectedCourse, studentInformationEditLog.SelectedVersion, studentInformationEditLog.SelectedGender, studentInformationEditLog.SelectedReligion, dateFrom, dateTo, studentInformationEditLog.SelectedStatus, batchList.Select(x => x.Id).ToList(), null, null, null);
                #endregion
                List<string> headerList = new List<string>();
                List<string> columnList = new List<string>();
                List<string> footerList = new List<string>();
                headerList.Add(_organizationService.LoadById(studentInformationEditLog.OrganizationId).ShortName);
                headerList.Add(programSessionHeader);
                headerList.Add("Student Info Edit Log");
                headerList.Add(branchHeader);
                headerList.Add(campusHeader);
                headerList.Add(batchDaysHeader);
                headerList.Add(batchTimeHeader);
                headerList.Add(batchHeader);
                headerList.Add(courseHeader);


                if (studentInformationEditLog.InformationViewList != null)
                {
                    foreach (string informationView in studentInformationEditLog.InformationViewList)
                    {
                        if (informationView != "Program Roll")
                        {
                            columnList.Add(informationView);
                            columnList.Add(informationView + " (Updated)");
                        }
                        else
                        {
                            columnList.Add(informationView);
                        }


                    }
                }
                columnList.Add("Update Count");
                columnList.Add("Details");
                footerList.Add("");
                var XlsList = new List<List<object>>();
                foreach (var c in studentInfo)
                {
                    var str = new List<object>();
                    if (studentInformationEditLog.InformationViewList != null)
                    {
                        foreach (string informationView in studentInformationEditLog.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case "Program Roll":
                                    str.Add(c.PrnNo);
                                    break;
                                case "Nick Name":
                                    str.Add(!string.IsNullOrEmpty(c.PrevNickName) ? c.PrevNickName : "");
                                    str.Add(!string.IsNullOrEmpty(c.NickName) ? c.NickName : "");
                                    break;
                                case "Registration Number":
                                    str.Add(!string.IsNullOrEmpty(c.PrevRegistrationNo) ? c.PrevRegistrationNo : "");
                                    str.Add(!string.IsNullOrEmpty(c.RegistrationNo) ? c.RegistrationNo : "");
                                    break;
                                case "Student's Name":
                                    str.Add(!string.IsNullOrEmpty(c.PrevFullName) ? c.PrevFullName : "");
                                    str.Add(!string.IsNullOrEmpty(c.FullName) ? c.FullName : "");
                                    break;
                                case "Father's Name":

                                    str.Add(!string.IsNullOrEmpty(c.PrevFatherName) ? c.PrevFatherName : "");
                                    str.Add(!string.IsNullOrEmpty(c.FatherName) ? c.FatherName : "");
                                    break;
                                case "Mobile Number (Student)":
                                    str.Add(!string.IsNullOrEmpty(c.PrevMobile) ? c.PrevMobile : "");
                                    str.Add(!string.IsNullOrEmpty(c.Mobile) ? c.Mobile : "");

                                    break;
                                case "Mobile Number (Father)":
                                    str.Add(!string.IsNullOrEmpty(c.PrevGuardiansMobile1) ? c.PrevGuardiansMobile1 : "");
                                    str.Add(!string.IsNullOrEmpty(c.GuardiansMobile1) ? c.GuardiansMobile1 : "");

                                    break;
                                case "Mobile Number (Mother)":

                                    str.Add(!string.IsNullOrEmpty(c.PrevGuardiansMobile2) ? c.PrevGuardiansMobile2 : "");
                                    str.Add(!string.IsNullOrEmpty(c.GuardiansMobile2) ? c.GuardiansMobile2 : "");

                                    break;
                                case "Institute Name":

                                    if (c.PrevInstitute != null)
                                    {
                                        str.Add(_instituteService.GetInstitute(Convert.ToInt64(c.PrevInstitute)).Name);

                                    }
                                    else
                                    {
                                        str.Add("");
                                    }
                                    if (c.InstituteId != null)
                                    {
                                        var institute = _instituteService.GetInstitute(Convert.ToInt64(c.InstituteId));
                                        str.Add(institute.Name);

                                    }
                                    else
                                    {
                                        str.Add("");
                                    }
                                    break;
                                case "Email":
                                    str.Add(!string.IsNullOrEmpty(c.PrevEmail) ? c.PrevEmail : "");
                                    str.Add(!string.IsNullOrEmpty(c.Email) ? c.Email : "");

                                    break;

                                default:
                                    break;
                            }

                        }
                        str.Add(c.totalEdited.ToString());

                    }

                    XlsList.Add(str);

                }
                ExcelGenerator.GenerateExcel(headerList, columnList, XlsList, footerList, "Student-Information-Edit-Log__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");

            }

        }
        [HttpGet]
        public ActionResult IndividualInformationEditLogReport(string studentIdString, string prnNo, string studentName, string editLogInfoSelection, string startDate, string endDate)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentProgram = _studentProgramService.GetStudentProgram(prnNo);
                if (studentProgram == null)
                {
                    ViewBag.ErrorMessage = "Student Does not exist.";
                    return View();
                }
                var message = _studentProgramService.CheckAuthorizationForStudentProgram(_userMenu, studentProgram);
                if (message != "")
                {
                    ViewBag.ErrorMessage = "You are not authorized to view this report";
                    return View();
                }


                var studentId = Convert.ToInt64(studentIdString);
                var editLogSelection =
                            new JavaScriptSerializer().Deserialize<StudentInfoSearch>(
                                editLogInfoSelection);
                var program = _programService.GetProgram(editLogSelection.ProgramId);
                var session = _sessionService.LoadById(editLogSelection.SessionId);
                ViewData["studentInfoEditLog"] = editLogInfoSelection;
                ViewBag.ProgramSession = program.Name + "-" + session.Name;
                ViewBag.Name = studentName;
                ViewBag.PrnNo = prnNo;
                ViewBag.informationViewList = editLogSelection.InformationViewList;
                ViewBag.StudentId = studentId;
                ViewBag.StartDate = startDate;
                ViewBag.EndDate = endDate;
                studentProgram = _studentProgramService.GetStudentProgram(prnNo);


                {
                    if (studentProgram.StudentImage.Count > 0)
                    {
                        StudentMediaImage mediaStudentImages =
                            _studentImagesMediaService.GetStudentImageMediaByRefId(
                                studentProgram.StudentImage[0].Id);
                        if (mediaStudentImages != null)
                        {
                            studentProgram.MediaStudentImage = mediaStudentImages;
                            ViewBag.StudentProgram = studentProgram;
                        }
                        else
                        {
                            ViewBag.StudentProgram = new StudentProgram();
                        }
                    }
                    else
                    {
                        ViewBag.StudentProgram = new StudentProgram();
                    }
                }

                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult IndividualInformationEditLogReport(int draw, int start, int length, string studentInfoEditLogSelection, string startDate, string endDate, long studentId)
        {
            try
            {
                DateTime dateFrom = DateTime.ParseExact(startDate.Trim(), "dd-MM-yyyy",
                                   CultureInfo.InvariantCulture);
                DateTime dateTo = DateTime.ParseExact(endDate.Trim(), "dd-MM-yyyy",
                                   CultureInfo.InvariantCulture);
                #region Authorization Check
                var studentInformationEditLog =
                            new JavaScriptSerializer().Deserialize<StudentInfoSearch>(
                                studentInfoEditLogSelection);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentInfo = _informationService.LoadIndividualEditedStudentsInfo(start, length, _userMenu, dateFrom, dateTo, studentId);

                #endregion
                int recordsTotal = _informationService.GetIndividualEditCount(_userMenu, dateFrom, dateTo, studentId);
                long recordsFiltered = recordsTotal;
                var data = new List<object>();
                int sl = start + 1;
                foreach (var c in studentInfo)
                {
                    var studentProgram = _studentProgramService.GetStudentProgram(c.StudentProgramId);
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    if (studentInformationEditLog.InformationViewList != null)
                    {
                        foreach (string informationView in studentInformationEditLog.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case "Program Roll":
                                    var prn = studentProgram.PrnNo;
                                    str.Add(prn);
                                    break;
                                case "Nick Name":
                                    str.Add(!string.IsNullOrEmpty(c.NickName) ? c.NickName : "");
                                    break;
                                case "Registration Number":
                                    str.Add(!string.IsNullOrEmpty(c.RegistrationNo) ? c.RegistrationNo : "");
                                    break;
                                case "Student's Name":
                                    str.Add(!string.IsNullOrEmpty(c.FullName) ? c.FullName : "");
                                    break;
                                case "Father's Name":

                                    str.Add(!string.IsNullOrEmpty(c.FatherName) ? c.FatherName : "");
                                    break;
                                case "Mobile Number (Student)":
                                    str.Add(!string.IsNullOrEmpty(c.Mobile) ? c.Mobile : "");

                                    break;
                                case "Mobile Number (Father)":
                                    str.Add(!string.IsNullOrEmpty(c.GuardiansMobile1) ? c.GuardiansMobile1 : "");

                                    break;
                                case "Mobile Number (Mother)":

                                    str.Add(!string.IsNullOrEmpty(c.GuardiansMobile2) ? c.GuardiansMobile2 : "");

                                    break;
                                case "Institute Name":

                                    var institute = studentProgram.Institute;
                                    if (institute != null)
                                    {
                                        str.Add(institute.Name);

                                    }
                                    else
                                    {
                                        str.Add("");
                                    }
                                    break;
                                case "Email":
                                    str.Add(!string.IsNullOrEmpty(c.Email) ? c.Email : "");

                                    break;

                                default:
                                    break;
                            }

                        }
                        if (c.CreateBy != null && c.CreateBy > 0)
                        {
                            var username = _userService.LoadAspNetUserById(c.CreateBy);
                            str.Add(username.UserName);
                        }
                        else
                        {
                            str.Add("");
                        }
                        if (c.CreationDate != null)
                        {
                            str.Add(c.CreationDate.ToString("dd-MM-yyyy"));
                        }
                        else
                        {
                            str.Add("");
                        }
                        if (c.InfoStatus == 1)
                        {
                            str.Add("Admission");
                        }
                        if (c.InfoStatus == 2)
                        {
                            str.Add("Update");
                        }

                    }
                    sl++;
                    data.Add(str);

                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                var data = new List<object>();
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });

            }

        }
        public ActionResult ExportIndividualInformationEditLogReport(string studentInfoEditLogSelection, string startDate, string endDate, long studentId, string programSessionHeader, string prnNo, string studentName)
        {
            try
            {
                DateTime dateFrom = DateTime.ParseExact(startDate.Trim(), "dd-MM-yyyy",
                                  CultureInfo.InvariantCulture);
                DateTime dateTo = DateTime.ParseExact(endDate.Trim(), "dd-MM-yyyy",
                                   CultureInfo.InvariantCulture);
                #region Authorization Check
                var studentInformationEditLog =
                            new JavaScriptSerializer().Deserialize<StudentInfoSearch>(
                                studentInfoEditLogSelection);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentInfo = _informationService.LoadIndividualEditedStudentsInfo(0, 0, _userMenu, dateFrom, dateTo, studentId);
                #endregion

                List<string> headerList = new List<string>();
                List<string> columnList = new List<string>();
                List<string> footerList = new List<string>();
                headerList.Add(programSessionHeader);
                headerList.Add("Individual Info Edit Log");
                headerList.Add(studentName);
                headerList.Add(prnNo);
                if (studentInformationEditLog.InformationViewList != null)
                {
                    foreach (string informationView in studentInformationEditLog.InformationViewList)
                    {

                        columnList.Add(informationView);

                    }
                }
                columnList.Add("Updated By");
                columnList.Add("Update Date");
                columnList.Add("Status");
                footerList.Add("");
                var XlsList = new List<List<object>>();
                foreach (var c in studentInfo)
                {
                    var studentProgram = _studentProgramService.GetStudentProgram(c.StudentProgramId);
                    var str = new List<object>();
                    if (studentInformationEditLog.InformationViewList != null)
                    {
                        foreach (string informationView in studentInformationEditLog.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case "Program Roll":
                                    var prn = studentProgram.PrnNo;
                                    str.Add(prn);
                                    break;
                                case "Nick Name":
                                    str.Add(!string.IsNullOrEmpty(c.NickName) ? c.NickName : "");
                                    break;
                                case "Registration Number":
                                    str.Add(!string.IsNullOrEmpty(c.RegistrationNo) ? c.RegistrationNo : "");
                                    break;
                                case "Student's Name":
                                    str.Add(!string.IsNullOrEmpty(c.FullName) ? c.FullName : "");
                                    break;
                                case "Father's Name":

                                    str.Add(!string.IsNullOrEmpty(c.FatherName) ? c.FatherName : "");
                                    break;
                                case "Mobile Number (Student)":
                                    str.Add(!string.IsNullOrEmpty(c.Mobile) ? c.Mobile : "");

                                    break;
                                case "Mobile Number (Father)":
                                    str.Add(!string.IsNullOrEmpty(c.GuardiansMobile1) ? c.GuardiansMobile1 : "");

                                    break;
                                case "Mobile Number (Mother)":

                                    str.Add(!string.IsNullOrEmpty(c.GuardiansMobile2) ? c.GuardiansMobile2 : "");

                                    break;
                                case "Institute Name":

                                    var institute = studentProgram.Institute;
                                    if (institute != null)
                                    {
                                        str.Add(institute.Name);

                                    }
                                    else
                                    {
                                        str.Add("");
                                    }
                                    break;
                                case "Email":
                                    str.Add(!string.IsNullOrEmpty(c.Email) ? c.Email : "");

                                    break;

                                default:
                                    break;
                            }

                        }
                        if (c.CreateBy != null && c.CreateBy > 0)
                        {
                            var username = _userService.LoadAspNetUserById(c.CreateBy);
                            str.Add(username.UserName);
                        }
                        else
                        {
                            str.Add("");
                        }
                        if (c.CreationDate != null)
                        {
                            str.Add(c.CreationDate.ToString("dd-MM-yyyy"));
                        }
                        else
                        {
                            str.Add("");
                        }
                        if (c.InfoStatus == 1)
                        {
                            str.Add("Admission");
                        }
                        if (c.InfoStatus == 2)
                        {
                            str.Add("Update");
                        }

                    }
                    XlsList.Add(str);

                }
                ExcelGenerator.GenerateExcel(headerList, columnList, XlsList, footerList, "individual-info-Edit-log__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }

        }

        #endregion

        #endregion

        #region SEND SMS
        public ActionResult SendMessage()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id",
                    "ShortName");
                var allowMobile = new string[] { "Mobile Number (Personal)", "Mobile Number (Father)", "Mobile Number (Mother)" };
                var smsReceivers = _smsService.LoadSmsReceiverActive().Where(x => allowMobile.Contains(x.Name));
                ViewBag.SmsReceiverList = new SelectList(smsReceivers, "Id", "Name");
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SmsMaskName = new SelectList(new List<SmsMask>(), "Name", "Name");
                ViewBag.DynamicOptionsList = new SelectList(_smsService.LoadSmsTypeById(9).DynamicOptions, "Id", "Name");
                var studentInfoObj = new StudentListSms();
                return View(studentInfoObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }

        }
        [HttpPost]
        public ActionResult SendMessage(StudentListSms studentInfoObj)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                var allowMobile = new string[] { "Mobile Number (Personal)", "Mobile Number (Father)", "Mobile Number (Mother)" };
                var smsReceivers = _smsService.LoadSmsReceiverActive().Where(x => allowMobile.Contains(x.Name));
                ViewBag.SmsReceiverList = new SelectList(smsReceivers, "Id", "Name");
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SmsMaskName = new SelectList(new List<SmsMask>(), "Name", "Name");
                var smsType = _smsService.LoadSmsTypeById(9);
                ViewBag.DynamicOptionsList = new SelectList(smsType.DynamicOptions, "Id", "Name");

                if (ModelState.IsValid)
                {
                    string startDate = "";
                    string endDate = "";

                    //string startDate = "2000-01-01";
                    //string endDate = DateTime.Today.ToString();
                    var selectedStatus = StudentProgram.EntityStatus.Active;
                    IList<StudentListDto> studentProgramList = _studentProgramService.LoadAuthorizedStudents(
                        _userMenu, studentInfoObj.ProgramId, studentInfoObj.SessionId, studentInfoObj.SelectedBranch, studentInfoObj.SelectedCampus, studentInfoObj.SelectedBatchDays,
                        studentInfoObj.SelectedBatchTime, studentInfoObj.SelectedBatch, studentInfoObj.SelectedCourse, studentInfoObj.SelectedVersion, studentInfoObj.SelectedGender,
                        studentInfoObj.SelectedReligion, "A.PrnNo", "ASC", 0, 0, startDate, endDate, selectedStatus,false, null, null, null).ToList();

                    if (studentProgramList.Any())
                    {
                        long successStudent = 0;
                        long failStudent = 0;
                        long successMobile = 0;
                        long failMobile = 0;
                        string campaign = "Student List " + DateTime.Now.ToString();
                        var smsTypeObj = _smsService.LoadSmsTypeName("Manual Student List");
                        int smsTypeId = smsTypeObj != null ? Convert.ToInt32(smsTypeObj.Id) : 0;
                        //smsHistory.Type = smsTypeObj != null ? Convert.ToInt32(smsTypeObj.Id) : 0;

                        foreach (var c in studentProgramList)
                        {
                            StudentProgram studentProgram = _studentProgramService.GetStudentProgram(c.Id);
                            //bool flag = SendSmsApi.GenerateManualStudentListSmsAndNumber(studentProgram.Student, studentProgram,
                            //    studentInfoObj.SelectedSmsReceiver, studentInfoObj.Template, studentInfoObj.MaskName);
                            var message = studentInfoObj.Template;
                            var options = smsType.DynamicOptions;
                            var student = studentProgram.Student;
                            var mask = studentInfoObj.MaskName;
                            var smsToBeSend = new List<SmsViewModel>();
                            if (options.Any())
                            {
                                foreach (var o in options)
                                {
                                    string optionField = "[[{{" + o.Name + "}}]]";
                                    string optionValue = "";
                                    switch (o.Name)
                                    {
                                        case "Nick Name":
                                            optionValue = student.NickName;
                                            break;
                                        case "Program Roll":
                                            optionValue = studentProgram.PrnNo;
                                            break;
                                        case "Registration Number":
                                            optionValue = student.RegistrationNo;
                                            break;
                                        default:
                                            optionValue = "";
                                            break;
                                    }
                                    message = message.Replace(optionField, optionValue);
                                }
                            }
                            foreach (var srId in studentInfoObj.SelectedSmsReceiver)
                            {
                                var sr = _smsService.LoadReceiverById(srId);
                                if (sr != null)
                                {
                                    var svm = new SmsViewModel();
                                    switch (sr.Name)
                                    {
                                        case "Mobile Number (Personal)":
                                            string studentMobile = SendSmsApi.CheckMobileNumber(student.Mobile);
                                            if (studentMobile != null)
                                            {
                                                svm.SmsReceiverId = sr.Id;
                                                svm.ReceiverNumber = "88" + studentMobile;
                                            }
                                            break;
                                        case "Mobile Number (Father)":
                                            string guardiansMobile1 = SendSmsApi.CheckMobileNumber(student.GuardiansMobile1);
                                            if (guardiansMobile1 != null)
                                            {
                                                svm.SmsReceiverId = sr.Id;
                                                svm.ReceiverNumber = "88" + guardiansMobile1;
                                            }
                                            break;
                                        case "Mobile Number (Mother)":
                                            string guardiansMobile2 = SendSmsApi.CheckMobileNumber(student.GuardiansMobile2);
                                            if (guardiansMobile2 != null)
                                            {
                                                svm.SmsReceiverId = sr.Id;
                                                svm.ReceiverNumber = "88" + guardiansMobile2;
                                            }
                                            break;
                                        case "Mobile Number (Campus Incharge)":
                                            string contactNumber = SendSmsApi.CheckMobileNumber(studentProgram.Batch.Campus.ContactNumber);
                                            if (contactNumber != null)
                                            {
                                                svm.SmsReceiverId = sr.Id;
                                                svm.ReceiverNumber = "88" + contactNumber;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                    if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                                    {
                                        var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                                        if (!alreadyExist.Any())
                                        {
                                            smsToBeSend.Add(svm);
                                        }
                                    }
                                }
                            }


                            if (smsToBeSend.Any())
                            {
                                successStudent++;
                                if (String.IsNullOrEmpty(mask))
                                {
                                    mask = "";
                                }
                                try
                                {

                                    foreach (var sts in smsToBeSend)
                                    {
                                        var smsHistory = new SmsHistory();
                                        smsHistory.ReceiverNumber = sts.ReceiverNumber;
                                        smsHistory.SmsReceiver = _smsService.LoadReceiverById(sts.SmsReceiverId);
                                        smsHistory.Sms = message;
                                        smsHistory.Program = studentProgram.Batch.Program;
                                        smsHistory.Organization = studentProgram.Batch.Program.Organization;
                                        smsHistory.SmsSettings = null;
                                        smsHistory.Student = student;
                                        smsHistory.Mask = mask;
                                        smsHistory.Status = SmsHistory.EntityStatus.Pending;
                                        smsHistory.CampaignName = campaign;
                                        smsHistory.Priority = 4;
                                        smsHistory.Type = smsTypeId;//for result
                                        _smsService.SaveSmsHistory(smsHistory);
                                        successMobile++;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(ex.Message);
                                    failMobile++;
                                }
                            }
                            else
                            {
                                failStudent++;
                            }
                        }
                        ViewBag.SuccessMessage = "Total Success: " + successStudent.ToString() + "(" + "Total Mobile: " + successMobile.ToString() + ")";
                        ViewBag.ErrorMessage = "Total Fail: " + failStudent.ToString() + "(" + "Total Mobile: " + failMobile.ToString() + ")";
                        //ViewBag.SuccessMessage = "SMS Send Succesfull";
                        ModelState.Clear();
                        studentInfoObj = new StudentListSms();
                    }
                    else
                        ViewBag.ErrorMessage = "Student List is Empty";
                }
                else
                    ViewBag.ErrorMessage = "Please Give Required Information";
                //var studentInfoObj = new StudentListSms();
                return View(studentInfoObj);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex.Message);
                throw;
            }

        }
        #endregion

        #region Helper Function

        [HttpGet]

        public ActionResult ExportStudentInfoSearchResult(long programId, long sessionId, string searchKey,
            string[] informationViewList, string prnNo, string name, string mobile)
        {
            try
            {
                #region Authorization

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<StudentListDto> studentProgramList = _studentProgramService.LoadAuthorizedStudents(_userMenu, programId, sessionId, searchKey, informationViewList, 0, 0, "", "", prnNo, name, mobile).ToList();
                #endregion

                List<string> headerList = new List<string>();
                headerList.Add("");
                List<string> footerList = new List<string>();
                footerList.Add("");

                List<string> columnList = new List<string>();
                foreach (var info in informationViewList)
                {
                    columnList.Add(info);
                }

                var data = new List<object>();
                var studentExcelList = new List<List<object>>();
                foreach (var c in studentProgramList)
                {
                    var xRow = new List<object>();
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case StudentListConstant.ProgramRoll:
                                    xRow.Add(c.PrnNo);
                                    break;
                                case StudentListConstant.RegistrationNumber:
                                    xRow.Add(c.RegistrationNo);
                                    break;
                                case StudentListConstant.StudentsName:
                                    xRow.Add(c.FullName);
                                    break;
                                case StudentListConstant.NickName:
                                    xRow.Add(c.NickName);
                                    break;
                                case StudentListConstant.MobileNumberStudent:
                                    xRow.Add(c.Mobile);
                                    break;
                                case StudentListConstant.MobileNumberFather:
                                    xRow.Add(c.GuardiansMobile1);
                                    break;
                                case StudentListConstant.MobileNumberMother:
                                    xRow.Add(c.GuardiansMobile2);
                                    break;
                                case StudentListConstant.FathersName:
                                    xRow.Add(c.FatherName);
                                    break;
                                case StudentListConstant.InstituteName:
                                    xRow.Add(c.Institute);
                                    break;
                                case StudentListConstant.Email:
                                    xRow.Add(c.Email);
                                    break;
                                case StudentListConstant.Branch:
                                    xRow.Add(c.Branch);
                                    break;
                                case StudentListConstant.Batch:
                                    xRow.Add(c.Batch);
                                    break;
                                case StudentListConstant.District:
                                    xRow.Add(c.District);
                                    break;
                                case StudentListConstant.VersionOfStudy:
                                    if (c.VersionOfStudy != null)
                                    {
                                        xRow.Add(((VersionOfStudy)c.VersionOfStudy).ToString());
                                        break;
                                    }
                                    else
                                    {
                                        xRow.Add("");
                                        break;
                                    }
                                case StudentListConstant.Gender:
                                    if (c.Gender != null)
                                    {
                                        xRow.Add(((Gender)c.Gender).ToString());
                                        break;
                                    }
                                    else
                                    {
                                        xRow.Add("");
                                        break;
                                    }
                                case StudentListConstant.Religion:
                                    if (c.Religion != null)
                                    {
                                        xRow.Add(((Religion)c.Religion).ToString());
                                        break;
                                    }
                                    else
                                    {
                                        xRow.Add("");
                                        break;
                                    }

                                default:
                                    break;
                            }
                        }

                    }
                    studentExcelList.Add(xRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "student-info-report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return View("AutoClose");
            }

        }
        public ActionResult ExportBatchWiseAdmissionResult(string organizationNameHeader, string programSessionNameHeader, string studentListReportObj)
        {
            try
            {
                #region Authorization

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentSearchInfo =
                        new JavaScriptSerializer().Deserialize<StudentInfoSearch>(
                            studentListReportObj);
                IList<BatchWiseAdmissionReportDto> batchWiseAdmissionReportDto =
                        _informationService.LoadAuthorizedBatchAdmissionReport(_userMenu, _commonHelper.ConvertIdToList(studentSearchInfo.ProgramId), _commonHelper.ConvertIdToList(studentSearchInfo.SessionId), studentSearchInfo.SelectedBranch.ToList(), studentSearchInfo.SelectedCampus.ToList(), studentSearchInfo.SelectedBatchDays, studentSearchInfo.SelectedBatchTime, studentSearchInfo.SelectedVersions, studentSearchInfo.SelectedGenders, studentSearchInfo.SelectedBatch, 0, 0).OrderBy(x => x.BatchRank).ToList();
                #endregion

                List<string> headerList = new List<string>();
                headerList.Add(organizationNameHeader);
                headerList.Add(programSessionNameHeader);
                headerList.Add("Batch Wise Admission Report");
                headerList.Add("");
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                if (studentSearchInfo.InformationViewList != null && !studentSearchInfo.InformationViewList.Contains(""))
                {
                    foreach (var info in studentSearchInfo.InformationViewList)
                    {
                        columnList.Add(info);
                    }
                }
                else
                {
                    columnList.Add("Branch");
                    columnList.Add("Campus");
                    columnList.Add("Version");
                    columnList.Add("Gender");
                    columnList.Add("Batch");
                    columnList.Add("Total Capacity");
                    columnList.Add("Student Admit");
                    columnList.Add("Available Capacity");
                }
                var data = new List<object>();
                var batchwiseAdmissionExcelList = new List<List<object>>();
                foreach (var c in batchWiseAdmissionReportDto)
                {
                    var xRow = new List<object>();
                    if (studentSearchInfo.InformationViewList != null && !studentSearchInfo.InformationViewList.Contains(""))
                    {
                        foreach (string informationView in studentSearchInfo.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case "Branch":
                                    xRow.Add(c.Branch);
                                    break;
                                case "Campus":
                                    xRow.Add(c.Campus);
                                    break;
                                case "Batch Days":
                                    xRow.Add(c.BatchDays);
                                    break;
                                case "Batch Time":
                                    string startTime = Convert.ToDateTime(c.BatchStartTime).ToString("hh:mm tt");
                                    string endTime = Convert.ToDateTime(c.BatchEndTime).ToString("hh:mm tt");
                                    xRow.Add(startTime + " To " + endTime);
                                    break;
                                case "Version":
                                    var versionEnumValue = (VersionOfStudy)c.Version;
                                    string versionEnumName = versionEnumValue.ToString();
                                    xRow.Add(versionEnumName);
                                    break;

                                case "Gender":
                                    var genderEnumValue = (Gender)c.Gender;
                                    string genderEnumName = genderEnumValue.ToString();
                                    xRow.Add(genderEnumName);
                                    break;

                                case "Batch Name":

                                    xRow.Add(c.Batch);
                                    break;

                                case "Total Capacity":
                                    xRow.Add(c.TotalCapacity.ToString());
                                    break;

                                case "Student Admit":

                                    xRow.Add(c.StudentAdmit.ToString());
                                    break;

                                case "Available Capacity":
                                    xRow.Add(c.AvailableCapacity.ToString());
                                    break;


                                default:
                                    break;
                            }
                        }

                    }
                    else
                    {
                        xRow.Add(c.Branch);
                        xRow.Add(c.Campus);
                        var versionEnumValue = (VersionOfStudy)c.Version;
                        string versionEnumName = versionEnumValue.ToString();
                        xRow.Add(versionEnumName);
                        var genderEnumValue = (Gender)c.Gender;
                        string genderEnumName = genderEnumValue.ToString();
                        xRow.Add(genderEnumName);
                        xRow.Add(c.Batch);
                        xRow.Add(c.TotalCapacity.ToString());
                        xRow.Add(c.StudentAdmit.ToString());
                        xRow.Add(c.AvailableCapacity.ToString());
                    }
                    batchwiseAdmissionExcelList.Add(xRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, batchwiseAdmissionExcelList, footerList,
                                        "batchwise-admission-report__"
                                        + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return View("AutoClose");
            }

        }


        #endregion

        #region Update Info Receiver
        [AllowAnonymous]
        public void Test()
        {

            var ss = @"{'SmsList':[{'Id':45763,'Sender':'8801833168172','Time':'23/8/2015 4:54:20 PM','Text':'SSC DHA 123465 2014 12150909680'}]}"
            ;
            UpdateStudentInfos(ss);

            PendingSmsRetriveAndSend();
        }

        [AllowAnonymous]
        public void TestParam(string exam = "SSC", string bord = "DHA", string roll = "123465", string year = "214", string prn = "12150909680", string mobile = "8801833168172")
        {
            string time = DateTime.Now.ToString();
            var ss = @"{'SmsList':[{'Id':45763,'Sender':'" + mobile + "','Time':'" + time + "','Text':'" + exam + " " + bord + " " + roll + " " + year + " " + prn + "'}]}";
            UpdateStudentInfos(ss);
            PendingSmsRetriveAndSend();
        }

        [AllowAnonymous]
        public void UpdateStudentInfos(string data = "")
        {
            try
            {
                var smsSenders = new List<SmsSender>();
                if (!String.IsNullOrEmpty(data))
                {
                    var serializer = new JavaScriptSerializer();
                    //deserialize data 
                    var smsSenderListJson = serializer.Deserialize<RootObject>(data);
                    if (smsSenderListJson == null || !smsSenderListJson.SmsList.Any()) return;
                    foreach (var smsSenderJson in smsSenderListJson.SmsList)
                    {
                        var newSmsSenderObj = new SmsSender
                        {
                            ResponseCode = smsSenderJson.Id.ToString(),
                            Text = smsSenderJson.Text,
                            Time = Convert.ToDateTime(smsSenderJson.Time),
                            Sender = smsSenderJson.Sender,
                            Status = SmsSender.EntityStatus.Pending
                        };
                        smsSenders.Add(newSmsSenderObj);
                    }
                    _smsSenderService.Save(smsSenders);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        [AllowAnonymous]
        public ContentResult PendingSmsRetriveAndSend()
        {
            try
            {
                if (Session["SCHDULLER_CALLLOG"] == null)
                {
                    Session["SCHDULLER_CALLLOG"] = false;
                }
                var previousCallLog = (bool)Session["SCHDULLER_CALLLOG"];
                if (previousCallLog == false)
                {
                    Session["SCHDULLER_CALLLOG"] = true;
                    var prevStudentAcademicInfo = new List<StudentAcademicInfo>();
                    var updateStudentAcademicInfos = new List<StudentAcademicInfo>();
                    var savableAcademicInfos = new List<StudentAcademicInfo>();

                    IList<SmsSender> pendingSmsSenders = _smsSenderService.LoadPendingSms();

                    if (pendingSmsSenders.Any())
                    {
                        WsSmsLocal[] wsSmsesList = new WsSmsLocal[pendingSmsSenders.Count];
                        int index = 0;
                        foreach (var pendingSmsSender in pendingSmsSenders)
                        {
                            string prnNo = "";
                            WsSmsLocal wsSms = new WsSmsLocal();
                            bool isvalidMessFormat = ChkValidMessage(pendingSmsSender.Text, out prnNo);
                            // sms format validation check
                            var infoAray = pendingSmsSender.Text.Split(' ');
                            if (infoAray.Count() != 5)
                            {
                                wsSms.MobileNumber = pendingSmsSender.Sender;
                                wsSms.SmsText =
                                    @"Sorry: Please follow the format below: Exam <space> first 3 letters of Board <space> Board Roll <space> Exam Year <space> Program Roll. Example: SSC DHA 123456 2013 12150900000";
                                wsSms.Type = "Text";
                                wsSms.PrnNo = prnNo;
                                wsSmsesList[index] = wsSms;
                            }
                            else
                            {
                                var examName = infoAray[0];
                                var boardName = infoAray[1];
                                var boardRoll = infoAray[2];
                                var year = infoAray[3];
                                var prn = infoAray[4];
                                if (isvalidMessFormat)
                                {
                                    BusinessModel.Entity.Students.Student student =
                                        _studentService.GetByMobileAndPrnNumber(prnNo,
                                            pendingSmsSender.Sender);
                                    //var newSmsSenderObj = new SmsSender
                                    //{
                                    //    ResponseCode = pendingSmsSender.Id.ToString(),
                                    //    Text = pendingSmsSender.Text,
                                    //    Time = Convert.ToDateTime(pendingSmsSender.Time),
                                    //    Sender = pendingSmsSender.Sender
                                    //};
                                    //smsSenders.Add(newSmsSenderObj);
                                    if (student != null)
                                    {
                                        StudentAcademicInfo acdInfo = null;
                                        if (student.StudentAcademicInfos.Count > 0)
                                        {
                                            acdInfo =
                                                student.StudentAcademicInfos.SingleOrDefault(
                                                    x =>
                                                        x.StudentExam.Name.ToLower().Substring(0, 3) ==
                                                        examName.Trim().ToLower());
                                        }
                                        wsSms.MobileNumber = pendingSmsSender.Sender;
                                        wsSms.Type = "TEXT";
                                        wsSms.PrnNo = prnNo;
                                        if (acdInfo != null)
                                        {
                                            prevStudentAcademicInfo.Add(acdInfo);
                                        }
                                        if (acdInfo == null)
                                        {

                                            //var stdAcdINfo = new StudentAcademicInfo();
                                            StudentExam studentExam = _studentExamService.GetByExamName(examName);
                                            StudentBoard studentBoard = _studentBoardService.GetByBoardName(boardName);

                                            //stdAcdINfo.StudentExam = _studentExamService.GetByExamName(examName);
                                            //stdAcdINfo.StudentBoard = _studentBoardService.GetByBoardName(boardName);
                                            //stdAcdINfo.BoradRoll = boardRoll;
                                            //stdAcdINfo.Year = year;
                                            //stdAcdINfo.Student = student;

                                            //if (stdAcdINfo.StudentExam == null)
                                            //{
                                            if (studentExam == null)
                                            {
                                                wsSms.SmsText = "Invalid Exam Code.Try only 3 character from Starting ";
                                            }
                                            //else if (stdAcdINfo.StudentBoard == null)
                                            //{
                                            else if (studentBoard == null)
                                            {
                                                wsSms.SmsText = "Invalid Board Name.Try only 3 character from Starting";
                                            }
                                            //else if (stdAcdINfo.Year.Count() != 4)
                                            //{
                                            else if (year.Count() != 4)
                                            {
                                                wsSms.SmsText = "Invalid Year";
                                            }
                                            else
                                            {
                                                wsSms.SmsText = "Thanks " + student.NickName +
                                                                ", Your Board Info is successfully updated.";
                                            }
                                            //if (stdAcdINfo.StudentExam != null && stdAcdINfo.StudentBoard != null && 
                                            //    wsSms.SmsText == "Thanks " + stdAcdINfo.Student.NickName +
                                            //    ", Your Board Info is successfully updated.")
                                            //{
                                            if (studentExam != null && studentBoard != null &&
                                               wsSms.SmsText == "Thanks " + student.NickName +
                                               ", Your Board Info is successfully updated.")
                                            {
                                                var stdAcdINfo = new StudentAcademicInfo();
                                                stdAcdINfo.StudentExam = studentExam;
                                                stdAcdINfo.StudentBoard = studentBoard;
                                                stdAcdINfo.BoradRoll = boardRoll;
                                                stdAcdINfo.Year = year;
                                                stdAcdINfo.Student = student;

                                                savableAcademicInfos.Add(stdAcdINfo);
                                            }
                                            wsSmsesList[index] = wsSms;
                                            //wsSms.SmsText = "Invalid Exam Code.Try only 3 character from Starting ";
                                        }
                                        else
                                        {
                                            //acdInfo.StudentExam = _studentExamService.GetByExamName(examName);
                                            //acdInfo.StudentBoard = _studentBoardService.GetByBoardName(boardName);
                                            StudentExam studentExam = _studentExamService.GetByExamName(examName);
                                            StudentBoard studentBoard = _studentBoardService.GetByBoardName(boardName);

                                            //acdInfo.BoradRoll = boardRoll;
                                            //acdInfo.Year = year;

                                            //if (acdInfo.StudentExam == null)
                                            //{
                                            if (studentExam == null)
                                            {
                                                wsSms.SmsText = "Invalid Exam Code.Try only 3 character from Starting ";
                                            }
                                            //else if (acdInfo.StudentBoard == null)
                                            //{
                                            else if (studentBoard == null)
                                            {
                                                wsSms.SmsText = "Invalid Board Name.Try only 3 character from Starting";
                                            }
                                            else if (acdInfo.Year.Count() != 4)
                                            {
                                                wsSms.SmsText = "Invalid Year";
                                            }
                                            else
                                            {
                                                wsSms.SmsText = "Thanks " + student.NickName +", Your Board Info is successfully updated.";
                                            }
                                            //if (acdInfo.StudentExam != null && acdInfo.StudentBoard != null &&
                                            //    wsSms.SmsText == "Thanks " + acdInfo.Student.NickName +
                                            //    ", Your Board Info is successfully updated.")
                                            //{
                                            if (studentExam != null && studentBoard != null && wsSms.SmsText == "Thanks " + student.NickName +", Your Board Info is successfully updated.")
                                            {
                                                acdInfo.StudentExam = studentExam;
                                                acdInfo.StudentBoard = studentBoard;
                                                acdInfo.BoradRoll = boardRoll;
                                                acdInfo.Year = year;
                                                updateStudentAcademicInfos.Add(acdInfo);
                                            }
                                            wsSmsesList[index] = wsSms;
                                        }
                                    }
                                    else
                                    {
                                        wsSms.MobileNumber = pendingSmsSender.Sender;
                                        wsSms.SmsText =
                                            "Sorry Mobile Number and Program Roll do not match. Please send correctly.";
                                        wsSms.Type = "Text";
                                        wsSms.PrnNo = prnNo;
                                        wsSmsesList[index] = wsSms;
                                    }
                                }
                            }
                            index++;
                        }
                        bool isSaveSuccess =
                            _studentAcademicInfoService.SaveSmsSenderAndUpdateStudentAcademicInfo(pendingSmsSenders,
                                updateStudentAcademicInfos, prevStudentAcademicInfo, savableAcademicInfos);
                       //Temporary commit
                        if (isSaveSuccess)
                        {
                            bool isSmsProcessSuccessfullyCompleted = _smsHistoryService.SendPendingSms(wsSmsesList,
                                pendingSmsSenders);
                            if (isSmsProcessSuccessfullyCompleted)
                            {
                                Session["SCHDULLER_CALLLOG"] = false;
                                return Content("Success");

                            }
                            Session["SCHDULLER_CALLLOG"] = false;
                            return Content("SendPendingSms db operation failed ");
                        }
                        //Temporary commit ENd
                        Session["SCHDULLER_CALLLOG"] = false;
                        return Content("SaveSmsSenderAndUpdateStudentAcademicInfo Failed Db operation");
                    }
                    Session["SCHDULLER_CALLLOG"] = false;
                    return Content("No pending SMS");
                }
                return Content("Schiduler busy");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        private bool ChkValidMessage(string message, out string prnNo)
        {
            var indexArray = message.Split(' ');
            if (indexArray.Count() == 5)
            {
                prnNo = indexArray[4];
                return true;
            }
            prnNo = null;
            return false;
        }
        #endregion
    }
    public class SmsList
    {
        public int Id { get; set; }
        public string Sender { get; set; }
        public string Time { get; set; }
        public string Text { get; set; }
    }

    public class RootObject
    {
        public List<SmsList> SmsList { get; set; }
    }


}