﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Util;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface ICourseProgressDao : IBaseDao<CourseProgress, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        List<CourseProgress> LoadCourseProgress(int start, int length, string branch, string campus, string batch, string subject, string lecture, IList<Lecture> lectureList, List<Batch> batchList);
        #endregion

        #region Others Function

        int GetCourseProgressCount(string branch, string campus, string batch,
            string subject, string lecture, IList<Lecture> lectureList, List<Batch> batchList);
        IList<DateTime> GetLectureDate(long lectureId, long? batchId=null);
        #endregion

        #region Helper Function

        #endregion


        
    }
    public class CourseProgressDao : BaseDao<CourseProgress, long>, ICourseProgressDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function

        public List<CourseProgress> LoadCourseProgress(int start, int length, string branch, string campus, string batch, string subject, string lecture, IList<Lecture> lectureList, List<Batch> batchList)
        {
            var criteria = LoadCourseProgressCriteria(branch, campus, batch, subject, lecture, lectureList, batchList);
            criteria.SetFirstResult(start).SetMaxResults(length);
            return criteria.List<CourseProgress>().ToList();
        }

        

        #endregion

        #region Others Function

        public int GetCourseProgressCount(string branch, string campus, string batch,
            string subject, string lecture, IList<Lecture> lectureList, List<Batch> batchList)
        {
            var criteria = LoadCourseProgressCriteria(branch, campus, batch, subject, lecture, lectureList, batchList);
            return criteria.SetProjection(Projections.RowCount()).UniqueResult<int>();
        }

        public IList<DateTime> GetLectureDate(long lectureId, long? batchId=null)
        {

            var courseProgress =
                Session.Query<CourseProgress>()
                    .Where(x => x.Lecture.Id == lectureId);
            if (batchId != null)
            {
                courseProgress=courseProgress.Where(x => x.Batch.Id == batchId);
            }

            var heldDate =courseProgress.Select(x => x.HeldDate).ToList();
            return heldDate;
        }
        #endregion

        #region Helper Function
        private ICriteria LoadCourseProgressCriteria(string branch, string campus, string batch, string subject, string lecture,
            IList<Lecture> lectureList, List<Batch> batchList)
        {
            var criteria = Session.CreateCriteria<CourseProgress>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", CourseProgress.EntityStatus.Delete)));
            criteria.CreateAlias("Batch", "b").Add(Restrictions.Eq("b.Status", CourseProgress.EntityStatus.Active));
            criteria.CreateAlias("Lecture", "l").Add(Restrictions.Eq("l.Status", CourseProgress.EntityStatus.Active));
            criteria.Add(Restrictions.In("b.Id", batchList.Select(x => x.Id).ToArray()));
            criteria.Add(Restrictions.In("l.Id", lectureList.Select(x => x.Id).ToArray()));
            if (!string.IsNullOrEmpty(branch))
            {
                criteria.CreateAlias("b.Branch", "br");
                criteria.Add(Restrictions.InsensitiveLike("br.Name", branch.ToLower(), MatchMode.Anywhere));
            }
            if (!string.IsNullOrEmpty(campus))
            {
                criteria.CreateAlias("b.Campus", "Camp");
                criteria.Add(Restrictions.InsensitiveLike("Camp.Name", campus.ToLower(), MatchMode.Anywhere));
            }
            if (!string.IsNullOrEmpty(batch))
            {
                criteria.Add(Restrictions.InsensitiveLike("b.Name", batch.ToLower(), MatchMode.Anywhere));
            }
            if (!string.IsNullOrEmpty(subject))
            {
                criteria.CreateAlias("l.LectureSettings", "ls");
                if (subject.ToLower() == "combined")
                {
                    criteria.Add(Restrictions.IsNull("ls.CourseSubject"));
                }
                else
                {
                    criteria.CreateAlias("ls.CourseSubject", "cs");
                    criteria.CreateAlias("cs.Subject", "s");
                    criteria.Add(Restrictions.InsensitiveLike("s.Name", subject.ToLower(), MatchMode.Anywhere));
                }
            }
            if (!string.IsNullOrEmpty(lecture))
            {
                criteria.Add(Restrictions.InsensitiveLike("l.Name", lecture, MatchMode.Anywhere));
            }
            return criteria;
        }
        #endregion

       
    }
}
