﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.MessageExceptions;
namespace UdvashERP.Services.Hr
{
    public interface IChildrenInfoService : IBaseService
    {
        #region Operational Function
        List<long> UpdateChildrenInfo(TeamMember member, List<TeammemberChildrenArray> childrenInfoArray);
        bool DeleteChildrenInfo(long id);
        #endregion

        #region Single Instances Loading Function

        ChildrenInfo GetChildrenInfo(long id);

        #endregion

        #region List Loading Function
        List<ChildrenInfo> GetTeamMemberChildrenInfo(long teamMemberId);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion        
    }
    public class ChildrenInfoService : BaseService, IChildrenInfoService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IChildrenInfoDao _childrenInfoDao;
        private readonly IAllowanceSheetSecondDetailsDao _allowanceSheetSecondDetailsDao;
        public ChildrenInfoService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _childrenInfoDao = new ChildrenInfoDao{Session = session};
            _allowanceSheetSecondDetailsDao = new AllowanceSheetSecondDetailsDao(){Session = session};

        }
        #endregion

        #region Operational Functions
        public List<long> UpdateChildrenInfo(TeamMember member, List<TeammemberChildrenArray> childrenInfoArray)
        {
            List<long> idList  = new List<long>();
            ITransaction trans = null;
            
            try
            {
                using (trans = Session.BeginTransaction())
                {

                    if (childrenInfoArray.Any())
                    {
                        ChildrenInfo childrenInfo = null;
                        foreach (var child in childrenInfoArray)
                        {
                            var canSaveLog = false;
                            if (child.Id > 0)
                            {
                                childrenInfo = GetChildrenInfo(child.Id);

                                #region Is Post Children allowance

                                AllowanceSheetSecondDetails allowanceSheetSecondDetails =
                                    _allowanceSheetSecondDetailsDao.GetTeamMemberIsSubmittedChildrenAllowanceSheetDetails(child.Id);
                                if (allowanceSheetSecondDetails != null && childrenInfo.Dob != Convert.ToDateTime(child.Dob))
                                {
                                    throw new InvalidDataException("You can't change "+child.Name+"'s date of birth. One or more allowance already posted for him/her.");
                                }
                                #endregion

                                
                                if (
                                    childrenInfo.Name != child.Name
                                    || Convert.ToDateTime(childrenInfo.Dob).Date != Convert.ToDateTime(child.Dob).Date
                                    || childrenInfo.Gender != Convert.ToInt32(child.Gender)
                                    || childrenInfo.Status != Convert.ToInt32(child.Status)
                                    || childrenInfo.IsStudying != (Convert.ToInt32(child.Studying) == 1)
                                    || childrenInfo.Remarks != child.Remarks
                                    )
                                {
                                    canSaveLog = true;
                                }
                                    

                            }
                            else
                            {
                                childrenInfo = new ChildrenInfo();
                                canSaveLog = true;
                            }

                            childrenInfo.Name = child.Name;
                            childrenInfo.Dob = Convert.ToDateTime(child.Dob);
                            childrenInfo.Gender = Convert.ToInt32(child.Gender);
                            childrenInfo.IsStudying = (Convert.ToInt32(child.Studying) == 1);
                            childrenInfo.Status = Convert.ToInt32(child.Status);
                            childrenInfo.TeamMember = member;
                            childrenInfo.Remarks = child.Remarks;
                            _childrenInfoDao.SaveOrUpdate(childrenInfo);
                            if (canSaveLog)
                                SaveChildrenInfoLog(childrenInfo);
                            idList.Add(childrenInfo.Id);

                        }
                    }
                    trans.Commit();
                    
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
            return idList;
        }

        public bool DeleteChildrenInfo(long id)
        {

            ITransaction trans = null;
            try
            {
                #region Is Post Children allowance

                AllowanceSheetSecondDetails allowanceSheetSecondDetails =
                    _allowanceSheetSecondDetailsDao.GetTeamMemberIsSubmittedChildrenAllowanceSheetDetails(id);
                if (allowanceSheetSecondDetails!=null)
                {
                    throw new InvalidDataException("You can't remove this children information. One or more allowance already posted for this.");
                }
                #endregion

                using (trans = Session.BeginTransaction())
                {

                    ChildrenInfo childrenInfo = GetChildrenInfo(id);
                    childrenInfo.Status = ChildrenInfo.EntityStatus.Delete;
                    _childrenInfoDao.SaveOrUpdate(childrenInfo);
                    SaveChildrenInfoLog(childrenInfo);
                    trans.Commit();
                    return true;
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }


        public bool Save()
        {
            return true;
        }

        #endregion

        #region Single Instances Loading Function
        public ChildrenInfo GetChildrenInfo(long Id)
        {
            try
            {
                return _childrenInfoDao.LoadById(Id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }


        #endregion

        #region List Loading Function

        public List<ChildrenInfo> GetTeamMemberChildrenInfo(long teamMemberId)
        {
            try
            {
                return _childrenInfoDao.GetTeamMemberChildrenInfo(teamMemberId).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region Others Function

        private void SaveChildrenInfoLog(ChildrenInfo childreninfo)
        {
            try
            {
                var hrChildrenInfoLog = new ChildrenInfoLog();
                hrChildrenInfoLog.TeamMemberId = childreninfo.TeamMember.Id;
                hrChildrenInfoLog.ChildrenInfoId = childreninfo.Id;
                hrChildrenInfoLog.CreationDate = DateTime.Now;
                hrChildrenInfoLog.CreateBy = GetCurrentUserId();
                hrChildrenInfoLog.Status = childreninfo.Status;
                hrChildrenInfoLog.Name = childreninfo.Name;
                hrChildrenInfoLog.Dob = childreninfo.Dob;
                hrChildrenInfoLog.Gender = childreninfo.Gender;
                hrChildrenInfoLog.IsStudying = childreninfo.IsStudying;
                hrChildrenInfoLog.Remarks = childreninfo.Remarks;
                Session.Save(hrChildrenInfoLog);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            
        }

        #endregion

        #region Helper function



        #endregion
       
    }
}
