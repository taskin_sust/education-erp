﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public partial class TeacherActivityMap : BaseClassMap<TeacherActivity, long>
    {
    public TeacherActivityMap() {
			Table("TeacherActivity");
			LazyLoad();
			Map(x => x.Name);
			Map(x => x.Type);
            HasMany(x => x.TeacherActivityPriorities).KeyColumn("TeacherActivityId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.TeacherAdmissionInfos).KeyColumn("TeacherActivityId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
