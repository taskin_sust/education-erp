﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class SpecialAllowanceController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private SpecialAllowanceSetting _specialAllowanceSetting;  
        private ISpecialAllowanceSettingService _specialAllowanceSettingService;
        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _userMenus;

        public SpecialAllowanceController()
        {
            var currentSession = NHibernateSessionFactory.OpenSession();
            _specialAllowanceSetting = new SpecialAllowanceSetting();
            _specialAllowanceSettingService = new SpecialAllowanceSettingService(currentSession);
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Function

        #region Manage

        public ActionResult Index()
        {
            try
            {
                Init();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #region Render Data Table

        [HttpPost]
        public JsonResult SpecialAllowanceSettingList(int draw, int start, int length, int? pin, string name, long? paymentType)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            bool isSuccess = false;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "teamMember.Pin";
                            break;
                        case "1":
                            orderBy = "teamMember.Name";
                            break;
                        case "2":
                            orderBy = "PaymentType";
                            break;
                        case "3":
                            orderBy = "PaymentType";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                _userMenus = (List<UserMenu>)ViewBag.UserMenu;

                recordsTotal = _specialAllowanceSettingService.GetSpecialAllowanceSettingCount(_userMenus, orderBy, orderDir, pin, name, paymentType);
                recordsFiltered = recordsTotal;

                IList<SpecialAllowanceSetting> specialAllowanceSettingList = _specialAllowanceSettingService.LoadSpecialAllowanceSettings(_userMenus, start, length, orderBy, orderDir, pin, name, paymentType).ToList();

                foreach (var specialAllowance in specialAllowanceSettingList)
                {
                    if (specialAllowance.ClosingDate != null)
                    {
                        DateTime x;
                        DateTime.TryParse(specialAllowance.ClosingDate.ToString(), out x);
                        specialAllowance.ClosingDateString = x.ToString("MMM dd, yyyy");
                    }
                    else
                    {
                        _specialAllowanceSetting.ClosingDateString = "";
                    }
                    var str = new List<string>
                    {
                        specialAllowance.TeamMember.Pin.ToString(),
                        specialAllowance.TeamMember.Name,
                        specialAllowance.Amount.ToString("n2"),
                        _commonHelper.GetEmumIdToValue<PaymentType>(specialAllowance.PaymentType),
                        specialAllowance.EffectiveDate.ToString("MMM dd, yyyy"),
                        specialAllowance.ClosingDateString ,
                        specialAllowance.Remarks,
                        LinkGenerator.GetGeneratedLink("SpecialAllowanceDetail", "SpecialAllowanceUpdate", "Delete", "SpecialAllowance", specialAllowance.Id, "PIN: "+specialAllowance.TeamMember.Pin+" Name: "+specialAllowance.TeamMember.Name)
                        //LinkGenerator.GetEditLink("SpecialAllowanceUpdate", "SpecialAllowance", specialAllowance.Id)
                    };
                    getData.Add(str);
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        #endregion


        #endregion

        #region Save

        [HttpGet]
        public ActionResult NewSpecialAllowance()
        {
            Init();
            return View(new SpecialAllowanceSetting());
        }

        [HttpPost]
        public ActionResult NewSpecialAllowance(SpecialAllowanceSetting specialAllowanceSetting)
        {
            try
            {
                #region Basic Validation

                if (specialAllowanceSetting.ResponsibleMemberPin <= 0)
                {
                    throw new InvalidDataException("Invalid Member PIN");
                }

                var paymentType = specialAllowanceSetting.PaymentType;
                if (!Enum.IsDefined(typeof(PaymentType), paymentType))
                    throw new InvalidDataException("Invalid payment type");

                #endregion

                _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                DateTime effectiveDate;
                DateTime.TryParse(specialAllowanceSetting.EffectiveDateString, out effectiveDate);
                specialAllowanceSetting.EffectiveDate = effectiveDate;

                if (specialAllowanceSetting.ClosingDateString != null)
                {
                    DateTime closingDate;
                    DateTime.TryParse(specialAllowanceSetting.ClosingDateString, out closingDate);
                    //DateTime today = DateTime.Today;
                    DateTime endOfMonth = new DateTime(closingDate.Year, closingDate.Month, DateTime.DaysInMonth(closingDate.Year, closingDate.Month));
                    specialAllowanceSetting.ClosingDate = endOfMonth;
                }
                else
                {
                    specialAllowanceSetting.ClosingDate = null;
                }
                _specialAllowanceSettingService.Save(_userMenus, specialAllowanceSetting);
                specialAllowanceSetting = new SpecialAllowanceSetting();
                ModelState.Clear();
                Init();
                ViewBag.SuccessMessage = "Special allowance successfully saved";
                return View(specialAllowanceSetting);
            }

            catch (NullObjectException ex)
            {
                //return Json(new Response(false, ex.Message));
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                //return Json(new Response(false, ex.Message));
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (MessageException ex)
            {
                // return Json(new Response(false, ex.Message));
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                // return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            finally
            {
                ModelState.Clear();
                Init();
            }
            //ModelState.Clear();
            //Init();
            return View();
        }

        #endregion

        #region Update

        [HttpGet]
        public ActionResult SpecialAllowanceUpdate(long id)
        {
            try
            {
                InitUpdate(id);
                if (_specialAllowanceSetting == null)
                    throw new MessageException("Invalid special allowance");
                return View(_specialAllowanceSetting);
            }
            catch (MessageException ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                throw;
            }
        }

        [HttpPost]
        public ActionResult SpecialAllowanceUpdate(SpecialAllowanceSetting specialAllowanceSetting, long id)
        {
            try
            {
                #region validation

                if (specialAllowanceSetting.ResponsibleMemberPin <= 0)
                {
                    throw new InvalidDataException("Invalid Member PIN");
                }

                var paymentType = specialAllowanceSetting.PaymentType;
                if (!Enum.IsDefined(typeof(PaymentType), paymentType))
                    throw new InvalidDataException("Invalid payment type");

                #endregion

                _userMenus = (List<UserMenu>)ViewBag.UserMenu;

                DateTime effectiveDate;
                DateTime.TryParse(specialAllowanceSetting.EffectiveDateString, out effectiveDate);

                if (specialAllowanceSetting.ClosingDateString != null)
                {
                    DateTime closingDate;
                    DateTime.TryParse(specialAllowanceSetting.ClosingDateString, out closingDate);
                    //DateTime today = DateTime.Today;
                    DateTime endOfMonth = new DateTime(closingDate.Year, closingDate.Month, DateTime.DaysInMonth(closingDate.Year, closingDate.Month));
                    specialAllowanceSetting.ClosingDate = endOfMonth;

                    if (DateTime.Compare(specialAllowanceSetting.EffectiveDate, specialAllowanceSetting.ClosingDate.Value.Date) >= 0)
                    {
                        throw new InvalidDataException("Effective date can't be less than or equal closing date!");
                    }
                }
                else
                {
                    specialAllowanceSetting.ClosingDate = null;
                }
                specialAllowanceSetting.EffectiveDate = effectiveDate;
                _specialAllowanceSettingService.Update(_userMenus, specialAllowanceSetting);
                ViewBag.SuccessMessage = "Allowance update successfully";
                InitUpdate(id);
                return View(_specialAllowanceSetting);
            }
            catch (NullObjectException ex)
            {
                //return Json(new Response(false, ex.Message));
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                //return Json(new Response(false, ex.Message));
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (MessageException ex)
            {
                //return Json(new Response(false, ex.Message));
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                //return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            InitUpdate(id);
            return View(_specialAllowanceSetting);
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                bool isSuccess = _specialAllowanceSettingService.Delete(_userMenus, id);
                //return Json(isSuccess ? new Response(true, "Special allowance settings sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
                if (isSuccess)
                    return Json(new Response(true, "Special allowance settings deleted."));
                return Json(new Response(false, "Special allowance settings Delete Fail !"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Special allowance settings Delete Fail !"));
            }
        }

        [HttpGet]
        public ActionResult SpecialAllowanceDetail(long id)
        {
            try
            {
                InitUpdate(id);
                if (_specialAllowanceSetting == null)
                    throw new MessageException("Invalid special allowance");
                return View(_specialAllowanceSetting);
            }
            catch (MessageException ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                throw;
            }
        }

        #endregion

        #endregion

        #region Ajax Request

        #endregion

        #region Helper

        private void Init()
        {
            ViewBag.PaymentType = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentType>(), "Key", "Value");
        }

        private void InitUpdate(long id)
        {
            try
            {
                _specialAllowanceSetting = _specialAllowanceSettingService.GetSpecialAllowanceSettings(id);
                if (_specialAllowanceSetting == null)
                    return;
                _specialAllowanceSetting.ResponsibleMemberPin = _specialAllowanceSetting.TeamMember.Pin;
                _specialAllowanceSetting.Amount = _specialAllowanceSetting.Amount;
                _specialAllowanceSetting.EffectiveDateString = _specialAllowanceSetting.EffectiveDate.ToString("MMM yyyy");
                _specialAllowanceSetting.ClosingDateString = _specialAllowanceSetting.ClosingDate.ToString();
                ViewBag.EffectiveDateString = _specialAllowanceSetting.EffectiveDateString;
                ViewBag.EffectiveDate = _specialAllowanceSetting.EffectiveDate.ToString("MMM yyyy");

                if (!string.IsNullOrEmpty(_specialAllowanceSetting.ClosingDateString))
                {
                    DateTime closingDate;
                    DateTime.TryParse(_specialAllowanceSetting.ClosingDateString, out closingDate);
                    ViewBag.ClosingDateString = closingDate.ToString("MMM yyyy");
                }

                ViewBag.PaymentType = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentType>(), "Key", "Value");
                ViewBag.SelectedPaymentType = _specialAllowanceSetting.PaymentType;

                if (_specialAllowanceSetting != null)
                {
                    _specialAllowanceSetting.CreateByText = _specialAllowanceSettingService.GetUserNameByAspNetUserId(_specialAllowanceSetting.CreateBy);
                    _specialAllowanceSetting.ModifyByText = _specialAllowanceSettingService.GetUserNameByAspNetUserId(_specialAllowanceSetting.ModifyBy);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }


        #endregion
    }
}