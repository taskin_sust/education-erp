using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class AttendanceSummaryMap : BaseClassMap<AttendanceSummary,long> {
        
        public AttendanceSummaryMap() {
			Table("HR_AttendanceSummary");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.ZoneSetting).Column("ZoneId");
            References(x => x.HolidaySetting).Column("HolidaySettingId");
            References(x => x.InDevice).Column("InDeviceId");
            References(x => x.OutDevice).Column("OutDeviceId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.AttendanceDate).Column("AttendanceDate");
			Map(x => x.EarlyEntry).Column("EarlyEntry");
			Map(x => x.LateLeave).Column("LateLeave");
			Map(x => x.TotalOvertime).Column("TotalOvertime");
			Map(x => x.LateEntry).Column("LateEntry");
			Map(x => x.EarlyLeave).Column("EarlyLeave");
			//Map(x => x.CalendarLeave).Column("CalendarLeave");
			Map(x => x.InTime).Column("InTime");
			Map(x => x.OutTime).Column("OutTime");
			Map(x => x.IsPosted).Column("IsPosted");
            Map(x => x.IsWeekend).Column("IsWeekend");
        }
    }
}
