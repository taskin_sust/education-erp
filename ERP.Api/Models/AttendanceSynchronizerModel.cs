﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UdvashERP.Api.Models
{
    public class AttendanceSynchronizerModel
    {
        public AttendanceSynchronizerModel()
        {
            AttendanceDevices = new List<AttendanceDeviceModel>();
        }
        public virtual string Name { get; set; }
        public virtual int DataCallingInterval { get; set; }
        public virtual List<AttendanceDeviceModel> AttendanceDevices { get; set; }
    }
}