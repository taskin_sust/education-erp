﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Exam;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Exam
{
    public interface IExamMcqQuestionSetDao : IBaseDao<ExamMcqQuestionSet, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        ExamMcqQuestionSet LoadLastSetIdByExamId(long examId);

        #endregion

        #region List Loading Function

        List<IGrouping<int, ExamMcqQuestionSet>> LoadExamMcqQuestionSetList(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, 
            int start, int length, string orderBy, string orderDir, string organization, string program, string session, string course, string uniqueSet);
        IList<ExamMcqQuestionApiDto> LoadExamMcqQuestionSet(long examId, int examVersion,  string uniqueSets);
        IList<ExamMcqQuestionSet> LoadExamMcqQuestionSet(long examId, List<int> uniqueSet = null, int? examMcqQuestionSerial = null, int? setId = null);
        IList<ExamMcqQuestionSetDto> LoadExamMcqQuestionSet(List<long> authorizeOrgIds, List<long> authorizeProgramIds, List<long> authorizeSessionIds, int start, int length, string orderBy, string orderDir, string course, string uniqueSet,string exam);
        List<QuestionAnswerDto> LoadExamMcqQuestionSet(long examId, string code);

        #endregion

        #region Others Function

        int GetExamMcqQuestionSetRowCount(List<long> authorizeOrgIds, List<long> authorizeProgramIds, List<long> authorizeSessionIds, string course, string uniqueSet,string exam);
        bool HasExamGenerateSet(long id, int uniqueSet);

        #endregion

    }
    public class ExamMcqQuestionSetDao : BaseDao<ExamMcqQuestionSet, long>, IExamMcqQuestionSetDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public ExamMcqQuestionSet LoadLastSetIdByExamId(long examId)
        {
            ICriteria criteria =
                    Session.CreateCriteria<ExamMcqQuestionSet>()
                        .Add(Restrictions.Eq("Status", ExamMcqQuestionSet.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Exams.Id", examId));
            criteria.AddOrder(Order.Desc("SetId"));
            var list = criteria.List<ExamMcqQuestionSet>();
            if (list.Any())
                return list.FirstOrDefault();
            return null;
        }

        #endregion

        #region List Loading Function

        public List<IGrouping<int, ExamMcqQuestionSet>> LoadExamMcqQuestionSetList(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, int start, int length, string orderBy, string orderDir, string organization, string program, string session, string course, string uniqueSet)
        {
            ICriteria criteria = CriteriaObject(authorizeOrgIds, authorizeProgramIds, authorizeSessionIds, organization, program, session,
                course, uniqueSet);

            if (orderDir == "ASC")
            {
                criteria.AddOrder(Order.Asc(orderBy));
            }
            else
            {
                criteria.AddOrder(Order.Desc(orderBy));
            }
            var list = criteria.List<ExamMcqQuestionSet>().GroupBy(x => x.SetId);
            return list.Skip(start).Take(length).ToList();

        }

        public IList<ExamMcqQuestionApiDto> LoadExamMcqQuestionSet(long examId, int examVersion, string uniqueSets)
        {

            string query = @"Select a.SetId,a.SetNo,a.UniqueSet,a.ExamId from (
	                            SELECT emqs.*
	                            FROM [dbo].[ExamMcqQuestionSet] as emqs
	                            inner join 
		                            [dbo].[ExamMcqQuestion] as emq 
		                            ON emq.QuestionSerial = emqs.ExamMcqQuestionSerial AND emqs.ExamId = emqs.ExamId AND emqs.ExamId = " + examId + @" 
		                            AND emqs.UniqueSet = emq.UniqueSet 
	                            Where 1=1
		                            AND emq.Version =  " + examVersion + @"  
		                            AND emq.ExamId = " + examId + @" 
	                                AND emqs.UniqueSet IN (" + uniqueSets + @")
                            ) as a 
                            Group By a.SetId,a.SetNo,a.UniqueSet,a.ExamId order by a.SetId ";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ExamMcqQuestionApiDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<ExamMcqQuestionApiDto>().ToList();
        }

        public IList<ExamMcqQuestionSet> LoadExamMcqQuestionSet(long examId, List<int> uniqueSet = null, int? examMcqQuestionSerial = null, int? setId = null)
        {
            ICriteria criteria =
                    Session.CreateCriteria<ExamMcqQuestionSet>()
                        .Add(Restrictions.Eq("Status", ExamMcqQuestionSet.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Exams.Id", examId));
            if (uniqueSet != null)
            {
                criteria.Add(Restrictions.In("UniqueSet", uniqueSet));
            }
            if (examMcqQuestionSerial != null)
            {
                criteria.Add(Restrictions.Eq("ExamMcqQuestionSerial", examMcqQuestionSerial));
            }
            if (setId != null)
            {
                criteria.Add(Restrictions.Eq("SetId", setId));
            }

            return criteria.List<ExamMcqQuestionSet>();
        }
        
        public IList<ExamMcqQuestionSetDto> LoadExamMcqQuestionSet(List<long> authorizeOrgIds, List<long> authorizeProgramIds, List<long> authorizeSessionIds, int start,
            int length, string orderBy, string orderDir, string course,string uniqueSet,string exam)
        {
            string queryString = QueryString(authorizeOrgIds, authorizeProgramIds, authorizeSessionIds, course, uniqueSet, exam);

            string orderByString = " order by a.ExamId ASC,a.SetNo ASC";
            if (!string.IsNullOrEmpty(orderBy) && !string.IsNullOrEmpty(orderDir))
            {
                //orderByString = " order by " + orderBy +" "+orderDir ;
            }
            string query = @"select * from (
                            "+queryString+@"
                        ) as a " + orderByString + @"
                        ";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ExamMcqQuestionSetDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<ExamMcqQuestionSetDto>().Skip(start).Take(length).ToList();
         }
        
        private string QueryString(List<long> authorizeOrgIds, List<long> authorizeProgramIds, List<long> authorizeSessionIds,  string course, string uniqueSet, string exam)
        {
            string uniqueSetFilter = "";
            string courseFilter = "";
            string sessionFilter = "";
            string programFilter = "";
            string organizationFilter = "";
            string examString = "";
            if (!string.IsNullOrEmpty(uniqueSet))
            {
                uniqueSetFilter = " AND a.UniqueSet  = " + Convert.ToInt32(uniqueSet);
            }
            if (!string.IsNullOrEmpty(course))
            {
                courseFilter = " AND e.CourseId = " + Convert.ToInt64(course);
            }
            
            if (authorizeOrgIds != null && authorizeOrgIds.Any())
            {
                organizationFilter += " AND o.Id IN( " + string.Join(",", authorizeOrgIds) + ") ";
            }

            if (authorizeProgramIds != null && authorizeProgramIds.Any())
            {
                programFilter += " AND p.Id IN (" + string.Join(",", authorizeProgramIds) + ") ";
            }
            if (authorizeSessionIds != null && authorizeSessionIds.Any())
            {
                sessionFilter += " AND s.Id IN(" + string.Join(",", authorizeSessionIds) + ")";
            }
            if (!string.IsNullOrEmpty(exam))
            {
                examString = " AND ( e.ShortName like '%" + exam + @"%' OR e.Name like '%" + exam + @"%' OR e.Code like '%" + exam + @"%')";
            }
            string query = @"select 
                            o.ShortName as OrganizationShortName
                            , o.Name as OrganizationFullName
                            , p.Name as ProgramFullName
                            , p.ShortName as ProgramShortName
                            , s.Name as SessionName
                            , e.Id as ExamId
                            , e.ShortName + ' ('+e.Code+')' as ExamShortName
                            , e.Name + ' ('+e.Code+')' as ExamFullName
                            , a.UniqueSet as UniqueSet
                            , a.SetId as SetNo
                            , asp.UserName as GeneratedBy
                            , FORMAT(a.GeneratedDate,'MMM dd, yyyy') as GeneratedDate
                            , a.GeneratedDate as OriginalGeneratedDate
                            from (
                            select ExamId, UniqueSet, SetId ,  max(GeneratedBy) as GeneratedBy, max(GeneratedDate) as GeneratedDate from ExamMcqQuestionSet
                            where Status=" + ExamMcqQuestionSet.EntityStatus.Active+@" 
                            group by ExamId, UniqueSet, SetId
                        ) as a
                        INNER JOIN Exams as e on e.Id = a.ExamId and e.Status = "+Exams.EntityStatus.Active+@"
                        INNER JOIN Program as p on p.Id = e.ProgramId and p.Status=" + Program.EntityStatus.Active + @"
                        INNER JOIN Session as s on s.Id = e.SessionId and s.Status=" + BusinessModel.Entity.Administration.Session.EntityStatus.Active + @"
                        INNER JOIN Organization as o on o.Id = p.OrganizationId and o.Status=" + Organization.EntityStatus.Active + @"
                        INNER JOIn AspNetUsers as asp on asp.Id = a.GeneratedBy and asp.Status = " + AspNetUser.EntityStatus.Active + @"
                        where 
                        1=1
                        " + organizationFilter + @"
                        " + sessionFilter + @"
                        " + programFilter + @"
                        " + courseFilter + @"
                        " + uniqueSetFilter + @"
                        "+examString+@"
                        --order by a.ExamId , a.SetId";
            return query;
        }

        public List<QuestionAnswerDto> LoadExamMcqQuestionSet(long examId, string code)
        {
            
            string query = @"SELECT emqs.QuestionSerial, emqs.CorrectAnswer,emqs.SetId FROM [dbo].[ExamMcqQuestionSet] as emqs
                            INNER JOIN [dbo].[ExamMcqQuestionSetPrint] as emqsp ON emqsp.SetId = emqs.SetId AND emqsp.ExamId = emqs.ExamId
                            WHERE emqs.ExamId = " + examId + " AND emqsp.CodeNo = '" + code + "' ORDER BY QuestionSerial ASC";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<QuestionAnswerDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<QuestionAnswerDto>().ToList();
        }

        #endregion

        #region Others Function

        public int GetExamMcqQuestionSetRowCount(List<long> authorizeOrgIds, List<long> authorizeProgramIds, List<long> authorizeSessionIds,
             string course, string uniqueSet,string exam)
        {

            string queryString = QueryString(authorizeOrgIds, authorizeProgramIds, authorizeSessionIds, course, uniqueSet, exam);

            string query = @"select count(*) from (
                            " + queryString + @"
                        ) as a ";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(5000);
            return iQuery.UniqueResult<int>();
        }

        public bool HasExamGenerateSet(long examId, int uniqueSet)
        {
            ICriteria criteria = Session.CreateCriteria<ExamMcqQuestionSet>().Add(Restrictions.Not(Restrictions.Eq("Status", ExamMcqQuestionSet.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("Exams.Id", examId));
            criteria.Add(Restrictions.Eq("UniqueSet", uniqueSet));
            criteria.SetProjection(Projections.RowCount());
            int totalRow = Convert.ToInt32(criteria.UniqueResult());
            if (totalRow > 0)
                return true;
            return false;
        }
        private ICriteria CriteriaObject(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, string organization, string program,
            string session, string course, string uniqueSet)
        {
            ICriteria criteria = Session.CreateCriteria<ExamMcqQuestionSet>().Add(Restrictions.Not(Restrictions.Eq("Status", ExamMcqQuestionSet.EntityStatus.Delete)));
            criteria.CreateAlias("Exams", "xm");
            criteria.CreateAlias("xm.Program", "p");
            criteria.CreateAlias("xm.Session", "s");
            criteria.CreateAlias("xm.Course", "c");
            criteria.CreateAlias("p.Organization", "o");

            if (!String.IsNullOrEmpty(organization))
            {
                criteria.Add(Restrictions.Eq("o.Id", Convert.ToInt64(organization)));
            }
            if (!String.IsNullOrEmpty(program))
            {
                criteria.Add(Restrictions.Eq("p.Id", Convert.ToInt64(program)));
            }
            if (!String.IsNullOrEmpty(session))
            {
                criteria.Add(Restrictions.Eq("s.Id", Convert.ToInt64(session)));
            }
            if (!String.IsNullOrEmpty(course))
            {
                criteria.Add(Restrictions.Eq("c.Id", Convert.ToInt64(course)));
            }
            if (!String.IsNullOrEmpty(uniqueSet))
            {
                criteria.Add(Restrictions.Eq("UniqueSet", Convert.ToInt32(uniqueSet)));
            }
            if (authorizeOrgIds != null && authorizeOrgIds.Count() > 0)
            {
                criteria.Add(Restrictions.In("o.Id", authorizeOrgIds));
            }

            if (authorizeProgramIds != null && authorizeProgramIds.Count() > 0)
            {
                criteria.Add(Restrictions.In("p.Id", authorizeProgramIds));
            }

            if (authorizeSessionIds != null && authorizeSessionIds.Count() > 0)
            {
                criteria.Add(Restrictions.In("s.Id", authorizeSessionIds));
            }

            return criteria;
        }

        #endregion

        #region Helper Function
        
        #endregion

    }
}