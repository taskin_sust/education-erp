using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class ItemMap : BaseClassMap<Item, long>
    {
        public ItemMap()
        {
            Table("UINV_Item");
            LazyLoad();
            References(x => x.ItemGroup).Column("ItemGroupId");
            References(x => x.Organization).Column("OrganizationId");
            References(x => x.SpecificationTemplate).Column("SpecificationTemplateId");
            //References(x => x.Supplier).Column("SupplierId");

            Map(x => x.Rank).Column("Rank").Not.Nullable();
            Map(x => x.Name).Column("Name").Not.Nullable();
            Map(x => x.ItemType).Column("ItemType").Not.Nullable();
            Map(x => x.ItemUnit).Column("ItemUnit").Not.Nullable();
            Map(x => x.CostBearer).Column("CostBearer").Not.Nullable();
            HasMany(x => x.CurrentStockSummaries).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.GoodsIssueDetails).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.GoodsReceiveDetails).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.GoodsReturnDetails).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.GoodsTransferDetails).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ProgramSessionItems).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.QuotationList).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.RequisitionDetails).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            //HasMany(x => x.SupplierItems).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.WorkOrderDetails).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            //HasMany(x => x.SupplierItems).KeyColumn("ItemId").Cascade.AllDeleteOrphan().Inverse();
            //HasManyToMany(x => x.Suppliers).Table("UINV_SupplierItem").ParentKeyColumn("ItemId").ChildKeyColumn("SupplierId").Cascade.SaveUpdate().Inverse();
        }
    }
}
