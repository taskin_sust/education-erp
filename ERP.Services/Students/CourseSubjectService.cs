﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface ICourseSubjectService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        CourseSubject GetCourseSubjectByCourseSubjectId(long id);
        CourseSubject LoadCourseSubjectById(long id);
        CourseSubject LoadCourseSubject(long courseId, long subjectId);
        #endregion

        #region List Loading Function
        IList<CourseSubject> LoadCourseSubject(long courseId);
        IList<CourseSubject> LoadCourseSubject(long programId,long sessionId,List<long> courseIdList);

        #endregion

        #region Others Function

        #endregion

        CourseSubject LoadCourseSubjectByCourseIdAndSubjectId(long courseId, long subId);
    }
    public class CourseSubjectService : BaseService, ICourseSubjectService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization

        private readonly ICourseSubjectDao _courseSubjectDao;
        public CourseSubjectService(ISession session)
        {
            Session = session;
            _courseSubjectDao = new CourseSubjectDao() { Session = session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public CourseSubject LoadCourseSubjectById(long id)
        {
            CourseSubject courseSubject;
            try
            {
                courseSubject = _courseSubjectDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return courseSubject;
        }

        public CourseSubject LoadCourseSubject(long courseId, long subjectId)
        {
            CourseSubject courseSubject;
            try
            {
                courseSubject = _courseSubjectDao.LoadCourseSubjectByCourseIdAndSubjectId(courseId, subjectId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return courseSubject;
        }

        public CourseSubject GetCourseSubjectByCourseSubjectId(long id)
        {
            CourseSubject courseSubject;
            try
            {
                courseSubject = Session.QueryOver<CourseSubject>().Where(x => x.Id == id).SingleOrDefault<CourseSubject>();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return courseSubject;
        }
        #endregion

        #region List Loading Function
        public IList<CourseSubject> LoadCourseSubject(long courseId)
        {
            IList<CourseSubject> courseSubjects;
            try
            {
                courseSubjects = _courseSubjectDao.LoadCourseSubjectByCourseId(courseId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return courseSubjects;
        }

        public IList<CourseSubject> LoadCourseSubject(long programId,long sessionId,List<long> courseIdList)
        {
            return _courseSubjectDao.LoadCourseSubjectByCourseId(programId,sessionId,courseIdList);
        }

        public CourseSubject LoadCourseSubjectByCourseIdAndSubjectId(long courseId, long subId)
        {
            try
            {
                return _courseSubjectDao.LoadCourseSubjectByCourseIdAndSubjectId(courseId, subId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
