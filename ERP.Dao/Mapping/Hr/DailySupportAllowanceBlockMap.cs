using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class DailySupportAllowanceBlockMap : BaseClassMap<DailySupportAllowanceBlock,long> {

        public DailySupportAllowanceBlockMap()
        {
            Table("HR_DailySupportAllowanceBlock");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
            Map(x => x.BlockingDate).Column("BlockingDate");
			Map(x => x.IsBlocked).Column("IsBlocked");
            Map(x => x.Reason).Column("Reason");
            Map(x => x.IsPost).Column("IsPost");
            HasMany(x => x.SupportAllowanceBlockLog).KeyColumn("DailySupportAllowanceBlockId");
        }
    }
}
