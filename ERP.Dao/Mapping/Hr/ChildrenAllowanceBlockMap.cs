using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{


    public partial class ChildrenAllowanceBlockMap : BaseClassMap<ChildrenAllowanceBlock, long>
    {

        public ChildrenAllowanceBlockMap()
        {
            Table("HR_ChildrenAllowanceBlock");
            LazyLoad();
            References(x => x.ChildrenAllowanceSetting).Column("ChildrenAllowanceSettingId");
            References(x => x.TeamMember).Column("TeamMemberId");
            Map(x => x.EffectiveDate).Column("EffectiveDate").Not.Nullable();
            Map(x => x.ClosingDate).Column("ClosingDate");
        }
    }
}
