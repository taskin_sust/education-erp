﻿using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentAcademicInfoSubjectResultMap : ClassMap<StudentAcademicInfoSubjectResult>
    {
        public StudentAcademicInfoSubjectResultMap()
        {
            Table("StudentAcademicInfoSubjectResult");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.CreationDate).Column("CreationDate");
            Map(x => x.Grade).Column("Grade");
            Map(x => x.GradePoint).Column("GradePoint");

            References(x => x.StudentAcademicInfoBaseData).Column("StudentAcademicInfoBaseDataId");
            References(x => x.StudentAcademicInfoExamSubject).Column("StudentAcademicInfoExamSubjectId");

        }
    }
}
