﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IHrBoardDao : IBaseDao<BusinessModel.Entity.Hr.Board, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        Board LoadBoardById(long id);
        #endregion

        #region List Loading Function

        IList<Board> BoardList();

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class HrBoardDao : BaseDao<BusinessModel.Entity.Hr.Board, long>, IHrBoardDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public Board LoadBoardById(long id)
        {
            var entity =
                Session.QueryOver<Board>()
                    .Where(x => x.Id == id)
                    .Where(x => x.Status == Board.EntityStatus.Active)
                    .SingleOrDefault<Board>();
            return entity;
        }
        #endregion

        #region List Loading Function

        

        public IList<Board> BoardList()
        {
                var list = Session.QueryOver<Board>().List();
                if (list != null && list.Count > 0)
                    return list;
                return null;
        }
        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion
    }

}
