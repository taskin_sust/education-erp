﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SessionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Session")]
    public class SessionLoadTest : SessionBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _sessionService.LoadById(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            SessionFactory sessionFactory =
                _sessionFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            var obj = _sessionService.LoadById(sessionFactory.Object.Id);
            Assert.NotNull(obj);
            
        }

    }
}
