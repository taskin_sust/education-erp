﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AcademicInfoTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AcademicInfo")]
    public class AcademicInfoLoadTest:AcademicInfoBaseTest
    {
        [Fact]
        public void Should_Return_LoadMemberAccademicInfo_Successfully()
        {
            var tm = PersistTeamMember();
            var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            var board = _hrBoardService.LoadBoard().FirstOrDefault();
            _InstituteFactory.Create().WithInstituteCategory().Persist();
            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = examLabel.Id.ToString(),
                Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            _academicInfoService.UpdateAcademicInfo(tm, academicInfoArray);

            Assert.True(_academicInfoService.LoadMemberAccademicInfo(tm.Id).Any());
        }

        [Fact]
        public void Should_Return_LoadMemberAccademicInfo_Null()
        {
            var tm = PersistTeamMember();
            var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            var board = _hrBoardService.LoadBoard().FirstOrDefault();
            _InstituteFactory.Create().WithInstituteCategory().Persist();
            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = examLabel.Id.ToString(),
                Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            _academicInfoService.UpdateAcademicInfo(tm, academicInfoArray);

            Assert.Null(_academicInfoService.LoadMemberAccademicInfo(tm.Id+1));
        }
    }
}
