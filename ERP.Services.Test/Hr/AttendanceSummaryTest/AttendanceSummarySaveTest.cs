﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSummaryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSummary")]
    public class AttendanceSummarySaveTest:AttendanceSummaryBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Successfully_Save()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016, 12, 5),
                new DateTime(2016, 12, 5, 10, 10, 10), new DateTime(2016, 12, 5, 11, 10, 10), "Due to", (int)AttendanceAdjustmentStatus.Approved, 0).WithTeamMember(tm);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            Assert.NotNull(_attendanceSummaryServiceHelper.GetAttendenceByPinAndDateTime(tm.Pin, new DateTime(2016, 12, 5, 0, 0, 0)));

        }

        [Fact]
        public void Should_Return_AttendanceAdjustment_InvalidException_For_Empty_Adjustment()
        {
            Assert.Throws<InvalidDataException>(
                () =>
                    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { }));

        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_Null_Object()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016, 12, 5),
                new DateTime(2016, 12, 5, 10, 10, 10), new DateTime(2016, 12, 5, 11, 10, 10), "Due to", (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });
            var a = _attendanceSummaryServiceHelper.GetAttendenceByPinAndDateTime(tm.Pin,
                new DateTime(2016, 12, 5, 0, 0, 0));
            Assert.Equal(0, a.Id);
        }

        [Fact]
        public void Should_Return_AttendanceAdjustment_InvalidDataException_For_Reason()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016, 12, 5), new DateTime(2016, 12, 5, 10, 10, 10), new DateTime(2016, 12, 5, 11, 10, 10)
                , "", (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm);
            Assert.Throws<InvalidDataException>(() => _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object }));
        }


        #endregion

    }
}