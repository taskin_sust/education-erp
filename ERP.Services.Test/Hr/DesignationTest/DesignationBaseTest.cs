﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Cache;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DesignationTest
{
    public class DesignationBaseTest : TestBase, IDisposable
    {
        #region Properties & Object Initialization

        internal readonly string TestName = "TestDesiganition_" + DateTime.Now.Second.ToString(CultureInfo.CurrentCulture)
                                                + "_" + DateTime.Now.Millisecond.ToString(CultureInfo.CurrentCulture);
        internal readonly string UpdatingTestName = "U_TestDesiganition_" + DateTime.Now.Second.ToString(CultureInfo.CurrentCulture)
                                                + "_" + DateTime.Now.Millisecond.ToString(CultureInfo.CurrentCulture);
        internal readonly string ShortName = "TDS_" + DateTime.Now.Second.ToString(CultureInfo.CurrentCulture)
                                                + "_" + DateTime.Now.Millisecond.ToString(CultureInfo.CurrentCulture);
        internal readonly string UpdatingShortName = "UTDS_" + DateTime.Now.Second.ToString(CultureInfo.CurrentCulture)
                                                + "_" + DateTime.Now.Millisecond.ToString(CultureInfo.CurrentCulture);

        internal readonly int TestRank = 404;
        internal readonly int UpdatingTestRank = 405;

        //TODO: use naming convention for private variable

        protected readonly CommonHelper _commonHelper;
        protected readonly DesignationService _designationService;
        protected readonly IOrganizationService _organizationService;
        protected readonly TestBaseService<Designation> _tbsHrd;
        protected List<long> IdList = new List<long>();
        internal DesignationFactory _designationFactory;

        private ISession _session;

        #endregion

        public DesignationBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _designationService = new DesignationService(_session);
            _organizationService = new OrganizationService(_session);
            _tbsHrd = new TestBaseService<Designation>(_session);
            _designationFactory = new DesignationFactory(null, _session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }


        public new void Dispose() 
        {
            _designationFactory.Cleanup();
            organizationFactory.Cleanup();
            base.Dispose();
        }

    }
}
