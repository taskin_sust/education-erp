﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.UserAuth
{
    public interface IAspNetUserDao : IBaseDao<AspNetUser, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<AspNetUser> LoadAuthorizedUserBybranch(int start, int length, string orderBy, string orderDir, string userName, string contactNo, long? branchId, string status,
            long? organizationId, string rank, List<long> authorizedProgramLists, List<long> authorizedBranchLists);

        #endregion

        #region Others Function
        bool HasDuplicateByContactNo(string contactNo);
        bool HasDuplicateByContactNo(string contactNo, long id);
        int RowCountAuthorizedUserBybranch(string orderBy, string orderDir, string userName, string contactNo, long? branchId, string status,long ?organizationId, string rank
            , List<long> authorizedProgramLists, List<long> authorizedBranchLists);
        #endregion

        //no need
        //IList<AspNetUser> LoadActive(int start, int length, string orderBy, string orderDir, string userName,
        //string contactNo, string status, string rank);
    }
    public class AspNetUserDao : BaseDao<AspNetUser, long>, IAspNetUserDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<AspNetUser> LoadAuthorizedUserBybranch(int start, int length, string orderBy, string orderDir, string userName, string contactNo, long? branchId, string status,long?organizationId, string rank
            , List<long> authorizedProgramLists, List<long> authorizedBranchLists)
        {
            ICriteria criteria = GetAuthorizedUserBybranchCriteria(orderBy, orderDir, userName, contactNo, branchId, status,organizationId, rank, authorizedProgramLists,  authorizedBranchLists);
            return length > 0 ? criteria.SetFirstResult(start).SetMaxResults(length).List<AspNetUser>() : criteria.List<AspNetUser>();
        }
        public ICriteria GetAuthorizedUserBybranchCriteria(string orderBy, string orderDir, string userName, string contactNo, long? branchId, string status,long? organizationId, string rank,  List<long> authorizedProgramLists, List<long> authorizedBranchLists)
        {
            ICriteria criteria = Session.CreateCriteria<UserProfile>();
            criteria.CreateAlias("Branch", "branch", JoinType.LeftOuterJoin);
            criteria.CreateAlias("Campus", "campus", JoinType.LeftOuterJoin);
            criteria.CreateAlias("AspNetUser", "aspU");
            if (authorizedBranchLists!=null)
            {
                criteria.Add(Restrictions.In("branch.Id", authorizedBranchLists));
            }
            if (branchId != null)
            {
                if (branchId !=-1)
                    criteria.Add(Restrictions.Eq("branch.Id", branchId));
            }
            if (organizationId != null)
            {
                criteria.Add(Restrictions.Eq("branch.Organization.Id", organizationId));
            }
            criteria.SetProjection(Projections.Property("AspNetUser"));
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            criteria.Add(Restrictions.Not(Restrictions.Eq("aspU.Status", AspNetUser.EntityStatus.Delete)));
            criteria.Add(Restrictions.Not(Restrictions.Eq("aspU.UserName", "superadmin")));
            if (!String.IsNullOrEmpty(userName))
            {
                criteria.Add(Restrictions.Or(
                        Restrictions.Like("NickName", userName, MatchMode.Anywhere),
                        Restrictions.Or(
                            Restrictions.Like("aspU.UserName", userName, MatchMode.Anywhere),
                            Restrictions.Like("aspU.FullName", userName, MatchMode.Anywhere)
                        )
                    ));
            }
            if (!String.IsNullOrEmpty(contactNo))
            {
                criteria.Add(Restrictions.Like("aspU.PhoneNumber", contactNo, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("aspU.Status", Convert.ToInt32(status)));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("aspU.Rank", Convert.ToInt32(rank)));
            }
            if (orderDir == "ASC")
            {
               criteria.AddOrder(Order.Asc(orderBy));
            }
            else
            {
               criteria.AddOrder(Order.Desc(orderBy));
            }
            return criteria;
        }
      
        #endregion

        #region Others Function
        public int RowCountAuthorizedUserBybranch(string orderBy, string orderDir, string userName, string contactNo, long? branchId, string status, long? organizationId, string rank, List<long> authorizedProgramLists, List<long> authorizedBranchLists)
        {
            ICriteria criteria = GetAuthorizedUserBybranchCriteria(orderBy, orderDir, userName, contactNo, branchId, status,organizationId, rank, authorizedProgramLists,  authorizedBranchLists);
            return criteria.List<AspNetUser>().Count;
        }
        public bool HasDuplicateByContactNo(string contactNo)
        {
            ICriteria criteria = Session.CreateCriteria<AspNetUser>();
            criteria.Add(Restrictions.Eq("PhoneNumber", contactNo));
            criteria.Add(Restrictions.Eq("Status", AspNetUser.EntityStatus.Active));
            IList<AspNetUser> rowList = criteria.List<AspNetUser>();
            if (rowList == null || rowList.Count < 1)
                return true;
            return false;
        }
        public bool HasDuplicateByContactNo(string contactNo, long id)
        {
            ICriteria criteria = Session.CreateCriteria<AspNetUser>();
            criteria.Add(Restrictions.Eq("PhoneNumber", contactNo));
            criteria.Add(Restrictions.Eq("Status", AspNetUser.EntityStatus.Active));
            criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            IList<AspNetUser> rowList = criteria.List<AspNetUser>();
            if (rowList == null || rowList.Count < 1)
                return true;
            return false;
        }
        #endregion  
    
        //no need
        //public IList<AspNetUser> LoadActive(int start, int length, string orderBy, string orderDir, string userName, string contactNo, string status, string rank)
        //{
        //    ICriteria criteria =
        //            Session.CreateCriteria<AspNetUser>()
        //                .Add(Restrictions.Not(Restrictions.Eq("Status", AspNetUser.EntityStatus.Delete)))
        //                .Add(Restrictions.Not(Restrictions.Eq("UserName", "superadmin")));

        //    if (!String.IsNullOrEmpty(userName))
        //    {
        //        criteria.Add(Restrictions.Like("UserName", userName, MatchMode.Anywhere));
        //    }

        //    if (!String.IsNullOrEmpty(contactNo))
        //    {
        //        criteria.Add(Restrictions.Like("PhoneNumber", contactNo, MatchMode.Anywhere));
        //    }

        //    if (!String.IsNullOrEmpty(status))
        //    {
        //        criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));

        //    }
        //    if (!String.IsNullOrEmpty(rank))
        //    {
        //        criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
        //    }

        //    if (orderDir == "ASC")
        //    {
        //        criteria.AddOrder(Order.Asc(orderBy));
        //    }
        //    else
        //    {
        //        criteria.AddOrder(Order.Desc(orderBy));
        //    }

        //    return (List<AspNetUser>)criteria.SetFirstResult(start).SetMaxResults(length).List<AspNetUser>();
        //}

    }
}
