﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using Xunit;

namespace UdvashERP.Services.Test.Administration.CourseTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Course")]
    public class GetCourseTest : CourseBaseTest
    {
        #region Basic Test
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _courseService.GetCourse(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            Organization organization = organizationFactory.Create().Persist().Object;
            Program program = _programFactory.Create().WithOrganization(organization).Persist().Object;
            Session sessionObj = _sessionFactory.Create().Persist().Object;
            List<Subject> subjectList = _subjectFactory.CreateMore(5).Persist().GetLastCreatedObjectList();

            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectList.Select(x => Convert.ToInt32(x.Id)).ToArray()
            };

            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, program.Id, sessionObj.Id);


            _courseFactory.Create().WithName(_name).WithStartDateAndEndDate(DateTime.Now.AddDays(5), DateTime.Now.AddDays(35))
                .WithOrganization(organization).WithProgram(program).WithSession(sessionObj)
                .WithMaxMinSubject(4, 2, 2);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            var object1 = _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList()).Persist().Object;
            var getObj = _courseService.GetCourse(object1.Id);
            Assert.NotNull(getObj);

            CleanUpProgramSessionSubject(program.Id,sessionObj.Id);
        }

        #endregion
    }
}
