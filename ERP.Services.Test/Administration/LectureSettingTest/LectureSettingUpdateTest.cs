﻿using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.LectureSettingTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "LectureSetting")]
    public class LectureSettingUpdateTest : LectureSettingBaseTest
    {

        #region Basic Test

        [Fact]
        public void Should_Update_Successfully()
        {
            var lectureSettingFactory =
                _lectureSettingFactory.Create().WithCourseSubject()
                    .Persist();
            
            LectureSettings lectureSettings = _lectureSettingService.LoadById(lectureSettingFactory.Object.Id);
            lectureSettings.ClassNamePrefix = Guid.NewGuid() + "modified class name prefix";
            lectureSettings.NumberOfClasses = 40;
            var newPrefix = Guid.NewGuid() + "modified class name prefix";
            _lectureService.Update(lectureSettings, 40, newPrefix);
            Assert.Equal(newPrefix, lectureSettings.ClassNamePrefix);
            Assert.Equal(40, lectureSettings.NumberOfClasses);
            IList<Lecture> lectures = _lectureService.LoadLecture(lectureSettings.Id);
            _TestBaseService.DeleteProgramSessionSubject(lectureSettings.Course.Program.Id, lectureSettings.Course.RefSession.Id);
            _TestBaseService.DeleteLecture(lectures);
        }

        #endregion

    }
}
