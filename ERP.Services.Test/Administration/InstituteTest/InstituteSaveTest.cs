﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.InstituteTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Institute")]
    public class InstituteSaveTest : InstituteBaseTest
    {

        #region Basic Test
        [Fact]
        public void Should_Save_Successfully()
        {
            InstituteFactory instituteFactory =
                _InstituteFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithInstituteCategory().Persist();
            var institute = _InstituteService.LoadById(instituteFactory.Object.Id);
            Assert.True(institute.Id>0);
            Assert.NotNull(institute);
        }

        [Fact]
        public void Should_Not_Save_Duplicate_Institute()
        {
            InstituteFactory instituteFactory =
                _InstituteFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithInstituteCategory().Persist();
            var institute2 = new Institute();
            foreach (PropertyInfo property in typeof(Institute).GetProperties())
            {
                property.SetValue(institute2, property.GetValue(instituteFactory.Object, null), null);
            }
            institute2.Id = 0;
            Assert.Throws<DuplicateEntryException>(() => _InstituteService.Save(institute2));
        }
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Not_Update_Empty_Name()
        {
            InstituteFactory instituteFactory =
                _InstituteFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithInstituteCategory().Persist();
            instituteFactory.Object.Name = "";
            Assert.Throws<EmptyFieldException>(() => _InstituteService.Update(instituteFactory.Object, instituteFactory.Object.InstituteCategory.Id));
        }

        #endregion
    }
}
