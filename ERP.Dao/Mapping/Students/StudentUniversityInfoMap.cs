﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentUniversityInfoMap : BaseClassMap<StudentUniversityInfo, long>
    {
        public StudentUniversityInfoMap()
        {
            Table("StudentUniversityInfo");
            LazyLoad();

            Map(x => x.Year);
            Map(x => x.Position);
         

            References(x => x.Student).Column("StudentId");
            References(x => x.Institute).Column("InstituteId");
            References(x => x.StudentUniversitySubject).Column("SubjectId");
        }
    }
}
