﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using UdvashERP.FingerPrint.ApiReference;
using UdvashERP.FingerPrint.BusinessModel;
using UdvashERP.FingerPrint.BusinessRule;
using zkemkeeper;

namespace UdvashERP.FingerPrint.Helper
{
    public class ZktecoColorDriver : IDeviceDriver
    {
        private CZKEMClass axCZKEM = new CZKEMClass();
        public ZktecoColorDriver()
        {

        }
        public bool Connect(Device device)
        {
            bool returnValue = false;
            try
            {
                int idwErrorCode = 0;
                axCZKEM.SetCommPassword(Int32.Parse(device.CommunicationKey));
                returnValue = axCZKEM.Connect_Net(device.IpAddress, Convert.ToInt32(device.Port));
                if (returnValue)
                {
                    axCZKEM.RegEvent(device.MachineNo, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                }
                else
                {
                    axCZKEM.GetLastError(ref idwErrorCode);
                }
            }
            catch (Exception ex)
            {

            }

            return returnValue;
        }

        public bool SetUserFingerTemplate(string enrollNumber, int fingerIndex, int flag)
        {
            bool returnValue = false;
            try
            {
                axCZKEM.CancelOperation();
                axCZKEM.SSR_DelUserTmpExt(10, enrollNumber, fingerIndex);
                if (axCZKEM.StartEnrollEx(enrollNumber, fingerIndex, flag))
                {
                    MessageBox.Show("Scan Finger Print For , UserID= " + enrollNumber + " FingerID= " + fingerIndex.ToString());
                    axCZKEM.StartIdentify();
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {

            }

            return returnValue;
        }

        public bool GetUserFingerTemplate(int machineNo, string enrollNumber, int fingerIndex, out int flag, out byte[] tmpData, out int tmpLength)
        {
            int iTmpLength = 0;
            byte[] byTmpData = new byte[2000];
            bool returnValue = axCZKEM.GetUserTmpEx(machineNo, enrollNumber, fingerIndex, out flag, out byTmpData[0], out iTmpLength);
            tmpData = byTmpData;
            tmpLength = iTmpLength;
            return returnValue;
        }


        public bool DeleteUserTemplate(int machineNo, string enrollNumber, int fingerIndex)
        {
            return axCZKEM.SSR_DelUserTmpExt(machineNo, enrollNumber, fingerIndex);
        }

        public void Disconnect()
        {
            axCZKEM.Disconnect();
        }

        internal void GetUserInfo()
        {
            string sdwEnrollNumber;
            string sName;
            string sPassword;
            int iPrivilege;
            bool bEnabled;
            int iTmpLength = 0;
            int iFlag = 0;
            byte[] byTmpData = new byte[2000];//modify by darcy on Dec.9 2009

            while (axCZKEM.SSR_GetAllUserInfo(1, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))
            {
                memberInfo = new JsonResponse();
                ApiRequestResponse _apiRequestResponse = new ApiRequestResponse();
                memberInfo = _apiRequestResponse.GetMemberInfo(sdwEnrollNumber.ToString());

                int idwFingerIndex;

                foreach (var item in memberInfo.Data.AttendanceDeviceApiModel)
                {
                    if (item.IsFinger)
                    {
                        for (int fIndex = 0; fIndex <= 9; fIndex++)
                        {
                            iTmpLength = 0;
                            byTmpData = new byte[2000];
                            idwFingerIndex = fIndex;
                            if (axCZKEM.GetUserTmpEx(1, sdwEnrollNumber, idwFingerIndex, out iFlag, out byTmpData[0], out iTmpLength))
                            {
                                string hexString = "";
                                if (iTmpLength > 0)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    hexString = "";
                                    foreach (byte b in byTmpData)
                                    {
                                        sb.Append(b.ToString("X2"));
                                    }
                                    hexString = sb.ToString();
                                }
                                //if (fIndex == 0)
                                //{
                                //    item.Index0 = "xxx";
                                //}
                                if (fIndex == 3)
                                {
                                    item.Index3 = hexString;
                                }
                                if (fIndex == 4)
                                {
                                    item.Index4 = hexString;
                                }
                                if (fIndex == 5)
                                {
                                    item.Index5 = hexString;
                                }
                                if (fIndex == 6)
                                {
                                    item.Index6 = hexString;
                                }
                            }
                        }
                    }
                }

                var deviceList = memberInfo.Data.AttendanceDeviceApiModel.Where(x => x.IsFinger).ToList();

                memberInfo.Data.AttendanceDeviceApiModel.Clear();
                memberInfo.Data.AttendanceDeviceApiModel = deviceList;

                var json = new JavaScriptSerializer().Serialize(memberInfo.Data);
                var param = new StringBuilder();
                param.Append("memberInfo=" + json);

                Response response = _apiRequestResponse.SetMemberFingerPrint(param.ToString());

            }
        }

        public JsonResponse memberInfo { get; set; }
    }
}
