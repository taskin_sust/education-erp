﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Teachers;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherPaymentService : IBaseService
    {
        #region Operational Function
        void Payment(List<TeacherPaymentListDto> teacherPaymentListDtoList, Branch fromPaidBranch, out  string successMessageFinal, out string voucherNo);
        #endregion

        #region Single Instances Loading Function

        TeacherPayment GetTeacherPaymentByVoucherNo(string voucherNo);

        #endregion

        #region List Loading Function

        List<TeacherPaymentListDto> LoadTeacherPaymentList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string teacherName, string heldDate, string classType, string organization, string program, string session, string branch, string campus, string paymentStatus, string tpin, int searchOn = 1); 
        List<TeacherPaymentReport> LoadPaymentReportList(List<UserMenu> userMenu, int start, string orderBy, string orderDir, TeacherPaymentFormViewModel teacherPayment);

        #endregion

        #region Others Function
        
        long TeacherPaymentListRowCount(List<UserMenu> userMenu, string teacherName, string heldDate, string classType, string organization, string program, string session, string branch, string campus, string paymentStatus, string tpin, int searchOn = 1);
        int GetPaymentReportListCount(List<UserMenu> userMenu, TeacherPaymentFormViewModel teacherPayment);
        
        #endregion
    }

    public class TeacherPaymentService : BaseService, ITeacherPaymentService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeacherService");
        #endregion

        #region Properties & Object & Initialization
        private readonly ITeacherClassTypeDao _teacherClassTypeDao;
        private readonly ITeacherClassEntryDao _teacherClassEntryDao;
        private readonly ITeacherClassEntryDetailsDao _teacherClassEntryDetailsDao;
        private readonly ITeacherScriptEvaluationDetailsDao _teacherScriptEvaluationDetailsDao;
        private readonly ITeacherAcademicMaterialDetailsDao _teacherAcademicMaterialDetailsDao;
        private readonly ITeacherPaymentDao _teacherPaymentDao;
        private readonly IBranchDao _branchDao;
        private readonly ITeacherDao _teacherDao;
        private readonly ICommonHelper _commonHelper;
        public TeacherPaymentService(ISession session)
        {
            Session = session;
            _teacherClassTypeDao = new TeacherClassTypeDao() { Session = session };
            _teacherClassEntryDao = new TeacherClassEntryDao() { Session = session };
            _teacherClassEntryDetailsDao = new TeacherClassEntryDetailsDao() { Session = session };
            _teacherPaymentDao = new TeacherPaymentDao() { Session = session };
            _teacherScriptEvaluationDetailsDao = new TeacherScriptEvaluationDetailsDao() { Session = session };
            _teacherAcademicMaterialDetailsDao = new TeacherAcademicMaterialDetailsDao() { Session = session };
            _branchDao = new BranchDao();
            _teacherDao = new TeacherDao { Session = session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function

        public void Payment(List<TeacherPaymentListDto> teacherPaymentListDtoList, Branch fromPaidBranch, out string successMessageFinal, out string returnVoucherNo)
        {
            decimal totalAmount = 0;
            string voucherNo = "";
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    DateTime paymentDate = DateTime.Now;
                    long currentUserId = Convert.ToInt64(Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId()));
                    
                    #region Teacher Payment Table Data
                    int totalClassQuantity = 0;
                    voucherNo = GetTeacherpaymentVoucherNo(fromPaidBranch, paymentDate);

                    TeacherPayment teacherPaymentObj = new TeacherPayment();
                    teacherPaymentObj.Teacher = _teacherDao.LoadById(teacherPaymentListDtoList[0].TeacherId);
                    teacherPaymentObj.PaidFromBranch = fromPaidBranch;
                    teacherPaymentObj.PaidBy = currentUserId;
                    teacherPaymentObj.PaymentDate = paymentDate;
                    teacherPaymentObj.VoucherNo = voucherNo;

                    #endregion Teacher Payment Table Data End

                    foreach (var tpl in teacherPaymentListDtoList)
                    {
                        #region Teacher Payment Details Table Data

                        Program program = null;
                        Session session = null;
                        int tempQuantity = 0;
                        DateTime? submissionHeldDate = null;
                        string description = "";
                        TeacherPaymentDetails teacherPaymentDetailsObj = new TeacherPaymentDetails();


                        #endregion Teacher Payment Details Table Data End
                        if (tpl.ActivityType == "Class")
                        {
                            var data = _teacherClassEntryDetailsDao.LoadById(tpl.Id);
                            data.PaymentStatus = 1;
                            data.Branch = fromPaidBranch;
                            data.PaymentDate = paymentDate;
                            _teacherClassEntryDetailsDao.Update(data);
                            data.PaidBy = currentUserId;
                            totalAmount += Convert.ToDecimal(tpl.Amount);
                            // successMessage += tpl.Teacher + " is paid Tk." + tpl.Amount +", ";

                            #region Class Entry Details for payment
                            
                            teacherPaymentDetailsObj.TeacherClassEntryDetails = data;
                            program = data.TeacherClassEntry.Program;
                            session = data.TeacherClassEntry.Session;
                            tempQuantity = Convert.ToInt32(data.Quantity);
                            submissionHeldDate = Convert.ToDateTime(data.TeacherClassEntry.HeldDate);
                            description = data.Description;
                            totalClassQuantity += tempQuantity;

                            #endregion Class Entry Details for payment End

                        }
                        else if (tpl.ActivityType == "Script Evaluation")
                        {
                            var data = _teacherScriptEvaluationDetailsDao.LoadById(tpl.Id);
                            data.PaymentStatus = 1;
                            data.PaidFromBranch = fromPaidBranch;
                            data.PaymentDate = paymentDate;
                            data.PaidBy = currentUserId;
                            _teacherScriptEvaluationDetailsDao.Update(data);
                            totalAmount += Convert.ToDecimal(tpl.Amount);
                            //successMessage += tpl.Teacher + " is paid Tk." + tpl.Amount + ", ";

                            #region Script Evaluation Details for payment

                            teacherPaymentDetailsObj.TeacherScriptEvaluationDetails = data;
                            program = data.TeacherScriptEvaluation.Program;
                            session = data.TeacherScriptEvaluation.Session;
                            tempQuantity = Convert.ToInt32(data.Quantity);
                            submissionHeldDate = Convert.ToDateTime(data.TeacherScriptEvaluation.SubmissionDate);
                            description = data.Description;

                            #endregion Script Evaluation Details for payment End

                        }
                        else if (tpl.ActivityType == "Materials Development")
                        {
                            var data = _teacherAcademicMaterialDetailsDao.LoadById(tpl.Id);
                            data.PaymentStatus = 1;
                            data.PaidFromBranch = fromPaidBranch;
                            data.PaymentDate = paymentDate;
                            data.PaidBy = currentUserId;
                            _teacherAcademicMaterialDetailsDao.Update(data);
                            totalAmount += Convert.ToDecimal(tpl.Amount);
                            // successMessage += tpl.Teacher + " is paid Tk." + tpl.Amount +  ", ";

                            #region Materials Development Details for payment

                            teacherPaymentDetailsObj.TeacherAcademicMaterialsDetails = data; 
                            program = data.TeacherAcademicMaterials.Program;
                            session = data.TeacherAcademicMaterials.Session;
                            tempQuantity = 0;
                            submissionHeldDate = Convert.ToDateTime(data.TeacherAcademicMaterials.SubmissionDate);
                            description = data.Description;

                            #endregion Materials Development Details for payment End

                        }
                        
                        #region Teacher Payment Details Table Data

                        teacherPaymentDetailsObj.TeacherPayment = teacherPaymentObj;
                        teacherPaymentDetailsObj.Program = program;
                        teacherPaymentDetailsObj.Session = session;
                        teacherPaymentDetailsObj.Amount = Convert.ToDecimal(tpl.Amount);
                        teacherPaymentDetailsObj.Quantity = tempQuantity;
                        teacherPaymentDetailsObj.SubmissionHeldDate = Convert.ToDateTime(submissionHeldDate);
                        teacherPaymentDetailsObj.Description = description;
                        teacherPaymentDetailsObj.Status = TeacherPaymentDetails.EntityStatus.Active;
                        teacherPaymentDetailsObj.CreateBy = currentUserId;
                        teacherPaymentDetailsObj.ModifyBy = currentUserId;

                        #endregion Teacher Payment Details Table Data End

                        teacherPaymentObj.TeacherPaymentDetails.Add(teacherPaymentDetailsObj);

                    }

                    teacherPaymentObj.TotalPaidAmount = totalAmount;
                    teacherPaymentObj.TotalClassQuantity = totalClassQuantity;
                    _teacherPaymentDao.Save(teacherPaymentObj);

                    trans.Commit();
                }
                successMessageFinal = teacherPaymentListDtoList.Select(x => x.Teacher).FirstOrDefault() + " is paid Tk." + totalAmount;
                returnVoucherNo = voucherNo;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }

        }

        private string GetTeacherpaymentVoucherNo(Branch fromPaidBranch, DateTime paymentDate)
        {
            string firstVoucherCode = "00001";
            string lastVoucherCode = fromPaidBranch.Code + paymentDate.ToString("yy");
            TeacherPayment teacherPaymentObj = _teacherPaymentDao.GetTeacherPaymentByBranchCodeAndYear2Digit(lastVoucherCode);
            if (teacherPaymentObj == null)
            {
                return lastVoucherCode + firstVoucherCode;
            }

            string voucherNo = teacherPaymentObj.VoucherNo;
            var preMrNoLastPart = voucherNo.Substring(4);
            return lastVoucherCode + (Convert.ToInt64(preMrNoLastPart) + 1).ToString().PadLeft(5, '0');

        }

        #endregion

        #region Single Instances Loading Function

        public TeacherPayment GetTeacherPaymentByVoucherNo(string voucherNo)
        {
            try
            {
                return _teacherPaymentDao.GetTeacherPaymentByVoucherNo(voucherNo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public List<TeacherPaymentListDto> LoadTeacherPaymentList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string teacherName, string heldDate, string classType, string organization, string program, string session, string branch, string campus, string paymentStatus, string tpin, int searchOn = 1)
        {
            try
            {
                // var authorizedBranchList = 
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu);
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu);
                //_branchDao.LoadAuthorizedBranch().Select(x => x.Id).ToArray(); 
                //return _teacherPaymentDao.LoadTeacherPaymentList(start, length, orderBy, orderDir, authBranchIdList, teacherName, heldDate, classType, organization, paymentType);
                return _teacherPaymentDao.LoadTeacherPaymentList(start, length, orderBy, orderDir, authProgramIdList, authBranchIdList, teacherName, heldDate, classType, organization, program, session, branch, campus, paymentStatus, tpin, searchOn);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public List<TeacherPaymentReport> LoadPaymentReportList(List<UserMenu> userMenu, int start, string orderBy, string orderDir, TeacherPaymentFormViewModel teacherPayment)
        {
            try
            {
                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (teacherPayment.OrganizationId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(teacherPayment.OrganizationId) : null);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationIdList, null, (teacherPayment.BranchId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(teacherPayment.BranchId) : null);

                return _teacherPaymentDao.LoadPaymentReportList(start, teacherPayment.NoOfData, orderBy, orderDir, authBranchIdList, teacherPayment.ActivityId, teacherPayment.DateFrom, teacherPayment.DateTo, teacherPayment.TpinList, teacherPayment.VoucherNo);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public long TeacherPaymentListRowCount(List<UserMenu> userMenu, string teacherName, string heldDate, string classType, string organization, string program, string session, string branch, string campus, string paymentStatus, string tpin, int searchOn = 1)
        {
            try
            {
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu);
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu);
                return _teacherPaymentDao.TeacherPaymentListRowCount(authProgramIdList, authBranchIdList, teacherName, heldDate, classType, organization, program, session, branch, campus, paymentStatus, tpin, searchOn);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetPaymentReportListCount(List<UserMenu> userMenu, TeacherPaymentFormViewModel teacherPayment)
        {
            try
            {
                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (teacherPayment.OrganizationId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(teacherPayment.OrganizationId) : null);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationIdList, null, (teacherPayment.BranchId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(teacherPayment.BranchId) : null);

                return _teacherPaymentDao.GetPaymentReportListCount(authBranchIdList, teacherPayment.ActivityId, teacherPayment.DateFrom, teacherPayment.DateTo, teacherPayment.TpinList, teacherPayment.VoucherNo);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ;
            }
        }

        #endregion


    }
}
