$(document).ready(function () {
    $("#MenuGroupId").change(function () {
        var MenuGroupId = $("#MenuGroupId").val();
        if (MenuGroupId != "") {
            var data = { "MenuGroupId": MenuGroupId };
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Menu/getRootMenu",
                contenttype: "application/json; charset=utf-8",
                data: data,
                success: function (result) {
                    $("#ParentId option").remove();
                    if (!($.isEmptyObject(result))) {
                        $("#ParentId").append('<option value="">---Select---</option>');
                        $.each(result, function (index, menu) {
                            $("#ParentId").append('<option value="' + menu.value + '">' + menu.text + '</option>');
                        });
                    }
                    else { }                  
                },
                error: function (result) {
                    alert("Unable to update status.");
                }
            });  
        }
    });    
});


    
