using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Testing.Values;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Students;

namespace UdvashERP.Dao.UInventory
{
    public interface ICurrentStockSummaryDao : IBaseDao<CurrentStockSummary, long>
    {
        #region Operational Function


        #endregion

        #region Single Instances Loading Function
        //CurrentStockSummary GetByProgramSessionBranchAndItem(long branchId, long itemId);
        //CurrentStockSummary GetStockQuantity(List<long> authProgramIdList, List<long> sessionIdList, List<long> authBranchFrom, long itemId);
        // CurrentStockSummary GetByProgramSessionBranchAndItem(long branchId, long itemId, Program program, Session session);
        CurrentStockSummary GetCurrentStockSummary(long itemId, long branchId, long? programId, long? sessionId);

        #endregion

        #region List Loading Function

        IList<dynamic> LoadCurrentStock(int start, int length, List<long> authBranchIdList, List<long> itemGroupIdList, List<long> itemIdList, List<long> authProgramList, List<long> sessionIdList, string branchNameStr, List<string> programsession, bool isOnlyNa);

        #endregion

        #region Others Function

        int GetCurrentStockQuantity(long itemId, long branchId, long? programId, long? sessionId);
        int CountCurrentStock(List<long> authBranchIdList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, string branchNameStr, List<string> proSessionList, bool isOnlyNa);

        //int GetItemCurrentStock(long itemId, long branchId, long? programId, long? sessionId);

        #endregion




    }

    public class CurrentStockSummaryDao : BaseDao<CurrentStockSummary, long>, ICurrentStockSummaryDao
    {
        #region Properties & Object & Initialization

        public static IResultTransformer ExpandoObject;
        private readonly ISessionDao _sessionDao;

        public CurrentStockSummaryDao()
        {
            ExpandoObject = new ExpandoObjectResultSetTransformers();
        }
        #endregion

        #region Operational Function


        #endregion

        #region Single Instances Loading Function

        //public CurrentStockSummary GetByProgramSessionBranchAndItem(long branchId, long itemId)
        //{
        //    return
        //        Session.QueryOver<CurrentStockSummary>().Where(x => x.Branch.Id == branchId && x.Item.Id == itemId).List<CurrentStockSummary>().OrderByDescending(x => x.Id).Take(1)
        //            .SingleOrDefault();
        //}

        //public int GetItemCurrentStock(long itemId, long branchId, long? programId, long? sessionId)
        //{
        //    int count = 0;
        //    ICriteria criteria = GetStockSummaryCountCriteria(itemId, branchId, programId, sessionId);
        //    var list = criteria.List<CurrentStockSummary>();
        //    if (list != null && list.Count > 0 && list[0] != null)
        //    {
        //        var stockQuantity = list[0].StockQuantity;
        //        if (stockQuantity != null) count = (int)stockQuantity;
        //    }
        //    return count;
        //}

        #endregion

        #region List Loading Function

        public IList<dynamic> LoadCurrentStock(int start, int length, List<long> authBranchIdList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> authProgramList, List<long> sessionIdList, string branchNameStr, List<string> programsession, bool isOnlyNa)
        {
            string[] brAry = branchNameStr.Split(',');

            string query = @" SELECT piv.* , ";
            int index = 0;
            foreach (var brName in brAry)
            {
                if (index == 0)
                    query += @" ((case when piv." + brName + " is null then 0 else piv." + brName + " end) + ";
                else
                    query += @" (case when piv." + brName + " is null then 0 else piv." + brName + " end) +";
                index++;
            }
            query = query.Trim();
            query = query.Remove(query.Length - 2);
            query += ") ";
            query += @" as Total  
                              FROM ( ";
            query += GenerateCurrentStockQuery(authBranchIdList, itemGroupIdList,
               itemIdList, authProgramList, sessionIdList, branchNameStr, programsession, isOnlyNa);
            query += @") as piv order by piv.[Item Group] OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            IQuery queryI = Session.CreateSQLQuery(query);
            IList<dynamic> result = DynamicList(queryI);
            return result;
        }

        private string GenerateCurrentStockQuery(List<long> authBranchIdList, List<long> itemGroupIdList, List<long> itemIdList, List<long> authProgramList, List<long> sessionIdList,

            string branchNameStr, List<string> programsessionList, bool isOnlyNa)
        {
            string where = "";
            if (programsessionList != null && programsessionList.Any() && !programsessionList.Contains(SelectionType.SelelectAll.ToString()))
            {
                where += " WHERE";
                if (programsessionList.Contains("-101") && isOnlyNa)
                {
                    programsessionList = programsessionList.Skip(1).Take(programsessionList.Count - 1).ToList();
                    where += " (PR.Id is null and SE.Id is null ) or ";
                }
                else if (programsessionList.Contains("-101"))
                {
                    programsessionList = programsessionList.Skip(1).Take(programsessionList.Count - 1).ToList();
                    where += " (PR.Id is null and SE.Id is null ) or ";
                }
                if (!isOnlyNa)
                {
                    foreach (var programsession in programsessionList)
                    {
                        var programId = programsession.Split(new string[] { "::" }, StringSplitOptions.None)[0];
                        var sessionId = programsession.Split(new string[] { "::" }, StringSplitOptions.None)[1];
                        where += "(PR.Id=" + programId + " and SE.Id=" + sessionId + ") or ";
                    }
                }
                where = where.Trim();
                where = where.Remove(where.Length - 2);
            }

            var query =
                @"SELECT IG.Name as [Item Group] ,IT.Name as [Item Name], ISNULL(PR.ShortName + ' ' + SE.Name, 'N/A') AS [Program & Session] , BR.Name as [Branch Name], css.StockQuantity as Quantuty
                        from [dbo].[UINV_CurrentStockSummary] css 
                        LEFT JOIN Program PR on css.ProgramId = PR.Id
                        LEFT JOIN [Session] AS SE ON  css.SessionId=SE.Id  
                        LEft JOIN [dbo].[UINV_Item] AS IT ON  css.ItemId =IT.Id ";

            if (itemIdList.Count > 0 && !itemIdList.Contains(SelectionType.SelelectAll))
                query += @" AND css.ItemId IN (" + string.Join(", ", itemIdList) + ") ";

            query += @"  INNER JOIN [UINV_ItemGroup] AS IG ON IG.Id = IT.ItemGroupId ";
            if (itemGroupIdList.Count > 0 && !itemGroupIdList.Contains(SelectionType.SelelectAll))
                query += @" AND IG.ID IN (" + string.Join(", ", itemGroupIdList) + ") ";

            query += @"  LEFT JOIN [Branch] AS BR ON BR.ID = css.BranchId ";
            if (authBranchIdList.Count > 0 && !authBranchIdList.Contains(SelectionType.SelelectAll))
                query += @" AND BR.Id IN (" + string.Join(", ", authBranchIdList) + ") ";

            query += where;
            query += @") AS FINAL
                        	                             PIVOT
                        	                             (
                        	                             SUM(Quantuty)
                        	                             FOR [Branch Name] IN( " + branchNameStr;
            query += @" )  ";
            return query;
        }

        #endregion

        #region Others Function

        private ICriteria GetCurrentStockCriteria(long itemId, long branchId, long? programId, long? sessionId)
        {
            ICriteria criteria = Session.CreateCriteria<CurrentStockSummary>().Add(Restrictions.Eq("Status", CurrentStockSummary.EntityStatus.Active));

            criteria.CreateAlias("Branch", "b").Add(Restrictions.Eq("b.Status", Branch.EntityStatus.Active));
            criteria.CreateAlias("Item", "i").Add(Restrictions.Eq("i.Status", Item.EntityStatus.Active));

            if (programId != null && sessionId != null)
            {
                criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
                criteria.CreateAlias("Session", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("p.Id", programId));
                criteria.Add(Restrictions.Eq("s.Id", sessionId));
            }
            else
            {
                criteria.Add(Restrictions.IsNull("Program"));
                criteria.Add(Restrictions.IsNull("Session"));
            }

            criteria.Add(Restrictions.Eq("b.Id", branchId));
            criteria.Add(Restrictions.Eq("i.Id", itemId));
            return criteria;
        }

        public CurrentStockSummary GetCurrentStockSummary(long itemId, long branchId, long? programId, long? sessionId)
        {
            ICriteria returnCriteria = GetCurrentStockCriteria(itemId, branchId, programId, sessionId);
            return returnCriteria.UniqueResult<CurrentStockSummary>();

        }


        //original qty
        public int GetCurrentStockQuantity(long itemId, long branchId, long? programId, long? sessionId)
        {
            ICriteria returnCriteria = GetCurrentStockCriteria(itemId, branchId, programId, sessionId);
            returnCriteria.SetProjection(Projections.Property("StockQuantity"));
            return Convert.ToInt32(returnCriteria.UniqueResult());

        }

        public int CountCurrentStock(List<long> authBranchIdList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList,
            List<long> sessionIdList, string branchNameStr, List<string> programsession, bool isOnlyNa)
        {
            string query = "SELECT COUNT(piv.[Item Group]) from ( ";
            query += GenerateCurrentStockQuery(authBranchIdList, itemGroupIdList, itemIdList, programIdList, sessionIdList, branchNameStr, programsession, isOnlyNa);
            query += @") as piv ";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        //public CurrentStockSummary GetByProgramSessionBranchAndItem(long branchId, long itemId, Program program, Session session)
        //{
        //    var criteria = Session.CreateCriteria<CurrentStockSummary>();
        //    criteria.CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
        //    criteria.CreateAlias("Item", "it").Add(Restrictions.Eq("it.Status", Item.EntityStatus.Active));
        //    if (program != null)
        //    {
        //        criteria.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
        //        criteria.Add(Restrictions.Eq("pr.Id", program.Id));
        //    }
        //    if (session != null)
        //    {
        //        criteria.CreateAlias("Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
        //        criteria.Add(Restrictions.Eq("se.Id", session.Id));
        //    }
        //    criteria.Add(Restrictions.Eq("br.Id", branchId));
        //    criteria.Add(Restrictions.Eq("it.Id", itemId));

        //    return criteria.UniqueResult<CurrentStockSummary>();
        //}

        #endregion

        #region Helper Function

        private ICriteria GetStockSummaryCountCriteria(long itemId, long branchId, long? programId, long? sessionId)
        {
            ICriteria criteria = Session.CreateCriteria<CurrentStockSummary>().Add(Restrictions.Not(Restrictions.Eq("Status", CurrentStockSummary.EntityStatus.Delete)));

            criteria.CreateAlias("Item", "item").Add(Restrictions.Not(Restrictions.Eq("item.Status", Branch.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("item.Id", itemId));

            criteria.CreateAlias("Branch", "branch").Add(Restrictions.Not(Restrictions.Eq("branch.Status", Branch.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("branch.Id", branchId));

            if (programId != null && programId > 0)
            {
                criteria.CreateAlias("Program", "program").Add(Restrictions.Not(Restrictions.Eq("program.Status", Branch.EntityStatus.Delete)));
                criteria.Add(Restrictions.Eq("program.Id", programId));
            }
            if (sessionId != null && sessionId > 0)
            {
                criteria.CreateAlias("Session", "session").Add(Restrictions.Not(Restrictions.Eq("session.Status", Branch.EntityStatus.Delete)));
                criteria.Add(Restrictions.Eq("session.Id", sessionId));
            }
            return criteria;
        }

        public IList<dynamic> DynamicList(IQuery query)
        {
            var result = query.SetResultTransformer(ExpandoObject).List<dynamic>();
            return result;
        }

        #endregion
    }
}
