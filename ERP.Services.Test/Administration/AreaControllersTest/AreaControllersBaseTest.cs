﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.AdminisTration.AreaControllersTest
{
    public class AreaControllersBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal AreaControllersService _areaControllersService;
        internal AreaControllersFactory _areaControllersFactory;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal readonly string _areaName = Guid.NewGuid() + "_(OPQ RST UVW XYZ ABC CDE FGH IJK LMN)";
        #endregion

        public AreaControllersBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _areaControllersFactory = new AreaControllersFactory(null, _session);
            _areaControllersService = new AreaControllersService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _areaControllersFactory.Cleanup();
            base.Dispose();
        }
    }
}
