﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.com.onnorokomsms.api2;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Sms;
using UdvashERP.Services.Students;
using UdvashERP.BusinessModel.Entity.Exam;
using System.Configuration;
using NHibernate.Exceptions;
using SmsViewModel = UdvashERP.BusinessModel.ViewModel.SmsViewModel;

namespace UdvashERP.App_code
{
    public static class WebHelper
    {
        public static string CommonErrorMessage
        {
            get { return "Something went wrong."; }
        }

        internal static string MakeFirstCharUpper(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                char firstChar = name[0];
                firstChar = Char.ToUpper(firstChar);
                name = name.Remove(0, 1);
                return firstChar + name;
            }
            return name;
        }

        public static string SetExceptionMessage(Exception ex, string moreInfo = "")
        {
            string errorCode = "";
            var exceptionType = ex.GetType();
            if (exceptionType == typeof(Exception))
                errorCode = "1000";
            else if (exceptionType == typeof(SystemException))
                errorCode = "1001";
            else if (exceptionType == typeof(AccessViolationException))
                errorCode = "1002";
            else if (exceptionType == typeof(ArgumentException))
                errorCode = "1003";
            else if (exceptionType == typeof(ArgumentNullException))
                errorCode = "1004";
            else if (exceptionType == typeof(ArgumentOutOfRangeException))
                errorCode = "1005";
            else if (exceptionType == typeof(ArithmeticException))
                errorCode = "1006";
            else if (exceptionType == typeof(ArrayTypeMismatchException))
                errorCode = "1007";
            else if (exceptionType == typeof(DivideByZeroException))
                errorCode = "1008";
            else if (exceptionType == typeof(IndexOutOfRangeException))
                errorCode = "1009";
            else if (exceptionType == typeof(InvalidCastException))
                errorCode = "1010";
            else if (exceptionType == typeof(InvalidOperationException))
                errorCode = "1011";
            else if (exceptionType == typeof(MissingMemberException))
                errorCode = "1012";
            else if (exceptionType == typeof(NotFiniteNumberException))
                errorCode = "1013";
            else if (exceptionType == typeof(NotSupportedException))
                errorCode = "1014";
            else if (exceptionType == typeof(NullReferenceException))
                errorCode = "1015";
            else if (exceptionType == typeof(OutOfMemoryException))
                errorCode = "1016";
            else if (exceptionType == typeof(StackOverflowException))
                errorCode = "1017";
            else if (exceptionType == typeof(GenericADOException))
                errorCode = "1018";
            else
                errorCode = "9999";

            if (!String.IsNullOrEmpty(errorCode))
            {
                errorCode = "Error code: " + errorCode + ".";
            }

            var exceptionMessage = errorCode + moreInfo;
            if (errorCode == "9999")
            {
                exceptionMessage = CommonErrorMessage + " " + exceptionMessage;
            }
            //ViewBag.
            return exceptionMessage;
        }
    }

    public class Response
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string AdditionalValue { get; set; }
        public Response(bool isSuccess, string message)
        {
            IsSuccess = isSuccess;
            Message = message;
        }
        public Response(bool isSuccess, string message, string additionalValue)
        {
            IsSuccess = isSuccess;
            Message = message;
            AdditionalValue = additionalValue;
        }
    }
    public static class TeacherPaymentStatus
    {
        public static int Paid { get { return 1; } }
        public static int Due { get { return 2; } }

        public static Dictionary<int, string> GetTeacherPaymentStatus()
        {
            try
            {
                var status = new Dictionary<int, string>
                             {
                                 {1,"Paid"},
                                 {2,"Due"}
                             };
                return status;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
    //public static class PaymentMethods
    //{
    //    public static Dictionary<string, int> GetPaymentMethods()
    //    {
    //        try
    //        {
    //            var status = new Dictionary<string, int>
    //           {
    //               {"Cash", PaymentMethod.Cash},
    //               {"BKash", PaymentMethod.BKash},
    //               {"Cheque", PaymentMethod.Cheque}
    //           };
    //            return status;
    //        }
    //        catch (Exception e)
    //        {
    //            throw e;
    //        }
    //    }
    //}

    public static class RazorViewToString
    {
        public static string RenderRazorViewToString(this Controller controller, string viewName, object model)
        {

            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                try
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                    return sw.GetStringBuilder().ToString();
                }
                catch (Exception e)
                {

                    throw e;
                }

            }
        }

    }

    public static class RazorViewToOptions
    {
        public static string RenderRazorViewToOptions(this Controller controller, string viewName, object model, object selectedValue, string selectMessage)
        {
            controller.ViewBag.OptionsData = model;
            controller.ViewBag.SelectedValue = selectedValue;
            controller.ViewBag.SelectMessage = selectMessage;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }

    public static class SendSmsApi
    {

        /// <summary>
        /// Habib
        /// </summary>
        private static readonly ISmsService _smsService;
        private static readonly IStudentService _studentService;
        private static readonly IStudentProgramService _studentProgramService;
        private static readonly IBatchService _batchService;
        private static readonly IExamsStudentMarksService _examsStudentMarksService;
        private static readonly IStudentPaymentService _studentPaymentService;
        static SendSmsApi()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _smsService = new SmsService(nHsession);
            _studentService = new StudentService(nHsession);
            _studentProgramService = new StudentProgramService(nHsession);
            _batchService = new BatchService(nHsession);
            _examsStudentMarksService = new ExamsStudentMarksService(nHsession);
            _studentPaymentService = new StudentPaymentService(nHsession);

        }
        public static string DeliverOneToManySms(SmsSettings smsSetting, List<SmsViewModel> reciverNumbersList, string sms, string mask, long programId, long studentId, long? batchId = null)
        {
            string receiverNumbers = "";
            foreach (SmsViewModel rN in reciverNumbersList)
            {
                receiverNumbers += rN.ReceiverNumber + ",";
            }
            receiverNumbers = receiverNumbers.Remove(receiverNumbers.Length - 1);
            var sendSms = new SendSms();
            var returnResult = sendSms.OneToMany(smsSetting.Organization.SmsApiUserName, smsSetting.Organization.SmsApiPassword, sms, receiverNumbers, "TEXT", mask, "");
            return returnResult;
        }
        public static bool GenerateInstantResultSmsAndNumber(long smsTypeId, StudentProgram studentProgram, Exams examinformation, int examType)
        {

            if (smsTypeId > 0 && studentProgram != null && examinformation != null)
            {
                var smsSettings = _smsService.LoadSmsSettingsBySmsTypePrgram(smsTypeId, studentProgram.Program.Organization.Id, studentProgram.Program.Id);
                if (smsSettings != null && smsSettings.SmsReceivers.Any() &&
                    !string.IsNullOrEmpty(smsSettings.Template))
                {
                    bool isValidMessageType = false;
                    if (examType == ExamType.Mcq)
                    {
                        if (smsSettings.Template.Contains("[[{{MCQ Marks}}]]"))
                        {
                            isValidMessageType = true;
                        }
                    }
                    else if (examType == ExamType.Written)
                    {
                        if (smsSettings.Template.Contains("[[{{Written Marks}}]]"))
                        {
                            isValidMessageType = true;
                        }
                    }

                    decimal mcqFullMarks = 0;
                    decimal writtenFullMarks = 0;
                    if (isValidMessageType)
                    {
                        var student = studentProgram.Student;
                        var options = smsSettings.SmsType.DynamicOptions;
                        var message = smsSettings.Template;
                        var mask = smsSettings.MaskName;
                        var smsToBeSend = new List<SmsViewModel>();
                        var mcqExamDetais = examinformation.ExamDetails.Where(x => x.ExamType == ExamType.Mcq && x.Status == ExamsDetails.EntityStatus.Active).ToList();
                        var writtenExamDetais = examinformation.ExamDetails.Where(x => x.ExamType == ExamType.Written && x.Status == ExamsDetails.EntityStatus.Active).ToList();

                        //var mcqFullMarks = mcqExamDetais.Sum(x => x.MarksPerQuestion * x.TotalQuestion);
                        //var writtenFullMarks = writtenExamDetais.Sum(x => x.WrittenFullMark);
                        //var fullMarks = mcqExamDetais.Sum(x => x.MarksPerQuestion * x.TotalQuestion) + writtenExamDetais.Sum(x => x.WrittenFullMark);
                        if (examinformation.IsMcq)
                        {
                            if (examinformation.McqFullMarks != null)
                            {
                                mcqFullMarks = examinformation.McqFullMarks;
                            }
                        }
                        if (examinformation.IsWritten)
                        {
                            if (examinformation.WrittenFullMark != null)
                            {
                                writtenFullMarks = examinformation.WrittenFullMark;
                            }
                        }
                        var fullMarks = mcqFullMarks + writtenFullMarks;

                        decimal mcqMarks = 0;
                        decimal writtenMarks = 0;
                        int flag = 0;

                        if (mcqExamDetais.Any())
                        {
                            var strudentMcqMarks = _examsStudentMarksService.LoadExamsStudentMarksByExamStudentProgram(examinformation.Id, studentProgram.Id, ExamType.Mcq);

                            //examinformation.ExamDetails[0].ExamsStudentMarks

                            if (strudentMcqMarks.Any())
                            {
                                mcqMarks = strudentMcqMarks.Sum(x => x.ObtainMark);
                                flag = 1;
                            }
                        }
                        if (writtenExamDetais.Any())
                        {
                            var strudentWrittenMarks =
                           _examsStudentMarksService.LoadExamsStudentMarksByExamStudentProgram(examinformation.Id,
                               studentProgram.Id, ExamType.Written);
                            if (strudentWrittenMarks.Any())
                            {
                                writtenMarks = strudentWrittenMarks.Sum(x => x.ObtainMark);
                                flag = flag == 1 ? 3 : 2;
                            }
                        }

                        if (options.Any())
                        {
                            foreach (var o in options)
                            {
                                string optionField = "[[{{" + o.Name + "}}]]";
                                //[{{Written Marks}}]]
                                string optionValue = "";

                                switch (o.Name)
                                {
                                    case "Nick Name":
                                        optionValue = student.NickName;
                                        break;
                                    case "Program Roll":
                                        optionValue = studentProgram.PrnNo;
                                        break;
                                    case "Registration Number":
                                        optionValue = student.RegistrationNo;
                                        break;
                                    case "Exam Name":
                                        optionValue = examinformation.Name;
                                        break;
                                    case "MCQ Marks":
                                        if (message.Contains("[[{{MCQ Marks}}]]") && message.Contains("[[{{MCQ Full Marks}}]]"))
                                        {
                                            optionField = message.Substring(message.IndexOf("[[{{MCQ Marks}}]]", System.StringComparison.Ordinal),
                                                 ((message.LastIndexOf("[[{{MCQ Full Marks}}]]", System.StringComparison.Ordinal) + 22) -
                                                  message.IndexOf("[[{{MCQ Marks}}]]", System.StringComparison.Ordinal)));
                                        }
                                        if (flag == 1 || flag == 3)
                                        {
                                            if (message.Contains("[[{{MCQ Marks}}]]") && message.Contains("[[{{MCQ Full Marks}}]]"))
                                            {
                                                optionValue =
                                                    "MCQ: " + mcqMarks.ToString() + optionField.Substring(17, optionField.Length - (22 + 17)) + mcqFullMarks.ToString();
                                            }
                                            else
                                            {
                                                optionValue = "MCQ: " + mcqMarks.ToString();
                                            }
                                        }
                                        else { optionValue = ""; }
                                        break;
                                    case "MCQ Full Marks":
                                        if (flag == 1 || flag == 3)
                                            optionValue = mcqFullMarks.ToString();
                                        else optionValue = "";
                                        break;
                                    case "Written Marks":
                                        if (message.Contains("[[{{Written Marks}}]]") &&
                                            message.Contains("[[{{Written Full Marks}}]]"))
                                        {
                                            optionField =
                                                message.Substring(
                                                    message.IndexOf("[[{{Written Marks}}]]",
                                                        System.StringComparison.Ordinal),
                                                    ((message.LastIndexOf("[[{{Written Full Marks}}]]",
                                                        System.StringComparison.Ordinal) + 26) -
                                                     message.IndexOf("[[{{Written Marks}}]]",
                                                         System.StringComparison.Ordinal)));
                                        }
                                        if (flag == 2 || flag == 3)
                                        {
                                            if (message.Contains("[[{{Written Marks}}]]") && message.Contains("[[{{Written Full Marks}}]]"))
                                            {
                                                optionValue =
                                                    "Written-" + writtenMarks.ToString() + optionField.Substring(21, optionField.Length - (26 + 21)) + writtenFullMarks.ToString();
                                            }
                                            else
                                            {
                                                optionValue = "Written-" + writtenMarks.ToString();
                                            }
                                        }
                                        else optionValue = "";
                                        break;
                                    case "Written Full Marks":
                                        if (flag == 2 || flag == 3)
                                            optionValue = writtenFullMarks.ToString();
                                        else optionValue = "";
                                        break;
                                    case "Total Marks":
                                        if (message.Contains("[[{{Total Marks}}]]") &&
                                            message.Contains("[[{{Full Marks}}]]"))
                                        {
                                            optionField =
                                                message.Substring(
                                                    message.IndexOf("[[{{Total Marks}}]]",
                                                        System.StringComparison.Ordinal),
                                                    ((message.LastIndexOf("[[{{Full Marks}}]]",
                                                        System.StringComparison.Ordinal) + 18) -
                                                     message.IndexOf("[[{{Total Marks}}]]",
                                                         System.StringComparison.Ordinal)));
                                        }
                                        if (flag == 3)
                                        {
                                            if (message.Contains("[[{{Total Marks}}]]") && message.Contains("[[{{Full Marks}}]]"))
                                            {
                                                optionValue = "Total-" + (writtenMarks + mcqMarks) + optionField.Substring(19, optionField.Length - (18 + 19)) + fullMarks.ToString();
                                            }
                                            else
                                            {
                                                optionValue = "Total-" + (writtenMarks + mcqMarks);
                                            }

                                        }

                                        else optionValue = "";
                                        break;
                                    case "Full Marks":
                                        optionValue = fullMarks.ToString();
                                        break;
                                    default:
                                        optionValue = "";
                                        break;
                                }
                                message = message.Replace(optionField, optionValue);
                            }
                        }
                        message = message.Trim();

                        foreach (var sr in smsSettings.SmsReceivers)
                        {
                            var svm = new SmsViewModel();
                            switch (sr.Name)
                            {
                                case "Mobile Number (Personal)":
                                    string studentMobile = CheckMobileNumber(student.Mobile);
                                    if (studentMobile != null)
                                    {
                                        svm.SmsReceiverId = sr.Id;
                                        svm.ReceiverNumber = "88" + studentMobile;
                                    }
                                    break;
                                case "Mobile Number (Father)":
                                    string guardiansMobile1 = CheckMobileNumber(student.GuardiansMobile1);
                                    if (guardiansMobile1 != null)
                                    {
                                        svm.SmsReceiverId = sr.Id;
                                        svm.ReceiverNumber = "88" + guardiansMobile1;
                                    }
                                    break;
                                case "Mobile Number (Mother)":
                                    string guardiansMobile2 = CheckMobileNumber(student.GuardiansMobile2);
                                    if (guardiansMobile2 != null)
                                    {
                                        svm.SmsReceiverId = sr.Id;
                                        svm.ReceiverNumber = "88" + guardiansMobile2;
                                    }
                                    break;
                                case "Mobile Number (Campus Incharge)":
                                    string contactNumber = CheckMobileNumber(studentProgram.Batch.Campus.ContactNumber);
                                    if (CheckMobileNumber(studentProgram.Batch.Campus.ContactNumber) != null)
                                    {
                                        svm.SmsReceiverId = sr.Id;
                                        svm.ReceiverNumber = "88" + contactNumber;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                            {
                                var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                                if (!alreadyExist.Any())
                                {
                                    smsToBeSend.Add(svm);
                                }
                            }
                        }


                        if (smsToBeSend.Any())
                        {
                            if (String.IsNullOrEmpty(mask))
                            {
                                mask = "";
                            }
                            string returnResult = DeliverOneToManySms(smsSettings, smsToBeSend, message, mask, studentProgram.Program.Id, student.Id, null);
                            var returnArray = returnResult.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var rMessagePerMobile in returnArray)
                            {
                                // string campaign = "Student List " + DateTime.Now.ToString();
                                try
                                {
                                    var smsHistory = new SmsHistory();
                                    var exploded = rMessagePerMobile.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                                    var receiverNumberfrmSplit = "88" + exploded[1];
                                    var currentNumber = smsToBeSend.FirstOrDefault(r => r.ReceiverNumber == receiverNumberfrmSplit);
                                    smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                                    smsHistory.SmsReceiver = _smsService.LoadReceiverById(currentNumber.SmsReceiverId);
                                    smsHistory.Sms = message;
                                    smsHistory.Program = studentProgram.Program;
                                    smsHistory.Organization = studentProgram.Program.Organization;
                                    smsHistory.SmsSettings = smsSettings;
                                    smsHistory.Student = student;
                                    smsHistory.Mask = mask;
                                    smsHistory.ErrorCode = exploded[0];
                                    smsHistory.Type = Convert.ToInt32(smsTypeId);//for result
                                    smsHistory.Priority = 5;
                                    if (exploded.Length == 3 && smsHistory.ErrorCode == "1900")
                                    {
                                        if (!string.IsNullOrEmpty(exploded[2]))
                                        {
                                            smsHistory.ResponseCode = exploded[2];
                                        }
                                    }
                                    else
                                    {
                                        smsHistory.Status = SmsHistory.EntityStatus.Pending;
                                    }
                                    _smsService.SaveSmsHistory(smsHistory);

                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public static bool GenarateInstantSmsAndNumber(long smsTypeId, long organizationId, long programId, long sessionId, long studentId, long? studentProgramId = null, long? batchId = null)
        {
            if (smsTypeId != 0 && programId != 0 && sessionId != 0 && studentId != 0)
            {
                var smsSettingdetails = _smsService.LoadSmsSettingsBySmsTypePrgram(smsTypeId, organizationId, programId);

                var student = _studentService.LoadById(studentId);

                if (smsSettingdetails != null && student != null && smsSettingdetails.SmsReceivers.Any() && !string.IsNullOrEmpty(smsSettingdetails.Template))
                {
                    var studentProgram = new StudentProgram();
                    studentProgram = studentProgramId != null ? _studentProgramService.GetStudentProgram(Convert.ToInt64(studentProgramId), true) : student.StudentPrograms.LastOrDefault();
                    //StudentPayment studentPayment = studentProgram.StudentPayments.OrderBy(x => x.Id).LastOrDefault();
                    StudentPayment studentPayment = _studentPaymentService.LoadLastOrDefaultPayment(studentProgram.Id);


                    var options = smsSettingdetails.SmsType.DynamicOptions;
                    var message = smsSettingdetails.Template;
                    var mask = smsSettingdetails.MaskName;
                    var smsToBeSend = new List<SmsViewModel>();
                    Batch prevBatch = null;
                    if (batchId != null)
                    {
                        prevBatch = _batchService.GetBatch(Convert.ToInt64(batchId));
                    }
                    if (options.Any())
                    {
                        foreach (var o in options)
                        {
                            string optionField = "[[{{" + o.Name + "}}]]";
                            string optionValue = "";

                            switch (o.Name)
                            {
                                case "Nick Name":
                                    optionValue = student.NickName;
                                    break;
                                case "Program Roll":
                                    optionValue = studentProgram.PrnNo;
                                    break;
                                case "Payable Amount":
                                    if (studentPayment.PayableAmount != null && studentPayment.PayableAmount > 0)
                                    {
                                        optionValue = Convert.ToDecimal(studentPayment.PayableAmount).ToString("n0").Replace(",", "");
                                    }
                                    else
                                    {
                                        optionValue = "0";
                                    }
                                    break;
                                case "Paid Amount":
                                    if (studentPayment.ReceivedAmount != null && studentPayment.ReceivedAmount > 0)
                                    {
                                        optionValue = Convert.ToDecimal(studentPayment.ReceivedAmount).ToString("n0").Replace(",", ""); ;
                                    }
                                    else
                                    {
                                        optionValue = "0";
                                    }
                                    break;
                                case "Due Amount":
                                    if (studentPayment.DueAmount != null && studentPayment.DueAmount > 0)
                                    {
                                        optionValue = Convert.ToDecimal(studentPayment.DueAmount).ToString("n0").Replace(",", ""); ;
                                    }
                                    else
                                    {
                                        optionValue = "0";
                                    }
                                    break;
                                case "Offered Discount":
                                    if (studentPayment.DiscountAmount != null && studentPayment.DiscountAmount > 0)
                                    {
                                        optionValue = Convert.ToDecimal(studentPayment.DiscountAmount).ToString("n0").Replace(",", ""); ;
                                    }
                                    else
                                    {
                                        optionValue = "0";
                                    }
                                    break;
                                case "Special Discount":
                                    if (studentPayment.SpDiscountAmount != null && studentPayment.SpDiscountAmount > 0)
                                    {
                                        optionValue = Convert.ToDecimal(studentPayment.SpDiscountAmount).ToString("n0").Replace(",", ""); ;
                                    }
                                    else
                                    {
                                        optionValue = "0";
                                    }
                                    break;
                                case "Session":
                                    optionValue = studentProgram.Batch.Session.Name;
                                    break;
                                case "Program":
                                    optionValue = studentProgram.Batch.Program.Name;
                                    break;
                                case "Branch":
                                    optionValue = studentProgram.Batch.Branch.Name;
                                    break;
                                case "Campus":
                                    optionValue = studentProgram.Batch.Campus.Name;
                                    break;
                                case "Next Payment Date":
                                    //if (studentPayment.NextReceivedDate.ToString().Length > 0 && Convert.ToInt64(studentPayment.NextReceivedDate.ToString("yyyyMMdd")) > 20000101 && Convert.ToInt64(Model.NextPaymentDate.ToString("yyyyMMdd")) < 30000101)
                                    if (studentPayment.DueAmount != null && studentPayment.DueAmount > 0)
                                    {
                                        optionValue = studentPayment.NextReceivedDate.Value.ToString("dd MMM yyyy");
                                    }
                                    else
                                    {
                                        optionValue = "N/A";
                                    }

                                    break;
                                case "Batch Days":
                                    optionValue = studentProgram.Batch.Days;
                                    break;
                                case "Batch Time":
                                    optionValue = studentProgram.Batch.FormatedBatchTime;
                                    break;
                                case "Batch Name":
                                    optionValue = studentProgram.Batch.Name;
                                    break;
                                case "Added Course(s)":
                                    if (studentPayment.Remarks != null)
                                    {
                                        if (studentPayment.Remarks.Contains("Added"))
                                        {
                                            string remarks = studentPayment.Remarks.Replace("Added.", "");
                                            string addedCources = "";
                                            string[] courcesWithSubject = remarks.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                                            for (int i = 0; i < courcesWithSubject.Length; i++)
                                            {
                                                string[] cources = courcesWithSubject[i].Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                                                if (!string.IsNullOrEmpty(cources[0]))
                                                {
                                                    addedCources = addedCources + cources[0] + ", ";
                                                }

                                            }
                                            if (addedCources != "")
                                            {
                                                addedCources = addedCources.Remove(addedCources.Length - 2);
                                            }
                                            optionValue = addedCources;
                                        }
                                        else
                                        {
                                            optionValue = "";
                                        }
                                    }
                                    else
                                    {
                                        optionValue = "";
                                    }
                                    break;
                                case "Added Subject(s)":
                                    if (studentPayment.Remarks != null)
                                    {
                                        if (studentPayment.Remarks.Contains("Added"))
                                        {
                                            string remarks = studentPayment.Remarks.Replace("Added.", "");
                                            optionValue = remarks;
                                        }
                                        else
                                        {
                                            optionValue = "";
                                        }
                                    }
                                    else
                                    {
                                        optionValue = "";
                                    }
                                    break;
                                case "Canceled Course(s)":
                                    if (studentPayment.Remarks != null)
                                    {
                                        if (studentPayment.Remarks.Contains("Cancelled"))
                                        {
                                            string remarks = studentPayment.Remarks.Replace("Cancelled.", "");
                                            string cCources = "";
                                            string[] courcesWithSubject = remarks.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                                            for (int i = 0; i < courcesWithSubject.Length; i++)
                                            {
                                                string[] cources = courcesWithSubject[i].Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                                                if (!string.IsNullOrEmpty(cources[0]))
                                                {
                                                    cCources = cCources + cources[0] + ", ";
                                                }

                                            }
                                            if (cCources != "")
                                            {
                                                cCources = cCources.Remove(cCources.Length - 2);
                                            }
                                            optionValue = cCources;
                                        }
                                        else
                                        {
                                            optionValue = "";
                                        }
                                    }
                                    else
                                    {
                                        optionValue = "";
                                    }
                                    break;
                                case "Canceled Subject(s)":
                                    if (studentPayment.Remarks != null)
                                    {
                                        if (studentPayment.Remarks.Contains("Cancelled"))
                                        {
                                            string remarks = studentPayment.Remarks.Replace("Cancelled.", "");
                                            optionValue = remarks;
                                        }
                                        else
                                        {
                                            optionValue = "";
                                        }
                                    }
                                    else
                                    {
                                        optionValue = "";
                                    }
                                    break;
                                case "Previous Due":
                                    if (studentPayment.PayableAmount != null && studentPayment.PayableAmount > 0)
                                    {
                                        optionValue = Convert.ToDecimal(studentPayment.PayableAmount).ToString("n0").Replace(",", ""); ;
                                    }
                                    else
                                    {
                                        optionValue = "0";
                                    }
                                    break;
                                case "Consideration Amount":
                                    if (studentPayment.ConsiderationAmount != null && studentPayment.ConsiderationAmount > 0)
                                    {
                                        optionValue = Convert.ToDecimal(studentPayment.ConsiderationAmount).ToString("n0").Replace(",", ""); ;
                                    }
                                    else
                                    {
                                        optionValue = "0";
                                    }
                                    break;
                                case "Cash Back":
                                    if (studentPayment.CashBackAmount != null && studentPayment.CashBackAmount > 0)
                                    {
                                        optionValue = Convert.ToDecimal(studentPayment.CashBackAmount).ToString("n0").Replace(",", ""); ;
                                    }
                                    else
                                    {
                                        optionValue = "0";
                                    }
                                    break;
                                case "Current Due Amount":
                                    if (studentPayment.DueAmount != null && studentPayment.DueAmount > 0)
                                    {
                                        optionValue = Convert.ToDecimal(studentPayment.DueAmount).ToString("n0").Replace(",", ""); ;
                                    }
                                    else
                                    {
                                        optionValue = "0";
                                    }
                                    break;
                                case "Campus (Transfer From)":
                                    optionValue = prevBatch != null ? prevBatch.Campus.Name : "";
                                    break;
                                case "Campus (Transfer To)":
                                    optionValue = studentProgram.Batch.Campus.Name;
                                    break;
                                case "Branch (Transfer From)":
                                    optionValue = prevBatch != null ? prevBatch.Branch.Name : "";
                                    break;
                                case "Branch (Transfer To)":
                                    optionValue = studentProgram.Batch.Branch.Name;
                                    break;
                                case "Batch Days (Transfer From)":
                                    optionValue = prevBatch != null ? prevBatch.Days : "";
                                    break;
                                case "Batch Days (Transfer To)":
                                    optionValue = studentProgram.Batch.Days;
                                    break;
                                case "Batch Time (Transfer From)":
                                    optionValue = prevBatch != null ? prevBatch.FormatedBatchTime : "";
                                    break;
                                case "Batch Time (Transfer To)":
                                    optionValue = studentProgram.Batch.FormatedBatchTime;
                                    break;
                                case "Batch Name (Transfer From)":
                                    optionValue = prevBatch != null ? prevBatch.Name : "";
                                    break;
                                case "Batch Name (Transfer To)":
                                    optionValue = studentProgram.Batch.Name;
                                    break;
                                default:
                                    optionValue = "";
                                    break;
                            }
                            message = message.Replace(optionField, optionValue);
                        }
                    }


                    foreach (var sr in smsSettingdetails.SmsReceivers)
                    {
                        var svm = new SmsViewModel();
                        switch (sr.Name)
                        {
                            case "Mobile Number (Personal)":
                                string studentMobile = CheckMobileNumber(student.Mobile);
                                if (studentMobile != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = "88" + studentMobile;
                                }
                                break;
                            case "Mobile Number (Father)":
                                string guardiansMobile1 = CheckMobileNumber(student.GuardiansMobile1);
                                if (guardiansMobile1 != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = "88" + guardiansMobile1;
                                }
                                break;
                            case "Mobile Number (Mother)":
                                string guardiansMobile2 = CheckMobileNumber(student.GuardiansMobile2);
                                if (guardiansMobile2 != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = "88" + guardiansMobile2;
                                }
                                break;
                            case "Mobile Number (Campus Incharge)":
                                string contactNumber = CheckMobileNumber(studentProgram.Batch.Campus.ContactNumber);
                                if (CheckMobileNumber(studentProgram.Batch.Campus.ContactNumber) != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = "88" + contactNumber;
                                }
                                break;
                            case "Mobile Number (Campus To)":
                                string contactNumber1 = CheckMobileNumber(studentProgram.Batch.Campus.ContactNumber);
                                if (contactNumber1 != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = "88" + contactNumber1;
                                }
                                break;
                            case "Mobile Number (Campus From)":
                                if (prevBatch != null)
                                {
                                    string contactNumber2 = CheckMobileNumber(prevBatch.Campus.ContactNumber);
                                    if (contactNumber2 != null)
                                    {
                                        svm.SmsReceiverId = sr.Id;
                                        svm.ReceiverNumber = "88" + contactNumber2;
                                    }
                                }
                                break;
                            default:

                                break;
                        }

                        if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                        {
                            var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                            if (!alreadyExist.Any())
                            {
                                smsToBeSend.Add(svm);
                            }
                        }
                    }

                    if (smsToBeSend.Any())
                    {
                        if (String.IsNullOrEmpty(mask))
                        {
                            mask = "";
                        }
                        string returnResult = DeliverOneToManySms(smsSettingdetails, smsToBeSend, message, mask, programId, studentId, batchId);
                        var returnArray = returnResult.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                        int mobIteration = 0;
                        foreach (var rMessagePerMobile in returnArray)
                        {
                            try
                            {
                                var smsHistory = new SmsHistory();
                                var exploded = rMessagePerMobile.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);

                                var receiverNumberfrmSplit = "88" + exploded[1];


                                smsHistory.Sms = message;
                                smsHistory.Program = studentProgram.Program;
                                smsHistory.Organization = studentProgram.Program.Organization;
                                smsHistory.SmsSettings = smsSettingdetails;
                                smsHistory.Student = student;
                                smsHistory.Mask = mask;
                                smsHistory.Type = Convert.ToInt32(smsTypeId);//for result
                                smsHistory.Priority = 2;
                                if (prevBatch != null)
                                {
                                    smsHistory.Batch = prevBatch;
                                }

                                smsHistory.ErrorCode = exploded[0];
                                if (exploded.Length == 3 && smsHistory.ErrorCode == "1900")
                                {
                                    if (!string.IsNullOrEmpty(exploded[2]))
                                    {
                                        smsHistory.ResponseCode = exploded[2];
                                        SmsViewModel currentNumber = smsToBeSend.FirstOrDefault(r => r.ReceiverNumber == receiverNumberfrmSplit);
                                        if (currentNumber != null)
                                        {
                                            smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                                            smsHistory.SmsReceiver = _smsService.LoadReceiverById(currentNumber.SmsReceiverId);
                                        }

                                    }
                                }
                                else
                                {
                                    smsHistory.Status = SmsHistory.EntityStatus.Pending;
                                    SmsViewModel currentNumber = smsToBeSend.Skip(mobIteration).Take(1).FirstOrDefault();
                                    if (currentNumber != null)
                                    {
                                        smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                                        smsHistory.SmsReceiver = _smsService.LoadReceiverById(currentNumber.SmsReceiverId);
                                    }
                                }

                                _smsService.SaveSmsHistory(smsHistory);

                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool GenarateScheduleSmsAndNumber(SmsSettings smsSettings, Student student, StudentProgram studentProgram = null)
        {
            long smsUserIds = Convert.ToInt64(ConfigurationManager.AppSettings["SmsUserId"]);
            if (smsSettings != null && student != null)
            {
                if (studentProgram == null)
                    studentProgram = student.StudentPrograms.LastOrDefault();
                StudentPayment studentPayment = studentProgram.StudentPayments.OrderBy(x => x.Id).LastOrDefault();
                IList<SmsDynamicOption> options = smsSettings.SmsType.DynamicOptions;
                var message = smsSettings.Template;
                var mask = smsSettings.MaskName;

                var smsToBeSend = new List<SmsViewModel>();

                if (options.Any())
                {
                    foreach (var o in options)
                    {
                        string optionField = "[[{{" + o.Name + "}}]]";
                        string optionValue = "";

                        switch (o.Name)
                        {
                            case "Nick Name":
                                optionValue = student.NickName;
                                break;
                            case "Program Roll":
                                optionValue = studentProgram.PrnNo;
                                break;
                            case "Payable Amount":
                                if (studentPayment.PayableAmount != null && studentPayment.PayableAmount > 0)
                                {
                                    optionValue = Convert.ToDecimal(studentPayment.PayableAmount).ToString("n0").Replace(",", "");
                                }
                                else
                                {
                                    optionValue = "0";
                                }
                                break;
                            case "Paid Amount":
                                if (studentPayment.ReceivedAmount != null && studentPayment.ReceivedAmount > 0)
                                {
                                    optionValue = Convert.ToDecimal(studentPayment.ReceivedAmount).ToString("n0").Replace(",", ""); ;
                                }
                                else
                                {
                                    optionValue = "0";
                                }
                                break;
                            case "Due Amount":
                                if (studentPayment.DueAmount != null && studentPayment.DueAmount > 0)
                                {
                                    optionValue = Convert.ToDecimal(studentPayment.DueAmount).ToString("n0").Replace(",", ""); ;
                                }
                                else
                                {
                                    optionValue = "0";
                                }
                                break;
                            case "Offered Discount":
                                if (studentPayment.DiscountAmount != null && studentPayment.DiscountAmount > 0)
                                {
                                    optionValue = Convert.ToDecimal(studentPayment.DiscountAmount).ToString("n0").Replace(",", ""); ;
                                }
                                else
                                {
                                    optionValue = "0";
                                }
                                break;
                            case "Special Discount":
                                if (studentPayment.SpDiscountAmount != null && studentPayment.SpDiscountAmount > 0)
                                {
                                    optionValue = Convert.ToDecimal(studentPayment.SpDiscountAmount).ToString("n0").Replace(",", ""); ;
                                }
                                else
                                {
                                    optionValue = "0";
                                }
                                break;
                            case "Session":
                                optionValue = studentProgram.Batch.Session.Name;
                                break;
                            case "Program":
                                optionValue = studentProgram.Batch.Program.Name;
                                break;
                            case "Branch":
                                optionValue = studentProgram.Batch.Branch.Name;
                                break;
                            case "Campus":
                                optionValue = studentProgram.Batch.Campus.Name;
                                break;
                            case "Next Payment Date":
                                if (studentPayment.DueAmount != null && studentPayment.DueAmount > 0)
                                {
                                    optionValue = studentPayment.NextReceivedDate.Value.ToString("dd MMM yyyy");
                                }
                                else
                                {
                                    optionValue = "N/A";
                                }

                                break;
                            case "Batch Days":
                                optionValue = studentProgram.Batch.Days;
                                break;
                            case "Batch Time":
                                optionValue = studentProgram.Batch.FormatedBatchTime;
                                break;
                            case "Batch Name":
                                optionValue = studentProgram.Batch.Name;
                                break;

                            case "Previous Due":
                                if (studentPayment.PayableAmount != null && studentPayment.PayableAmount > 0)
                                {
                                    optionValue = Convert.ToDecimal(studentPayment.PayableAmount).ToString("n0").Replace(",", ""); ;
                                }
                                else
                                {
                                    optionValue = "0";
                                }
                                break;

                            case "Current Due Amount":
                                if (studentPayment.DueAmount != null && studentPayment.DueAmount > 0)
                                {
                                    optionValue = Convert.ToDecimal(studentPayment.DueAmount).ToString("n0").Replace(",", ""); ;
                                }
                                else
                                {
                                    optionValue = "0";
                                }
                                break;

                            default:
                                optionValue = "";
                                break;
                        }
                        message = message.Replace(optionField, optionValue);

                    }
                }
                //message = message + student.DateOfBirth.Value.ToString("dd MMM yyyy");
                foreach (var sr in smsSettings.SmsReceivers)
                {
                    var svm = new SmsViewModel();
                    switch (sr.Name)
                    {
                        case "Mobile Number (Personal)":
                            string studentMobile = CheckMobileNumber(student.Mobile);
                            if (studentMobile != null)
                            {
                                svm.SmsReceiverId = sr.Id;
                                svm.ReceiverNumber = "88" + studentMobile;
                            }
                            break;
                        case "Mobile Number (Father)":
                            string guardiansMobile1 = CheckMobileNumber(student.GuardiansMobile1);
                            if (guardiansMobile1 != null)
                            {
                                svm.SmsReceiverId = sr.Id;
                                svm.ReceiverNumber = "88" + guardiansMobile1;
                            }
                            break;
                        case "Mobile Number (Mother)":
                            string guardiansMobile2 = CheckMobileNumber(student.GuardiansMobile2);
                            if (guardiansMobile2 != null)
                            {
                                svm.SmsReceiverId = sr.Id;
                                svm.ReceiverNumber = "88" + guardiansMobile2;
                            }
                            break;
                        case "Mobile Number (Campus Incharge)":
                            string contactNumber = CheckMobileNumber(studentProgram.Batch.Campus.ContactNumber);
                            if (CheckMobileNumber(studentProgram.Batch.Campus.ContactNumber) != null)
                            {
                                svm.SmsReceiverId = sr.Id;
                                svm.ReceiverNumber = "88" + contactNumber;
                            }
                            break;
                        default:
                            break;
                    }

                    if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                    {
                        var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                        if (!alreadyExist.Any())
                        {
                            smsToBeSend.Add(svm);
                        }
                    }
                }

                if (smsToBeSend.Any())
                {
                    if (String.IsNullOrEmpty(mask))
                    {
                        mask = "";
                    }
                    string returnResult = DeliverOneToManySms(smsSettings, smsToBeSend, message, mask, studentProgram.Program.Id, student.Id, null);
                    var returnArray = returnResult.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                    long smsUserId = Convert.ToInt64(ConfigurationManager.AppSettings["SmsUserId"]);
                    foreach (var rMessagePerMobile in returnArray)
                    {
                        try
                        {
                            var smsHistory = new SmsHistory();
                            var exploded = rMessagePerMobile.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                            var receiverNumberfrmSplit = "88" + exploded[1];
                            var currentNumber = smsToBeSend.FirstOrDefault(r => r.ReceiverNumber == receiverNumberfrmSplit);
                            smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                            smsHistory.SmsReceiver = _smsService.LoadReceiverById(currentNumber.SmsReceiverId);
                            smsHistory.Sms = message;
                            smsHistory.Organization = studentProgram.Program.Organization;
                            smsHistory.Program = studentProgram.Program;
                            smsHistory.SmsSettings = smsSettings;
                            smsHistory.Student = student;
                            smsHistory.ErrorCode = exploded[0];
                            smsHistory.CreateBy = smsUserId;//Create By Super Admin
                            smsHistory.ModifyBy = smsUserId;//Create By Super Admin
                            smsHistory.Mask = mask;
                            smsHistory.Type = Convert.ToInt32(smsSettings.SmsType.Id);//for result
                            smsHistory.Priority = 4;
                            if (exploded.Length == 3 && smsHistory.ErrorCode == "1900")
                            {
                                if (!string.IsNullOrEmpty(exploded[2]))
                                {
                                    smsHistory.ResponseCode = exploded[2];
                                }
                            }
                            else
                            {
                                smsHistory.Status = SmsHistory.EntityStatus.Pending;
                            }


                            _smsService.SaveSmsHistory(smsHistory);

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    return true;
                }

            }
            return false;
        }

        public static string CheckMobileNumber(string mobile)
        {
            if (mobile != null)
            {
                var regex = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
                var match = regex.Match(mobile);
                if (match.Success == true)
                {
                    switch (mobile.Length)
                    {
                        case 14:
                            mobile = mobile.Substring(3);
                            break;
                        case 13:
                            mobile = mobile.Substring(2);
                            break;
                        case 10:
                            mobile = "0" + mobile;
                            break;
                    }
                }
                else { mobile = null; }
            }
            return mobile;
        }
    }

    public class SendSmsNewApi
    {
        private readonly ILog _logger = LogManager.GetLogger("SMSHelper");

        private void LogError(string message, Exception exception)
        {
            if (exception != null)
            {
                _logger.Error(message, exception);
            }
            else
            {
                _logger.Error(message);
            }
        }
        private static readonly ISmsService _smsService;
        private static readonly IStudentService _studentService;
        private static readonly IStudentProgramService _studentProgramService;
        private static readonly IBatchService _batchService;
        private static readonly IExamsStudentMarksService _examsStudentMarksService;
        static SendSmsNewApi()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _smsService = new SmsService(nHsession);
            _studentService = new StudentService(nHsession);
            _studentProgramService = new StudentProgramService(nHsession);
            _batchService = new BatchService(nHsession);
            _examsStudentMarksService = new ExamsStudentMarksService(nHsession);

        }
        public static string CheckMobileNumber(string mobile)
        {
            if (mobile != null)
            {
                var regex = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
                var match = regex.Match(mobile);
                if (match.Success == true)
                {
                    switch (mobile.Length)
                    {
                        case 14:
                            mobile = mobile.Substring(3);
                            break;
                        case 13:
                            mobile = mobile.Substring(2);
                            break;
                        case 10:
                            mobile = "0" + mobile;
                            break;
                    }
                }
                else { mobile = null; }
            }
            return mobile;
        }
        public bool TemplateSmsGenerateAndSaveToDb(List<TemporarySms> tempSmses)
        {
            var smsSettings = new SmsSettings();
            IList<SmsHistory> smsHistoryList = new List<SmsHistory>();
            IList<TemporarySms> invalidTemoSmsData = new List<TemporarySms>();
            var program = new Program();
            var organization = new Organization();
            try
            {
                program = tempSmses.FirstOrDefault().StudentProgram.Program;
                organization = program.Organization;
                smsSettings = _smsService.LoadSmsSettingsBySmsTypePrgram(6, organization.Id, program.Id);
            }
            catch (Exception ex)
            {
                return false;
            }
            if (smsSettings != null && smsSettings.SmsReceivers.Any() &&
                    !string.IsNullOrEmpty(smsSettings.Template))
            {
                try
                {
                    #region Generate SMSHistory List
                    foreach (var xmlsms in tempSmses)
                    {
                        bool isValidMessageType = false;
                        //bool isBlocked = 
                        if (xmlsms.ExamType == ExamType.Mcq)
                        {
                            if (smsSettings.Template.Contains("[[{{MCQ Marks}}]]"))
                            {
                                isValidMessageType = true;
                            }
                        }
                        else if (xmlsms.ExamType == ExamType.Written)
                        {
                            if (smsSettings.Template.Contains("[[{{Written Marks}}]]"))
                            {
                                isValidMessageType = true;
                            }
                        }
                        decimal mcqFullMarks = 0;
                        decimal writtenFullMarks = 0;
                        if (isValidMessageType)
                        {
                            var student = xmlsms.Student;
                            var options = smsSettings.SmsType.DynamicOptions;
                            var message = smsSettings.Template;
                            var mask = smsSettings.MaskName;
                            var smsToBeSend = new List<SmsViewModel>();
                            var mcqExamDetais =
                                xmlsms.Exams.ExamDetails.Where(
                                    x => x.ExamType == ExamType.Mcq && x.Status == ExamsDetails.EntityStatus.Active)
                                    .ToList();
                            var writtenExamDetais =
                                xmlsms.Exams.ExamDetails.Where(
                                    x => x.ExamType == ExamType.Written && x.Status == ExamsDetails.EntityStatus.Active)
                                    .ToList();

                            if (xmlsms.Exams.IsMcq)
                            {
                                if (xmlsms.Exams.McqFullMarks != null)
                                {
                                    mcqFullMarks = xmlsms.Exams.McqFullMarks;
                                }
                            }
                            if (xmlsms.Exams.IsWritten)
                            {
                                if (xmlsms.Exams.WrittenFullMark != null)
                                {
                                    writtenFullMarks = xmlsms.Exams.WrittenFullMark;
                                }
                            }
                            var fullMarks = mcqFullMarks + writtenFullMarks;

                            decimal mcqMarks = 0;
                            decimal writtenMarks = 0;
                            int flag = 0;

                            if (mcqExamDetais.Any())
                            {
                                var strudentMcqMarks =
                                    _examsStudentMarksService.LoadExamsStudentMarksByExamStudentProgram(
                                        xmlsms.Exams.Id,
                                        xmlsms.StudentProgram.Id, ExamType.Mcq);
                                if (strudentMcqMarks.Any())
                                {
                                    mcqMarks = strudentMcqMarks.Sum(x => x.ObtainMark);
                                    flag = 1;
                                }
                            }
                            if (writtenExamDetais.Any())
                            {
                                var strudentWrittenMarks =
                                    _examsStudentMarksService.LoadExamsStudentMarksByExamStudentProgram(
                                        xmlsms.Exams.Id,
                                        xmlsms.StudentProgram.Id, ExamType.Written);
                                if (strudentWrittenMarks.Any())
                                {
                                    writtenMarks = strudentWrittenMarks.Sum(x => x.ObtainMark);
                                    flag = flag == 1 ? 3 : 2;
                                }
                            }
                            if (options.Any())
                            {
                                foreach (var o in options)
                                {
                                    string optionField = "[[{{" + o.Name + "}}]]";
                                    string optionValue = "";

                                    switch (o.Name)
                                    {
                                        case "Nick Name":
                                            optionValue = student.NickName;
                                            break;
                                        case "Program Roll":
                                            optionValue = xmlsms.StudentProgram.PrnNo;
                                            break;
                                        case "Registration Number":
                                            optionValue = student.RegistrationNo;
                                            break;
                                        case "Exam Name":
                                            optionValue = xmlsms.Exams.Name;
                                            break;
                                        case "MCQ Marks":
                                            if (message.Contains("[[{{MCQ Marks}}]]") &&
                                                message.Contains("[[{{MCQ Full Marks}}]]"))
                                            {
                                                optionField =
                                                    message.Substring(
                                                        message.IndexOf("[[{{MCQ Marks}}]]",
                                                            System.StringComparison.Ordinal),
                                                        ((message.LastIndexOf("[[{{MCQ Full Marks}}]]",
                                                            System.StringComparison.Ordinal) + 22) -
                                                         message.IndexOf("[[{{MCQ Marks}}]]",
                                                             System.StringComparison.Ordinal)));
                                            }
                                            if (flag == 1 || flag == 3)
                                            {
                                                if (message.Contains("[[{{MCQ Marks}}]]") &&
                                                    message.Contains("[[{{MCQ Full Marks}}]]"))
                                                {
                                                    optionValue =
                                                        "MCQ: " + mcqMarks.ToString() +
                                                        optionField.Substring(17, optionField.Length - (22 + 17)) +
                                                        mcqFullMarks.ToString();
                                                }
                                                else
                                                {
                                                    optionValue = "MCQ: " + mcqMarks.ToString();
                                                }
                                            }
                                            else
                                            {
                                                optionValue = "";
                                            }
                                            break;
                                        case "MCQ Full Marks":
                                            if (flag == 1 || flag == 3)
                                                optionValue = mcqFullMarks.ToString();
                                            else optionValue = "";
                                            break;
                                        case "Written Marks":
                                            if (message.Contains("[[{{Written Marks}}]]") &&
                                                message.Contains("[[{{Written Full Marks}}]]"))
                                            {
                                                optionField =
                                                    message.Substring(
                                                        message.IndexOf("[[{{Written Marks}}]]",
                                                            System.StringComparison.Ordinal),
                                                        ((message.LastIndexOf("[[{{Written Full Marks}}]]",
                                                            System.StringComparison.Ordinal) + 26) -
                                                         message.IndexOf("[[{{Written Marks}}]]",
                                                             System.StringComparison.Ordinal)));
                                            }
                                            if (flag == 2 || flag == 3)
                                            {
                                                if (message.Contains("[[{{Written Marks}}]]") &&
                                                    message.Contains("[[{{Written Full Marks}}]]"))
                                                {
                                                    optionValue =
                                                        "Written-" + writtenMarks.ToString() +
                                                        optionField.Substring(21, optionField.Length - (26 + 21)) +
                                                        writtenFullMarks.ToString();
                                                }
                                                else
                                                {
                                                    optionValue = "Written-" + writtenMarks.ToString();
                                                }
                                            }
                                            else optionValue = "";
                                            break;
                                        case "Written Full Marks":
                                            if (flag == 2 || flag == 3)
                                                optionValue = writtenFullMarks.ToString();
                                            else optionValue = "";
                                            break;
                                        case "Total Marks":
                                            if (message.Contains("[[{{Total Marks}}]]") &&
                                                message.Contains("[[{{Full Marks}}]]"))
                                            {
                                                optionField =
                                                    message.Substring(
                                                        message.IndexOf("[[{{Total Marks}}]]",
                                                            System.StringComparison.Ordinal),
                                                        ((message.LastIndexOf("[[{{Full Marks}}]]",
                                                            System.StringComparison.Ordinal) + 18) -
                                                         message.IndexOf("[[{{Total Marks}}]]",
                                                             System.StringComparison.Ordinal)));
                                            }
                                            if (flag == 3)
                                            {
                                                if (message.Contains("[[{{Total Marks}}]]") &&
                                                    message.Contains("[[{{Full Marks}}]]"))
                                                {
                                                    optionValue = "Total-" + (writtenMarks + mcqMarks) +
                                                                  optionField.Substring(19,
                                                                      optionField.Length - (18 + 19)) +
                                                                  fullMarks.ToString();
                                                }
                                                else
                                                {
                                                    optionValue = "Total-" + (writtenMarks + mcqMarks);
                                                }

                                            }

                                            else optionValue = "";
                                            break;
                                        case "Full Marks":
                                            optionValue = fullMarks.ToString();
                                            break;
                                        default:
                                            optionValue = "";
                                            break;
                                    }
                                    message = message.Replace(optionField, optionValue);
                                }
                            }
                            message = message.Trim();
                            foreach (var sr in smsSettings.SmsReceivers)
                            {
                                var svm = new SmsViewModel();
                                switch (sr.Name)
                                {
                                    case "Mobile Number (Personal)":
                                        string studentMobile = CheckMobileNumber(student.Mobile);
                                        if (studentMobile != null)
                                        {
                                            svm.SmsReceiverId = sr.Id;
                                            svm.ReceiverNumber = "88" + studentMobile;
                                        }
                                        break;
                                    case "Mobile Number (Father)":
                                        string guardiansMobile1 = CheckMobileNumber(student.GuardiansMobile1);
                                        if (guardiansMobile1 != null)
                                        {
                                            svm.SmsReceiverId = sr.Id;
                                            svm.ReceiverNumber = "88" + guardiansMobile1;
                                        }
                                        break;
                                    case "Mobile Number (Mother)":
                                        string guardiansMobile2 = CheckMobileNumber(student.GuardiansMobile2);
                                        if (guardiansMobile2 != null)
                                        {
                                            svm.SmsReceiverId = sr.Id;
                                            svm.ReceiverNumber = "88" + guardiansMobile2;
                                        }
                                        break;
                                    case "Mobile Number (Campus Incharge)":
                                        string contactNumber =
                                            CheckMobileNumber(xmlsms.StudentProgram.Batch.Campus.ContactNumber);
                                        if (CheckMobileNumber(xmlsms.StudentProgram.Batch.Campus.ContactNumber) != null)
                                        {
                                            svm.SmsReceiverId = sr.Id;
                                            svm.ReceiverNumber = "88" + contactNumber;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                                {
                                    var alreadyExist =
                                        smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                                    if (!alreadyExist.Any())
                                    {
                                        smsToBeSend.Add(svm);
                                    }
                                }
                            }
                            if (smsToBeSend.Any())
                            {
                                if (String.IsNullOrEmpty(mask))
                                {
                                    mask = "";
                                }
                                foreach (var stbs in smsToBeSend)
                                {
                                    var smsHistory = new SmsHistory();
                                    var currentNumber = stbs;
                                    smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                                    smsHistory.SmsReceiver =
                                        smsSettings.SmsReceivers.FirstOrDefault(x => x.Id == currentNumber.SmsReceiverId);
                                    smsHistory.Sms = message;
                                    smsHistory.Program = xmlsms.StudentProgram.Program;
                                    smsHistory.Organization = xmlsms.StudentProgram.Program.Organization;
                                    smsHistory.SmsSettings = smsSettings;
                                    smsHistory.Student = student;
                                    smsHistory.Mask = mask;
                                    smsHistory.Type = 6; //for result
                                    smsHistory.Status = SmsHistory.EntityStatus.Pending;
                                    smsHistory.CreateBy = xmlsms.CreateBy;
                                    smsHistory.ModifyBy = xmlsms.ModifyBy;
                                    smsHistoryList.Add(smsHistory);

                                }
                            }
                        }
                        else
                        {
                            invalidTemoSmsData.Add(xmlsms);
                        }
                    }

                    bool isSucess = _smsService.SaveSmsHistoryListAndDeleteTemporarySmsData(smsHistoryList, tempSmses, invalidTemoSmsData);
                    #endregion

                    return true;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return false;
                }
            }
            return false;
        }
    }

    public static class FileUpload
    {
        public static string GetFilePath(string fileNamePart, string fileName)
        {
            string filePath = "";
            try
            {
                filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Uploads/" + fileNamePart.ToString() + "/" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "__" + fileName);
            }
            catch (Exception ex) { }

            return filePath;
        }

        public static string XmlFileValidationCheck(string currentUser, HttpPostedFileBase postedFile)
        {
            try
            {
                string extension = postedFile.FileName.Substring(postedFile.FileName.Length - 3, 3);
                if (StringComparer.CurrentCultureIgnoreCase.Equals(extension, "xml"))
                {
                    var dir = new DirectoryInfo(GetFilePath(currentUser, ""));
                    if (!dir.Exists)
                    {
                        dir.Create();
                    }
                    var fileName = DateTime.Now.ToString("yyyyMMdd_HHmmss_") + postedFile.FileName;
                    //string filePath = GetFilePath(currentUser, postedFile.FileName);

                    var fileInfo = new FileInfo(dir.ToString() + "/" + fileName);
                    if (fileInfo.Exists)
                    {
                        fileInfo.Delete();
                    }
                    postedFile.SaveAs(dir.ToString() + "/" + fileName);
                    return dir.ToString() + "/" + fileName;
                }
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
            return "";
        }
    }


    
}