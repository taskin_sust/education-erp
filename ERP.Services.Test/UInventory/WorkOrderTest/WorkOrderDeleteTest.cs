﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.WorkOrderTest
{
    interface IWorkOrderDeleteTest
    {
        #region basic Test

        void Should_Return_True();
        void Should_Return_Null_Exception();
        void Should_Return_InvalidDataException();

        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "WorkOrder")]
    public class WorkOrderDeleteTest : WorkOrderBaseTest, IWorkOrderDeleteTest
    {
        #region basic Test

        [Fact]
        public void Should_Return_True()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.PertialIssued, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(_organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create()
                .CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList)
                .Persist();

            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierFactory.Object.SupplierPriceQuoteList.Add(_supplierPriceQuoteFactory.Object);//Add price quote list in supplier

            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object)
                .Persist();
            Assert.True(_workOrderService.IsCancel(_workOrderFactory.Object.Id));
        }

        [Fact]
        public void Should_Return_Null_Exception()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.PertialIssued, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(_organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierFactory.Object.SupplierPriceQuoteList.Add(_supplierPriceQuoteFactory.Object);//Add price quote list in supplier

            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object)
                .Persist();
            Assert.Throws<NullObjectException>(() => _workOrderService.IsCancel(_workOrderFactory.Object.Id + 1));
        }

        [Fact]
        public void Should_Return_InvalidDataException()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.PertialIssued, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(_organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierFactory.Object.SupplierPriceQuoteList.Add(_supplierPriceQuoteFactory.Object);//Add price quote list in supplier

            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object)
                .Persist();

            _goodsReceiveFactory.Create()
                .WithBranch(_branchFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrder(_workOrderFactory.Object);

            _goodsReceiveDetailsFactory.Create()
                .WithProgram(_quotationFactory.Object.Program)
                .WithSession(_quotationFactory.Object.Session)
                .WithItem(_itemFactory.Object)
                .WithPurpose((int)_quotationFactory.Object.Purpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);

            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Persist(false);

            _workOrderFactory.Object.GoodsReceiveList.Add(_goodsReceiveFactory.Object);

            Assert.Throws<InvalidDataException>(() => _workOrderService.IsCancel(_workOrderFactory.Object.Id));
        }

        #endregion
    
    }
}
