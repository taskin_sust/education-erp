﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.ItemGroupTest
{
    interface IItemGroupLoadByIdTest
    {
        void Should_Return_LoadById_Null();
        void Should_Return_LoadById_NotNull();
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "ItemGroup")]
    public class ItemGroupLoadByIdTest : ItemGroupBaseTest, IItemGroupLoadByIdTest
    {
        [Fact]
        public void Should_Return_LoadById_Null()
        {
            var itemGroupFactory = _itemGroupFactory.Create().Persist();
            ItemGroup itemGroup = _itemGroupService.LoadById(itemGroupFactory.Object.Id+1);
            Assert.Null(itemGroup);
        }

        [Fact]
        public void Should_Return_LoadById_NotNull()
        {
            var itemGroupFactory = _itemGroupFactory.Create().Persist();
            ItemGroup itemGroup = _itemGroupService.LoadById(itemGroupFactory.Object.Id);
            Assert.NotNull(itemGroup);
            Assert.True(itemGroup.Id > 0);
        }
    }
}
