﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class ItemGroupController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IItemGroupService _itemGroupService;
        private readonly IUserService _userService;
        private ItemGroup _itemGroup;

        public ItemGroupController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _itemGroupService = new ItemGroupService(session);
            _userService = new UserService(session);
        }

        #endregion

        #region Index/Manage Page

        public ActionResult Index()
        {
            try
            {
                ViewBag.StatusText = _commonHelper.GetStatus();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #endregion

        #region Render Data Table

        public JsonResult ItemGroupList(int draw, int start, int length, string name, string status)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            int recordsFiltered = 0;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "Rank";
                            break;
                        case "2":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                recordsTotal = _itemGroupService.LoadItemGroupCount(orderBy, orderDir.ToUpper(), name, status);
                recordsFiltered = recordsTotal;
                var itemGroupList = _itemGroupService.LoadItemGroup(start, length, orderBy, orderDir.ToUpper(), name, status).ToList();

                foreach (var itemGroup in itemGroupList)
                {
                    var str = new List<string>();
                    string action = "";
                    action += LinkGenerator.GetDetailsLink("Details", "ItemGroup", itemGroup.Id);
                    action += LinkGenerator.GetEditLink("Edit", "ItemGroup", itemGroup.Id);
                    action += LinkGenerator.GetDeleteLinkForModal(itemGroup.Id, itemGroup.Name);

                    str.Add(itemGroup.Name);
                    str.Add(itemGroup.Rank.ToString());
                    str.Add(StatusTypeText.GetStatusText(itemGroup.Status));

                    str.Add(action);
                    getData.Add(str);
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData
            });
        }

        #endregion

        #region Details Function

        public ActionResult Details(int id)
        {
            try
            {
                _itemGroup = _itemGroupService.LoadById(Convert.ToInt64(id));
                if (_itemGroup != null)
                {
                    _itemGroup.CreateByText = _userService.GetUserNameByAspNetUserId(_itemGroup.CreateBy);
                    _itemGroup.ModifyByText = _userService.GetUserNameByAspNetUserId(_itemGroup.ModifyBy);
                }
                else
                {
                    ViewBag.ErrorMessage = "Invalid Id";
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View(_itemGroup);
        }

        #endregion

        #region Create Function

        public ActionResult Create()
        {
            try
            {
                if ((string)TempData["ItemGroupMessage"] != "")
                    ViewBag.SuccessMessage = TempData["ItemGroupMessage"];
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(ItemGroup itemGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _itemGroupService.SaveOrUpdate(itemGroup);
                    TempData["ItemGroupMessage"] = "Item Group saved successfully.";
                    return RedirectToAction("Create");
                    
                }
                ViewBag.ErrorMessage = "Item Created fail !!";
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return View();
        }

        #endregion

        #region Update Operation

        public ActionResult Edit(int id)
        {
            _itemGroup = _itemGroupService.LoadById(id);
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            if (_itemGroup == null)
            {
                ViewBag.Errormessage = "Invalid id";
                RedirectToAction("Index");
            }
            return View(_itemGroup);
        }

        [HttpPost]
        public ActionResult Edit(int id, ItemGroup itemGroup)
        {
            try
            {
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                _itemGroup = _itemGroupService.LoadById(id);
                if (ModelState.IsValid && _itemGroup != null)
                {
                    _itemGroupService.SaveOrUpdate(itemGroup);
                    ViewBag.SuccessMessage = "Item Group successfully updated.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Problem Occurred, during item group update.";
                }
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View(_itemGroup);
        }

        #endregion

        #region Delete Operation

        public ActionResult Delete(long id)
        {
            string errorMessage = "";
            try
            {
                _itemGroupService.IsDelete(id);
                return Json(new Response(true, "Item Group sucessfully deleted."));
            }
            catch (NullObjectException ex)
            {
                errorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                errorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new Response(false, errorMessage));
        }

        #endregion
    }
}
