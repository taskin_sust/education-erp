﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface IAcademicInfoService : IBaseService
    {
        #region Operational Function
        
        List<long> UpdateAcademicInfo(TeamMember member, List<TeamMemberAcademicInfoArray> academicInfoArray);
        bool DeleteAcademicInfo(long id);

        #endregion

        #region Single Instances Loading Function

        AcademicInfo GetById(long id);
        #endregion

        #region List Loading Function
        IList<AcademicInfo> LoadMemberAccademicInfo(long teamMemberId);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class AcademicInfoService : BaseService, IAcademicInfoService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IAcademicInfoDao _academicInfoDao;
        private readonly IAcademicExamDao _hrExamDao;
        private readonly IHrBoardDao _hrBoardDao;
        private readonly IInstituteDao _instituteDao;
        public AcademicInfoService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _academicInfoDao = new AcademicInfoDao() { Session = session };
            _hrExamDao = new AcademicExamDao() { Session = session };
            _hrBoardDao = new HrBoardDao() { Session = session };
            _instituteDao = new InstituteDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        //public AcademicInfo Save(AcademicInfo academicInfo)
        //{
        //    ITransaction trans = null;
        //    try
        //    {
        //        using (trans = Session.BeginTransaction())
        //        {
        //            _academicInfoDao.Save(academicInfo);
        //            trans.Commit();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    finally
        //    {
        //        if (trans != null && trans.IsActive)
        //            trans.Rollback();
        //    }
        //    return academicInfo;
        //}

        public List<long> UpdateAcademicInfo(TeamMember member, List<TeamMemberAcademicInfoArray> academicInfoArray)
        {
            List<long> idList = new List<long>();
            ITransaction trans = null;

            try
            {
                if (member == null)
                    throw new InvalidDataException("Invalid Team Member");

                if (academicInfoArray.Any())
                {
                    using (trans = Session.BeginTransaction())
                    {
                        int dataCount = _academicInfoDao.LoadAccademicInfoByMember(member.Id).Count;
                        bool newRank = true;
                        foreach (var row in academicInfoArray)
                        {
                            AcademicInfo academicInfoEntity = null;
                            if (row.Id > 0)
                            {
                                var oldEntity = _academicInfoDao.LoadById(row.Id);
                                if (oldEntity != null)
                                {
                                    academicInfoEntity = oldEntity;
                                    newRank = false;
                                }
                                else
                                {
                                    academicInfoEntity = new AcademicInfo();
                                }
                            }
                            else
                            {
                                academicInfoEntity = new AcademicInfo();
                            }
                            var exam = _hrExamDao.LoadById(Convert.ToInt64(row.ExamName));
                            var board = _hrBoardDao.LoadById(Convert.ToInt64(row.Board));
                            var institute = _instituteDao.LoadById(Convert.ToInt64(row.Institute));

                            if (exam == null || board == null || institute == null)
                                throw new InvalidDataException("Please fillup correct information");

                            academicInfoEntity.DegreeTitle = row.DegreeTitle;
                            academicInfoEntity.Year = row.PassingYear;
                            academicInfoEntity.Major = row.MajorOrGroup;
                            academicInfoEntity.Exam = (BusinessModel.Entity.Hr.AcademicExam) exam;
                            academicInfoEntity.TeamMember = member;
                            academicInfoEntity.Board = board;
                            academicInfoEntity.Institute = institute;

                            if (row.Result != (int) HrAcademicResult.Grade)
                            {
                                academicInfoEntity.ResultType = (int) HrAcademicResultType.Division;
                                academicInfoEntity.Result = row.Result;
                                academicInfoEntity.CurrentAcademicStatus = null;
                            }
                            else if (row.Result == (int) HrAcademicResult.Grade)
                            {
                                academicInfoEntity.ResultType = (int) HrAcademicResultType.Grade;
                                //academicInfoEntity.Result = row.Result;
                                academicInfoEntity.Grade = row.Grade;
                                academicInfoEntity.GradeScale = row.GradeScale;
                                academicInfoEntity.CurrentAcademicStatus = null;
                            }
                            else if (row.Result == 0 && row.ExamStatus > 0)
                            {
                                if (row.ExamStatus > 0)
                                    academicInfoEntity.CurrentAcademicStatus = row.ExamStatus;
                                else
                                    academicInfoEntity.CurrentAcademicStatus = (int) HrAcademicExamStatus.Appeared;
                            }

                            if (newRank)
                            {
                                academicInfoEntity.Rank = dataCount + 1;
                                dataCount++;
                            }
                            academicInfoEntity.CreationDate = DateTime.Now;
                            academicInfoEntity.ModificationDate = DateTime.Now;
                            academicInfoEntity.Status = AcademicInfo.EntityStatus.Active;
                            _academicInfoDao.SaveOrUpdate(academicInfoEntity);
                            idList.Add(academicInfoEntity.Id);
                        }
                        trans.Commit();
                    }
                }
                else throw new InvalidDataException("Academic info can't empty");
            }
            catch (FormatException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
            return idList;
        }

        public bool DeleteAcademicInfo(long id)
        {
            ITransaction trans = null;
            try
            {
                AcademicInfo academicInfo = _academicInfoDao.LoadById(id);
                if (academicInfo == null)
                    throw new InvalidDataException("Invalid academic info");
                using (trans = Session.BeginTransaction())
                {
                    academicInfo.Status = AcademicInfo.EntityStatus.Delete;
                    _academicInfoDao.SaveOrUpdate(academicInfo);
                    trans.Commit();
                    return true;
                }
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public AcademicInfo GetById(long id)
        {
            try
            {
                var entity = _academicInfoDao.LoadById(id);
                return entity ?? null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
        }
        #endregion

        #region List Loading Function
        public IList<AcademicInfo> LoadMemberAccademicInfo(long teamMemberId)
        {
            try
            {
                var list = _academicInfoDao.LoadAccademicInfoByMember(teamMemberId);
                if (list != null && list.Count > 0)
                {
                    return list;
                }
                return null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return null;
        }
        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}
