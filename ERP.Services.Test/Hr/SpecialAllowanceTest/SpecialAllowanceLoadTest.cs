﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.SpecialAllowanceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "SpecialAllowanceSetting")]
    public class SpecialAllowanceLoadTest : SpecialAllowanceBaseTest
    {
        #region Base Test
        
        [Fact]
        public void GetSpecialAllowance_Successfull()
        {
            const string effectiveDate = "Oct 2016";
            var teamMember = PersistTeamMember(MentorId);
            Branch branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).SingleOrDefault();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, branch);


            _specialAllowanceSettingFactory.Create(teamMember.Pin)
                .WithAmount(2000).WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate).WithRemarks("TEST REMARK");

            _specialAllowanceSettingService.SaveOrUpdate(userMenu, _specialAllowanceSettingFactory.Object);
            var specialAllowanceSetting = _specialAllowanceSettingService.GetSpecialAllowanceSettings(_specialAllowanceSettingFactory.Object.Id);
            Assert.NotNull(specialAllowanceSetting);
        }

        [Fact]
        public void GetSpecialAllowance_Count()
        {
            var teamMember = PersistTeamMember(MentorId);
            Branch branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).SingleOrDefault();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, branch);
            const string effectiveDate = "Oct 2016";

            _specialAllowanceSettingFactory.Create(teamMember.Pin)
                .WithAmount(2000).WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate).WithRemarks("TEST REMARK");

            _specialAllowanceSettingService.SaveOrUpdate(userMenu, _specialAllowanceSettingFactory.Object);
            int count = _specialAllowanceSettingService.GetSpecialAllowanceSettingCount(userMenu, "", "", null, "", null);
            Assert.True(count > 0);
        }

        [Fact]
        public void LoadIndividualTeamMemberSpecialAllowanceSetting_Should_Successful()
        {
            var teamMember = PersistTeamMember(MentorId);
            Branch branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).SingleOrDefault();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, branch);
            const string effectiveDate = "Oct 2016";

            _specialAllowanceSettingFactory.Create(teamMember.Pin)
                .WithAmount(2000).WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate).WithRemarks("TEST REMARK");

            _specialAllowanceSettingService.SaveOrUpdate(userMenu, _specialAllowanceSettingFactory.Object);
            var entity = _specialAllowanceSettingService.LoadIndividualTeamMemberSpecialAllowanceSetting(teamMember.Pin, _specialAllowanceSettingFactory.Object.EffectiveDate);
            Assert.NotNull(entity);
        }

        [Fact]
        public void LoadSpecialAllowanceSettings_Should_Return_List()
        {
            var teamMember = PersistTeamMember(MentorId);
            Branch branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).SingleOrDefault();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, branch);
            const string effectiveDate = "Oct 2016";

            _specialAllowanceSettingFactory
                .Create(teamMember.Pin).WithAmount(2000)
                .WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate)
                .WithRemarks("TEST REMARK");

            _specialAllowanceSettingService.SaveOrUpdate(userMenu, _specialAllowanceSettingFactory.Object);
            var specialSettings = _specialAllowanceSettingService.LoadSpecialAllowanceSettings(userMenu, 0, 1, "", "", null, "", null);
            Assert.True(specialSettings.Count > 0);

        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void GetSpecialAllowance_Null_Value_Check()
        {
            SpecialAllowanceSetting specialAllowanceSetting = _specialAllowanceSettingService.GetSpecialAllowanceSettings(0);
            Assert.Null(specialAllowanceSetting);
        }

        [Fact]
        public void GetSpecialAllowanceSettingCount_Failed_For_Null_UserMenu()
        {
            Assert.Throws<InvalidDataException>(() => _specialAllowanceSettingService.GetSpecialAllowanceSettingCount(null, "", "", null, "", null));
        }
       
        [Fact]
        public void GetSpecialAllowanceSettingCount_Failed_For_Empty_UserMenu()
        {
            Assert.Throws<InvalidDataException>(() => _specialAllowanceSettingService.GetSpecialAllowanceSettingCount(new List<UserMenu>(), "", "", null, "", null));
        }
       

        #endregion
    }
}
