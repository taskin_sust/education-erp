﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using FluentNHibernate.Utils;
using HibernatingRhinos.Profiler.Appender.Messages;
using iTextSharp.text;
using log4net;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Mapping;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Sms;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using Array = System.Array;

namespace UdvashERP.Areas.Exam.Controllers
{
    /// <summary>
    /// Habib
    /// </summary>
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Exam")]
    public class MarksEntryController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("ExamArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly ICourseService _courseService;
        private readonly IExamsService _examsService;
        private readonly IUserService _userService;
        private readonly IStudentProgramService _studentProgram;
        private readonly IExamsStudentMarksService _examsStudentMarksService;
        private readonly IOrganizationService _organizationService;
        private readonly ISmsService _smsService;
        private readonly CommonHelper _commonHelper;
        private List<UserMenu> _userMenu;
        private readonly IBatchService _batchService;
        List<MarksEntryReturnMessage> _returnVal = new List<MarksEntryReturnMessage>();
        List<TemporarySms> _tempListData = new List<TemporarySms>();
        IList<ExamsStudentMarks> _examStudentMarksData = new List<ExamsStudentMarks>();
        private readonly IServiceBlockService _serviceBlockService;
        private ISession currentSession = null;


        public MarksEntryController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            currentSession = nHsession;
            _programService = new ProgramService(nHsession);
            _sessionService = new SessionService(nHsession);
            _courseService = new CourseService(nHsession);
            _examsService = new ExamsService(nHsession);
            _userService = new UserService(nHsession);
            _studentProgram = new StudentProgramService(nHsession);
            _examsStudentMarksService = new ExamsStudentMarksService(nHsession);
            _organizationService = new OrganizationService(nHsession);
            _commonHelper = new CommonHelper();
            _batchService = new BatchService(nHsession);
            _smsService = new SmsService(nHsession);
            _serviceBlockService = new ServiceBlockService(nHsession);
        }

        public List<MarksEntryReturnMessage> ReturnValueList
        {
            get { return _returnVal; }
        }
       
        #endregion

        #region Marks Entry By Xml
        public ActionResult MarksEntryByXml()
        {
            //var serviceType = _commonHelper.GetBlockService();
            //var fromDictionaryValueByKey = 
            //        (from p in serviceType
            //         where p.Key == "Auto Result"
            //         select p.Value)
            //        .FirstOrDefault();
            //var fromDictionaryKeyByValue = 
            //        (from p in serviceType
            //         where p.Value == 1
            //         select p.Key)
            //        .FirstOrDefault();


            //var sp = _studentProgram.LoadStudentProgram("12150900076");
            //if (sp.Any())
            //{
            //    var spObj = sp.FirstOrDefault();
            //    bool isBlocked = _serviceBlockService.IsBlocked(68, 44, (int)BlockService.AutoResult, spObj);
            //}
           


            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //ViewBag.ProgramList = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
            ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.CourseList = new SelectList(string.Empty, "Value", "Text");
            List<SelectListItem> examTypes = new List<SelectListItem>();
            examTypes.Add(new SelectListItem() { Text = "--Select Exam Type--", Value = "" });
            ViewBag.ExamTypeList = examTypes;
            return View();
        }

        public string studentCheck(Exams exam, StudentProgram studentProgram)
        {
            //Need To Check            
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu, null, _commonHelper.ConvertIdToList(studentProgram.Program.Id), _commonHelper.ConvertIdToList(studentProgram.Batch.Branch.Id));
            List<long> programIdList = AuthHelper.LoadProgramIdList(_userMenu, null, _commonHelper.ConvertIdToList(studentProgram.Batch.Branch.Id), _commonHelper.ConvertIdToList(studentProgram.Program.Id));

            var studentBranchId = studentProgram.Batch.Branch.Id;
            var studentProgramId = studentProgram.Program.Id;

            if (!branchIdList.Any() || !programIdList.Any())
            {
                return "Invalid Program Roll:" + studentProgram.PrnNo;
            }
            else
            {
                if (exam.Session.Id != studentProgram.Batch.Session.Id)
                {
                    return "Session Not match :" + studentProgram.PrnNo;
                }
                else if (exam.Program.Id != studentProgram.Program.Id)
                {
                    return "Program Not match :" + studentProgram.PrnNo;
                }
                else
                {
                    return "";
                }
            }
        }

        public ExamStudentCalculatedMarks CalculateMarks(IList<ExamsStudentAnswerScript> examAnswer, ExamsDetails ed, string[] studentAnsArrayFromXml, bool isAcceptPartial)
        {
            float marksGet = 0;
            float nMarks = 0;
            int totalCorrectAnswer = 0; 
            int totalWrongAnswer = 0;  


            if (isAcceptPartial)
            {
                for (int i = 0; i < examAnswer.Count; i++)
                {
                    if (examAnswer[i].QuestionAnswer.Trim().Contains(studentAnsArrayFromXml[i]))
                    {
                        marksGet = marksGet + (float)(ed.MarksPerQuestion);
                        totalCorrectAnswer++;
                    }
                    else
                    {
                        // var sf =  studentAnsArrayFromXml[i].Trim();
                        if (studentAnsArrayFromXml[i].Trim() != "")
                        {
                            nMarks = nMarks + (float)(ed.MarksPerQuestion * (ed.NegativeMark / 100));
                            totalWrongAnswer++;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < examAnswer.Count; i++)
                {
                    if (examAnswer[i].QuestionAnswer == studentAnsArrayFromXml[i])
                    {
                        marksGet = marksGet + (float)(ed.MarksPerQuestion);
                        totalCorrectAnswer++;
                    }
                    else
                    {
                        if (studentAnsArrayFromXml[i].Trim() != "")
                        {
                            nMarks = nMarks + (float)(ed.MarksPerQuestion * (ed.NegativeMark / 100));
                            totalWrongAnswer++;
                        }
                    }
                }
            }
            decimal obtainMarks = (decimal)(marksGet - nMarks);
            if (obtainMarks < 0)
            {
                obtainMarks = 0;
            }
            return new ExamStudentCalculatedMarks() { ObtainMark = obtainMarks, TotalCorrectAnswer = totalCorrectAnswer, TotalWrongAnswer = totalWrongAnswer};
        }
        public List<MarksEntryReturnMessage> UploadStudentMarksNew(MarksEntryViewModel marksEntryViewModel, string answerSheetArray)
        {
            IList<ExamsStudentMarks> studentMarksDetailsList = new List<ExamsStudentMarks>();
            Exams examinformation = _examsService.GetExamById(marksEntryViewModel.ExamId);
            if (examinformation != null)
            {
                if (examinformation.IsMcq && marksEntryViewModel.ExamType == ExamType.Mcq)
                {
                    IList<ExamsDetails> examsDetailses = examinformation.ExamDetails.Where(x => x.ExamType == ExamType.Mcq).OrderByDescending(y => y.IsCompulsary).ToList();
                    var setCodeList = SetCode.LoadSetCodes();
                    var totalQuestion = examsDetailses.Sum(item => item.TotalQuestion);
                    var maxSubject = examinformation.McqMaxSubjectCount;
                    var isAcceptPartial = examinformation.IsAcceptPartialAnswer;

                    #region IsExamDetails Exits
                    if (examsDetailses.Any())
                    {
                        XElement element = XElement.Load(new StreamReader(marksEntryViewModel.FileField.InputStream));
                        var students = element.Descendants("Student");
                        int subjectType = examsDetailses.FirstOrDefault().SubjectType;
                        List<MarksEntryByXmlSms> smsXmlList = new List<MarksEntryByXmlSms>();

                        #region Load Service Block
                        //var serviceType = _commonHelper.GetBlockService()["Auto Result"];
                      //  var serviceBlocks = _serviceBlockService.LoadContentTypeList(marksEntryViewModel.Organization, marksEntryViewModel.Program, marksEntryViewModel.Session, (int)BlockService.AutoResult);
                        #endregion

                        #region Looping Through Xml Student Obj

                        foreach (var student in students)
                        {
                            if (student.Attribute("Roll") != null && student.Attribute("Roll").Value.Trim() != "" &&
                                student.Attribute("ExamCode") != null &&
                                student.Attribute("ExamCode").Value.Trim() != "" &&
                                student.Element("QueAns") != null &&
                                student.Element("QueAns").Element("Answer") != null &&
                                student.Element("QueAns").Element("Answer").Attribute("Set") != null &&
                                student.Element("QueAns").Element("Answer").Attribute("Set").Value.Trim() != "" &&
                                student.Element("QueAns").Element("Answer").Attribute("Ans") != null &&
                                student.Element("QueAns").Element("Answer").Attribute("Ans").Value != ""
                                )
                            {
                                var studentRoll = student.Attribute("Roll").Value;
                                var registrationNo = student.Attribute("RegNo").Value;
                                var examCode = student.Attribute("ExamCode").Value;
                                var ansElement = student.Element("QueAns").Element("Answer");
                                var set = ansElement.Attribute("Set").Value;
                                var setcode = 0;
                                setCodeList.TryGetValue(set, out setcode);
                                var ans = ansElement.Attribute("Ans").Value;
                                if (examCode != examinformation.Code.Trim())
                                {
                                    _returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        mesageType = 2,
                                        message = "Invalid Exam Code For Roll:" + studentRoll,
                                        Prn = studentRoll
                                    });
                                }
                                else
                                {
                                    StudentProgram studentProgram = _studentProgram.GetStudentProgram(studentRoll);
                                    if (studentProgram == null)
                                    {
                                        _returnVal.Add(new MarksEntryReturnMessage
                                        {
                                            mesageType = 2,
                                            message = "Invalid Program Roll:" + studentRoll,
                                            Prn = studentRoll
                                        });
                                    }
                                    else
                                    {
                                        BusinessModel.Entity.Students.Student studentObj = studentProgram.Student;
                                        if (!examinformation.IsTakeAnswerSheetWhenUploadMarks)
                                        {
                                            if (studentObj.RegistrationNo.Trim() != registrationNo.Trim())
                                            {
                                                _returnVal.Add(new MarksEntryReturnMessage
                                                {
                                                    mesageType = 2,
                                                    message = "Invalid Reg. No. for PRN :" + studentProgram.PrnNo,
                                                    Prn = studentProgram.PrnNo
                                                });
                                            }
                                            else
                                            {
                                                String studentCheckReturn = studentCheck(examinformation, studentProgram);
                                                if (studentCheckReturn != "")
                                                {
                                                    _returnVal.Add(new MarksEntryReturnMessage
                                                    {
                                                        mesageType = 2,
                                                        message = studentCheckReturn,
                                                        Prn = studentProgram.PrnNo
                                                    });
                                                }
                                                else
                                                {
                                                    var studentAnsArrayFromXml = ans.Split(new string[] {","},
                                                        StringSplitOptions.None);

                                                    studentAnsArrayFromXml =
                                                        studentAnsArrayFromXml.Skip(0).Take(totalQuestion).ToArray();

                                                    studentMarksDetailsList = GenerateStudentMarkDetailsList(
                                                        examsDetailses, setcode, subjectType,
                                                        studentAnsArrayFromXml, isAcceptPartial, studentProgram,
                                                        maxSubject,
                                                        set, answerSheetArray);

                                                    List<TemporarySms> temporarySmsList =
                                                        GenerateTemporarySmsDataList(studentMarksDetailsList,
                                                            subjectType,
                                                            studentProgram);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (studentProgram.PrnNo.Trim() != studentRoll.Trim())
                                            {
                                                _returnVal.Add(new MarksEntryReturnMessage
                                                {
                                                    mesageType = 2,
                                                    message = "Invalid Reg. No. for PRN :" + studentProgram.PrnNo,
                                                    Prn = studentProgram.PrnNo
                                                });
                                            }
                                            else
                                            {
                                                String studentCheckReturn = studentCheck(examinformation, studentProgram);
                                                if (studentCheckReturn != "")
                                                {
                                                    _returnVal.Add(new MarksEntryReturnMessage
                                                    {
                                                        mesageType = 2,
                                                        message = studentCheckReturn,
                                                        Prn = studentProgram.PrnNo
                                                    });
                                                }
                                                else
                                                {
                                                    var studentAnsArrayFromXml = ans.Split(new string[] { "," },
                                                        StringSplitOptions.None);

                                                    studentAnsArrayFromXml =
                                                        studentAnsArrayFromXml.Skip(0).Take(totalQuestion).ToArray();

                                                    studentMarksDetailsList = GenerateStudentMarkDetailsList(
                                                        examsDetailses, setcode, subjectType,
                                                        studentAnsArrayFromXml, isAcceptPartial, studentProgram,
                                                        maxSubject,
                                                        set, answerSheetArray);

                                                    List<TemporarySms> temporarySmsList =
                                                        GenerateTemporarySmsDataList(studentMarksDetailsList,
                                                            subjectType,
                                                            studentProgram);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _returnVal.Add(new MarksEntryReturnMessage { mesageType = 2, message = "Invalid Xml Tag" });
                            }
                        }
                        _smsService.SaveTemporarySmsData(_tempListData);
                        //});
                        #endregion
                    }
                    #endregion

                    else
                    {
                        _returnVal.Add(new MarksEntryReturnMessage { mesageType = 2, message = "This is not mcq exam" });
                    }
                }
                else
                {
                    _returnVal.Add(new MarksEntryReturnMessage { mesageType = 2, message = "This is not mcq exam" });
                }
            }
            else
            {
                _returnVal.Add(new MarksEntryReturnMessage { mesageType = 2, message = "Exam information not found" });
            }
            return ReturnValueList;
        }

        private List<TemporarySms> GenerateTemporarySmsDataList(IList<ExamsStudentMarks> studentMarksDetailsList, int subjectType, StudentProgram studentProgram)
        {
            #region Looping through studentMarksDetailsList
            try
            {
                if (studentMarksDetailsList.Any())
                {
                    bool isAbleSms = true;
                    try
                    {
                        foreach (var smd in studentMarksDetailsList)
                        {
                            string subjectName = "";
                            subjectName = subjectType == SubjectType.Combined ? "Combined" : smd.ExamsDetails.Subject.Name;
                            ExamsStudentMarks isAlreadyExistMarks =
                                _examsStudentMarksService
                                    .LoadExamsStudentMarksByExamsDetailsStudentProgram(
                                        smd.ExamsDetails.Id, smd.StudentProgram.Id);
                            if (isAlreadyExistMarks != null)
                            {
                                //isAbleSms = false;
                                var tempOldMarks = isAlreadyExistMarks.ObtainMark;
                                isAlreadyExistMarks.ObtainMark = smd.ObtainMark;
                                isAlreadyExistMarks.TotalCorrectAnswer = smd.TotalCorrectAnswer;
                                isAlreadyExistMarks.TotalWrongAnswer = smd.TotalWrongAnswer;
                                isAlreadyExistMarks.TotalAnswered = smd.TotalAnswered;
                                isAlreadyExistMarks.TotalSkip = smd.TotalSkip;
                                bool isUpdate =
                                    _examsStudentMarksService.Update(isAlreadyExistMarks);
                                if (isUpdate)
                                {
                                    if (tempOldMarks == smd.ObtainMark)
                                    {
                                        isAbleSms = false;
                                    }
                                    _returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        mesageType = 1,
                                        message =
                                            "Student Answer Update Success for prnNo: " +
                                            smd.StudentProgram.Student.RegistrationNo + " and exam code: " +
                                            smd.ExamsDetails.Exams.Code + " and subject: " +
                                            subjectName,
                                        Prn = smd.StudentProgram.PrnNo
                                    });
                                }
                                else
                                {
                                    isAbleSms = false;
                                    _returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        mesageType = 2,
                                        message =
                                            "Student Answer Update fail for prnNo: " +
                                            smd.StudentProgram.PrnNo + " and exam code: " +
                                            smd.ExamsDetails.Exams.Code + " and subject: " +
                                            subjectName,
                                        Prn = smd.StudentProgram.PrnNo

                                    });
                                }
                            }
                            else
                            {
                                bool isSave = _examsStudentMarksService.Save(smd);
                                if (isSave)
                                {
                                    _returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        mesageType = 0,
                                        message =
                                            "Student Answer Save Success for prnNo: " +
                                            smd.StudentProgram.PrnNo + " and exam code: " +
                                            smd.ExamsDetails.Exams.Code + " and subject: " +
                                            subjectName,
                                        Prn = smd.StudentProgram.PrnNo
                                    });
                                }
                                else
                                {
                                    isAbleSms = false;
                                    _returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        mesageType = 2,
                                        message =
                                            "Student Answer Save fail for prnNo: " +
                                            smd.StudentProgram.PrnNo + " and exam code: " +
                                            smd.ExamsDetails.Exams.Code + " and subject: " +
                                            subjectName,
                                        Prn = smd.StudentProgram.PrnNo
                                    });
                                }
                            }
                        }
                        if (isAbleSms)
                        {
                            _tempListData.Add(new TemporarySms()
                            {
                                Student = studentProgram.Student,
                                StudentProgram = studentProgram,
                                Exams = studentMarksDetailsList[0].ExamsDetails.Exams,
                                ExamType = ExamType.Mcq,
                                Status = 1
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        _returnVal.Add(new MarksEntryReturnMessage
                        {
                            mesageType = 2,
                            message = WebHelper.CommonErrorMessage + " For Student Program Roll: " + studentProgram.PrnNo,
                            Prn = studentProgram.PrnNo
                        });
                    }
                }
                return _tempListData;
            }
            catch (Exception wx)
            {
                _logger.Error(wx);
                throw;
            }
            
            #endregion
        }

        private bool TemplateSmsGenerateAndSaveToDb(List<TemporarySms> tempSmses)
        {
            var smsSettings = new SmsSettings();
            IList<SmsHistory> smsHistoryList = new List<SmsHistory>();
            IList<TemporarySms> invalidTemoSmsData = new List<TemporarySms>();
            var program = new Program();
            var organization = new Organization();
            try
            {
                program = tempSmses.FirstOrDefault().StudentProgram.Program;
                organization = program.Organization;
                smsSettings = _smsService.LoadSmsSettingsBySmsTypePrgram(6, organization.Id, program.Id);
            }
            catch (Exception ex)
            {
                return false;
            }
            if (smsSettings != null && smsSettings.SmsReceivers.Any() && !string.IsNullOrEmpty(smsSettings.Template))
            {
                try
                {
                    #region Generate SMSHistory List
                    foreach (var xmlsms in tempSmses)
                    {
                        bool isValidMessageType = false;
                        Batch batch = xmlsms.StudentProgram.Batch;
                        bool isBlocked = _serviceBlockService.IsBlocked(
                            batch.Program.Id, batch.Session.Id, (int)BlockService.AutoResult, xmlsms.StudentProgram);
                        if (xmlsms.ExamType == ExamType.Mcq)
                        {
                            if (smsSettings.Template.Contains("[[{{MCQ Marks}}]]"))
                            {
                                isValidMessageType = true;
                            }
                        }
                        else if (xmlsms.ExamType == ExamType.Written)
                        {
                            if (smsSettings.Template.Contains("[[{{Written Marks}}]]"))
                            {
                                isValidMessageType = true;
                            }
                        }
                        decimal mcqFullMarks = 0;
                        decimal writtenFullMarks = 0;
                        if (isValidMessageType && !(isBlocked))
                        {
                            var student = xmlsms.Student;
                            var options = smsSettings.SmsType.DynamicOptions;
                            var message = smsSettings.Template;
                            var mask = smsSettings.MaskName;
                            var smsToBeSend = new List<SmsViewModel>();
                            var mcqExamDetais =
                                xmlsms.Exams.ExamDetails.Where(
                                    x => x.ExamType == ExamType.Mcq && x.Status == ExamsDetails.EntityStatus.Active)
                                    .ToList();
                            var writtenExamDetais =
                                xmlsms.Exams.ExamDetails.Where(
                                    x => x.ExamType == ExamType.Written && x.Status == ExamsDetails.EntityStatus.Active)
                                    .ToList();

                            if (xmlsms.Exams.IsMcq)
                            {
                                if (xmlsms.Exams.McqFullMarks != null)
                                {
                                    mcqFullMarks = xmlsms.Exams.McqFullMarks;
                                }
                            }
                            if (xmlsms.Exams.IsWritten)
                            {
                                if (xmlsms.Exams.WrittenFullMark != null)
                                {
                                    writtenFullMarks = xmlsms.Exams.WrittenFullMark;
                                }
                            }
                            var fullMarks = mcqFullMarks + writtenFullMarks;

                            decimal mcqMarks = 0;
                            decimal writtenMarks = 0;
                            int totalCorrectAnswer = 0;
                            int totalWrongAnswer = 0;
                            int totalAnswered = 0;
                            int totalSkip = 0; 

                            int flag = 0;

                            if (mcqExamDetais.Any())
                            {
                                var strudentMcqMarks =
                                    _examsStudentMarksService.LoadExamsStudentMarksByExamStudentProgram(
                                        xmlsms.Exams.Id,
                                        xmlsms.StudentProgram.Id, ExamType.Mcq);
                                if (strudentMcqMarks.Any())
                                {
                                    mcqMarks = strudentMcqMarks.Sum(x => x.ObtainMark);
                                    totalCorrectAnswer = strudentMcqMarks.Sum(x => x.TotalCorrectAnswer);
                                    totalWrongAnswer = strudentMcqMarks.Sum(x => x.TotalWrongAnswer);
                                    totalAnswered = strudentMcqMarks.Sum(x => x.TotalAnswered);
                                    totalSkip = strudentMcqMarks.Sum(x => x.TotalSkip);

                                    flag = 1;
                                }
                            }
                            if (writtenExamDetais.Any())
                            {
                                var strudentWrittenMarks =
                                    _examsStudentMarksService.LoadExamsStudentMarksByExamStudentProgram(
                                        xmlsms.Exams.Id,
                                        xmlsms.StudentProgram.Id, ExamType.Written);
                                if (strudentWrittenMarks.Any())
                                {
                                    writtenMarks = strudentWrittenMarks.Sum(x => x.ObtainMark);
                                    flag = flag == 1 ? 3 : 2;
                                }
                            }
                            if (options.Any())
                            {
                                foreach (var o in options)
                                {
                                    string optionField = "[[{{" + o.Name + "}}]]";
                                    string optionValue = "";

                                    switch (o.Name)
                                    {
                                        case "Nick Name":
                                            optionValue = student.NickName;
                                            break;
                                        case "Program Roll":
                                            optionValue = xmlsms.StudentProgram.PrnNo;
                                            break;
                                        case "Registration Number":
                                            optionValue = student.RegistrationNo;
                                            break;
                                        case "Exam Name":
                                            optionValue = xmlsms.Exams.Name;
                                            break;
                                        case "MCQ Marks":
                                            if (message.Contains("[[{{MCQ Marks}}]]") &&
                                                message.Contains("[[{{MCQ Full Marks}}]]"))
                                            {
                                                optionField =
                                                    message.Substring(
                                                        message.IndexOf("[[{{MCQ Marks}}]]",
                                                            System.StringComparison.Ordinal),
                                                        ((message.LastIndexOf("[[{{MCQ Full Marks}}]]",
                                                            System.StringComparison.Ordinal) + 22) -
                                                         message.IndexOf("[[{{MCQ Marks}}]]",
                                                             System.StringComparison.Ordinal)));
                                            }
                                            if (flag == 1 || flag == 3)
                                            {
                                                if (message.Contains("[[{{MCQ Marks}}]]") &&
                                                    message.Contains("[[{{MCQ Full Marks}}]]"))
                                                {
                                                    optionValue =
                                                        "MCQ: " + mcqMarks.ToString() +
                                                        optionField.Substring(17, optionField.Length - (22 + 17)) +
                                                        mcqFullMarks.ToString();
                                                }
                                                else
                                                {
                                                    optionValue = "MCQ: " + mcqMarks.ToString();
                                                }
                                            }
                                            else
                                            {
                                                optionValue = "";
                                            }
                                            break;
                                        case "MCQ Full Marks":
                                            if (flag == 1 || flag == 3)
                                                optionValue = mcqFullMarks.ToString();
                                            else optionValue = "";
                                            break;
                                        case "Written Marks":
                                            if (message.Contains("[[{{Written Marks}}]]") &&
                                                message.Contains("[[{{Written Full Marks}}]]"))
                                            {
                                                optionField =
                                                    message.Substring(
                                                        message.IndexOf("[[{{Written Marks}}]]",
                                                            System.StringComparison.Ordinal),
                                                        ((message.LastIndexOf("[[{{Written Full Marks}}]]",
                                                            System.StringComparison.Ordinal) + 26) -
                                                         message.IndexOf("[[{{Written Marks}}]]",
                                                             System.StringComparison.Ordinal)));
                                            }
                                            if (flag == 2 || flag == 3)
                                            {
                                                if (message.Contains("[[{{Written Marks}}]]") &&
                                                    message.Contains("[[{{Written Full Marks}}]]"))
                                                {
                                                    optionValue =
                                                        "Written-" + writtenMarks.ToString() +
                                                        optionField.Substring(21, optionField.Length - (26 + 21)) +
                                                        writtenFullMarks.ToString();
                                                }
                                                else
                                                {
                                                    optionValue = "Written-" + writtenMarks.ToString();
                                                }
                                            }
                                            else optionValue = "";
                                            break;
                                        case "Written Full Marks":
                                            if (flag == 2 || flag == 3)
                                                optionValue = writtenFullMarks.ToString();
                                            else optionValue = "";
                                            break;
                                        case "Total Marks":
                                            if (message.Contains("[[{{Total Marks}}]]") &&
                                                message.Contains("[[{{Full Marks}}]]"))
                                            {
                                                optionField =
                                                    message.Substring(
                                                        message.IndexOf("[[{{Total Marks}}]]",
                                                            System.StringComparison.Ordinal),
                                                        ((message.LastIndexOf("[[{{Full Marks}}]]",
                                                            System.StringComparison.Ordinal) + 18) -
                                                         message.IndexOf("[[{{Total Marks}}]]",
                                                             System.StringComparison.Ordinal)));
                                            }
                                            if (flag == 3)
                                            {
                                                if (message.Contains("[[{{Total Marks}}]]") &&
                                                    message.Contains("[[{{Full Marks}}]]"))
                                                {
                                                    optionValue = "Total-" + (writtenMarks + mcqMarks) +
                                                                  optionField.Substring(19,
                                                                      optionField.Length - (18 + 19)) +
                                                                  fullMarks.ToString();
                                                }
                                                else
                                                {
                                                    optionValue = "Total-" + (writtenMarks + mcqMarks);
                                                }

                                            }
                                            
                                            else optionValue = "";
                                            break;
                                        case "Full Marks":
                                            optionValue = fullMarks.ToString();
                                            break;
                                        case "Total Correct Answer":
                                            if (flag == 1 || flag == 3)
                                            {
                                                optionValue = (totalAnswered + totalSkip)>0 ? totalCorrectAnswer.ToString() : "";
                                            }
                                            else optionValue = "";
                                            break;
                                        case "Total Wrong Answer":
                                            if (flag == 1 || flag == 3)
                                            {
                                                optionValue = (totalAnswered + totalSkip) > 0 ? totalWrongAnswer.ToString() : "";
                                            }
                                            else optionValue = "";
                                            break;
                                        case "Total Answered":
                                            if (flag == 1 || flag == 3)
                                            {
                                                optionValue = (totalAnswered + totalSkip) > 0 ? totalAnswered.ToString() : "";
                                            }
                                            else optionValue = "";
                                            break;
                                        case "Total Skip":
                                           if (flag == 1 || flag == 3)
                                            {
                                                optionValue = (totalAnswered + totalSkip) > 0 ? totalSkip.ToString() : "";
                                            }
                                            else optionValue = "";
                                            break;

                                        default:
                                            optionValue = "";
                                            break;
                                    }
                                    message = message.Replace(optionField, optionValue);
                                }
                            }
                            message = message.Trim();
                            foreach (var sr in smsSettings.SmsReceivers)
                            {
                                var svm = new SmsViewModel();
                                switch (sr.Name)
                                {
                                    case "Mobile Number (Personal)":
                                        string studentMobile = CheckMobileNumber(student.Mobile);
                                        if (studentMobile != null)
                                        {
                                            svm.SmsReceiverId = sr.Id;
                                            svm.ReceiverNumber = "88" + studentMobile;
                                        }
                                        break;
                                    case "Mobile Number (Father)":
                                        string guardiansMobile1 = CheckMobileNumber(student.GuardiansMobile1);
                                        if (guardiansMobile1 != null)
                                        {
                                            svm.SmsReceiverId = sr.Id;
                                            svm.ReceiverNumber = "88" + guardiansMobile1;
                                        }
                                        break;
                                    case "Mobile Number (Mother)":
                                        string guardiansMobile2 = CheckMobileNumber(student.GuardiansMobile2);
                                        if (guardiansMobile2 != null)
                                        {
                                            svm.SmsReceiverId = sr.Id;
                                            svm.ReceiverNumber = "88" + guardiansMobile2;
                                        }
                                        break;
                                    case "Mobile Number (Campus Incharge)":
                                        string contactNumber =
                                            CheckMobileNumber(xmlsms.StudentProgram.Batch.Campus.ContactNumber);
                                        if (CheckMobileNumber(xmlsms.StudentProgram.Batch.Campus.ContactNumber) != null)
                                        {
                                            svm.SmsReceiverId = sr.Id;
                                            svm.ReceiverNumber = "88" + contactNumber;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                                {
                                    var alreadyExist =
                                        smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                                    if (!alreadyExist.Any())
                                    {
                                        smsToBeSend.Add(svm);
                                    }
                                }
                            }
                            if (smsToBeSend.Any())
                            {
                                if (String.IsNullOrEmpty(mask))
                                {
                                    mask = "";
                                }
                                foreach (var stbs in smsToBeSend)
                                {
                                    var smsHistory = new SmsHistory();
                                    var currentNumber = stbs;
                                    smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                                    smsHistory.SmsReceiver =
                                        smsSettings.SmsReceivers.FirstOrDefault(x => x.Id == currentNumber.SmsReceiverId);
                                    smsHistory.Sms = message;
                                    smsHistory.Program = xmlsms.StudentProgram.Program;
                                    smsHistory.Organization = xmlsms.StudentProgram.Program.Organization;
                                    smsHistory.SmsSettings = smsSettings;
                                    smsHistory.Student = student;
                                    smsHistory.Mask = mask;
                                    smsHistory.Type = 6; //for result
                                    smsHistory.Status = SmsHistory.EntityStatus.Pending;
                                    smsHistory.CreateBy = xmlsms.CreateBy;
                                    smsHistory.ModifyBy = xmlsms.ModifyBy;
                                    smsHistory.Priority = 5;
                                    smsHistoryList.Add(smsHistory);

                                }
                            }
                        }
                        else
                        {
                            invalidTemoSmsData.Add(xmlsms);
                        }
                    }

                    bool isSucess = _smsService.SaveSmsHistoryListAndDeleteTemporarySmsData(smsHistoryList, tempSmses, invalidTemoSmsData);
                    #endregion

                    return true;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return false;
                }
            }
            else
            {
                bool isSucess = _smsService.SaveSmsHistoryListAndDeleteTemporarySmsData(smsHistoryList, tempSmses, invalidTemoSmsData);
            }
            return false;
        }
        [AllowAnonymous]
        public void GenerateSmsHistory()
        {
            if (System.Web.HttpContext.Current.Application["RunningShcedule"] == null)
            {
                System.Web.HttpContext.Current.Application["RunningShcedule"] = "Exam/MarksEntry/GenerateSmsHistory(" + DateTime.Now + ")";
                try
                {
                    var sendSmsObj = new SendSmsNewApi();
                    List<TemporarySms> temporarySmses = _smsService.LoadAllPendingTempSms(500);
                    List<TemporarySms> temporarySmsGroupList = new List<TemporarySms>();
                    var organization = new Organization();
                    var program = new Program();
                    if (temporarySmses.Any())
                    {
                        var temporarySmsGroup = temporarySmses.GroupBy(m => m.StudentProgram.Program.Organization,
                            (key, group) => new { Key = key, Group = group.ToList() });
                        foreach (var tsg in temporarySmsGroup)
                        {
                            organization = tsg.Key;
                            var programWizeGroup = tsg.Group.GroupBy(m => m.StudentProgram.Program,
                                (key, group) => new { Key = key, Group = group.ToList() });

                            foreach (var pwg in programWizeGroup)
                            {
                                program = pwg.Key;
                                temporarySmsGroupList = pwg.Group; 
                                TemplateSmsGenerateAndSaveToDb(temporarySmsGroupList);
                                //break;
                            }
                            //break;
                        }
                    }
                    //  sendSmsObj.TemplateSmsGenerateAndSaveToDb(temporarySmsGroupList);
                    //TemplateSmsGenerateAndSaveToDb(temporarySmsGroupList);
                    System.Web.HttpContext.Current.Application.Remove("RunningShcedule");
                }
                catch (Exception wx)
                {
                    _logger.Error(wx);
                    System.Web.HttpContext.Current.Application.Remove("RunningShcedule");
                    throw;
                }
            }
            else
            {
                var running = "Another Schedule is running.";
                try
                {
                    running = ", Schedule name: " + (string)System.Web.HttpContext.Current.Application["RunningShcedule"];
                }
                catch (Exception ex)
                {
                    _logger.Error("RunningShcedule Exception");
                }

                _logger.Error(running);
            }
        }
       
        private IList<ExamsStudentMarks> GenerateStudentMarkDetailsList(IList<ExamsDetails> examsDetailses, int setcode, int subjectType, string[] studentAnsArrayFromXml,
        bool isAcceptPartial, StudentProgram studentProgram, int maxSubject, string set, string answerSheetArray)
        {
            #region Inner loop through student examdetails

            try
            {
                IList<ExamsStudentMarks> studentMarksDetailsList = new List<ExamsStudentMarks>();
              
                foreach (var ed in examsDetailses)
                {

                    var examModel = _examsService.LoadById(ed.Exams.Id);
                    var existAnswer = new List<ExamsStudentAnswerScript>();
                    if (examModel.IsTakeAnswerSheetWhenUploadMarks)
                    {
                        
                        var ansList = new List<ExamsStudentAnswerScript>();

                        string str = answerSheetArray;
                        string mainStr = str.Remove(str.Length-1);
                        var strArrays = mainStr.Split('/');
                        foreach (var s in strArrays)
                        {
                            var rmv = s.Remove(1);
                            var strs = "";
                            strs = rmv == "," ? s.Remove(0, 1) : s;
                            var strArray = strs.Split(',');
                            var i = 0;
                            var setCodeId = strArray[0];
                            foreach (var s1 in strArray.Skip(1).ToList())
                            {
                                var ans = new ExamsStudentAnswerScript
                                {
                                    SetCode = Convert.ToInt32(setCodeId),
                                    QuestionNo = i + 1,
                                    QuestionAnswer = s1
                                };
                                i++;
                                ansList.Add(ans);
                            }
                        }
                        existAnswer = ansList.Where(x => x.SetCode == setcode).ToList();
                    }
                    else
                    {
                         existAnswer = ed.ExamsStudentAnswerScripts.Where(x => x.SetCode == setcode).ToList();
                    }

                    
                    string subjectName = "";
                    subjectName = subjectType == SubjectType.Combined ? "Combined" : ed.Subject.Name;

                    if (existAnswer.Any())
                    {
                        if (existAnswer.Count == ed.TotalQuestion)
                        {
                            if (subjectType == SubjectType.Single ||subjectType == SubjectType.Combined)
                            {
                                var examsStudentMarks = new ExamsStudentMarks();
                                ExamStudentCalculatedMarks examStudentCalculatedMarks = CalculateMarks(existAnswer, ed, studentAnsArrayFromXml, isAcceptPartial);
                                examsStudentMarks.ExamsDetails = ed;
                                examsStudentMarks.StudentProgram = studentProgram;
                                examsStudentMarks.ObtainMark = examStudentCalculatedMarks.ObtainMark;
                                examsStudentMarks.TotalCorrectAnswer = examStudentCalculatedMarks.TotalCorrectAnswer;
                                examsStudentMarks.TotalWrongAnswer = examStudentCalculatedMarks.TotalWrongAnswer;
                                examsStudentMarks.TotalAnswered = examStudentCalculatedMarks.TotalCorrectAnswer +
                                                                  examStudentCalculatedMarks.TotalWrongAnswer;
                                examsStudentMarks.TotalSkip = ed.TotalQuestion -
                                                              (examStudentCalculatedMarks.TotalCorrectAnswer +
                                                               examStudentCalculatedMarks.TotalWrongAnswer);
                                examsStudentMarks.SetCode = setcode;
                                studentMarksDetailsList.Add(examsStudentMarks);
                            }
                            else
                            {
                                var examsStudentMarks = new ExamsStudentMarks();
                                int qSrart = ed.QuestionFrom;
                                int qEnd = ed.QuestionTo;
                                int qTotal = ed.TotalQuestion;
                                var studentAnsArrayFromXmlMarged = studentAnsArrayFromXml.Skip(qSrart - 1).Take(qTotal).ToArray();
                                int blankAns = studentAnsArrayFromXmlMarged.Count(i => i != " ");
                                if (ed.IsCompulsary || blankAns > 0)
                                {
                                    ExamStudentCalculatedMarks examStudentCalculatedMarks = CalculateMarks(
                                        existAnswer, ed,
                                        studentAnsArrayFromXmlMarged,
                                        isAcceptPartial);
                                    examsStudentMarks.ExamsDetails = ed;
                                    examsStudentMarks.StudentProgram =
                                        studentProgram;
                                    examsStudentMarks.ObtainMark = examStudentCalculatedMarks.ObtainMark;
                                    examsStudentMarks.TotalCorrectAnswer = examStudentCalculatedMarks.TotalCorrectAnswer;
                                    examsStudentMarks.TotalWrongAnswer = examStudentCalculatedMarks.TotalWrongAnswer;
                                    examsStudentMarks.TotalAnswered = examStudentCalculatedMarks.TotalCorrectAnswer +
                                                                      examStudentCalculatedMarks.TotalWrongAnswer;
                                    examsStudentMarks.TotalSkip = ed.TotalQuestion -
                                                                  (examStudentCalculatedMarks.TotalCorrectAnswer +
                                                                   examStudentCalculatedMarks.TotalWrongAnswer);
                                    examsStudentMarks.SetCode = setcode;
                                    studentMarksDetailsList.Add(examsStudentMarks);
                                }
                            }
                        }
                    }
                    else
                    {
                        _returnVal.Add(new MarksEntryReturnMessage
                        {
                            mesageType = 2,
                            message =
                                "Answer Does not found in database for exam: " +
                                ed.Exams.Code + " and stubject: " + subjectName +
                                " and Set: " + set, //set Invalid fix later 
                            Prn = studentProgram.PrnNo

                        });
                        //break;
                    }
                }

                if (studentMarksDetailsList.Count > maxSubject)
                {
                    studentMarksDetailsList = new List<ExamsStudentMarks>();
                    _returnVal.Add(new MarksEntryReturnMessage
                    {
                        mesageType = 2,
                        message = "Disqualify Program Roll:" + studentProgram.PrnNo + " Exam Code: " + examsDetailses[0].Exams.Code,
                        Prn = studentProgram.PrnNo
                    });
                }
                

                return studentMarksDetailsList;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
            #endregion
        }

        private void SaveXMLFile(HttpPostedFileBase xmlFile)
        {
            try
            {
                var loginUser =
                    _userService.LoadAspNetUserById(
                        Convert.ToInt64(IdentityExtensions.GetUserId(System.Web.HttpContext.Current.User.Identity)));
                var actualPath = FileUpload.XmlFileValidationCheck(loginUser.FullName, xmlFile);
            }
            catch (Exception ex)
            {
                _logger.Error("Xml File Error On Saving");
                _logger.Error(ex);
            }

        }

        [HttpPost]
        public ActionResult MarksEntryNewByXml()
        {
            try
            {
                var xmlFile = Request.Files[0];
                var formData = Request.Form;
                var marksEntryViewModel = new MarksEntryViewModel();
                marksEntryViewModel.Organization = Convert.ToInt64(formData["Organization"]);
                marksEntryViewModel.Program = Convert.ToInt64(formData["Program"]);
                marksEntryViewModel.Session = Convert.ToInt64(formData["Session"]);
                marksEntryViewModel.Course = Convert.ToInt64(formData["Course"]);
                marksEntryViewModel.ExamType = Convert.ToInt32(formData["ExamType"]);
                marksEntryViewModel.ExamCode = formData["ExamCode"];
                marksEntryViewModel.ExamId = Convert.ToInt64(formData["ExamId"]);
                marksEntryViewModel.FileField = xmlFile;
                string allAnswerSheet = formData["allAnswerSheet"];
                if (allAnswerSheet == "")
                {
                    return Json(new Response(false,"Answer Missing"));
                }
                //var answerSheetArray = allAnswerSheet.Split('/');
                
                var examTypes = new List<SelectListItem>();
                if (marksEntryViewModel.ExamId != 0)
                {
                    Exams exam = _examsService.GetExamById(marksEntryViewModel.ExamId);
                    if (exam != null)
                    {
                        if (exam.IsMcq == true && exam.IsWritten == true)
                        {
                            examTypes.Add(new SelectListItem() { Text = "MCQ", Value = ExamType.Mcq.ToString() });
                        }
                        else if (exam.IsMcq == true && exam.IsWritten == false)
                        {
                            examTypes.Add(new SelectListItem() { Text = "MCQ", Value = ExamType.Mcq.ToString() });
                        }
                        else if (exam.IsMcq == false && exam.IsWritten == true)
                        {
                            examTypes.Add(new SelectListItem() { Text = "-- No MCQ Exam for This--", Value = "" });
                        }
                    }
                    else
                    {
                        examTypes.Add(new SelectListItem() { Text = "--Select Exam Type--", Value = "" });
                    }
                }
                else
                {
                    examTypes.Add(new SelectListItem() { Text = "--Select Exam Type--", Value = "" });
                }
                ViewBag.ExamTypeList = examTypes;
                if (ModelState.IsValid)
                {

                    if (marksEntryViewModel.FileField.ContentLength > 0 && marksEntryViewModel.FileField.ContentType.ToLower() == "text/xml")
                    {
                        try
                        {
                            // XElement element = XElement.Load(new StreamReader(xmlFile.InputStream));
                            List<MarksEntryReturnMessage> returnVal = UploadStudentMarksNew(marksEntryViewModel, allAnswerSheet);

                            var returnMessageSuccessList = returnVal.Where(x => x.mesageType == 0).ToList();
                            //ViewBag.ReturnMessageSuccess = returnMessageSuccessList.Select(x => x.Prn).Distinct().ToList().Count;
                            var returnMessageSuccess = returnMessageSuccessList.Select(x => x.Prn).Distinct().ToList().Count;
                            var returnMessageSuccessUpdateList = returnVal.Where(x => x.mesageType == 1).ToList();
                            //ViewBag.ReturnMessageSuccessUpdate = returnMessageSuccessUpdateList.Select(x => x.Prn).Distinct().ToList().Count;
                            var returnMessageSuccessUpdate = returnMessageSuccessUpdateList.Select(x => x.Prn).Distinct().ToList().Count;
                            //ViewBag.SuccessPrnListUpdate = string.Join(", ", returnMessageSuccessUpdateList.Select(x => x.Prn).Distinct().ToList());
                            var successPrnListUpdate = string.Join(", ", returnMessageSuccessUpdateList.Select(x => x.Prn).Distinct().ToList());
                            var returnMessageFailList = returnVal.Where(x => x.mesageType == 2).ToList();
                            //ViewBag.ReturnMessageFail = returnMessageFailList;
                            var returnMessageFail = returnMessageFailList;
                            string inValidRolls = "";
                            string inValidRegNo = "";
                            string invalidExamCode = "";
                            string rmfMessase = "";
                            string invalidSession = "";
                            string invalidAllMsg = "";
                            string answerDoesnotMatch = "";
                            foreach (var rmf in returnMessageFail)
                            {
                                String m = rmf.message;
                                if (m.Contains("Invalid Program Roll") || m.Contains("Program Not match"))
                                {
                                    string[] rolls = m.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                                    inValidRolls = inValidRolls + rolls[1] + ", ";
                                }
                                else if (m.Contains("Invalid Reg. No. for PRN"))
                                {
                                    string[] rolls = m.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                                    inValidRegNo = inValidRegNo + rolls[1] + ", ";
                                }
                                else if (m.Contains("Invalid Exam Code For Roll"))
                                {
                                    string[] rolls = m.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                                    invalidExamCode = invalidExamCode + rolls[1] + ", ";
                                }
                                else if (m.Contains("Session Not match"))
                                {
                                    string[] rolls = m.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                                    invalidSession = invalidSession + rolls[1] + ", ";
                                }
                                else if (m.Contains("Answer Does not found in database for exam"))
                                {
                                    //string[] rolls = m.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                                    answerDoesnotMatch = answerDoesnotMatch + m + ", ";
                                }
                                else
                                {
                                    rmfMessase = m;
                                }
                            }
                            if (inValidRolls != "")
                            {
                                invalidAllMsg += " Invalid Program Roll:" + inValidRolls;
                            }
                            if (inValidRegNo != "")
                            {
                                invalidAllMsg += " Invalid Reg. No. for PRN :" + inValidRegNo;
                            }
                            if (invalidExamCode != "")
                            {
                                invalidAllMsg += " Invalid Exam Code For Roll :" + invalidExamCode;
                            }
                            if (invalidSession != "")
                            {
                                invalidAllMsg += " Invalid Session For Roll :" + invalidSession;
                            }
                            if (answerDoesnotMatch != "")
                            {
                                invalidAllMsg += " " + answerDoesnotMatch;
                            }

                            return Json(new
                            {
                                isSuccess = true, 
                                returnMessageSuccess = returnMessageSuccess, 
                                returnMessageSuccessUpdate = returnMessageSuccessUpdate,
                                successPrnListUpdate = successPrnListUpdate,
                                returnMessageFail = returnMessageFail,
                                invalidAllMsg = invalidAllMsg,
                                rmfMessase = rmfMessase,
                               
                            });

                        }
                        catch (Exception ex)
                        {
                            _logger.Error("Inner Xml Error");
                            _logger.Error(ex);
                            //ViewBag.ErrorMessage = " Invalid Xml File";
                            SaveXMLFile(marksEntryViewModel.FileField);
                            return Json(new Response(false, " Invalid Xml File"));
                        }
                    }
                    else
                    {
                        return Json(new Response(false, " Invalid Xml File"));
                       // ViewBag.ErrorMessage = " Invalid Xml File";
                    }
                    //return View(marksEntryViewModel);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Outer Xml Error");
                _logger.Error(ex);
                //ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }

        
        [HttpPost]
        public ActionResult MarksEntryByXml(MarksEntryViewModel marksEntryViewModel)//, List<String> allAnswerSheet
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                if (marksEntryViewModel.Organization != 0)
                {
                    ViewBag.ProgramList =
                         new SelectList(_programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(marksEntryViewModel.Organization)), "Id", "Name").ToList();
                }
                else
                {
                    ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
                }

                if (marksEntryViewModel.Program != 0)
                {
                    ViewBag.SessionList = new SelectList(_sessionService.LoadAuthorizedSession(_userMenu, null, _commonHelper.ConvertIdToList(marksEntryViewModel.Program)),
                            "Id", "Name");
                }
                else
                {
                    ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
                }

                if (marksEntryViewModel.Session != 0 && marksEntryViewModel.Program != 0)
                {
                    ViewBag.CourseList =
                        new SelectList(_courseService.LoadCourse(marksEntryViewModel.Program, marksEntryViewModel.Session),
                            "Id", "Name");
                }
                else
                {
                    ViewBag.CourseList = new SelectList(string.Empty, "Value", "Text");
                }

                List<SelectListItem> examTypes = new List<SelectListItem>();
                if (marksEntryViewModel.ExamId != 0)
                {
                    Exams exam = _examsService.GetExamById(marksEntryViewModel.ExamId);
                    if (exam != null)
                    {
                        if (exam.IsMcq == true && exam.IsWritten == true)
                        {
                            examTypes.Add(new SelectListItem() { Text = "MCQ", Value = ExamType.Mcq.ToString() });
                        }
                        else if (exam.IsMcq == true && exam.IsWritten == false)
                        {
                            examTypes.Add(new SelectListItem() { Text = "MCQ", Value = ExamType.Mcq.ToString() });
                        }
                        else if (exam.IsMcq == false && exam.IsWritten == true)
                        {
                            examTypes.Add(new SelectListItem() { Text = "-- No MCQ Exam for This--", Value = "" });
                        }
                    }
                    else
                    {
                        examTypes.Add(new SelectListItem() { Text = "--Select Exam Type--", Value = "" });
                    }
                }
                else
                {
                    examTypes.Add(new SelectListItem() { Text = "--Select Exam Type--", Value = "" });
                }
                ViewBag.ExamTypeList = examTypes;
                if (ModelState.IsValid)
                {

                    if (marksEntryViewModel.FileField.ContentLength > 0 &&
                        marksEntryViewModel.FileField.ContentType.ToLower() == "text/xml")
                    {
                        try
                        {
                            // XElement element = XElement.Load(new StreamReader(xmlFile.InputStream));
                            List<MarksEntryReturnMessage> returnVal = UploadStudentMarksNew(marksEntryViewModel,null);

                            var returnMessageSuccessList = returnVal.Where(x => x.mesageType == 0).ToList();
                            ViewBag.ReturnMessageSuccess = returnMessageSuccessList.Select(x => x.Prn).Distinct().ToList().Count;
                            var returnMessageSuccessUpdateList = returnVal.Where(x => x.mesageType == 1).ToList();
                            ViewBag.ReturnMessageSuccessUpdate = returnMessageSuccessUpdateList.Select(x => x.Prn).Distinct().ToList().Count;
                            ViewBag.SuccessPrnListUpdate = string.Join(", ", returnMessageSuccessUpdateList.Select(x => x.Prn).Distinct().ToList());
                            var returnMessageFailList = returnVal.Where(x => x.mesageType == 2).ToList();
                            ViewBag.ReturnMessageFail = returnMessageFailList;
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("Inner Xml Error");
                            _logger.Error(ex);
                            ViewBag.ErrorMessage = " Invalid Xml File";
                            SaveXMLFile(marksEntryViewModel.FileField);
                        }
                    }
                    else
                    {

                        ViewBag.ErrorMessage = " Invalid Xml File";
                    }
                    return View(marksEntryViewModel);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Outer Xml Error");
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View(marksEntryViewModel);
        }
       
        #endregion

        #region Manual Marks Entry
        public ActionResult ManualMarksEntry()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
            ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.CourseList = new SelectList(string.Empty, "Value", "Text");

            List<SelectListItem> examTypes = new List<SelectListItem>();
            examTypes.Add(new SelectListItem() { Text = "--Select Exam Type--", Value = "" });
            ViewBag.ExamTypeList = examTypes;

            return View();
        }
        [HttpPost]
        public ActionResult AjaxRequestForSubmitStudentMarks(MarksEntryViewModel data)
        {
            if (data != null && data.StudentMarksList.Any())
            {
                Exams examinformation = _examsService.GetExamById(data.ExamId);
                if (examinformation != null)
                {
                    bool isExamTypeValid = false;
                    int maxSubject = 0;
                    if (examinformation.IsMcq && data.ExamType == ExamType.Mcq)
                    {
                        isExamTypeValid = true;
                        maxSubject = examinformation.McqMaxSubjectCount;
                    }
                    else if (examinformation.IsWritten && data.ExamType == ExamType.Written)
                    {
                        isExamTypeValid = true;
                        maxSubject = examinformation.WrittenMaxSubjectCount;
                    }
                    else
                    {
                        isExamTypeValid = false;
                    }
                    IList<ExamsDetails> examsDetailses =
                           examinformation.ExamDetails.Where(x => x.ExamType == data.ExamType).OrderByDescending(y => y.IsCompulsary).ToList();

                    Dictionary<string, int> setCodeList = SetCode.LoadSetCodes();
                    if (examinformation.Program.Id == data.Program && examinformation.Session.Id == data.Session && examinformation.Course.Id == data.Course && isExamTypeValid && examsDetailses.Any())
                    {
                        List<MarksEntryReturnMessage> returnVal = new List<MarksEntryReturnMessage>();
                        int subjectType = examsDetailses.FirstOrDefault().SubjectType;



                        foreach (var student in data.StudentMarksList)
                        {
                            StudentProgram studentProgram = _studentProgram.GetStudentProgram(student.PrnNo);
                            if (studentProgram != null)
                            {
                                String studentCheckReturn = studentCheck(examinformation, studentProgram);
                                if (studentCheckReturn == "")
                                {
                                    var markList = student.MarksList.Where(x => x.Marks != null).ToList();
                                    IList<ExamsStudentMarks> studentMarksList =
                                                                _examsStudentMarksService.LoadExamsStudentMarksByExamStudentProgram(Convert.ToInt64(examinformation.Id), studentProgram.Id, data.ExamType);

                                    if (studentMarksList.Any())
                                    {
                                        returnVal.Add(new MarksEntryReturnMessage
                                        {
                                            Sl = Convert.ToInt32(student.Sl),
                                            mesageType = 1,
                                            message =
                                                "Marks Already Exist for Program Roll:" + student.PrnNo
                                        });
                                    }
                                    else
                                    {
                                        if (markList.Any() && markList.Count <= maxSubject)
                                        {
                                            int setcode = 0;
                                            setCodeList.TryGetValue(student.SetCode, out setcode);
                                            if (setcode != 0)
                                            {
                                                bool isAbleSms = true;
                                                foreach (var m in markList)
                                                {
                                                    var ed = examsDetailses.FirstOrDefault(x => x.Id == Convert.ToInt64(m.ExamDetailsId));
                                                    decimal maxMarks = 0;
                                                    if (ed.ExamType == ExamType.Mcq)
                                                    {
                                                        maxMarks = ed.TotalQuestion * ed.MarksPerQuestion;
                                                    }
                                                    else
                                                    {
                                                        maxMarks = ed.WrittenFullMark;
                                                    }
                                                    string subjectName = "";
                                                    if (subjectType == SubjectType.Combined)
                                                    {
                                                        subjectName = "Combined";
                                                    }
                                                    else
                                                    {
                                                        subjectName = ed.Subject.Name;
                                                    }
                                                    decimal marks = 0;
                                                    marks = Convert.ToDecimal(m.Marks);
                                                    if (marks <= maxMarks)
                                                    {
                                                        ExamsStudentMarks examsStudentMarks = new ExamsStudentMarks();
                                                        examsStudentMarks.ExamsDetails = ed;
                                                        examsStudentMarks.StudentProgram = studentProgram;
                                                        examsStudentMarks.ObtainMark = marks;
                                                        examsStudentMarks.SetCode = setcode;
                                                        bool isSave = _examsStudentMarksService.Save(examsStudentMarks);
                                                        if (isSave)
                                                        {
                                                            returnVal.Add(new MarksEntryReturnMessage
                                                            {
                                                                Sl = Convert.ToInt32(student.Sl),
                                                                mesageType = 0,
                                                                message = "Student Answer Save Success for prnNo: " + student.PrnNo + " and exam code: " + examinformation.Code + " and subject: " + subjectName
                                                            });
                                                        }
                                                        else
                                                        {
                                                            isAbleSms = false;
                                                            returnVal.Add(new MarksEntryReturnMessage
                                                            {
                                                                Sl = Convert.ToInt32(student.Sl),
                                                                mesageType = 3,
                                                                message = "Student Answer Save fail for prnNo: " + student.PrnNo + " and exam code: " + examinformation.Code + " and subject: " + subjectName
                                                            });
                                                        }
                                                    }
                                                    else
                                                    {
                                                        isAbleSms = false;
                                                        returnVal.Add(new MarksEntryReturnMessage
                                                        {
                                                            Sl = Convert.ToInt32(student.Sl),
                                                            mesageType = 2,
                                                            message = "Invalid Marks for subject :" + subjectName + " of Program Roll:" + student.PrnNo
                                                        });
                                                        //break;
                                                    }
                                                }
                                                if (isAbleSms)
                                                {
                                                    bool flagForSms = SendSmsApi.GenerateInstantResultSmsAndNumber(6, studentProgram, examinformation, data.ExamType);

                                                    // _logger.Info("Sms send success = " + flagForSms);
                                                }


                                            }
                                            else
                                            {
                                                returnVal.Add(new MarksEntryReturnMessage
                                                {
                                                    Sl = Convert.ToInt32(student.Sl),
                                                    mesageType = 2,
                                                    message = "Invalid Set Code:" + student.SetCode + " of Program Roll:" + student.PrnNo
                                                });
                                            }
                                        }
                                        else
                                        {
                                            returnVal.Add(new MarksEntryReturnMessage
                                            {
                                                Sl = Convert.ToInt32(student.Sl),
                                                mesageType = 2,
                                                message = "Invalid Marks List For Program Roll:" + student.PrnNo
                                            });
                                        }
                                    }

                                    #region Old Code---Check exam marks exist by examdetails Id/Please dont remove
                                    //if (markList.Any() && markList.Count <= maxSubject)
                                    //{
                                    //    int setcode = 0;
                                    //    setCodeList.TryGetValue(student.SetCode, out setcode);
                                    //    if (setcode != 0)
                                    //    {
                                    //        foreach (var m in markList)
                                    //        {
                                    //            var ed = examsDetailses.FirstOrDefault(x => x.Id == Convert.ToInt64(m.ExamDetailsId));
                                    //            decimal maxMarks = 0;
                                    //            if (ed.ExamType == ExamType.Mcq)
                                    //            {
                                    //                maxMarks = ed.TotalQuestion * ed.MarksPerQuestion;
                                    //            }
                                    //            else
                                    //            {
                                    //                maxMarks = ed.WrittenFullMark;
                                    //            }
                                    //            string subjectName = "";
                                    //            if (subjectType == SubjectType.Combined)
                                    //            {
                                    //                subjectName = "Combined";
                                    //            }
                                    //            else
                                    //            {
                                    //                subjectName = ed.Subject.Name;
                                    //            }
                                    //            decimal marks = 0;
                                    //            marks = Convert.ToDecimal(m.Marks);
                                    //            if (marks <= maxMarks)
                                    //            {
                                    //                IList<ExamsStudentMarks> studentmarksList =
                                    //                            _examsStudentMarksService.LoadExamsStudentMarksByExamStudentProgram(Convert.ToInt64(examinformation.Id), studentProgram.Id, data.ExamType);
                                    //                var alreadyExistSubject = 0;
                                    //                bool isAlreadyExistMarks = false;

                                    //                if (studentmarksList.Any())
                                    //                {
                                    //                    alreadyExistSubject = studentmarksList.Count;
                                    //                    ExamsStudentMarks iae = studentmarksList.FirstOrDefault(x => x.ExamsDetails.Id == Convert.ToInt64(m.ExamDetailsId));
                                    //                    if (iae != null)
                                    //                    {
                                    //                        isAlreadyExistMarks = true;
                                    //                    }
                                    //                }

                                    //                if (alreadyExistSubject < maxSubject)//Check for maximum subject 
                                    //                {
                                    //                    if (isAlreadyExistMarks) //check marks already entry for specific exam details Id-Need Optimize
                                    //                    {
                                    //                        returnVal.Add(new MarksEntryReturnMessage
                                    //                        {
                                    //                            Sl = Convert.ToInt32(student.Sl),
                                    //                            mesageType = 1,
                                    //                            message =
                                    //                                "Marks Already Exist for subject :" + subjectName +
                                    //                                " of Program Roll:" + student.PrnNo
                                    //                        });
                                    //                    }
                                    //                    else
                                    //                    {
                                    //                        ExamsStudentMarks examsStudentMarks = new ExamsStudentMarks();
                                    //                        examsStudentMarks.ExamsDetails = ed;
                                    //                        examsStudentMarks.StudentProgram = studentProgram;
                                    //                        examsStudentMarks.ObtainMark = marks;
                                    //                        examsStudentMarks.SetCode = setcode;
                                    //                        bool isSave = _examsStudentMarksService.Save(examsStudentMarks);
                                    //                        if (isSave)
                                    //                        {
                                    //                            returnVal.Add(new MarksEntryReturnMessage
                                    //                            {
                                    //                                Sl = Convert.ToInt32(student.Sl),
                                    //                                mesageType = 0,
                                    //                                message = "Student Answer Save Success for prnNo: " + student.PrnNo + " and exam code: " + examinformation.Code + " and subject: " + subjectName
                                    //                            });
                                    //                        }
                                    //                        else
                                    //                        {
                                    //                            returnVal.Add(new MarksEntryReturnMessage
                                    //                            {
                                    //                                Sl = Convert.ToInt32(student.Sl),
                                    //                                mesageType = 3,
                                    //                                message = "Student Answer Save fail for prnNo: " + student.PrnNo + " and exam code: " + examinformation.Code + " and subject: " + subjectName
                                    //                            });
                                    //                        }
                                    //                    }
                                    //                }
                                    //                else
                                    //                {
                                    //                    returnVal.Add(new MarksEntryReturnMessage
                                    //                    {
                                    //                        Sl = Convert.ToInt32(student.Sl),
                                    //                        mesageType = 2,
                                    //                        message = "Max subject limit exceeded of Program Roll:" + student.PrnNo
                                    //                    });
                                    //                    break;
                                    //                }
                                    //            }
                                    //            else
                                    //            {
                                    //                returnVal.Add(new MarksEntryReturnMessage
                                    //                {
                                    //                    Sl = Convert.ToInt32(student.Sl),
                                    //                    mesageType = 2,
                                    //                    message = "Invalid Marks for subject :" + subjectName + " of Program Roll:" + student.PrnNo
                                    //                });
                                    //                //break;
                                    //            }

                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        returnVal.Add(new MarksEntryReturnMessage
                                    //        {
                                    //            Sl = Convert.ToInt32(student.Sl),
                                    //            mesageType = 2,
                                    //            message = "Invalid Set Code:" + student.SetCode + " of Program Roll:" + student.PrnNo
                                    //        });
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    returnVal.Add(new MarksEntryReturnMessage
                                    //    {
                                    //        Sl = Convert.ToInt32(student.Sl),
                                    //        mesageType = 2,
                                    //        message = "Invalid Marks List For Program Roll:" + student.PrnNo
                                    //    });
                                    //} 
                                    #endregion

                                }
                                else
                                {
                                    returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        Sl = Convert.ToInt32(student.Sl),
                                        mesageType = 2,
                                        message = "Invalid Program Roll:" + student.PrnNo
                                    });
                                }
                            }
                            else
                            {
                                returnVal.Add(new MarksEntryReturnMessage
                                {
                                    Sl = Convert.ToInt32(student.Sl),
                                    mesageType = 2,
                                    message = "Invalid Program Roll:" + student.PrnNo
                                });
                            }
                        }

                        var returnVals = returnVal.GroupBy(m => m.Sl, (key, group) => new { Key = key, Group = group.ToList() });

                        //return Json(new Response(true, WebHelper.CommonErrorMessage));
                        return Json(new { returnList = returnVals, IsSuccess = true });

                    }
                }
                //return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
            // "programId": programId, "sessionId": sessionId, "courseId": courseId, "examId": examId, "examType": examType, "dataArray": dataArray string programId, string sessionId, string courseId, string examId, string examType, StudentMarksListViewModel[] dataArray

        }
        #endregion

        #region Manage Marks Entry
        public ActionResult ManageMarksEntry()
        {

            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
            ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.CourseList = new SelectList(string.Empty, "Value", "Text");

            List<SelectListItem> examTypes = new List<SelectListItem>();
            examTypes.Add(new SelectListItem() { Text = "--Select Exam Type--", Value = "" });
            ViewBag.ExamTypeList = examTypes;
            return View();
        }

        public ActionResult AjaxRequestForDeleteStudentMarks(MarksEntryViewModel data)
        {
            if (data != null)
            {
                Exams examinformation = _examsService.GetExamById(data.ExamId);
                StudentProgram studentProgram = _studentProgram.GetStudentProgram(data.PrnNo);
                if (studentProgram != null)
                {
                    String studentCheckReturn = studentCheck(examinformation, studentProgram);
                    if (studentCheckReturn == "")
                    {
                        IList<ExamsStudentMarks> studentmarksList = _examsStudentMarksService.LoadExamsStudentMarksByExamStudentProgram(Convert.ToInt64(examinformation.Id), studentProgram.Id, data.ExamType);

                        if (studentmarksList.Any())
                        {
                            bool isDelete = _examsStudentMarksService.deleteExamMarksByStudent(studentmarksList);
                            // bool isDelete = true;
                            if (isDelete)
                            {
                                return Json(new Response(true, "Student marks deleted successfully"));
                            }
                            else
                            {
                                return Json(new Response(false, WebHelper.CommonErrorMessage));
                            }
                        }
                        else
                        {
                            return Json(new Response(false, "This student has no marks for this exam"));
                        }
                    }
                }
            }
            return Json(new Response(false, "Invalid Program Roll"));
        }
        [HttpPost]
        public ActionResult AjaxRequestForUpdateStudentMarks(MarksEntryViewModel data)
        {
          
            var tempSmses = new List<TemporarySms>();
         
            if (data != null && data.StudentMarksList.Any())
            {
                Exams examinformation = _examsService.GetExamById(data.ExamId);
                if (examinformation != null)
                {
                    bool isExamTypeValid = false;
                    int maxSubject = 0;
                    if (examinformation.IsMcq && data.ExamType == ExamType.Mcq)
                    {
                        isExamTypeValid = true;
                        maxSubject = examinformation.McqMaxSubjectCount;
                    }
                    else if (examinformation.IsWritten && data.ExamType == ExamType.Written)
                    {
                        isExamTypeValid = true;
                        maxSubject = examinformation.WrittenMaxSubjectCount;
                    }
                    else
                    {
                        isExamTypeValid = false;
                    }

                    IList<ExamsDetails> examsDetailses =
                           examinformation.ExamDetails.Where(x => x.ExamType == data.ExamType).OrderByDescending(y => y.IsCompulsary).ToList();

                    Dictionary<string, int> setCodeList = SetCode.LoadSetCodes();
                    if (examinformation.Program.Id == data.Program && examinformation.Session.Id == data.Session && examinformation.Course.Id == data.Course && isExamTypeValid && examsDetailses.Any())
                    {
                        List<MarksEntryReturnMessage> returnVal = new List<MarksEntryReturnMessage>();
                        int subjectType = examsDetailses.FirstOrDefault().SubjectType;
                        foreach (var student in data.StudentMarksList)
                        {
                            StudentProgram studentProgram = _studentProgram.GetStudentProgram(student.PrnNo);
                            if (studentProgram != null)
                            {
                                String studentCheckReturn = studentCheck(examinformation, studentProgram);
                                if (studentCheckReturn == "")
                                {
                                    var markList = student.MarksList.Where(x => x.Marks != null).ToList();
                                    //var markList = student.MarksList;
                                    var markDeletedList = student.MarksList.Where(x => x.Marks == null).ToList();

                                    if (markList.Any() && markList.Count <= maxSubject)
                                    {
                                        int setcode = 0;
                                        setCodeList.TryGetValue(student.SetCode, out setcode);
                                        if (setcode != 0)
                                        {
                                            var markList2 = student.MarksList;
                                            bool isAbleSms = false;

                                            foreach (var m in markList2)
                                            {
                                                var ed = examsDetailses.FirstOrDefault(x => x.Id == Convert.ToInt64(m.ExamDetailsId));
                                                decimal maxMarks = 0;
                                                if (ed.ExamType == ExamType.Mcq)
                                                {
                                                    maxMarks = ed.TotalQuestion * ed.MarksPerQuestion;
                                                }
                                                else
                                                {
                                                    maxMarks = ed.WrittenFullMark;
                                                }
                                                string subjectName = "";
                                                subjectName = subjectType == SubjectType.Combined ? "Combined" : ed.Subject.Name;
                                                decimal marks = 0;
                                                marks = Convert.ToDecimal(m.Marks);
                                                if (marks <= maxMarks)
                                                {
                                                    IList<ExamsStudentMarks> studentmarksList =
                                                                _examsStudentMarksService.LoadExamsStudentMarksByExamStudentProgram(Convert.ToInt64(examinformation.Id), studentProgram.Id, data.ExamType);
                                                    bool isAlreadyExistMarks = false;

                                                    if (studentmarksList.Any())
                                                    {
                                                        ExamsStudentMarks iae =
                                                            studentmarksList.FirstOrDefault(
                                                                x =>
                                                                    x.ExamsDetails.Id ==
                                                                    Convert.ToInt64(m.ExamDetailsId));
                                                        if (iae != null)
                                                        {
                                                            bool isDeleted = false;
                                                            if (markDeletedList.Any())
                                                            {

                                                                var isDeletedMarks =
                                                                    markDeletedList.FirstOrDefault(
                                                                        x =>
                                                                            x.ExamDetailsId ==
                                                                            iae.ExamsDetails.Id.ToString());
                                                                if (isDeletedMarks != null)
                                                                {
                                                                    isDeleted =
                                                                        _examsStudentMarksService.Delete(iae);
                                                                }
                                                            }

                                                            if (!isDeleted)
                                                            {
                                                                var tempOldMarks = iae.ObtainMark;
                                                                iae.ObtainMark = marks;
                                                                bool isUpdate =
                                                                    _examsStudentMarksService.Update(iae);

                                                                if (isUpdate)
                                                                {
                                                                    if (tempOldMarks != marks)
                                                                    {
                                                                        isAbleSms = true;
                                                                    }
                                                                    returnVal.Add(new MarksEntryReturnMessage
                                                                    {
                                                                        Sl = Convert.ToInt32(student.Sl),
                                                                        mesageType = 0,
                                                                        message =
                                                                            "Student Answer Update Success for prnNo: " +
                                                                            student.PrnNo + " and exam code: " +
                                                                            examinformation.Code + " and subject: " +
                                                                            subjectName
                                                                    });
                                                                }
                                                                else
                                                                {
                                                                    returnVal.Add(new MarksEntryReturnMessage
                                                                    {
                                                                        Sl = Convert.ToInt32(student.Sl),
                                                                        mesageType = 3,
                                                                        message =
                                                                            "Student Answer Update fail for prnNo: " +
                                                                            student.PrnNo + " and exam code: " +
                                                                            examinformation.Code + " and subject: " +
                                                                            subjectName
                                                                    });
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ExamsStudentMarks examsStudentMarks = new ExamsStudentMarks();
                                                            examsStudentMarks.ExamsDetails = ed;
                                                            examsStudentMarks.StudentProgram = studentProgram;
                                                            examsStudentMarks.ObtainMark = marks;
                                                            examsStudentMarks.SetCode = setcode;
                                                            bool isSave =
                                                                _examsStudentMarksService.Save(examsStudentMarks);
                                                            if (isSave)
                                                            {
                                                                isAbleSms = true;
                                                                returnVal.Add(new MarksEntryReturnMessage
                                                                {
                                                                    Sl = Convert.ToInt32(student.Sl),
                                                                    mesageType = 0,
                                                                    message =
                                                                        "Student Answer Save Success for prnNo: " +
                                                                        student.PrnNo + " and exam code: " +
                                                                        examinformation.Code + " and subject: " +
                                                                        subjectName
                                                                });
                                                            }
                                                            else
                                                            {
                                                                returnVal.Add(new MarksEntryReturnMessage
                                                                {
                                                                    Sl = Convert.ToInt32(student.Sl),
                                                                    mesageType = 3,
                                                                    message =
                                                                        "Student Answer Save fail for prnNo: " +
                                                                        student.PrnNo + " and exam code: " +
                                                                        examinformation.Code + " and subject: " +
                                                                        subjectName
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    returnVal.Add(new MarksEntryReturnMessage
                                                    {
                                                        Sl = Convert.ToInt32(student.Sl),
                                                        mesageType = 2,
                                                        message = "Invalid Marks for subject :" + subjectName + " of Program Roll:" + student.PrnNo
                                                    });
                                                    //break;
                                                }
                                            }

                                            if (isAbleSms)
                                            {
                                                var temporarySms = new TemporarySms();
                                                temporarySms.Student = studentProgram.Student;
                                                temporarySms.StudentProgram = studentProgram;
                                                temporarySms.Exams = examinformation;
                                                temporarySms.ExamType = data.ExamType;
                                                tempSmses.Add(temporarySms);
                                                
                                            }

                                        }
                                        else
                                        {
                                            returnVal.Add(new MarksEntryReturnMessage
                                            {
                                                Sl = Convert.ToInt32(student.Sl),
                                                mesageType = 2,
                                                message = "Invalid Set Code:" + student.SetCode + " of Program Roll:" + student.PrnNo
                                            });
                                        }
                                    }
                                    else
                                    {
                                        returnVal.Add(new MarksEntryReturnMessage
                                        {
                                            Sl = Convert.ToInt32(student.Sl),
                                            mesageType = 2,
                                            message = "Invalid Marks List For Program Roll:" + student.PrnNo
                                        });
                                    }
                                }
                                else
                                {
                                    returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        Sl = Convert.ToInt32(student.Sl),
                                        mesageType = 2,
                                        message = "Invalid Program Roll:" + student.PrnNo
                                    });
                                }
                            }
                            else
                            {
                                returnVal.Add(new MarksEntryReturnMessage
                                {
                                    Sl = Convert.ToInt32(student.Sl),
                                    mesageType = 2,
                                    message = "Invalid Program Roll:" + student.PrnNo
                                });
                            }
                        }

                        if (tempSmses.Any())
                        {
                            var isSmsSave = TemplateSmsGenerateAndSaveToDb(tempSmses);
                            //foreach (var esmvm in examStudentMarksViewModels)  
                            //{
                            //    bool flagForSms = SendSmsApi.GenerateInstantResultSmsAndNumber(esmvm.SmsTypeId, esmvm.StudentProgram, esmvm.Exam, esmvm.ExamType);
                            //}
                        }
                        var returnVals = returnVal.GroupBy(m => m.Sl, (key, group) => new { Key = key, Group = group.ToList() });
                        currentSession.Clear();
                        return Json(new { returnList = returnVals, IsSuccess = true });
                    }
                }
            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }
        #endregion

        #region Exam Marks Delete
        [HttpGet]
        public ActionResult ClearMarksEntry()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ClearMarksViewModel clearMarksViewModel = new ClearMarksViewModel();

                SelectList organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");

                List<SelectListItem> listOrganization = organization.ToList();
                listOrganization.Insert(0, new SelectListItem() { Value = " ", Text = "Select Organization" });
                ViewBag.organizationList = new SelectList(listOrganization, "Value", "Text");

                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");

                ViewBag.SelectedGender = new SelectList(new List<SelectListItem>()
                                                               {
                                                                   new SelectListItem() {Value = "0", Text = "Select Gender"},
                                                                   new SelectListItem() {Value = SelectionType.All.ToString(), Text = "All"},
                                                                   new SelectListItem() {Value = SelectionType.Male.ToString(), Text = "Male"},
                                                                   new SelectListItem() {Value = SelectionType.Female.ToString(), Text = "Female"}
                                                                  // new SelectListItem() {Value = SelectionType.Other.ToString(), Text = "Other"}
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());

                List<SelectListItem> examTypes = new List<SelectListItem>();
                examTypes.Add(new SelectListItem() { Text = "--Select Exam Type--", Value = "" });
                ViewBag.ExamTypeList = examTypes;

                return View(clearMarksViewModel);

            }
            catch (Exception e)
            {
                _logger.Error(e);
                return View(new ClearMarksViewModel());
            }
        }

        [HttpPost]
        public JsonResult DeleteClearMarks(long organizationId, long programId, long sessionId, string gender, long[] branchIds, long[] campusIds,
            string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long examId, long examType)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(organizationId),
                    _commonHelper.ConvertIdToList(programId), branchIds.ToList(), _commonHelper.ConvertIdToList(sessionId), campusIds.ToList(), batchDays, batchTimes,
                    null, Convert.ToString(gender) == "4" ? null : _commonHelper.ConvertIdToList(Convert.ToInt32(Convert.ToString(gender)))).ToList();
                if (!batchIds.Contains(0))
                {
                    batchList = batchList.FindAll(x => x.Id.In(batchIds)).ToList();
                }
                List<ServiceBlock> serviceBlocks = new List<ServiceBlock>();
                long[] cIds;
                if ((Array.Exists(courseIds, item => item == SelectionType.SelelectAll)))
                {
                    cIds = _courseService.LoadCourse(programId, sessionId).Select(x => x.Id).ToArray();
                }
                else
                {
                    cIds = courseIds;
                }
                var clearMarksDelete = _examsStudentMarksService.ClearExamMarksOfStudents(_userMenu, _commonHelper.ConvertIdToList(examId).ToArray(),
                    "1", Convert.ToString(gender), "1", programId, sessionId, cIds, branchIds, campusIds, Convert.ToString(examType), "1", "1", batchList, serviceBlocks);
                

                //var clearMarksDelete =  _examsStudentMarksService.ClearMarksStudentList(_userMenu, programId, sessionId, gender, branchIds, campusIds,
                                   // batchDays, batchTimes, batchIds, courseIds, examId, examType);
                return Json(new { clearMarksDelete = clearMarksDelete, IsSuccess = true });
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public JsonResult GetClearMarksCount(long organizationId, long programId, long sessionId, string gender, long[] branchIds, long[] campusIds,
            string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long examId, long examType)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(organizationId),
                    _commonHelper.ConvertIdToList(programId), branchIds.ToList(), _commonHelper.ConvertIdToList(sessionId), campusIds.ToList(), batchDays, batchTimes,
                    null, Convert.ToString(gender) == "4" ? null : _commonHelper.ConvertIdToList(Convert.ToInt32(Convert.ToString(gender)))).ToList();
                if (!batchIds.Contains(0))
                {
                    batchList = batchList.FindAll(x => x.Id.In(batchIds)).ToList();
                }
                List<ServiceBlock> serviceBlocks = new List<ServiceBlock>();
                long[] cIds;
                if ((Array.Exists(courseIds, item => item == SelectionType.SelelectAll)))
                {
                    cIds = _courseService.LoadCourse(programId, sessionId).Select(x => x.Id).ToArray();
                }
                else
                {
                    cIds = courseIds;
                }
                var clearMarksCount = _examsStudentMarksService.GetExamResultListCountForClearMarks(_userMenu, _commonHelper.ConvertIdToList(examId).ToArray(),
                    "1", Convert.ToString(gender), "1", programId, sessionId, cIds, branchIds, campusIds, Convert.ToString(examType), "1", "1", batchList, serviceBlocks);
                
                return Json(new { clearMarksCount = clearMarksCount, IsSuccess = true });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #region Helper Method

        private string CheckMobileNumber(string mobile)
        {
            if (mobile != null)
            {
                var regex = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
                var match = regex.Match(mobile);
                if (match.Success == true)
                {
                    switch (mobile.Length)
                    {
                        case 14:
                            mobile = mobile.Substring(3);
                            break;
                        case 13:
                            mobile = mobile.Substring(2);
                            break;
                        case 10:
                            mobile = "0" + mobile;
                            break;
                    }
                }
                else { mobile = null; }
            }
            return mobile;
        }
        [HttpPost]
        public ActionResult AjaxRequestForExamCoad(string query, string programId, string sessionId, string courseId, string isMcq = "")
        {
            bool? examTypeValue = (isMcq != "") ? Convert.ToBoolean(isMcq) : (bool?)null;
            try
            {
                if (!String.IsNullOrEmpty(programId) && !String.IsNullOrEmpty(sessionId) &&
                    !String.IsNullOrEmpty(courseId))
                {
                    programId = programId.Trim();
                    sessionId = sessionId.Trim();
                    courseId = courseId.Trim();
                }
                else
                {
                    programId = "-1";
                    sessionId = "-1";
                    courseId = "-1";
                }
                SelectList examsList = new SelectList(_examsService.GetExamByProgramSessionCourseAutoComplete(query, Convert.ToInt64(programId), Convert.ToInt64(sessionId), Convert.ToInt64(courseId), examTypeValue), "Id", "Code");
                List<SelectListItem> selectExamList = examsList.ToList();
                return Json(new { returnList = selectExamList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult AjaxRequestForExamInformation(long id, string isAnswerUpload = "N")
        {
            try
            {
                Exams exam = _examsService.GetExamById(id);

                if (isAnswerUpload=="Y")
                {
                    if (exam.IsTakeAnswerSheetWhenUploadMarks)
                    {
                        return
                            Json(new Response(false,
                                "When you will upload marks This kind of exam's answer sheet will be added instantly.you can't upload answer sheet from here!!"));
                    }
                }
                //else
                //{
                //    if (!exam.IsTakeAnswerSheetWhenUploadMarks)
                //    {
                //        return Json(new Response(false, "When you will upload marks This kind of exam's answer sheet will be added instantly.you can't upload answer sheet from here!!"));
                //    }
                //}
                


                Dictionary<string, string> examinfo = new Dictionary<string, string>();
                if (exam != null)
                {
                    int totalQuestion =exam.ExamDetails.Where(x => x.ExamType == ExamType.Mcq)
                            .Select(x => x.TotalQuestion)
                            .Take(1)
                            .SingleOrDefault();
                    examinfo.Add("Name", exam.Name);
                    examinfo.Add("IsMcq", exam.IsMcq.ToString());
                    examinfo.Add("IsWritten", exam.IsWritten.ToString());
                    examinfo.Add("IsMcqValue", ExamType.Mcq.ToString());
                    examinfo.Add("IsWrittenValue", ExamType.Written.ToString());
                    examinfo.Add("TotalQuestion", totalQuestion.ToString());
                    examinfo.Add("IsTakeAnswerSheetWhenUploadMarks",exam.IsTakeAnswerSheetWhenUploadMarks.ToString());
                    return Json(new { returnList = examinfo, IsSuccess = true });
                }
                else
                {
                    return Json(new { returnList = examinfo, IsSuccess = false });
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult AjaxRequestForDrawingManualEntryTable(long id, long totalRows, long examType, string commonSetCode)
        {
            try
            {
                if (id <= 0)
                {
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
                else
                {
                    Exams exam = _examsService.GetExamById(id);
                    ViewBag.Exam = exam;
                    if (totalRows <= 0)
                        totalRows = 40;
                    ViewBag.totalRows = totalRows;
                    ViewBag.examType = examType;
                    ViewBag.commonSetCode = commonSetCode;

                    return PartialView("_ManualEntryTable");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        public ActionResult AjaxRequestForStudentCheck(string prnNo, string programId, string sessionId)
        {
            try
            {
                if (!String.IsNullOrEmpty(prnNo) && !String.IsNullOrEmpty(programId) && !String.IsNullOrEmpty(sessionId))
                {
                    StudentProgram studentProgram = _studentProgram.GetStudentProgram(prnNo);
                    if (studentProgram != null)
                    {
                        var studentBranchId = studentProgram.Batch.Branch.Id;
                        var studentSessionId = studentProgram.Batch.Session.Id;
                        var studentProgramId = studentProgram.Program.Id;

                        if (programId == studentProgramId.ToString() && sessionId == studentSessionId.ToString())
                        {
                            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                            //bool allBranch = false;
                            //bool allProgram = false;
                            //long[] branchIdList = AuthHelper.GetUserBranchIdList(_userMenu, out allBranch);
                            //long[] programIdList = AuthHelper.GetUserProgramIdList(_userMenu, out allProgram);
                            List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                            List<long> programIdList = AuthHelper.LoadProgramIdList(_userMenu);

                            if ((branchIdList == null || (branchIdList.Contains(studentBranchId))) && (programIdList == null || (programIdList.Contains(studentProgramId))))
                            {
                                return Json(new Response(true, "Valid PrnNo"));
                            }
                        }
                    }
                }
                return Json(new Response(false, "Invalid Program Roll"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult AjaxRequestForDrawingManageEntryTable(long examId, string prnNo, int examType, string programId, string sessionId)
        {
            try
            {
                if (!String.IsNullOrEmpty(prnNo) && !String.IsNullOrEmpty(programId) && !String.IsNullOrEmpty(sessionId))
                {
                    StudentProgram studentProgram = _studentProgram.GetStudentProgram(prnNo);
                    if (studentProgram != null)
                    {
                        var studentBranchId = studentProgram.Batch.Branch.Id;
                        var studentSessionId = studentProgram.Batch.Session.Id;
                        var studentProgramId = studentProgram.Program.Id;
                        if (programId == studentProgramId.ToString() && sessionId == studentSessionId.ToString())
                        {
                            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                            List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                            List<long> programIdList = AuthHelper.LoadProgramIdList(_userMenu);

                            if ((branchIdList == null || (branchIdList.Contains(studentBranchId))) &&
                                (programIdList == null || (programIdList.Contains(studentProgramId))))
                            {
                                bool isAlreadyExistMarks = _examsStudentMarksService.isAlreadyExistMarks(examId, studentProgram.Id, examType);
                                if (isAlreadyExistMarks)
                                {
                                    Exams exam = _examsService.GetExamById(examId);
                                    IList<ExamsStudentMarks> examsStudentMarkses = _examsStudentMarksService.GetExamMarksByProgramId(examId, studentProgram.Id, examType);
                                    ViewBag.studentProgram = studentProgram;
                                    ViewBag.Exam = exam;
                                    ViewBag.StudentMarkses = examsStudentMarkses;
                                    ViewBag.examType = examType;
                                    ViewBag.prnNo = prnNo;
                                    return PartialView("_ManageEntryTable");
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = "This Student has no marks for this exam";
                                }
                            }
                            else
                            {
                                ViewBag.ErrorMessage = "Invalid Program Roll";
                            }
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "Invalid Program Roll";
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "Invalid Program Roll";
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Invalid Program Roll";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Invalid Program Roll";
                _logger.Error(ex);
            }
            return PartialView("_ManageEntryTable");
        }




        #endregion
    }

}