﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class CampusRoomFactory : ObjectFactoryBase<CampusRoom>
    {
        #region Object Initialization

        private readonly CampusRoomService _campusRoomService;
        private readonly CampusFactory _campusFactory;
        public CampusRoomFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _campusRoomService = new CampusRoomService(session);
            _campusFactory = new CampusFactory(this, session);
        }
        #endregion

        #region Create
        public CampusRoomFactory Create()
        {
            Object = new CampusRoom()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                RoomNo = "420_" + DateTime.Now.ToString("yyyyMMddHHmmss"),
                Status = CampusRoom.EntityStatus.Active,
                ClassCapacity = 420,
                ExamCapacity = 420
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public CampusRoomFactory CreateMore(int numberOfcampusRoom)
        {
            var camNamePrefix = Guid.NewGuid().ToString();
            _campusFactory.Create().WithNameAndRank(camNamePrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"), 1).WithBranch().Persist();
            CreateCampusList(numberOfcampusRoom, _campusFactory.Object);
            return this;
        }

        public CampusRoomFactory CreateMore(int numberOfcampusRoom, Campus campus)
        {
            CreateCampusList(numberOfcampusRoom, campus);
            return this;
        }

        private IList<CampusRoom> CreateCampusList(int numberOfcampus, Campus campus)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<CampusRoom>();
            for (int i = 0; i < numberOfcampus; i++)
            {
                var campusRoom = new CampusRoom()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = Name,
                    Rank = i+1,
                    CreateBy = 1,
                    ModifyBy = 1,
                    RoomNo = "420_" + i+Guid.NewGuid(),
                    Status = CampusRoom.EntityStatus.Active,
                    ClassCapacity = 420,
                    ExamCapacity = 420,
                    Campus = campus
                };
                list.Add(campusRoom);

            }
            ObjectList.AddRange(list);
            return list;
        }

        public CampusRoomFactory WithCampus()
        {
            _campusFactory.Create().WithBranch().WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                }).Persist();
            Object.Campus = _campusFactory.Object;
            return this;
        }
        public CampusRoomFactory WithCampus(Campus campus)
        {
            Object.Campus = campus;
            return this;
        }

        public CampusRoomFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _campusRoomService.Save(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _campusRoomService.Save(item);
                    }
                }
            }
            return this;
        }

        #endregion

        #region Clean up
        public void CleanUp()
        {
            DeleteAll();
            _campusFactory.Cleanup();
        }
        #endregion
    }
}
