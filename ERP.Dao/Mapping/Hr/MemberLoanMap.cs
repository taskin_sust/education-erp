using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{


    public partial class MemberLoanMap : BaseClassMap<MemberLoan,long>
    {

        public MemberLoanMap()
        {
			Table("HR_MemberLoan");
			LazyLoad();
			References(x => x.MemberLoanApplication).Column("MemberLoanApplicationId");
			Map(x => x.LoanApprovedAmount).Column("LoanApprovedAmount").Not.Nullable();
			Map(x => x.MonthlyRefund).Column("MonthlyRefund").Not.Nullable();
			Map(x => x.RefundStart).Column("RefundStart").Not.Nullable();
			Map(x => x.ApprovedDate).Column("ApprovedDate");
        }
    }
}
