using log4net;
using NHibernate;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
public interface ISpecificationCriteriaService : IBaseService
{
    #region Operational Function

    bool SaveOrUpdate(SpecificationCriteria specificationCriteria);

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function

    #endregion

    #region Helper Function

    #endregion
}

public class SpecificationCriteriaService : BaseService, ISpecificationCriteriaService {
    #region Logger
    private readonly ILog _logger = LogManager.GetLogger("InventoryService");
    #endregion

    #region Propertise & Object Initialization
    private readonly CommonHelper _commonHelper;
    private readonly ISpecificationCriteriaDao _specificationCriteriaDao;
    public SpecificationCriteriaService(ISession session)
    {
        Session = session;
        _commonHelper = new CommonHelper();
        _specificationCriteriaDao=new SpecificationCriteriaDao{Session = session};}
    #endregion

    #region Operational Functions

    public bool SaveOrUpdate(SpecificationCriteria specificationCriteria)
    {

        return true;
    }

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function


    #endregion

    #region Helper function



    #endregion
}
}
                                    
