﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface IManagementTimeService : IBaseService
    {
        #region Operational Function

        void SaveManagementTime(ManagementTime managementTime);
        void UpdateManagementTime(ManagementTime managementTime);
        void DeleteManagementTime(long id);

        #endregion

        #region Single Instances Loading Function

        ManagementTime GetManagementTime(long id);

        #endregion

        #region List Loading Function

        IList<ManagementTime> LoadManagementTimeList(List<UserMenu> userMenuList, int start, int length, string orderBy, string orderDir, string pinStringList = "", DateTime? workingDateFrom = null, DateTime? workingDateTo = null, long? createdByUserId = null);

        #endregion

        #region Others Function

        int GetManagementTimeCount(List<UserMenu> userMenuList, string orderBy, string orderDir, string pinStringList = "", DateTime? workingDateFrom = null, DateTime? workingDateTo = null, long? createdByUserId = null);

        #endregion

        #region Helper Function

        #endregion

    }
    public class ManagementTimeService : BaseService, IManagementTimeService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        
        private readonly CommonHelper _commonHelper;
        private readonly IManagementTimeDao _managementTimeDao;
        private readonly IEmploymentHistoryDao _employmentHistoryDao;
        private readonly ITeamMemberDao _teamMemberDao;

        public ManagementTimeService(ISession session)
        {
            Session = session;
            _managementTimeDao = new ManagementTimeDao { Session = session };
            _employmentHistoryDao = new EmploymentHistoryDao { Session = session };
            _teamMemberDao = new TeamMemberDao { Session = session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public void SaveManagementTime(ManagementTime managementTime)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSave(managementTime);
                using (transaction = Session.BeginTransaction())
                {
                    _managementTimeDao.Save(managementTime);
                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void UpdateManagementTime(ManagementTime managementTime)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSave(managementTime);
                ManagementTime oldManagementTime = _managementTimeDao.LoadById(managementTime.Id);
                if(oldManagementTime == null)
                    throw new InvalidDataException("Invalid Management Time entry!");
                using (transaction = Session.BeginTransaction())
                {
                    oldManagementTime.TeamMember = managementTime.TeamMember;
                    oldManagementTime.WorkDate = managementTime.WorkDate;
                    oldManagementTime.Time = managementTime.Time;
                    oldManagementTime.Remarks = managementTime.Remarks;
                    _managementTimeDao.Update(oldManagementTime);
                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void DeleteManagementTime(long id)
        {
            ITransaction transaction = null;
            try
            {
                long currentUserId = 0;
                 if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
                     currentUserId = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                
                ManagementTime managementTime = _managementTimeDao.LoadById(id);
                if (managementTime == null)
                    throw new InvalidDataException("Invalid Management Time entry!");

                if (managementTime.CreateBy != currentUserId)
                    throw new InvalidDataException("This Management Time entry is not your!");
                
                using (transaction = Session.BeginTransaction())
                {
                    managementTime.Status = ManagementTime.EntityStatus.Delete;
                    _managementTimeDao.Update(managementTime);
                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function
        
        public ManagementTime GetManagementTime(long id)
        {
            try
            {
                return _managementTimeDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function
        public IList<ManagementTime> LoadManagementTimeList(List<UserMenu> userMenuList, int start, int length, string orderBy, string orderDir, string pinStringList = "", DateTime? workingDateFrom = null, DateTime? workingDateTo = null, long? createdByUserId = null)
        {
            try
            {
                List<int> memberPinList = new List<int>();
                List<long> authoMemberIdList = new List<long>();
                if (!String.IsNullOrEmpty(pinStringList))
                {
                    pinStringList = pinStringList.Replace(',', ' ');
                    string[] pinArray = pinStringList.Split(' ');
                    memberPinList.AddRange(pinArray.Select(pin => Convert.ToInt32(pin)));
                }
                else
                {
                    memberPinList.Add(Convert.ToInt32(SelectionType.SelelectAll));
                }
                if (userMenuList != null && userMenuList.Any())
                {
                    List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenuList);
                    List<long> authoBranchIdList = AuthHelper.LoadBranchIdList(userMenuList, authoOrganizationIdList);
                    DateTime dateTime = DateTime.Now;
                    authoMemberIdList = _teamMemberDao.LoadHrAuthorizedTeamMemberId(dateTime, authoBranchIdList).ToList();
                }
                return _managementTimeDao.LoadManagementTimeList(start, length, orderBy, orderDir, memberPinList, authoMemberIdList, workingDateFrom, workingDateTo, createdByUserId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Others Function

        public int GetManagementTimeCount(List<UserMenu> userMenuList, string orderBy, string orderDir, string pinStringList = "", DateTime? workingDateFrom = null, DateTime? workingDateTo = null, long? createdByUserId = null)
        {
            try
            {
                List<int> memberPinList = new List<int>();
                List<long> authoMemberIdList = new List<long>();
                if (!String.IsNullOrEmpty(pinStringList))
                {
                    pinStringList = pinStringList.Replace(',', ' ');
                    string[] pinArray = pinStringList.Split(' ');
                    memberPinList.AddRange(pinArray.Select(pin => Convert.ToInt32(pin)));
                }
                else
                {
                    memberPinList.Add(Convert.ToInt32(SelectionType.SelelectAll));
                }
                if (userMenuList != null && userMenuList.Any())
                {
                    List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenuList);
                    List<long> authoBranchIdList = AuthHelper.LoadBranchIdList(userMenuList, authoOrganizationIdList);
                    DateTime dateTime = DateTime.Now;
                    authoMemberIdList = _teamMemberDao.LoadHrAuthorizedTeamMemberId(dateTime, authoBranchIdList).ToList();
                }
                return _managementTimeDao.GetManagementTimeCount(orderBy, orderDir, memberPinList, authoMemberIdList, workingDateFrom, workingDateTo, createdByUserId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Helper function

        private void CheckBeforeSave(ManagementTime managementTime)
        {
            if (managementTime.TeamMember == null)
                throw new InvalidDataException("No team member found!");

            if (managementTime.Time <= 0)
                throw new InvalidDataException("Please enter your management time!");

            EmploymentHistory joinEmploymentHistory = _employmentHistoryDao.GetTeamMemberJoiningEmploymentHistory(managementTime.TeamMember.Id);
            if (joinEmploymentHistory != null && managementTime.WorkDate.Date < joinEmploymentHistory.EffectiveDate.Date)
                throw new InvalidDataException("This team member joining date is " + joinEmploymentHistory.EffectiveDate.Date.ToString("yyyy-MM-dd") + ". Please enter working date after this date.");

            if (String.IsNullOrEmpty(managementTime.Remarks))
                throw new InvalidDataException("Please enter remarks of your management time!");

        }


        #endregion

       
    }
}
