﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.BusinessRules.UInventory
{
    public enum ItemType
    {
        [Description("Program Item")]
        ProgramItem = 1,
        [Description("Common Item")]
        CommonItem = 2
    }

    public enum ItemUnit
    {
        [Description("Pcs")]
        Pcs = 1,
        [Description("Kg")]
        Kg = 2,
        [Description("Feet")]
        Feet = 3,
        [Description("Liter")]
        Liter = 4
    }

    public enum CostBearer
    {
        [Description("Corporate Branch")]
        Corporate = 1,
        [Description("Individual Branch")]
        Individual = 2
    }

    public enum FieldType
    {
        [Description("Dropdown")]
        Dropdown = 1,
        [Description("Text")]
        Text = 2
    }

    public enum Purpose
    {
        [Description("Program Purpose")]
        ProgramPurpose = 1,
        [Description("Marketing Purpose")]
        MarketingPurpose = 2,
        [Description("Gift & Promotion Purpose")]
        GiftAndPromotionPurpose = 3,
        [Description("Administrative Purpose")]
        AdministrativePurpose = 4,
        [Description("Branding Purpose")]
        BrandingPurpose = 5
    }

    public enum QuotationStatus
    {
        [Description("Not Published")]
        NotPublished = 1,
        [Description("Running")]
        Running = 2,
        [Description("Submission Closed")]//Not Reviewed
        SubmissionClosed = 3,
        [Description("Pertial Issued")]
        PertialIssued = 4,
        [Description("Issued")]
        Issued = 5,
        [Description("Cancelled")]
        Cancelled = 6
    }

    public enum WorkOrderStatus
    {
        [Description("Issued")]
        Issued = 1,
        [Description("Partially Received")]
        PartiallyReceived = 2,
        [Description("Received")]
        Received = 3,
        [Description("Cancelled")]
        Cancelled = 4
    }

    public enum GoodsIssuedStatus
    {
        [Description("Issued")]
        Issued = 1
    }
    public enum PriceQouteStatus
    {
        [Description("Submitted")]
        Submitted = 1,
        [Description("WO Received")]
        WoReceived = 2,
        [Description("Cancelled")]
        Cancelled = 3
    }
    public enum RequisitionStatus
    {
        [Description("Pending")]
        Pending = 1,
        [Description("Partial")]
        Partially = 2,
        [Description("Completed")]
        Completed = 3,
        [Description("Cancelled")]
        Cancelled = 4
    }
    public enum RequisitionReportType
    {
        [Description("List Wise")]
        ListWise = 1,
        [Description("Branch Wise")]
        BranchWise = 2,
        [Description("Program Wise")]
        ProgramWise = 3
    }

    public enum GoodsReceivedStatus
    {
        [Description("Partially Received")]
        PartiallyReceived = 1,
        [Description("Received")]
        Received = 2
    }

    public enum GoodsReturnStatus
    {
        [Description("Partially Returned")]
        PartiallyReturned = 1,
        [Description("Returned")]
        Returned = 2

    }

    public static class GetEnumDescription
    {
        public static string Description(Enum value)
        {
            FieldInfo info = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])info.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
    }

}
