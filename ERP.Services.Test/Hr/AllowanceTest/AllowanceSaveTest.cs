﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;
using Xunit.Sdk;

namespace UdvashERP.Services.Test.Hr.AllowanceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Allowance")]
    public class AllowanceSaveTest : AllowanceBaseTest
    {
        //#region Basic Test

        //[Fact]
        //public void Allowance_Save_By_Mentor_Should_Return_True()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(1);
        //    var teamMember = PersistTeamMember(MentorId);
        //    var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
        //    var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });
        //    var campus = teamMember.EmploymentHistory.Select(x => x.Campus).LastOrDefault();
        //    var userMenu = BuildUserMenu(new List<Organization>() { campus.Branch.Organization },
        //                                    null, new List<Branch>() { campus.Branch });
            
        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);
        //    _allowanceService.Save(userMenu, date.ToString("MM-dd-yyyy")
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "", true);
        //    var idList = Session.QueryOver<Allowance>().Select(x => x.Id).Where(x => x.TeamMember == teamMember).List<long>();
        //    _allowanceFactory.Object.Id = idList[0];
        //    Assert.NotNull(_allowanceService.GetAllowance(_allowanceFactory.Object.Id));
        //}
        
        //[Fact]
        //public void Allowance_Save_By_Hr_Should_Return_True()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(1);
        //    var teamMember = PersistTeamMember(MentorId);
        //    var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
        //    var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });

        //    var campus = teamMember.EmploymentHistory.Select(x => x.Campus).LastOrDefault();
        //    var userMenu = BuildUserMenu(new List<Organization>() { campus.Branch.Organization },
        //                                    null, new List<Branch>() { campus.Branch });


        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);
        //    _allowanceService.Save(userMenu, date.ToString("MM-dd-yyyy")
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "");
        //    var idList = Session.QueryOver<Allowance>().Select(x => x.Id).Where(x => x.TeamMember == teamMember).List<long>();
        //    _allowanceFactory.Object.Id = idList[0];
        //    Assert.True(true);
        //}

        //#endregion

        //#region Business Logic Test
        
        //[Fact]
        //public void Allowance_Update_Should_Return_InvalidDataException_For_UserMenu_Empty()
        //{
        //    Assert.Throws<ArgumentNullException>(() => _allowanceService.Save(new List<UserMenu>(), DateTime.Now.ToString(CultureInfo.InvariantCulture), "", "", ""));
        //}
        
        //[Fact]
        //public void Allowance_Update_Should_Return_InvalidDataException_For_UserMenu_Null()
        //{
        //    Assert.Throws<ArgumentNullException>(() => _allowanceService.Save(null, DateTime.Now.ToString(CultureInfo.InvariantCulture), "", "", ""));
        //}

        //[Fact]
        //public void InvalidDataException_Allowance_Update_For_Date_Invalid()
        //{
        //    var teamMember = CreateTeamMember();
        //    var userMenu = BuildUserMenu(new List<Organization>() { _organizationFactory.Object });
        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);

        //    Assert.Throws<InvalidDataException>(() => _allowanceService.Save(userMenu, null, allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), ""));
        //}
        //[Fact]
        //public void Allowance_Update_Should_Return_InvalidDataException_For_Pin()
        //{
        //    var teamMember = CreateTeamMember();
        //    var userMenu = BuildUserMenu(new List<Organization>() { _organizationFactory.Object });
        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);

        //    Assert.Throws<InvalidDataException>(() => _allowanceService.Save(userMenu, allowanceFactory.Object.DailyAllowanceDate.ToString()
        //                                                                    , ""
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), ""));
        //}

        //[Fact]
        //public void Allowance_Update_Should_Return_InvalidDataException_For_Identical_Pin_List()
        //{
        //    var teamMember = CreateTeamMember();
        //    var userMenu = BuildUserMenu(new List<Organization>() { _organizationFactory.Object });
        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);

        //    Assert.Throws<InvalidDataException>(() => _allowanceService.Save(userMenu, allowanceFactory.Object.DailyAllowanceDate.ToString()
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)));
        //}
        
        //[Fact]
        //public void Allowance_Update_Should_Return_InvalidDataException_For_Auth_Identical_Pin()
        //{
        //    var teamMember = CreateTeamMember();
        //    var userMenu = BuildUserMenu(new List<Organization>() { _organizationFactory.Object });
        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);
        //    string disallowPin = _teamMemberFactory.Object.MentorHistory.Select(x => x.Mentor.Pin).LastOrDefault().ToString(CultureInfo.InvariantCulture);
        //    Assert.Throws<InvalidDataException>(() => _allowanceService.Save(userMenu, allowanceFactory.Object.DailyAllowanceDate.ToString()
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                                                                    , disallowPin, true));
        //}
        
        //[Fact]
        //public void Allowance_Update_Should_Return_InvalidDataException_For_Empty_Pin_List()
        //{
        //    var teamMember = CreateTeamMember();
        //    var userMenu = BuildUserMenu(new List<Organization>() { _organizationFactory.Object });
        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);

        //    Assert.Throws<InvalidDataException>(() => _allowanceService.Save(userMenu, allowanceFactory.Object.DailyAllowanceDate.ToString()
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                                                                    , "", ""));
        //}
        
        //[Fact]
        //public void InvalidDataException_Allowance_Save_By_Mentor_Should_Return_For_TeamMember_Null()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(-1);
        //    var teamMember = CreateTeamMember();
        //    var startTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 10, 10, 10);
        //    var endTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 17, 10, 10);

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(_teamMemberFactory.Object);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });
        //    var userMenu = BuildUserMenu(new List<Organization>() { _organizationFactory.Object });

        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);

        //    using (ITransaction trans = Session.BeginTransaction())
        //    {
        //        for (int i = 0; i < _teamMemberFactory.Object.MentorHistory.Count; i++)
        //        {
        //            _teamMemberFactory.Object.MentorHistory.RemoveAt(i);
        //        }
        //        trans.Commit();
        //    }

        //    Assert.Throws<InvalidDataException>(() => _allowanceService.Save(userMenu, allowanceFactory.Object.DailyAllowanceDate.ToString()
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "", true));
        //}

        //[Fact]
        //public void InvalidDataException_Allowance_Save_By_Mentor_Should_Return_For_Unauthorized_Branch()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(-1);
        //    var teamMember = CreateTeamMember();
        //    var startTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 10, 10, 10);
        //    var endTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 17, 10, 10);

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(_teamMemberFactory.Object);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });

        //    _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
        //    var userMenu = BuildUserMenu(new List<Organization>() { _organizationFactory.Object }, null, new List<Branch>() { _branchFactory.Object });
        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);

        //    Assert.Throws<InvalidDataException>(() => _allowanceService.Save(userMenu, allowanceFactory.Object.DailyAllowanceDate.ToString()
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                                                                    , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "", true));
        //}

        //#endregion
    }
}
