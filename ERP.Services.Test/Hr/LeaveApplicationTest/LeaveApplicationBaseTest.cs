﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.LeaveApplicationTest
{
    //tbs: Test Base Service
    public class LeaveApplicationBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initialization

        private ISession _session;
        protected readonly CommonHelper CommonHelper;
        protected readonly ILeaveService _leaveService;
        protected readonly ILeaveApplicationService _leaveApplicationService;
        protected readonly IOrganizationService OrganizationService;
        protected readonly TestBaseService<Leave> tbsHrd;
        protected readonly IMembersLeaveSummaryService _membersLeaveSummaryService;
        protected List<long> idList = new List<long>();
        private IList<Leave> actualLeaves;
        private Leave actualLeave;
        // private string uniqueName = "demo" + DateTime.Now.Year.ToString();
        protected readonly LeaveApplicationFactory _leaveApplicationFactory;
        protected readonly LeaveApplicationDetailFactory _leaveApplicationDetailFactory;
        protected readonly LeaveFactory _leaveFactory;
        protected readonly MemberLeaveSummaryFactory _memberLeaveSummaryFactory;
        protected readonly EmploymentHistoryFactory _employmentHistoryFactory;
        protected readonly DepartmentFactory _departmentFactory;
        public LeaveApplicationBaseTest()
        {
            _session = Session;
            CommonHelper = new CommonHelper();
            _leaveService = new LeaveService(_session);
            OrganizationService = new OrganizationService(_session);
            _leaveApplicationService = new LeaveApplicationService(_session);
            tbsHrd = new TestBaseService<Leave>(_session);
            _membersLeaveSummaryService = new MembersLeaveSummaryService(Session);

            _leaveApplicationFactory = new LeaveApplicationFactory(null, _session);
            _leaveApplicationDetailFactory = new LeaveApplicationDetailFactory(null,Session);
            _leaveFactory = new LeaveFactory(null, _session);
            _memberLeaveSummaryFactory = new MemberLeaveSummaryFactory(null, _session);
            _employmentHistoryFactory = new EmploymentHistoryFactory(null, _session);
            _departmentFactory = new DepartmentFactory(null, _session);
        }

        #endregion

        #region Private Method

        public TeamMember PersistNewTeamMemberForLeaveApplication(IList<EmploymentHistory> employmentHistories, IList<ShiftWeekendHistory> shiftWeekendHistories,
          DateTime startDate, DateTime endDate, IList<MaritalInfo> maritalInfos, IList<MentorHistory> mentorHistories)
        {
            teamMemberFactory.Create()
                .WithEmploymentHistory(employmentHistories.ToList())
                .WithShiftWeekend(shiftWeekendHistories.ToList())
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfos.ToList())
                .WithMentorHistory(mentorHistories.ToList())
                .Persist();
            return teamMemberFactory.Object;
        }

        #endregion

        public void Dispose()
        {
            if (_leaveApplicationFactory.Object!=null && _leaveApplicationFactory.Object.Leave != null)
            {
                string swData = "DELETE FROM [HR_MemberLeaveSummary] WHERE [LeaveId] =" + _leaveApplicationFactory.Object.Leave.Id + " ; ";
                Session.CreateSQLQuery(swData).ExecuteUpdate();
            }
            _leaveApplicationFactory.CleanUp();
            _leaveFactory.Cleanup();
            base.Dispose();
        }
    }
}
