﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Conventions;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.Hr
{
    public interface IMentorHistoryDao : IBaseDao<MentorHistory, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        MentorHistory GetTeamMemberMentorHistory(DateTime? searchingDateTime, long? teamMemberId, int? teamMemberPin);
        #endregion

        #region List Loading Function
        IList<MentorHistory> LoadMentorHistory(int teamMemberPin, DateTime? searchingDate);
        IList<MentorHistory> LoadByMentorId(long memId);
        List<long> LoadMentorTeamMemberId(List<long> mentorIdList, DateTime searceingDate, List<long> givenTeamMemberIdList = null, List<int> givenTeamMemberPinList = null, bool isRetired = true, List<int> employmentStatusList = null);
        List<int> LoadMentorTeamMemberPinList(List<long> mentorIdList, DateTime searceingDate, List<long> givenTeamMemberIdList = null, List<int> givenTeamMemberPinList = null, bool isRetired = true, List<int> employmentStatusList = null);
        List<TeamMember> LoadMentorTeamMember(List<long> mentorIdList, DateTime searceingDate, List<long> givenTeamMemberIdList, List<int> givenTeamMemberPinList, List<int> employmentStatusList = null);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion


    }
    public class MentorHistoryDao : BaseDao<MentorHistory, long>, IMentorHistoryDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public MentorHistory GetTeamMemberMentorHistory(DateTime? searchingDateTime, long? teamMemberId, int? teamMemberPin)
        {
            if (teamMemberId != null)
                return Session.QueryOver<MentorHistory>().Where(x => x.TeamMember.Id == teamMemberId
                    && x.EffectiveDate <= searchingDateTime
                    && x.Status == MentorHistory.EntityStatus.Active
                ).OrderBy(x => x.EffectiveDate).Desc.List<MentorHistory>().FirstOrDefault();

            return Session.QueryOver<MentorHistory>().Where(x => x.TeamMember.Pin == teamMemberPin
                    && x.EffectiveDate <= searchingDateTime
                    && x.Status == MentorHistory.EntityStatus.Active
                ).OrderBy(x => x.EffectiveDate).Desc.List<MentorHistory>().FirstOrDefault();
        }
        #endregion

        #region List Loading Function

        public IList<MentorHistory> LoadMentorHistory(int teamMemberPin, DateTime? searchingDate)
        {
            var criteria = Session.CreateCriteria<MentorHistory>().Add(Restrictions.Eq("Status", MentorHistory.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "tm").Add(Restrictions.Not(Restrictions.Eq("tm.Status", TeamMember.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("tm.Pin", teamMemberPin));
            if (searchingDate != null)
                criteria.Add(Restrictions.Le("EffectiveDate", searchingDate.Value.Date));
            criteria.AddOrder(Order.Desc("EffectiveDate"));
            return criteria.List<MentorHistory>().ToList();
        }

        public IList<MentorHistory> LoadByMentorId(long memId)
        {
            var query = Session.QueryOver<MentorHistory>().Where(x => x.Mentor.Id == memId
                                                                        && x.EffectiveDate < DateTime.Now)
                .List<MentorHistory>();
            //.SelectList(list => list
            //    .SelectGroup(memberId => memberId.HRMember.Id))
            //.OrderBy(x => x.EffectiveDate)
            //.Desc.Select(x => x.HRMember)
            //.List<HRMember>();
            return query;


        }

        public List<TeamMember> LoadMentorTeamMember(List<long> mentorIdList, DateTime searceingDate, List<long> givenTeamMemberIdList, List<int> givenTeamMemberPinList, List<int> employmentStatusList = null)
        {
            var teamMemberList = new List<TeamMember>();
            string query = GetMentorTeamMemberQuery(mentorIdList, searceingDate, givenTeamMemberIdList, givenTeamMemberPinList, true, true, employmentStatusList);

            IQuery iQuery = Session.CreateSQLQuery(query).AddEntity("Hrm", typeof(TeamMember));
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberQueryTransfer>());
            var res = iQuery.List<TeamMemberQueryTransfer>().ToList();
            teamMemberList = res.Select(teamMemberDto => teamMemberDto.Hrm).ToList();

            return teamMemberList;
        }



        public List<long> LoadMentorTeamMemberId(List<long> mentorIdList, DateTime searceingDate, List<long> givenTeamMemberIdList, List<int> givenTeamMemberPinList, bool isRetired = true, List<int> employmentStatusList = null)
        {
            var teamMemberIdList = new List<long>();
            string query = GetMentorTeamMemberQuery(mentorIdList, searceingDate, givenTeamMemberIdList, givenTeamMemberPinList, false,isRetired, employmentStatusList);

            teamMemberIdList = Session.CreateSQLQuery(query)
                   .List<long>()
                   .ToList();
            return teamMemberIdList;
        }


        public List<int> LoadMentorTeamMemberPinList(List<long> mentorIdList, DateTime searceingDate, List<long> givenTeamMemberIdList, List<int> givenTeamMemberPinList, bool isRetired = true, List<int> employmentStatusList = null)
        {
            string query = GetMentorTeamMemberPinListQuery(mentorIdList, searceingDate, givenTeamMemberIdList, givenTeamMemberPinList, false, isRetired, employmentStatusList);
            List<int> teamMemberIdList = Session.CreateSQLQuery(query).List<int>().ToList();
            return teamMemberIdList;
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        private string GetMentorTeamMemberQuery(List<long> mentorIdList, DateTime searchingDate, List<long> givenTeamMemberIdList, List<int> givenTeamMemberPinList, bool isObject = true, bool isRetired = true, List<int> employmentStatusList = null )
        {
            string selectQuery = " Hrm.Id ";
            string filterRetired = " ";
            string filterEmploymentStatus = " ";

            if (isObject)
            {
                selectQuery = "{Hrm.*}";
            }

            if (isRetired == false)
            {
                filterRetired = " AND EmploymentStatus <> " + ((int)MemberEmploymentStatus.Retired).ToString() + " ";
            }

            if (employmentStatusList != null && employmentStatusList.Any() && !employmentStatusList.Contains(SelectionType.SelelectAll))
            {
                filterEmploymentStatus = " and EmploymentStatus IN ( " + string.Join(",", employmentStatusList) + @" ) ";
            }

            string query = @"SELECT DISTINCT " + selectQuery + @"  
                            FROM (
	                            SELECT rank() OVER(PARTITION BY eh.teammemberid ORDER BY eh.effectivedate DESC) AS r1, eh.EmploymentStatus, hrm.*
	                            FROM (
		                            SELECT rank() OVER(PARTITION BY teammemberid ORDER BY effectivedate DESC) AS r, * 
		                            FROM [dbo].[HR_MentorHistory]
		                            WHERE EffectiveDate <= '" + searchingDate.ToString("yyyy-MM-dd HH:mm:ss") + @"' and status = " + MentorHistory.EntityStatus.Active + @" 
	                            ) AS a
	                            INNER JOIN HR_TeamMember Hrm ON a.TeamMemberId = hrm.Id and Hrm.[Status] = " + TeamMember.EntityStatus.Active + @" 
	                            INNER JOIN HR_EmploymentHistory eh ON a.TeamMemberId = eh.TeamMemberId  and eh.Status = " + EmploymentHistory.EntityStatus.Active + @"
	                            WHERE a.r = 1  
		                            AND eh.EffectiveDate <= '" + searchingDate.ToString("yyyy-MM-dd HH:mm:ss") + @"'
		                            AND a.mentorid IN ( " + string.Join(",", mentorIdList) + @" )  
                            ) AS hrm
                            WHERE r1=1
	                            " + filterRetired + filterEmploymentStatus + @" ";

            if (givenTeamMemberIdList != null && givenTeamMemberIdList.Any() && !givenTeamMemberIdList.Contains(0))
                query += @"AND Hrm.Id IN ( " + string.Join(",", givenTeamMemberIdList) + ") ";

            if (givenTeamMemberPinList != null && givenTeamMemberPinList.Any()  && !givenTeamMemberPinList.Contains(0))
                query += @"AND Hrm.Pin IN ( " + string.Join(",", givenTeamMemberPinList) + " )";

            return query;
        }
        private string GetMentorTeamMemberPinListQuery(List<long> mentorIdList, DateTime searchingDate, List<long> givenTeamMemberIdList, List<int> givenTeamMemberPinList, bool isObject = true, bool isRetired = true, List<int> employmentStatusList = null )
        {
            string selectQuery = " Hrm.Pin ";
            string filterRetired = " ";
            string filterEmploymentStatus = " ";

            if (isObject)
            {
                selectQuery = "{Hrm.*}";
            }

            if (isRetired == false)
            {
                filterRetired = " AND EmploymentStatus <> " + ((int)MemberEmploymentStatus.Retired).ToString() + " ";
            }

            if (employmentStatusList != null && employmentStatusList.Any() && !employmentStatusList.Contains(SelectionType.SelelectAll))
            {
                filterEmploymentStatus = " and EmploymentStatus IN ( " + string.Join(",", employmentStatusList) + @" ) ";
            }

            string query = @"SELECT DISTINCT " + selectQuery + @"  
                            FROM (
	                            SELECT rank() OVER(PARTITION BY eh.teammemberid ORDER BY eh.effectivedate DESC) AS r1, eh.EmploymentStatus, hrm.*
	                            FROM (
		                            SELECT rank() OVER(PARTITION BY teammemberid ORDER BY effectivedate DESC) AS r, * 
		                            FROM [dbo].[HR_MentorHistory]
		                            WHERE EffectiveDate <= '" + searchingDate.ToString("yyyy-MM-dd HH:mm:ss") + @"' and status = " + MentorHistory.EntityStatus.Active + @" 
	                            ) AS a
	                            INNER JOIN HR_TeamMember Hrm ON a.TeamMemberId = hrm.Id and Hrm.[Status] = " + TeamMember.EntityStatus.Active + @" 
	                            INNER JOIN HR_EmploymentHistory eh ON a.TeamMemberId = eh.TeamMemberId  and eh.Status = " + EmploymentHistory.EntityStatus.Active + @"
	                            WHERE a.r = 1  
		                            AND eh.EffectiveDate <= '" + searchingDate.ToString("yyyy-MM-dd HH:mm:ss") + @"'
		                            AND a.mentorid IN ( " + string.Join(",", mentorIdList) + @" )  
                            ) AS hrm
                            WHERE r1=1
	                            " + filterRetired + filterEmploymentStatus + @" ";

            if (givenTeamMemberIdList != null && givenTeamMemberIdList.Any() && !givenTeamMemberIdList.Contains(0))
                query += @"AND Hrm.Id IN ( " + string.Join(",", givenTeamMemberIdList) + ") ";

            if (givenTeamMemberPinList != null && givenTeamMemberPinList.Any()  && !givenTeamMemberPinList.Contains(0))
                query += @"AND Hrm.Pin IN ( " + string.Join(",", givenTeamMemberPinList) + " )";

            return query;
        }

        #endregion
    }
}
