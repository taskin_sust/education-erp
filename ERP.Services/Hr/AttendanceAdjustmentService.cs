﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr.AttendanceSummaryServices;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.Hr
{
    public interface IAttendanceAdjustmentService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(IList<AttendanceAdjustment> hrAttendanceAdjustments);
        bool Delete(AttendanceAdjustment hrAttendanceAdjustment);
        bool RejectedAttendanceAdjustment(AttendanceAdjustment model);
        void AttendanceAdjustmentFinalize(List<long> pinList, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Single Instances Loading Function

        AttendanceAdjustment LoadById(long id);

        #endregion

        #region List Loading Function

        IList<AttendanceAdjustment> LoadPendingListForMentor(TeamMember hrMember);
        IList<AttendanceAdjustment> LoadAllRowAttendanceAdjustmentSelfMember(TeamMember hrMember, int start, int length, string orderBy,
            string orderDir);
        IList<AttendanceAdjustment> LoadAllMentorAttendanceAdjustmentRecentHistory(int start, int length, string orderBy, string toUpper,
            TeamMember hrMember, string pin, DateTime dateFrom, DateTime dateTo);
        IList<AttendanceAdjustmentHrDto> LoadPendingListForHr(List<UserMenu> userMenu, string effectiveDate, List<long> organizationIdList = null,
            List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIds = null, string pin = null);
        IList<AttendanceAdjustmentHrDto> LoadAllHrAttendanceAdjustmentRecentHistory(List<UserMenu> userMenu, int start, int length,
            string orderBy, string toUpper, string pin, DateTime dateFrom, DateTime dateTo, List<long> organizationIdList,
            List<long> branchIdList, List<long> campusIdList, List<long> departmentIdList);

        #endregion

        #region Others Function

        int CountMentorAttendanceAdjustmentRecentHistory(TeamMember hrMember, string pin, DateTime dateFrom, DateTime dateTo);
        int CountHrAttendanceAdjustmentRecentHistory(List<UserMenu> userMenu, string pin, DateTime dateFrom, DateTime dateTo,
            List<long> organizationIdList, List<long> branchIdList, List<long> campusIdList, List<long> departmentIdList);
        int CountRowAttendanceAdjustmentSelfMember(TeamMember hrMember);

        DateTime? GetTeamMemberAttendanceAdjustmentPostDateTime(long? teamMemberId, int? pin = null);

        #endregion

        #region Helper Function

        #endregion
    }
    public class AttendanceAdjustmentService : BaseService, IAttendanceAdjustmentService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IAttendanceAdjustmentDao _hrAttendanceAdjustmentDao;
        private readonly IMentorHistoryDao _mentorHistoryDao;
        private readonly AttendanceSummaryServiceHelper _attendanceSummaryServiceHelper;
        private readonly IHolidayWorkDao _holidayWorkDao;
        private readonly IAllowanceDao _allowanceDao;
        private readonly INightWorkDao _nightWorkDao;
        private readonly IOvertimeDao _overtimeDao;
        private readonly IDayOffAdjustmentDao _dayOffAdjustmentDao;
        private readonly ISalarySheetDao _salarySheetDao;
        public AttendanceAdjustmentService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrAttendanceAdjustmentDao = new AttendanceAdjustmentDao() { Session = session };
            _mentorHistoryDao = new MentorHistoryDao { Session = session };
            _attendanceSummaryServiceHelper = new AttendanceSummaryServiceHelper(session);
            _holidayWorkDao = new HolidayWorkDao() { Session = session };
            _allowanceDao = new AllowanceDao { Session = session };
            _nightWorkDao = new NightWorkDao { Session = session };
            _overtimeDao = new OvertimeDao { Session = session };
            _dayOffAdjustmentDao = new DayOffAdjustmentDao { Session = session };
            _salarySheetDao = new SalarySheetDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public void SaveOrUpdate(IList<AttendanceAdjustment> hrAttendanceAdjustments)
        {
            ITransaction transaction = null;
            try
            {
                if (hrAttendanceAdjustments != null && hrAttendanceAdjustments.Any())
                {
                    using (transaction = Session.BeginTransaction())
                    {
                        foreach (var hrAttendanceAdj in hrAttendanceAdjustments)
                        {
                            var hrAttendanceAd = ValidateAttendanceAdjustment(hrAttendanceAdj);
                            bool check = _hrAttendanceAdjustmentDao.HasDuplicate(hrAttendanceAd);
                            if (check)
                                throw new DuplicateEntryException("[PIN : " + hrAttendanceAdj.TeamMember.Pin +
                                                                  "] Adjustment Already Exist For Date(" +
                                                                  hrAttendanceAdj.Date.Value.ToString("dd MMM, yyyy") +
                                                                  ") ");

                            AttendanceAdjustment attendanceAdjustment = hrAttendanceAdj;
                            if (attendanceAdjustment.Id > 0)
                            {
                                attendanceAdjustment = _hrAttendanceAdjustmentDao.LoadById(hrAttendanceAdj.Id);
                                attendanceAdjustment.TeamMember = hrAttendanceAdj.TeamMember;
                                attendanceAdjustment.Reason = hrAttendanceAdj.Reason;
                                attendanceAdjustment.StartTime = hrAttendanceAdj.StartTime;
                                attendanceAdjustment.EndTime = hrAttendanceAdj.EndTime;
                                attendanceAdjustment.Date = hrAttendanceAdj.Date;
                                if (hrAttendanceAdj.AdjustmentStatus != null)
                                    attendanceAdjustment.AdjustmentStatus = hrAttendanceAdj.AdjustmentStatus;
                            }

                            _hrAttendanceAdjustmentDao.SaveOrUpdate(attendanceAdjustment);

                            var hrAtAdjustmentLog = new AttendanceAdjustmentLog
                            {
                                AttendanceAdjustment = attendanceAdjustment,
                                Date = attendanceAdjustment.Date,
                                StartTime = attendanceAdjustment.StartTime,
                                EndTime = attendanceAdjustment.EndTime,
                                AdjustmentStatus = attendanceAdjustment.AdjustmentStatus,
                                Reason = attendanceAdjustment.Reason,
                                CreationDate = DateTime.Now,
                                CreateBy = GetCurrentUserId()
                            };
                            Session.Save(hrAtAdjustmentLog);
                            if (attendanceAdjustment.AdjustmentStatus.Value == (int)AttendanceAdjustmentStatus.Approved || attendanceAdjustment.AdjustmentStatus.Value == (int)AttendanceAdjustmentStatus.Rejected)
                            {
                                bool isUpdateAttendenceSummery = _attendanceSummaryServiceHelper.UpdateAttendenceSummeryByPinAndDateTime(attendanceAdjustment.TeamMember.Pin, attendanceAdjustment.Date.Value);
                            }

                        }
                        transaction.Commit();
                    }
                }
                else
                {
                    throw new InvalidDataException("Attendence adjustment list can't empty.");
                }
            }
            catch (DuplicateEntryException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public bool Delete(AttendanceAdjustment hrAttendanceAdjustment)
        {
            ITransaction transaction = null;


            try
            {

                if (hrAttendanceAdjustment == null)
                    throw new InvalidDataException("Invalid attendance adjustment");

                #region Is Post Check
                var lastPostDate = GetTeamMemberAttendanceAdjustmentPostDateTime(hrAttendanceAdjustment.TeamMember.Id, hrAttendanceAdjustment.TeamMember.Pin);
                if (lastPostDate != null && lastPostDate >= hrAttendanceAdjustment.Date)
                {
                    throw new InvalidDataException("[PIN : " + hrAttendanceAdjustment.TeamMember.Pin + "] Can only cancel attendance adjustment after date(" + lastPostDate.Value.ToString("dd MMM, yyyy") + ") ");
                }
                #endregion

                using (transaction = Session.BeginTransaction())
                {
                    hrAttendanceAdjustment.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Cancel;
                    _hrAttendanceAdjustmentDao.Update(hrAttendanceAdjustment);

                    var hrAtAdjustmentLog = new AttendanceAdjustmentLog
                    {
                        Date = hrAttendanceAdjustment.Date,
                        StartTime = hrAttendanceAdjustment.StartTime,
                        EndTime = hrAttendanceAdjustment.EndTime,
                        AdjustmentStatus = hrAttendanceAdjustment.AdjustmentStatus,
                        AttendanceAdjustment = hrAttendanceAdjustment,
                        Reason = hrAttendanceAdjustment.Reason,
                        CreationDate = DateTime.Now,
                        CreateBy = GetCurrentUserId()
                    };
                    Session.Save(hrAtAdjustmentLog);
                    bool isUpdateAttendenceSummery = _attendanceSummaryServiceHelper.UpdateAttendenceSummeryByPinAndDateTime(hrAttendanceAdjustment.TeamMember.Pin, hrAttendanceAdjustment.Date.Value);
                    transaction.Commit();
                    return true;
                }
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }

        }

        public bool RejectedAttendanceAdjustment(AttendanceAdjustment model)
        {
            try
            {
                #region

                if (model == null)
                    throw new InvalidDataException("Attendance Adjustment can't empty");

                if (model.TeamMember==null)
                    throw new InvalidDataException("Team member of Attendance Adjustment can't empty");

                if (model.Date==null)
                    throw new InvalidDataException("Date of Attendance Adjustment can't empty");

                //Holiday Work
                if (_holidayWorkDao.CheckhasHolidayWorkOnADate(model.TeamMember.Id, model.Date.Value))
                {
                    throw new DependencyException("This adjustment has one or more Holiday Work. You can't reject this.");
                }

                //Daily Allowance Work
                if (_allowanceDao.CheckHasAllounceOnADate(model.TeamMember.Id, model.Date.Value))
                {
                    throw new DependencyException("This adjustment has one or more Daily Allowance. You can't reject this.");
                }

                //Night Work
                if (_nightWorkDao.CheckHasNightWorkOnADate(model.TeamMember.Id, model.Date.Value))
                {
                    throw new DependencyException("This adjustment has one or more Night Work. You can't reject this.");
                }

                //OverTime
                if (_overtimeDao.CheckHasOverTimeOnADate(model.TeamMember.Id, model.Date.Value))
                {
                    throw new DependencyException("This adjustment has one or more Over Time. You can't reject this.");
                }

                //Day Off Adjustment
                if (_dayOffAdjustmentDao.CheckHasDayOffAdjustmentOnADate(model.TeamMember.Id, model.Date.Value))
                {
                    throw new DependencyException("This adjustment has one or more Day off Adjustment. You can't reject this.");
                }


                #endregion

                model.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Rejected;
                IList<AttendanceAdjustment> attendanceAdjustments = new List<AttendanceAdjustment>();
                attendanceAdjustments.Add(model);
                //Update(model);
                SaveOrUpdate(attendanceAdjustments);
                return true;
            }
            catch (DuplicateEntryException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public void AttendanceAdjustmentFinalize(List<long> pinList, DateTime dateFrom, DateTime dateTo)
        {
            ITransaction transaction = null;
            try
            {
                if(pinList==null || !pinList.Any())
                    throw new NullObjectException("No Pin list Found.");

                IList<AttendanceAdjustment> attendanceAdjustments = _hrAttendanceAdjustmentDao.LoadAttendanceAdjustments(pinList, dateFrom, dateTo);
                if (attendanceAdjustments==null || !attendanceAdjustments.Any())
                    throw new NullObjectException("No Attendance Adjustment Found.");
                foreach (var attendanceAdjustment in attendanceAdjustments)
                {
                    attendanceAdjustment.IsPost = true;
                    _hrAttendanceAdjustmentDao.Update(attendanceAdjustment);
                    var hrAtAdjustmentLog = new AttendanceAdjustmentLog
                    {
                        AttendanceAdjustment = attendanceAdjustment,
                        Date = attendanceAdjustment.Date,
                        StartTime = attendanceAdjustment.StartTime,
                        EndTime = attendanceAdjustment.EndTime,
                        AdjustmentStatus = attendanceAdjustment.AdjustmentStatus,
                        Reason = attendanceAdjustment.Reason,
                        IsPost = true,
                        CreationDate = DateTime.Now,
                        CreateBy = GetCurrentUserId()
                    };
                    Session.Save(hrAtAdjustmentLog);
                }

            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public IList<AttendanceAdjustment> LoadPendingListForMentor(TeamMember hrMember)
        {
            try
            {
                if (hrMember == null)
                    throw new InvalidDataException("Invalid team member.");
                List<long> teamMemberIdList =
                    _mentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(hrMember.Id),
                        DateTime.Now.Date, null, null);
                return _hrAttendanceAdjustmentDao.LoadPendingListForMentor(hrMember, teamMemberIdList);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public AttendanceAdjustment LoadById(long id)
        {
            try
            {
                return _hrAttendanceAdjustmentDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<AttendanceAdjustment> LoadAllRowAttendanceAdjustmentSelfMember(TeamMember hrMember, int start, int length, string orderBy, string orderDir)
        {
            try
            {
                return _hrAttendanceAdjustmentDao.LoadAllRowAttendanceAdjustmentSelfMember(hrMember, start, length, orderBy, orderDir);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<AttendanceAdjustment> LoadAllMentorAttendanceAdjustmentRecentHistory(int start, int length, string orderBy, string toUpper, TeamMember hrMember, string pin, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (hrMember == null)
                    throw new InvalidDataException("Invalid user menu");
                List<long> authoTeamMemberIdList =
                    _mentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(hrMember.Id), dateTo, null,
                        (!String.IsNullOrEmpty(pin)) ? _commonHelper.ConvertIdToList(Convert.ToInt32(pin)) : null);
                return _hrAttendanceAdjustmentDao.LoadAllMentorAttendanceAdjustmentRecentHistory(start, length, orderBy,
                    toUpper, authoTeamMemberIdList, dateFrom, dateTo);
                //return _hrAttendanceAdjustmentDao.LoadAllMentorAttendanceAdjustmentRecentHistory(start, length, orderBy, toUpper, hrMember, pin, date);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<AttendanceAdjustmentHrDto> LoadPendingListForHr(List<UserMenu> userMenu, string effectiveDate, List<long> organizationIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIds = null, string pin = null)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid user menu");

                List<long> organizationAuthIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationIdList != null && organizationIdList.Contains(SelectionType.SelelectAll)) ? null : organizationIdList);
                List<long> branchAuthIdList = AuthHelper.LoadBranchIdList(userMenu, (organizationIdList != null && organizationIdList.Contains(SelectionType.SelelectAll)) ? null : organizationIdList, null, (branchIdList != null && branchIdList.Contains(SelectionType.SelelectAll)) ? null : branchIdList);


                return _hrAttendanceAdjustmentDao.LoadPendingListForHr(effectiveDate, organizationAuthIdList, branchAuthIdList, campusIdList, departmentIds, pin);

            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<AttendanceAdjustmentHrDto> LoadAllHrAttendanceAdjustmentRecentHistory(List<UserMenu> userMenu, int start, int length, string orderBy, string toUpper, string pin, DateTime dateFrom, DateTime dateTo, List<long> organizationIdList, List<long> branchIdList, List<long> campusIdList, List<long> departmentIdList)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid user menu");

                List<long> authorizedOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationIdList != null && !organizationIdList.Contains(SelectionType.SelelectAll)) ? organizationIdList : null);

                if (authorizedOrganizationIdList == null || !authorizedOrganizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                List<long> autorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authorizedOrganizationIdList, null, (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll)) ? branchIdList : null);

                return _hrAttendanceAdjustmentDao.LoadAllHrAttendanceAdjustmentRecentHistory(start, length, orderBy, toUpper, pin, dateFrom, dateTo, autorizedBranchIdList, campusIdList, departmentIdList);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int CountMentorAttendanceAdjustmentRecentHistory(TeamMember hrMember, string pin, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (hrMember == null)
                    throw new InvalidDataException("Invalid team member");

                List<long> authoTeamMemberIdList = _mentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(hrMember.Id), dateTo, null, (!String.IsNullOrEmpty(pin)) ? _commonHelper.ConvertIdToList(Convert.ToInt32(pin)) : null);
                return _hrAttendanceAdjustmentDao.CountMentorAttendanceAdjustmentRecentHistory(authoTeamMemberIdList, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int CountHrAttendanceAdjustmentRecentHistory(List<UserMenu> userMenu, string pin, DateTime dateFrom, DateTime dateTo, List<long> organizationIdList,
            List<long> branchIdList, List<long> campusIdList, List<long> departmentIdList)
        {
            try
            {

                if (userMenu == null)
                    throw new InvalidDataException("Invalid user menu");

                List<long> authorizedOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu,
                    (!organizationIdList.Contains(SelectionType.SelelectAll)) ? organizationIdList : null);

                if (authorizedOrganizationIdList != null && !authorizedOrganizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                List<long> autorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authorizedOrganizationIdList,
                    null, (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll)) ? branchIdList : null);

                return _hrAttendanceAdjustmentDao.CountHrAttendanceAdjustmentRecentHistory(pin, dateFrom, dateTo, autorizedBranchIdList, campusIdList, departmentIdList);
            }

            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int CountRowAttendanceAdjustmentSelfMember(TeamMember hrMember)
        {
            try
            {
                return _hrAttendanceAdjustmentDao.CountRowAttendanceAdjustmentSelfMember(hrMember);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public DateTime? GetTeamMemberAttendanceAdjustmentPostDateTime(long? teamMemberId, int? pin = null)
        {
            DateTime? postDateTime = null;
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(teamMemberId, pin);

            AttendanceAdjustment attendanceAdjustment =
                _hrAttendanceAdjustmentDao.GetTeamMemberLastIsPostAttendanceAdjustment(teamMemberId, pin);

            // LeaveApplicationDetails leaveApplicationDetails = _leaveApplicationDetailsDao.GetTeamMemberLastIsPostLeaveApplicationDetails(teamMemberId, pin);

            if (salarySheet != null && attendanceAdjustment != null)
            {
                postDateTime = salarySheet.EndDate;
                if (attendanceAdjustment.Date > postDateTime)
                    postDateTime = attendanceAdjustment.Date;
            }
            else if (salarySheet != null)
            {
                postDateTime = salarySheet.EndDate;
            }
            else if (attendanceAdjustment != null)
            {
                postDateTime = attendanceAdjustment.Date;
            }
            return postDateTime;
        }

        #endregion

        #region Helper function

        private AttendanceAdjustment ValidateAttendanceAdjustment(AttendanceAdjustment attendanceAdjustment)
        {
            if (attendanceAdjustment == null)
                throw new InvalidDataException("Invalid Attendance adjustment");

            DateTime attendenceDate = attendanceAdjustment.Date.Value.Date;
            TeamMember member = attendanceAdjustment.TeamMember;

            if (attendanceAdjustment.Id > 0)
            {
                Session.Evict(attendanceAdjustment);
                AttendanceAdjustment prevAttenAdj = _hrAttendanceAdjustmentDao.LoadById(attendanceAdjustment.Id);
                if (member != null && prevAttenAdj != null && member.Id != prevAttenAdj.TeamMember.Id)
                {
                    throw new InvalidDataException("[PIN : " + member.Pin.ToString() + "] Invalid PIN.");
                }
            }

            #region Is Post Check
            var lastPostDate = GetTeamMemberAttendanceAdjustmentPostDateTime(member.Id, member.Pin);
            if (lastPostDate != null && lastPostDate >= attendenceDate)
            {
                if (attendanceAdjustment.AdjustmentStatus == (int)AttendanceAdjustmentStatus.Pending)
                {
                    throw new InvalidDataException("You Can only apply for attendance adjustment after date(" + lastPostDate.Value.ToString("dd MMM, yyyy") + ") ");
                }
                else if (attendanceAdjustment.AdjustmentStatus == (int)AttendanceAdjustmentStatus.Approved)
                {
                    throw new InvalidDataException("[PIN : " + member.Pin + "(" + attendenceDate.ToString("dd MMM, yyyy") + ")" + "] This Adjustment is already finalized(" + lastPostDate.Value.ToString("dd MMM, yyyy") + ")" + ". You can't Approve this.");
                }
                else if (attendanceAdjustment.AdjustmentStatus == (int)AttendanceAdjustmentStatus.Rejected)
                {
                    throw new InvalidDataException("[PIN : " + member.Pin + "(" + attendenceDate.ToString("dd MMM, yyyy") + ")" + "] This Adjustment is already finalized(" + lastPostDate.Value.ToString("dd MMM, yyyy") + ")" + " You can't Reject this.");
                }
                else if (attendanceAdjustment.AdjustmentStatus == (int)AttendanceAdjustmentStatus.Cancel)
                {
                    throw new InvalidDataException("[PIN : " + member.Pin +
                                                    "(" + attendenceDate.ToString("dd MMM, yyyy") + ")" + "] This Adjustment is already finalized(" + lastPostDate.Value.ToString("dd MMM, yyyy") + ")" + ". You can't Cancel this.");
                }
                else
                {
                    throw new InvalidDataException("[PIN : " + member.Pin +
                                                    "(" + attendenceDate.ToString("dd MMM, yyyy") + ")" + "] This Adjustment is already finalized(" + lastPostDate.Value.ToString("dd MMM, yyyy") + ")" + ". You can't Update this.");
                }

            }
            #endregion

            attendanceAdjustment.Reason = attendanceAdjustment.Reason.Trim();
            if (attendanceAdjustment.Reason == "")
            {
                throw new InvalidDataException("[PIN : " + member.Pin.ToString() + "] Reason is empty.");
            }

            EmploymentHistory memberEmploymentHistory = member.EmploymentHistory
                            .Where(s => s.EffectiveDate <= DateTime.Now && s.Status == EmploymentHistory.EntityStatus.Active)
                            .OrderByDescending(s => s.EffectiveDate)
                            .ThenByDescending(s => s.Id)
                            .FirstOrDefault();

            Organization memberOrganization = memberEmploymentHistory.Department.Organization;
            DateTime orgStartTime = attendenceDate.AddHours(memberOrganization.AttendanceStartTime.Hour).AddMinutes(memberOrganization.AttendanceStartTime.Minute);
            DateTime orgEndTime = orgStartTime.AddDays(1).AddMinutes(-1);

            attendanceAdjustment.Date = attendenceDate;

            attendanceAdjustment.StartTime = attendenceDate.AddHours(attendanceAdjustment.StartTime.Value.Hour).AddMinutes(attendanceAdjustment.StartTime.Value.Minute);
            attendanceAdjustment.EndTime = attendenceDate.AddHours(attendanceAdjustment.EndTime.Value.Hour).AddMinutes(attendanceAdjustment.EndTime.Value.Minute);

            if (attendanceAdjustment.StartTime.Value < attendanceAdjustment.EndTime.Value && orgStartTime <= attendanceAdjustment.StartTime.Value)
            {
                // VALUE IS OK
            }
            else if (attendanceAdjustment.StartTime.Value < attendanceAdjustment.EndTime.Value && orgStartTime > attendanceAdjustment.StartTime.Value && orgStartTime > attendanceAdjustment.EndTime.Value)
            {
                attendanceAdjustment.StartTime = attendenceDate.AddDays(1).AddHours(attendanceAdjustment.StartTime.Value.Hour).AddMinutes(attendanceAdjustment.StartTime.Value.Minute);
                attendanceAdjustment.EndTime = attendenceDate.AddDays(1).AddHours(attendanceAdjustment.EndTime.Value.Hour).AddMinutes(attendanceAdjustment.EndTime.Value.Minute);

                // Previous Day's Time Adjustment
                //attendanceAdjustment.Date = attendanceAdjustment.Date.Value.AddDays(-1);
                //attendanceAdjustment.Date = attendanceAdjustment.StartTime.Value.AddDays(-1);
            }
            else if (attendanceAdjustment.StartTime.Value > attendanceAdjustment.EndTime.Value && orgEndTime > attendanceAdjustment.EndTime.Value.AddDays(1))
            {
                attendanceAdjustment.EndTime = attendanceAdjustment.EndTime.Value.AddDays(1);
            }
            else
            {
                throw new InvalidDataException("[PIN : " + member.Pin.ToString() + "] You entered invalid Time. Time Range(" + orgStartTime.ToString("yyyy-MM-dd hh:mm tt") + " To " + orgEndTime.ToString("yyyy-MM-dd hh:mm tt") + ")");
            }

            return attendanceAdjustment;
        }

        #endregion
    }
}