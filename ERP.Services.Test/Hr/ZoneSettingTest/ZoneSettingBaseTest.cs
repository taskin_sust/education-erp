﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.ZoneSettingTest
{
    public class ZoneSettingBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal ZoneSettingService _zoneSettingService;
        internal OrganizationService _organizationService;
        internal List<long> IdList = new List<long>();

        private ISession _session;
        internal ZoneSettingFactory _zoneSettingFactory;

        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal const long OrgId = 1;

        #endregion

        public ZoneSettingBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _zoneSettingFactory = new ZoneSettingFactory(null, _session);
            _zoneSettingService = new ZoneSettingService(_session);
            _organizationService = new OrganizationService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _zoneSettingFactory.Cleanup();
            base.Dispose();
        }
    }   
}
