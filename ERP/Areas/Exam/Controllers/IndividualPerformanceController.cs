﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Students;

namespace UdvashERP.Areas.Exam.Controllers
{
    [UerpArea("Exam")]
    [Authorize]
    [AuthorizeAccess]

    public class IndividualPerformanceController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private readonly IIndividualPerformanceService _individualPerformanceService;
        private readonly IExamsSettings _examsSettingsService;
        private readonly IStudentProgramService _studentProgramService;
        private List<UserMenu> _userMenu;
        private readonly IStudentImagesMediaService _studentImagesMediaService;
        private readonly ICourseService _courseService;
        public IndividualPerformanceController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _examsSettingsService = new ExamsSettingsService(session);
                _individualPerformanceService = new IndividualPerformanceService(session);
                _studentProgramService = new StudentProgramService(session);
                _commonHelper = new CommonHelper();
                _studentImagesMediaService = new StudentImagesMediaService();
                _courseService = new CourseService(session);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Individual Performance Report
        public ActionResult IndividualPerformanceReport(string message = "", string roll = "")
        {
            try
            {
                if (!String.IsNullOrEmpty(message))
                {
                    ViewBag.ErrorMessage = message;
                    ViewBag.roll = roll;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }
        [HttpGet]
        public ActionResult IndividualPerformanceReportView(string programRoll)
        {
            try
            {
                if (!String.IsNullOrEmpty(programRoll) && programRoll.Trim().Length == 11 &&
                                System.Text.RegularExpressions.Regex.IsMatch(programRoll, "^[0-9]*$"))
                {
                    programRoll = programRoll.Trim();
                    var studentProgram = _studentProgramService.GetStudentProgram(programRoll);
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    if (studentProgram != null)
                    {
                        var message = _studentProgramService.CheckAuthorizationForStudentProgram(_userMenu, studentProgram);
                        if (message != "")
                            return RedirectToAction("IndividualPerformanceReport", new { message = "You are not authorized to view this report", roll = programRoll });

                        //decimal totalObtainedMarks = 0;
                        //decimal totalFullMarks = 0;
                        //decimal finalMeritPosition = 0;
                        List<IndividualMeritListNewDto> individualMeritListReport = _individualPerformanceService.LoadIndividualMeritListReport(studentProgram).OrderBy(x => x.Name).ToList();
                        //var individualReportResult = _individualPerformanceService.LoadIndividualReport(programRoll, out totalObtainedMarks, out totalFullMarks, out finalMeritPosition);
                        
                        StudentMediaImage mediaStudentImages = new StudentMediaImage();
                        if (studentProgram.StudentImage.Count > 0)
                        {
                            mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(
                             studentProgram.StudentImage[0].Id);
                            studentProgram.MediaStudentImage = mediaStudentImages != null
                                ? mediaStudentImages
                                : new StudentMediaImage();
                        }
                        ViewBag.individualReportResultList = individualMeritListReport;
                        ViewBag.totalObtainedMarks = individualMeritListReport.Sum(x=>x.Total);
                        ViewBag.FinalMeritPosition = individualMeritListReport.Select(x=>x.FMP).First();
                        ViewBag.totalFullMarks = individualMeritListReport.Sum(x=>x.FullMarks);
                        var totalCourse = individualMeritListReport.Select(x=>x.CourseId).Distinct().ToList();

                        var courtList = totalCourse.Select(tc => _courseService.GetCourse(tc)).ToList();
                        ViewBag.CourseList = courtList;

                        //ViewBag.individualReportResultList = individualReportResult.OrderBy(x => x.ElementAt(0));
                        //ViewBag.totalObtainedMarks = totalObtainedMarks;
                        //ViewBag.FinalMeritPosition = finalMeritPosition;
                        //ViewBag.totalFullMarks = totalFullMarks;
                        ViewBag.StudentProgram = studentProgram;
                        var examsSettings = _examsSettingsService.LoadByType(ExamsSettingsType.IndividualPerformanceReport);
                        int headerFont = 16, tableHeaderFont = 14, tableFont = 12;
                        if (examsSettings.Count > 0)
                        {
                            headerFont = examsSettings[0].Value;
                            tableHeaderFont = examsSettings[1].Value;
                            tableFont = examsSettings[2].Value;
                        }
                        ViewBag.headerFont = headerFont;
                        ViewBag.tableHeaderFont = tableHeaderFont;
                        ViewBag.tableFont = tableFont;
                        return View("IndividualPerformanceReportView");
                    }
                    return RedirectToAction("IndividualPerformanceReport",
                            new { message = "Program Roll not found", roll = programRoll });
                }
                return RedirectToAction("IndividualPerformanceReport", new { message = "Program Roll must contain 11 digits number", roll = programRoll });

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("IndividualPerformanceReport", new { message = WebHelper.CommonErrorMessage, roll = programRoll });
            }
        }
        [HttpGet]
        #region Individua lPerformance Report For MeritList
        public ActionResult IndividualPerformanceReportCalledFromMeritList(string programRoll)
        {
            try
            {
                if (!String.IsNullOrEmpty(programRoll) && programRoll.Trim().Length == 11 &&
                                System.Text.RegularExpressions.Regex.IsMatch(programRoll, "^[0-9]*$"))
                {
                    programRoll = programRoll.Trim();
                    var studentProgram = _studentProgramService.GetStudentProgram(programRoll);
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    if (studentProgram != null)
                    {
                        var message = _studentProgramService.CheckAuthorizationForStudentProgram(_userMenu, studentProgram);
                        if (message != "")
                            return RedirectToAction("IndividualPerformanceReport", new { message = "You are not authorized to view this report", roll = programRoll });

                        decimal totalObtainedMarks = 0;
                        decimal totalFullMarks = 0;
                        decimal finalMeritPosition = 0;
                        var individualReportResult = _individualPerformanceService.LoadIndividualReport(programRoll,
                            out totalObtainedMarks, out totalFullMarks, out finalMeritPosition);
                        ViewBag.individualReportResultList = individualReportResult;
                        ViewBag.totalObtainedMarks = totalObtainedMarks;
                        ViewBag.totalFullMarks = totalFullMarks;
                        ViewBag.StudentProgram = studentProgram;
                        var examsSettings = _examsSettingsService.LoadByType(ExamsSettingsType.IndividualPerformanceReport);
                        int headerFont = 16, tableHeaderFont = 14, tableFont = 12;
                        if (examsSettings.Count > 0)
                        {
                            headerFont = examsSettings[0].Value;
                            tableHeaderFont = examsSettings[1].Value;
                            tableFont = examsSettings[2].Value;
                        }
                        ViewBag.headerFont = headerFont;
                        ViewBag.tableHeaderFont = tableHeaderFont;
                        ViewBag.tableFont = tableFont;
                        return View("IndividualPerformanceReportView");
                    }
                    return RedirectToAction("IndividualPerformanceReport",
                            new { message = "Program Roll not found", roll = programRoll });
                }
                return RedirectToAction("IndividualPerformanceReport", new { message = "Program Roll must contain 11 digits number", roll = programRoll });

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("IndividualPerformanceReport", new { message = WebHelper.CommonErrorMessage, roll = programRoll });

            }
        }
        #endregion
        #endregion

        #region Ajax Request Function
        #region Save Fonts Settings
        [HttpPost]
        public JsonResult SaveFontsSettings(int headerFont, int tableHeaderFont, int tableFont)
        {
            try
            {

                var examSettings = _examsSettingsService.LoadByType(ExamsSettingsType.IndividualPerformanceReport);
                if (examSettings.Count > 0)
                {
                    examSettings[0].Value = headerFont;
                    examSettings[1].Value = tableHeaderFont;
                    examSettings[2].Value = tableFont;
                    for (int i = 0; i < 3; i++)
                    {
                        _examsSettingsService.Update(examSettings[i]);
                    }
                }
                else
                {
                    var examSettingsList = new List<ExamsSettings>();
                    var headerFontObject = new ExamsSettings();
                    headerFontObject.Type = ExamsSettingsType.IndividualPerformanceReport;
                    headerFontObject.KeyName = "HeaderFont";
                    headerFontObject.Value = headerFont;
                    examSettingsList.Add(headerFontObject);

                    var tableHeaderFontObject = new ExamsSettings();
                    tableHeaderFontObject.Type = ExamsSettingsType.IndividualPerformanceReport;
                    tableHeaderFontObject.KeyName = "TableHeaderFont";
                    tableHeaderFontObject.Value = tableHeaderFont;
                    examSettingsList.Add(tableHeaderFontObject);

                    var tableFontObject = new ExamsSettings();
                    tableFontObject.Type = ExamsSettingsType.IndividualPerformanceReport;
                    tableFontObject.KeyName = "TableFont";
                    tableFontObject.Value = tableFont;
                    examSettingsList.Add(tableFontObject);

                    _examsSettingsService.Save(examSettingsList);
                }
                return Json(new { IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred! Retry.";
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion
        #endregion
    }
}
