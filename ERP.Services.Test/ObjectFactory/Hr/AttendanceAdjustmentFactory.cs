﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class AttendanceAdjustmentFactory : ObjectFactoryBase<AttendanceAdjustment>
    {
        private TeamMemberFactory _teamMemberFactory;
        private TestBaseService<AttendanceAdjustmentLog> _attandanceAdjustmentLogService;


        private readonly IAttendanceAdjustmentService _attendanceAdjustmentService;
        public AttendanceAdjustmentFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _attendanceAdjustmentService = new AttendanceAdjustmentService(session);
            _attandanceAdjustmentLogService = new TestBaseService<AttendanceAdjustmentLog>(session);
        }


        #region Create
        public AttendanceAdjustmentFactory Create()
        {
            Object = new AttendanceAdjustment()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name + "_A" + DateTime.Now.ToString("ss"),
                CreateBy = 1,
                ModifyBy = 1,
                Status = Department.EntityStatus.Active,
                Date = DateTime.Now,
                StartTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 10, 10),
                EndTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 10, 10),
                Reason = "Testing Cause",
                AdjustmentStatus = (int)AttendanceAdjustmentStatus.Pending
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public AttendanceAdjustmentFactory CreateWith(string name, DateTime adjustmentDate, DateTime startTime, DateTime endDate, string reason,
            int adjustmentStatus)
        {
            Object = new AttendanceAdjustment()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = 1,
                Date = adjustmentDate,
                StartTime = startTime,
                EndTime = endDate,
                Reason = reason,
                AdjustmentStatus = adjustmentStatus,
            };
            SingleObjectList.Add(Object);
            return this;
        }


        public AttendanceAdjustmentFactory CreateWith(string name, DateTime date, DateTime startTime, DateTime endDate, string reason = "Official Cause",
            int adjustmentStatus = (int) AttendanceAdjustmentStatus.Pending, long id = 0, int status = 1)
        {
            Object = new AttendanceAdjustment()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                Name = name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = status,
                Date = date,
                StartTime = startTime,
                EndTime = endDate,
                Reason = reason,
                AdjustmentStatus = adjustmentStatus
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public AttendanceAdjustmentFactory WithTeamMember(List<EmploymentHistory> empObjList, List<MaritalInfo> maritalInfoObjList, List<ShiftWeekendHistory> swHistoryObjList)
        {
            var campus = empObjList.FirstOrDefault().Campus;
            _teamMemberFactory.Create()
                .WithEmploymentHistory(empObjList)
                .WithMaritalInfo(maritalInfoObjList)
                .WithShiftWeekend(swHistoryObjList)
                .WithUserProfile(campus)
                .Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }
        public AttendanceAdjustmentFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public AttendanceAdjustmentFactory Persist()
        {
            if (ObjectList != null && ObjectList.Count > 0)
            {
                _attendanceAdjustmentService.SaveOrUpdate(ObjectList);
            }
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                _attendanceAdjustmentService.SaveOrUpdate(SingleObjectList);
            }
            return this;
        }

        public AttendanceAdjustmentFactory CreateMoreWithPendingAdjustment(DateTime dateFrom, TeamMember tm)
        {
            int totalDaysOfMonth = DateTime.DaysInMonth(dateFrom.Year, dateFrom.Month);
            //var lastDayOfMonth = DateTime.DaysInMonth(dateFrom.Year, dateFrom.Month);
            //var startDate = new DateTime(dateFrom.Year, dateFrom.Month, 1);
            //var endDate = new DateTime(dateTo.Year, dateTo.Month, lastDayOfMonth);

            for (int i = 1; i <= totalDaysOfMonth; i++)
            {

                var obj = new AttendanceAdjustment()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = "Testing_" + DateTime.Now.Millisecond.ToString(CultureInfo.CurrentCulture) + "_" + i.ToString(CultureInfo.CurrentCulture),
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = AttendanceAdjustment.EntityStatus.Active,
                    Date = new DateTime(dateFrom.Year, dateFrom.Month, i),
                    StartTime = Convert.ToDateTime(dateFrom.ToShortDateString() + " " + TimeSpan.Parse("9:00:00.000")),
                    EndTime = Convert.ToDateTime(dateFrom.ToShortDateString() + " " + TimeSpan.Parse("18:00:00.000")),
                    Reason = "Testing Adjustment_" + i.ToString(CultureInfo.CurrentCulture),
                    // AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved,
                    IsPost = false,
                    TeamMember = tm
                };
                if (i == 1 || i == 13 || i == 25)
                    obj.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Pending;
                else
                    obj.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Pending;

                ObjectList.Add(obj);
            }
            return this;
        }

        public AttendanceAdjustmentFactory CreateMore(DateTime dateFrom, TeamMember tm)
        {
            int totalDaysOfMonth = DateTime.DaysInMonth(dateFrom.Year, dateFrom.Month);
            //var lastDayOfMonth = DateTime.DaysInMonth(dateFrom.Year, dateFrom.Month);
            //var startDate = new DateTime(dateFrom.Year, dateFrom.Month, 1);
            //var endDate = new DateTime(dateTo.Year, dateTo.Month, lastDayOfMonth);

            for (int i = 1; i <= totalDaysOfMonth; i++)
            {

                var obj = new AttendanceAdjustment()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = "Testing_" + DateTime.Now.Millisecond.ToString(CultureInfo.CurrentCulture) + "_" + i.ToString(CultureInfo.CurrentCulture),
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = AttendanceAdjustment.EntityStatus.Active,
                    Date = new DateTime(dateFrom.Year, dateFrom.Month, i),
                    StartTime = Convert.ToDateTime(dateFrom.ToShortDateString() + " " + TimeSpan.Parse("9:00:00.000")),
                    EndTime = Convert.ToDateTime(dateFrom.ToShortDateString() + " " + TimeSpan.Parse("18:00:00.000")),
                    Reason = "Testing Adjustment_" + i.ToString(CultureInfo.CurrentCulture),
                    AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved,
                    IsPost = false,
                    TeamMember = tm
                };
                ObjectList.Add(obj);
            }
            return this;
        }


        #endregion

        #region Others
        #endregion

        #region CleanUp

        public void LogCleanUp(AttendanceAdjustmentFactory attendanceAdjustmentFactory)
        {
            if (attendanceAdjustmentFactory.ObjectList.Any())
            {
                DeleteLog(attendanceAdjustmentFactory.ObjectList);
            }
            if (attendanceAdjustmentFactory.SingleObjectList.Any())
            {
                DeleteLog(attendanceAdjustmentFactory.SingleObjectList);
            }
        }
        public AttendanceAdjustmentFactory LogCleanUp()
        {
            string queryFinal = "delete from HR_AttendanceAdjustmentLog";
            _session.CreateSQLQuery(queryFinal).ExecuteUpdate();
            return this;
        }

        internal AttendanceAdjustmentFactory CleanUpByTeamMember(long teamMemberId)
        {
            string queryFinal = "delete from HR_AttendanceAdjustment where TeamMemberId in ( " + string.Join(",", teamMemberId) + ")";
            _session.CreateSQLQuery(queryFinal).ExecuteUpdate();
            return this;
        }
        private void DeleteLog(IEnumerable<AttendanceAdjustment> list)
        {
            foreach (var attendanceAdjustment in list)
            {
                var tableName = GetEntityTableName(typeof(AttendanceAdjustmentLog));
                string swData = "DELETE FROM " + tableName + " WHERE [AttendanceAdjustmentId] = " + attendanceAdjustment.Id + ";";
                _session.CreateSQLQuery(swData).ExecuteUpdate();
            }
        }

        public void CleanUp(AttendanceAdjustmentFactory attendanceAdjustmentFactory)
        {
            if (attendanceAdjustmentFactory.ObjectList.Any())
            {
                DeleteObjects(attendanceAdjustmentFactory.ObjectList);
            }
            if (attendanceAdjustmentFactory.SingleObjectList.Any())
            {
                DeleteObjects(attendanceAdjustmentFactory.SingleObjectList);
            }
        }

        public AttendanceAdjustmentFactory CleanUp()
        {
            //var adIdList = _session.QueryOver<AttendanceAdjustmentLog>().Select(x => x.Id).Where(x => x.CreateBy == 0).List<long>();

            //if (adIdList.Count > 0)
            //    _attandanceAdjustmentLogService.DeleteByIdList(adIdList.ToList());

            DeleteAll();
            _teamMemberFactory.Cleanup();
            return this;
        }

        #endregion
        
    }

}
