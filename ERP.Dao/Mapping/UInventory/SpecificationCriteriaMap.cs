using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory {


    public class SpecificationCriteriaMap : BaseClassMap<SpecificationCriteria, long>
    {
        
        public SpecificationCriteriaMap() {
			Table("UINV_SpecificationCriteria");
			LazyLoad();
			References(x => x.SpecificationTemplate).Column("SpecificationTemplateId");
			Map(x => x.Rank).Column("Rank").Not.Nullable();
			Map(x => x.Name).Column("Name").Not.Nullable();
			Map(x => x.Controltype).Column("ControlType").Not.Nullable();
            HasMany(x => x.SpecificationCriteriaLogs).KeyColumn("SpecificationCriteriaId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SpecificationCriteriaOptions).KeyColumn("SpecificationCriteriaId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
