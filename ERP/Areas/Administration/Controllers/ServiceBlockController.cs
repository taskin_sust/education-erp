﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class ServiceBlockController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> _authorizeMenu;
        private readonly IServiceBlockService _serviceBlockService;

        public ServiceBlockController()
        {
            var session = NHibernateSessionFactory.OpenSession();
           _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _organizationService = new OrganizationService(session);
            _commonHelper = new CommonHelper();
            _serviceBlockService = new ServiceBlockService(session);
        }

        #endregion

        #region Operational Function

        public ActionResult Create()
        {
            _authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_authorizeMenu), "Id", "ShortName");
            ViewBag.Program = new SelectList(string.Empty, "Id", "Name");
            ViewBag.Session = new SelectList(string.Empty, "Id", "Name");
            ViewBag.blockServices = new SelectList(_commonHelper.LoadEmumToDictionary<BlockService>(), "Key", "Value");
            ViewBag.ConditionType = _commonHelper.LoadEmumToDictionary<ConditionType>();
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(ServiceBlockViewModel serviceBlockViewModel)
        {
            _authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_authorizeMenu), "Id", "ShortName");
            ViewBag.Program = new SelectList(string.Empty, "Id", "Name");
            ViewBag.Session = new SelectList(string.Empty, "Id", "Name");
            ViewBag.blockServices = new SelectList(_commonHelper.LoadEmumToDictionary<BlockService>(), "Key", "Value");
            ViewBag.ConditionType = _commonHelper.LoadEmumToDictionary<ConditionType>();
            try
            {
                foreach (var serviceBlockListContentTypeViewModel in serviceBlockViewModel.ServiceBlockListContentTypeViewModels)
                {
                    var sb = new ServiceBlock
                    {
                        Program = _programService.GetProgram(serviceBlockViewModel.ProgramId),
                        Session = _sessionService.LoadById(serviceBlockViewModel.SessionId),
                        ServiceType = serviceBlockViewModel.ServiceType
                    };
                    var sblockObj = _serviceBlockService.GetServiceBlock(serviceBlockViewModel.ProgramId, serviceBlockViewModel.SessionId, serviceBlockViewModel.ServiceType,
                        serviceBlockListContentTypeViewModel.ContentTypeId);
                    sb.ConditionType = serviceBlockListContentTypeViewModel.ContentTypeId;
                    if (sblockObj!=null && serviceBlockListContentTypeViewModel.CheckedValue == false)
                    {
                        //sb.Id = serviceBlockListContentTypeViewModel.Id;
                        _serviceBlockService.Delete(sblockObj);
                    }
                    else if (sblockObj == null && serviceBlockListContentTypeViewModel.CheckedValue)
                    {
                        _serviceBlockService.Save(sb);
                    }
                }
                return Json(new Response(true, "Service block successfully updated"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Helper Function

        [HttpPost]
        public ActionResult GetContentTypeValue(long? proId, long? sessId, int? serviceTypeId)
        {
            try
            {
                ViewBag.ConditionType = _commonHelper.LoadEmumToDictionary<ConditionType>();
                IList<ServiceBlock> selectedContentTypes = null;
                if (proId != null && sessId != null && serviceTypeId != null)
                    selectedContentTypes = _serviceBlockService.LoadContentTypeList(proId, sessId, serviceTypeId);
                ViewBag.SelectedType = selectedContentTypes;
                return PartialView("Partial/_ContentTypeList");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }

        }

        #endregion
       
    }
}