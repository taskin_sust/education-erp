﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Administration
{
    public interface IReferenceService : IBaseService
    {
        #region Operational Function
        int Save(Referrer referrer);
        bool UpdateRank(Referrer referrerOldObj, Referrer referrerUpdateObj);
        int Update(Referrer referrer);
        bool Delete(Referrer referrer);
        #endregion

        #region Single Instances Loading Function
        Referrer LoadById(long id);
        Referrer LoadByRankNextOrPrevious(int rank, string action);
        #endregion

        #region List Loading Function
        IList<Referrer> LoadAll();
        List<Referrer> GetReferrerList(int start, int length, string orderBy, string direction, string name, string code, string status, string rank);
        IList<Referrer> GetAllRefferer();
        IList<Referrer> LoadReferrerByCode(string code, long id);
        IList<Referrer> LoadReferrerByName(string name, long id);
        #endregion

        #region Others Function
        int GetMaximumRank(Referrer r);
        int GetMinRank(Referrer r);
        int ReferrerRowCount(string name, string code, string status, string rank); 
        #endregion
    }
    public class ReferenceService : BaseService, IReferenceService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion
       
        #region Propertise & Object Initialization
        private readonly IReferrerDao _referrerDao;
        public ReferenceService(ISession ssession)
        {
            Session = ssession;
            _referrerDao = new ReferrerDao() { Session = Session };
        }
        #endregion

        #region Operational Function
        private int CheckBeforeSave(Referrer referrer)
        {
            //check duplicate
            if (_referrerDao.HasDuplicateByName(referrer.Name, referrer.Id) == true)
                return Constants.MessageError;
            return Constants.MessageSuccess;
        }
        public int Save(Referrer referrer)
        {
            //check before save
            int customMessage = CheckBeforeSave(referrer);
            int maxRank = _referrerDao.GetMaximumRank(referrer);
            referrer.Rank = maxRank + 1;
            if (customMessage == Constants.MessageSuccess)
            {
                _referrerDao.Save(referrer);
            }
            return customMessage;
        }
        public int Update(Referrer referrer)
        {
            try
            {
                int customMessage = CheckBeforeSave(referrer);
                if (customMessage == Constants.MessageSuccess)
                {
                    using (var trans = Session.BeginTransaction())
                    {
                        try
                        {
                            _referrerDao.Update(referrer);
                            trans.Commit();
                            return customMessage;
                        }
                        catch (Exception e)
                        {
                            trans.Rollback();
                            return Constants.MessageError;
                        }
                    }
                }
                else
                {
                    return customMessage;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public bool Delete(Referrer referrer)
        {
            try
            {
                if (referrer.StudentPayments.Count > 0)
                {
                    throw new DependencyException("This referrer has one or more referrences in StudentPayments.");
                }
                else
                {
                    using (var trans = Session.BeginTransaction())
                    {
                        try
                        {
                            referrer.Status = Referrer.EntityStatus.Delete;
                            _referrerDao.Update(referrer);
                            trans.Commit();
                            return true;
                        }
                        catch (Exception e)
                        {
                            trans.Rollback();

                        }
                    }
                }
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        public bool UpdateRank(Referrer referrerOldObj, Referrer referrerUpdateObj)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        _referrerDao.Update(referrerOldObj);
                        _referrerDao.Update(referrerUpdateObj);
                        trans.Commit();
                        return true;
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public Referrer LoadById(long id)
        {
            try
            {
                return _referrerDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }      
        public Referrer LoadByRankNextOrPrevious(int rank, string action)
        {
            try
            {
                return _referrerDao.LoadByRankDirection(rank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public List<Referrer> GetReferrerList(int start, int length, string orderBy, string direction, string name, string code, string status, string rank)
        {
            try
            {
                return _referrerDao.GetReferrerList(start, length, orderBy, direction, name, code, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public IList<Referrer> GetAllRefferer()
        {
            try
            {
                return _referrerDao.LoadAll();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public IList<Referrer> LoadReferrerByCode(string code, long id)
        {
            try
            {
                return _referrerDao.LoadReferrerByCode(code, id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public IList<Referrer> LoadReferrerByName(string name, long id)
        {
            try
            {
                return _referrerDao.LoadReferrerByName(name, id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public IList<Referrer> LoadAll()
        {
            try
            {
                var referrerList = _referrerDao.LoadAll();
                return referrerList = referrerList.OrderBy(referrer => referrer.Rank).Cast<Referrer>().ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region Others Function
        public int GetMaximumRank(Referrer referrer)
        {
            try
            {
                return _referrerDao.GetMaximumRank(referrer);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public int GetMinRank(Referrer referrer)
        {
            try
            {
                return _referrerDao.GetMinimumRank(referrer);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public int ReferrerRowCount(string name, string code, string status, string rank)
        {
            try
            {
                return _referrerDao.ReferrerRowCount(name, code, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion
    }
}
