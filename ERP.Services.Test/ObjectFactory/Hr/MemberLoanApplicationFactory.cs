using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Hr;
namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class MemberLoanApplicationFactory : ObjectFactoryBase<MemberLoanApplication>
    {
        private readonly IMemberLoanApplicationService _prMemberLoanApplicationService;

        public MemberLoanApplicationFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _prMemberLoanApplicationService = new MemberLoanApplicationService(session);
        }

        #region Create

        public MemberLoanApplicationFactory Create()
        {
            Object = new MemberLoanApplication()
            {
                LoanStatus = (int)LoanStatus.Pending,
                RequestedAmount = 10000,
                Remarks = "Applied for BDT 10000"
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanApplicationFactory CreateWith(long id)
        {
            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public MemberLoanApplicationFactory WithTeamMember(TeamMember member)
        {
            Object.TeamMember = member;
            return this;
        }

        public MemberLoanApplicationFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _prMemberLoanApplicationService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_prMemberLoanApplicationService.SaveOrUpdate(x); });

            }
            return this;
        }

        public MemberLoanApplicationFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new MemberLoanApplication() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public MemberLoanApplicationFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion


    }
}
