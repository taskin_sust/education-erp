using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.Test.ObjectFactory.Administration;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class SupplierFactory : ObjectFactoryBase<Supplier>
    {
        private readonly ISupplierService _supplierService;
        public List<string> AssignTinList = new List<string>();
        public List<string> AssignTlnIlcList = new List<string>();
        public List<string> AssignVatRegNoList = new List<string>();

        public SupplierFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _supplierService = new SupplierService(session);

        }

        #region Create

        public SupplierFactory Create()
        {

            Object = new Supplier()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                ContactPerson = "SupplierContactPerson",
                Email = "email@email.com",
                Name = GetPcUserName() + "SupplierName" + DateTime.Now.Second + DateTime.Now.Millisecond,
                OwnerName = "OwnerName",
                Status = Supplier.EntityStatus.Active,
                Rank = 1,
                RegistrationAddress = "Supplier Address",
                TaxCircle = "Tax Circle",
                TaxZone = "Tax Zone",
                VatCircle = "Vat Circle",
                VatDivision = "Vat Division",
                VatRegistrationNo = GetUniqueVatRegNumber(),
                TelephoneNo = "01712111111",
                MobileNo = "01912000000",
                Tin = GetUniqueTinNumber(),
                TlnInc = GetUniqueTlnIncNumber(),
                WebsiteAdress = "www.supplier.com"
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public SupplierFactory CreateWith(long id)
        {
            Object = null;
            SingleObjectList.Add(Object);
            return this;
        }

        public SupplierFactory CreateWithSupplierBankDetails(IList<SupplierBankDetails> bankList)
        {
            foreach (var bank in bankList)
            {
                Object.SupplierBankDetails.Add(bank);
            }
            return this;
        }

        //public SupplierFactory CreateWithSupplierBankDetails()
        //{
        //    _bankBranchFactory.Create().WithBank().Persist();
        //    var obj = new SupplierBankDetails()
        //    {
        //        Supplier = Object,
        //        BankBranch = _bankBranchFactory.Object,
        //        Rank = 100,
        //        AccountTitle = Guid.NewGuid().ToString(),
        //        AccountNo = "12345679#"
        //    };
        //    Object.SupplierBankDetails.Add(obj);
        //    return this;
        //}

        public SupplierFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _supplierService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                foreach (var obj in ObjectList)
                {
                    _supplierService.SaveOrUpdate(obj);
                }
            }
            return this;
        }

        public SupplierFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                Object = new Supplier()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    ContactPerson = i.ToString() + "_SupplierContactPerson",
                    Email = i.ToString() + "_email@email.com",
                    Name = i.ToString() + "_SupplierName" + GetPcUserName(),
                    OwnerName = i.ToString() + "_OwnerName",
                    Status = Supplier.EntityStatus.Active,
                    Rank = 1,
                    RegistrationAddress = i.ToString() + "_Supplier Address",
                    TaxCircle = i.ToString() + "_Tax Circle",
                    TaxZone = i.ToString() + "_Tax Zone",
                    VatCircle = i.ToString() + "_Vat Circle",
                    VatDivision = i.ToString() + "_Vat Division",
                    VatRegistrationNo = GetUniqueVatRegNumber(),
                    TelephoneNo = "01712111111",
                    MobileNo = "01912000000",
                    Tin = GetUniqueTinNumber(),
                    TlnInc = GetUniqueTlnIncNumber(),
                    WebsiteAdress = "www.supplier.com"
                };
                ObjectList.Add(Object);
            }
            return this;
        }

        #endregion

        #region other

        public string GetUniqueTinNumber()
        {
            int min = 1;
            int max = 999999;
            IList<Supplier> suppliers = _supplierService.LoadAll();
            var codeList = suppliers.Select(x => x.Tin).ToList<string>();
            if (AssignTinList != null && AssignTinList.Count > 0)
                codeList.AddRange(AssignTinList);
            do
            {
                Random r = new Random();
                var x = r.Next(min, max);
                string s = x.ToString("000000");
                if (codeList.Count > 0 && !codeList.Contains(s))
                {
                    AssignTinList.Add(s);
                    return s;
                }
                else if (codeList.Count <= 0)
                {
                    AssignTinList.Add(s);
                    return s;
                }
                min++;
            } while (min <= max);
            return "Error Tin found";
        }

        public string GetUniqueTlnIncNumber()
        {
            int min = 1;
            int max = 999999;

            IList<Supplier> suppliers = _supplierService.LoadAll();
            var codeList = suppliers.Select(x => x.TlnInc).ToList<string>();
            if (AssignTlnIlcList != null && AssignTlnIlcList.Count > 0)
                codeList.AddRange(AssignTlnIlcList);
            do
            {
                var r = new Random();
                var x = r.Next(min, max);
                string s = x.ToString("000000");
                if (codeList.Count > 0 && !codeList.Contains(s))
                {
                    AssignTlnIlcList.Add(s);
                    return s;
                }
                else if (codeList.Count <= 0)
                {
                    AssignTlnIlcList.Add(s);
                    return s;
                }
                min++;
            } while (min <= max);
            return "Error Tin found";
        }

        public string GetUniqueVatRegNumber()
        {
            int min = 1;
            int max = 999999;
            IList<Supplier> suppliers = _supplierService.LoadAll();
            var codeList = suppliers.Select(x => x.VatRegistrationNo).ToList<string>();
            if (AssignVatRegNoList != null && AssignVatRegNoList.Count > 0)
                codeList.AddRange(AssignVatRegNoList);
            do
            {
                var r = new Random();
                var x = r.Next(min, max);
                string s = x.ToString("000000");
                if (codeList.Count > 0 && !codeList.Contains(s))
                {
                    AssignVatRegNoList.Add(s);
                    return s;
                }
                else if (codeList.Count <= 0)
                {
                    AssignVatRegNoList.Add(s);
                    return s;
                }
                min++;
            } while (min <= max);
            return "Error Tin found";
        }

        #endregion

        #region CleanUp

        public SupplierFactory CleanUp()
        {
            DeleteAll();
            return this;
        }

        #endregion

    }
}
