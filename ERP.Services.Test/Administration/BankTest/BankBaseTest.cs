﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.BankTest
{
    public class BankBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal IBankService _bankService;
        private ITestBaseService<Bank> _TestBaseService;
        internal List<long> IdList = new List<long>();
        private ISession _session;
        internal BankFactory _bankFactory;
        internal readonly string _name = Guid.NewGuid().ToString().Substring(0, 15) + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal readonly string _shortName = Guid.NewGuid().ToString().Substring(0, 15) + "_(ABC CDE FGH)";
        #endregion

        public BankBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _bankFactory = new BankFactory(null, _session);
            _bankService=new BankService(_session);
            _TestBaseService = new TestBaseService<Bank>(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }
        public void Dispose()
        {
            _bankFactory.Cleanup();
            base.Dispose();
        }
    }
}
