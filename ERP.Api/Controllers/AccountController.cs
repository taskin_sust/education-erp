﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Api.Controllers
{
    public class AccountController : Controller
    {
        string token = "kjlhdfkjh76876jakdfuighsadifhhfdfhdhdfg234234234";
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AttendanceLogin");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IUserService _userService;
        public AccountController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _userService = new UserService(session);
            }
            catch (Exception e)
            {
                //Console.WriteLine("{0}", e.Message);
                _logger.Error(e);
            }
        }
        #endregion


        private string login1(string username, string password)
        {
            return "Token";
        }

        [HttpGet]
        public JsonResult login(string appName, string appversion, string username, string password, string hddSerial, string macAddress)
        {
            JsonResultResponse resultResponse = new JsonResultResponse(false, false, "Invalid Login", "");
            if (appName.Trim() != "" && appversion.Trim() != "" && username.Trim() != "" && password.Trim() != "" && hddSerial.Trim() != "" && macAddress.Trim() != "")
            {
                resultResponse.IsAuthenticated = true;
                resultResponse.IsSuccess = true;
                resultResponse.Message = "Login Successfull";
                resultResponse.Data = token;                
            }
            return Json(new JsonResponse(resultResponse.IsSuccess, resultResponse), JsonRequestBehavior.AllowGet);
        }
    }
}