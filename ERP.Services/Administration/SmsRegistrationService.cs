﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text.RegularExpressions;
using System.Web.Helpers;
using System.Web.Script.Serialization;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.Administration;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.Dao.Sms;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ISmsRegistrationService : IBaseService
    {
        #region Operational Function

        SmsRegistrationDto RegisterStudentBySms(SmsRegistrationReceiverLog registrationLog, SmsRegistrationDto srd);
        
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function


        #endregion

        #region Others Function


        #endregion

        #region Helper Function

        #endregion

        #region Public API

        #endregion

    }
    public class SmsRegistrationService : BaseService, ISmsRegistrationService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IProgramDao _programDao;
        private readonly IBranchDao _branchDao;
        private readonly ISessionDao _sessionDao;
        private readonly ICourseDao _courseDao;
        private readonly IStudentDao _studentDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IStudentCourseDetailDao _studentCourseDetailsDao;
        private readonly IStudentPaymentDao _studentPaymentDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly ICampusDao _campusDao;
        private readonly ISmsStudentRegistrationSettingDao _smsStudentRegistrationDao;
        private readonly ISmsTypeDao _smsTypeDao;
        private readonly IBatchDao _batchDao;
        private readonly ISmsReceiverDao _smsReceiverDao;
        private readonly IStudentAcademicInfoDao _studentAcademicInfoDao;
        private readonly IStudentBoardDao _studentBoardDao;
        private readonly ICommonHelper _commonHelper;
        public SmsRegistrationService(ISession session)
        {
            Session = session;
            _programDao = new ProgramDao { Session = Session };
            _branchDao = new BranchDao { Session = Session };
            _sessionDao = new SessionDao { Session = Session };
            _courseDao = new CourseDao { Session = Session };
            _organizationDao = new OrganizationDao { Session = Session };
            _studentDao = new StudentDao { Session = Session };
            _campusDao = new CampusDao { Session = Session };
            _studentProgramDao = new StudentProgramDao { Session = Session };
            _studentCourseDetailsDao = new StudentCourseDetailDao { Session = Session };
            _studentPaymentDao = new StudentPaymentDao { Session = Session };
            _smsStudentRegistrationDao = new SmsStudentRegistrationSettingDao { Session = Session };
            _smsTypeDao = new SmsTypeDao { Session = Session };
            _batchDao = new BatchDao { Session = Session };
            _smsReceiverDao = new SmsReceiverDao { Session = Session };
            _studentAcademicInfoDao = new StudentAcademicInfoDao { Session = Session };
            _studentBoardDao = new StudentBoardDao { Session = Session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function

        public SmsRegistrationDto RegisterStudentBySms(SmsRegistrationReceiverLog registrationLog, SmsRegistrationDto srd)
        {
            ITransaction trans = null;
            try
            {
                var smsUserId = Convert.ToInt64(ConfigurationManager.AppSettings["SmsUserId"]);
                //validation check with meta data
                registrationLog.SenderMobileNumber = CheckMobileNumber(registrationLog.SenderMobileNumber);
                if (string.IsNullOrEmpty(registrationLog.SenderMobileNumber))
                {
                    throw new InvalidDataException("Sender mobile number is not valid.");
                }
                var validationResult = ValidationHelper.ValidateEntity<SmsRegistrationReceiverLog, SmsRegistrationReceiverLog>(registrationLog);
                if (validationResult.HasError)
                {
                    string errorMessage = "";
                    validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                    throw new InvalidDataException(errorMessage);
                }
                using (trans = Session.BeginTransaction())
                {
                    int messageParts = 9;
                    string programShortName = "";
                    string nickName = "";
                    string branchCode = "";
                    string gender = "";
                    string studyVersion = "";
                    string boardShortName = "";
                    string boardExamYear = "";
                    string boardRollNumber = "";
                    string boardRegistrationNumber = "";
                    string senderMobileNo = registrationLog.SenderMobileNumber;
                    string message = registrationLog.Message;
                    var messageParsedList = message.Split(' ').ToList();
                    if (messageParsedList.Count != messageParts)
                    {
                        throw new InvalidDataException("Invalid SMS format.");
                    }
                    programShortName = messageParsedList[0];
                    nickName = messageParsedList[1];
                    gender = messageParsedList[2];
                    studyVersion = messageParsedList[3];
                    boardShortName = messageParsedList[4].ToLower();
                    boardExamYear = messageParsedList[5];
                    boardRollNumber = messageParsedList[6];
                    boardRegistrationNumber = messageParsedList[7];
                    branchCode = messageParsedList[8];

                    //check numeric roll, registration no, year
                    CheckProperty("board roll no", boardRollNumber, 6);
                    CheckProperty("board registration no", boardRegistrationNumber, 10);
                    CheckProperty("board exam year", boardExamYear, 4);

                    //find and check board by shortname
                    string boardName = "";
                    if (boardShortName == "bar")
                        boardName = "Barisal";
                    else if (boardShortName == "chi")
                        boardName = "Chittagong";
                    else if (boardShortName == "com")
                        boardName = "Comilla";
                    else if (boardShortName == "dha")
                        boardName = "Dhaka";
                    else if (boardShortName == "din")
                        boardName = "Dinajpur";
                    else if (boardShortName == "jes")
                        boardName = "Jessore";
                    else if (boardShortName == "raj")
                        boardName = "Rajshahi";
                    else if (boardShortName == "syl")
                        boardName = "Sylhet";
                    else if (boardShortName == "mad")
                        boardName = "Madrasah";
                    else if (boardShortName == "tec")
                        boardName = "Technical";
                    StudentBoard studentBoard;
                    try
                    {
                        studentBoard = _studentBoardDao.GetByBoardName(boardName, true);
                    }
                    catch (Exception e)
                    {
                        throw new InvalidDataException("Student board short name is not valid.");
                    }
                    if (studentBoard == null)
                    {
                        throw new InvalidDataException("Student board short name is not valid.");
                    }
                    var program = _programDao.GetProgram(programShortName, false);
                    if (program == null)
                    {
                        throw new InvalidDataException("Program short name is not valid.");
                    }
                    srd.Program = program;
                    var branch = _branchDao.GetBranch(branchCode);
                    if (branch == null)
                    {
                        throw new InvalidDataException("Exam center is not valid.");
                    }

                    var sessionList = _sessionDao.LoadPublicSessionByProgram(program.Organization.BusinessId, program.Id, branch.Id);
                    if (sessionList == null || sessionList.Count == 0)
                    {
                        throw new InvalidDataException("Session not found for this program and branch.");
                    }
                    var session = sessionList[0];
                    //var courseList = _courseDao.LoadCourse(program.Id, session.Id);
                    //if (courseList == null || courseList.Count == 0)
                    //{
                    //    throw new InvalidDataException("Course not found.");
                    //}
                    //var course = courseList[0];
                    //check sms setting exists
                    //check valid year
                    if (boardExamYear != session.Name.Trim())
                        throw new InvalidDataException("Board exam year must be " + session.Name);
                    var smsSetting = _smsStudentRegistrationDao.GetRecentSmsStudentRegistrationSetting(program.Id, session.Id);
                    if (smsSetting == null)
                    {
                        throw new InvalidDataException("SMS Registration Setting not found for this program.");
                    }
                    var studentExam = smsSetting.StudentExam;
                    var course = smsSetting.Course;
                    var campusList = _campusDao.LoadAuthorizeCampus(_commonHelper.ConvertIdToList(program.Id), _commonHelper.ConvertIdToList(branch.Id), _commonHelper.ConvertIdToList(session.Id));
                    if (campusList == null || campusList.Count == 0)
                    {
                        throw new InvalidDataException("Campus not found.");
                    }
                    var campus = campusList[0];
                    var batchList = campus.Batches.Where(x => x.Session.Id == session.Id && x.Program.Id == program.Id).ToList();
                    if (batchList.Count == 0)
                    {
                        throw new InvalidDataException("Batch not found.");
                    }
                    var batch = batchList[0];
                    srd.Batch = batch;
                    //Student student = _studentDao.GetByNickNameAndMobile(nickName, senderMobileNo, false);
                    Student student = _studentDao.GetByNickNameAndMobile(nickName, senderMobileNo);
                    if (student != null && student.Id > 0)
                    {
                        srd.Student = student;
                        //save or update academic info
                        StudentAcademicInfo existingInfo = _studentAcademicInfoDao.LoadByStudentId(student.Id, studentExam.Id);
                        if (existingInfo == null)
                        {
                            var academicInfo = new StudentAcademicInfo
                            {
                                Student = student,
                                Year = boardExamYear,
                                BoradRoll = boardRollNumber,
                                RegistrationNumber = boardRegistrationNumber,
                                StudentBoard = studentBoard,
                                StudentExam = studentExam,
                                CreateBy = smsUserId,
                                ModifyBy = smsUserId
                            };
                            _studentAcademicInfoDao.Save(academicInfo);
                        }
                        else
                        {
                            existingInfo.Year = boardExamYear;
                            existingInfo.BoradRoll = boardRollNumber;
                            existingInfo.RegistrationNumber = boardRegistrationNumber;
                            existingInfo.StudentBoard = studentBoard;
                            existingInfo.StudentExam = studentExam;
                            existingInfo.ModifyBy = smsUserId;
                            _studentAcademicInfoDao.Update(existingInfo);
                        }
                        //check student already registered for this program session
                        var studentProgram =
                            _studentProgramDao.GetStudentProgram(nickName,
                                senderMobileNo, program.Id, session.Id);
                        if (studentProgram != null)
                        {
                            registrationLog.StudentProgram = studentProgram;
                            //throw new DuplicateEntryException("Duplicate Student Found !! Student already registered by using this Nick name and mobile number in this program and session.");
                            var smsType = (SmsType)_smsTypeDao.LoadByName("SMS Registration Duplicate").First();
                            srd = GenarateSuccessAndDuplicateSmsReceiverList(smsType.Id, program, session, course.Id, student, smsSetting, studentProgram.Id, batch);
                            registrationLog.RegistrationStatus = (int)SmsRegistrationStatus.Duplicate;
                            registrationLog.StudentProgram = studentProgram;
                            //commit transaction
                            trans.Commit();
                            return srd;

                        }
                    }
                    else
                    {
                        //Board roll and registration duplicate check
                        //var studentProgramByAcademicInfo = _studentProgramDao.GetStudentProgram(studentExam.Id, studentBoard.Id, boardExamYear, boardRollNumber, boardRegistrationNumber, program.Id, session.Id);
                        if (_studentProgramDao.HasStudentProgram(studentExam.Id, studentBoard.Id, boardExamYear, boardRollNumber, boardRegistrationNumber, program.Id, session.Id))
                        {
                            //registrationLog.StudentProgram = studentProgramByAcademicInfo;
                            throw new DuplicateEntryException(@"Sorry, Registration failed because Roll " + boardRollNumber + @", Reg. No. " + boardRegistrationNumber + @" of " + studentBoard.Name + @" Board " + studentExam.Name + @"-" + boardExamYear + @" is already registered. UDVASH-UNMESH shikkha poribar.");
                        }

                        //build student object
                        student = new Student();
                        student.NickName = nickName;
                        student.Mobile = "88" + senderMobileNo;
                        switch (gender.ToLower())
                        {
                            case "m":
                                student.Gender = (int)Gender.Male;
                                break;
                            case "f":
                                student.Gender = (int)Gender.Female;
                                break;
                            default:
                                //student.Gender = (int)Gender.Combined;
                                throw new InvalidDataException("Student Gender is not valid.");
                                break;
                        }
                        student.Religion = (int)Religion.Other;
                        student.IsJsc = false;
                        student.IsSsc = false;
                        student.IsHsc = false;
                        student.Status = Student.EntityStatus.Active;
                        student.CreateBy = smsUserId;
                        student.ModifyBy = smsUserId;
                        //registration no generation
                        var regNumber = GetRegistrationNumber();
                        if (string.IsNullOrEmpty(regNumber))
                        {
                            throw new InvalidDataException("Could not generate registration no.");
                        }
                        student.RegistrationNo = regNumber;
                        //save student 
                        srd.Student = student;
                        _studentDao.Save(student);
                        //save academic info
                        var academicInfo = new StudentAcademicInfo
                        {
                            Student = student,
                            Year = boardExamYear,
                            BoradRoll = boardRollNumber,
                            RegistrationNumber = boardRegistrationNumber,
                            StudentBoard = studentBoard,
                            StudentExam = studentExam,
                            CreateBy = smsUserId,
                            ModifyBy = smsUserId
                        };
                        _studentAcademicInfoDao.Save(academicInfo);
                    }

                    //Create StudentProgram object
                    var studentProgramObj = new StudentProgram
                    {
                        Batch = batch,
                        Program = program,
                        Student = student,
                        CreateBy = smsUserId,
                        ModifyBy = smsUserId
                    };
                    switch (studyVersion.ToLower())
                    {
                        case "e":
                            studentProgramObj.VersionOfStudy = (int)VersionOfStudy.English;
                            break;
                        case "b":
                            studentProgramObj.VersionOfStudy = (int)VersionOfStudy.Bangla;
                            break;
                        default:
                            throw new InvalidDataException("Version not valid.");
                            break;
                    }
                    //generate prn no
                    var prnNum = GetPrNumber(branch.Code, session.Code, program.Code);
                    if (string.IsNullOrEmpty(prnNum))
                    {
                        throw new InvalidDataException("Could not generate prn no.");
                    }
                    studentProgramObj.PrnNo = prnNum;
                    studentProgramObj.Student = student;
                    //Create StudentPayment
                    StudentPayment studentPayment = new StudentPayment();
                    studentPayment.CreateBy = smsUserId;
                    studentPayment.ModifyBy = smsUserId;
                    studentPayment.CreationDate = DateTime.Now;
                    studentPayment.ModificationDate = DateTime.Now;
                    studentPayment.ReceivedDate = DateTime.Now;
                    studentPayment.NextReceivedDate = null;
                    studentPayment.PayableAmount = 0;
                    studentPayment.ReceivedAmount = 0;
                    studentPayment.DiscountAmount = 0;
                    studentPayment.DueAmount = 0;
                    studentPayment.PaymentMethod = 0;
                    studentPayment.ReceivedDate = DateTime.Now;
                    studentPayment.Status = StudentPayment.EntityStatus.Active;
                    studentPayment.PaymentType = PaymentType.MoneyReceipt;
                    studentPayment.NextReceivedDate = null;
                    studentPayment.CourseSubjectList = new List<CourseSubject>();
                    studentPayment.StudentProgram = studentProgramObj;
                    studentPayment.CourseFees = 0;
                    studentPayment.Remarks = "New admisson by sms";
                    var mrNo = GetMrNumber(branch.Code, session.Code, studentPayment.PaymentType);
                    if (!String.IsNullOrEmpty(mrNo))
                    {
                        studentPayment.ReceiptNo = mrNo;
                    }
                    studentPayment.StudentProgram = studentProgramObj;
                    studentPayment.Batch = studentProgramObj.Batch;
                    studentPayment.CampusReceiveDate = null;
                    //create course details
                    var courseSubjectList = course.CourseSubjects;
                    if (courseSubjectList == null || courseSubjectList.Count == 0)
                    {
                        throw new InvalidDataException("No course subject found.");
                    }
                    foreach (var courseSubject in courseSubjectList)
                    {
                        var studentCourseDetailObj = new StudentCourseDetail();
                        studentCourseDetailObj.CourseId = course.Id;
                        studentCourseDetailObj.CourseSubject = courseSubject;
                        studentCourseDetailObj.StudentProgram = studentProgramObj;
                        studentCourseDetailObj.CreateBy = smsUserId;
                        studentCourseDetailObj.ModifyBy = smsUserId;
                        studentProgramObj.StudentCourseDetails.Add(studentCourseDetailObj);
                        studentPayment.CourseSubjectList.Add(courseSubject);
                        //save student course details
                        _studentCourseDetailsDao.Save(studentCourseDetailObj);
                    }
                    //save student program
                    registrationLog.StudentProgram = studentProgramObj;
                    _studentProgramDao.Save(studentProgramObj);
                    //save payment
                    _studentPaymentDao.Save(studentPayment);
                    //commit transaction
                    trans.Commit();
                    SmsType successSmsType = (SmsType)_smsTypeDao.LoadByName("SMS Registration Success").First();
                    srd = GenarateSuccessAndDuplicateSmsReceiverList(successSmsType.Id, program, session, course.Id, student, smsSetting, studentProgramObj.Id, batch);
                }
            }
            catch (InvalidDataException ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw ex;
            }
            catch (DuplicateEntryException ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw ex;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
            return srd;
        }

        public SmsRegistrationDto GenarateSuccessAndDuplicateSmsReceiverList(long smsTypeId, Program program, Session session, long courseId, Student student, SmsStudentRegistrationSetting smsSetting, long? studentProgramId = null, Batch batch = null)
        {
            SmsRegistrationDto srd = new SmsRegistrationDto();
            try
            {
                long programId = program.Id;
                long sessionId = session.Id;
                if (smsTypeId != 0 && programId != 0 && sessionId != 0 && student != null)
                {
                    var smsType = _smsTypeDao.LoadById(smsTypeId);
                    string message = "";
                    if (smsType.Name == "SMS Registration Success")
                        message = smsSetting.SuccessMessage;
                    else if (smsType.Name == "SMS Registration Duplicate")
                        message = smsSetting.DuplicateMessage;
                    var smsReceivers = smsSetting.SmsStudentRegistrationReceiverList;
                    if (smsReceivers.Any() && !string.IsNullOrEmpty(message))
                    {
                        var studentProgram = studentProgramId != null ? _studentProgramDao.LoadById(Convert.ToInt64(studentProgramId)) : student.StudentPrograms.LastOrDefault(x => x.Program.Id == program.Id);
                        var mask = smsSetting.SmsMask == null ? "" : smsSetting.SmsMask.Name;
                        var smsToBeSend = new List<SmsViewModel>();
                        message = message.Replace("[[{{Program Roll}}]]", studentProgram.PrnNo);
                        message = message.Replace("[[{{Nick Name}}]]", student.NickName);
                        message = message.Replace("[[{{Registration Number}}]]", student.RegistrationNo);
                        message = message.Replace("[[{{Session}}]]", session.Name);
                        message = message.Replace("[[{{Program}}]]", program.Name);
                        foreach (var sr in smsReceivers)
                        {
                            var svm = new SmsViewModel();
                            if (sr.SmsReceiver.Name == "Mobile Number (Personal)")
                            {
                                string studentMobile = CheckMobileNumber(student.Mobile);
                                if (studentMobile == null)
                                {
                                    throw new InvalidDataException("Student mobile number is not valid.");
                                }
                                svm.SmsReceiverId = sr.Id;
                                svm.ReceiverNumber = studentMobile;
                            }
                            else if (sr.SmsReceiver.Name == "Mobile Number (Father)")
                            {
                                string guardiansMobile1 = CheckMobileNumber(student.GuardiansMobile1);
                                if (guardiansMobile1 != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = guardiansMobile1;
                                }
                            }
                            else if (sr.SmsReceiver.Name == "Mobile Number (Mother)")
                            {
                                string guardiansMobile2 = CheckMobileNumber(student.GuardiansMobile2);
                                if (guardiansMobile2 != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = guardiansMobile2;
                                }
                            }
                            else
                            {
                                throw new InvalidDataException("Valid receiver number not found.");
                            }

                            if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                            {
                                var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                                if (!alreadyExist.Any())
                                {
                                    smsToBeSend.Add(svm);
                                }
                            }
                        }
                        if (smsToBeSend.Any())
                        {
                            srd = new SmsRegistrationDto();
                            srd.Message = message;
                            srd.Program = program;
                            srd.Student = student;
                            srd.Mask = mask;
                            srd.SmsType = Convert.ToInt32(smsTypeId);
                            srd.Batch = batch;
                            srd.SmsToBeSend = smsToBeSend;
                        }
                    }
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return srd;
        }

        public string GetRegistrationNumber()
        {
            try
            {
                var studentObj = _studentDao.GetLastRegisteredStudent();
                if (studentObj == null) return "1000001";
                return (Convert.ToInt64(studentObj.RegistrationNo.Trim()) + 1).ToString();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "";
            }
        }

        private string GetPrNumber(string bCode, string sCode, string pCode)
        {
            try
            {
                if (!String.IsNullOrEmpty(bCode) && !String.IsNullOrEmpty(sCode) && !String.IsNullOrEmpty(pCode))
                {
                    var firstPrCode = "00001";
                    var lastPrCode = bCode + sCode + pCode;
                    StudentProgram studentProgram = _studentProgramDao.GetLastStudentProgram(lastPrCode);
                    if (studentProgram == null)
                    {
                        return lastPrCode + firstPrCode;
                    }
                    var prePrNo = studentProgram.PrnNo;
                    var prePrNoLastPart = prePrNo.Substring(6);

                    long newFirstCode = Convert.ToInt64(prePrNoLastPart.Trim()) + 1;

                    string finalCode = lastPrCode + newFirstCode.ToString().PadLeft(5, '0');
                    return finalCode;
                }
                return "";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "";
            }

        }

        private string GetMrNumber(string brCode, string sCode, int paymentType)
        {
            try
            {
                if (!String.IsNullOrEmpty(brCode) && !String.IsNullOrEmpty(sCode))
                {
                    var firstMrCode = "000001";
                    var lastMrCode = brCode + sCode;
                    StudentPayment studentPaymentObj = _studentPaymentDao.LoadBybrCodeAndsCode(lastMrCode, paymentType);
                    if (studentPaymentObj == null)
                    {
                        return lastMrCode + firstMrCode;
                    }

                    var preMrNo = studentPaymentObj.ReceiptNo;
                    var preMrNoLastPart = preMrNo.Substring(4);

                    return lastMrCode + (Convert.ToInt64(preMrNoLastPart) + 1).ToString().PadLeft(6, '0');
                }
                return "";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "";
                //throw;
            }

        }

        public void CheckProperty(string propertyName, string value, int length)
        {
            try
            {
                if (value.Length != length)
                    throw new InvalidDataException("Invalid " + propertyName + "! " + propertyName + " length must be " + length);
                Convert.ToInt64(value);
            }
            catch (Exception)
            {
                throw new InvalidDataException(propertyName + " is not valid");
            }
        }

        public string CheckMobileNumber(string mobile)
        {
            if (mobile != null)
            {
                var regex = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
                var match = regex.Match(mobile);
                if (match.Success)
                {
                    switch (mobile.Length)
                    {
                        case 14:
                            mobile = mobile.Substring(3);
                            break;
                        case 13:
                            mobile = mobile.Substring(2);
                            break;
                        case 10:
                            mobile = "0" + mobile;
                            break;
                    }
                }
                else { mobile = null; }
            }
            return mobile;
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        #region Public API

        #endregion
    }
}
