﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.Services;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class MenuGroupController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly IMenuService _menuService;
        private readonly ICommonHelper _commonHelper;
        public MenuGroupController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _menuService = new MenuService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
        }
        #endregion

        #region Index/Manage
        public ActionResult ManageMenuGroup()
        {
            try
            {
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Menu Group Loading Fail";
            }
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }

        [HttpPost]
        public JsonResult MenuGroupListResult(int draw, int start, int length, string Name, string Status, string Rank)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;

                    string orderBy = "";
                    string orderDir = "";


                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {

                            case "0":
                                orderBy = "Name";
                                break;
                            case "1":
                                orderBy = "Status";
                                break;
                            case "2":
                                orderBy = "Rank";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }

                    int recordsTotal = _menuService.MenuGroupRowCount(Name, Status, Rank);
                    long recordsFiltered = recordsTotal;
                    List<MenuGroup> menuGroupList = _menuService.GetMenuGroupList(start, length, orderBy, orderDir.ToUpper(), Name, Status, Rank).ToList();

                    MenuGroup m = null;
                    int[] MaxMinRank = new int[2];
                    MaxMinRank[0] = 1;
                    MaxMinRank[1] = _menuService.GetMaximumRank(m);

                    var data = new List<object>();
                    foreach (var c in menuGroupList)
                    {
                        var str = new List<string>();
                        str.Add(c.Name);
                        str.Add(c.DisplayText);
                        str.Add(StatusTypeText.GetStatusText(c.Status));
                        string rankbtn = c.Rank.ToString();
                        if (c.Rank > MaxMinRank[0])
                        {
                            rankbtn += "<a href='#'  id='" + c.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                        }
                        if (c.Rank < MaxMinRank[1])
                        {
                            rankbtn += "<a href='#'  id='" + c.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                        }

                        str.Add(rankbtn);
                        str.Add("<a href='" + Url.Action("Details", "MenuGroup") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;<a href='" + Url.Action("Edit", "MenuGroup") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;<a id='" + c.Id.ToString() + "' href='#' data-name='" + c.Name.ToString() + "' class='glyphicon glyphicon-trash'> </a>");
                        data.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return Json(HttpNotFound());
        }
        #endregion

        #region Save Operation
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MenuGroupForm menuGroupForm)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    MenuGroup menuGroup = new MenuGroup();
                    menuGroup.Name = menuGroupForm.Name;
                    menuGroup.DisplayText = menuGroupForm.DisplayText;
                    _menuService.Save(menuGroup);
                    ViewBag.SuccessMessage = "Menu Group Addded Successfully";
                    ModelState.Clear();
                    return View(new MenuGroupForm());
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    ViewBag.ErrorMessage = "Menu Group Addded Fail";
                }
            }
            return View(menuGroupForm);
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            try
            {
                MenuGroup menuGroup = _menuService.LoadById(id);
                if (menuGroup == null)
                {
                    return HttpNotFound();
                }
                MenuGroupForm menuGroupForm = new MenuGroupForm();
                menuGroupForm.Name = menuGroup.Name;
                menuGroupForm.DisplayText = menuGroup.DisplayText;
                menuGroupForm.Status = menuGroup.Status;
                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                return View(menuGroupForm);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MenuGroupForm menuGroupForm, long id)
        {
            try
            {
                MenuGroup updateMenuGroup = _menuService.LoadById(id);
                updateMenuGroup.Name = menuGroupForm.Name;
                updateMenuGroup.DisplayText = menuGroupForm.DisplayText;
                updateMenuGroup.Status = menuGroupForm.Status;
                var success=_menuService.Update(updateMenuGroup);
                ViewBag.SuccessMessage = "Menu Group Update Successfully";
                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Menu Group Update Fail";
            }
            return View(menuGroupForm);
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                MenuGroup deleteMenuGroup = _menuService.LoadById(id);
                deleteMenuGroup.Status = MenuGroup.EntityStatus.Delete;
                _menuService.Delete(deleteMenuGroup);
                return Json(new Response(true, "Menu Delete Successful !"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Menu Group Delete Fail !"));
            }
        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            try
            {
                MenuGroup menuGroup = _menuService.LoadById(id);
                if (menuGroup == null)
                {
                    return HttpNotFound();
                }
                return View(menuGroup);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Menu Group Loading Fail";
                return View();
            }
        }
        #endregion

        #region Rank Operation
        public ActionResult RankChange(int id, int current, string action)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    MenuGroup menuGroupUpdateObj = _menuService.LoadById(Convert.ToInt64(id));
                    if (menuGroupUpdateObj == null)
                    {
                        return HttpNotFound();
                    }
                    int newRank;
                    if (action == "up")
                        newRank = menuGroupUpdateObj.Rank - 1;
                    else
                        newRank = menuGroupUpdateObj.Rank + 1;
                    var menuGroupOldObj = _menuService.LoadByRank(newRank);
                    menuGroupOldObj.Rank = menuGroupUpdateObj.Rank;
                    menuGroupUpdateObj.Rank = newRank;
                    bool isSuccessfullyUpdateRank = _menuService.UpdateRank(menuGroupOldObj, menuGroupUpdateObj);
                    if (isSuccessfullyUpdateRank)
                        return Json("Successfully changed");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            return Json("Something Wrong Occured!!");
        }
        #endregion

        #region Helper Function
        public ActionResult GetMenuGroup()
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    IList<MenuGroup> menuGroupList = _menuService.LoadAllMenuGroup();
                    return PartialView("Partial/_MenuGroup", menuGroupList);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            return HttpNotFound();
        }
        public JsonResult doesMenuGroupExist(string Name, long? Id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool isNotDuplicate = _menuService.DuplicationCheckMenuGroup(Name, Id);
                    return Json(isNotDuplicate);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            return Json(false);
        }
        #endregion
    }
}