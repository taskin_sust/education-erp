﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.GoodsReturnTest
{
    public interface IGoodsReturnLoadByIdTest
    {
        void LoadById_Null_Check();
        void GetWorkOrder_NotNull_Check();
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReturn")]
    public class GoodsReturnLoadByIdTest : GoodsReturnBaseTest, IGoodsReturnLoadByIdTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _goodsReturnService.GetGoodsReturn(-1);
            Assert.Null(obj);

        }

        [Fact]
        public void GetWorkOrder_NotNull_Check()
        {
            _goodsReturnFactory.Create().WithGoodsReturnDetailsAndGoodsReceive().Persist();
            var goodsreturn = _goodsReturnService.GetGoodsReturn(_goodsReturnFactory.Object.Id);
            Assert.NotNull(goodsreturn);
            Assert.True(goodsreturn.Id > 0);
        }
    }
}
