﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BranchTest
{
    interface IBranchUpdateTest
    {

        #region Basic Test
        void Update_Should_Return_Null_Exception();
        void Should_Update_Successfully();
        void Should_Not_Update_Empty_Name();
        void Should_Not_Update_Empty_Code();

        #endregion

        #region Business Logic Test
        void Should_Not_Update_Empty_Organization();
        void Update_Should_Check_Duplicate_Name_Entry();

        #endregion

    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Branch")]
    public class BranchUpdateTest : BranchBaseTest, IBranchUpdateTest
    {

        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var branchFactory =_branchFactory.Create().WithOrganization().Persist();
            Assert.Throws<NullObjectException>(() => _branchService.IsUpdate(branchFactory.Object.Id, null));
        }
        
        [Fact]
        public void Should_Update_Successfully()
        {
            var branchFactory =
                _branchFactory.Create().WithOrganization().WithNameAndRank(_name, 1).WithCode(_branchFactory.GetRandomNumber())
                    .Persist();
            Branch branch = _branchService.GetBranch(branchFactory.Object.Id);
            branch.Name = branch.Name + " DEF GHI";
            var success = _branchService.IsUpdate(branch.Id, branch);
            Assert.True(success);
        }
        [Fact]
        public void Should_Not_Update_Empty_Name()
        {
            var branchFactory =
                _branchFactory.Create().WithOrganization().WithNameAndRank(_name, 1).WithCode(_branchFactory.GetRandomNumber())
                    .Persist();
            branchFactory.Object.Name = "";
            Assert.Throws<MessageException>(() => _branchService.IsUpdate(branchFactory.Object.Id, branchFactory.Object));
        }
        [Fact]
        public void Should_Not_Update_Empty_Code()
        {
            var branchFactory =
                 _branchFactory.Create().WithOrganization().WithNameAndRank(_name, 1).WithCode(_branchFactory.GetRandomNumber())
                     .Persist();
            branchFactory.Object.Code = "";
            Assert.Throws<MessageException>(() => _branchService.IsUpdate(branchFactory.Object.Id, branchFactory.Object));
        }
        #endregion

        #region Business Logic Test
        [Fact]
        public void Update_Should_Check_Duplicate_Name_Entry()
        {
            var branchFactory = _branchFactory.Create().WithOrganization().Persist().Object;
            var branchFactory1 = _branchFactory.Create()
                .WithOrganization(branchFactory.Organization)
                .WithNameAndRank(Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZABC)",1)
                .Persist().Object;
            Branch branch = _branchService.GetBranch(branchFactory1.Id);
            branch.Name = branchFactory.Name;
            Assert.Throws<DuplicateEntryException>(() => _branchService.IsUpdate(branch.Id, branch));
        }
        [Fact]
        public void Should_Not_Update_Empty_Organization()
        {
            var branchFactory = _branchFactory.Create().WithOrganization().Persist();
            branchFactory.Object.Organization = null;
            Assert.Throws<MessageException>(() => _branchService.IsUpdate(branchFactory.Object.Id, branchFactory.Object));
        }
        #endregion

    }
}
