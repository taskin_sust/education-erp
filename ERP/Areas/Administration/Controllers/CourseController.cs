﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.App_Start;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class CourseController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ISubjectService _subjectService;
        private readonly ICourseService _courseService;
        private readonly ICommonHelper _commonHelper;
        private readonly IProgramSessionSubjectService _programSessionSubjectService;
        private readonly ICourseSubjectService _courseSubjectService;
        private readonly IUserService _userService;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> _userMenu;

        public CourseController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _subjectService = new SubjectService(session);
                _courseService = new CourseService(session);
                _commonHelper = new CommonHelper();
                _programSessionSubjectService = new ProgramSessionSubjectService(session);
                _courseSubjectService = new CourseSubjectService(session);
                _userService = new UserService(session);
                _organizationService = new OrganizationService(session);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }

        }

        #endregion

        #region Index/Manage Page

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ManagePackage()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                var program = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
                ViewBag.Organization = organization;
                ViewBag.Program = program;
                ViewBag.StatusText = _commonHelper.GetStatus();
                ViewBag.PageSize = Constants.PageSize;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }

            return View();
        }
       
        [HttpPost]
        public ActionResult ManagePackage(int draw, int start, int length, string organizationId, string programId, string sessionName, string name, string rank, string status)
        {
            int recordsTotal = 0;
            long recordsFiltered = 0;
            var data = new List<object>();
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    NameValueCollection nvc = Request.Form;
                    var orderBy = "";
                    var orderDir = "";
                    
                    #region OrderBy and Direction
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                                orderBy = "organization.Name";
                                break;
                            case "1":
                                orderBy = "program.Name";
                                break;
                            case "2":
                                orderBy = "refSession.Name";
                                break;
                            case "3":
                                orderBy = "Name";
                                break;
                            case "4":
                                orderBy = "Rank";
                                break;
                            case "5":
                                orderBy = "Status";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }
                    #endregion

                    recordsTotal = _courseService.GetCourseCount(orderBy, orderDir.ToUpper(),_userMenu, organizationId, programId, sessionName, name, rank, status);
                    recordsFiltered = recordsTotal;
                    var courselList = _courseService.LoadCourse(start, length, orderBy, orderDir.ToUpper(), _userMenu,organizationId, programId, sessionName, name, rank, status).ToList();
                    var course = new Course();
                    var maxMinRank = new int[2];
                    maxMinRank[0] = _courseService.GetMinimumRank(course);
                    maxMinRank[1] = _courseService.GetMaximumRank(course);
                    foreach (var c in courselList)
                    {
                        var str = new List<string>();
                        str.Add(c.Program.Organization.ShortName);
                        str.Add(c.Program.Name);
                        str.Add(c.RefSession.Name);
                        str.Add(c.Name);
                        str.Add(c.StartDate != null ? c.StartDate.Value.ToShortDateString() : "");
                        str.Add(c.EndDate != null ? c.EndDate.Value.ToShortDateString() : "");
                        str.Add(LinkGenerator.GetRankLinkForModal(c.Id, c.Rank, maxMinRank[0], maxMinRank[1]));
                        str.Add(_commonHelper.GetSatus(c.Status.ToString()));
                        str.Add(LinkGenerator.GetGeneratedDetailsEditModelDeleteLink("Details", "Edit", "Course", c.Id, c.Name));
                        data.Add(str);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });

        }

        #endregion

        #region Operational Function

        #region Save Operation
        
        [HttpGet]
        public ActionResult AddCourse()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult AddCourse(string[] courseFeesObjArray, long programId, long sessionId, string courseName, DateTime startDate, DateTime endDate, int maxSubject, int offMinSub, int pubMinSub, int offMinPay, int pubMinPay, bool offComp, bool pubComp)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;

            ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            try
            {
                if (String.IsNullOrEmpty(courseName))
                {
                    return Json(new Response(false, "Course name is required"));
                }

                if (courseFeesObjArray.Length < maxSubject)
                {
                    return Json(new Response(false, "Maximum subject Count Must be less than or equal to selected subject"));
                }

                if (courseFeesObjArray.Length > 0)
                {
                    var program = _programService.GetProgram(programId);
                    var session = _sessionService.LoadById(sessionId);
                    //assign course object
                    var course = new Course
                    {
                        Program = program,
                        RefSession = session,
                        Name = courseName.Trim(),
                        StartDate = startDate,
                        EndDate = endDate,
                        MaxSubject = maxSubject,
                        OfficeMinSubject = offMinSub,
                        PublicMinSubject = pubMinSub,
                        OfficeMinPayment = offMinPay,
                        PublicMinPayment = pubMinPay,
                        OfficeCompulsory = offComp,
                        PublicCompulsory = pubComp
                    };

                    var courseSubjects = new List<CourseSubject>();
                    foreach (var courseSubject in courseFeesObjArray)
                    {
                        var courseFees = System.Web.Helpers.Json.Decode(courseSubject);

                        var coursesubject = new CourseSubject();
                        foreach (KeyValuePair<string, object> courseFeesObj in courseFees)
                        {
                            if (courseFeesObj.Key == "id")
                            {
                                coursesubject.Subject = _subjectService.GetSubject(Convert.ToInt64(courseFeesObj.Value));
                            }
                            else if (courseFeesObj.Key == "guide")
                            {
                                coursesubject.GuideFee = Convert.ToDecimal(courseFeesObj.Value);
                            }
                            else if (courseFeesObj.Key == "tutionfee")
                            {
                                coursesubject.TutionFee = Convert.ToDecimal(courseFeesObj.Value);
                            }
                            else if (courseFeesObj.Key == "totalPay")
                            {
                                coursesubject.Payment = Convert.ToInt64(courseFeesObj.Value);
                            }
                        }
                        courseSubjects.Add(coursesubject);
                    }
                    course.CourseSubjects = courseSubjects;
                    _courseService.CourseSaveOrUpdate(course);
                    return Json(new Response(true, "Course Add Successfully"));

                }

                return View();
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (MessageException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return RedirectToAction("AddCourse");
            }
        }

        #endregion

        #region Update Operation

        [HttpGet]
        public ActionResult Edit(long id, string message = "")
        {
            Course course;
            SelectList statusList;
            try
            {
                long cId;
                bool isNum = long.TryParse(id.ToString(), out cId);
                if (!isNum)
                {
                    return HttpNotFound();
                }
                course = _courseService.GetCourse(id);

                ViewBag.CheckCourse = _courseService.CheckCouseAssignByStudent(id);

                IList<ProgramSessionSubject> programSessionSubjects = _programSessionSubjectService.LoadProgramSessionSubject(course.Program.Id, course.RefSession.Id);
                IList<CourseSubject> courseSubjects = _courseSubjectService.LoadCourseSubject(id);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                var programList = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name", course.Program.Id);
                var sessionList = new SelectList(_sessionService.LoadAuthorizedSession(_userMenu, null, _commonHelper.ConvertIdToList(course.Program.Id)), "Id", "Name", course.RefSession.Id);
                statusList = new SelectList(_commonHelper.GetStatus(), "Key", "Value");
                var courselStatusList = (from KeyValuePair<string, int> statusKeyValuePair in statusList.Items
                                         select statusKeyValuePair.Value.ToString().Trim().Equals(course.Status.ToString()) ? new SelectListItem()
                                         {
                                             Text = statusKeyValuePair.Key,
                                             Value = statusKeyValuePair.Value.ToString(),
                                             Selected = true
                                         } : new SelectListItem()
                                         {
                                             Text = statusKeyValuePair.Key,
                                             Value = statusKeyValuePair.Value.ToString()
                                         }).ToList();
                ViewBag.program = programList;
                ViewBag.session = sessionList;
                ViewBag.status = courselStatusList;
                ViewBag.allSubList = programSessionSubjects;
                ViewBag.assignedSubjectList = courseSubjects;
                if (!String.IsNullOrEmpty(message))
                {
                    ViewBag.SuccessMessage = message;
                }

                return View(course);
            }
            catch (Exception ex)
            {
                course = _courseService.GetCourse(id);
                statusList = new SelectList(_commonHelper.GetStatus(), "Key", "Value");
                var programType = new SelectList(_programService.GetProgramType(), "Key", "Value");
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                ViewBag.Status = statusList;
                ViewBag.ProgramType = programType;
                _logger.Error(ex);
                return View(course);
            }
        }

        [HttpPost]
        public ActionResult Edit(string[] courseFeesObjArray, string programId, string sessionId, string courseName, int maxSubject, string courseId, string status, int offMinSub, int pubMinSub, decimal offMinPay, decimal pubMinPay, bool offComp, bool pubComp, DateTime startDate, DateTime endDate)
        {
            var courselStatusList = new List<SelectListItem>();
            try
            {
                var oldCourse = _courseService.GetCourse(Convert.ToInt64(courseId));

                if (oldCourse != null)
                {
                    var checkCourse = _courseService.CheckCouseAssignByStudent(Convert.ToInt64(courseId));
                    if (checkCourse)
                    {
                        var oldEndDate = oldCourse.EndDate;
                        if (oldEndDate <= endDate) oldCourse.EndDate = endDate;
                        else return Json(new Response(false, "End date must be greater than previous end date"));

                        var subjectIds = new long[courseFeesObjArray.Length + 1];
                        long cId = 0;
                        int jj = 0;
                        foreach (var courseSubject in courseFeesObjArray)
                        {
                            long subId = 0;
                            //decimal payment = 0;
                            var courseFees = System.Web.Helpers.Json.Decode(courseSubject);
                            var ii = 0;

                            foreach (KeyValuePair<string, object> courseFeesObj in courseFees)
                            {
                                if (courseFeesObj.Key == "id")
                                {
                                    subId = Convert.ToInt64(courseFeesObj.Value);
                                }
                                else if (courseFeesObj.Key == "courseid")
                                {
                                    cId = Convert.ToInt64(courseFeesObj.Value);
                                }
                            }

                            subjectIds[jj++] = subId;
                        }
                        var fullCourseSubjectList = oldCourse.CourseSubjects.ToList();
                        var commonCourseSubjectList = oldCourse.CourseSubjects.Where(x => subjectIds.Contains(x.Subject.Id)).ToList();
                        var removedCourseSubjectList = fullCourseSubjectList.Except(commonCourseSubjectList).ToList();
                        foreach (var courseSubject in removedCourseSubjectList)
                        {
                            if (courseSubject.StudentPaymentList.Any())
                            {
                                return Json(new Response(false, "You can't remove \"" + courseSubject.Subject.Name + "\". A payment already made against this subject."));
                            }
                        }
                    }
                    else
                    {
                        oldCourse.StartDate = startDate;
                        oldCourse.EndDate = endDate;
                    }
                    var programObj = _programService.GetProgram(Convert.ToInt64(programId));
                    var sessionObj = _sessionService.LoadById(Convert.ToInt64(sessionId));
                    oldCourse.Program = programObj;
                    oldCourse.RefSession = sessionObj;
                    oldCourse.Status = Convert.ToInt32(status);
                    oldCourse.Name = courseName;
                    oldCourse.MaxSubject = maxSubject;
                    oldCourse.OfficeMinSubject = offMinSub;
                    oldCourse.PublicMinSubject = pubMinSub;
                    oldCourse.OfficeMinPayment = offMinPay;
                    oldCourse.PublicMinPayment = pubMinPay;
                    oldCourse.OfficeCompulsory = offComp;
                    oldCourse.PublicCompulsory = pubComp;
                    if (courseFeesObjArray.Length < maxSubject)
                    {
                        return Json(new Response(false, "Maximum subject Count Must be less then or equal to selected subject"));
                    }

                    #region Move from Service
                    IList<CourseSubject> cSubList = new List<CourseSubject>();
                    foreach (var courseSubject in courseFeesObjArray)
                    {
                        var flag = 0;
                        var jj = 0;
                        foreach (var oldCSubject in oldCourse.CourseSubjects)
                        {
                            jj++;
                            long subId = 0;
                            long cId = 0;
                            decimal payment = 0;
                            decimal guide = 0;
                            decimal tutionfee = 0;

                            var courseFees = System.Web.Helpers.Json.Decode(courseSubject);

                            foreach (KeyValuePair<string, object> courseFeesObj in courseFees)
                            {
                                switch (courseFeesObj.Key)
                                {
                                    case "id":
                                        subId = Convert.ToInt64(courseFeesObj.Value);
                                        break;
                                    case "courseid":
                                        cId = Convert.ToInt64(courseFeesObj.Value);
                                        break;
                                    case "guide":
                                        guide = Convert.ToInt64(courseFeesObj.Value);
                                        break;
                                    case "tutionfee":
                                        tutionfee = Convert.ToInt64(courseFeesObj.Value);
                                        break;
                                    case "totalPay":
                                        payment = Convert.ToInt64(courseFeesObj.Value);
                                        break;
                                }
                            }

                            // payment = tutionfee + guide;

                            var subject = _subjectService.GetSubject(subId);
                            var oldCourseSubject = _courseSubjectService.LoadCourseSubjectByCourseIdAndSubjectId(cId, subId);

                            if (oldCSubject == oldCourseSubject)
                            {
                                oldCourseSubject.Subject = subject;
                                oldCourseSubject.Course = oldCourse;
                                oldCourseSubject.Payment = payment;
                                oldCourseSubject.GuideFee = guide;
                                oldCourseSubject.TutionFee = tutionfee;
                                if (oldCourseSubject.Status == 0)
                                {
                                    oldCourseSubject.Status = Course.EntityStatus.Active;
                                }
                                if (oldCourseSubject.CreateBy == 0)
                                {
                                    oldCourseSubject.CreateBy = oldCourse.CreateBy;
                                }
                                oldCourseSubject.ModifyBy = _courseService.GetCurrentUserId();
                                cSubList.Add(oldCourseSubject);
                                flag = 1;
                            }
                            if (flag == 1) { break; }
                            if (flag == 0 && oldCourse.CourseSubjects.Count == jj)
                            {
                                var courseSubjectObj = new CourseSubject
                                {
                                    Course = oldCourse,
                                    Subject = subject,
                                    Payment = payment,
                                    GuideFee = guide,
                                    TutionFee = tutionfee,
                                    Status = Course.EntityStatus.Active,
                                    CreateBy = _courseService.GetCurrentUserId(),
                                    ModifyBy = _courseService.GetCurrentUserId(),
                                };
                                cSubList.Add(courseSubjectObj);
                            }
                        }
                    }

                    #endregion
                    oldCourse.CourseSubjects = cSubList;
                    _courseService.CourseSaveOrUpdate(oldCourse);

                    return Json(new Response(true, "Success"));

                    //return Json(new Response(false, "Failed"));
                }

                return Json(new Response(false, "Course not found"));
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new Response(false, ex.Message));

            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                ViewBag.program = new SelectList(_programService.LoadProgram(null, null), "Id", "Name");
                ViewBag.session = new SelectList(_sessionService.LoadSession(), "Id", "Name");
                ViewBag.status = courselStatusList;
                return View();
            }
        }

        #endregion

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                if (id == 0)
                {
                    return HttpNotFound();
                }
                var courseObj = _courseService.GetCourse(id);
                bool isDeleteSuccess = _courseService.IsDelete(courseObj);
                if (isDeleteSuccess)
                {
                    return Json(new Response(true, "Course Delete Successfully"));
                }
                return Json(new Response(false, "Course Delete Fail"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Details Operation

        [HttpGet]
        public ActionResult Details(long id)
        {
            try
            {
                if (id == 0)
                {
                    return HttpNotFound();
                }
                long cId;
                bool isNum = long.TryParse(id.ToString(CultureInfo.InvariantCulture), out cId);
                if (!isNum)
                {
                    return HttpNotFound();
                }
                var course = _courseService.GetCourse(id);
                if (course != null)
                {
                    course.CreateByText = _userService.GetUserNameByAspNetUserId(course.CreateBy);
                    course.ModifyByText = _userService.GetUserNameByAspNetUserId(course.ModifyBy);
                }
                return View(course);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return View();
            }
        }

        #endregion

        #region Rank Operation

        public ActionResult RankChange(int id, int current, string action)
        {
            if (!Request.IsAjaxRequest()) return HttpNotFound();
            try
            {
                var courseUpdateObj = _courseService.GetCourse(Convert.ToInt64(id));
                if (courseUpdateObj == null)
                {
                    return HttpNotFound();
                }
                int newRank;
                if (action == "up")
                    newRank = courseUpdateObj.Rank - 1;
                else
                    newRank = courseUpdateObj.Rank + 1;

                var courseGroupOldObj = _courseService.GetCourseByRankNextOrPrevious(newRank, action);//LoadByRank(newRank);
                newRank = courseGroupOldObj.Rank;

                courseGroupOldObj.Rank = courseUpdateObj.Rank;
                courseUpdateObj.Rank = newRank;

                bool isSuccessfullyUpdateRank = _courseService.UpdateRank(courseGroupOldObj, courseUpdateObj);
                if (isSuccessfullyUpdateRank)
                {
                    return Json(new Response(false, "Successfully changed"));
                }
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }

        }

        #endregion

        #endregion

        #region Helper Function

        [HttpGet]
        public ActionResult GetAssignedSubject(string program = "", string session = "")
        {
            try
            {
                IList<Subject> subList = new List<Subject>();
                Program programForView = new Program();
                ViewBag.IsPaid = false;
                if (!String.IsNullOrEmpty(program) && !String.IsNullOrEmpty(session))
                {
                    program = program.Trim();
                    session = session.Trim();
                    programForView = _programService.GetProgram(Convert.ToInt64(program));
                    IList<ProgramSessionSubject> programBranchSessionSubjects = _programSessionSubjectService.LoadProgramSessionSubject(Convert.ToInt64(program), Convert.ToInt64(session));
                    foreach (Subject subject in from programBranchSessionSubject in programBranchSessionSubjects
                                                where programBranchSessionSubject.Subject.Status == Subject.EntityStatus.Active
                                                select _subjectService.GetSubject(programBranchSessionSubject.Subject.Id))
                    {
                        subList.Add(subject);
                    }
                }
                if (programForView != null)
                    ViewBag.IsPaid = (programForView.Type == 1) ? 1 : 0;
                ViewBag.subject = subList;
                return PartialView("Partial/_SubjectList");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion
    }
}