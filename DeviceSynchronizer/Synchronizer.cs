﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DeviceSynchronizer.Dao;
using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model;
using DeviceSynchronizer.Services;
using log4net;
using log4net.Config;
using Newtonsoft.Json;

namespace DeviceSynchronizer
{
    public partial class Synchronizer : Form
    {
        #region Tab-1 Main Synchronizer

        #region Declaration

        private static readonly ILog Log = LogManager.GetLogger("SynchronizerLogger");
        private SynchronizerThread _deviceSynchronizerThread;
        private ServiceDelegate _serviceDelegate;
        private MessageCleared _messageCleared;
        private bool _manualExit = false;
        private bool _fromResizeByCode = true;

        #endregion

        #region Events and delegate

        private delegate void UpdateListViewInvoker(string message, MessageType messageType);
        private delegate void ClearListViewInvoker();
        public delegate void MessageCleared();

        #endregion

        #region Constructor
        public Synchronizer()
        {
            try
            {
                InitializeComponent();
                SetTitle();
                axCZKEM1.Size = new Size(50, 50);
                _messageCleared = OnMessageCleared;
                XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "log4net.config"));

                //remove tab
                tabControl1.TabPages.Clear();
                tabControl1.TabPages.Add(Main_Synchronizer);

                //notification icon
                var title = BusinessRules.AppName + BusinessRules.AppVersion;
                notifyIcon.BalloonTipTitle = title;
                notifyIcon.BalloonTipText = BusinessRules.AppName + " is minimized here.";
                notifyIcon.Text = title;
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        #endregion

        #region Start and Stop Button

        [STAThread]
        private void startBtn_Click(object sender, EventArgs e)
        {
            try
            {
                startBtn.Enabled = false;
                stopBtn.Enabled = true;

                if (_deviceSynchronizerThread == null)
                {
                    //for first time
                    DbHelper.CreateDb();
                    _deviceSynchronizerThread = new SynchronizerThread(OnGuiMessaging, SetTitle);
                    _deviceSynchronizerThread.Start();
                }
                else if (_deviceSynchronizerThread._serviceStatus.IsRunning)
                {
                    //Stop service first
                    OnGuiMessaging("program already running plz stop it first..", MessageType.Info);
                }
                else
                {
                    //just restart 
                    _messageCleared();
                    _deviceSynchronizerThread.Restart();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {
            try
            {
                startBtn.Enabled = true;
                stopBtn.Enabled = false;

                if (_deviceSynchronizerThread != null)
                    _deviceSynchronizerThread.Stop();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            string msg = "Are you sure you want to close \"" + BusinessRules.AppName + "\" software?";
            if (MessageBox.Show(msg, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _manualExit = true;
                Close();
            }
        }
        #endregion

        #region Others

        private void OnMessageCleared()
        {
            try
            {
                if (currentStatedataGridView.InvokeRequired)
                {
                    var me = new ClearListViewInvoker(OnMessageCleared);
                    currentStatedataGridView.Invoke(me);
                }
                else
                {
                    currentStatedataGridView.Rows.Clear();
                    currentStatedataGridView.Refresh();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void OnGuiMessaging(string message, MessageType messageType)
        {
            try
            {
                if (currentStatedataGridView.InvokeRequired)
                {
                    var mi = new UpdateListViewInvoker(OnGuiMessaging);
                    currentStatedataGridView.Invoke(mi, message, messageType);
                }
                else
                {
                    //set color
                    var rowColor = Color.WhiteSmoke;
                    if (messageType == MessageType.Attention)
                        rowColor = Color.LightGoldenrodYellow;
                    else if (messageType == MessageType.Success)
                        rowColor = Color.LightGreen;
                    else if (messageType == MessageType.Error)
                        rowColor = Color.LightCoral;
                    else if (messageType == MessageType.Important)
                        rowColor = Color.LightSkyBlue;

                    //add row
                    string[] rowData = new string[] { DateTime.Now.ToString(BusinessRules.DateTimeFormat), message };
                    currentStatedataGridView.Rows.Insert(0, rowData);
                    currentStatedataGridView.Rows[0].DefaultCellStyle.BackColor = rowColor;

                    //clear
                    if (currentStatedataGridView.Rows.Count > 20000)
                        _messageCleared();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        #endregion

        #region Helper

        private void SetTitle(string syncName = null)
        {
            if (this.InvokeRequired)
            {
                var mi = new ServiceDelegate.SetAppTitleDelegate(SetTitle);
                this.Invoke(mi, syncName);
            }
            else
            {
                var title = BusinessRules.AppName + BusinessRules.AppVersion + "DevForm";
                if (!String.IsNullOrEmpty(syncName))
                    title += " [" + syncName + "]";

                this.Text = title;
            }
        }

        #endregion

        #endregion

        #region Tab-2 Attd log Clear
        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIP.Text.Trim() == "" || txtPort.Text.Trim() == "")
                {
                    MessageBox.Show(@"IP and Port cannot be null", @"Error");
                    return;
                }


                int idwErrorCode = 0;
                Cursor = Cursors.WaitCursor;
                axCZKEM1.SetCommPassword(419555);

                if (Connect_t2_Btn.Text == @"DisConnect")
                {
                    axCZKEM1.Disconnect();
                    bIsConnected = false;
                    Connect_t2_Btn.Text = @"Connect";
                    lblState.Text = @"Current State:DisConnected";


                    //this.axCZKEM1.OnVerify -= new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                    //this.axCZKEM1.OnAttTransaction -= new zkemkeeper._IZKEMEvents_OnAttTransactionEventHandler(axCZKEM1_OnAttTransaction);
                    //this.axCZKEM1.OnAttTransactionEx -= new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
                    //this.axCZKEM1.OnNewUser -= new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
                    //this.axCZKEM1.OnHIDNum -= new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
                    //this.axCZKEM1.OnWriteCard -= new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
                    //this.axCZKEM1.OnEmptyCard -= new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);

                    Cursor = Cursors.Default;
                    return;
                }
                bIsConnected = axCZKEM1.Connect_Net(txtIP.Text, Convert.ToInt32(txtPort.Text));
                if (bIsConnected == true)
                {
                    Connect_t2_Btn.Text = @"DisConnect";
                    Connect_t2_Btn.Refresh();
                    lblState.Text = @"Current State:Connected";
                    iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                    if (axCZKEM1.RegEvent(iMachineNumber, 65535))
                    //Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                    {
                        //this.axCZKEM1.OnVerify += axCZKEM1_OnVerify;
                        //this.axCZKEM1.OnAttTransaction += axCZKEM1_OnAttTransaction;
                        //this.axCZKEM1.OnNewUser += axCZKEM1_OnNewUser;
                        //this.axCZKEM1.OnAttTransactionEx += axCZKEM1_OnAttTransactionEx;
                        //this.axCZKEM1.OnWriteCard += axCZKEM1_OnWriteCard;
                        //this.axCZKEM1.OnEmptyCard += axCZKEM1_OnEmptyCard;
                    }
                }
                else
                {
                    axCZKEM1.GetLastError(ref idwErrorCode);
                    MessageBox.Show(@"Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), @"Error");
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Connection Failed. " + ex.Message, @"Error");
            }
        }


        /*************************************************************************************************
        * Before you refer to this demo,we strongly suggest you read the development manual deeply first.*
        * This part is for demonstrating operations with(read/get/clear) the attendance records.         *
        * ************************************************************************************************/
        #region AttLogs

        //Download the attendance records from the device.
        private void btnGetGeneralLogData_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show(@"Please connect the device first", @"Error");
                return;
            }

            int idwTMachineNumber = 0;
            int idwEnrollNumber = 0;
            int idwEMachineNumber = 0;
            int idwVerifyMode = 0;
            int idwInOutMode = 0;
            int idwYear = 0;
            int idwMonth = 0;
            int idwDay = 0;
            int idwHour = 0;
            int idwMinute = 0;

            int idwErrorCode = 0;
            int iGLCount = 0;
            int iIndex = 0;

            Cursor = Cursors.WaitCursor;
            lvLogs.Items.Clear();
            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
            {
                while (axCZKEM1.GetGeneralLogData(iMachineNumber, ref idwTMachineNumber, ref idwEnrollNumber,
                        ref idwEMachineNumber, ref idwVerifyMode, ref idwInOutMode, ref idwYear, ref idwMonth, ref idwDay, ref idwHour, ref idwMinute))//get records from the memory
                {
                    iGLCount++;
                    lvLogs.Items.Add(iGLCount.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwEnrollNumber.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwVerifyMode.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwInOutMode.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString());
                    iIndex++;
                }
            }
            else
            {
                Cursor = Cursors.Default;
                axCZKEM1.GetLastError(ref idwErrorCode);

                if (idwErrorCode != 0)
                {
                    MessageBox.Show(@"Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), @"Error");
                }
                else
                {
                    MessageBox.Show(@"No data from terminal returns!", @"Error");
                }
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
            Cursor = Cursors.Default;
        }
        #endregion
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private int iMachineNumber = 1;//the serial number of the device.After connecting the device ,this value will be changed.

        private void Connect_t2_Btn_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIP.Text.Trim() == "" || txtPort.Text.Trim() == "")
                {
                    MessageBox.Show(@"IP and Port cannot be null", @"Error");
                    return;
                }
                int idwErrorCode = 0;
                Cursor = Cursors.WaitCursor;
                //axCZKEM1.SetCommPassword(419555);
                axCZKEM1.SetCommPassword(Convert.ToInt32(txtPass.Text));

                if (Connect_t2_Btn.Text == @"DisConnect")
                {
                    axCZKEM1.Disconnect();
                    bIsConnected = false;
                    Connect_t2_Btn.Text = @"Connect";
                    lblState.Text = @"Current State:DisConnected";
                    Cursor = Cursors.Default;
                    return;
                }
                bIsConnected = axCZKEM1.Connect_Net(txtIP.Text, Convert.ToInt32(txtPort.Text));
                if (bIsConnected == true)
                {
                    Connect_t2_Btn.Text = @"DisConnect";
                    Connect_t2_Btn.Refresh();
                    lblState.Text = @"Current State:Connected";
                    iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                }
                else
                {
                    axCZKEM1.GetLastError(ref idwErrorCode);
                    MessageBox.Show(@"Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), @"Error");
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Connection Failed. " + ex.Message, @"Error");
            }
        }

        private void btnClearGLog_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show(@"Please connect the device first", "Error");
                return;
            }
            int idwErrorCode = 0;

            lvLogs.Items.Clear();
            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device

            if (axCZKEM1.ClearGLog(iMachineNumber))
            {
                axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                MessageBox.Show(@"All att Logs have been cleared from teiminal!", @"Success");
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show(@"Operation failed,ErrorCode=" + idwErrorCode.ToString(), @"Error");
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
        }

        private void btnGetGeneralLogDataStr_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show(@"Please connect the device first", @"Error");
                return;
            }
            int idwErrorCode = 0;

            int idwEnrollNumber = 0;
            int idwVerifyMode = 0;
            int idwInOutMode = 0;
            string sTime = "";

            int iGLCount = 0;
            int iIndex = 0;

            Cursor = Cursors.WaitCursor;
            lvLogs.Items.Clear();
            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read the records to the memory
            {
                while (axCZKEM1.GetGeneralLogDataStr(iMachineNumber, ref idwEnrollNumber, ref idwVerifyMode, ref idwInOutMode, ref sTime))//get the records from memory
                {
                    iGLCount++;
                    lvLogs.Items.Add(iGLCount.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwEnrollNumber.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwVerifyMode.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwInOutMode.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(sTime);
                    iIndex++;
                }
            }
            else
            {
                Cursor = Cursors.Default;
                axCZKEM1.GetLastError(ref idwErrorCode);

                if (idwErrorCode != 0)
                {
                    MessageBox.Show(@"Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), @"Error");
                }
                else
                {
                    axCZKEM1.GetLastError(ref idwErrorCode);
                    MessageBox.Show(@"Operation failed,ErrorCode=" + idwErrorCode.ToString(), @"Error");
                }
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
            Cursor = Cursors.Default;
        }

        private void btnGetGeneralExtLogData_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show(@"Please connect the device first", @"Error");
                return;
            }

            int idwErrorCode = 0;

            int idwEnrollNumber = 0;
            int idwVerifyMode = 0;
            int idwInOutMode = 0;

            int idwYear = 0;
            int idwMonth = 0;
            int idwDay = 0;
            int idwHour = 0;
            int idwMinute = 0;
            int idwSecond = 0;
            int idwWorkCode = 0;
            int idwReserved = 0;

            int iGLCount = 0;
            int iIndex = 0;

            Cursor = Cursors.WaitCursor;
            lvLogs.Items.Clear();
            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
            {
                while (axCZKEM1.GetGeneralExtLogData(iMachineNumber, ref idwEnrollNumber, ref idwVerifyMode, ref idwInOutMode,
                         ref idwYear, ref idwMonth, ref idwDay, ref idwHour, ref idwMinute, ref idwSecond, ref idwWorkCode, ref idwReserved))//get records from the memory
                {
                    iGLCount++;
                    lvLogs.Items.Add(iGLCount.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwEnrollNumber.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwVerifyMode.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwInOutMode.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwWorkCode.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwReserved.ToString());
                    iIndex++;
                }
            }
            else
            {
                Cursor = Cursors.Default;
                axCZKEM1.GetLastError(ref idwErrorCode);

                if (idwErrorCode != 0)
                {
                    MessageBox.Show(@"Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), @"Error");
                }
                else
                {
                    MessageBox.Show(@"No data from terminal returns!", @"Error");
                }
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
            Cursor = Cursors.Default;
        }

        private void btnGetDeviceStatus_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show(@"Please connect the device first", @"Error");
                return;
            }
            int idwErrorCode = 0;
            int iValue = 0;

            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.GetDeviceStatus(iMachineNumber, 6, ref iValue)) //Here we use the function "GetDeviceStatus" to get the record's count.The parameter "Status" is 6.
            {
                MessageBox.Show(@"The count of the AttLogs in the device is " + iValue.ToString(), @"Success");
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show(@"Operation failed,ErrorCode=" + idwErrorCode.ToString(), @"Error");
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (bIsConnected == false)
                {
                    MessageBox.Show(@"Please connect the device first", @"Error");
                    return;
                }
                int idwErrorCode = 0;
                int iValue = 0;
                lvLogs.Items.Clear();
                axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device

                int selectedIndex = dataType.SelectedIndex + 1;
                Object selectedItem = dataType.SelectedItem;
                // ComboboxItem selectedCar = (ComboboxItem)cmb.SelectedItem;
                //int dataTypeValue = Convert.ToInt32(dataType.Text);

                if (axCZKEM1.ClearData(iMachineNumber, selectedIndex))
                {
                    axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                    MessageBox.Show(@"All " + selectedItem + @"data have been cleared from teiminal!", @"Success");
                }
                else
                {
                    axCZKEM1.GetLastError(ref idwErrorCode);
                    MessageBox.Show(@"Operation failed,ErrorCode=" + idwErrorCode.ToString(), @"Error");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Connection Failed. " + ex.Message, @"Error");
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (bIsConnected == false)
                {
                    MessageBox.Show(@"Please connect the device first", @"Error");
                    return;
                }
                int idwErrorCode = 0;
                int iValue = 0;
                lvLogs.Items.Clear();
                axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device

                int selectedIndex = dataType.SelectedIndex + 1;
                Object selectedItem = dataType.SelectedItem;
                // ComboboxItem selectedCar = (ComboboxItem)cmb.SelectedItem;
                //int dataTypeValue = Convert.ToInt32(dataType.Text);

                if (axCZKEM1.ClearData(iMachineNumber, selectedIndex))
                {
                    axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                    MessageBox.Show(@"All " + selectedItem + @"data have been cleared from teiminal!", @"Success");
                }
                else
                {
                    axCZKEM1.GetLastError(ref idwErrorCode);
                    MessageBox.Show(@"Operation failed,ErrorCode=" + idwErrorCode.ToString(), @"Error");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Connection Failed. " + ex.Message, @"Error");
            }
        }

        #endregion

        #region tab-3 Attd data show and reg new Member

        /**************************************************************************************************
        * Before you refer to this demo,we strongly suggest you read the development manual deeply first. *
        * This part is for demonstrating  operations on card(ID card and HID card) device.                *
        * It shows how to get or set card number,how to write data to Mifare card or empty it, etc.       *
        * *************************************************************************************************/
        #region Card Operation
        private void btnGetStrCardNumber_Click(object sender, EventArgs e)
        {
            try
            {
                if (bIsConnected == false)
                {
                    MessageBox.Show(@"Please connect the device first!", @"Error");
                    return;
                }

                int idwEnrollNumber = 0;
                string sName = "";
                string sPassword = "";
                int iPrivilege = 0;
                bool bEnabled = false;
                string sCardnumber = "";

                lvCard.Items.Clear();
                lvCard.BeginUpdate();
                Cursor = Cursors.WaitCursor;
                axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
                axCZKEM1.ReadAllUserID(iMachineNumber);//read all the user information to the memory

                while (axCZKEM1.GetAllUserInfo(iMachineNumber, ref idwEnrollNumber, ref sName, ref sPassword, ref iPrivilege, ref bEnabled))//get user information from memory
                {
                    bool isSuccess = axCZKEM1.GetStrCardNumber(out sCardnumber);
                    string CardNo2 = axCZKEM1.get_STR_CardNumber(idwEnrollNumber);
                    if (true)//get the card number from the memory
                    {
                        ListViewItem list = new ListViewItem();
                        list.Text = idwEnrollNumber.ToString();
                        list.SubItems.Add(sName);
                        list.SubItems.Add(sCardnumber + "  -  " + CardNo2);
                        list.SubItems.Add(iPrivilege.ToString());
                        list.SubItems.Add(sPassword);
                        if (bEnabled == true)
                        {
                            list.SubItems.Add("true");
                        }
                        else
                        {
                            list.SubItems.Add("false");
                        }
                        lvCard.Items.Add(list);
                    }
                }
                axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
                lvCard.EndUpdate();
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Failed. " + ex.Message, @"Error");
            }
        }

        private void btnSetStrCardNumber_Click(object sender, EventArgs e)
        {
            try
            {
                if (bIsConnected == false)
                {
                    MessageBox.Show(@"Please connect the device first!", @"Error");
                    return;
                }

                if (txtUserID.Text.Trim() == "" || cbPrivilege.Text.Trim() == "" || txtCardnumber.Text.Trim() == "")
                {
                    MessageBox.Show(@"UserID,Privilege,Cardnumber must be inputted first!", @"Error");
                    return;
                }
                int idwErrorCode = 0;

                bool bEnabled = true;
                if (chbEnabled.Checked)
                {
                    bEnabled = true;
                }
                else
                {
                    bEnabled = false;
                }
                int idwEnrollNumber = Convert.ToInt32(txtUserID.Text.Trim());
                string sName = txtName.Text.Trim();
                string sPassword = txtPassword.Text.Trim();
                int iPrivilege = Convert.ToInt32(cbPrivilege.Text.Trim());
                string sCardnumber = txtCardnumber.Text.Trim();

                Cursor = Cursors.WaitCursor;
                axCZKEM1.EnableDevice(iMachineNumber, false);
                axCZKEM1.SetStrCardNumber(sCardnumber);//Before you using function SetUserInfo,set the card number to make sure you can upload it to the device
                if (axCZKEM1.SetUserInfo(iMachineNumber, idwEnrollNumber, sName, sPassword, iPrivilege, bEnabled))//upload the user's information(card number included)
                {
                    MessageBox.Show(@"SetUserInfo,UserID:" + idwEnrollNumber.ToString() + @" Privilege:" + iPrivilege.ToString() + @" Cardnumber:" + sCardnumber + @" Enabled:" + bEnabled.ToString(), @"Success");
                }
                else
                {
                    axCZKEM1.GetLastError(ref idwErrorCode);
                    MessageBox.Show(@"Operation failed,ErrorCode=" + idwErrorCode.ToString(), @"Error");
                }
                axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                axCZKEM1.EnableDevice(iMachineNumber, true);
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Failed. " + ex.Message, @"Error");
            }
        }
        #endregion

        private void Connect_t3_btn_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIP.Text.Trim() == "" || txtPort.Text.Trim() == "")
                {
                    MessageBox.Show(@"IP and Port cannot be null", @"Error");
                    return;
                }
                int idwErrorCode = 0;
                Cursor = Cursors.WaitCursor;
                //axCZKEM1.SetCommPassword(419555);
                axCZKEM1.SetCommPassword(Convert.ToInt32(txtPass2.Text));

                if (Connect_t3_btn.Text == @"DisConnect")
                {
                    axCZKEM1.Disconnect();
                    bIsConnected = false;
                    Connect_t3_btn.Text = @"Connect";
                    label7.Text = @"Current State:DisConnected";
                    Cursor = Cursors.Default;
                    return;
                }
                bIsConnected = axCZKEM1.Connect_Net(txtIP.Text, Convert.ToInt32(txtPort.Text));
                if (bIsConnected == true)
                {
                    Connect_t3_btn.Text = @"DisConnect";
                    Connect_t3_btn.Refresh();
                    label7.Text = @"Current State:Connected";
                    iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                }
                else
                {
                    axCZKEM1.GetLastError(ref idwErrorCode);
                    MessageBox.Show(@"Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), @"Error");
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Connection Failed. " + ex.Message, @"Error");
            }
        }

        private void deleteUser_Btn_Click(object sender, EventArgs e)
        {
            try
            {
                if (bIsConnected == false)
                {
                    MessageBox.Show(@"Please connect the device first", @"Error");
                    return;
                }
                if (delPin.Text.Trim() == "")
                {
                    MessageBox.Show(@"Invalid UserID", @"Error");
                    return;
                }
                int idwErrorCode = 0;
                Cursor = Cursors.WaitCursor;
                axCZKEM1.EnableDevice(iMachineNumber, false);

                if (axCZKEM1.DeleteUserInfoEx(iMachineNumber, Convert.ToInt32(delPin.Text)))
                {
                    MessageBox.Show(@"User Delete Success", @"Success");
                }
                else
                {
                    axCZKEM1.GetLastError(ref idwErrorCode);
                    MessageBox.Show(@"User Delete Failed, error code: " + idwErrorCode, @"Error");
                }
                Cursor = Cursors.Default;
                //axCZKEM1.SetStrCardNumber(sCardnumber);//Before you using function SetUserInfo,set the card number to make sure you can upload it to the device

            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Failed. " + ex.Message, @"Error");
            }
        }
        #endregion

        #region Form events

        private void Synchronizer_Load(object sender, EventArgs e)
        {
            if (BusinessRules.DevMode)
            {
                this.ShowInTaskbar = true;
            }
            else
            {
                this.startBtn.PerformClick();
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void Synchronizer_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon.Visible = true;
                notifyIcon.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon.Visible = false;
            }
        }

        private void Synchronizer_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (_manualExit)
                {
                    stopBtn.PerformClick();
                }
                else
                {
                    e.Cancel = true;
                    this.WindowState = FormWindowState.Minimized;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //first time resize to half-window-size
            if (_fromResizeByCode)
            {
                this.Top = this.Left = 0;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width / 2;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height;
                this.ShowInTaskbar = true;
                this._fromResizeByCode = false;
            }

            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
        #endregion
    }
}
