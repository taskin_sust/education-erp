﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class AttendanceDeviceController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICampusService _campusService;
        private readonly IBranchService _branchService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IAttendanceDeviceService _hrAttendanceDeviceService;
        private readonly IAttendanceSynchronizerService _hrAttendanceSynchronizerService;
        private readonly IAttendanceDeviceTypeService _attendanceDeviceTypeService;

        public AttendanceDeviceController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _campusService = new CampusService(session);
            _branchService = new BranchService(session);
            _userService = new UserService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _hrAttendanceDeviceService = new AttendanceDeviceService(session);
            _hrAttendanceSynchronizerService = new AttendanceSynchronizerService(session);
            _attendanceDeviceTypeService = new AttendanceDeviceTypeService(session);
        }

        #endregion

        #region Save Operation

        public ActionResult Create()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                ViewBag.attdDeviceType = new SelectList(_attendanceDeviceTypeService.LoadAttendanceDeviceType(), "Id", "Name");
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.CampusList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.SynchronizerList = new SelectList(string.Empty, "Value", "Text");

                ViewBag.CommunicationTypeList = Enum.GetValues(typeof(DeviceCommunicationType)).Cast<DeviceCommunicationType>().Select(x => new SelectListItem { Text = x.GetDescription(), Value = ((int)x).ToString() }).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AttendanceDeviceViewModel hrAttendanceDeviceObj, string saveType = "1")
        {

            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");

                if (hrAttendanceDeviceObj.Organization <= 0)
                {
                    ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> organizationIdList = new List<long>();
                    organizationIdList.Add(hrAttendanceDeviceObj.Organization);
                    ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, organizationIdList), "Id", "Name");
                }

                if (hrAttendanceDeviceObj.Branch <= 0)
                {
                    ViewBag.CampusList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> branchIdList = new List<long>();
                    branchIdList.Add(hrAttendanceDeviceObj.Branch);
                    ViewBag.CampusList = new SelectList(_campusService.LoadAuthorizeCampus(userMenu, null, null, branchIdList), "Id", "Name");
                }

                if (hrAttendanceDeviceObj.Campus <= 0)
                {
                    ViewBag.SynchronizerList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> campusIdList = new List<long>();
                    campusIdList.Add(hrAttendanceDeviceObj.Campus);
                    ViewBag.SynchronizerList = new SelectList(_hrAttendanceSynchronizerService.LoadSynchronizer(null, null, campusIdList), "Id", "Name");
                }

                ViewBag.CommunicationTypeList = Enum.GetValues(typeof(DeviceCommunicationType)).Cast<DeviceCommunicationType>().Select(x => new SelectListItem { Text = x.GetDescription(), Value = ((int)x).ToString() }).ToList();
                hrAttendanceDeviceObj.IsReset = false;


                if (ModelState.IsValid)
                {

                    AttendanceDevice obj = new AttendanceDevice();
                    obj.Campus = _campusService.GetCampus(hrAttendanceDeviceObj.Campus);
                    obj.AttendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(hrAttendanceDeviceObj.Synchronizer);
                    obj.Name = hrAttendanceDeviceObj.Name;
                    obj.DeviceModelNo = hrAttendanceDeviceObj.DeviceModelNo;
                    obj.CommunicationType = hrAttendanceDeviceObj.CommunicationType;
                    obj.IpAddress = hrAttendanceDeviceObj.IPAddress;
                    obj.Port = hrAttendanceDeviceObj.Port;
                    obj.MachineNo = hrAttendanceDeviceObj.MachineNo;
                    obj.CommunicationKey = hrAttendanceDeviceObj.CommunicationKey;
                    obj.IsReset = hrAttendanceDeviceObj.IsReset;
                    obj.AttendanceDeviceType = _attendanceDeviceTypeService.GetAttendanceDeviceType(hrAttendanceDeviceObj.AttdDeviceType);
                    _hrAttendanceDeviceService.Save(obj);
                    if (saveType == "0")
                    {
                        ViewBag.SuccessMessage = "Device Add successfull";
                        ModelState.Clear();
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
                ViewBag.attdDeviceType = new SelectList(_attendanceDeviceTypeService.LoadAttendanceDeviceType(), "Id", "Name");
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        #endregion

        #region Index/Manage Page

        public ActionResult Index()
        {
            SelectList branch = null, organization = null, campus = null;
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            ViewBag.organizationlist = organization;
            ViewBag.BranchId = new SelectList(string.Empty, "Value", "Text");
            ViewBag.CampusId = new SelectList(string.Empty, "Value", "Text");
            return View();

        }
        [HttpPost]
        public ActionResult Index(int draw, int start, int length, string organizationId, string branchId, string campusId, string status, string iPAddress)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var data = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        // orderBy = nvc["order[0][column]"];
                        orderBy = "LastMemberInfoUpdateTime";
                        orderDir = nvc["order[0][dir]"];
                    }
                    #endregion

                    recordsTotal = _hrAttendanceDeviceService.DeviceRowCount(organizationId, branchId, campusId, status, iPAddress);
                    recordsFiltered = recordsTotal;
                    List<AttendanceDevice> dataList = _hrAttendanceDeviceService.LoadDevices(start, length, orderBy, orderDir.ToUpper(), organizationId, branchId, campusId, status, iPAddress);
                    foreach (var c in dataList)
                    {
                        var str = new List<string>();
                        str.Add(c.Campus.Branch.Organization.ShortName);
                        str.Add(c.Campus.Branch.Name);
                        str.Add(c.Campus.Name);
                        str.Add(c.Name);
                        str.Add(c.Status == 1 && c.LastMemberInfoUpdateTime != null ? (DateTime.Now - c.LastMemberInfoUpdateTime.Value).TotalMinutes.ToString("F") : "-");
                        str.Add(c.LastMemberInfoUpdateTime == null ? "-" : c.LastMemberInfoUpdateTime.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                        str.Add(c.LastRequestIp);
                        str.Add(c.Status == 1 ? "Active" : "Inactive");
                        str.Add(c.DisableCard == true? "Yes" : "No");
                        str.Add(c.IpAddress);
                        str.Add(c.Port.ToString());
                        str.Add(c.CommunicationKey);
                        str.Add(c.DeviceModelNo);
                        str.Add(LinkGenerator.GetEditLink("Edit", "AttendanceDevice", c.Id) + LinkGenerator.GetDeleteLinkForModal(c.Id, c.Name));
                        data.Add(str);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                    _logger.Error(ex);
                }
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        public ActionResult Monitor()
        {
            SelectList branch = null, organization = null, campus = null;
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            ViewBag.PageSize = 100;
            ViewBag.CurrentPage = 1;
            ViewBag.organizationlist = organization;
            ViewBag.BranchId = new SelectList(string.Empty, "Value", "Text");
            ViewBag.CampusId = new SelectList(string.Empty, "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult Monitor(int draw, int start, int length, string organizationId, string branchId, string campusId, string status, string iPAddress)
        {
            var data = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        // orderBy = nvc["order[0][column]"];
                        orderBy = "LastMemberInfoUpdateTime";
                        orderDir = nvc["order[0][dir]"];
                    }
                    #endregion

                    recordsTotal = _hrAttendanceDeviceService.DeviceRowCount(organizationId, branchId, campusId, status, iPAddress);
                    recordsFiltered = recordsTotal;
                    List<AttendanceDevice> dataList = _hrAttendanceDeviceService.LoadDevices(start, length, orderBy, orderDir.ToUpper(), organizationId, branchId, campusId, status, iPAddress);
                    foreach (var c in dataList)
                    {
                        var str = new List<string>();
                        str.Add(c.Campus.Branch.Organization.ShortName);
                        str.Add(c.Campus.Branch.Name);
                        str.Add(c.Campus.Name);
                        str.Add(c.Name);
                        str.Add(c.Status != 1 || c.LastMemberInfoUpdateTime == null ? "-" : String.Format("{0}:{1}", (int)(DateTime.Now - c.LastMemberInfoUpdateTime.Value).TotalHours, (DateTime.Now - c.LastMemberInfoUpdateTime.Value).Minutes));
                        str.Add(c.LastMemberInfoUpdateTime == null ? "-" : c.LastMemberInfoUpdateTime.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                        str.Add(c.LastRequestIp);
                        str.Add(c.Status == 1 ? "Active" : "Inactive");
                        str.Add(c.DeviceModelNo);

                        data.Add(str);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                    _logger.Error(ex);
                }
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        #endregion

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                AttendanceDevice deleteHRAttendanceDevice = _hrAttendanceDeviceService.GetAttendanceDevice(id);
                deleteHRAttendanceDevice.Status = AttendanceDevice.EntityStatus.Delete;
                _hrAttendanceDeviceService.Delete(deleteHRAttendanceDevice);
                return Json(new Response(true, "Attendance Device Delete Successful !"));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Update Operation

        [HttpGet]
        public ActionResult Edit(long id)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                AttendanceDevice hRAttendanceDevice = _hrAttendanceDeviceService.GetAttendanceDevice(id);
                if (hRAttendanceDevice == null)
                {
                    return HttpNotFound();
                }
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.DeviceTypesList = new SelectList(_attendanceDeviceTypeService.LoadAttendanceDeviceType(), "Id", "Name", hRAttendanceDevice.AttendanceDeviceType.Id);
                List<long> organizationIdList = new List<long>();
                organizationIdList.Add(hRAttendanceDevice.Campus.Branch.Organization.Id);
                ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, organizationIdList, null, null, false), "Id", "Name");

                List<long> branchIdList = new List<long>();
                branchIdList.Add(hRAttendanceDevice.Campus.Branch.Id);
                ViewBag.CampusList = new SelectList(_campusService.LoadAuthorizeCampus(userMenu, null, null, branchIdList, null, false), "Id", "Name");

                List<long> campusIdList = new List<long>();
                campusIdList.Add(hRAttendanceDevice.Campus.Id);
                ViewBag.SynchronizerList = new SelectList(_hrAttendanceSynchronizerService.LoadSynchronizer(null, null, campusIdList), "Id", "Name");

                ViewBag.CommunicationTypeList = Enum.GetValues(typeof(DeviceCommunicationType)).Cast<DeviceCommunicationType>().Select(x => new SelectListItem { Text = x.GetDescription(), Value = ((int)x).ToString() }).ToList();

                AttendanceDeviceViewModel obj = new AttendanceDeviceViewModel();

                obj.Organization = hRAttendanceDevice.Campus.Branch.Organization.Id;
                obj.AttdDeviceType = hRAttendanceDevice.AttendanceDeviceType.Id;
                obj.Branch = hRAttendanceDevice.Campus.Branch.Id;
                obj.Campus = hRAttendanceDevice.Campus.Id;
                obj.Synchronizer = hRAttendanceDevice.AttendanceSynchronizer.Id;
                obj.Name = hRAttendanceDevice.Name;
                obj.DeviceModelNo = hRAttendanceDevice.DeviceModelNo;
                obj.DisableCard = hRAttendanceDevice.DisableCard;
                obj.CommunicationType = hRAttendanceDevice.CommunicationType;
                obj.IPAddress = hRAttendanceDevice.IpAddress;
                obj.Port = hRAttendanceDevice.Port;
                obj.MachineNo = hRAttendanceDevice.MachineNo;
                obj.CommunicationKey = hRAttendanceDevice.CommunicationKey;
                obj.Status = hRAttendanceDevice.Status;
                obj.IsReset = hRAttendanceDevice.IsReset;
                return View(obj);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AttendanceDeviceViewModel hrAttendanceDeviceObj, long id)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            AttendanceDevice obj = new AttendanceDevice();
            try
            {
                obj = _hrAttendanceDeviceService.GetAttendanceDevice(id);
                if (obj == null)
                {
                    return HttpNotFound();
                }

                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.attdDeviceType = new SelectList(_attendanceDeviceTypeService.LoadAttendanceDeviceType(), "Id", "Name");
                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                if (hrAttendanceDeviceObj.Organization <= 0)
                {
                    ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> organizationIdList = new List<long>();
                    organizationIdList.Add(hrAttendanceDeviceObj.Organization);
                    ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, organizationIdList, null, null, false), "Id", "Name");
                }

                if (hrAttendanceDeviceObj.Branch <= 0)
                {
                    ViewBag.CampusList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> branchIdList = new List<long>();
                    branchIdList.Add(hrAttendanceDeviceObj.Branch);
                    ViewBag.CampusList = new SelectList(_campusService.LoadAuthorizeCampus(userMenu, null, null, branchIdList, null, false), "Id", "Name");
                }

                if (hrAttendanceDeviceObj.Campus <= 0)
                {
                    ViewBag.SynchronizerList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> campusIdList = new List<long>();
                    campusIdList.Add(hrAttendanceDeviceObj.Campus);
                    ViewBag.SynchronizerList = new SelectList(_hrAttendanceSynchronizerService.LoadSynchronizer(null, null, campusIdList), "Id", "Name");
                }

                ViewBag.CommunicationTypeList = Enum.GetValues(typeof(DeviceCommunicationType)).Cast<DeviceCommunicationType>().Select(x => new SelectListItem { Text = x.GetDescription(), Value = ((int)x).ToString() }).ToList();

                if (ModelState.IsValid)
                {
                    obj.Campus = _campusService.GetCampus(hrAttendanceDeviceObj.Campus);
                    obj.AttendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(hrAttendanceDeviceObj.Synchronizer);
                    obj.AttendanceDeviceType = _attendanceDeviceTypeService.GetAttendanceDeviceType(hrAttendanceDeviceObj.AttdDeviceType);
                    obj.Name = hrAttendanceDeviceObj.Name;
                    obj.DeviceModelNo = hrAttendanceDeviceObj.DeviceModelNo;
                    obj.DisableCard = hrAttendanceDeviceObj.DisableCard;
                    obj.CommunicationType = hrAttendanceDeviceObj.CommunicationType;
                    obj.IpAddress = hrAttendanceDeviceObj.IPAddress;
                    obj.Port = hrAttendanceDeviceObj.Port;
                    obj.CommunicationKey = hrAttendanceDeviceObj.CommunicationKey;
                    obj.Status = hrAttendanceDeviceObj.Status;
                    obj.MachineNo = hrAttendanceDeviceObj.MachineNo;
                    obj.IsReset = hrAttendanceDeviceObj.IsReset;
                    _hrAttendanceDeviceService.Update(obj);
                    ViewBag.SuccessMessage = "Device Update successfull";


                }
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View(hrAttendanceDeviceObj);
        }

        #endregion

        #region Helper Function

        public JsonResult LoadSynchronizer(List<long> organizationIds = null, List<long> branchIds = null, List<long> campusIds = null, bool isAuthorized = true)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<AttendanceSynchronizer> synchronizerList = _hrAttendanceSynchronizerService.LoadSynchronizer(organizationIds, branchIds, campusIds);
                var campusSelectList = new SelectList(synchronizerList.ToList(), "Id", "Name");
                return Json(new { returnCampusList = campusSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        public ActionResult InitialEntry()
        {
            try
            {
                _hrAttendanceDeviceService.InitialEntry();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return Json(new Response(true, ""));
        }
    }
}