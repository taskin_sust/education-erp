using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Common;

namespace UdvashERP.Dao.Mapping.Common {

        public partial class ThanaMap : BaseClassMap<Thana,long>
        {

            public ThanaMap()
            {
                Table("Thana");
                LazyLoad();
                
                Map(x => x.Name).Column("Name");
                References(x => x.District).Column("DistrictId");
                HasMany(x => x.PermanentTeamMembers).KeyColumn("PermanentThanaId");
                HasMany(x => x.PresentTeamMembers).KeyColumn("PresentThanaId");
            }
        }
    }
