using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{
    
    
    public partial class EmployeeBenefitsFundSettingEntitlementMap : BaseClassMap<EmployeeBenefitsFundSettingEntitlement,long> {

        public EmployeeBenefitsFundSettingEntitlementMap()
        {
			Table("HR_EmployeeBenefitsFundSettingEntitlement");
			LazyLoad();
			References(x => x.EmployeeBenefitsFundSetting).Column("EmployeeBenefitsFundSettingId");
			Map(x => x.ServicePeriod).Column("ServicePeriod").Not.Nullable();
			Map(x => x.EmployerContribution).Column("EmployerContribution").Not.Nullable();
        }
    }
}
