﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Exam
{
    public class ExamMcqQuestionSetMap : BaseClassMap<ExamMcqQuestionSet, long>
    {
        public ExamMcqQuestionSetMap()
        {
            Table("ExamMcqQuestionSet");
            LazyLoad();

            Map(x => x.UniqueSet).Column("UniqueSet");
            Map(x => x.ExamMcqQuestionSerial).Column("ExamMcqQuestionSerial");
            Map(x => x.SetId).Column("SetId");
            Map(x => x.SetNo).Column("SetNo");
            Map(x => x.QuestionSerial).Column("QuestionSerial");
            Map(x => x.OptionA).Column("OptionA");
            Map(x => x.OptionB).Column("OptionB");
            Map(x => x.OptionC).Column("OptionC");
            Map(x => x.OptionD).Column("OptionD");
            Map(x => x.OptionE).Column("OptionE");
            Map(x => x.CorrectAnswer).Column("CorrectAnswer");
            Map(x => x.GeneratedBy).Column("GeneratedBy");
            Map(x => x.GeneratedDate).Column("GeneratedDate");
            References(x => x.Exams).Column("ExamId");
        }
    }
}
