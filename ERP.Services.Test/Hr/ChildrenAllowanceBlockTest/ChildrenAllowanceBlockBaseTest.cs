﻿using System;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.ChildrenAllowanceBlockTest
{
    public class ChildrenAllowanceBlockBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initialization

        protected readonly CommonHelper _commonHelper;
        protected readonly ChildrenAllowanceSettingFactory _childrenAllowanceSettingFactory;
        protected readonly AllowanceBlockFactory _allowanceBlockFactory;

        protected readonly IChildrenAllowanceSettingService _childrenAllowanceSettingService;
        protected readonly IChildrenAllowanceBlockService _allowanceBlockService;
        #endregion

        #region Constructor

        public ChildrenAllowanceBlockBaseTest()
        {
            _commonHelper = new CommonHelper();
            _childrenAllowanceSettingFactory = new ChildrenAllowanceSettingFactory(null, Session);
            _allowanceBlockFactory = new AllowanceBlockFactory(null, Session);

            _childrenAllowanceSettingService = new ChildrenAllowanceSettingService(Session);
            _allowanceBlockService = new ChildrenAllowanceBlockService(Session);
        }

        #endregion

        #region disposal

        public void Dispose()
        {
            _allowanceBlockFactory.DeleteAll();
            _childrenAllowanceSettingFactory.DeleteAll();
            base.Dispose();
        }

        #endregion
    }
}
