﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{

    public interface IJobExperienceService : IBaseService
    {
        #region Operational Function
        bool DeleteJobExperienceInfo(long id);
        List<long> UpdateJobExperienceInfo(TeamMember member, List<JobExperience> jobInfoArray);
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<JobExperience> LoadMemberJobExperienceInfo(long memberId);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class JobExperienceService : BaseService, IJobExperienceService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IJobExperienceDao _hrJobDao;
        public JobExperienceService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrJobDao = new JobExperienceDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool DeleteJobExperienceInfo(long id)
        {
            ITransaction trans = null;
            try
            {
                JobExperience entity = _hrJobDao.LoadById(id);
                if (entity == null)
                    throw new InvalidDataException("Invalid job experience");
                using (trans = Session.BeginTransaction())
                {
                    entity.Status = ChildrenInfo.EntityStatus.Delete;
                    _hrJobDao.SaveOrUpdate(entity);
                    trans.Commit();
                    return true;
                }
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public List<long> UpdateJobExperienceInfo(TeamMember member, List<JobExperience> jobInfoArray)
        {
            var idList = new List<long>();
            ITransaction trans = null;
            if (member == null)
            {
                throw new NullObjectException("MemberEntityNotFound");
            }
            if (jobInfoArray == null)
            {
                throw new NullObjectException("NullObject");
            }

            try
            {
                using (trans = Session.BeginTransaction())
                {
                    if (jobInfoArray.Any())
                    {
                        int rank = 1;
                        foreach (var row in jobInfoArray)
                        {
                            JobExperience jEInfoEntity = null;
                            if (row.Id > 0)
                            {
                                var oldEntity = _hrJobDao.LoadById(row.Id);
                                jEInfoEntity = oldEntity ?? new JobExperience();
                            }
                            else
                            {
                                jEInfoEntity = new JobExperience();
                            }
                            DoBeforeUpdateJobExperienceInfo(member, row);
                            jEInfoEntity.CompanyName = row.CompanyName;
                            jEInfoEntity.CompanyType = Convert.ToInt32(row.CompanyType);
                            jEInfoEntity.CompanyLocation = row.CompanyLocation;
                            jEInfoEntity.DepartmentName = row.DepartmentName;
                            jEInfoEntity.ExperienceArea = row.ExperienceArea;
                            jEInfoEntity.Position = row.Position;
                            jEInfoEntity.Duration = row.Duration;
                            jEInfoEntity.Rank = rank;

                            jEInfoEntity.CreationDate = DateTime.Now;
                            jEInfoEntity.ModificationDate = DateTime.Now;
                            jEInfoEntity.Status = AcademicInfo.EntityStatus.Active;
                            jEInfoEntity.TeamMember = member;
                            if (!string.IsNullOrEmpty(row.BusinessId))
                            {
                                jEInfoEntity.BusinessId = row.BusinessId;
                            }
                            _hrJobDao.SaveOrUpdate(jEInfoEntity);
                            idList.Add(jEInfoEntity.Id);

                            row.Id = jEInfoEntity.Id;
                            rank++;
                        }
                    }
                    trans.Commit();
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                {
                    trans.Rollback();
                }
            }
            return idList;
        }

        

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<JobExperience> LoadMemberJobExperienceInfo(long memberId)
        {
            try
            {
                IList<JobExperience> list = _hrJobDao.GetTeamMemberJobExperiences(memberId);
                return list != null && list.Count > 0 ? list : null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function


        #endregion

        #region Helper function
        private void DoBeforeUpdateJobExperienceInfo(TeamMember member, JobExperience row)
        {
            if (string.IsNullOrEmpty(row.CompanyName.Trim()))
            {
                throw new NullObjectException("Company Name Not Found!");
            }
            if (Convert.ToInt32(row.CompanyType) <= 0)
            {
                throw new NullObjectException("Company Type Not Found!");
            }
            if (string.IsNullOrEmpty(row.CompanyLocation.Trim()))
            {
                throw new NullObjectException("Company Location Not Found!");
            }
            if (string.IsNullOrEmpty(row.DepartmentName.Trim()))
            {
                throw new NullObjectException("Department Name Not Found!");
            }
            if (string.IsNullOrEmpty(row.ExperienceArea.Trim()))
            {
                throw new NullObjectException("Experience Area Not Found!");
            }
            if (string.IsNullOrEmpty(row.Position.Trim()))
            {
                throw new NullObjectException("Job Position Not Found!");
            }
            if (string.IsNullOrEmpty(row.Duration.Trim()))
            {
                throw new NullObjectException("Job Duration Not Found!");
            }
        }


        #endregion

    }
}
