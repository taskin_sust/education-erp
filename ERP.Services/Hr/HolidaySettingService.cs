﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using NHibernate.Linq;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{
    public interface IHolidaySettingService : IBaseService
    {
        #region Operational Function

        bool Save(HolidaySetting holidaySetting);
        bool Edit(long id, HolidaySetting hrHolidaySetting);
        bool Delete(long id);

        #endregion

        #region Single Instances Loading Function

        HolidaySetting GetHolidaySetting(long id);

        #endregion

        #region List Loading Function

        IList<HolidaySetting> LoadHoliday(int start, int length, string orderBy, string p, List<long> organizationIdList, int? month, int? year, bool isExport = false);

        #endregion

        #region Others Function

        int GetHolidayCount(List<UserMenu> userMenus, List<long> organizationIdList, int? month, int? year);

        #endregion

        #region Helper Function
        #endregion

    }
    public class HolidaySettingService : BaseService, IHolidaySettingService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly IHolidaySettingDao _holidaySettingDao;
        private readonly IYearlyHolidayDao _yearlyHolidayDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly CommonHelper _commonHelper;
        public HolidaySettingService(ISession session)
        {
            Session = session;
            _holidaySettingDao = new HolidaySettingDao()
            {
                Session = session
            };
            _yearlyHolidayDao = new YearlyHolidayDao()
            {
                Session = session
            };
            _organizationDao = new OrganizationDao()
            {
                Session = session
            };
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Functions

        public bool Save(HolidaySetting holidaySetting)
        {
            var holidayRepeatationTypes = Enum.GetValues(typeof(HolidayRepeatationType)).Cast<int>().ToArray();
            var holidayTypes = Enum.GetValues(typeof(HolidayTypeEnum)).Cast<int>().ToArray();
            DateTime tempDate;
            if (holidaySetting == null)
            {
                throw new NullObjectException("Holiday object can not be null");
            }
            if (holidaySetting.OrganizationId < 1 && holidaySetting.Organization == null)
            {
                throw new ValueNotAssignedException("Organization must be assigned");
            }
            if (string.IsNullOrEmpty(holidaySetting.Name))
            {
                throw new ValueNotAssignedException("Holiday name can not be empty.");
            }
            //if (holidaySetting.DateFrom.Date == null || holidaySetting.DateTo.Date == null)
            //{
            //    throw new ValueNotAssignedException("Date from and date to must be assigned.");
            //}

            if (holidaySetting.RepeatationType == null || !holidayRepeatationTypes.Contains(Convert.ToInt32(holidaySetting.RepeatationType)))
            {
                throw new ValueNotAssignedException("Repeatation type must be assigned.");
            }
            if (holidaySetting.HolidayType == null || !holidayTypes.Contains(Convert.ToInt32(holidaySetting.HolidayType)))
            {
                throw new ValueNotAssignedException("Holiday type must be assigned.");
            }

            if (holidaySetting.DateFrom.Date > holidaySetting.DateTo.Date)
            {
                throw new InvalidDataException("Date from can not exceed date to.");
            }

            if (holidaySetting.DateFrom.Date < DateTime.Now.Date)
            {
                throw new InvalidDataException("Holiday can not be created in previous date.");
            }
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    if (holidaySetting.Organization == null || holidaySetting.OrganizationId > 0)
                    {
                        holidaySetting.Organization = _organizationDao.LoadById(holidaySetting.OrganizationId);
                    }
                    var checkExistingHoliday = _holidaySettingDao.CheckExistingHoliday(holidaySetting.Organization.Id, holidaySetting.Name.Trim(), holidaySetting.DateFrom);
                    if (!checkExistingHoliday)
                    {
                        throw new DuplicateEntryException("Holiday name can not be duplicate for same organization on same date.");
                    }
                    holidaySetting.Name = holidaySetting.Name.Trim();
                    holidaySetting.Description = holidaySetting.Description != null ? holidaySetting.Description.Trim() : "";
                    _holidaySettingDao.Save(holidaySetting);
                    if (holidaySetting.RepeatationType == (int)HolidayRepeatationType.Yearly)
                    {
                        var yearlyHoliday = new YearlyHoliday()
                        {
                            Name = holidaySetting.Name,
                            DateFrom = holidaySetting.DateFrom,
                            DateTo = holidaySetting.DateTo,
                            RepeatationType = (int)HolidayRepeatationType.Yearly,
                            HolidayType = holidaySetting.HolidayType,
                            Description = holidaySetting.Description,
                            Organization = holidaySetting.Organization
                        };
                        _yearlyHolidayDao.Save(yearlyHoliday);
                    }
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (ValueNotAssignedException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        public bool Edit(long id, HolidaySetting holidaySetting)
        {
            var holidayRepeatationTypes = Enum.GetValues(typeof(HolidayRepeatationType)).Cast<int>().ToArray();
            var holidayTypes = Enum.GetValues(typeof(HolidayTypeEnum)).Cast<int>().ToArray();
            DateTime tempDate;
            if (id == 0 || holidaySetting == null)
            {
                throw new NullObjectException("Holiday object can not be null");
            }
            if (holidaySetting.OrganizationId < 1 && holidaySetting.Organization == null)
            {
                throw new ValueNotAssignedException("Organization must be assigned");
            }
            if (string.IsNullOrEmpty(holidaySetting.Name))
            {
                throw new ValueNotAssignedException("Holiday name can not be empty.");
            }
            //if (holidaySetting.DateFrom.Date == null || holidaySetting.DateTo.Date == null)
            //{
            //    throw new ValueNotAssignedException("Date from and date to must be assigned.");
            //}

            if (holidaySetting.RepeatationType == null || !holidayRepeatationTypes.Contains(Convert.ToInt32(holidaySetting.RepeatationType)))
            {
                throw new ValueNotAssignedException("Repeatation type must be assigned.");
            }
            if (holidaySetting.HolidayType == null || !holidayTypes.Contains(Convert.ToInt32(holidaySetting.HolidayType)))
            {
                throw new ValueNotAssignedException("Holiday type must be assigned.");
            }
            if (holidaySetting.DateFrom.Date > holidaySetting.DateTo.Date)
            {
                throw new InvalidDataException("Date from can not exceed date to.");
            }
            if (holidaySetting.DateFrom.Date < DateTime.Now.Date)
            {
                throw new InvalidDataException("Holiday can not be edited in previous date.");
            }
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    if (holidaySetting.Organization == null || holidaySetting.OrganizationId > 0)
                    {
                        holidaySetting.Organization = _organizationDao.LoadById(holidaySetting.OrganizationId);
                    }
                    var previousHolidaySetting = _holidaySettingDao.LoadById(id);
                    if (previousHolidaySetting.DateFrom.Date < DateTime.Now.Date)
                    {
                        throw new InvalidDataException("Holiday of previous date can not be edited.");
                    }
                    var checkExistingHoliday = _holidaySettingDao.CheckExistingHoliday(holidaySetting.Organization.Id, holidaySetting.Name.Trim(), holidaySetting.DateFrom, holidaySetting.Id);
                    if (!checkExistingHoliday)
                    {
                        throw new DuplicateEntryException("Holiday name can not be duplicate for same organization on same date.");
                    }
                    previousHolidaySetting.Organization = _organizationDao.LoadById(holidaySetting.Organization.Id);
                    previousHolidaySetting.Name = holidaySetting.Name.Trim();
                    previousHolidaySetting.DateFrom = holidaySetting.DateFrom;
                    previousHolidaySetting.DateTo = holidaySetting.DateTo;
                    previousHolidaySetting.RepeatationType = holidaySetting.RepeatationType;
                    previousHolidaySetting.HolidayType = holidaySetting.HolidayType;
                    previousHolidaySetting.Description = holidaySetting.Description != null ? holidaySetting.Description.Trim() : "";
                    _holidaySettingDao.Update(previousHolidaySetting);
                    if (holidaySetting.RepeatationType == (int)HolidayRepeatationType.Yearly)
                    {
                        var yearlyHoliday = _yearlyHolidayDao.GetYearlyHoliday(holidaySetting.Name, holidaySetting.Organization.Id);
                        if (yearlyHoliday != null)
                        {
                            yearlyHoliday.Name = holidaySetting.Name.Trim();
                            yearlyHoliday.DateFrom = holidaySetting.DateFrom;
                            yearlyHoliday.DateTo = holidaySetting.DateTo;
                            yearlyHoliday.RepeatationType = (int)HolidayRepeatationType.Yearly;
                            yearlyHoliday.HolidayType = holidaySetting.HolidayType;
                            yearlyHoliday.Description = holidaySetting.Description != null
                                ? holidaySetting.Description.Trim()
                                : "";
                            yearlyHoliday.Organization = _organizationDao.LoadById(holidaySetting.Organization.Id);
                            _yearlyHolidayDao.Update(yearlyHoliday);
                        }
                        else
                        {
                            yearlyHoliday = new YearlyHoliday();
                            yearlyHoliday.Name = holidaySetting.Name.Trim();
                            yearlyHoliday.DateFrom = holidaySetting.DateFrom;
                            yearlyHoliday.DateTo = holidaySetting.DateTo;
                            yearlyHoliday.RepeatationType = (int)HolidayRepeatationType.Yearly;
                            yearlyHoliday.HolidayType = holidaySetting.HolidayType;
                            yearlyHoliday.Description = holidaySetting.Description != null
                                ? holidaySetting.Description.Trim()
                                : "";
                            yearlyHoliday.Organization = _organizationDao.LoadById(holidaySetting.Organization.Id);
                            _yearlyHolidayDao.Save(yearlyHoliday);
                        }
                    }
                    if (holidaySetting.RepeatationType == (int)HolidayRepeatationType.Once)
                    {
                        var yearlyHoliday = _yearlyHolidayDao.GetYearlyHoliday(holidaySetting.Name, holidaySetting.Organization.Id);
                        if (yearlyHoliday != null)
                        {
                            yearlyHoliday.Status = 0;
                            _yearlyHolidayDao.Update(yearlyHoliday);
                        }
                    }
                    transaction.Commit();
                    return true;
                }

            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (ValueNotAssignedException ex)
            {
                throw;
            }
            catch (ServiceException ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        public bool Delete(long id)
        {
            if (id < 1)
            {
                throw new NullObjectException("Holiday can not be null.");
            }
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    var dbHolidaySetting = _holidaySettingDao.LoadById(id);

                    if (dbHolidaySetting.DateFrom.Date <= DateTime.Now.Date)
                    {
                        throw new BelowLimitException("Holiday of current or previous date can not be deleted.");
                    }
                    dbHolidaySetting.Status = HolidaySetting.EntityStatus.Delete;
                    var yearlyHoliday = _yearlyHolidayDao.GetYearlyHoliday(dbHolidaySetting.Name,
                        dbHolidaySetting.Organization.Id);
                    if (yearlyHoliday != null) yearlyHoliday.Status = YearlyHoliday.EntityStatus.Delete;
                    _holidaySettingDao.Update(dbHolidaySetting);
                    trans.Commit();
                    return true;
                }
            }
            catch (BelowLimitException ex)
            {
                throw;
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public HolidaySetting GetHolidaySetting(long id)
        {
            try
            {
                if (id == 0)
                {
                    throw new InvalidDataException("Invalid id");
                }
                var holidaySetting = _holidaySettingDao.LoadById(id);
                return holidaySetting;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function

        public IList<HolidaySetting> LoadHoliday(int start, int length, string orderBy, string dir, List<long> organizationIdList, int? month, int? year, bool isExport = false)
        {
            try
            {
                var holidayList = _holidaySettingDao.LoadHoliday(start, length, orderBy, dir, organizationIdList, month, year, isExport);
                return holidayList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Others Function


        public int GetHolidayCount(List<UserMenu> userMenus, List<long> organizationIdList, int? month, int? year)
        {
            try
            {
                if (userMenus == null || !userMenus.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenus, organizationIdList);

                if (authOrganizationIdList == null || !authOrganizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                var holidayCount = _holidaySettingDao.GetHolidayCount(authOrganizationIdList, month, year);
                return holidayCount;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function
        #endregion
    }
}