﻿
/*Written By Taskin */

$(document).ready(function () {
    var AJAX_ERROR = "Slow Internet Connection";

    $(document).on('click', '#paymentPaid', function () {

        var receivableAmount = $('#receivableAmount').val();
        var netReceivable = $('#netReceivable').val();
        var receivedAmount = $('#receivedAmount').val();
        var spDiscountAmount = $('#spDiscountAmount').val();
        var dueAmount = $('#dueAmount').val();
        console.log(dueAmount);
        var paymentMethod = $('#PAYMENTMETHODLIST').val();
        var referrer = $('#REFERRERLIST').val();
        var referrerenceNote = $('#referrerenceNote').val();
        var nextRecDate = $('#nextRecDate').val();
        var stdPId = $('#stdPId').val();
        if (stdPId.length <= 0) {
            stdPId = $('#stdProRoll').val();
        }
        //console.log(referrerenceNote);
        receivableAmount = Number(receivableAmount);
        netReceivable = Number(netReceivable);
        dueAmount = Number(dueAmount);
        console.log(dueAmount);
        receivedAmount = Number(receivedAmount);
        spDiscountAmount = Number(spDiscountAmount);
        //console.log()
        var nRec = parseFloat(netReceivable);
        var sDis = parseFloat("0"+spDiscountAmount);
        var recvdAmount = parseFloat("0"+receivedAmount);
        var recavailAmount = parseFloat(receivableAmount);
        var dAmount = nRec - recvdAmount;/*dAmount : Due Amount*/

        console.log(dueAmount);

        if (!validateAddCoursePaymentForm()) {
            return;
        }

        if (dAmount > 0 && nextRecDate.length <= 0) {
            $('#nextRecDate').addClass('highlight');
            return;
        }

        if (dAmount >= 0) {

            if (recavailAmount > 0 && nRec >= 0) {
                if (sDis > 0) {
                    if (referrer.length > 0) {
                        if (referrerenceNote.length > 0) {
                            $.ajax({
                                url: "/Student/Payment/PaymentPaidAndMoneyReceiptGeneration",
                                type: "POST",
                                data: { stdPId: stdPId, recavailAmount: recavailAmount, nRec: nRec, sDis: sDis, recvdAmount: recvdAmount, paymentMethod: paymentMethod, referrerenceNote: referrerenceNote, nextRecDate: nextRecDate, referrer: referrer },
                                beforeSend: function () {
                                    $.blockUI({
                                        timeout: 0,
                                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                    });
                                },
                                success: function (data) {

                                    $('#mainBody').html(data);
                                    $('.totalPage').html(data);

                                    $.unblockUI();
                                    if (data.IsSuccess) {

                                    } else {
                                        $.fn.customMessage({
                                            displayMessage: data.Message,
                                            displayMessageType: "error",
                                            
                                        });
                                        //$('.customMessage').append(data.Message);
                                    }
                                },
                                complete: function () {
                                    $.unblockUI();
                                },
                                error: function (data) {
                                    $.fn.customMessage({
                                        displayMessage: AJAX_ERROR,
                                        displayMessageType: "error",
                                        displayTime: 5000
                                    });
                                    //bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                                }
                            });

                        } else {
                            $('#referrerenceNote').addClass('highlight');
                        }
                    } else {
                        $('#REFERRERLIST').addClass('highlight');
                    }

                } else {
                    sDis = 0;
                    if (nRec < 0) {
                        $('#netReceivable').addClass('highlight');
                        $('#netReceivable').after("<p style ='color:red'>Net Receivable Amount 0 Not accepted</p>");
                        return;
                    }
                    if (recvdAmount > 0) {
                        $.ajax({
                            url: "/Student/Payment/PaymentPaidAndMoneyReceiptGeneration",
                            type: "POST",
                            data: { stdPId: stdPId, recavailAmount: recavailAmount, nRec: nRec, sDis: sDis, recvdAmount: recvdAmount, paymentMethod: paymentMethod, referrerenceNote: referrerenceNote, nextRecDate: nextRecDate, referrer: referrer, },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data) {
                                //alert(data);
                                $('#mainBody').html(data);
                                $('.totalPage').html(data);
                                $.unblockUI();
                                if (data.IsSuccess) {
                                } else {
                                    $('.customMessage').append(data.Message);
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                            },

                            error: function (data) {
                                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                            }
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: "Empty Transication Not Supported",
                            displayMessageType: "w",
                            displayTime: 5000
                        });
                    }
                    
                }
            }
        } else {
            $('#dueAmount').addClass('highlight');
            $('#dueAmount').after("<p style ='color:red'>Due amount Negative Not acceptable</p>");
        }
    });

    //function GetAmount(receivable, spDiscount) {
    //    var val = parseFloat("0" + receivable) - parseFloat("0" + spDiscount);
    //    return val;
    //}

    function ShowErrorMessage(ctrlId, message, show) {

        var divHtml = '<div style="color:red;" id="' + ctrlId + '_err_div" >' + message + '</div>';

        if (show == true) {
            $('#' + ctrlId).addClass("highlight");
            $('#' + ctrlId).after(divHtml);
        } else {
            $('#' + ctrlId).removeClass("highlight");
            $('#' + ctrlId + '_err_div').remove();
        }
    }

    /*#receivedAmount,#spDiscountAmount */

    //$(document).on("input propertychange", "#receivedAmount", function (event) {

    //    var receivableAmount = $('#receivableAmount').val();
    //    var netReceivable = $('#netReceivable').val();
    //    var receivedAmount = $('#receivedAmount').val();

    //    if (receivableAmount.length > 0 && netReceivable.length > 0) {

    //        if (receivedAmount.length > 0) {
    //            var dAmount = GetAmount(netReceivable, receivedAmount);
    //            $('#dueAmount').val(dAmount);
    //        } else {
    //            $('#dueAmount').val(netReceivable);
    //        }

    //        console.log("netReceivable: " + netReceivable);
    //    }
    //});

    //$(document).on("input propertychange", "#spDiscountAmount", function (event) {

    //    var receivableAmount = $('#receivableAmount').val();
    //    var netReceivable = $('#netReceivable').val();
    //    var receivedAmount = $('#receivedAmount').val();
    //    var spDiscountAmount = $('#spDiscountAmount').val();
    //    var dAmount = 0;

    //    if (receivableAmount.length > 0 && netReceivable.length > 0) {

    //        var sDis = parseFloat("0" + spDiscountAmount);
    //        var receiveableAmountF = parseFloat("0" + receivableAmount);

    //        if (sDis > 0) {

    //            $('#REFERRERLIST').removeAttr('disabled');
    //            ShowErrorMessage('spDiscountAmount', '', false);

    //            if (sDis >= 0 && sDis <= receiveableAmountF) {
    //                netReceivable = GetAmount(receivableAmount, spDiscountAmount);
    //                $('#netReceivable').val(netReceivable);
    //                dAmount = GetAmount(netReceivable, receivedAmount);
    //                $('#dueAmount').val(dAmount);
    //            } else {
    //                ShowErrorMessage('spDiscountAmount', 'Special discount can not be negative or higher than receiveable amount.', true);
    //            }

    //        } else {

    //            $('#netReceivable').val(receivableAmount);
    //            receivedAmount = $('#receivedAmount').val();
    //            netReceivable = $('#netReceivable').val();
    //            var due = GetAmount(netReceivable, receivedAmount);
    //            $('#dueAmount').val(due);
    //            $('#REFERRERLIST').prop('disabled', true);
    //        }
    //    }
    //});

    function CourseListGenerate() {

        var student = new StudentViewModel();

        $('.course-name-check').each(function (index, value) {

            if ($(this).is(':checked')) {

                var courseDetail = new CourseViewModel();
                courseDetail.Name = $(this).attr("data-course-name");
                courseDetail.ProgramId = $(this).attr('data-program-id');
                courseDetail.SessionId = $(this).attr('data-session-id');
                courseDetail.Id = $(this).attr('data-course-id');

                $('.course-' + courseDetail.Id + '-subjects').each(function (index, value) {
                    console.log("Entered");
                    if ($(this).is(':checked') && $(this).attr('disabled') != 'disabled') {

                        var subject = new SubjectViewModel();
                        subject.Id = $(this).attr('data-course-subject-id');
                        subject.Name = $(this).attr('data-course-subject-name');
                        subject.Payment = $(this).attr('data-course-subject-payment');

                        courseDetail.SubjectViewModels.push(subject);
                    }
                });

                student.CourseViewModels.push(courseDetail);
            }
        });
        var studentObj = JSON.stringify(student);
        console.log(studentObj);
        return studentObj;
    }

    //function courseListGenerateForAddCourse() {

    //    var student = new StudentViewModel();

    //    $('.course-name-check').each(function (index, value) {

    //        if ($(this).is(':checked')) {

    //            var courseDetail = new CourseViewModel();
    //            courseDetail.Name = $(this).attr("data-course-name");
    //            courseDetail.ProgramId = $(this).attr('data-program-id');
    //            courseDetail.SessionId = $(this).attr('data-session-id');
    //            courseDetail.Id = $(this).attr('data-course-id');

    //            $('.course-' + courseDetail.Id + '-subjects').each(function (index, value) {
    //                console.log("Entered");
    //                if ($(this).is(':checked')) {

    //                    var subject = new SubjectViewModel();
    //                    subject.Id = $(this).attr('data-course-subject-id');
    //                    subject.Name = $(this).attr('data-course-subject-name');
    //                    subject.Payment = $(this).attr('data-course-subject-payment');

    //                    courseDetail.SubjectViewModels.push(subject);
    //                }
    //            });

    //            student.CourseViewModels.push(courseDetail);
    //        }
    //    });
    //    var studentObj = JSON.stringify(student);
    //    console.log(studentObj);
    //    return studentObj;
    //}

    function validateAddCoursePaymentForm() {

        var receivedAmount = $('#receivedAmount').val();
        if (parseInt("0"+receivedAmount) < 0) {
            $('#receivedAmount').addClass('highlight');
            $("#receivedAmountContainer").remove();
            $('#receivedAmount').after("<div style='color:red' id='receivedAmountContainer'>Please enter valid amount</div>");
            return false;
        } else {
            $('#receivedAmount').removeClass('highlight');
            $('#receivedAmountContainer').remove();
        }

        var nextRecDate = $('#nextRecDate').val();
        var dueAmount = $('#dueAmount').val();

        if (nextRecDate.length <= 0 && parseInt("0" + dueAmount) > 0) {
            $('#nextRecDate').addClass('highlight');
            $("#nextPaymentDateErrorMessageContainer").remove();
            $('#nextRecDate').after("<div id='nextPaymentDateErrorMessageContainer'>Please select next payment date</div>");
            return false;
        } else {
            $('#nextRecDate').removeClass('highlight');
            $("#nextPaymentDateErrorMessageContainer").remove();
        }

        return true;
    }

    ///*Payment Paid for Course Add*/
    //$(document).on('click', '#paymentPaidCourseAddBtn', function () {

    //    var receivableAmount = $('#receivableAmount').val();
    //    var netReceivable = $('#netReceivable').val();
    //    var receivedAmount = $('#receivedAmount').val();
    //    var spDiscountAmount = $('#spDiscountAmount').val();
    //    var dueAmount = $('#dueAmount').val();
    //    console.log(dueAmount);
    //    var paymentMethod = $('#PAYMENTMETHODLIST').val();
    //    var referrer = $('#REFERRERLIST').val();
    //    var referrerenceNote = $('#referrerenceNote').val();
    //    var nextRecDate = $('#nextRecDate').val();
    //    var stdPId = $('#stdProRoll').val();

    //    receivableAmount = Number(receivableAmount);
    //    netReceivable = Number(netReceivable);
    //    dueAmount = Number(dueAmount);
    //    console.log(dueAmount);

    //    receivedAmount = Number(receivedAmount);
    //    spDiscountAmount = Number(spDiscountAmount);


    //    var nRec = parseFloat(netReceivable);
    //    var sDis = parseFloat(spDiscountAmount);
    //    var recvdAmount = parseFloat(receivedAmount);
    //    var recavailAmount = parseFloat(receivableAmount);
    //    var dAmount = nRec - recvdAmount;/*dAmount : Due Amount*/

    //    console.log(dueAmount);

    //    if (!validateAddCoursePaymentForm()) {
    //        return;
    //    }

    //    if (dAmount >= 0) {
    //        if (recavailAmount > 0 && nRec >= 0) {
    //            if (sDis > 0) {
    //                if (referrer.length > 0) {

    //                    var studentObj = courseListGenerateForAddCourse();

    //                    $.ajax({
    //                        url: "/Student/Payment/NewCourseAddPaymentAndMoneyReceiptGeneration",
    //                        type: "POST",
    //                        data: { studentObj: studentObj, stdPId: stdPId, recavailAmount: recavailAmount, nRec: nRec, sDis: sDis, recvdAmount: recvdAmount, paymentMethod: paymentMethod, referrerenceNote: referrerenceNote, nextRecDate: nextRecDate, referrer: referrer },
    //                        beforeSend: function () {
    //                            $.blockUI({
    //                                timeout: 0,
    //                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //                            });
    //                        },
    //                        success: function (data) {
    //                            $('#mainBody').html(data);
    //                            $('.totalPage').html(data);
    //                            $.unblockUI();
    //                            if (data.IsSuccess) {

    //                            } else {
    //                                $('.customMessage').append(data.Message);
    //                            }
    //                        },

    //                        complete: function () {
    //                            $.unblockUI();
    //                        },
    //                        error: function (data) {
    //                            bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
    //                        }
    //                    });

    //                } else {
    //                    $('#REFERRERLIST').addClass('highlight');
    //                }

    //            } else {
    //                sDis = 0;
    //                if (nRec < 0) {
    //                    $('#netReceivable').addClass('highlight');
    //                    $('#netReceivable').after("<p style ='color:red'>Net Receivable Amount 0 Not accepted</p>");
    //                    return;
    //                }
    //                studentObj = courseListGenerateForAddCourse();

    //                $.ajax({
    //                    url: "/Student/Payment/NewCourseAddPaymentAndMoneyReceiptGeneration",
    //                    type: "POST",
    //                    data: { studentObj: studentObj, stdPId: stdPId, recavailAmount: recavailAmount, nRec: nRec, sDis: sDis, recvdAmount: recvdAmount, paymentMethod: paymentMethod, referrerenceNote: referrerenceNote, nextRecDate: nextRecDate, referrer: referrer, },
    //                    beforeSend: function () {
    //                        $.blockUI({
    //                            timeout: 0,
    //                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //                        });
    //                    },
    //                    success: function (data) {
    //                        //alert(data);
    //                        $('#mainBody').html(data);
    //                        $('.totalPage').html(data);
    //                        $.unblockUI();
    //                        if (data.IsSuccess) {

    //                        } else {
    //                            $('.customMessage').append(data.Message);
    //                        }
    //                    },
    //                    complete: function () {
    //                        $.unblockUI();
    //                    },
    //                    error: function (data) {
    //                        bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
    //                    }
    //                });
    //            }
    //        }
    //    } else {
    //        $('#dueAmount').addClass('highlight');
    //        $('#dueAmount').after("<p style ='color:red'>Due amount Negative Not acceptable</p>");
    //    }
    //});
});