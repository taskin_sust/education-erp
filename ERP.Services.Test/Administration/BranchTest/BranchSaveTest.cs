﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BranchTest
{
    interface IBranchSaveTest
    {
        #region Basic Test
        void Save_Should_Return_Null_Exception();
        void Should_Save_Successfully();
        #endregion

        #region Business Logic Test
        void Save_Should_Check_Duplicate_Name_Entry();
        void Save_Should_Check_Duplicate_Code_Entry();
        void Should_Not_Save_Empty_Name();
        void Should_Not_Save_Empty_Code();
        void Should_Not_Save_Empty_Organization();

        #endregion

    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Branch")]
    public class BranchSaveTest : BranchBaseTest, IBranchSaveTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _branchService.IsSave(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var branchFactory =
                _branchFactory.Create().WithOrganization().WithNameAndRank(_name, 1).Persist();
            Branch branch = _branchService.GetBranch(branchFactory.Object.Id);
            Assert.NotNull(branch);
            Assert.True(branch.Id > 0);
        }
        
        #endregion

        #region Business Logic Test
        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            var branchFactory1 =
                _branchFactory.Create().WithOrganization().WithNameAndRank(_name, 1).Persist();
            var organization = branchFactory1.Object.Organization;
            var branchFactory2 =_branchFactory.Create().WithOrganization(organization).WithNameAndRank(_name, 1);

            Assert.Throws<DuplicateEntryException>(() => branchFactory2.Persist());
        }
        [Fact]
        public void Save_Should_Check_Duplicate_Code_Entry()
        {
            var branchFactory1 =_branchFactory.Create().WithOrganization().Persist();
            var organization = branchFactory1.Object.Organization;
            var branchFactory2 = _branchFactory.Create()
                .WithOrganization(organization)
                .WithNameAndRank("ShortName_" + Guid.NewGuid().ToString(),1)
                .WithCode(branchFactory1.SingleObjectList.Where(x=>x.Id!=0).Select(x=>x.Code).FirstOrDefault());

            Assert.Throws<DuplicateEntryException>(() => branchFactory2.Persist());
        }
        [Fact]
        public void Should_Not_Save_Empty_Name()
        {
            var branchFactory1 =_branchFactory.Create().WithOrganization().WithNameAndRank("", 1);
            Assert.Throws<MessageException>(() => branchFactory1.Persist());
        }
        [Fact]
        public void Should_Not_Save_Empty_Code()
        {
            var branchFactory1 =
                _branchFactory.Create().WithOrganization().WithNameAndRank(_name, 1).WithCode("");
            Assert.Throws<MessageException>(() => branchFactory1.Persist());
        }
        [Fact]
        public void Should_Not_Save_Empty_Organization()
        {
            var branchFactory = _branchFactory.Create().WithCode(_branchFactory.GetRandomNumber());
            Assert.Throws<MessageException>(() => branchFactory.Persist());
        }


        #endregion
    }
}
