﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ICourseService : IBaseService
    {
        #region Operational Function
        void CourseSaveOrUpdate(Course course);
        bool IsDelete(Course courseObj);
        bool UpdateRank(Course courseGroupOldObj, Course courseUpdateObj);
        #endregion

        #region Single Instances Loading Function
        Course GetCourse(long courseId);
        Course GetCourseByRankNextOrPrevious(int rank, string action);

        #endregion

        #region List Loading Function
        IList<Course> LoadCourse(List<long> organizationIds = null, List<long> programIds = null, List<long> sessionIds = null);
        IList<Course> LoadComplementaryCourse(List<long> organizationIds, List<long> programIds, List<long> sessionIds, long courseId);
        IList<Course> LoadCourse(long[] courseIds);
        IList<Course> LoadCourse(long programId, long sessionId);
        IList<Course> LoadCourse(long organizatonId, long programId, long sessionId, long[] courseIds);
        //IList<Course> LoadCourse(int start, int length, string orderBy, string orderDir, string organizationId, string programId, string sessionName, string name, string rank, string status);
        IList<Course> LoadCourse(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string organizationId, string programId, string sessionName, string name, string rank, string status);

        IList<Course> LoadCourse(string orgBusinessId, string prnNo);
        IList<Course> LoadCourse(DateTime date, long teacherId, List<long> programIds);
        #endregion

        #region Others Function
        int GetMaximumRank(Course course);
        int GetMinimumRank(Course course);
        bool CheckCouseAssignByStudent(long id);
        //int GetCourseCount(string orderBy, string orderDir, string organizationId, string programId, string sessionName, string name, string rank, string status);
        int GetCourseCount(string orderBy, string orderDir, List<UserMenu> userMenu, string organizationId, string programId, string sessionName, string name, string rank, string status);
        #endregion

        #region Public API
        IList<Course> LoadCourse(string organizationBusinessId, long programId, long sessionId);
        #endregion

    }
    public class CourseService : BaseService, ICourseService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ICourseDao _courseDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        private readonly ISubjectDao _subjectDao;
        private readonly ICourseSubjectDao _courseSubjectDao;
        private readonly IStudentCourseDetailDao _studentCourseDetailDao;
        private readonly IComplementaryCourseDao _complementaryCourseDao;
        private readonly ICommonHelper _commonHelper;
        public CourseService(ISession session)
        {
            Session = session;
            _courseDao = new CourseDao { Session = Session };
            _programDao = new ProgramDao { Session = Session };
            _sessionDao = new SessionDao { Session = Session };
            _subjectDao = new SubjectDao { Session = Session };
            _studentCourseDetailDao = new StudentCourseDetailDao { Session = Session };
            _courseSubjectDao = new CourseSubjectDao { Session = Session };
            _complementaryCourseDao = new ComplementaryCourseDao { Session = Session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function
        //need to review
        public void CourseSaveOrUpdate(Course course)
        {
            ITransaction trans = null;
            try
            {
                if (course == null)
                {
                    throw new NullObjectException("Course can not be null");
                }
                CheckValidation(course);
                if (course.Id < 1)
                {
                    //save
                    if (course.CourseSubjects.Count < course.MaxSubject)
                    {
                        throw new InvalidDataException(
                            "Maximum subject Count Must be less than or equal to selected subject");
                    }

                    course.Rank = _courseDao.GetMaximumRank(course) + 1;
                    course.Status = Course.EntityStatus.Active;
                    //check duplicate
                    CheckDuplicateCourse(course);

                    using (trans = Session.BeginTransaction())
                    {
                        //responsible for save course object
                        _courseDao.Save(course);

                        foreach (var courseSubject in course.CourseSubjects)
                        {
                            ZeroPaymentCheck(course, courseSubject.Payment);
                            courseSubject.Course = course;
                            courseSubject.Status = Course.EntityStatus.Active;
                            courseSubject.CreateBy = GetCurrentUserId();
                            courseSubject.ModifyBy = GetCurrentUserId();
                            _courseSubjectDao.Save(courseSubject);
                        }
                        trans.Commit();
                    }
                }
                else
                {
                    //update
                    Course courseDb = _courseDao.LoadById(course.Id);
                    if (courseDb == null)
                    {
                        throw new NullObjectException("Course can not be null");
                    }
                    if (courseDb.CourseSubjects.Count < courseDb.MaxSubject)
                    {
                        throw new InvalidDataException(
                            "Maximum subject Count Must be less than or equal to selected subject");
                    }

                    CheckDuplicateCourse(course);

                    using (trans = Session.BeginTransaction())
                    {
                        foreach (var courseSubject in course.CourseSubjects)
                        {
                            ZeroPaymentCheck(course, courseSubject.Payment);
                        }
                        _courseDao.Update(course);
                        trans.Commit();
                    }
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                throw;
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }

        }
        private void ZeroPaymentCheck(Course course, decimal payment)
        {
            // They fource to remove this 
            //if (course.Program.Type == 1 && payment <= 0)
            //{
            //    throw new InvalidDataException("Course Payment must be greater then 0 for this program.");
            //}
        }

        //need to review
        //public void CourseSaveOrUpdate(Course course)
        //{
        //    ITransaction trans = null;
        //    try
        //    {

        //        if (course.Id > 0)
        //        {
        //            Course courseDb = _courseDao.LoadById(course.Id);
        //            if (courseDb == null)
        //            {
        //                throw new NullObjectException("Course can not be null");
        //            }
        //            CheckValidation(course);
        //            if (courseDb.CourseSubjects.Count < courseDb.MaxSubject)
        //            {
        //                throw new InvalidDataException("Maximum subject Count Must be less than or equal to selected subject");
        //            }

        //            CheckDuplicateCourse(course);

        //            using (trans = Session.BeginTransaction())
        //            {
        //                foreach (var courseSubject in course.CourseSubjects)
        //                {
        //                    ZeroPaymentCheck(course, courseSubject.Payment);
        //                }
        //                _courseDao.Update(course);
        //                trans.Commit();
        //            }
        //        }
        //    }
        //    catch (NullObjectException ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    catch (InvalidDataException ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    catch (DuplicateEntryException ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    finally
        //    {
        //        if (trans != null && trans.IsActive)
        //            trans.Rollback();
        //    }
        //}
        //need to review
        public bool IsDelete(Course courseObj)
        {
            ITransaction trans = null;
            try
            {
                //var isThrowException = false;
                if ((courseObj.DiscountDetails.Count > 0))
                {
                    throw new DependencyException("this course have one or more dependency data.");
                }
                if (courseObj.CourseSubjects.Count > 0)
                {
                    foreach (var csubject in courseObj.CourseSubjects)
                    {
                        // var studentCourseDetails = _studentCourseDetailDao.LoadById(csubject.Id);
                        var studentCourseDetails = _studentCourseDetailDao.LoadStudentCourseDetailByCourseSubjectId(csubject.Id);
                        if (studentCourseDetails != null)
                        {
                            throw new DependencyException("This course have one or more dependency data.");
                        }
                    }
                }
                courseObj.Status = Course.EntityStatus.Delete;
                using (trans = Session.BeginTransaction())
                {
                    //try
                    //{
                    _courseDao.Update(courseObj);
                    trans.Commit();
                    return true;
                    //}
                    //catch (Exception ex)
                    //{
                    //    _logger.Error(ex);
                    //    return false;
                    //}
                }
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public bool UpdateRank(Course courseGroupOldObj, Course courseUpdateObj)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    //try
                    //{
                    //   
                    _courseDao.Update(courseGroupOldObj);
                    _courseDao.Update(courseUpdateObj);
                    trans.Commit();
                    return true;
                    //}
                    //catch (Exception e)
                    //{
                    //    trans.Rollback();
                    //    return false;
                    //}
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                return false;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public Course GetCourse(long courseId)
        {
            try
            {
                return _courseDao.LoadById(courseId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public Course GetCourseByRankNextOrPrevious(int newRank, string action)
        {
            try
            {
                return _courseDao.LoadByRankDirection(newRank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function
        public IList<Course> LoadCourse(long[] courseIds)
        {
            try
            {
                return _courseDao.LoadCourse(courseIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<Course> LoadCourse(long programId, long sessionId)
        {
            try
            {
                return _courseDao.LoadCourse(programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<Course> LoadCourse(long organizatonId, long programId, long sessionId, long[] courseIds)
        {
            try
            {
                return _courseDao.LoadCourse(organizatonId, programId, sessionId, courseIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        //public IList<Course> LoadCourse(int start, int length, string orderBy, string orderDir, string organizationId, string programId, string sessionName, string name, string rank, string status)
        public IList<Course> LoadCourse(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string organizationId, string programId, string sessionName, string name, string rank, string status)
        {
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organizationId) ? (long?)null : Convert.ToInt64(organizationId);
                long? convertedProgramId = String.IsNullOrEmpty(programId) ? (long?)null : Convert.ToInt64(programId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (convertedProgramId != null) ? _commonHelper.ConvertIdToList((long)convertedProgramId) : null);

                return _courseDao.LoadCourse(start, length, orderBy, orderDir, organizationIdList, programIdList, sessionName, name, rank, status);
                //return _courseDao.LoadCourse(start, length, orderBy, orderDir, organizationId, programId, sessionName, name, rank, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<Course> LoadCourse(List<long> organizationIds = null, List<long> programIds = null, List<long> sessionIds = null)
        {
            try
            {
                IList<Course> courseList = new List<Course>();
                if (organizationIds != null && programIds != null && sessionIds != null)
                {
                    courseList = _courseDao.LoadCourse(organizationIds, programIds, sessionIds);
                }

                if (organizationIds == null && programIds != null && sessionIds != null)
                {
                    courseList = _courseDao.LoadCourse(programIds, sessionIds);
                }
                if (organizationIds == null && programIds == null && sessionIds == null)
                {
                    courseList = _courseDao.LoadCourse();
                }

                return courseList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        public IList<Course> LoadComplementaryCourse(List<long> organizationIds, List<long> programIds, List<long> sessionIds, long courseId)
        {
            try
            {
                IList<Course> courseList = new List<Course>();
                //if (!_complementaryCourseDao.IsComplementaryCourse(organizationIds, programIds, sessionIds, courseId))
                //{
                courseList = _courseDao.LoadComplementaryCourse(organizationIds, programIds, sessionIds, courseId);
                //}
                return courseList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Course> LoadCourse(string orgBusinessId, string prnNo)
        {
            try
            {
                return _courseDao.LoadCourse(orgBusinessId, prnNo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Course> LoadCourse(DateTime date, long teacherId, List<long> programIds)
        {
            var sessionList = _sessionDao.LoadSession(date, teacherId, programIds);
            var sessionIds = sessionList.Select(x => x.Id).ToList();
            var courseList = _courseDao.LoadCourse(programIds, sessionIds);
            //var courses = new List<Course>();
            //courses.AddRange(courseList);
            //foreach (var course in courseList)
            //{
            //    courses.AddRange(course.ComplementaryCourses
            //        .Where(x => x.Program.Id.In(programIds.ToArray()) && x.Session.Id.In(sessionIds.ToArray()))
            //        .Select(x => x.CompCourse));
            //}
            //return courses;
            return courseList;
        }
        #endregion

        #region Others Function
        public int GetMaximumRank(Course course)
        {
            int maximumRank;
            try
            {
                maximumRank = _courseDao.GetMaximumRank(course);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return maximumRank;
        }
        public int GetMinimumRank(Course course)
        {
            int minimumRank;
            try
            {
                minimumRank = _courseDao.GetMinimumRank(course);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return minimumRank;
        }
        public bool CheckCouseAssignByStudent(long id)
        {
            try
            {
                return _courseDao.IsCourseAssignByStudent(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        //public int GetCourseCount(string orderBy, string orderDir, string organizationId, string programId, string sessionName, string name, string rank, string status)
        public int GetCourseCount(string orderBy, string orderDir, List<UserMenu> userMenu, string organizationId, string programId, string sessionName, string name, string rank, string status)
        {
            int courseCount;
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organizationId) ? (long?)null : Convert.ToInt32(organizationId);
                long? convertedProgramId = String.IsNullOrEmpty(programId) ? (long?)null : Convert.ToInt32(programId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (convertedProgramId != null) ? _commonHelper.ConvertIdToList((long)convertedProgramId) : null);

                //courseCount = _courseDao.GetCourseCount(orderBy, orderDir, organizationId, programId, sessionName, name, rank, status);
                courseCount = _courseDao.GetCourseCount(orderBy, orderDir, organizationIdList, programIdList, sessionName, name, rank, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return courseCount;
        }


        #endregion

        #region Helper Function
        private void CheckDuplicateCourse(Course course)
        {
            try
            {
                if (!String.IsNullOrEmpty(course.Name))
                {
                    bool isDuplicateCourse = _courseDao.IsDuplicateCourse(course.Name.Trim(), course.Program.Id, course.RefSession.Id, course.Id);
                    if (isDuplicateCourse)
                    {
                        throw new DuplicateEntryException("Duplicate course found");
                    }
                }
            }

            catch (DuplicateEntryException ex)
            {
                throw;
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                //return false;
                throw;
            }
        }
        private static void CheckValidation(Course course)
        {
            var validationResult = ValidationHelper.ValidateEntity<Course, Course.CourseMetaData>(course);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }
        #endregion

        #region Public API
        public IList<Course> LoadCourse(string organizationBusinessId, long programId, long sessionId)
        {
            IList<Course> courses;
            try
            {
                courses = _courseDao.LoadCourse(organizationBusinessId, programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return courses;
        }
        #endregion
    }
}
