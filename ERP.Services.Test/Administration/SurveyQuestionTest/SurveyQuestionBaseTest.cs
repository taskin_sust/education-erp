﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.SurveyQuestionTest
{
    public class SurveyQuestionBaseTest : TestBase, IDisposable
    {
       
        #region Object Initialization
        private ISession _session;
        internal SurveyQuestionService _surveyQuestionService;
        internal SurveyQuestionFactory _surveyQuestionFactory;
        internal SurveyQuestionAnswerFactory _surveyQuestionAnswerFactory;
        internal readonly string _question = "Survey_Question_" + Guid.NewGuid().ToString().Substring(0, 6);
        #endregion 

        public SurveyQuestionBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _surveyQuestionService = new SurveyQuestionService(_session);
            _surveyQuestionFactory = new SurveyQuestionFactory(null, _session);
            _surveyQuestionAnswerFactory = new SurveyQuestionAnswerFactory(null, _session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _surveyQuestionAnswerFactory.Cleanup();
            _surveyQuestionFactory.Cleanup();
            base.Dispose();
        }
    }
}
