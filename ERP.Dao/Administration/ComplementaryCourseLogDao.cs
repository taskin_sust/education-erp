﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{

    public interface IComplementaryCourseLogDao : IBaseDao<ComplementaryCourseLog, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion

        #region Public API

        #endregion






    }

    public class ComplementaryCourseLogDao : BaseDao<ComplementaryCourseLog, long>, IComplementaryCourseLogDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function


        #endregion

        #region Helper Function

        #endregion

        #region Public API

        #endregion
    }
}
