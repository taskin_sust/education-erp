﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.LectureSettingTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "LectureSetting")]
    public class LectureSettingSaveTest : LectureSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Save_Successfully()
        {
            var lectureSettingFactory =
                _lectureSettingFactory.Create().WithCourseSubject()
                    .Persist();
            LectureSettings lectureSettings = _lectureSettingService.LoadById(lectureSettingFactory.Object.Id);
            //IList<Lecture> lectures = _lectureService.LoadLecture(lectureSettingFactory.Object.Lectures.Select(x => x.Id).ToArray());
            IList<Lecture> lectures = _lectureService.LoadLecture(lectureSettings.Id);
            Assert.NotNull(lectures);
            Assert.True(lectures.Count > 0);
            _TestBaseService.DeleteProgramSessionSubject(lectureSettings.Course.Program.Id, lectureSettings.Course.RefSession.Id);
            _TestBaseService.DeleteLecture(lectures);
        }
        #endregion

    }
}
