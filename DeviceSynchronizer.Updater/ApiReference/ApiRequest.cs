﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using log4net;
using Synchronizer.Updater.BusinessRule;
using Synchronizer.Updater.Helper;
using Synchronizer.Updater.Model;
using Synchronizer.Updater.Services;

namespace Synchronizer.Updater.ApiReference
{
    public class ApiRequest
    {
        #region Objects/Properties/Services/Dao & Initialization

        private static readonly ILog Log = LogManager.GetLogger("ApiLogger");

        private readonly string _baseUrl;
        private readonly HttpHelper _httpHelper;
        private const string SoftwareUpdateFunctioName = "GetSoftwareUpdate";
        private const string LogSoftwareUpdateFunctionName = "LogSoftwareUpdate";
        private readonly SynchronizerUpdater.ViewMessageDelegate _viewMessage;

        public ApiRequest(SynchronizerUpdater.ViewMessageDelegate currentMessage)
        {
            _baseUrl = AppConfigHelper.GetString(UpdaterConstant.GetBaseUrl);
            _httpHelper = new HttpHelper();
            _viewMessage = currentMessage;
        }

        #endregion

        #region Delegate & Function

        private void OnViewMessage(string message)
        {
            if (_viewMessage != null)
            {
                _viewMessage(message);
            }
        }

        #endregion

        #region Operational Function

        public JsonResponse GetUrl(string appVersion)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                var dataDictionary = new Dictionary<string, string> { { "versionNumber", appVersion } };
                string param = GenerateParam(dataDictionary);
                string url = GenerateUrl(SoftwareUpdateFunctioName, param);
                response = _httpHelper.HttpGetRequest(url);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return response;
        }

        public JsonResponse GetLog(string appVersion, string isSucess, string message)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                var dataDictionary = new Dictionary<string, string>
                { 
                    { "versionNumber", appVersion },
                    { "isSuccess", isSucess } ,
                    { "message",message}
                };
                string param = GenerateParam(dataDictionary);
                string url = GenerateUrl(LogSoftwareUpdateFunctionName, param);
                response = _httpHelper.HttpGetRequest(url);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return response;
        }

        public string Download(string url)
        {
            string downloadDirectorPath = null;
            try
            {
                Uri uri = new Uri(url);
                string fileName = Path.GetFileName(uri.LocalPath);
                var downloadDirectory = AppHelper.GetRequiredDirectory(AppHelper.RequiredDirectory.Download);

                //assign download directory to the globa variable
                downloadDirectorPath = downloadDirectory + "\\" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "_" + fileName;
                WebClient webClient = new WebClient();
                webClient.DownloadFile(uri, downloadDirectorPath);
                OnViewMessage("Latest software successfully downloaded.");
            }
            catch (Exception ex)
            {
                OnViewMessage("Failed to download software.");
                Log.Error(ex);
            }
            return downloadDirectorPath;
        }

        #endregion

        #region Helper Function

        private string GenerateUrl(string functionName, string parameter)
        {
            try
            {
                return _baseUrl + functionName + "?" + parameter;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        private string GenerateParam(Dictionary<string, string> dataDictionary)
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("key=" + AppConfigHelper.GetString(UpdaterConstant.GetSecurityKey));
                if (dataDictionary != null)
                {
                    foreach (var data in dataDictionary)
                    {
                        builder.Append("&" + data.Key + "=" + data.Value);
                    }
                }
                return builder.ToString();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return "";
        }

        #endregion
    }
}
