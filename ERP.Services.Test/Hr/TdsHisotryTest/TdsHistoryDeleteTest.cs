﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.TdsHisotryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "TdsHistory")]
    public class TdsHistoryDeleteTest : TdsHistoryBaseTest
    {
        #region Basic Test
        //[Fact]
        //public void TdsHisotry_Delete_Sucessfully()
        //{
        //    var teamMember = PersistTeamMember(MentorId);
        //    _tdsHistoryFactory.Create().WithTeamMember(teamMember);
        //    _tdsHistoryService.Save(_tdsHistoryFactory.Object);
        //    teamMember.TdsHistory.Add(_tdsHistoryFactory.Object);
        //    bool result = _tdsHistoryService.DeleteTdsHistory(_tdsHistoryFactory.Object);
        //    Assert.True(result);
        //} 
        #endregion

        #region Business Logic

        [Fact]
        public void DependencyEeception_DeleteTdsHistory_For_TdsHistory()
        {
            var teamMember = PersistTeamMember(MentorId);
            _tdsHistoryFactory.Create().WithTeamMember(teamMember);
            _tdsHistoryService.Save(_tdsHistoryFactory.Object);
            Assert.Throws<DependencyException>(()=>_tdsHistoryService.DeleteTdsHistory(_tdsHistoryFactory.Object));
            
        }

        #endregion
    }
}
