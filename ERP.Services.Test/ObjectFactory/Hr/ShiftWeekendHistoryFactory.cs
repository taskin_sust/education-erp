﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class ShiftWeekendHistoryFactory : ObjectFactoryBase<ShiftWeekendHistory>
    {
        private readonly ShiftFactory _shiftFactory;
        private readonly TeamMemberFactory _teamMemberFactory;
        private readonly OrganizationFactory _organizationFactory;
        private readonly IShiftWeekendHistoryService _shiftWeekendHistoryService;
        private readonly TestBaseService<ShiftWeekendHistoryLog> _shiftWeekendHistoryLogService;
        private ISession _session;
        public ShiftWeekendHistoryFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _session = session;
            _shiftFactory = new ShiftFactory(this, session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _organizationFactory = new OrganizationFactory(this, session);

            _shiftWeekendHistoryService = new ShiftWeekendHistoryService(session);
            _shiftWeekendHistoryLogService = new TestBaseService<ShiftWeekendHistoryLog>(session);
        }

        public ShiftWeekendHistoryFactory Create()
        {
            Object = new ShiftWeekendHistory()
            {
                Id = 0,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Department.EntityStatus.Active,
                Weekend = 1,
                EffectiveDate = DateTime.Now.AddDays(-1),
                BusinessId = GetPcUserName(),
            };

            SingleObjectList.Add(Object);

            return this;
        }

        public ShiftWeekendHistoryFactory CreateWith(long id, int weekend, DateTime effectiveDate)
        {
            Object = new ShiftWeekendHistory()
            {
                Id = id,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Department.EntityStatus.Active,
                Weekend = weekend,
                EffectiveDate = effectiveDate,
                BusinessId = GetPcUserName(),
            };

            SingleObjectList.Add(Object);

            return this;
        }

        public ShiftWeekendHistoryFactory CreateMore(int numberOfCount)
        {
            _organizationFactory.Create();
            CreateShiftWeekendHistoryList(numberOfCount, _organizationFactory.ObjectList);
            return this;
        }

        public ShiftWeekendHistoryFactory CreateMore(int numberOfCount, Organization organization)
        {
            CreateShiftWeekendHistoryList(numberOfCount, new List<Organization>() { organization });
            return this;
        }
        public ShiftWeekendHistoryFactory CreateMore(int numberOfCount, List<Organization> organizations)
        {
            CreateShiftWeekendHistoryList(numberOfCount, organizations);
            return this;
        }

        private IList<ShiftWeekendHistory> CreateShiftWeekendHistoryList(int numberOfCount, List<Organization> organizations)
        {
            _shiftFactory.CreateMore(numberOfCount, organizations[0]).Persist();

            foreach (var organization in organizations)
            {
                for (int i = 0; i < numberOfCount; i++)
                {
                    //_shiftFactory.Create().WithNameAndRank(Guid.NewGuid().ToString(), new Random().Next(99999))
                    //     .WithStartTimeEndTime()
                    //     .WithOrganization(organization).Persist();
                    var _object = new ShiftWeekendHistory()
                    {
                        Id = 0,
                        Rank = 1,
                        CreateBy = 1,
                        ModifyBy = 1,
                        Status = Department.EntityStatus.Active,
                        Weekend = 1,
                        EffectiveDate = DateTime.Now.AddDays(-1),
                        Organization = organization,
                        // Shift = _shiftFactory.Object,
                        BusinessId = GetPcUserName(),
                    };
                    ObjectList.Add(_object);
                    ObjectList[i].Shift = _shiftFactory.ObjectList[i];
                }
            }
            return ObjectList;
        }


        public ShiftWeekendHistoryFactory CreateMoreShift(int numberOfCount, List<Organization> organizations)
        {
            CreateShiftWeekendHistoryListForMultipleShift(numberOfCount, organizations);
            return this;
        }

        private IList<ShiftWeekendHistory> CreateShiftWeekendHistoryListForMultipleShift(int numberOfCount, List<Organization> organizations)
        {
            //_shiftFactory.CreateMore(numberOfCount, organizations).Persist();

            foreach (var organization in organizations)
            {
                _shiftFactory.Create().WithNameAndRank(Guid.NewGuid().ToString(),Convert.ToInt32(organization.Id))
                     .WithStartTimeEndTime().WithOrganization(organization).Persist();

                for (int i = 0; i < numberOfCount; i++)
                {
                    var _object = new ShiftWeekendHistory()
                    {
                        Id = 0,
                        Rank = 1,
                        CreateBy = 1,
                        ModifyBy = 1,
                        Status = Department.EntityStatus.Active,
                        Weekend = 1,
                        EffectiveDate = DateTime.Now.AddDays(-1),
                        Organization = organization,
                        Shift = _shiftFactory.Object,
                        BusinessId = GetPcUserName(),
                    };
                    ObjectList.Add(_object);
                   // ObjectList[i].Shift = _shiftFactory.ObjectList[i];
                }
            }
            return ObjectList;
        }
        
        public ShiftWeekendHistoryFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }

        public ShiftWeekendHistoryFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public ShiftWeekendHistoryFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }
        public ShiftWeekendHistoryFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public ShiftWeekendHistoryFactory WithShift()
        {
            _shiftFactory.Create().Persist();
            Object.Shift = _shiftFactory.Object;
            return this;
        }

        public ShiftWeekendHistoryFactory WithShift(Organization organization)
        {
            _shiftFactory.Create().WithStartTimeEndTime().WithOrganization(organization).Persist();
            Object.Shift = _shiftFactory.Object;
            return this;
        }
        public ShiftWeekendHistoryFactory WithShift(Shift shift)
        {
            Object.Shift = shift;
            return this;
        }

        public ShiftWeekendHistoryFactory Persist()
        {
            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    _shiftWeekendHistoryService.Save(x);
                }
            });

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _shiftWeekendHistoryService.Save(item);
                    }
                }
            }

            return this;
        }

        public void CleanUp()
        {
            DeleteAll();
            _shiftFactory.Cleanup();
        }

        public void ShiftCleanUp()
        {
            _shiftFactory.Cleanup();
        }

        public void LogCleanUp(TeamMember teamMember)
        {
            //_shiftWeekendHistoryLogService.DeleteShiftWeekendLogByShiftWeekendIdList(teamMember);
            var list = teamMember.ShiftWeekendHistory.Select(x => x.Id).ToList();
            var logHistoryIdList =

               _session.QueryOver<ShiftWeekendHistoryLog>()
                // .Select(x => x.Id)
                   .Where(x => x.TeamMemberId == teamMember.Id || x.ShiftWeekendHistoryId.IsIn(list.ToArray()) || x.TeamMemberId == null)
                   .List<ShiftWeekendHistoryLog>();
            if (logHistoryIdList.Count > 0)
            {
                foreach (var swhl in logHistoryIdList)
                {
                    string a = "DELETE FROM [HR_ShiftWeekendHistoryLog] WHERE [Id] = " + swhl.Id + ";";
                    _session.CreateSQLQuery(a).ExecuteUpdate();
                }
                //dao.Session.Flush();
                //string a = "DELETE FROM [HR_ShiftWeekendHistoryLog] WHERE [Id] IN (" + string.Join(",", logHistoryIdList) + "); ";
                //dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                //dao.Session.Flush();
            }
        }


        public void CleanUp(TeamMember teamMember)
        {
            IList<ShiftWeekendHistory> list = _shiftWeekendHistoryService.LoadShiftWeekendHistory(teamMember.Id);
            foreach (var shift in list)
            {
                string a = "DELETE FROM [HR_ShiftWeekendHistory] WHERE [TeamMemberId] = " + shift.TeamMember.Id + ";";
                _session.CreateSQLQuery(a).ExecuteUpdate();
            }
        }
    }
}
