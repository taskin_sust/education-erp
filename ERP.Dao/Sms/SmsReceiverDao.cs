﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public interface ISmsReceiverDao : IBaseDao<SmsReceiver, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
   IList<SmsReceiver> LoadSmsReceiverActive();
        IList<SmsReceiver> LoadSmsReceiverByIds(long[] SmsReceivers);
        #endregion

        #region Others Function

        #endregion

     
    }
    public class SmsReceiverDao:BaseDao<SmsReceiver, long>,ISmsReceiverDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<SmsReceiver> LoadSmsReceiverActive()
        {
            ICriteria criteria = Session.CreateCriteria<SmsReceiver>();
            criteria.Add(Restrictions.Eq("Status", SmsReceiver.EntityStatus.Active));
            criteria.AddOrder(Order.Asc("Rank"));
            return criteria.List<SmsReceiver>();
        }

        public IList<SmsReceiver> LoadSmsReceiverByIds(long[] SmsReceivers)
        {
            ICriteria criteria = Session.CreateCriteria<SmsReceiver>();
            criteria.Add(Restrictions.Eq("Status", SmsReceiver.EntityStatus.Active));
            criteria.Add(Restrictions.In("Id", SmsReceivers));
            criteria.AddOrder(Order.Asc("Rank"));
            return criteria.List<SmsReceiver>();
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
       
    }
}
