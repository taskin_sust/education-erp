﻿using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.OpeningBalanceTest
{
    interface IItemOpeningBalanceTest
    {
        void Save_Should_Be_Success();
        void Save_Should_Be_Failed_Without_Branch();
        void Save_Should_Be_Failed_Without_Item();
        void Save_Should_Be_Failed_Without_GoodsReceiveDetails();
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "ItemOpeningBalance")]
    public class ItemOpeningBalanceSaveTest : ItemOpeningBalanceBaseTest, IItemOpeningBalanceTest
    {
        [Fact]
        public void Save_Should_Be_Success()
        {
            organizationFactory.Create().Persist();
            _ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _BranchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _SessionFactory.Create().Persist();
            _itemFactory.Create(ItemType.ProgramItem).WithItemGroup().WithOrganization(organizationFactory.Object).Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_ProgramFactory.Object, _SessionFactory.Object).Persist();
            _goodsReceiveDetailsFactory.Create().WithPurpose().WithItem(_itemFactory.Object).WithProgram(_ProgramFactory.Object).WithSession(_SessionFactory.Object);
            _goodsReceiveFactory.Create().WithGoodsReceiveDetails(_goodsReceiveDetailsFactory.Object);
            var receiving = _goodsReceiveFactory.WithBranch(_BranchFactory.Object).Persist(true);
            GoodsReceive goodsReceive = _goodsReceiveService.GetGoodsReceive(receiving.Object.Id);
            Assert.NotNull(goodsReceive);
        }

        [Fact]
        public void Save_Should_Be_Failed_Without_Branch()
        {
            organizationFactory.Create().Persist();
            _ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _BranchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _SessionFactory.Create().Persist();
            _itemFactory.Create(ItemType.ProgramItem).WithItemGroup().WithOrganization(organizationFactory.Object).Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_ProgramFactory.Object, _SessionFactory.Object).Persist();
            _goodsReceiveDetailsFactory.Create().WithPurpose().WithItem(_itemFactory.Object).WithProgram(_ProgramFactory.Object).WithSession(_SessionFactory.Object);
            _goodsReceiveFactory.Create().WithGoodsReceiveDetails(_goodsReceiveDetailsFactory.Object);
            Assert.Throws<NullObjectException>(() => _goodsReceiveFactory.Create().Persist(true));
        }


        [Fact]
        public void Save_Should_Be_Failed_Without_Item()
        {
            organizationFactory.Create().Persist();
            _ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _BranchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _SessionFactory.Create().Persist();
            _itemFactory.Create(ItemType.ProgramItem).WithItemGroup().WithOrganization(organizationFactory.Object).Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_ProgramFactory.Object, _SessionFactory.Object).Persist();
            _goodsReceiveDetailsFactory.Create().WithPurpose().WithProgram(_ProgramFactory.Object).WithSession(_SessionFactory.Object);
            _goodsReceiveFactory.Create().WithGoodsReceiveDetails(_goodsReceiveDetailsFactory.Object);
            Assert.Throws<NullObjectException>(() => _goodsReceiveFactory.WithBranch(_BranchFactory.Object).Persist(true));
        }


        [Fact]
        public void Save_Should_Be_Failed_Without_GoodsReceiveDetails()
        {
            organizationFactory.Create().Persist();
            _ProgramFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _BranchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _SessionFactory.Create().Persist();
            _itemFactory.Create(ItemType.ProgramItem).WithItemGroup().WithOrganization(organizationFactory.Object).Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_ProgramFactory.Object, _SessionFactory.Object).Persist();
            _goodsReceiveDetailsFactory.Create().WithPurpose().WithItem(_itemFactory.Object).WithProgram(_ProgramFactory.Object).WithSession(_SessionFactory.Object);
            // _goodsReceiveFactory.Create().WithGoodsReceiveDetails(_goodsReceiveDetailsFactory.Object);
            //_goodsReceiveFactory.WithBranch(_BranchFactory.Object).Persist(true);
            Assert.Throws<NullObjectException>(() => _goodsReceiveFactory.Create().WithBranch(_BranchFactory.Object).Persist(true));
        }
    }
}
