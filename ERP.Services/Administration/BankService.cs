using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Administration
{
    public interface IBankService : IBaseService
    {
        #region Operational Function
        bool SaveOrUpdate(Bank bank);
        bool UpdateRank(long id, int current, string action);
        bool Delete(long id);
        #endregion

        #region Single Instances Loading Function
        Bank GetBank(long id);
        #endregion

        #region List Loading Function

        IList<Bank> LoadBankList(int start, int length, string orderBy = "", string orderDir = "", string name = "", string shortname = "",
            string rank = "", string status = "");
        IList<Bank> LoadBankList();
        #endregion

        #region Others Function
        int BankRowCount(string name, string shortname, string rank, string status);

        int GetMinimumRank(Bank bank);

        int GetMaximumRank(Bank bank);
        #endregion

        #region Helper Function

        #endregion

    }
    public class BankService : BaseService, IBankService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IBankDao _bankDao;
        public BankService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _bankDao = new BankDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        #region save or update methods
        public void ModelValidationCheck(Bank bank)
        {
            try
            {
                var validationResult = ValidationHelper.ValidateEntity<Bank, Bank.BankMetaData>(bank);
                if (validationResult.HasError)
                {
                    string errorMessage = "";
                    validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                    throw new InvalidDataException(errorMessage);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        private void CheckDuplicateFields(Bank bank, long id = 0)
        {
            try
            {
                var isDuplicateName = _bankDao.CheckDuplicateFields(id, bank.Name);
                if (isDuplicateName)
                    throw new DuplicateEntryException("Name can not be duplicate");
                var isDuplicateShortName = _bankDao.CheckDuplicateFields(id, shortname: bank.ShortName);
                if (isDuplicateShortName)
                    throw new DuplicateEntryException("Short name can not be duplicate");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        private void CheckBeforeSave(Bank bank)
        {
            CheckDuplicateFields(bank);
        }

        public bool SaveOrUpdate(Bank bank)
        {
            ITransaction transaction = null;
            try
            {
                if (bank == null)
                {
                    throw new NullObjectException("Bank can not be null");
                }

                ModelValidationCheck(bank);

                using (transaction = Session.BeginTransaction())
                {
                    if (bank.Id < 1)
                    {
                        CheckBeforeSave(bank);
                        bank.Rank = _bankDao.GetMaximumRank(bank) + 1;
                        _bankDao.Save(bank);
                    }
                    else
                    {
                        var oldBank = _bankDao.LoadById(bank.Id);
                        //ModelValidationCheck(organization);
                        CheckDuplicateFields(bank, bank.Id);
                        oldBank.Name = bank.Name;
                        oldBank.ShortName = bank.ShortName;
                        oldBank.Address = bank.Address;
                        oldBank.Status = bank.Status;
                        _bankDao.Update(oldBank);
                    }
                    transaction.Commit();
                    return true;
                }
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        public bool UpdateRank(long id, int current, string action)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Bank bankUpdateObj = _bankDao.LoadById(Convert.ToInt64(id));
                    if (bankUpdateObj != null)
                    {
                        int newRank;
                        if (action == RankOperation.Up)
                            newRank = bankUpdateObj.Rank - 1;
                        else
                            newRank = bankUpdateObj.Rank + 1;
                        var bankOldObj = _bankDao.LoadByRankDirection(newRank, action);//LoadByRank(newRank);
                        newRank = bankOldObj.Rank;
                        bankOldObj.Rank = bankUpdateObj.Rank;
                        bankUpdateObj.Rank = newRank;
                        _bankDao.Update(bankOldObj);
                        _bankDao.Update(bankUpdateObj);
                        transaction.Commit();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region delete operation
        public bool Delete(long id)
        {
            ITransaction transaction = null;
            try
            {
                var tempBankObj = _bankDao.LoadById(id);
                CheckBeforeDelete(tempBankObj);
                using (transaction = Session.BeginTransaction())
                {
                    tempBankObj.Status = BusinessModel.Entity.Administration.Bank.EntityStatus.Delete;
                    _bankDao.Update(tempBankObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        private void CheckBeforeDelete(Bank bank)
        {
            try
            {
                if (bank.BankBranch != null && bank.BankBranch.Count > 0)
                    throw new DependencyException("You can't delete this bank, bank is already assigned to bank branch(s).");
                //needs more dependency checks later
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #endregion

        #region Single Instances Loading Function
        public Bank GetBank(long id)
        {
            try
            {
                var bank = _bankDao.LoadById(id);
                return bank;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function

        public IList<Bank> LoadBankList(int start, int length, string orderBy = "", string orderDir = "", string name = "", string shortname = "", string rank = "", string status = "")
        {
            try
            {
                var bankList = _bankDao.LoadBankList(start, length, orderBy, orderDir, name, shortname, rank, status);
                return bankList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<Bank> LoadBankList()
        {
            try
            {
                var bankList = _bankDao.LoadBankList();
                return bankList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function

        public int BankRowCount(string name, string shortname, string rank, string status)
        {
            int rowCount = _bankDao.GetBankRowCount(name, shortname, rank, status);
            return rowCount;
        }

        public int GetMinimumRank(Bank bank)
        {
            int minimumRank;
            try
            {
                minimumRank = _bankDao.GetMinimumRank(bank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return minimumRank;
        }

        public int GetMaximumRank(Bank bank)
        {
            int maximumRank;
            try
            {
                maximumRank = _bankDao.GetMaximumRank(bank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return maximumRank;
        }
        #endregion

        #region Helper function
        #endregion
    }
}

