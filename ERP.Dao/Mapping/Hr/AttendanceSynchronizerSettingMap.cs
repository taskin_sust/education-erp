using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{
    public partial class AttendanceSynchronizerSettingMap : BaseClassMap<AttendanceSynchronizerSetting,long> {        

        public AttendanceSynchronizerSettingMap() {
			Table("HR_AttendanceSynchronizerSetting");
			LazyLoad();
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.SynchronizerVersion).Column("SynchronizerVersion");
			Map(x => x.UpdateUrl).Column("UpdateUrl");
			Map(x => x.ReleaseDate).Column("ReleaseDate");
			Map(x => x.Description).Column("Description");
        }
    }
}
