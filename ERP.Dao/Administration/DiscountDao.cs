﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IDiscountDao : IBaseDao<Discount, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function    
        IList<Discount> GetDiscountByProgramSessionAndDate(Program program, Session session, DateTime stdAdmitDate);
        //IList<Discount> LoadDiscount(int start, int length, string orderBy, string orderDir, string organizationId, string programId, string sessionId, string courseId, string formatDate, string toDate, string amount, string status);
        IList<Discount> LoadDiscount(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionId, string courseId, string formatDate, string toDate, string amount, string status);
        #endregion

        #region Others Function
        bool IsDuplicateDiscount(Discount discount);
        //int GetDiscountCount(string orderBy, string orderDir, string organizationId, string programId, string sessionId, string courseId, string formatDate, string toDate, string amount, string status);
        int GetDiscountCount(string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionId, string courseId, string formatDate, string toDate, string amount, string status);
        #endregion
    }
    public class DiscountDao : BaseDao<Discount, long>, IDiscountDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<Discount> GetDiscountByProgramSessionAndDate(Program program, Session session, DateTime stdAdmitDate)
        {
            return Session.QueryOver<Discount>().Where(x =>x.Session == session && x.Program == program && x.Status == Discount.EntityStatus.Active 
                && stdAdmitDate >= x.StartDate && stdAdmitDate <= x.EndDate).List<Discount>();
        }
        //public IList<Discount> LoadDiscount(int start, int length, string orderBy, string orderDir, string organizationId, string programId, string sessionId, string courseId,string fromtDate, string toDate, string amount, string status)
        public IList<Discount> LoadDiscount(int start, int length, string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionId, string courseId, string fromtDate, string toDate, string amount, string status)
        {
            //var criteria = LoadSessionCriteria(orderBy, orderDir, organizationId, programId, sessionId, courseId,
            //    fromtDate, toDate, amount, status);
            var criteria = LoadSessionCriteria(orderBy, orderDir, organizationIdList, programIdList, sessionId, courseId, fromtDate, toDate, amount, status);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Discount>();
        }
        
        #endregion

        #region Others Function
        //public int GetDiscountCount(string orderBy, string orderDir, string organizationId, string programId, string sessionId, string courseId, string fromDate, string toDate, string amount, string status)
        public int GetDiscountCount(string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionId, string courseId, string fromDate, string toDate, string amount, string status)
        {
            //var criteria = LoadSessionCriteria(orderBy, orderDir, organizationId, programId, sessionId, courseId, fromDate, toDate, amount, status, true);
            var criteria = LoadSessionCriteria(orderBy, orderDir, organizationIdList, programIdList, sessionId, courseId, fromDate, toDate, amount, status, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
        public bool IsDuplicateDiscount(Discount discount)
        {
            //try
            //{
                var exitingDiscountList =
                Session.QueryOver<Discount>()
                    .Where(
                        x =>
                            x.Session == discount.Session && x.Program == discount.Program &&
                            x.Status == Discount.EntityStatus.Active)
                    .List<Discount>();
                if (exitingDiscountList.Count <= 0)
                {
                    return false;
                }
                var countCurrentDiscountCourse = discount.DiscountDetails.Count;
                foreach (var exitingDiscount in exitingDiscountList)
                {
                    if (countCurrentDiscountCourse == exitingDiscount.DiscountDetails.Count)
                    {
                        var totalCourseinthisDiscount = exitingDiscount.DiscountDetails.Count;
                        int ii = 0;
                        foreach (var discountDetails in exitingDiscount.DiscountDetails)
                        {
                            var existingDiscountCourse = discountDetails.Course;
                            var existingDiscountMinSub = discountDetails.MinSubject;
                            var existingDiscountStartDate = exitingDiscount.StartDate;
                            var existingDiscountEndDate = exitingDiscount.EndDate;
                            var discountAmount = exitingDiscount.Amount;
                            foreach (DiscountDetail newlyConstructDiscount in discount.DiscountDetails)
                            {
                                if (existingDiscountCourse == newlyConstructDiscount.Course && existingDiscountMinSub == newlyConstructDiscount.MinSubject
                                    && discount.StartDate >= existingDiscountStartDate
                                    && discount.EndDate <= existingDiscountEndDate
                                    && discountAmount == discount.Amount
                                    )
                                {
                                    ii++;
                                    break;
                                }
                            }
                        }
                        if (totalCourseinthisDiscount == ii)
                        {
                            /*Duplicate Discount Found */
                            return true;
                        }
                    }
                    //return false;
                }
                return false;
            //}
            //catch (Exception e)
            //{
            //    return true;
            //}
        }

        #endregion

        #region Helper Function
        //private ICriteria LoadSessionCriteria(string orderBy, string orderDir, string organizationId, string programId, string sessionId, string courseId,
        //string fromtDate, string toDate, string amount, string status, bool countQuery = false)
        private ICriteria LoadSessionCriteria(string orderBy, string orderDir, List<long> organizationIdList, List<long> programIdList, string sessionId, string courseId,
        string fromtDate, string toDate, string amount, string status, bool countQuery = false)
        {
            var criteria = Session.CreateCriteria<Discount>().Add(Restrictions.Not(Restrictions.Eq("Status", Discount.EntityStatus.Delete)));
            criteria.CreateAlias("Program", "program");
            criteria.CreateAlias("Session", "session");
            criteria.CreateAlias("program.Organization", "organization");
            //if (!String.IsNullOrEmpty(organizationId))
            //{
            //    criteria.Add(Restrictions.Eq("program.Organization.Id", Convert.ToInt64(organizationId.Trim())));
            //}
            //if (!String.IsNullOrEmpty(programId))
            //{
            //    criteria.Add(Restrictions.Eq("Program.Id", Convert.ToInt64(programId.Trim())));
            //}
            if (organizationIdList != null)
            {
                criteria.Add(Restrictions.In("program.Organization.Id", organizationIdList));
            }
            if (programIdList != null)
            {
                criteria.Add(Restrictions.In("Program.Id", programIdList));
            }

            if (!String.IsNullOrEmpty(sessionId))
            {
                criteria.Add(Restrictions.Eq("Session.Id", Convert.ToInt64(sessionId.Trim())));
            }
            if (!String.IsNullOrEmpty(fromtDate))
            {
                fromtDate = fromtDate + " 00:00:00.000";
                criteria.Add(Restrictions.Ge("StartDate", Convert.ToDateTime(fromtDate.Trim())));
            }
            if (!String.IsNullOrEmpty(toDate))
            {
                toDate = toDate + " 23:59:59.000";
                criteria.Add(Restrictions.Le("EndDate", Convert.ToDateTime(toDate.Trim())));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!String.IsNullOrEmpty(amount))
            {
                //criteria.Add(Restrictions.Ge("Amount", Convert.ToDecimal(amount)));
                criteria.Add(Restrictions.Eq("Amount", Convert.ToDecimal(amount)));
            }
            if (!String.IsNullOrEmpty(courseId))
            {
                DetachedCriteria discountDetails = DetachedCriteria.For<DiscountDetail>()
                    .SetProjection(Projections.Distinct(Projections.Property("Discount.Id")))
                    .Add(Restrictions.Eq("Course.Id", Convert.ToInt64(courseId)));

                criteria.Add(Subqueries.PropertyIn("Id", discountDetails));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }

            return criteria;

        }
        #endregion
    }
}