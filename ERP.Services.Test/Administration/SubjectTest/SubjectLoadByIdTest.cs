﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SubjectTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Subject")]
    public class SubjectLoadByIdTest : SubjectBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _subjectService.GetSubject(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            SubjectFactory sessionFactory =
                _subjectFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithProperties(_shortName)
                    .Persist();
            var obj = _subjectService.GetSubject(sessionFactory.Object.Id);
            Assert.NotNull(obj);
            
        }
        [Theory]
        [InlineData(1, "down")]
        [InlineData(1000, "up")]
        public void GetSubjectByRankNextOrPrevious_NotNull_Check(int rank, string action)
        {
            _subjectFactory.Create().WithProperties(_shortName).Persist();
            var subjectByRank = _subjectService.GetSubjectByRankNextOrPrevious(rank, action);
            Assert.NotNull(subjectByRank);
        } 
        
    }
}
