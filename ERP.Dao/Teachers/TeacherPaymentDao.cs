﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Teachers
{

    public interface ITeacherPaymentDao : IBaseDao<TeacherPayment, long>
    {
      //  ISession Session { get; set; }
        #region Operational Function

        //void SaveTeacherPayment();

        #endregion

        #region Single Instances Loading Function

        TeacherPayment GetTeacherPaymentByBranchCodeAndYear2Digit(string branchYearCode);
        TeacherPayment GetTeacherPaymentByVoucherNo(string voucherNo);

        #endregion

        #region List Loading Function

        //List<TeacherPaymentListDto> LoadTeacherPaymentList(int start, int length, string orderBy, string orderDir, List<long> authBranchIds, string teacherName, string heldDate, string classType, string organization, string paymentType);
        List<TeacherPaymentListDto> LoadTeacherPaymentList(int start, int length, string orderBy, string orderDir, List<long> authProgramIds, List<long> authBranchIds, string teacherName, string heldDate, string classType, string organization, string program, string session, string branch, string campus, string paymentStatus, string tpin, int searchOn = 1);
        List<TeacherPaymentReport> LoadPaymentReportList(int start, int noOfData, string orderBy, string orderDir, List<long> authBranchIdList, long activityId, DateTime dateFrom, DateTime dateTo, string tpinList, string voucherNo);
        
        #endregion

        #region Others Function
        //long TeacherPaymentListRowCount(List<long> authBranchIds, string teacherName, string heldDate, string classType, string organization, string paymentType);
        
        long TeacherPaymentListRowCount(List<long> authProgramIds, List<long> authBranchIds, string teacherName, string heldDate, string classType, string organization, string program, string session, string branch, string campus, string paymentStatus, string tpin, int searchOn = 1);
        int GetPaymentReportListCount(List<long> authBranchIdList, long activityId, DateTime dateFrom, DateTime dateTo, string tpinList, string voucherNo);

        #endregion
        
    }

    public class TeacherPaymentDao : BaseDao<TeacherPayment, long>, ITeacherPaymentDao
    {
        #region Properties & Object & Initialization
        //private ISession _session;

        //public ISession Session
        //{
        //    get { return _session; }
        //    set { _session = value; }
        //}

        #endregion

        #region Operational Function

        //public void SaveTeacherPayment()
        //{
        //    // Your Code Here
        //}

        #endregion

        #region Single Instances Loading Function

        public TeacherPayment GetTeacherPaymentByBranchCodeAndYear2Digit(string branchYearCode)
        {
            var criteria = Session.CreateCriteria<TeacherPayment>();
            criteria.Add(Restrictions.Eq("Status", TeacherPayment.EntityStatus.Active));
            criteria.Add(Restrictions.Like("VoucherNo", branchYearCode, MatchMode.Start));

            TeacherPayment teacherPayment = criteria.AddOrder(Order.Desc("Id")).SetMaxResults(1).UniqueResult<TeacherPayment>();
            return teacherPayment;
        }

        public TeacherPayment GetTeacherPaymentByVoucherNo(string voucherNo)
        {
            ICriteria criteria = Session.CreateCriteria<TeacherPayment>();
            criteria.Add(Restrictions.Eq("Status", TeacherPayment.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("VoucherNo", voucherNo));

            TeacherPayment teacherPayment = criteria.UniqueResult<TeacherPayment>();
            return teacherPayment;
        }

        #endregion

        #region List Loading Function

        public List<TeacherPaymentListDto> LoadTeacherPaymentList(int start, int length, string orderBy, string orderDir, List<long> authProgramIds, List<long> authBranchIds, string teacherName, string heldDate, string classType, string organization, string program, string session, string branch, string campus, string paymentStatus, string tpin, int searchOn = 1)
        {
            var query = "DECLARE @rowsperpage INT DECLARE @start INT SET @start = " + (start + 1) + " SET @rowsperpage = " + length + " select * from ( " + GetTeacherPaymentListQuery(authProgramIds, authBranchIds, teacherName, heldDate, classType, organization, program,session,branch,campus,paymentStatus, tpin, searchOn, false);
            query += " ) as pagination";

            if (length > 0)
            {
                query += " where  pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeacherPaymentListDto>());
            return iQuery.List<TeacherPaymentListDto>().ToList();
        }

        public List<TeacherPaymentReport> LoadPaymentReportList(int start, int noOfData, string orderBy, string orderDir, List<long> authBranchIdList, long activityId, DateTime dateFrom, DateTime dateTo, string tpinList, string voucherNo)
        {
            string query = GetPaymentReportListQuery(authBranchIdList, activityId, dateFrom, dateTo, tpinList, voucherNo);
            query = query + " Order By " + orderBy + " " + orderDir + " ";

            if (noOfData > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + noOfData + " ROWS ONLY";
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeacherPaymentReport>());
            List<TeacherPaymentReport> list = iQuery.List<TeacherPaymentReport>().ToList();
            return list;
        }

        #endregion

        #region Others Function

        public long TeacherPaymentListRowCount(List<long> authProgramIds, List<long> authBranchIds, string teacherName, string heldDate, string classType, string organization, string program, string session, string branch, string campus, string paymentStatus, string tpin, int searchOn = 1)
        {
            var query = GetTeacherPaymentListQuery(authProgramIds,authBranchIds, teacherName, heldDate, classType, organization, program,session,branch,campus,paymentStatus, tpin, searchOn);
            IQuery iQuery = Session.CreateSQLQuery(query);
            return Convert.ToInt64(iQuery.UniqueResult());
        }

        public int GetPaymentReportListCount(List<long> authBranchIdList, long activityId, DateTime dateFrom, DateTime dateTo, string tpinList, string voucherNo)
        {
            string query = GetPaymentReportListQuery(authBranchIdList, activityId, dateFrom, dateTo, tpinList, voucherNo);

            string countQuery = "Select count(*) From ( " + query + " ) as a ";
            IQuery iQuery = Session.CreateSQLQuery(countQuery);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function

        private string GetTeacherPaymentListQuery(List<long> authProgramIds, List<long> authBranchIds, string teacherName, string heldDate, string classType, string organization, string program, string session, string branch, string campus, string paymentStatus, string tpin, int searchOn = 1, bool isCount = true)
        {
            string tpinFilter = "";
            if (!String.IsNullOrEmpty(tpin))
            {
                int teacherPin;
                if (int.TryParse(tpin, out teacherPin))
                {
                    tpinFilter = " and t.Tpin = " + teacherPin.ToString() + " ";   
                }
            }
            string tFilter = "";
            if (!String.IsNullOrEmpty(teacherName))
            {
                tFilter = " and t.NickName LIKE '%" + teacherName + "%'";
            }
            string oFilter = "";
            if (!String.IsNullOrEmpty(organization))
            {
                oFilter = " and o.Id = " + Convert.ToInt64(organization);
            }

            string pFilter = "";
            if (!String.IsNullOrEmpty(program))
            {
                pFilter = " and p.Id = " + Convert.ToInt64(program);
                if (program=="-1")
                {
                    pFilter = " and p.Id IS NULL";
                }
            }
            string sFilter = ""; 
            if (!String.IsNullOrEmpty(session))
            {
                sFilter = " and s.Id = " + Convert.ToInt64(session);
                if (session == "-1")
                {
                    sFilter = " and s.Id IS NULL";
                }
            }
            string brFilter = "";
            if (!String.IsNullOrEmpty(branch))
            {
                brFilter = " and br.Id = " + Convert.ToInt64(branch);
                if (branch == "-1")
                {
                    brFilter = " and br.Id IS NULL";
                }
            }
            string cFilter = "";
            if (!String.IsNullOrEmpty(campus))
            {
                cFilter = " and c.Id = " + Convert.ToInt64(campus);
                if (campus == "-1")
                {
                    cFilter = " and c.Id IS NULL";
                }
            }
            string paymentFilter = "";
            if (!String.IsNullOrEmpty(paymentStatus))
            {
                paymentFilter = " and aa.PaymentStatus = " + Convert.ToInt64(paymentStatus);
               
            }
            string subQuery = GetSubQueryString(authProgramIds, authBranchIds, classType, heldDate, "", searchOn);
            string query = "";
            query = isCount ? "select count(aa.Id) as total from (" : "select row_number() OVER (ORDER BY t.Tpin) AS RowNum, aa.Id,o.ShortName as Organization, o.Id as OrganizationId, CONCAT(t.NickName,' (',RIGHT(CONVERT(VARCHAR(10), t.HscPassingYear), 2),')',CASE WHEN FullName is NULL THEN '' ELSE ' '+FullName END) as Teacher, t.Id as TeacherId, t.Tpin as TeacherTpin,  aa.[Description], aa.Amount,aa.[type] as ActivityType,aa.HeldDate, br.Name AS Branch, aa.PaidBy, aa.PaymentDate as PaidDate, aa.PaymentStatus, aa.Quantity," +
                                                                      " p.Name AS Program " +
                                                                      " ,s.Name AS SessionName " +
                                                                      " ,c.Name AS Campus, coalesce(aa.ClassType,'') as ClassType from (";
            query = query + subQuery;
            query = query + ") as aa left join Organization as o on aa.OrganizationId = o.Id left join Teacher as t on aa.TeacherId = t.Id left join Branch as br on br.Id = aa.branchId " +
                    " left join Program as p on p.Id = aa.ProgramId" +
                    " left join [Session] as s on s.Id = aa.SessionId" +
                    " left join Campus as c on c.Id = aa.CampusId " +
                    " where 1=1 " + tpinFilter + tFilter + oFilter + pFilter + sFilter + brFilter + cFilter + paymentFilter;
            return query;
        }

        private string GetSubQueryString(List<long> authProgramIds, List<long> authBranchIds, string classType, string heldDate, string paymentType, int searchOn)
        {
            string q = "";
            string q2 = "";
            string q3 = "";

            string branchFilter = "";
            string branchFilter2 = "";
            string branchFilter3 = "";

            string classProgramFilter = "";
            string scriptProgramFilter = "";
            string materialProgramFilter = "";

            if (!String.IsNullOrEmpty(classType))
            {
                if (classType == "Class")
                {
                    if (!String.IsNullOrEmpty(heldDate))
                    {
                        DateTime dt = DateTime.ParseExact(heldDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        if (searchOn == 1)
                            q += " and tc.HeldDate = '" + dt.ToString() + "' ";
                        else
                            q += " and convert(varchar,tcd.PaymentDate,112) = '" + dt.ToString("yyyyMMdd") + "' ";
                    }
                    if (!String.IsNullOrEmpty(paymentType))
                    {
                        q += " and tcd.PaymentStatus = " + paymentType;
                    }

                    if (authProgramIds.Any())
                    {
                        classProgramFilter = " and ( tc.ProgramId IN(" + string.Join(",", authProgramIds) + ") or tc.ProgramId IS NULL)";
                    }
                    else
                    {
                        classProgramFilter = " and tc.ProgramId IS NULL";
                    }

                    if (authBranchIds.Any())
                    {
                        branchFilter = " and ( tc.HeldBranchId IN(" + string.Join(",", authBranchIds) + ") or tc.HeldBranchId IS NULL)";
                    }
                    else
                    {
                        branchFilter = " and tc.HeldBranchId IS NULL";
                    }

                    return " select tcd.Id,TeacherId, 0 as TeacherTpin,  [Description],[Amount],'Class' as [type], tc.OrganizationId, Quantity, tc.ProgramId,tc.SessionId,tc.HeldCampusId AS CampusId,tc.HeldDate as HeldDate, tc.HeldBranchId as branchId,  tcd.PaidBy, tcd.PaymentDate, tcd.PaymentStatus, tct.Name as ClassType" + " from TeacherClassEntryDetails as tcd,TeacherClassEntry as tc left join TeacherClassType tct on tc.TeacherClassTypeId=tct.Id" +
                            " where tcd.TeacherClassEntryId=tc.Id and tcd.Status=" + TeacherClassEntryDetails.EntityStatus.Active + q + branchFilter + classProgramFilter ;
                }
                else if (classType == "Script Evaluation")
                {
                    if (!String.IsNullOrEmpty(heldDate))
                    {
                        DateTime dt = DateTime.ParseExact(heldDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        if (searchOn == 1)
                            q = " and tse.SubmissionDate = '" + dt.ToString() + "' ";
                        else
                            q = " and convert(varchar,tsed.PaymentDate,112) = '" + dt.ToString("yyyyMMdd") + "' ";

                    }
                    if (!String.IsNullOrEmpty(paymentType))
                    {
                        q += " and tsed.PaymentStatus = " + paymentType;
                    }
                    if (authProgramIds.Any())
                    {
                        scriptProgramFilter = " and ( tse.ProgramId IN(" + string.Join(",", authProgramIds) + ") or tse.ProgramId IS NULL)";
                    }
                    else
                    {
                        scriptProgramFilter = " and tse.ProgramId IS NULL";
                    }
                    if (authBranchIds.Any())
                    {
                        branchFilter = " and ( tsed.ForBranchId IN(" + string.Join(",", authBranchIds) + ") or tsed.ForBranchId IS NULL)";
                    }
                    else
                    {
                        branchFilter = " and tsed.ForBranchId IS NULL";
                    }
                    return
                        " select tsed.Id,TeacherId,  0 as TeacherTpin, [Description],[Amount],'Script Evaluation' as [type],tse.OrganizationId,Quantity, tse.ProgramId,tse.SessionId,'0' AS CampusId, tse.SubmissionDate as HeldDate, tsed.ForBranchId as branchId,  tsed.PaidBy, tsed.PaymentDate,  tsed.PaymentStatus,'' as ClassType" +
                            " from TeacherScEvaluationDetails as tsed,TeacherScEvaluation as tse" +
                            " where tsed.TeacherScEvaluationId=tse.Id and tsed.Status=" + TeacherScriptEvaluationDetails.EntityStatus.Active + q + branchFilter + scriptProgramFilter;
                }
                else if (classType == "Materials Development")
                {
                    if (!String.IsNullOrEmpty(heldDate))
                    {
                        DateTime dt = DateTime.ParseExact(heldDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        if (searchOn == 1)
                            q = " and tam.SubmissionDate = '" + dt.ToString() + "' ";
                        else
                            q = " and convert(varchar,tamd.PaymentDate,112) = '" + dt.ToString("yyyyMMdd") + "' ";
                    }
                    if (!String.IsNullOrEmpty(paymentType))
                    {
                        q += " and tamd.PaymentStatus = " + paymentType;
                    }
                    if (authProgramIds.Any())
                    {
                        materialProgramFilter = " and ( tam.ProgramId IN(" + string.Join(",", authProgramIds) + ") or tam.ProgramId IS NULL)";
                    }
                    else
                    {
                        materialProgramFilter = " and tam.ProgramId IS NULL";
                    }
                    if (authBranchIds.Any())
                    {
                        branchFilter = " and ( tamd.ForBranchId IN(" + string.Join(",", authBranchIds) + ") or tamd.ForBranchId IS NULL)";
                    }
                    else
                    {
                        branchFilter = " and tamd.ForBranchId IS NULL";
                    }
                    return " select tamd.Id,TeacherId, 0 as TeacherTpin, [Description],[Amount],'Materials Development' as [type],tam.OrganizationId,0 AS Quantity, tam.ProgramId,tam.SessionId,'0' AS CampusId, tam.SubmissionDate  as HeldDate,  tamd.ForBranchId as branchId,  tamd.PaidBy, tamd.PaymentDate, tamd.PaymentStatus,''as ClassType" +
                            " from TeacherAcMaterialsDetails as tamd,TeacherAcMaterials as tam" +
                            " where tamd.TeacherAcMaterialsId=tam.Id and tamd.Status=" + TeacherAcademicMaterialsDetails.EntityStatus.Active + q + branchFilter + materialProgramFilter;
                }
            }



            if (!String.IsNullOrEmpty(heldDate))
            {
                DateTime dt = DateTime.ParseExact(heldDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (searchOn == 1)
                {
                    q += " and tc.HeldDate = '" + dt.ToString() + "' ";
                    q2 += " and tse.SubmissionDate = '" + dt.ToString() + "' ";
                    q3 += " and tam.SubmissionDate = '" + dt.ToString() + "' ";
                }
                else
                {
                    q += " and convert(varchar,tcd.PaymentDate,112) = '" + dt.ToString("yyyyMMdd") + "' ";
                    q2 += " and convert(varchar,tsed.PaymentDate,112) = '" + dt.ToString("yyyyMMdd") + "' ";
                    q3 += " and convert(varchar,tamd.PaymentDate,112) = '" + dt.ToString("yyyyMMdd") + "' ";
                }
            }
            if (!String.IsNullOrEmpty(paymentType))
            {
                q += " and tcd.PaymentStatus = " + paymentType;
                q2 += " and tsed.PaymentStatus = " + paymentType;
                q3 += " and tamd.PaymentStatus = " + paymentType;
            }
            if (authBranchIds.Any())
            {
                branchFilter = " and ( tc.HeldBranchId IN(" + string.Join(",", authBranchIds) + ") or tc.HeldBranchId IS NULL)";
                branchFilter2 = " and ( tsed.ForBranchId IN(" + string.Join(",", authBranchIds) + ") or tsed.ForBranchId IS NULL)";
                branchFilter3 = " and ( tamd.ForBranchId IN(" + string.Join(",", authBranchIds) + ") or tamd.ForBranchId IS NULL)";
            }
            else
            {
                branchFilter = " and tc.HeldBranchId IS NULL";
                branchFilter2 = " and tsed.ForBranchId IS NULL";
                branchFilter3 = " and tamd.ForBranchId IS NULL";
            }

            if (authProgramIds.Any())
            {
                classProgramFilter = " and ( tc.ProgramId IN(" + string.Join(",", authProgramIds) + ") or tc.ProgramId IS NULL)";
                scriptProgramFilter = " and ( tse.ProgramId IN(" + string.Join(",", authProgramIds) + ") or tse.ProgramId IS NULL)";
                materialProgramFilter = " and ( tam.ProgramId IN(" + string.Join(",", authProgramIds) + ") or tam.ProgramId IS NULL)";
            }
            else
            {
                classProgramFilter = " and tc.ProgramId IS NULL";
                scriptProgramFilter = " and tse.ProgramId IS NULL";
                materialProgramFilter = " and tam.ProgramId IS NULL";
            }

            return "select * from ( " +
                            " select tcd.Id,TeacherId, [Description],[Amount],'Class' as [type],tc.OrganizationId, Quantity, tc.ProgramId,tc.SessionId,tc.HeldCampusId AS CampusId,tc.HeldDate as HeldDate,  tc.HeldBranchId as branchId,  tcd.PaidBy, tcd.PaymentDate, tcd.PaymentStatus,tct.Name as ClassType" +
                            " from TeacherClassEntryDetails as tcd,TeacherClassEntry as tc left join TeacherClassType tct on tc.TeacherClassTypeId=tct.Id" +
                            " where tcd.TeacherClassEntryId=tc.Id and tcd.Status=" + TeacherClassEntryDetails.EntityStatus.Active + q + branchFilter + classProgramFilter +
                    " ) as a union all  select * from (" +
                            " select tsed.Id,TeacherId, [Description],[Amount],'Script Evaluation' as [type],tse.OrganizationId,Quantity, tse.ProgramId,tse.SessionId,'0' AS CampusId, tse.SubmissionDate as HeldDate,  tsed.ForBranchId as branchId, tsed.PaidBy, tsed.PaymentDate, tsed.PaymentStatus,'' as ClassType" +
                            " from TeacherScEvaluationDetails as tsed,TeacherScEvaluation as tse" +
                            " where tsed.TeacherScEvaluationId=tse.Id and tsed.Status=" + TeacherScriptEvaluationDetails.EntityStatus.Active + q2 + branchFilter2 + scriptProgramFilter +
                    " ) as b union all select * from (" +
                            " select tamd.Id,TeacherId, [Description],[Amount],'Materials Development' as [type],tam.OrganizationId,0 AS Quantity, tam.ProgramId,tam.SessionId,'0' AS CampusId, tam.SubmissionDate  as HeldDate,  tamd.ForBranchId as branchId, tamd.PaidBy, tamd.PaymentDate, tamd.PaymentStatus,'' as ClassType" +
                            " from TeacherAcMaterialsDetails as tamd,TeacherAcMaterials as tam" +
                            " where tamd.TeacherAcMaterialsId=tam.Id and tamd.Status=" + TeacherAcademicMaterialsDetails.EntityStatus.Active + q3 + branchFilter3+ materialProgramFilter +
                    " ) as c";
        }

        private string GetPaymentReportListQuery(List<long> authBranchIdList, long activityId, DateTime dateFrom, DateTime dateTo, string tpinList, string voucherNo)
        {
            string activityInnerJoin = "";
            string branchFilter = "";
            string dateFromFilter = " and tp.PaymentDate >= '" + dateFrom.ToString("yyyy-MM-dd") + " 00:00:00.000' ";
            string dateToFilter = " and tp.PaymentDate <= '" + dateTo.ToString("yyyy-MM-dd") + " 23:59:59.999' ";
            string tpinNameFilter = "";
            string voucherNoFilter = "";

            if (authBranchIdList.Any() && !authBranchIdList.Contains(SelectionType.SelelectAll))
            {
                branchFilter = " and tp.PaidFromBranchId IN(" + string.Join(",", authBranchIdList) + ") ";
            }
            if (!String.IsNullOrEmpty(tpinList))
            {
                tpinNameFilter = " and (CONVERT(varchar(10), t.Tpin) = '" + tpinList + "' or t.NickName like '%" + tpinList + "%') ";
            }
            if (!String.IsNullOrEmpty(voucherNo))
            {
                voucherNoFilter = " and tp.VoucherNo = '" + voucherNo + "' ";
            }

            if (activityId != SelectionType.SelelectAll)
            {
                string activityInnerInerJoin = "";
                if (activityId == (long)TeacherActivityEnum.Class)
                    activityInnerInerJoin = " inner join TeacherClassEntryDetails as tced on tced.Id = tpd.TeacherClassEntryDetailsId ";
                else if (activityId == (long)TeacherActivityEnum.ScriptEvaluation)
                    activityInnerInerJoin = " inner join TeacherScEvaluationDetails as tscd on tscd.Id = tpd.TeacherScriptEvaluationDetailsId ";
                else if (activityId == (long)TeacherActivityEnum.MaterialsDevelopment)
                    activityInnerInerJoin = " inner join TeacherAcMaterialsDetails as tamd on tamd.Id = tpd.TeacherAcademicMaterialsDetailsId ";
                activityInnerJoin = @" inner join (
	                                    select 
	                                    tpd.TeacherPaymentId as TeacherPaymentId
	                                    from TeacherPaymentDetails as tpd
	                                    " + activityInnerInerJoin + @"
	                                    group by tpd.TeacherPaymentId
                                    ) as tactivity on tactivity.TeacherPaymentId = tp.Id";
            }
            string query = @"Select 
                            tp.Id as TeacherPaymentId
                            , tp.TeacherId as TeacherId
                            , tp.PaymentDate as PaidDate
                            , t.Tpin as Tpin
                            , t.NickName+' ('+CAST(t.HscPassingYear as nvarchar(20))+') '+ (CASE WHEN t.FullName is null then '' else t.FullName end)  as TeacherName
                            , b.Name as PaidBranchName
                            , asp.Email as PaidBy
                            , tp.TotalPaidAmount as TotalAmount
                            , tp.VoucherNo as VoucherNo
                            from TeacherPayment as tp
                            inner join Teacher as t on tp.TeacherId = t.Id
                            inner join Branch as b on b.Id = tp.PaidFromBranchId
                            inner join AspNetUsers as asp on asp.Id = tp.PaidBy
                             " + activityInnerJoin + @" 
                            where 1=1
                             " + branchFilter + dateFromFilter + dateToFilter + tpinNameFilter + voucherNoFilter + @"
                            ";

            return query;
        }

        #endregion
       
    }
}
