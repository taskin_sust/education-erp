﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    class TeacherScriptEvaluationDetailsMap : BaseClassMap<TeacherScriptEvaluationDetails, long>
    {
        public TeacherScriptEvaluationDetailsMap()
        {
            Table("TeacherScEvaluationDetails");
            LazyLoad();
            References(x => x.TeacherScriptEvaluation).Column("TeacherScEvaluationId");
            References(x => x.Teacher).Column("TeacherId");
            References(x => x.ForBranch).Column("ForBranchId");
            References(x => x.PaidFromBranch).Column("PaidFromBranchId");
            Map(x => x.Description).Column("Description");
            Map(x => x.Quantity).Column("Quantity");
            Map(x => x.Amount).Column("Amount");
            Map(x => x.PaymentStatus).Column("PaymentStatus");
            Map(x => x.PaymentDate).Column("PaymentDate");
            Map(x => x.PaidBy).Column("PaidBy");
        }
    }
}
