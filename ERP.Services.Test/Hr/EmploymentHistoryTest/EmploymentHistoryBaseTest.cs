﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.EmploymentHistoryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "EmploymentHistory")]
    public class EmploymentHistoryBaseTest : TestBase, IDisposable
    {
        #region Init

        public const int MentorId = 2;
        protected readonly CommonHelper _commonHelper; 
        protected readonly ITeamMemberService _teamMemberService;
        protected readonly IEmploymentHistoryService _employmentHistoryService;
        protected readonly EmploymentHistoryFactory _employmentHistoryFactory;

        public EmploymentHistoryBaseTest()
        {
            _commonHelper = new CommonHelper();
            _employmentHistoryService = new EmploymentHistoryService(Session);
            _employmentHistoryFactory = new EmploymentHistoryFactory(null, Session);
        }

        #endregion
        
        public void Dispose()
        {
            _employmentHistoryFactory.LogCleanUp(_employmentHistoryFactory);
            _employmentHistoryFactory.CleanUp();
            base.Dispose();
        }

    }
}
