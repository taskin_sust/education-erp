﻿$(document).ready(function () {
    
    $(document).on("focus", "input.datepicker", function () {
        var today = new Date();
        var initialDate = today;//today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();
        var startDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + "01";
        var endDate = new Date(today.getFullYear(),(today.getMonth()+1),0);
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: initialDate,
            startDate: startDate,
            endDate: endDate,
            startView: 2,
            minView: 2,
            maxView: 4
        });
    });

    $(document).on("focus", "input.mentorDate", function () {
        var today = new Date();
        // var startDate = today.getFullYear().toString() + "-" + (today.getMonth()) + "- 01";//last Month
        var endDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);//Current Month
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: today,
         //   startDate: startDate,
            endDate: endDate,
            startView: 2,
            minView: 2,
            maxView: 4
        });
    });
    
    $(document).on("focus", "input.historyDate", function () {
        var today = new Date();
        var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: today,
            //startDate: startDate,
            endDate: lastDay,
            startView: 2,
            minView: 2,
            maxView: 4
        });
    });

    $(document).on("input propertychange", ".memberPin", function (event) {
        var $this = $(this);
        var memberPinPrevious = parseInt($(this).val().trim());
        var tr = $(this).parent().parent();
        setTimeout(function () {
            var memberPin = parseInt($this.val().trim());
            if (memberPin != "" && memberPin == memberPinPrevious) {
                if (memberPin > 0) {
                    tr.find('input.employeeId').val(0);
                    tr.find('input.employeeName').val("");
                    tr.find('.insteadOfDate').empty();
                    $.ajax({
                        //url: "/Hr/LeaveApplication/GetTeamMemberInfo",
                        url: $("body").attr("data-project-root") + "Hr/CommonAjaxHr/GetTeamMemberInformation",
                        type: "POST",
                        data: { pin: memberPin, isMentor: true, isInsteadDate:true },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (data) {
                            $.unblockUI();
                            if (data.IsSuccess) {
                                tr.find('input.employeeId').val(data.teamMemberId);
                                tr.find('input.employeeName').val(data.teamMemberName);
                                tr.find('.insteadOfDate').empty("");
                                tr.find('.insteadOfDate').append($('<option>').text("Select Date").attr('value', ""));
                                $.each(data.insteadDaylist, function (i, v) {
                                    tr.find('.insteadOfDate').append($('<option>').text(v.Text).attr('value', v.Text));
                                });
                            } else {
                                tr.find('input.employeeId').val(0);
                                tr.find('input.employeeName').val("");
                                tr.find('insteadOfDate').empty("");
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.unblockUI();
                            bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                        }
                    });
                }
            }
        }, 1000);
    });

    $(document).on("click", ".mentorRecentAdjustmentTab, #showFormRecentAdjustment", function () {
        var dateFrom = $("#dateFrom").val();
        var dateTo = $("#dateTo").val();
        if (dateFrom > dateTo) {

            $.fn.customMessage({
                displayMessage: "From Datetime must be less than or equal to To Datetime",
                displayMessageType: "error",
            });
        }
        else {
            dataTableRender();
        }
    });

    $(document).on("input propertychange", ".dayOffDate,.autoComplete,.insteadOfDate,.reasonForAdjustment", function () {
        var missing = $(this).val() === "";
        $(this).toggleClass('highlight', missing);
    });

    //$("#submitForm").click(function () {
    $(document).on("click", "#submitForm", function () {
        var checkValidRow;
        var e = 0;
        var mentorDayOffAllRowArray = [];
        $("#mentorDayOffAdjustmentTableList tbody tr").each(function () {
            checkValidRow = checkIndividualRowValidity($(this));
            if (checkValidRow > 0) {
                e = e + 1;
            } else {
                var mentorDayOffRowViewModel = {};
                mentorDayOffRowViewModel["MemberId"] = $(this).find(".employeeId").val();
                mentorDayOffRowViewModel["Date"] = $(this).find(".dayOffDate").val();
                mentorDayOffRowViewModel["ReasonForAdjustment"] = $(this).find(".reasonForAdjustment").val();
                mentorDayOffRowViewModel["InsteadOfDate"] = $(this).find(".insteadOfDate").val();

                var checkObject = containsObject(mentorDayOffRowViewModel, mentorDayOffAllRowArray);
                if (checkObject == true) {
                    e = e + 1;
                    notValidRow($(this));
                } else {
                    mentorDayOffAllRowArray.push(mentorDayOffRowViewModel);
                }
            }
        });

        if (e === 0) {
            var dayOffObjectList = JSON.stringify(mentorDayOffAllRowArray);
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Hr/DayOffAdjustment/MentorApproveAllDayOffAdjustment",
                data: { dayOffObjectList: dayOffObjectList },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.fn.customMessage({
                            displayTime: 120000,
                            displayMessage: result.returnSuccess,
                            displayMessageType: "success",
                        });

                        $("#mentorDayOffAdjustmentTableList tbody.mentorDayOffAdjustmentTableNew_table").html("");
                    } else {
                        $.fn.customMessage({
                            displayMessage: result.returnSuccess,
                            displayMessageType: "error",
                        });
                    }

                },
                complete: function () {
                    $.unblockUI();
                },
                error: function () {
                    $.unblockUI();
                }
            });
        }
    });

    var itemCount = 1; //$(".totalCount").val();
    $(document).on("click", ".dynamicRowGenerate", function (event) {
        var html = "";
        console.log(event);
        // dynamically create rows in the table
        html = "<tr id='tr" + itemCount + "' >" +
            "<td><input style='width:118px' type='text' name='dayOffDate" + itemCount + "' id='dayOffDate_" + itemCount + "'  value='' class='dayOffDate datepicker form-control text-box single-line' readonly='readonly' /></td>" +
            "<td><input type='hidden' id='employeeId" + itemCount + "' value='' class='employeeId'/><input type='text' style='width:60px;' name='pinName" + itemCount + "' id='pinName_" + itemCount + "' class='form-control text-box single-line autoComplete memberPin' value='' rowindex='" + itemCount + "'/></td>" +
            "<td><input type='text' name='employeeName" + itemCount + "' id='employeeName_" + itemCount + "'  value='' class='employeeName form-control text-box single-line' readonly='readonly'/></td>" +
            "<td><select style='width:152px' name='insteadOfDate" + itemCount + "' id='insteadOfDate_" + itemCount + "' value='' class='insteadOfDate form-control'><option value=''>Select Date</option></select> </td>" +
            "<td><input style='width: 250px' type='text' name='reasonforad" + itemCount + "' id='reasonforad_" + itemCount + "' class='form-control text-box single-line reasonForAdjustment' placeholder='Reason for Adjustment'> </td>" +
            "<td><span  id='" + itemCount + "'  class='btn btn-primary' style='cursor: pointer'>-</span></td> " +
            "</tr>";
        $(".mentorDayOffAdjustmentTableNew_table").append(html);
        $("#" + itemCount).click(function () {
            var buttonId = $(this).attr("id");
            $("#tr" + buttonId).remove();
        });
        itemCount++;
    });

});

function dataTableRender() {
    var pin = $("#pin").val();
    var dateFrom = $("#dateFrom").val();
    var dateTo = $("#dateTo").val();
    var displayNo = 10;
    if ($('#DisplayNo').val() != "" && /^\d+$/.test($('#DisplayNo').val())) {
        displayNo = parseInt($('#DisplayNo').val());
    } else {
        $('#DisplayNo').val(displayNo);
    }
    $('#MentorDayOffAdjustmentRecentHistory').dataTable({
        destroy: true,
        "processing": true,
        searching: false,
        serverSide: true,
        "scrollX": true,
        "bLengthChange": false,
        "iDisplayLength": displayNo,
        "aoColumnDefs": [{ "bSortable": false, "aTargets": ['_all'] }],
        //stateSave: true,
        order: [[0, "Desc"]],
        ajax: {
            //url: '@Url.Action("MentorDayOffRecentAdjustment", "DayOffAdjustment")',
            url: $("body").attr("data-project-root") + "Hr/DayOffAdjustment/MentorDayOffRecentAdjustment",
            type: 'POST',
            data: function (d) {
                d.pin = pin;
                d.dateFrom = dateFrom;
                d.dateTo = dateTo;
            },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (response) {
                ShowErrorMessage("data load error. Please try again.");
            }
        }
    });
}

function checkIndividualRowValidity(row) {
    var error = 0;
    var date = row.find(".dayOffDate").val();
    var pin = row.find(".autoComplete").val();
    var insteadOfDate = row.find(".insteadOfDate").val();
    var reasonForAdjustment = row.find(".reasonForAdjustment").val();

    if (date == "") {
        error = error + 1;
        row.find(".dayOffDate").addClass("highlight");
    } else {
        row.find(".dayOffDate").removeClass("highlight");
    }
    if (pin == "") {
        error = error + 1;
        row.find(".autoComplete").addClass("highlight");
    } else {
        row.find(".autoComplete").removeClass("highlight");
    }
    if (insteadOfDate == "") {
        error = error + 1;
        row.find(".insteadOfDate").addClass("highlight");
    } else {
        row.find(".insteadOfDate").removeClass("highlight");
    }
    if (reasonForAdjustment == "") {
        error = error + 1;
        row.find(".reasonForAdjustment").addClass("highlight");
    } else {
        row.find(".reasonForAdjustment").removeClass("highlight");
    }
    if (error == 0) {
        row.removeClass("danger");
        row.addClass("info");
    } else {
        row.addClass("danger");
        row.removeClass("info");
    }
    return error;
}

function myFunction() {
    var form = $("#mentorDayOffAdjustmentTableList").closest('form');

    $(form.prop('elements')).each(function () {
        var missing = $(this).val() === "";
        $(this).parent().toggleClass('error', missing);
    });
    return form.find(".error").length == 0;
}

function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (JSON.stringify(list[i]) === JSON.stringify(obj)) {
            //alert("1");
            return true;
        } else if (JSON.stringify(list[i].Date) === JSON.stringify(obj.Date)) {
            //alert("2");
            return true;
        } else if ((JSON.stringify(list[i].InsteadOfDate) === JSON.stringify(obj.InsteadOfDate))) {
            //alert("3");
            return true;
        }
    }
    return false;
}

function notValidRow(row) {
    try {
        row.find(".dayOffDate").addClass("highlight");
        row.find(".reasonForAdjustment").addClass("highlight");
        row.find(".autoComplete").addClass("highlight");
        row.find(".insteadOfDate").addClass("highlight");
        return 1;
    } catch (e) {
        return 0;
    }

}
