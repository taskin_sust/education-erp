﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Org.BouncyCastle.Ocsp;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.Areas.UInventory.Models;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("UInventory")]
    public class RequisitionGoodsIssueController : Controller
    {

        #region logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services & Initialization

        private readonly ICommonHelper _commonHelper;
        public readonly IOrganizationService _organizationService;
        public readonly IItemGroupService _itemGroupService;
        public readonly IItemService _itemService;
        public readonly IProgramSessionItemService _itemProgramSessionService;
        public readonly IBranchService _branchService;
        public readonly IRequisitionService _requisitionService;
        public readonly ISessionService _sessionService;
        public readonly IProgramService _programService;
        public readonly IGoodsIssueService _goodsIssueService;
        public readonly ICurrentStockSummaryService _currentStockSummaryService;


        public RequisitionGoodsIssueController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _organizationService = new OrganizationService(session);
                _branchService = new BranchService(session);
                _itemGroupService = new ItemGroupService(session);
                _itemService = new ItemService(session);
                _itemProgramSessionService = new ProgramSessionItemService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _requisitionService = new RequisitionService(session);
                _goodsIssueService = new GoodsIssueService(session);
                _currentStockSummaryService = new CurrentStockSummaryService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        #endregion

        #region Details/View

        [HttpGet]
        public ActionResult GoodsIssue(long id, string message = "", string type = "")
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                if (type.Equals("s"))
                {
                    ViewBag.SuccessMessage = message;
                }
                else if (type.Equals("i"))
                {
                    ViewBag.InfoMessage = message;
                }
                else if (type.Equals("e"))
                {
                    ViewBag.ErrorMessage = message;
                }
            }
            try
            {
                if (id <= 0)
                {
                    return HttpNotFound();
                }
                var gIObj = _goodsIssueService.LoadById(id);
                if (gIObj == null)
                    return RedirectToAction("RequisitionGoodsIssue", "Requisition", new { message = WebHelper.CommonErrorMessage, type = "e" });
                var viewModel = InitializeGoodsIssueDetailsViewModel(gIObj);
                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("RequisitionGoodsIssue", "Requisition", new { message = WebHelper.CommonErrorMessage, type = "e" });
            }
        }

        [HttpGet]
        public ActionResult GoodsIssueLog(long id)
        {
            if (id <= 0)
            {
                return HttpNotFound();
            }
            var requisitionObj = _requisitionService.LoadById(id);
            if (requisitionObj == null)
                return RedirectToAction("RequisitionGoodsIssue", "Requisition", new { message = "Requisition Not Found! Please try for another!", type = "e" });
            if (requisitionObj.RequisionStatus == (int)RequisitionStatus.Pending
                || requisitionObj.RequisionStatus == (int)RequisitionStatus.Cancelled)
            {
                string message = "";
                switch (requisitionObj.RequisionStatus)
                {
                    case (int)RequisitionStatus.Pending:
                        message = "Requisition is Pending! Please issue goods first!";
                        break;
                    case (int)RequisitionStatus.Cancelled:
                        message = "Requisition is Canceled! Please try for another requisition!";
                        break;
                }
                return RedirectToAction("RequisitionGoodsIssue", "Requisition", new { message = message, type = "i" });
            }
            if (requisitionObj.GoodsIssueList == null || !requisitionObj.GoodsIssueList.Any())
                return RedirectToAction("RequisitionGoodsIssue", "Requisition", new { message = "Requisition has no Goods Issue Log! Please issue goods first!", type = "e" });
            var vm = InitializeGoodsIssueLogViewModel(requisitionObj);
            return View(vm);
        }

        #endregion

        #region Oparetional function

        [HttpGet]
        public ActionResult RequisitionGoodsIssue(long id, string message = "", string type = "")
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                if (type.Equals("s"))
                {
                    ViewBag.SuccessMessage = message;
                }
                else if (type.Equals("i"))
                {
                    ViewBag.InfoMessage = message;
                }
                else if (type.Equals("e"))
                {
                    ViewBag.ErrorMessage = message;
                }
            }

            try
            {
                if (id <= 0)
                {
                    return HttpNotFound();
                }
                var requisitionObj = _requisitionService.LoadById(id);
                if (requisitionObj == null)
                    return RedirectToAction("RequisitionGoodsIssue", "Requisition", new { message = "Requisition Not Found! Please try for another!", type = "i" });
                if (requisitionObj.RequisionStatus > (int)RequisitionStatus.Partially)
                {
                    return RedirectToAction("RequisitionGoodsIssue", "Requisition", new { message = "Requisition Completed or Cancel! Please try for another!", type = "i" });
                }
                var viewModel = InitializeGoodsIssueViewModel(requisitionObj);
                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("RequisitionGoodsIssue", "Requisition", new { message = ex.Message, type = "e" });
            }
        }

        [HttpPost]
        public ActionResult RequisitionGoodsIssue(GoodsIssueForm model)
        {
            if (model == null)
            {
                return RedirectToAction("RequisitionGoodsIssue", "Requisition", new { message = "Goods Issue Form Not Found! Please try again!", type = "e" });
            }

            try
            {
                if (model.RequisitionItemList == null || model.RequisitionItemList.Count <= 0)
                {
                    return RedirectToAction("RequisitionGoodsIssue", new { id = model.RequisitionId, message = "Goods Issues Item list not found! Please try again!", type = "e" });
                }

                var requisitionObj = _requisitionService.LoadById(model.RequisitionId);
                if (requisitionObj == null)
                {
                    return RedirectToAction("RequisitionGoodsIssue", "Requisition", new { id = model.RequisitionId, message = "Requisition Not Found! Please try for another!", type = "i" });
                }

                var entityModel = new GoodsIssue()
                                        {
                                            Branch = requisitionObj.Branch,
                                            Requisition = requisitionObj,
                                            GoodsIssueNo = _goodsIssueService.GetGoodsIssueNo(requisitionObj.Branch
                                                                                                    .Organization.Branches
                                                                                                    .Where(x => x.Status == Branch.EntityStatus.Active)
                                                                                                    .Single(x => x.IsCorporate)),
                                            Remarks = model.Remarks
                                        };
                bool itemIssueDone = true;

                var itemList = new List<long>();
                var quantityList = new List<int>();
                var stockValueList = new List<int>();

                bool validQuantityList = true;
                string errorText = "";


                if (model.RequisitionItemList.Any(x => Convert.ToInt32(x.CurrentIssuedQuantity)
                    > Convert.ToInt32(x.RequiredQuantityValue) + Convert.ToInt32(x.PreviousIssueQuantityValue)))
                {
                    validQuantityList = false;
                    errorText = "One or more item(s) issued quantity is invalid! Please try with valid input!";
                }
                else
                {
                    foreach (var row in model.RequisitionItemList)
                    {
                        if (!row.IsIssueAble || row.CurrentIssuedQuantity == null || Convert.ToInt32(row.CurrentIssuedQuantity) <= 0)
                        {
                            continue;
                        }

                        int issuedQuantity = Convert.ToInt32(row.CurrentIssuedQuantity);
                        if (itemList.Contains(row.ItemId))
                        {
                            var i = itemList.IndexOf(row.ItemId);
                            var q = quantityList[i] + issuedQuantity;
                            quantityList[i] = q;
                        }
                        else
                        {
                            itemList.Add(row.ItemId);
                            quantityList.Add(issuedQuantity);
                            stockValueList.Add(row.StockQuantityValue);
                        }

                        var gIrow = new GoodsIssueDetails();
                        int purpose = row.PurposeValue != null ? Convert.ToInt32(row.PurposeValue) : 0;
                        Program program = row.ProgramId > 0 ? _programService.GetProgram(row.ProgramId) : null;
                        Session session = row.SessionId > 0 ? _sessionService.LoadById(row.SessionId) : null; ;

                        gIrow.Purpose = purpose;
                        gIrow.Program = program;
                        gIrow.Session = session;
                        gIrow.Item = _itemService.LoadById(row.ItemId);
                        gIrow.IssuedQuantity = issuedQuantity;
                        gIrow.GoodsIssue = entityModel;
                        if (itemIssueDone && row.RequiredQuantityValue !=
                            row.PreviousIssueQuantityValue + Convert.ToInt32(row.CurrentIssuedQuantity))
                        {
                            itemIssueDone = false;
                        }
                        gIrow.Status = RequisitionDetails.EntityStatus.Active;
                        gIrow.CreateBy = _requisitionService.GetCurrentUserId();
                        gIrow.ModifyBy = _requisitionService.GetCurrentUserId();
                        gIrow.CreationDate = DateTime.Now;
                        gIrow.ModificationDate = DateTime.Now;
                        entityModel.GoodsIssueDetails.Add(gIrow);

                    }
                }
                if (quantityList.Count > 0)
                {
                    for (var i = 0; i < quantityList.Count; i++)
                    {
                        int rq = quantityList[i];
                        int sq = stockValueList[i];
                        if (rq > sq)
                        {
                            validQuantityList = false;
                            errorText = "One or more item(s) issued quantity value is higher then stock quantity! Please try with valid input!";
                            break;
                        }
                    }
                    if (!validQuantityList)
                    {
                        var issueList = model.RequisitionItemList.Select(r => r.CurrentIssuedQuantity).ToList();
                        var vm = InitializeGoodsIssueViewModel(requisitionObj, issueList);
                        ViewBag.ErrorMessage = errorText;
                        return View(vm);
                    }
                    requisitionObj.RequisionStatus = itemIssueDone
                            ? (int)RequisitionStatus.Completed
                            : (int)RequisitionStatus.Partially;

                    var isSuccess = _goodsIssueService.AuthorizeSaveOrUpdateRequisitionGoodsIssue((List<UserMenu>)ViewBag.UserMenu, entityModel);
                    if (isSuccess)
                        return RedirectToAction("GoodsIssue", new { id = entityModel.Id, message = "Goods Successfully Issued!", type = "s" });
                }
                //Prepare view for go back
                var qq = model.RequisitionItemList.Select(r => r.CurrentIssuedQuantity).ToList();
                var viewModel = InitializeGoodsIssueViewModel(requisitionObj, qq);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return View(viewModel);
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return RedirectToAction("RequisitionGoodsIssue", new { id = model.RequisitionId, message = WebHelper.CommonErrorMessage, type = "e" });
        }

        #endregion

        #region Ajax Functions

        public JsonResult IsIssuedQuantityValid([ModelBinder(typeof(JsonGoodsIssueModelBinder))] RequisitionItemList model)
        {
            if (Request.IsAjaxRequest())
            {
                bool rtn = true;
                try
                {
                    int issuedQuantity = Convert.ToInt32(model.CurrentIssuedQuantity);
                    int previousIssued = model.PreviousIssueQuantityValue;
                    int requiredQuantity = model.RequiredQuantityValue;
                    int stockQuanity = model.StockQuantityValue;
                    if (stockQuanity <= 0 || issuedQuantity <= 0 || stockQuanity < issuedQuantity ||
                        requiredQuantity < issuedQuantity || issuedQuantity > (requiredQuantity - previousIssued))
                    {
                        rtn = false;
                    }

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    rtn = false;
                }
                return Json(rtn);
            }
            return Json(false);
        }

        #endregion

        #region Helper Function

        private GoodsIssueForm InitializeGoodsIssueViewModel(Requisition requisitionObj, List<string> qq = null)
        {
            var viewModel = new GoodsIssueForm();
            viewModel.RequisitionId = requisitionObj.Id;
            viewModel.Organization = requisitionObj.Branch.Organization.ShortName;
            viewModel.Branch = requisitionObj.Branch.Name;
            var corporateBranch = requisitionObj.Branch.Organization.Branches.Where(x => x.Status == Branch.EntityStatus.Active).Single(x => x.IsCorporate);
            IList<RequisitionItemList> list = new List<RequisitionItemList>();
            int i = 0;
            foreach (var slip in requisitionObj.RequisitionDetails)
            {
                var row = new RequisitionItemList();
                string purpose = "N/A";
                string programSession = "N/A";
                Program program = null;
                Session session = null;
                row.PreviousIssueQuantityValue = 0;
                row.IsIssueAble = true;
                row.ProgramId = 0;
                row.SessionId = 0;

                int stockQuantity = _currentStockSummaryService.GetCurrentStockQuantity((List<UserMenu>)ViewBag.UserMenu,
                                        slip.Item.Id, corporateBranch.Id, slip.Program != null ? slip.Program.Id : (long?)null, slip.Session != null ? slip.Session.Id : (long?)null);

                if (slip.Purpose != null && slip.Purpose > 0)
                {
                    purpose = GetEnumDescription.Description((Purpose)slip.Purpose);
                    row.PurposeValue = slip.Purpose.Value;
                }
                if (slip.Item.ItemType == (int)ItemType.ProgramItem)
                {
                    if (slip.Program == null && slip.Session == null) return new GoodsIssueForm();
                    program = _programService.GetProgram(slip.Program.Id);
                    session = _sessionService.LoadById(slip.Session.Id);
                    programSession = program.ShortName + " " + session.Name;
                    row.ProgramId = program.Id;
                    row.SessionId = session.Id;
                }
                row.Purpose = purpose;
                row.ProgramSession = programSession;
                row.Item = slip.Item.Name;
                row.ItemId = slip.Item.Id;
                row.ItemGroup = slip.Item.ItemGroup.Name;
                row.RequiredQuantity = slip.RequiredQuantity.ToString();
                row.StockQuantity = stockQuantity.ToString();
                if (requisitionObj.RequisionStatus == (int)RequisitionStatus.Partially)
                {
                    var goodsIssueList = requisitionObj.GoodsIssueList.ToList<GoodsIssue>();
                    int count = 0;
                    foreach (var goodsIssue in goodsIssueList)
                    {
                        GoodsIssueDetails itemCount = goodsIssue.GoodsIssueDetails
                                                                .Where(x => x.Item.Id == row.ItemId)
                                                                .Where(x => x.Program == program)
                                                                .Where(x => x.Session == session)
                                                                .SingleOrDefault(x => x.Purpose == row.PurposeValue);
                        count += itemCount != null ? itemCount.IssuedQuantity : 0;
                    }
                    row.PreviousIssueQuantityValue = count;
                }
                if (stockQuantity <= 0 || slip.RequiredQuantity == row.PreviousIssueQuantityValue
                    //|| (slip.RequiredQuantity - row.PreviousIssueQuantityValue) > stockQuantity
                    )
                {
                    row.IsIssueAble = false;
                }
                if (qq != null && qq.Any())
                {
                    row.CurrentIssuedQuantity = qq[i] ?? "";
                }
                row.Id = slip.Id;
                row.StockQuantityValue = stockQuantity;
                row.RequiredQuantityValue = slip.RequiredQuantity != null ? slip.RequiredQuantity.Value : 0;
                row.PreviousIssueQuantity = row.PreviousIssueQuantityValue.ToString();
                list.Add(row);
                i++;
            }
            viewModel.RequisitionItemList = list;
            return viewModel;
        }

        private object InitializeGoodsIssueDetailsViewModel(GoodsIssue gIObj)
        {
            var vm = new GoodsIssueViewModel()
            {
                Id = gIObj.Id,
                Branch = gIObj.Branch.Name,
                Organization = gIObj.Branch.Organization.Name,
                GoodsIssueNo = gIObj.GoodsIssueNo,
                GoodsIssueDate = gIObj.CreationDate.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture),
                Remarks = gIObj.Remarks
            };
            var requisitionObj = _requisitionService.LoadById(gIObj.Requisition.Id);



            var list = new List<GoodsIssueItems>();
            foreach (var gId in gIObj.GoodsIssueDetails)
            {
                var reqDr = requisitionObj.RequisitionDetails.SingleOrDefault(x => x.Item.Id == gId.Item.Id && x.Session
                                                                                   == gId.Session && x.Program == gId.Program &&
                                                                                   x.Purpose == gId.Purpose);
                var row = new GoodsIssueItems();
                row.RequiredQuantity = reqDr.RequiredQuantity.ToString();
                row.CurrentIssuedQuantity = gId.IssuedQuantity.ToString(CultureInfo.InvariantCulture);
                row.Item = gId.Item.Name;
                row.ItemGroup = gId.Item.ItemGroup.Name;
                row.Purpose = GetEnumDescription.Description((Purpose)gId.Purpose);
                row.ProgramSession = gId.Program != null && gId.Session != null ? gId.Program.ShortName.ToString(CultureInfo.InvariantCulture)
                                                                                    + " " + gId.Session.Name.ToString(CultureInfo.InvariantCulture) : "N/A";

                var goodsIssueList = requisitionObj.GoodsIssueList.ToList<GoodsIssue>();

                if (goodsIssueList.Count > 1)
                {
                    int count = 0;
                    goodsIssueList.Remove(gIObj);
                    foreach (var goodsIssue in goodsIssueList)
                    {
                        GoodsIssueDetails gd = goodsIssue.GoodsIssueDetails
                                                                .Where(x => x.Item.Id == gId.Item.Id)
                                                                .Where(x => x.Program == gId.Program)
                                                                .Where(x => x.Session == gId.Session)
                                                                .SingleOrDefault(x => x.Purpose == gId.Purpose);
                        count += gd != null ? gd.IssuedQuantity : 0;
                    }
                    row.PreviousIssueQuantity = count.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    row.PreviousIssueQuantity = "0";
                }
                list.Add(row);
            }
            vm.GoodsIssueItems = list;
            return vm;
        }

        private IList<GoodsIssuesLog> InitializeGoodsIssueLogViewModel(Requisition requisitionObj)
        {
            var list = new List<GoodsIssuesLog>();
            foreach (var gI in requisitionObj.GoodsIssueList)
            {
                foreach (var gId in gI.GoodsIssueDetails)
                {
                    string goodsIssueNo = LinkGenerator.GetDynamicLabelLink("GoodsIssue", "RequisitionGoodsIssue", gI.Id, gI.GoodsIssueNo);
                    string requiredQty = "";
                    foreach (var reqDetails in requisitionObj.RequisitionDetails)
                    {
                        if (reqDetails.Program == gId.Program && reqDetails.Session == gId.Session &&
                            reqDetails.Item == gId.Item && reqDetails.Purpose == gId.Purpose)
                        {
                            requiredQty = reqDetails.RequiredQuantity.ToString();
                            break;
                        }
                    }
                    string purpose = "N/A";
                    string programSession = "N/A";
                    if (gId.Purpose != null && gId.Purpose > 0)
                    {
                        purpose = GetEnumDescription.Description((Purpose)gId.Purpose).ToString();
                    }
                    if (gId.Program != null && gId.Session != null)
                    {
                        programSession = _programService.GetProgram(gId.Program.Id).ShortName + " " +
                                         _sessionService.LoadById(gId.Session.Id).Name;
                    }
                    var row = new GoodsIssuesLog()
                    {
                        GoodsIssueNo = goodsIssueNo,
                        Purpose = purpose,
                        Item = gId.Item.Name,
                        ItemGroup = gId.Item.ItemGroup.Name,
                        ProgramSession = programSession,
                        RequiredQuantity = requiredQty,
                        IssuedDate = gId.CreationDate.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture),
                        IssuedQuantity = gId.IssuedQuantity.ToString(CultureInfo.InvariantCulture)
                    };
                    list.Add(row);
                }
            }
            return list;
        }

        private string GetGoodsIssueNo(Branch branch)
        {
            string name = "";
            try
            {
                var organizationName = branch.Organization.ShortName.Substring(0, 3);
                var branchCode = branch.Code.ToString(CultureInfo.CurrentCulture);

                var lastGoodsIssueNo = _goodsIssueService.GetGoodsIssueNo(branch.Id);
                string goodsIssueNo = "";

                if (!string.IsNullOrWhiteSpace(lastGoodsIssueNo))
                {
                    string newSl = (Convert.ToInt32(lastGoodsIssueNo.Substring(lastGoodsIssueNo.Length - 6, 6)) + 1).ToString(CultureInfo.CurrentCulture);
                    int lengthDiff = 6 - newSl.Length;
                    for (int i = 0; i < lengthDiff; i++)
                    {
                        newSl = "0" + newSl;
                    }
                    goodsIssueNo = newSl;
                }
                else
                {
                    goodsIssueNo = "000001";
                }
                name = organizationName + branchCode + "GI" + goodsIssueNo;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return name;
        }

        #endregion
    }
}