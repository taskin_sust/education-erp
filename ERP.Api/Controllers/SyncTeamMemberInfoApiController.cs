﻿using System.Text;
using System.Web.Script.Serialization;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.Api.Models;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;

namespace UdvashERP.Api.Controllers
{
    public class SyncTeamMemberInfoApiController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AttendanceAPI");

        #endregion

        #region Properties & Object Initialization

        private readonly IAttendanceSynchronizerService _hrAttendanceSynchronizerService;
        private readonly IAttendanceDeviceMemberService _attendanceDeviceMemberService;
        private readonly IAttendanceDeviceRawDataService _attendanceDeviceRawDataService;
        private readonly IAttendanceDeviceErrorPinService _attendanceDeviceErrorPinService;
        private readonly IAttendanceDeviceService _attendanceDeviceService;
        public SyncTeamMemberInfoApiController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _hrAttendanceSynchronizerService = new AttendanceSynchronizerService(session);
            _attendanceDeviceMemberService = new AttendanceDeviceMemberService(session);
            _attendanceDeviceRawDataService = new AttendanceDeviceRawDataService(session);
            _attendanceDeviceErrorPinService = new AttendanceDeviceErrorPinService(session);
            _attendanceDeviceService = new AttendanceDeviceService(session);
        }

        #endregion

        #region Operational Function

        [HttpGet]
        public JsonResult GetCurrentDateTime(string apiKey, string syncKey)
        {
            try
            {
                if (Authenticate(apiKey) > 0)
                    return Json(new JsonResponse(false, "Invalid apiKey"), JsonRequestBehavior.AllowGet);
                var attendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(syncKey);

                if (attendanceSynchronizer == null)
                    return Json(new JsonResponse(false, "Invalid Sync Key"), JsonRequestBehavior.AllowGet);
                _logger.Error(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                return Json(new JsonResponse(true, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new JsonResponse(false, ex), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult GetAttendanceSynchronizer(string apiKey, string syncKey, string hddSerialList = "", string version = "")
        {
            try
            {
                if (Authenticate(apiKey) > 0)
                {
                    throw new InvalidDataException("Invalid Key");
                }

                var attendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizerKey(syncKey);
                DateTime serverTime = DateTime.Now;
                string serverTimeStr = serverTime.ToString("yyyy-MM-dd HH:mm:ss");
                if (attendanceSynchronizer == null || attendanceSynchronizer.AttendanceDevice.Any() == false)
                {
                    return Json(new JsonResponse(false, "No Device Found"), JsonRequestBehavior.AllowGet);
                }

                IList<AttendanceDeviceModel> attendanceDeviceApiList = new List<AttendanceDeviceModel>();
                foreach (var attendanceDevice in attendanceSynchronizer.AttendanceDevice.Where(x => x.Status == 1))
                {
                    IList<AttendanceDeviceMemberDto> attendanceDeviceMembers = _attendanceDeviceMemberService.LoadMemberInfos(attendanceDevice);
                    AttendanceDeviceModel attendanceDeviceApi = new AttendanceDeviceModel();
                    attendanceDeviceApi.Id = attendanceDevice.Id;
                    attendanceDeviceApi.DeviceTypeCode = attendanceDevice.AttendanceDeviceType.Code;
                    attendanceDeviceApi.Name = attendanceDevice.Name;
                    attendanceDeviceApi.DeviceModelNo = attendanceDevice.DeviceModelNo;
                    attendanceDeviceApi.CommunicationType = attendanceDevice.CommunicationType;
                    attendanceDeviceApi.IPAddress = attendanceDevice.IpAddress;
                    attendanceDeviceApi.Port = attendanceDevice.Port != null ? attendanceDevice.Port : default(int);
                    attendanceDeviceApi.CommunicationKey = attendanceDevice.CommunicationKey;
                    attendanceDeviceApi.MachineNo = attendanceDevice.MachineNo;
                    attendanceDeviceApi.IsReset = attendanceDevice.IsReset;
                    attendanceDeviceApi.LastUpdateTime = serverTimeStr;

                    IList<TeamMemberModel> teamMemberApiList = new List<TeamMemberModel>();
                    foreach (var deviceMember in attendanceDeviceMembers)
                    {
                        TeamMemberModel teamMemberApi = new TeamMemberModel();
                        teamMemberApi.Pin = deviceMember.Pin;
                        teamMemberApi.EnrollNo = deviceMember.EnrollNo;
                        teamMemberApi.Name = GetDisplayName(deviceMember, attendanceDevice);
                        teamMemberApi.IsAdmin = deviceMember.Pin == 1 ? true : false;
                        teamMemberApi.SaveToDeviceSuccessed = false;
                        teamMemberApi.Status = deviceMember.Status;
                        teamMemberApi.FingerIndex0 = deviceMember.Index0;
                        teamMemberApi.FingerIndex1 = deviceMember.Index1;
                        teamMemberApi.FingerIndex2 = deviceMember.Index2;
                        teamMemberApi.FingerIndex3 = deviceMember.Index3;
                        teamMemberApi.FingerIndex4 = deviceMember.Index4;
                        teamMemberApi.FingerIndex5 = deviceMember.Index5;
                        teamMemberApi.FingerIndex6 = deviceMember.Index6;
                        teamMemberApi.FingerIndex7 = deviceMember.Index7;
                        teamMemberApi.FingerIndex8 = deviceMember.Index8;
                        teamMemberApi.FingerIndex9 = deviceMember.Index9;
                        if (!String.IsNullOrEmpty(deviceMember.CardNo))
                            teamMemberApi.CardNumber = attendanceDevice.DisableCard == false ? deviceMember.CardNo : new string(deviceMember.CardNo.Reverse().ToArray());
                        teamMemberApiList.Add(teamMemberApi);
                    }
                    attendanceDeviceApi.TeamMembers.AddRange(attendanceDevice.IsReset
                        ? teamMemberApiList.Where(x => x.Status == TeamMember.EntityStatus.Active)
                        : teamMemberApiList);
                    attendanceDeviceApiList.Add(attendanceDeviceApi);
                }

                AttendanceSynchronizerModel attendanceSynchronizerApi = new AttendanceSynchronizerModel();
                attendanceSynchronizerApi.Name = attendanceSynchronizer.Name;
                attendanceSynchronizerApi.DataCallingInterval = attendanceSynchronizer.DataCallingInterval;
                attendanceSynchronizerApi.AttendanceDevices.AddRange(attendanceDeviceApiList.OrderByDescending(x => x.Id));

                return Json(new JsonResponse(true, attendanceSynchronizerApi), "application/json", Encoding.Default, JsonRequestBehavior.AllowGet);
            }
            catch (InvalidDataException ex)
            {
                return Json(new JsonResponse(false, ex.Message), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new JsonResponse(false, ex.Message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UploadAttendanceErrorPins(string apiKey, string syncKey, string attendanceErrorInfoList)
        {
            bool success = false;
            string message = "";
            try
            {
                if (Authenticate(apiKey) > 0)
                    message += " Request not authenticated";
                var attendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(syncKey);
                if (attendanceSynchronizer == null || attendanceSynchronizer.AttendanceDevice.Any() == false)
                    message += "No Device Found";
                if (!String.IsNullOrEmpty(message))
                    return Json(new JsonResponse(success, message));
                if (!String.IsNullOrEmpty(attendanceErrorInfoList))
                {
                    List<AttendanceErrorInfoModel> attendanceErrorInfoModelList = new JavaScriptSerializer().Deserialize<List<AttendanceErrorInfoModel>>(attendanceErrorInfoList);
                    foreach (var model in attendanceErrorInfoModelList)
                    {
                        //save operation 
                        var entitymodel = CastToModel(model);
                        _attendanceDeviceErrorPinService.Save(entitymodel);
                    }
                }
                success = true;
                return Json(new JsonResponse(success, message), JsonRequestBehavior.DenyGet);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new JsonResponse(false, message));
            }

        }

        #endregion

        #region Helper Function

        private AttendanceDeviceErrorPin CastToModel(AttendanceErrorInfoModel model)
        {
            return new AttendanceDeviceErrorPin()
            {
                Pin = model.Pin,
                EnrollNo = model.EnrollNo,
                LogTime = model.LogTime,
                AttendanceDevice = _attendanceDeviceService.GetAttendanceDevice(model.DeviceId),
                CreateBy = Convert.ToInt64(ConfigurationManager.AppSettings["UserId"]),
                ModifyBy = Convert.ToInt64(ConfigurationManager.AppSettings["UserId"])
            };
        }

        private int Authenticate(string key)
        {
            int error = 0;
            var apiKey = ConfigurationManager.AppSettings["ApiKey"];

            if (key != apiKey)
            {
                error = 1;
            }
            return error;
        }

        private string GetDisplayName(AttendanceDeviceMemberDto deviceMember, AttendanceDevice attendanceDevice)
        {
            string returnName = "";

            if (!String.IsNullOrEmpty(deviceMember.FullName))
            {
                returnName = deviceMember.FullName + " (" + deviceMember.Pin + ")";
                if (returnName.Length > attendanceDevice.AttendanceDeviceType.MaxDisplayName)
                {
                    returnName = "";
                    returnName = "PIN:" + deviceMember.Pin.ToString().PadLeft(4, ' ');
                }
                else
                {
                    returnName = deviceMember.FullName + " (" + deviceMember.Pin + ")";
                }
            }
            else
            {
                returnName = "PIN:" + deviceMember.Pin;
            }
            return returnName;
        }

        #endregion

        #region Extension Method

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }


        #endregion

    }
}