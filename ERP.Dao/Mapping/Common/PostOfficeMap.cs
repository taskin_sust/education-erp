using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Common;

namespace UdvashERP.Dao.Mapping.Common {


    public partial class PostOfficeMap : BaseClassMap<Postoffice,long>
    {
        
        public PostOfficeMap() {
			Table("Postoffice");
			LazyLoad();
            Map(x => x.Name).Column("Name");
            Map(x => x.PostCode).Column("PostCode");
            References(x => x.District).Column("DistrictId");
            //References(x => x.Thana).Column("ThanaId");
			HasMany(x => x.PermanentTeamMembers).KeyColumn("PermanentPostofficeId");
			HasMany(x => x.PresentTeamMembers).KeyColumn("PresentPostOfficeId");
        }
    }
}
