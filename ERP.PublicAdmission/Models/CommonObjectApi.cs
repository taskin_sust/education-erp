﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UdvashERP.PublicAdmission.Models
{
    public class CommonObjectApi
    {
        public virtual long Id { get; set; }
        public virtual String Name { get; set; }
    }

    public class JsonResponseViewModel
    {
        public virtual bool IsSuccess { get; set; }
        public virtual String Message { get; set; }
        public virtual List<CommonObjectApi> Data { get; set; }
    }
}