﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ChildrenInfoTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ChildrenInfo")]
    public class ChildrenInfoUpdateTest : ChildrenInfoBaseTest
    {
        [Fact]
        public void Children_NewAndOld_Object_UpdateReturnTrue()
        {
            var teamMemberObj = PersistTeamMember();
            var objFactory = ChildrenInfoFactory.Create();
            var entity = new TeammemberChildrenArray
            {
                Name = objFactory.Object.Name,
                Gender = objFactory.Object.Gender.ToString(),
                Studying = "1",
                Dob = objFactory.Object.Dob.ToString(),
                Status = objFactory.Object.Status.ToString(CultureInfo.CurrentCulture),
                Remarks = objFactory.Object.Remarks
            };
            var arrayList = new List<TeammemberChildrenArray> { entity };
            var ids = ChildrenInfoService.UpdateChildrenInfo(teamMemberObj, arrayList);
            objFactory.Object.Id = ids[0];
            Assert.NotEmpty(ids);
            Assert.NotEqual(ids.Count, 0);
        }
    }
}
