﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Data;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface INomineeInfoDao : IBaseDao<NomineeInfo, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        NomineeInfo GetNomineeInfoByMemberId(long teamMemberId);

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class NomineeInfoDao : BaseDao<NomineeInfo, long>, INomineeInfoDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
        public NomineeInfo GetNomineeInfoByMemberId(long teamMemberId)
        {
            var entity = Session.QueryOver<NomineeInfo>().Where(x => x.TeamMember.Id == teamMemberId && x.Status == NomineeInfo.EntityStatus.Active).List<NomineeInfo>();
            if (entity != null && entity.Count > 0)
            {
                return entity[0];
            }
            return null;
        }
        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function
        
        #endregion

        
    }
}
