﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class TeamMemberTransferController : Controller
    {
        
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _userMenu;
        private ITeamMemberService _teamMemberService;
        private IOrganizationService _organizationService;
        private IMembersLeaveSummaryService _membersLeaveSummaryService;
        private ICampusService _campusService;
        private IDepartmentService _departmentService;
        private IDesignationService _designationService;
        private IShiftService _shiftService;

        public TeamMemberTransferController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                var sessionMedia = NHibernateMediaSessionFactory.OpenSession();
                _teamMemberService = new TeamMemberService(session);
                _organizationService = new OrganizationService(session);
                _membersLeaveSummaryService = new MembersLeaveSummaryService(session);
                _campusService = new CampusService(session);
                _departmentService = new DepartmentService(session);
                _designationService = new DesignationService(session);
                _shiftService = new ShiftService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        #region TeamMember Transfer

        [HttpGet]
        public ActionResult Transfer(string memberPin = "")
        {
            ViewBag.TeamMember = null;
            ViewBag.TeamMemberTransferInfo = null;
            ViewBag.MemberPin = null;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "ShortName");
            ViewBag.DepartmentList = new SelectList(new List<Department>(), "Id", "Name");
            ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.DesignationList = new SelectList(new List<Designation>(), "Id", "Name");
            //ViewBag.EmploymentStatus = new SelectList(new List<MemberEmploymentStatus>(), "Id", "Name");
            //ViewBag.WeekDays = new SelectList(new List<WeekDay>(), "Id", "Name"); 

            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                if (!String.IsNullOrEmpty(memberPin))
                {
                    ViewBag.MemberPin = memberPin;
                    List<long> memberIdList = _teamMemberService.LoadHrAuthorizedTeamMemberId(_userMenu, DateTime.Now.Date, pinList: new List<int>() { Convert.ToInt32(memberPin) });
                    if (memberIdList == null || memberIdList.Count == 0)
                    {
                        throw new DataNotFoundException("Member is not found/authorized.");
                    }
                    int memPin = Convert.ToInt32(memberPin);
                    TeamMember member = _teamMemberService.GetByPin(memPin);
                    if (member == null)
                        throw new DataNotFoundException("Member is not found.");

                    ViewBag.TeamMember = member;
                    TeamMemberTransferViewModel teamMemberTransferInfo = _teamMemberService.GetTeamMemberTransferInfo(member.Id);

                    if (teamMemberTransferInfo == null)
                        throw new DataNotFoundException("TeamMember Info not vaild.");

                    List<TeamMemberTransferLeaveDetailsViewModel> memberCurrentLeave = _membersLeaveSummaryService.LoadMemberLeaveSumarryForTransfer(teamMemberTransferInfo.Id, DateTime.Now.Year).ToList();
                    if (memberCurrentLeave.Any())
                    {
                        teamMemberTransferInfo.LeaveDetails = memberCurrentLeave;
                        //var memberCurrentLeave = new List<LeaveTypeDtoList>();
                        //foreach (var currentLeave in memberCurrentLeave)
                        //{
                        //    teamMemberTransferInfo.LeaveDetails.Add(currentLeave);
                        //}
                    }

                    Organization memberCurrentOrganization = _teamMemberService.GetTeamMemberOrganization(null, member.Id);
                    //List<long> authorizeOrganizationIdList = AuthHelper.LoadOrganizationIdList(_userMenu);
                    //List<Organization> authorizeOrganizationList = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                    //if (authorizeOrganizationList.Any())
                    //{
                    //    authorizeOrganizationList = authorizeOrganizationList.Where(x => x.Id != teamMemberTransferInfo.OrganizationId).ToList();
                    //}
                    //else
                    //{
                    //    throw new UdvashERP.MessageExceptions.InvalidDataException("You have no autorization info.");
                    //}
                    List<Organization> organizationList = _organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList();
                    if (memberCurrentOrganization != null)
                    {
                        organizationList = organizationList.Where(x => x.Id != teamMemberTransferInfo.OrganizationId).ToList();
                    }
                    //List<Organization> organizationList = _organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList();
                    ViewBag.OrganizationList = new SelectList(organizationList, "Id", "ShortName");
                    ViewBag.TeamMemberTransferInfo = teamMemberTransferInfo;
                    var empStatus = from MemberEmploymentStatus mt in Enum.GetValues(typeof(MemberEmploymentStatus))
                                    where (int)mt != 6
                                    select new { Id = (int)mt, Name = mt.ToString() };
                    ViewBag.EmploymentStatus = new SelectList(empStatus, "Id", "Name");
                    var weekDays = from WeekDay ed in Enum.GetValues(typeof(WeekDay))
                                   select new { Id = (int)ed, Name = ed.ToString() };
                    ViewBag.WeekDays = new SelectList(weekDays, "Id", "Name");
                }
            }
            catch (UdvashERP.MessageExceptions.InvalidDataException ex)
            {
                ViewBag.ErrorMEssage = ex.Message;
            }
            catch (DataNotFoundException ex)
            {
                ViewBag.ErrorMEssage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public JsonResult Transfer(TeamMemberTransferViewModel teamMemberTransfer)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            string message = "";
            bool successState = false;
            try
            {
                if (teamMemberTransfer != null)
                {
                    DateTime effectiveTime = Convert.ToDateTime(teamMemberTransfer.EffectiveDate);
                    TeamMember transferingTeamMember = new TeamMember();

                    //basic information
                    transferingTeamMember.Id = teamMemberTransfer.Id;
                    //end basic information

                    #region Employment History
                    List<EmploymentHistory> employmentHistoryList = new List<EmploymentHistory>
                    {
                        new EmploymentHistory
                        {
                            Campus = _campusService.GetCampus(teamMemberTransfer.CampusId),
                            Department = _departmentService.LoadById(teamMemberTransfer.DepartmentId),
                            Designation = _designationService.LoadById(teamMemberTransfer.DesignationId),
                            EmploymentStatus = teamMemberTransfer.EmploymentStatusId,
                            EffectiveDate = effectiveTime
                        }
                    };
                    transferingTeamMember.EmploymentHistory = employmentHistoryList;
                    #endregion

                    #region Offical Information
                     List<MemberOfficialDetail> memberOfficialDetailList = new List<MemberOfficialDetail>();
                    if (!String.IsNullOrEmpty(teamMemberTransfer.CardNumber) ||
                        !String.IsNullOrEmpty(teamMemberTransfer.OfficialContact) ||
                        !String.IsNullOrEmpty(teamMemberTransfer.OfficialEmail))
                    {
                        MemberOfficialDetail memberOfficialDetail = new MemberOfficialDetail
                        {
                            Id = teamMemberTransfer.OfficalDetailsId,
                            Status = teamMemberTransfer.OfficalDetailsStatus,
                            CardNo = teamMemberTransfer.CardNumber,
                            OfficialContact = teamMemberTransfer.OfficialContact,
                            OfficialEmail = teamMemberTransfer.OfficialEmail

                        };
                        memberOfficialDetailList.Add(memberOfficialDetail);
                    }
                   
                    //{
                    //    new MemberOfficialDetail
                    //    {
                    //        Id = teamMemberTransfer.OfficalDetailsId,
                    //        Status = teamMemberTransfer.OfficalDetailsStatus,
                    //        CardNo = teamMemberTransfer.CardNumber,
                    //        OfficialContact = teamMemberTransfer.OfficialContact,
                    //        OfficialEmail = teamMemberTransfer.OfficialEmail
                    //    }
                    //};
                    transferingTeamMember.MemberOfficialDetails = memberOfficialDetailList;
                    #endregion

                    #region Mentor History

                    List<MentorHistory> mentorHistoryList = new List<MentorHistory>();
                    if (teamMemberTransfer.MentorPin > 0)
                    {
                        TeamMember mentor = _teamMemberService.GetByPin(teamMemberTransfer.MentorPin);
                        mentorHistoryList = new List<MentorHistory>
                        {
                            new MentorHistory
                            {
                                Mentor = mentor,
                                Department = _departmentService.LoadById(teamMemberTransfer.DepartmentId),
                                Designation = _designationService.LoadById(teamMemberTransfer.DesignationId),
                                Organization = _organizationService.LoadById(teamMemberTransfer.OrganizationId),
                                EffectiveDate = effectiveTime
                            }
                        };
                    }
                    
                    transferingTeamMember.MentorHistory = mentorHistoryList;

                    #endregion

                    #region Shift Weekend History

                    List<ShiftWeekendHistory> shiftWeekendHistoryList = new List<ShiftWeekendHistory>
                    {
                        new ShiftWeekendHistory
                        {
                            Organization = _organizationService.LoadById(teamMemberTransfer.OrganizationId),
                            Shift = _shiftService.LoadById(teamMemberTransfer.OfficeShiftId),
                            Weekend = teamMemberTransfer.Weekend,
                            EffectiveDate = effectiveTime,
                        }
                    };
                    transferingTeamMember.ShiftWeekendHistory = shiftWeekendHistoryList;

                    #endregion

                    _teamMemberService.TranferTeamMember(_userMenu, transferingTeamMember, effectiveTime);
                    message = "Transfer Successfully.";
                    successState = true;
                }
                else
                {
                    message = "There is no information to transfer team member";
                }
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });

        }

        #endregion

    }
}