using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students {

    public partial class StudentVisitedMap : BaseClassMap<StudentVisited,long>
    {
        public StudentVisitedMap()
        {
            Table("StudentVisited");
			LazyLoad();

            Map(x => x.NickName).Column("NickName");
            Map(x => x.Mobile).Column("Mobile");
            Map(x => x.MobileFather).Column("MobileFather");
            Map(x => x.MobileMother).Column("MobileMother");

            Map(x => x.VersionOfStudy).Column("VersionOfStudy");
            Map(x => x.Gender).Column("Gender");
            Map(x => x.Religion).Column("Religion");
            Map(x => x.Institute).Column("Institute");
            Map(x => x.Class).Column("Class");           
			Map(x => x.Year).Column("Year");

            Map(x => x.Exam).Column("Exam");
            Map(x => x.Gpa).Column("Gpa");
            Map(x => x.MeritPosition).Column("MeritPosition");
            Map(x => x.Section).Column("Section");

            Map(x => x.Profession).Column("Profession");
            Map(x => x.IsProfessionDoctor).Column("IsProfessionDoctor");
            Map(x => x.IsProfessionEngineer).Column("IsProfessionEngineer");
            Map(x => x.IsProfessionOthers).Column("IsProfessionOthers");

            Map(x => x.StudentVisitType).Column("StudentVisitType");
            References(x => x.InterestedOrganization).Column("InterestedOrganizationId").Nullable();
            References(x => x.InterestedBranch).Column("InterestedBranchId").Nullable();
            References(x => x.VisitedBranch).Column("VisitedBranchId").Nullable();

            Map(x => x.IsAdmitted).Column("IsAdmitted");
            References(x => x.AdmittedStudentProgram).Column("AdmittedStudentProgramId").Nullable();   
        }
    }
}
