﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IStudentUniversitySubjectService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<StudentUniversitySubject> LoadActive();
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }

    public class StudentUniversitySubjectService : BaseService, IStudentUniversitySubjectService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly StudentUniversitySubjectDao _studentUniversitySubjectDao;

        public StudentUniversitySubjectService(ISession session)
        {
            Session = session;
            _studentUniversitySubjectDao = new StudentUniversitySubjectDao() { Session = Session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<StudentUniversitySubject> LoadActive()
        {
            try
            {
                return _studentUniversitySubjectDao.LoadAllOk();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
