using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.Dao.Mapping.LogBase;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class LeaveApplicationLogMap : LogBaseMap<LeaveApplicationLog,long> {
        
        public LeaveApplicationLogMap() {
			Table("HR_LeaveApplicationLog");
			LazyLoad();
			//References(x => x.LeaveApplication).Column("LeaveApplicationId");//old
            Map(x => x.LeaveApplicationId);//new
			Map(x => x.DateFrom).Column("DateFrom");
			Map(x => x.DateTo).Column("DateTo");
			Map(x => x.LeaveNote).Column("LeaveNote");
            //References(x => x.ResponsiblePerson).Column("ResponsiblePersonId");//old
            Map(x => x.ResponsiblePersonId);//new
			Map(x => x.LeaveStatus).Column("LeaveStatus");
            //References(x => x.Leave).Column("LeaveId");//old
            Map(x => x.LeaveId);//new
            //References(x => x.TeamMember).Column("TeamMemberId");//old
            Map(x => x.TeamMemberId);//new
            //Map(x => x.Remarks).Column("Remarks");
        }
    }
}
