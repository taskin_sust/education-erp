using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class SpecificationCriteriaOptionFactory : ObjectFactoryBase<SpecificationCriteriaOption>
   {

       private readonly ISpecificationCriteriaOptionService _specificationCriteriaOptionService;

       public SpecificationCriteriaOptionFactory(IObjectFactoryBase caller, ISession session): base(caller, session){

       _specificationCriteriaOptionService = new SpecificationCriteriaOptionService(session);

       }

       #region Create 

       public SpecificationCriteriaOptionFactory Create(){

       Object = null;
       SingleObjectList.Add(Object);
       return this;
       }

       public SpecificationCriteriaOptionFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public SpecificationCriteriaOptionFactory Persist()       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _specificationCriteriaOptionService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x =>{if (x.Id == 0)_specificationCriteriaOptionService.SaveOrUpdate(x);});

          }
          return this;
       }

       public SpecificationCriteriaOptionFactory CreateMore(int num){

       for (int i = 0; i < num; i++){
       var obj = new SpecificationCriteriaOption(){};
       ObjectList.Add(obj);}
       return this;}

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public SpecificationCriteriaOptionFactory CleanUp()
       {

          DeleteAll();
          return this;
       }

       #endregion

    }
}
