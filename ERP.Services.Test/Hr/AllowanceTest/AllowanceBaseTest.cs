﻿  using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.Hr.AllowanceTest
{
    public class AllowanceBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        public const int MentorId = 2;
        internal readonly CommonHelper CommonHelper;
        internal List<long> IdList = new List<long>();

        protected readonly ISession _session;
        internal readonly string Name = DateTime.Now.ToString("yyyyMMddHHmmss") + "_(ABC CDE)";
        protected readonly AllowanceService _allowanceService;
        protected readonly IAttendanceAdjustmentService _attendanceAdjustmentService;
        protected readonly ITeamMemberService _teamMemberService;
        internal AllowanceFactory _allowanceFactory;
        internal AttendanceAdjustmentFactory _attendanceAdjustmentFactory;

        #endregion

        public AllowanceBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _allowanceService = new AllowanceService(_session);
            _attendanceAdjustmentService = new AttendanceAdjustmentService(_session);
            _teamMemberService = new TeamMemberService(_session);
            _attendanceAdjustmentFactory = new AttendanceAdjustmentFactory(null, _session);
            _allowanceFactory = new AllowanceFactory(null, _session);
        }
        
        public TeamMember CreateTeamMember()
        {
            DateTime startDate = DateTime.Now.AddYears(-3);
            DateTime endDate = DateTime.Now.AddYears(-1);

            #region leave Variable
            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;
            #endregion

            organizationFactory.Create().Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.Object);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.Object);
            maritalInfoFactory.CreateMore(1);

            var mentorObj = _teamMemberService.GetMember(MentorId);

            var mentorEmployment = mentorObj.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit, applicationtype, applicationDays, isEncash, minEncashReserveDays,
                effectiveDate, null, isMale, isFemale, isProbation, isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organizationFactory.Object).Persist();

            var dept = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();
            mentorHistoryFactory.CreateMore(1, mentorObj, dept, designation, organizationFactory.Object
                , mentorEmployment.Department, mentorEmployment.Designation, mentorEmployment.Department.Organization);

            teamMemberFactory.Create()
                .WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                .WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList)
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(mentorHistoryFactory.ObjectList)
                .Persist();
            return teamMemberFactory.Object;
        }

        public void Dispose()
        {
            _attendanceAdjustmentFactory.LogCleanUp(_attendanceAdjustmentFactory);
            _attendanceAdjustmentFactory.CleanUp();
            _allowanceFactory.LogCleanUp(_allowanceFactory);
            _allowanceFactory.CleanUp();
            branchFactory.Cleanup();
            base.Dispose();
        }
    }
}
