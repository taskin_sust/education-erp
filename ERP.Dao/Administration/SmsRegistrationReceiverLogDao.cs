using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.Administration
{
    public interface ISmsRegistrationReceiverLogDao : IBaseDao<SmsRegistrationReceiverLog, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function

        
        #endregion

        #region Others Function

        #endregion

    }

    public class SmsRegistrationReceiverLogDao : BaseDao<SmsRegistrationReceiverLog, long>, ISmsRegistrationReceiverLogDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        

        #endregion

        #region List Loading Function

       
       
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        

        #endregion

        #region Other Function

        
        #endregion
    }
}
