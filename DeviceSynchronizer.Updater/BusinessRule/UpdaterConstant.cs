﻿namespace Synchronizer.Updater.BusinessRule
{
    public class UpdaterConstant
    {
     
        #region Directory Name & Config File

        public static readonly string SynchronizerDirectory = "Synchronizer";
        public static readonly string DownloadDirectory = "Download";
        public static readonly string BackupDirectory = "Backup";
        public static readonly string GetBaseUrl = "attendanceUpdaterBaseUrl";
        public static readonly string GetSecurityKey = "securityKey";

        #endregion

        public static int TryToDownloadSoftwareCount = 2;

    }
}
