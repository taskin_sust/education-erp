﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.MemberBenefitFundOpening
{
    public class MemberBenefitFundOpeningBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initialization

        protected int MentorId = 2;
        private ICommonHelper _commonHelper;
        private OrganizationFactory _organizationFactory;
        private MemberEbfBalanceFactory _memberEbfBalanceFactory;
        internal MemberEbfBalanceService _memberEbfBalanceService;
        internal IEmployeeBenefitsFundSettingService _employeeBenefitsFundSettingService;
        private TeamMemberFactory _teamMemberFactory;
        public CampusRoomFactory _campusRoomFactory;
        public CampusFactory _campusFactory;
        public EmploymentHistoryFactory _employmentHistoryFactory;
        public DepartmentFactory _departmentFactory;
        public DesignationFactory _designationFactory;
        public MemberBenefitFundOpeningBaseTest()
        {
            _commonHelper=new CommonHelper();
            //_organizationFactory=new OrganizationFactory(null,session);
            _memberEbfBalanceFactory=new MemberEbfBalanceFactory(null,Session);
            _memberEbfBalanceService=new MemberEbfBalanceService(Session);
            _employeeBenefitsFundSettingService=new EmployeeBenefitsFundSettingService(Session);
            _teamMemberFactory=new TeamMemberFactory(null,Session);
            _campusRoomFactory=new CampusRoomFactory(null,Session);
            _campusFactory=new CampusFactory(null,Session);
            _employmentHistoryFactory=new EmploymentHistoryFactory(null,Session);
            _departmentFactory=new DepartmentFactory(null,Session);
            _designationFactory=new DesignationFactory(null,Session);
        }

        #endregion

        #region Disposal

        public void Dispose()
        {

            _memberEbfBalanceFactory.CleanUp();
            base.Dispose();
            
            //if (_teamMemberFactory.SingleObjectList != null && _teamMemberFactory.SingleObjectList.Count()>0)
            //{
            //    foreach (var member in _teamMemberFactory.SingleObjectList)
            //    {
            //        var teamMember = new TeamMember();
            //        teamMember = member;
            //        _shiftWeekendHistoryFactory.LogCleanUp(teamMember);
            //        _mentorHistoryFactory.LogCleanUp(teamMember);
            //        _employmentHistoryFactory.LogCleanUp(_employmentHistoryFactory);
            //        _memberOfficialDetailFactory.LogCleanUp(teamMember);
            //    }
                
            //}
            //_teamMemberFactory.TeamMemberChieldCleanup();
            //_teamMemberFactory.Cleanup();
            //_departmentFactory.Cleanup();
            //_designationFactory.Cleanup();
           // _campusRoomFactory.CleanUp();
           // _campusFactory.Cleanup();
            
           //_ShiftFactory.Cleanup();
           // _leaveFactory.Cleanup();
           // _organizationFactory.Cleanup();
        }
        #endregion
    }
}
