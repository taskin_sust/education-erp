﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using FluentNHibernate.Utils;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.UInventory;
using UdvashERP.Services.Helper;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.Administration
{
    public interface IProgramBranchSessionService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<ProgramStudentExamSession> LoadProgramStudentExamSession(long programId, long sessionId);
        IList<ProgramBranchSession> LoadProgramBranchSession(long programId, long? sessionId = null);
        IList<ProgramBranchSession> LoadProgramBranchSessionB(long branchId, long? sessionId = null);//do this marge
        IList<ProgramSessionDto> LoadProgramSession(List<UserMenu> authMenu, long organizationId, List<long> branchIdList = null, List<long> itemIdList = null, bool ? withOutCurrentProgramSession = false,bool isReport=false);
        //for goods transfer report perpose, need to marge with above method 
        IList<ProgramSessionDto> LoadProgramSession(List<UserMenu> authMenu, List<long> organizationId, List<long> branchIdList = null, List<long> itemIdList = null);
        IList<ProgramSessionDto> LoadProgramSession(List<UserMenu> authMenu, long branchIdFrom, long branchIdTo, long itemId);

        IList<ProgramSessionDto> LoadProgramSessionForReport(List<UserMenu> authMenu, long organizationId,
            List<long> branchIdList = null, List<long> itemIdList = null);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function
        List<ResponseMessage> AssignProgram(long organizedId, long programId, long sessionId, long[] selectedBranchList, int?[] selectedAdmissionTypeList, 
            int?[] selectedStudentExamList, out List<long> nonDeletableBranchIds);
        #endregion


    }
    public class ProgramBranchSessionService : BaseService, IProgramBranchSessionService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IBranchDao _branchDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        private readonly IProgramBranchSessionDao _programBranchSessionDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly CommonHelper _commonHelper;
        private readonly IProgramStudentExamSessionDao _programStudentExamSessionDao;
        private readonly IStudentExamDao _studentExamDao;
        private IItemDao _itemDao;

        public ProgramBranchSessionService(ISession session)
        {
            Session = session;
            _branchDao = new BranchDao() { Session = Session };
            _programDao = new ProgramDao() { Session = Session };
            _sessionDao = new SessionDao() { Session = Session };
            _programBranchSessionDao = new ProgramBranchSessionDao() { Session = Session };
            _studentProgramDao = new StudentProgramDao() { Session = Session };
            _commonHelper = new CommonHelper();
            _programStudentExamSessionDao = new ProgramStudentExamSessionDao() { Session = Session };
            _studentExamDao = new StudentExamDao() { Session = Session };
            _itemDao = new ItemDao() { Session = Session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<ProgramStudentExamSession> LoadProgramStudentExamSession(long programId, long sessionId)
        {
            try
            {
                return _programStudentExamSessionDao.LoadProgramStudentExamSession(programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ProgramBranchSession> LoadProgramBranchSession(long programId, long? sessionId = null)
        {
            try
            {
                return _programBranchSessionDao.LoadProgramBranchSession(programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ProgramBranchSession> LoadProgramBranchSessionB(long branchId, long? sessionId = null)
        {
            try
            {
                return _programBranchSessionDao.LoadProgramBranchSessionB(branchId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        public IList<ProgramSessionDto> LoadProgramSession(List<UserMenu> authMenu, long organizationId, List<long> branchIdList = null, List<long> itemIdList = null, bool? withOutCurrentProgramSession = false,bool isReport=false)
        {
           try
            {
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authMenu, (organizationId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(organizationId) : null);
                List<long> authBr = AuthHelper.LoadBranchIdList(authMenu, authOrgList, null, branchIdList);
                var authbranchIdlist = new List<long>();
                foreach (var branchId in authBr)
                {
                    if (branchId > 0)
                    {
                        var branch = _branchDao.LoadById((long)branchId);
                        if (branch.IsCorporate)
                        {
                            authbranchIdlist = null;
                            break;
                        }
                        else
                            authbranchIdlist.Add((long)branchId);
                    }
                }
                var authProIdList = AuthHelper.LoadProgramIdList(authMenu, authOrgList, authbranchIdlist);
                if (!authProIdList.Any())
                    throw new InvalidDataException("Program can't empty");
                return _programBranchSessionDao.LoadProgramSession(authOrgList, authbranchIdlist, authProIdList, itemIdList, withOutCurrentProgramSession,isReport);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        public IList<ProgramSessionDto> LoadProgramSession(List<UserMenu> authMenu, List<long> organizationId, List<long> branchIdList = null, List<long> itemIdList = null)
        {
            try
            {
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authMenu, organizationId );
                List<long> authBr = AuthHelper.LoadBranchIdList(authMenu, authOrgList, null, branchIdList);
                var authbranchIdlist = new List<long>();
                foreach (var branchId in authBr)
                {
                    if (branchId > 0)
                    {
                        var branch = _branchDao.LoadById((long)branchId);
                        if (branch.IsCorporate)
                        {
                            authbranchIdlist = null;
                            break;
                        }
                        else
                            authbranchIdlist.Add((long)branchId);
                    }
                }
                var authProIdList = AuthHelper.LoadProgramIdList(authMenu, authOrgList, authbranchIdlist);

                //if (!authProIdList.Any())
                //    throw new InvalidDataException("Program can't empty");

                return _programBranchSessionDao.LoadProgramSession(authOrgList, authbranchIdlist, authProIdList, itemIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ProgramSessionDto> LoadProgramSession(List<UserMenu> authMenu, long branchIdFrom, long branchIdTo, long itemId)
        {
            try
            {
                long branchFromId = 0;
                long branchToId = 0;

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(authMenu);
                List<long> authBranchFrom = AuthHelper.LoadBranchIdList(authMenu, null, null, _commonHelper.ConvertIdToList(branchIdFrom));
                List<long> authBranchTo = AuthHelper.LoadBranchIdList(authMenu, null, null, _commonHelper.ConvertIdToList(branchIdTo));
                List<long> authProgramList = AuthHelper.LoadProgramIdList(authMenu, authOrganizationIdList, authBranchFrom);
                if (authBranchFrom.Any())
                    branchFromId = authBranchFrom[0];

                if (authBranchTo.Any())
                    branchToId = authBranchTo[0];

                //get item
                Item item = _itemDao.LoadById(itemId);

                IList<ProgramSessionDto> programSessionList=new List<ProgramSessionDto>();
                if (item.ItemType == (int) ItemType.CommonItem)
                {
                    ProgramSessionDto programSessionDto = new ProgramSessionDto
                    {
                        ProgramId = 0,
                        SessionId = 0
                    };
                    programSessionList.Add(programSessionDto);
                }
                else
                {
                    programSessionList=_programBranchSessionDao.LoadProgramSessionDto(authProgramList,branchFromId, branchToId, itemId);
                }

                return programSessionList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<ProgramSessionDto> LoadProgramSessionForReport(List<UserMenu> authMenu, long organizationId, List<long> branchIdList = null, List<long> itemIdList = null)
        {
            try
            {
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authMenu, (organizationId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(organizationId) : null);
                List<long> authBr = AuthHelper.LoadBranchIdList(authMenu, authOrgList, null, branchIdList);
                var authbranchIdlist = new List<long>();
                foreach (var branchId in authBr)
                {
                    if (branchId > 0)
                    {
                        authbranchIdlist.Add((long)branchId);    
                    }
                    }
                var authProIdList = AuthHelper.LoadProgramIdList(authMenu, authOrgList, authbranchIdlist);
                if (!authProIdList.Any())
                    throw new InvalidDataException("Program can't empty");
                return _programBranchSessionDao.LoadProgramSession(authOrgList, authbranchIdlist, authProIdList, itemIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        private void CheckBeforeAssignProgram(long organizationId, long programId, long sessionId)
        {
            if (organizationId <= 0)
            {
                throw new InvalidDataException("organization is invalid");
            }

            if (programId <= 0)
            {
                throw new InvalidDataException("program is invalid");
            }

            if (sessionId <= 0)
            {
                throw new InvalidDataException("session is invalid");
            }

        }
        #endregion

        #region Helper Function
        public List<ResponseMessage> AssignProgram(long organizationId, long programId, long sessionId, long[] selectedBranchList, int?[] selectedAdmissionTypeList, int?[] selectedStudentExamList, out List<long> nonDeletableBranchIds)
        {
            var messageList = new List<ResponseMessage>();
            nonDeletableBranchIds = new List<long>();
            string successfullyAssignedBranches = "";
            string successfullyUnassignedBranches = "";
            ITransaction trans = null;
            try
            {
                CheckBeforeAssignProgram(organizationId, programId, sessionId);

                using (trans = Session.BeginTransaction())
                {
                    if (selectedAdmissionTypeList == null)
                    {
                        var previouslyAssignedBranch = _branchDao.LoadBranchByProgramSession(programId, sessionId).ToList();
                        var previouslyAssignedBranchesWithoutAdmissionType = _branchDao.LoadBranchByProgramSession(programId, sessionId, false).ToList();

                        if (selectedBranchList != null)
                        {

                            if (previouslyAssignedBranchesWithoutAdmissionType.Count > 0)
                            {
                                foreach (var branch in previouslyAssignedBranchesWithoutAdmissionType)
                                {
                                    if (!selectedBranchList.Contains(branch.Id))
                                    {
                                        var branchToDelete =
                                           _programBranchSessionDao
                                               .GetAllProgramBranchSessionByBranchIdSessionIdProgramId(branch.Id,
                                                   sessionId, programId);
                                        if (branchToDelete != null)
                                        {
                                            bool isAlreadyAssignedStudentTothisBranch = _studentProgramDao.IsAssignedStudentToThisBranch(programId, branch.Id, sessionId);
                                            if (isAlreadyAssignedStudentTothisBranch)
                                            {

                                                messageList.Add(new ResponseMessage(errorMessage:
                                                    branch.Name + " has students already. "));
                                                nonDeletableBranchIds.Add(branch.Id);
                                            }
                                            else
                                            {
                                                _programBranchSessionDao.Delete(branchToDelete);
                                                successfullyUnassignedBranches = successfullyUnassignedBranches + branch.Name + ", ";
                                            }
                                        }
                                    }
                                }
                            }
                            if (previouslyAssignedBranch.Count > 0)
                            {
                                foreach (var branch in previouslyAssignedBranch)
                                {
                                    if (selectedBranchList.Contains(branch.Id))
                                    {
                                        var branchToUpdate = _programBranchSessionDao.GetAllProgramBranchSessionByBranchIdSessionIdProgramId(branch.Id, sessionId, programId);
                                        if (branchToUpdate != null)
                                        {
                                            branchToUpdate.IsOffice = false;
                                            branchToUpdate.IsPublic = false;
                                            _programBranchSessionDao.Update(branchToUpdate);
                                        }
                                        successfullyAssignedBranches = successfullyAssignedBranches + branch.Name + ", ";
                                    }
                                }

                            }

                            foreach (var sbl in selectedBranchList)
                            {
                                if (previouslyAssignedBranch.Count > 0)
                                {
                                    var previouslyAssignedBranchIds = previouslyAssignedBranch.Select(x => x.Id).ToArray();
                                    if (!previouslyAssignedBranchIds.Contains(sbl))
                                    {
                                        var branchToUpdate = _programBranchSessionDao.GetAllProgramBranchSessionByBranchIdSessionIdProgramId(sbl, sessionId, programId);
                                        if (branchToUpdate != null)
                                        {
                                            branchToUpdate.IsOffice = false;
                                            branchToUpdate.IsPublic = false;
                                            _programBranchSessionDao.Update(branchToUpdate);
                                        }
                                        else
                                        {
                                            var pbs = new ProgramBranchSession();
                                            pbs.Program = _programDao.LoadById(programId);
                                            pbs.Session = _sessionDao.LoadById(sessionId);
                                            pbs.Branch = _branchDao.LoadById(sbl);
                                            pbs.IsOffice = false;
                                            pbs.IsPublic = false;
                                            _programBranchSessionDao.Save(pbs);
                                            successfullyAssignedBranches = successfullyAssignedBranches + _branchDao.LoadById(sbl).Name + ", ";
                                        }

                                    }
                                }
                                else
                                {
                                    var pbs = new ProgramBranchSession();
                                    pbs.Program = _programDao.LoadById(programId);
                                    pbs.Session = _sessionDao.LoadById(sessionId);
                                    pbs.Branch = _branchDao.LoadById(sbl);
                                    pbs.IsOffice = false;
                                    pbs.IsPublic = false;
                                    _programBranchSessionDao.Save(pbs);
                                    successfullyAssignedBranches = successfullyAssignedBranches + _branchDao.LoadById(sbl).Name + ", ";
                                }
                            }
                        }
                        //if no branch is selected
                        else
                        {
                            foreach (var branch in previouslyAssignedBranch)
                            {

                                bool isAlreadyAssignedStudentTothisBranch = _studentProgramDao.IsAssignedStudentToThisBranch(programId, branch.Id, sessionId);
                                if (isAlreadyAssignedStudentTothisBranch)
                                {

                                    messageList.Add(new ResponseMessage(errorMessage:
                                        branch.Name + " has students already. "));
                                    nonDeletableBranchIds.Add(branch.Id);
                                    continue;
                                }
                                else
                                {
                                    var branchToDelete = _programBranchSessionDao.GetAllProgramBranchSessionByBranchIdSessionIdProgramId(branch.Id, sessionId, programId, null, false);
                                    if (branchToDelete != null)
                                    {
                                        _programBranchSessionDao.Delete(branchToDelete);
                                        successfullyUnassignedBranches = successfullyUnassignedBranches + branch.Name + ", ";
                                    }
                                }


                            }
                        }
                    }
                    else
                    {
                        var deletedBranchIds = new List<long>();
                        var commonBranchIds = new List<long>();
                        var newBranchIds = new List<long>();
                        var previouslyAssignedBranches = _branchDao.LoadBranch(_commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId));

                        #region no branch selected
                        if (selectedBranchList == null)
                        {
                            var previouslyAssignedBranchesByAdmissionType = _branchDao.LoadBranch(_commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId), null, selectedAdmissionTypeList.ToList()).ToList();
                            if (previouslyAssignedBranchesByAdmissionType.Count > 0)
                            {
                                foreach (var pab in previouslyAssignedBranchesByAdmissionType.Select(x => x.Id).ToList())
                                {
                                    if (selectedAdmissionTypeList.Contains(1))
                                    {
                                        bool isAlreadyAssignedStudentTothisBranch =
                                            _studentProgramDao.IsAssignedStudentToThisBranch(programId, pab, sessionId);
                                        if (isAlreadyAssignedStudentTothisBranch)
                                        {

                                            messageList.Add(new ResponseMessage(errorMessage:
                                                previouslyAssignedBranches.Where(x => x.Id == pab)
                                                    .Select(x => x.Name)
                                                    .FirstOrDefault() + " has students already. "));
                                            nonDeletableBranchIds.Add(pab);
                                            continue;

                                        }
                                    }

                                    var branchToDelete =
                                        _programBranchSessionDao.GetAllProgramBranchSessionByBranchIdSessionIdProgramId(
                                            pab, sessionId, programId);
                                    if (branchToDelete != null)
                                    {

                                        if (selectedAdmissionTypeList.Contains(1) && selectedAdmissionTypeList.Contains(2))
                                        {

                                            branchToDelete.IsOffice = false;
                                            branchToDelete.IsPublic = false;
                                            _programBranchSessionDao.Update(branchToDelete);

                                        }
                                        else
                                        {
                                            if (selectedAdmissionTypeList.Contains(1))
                                            {

                                                branchToDelete.IsOffice = false;
                                                _programBranchSessionDao.Update(branchToDelete);

                                            }
                                            if (selectedAdmissionTypeList.Contains(2))
                                            {

                                                branchToDelete.IsPublic = false;
                                                _programBranchSessionDao.Update(branchToDelete);

                                            }
                                            var programBranchSession = _programBranchSessionDao.LoadById(branchToDelete.Id);
                                            if (branchToDelete.IsOffice == false && branchToDelete.IsPublic == false)
                                            {
                                                _programBranchSessionDao.Delete(branchToDelete);
                                            }
                                        }
                                        successfullyUnassignedBranches = successfullyUnassignedBranches +
                                                                         _branchDao.LoadById(pab).Name + ", ";

                                    }

                                }
                            }
                        }
                        #endregion
                        else
                        {
                            if (previouslyAssignedBranches.Count > 0)
                            {
                                var previouslyAssignedBrancheIds = previouslyAssignedBranches.Select(x => x.Id).ToArray();
                                commonBranchIds = previouslyAssignedBrancheIds.Intersect(selectedBranchList).ToList();
                                if (commonBranchIds.Count > 0)
                                {
                                    newBranchIds = selectedBranchList.Except(commonBranchIds).ToList();
                                }
                                else
                                {
                                    newBranchIds = selectedBranchList.ToList();
                                }
                            }
                            else
                            {
                                newBranchIds = selectedBranchList.ToList();
                            }

                            var previouslyAssignedBranchesByAdmissionType = _branchDao.LoadBranch(_commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId), null, selectedAdmissionTypeList.ToList()).ToList();
                            if (previouslyAssignedBranchesByAdmissionType.Count > 0)
                            {
                                deletedBranchIds =
                                    previouslyAssignedBranchesByAdmissionType.Select(x => x.Id)
                                        .ToList().Except(selectedBranchList).ToList();
                            }
                            else
                            {
                                deletedBranchIds = null;
                            }
                            if (deletedBranchIds != null && deletedBranchIds.Count > 0)
                            {

                                foreach (var db in deletedBranchIds)
                                {
                                    bool isAlreadyAssignedStudentTothisBranch = _studentProgramDao.IsAssignedStudentToThisBranch(programId, db, sessionId);
                                    if (isAlreadyAssignedStudentTothisBranch)
                                    {

                                        messageList.Add(new ResponseMessage(errorMessage:
                                                                       previouslyAssignedBranches.Where(x => x.Id == db).Select(x => x.Name).FirstOrDefault() + " has students already. "));
                                        nonDeletableBranchIds.Add(db);
                                        continue;

                                    }
                                    else
                                    {
                                        var branchToDelete = _programBranchSessionDao.GetAllProgramBranchSessionByBranchIdSessionIdProgramId(db, sessionId, programId);
                                        if (branchToDelete != null)
                                        {
                                            if (selectedAdmissionTypeList.Contains(1) && selectedAdmissionTypeList.Contains(2))
                                            {
                                                _programBranchSessionDao.Delete(branchToDelete);

                                            }


                                            else
                                            {
                                                if (selectedAdmissionTypeList.Contains(1))
                                                {

                                                    branchToDelete.IsOffice = false;
                                                    _programBranchSessionDao.Update(branchToDelete);

                                                }
                                                if (selectedAdmissionTypeList.Contains(2))
                                                {

                                                    branchToDelete.IsPublic = false;
                                                    _programBranchSessionDao.Update(branchToDelete);

                                                }
                                                var programBranchSession = _programBranchSessionDao.LoadById(branchToDelete.Id);
                                                if (branchToDelete.IsOffice == false && branchToDelete.IsPublic == false)
                                                {
                                                    _programBranchSessionDao.Delete(branchToDelete);
                                                }
                                            }
                                            successfullyUnassignedBranches = successfullyUnassignedBranches + _branchDao.LoadById(db).Name + ", ";

                                        }
                                    }
                                }

                            }
                            if (commonBranchIds.Count > 0)
                            {
                                foreach (var cb in commonBranchIds)
                                {
                                    var programBranchSessionToUpdate = _programBranchSessionDao.GetAllProgramBranchSessionByBranchIdSessionIdProgramId(cb, sessionId,
                                              programId);
                                    if (programBranchSessionToUpdate != null)
                                    {
                                        if (selectedAdmissionTypeList.Contains(1) &&
                                            selectedAdmissionTypeList.Contains(2))
                                        {
                                            programBranchSessionToUpdate.IsOffice = true;
                                            programBranchSessionToUpdate.IsPublic = true;
                                        }
                                        else
                                        {
                                            if (selectedAdmissionTypeList.Contains(1))
                                            {
                                                programBranchSessionToUpdate.IsOffice = true;
                                                programBranchSessionToUpdate.IsPublic = false;
                                            }

                                            if (selectedAdmissionTypeList.Contains(2))
                                            {
                                                programBranchSessionToUpdate.IsPublic = true;
                                                programBranchSessionToUpdate.IsOffice = false;
                                            }
                                        }

                                        _programBranchSessionDao.Update(programBranchSessionToUpdate);
                                        successfullyAssignedBranches = successfullyAssignedBranches + _branchDao.LoadById(cb).Name + ", ";

                                    }

                                }
                            }


                            if (newBranchIds.Count > 0)
                            {
                                foreach (var nb in newBranchIds)
                                {
                                    var pbs = new ProgramBranchSession();
                                    pbs.Program = _programDao.LoadById(programId);
                                    pbs.Session = _sessionDao.LoadById(sessionId);
                                    pbs.Branch = _branchDao.LoadById(nb);
                                    if (selectedAdmissionTypeList.Contains(1))
                                    {
                                        pbs.IsOffice = true;

                                    }
                                    if (selectedAdmissionTypeList.Contains(2))
                                    {
                                        pbs.IsPublic = true;
                                    }
                                    _programBranchSessionDao.Save(pbs);
                                    successfullyAssignedBranches = successfullyAssignedBranches + _branchDao.LoadById(nb).Name + ", ";
                                }
                            }
                        }
                    }



                    #region Save Boardinfo

                    IList<ProgramStudentExamSession> existingStudentExamList = _programStudentExamSessionDao.LoadProgramStudentExamSession(programId, sessionId);

                    IList<long> nonDeleteAbleStudentExamList = new List<long>();
                    string returnMessage = "";

                    foreach (var exisSE in existingStudentExamList)
                    {
                        bool isExist = false;
                        _programStudentExamSessionDao.Delete(exisSE);
                    }

                    if (selectedStudentExamList != null)
                    {
                        foreach (var se in selectedStudentExamList)
                        {
                            var studentExamObj = _studentExamDao.LoadById(Convert.ToInt64(se));
                            var programObj = _programDao.LoadById(programId);
                            var sessionObj = _sessionDao.LoadById(sessionId);

                            var programStudentExamSession = new ProgramStudentExamSession
                            {
                                StudentExam = studentExamObj,
                                Program = programObj,
                                Session = sessionObj,
                            };
                            _programStudentExamSessionDao.Save(programStudentExamSession);
                        }
                    }

                    #endregion

                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }

            if (!string.IsNullOrEmpty(successfullyAssignedBranches))
            {
                successfullyAssignedBranches = successfullyAssignedBranches.Remove(successfullyAssignedBranches.LastIndexOf(","), 1); ;
                messageList.Add(new ResponseMessage(successMessage: successfullyAssignedBranches +
                                                            " assigned sucessfully"));
            }
            if (!string.IsNullOrEmpty(successfullyUnassignedBranches))
            {
                successfullyUnassignedBranches = successfullyUnassignedBranches.Remove(successfullyUnassignedBranches.LastIndexOf(","), 1); ;
                messageList.Add(new ResponseMessage(successMessage: successfullyUnassignedBranches +
                                                        " unassigned sucessfully"));
            }
            return messageList;
        }


        #endregion



    }
}
