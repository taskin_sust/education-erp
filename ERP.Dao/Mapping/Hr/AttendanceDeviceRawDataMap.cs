using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class AttendanceDeviceRawDataMap : BaseClassMap<AttendanceDeviceRawData,long> {

        public AttendanceDeviceRawDataMap()
        {
			Table("HR_AttendanceDeviceRawData");
			LazyLoad();
            //Map(x => x.Rank).Column("Rank");
            //Map(x => x.Name).Column("Name");
            //Map(x => x.Din).Column("Din");
			Map(x => x.Pin).Column("Pin");
			Map(x => x.IsIn).Column("IsIn");
            Map(x => x.PunchType).Column("PunchType");
			Map(x => x.PunchTime).Column("PunchTime");
            //Map(x => x.SynchonizerKey).Column("SynchonizerKey");
            //Map(x => x.CardNumber).Column("CardNumber");
            Map(x => x.RequestIp).Column("RequestIp");
            //Map(x => x.DeviceCallingInterval).Column("DeviceCallingInterval");
            Map(x => x.Remarks).Column("Remarks");
            References(x => x.AttendanceDevice).Column("AttendanceDeviceId");
        }
    }
}
