﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentPaymentReceiveByCampusMap : BaseClassMap<StudentPaymentReceiveByCampus, long>
    {
        public StudentPaymentReceiveByCampusMap()
        {
            Table("StudentPaymentReceiveByCampus");
            LazyLoad();

            Map(x => x.ReceiveAmount).Column("ReceiveAmount");
            Map(x => x.CashBackAmount).Column("CashBackAmount");
            Map(x => x.NetReceive).Column("NetReceive");
            Map(x => x.CashReceive).Column("CashReceive");
            Map(x => x.BKashReceive).Column("BKashReceive");
            Map(x => x.VoucherReceive).Column("VoucherReceive");
            Map(x => x.IouReceive).Column("IouReceive");
            Map(x => x.ReceiveFromDate).Column("ReceiveFromDate");
            Map(x => x.ReceiveDate).Column("ReceiveDate");
            References(x => x.ReceiveFromCampus).Column("ReceiveFromCampusId");
            References(x => x.ReceiveByCampus).Column("ReceiveByCampusId");
            References(x => x.ReceiveFromUser).Column("ReceiveFromUserId");
            References(x => x.ReceiveByUser).Column("ReceiveByUserId");            
        }
    }
}
