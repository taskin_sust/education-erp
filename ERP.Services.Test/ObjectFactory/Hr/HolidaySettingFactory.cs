﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class HolidaySettingFactory : ObjectFactoryBase<HolidaySetting>
    {
        public IHolidaySettingService _HolidaySettingService;
        protected OrganizationFactory _organizationFactory;
        //protected YearlyHolidayFactory _YearlyHolidayFactory;
        
        public HolidaySettingFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
        {                  
            _HolidaySettingService = new HolidaySettingService(session);
            _organizationFactory = new OrganizationFactory(this, _session);
            //_YearlyHolidayFactory=new YearlyHolidayFactory(this,session);
        }

        #region Create
        public HolidaySettingFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }
        public HolidaySettingFactory WithHolidayType(int holidayType)
        {
            Object.HolidayType = holidayType;
            return this;
        }
        public HolidaySettingFactory WithHolidayRepeatationType(int holidayRepeatationType)
        {
            Object.RepeatationType = holidayRepeatationType;
            return this;
        }
        public HolidaySettingFactory Create()
        {
            Object = new HolidaySetting()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                DateFrom = DateTime.Now.AddDays(2),
                DateTo = DateTime.Now.AddDays(3),
                CreateBy = 1,
                ModifyBy = 1,
                Status = HolidaySetting.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public HolidaySettingFactory WithDateFromAndDateTo(DateTime dateFrom,DateTime dateTo)
        {
            Object.DateFrom = dateFrom;
            Object.DateTo = dateTo;
            return this;
        }

        public HolidaySettingFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }
        
        public HolidaySettingFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public HolidaySettingFactory CreateMore(int numberOfHolidaySetting)
        {
            CreateHolidaySetting(numberOfHolidaySetting);
            return this;
        }

        private List<HolidaySetting> CreateHolidaySetting(int numberOfHolidaySetting)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<HolidaySetting>();
            var orgNamePrefix = Guid.NewGuid().ToString();
            _organizationFactory.Create()
                .WithNameAndRank(orgNamePrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"), 1)
                .Persist();

            for (int i = 0; i < numberOfHolidaySetting; i++)
            {
                var holidaySetting = new HolidaySetting()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = Name + i,
                    Rank = 1,
                    DateFrom = DateTime.Now.AddDays(2),
                    DateTo = DateTime.Now.AddDays(3),
                    HolidayType = (int) HolidayTypeEnum.Gazetted,
                    RepeatationType = (int) HolidayRepeatationType.Once,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = HolidaySetting.EntityStatus.Active
                };
                holidaySetting.Organization = _organizationFactory.Object;
                list.Add(holidaySetting);
            }
            ObjectList.AddRange(list);
            return list;
        }

        public HolidaySettingFactory WithOrganization(string orgName, int rank, string shortName, string contact, decimal basicSalary, decimal houseRent, decimal medicalAllowance, decimal convenyance, int monthlyWorkingDay, decimal deductionPerAbsent)
        {
            _organizationFactory.CreateWith(orgName, rank, shortName, contact, basicSalary, houseRent, medicalAllowance, convenyance, monthlyWorkingDay, deductionPerAbsent);
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        public HolidaySettingFactory Persist()
        {
            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                    _HolidaySettingService.Save(x);
            });

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _HolidaySettingService.Save(item);
                    }
                }
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region CleanUp

        public void DeleteYearlyHoliday(long yearlyHolidayId)
        {
            var tableName = GetEntityTableName(typeof(YearlyHoliday));
            _session.CreateSQLQuery("delete from " + tableName + " where id=" + yearlyHolidayId).ExecuteUpdate();
        }
        public void Cleanup()
        {
            DeleteAll();
            _organizationFactory.Cleanup();
        }
        #endregion
    }
}
