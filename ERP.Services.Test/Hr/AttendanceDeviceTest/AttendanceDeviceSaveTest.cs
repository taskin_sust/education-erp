﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceDeviceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceDevice")]
    public class AttendanceDeviceSaveTest : AttendanceDeviceBaseTest
    {
        #region Basic test

        [Fact]
        public void Should_Save_Successfully()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create().WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object);
            _attendanceDeviceService.Save(_attendanceDeviceFactory.Object);
            Assert.NotNull(_attendanceDeviceFactory.Object.Id);
            Assert.NotEqual(_attendanceDeviceFactory.Object.Id, 0);
        }

        #endregion

        #region Invalid property Check

       [Fact]
        public void Save_Should_Return_InValid_Model_For_EmptyCampus()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object);
            _attendanceDeviceFactory.Object.Campus = null;
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Save(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_EmptySynchronizer()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create().WithCampus(_campusFactory.Object);
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Save(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_EmptyDeviceModelNo()
        {

            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object);
            _attendanceDeviceFactory.Object.DeviceModelNo = null;
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Save(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_Invalid_CommunicationType()
        {

            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object);
            _attendanceDeviceFactory.Object.CommunicationType = 0;
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Save(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_Invalid_MachineNumber()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object);
            _attendanceDeviceFactory.Object.MachineNo = -1;
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Save(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_Invalid_Ip()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object);
            _attendanceDeviceFactory.Object.IpAddress = "";
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Save(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_Invalid_CommunicationKey()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object);
            _attendanceDeviceFactory.Object.CommunicationKey = "";
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Save(_attendanceDeviceFactory.Object));
        }

        #endregion

        #region Business logic test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _attendanceDeviceService.Save(null));
        }

        [Fact]
        public void Save_Should_Return_Model_InValid_ForIPAddress()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object);
            _attendanceDeviceFactory.Object.IpAddress = "";
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Save(_attendanceDeviceFactory.Object));
            _attendanceDeviceFactory.Object.IpAddress = "34543sdfg"; //Please Enter valid IP address
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Save(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void DuplicateEntryException_For_Save()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            var obj1 = _attendanceDeviceFactory.Create()
                  .WithCampus(_campusFactory.Object)
                  .WithAttendanceSyncronizer(_campusFactory.Object).WithDeviceMachineNumber(_deviceMachineNo).Persist().Object;
            var obj2 = _attendanceDeviceFactory.Create()
                 .WithCampus(_campusFactory.Object)
                 .WithAttendanceSyncronizer(obj1.AttendanceSynchronizer).WithDeviceMachineNumber(_deviceMachineNo);
            Assert.Throws<DuplicateEntryException>(() => _attendanceDeviceService.Save(_attendanceDeviceFactory.Object));
        }

        //TODO: need more validation test
        //1. Save_should_fail_without_org_branch_campus_synchronizer
        //2. Save_should_fail_without_synchronizer
        //3. Save_should_fail_without_name_model_ip_port_comkey

        #endregion

        #region Validation Check

        #endregion

    }
}
