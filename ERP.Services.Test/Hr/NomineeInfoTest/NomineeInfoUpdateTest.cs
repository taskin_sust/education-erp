﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.NomineeInfoTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "NomineeInfo")]
    public class NomineeInfoUpdateTest : NomineeInfoBaseTest
    {
        [Fact]
        public void JobExperience_New_Object_UpdateReturnTrue()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            var objFactory = NomineeInfoFactory.Create();
            bool saveNewEntity = NomineeInfoService.UpdateNomineeInformation(teamMemberObj, objFactory.Object);
            Assert.True(saveNewEntity);
        }
        [Fact]
        public void JobExperience_Old_Object_UpdateReturnTrue()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            var objFactory = NomineeInfoFactory.Create();
            NomineeInfoService.UpdateNomineeInformation(teamMemberObj, objFactory.Object);
            var oldEntity = NomineeInfoService.GetTeamMemberNomineeInfo(teamMemberObj.Id);
            oldEntity.Name = "Nominee New Name";
            bool saveOldEntity = NomineeInfoService.UpdateNomineeInformation(teamMemberObj, oldEntity);
            Assert.True(saveOldEntity);
        }

        [Fact]
        public void JobExperience_Update_Member_Null()
        {
            var objFactory = NomineeInfoFactory.Create();
            Exception ex = Assert.Throws<NullObjectException>(() => NomineeInfoService.UpdateNomineeInformation(null, objFactory.Object));
            Assert.Equal(ex.Message, MemberEntityNotFound);
        }
        [Fact]
        public void Nominee_Update_Null_Object()
        {
            var teamMemberObj = PersistTeamMember(MentorId);
            Exception ex = Assert.Throws<NullObjectException>(() => NomineeInfoService.UpdateNomineeInformation(teamMemberObj, null));
            Assert.Equal(ex.Message, NullObjectError);
        }
    }
}
