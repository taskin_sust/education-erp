﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Hr
{
    public interface IAttendanceDeviceErrorPinService : UdvashERP.Services.Base.IBaseService
    {

        void Save(BusinessModel.Entity.Hr.AttendanceDeviceErrorPin entitymodel);
    }

    public class AttendanceDeviceErrorPinService : BaseService, IAttendanceDeviceErrorPinService
    {
        private readonly IAttendanceDeviceErrorPinDao _attendanceDeviceErrorPinDao;
        public AttendanceDeviceErrorPinService(ISession session)
        {
            Session = session;
            _attendanceDeviceErrorPinDao = new AttendanceDeviceErrorPinDao() { Session = session };
        }

        public void Save(BusinessModel.Entity.Hr.AttendanceDeviceErrorPin entitymodel)
        {
            try
            {
                if (entitymodel == null) throw new InvalidDataException("invalid object ");
                _attendanceDeviceErrorPinDao.Save(entitymodel);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
