using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class SubjectMap : BaseClassMap<Subject, long>
    {

        public SubjectMap()
        {
			Table("Subject");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.ShortName).Column("ShortName");
            Map(x => x.Rank);
            Map(x => x.Name);
            HasMany(x => x.CourseSubjects).KeyColumn("SubjectId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentClassAttendences).KeyColumn("SubjectId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.TeacherSubjectPriorities).KeyColumn("SubjectId").Cascade.AllDeleteOrphan().Inverse();
            
            //HasManyToMany(x => x.Programs).Cascade.AllDeleteOrphan().Inverse().Table("SubjectProgram");
        }
    }
}
