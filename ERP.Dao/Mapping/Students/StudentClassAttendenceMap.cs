using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students {


    public partial class StudentClassAttendenceMap : BaseClassMap<StudentClassAttendence, long>
    {
        
        public StudentClassAttendenceMap() {
			Table("StudentClassAttendence");
			LazyLoad();
			Map(x => x.HeldDate);
            Map(x => x.Classstart);
            Map(x => x.Teacherlate);
			//Map(x => x.StudentFeedback);
            //References(x => x.StudentProgram).Column("StudentProgramId");
            //References(x => x.Batch).Column("BatchId");
            References(x => x.Lecture).Column("LectureId");
            References(x => x.Teacher).Column("TeacherId");
            References(x => x.CourseSubject).Column("CourseSubjectId");
            HasMany(x => x.StudentClassAttendanceDetails).KeyColumn("StudentClassAttendanceId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
