﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.HolidayWorkTest
{
    public class HolidayWorkBaseTest : TestBase, IDisposable
    {
        #region Initialization
        
        private const int MentorId = 2;
        protected ISession _session;
        protected readonly IHolidayWorkService _holidayWorkService;
        protected readonly IAttendanceAdjustmentService _attendanceAdjustmentService;
        protected readonly HolidayWorkFactory _holidayWorkFactory;
        protected readonly AttendanceAdjustmentFactory _attendanceAdjustmentFactory;

        #endregion

        public HolidayWorkBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _holidayWorkService = new HolidayWorkService(_session);
            _attendanceAdjustmentService = new AttendanceAdjustmentService(_session);

            _holidayWorkFactory = new HolidayWorkFactory(null, _session);
            _attendanceAdjustmentFactory = new AttendanceAdjustmentFactory(null, _session);

        }

        public TeamMember GetTeamMember()
        {
            DateTime startDate = DateTime.Now.AddYears(-3);
            DateTime endDate = DateTime.Now.AddYears(-1);

            #region leave Variable
            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;
            #endregion

            organizationFactory.Create().Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.Object);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.Object);
            maritalInfoFactory.CreateMore(1);

            var mentorObj = teamMemberService.GetMember(MentorId);

            var mentorEmployment = mentorObj.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit, applicationtype,
                applicationDays, isEncash, minEncashReserveDays, effectiveDate, null, isMale, isFemale, isProbation, isPermanent, isPartTime, isContractual, 
                isIntern, isSingle, isMarried, isWidow, isWidower, isDevorced).WithOrganization(organizationFactory.Object).Persist();

            var dept = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();
            mentorHistoryFactory.CreateMore(1, mentorObj, dept, designation, organizationFactory.Object
                , mentorEmployment.Department, mentorEmployment.Designation, mentorEmployment.Department.Organization);

            teamMemberFactory.Create()
                .WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                .WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList)
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(mentorHistoryFactory.ObjectList)
                .Persist();
            return teamMemberFactory.Object;
        }

        #region Disposal

        public void Dispose()
        {
            _attendanceAdjustmentFactory.LogCleanUp(_attendanceAdjustmentFactory);
            _attendanceAdjustmentFactory.CleanUp();
            _holidayWorkFactory.LogCleanUp(_holidayWorkFactory);
            _holidayWorkFactory.CleanUpFactory();
            base.Dispose();
        }

        #endregion
    }
}
