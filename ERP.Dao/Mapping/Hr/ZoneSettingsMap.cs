using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{
    public partial class ZoneSettingMap : BaseClassMap<ZoneSetting,long> {
        
        public ZoneSettingMap() {
			Table("HR_ZoneSetting");
			LazyLoad();
			References(x => x.Organization).Column("OrganizationId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.ToleranceTime).Column("ToleranceTime");
			Map(x => x.ToleranceDay).Column("ToleranceDay");
			Map(x => x.DeductionMultiplier).Column("DeductionMultiplier");
			HasMany(x => x.AttendanceSummary).KeyColumn("ZoneId");
        }
    }
}
