﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Ionic.Zlib;
using iTextSharp.text.pdf;
using Ionic.Zip;
using log4net;
using Newtonsoft.Json;
using Rotativa;
using Rotativa.Options;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.Areas.Exam.Models;
using UdvashERP.BusinessModel.Dto.Exam;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Exam;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;
using Winnovative;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;
using System.Threading;

namespace UdvashERP.Areas.Exam.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Exam")]
    public class ExamMcqQuestionSetController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("ExamArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly ICourseService _courseService;
        private readonly IOrganizationService _organizationService;
        private readonly ICommonHelper _commonHelper;
        private readonly IExamsService _examsService;
        private List<UserMenu> _userMenu;
        private readonly IExamMcqQuestionService _examMcqQuestionService;
        private readonly IExamMcqQuestionSetService _examMcqQuestionSetService;
        private readonly IUserService _userService;
        public ExamMcqQuestionSetController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _sessionService = new SessionService(session);
                _programService = new ProgramService(session);
                _courseService = new CourseService(session);
                _organizationService = new OrganizationService(session);
                _commonHelper = new CommonHelper();
                _examsService = new ExamsService(session);
                _userMenu = new List<UserMenu>();
                _examMcqQuestionService = new ExamMcqQuestionService(session);
                _examMcqQuestionSetService = new ExamMcqQuestionSetService(session);
                _userService = new UserService(session);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        #endregion

        #region Manage Generate
        
        public ActionResult MangeSetGenerate()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.Program = new SelectList(_programService.LoadAuthorizedProgram(userMenu), "Id", "Name");
                ViewBag.Session = new SelectList(_sessionService.LoadAuthorizedSession(userMenu), "Id", "Name");
                ViewBag.Course = new SelectList(_courseService.LoadCourse());
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }

            return View();
        }

        [HttpPost]
        public ActionResult MangeSetGenerate(int draw, int start, int length, string organization, string program, string session, string course, string uniqueSet,
            string examString)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    int sr = start + 1;
                    string orderBy = "";
                    string orderDir = "";

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                                orderBy = "OrganizationShortName";
                                break;
                            case "1":
                                orderBy = "ProgramShortName";
                                break;
                            case "2":
                                orderBy = "SessionName";
                                break;
                            case "3":
                                orderBy = "ExamShortName";
                                break;
                            case "4":
                                orderBy = "UniqueSet";
                                break;
                            case "5":
                                orderBy = "SetNo";
                                break;
                            case "6":
                                orderBy = "GeneratedBy";
                                break;
                            case "7":
                                orderBy = "OriginalGeneratedDate";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }


                    #endregion

                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                        int recordsTotal = _examMcqQuestionSetService.GetExamMcqQuestionSetRowCount(userMenu,organization, program, session, course, uniqueSet, examString);

                        List<ExamMcqQuestionSetDto> examMcqQuestionSetList = _examMcqQuestionSetService.LoadExamMcqQuestionSet(userMenu, start, length, orderBy, orderDir.ToUpper(), organization, program, session, course, uniqueSet, examString).ToList();

                        int recordsFiltered = recordsTotal;
                        var data = new List<object>();

                        foreach (var c in examMcqQuestionSetList)
                        {
                            //var c = objectSet;
                            var str = new List<string>();
                            str.Add(c.OrganizationShortName);
                            str.Add(c.ProgramShortName);
                            str.Add(c.SessionName);
                            str.Add(c.ExamShortName);
                            str.Add(c.UniqueSet.ToString());
                            str.Add(c.SetNo.ToString());
                            str.Add(c.GeneratedBy);
                            str.Add(c.GeneratedDate);

                            str.Add("<div style='width:100px;'> Ban &nbsp;&nbsp;<a target='_blank' title='View Question' href='" +
                                    Url.Action("PreviewQuestion", "ExamMcqQuestionSet", new { examId = c.ExamId, setId = c.SetNo, version = (int)ExamVersion.Bangla }) + "' " +
                                    "class='glyphicon glyphicon-eye-open'></a>&nbsp;&nbsp;<a target='_blank' title='View Pdf' href='" + Url.Content("~") + "Question/" + c.ExamId + "/" + (int)ExamVersion.Bangla + "/QuestionSet_" + c.SetNo + ".pdf" + "' class='glyphicon glyphicon-book'></a>&nbsp;&nbsp;<a target='_blank' title='Show Correct Answer' href='" +
                                    Url.Action("PreviewQuestion", "ExamMcqQuestionSet", new { examId = c.ExamId, setId = c.SetNo, version = (int)ExamVersion.Bangla, showCorrectAnswer = 1 }) + "' " +
                                    "class='glyphicon glyphicon-ok'></a></div>" +


                                    "<div style='width:100px;'>Eng &nbsp;&nbsp<a target='_blank' title='View Question' href='" +
                                    Url.Action("PreviewQuestion", "ExamMcqQuestionSet", new { examId = c.ExamId, setId = c.SetNo, version = (int)ExamVersion.English }) + "' " +
                                    "class='glyphicon glyphicon-eye-open'></a>&nbsp;&nbsp;<a target='_blank' title='View Pdf' href='" + Url.Content("~") + "Question/" + c.ExamId + "/" + (int)ExamVersion.English + "/QuestionSet_" + c.SetNo + ".pdf" + "' class='glyphicon glyphicon-book'></a>&nbsp;&nbsp;<a target='_blank' title='Show Correct Answer' href='" +
                                    Url.Action("PreviewQuestion", "ExamMcqQuestionSet", new { examId = c.ExamId, setId = c.SetNo, version = (int)ExamVersion.English, showCorrectAnswer = 1 }) + "' " +
                                    "class='glyphicon glyphicon-ok'></a></div>" +

                                    "<a href='#' " +
                                    "class='glyphicon glyphicon-repeat RegenerateMcqQuestionSet' examId='" + c.ExamId + "' uniqueSet='" + c.UniqueSet + "' setId='" + c.SetNo + "'>&nbsp;Regenerate</a>&nbsp;&nbsp;" +
                                    "");
                            data.Add(str);
                        }

                        return Json(new
                        {
                            draw = draw,
                            recordsTotal = recordsTotal,
                            recordsFiltered = recordsFiltered,
                            start = start,
                            length = length,
                            data = data
                        });
                    //}
                    //else
                    //{
                    //    var data = new List<object>();
                    //    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;

                    //    return Json(new
                    //    {
                    //        draw = draw,
                    //        recordsTotal = 0,
                    //        recordsFiltered = 0,
                    //        start = start,
                    //        length = length,
                    //        data = data
                    //    });
                    //}
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        #endregion

        #region RegenerateMcqQuestionSet

        [HttpPost]
        public ActionResult RegenerateMcqQuestionSet(long examId, int uniqueSet, int questionSetId)
        {
            string error = "";
            try
            {
                IList<ExamMcqQuestionSet> examMcqQuestionSetList = _examMcqQuestionSetService.LoadExamMcqQuestionSet(examId, _commonHelper.ConvertIdToList(uniqueSet),null, questionSetId);
                CreateQuestionSetPdfAndCompress(examMcqQuestionSetList, examId);
                return Json(new { returnResult = "Successfully save", IsSuccess = true });
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                error = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { returnList = error, IsSuccess = false });
        }
        
        public ActionResult UniqueSetWiseReGenerate()
        {
            try
            {
                InitializeView();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult UniqueSetWiseReGenerate(string examMcqQuestionSetViewModel)
        {
            string errorMsg = "";
            try
            {
                if (ModelState.IsValid)
                {
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var viewModel = new JavaScriptSerializer().Deserialize<ExamMcqQuestionSetViewModel>(examMcqQuestionSetViewModel);
                    var exam = _examsService.LoadById(viewModel.ExamId);
                    if(exam==null)
                        throw new NullObjectException("Exam is empty");
                    int examQuestionCount = exam.ExamDetails.Where(x => x.ExamType == (int)ExamType.Mcq && x.Status == (int)ExamsDetails.EntityStatus.Active).Sum(x => x.TotalQuestion);
                    if (examQuestionCount <= 0)
                        throw new InvalidDataException("This exam has no total question");
                    List<int> examSetNo = new List<int>();
                    if (viewModel.UniqueSet == null || viewModel.UniqueSet.Contains(SelectionType.SelelectAll))
                    {
                        for (int j = 1; j <= exam.TotalUniqueSet; j++)
                        {
                            if (exam.IsBanglaVersion)
                            {
                                var totalBanglaQuestionList = _examMcqQuestionService.LoadExamMcqQuestion(userMenu,exam.Id, j, (int)ExamVersion.Bangla);
                                if (totalBanglaQuestionList != null && totalBanglaQuestionList.Any() && examQuestionCount == totalBanglaQuestionList.Count())
                                {
                                    examSetNo.Add(j);
                                }
                            }
                            else if (exam.IsEnglishversion)
                            {
                                var totalEnglishQuestion = _examMcqQuestionService.LoadExamMcqQuestion(userMenu,exam.Id, j, (int)ExamVersion.English);
                                if (totalEnglishQuestion != null && totalEnglishQuestion.Any() && examQuestionCount == totalEnglishQuestion.Count())
                                {
                                    examSetNo.Add(j);
                                }
                            }
                        }
                    }
                    else
                    {
                        examSetNo = viewModel.UniqueSet;
                    }
                    IList<ExamMcqQuestionSet> examMcqQuestionSetList = _examMcqQuestionSetService.LoadExamMcqQuestionSet(viewModel.ExamId, examSetNo);

                    if (examMcqQuestionSetList == null || !examMcqQuestionSetList.Any())
                    {
                        throw new NullObjectException("There is no mcq question set for this correspoding exam");
                    }
                    CreateQuestionSetPdfAndCompress(examMcqQuestionSetList, viewModel.ExamId);
                    return Json(new { returnResult = "Successfully Regenerate", IsSuccess = true });
                }
            }
            catch (MessageException ex)
            {
                errorMsg = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                errorMsg = ex.Message;
            }
            catch (NullObjectException ex)
            {
                errorMsg = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                errorMsg = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMsg = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                errorMsg = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { returnResult = errorMsg, IsSuccess = false });
        }

        #endregion

        #region Set Generate

        [HttpGet]
        public ActionResult SetGenerate()
        {
            try
            {
                InitializeView();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }

            return View();
        }

        [HttpPost]
        public ActionResult SetGenerate(string examMcqQuestionSetViewModel)
        {
            InitializeView();
            string errorMsg = "";
            try
            {
                if (ModelState.IsValid)
                {
                    var viewModel = new JavaScriptSerializer().Deserialize<ExamMcqQuestionSetViewModel>(examMcqQuestionSetViewModel);
                    IList<ExamMcqQuestionSet> examMcqQuestionSetList = _examMcqQuestionSetService.SaveOrUpdate(viewModel);
                    CreateQuestionSetPdfAndCompress(examMcqQuestionSetList, viewModel.ExamId);
                    return Json(new { returnResult = "Successfully save", IsSuccess = true });
                }
            }
            catch (MessageException ex)
            {
                errorMsg = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                errorMsg = ex.Message;
            }
            catch (NullObjectException ex)
            {
                errorMsg = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                errorMsg = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMsg = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                errorMsg = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { returnResult = errorMsg, IsSuccess = false });
        }

        private void CreateQuestionSetPdfAndCompress(IList<ExamMcqQuestionSet> examMcqQuestionSetList, long examId)
        {
            if (examMcqQuestionSetList != null && examMcqQuestionSetList.Any())
            {
                string fileDir = AppDomain.CurrentDomain.BaseDirectory + "Question/";
                Directory.CreateDirectory(fileDir + examId);
                var xm = _examsService.LoadById(examId);

                if (xm.IsBanglaVersion)
                    Directory.CreateDirectory(fileDir + examId + "/" + (int)ExamVersion.Bangla);

                if (xm.IsEnglishversion)
                    Directory.CreateDirectory(fileDir + examId + "/" + (int)ExamVersion.English);

                var setWiseList = examMcqQuestionSetList.GroupBy(m => m.SetId,
                    (key, group) => new { Key = key, Group = @group.ToList() });

                var taskList = new List<Task>();

                foreach (var setWiseObjectList in setWiseList)
                {
                    IList<Question> newMcqQuestionSetsOfBangla = new List<Question>();
                    IList<Question> newMcqQuestionSetsOfEnglish = new List<Question>();

                    examMcqQuestionSetList = setWiseObjectList.Group;

                    foreach (var examMcqQuestionSet in examMcqQuestionSetList)
                    {
                        if (xm.IsBanglaVersion)
                        {
                            Question ques = BindQuestionSet(_userMenu, xm.Id, examMcqQuestionSet, (int)ExamVersion.Bangla);
                            newMcqQuestionSetsOfBangla.Add(ques);
                        }

                        if (xm.IsEnglishversion)
                        {
                            Question ques = BindQuestionSet(_userMenu, xm.Id, examMcqQuestionSet, (int)ExamVersion.English);
                            newMcqQuestionSetsOfEnglish.Add(ques);
                        }
                    }

                    var pdfObject = new ExamMcqQuestionSetPdfDto
                    {
                        ExamName = xm.Name,
                        Program = xm.Program.Name,
                        Session = xm.Session.Name,
                        DurationHour = xm.DurationHour,
                        DurationMinute = xm.DurationMinute,
                        Fullmarks = xm.McqFullMarks.ToString()
                    };

                    if (xm.IsBanglaVersion)
                    {
                        pdfObject.QuestionList = newMcqQuestionSetsOfBangla.ToList();
                        var setId = setWiseObjectList.Key;

                        #region Save as Pdf

                        //MakePdf(pdfObject, xm.Id, setId, (int)ExamVersion.Bangla, fileDir);
                        
                        var task = new Task(o =>
                        {
                            var param = o as TaskParams;
                            MakePdf(param.ExamMcqQuestionSetPdfDto, param.ExamId, param.SetId, param.Version, param.FileDir);
                        },
                            new TaskParams()
                            {
                                ExamMcqQuestionSetPdfDto = pdfObject,
                                ExamId = xm.Id,
                                SetId = setId,
                                Version = (int)ExamVersion.Bangla,
                                FileDir = fileDir
                            });

                        taskList.Add(task);

                        #endregion
                    }

                    if (xm.IsEnglishversion)
                    {
                        pdfObject.QuestionList = newMcqQuestionSetsOfEnglish.ToList();
                        var setId = setWiseObjectList.Key;

                        #region Save as Pdf
                                                
                        //MakePdf(pdfObject, xm.Id, setId, (int)ExamVersion.English, fileDir);

                        var task = new Task(o =>
                        {
                            var param = o as TaskParams;
                            MakePdf(param.ExamMcqQuestionSetPdfDto, param.ExamId, param.SetId, param.Version, param.FileDir);
                        },
                            new TaskParams()
                            {
                                ExamMcqQuestionSetPdfDto = pdfObject,
                                ExamId = xm.Id,
                                SetId = setId,
                                Version = (int)ExamVersion.English,
                                FileDir = fileDir
                            });

                        taskList.Add(task);

                        //MakePdf(pdfObject, xm.Id, setId, (int)ExamVersion.English, fileDir);

                        #endregion
                    }
                }

                //Parallel.ForEach(taskList, new ParallelOptions() { MaxDegreeOfParallelism = 200 }, t => t.Start());                
                taskList.ForEach(t => t.Start());
                Task.WaitAll(taskList.ToArray());

                if (xm.IsBanglaVersion)
                    CompressQuestions(examId, xm, fileDir, (int)ExamVersion.Bangla);
                if (xm.IsEnglishversion)
                    CompressQuestions(examId, xm, fileDir, (int)ExamVersion.English);
            }
        }

        private static void CompressQuestions(long examId, Exams xm, string fileDir, int version)
        {
            string zipFileName = "Question_bn.zip";
            if (version == (int)ExamVersion.English) zipFileName = "Question_en.zip";

            using (var zip = new ZipFile())
            {
                System.IO.File.Delete(fileDir + examId + "/" + version + "/" + zipFileName);
                zip.AddDirectory(fileDir + examId + "/" + version + "/");
                zip.CompressionLevel = CompressionLevel.Level5;
                zip.CompressionMethod = CompressionMethod.BZip2;
                zip.Save(fileDir + examId + "/" + version + "/" + zipFileName);
            }
        }

        #endregion

        #region Preview Question

        [AllowAnonymous]
        public ActionResult PreviewQuestion(long examId, int setId, int version, double textFontSize = 5, double eqFontSize = 6, int targetPage = 2, int showCorrectAnswer = 0)
        {
            ViewBag.textFontSize = textFontSize + "mm";
            ViewBag.textFontSizeInt = textFontSize;
            ViewBag.eqFontSize = eqFontSize + "mm";
            ViewBag.eqFontSizeInt = eqFontSize;
            ViewBag.targetPage = targetPage;

            ViewBag.examId = examId;
            ViewBag.setId = setId;
            ViewBag.version = version;
            ViewBag.uniqueSet = 0;
            ViewBag.ShowCorrectAnswer = showCorrectAnswer;

            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var listOfQuestionSet = _examMcqQuestionSetService.LoadExamMcqQuestionSet(examId, null, null, setId);

                var newMcqQuestionSets = new List<Question>();

                foreach (var examMcqQuestionSet in listOfQuestionSet)
                {
                    if (version == (int)ExamVersion.Bangla)
                    {
                        var mcqQuestionOfBangla = _examMcqQuestionService.LoadExamMcqQuestion(_userMenu, examId, examMcqQuestionSet.UniqueSet, (int)ExamVersion.Bangla, examMcqQuestionSet.ExamMcqQuestionSerial).FirstOrDefault();

                        var xmMcqQuestionSet = ReplaceOption(examMcqQuestionSet, mcqQuestionOfBangla);
                        var obj = new Question
                        {
                            SetId = xmMcqQuestionSet.SetId,
                            OptionA = xmMcqQuestionSet.OptionA,
                            OptionB = xmMcqQuestionSet.OptionB,
                            OptionC = xmMcqQuestionSet.OptionC,
                            OptionD = xmMcqQuestionSet.OptionD,
                            OptionE = xmMcqQuestionSet.OptionE,
                            CorrectAnswer = xmMcqQuestionSet.CorrectAnswer,
                            QuestionText = mcqQuestionOfBangla.QuestionText,
                            Version = mcqQuestionOfBangla.Version
                        };
                        newMcqQuestionSets.Add(obj);
                    }

                    else if (version == (int)ExamVersion.English)
                    {
                        var mcqQuestionOfEnglish = _examMcqQuestionService.LoadExamMcqQuestion(_userMenu, examId, examMcqQuestionSet.UniqueSet, (int)ExamVersion.English, examMcqQuestionSet.ExamMcqQuestionSerial).FirstOrDefault();
                        var xmMcqQuestionSet = ReplaceOption(examMcqQuestionSet, mcqQuestionOfEnglish);

                        var obj = new Question
                        {
                            SetId = xmMcqQuestionSet.SetId,
                            OptionA = xmMcqQuestionSet.OptionA,
                            OptionB = xmMcqQuestionSet.OptionB,
                            OptionC = xmMcqQuestionSet.OptionC,
                            OptionD = xmMcqQuestionSet.OptionD,
                            OptionE = xmMcqQuestionSet.OptionE,
                            CorrectAnswer = xmMcqQuestionSet.CorrectAnswer,
                            QuestionText = mcqQuestionOfEnglish.QuestionText,
                            Version = mcqQuestionOfEnglish.Version
                        };
                        newMcqQuestionSets.Add(obj);
                    }
                }

                var examMcqQuestionSetFirst = listOfQuestionSet.FirstOrDefault();

                if (examMcqQuestionSetFirst != null)
                {
                    var exam = examMcqQuestionSetFirst.Exams;
                    var examMcqQuestionSetDto = new ExamMcqQuestionSetPdfDto
                    {
                        ExamName = exam.Name,
                        Program = exam.Program.Name,
                        Session = exam.Session.Name,
                        Fullmarks = exam.McqFullMarks.ToString(),
                        DurationHour = exam.DurationHour,
                        DurationMinute = exam.DurationMinute,
                        QuestionList = newMcqQuestionSets
                    };

                    ViewBag.ExamMcqQuestionSetPdfDto = examMcqQuestionSetDto;
                    var content = "ExamId-" + examId + "\nVersion-" + version + "\nUniqueset-" + examMcqQuestionSetFirst.UniqueSet + "\nSetId-" + setId;
                    ViewBag.Barcode = "data:image/gif;base64," + _commonHelper.GetBarCodeBase64Data(content);
                }
                //ViewBag.Questions = newMcqQuestionSets;
                //ViewBag.Exam = _examsService.GetExamById(examId);
            }
            catch (System.IO.InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            if (showCorrectAnswer == 1)
                return View("~/Views/Shared/_CorrectAnswerQuestionPdf.cshtml");
            return View("~/Views/Shared/_QuestionPdf.cshtml");
        }

        #endregion

        #region Clear Set Generate

        public ActionResult ClearSetGenerate()
        {
            try
            {
                InitializeView();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult ClearSetGenerate(string examMcqQuestionSetClearViewModel)
        {
            InitializeView();
            string errorMsg = "";
            try
            {
                if (ModelState.IsValid)
                {
                    var viewModel = new JavaScriptSerializer().Deserialize<ExamMcqQuestionSetClearViewModel>(examMcqQuestionSetClearViewModel);
                    var setList = _examMcqQuestionSetService.LoadExamMcqQuestionSet(viewModel.ExamId);
                    _examMcqQuestionSetService.Delete(setList);
                    bool exts = Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Question/" + viewModel.ExamId);
                    if (exts)
                    {
                        Directory.Delete(AppDomain.CurrentDomain.BaseDirectory + "Question/" + viewModel.ExamId + "", true);
                    }
                    return Json(new { returnResult = "Successfully Delete", IsSuccess = true });
                }
            }
            catch (MessageException ex)
            {
                errorMsg = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                errorMsg = ex.Message;
            }
            catch (NullObjectException ex)
            {
                errorMsg = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                errorMsg = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMsg = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                errorMsg = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { returnResult = errorMsg, IsSuccess = false });
        }

        #endregion

        #region Ajax Request

        [HttpPost]
        public ActionResult AjaxRequestForExamUniqueSet(long id)
        {
            try
            {
                var xm = _examsService.GetExamById(id);
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                var uniqueSetList = new Dictionary<int, int>();

                if (xm != null)
                {
                    int examQuestionCount = xm.ExamDetails.Where(x => x.ExamType == (int)ExamType.Mcq && x.Status == (int)ExamsDetails.EntityStatus.Active).Sum(x => x.TotalQuestion);
                    for (int i = 1; i <= xm.TotalUniqueSet; i++)
                    {
                        if (xm.IsBanglaVersion)
                        {
                            var totalBanglaQuestionList = _examMcqQuestionService.LoadExamMcqQuestion(userMenu, xm.Id, i, (int)ExamVersion.Bangla);
                            if (totalBanglaQuestionList != null && totalBanglaQuestionList.Any() && examQuestionCount == totalBanglaQuestionList.Count())
                            {
                                uniqueSetList.Add(i, i);
                            }
                        }
                        else if (xm.IsEnglishversion)
                        {
                            var totalEnglishQuestion = _examMcqQuestionService.LoadExamMcqQuestion(userMenu, xm.Id, i, (int)ExamVersion.English);
                            if (totalEnglishQuestion != null && totalEnglishQuestion.Any() && examQuestionCount == totalEnglishQuestion.Count())
                            {
                                uniqueSetList.Add(i, i);
                            }
                        }
                    }
                    var list = new SelectList(uniqueSetList, "Key", "Value").ToList();
                    if ((xm.IsBanglaVersion || xm.IsEnglishversion) && uniqueSetList.Any())
                    {
                        list.Insert(0,
                            new SelectListItem() { Value = SelectionType.NotApplicable.ToString(), Text = "All" });
                    }
                    return Json(new { returnList = list, IsSuccess = true });

                }
                else
                {
                    return Json(new { returnList = uniqueSetList, IsSuccess = false });
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Private method

        private void InitializeView()
        {

            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            ViewBag.Program = new SelectList(_programService.LoadAuthorizedProgram(userMenu), "Id", "Name");
            ViewBag.Session = new SelectList(_sessionService.LoadAuthorizedSession(userMenu), "Id", "Name");
            ViewBag.Course = new SelectList(_courseService.LoadCourse());

        }

        private string GetBaseUrl()
        {
            bool protocol = Request.IsSecureConnection;
            var protocolTxt = "http://";
            if ( protocol )                
                protocolTxt = "https://";
            var url = protocolTxt + Request.Url.Host + ":" + Request.Url.Port;
            _logger.Info(url);
            return url;
        }

        private void MakePdf(ExamMcqQuestionSetPdfDto pdfObject, long examId, int setId, int version, string fileDir, double textFontSize = 5, double eqFontSize = 6, int targetPage = 2)
        {
            /*Temp fix for SSC Udvashito mukh Exam*/
            //if (examId == 5850)
            //{
            //    //if ((setId % 4) == 0)
            //    //{
            //    //    textFontSize = 3.2;
            //    //    eqFontSize = 4.3;
            //    //}
            //    //else 
            //    //if ((setId % 3) == 0)
            //    //{
            //    //    textFontSize = 3.0;
            //    //    eqFontSize = 4.1;
            //    //}
            //    //else if ((setId % 2) == 0)
            //    //{
            //    //    if (version == 1)
            //    //    {
            //    //        textFontSize = 3.9;
            //    //        eqFontSize = 5.2;
            //    //    }
            //    //    else
            //    //    {
            //    //        textFontSize = 3.8;
            //    //        eqFontSize = 4.9;
            //    //    }
            //    //}
            //    //else
            //    //{
            //    //    if (version == 1)
            //    //    {
            //    //        textFontSize = 3.9;
            //    //        eqFontSize = 5.2;
            //    //    }
            //    //    else
            //    //    {
            //    //        textFontSize = 4.1;
            //    //        eqFontSize = 5.1;
            //    //    }
            //    //}
            //}
            /*Temp fix for SSC Udvashito mukh Exam*/
            var fileName = fileDir + examId + "\\" + version + "\\QuestionSet_" + setId + ".pdf";
            var partOnePath = fileDir + examId + "\\" + version + "\\QuestionSet_" + setId + "_temp1.pdf";
            var partTwoPath = fileDir + examId + "\\" + version + "\\QuestionSet_" + setId + "_temp2.pdf";
            var omrPage = Server.MapPath("~/Content/Image/OMR_font_bn.pdf");
            if (version == 2)
            {
                omrPage = Server.MapPath("~/Content/Image/OMR_font_en.pdf");
            }
            bool isProcessRunning = true;
            var ConversionDelay = 60;
            var fontSizeDecrement = 0.1;
            var loopCount = 0;
            while (isProcessRunning)
            {
                loopCount++;
                using (var partOnefs = new FileStream(partOnePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    var pdfUrl = GetBaseUrl() + "/Exam/ExamMcqQuestionSet/PreviewQuestion?examId={0}&setId={1}&version={2}&textFontSize={3}&eqFontSize={4}&targetPage={5}";
                    _logger.Info(pdfUrl);
                    try
                    {
                        HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();
                        htmlToPdfConverter.HtmlViewerWidth = 800;
                        htmlToPdfConverter.NavigationTimeout = 600;
                        htmlToPdfConverter.ConversionDelay = ConversionDelay;
                        htmlToPdfConverter.PdfDocumentOptions.EmbedFonts = true;

                        _logger.Info(string.Format(pdfUrl, examId, setId, version, textFontSize, eqFontSize, targetPage));
                        htmlToPdfConverter.ConvertUrlToStream(string.Format(pdfUrl, examId, setId, version, textFontSize, eqFontSize, targetPage), partOnefs);


                        var reader = RemoveWaterMark(partOnefs, 1);

                        if (reader.NumberOfPages < targetPage)
                        {
                            //re-generation problem occared
                            //so generate again with the same configeration
                            //if (fontSizeDecrement == 0.5)
                            //{
                            //    fontSizeDecrement = 0.1;
                            //}
                            //else
                                ConversionDelay = ConversionDelay + 10;
                        } 
                        else if (reader.NumberOfPages == targetPage)
                        {
                            using (var partTwoFs = new FileStream(partTwoPath, FileMode.Create, FileAccess.Write))
                            {
                                using (PdfStamper stamper = new PdfStamper(reader, partTwoFs)) { }
                            }
                            MargePdfFile(partTwoPath, omrPage, fileName);
                            isProcessRunning = false; //task complete
                        }
                        else
                        {
                            if (textFontSize >= eqFontSize - 1.1)
                            {
                                textFontSize = textFontSize - fontSizeDecrement;
                            }
                            else
                            {
                                eqFontSize = eqFontSize - fontSizeDecrement;
                            }
                            //page number is greater then target so it need to reduce
                        }

                        System.IO.File.Delete(partOnePath);
                        System.IO.File.Delete(partTwoPath);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                    }
                    Thread.Sleep(1000);
                }
            }
            _logger.Info("examId:" + examId + " setId:" + setId + " version:" + version + " textFontSize:" + textFontSize + " eqFontSize:" + eqFontSize + " loopCount:" + loopCount);
        }

        private byte[] MargeOmrPageWithPdf(FileStream questionPdf, string omrPdfPath)
        {
            Document mergeResultPdfDocument = new Document(questionPdf);
            mergeResultPdfDocument.AutoCloseAppendedDocs = true;

            try
            {
                Document secondPdfDocumentToMerge = new Document(omrPdfPath);
                mergeResultPdfDocument.AppendDocument(secondPdfDocumentToMerge);
                byte[] outPdfBuffer = mergeResultPdfDocument.Save();
                return outPdfBuffer;
            }
            finally
            {
                mergeResultPdfDocument.Close();
            }
            return null;
        }

        private void MargePdfFile(string partOnePath, string partTwoPath, string outputPdfPath)
        {
            var sourceOne = new FileStream(partOnePath, FileMode.Open, FileAccess.Read);
            var sourceTwo = new FileStream(partTwoPath, FileMode.Open, FileAccess.Read);

            var destinationDocumentStream = new FileStream(outputPdfPath, FileMode.Create);
            var pdfConcat = new PdfConcatenate(destinationDocumentStream);

            var pdfReaderOne = new PdfReader(sourceOne);
            var pdfReaderTwo = new PdfReader(sourceTwo);
                        
            pdfConcat.AddPages(pdfReaderTwo);
            pdfConcat.AddPages(pdfReaderOne);

            pdfReaderOne.Close();
            pdfReaderTwo.Close();
            pdfConcat.Close();
        }

        private PdfReader RemoveWaterMark(FileStream sourceFs, int removeLayerIndex)
        {
            try
            {
                sourceFs.Seek(0, SeekOrigin.Begin);
                PdfReader reader = new PdfReader(sourceFs);                
                reader.RemoveUnusedObjects();

                PRStream stream;
                //String content;
                PdfDictionary page;
                PdfArray contentarray;

                int pageCount2 = reader.NumberOfPages;

                for (int i = 1; i <= pageCount2; i++)
                {
                    page = reader.GetPageN(i);
                    contentarray = page.GetAsArray(PdfName.CONTENTS);

                    if (contentarray != null)
                    {
                        int layerSize = contentarray.Size;
                        for (int j = 0; j < layerSize; j++)
                        {
                            //stream = (PRStream)contentarray.GetAsStream(j);
                            //content = System.Text.Encoding.ASCII.GetString(PdfReader.GetStreamBytes(stream));
                            //if (content.IndexOf("/OC") >= 0 && content.IndexOf(waterMarkText) >= 0)
                            if (j == removeLayerIndex)
                            {
                                stream = (PRStream)contentarray.GetAsStream(j);
                                stream.Put(PdfName.LENGTH, new PdfNumber(0));
                                stream.SetData(new byte[0]);
                            }
                        }                        
                    }
                }

                return reader;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);                
            }
            return null;
        }

        private Question BindQuestionSet(List<UserMenu> _userMenu, long xmId, ExamMcqQuestionSet examMcqQuestionSet, int p2)
        {
            var mcqQuestionOfBangla = _examMcqQuestionService.LoadExamMcqQuestion(_userMenu, xmId, examMcqQuestionSet.UniqueSet, (int)ExamVersion.Bangla, examMcqQuestionSet.ExamMcqQuestionSerial).FirstOrDefault();

            var xmMcqQuestionSet = ReplaceOption(examMcqQuestionSet, mcqQuestionOfBangla);
            var obj = new Question
            {
                SetId = xmMcqQuestionSet.SetId,
                OptionA = xmMcqQuestionSet.OptionA,
                OptionB = xmMcqQuestionSet.OptionB,
                OptionC = xmMcqQuestionSet.OptionC,
                OptionD = xmMcqQuestionSet.OptionD,
                OptionE = xmMcqQuestionSet.OptionE,
                QuestionText = mcqQuestionOfBangla.QuestionText,
                Version = mcqQuestionOfBangla.Version
            };
            return obj;
        }

        private ExamMcqQuestionSet ReplaceOption(ExamMcqQuestionSet examMcqQuestionSet, ExamMcqQuestion mcqQuestion)
        {
            var examMcqQuestionSetReplaced = new ExamMcqQuestionSet();

            #region Option A

            if (examMcqQuestionSet.OptionA != null && examMcqQuestionSet.OptionA.ToUpper() == "A")
            {
                examMcqQuestionSetReplaced.OptionA = mcqQuestion.OptionA;
            }
            else if (examMcqQuestionSet.OptionA != null && examMcqQuestionSet.OptionA.ToUpper() == "B")
            {
                examMcqQuestionSetReplaced.OptionA = mcqQuestion.OptionB;
            }
            else if (examMcqQuestionSet.OptionA != null && examMcqQuestionSet.OptionA.ToUpper() == "C")
            {
                examMcqQuestionSetReplaced.OptionA = mcqQuestion.OptionC;
            }
            else if (examMcqQuestionSet.OptionA != null && examMcqQuestionSet.OptionA.ToUpper() == "D")
            {
                examMcqQuestionSetReplaced.OptionA = mcqQuestion.OptionD;
            }
            else if (examMcqQuestionSet.OptionA != null && examMcqQuestionSet.OptionA.ToUpper() == "E")
            {
                examMcqQuestionSetReplaced.OptionA = mcqQuestion.OptionE;
            }

            #endregion

            #region Option B

            if (examMcqQuestionSet.OptionB != null && examMcqQuestionSet.OptionB.ToUpper() == "A")
            {
                examMcqQuestionSetReplaced.OptionB = mcqQuestion.OptionA;
            }
            else if (examMcqQuestionSet.OptionB != null && examMcqQuestionSet.OptionB.ToUpper() == "B")
            {
                examMcqQuestionSetReplaced.OptionB = mcqQuestion.OptionB;
            }
            else if (examMcqQuestionSet.OptionB != null && examMcqQuestionSet.OptionB.ToUpper() == "C")
            {
                examMcqQuestionSetReplaced.OptionB = mcqQuestion.OptionC;
            }
            else if (examMcqQuestionSet.OptionB != null && examMcqQuestionSet.OptionB.ToUpper() == "D")
            {
                examMcqQuestionSetReplaced.OptionB = mcqQuestion.OptionD;
            }
            else if (examMcqQuestionSet.OptionB != null && examMcqQuestionSet.OptionB.ToUpper() == "E")
            {
                examMcqQuestionSetReplaced.OptionB = mcqQuestion.OptionE;
            }

            #endregion

            #region Option C

            if (examMcqQuestionSet.OptionC != null && examMcqQuestionSet.OptionC.ToUpper() == "A")
            {
                examMcqQuestionSetReplaced.OptionC = mcqQuestion.OptionA;
            }
            else if (examMcqQuestionSet.OptionC != null && examMcqQuestionSet.OptionC.ToUpper() == "B")
            {
                examMcqQuestionSetReplaced.OptionC = mcqQuestion.OptionB;
            }
            else if (examMcqQuestionSet.OptionC != null && examMcqQuestionSet.OptionC.ToUpper() == "C")
            {
                examMcqQuestionSetReplaced.OptionC = mcqQuestion.OptionC;
            }
            else if (examMcqQuestionSet.OptionC != null && examMcqQuestionSet.OptionC.ToUpper() == "D")
            {
                examMcqQuestionSetReplaced.OptionC = mcqQuestion.OptionD;
            }
            else if (examMcqQuestionSet.OptionC != null && examMcqQuestionSet.OptionC.ToUpper() == "E")
            {
                examMcqQuestionSetReplaced.OptionC = mcqQuestion.OptionE;
            }

            #endregion

            #region Option D

            if (examMcqQuestionSet.OptionD != null && examMcqQuestionSet.OptionD.ToUpper() == "A")
            {
                examMcqQuestionSetReplaced.OptionD = mcqQuestion.OptionA;
            }
            else if (examMcqQuestionSet.OptionD != null && examMcqQuestionSet.OptionD.ToUpper() == "B")
            {
                examMcqQuestionSetReplaced.OptionD = mcqQuestion.OptionB;
            }
            else if (examMcqQuestionSet.OptionD != null && examMcqQuestionSet.OptionD.ToUpper() == "C")
            {
                examMcqQuestionSetReplaced.OptionD = mcqQuestion.OptionC;
            }
            else if (examMcqQuestionSet.OptionD != null && examMcqQuestionSet.OptionD.ToUpper() == "D")
            {
                examMcqQuestionSetReplaced.OptionD = mcqQuestion.OptionD;
            }
            else if (examMcqQuestionSet.OptionD != null && examMcqQuestionSet.OptionD.ToUpper() == "E")
            {
                examMcqQuestionSetReplaced.OptionD = mcqQuestion.OptionE;
            }

            #endregion

            #region Option E

            if (examMcqQuestionSet.OptionE != null && examMcqQuestionSet.OptionE.ToUpper() == "A")
            {
                examMcqQuestionSetReplaced.OptionE = mcqQuestion.OptionA;
            }
            else if (examMcqQuestionSet.OptionE != null && examMcqQuestionSet.OptionE.ToUpper() == "B")
            {
                examMcqQuestionSetReplaced.OptionE = mcqQuestion.OptionB;
            }
            else if (examMcqQuestionSet.OptionE != null && examMcqQuestionSet.OptionE.ToUpper() == "C")
            {
                examMcqQuestionSetReplaced.OptionE = mcqQuestion.OptionC;
            }
            else if (examMcqQuestionSet.OptionE != null && examMcqQuestionSet.OptionE.ToUpper() == "D")
            {
                examMcqQuestionSetReplaced.OptionE = mcqQuestion.OptionD;
            }
            else if (examMcqQuestionSet.OptionE != null && examMcqQuestionSet.OptionE.ToUpper() == "E")
            {
                examMcqQuestionSetReplaced.OptionE = mcqQuestion.OptionE;
            }


            #endregion

            #region CorrectAnswer

            if (!String.IsNullOrEmpty(mcqQuestion.CorrectAnswer) && !String.IsNullOrEmpty(examMcqQuestionSet.CorrectAnswer))
            {
                examMcqQuestionSetReplaced.CorrectAnswer = examMcqQuestionSet.CorrectAnswer;
            }


            #endregion

            return examMcqQuestionSetReplaced;
        }

        #endregion

    }

    public class ViewAsPdf2 : ViewAsPdf
    {
        public ViewAsPdf2(string viewName, object model) : base(viewName, model) { }
        public byte[] GetByte(ControllerContext context)
        {
            base.PrepareResponse(context.HttpContext.Response);
            base.ExecuteResult(context);
            return base.CallTheDriver(context);
        }
    }
}