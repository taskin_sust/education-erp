﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{
    public class AttendanceDeviceMemberMap : ClassMap<AttendanceDeviceMember>
    {
        public AttendanceDeviceMemberMap()
        {
            Table("HR_AttendanceDeviceMember");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.EnrollNo).Column("EnrollNo");
         
            Map(x => x.LastUpdateDateTime).Column("LastUpdateDateTime");
            Map(x => x.IsSynced).Column("IsSynced");
            Map(x => x.LastSyncedDateTime).Column("LastSyncedDateTime");
            References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.AttendanceDevice).Column("AttendanceDeviceId");
        }
    }
}
