﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class AttendanceDeviceFactory : ObjectFactoryBase<AttendanceDevice>
    {
        private readonly AttendanceSynchronizerFactory _attendanceSynchronizerFactory;
        private CampusFactory _campusFactory;

        private readonly AttendanceDeviceService _attendanceDeviceService;

        public AttendanceDeviceFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _attendanceSynchronizerFactory = new AttendanceSynchronizerFactory(this, session);
            _campusFactory = new CampusFactory(this, session);
            _attendanceDeviceService = new AttendanceDeviceService(session);
        }

        public AttendanceDeviceFactory WithAttendanceSyncronizer()
        {
            _attendanceSynchronizerFactory.Create().WithCampus().Persist();
            Object.AttendanceSynchronizer = _attendanceSynchronizerFactory.Object;
            return this;
        }

        public AttendanceDeviceFactory WithAttendanceSyncronizer(Campus campus)
        {
            _attendanceSynchronizerFactory.Create().WithCampus(campus).Persist();
            Object.AttendanceSynchronizer = _attendanceSynchronizerFactory.Object;
            return this;
        }

        public AttendanceDeviceFactory WithDeviceMachineNumber(int machineNumber)
        {
            Object.MachineNo = machineNumber;
            return this;
        }

        public AttendanceDeviceFactory WithAttendanceSyncronizer(AttendanceSynchronizer synchronizer)
        {
            Object.AttendanceSynchronizer = synchronizer;
            return this;
        }

        public AttendanceDeviceFactory Create()
        {
            Object = CreateObject("_device" + DateTime.Now + "_" + Guid.NewGuid().ToString(), DeviceCommunicationType.Ethernet, "192.168.0.28", 4370, GenerateNumaricGuid(), "123456");
            SingleObjectList.Add(Object);
            return this;
        }

        public AttendanceDeviceFactory Create(long id, string name,
            DeviceCommunicationType communicationType, string ipAddress, int port, int machineNumber, string communicationKey)
        {
            Object = CreateObject(name, communicationType, ipAddress, port, machineNumber, communicationKey);
            SingleObjectList.Add(Object);
            return this;
        }

        private AttendanceDevice CreateObject(string name, DeviceCommunicationType communicationType,
            string ipAddress, int port, int machineNumber, string communicationKey)
        {
            var obj = new AttendanceDevice()
            {
                Id = 0,
                Name = name,
                CommunicationType = communicationType,
                IpAddress = ipAddress,
                Port = port,
                MachineNo = machineNumber,
                CommunicationKey = communicationKey,
                BusinessId = GetPcUserName(),
                DeviceModelNo = GenerateNumaricGuid().ToString()
            };
            return obj;
        }


        public AttendanceDeviceFactory WithCampus(List<CampusRoom> campusRoomList)
        {
            _campusFactory.Create().WithCampusRoomList(campusRoomList).WithBranch().Persist();
            Object.Campus = _campusFactory.Object;
            return this;
        }
        public AttendanceDeviceFactory WithCampus(Campus campus)
        {
            Object.Campus = campus;
            return this;
        }

        public AttendanceDeviceFactory WithAttendanceSynchronizer()
        {
            if (Object.Campus == null)
                throw new Exception("Campus should not be null. Call WithCampus before this method call.");
            _attendanceSynchronizerFactory.Create().WithCampus(Object.Campus).Persist();
            Object.AttendanceSynchronizer = _attendanceSynchronizerFactory.Object;
            return this;
        }


        public AttendanceDeviceFactory CreateMore(int numberOfAttdSync, Campus campus)
        {
            ListStartIndex = ObjectList.Count;
            _attendanceSynchronizerFactory.Create().WithCampus(campus).Persist();
            for (int i = 0; i < numberOfAttdSync; i++)
            {
                var obj = CreateObject("_device" + DateTime.Now + "_" + Guid.NewGuid().ToString(), DeviceCommunicationType.Ethernet, "192.168.0.28", 4370, GenerateNumaricGuid(), "123456");
                obj.AttendanceSynchronizer = _attendanceSynchronizerFactory.Object;
                obj.Campus = campus;
                ObjectList.Add(obj);
            }
            return this;
        }

        public AttendanceDeviceFactory CreateMore(int numberOfAttdSync)
        {
            ListStartIndex = ObjectList.Count;
            _attendanceSynchronizerFactory.Create().WithCampus().Persist();
            for (int i = 0; i < numberOfAttdSync; i++)
            {
                var obj = CreateObject("_device" + DateTime.Now + "_" + Guid.NewGuid().ToString(), DeviceCommunicationType.Ethernet, "192.168.0.28", 4370, GenerateNumaricGuid(), "123456");
                obj.AttendanceSynchronizer = _attendanceSynchronizerFactory.Object;
                ObjectList.Add(obj);
            }
            return this;
        }
        private IList<AttendanceDevice> CreateAttendanceDeviceList(int numberOfcampus, Campus campus, AttendanceSynchronizer attendanceSynchronizer)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<AttendanceDevice>();
            for (int i = 0; i < numberOfcampus; i++)
            {
                var attendanceDevice = new AttendanceDevice()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = "_device" + DateTime.Now + "_" + Guid.NewGuid().ToString(),
                    Rank = 1,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = AttendanceDevice.EntityStatus.Active,
                    Campus = campus,
                    DeviceModelNo = GenerateNumaricGuid().ToString(),
                    CommunicationType = DeviceCommunicationType.Ethernet,
                    IpAddress = "192.168.0.28",
                    Port = 4370,
                    MachineNo = GenerateNumaricGuid(),
                    CommunicationKey = "123456",
                    AttendanceSynchronizer = attendanceSynchronizer

                };
                list.Add(attendanceDevice);
            }
            ObjectList.AddRange(list);
            return ObjectList;
        }
        public AttendanceDeviceFactory CreateMore(int numberOfcampusRoom, Campus campus, AttendanceSynchronizer attendanceSynchronizer)
        {
            CreateAttendanceDeviceList(numberOfcampusRoom, campus, attendanceSynchronizer);
            return this;
        }
        public AttendanceDeviceFactory Persist()
        {
            SingleObjectList.ForEach(item =>
            {
                if (item.Id == 0)
                {
                    _attendanceDeviceService.Save(item);
                }
            });
            ObjectList.ForEach(item =>
            {
                if (item.Id == 0)
                {
                    _attendanceDeviceService.Save(item);
                }
            });
            return this;
        }

        public AttendanceDeviceFactory CleanUp()
        {
            DeleteAll();
            _attendanceSynchronizerFactory.Cleanup();
            return this;
        }
        internal void Evict(AttendanceDevice attendanceDevice)
        {
            _attendanceDeviceService.Session.Evict(attendanceDevice);
        }
        private static int GenerateNumaricGuid()
        {
            return new Random().Next(1, Int32.MaxValue);
            //byte[] buffer = Guid.NewGuid().ToByteArray();
            //var val = BitConverter.ToInt32(buffer, 0);
            //if (val < 1) GenerateNumaricGuid();
            //return val;
        }
    }
}
