﻿using System;
using System.Collections.Generic;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using System.Linq;
namespace UdvashERP.Services.Hr
{

    public interface IOvertimeService : IBaseService
    {
        #region Operational Function

        void Save(IList<Overtime> overTimeList);
        void Update(int pin, DateTime overTimeDate, DateTime overTime);

        #endregion

        #region Single Instances Loading Function

        Overtime GetOverTime(int teamMemberPin, DateTime overTimeDate);
        Overtime LoadById(long id);

        #endregion

        #region List Loading Function

        IList<OverTimeDto> LoadOvertime(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList = null, List<UserMenu> userMenu = null, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, int start = 0, int length = 0, string orderBy = null, string orderDir = null, bool isHistory = false);

        #endregion

        #region Others Function

        int LoadOvertimeCount(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, bool isHistory = false);
        DateTime? GetTeamMemberOverTimeIsPostDateTime(long? teamMemberId = null, int? pin = null);

        #endregion

        #region Helper Function

        #endregion
    }
    public class OvertimeService : BaseService, IOvertimeService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IOvertimeDao _overtimeDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;

        public OvertimeService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _overtimeDao = new OvertimeDao() { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public void Save(IList<Overtime> overTimeList)
        {
            ITransaction trans = null;
            try
            {

                #region check vaild OverTime

                List<Overtime> vaildOverTimeList = new List<Overtime>();
                if (!overTimeList.Any())
                    throw new InvalidDataException("No Team Member's over time found for saving.");
                var dublicate = overTimeList.GroupBy(x => x.TeamMember.Pin)
                     .Select(x => new { teammemberPin = x, count = x.Count() })
                     .Where(x => x.count > 1)
                     .ToList();
                if (dublicate.Any())
                    throw new InvalidDataException("Duplicate entry for TeamMember with pin (" + dublicate[0].teammemberPin.Key + ").");
                
                foreach (Overtime overtime in overTimeList)
                {
                    if(overtime.TeamMember == null)
                        throw new InvalidDataException("Invaild Team Member found.");
                    if (overtime.OverTimeDate == null)
                        throw new InvalidDataException("Invaild over time date found for pin (" + overtime.TeamMember.Pin + ").");

                    AttendanceSummary attendanceInfo = _overtimeDao.GetAttendance(overtime.TeamMember.Pin, overtime.OverTimeDate.Value);
                    if (attendanceInfo == null)
                        throw new InvalidDataException("No Attendance found for pin (" + overtime.TeamMember.Pin + ") to apply over time.");

                    if (overtime.ApprovedTime > attendanceInfo.TotalOvertime)
                        throw new InvalidDataException("Over Time is greater then extra time for pin (" + overtime.TeamMember.Pin + ").");

                    DateTime? postDateTime = GetTeamMemberOverTimeIsPostDateTime(overtime.TeamMember.Id);
                    if (postDateTime != null && postDateTime.Value.Date >= overtime.OverTimeDate.Value.Date)
                        throw new InvalidDataException("TeamMember can only applicable for over time after Date (" + postDateTime.Value.ToString("d MMM, yyyy") + ") for Pin (" + overtime.TeamMember.Pin + ") .");

                    vaildOverTimeList.Add(overtime);
                }
                if (!vaildOverTimeList.Any())
                    throw new InvalidDataException("No vaild Team Member's over time found for saving.");

                #endregion

                using (trans = Session.BeginTransaction())
                {
                    //foreach (var overTime in overTimeList)
                    //{
                    //    _overtimeDao.Save(overTime);
                    //    SaveOverTimeLog(overTime);
                    //}
                    foreach (var overTime in vaildOverTimeList)
                    {
                        _overtimeDao.Save(overTime);
                        SaveOverTimeLog(overTime);
                    }
                    trans.Commit();
                }
            }

            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        public void Update(int pin, DateTime overTimeDate, DateTime overTime)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    var overtime = _overtimeDao.GetOverTime(pin, overTimeDate);
                    var attendanceInfo = _overtimeDao.GetAttendance(pin, overTimeDate);
                    if (overTime > attendanceInfo.TotalOvertime)
                    {
                        throw new InvalidDataException("Over Time is greater then extra time !!");
                    }
                    DateTime? postDateTime = GetTeamMemberOverTimeIsPostDateTime(overtime.TeamMember.Id);
                    if (postDateTime != null && postDateTime.Value.Date >= overtime.OverTimeDate.Value.Date)
                        throw new InvalidDataException("TeamMember's over time can change only after Date (" + postDateTime.Value.ToString("d MMM, yyyy") + ") for Pin (" + overtime.TeamMember.Pin + ") .");

                    overtime.ApprovedTime = overTime;
                    _overtimeDao.Save(overtime);
                    SaveOverTimeLog(overtime);

                    trans.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                
            }
        }

        private void SaveOverTimeLog(Overtime overTime)
        {
            try
            {
                OvertimeLog overtimeLog = new OvertimeLog();
                overtimeLog.OverTimeDate = overTime.OverTimeDate;
                overtimeLog.Overtime = overTime;
                overtimeLog.ApprovedTime = overTime.ApprovedTime;
                overtimeLog.CreationDate = DateTime.Now;
                overtimeLog.CreateBy = GetCurrentUserId();
                Session.Save(overtimeLog);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public Overtime GetOverTime(int teamMemberPin, DateTime overTimeDate)
        {
            try
            {
                return _overtimeDao.GetOverTime(teamMemberPin, overTimeDate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Overtime LoadById(long id)
        {
            try
            {
                return _overtimeDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<OverTimeDto> LoadOvertime(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList = null, List<UserMenu> userMenu = null, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, int start = 0, int length = 0, string orderBy = null, string orderDir = null, bool isHistory = false)
        {
            try
            {
                List<long> organizationIdList = new List<long>();
                List<long> branchIdList = new List<long>();
                if (userMenu != null)
                {
                    organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                    branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);
                }
                return _overtimeDao.LoadOvertime(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdList, campusId, departmentId, start, length, orderBy, orderDir, isHistory);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int LoadOvertimeCount(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, long? campusId = null, long? departmentId = null, bool isHistory = false)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationId != null) ? _commonHelper.ConvertIdToList((long)organizationId) : null);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, (branchId != null) ? _commonHelper.ConvertIdToList((long)branchId) : null);

                return _overtimeDao.LoadOvertimeCount(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdList, campusId, departmentId, isHistory);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public DateTime? GetTeamMemberOverTimeIsPostDateTime(long? teamMemberId = null, int? pin = null)
        {
            DateTime? postDateTime = null;
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            AllowanceSheet allowanceSheet = _allowanceSheetDao.GetTeamMemberLastIsSubmittedAllowanceSheet(teamMemberId, pin);
            Overtime overtime = _overtimeDao.GetTeamMemberLastIsPostOverTime(teamMemberId, pin);

            if (allowanceSheet != null && overtime != null)
            {
                postDateTime = allowanceSheet.DateTo;
                if (overtime.OverTimeDate != null && overtime.OverTimeDate > postDateTime)
                    postDateTime = overtime.OverTimeDate;
            }
            else if (allowanceSheet != null)
            {
                postDateTime = allowanceSheet.DateTo;
            }
            else if (overtime != null && overtime.OverTimeDate != null)
            {
                postDateTime = overtime.OverTimeDate;
            }
            return postDateTime;
        }

        #endregion

        #region Helper function

        #endregion

    }
}
