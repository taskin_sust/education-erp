using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class MemberAchievementDetailsMap : BaseClassMap<MemberAchievementDetails,long> {

        public MemberAchievementDetailsMap()
        {
			Table("TM_MemberAchievementDetails");
			LazyLoad();
            References(x => x.MemberAchievement).Column("MemberAchievementId");
            Map(x => x.TaskType).Column("TaskType");
            Map(x => x.TaskName).Column("TaskName");
        }
    }
}
