using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.Hr
{
    public interface IExtradayAllowanceSettingService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(ExtradayAllowanceSetting entity, List<UserMenu> userMenus, bool isNew = true);

        void Delete(ExtradayAllowanceSetting entity, List<UserMenu> userMenus);

        #endregion

        #region Single Instances Loading Function

        ExtradayAllowanceSetting GetExtradayAllowanceSetting(long id);
        

        #endregion

        #region List Loading Function

        IList<ExtradayAllowanceSetting> LoadExtradayAllowanceSetting(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu
                                                        , long organizationId, List<AllowanceSettingHolidayType> holidayTypeList, List<MemberEmploymentStatus> employmentTypeList);

        #endregion

        #region Others Function

        int GetExtradayAllowanceSettingsCount(List<UserMenu> userMenu, long organizationId, List<AllowanceSettingHolidayType> holidayTypeList, List<MemberEmploymentStatus> employmentTypeList);
        void GetEditTypeAndMaxDate(ExtradayAllowanceSetting entity, out int editType, out DateTime? maxDate);

        #endregion

        #region Helper Function

        #endregion

    }
    public class ExtradayAllowanceSettingService : BaseService, IExtradayAllowanceSettingService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("PayrollService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IExtradayAllowanceSettingDao _extradayAllowanceSettingDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;
        private readonly IHolidayWorkDao _holidayWorkDao;
        public ExtradayAllowanceSettingService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _extradayAllowanceSettingDao = new ExtradayAllowanceSettingDao() { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao() { Session = session };
            _holidayWorkDao = new HolidayWorkDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(ExtradayAllowanceSetting model, List<UserMenu> userMenus, bool isNew = true)
        {
            bool result = false;
            try
            {
                if (model == null)
                {
                    throw new NullObjectException("Form Data not found!");
                }
                if (userMenus == null || userMenus.Count <= 0)
                {
                    throw new InvalidDataException("User Menu form not found!");
                }
                CheckValidation(model, isNew);
                var authOrgId = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(model.Organization.Id));
                if (authOrgId.Count <= 0)
                    throw new InvalidDataException("Permission Denied for selected Organization!");
                result = SaveOrUpdate(model);
            }
            catch (Exception ex) 
            {
                _logger.Error(ex);
                throw;
            }
            return result;
        }

        private bool SaveOrUpdate(ExtradayAllowanceSetting entity)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    _extradayAllowanceSettingDao.SaveOrUpdate(entity);
                    trans.Commit();
                }
                
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
            return true;
        }


        public void Delete(ExtradayAllowanceSetting entity, List<UserMenu> userMenus)
        {
            ITransaction transaction = null;
            try
            {
                if (entity == null)
                    throw new NullObjectException("Form Data not found!");
                if (userMenus == null || userMenus.Count <= 0)
                    throw new InvalidDataException("User Menu form not found!");
                var authOrgId = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(entity.Organization.Id));
                if (authOrgId.Count <= 0)
                    throw new InvalidDataException("Permission Denied for selected Organization!");
                var allowanceSheet = _allowanceSheetDao.GetEligibleAllowanceSheetByOrganizationId(entity.Organization.Id);
                if (allowanceSheet != null && allowanceSheet.DateTo >= entity.EffectiveDate)
                    throw new DependencyException("Can't delete this settings! It' already in use!");
                
                using (transaction = Session.BeginTransaction())
                {
                    entity.Status = ExtradayAllowanceSetting.EntityStatus.Delete;
                    _extradayAllowanceSettingDao.Update(entity);
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public ExtradayAllowanceSetting GetExtradayAllowanceSetting(long id)
        {
            try
            {
                var entity = _extradayAllowanceSettingDao.LoadById(id);
                return entity;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return null;
        }


        #endregion

        #region List Loading Function

        public IList<ExtradayAllowanceSetting> LoadExtradayAllowanceSetting(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus
                                                , long organizationId, List<AllowanceSettingHolidayType> holidayTypeList, List<MemberEmploymentStatus> employmentTypeList)
        {
            try
            {
                var organizationList = new List<long>();
                if (organizationId > 0)
                {
                    var authOrgIds = AuthHelper.LoadOrganizationIdList(userMenus,
                        _commonHelper.ConvertIdToList(organizationId)).ToList();
                    if (authOrgIds.Count <= 0)
                        throw new InvalidDataException("Permission Denied for selected Organization!");
                    organizationList.Add(authOrgIds[0]);
                }
                else
                {
                    organizationList = AuthHelper.LoadOrganizationIdList(userMenus);
                    if (organizationList.Count <= 0)
                        throw new InvalidDataException("Permission Denied for selected Organization!");
                }
                IList<ExtradayAllowanceSetting> list = _extradayAllowanceSettingDao.LoadExtradayAllowanceSetting(start, length, orderBy, orderDir, organizationList, holidayTypeList, employmentTypeList);
                return list;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return null;
        }

        #endregion

        #region Others Function

        public int GetExtradayAllowanceSettingsCount(List<UserMenu> userMenus, long organizationId
                                                        , List<AllowanceSettingHolidayType> holidayTypeList, List<MemberEmploymentStatus> employmentTypeList)
        {
            int count = 0;
            try
            {
                var organizationList = new List<long>();
                if (organizationId > 0)
                {
                    var authOrgIds = AuthHelper.LoadOrganizationIdList(userMenus,
                        _commonHelper.ConvertIdToList(organizationId)).ToList();
                    if (authOrgIds.Count <= 0)
                        throw new InvalidDataException("Permission Denied for selected Organization!");
                    organizationList.Add(authOrgIds[0]);
                }
                else
                {
                    organizationList = AuthHelper.LoadOrganizationIdList(userMenus);
                    if (organizationList.Count <= 0)
                        throw new InvalidDataException("Permission Denied for selected Organization!");
                }
                count = _extradayAllowanceSettingDao.GetExtradayAllowanceSettingsCount(organizationList, holidayTypeList, employmentTypeList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return count;
        }
        
        #endregion

        #region Helper function

        private void CheckValidation(ExtradayAllowanceSetting entity, bool isNew)
        {
            var validationResult = ValidationHelper.ValidateEntity<ExtradayAllowanceSetting, ExtradayAllowanceSettingMetaData>(entity);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
            CheckHolidayTypesAndEmploymentType(entity);
            CheckForValidDateEntry(entity, isNew);
            if (_extradayAllowanceSettingDao.CheckForDuplicate(entity))
            {
                throw new DuplicateEntryException("Duplicate entry found with same employee status and holiday type(s) combination.");
            }
        }

        private void CheckForValidDateEntry(ExtradayAllowanceSetting entity, bool isNew)
        {
            if (entity.ClosingDate != null)
            {
                if (DateTime.Compare(entity.EffectiveDate.Date, entity.ClosingDate.Value.Date) >= 0)
                {
                    throw new InvalidDataException("Effective date can't be less then closing date!");
                }
            }

            var allowanceSheet = _allowanceSheetDao.GetEligibleAllowanceSheetByOrganizationId(entity.Organization.Id);

            //date validation for new entity
            if (allowanceSheet != null && allowanceSheet.DateTo >= entity.EffectiveDate)
            {
                throw new InvalidDataException("Salary Sheet renamed by Allowance sheet!");
            }

            //date validation for old entity
            if (!isNew)
            {
                int editType;
                DateTime? maxDate;
                GetEditTypeAndMaxDate(entity, out editType, out maxDate);
                switch (editType)
                {
                    case (int)AllowanceSettingsEditType.Uneditable:
                        throw new InvalidDataException("Salary Sheet renamed by Allowance sheet!");

                    case (int)AllowanceSettingsEditType.MinimumEditable:
                        if (maxDate != null && entity.ClosingDate < maxDate)
                        {
                            throw new InvalidDataException("Salary Sheet renamed by Allowance sheet!");
                        }
                        break;

                    case (int)AllowanceSettingsEditType.MaximumEditable:
                        if (maxDate != null && entity.EffectiveDate <= maxDate)
                        {
                            throw new InvalidDataException("Salary Sheet renamed by Allowance sheet!");
                        }
                        if (maxDate != null && entity.ClosingDate < maxDate)
                        {
                            throw new InvalidDataException("Salary Sheet renamed by Allowance sheet!");
                        }
                        break;
                }
            }
        }

        private static void CheckHolidayTypesAndEmploymentType(ExtradayAllowanceSetting entity)
        {
            if (!entity.IsGazetted && !entity.IsManagement && !entity.IsWeekend)
            {
                throw new InvalidDataException("Holiday Types is not selected!");
            }
            if (!entity.IsContractual && !entity.IsPartTime && !entity.IsPermanent && !entity.IsProbation && !entity.IsIntern)
            {
                throw new InvalidDataException("Employment Types is not selected!");
            }
        }

        public void GetEditTypeAndMaxDate(ExtradayAllowanceSetting entity, out int editType, out DateTime? maxDate)
        {
            editType = (int)AllowanceSettingsEditType.Uneditable;
            var allowanceSheet = _allowanceSheetDao.GetEligibleAllowanceSheetByOrganizationId(entity.Organization.Id);
            maxDate = null;
            if (allowanceSheet == null)
            {
                editType = (int)AllowanceSettingsEditType.MaximumEditable;
            }
            else
            {
                maxDate = allowanceSheet.DateTo;
                if (maxDate.Value < entity.EffectiveDate)
                {
                    editType = (int)AllowanceSettingsEditType.MaximumEditable;
                }
                else if (maxDate.Value >= entity.EffectiveDate)
                {
                    if (entity.ClosingDate == null || maxDate < entity.ClosingDate)
                    {
                        editType = (int)AllowanceSettingsEditType.MinimumEditable;
                    }
                }
            }
        }

        #endregion
    }
}

