﻿using System.Collections;
using System.Collections.Generic;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IAcademicInfoDao : IBaseDao<AcademicInfo, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<AcademicInfo> LoadAccademicInfoByMember(long teamMemberId);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class AcademicInfoDao : BaseDao<AcademicInfo, long>, IAcademicInfoDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<AcademicInfo> LoadAccademicInfoByMember(long teamMemberId)
        {
            return Session.QueryOver<AcademicInfo>().Where(x => x.TeamMember.Id == teamMemberId)
                .Where(x=>x.Status == AcademicInfo.EntityStatus.Active).List<AcademicInfo>();
        }
        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion

        
    }

}
