


function responseReturn() {
    
    var check = true;

    
    var smsReceivers = $("#SmsReceivers").val();
    if (smsReceivers == null) {
        $("#SmsReceivers").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Please select one or more sms receiver.");
        check = false;
    }
    else {
        $("#SmsReceivers").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
    var template = $("#Template").val();
    if (template == "") {
        $("#Template").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Template is required.");
        check = false;
    }
    else {
        $("#Template").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
    if (check) {
        console.log("SUBMIT");
        $("#smsForm").get(0).submit();
    }
}

$(document).on("keyup", "#Template", function (e) {
    var val = $(this).val();
    var attr = $(this).attr("id");
    if (val == "") {
        $(this).parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Template is required.");
    }
    else {
        $(this).parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
});

$(document).on("change", "#SmsReceivers", function (e) {
    var val = $(this).val();
    var attr = $(this).attr("id");
    if (val == null) {
        $(this).parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("Please select one or more sms receiver.");
    }
    else {
        $(this).parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
    }
});


//$(document).bind('keydown keyup', function (e) {
//    if (e.which === 116) {
//        console.log('blocked');
//        return false;
//    }
//    if (e.which === 82 && e.ctrlKey) {
//        console.log('blocked');
//        return false;
//    }
//});


$(document).on("click", "#DynamicOptions", function (e) {
   var text= $("#DynamicOptions option:selected").text();  
   var cursorPosition = $('#Template').prop("selectionStart");
   var message = $("#Template").val();
   var start = $('#Template').get(0).selectionStart;
   var finish = $('#Template').get(0).selectionEnd;
   var sel = message.substring(start, finish);
   if (sel == "")
   {     
       if (message.length == cursorPosition) {
           text = "[[{{" + text + "}}]]";
       }
       else {
           text = "[[{{" + text + "}}]]";
       }      
   }
   else
   {
       message = message.replace(sel, "");
       text = "[[{{" + text + "}}]]";
   }
  
    message = [message.slice(0, cursorPosition), text, message.slice(cursorPosition)].join('');



   
  // message = message + " " + text;
   $("#Template").val(message);
   $("#Template").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
   
});


$(document).ready(function () {
    var hideField = $("#hideField").val();
    if (hideField != undefined && hideField != null && hideField!="")
    {
        $(document).bind('keydown keyup', function (e) {
            if (e.which === 116) {
                console.log('blocked');
                return false;
            }
            if (e.which === 82 && e.ctrlKey) {
                console.log('blocked');
                return false;
            }
        });
    }
    
    //$('select').removeAttr('multiple');
    //$("select").attr("size", 5);

    $("#smsForm").submit(function() {
        return false;
    });


    //$('body').keydown(function (e) {
    //    if (e.keyCode == 13) {
    //        responseReturn();
    //    }
    //});


    $("#submitForm").click(function () {        
        responseReturn();
    });

});

