﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.AdminisTration.MenuTest
{
    public class MenuBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal IMenuService _menuService;
        internal MenuFactory _menuFactory;
        private ISession _session;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        #endregion

        public MenuBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _menuFactory = new MenuFactory(null, _session);
            _menuService = new MenuService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _menuFactory.Cleanup();
            base.Dispose();
        }
    }
}
