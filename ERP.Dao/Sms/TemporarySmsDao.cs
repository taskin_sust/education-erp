﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Sms
{
    public interface ITemporarySmsDao : IBaseDao<TemporarySms, long>
    {

        List<TemporarySms> LoadAllPendingTempSms(int defaultCount);
    }
    public class TemporarySmsDao : BaseDao<TemporarySms, long>, ITemporarySmsDao
    {
        public List<TemporarySms> LoadAllPendingTempSms(int defaultCount)
        {
            return (List<TemporarySms>)Session.QueryOver<TemporarySms>().Where(x => x.Status == 1).Take(defaultCount).List<TemporarySms>();
        }
    }
}