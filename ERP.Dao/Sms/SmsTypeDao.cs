﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public interface ISmsTypeDao : IBaseDao<SmsType, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
 IList<SmsType> LoadSmsTypeActive(); 
        #endregion

        #region Others Function

        #endregion

        SmsType LoadByTypeName(string name);
    }
    public class SmsTypeDao:BaseDao<SmsType, long>,ISmsTypeDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public SmsType LoadByTypeName(string name)
        {
            ICriteria criteria = Session.CreateCriteria<SmsType>();
            criteria.Add(Restrictions.Eq("Status", SmsType.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Name", name));
            return criteria.UniqueResult<SmsType>();
        }
        #endregion

        #region List Loading Function
        public IList<SmsType> LoadSmsTypeActive()
        {
            ICriteria criteria = Session.CreateCriteria<SmsType>();
            criteria.Add(Restrictions.Eq("Status", SmsType.EntityStatus.Active));
            criteria.AddOrder(Order.Asc("Rank"));
            return criteria.List<SmsType>();
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
       
    }
}
