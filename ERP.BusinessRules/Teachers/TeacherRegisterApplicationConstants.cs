﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.BusinessRules.Teachers
{
    public class TeacherRegisterApplicationConstants
    {
        public const string FullName = "Full Name";
        public const string NickName = "Nick Name";
        public const string MobileNumber1 = "Mobile Number 1";
        public const string MobileNumber2 = "Mobile Number 2";
        public const string Email = "Email Address";
        public const string FacebookId = "Facebook ID";
        public const string Institute = "Institute";
        public const string Department = "Department";
        public const string HscPassingYear = "Hsc Passing Year";
        public const string Religion = "Religion";
        public const string Gender = "Gender";
        public const string TeacherActivity = "Teacher Activity";
        public const string SubjectPriority = "Subject Priority";
        public const string VersionPriority = "Version Priority";
        public const string Status = "Status";
        public const string Action = "Action";

        public static List<string> GetTeacherRegistrationListInformationStringList()
        {
            return new List<string>
            {
                FullName
                , NickName
                , MobileNumber1
                , MobileNumber2
                , Email
                , FacebookId
                , Institute
                , Department
                , HscPassingYear
                , Religion
                , Gender
                , TeacherActivity
                , SubjectPriority
                , VersionPriority
            };
        }
    }
}
