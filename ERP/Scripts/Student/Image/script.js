// HabibXP
dataList = new Array();
dataIndex = 0;

function removeData(index) {
    var i = parseInt(index);
    dataList[i] = null;
    console.log(i);

    var tpl = $("#file_" + index);
    tpl.fadeOut(function () {
        tpl.remove();
    });

}

uploader = $(function () {

    var ul = $('#upload ul');

    $('#drop a').click(function () {
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    liId = 1;

    $('#upload').fileupload({
        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),


        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        formData: function (form) {
            return form.serializeArray();
        },

        add: function (e, data) {



            var tpl = $('<li id="file_' + liId + '" class="working ' + data.files[0].name + '">' +
                '<div style="float: left";width: 60px;">' +
                '<input type="text" value="0" data-width="48" data-height="48"' +
                ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /></div><div style="float: left; padding-left: 60px;"><p></p><span id="span_"' + liId + '></span><br/><br/>' +
                '<div id="message" class = "message">' +
                '</div></li></div>');

            // Append the file name and file size
            tpl.find('p').addClass('para')
                         .text(data.files[0].name)
                         .append('<i>' + formatFileSize(data.files[0].size) + '</i>');
            // Add the HTML to the UL element
            //tpl.append($('<p>').addClass('message'));

            //if (liId == 1) {
            //    document.getElementById('upper').className = 'col-md-12';
            //}
            var li = $('#upload ul li');

            //if (li.length == 0) {
            //    document.getElementById('upper').className = 'col-md-12';
            //}

            data.context = tpl.appendTo(ul);

            // Initialize the knob plugin
            tpl.find('input').knob();

            // Listen for clicks on the cancel icon
            tpl.find('span').attr("onclick", "removeData(" + liId + ")");

            /*tpl.find('span').click(function () {

                tpl.fadeOut(function(){
                    tpl.remove();
                });

            });
            */
            //$('<input type="checkbox" > </input>').insertBefore("#file_" + liId);
            //data.push(liId);
            dataList[liId] = data;
            //dataIndex = liId;
            //console.log(dataList);
            liId++;
            // Automatically upload the file once it is added to the queue
            //var jqXHR = data.submit();
        },

        progress: function (e, data) {

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if (progress == 100) {
                data.context.removeClass('working');
            }
        },

        done: function (e, data) {
            var para = document.getElementsByClassName(data.files[0].name).innerText;
            data.context.text = data.result;
            var li = $('li[class="' + data.files[0].name + '"]');
            var pUploadMessage = li.find('div.message');

            var result = data.result;

            //Total Counter
            var counter1 = $("#TotalCounter").text();
            if (!isNaN(counter1)) {

                var currentCount = parseInt(counter1);
                ++currentCount;

                counter1 = currentCount;
            } else
                counter1 = 1;

            $("#TotalCounter").html(counter1);

            //Failed Counter
            if (result.length > 10) {
                data.context.addClass('working');
                pUploadMessage.html(data.result).css('color', '#F00');
                var counter1 = $("#FailedCounter").text();
                if (!isNaN(counter1)) {

                    var currentCount = parseInt(counter1);
                    ++currentCount;

                    counter1 = currentCount;
                } else
                    counter1 = 1;

                $("#FailedCounter").html(counter1);
            }
            else {

                //Duplicate Counter
                if (result == "Duplicate") {
                    pUploadMessage.html(data.result).css('color', '#00F');
                    var counter1 = $("#DuplicateCounter").text();
                    if (!isNaN(counter1)) {

                        var currentCount = parseInt(counter1);
                        ++currentCount;

                        counter1 = currentCount;
                    } else
                        counter1 = 1;

                    $("#DuplicateCounter").html(counter1);
                }
                    //Success Counter
                else {

                    pUploadMessage.html(data.result).css('color', '#0C0');

                    var counter1 = $("#SuccessCounter").text();
                    if (!isNaN(counter1)) {

                        var currentCount = parseInt(counter1);
                        ++currentCount;

                        counter1 = currentCount;
                    } else
                        counter1 = 1;

                    $("#SuccessCounter").html(counter1);
                }
            }
        },

        fail: function (e, data) {
            // Something has gone wrong!
            data.context.addClass('error');
        }
    });


    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }

});