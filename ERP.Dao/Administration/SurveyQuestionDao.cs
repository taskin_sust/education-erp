﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface ISurveyQuestionDao : IBaseDao<SurveyQuestion, long>
    {        
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        SurveyQuestion LoadByRank(int newRank);
        #endregion

        #region List Loading Function
        IList<SurveyQuestion> GetSurveyQuestionList(int start, int length, string orderBy, string orderDir, string question, string status);
        IList<SurveyQuestion> LoadActive();
        IList<SurveyQuestion> LoadQuestionWiseSurvey(int start, int length, long[] questionId);
        #endregion

        #region Others Function
        bool HasDuplicateByName(string question, long? id = null);
        int SurveyQuestionRowCount(string question, string status);
        int GetQuestionWiseSurveyCount(int start, int length, long[] questionId);
        #endregion    
    }

    public class SurveyQuestionDao : BaseDao<SurveyQuestion, long>, ISurveyQuestionDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public SurveyQuestion LoadByRank(int newRank)
        {
            return Session.QueryOver<SurveyQuestion>().Where(x => x.Rank == newRank).Take(1).SingleOrDefault<SurveyQuestion>();
        }
        #endregion

        #region List Loading Function
        public IList<SurveyQuestion> GetSurveyQuestionList(int start, int length, string orderBy, string orderDir, string question, string status)
        {
            ICriteria criteria = Session.CreateCriteria<SurveyQuestion>().Add(Restrictions.Not(Restrictions.Eq("Status", SurveyQuestion.EntityStatus.Delete)));
            if (!String.IsNullOrEmpty(question))
                criteria.Add(Restrictions.Like("Question", question, MatchMode.Anywhere));
            if (!String.IsNullOrEmpty(status))
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            return (List<SurveyQuestion>)criteria.SetFirstResult(start).SetMaxResults(length).List<SurveyQuestion>();
        }
        public IList<SurveyQuestion> LoadActive()
        {
            ICriteria criteria = Session.CreateCriteria(typeof(SurveyQuestion));
            criteria.Add(Restrictions.Eq("Status", SurveyQuestion.EntityStatus.Active));
            return criteria.List<SurveyQuestion>();
        }
        public IList<SurveyQuestion> LoadQuestionWiseSurvey(int start, int length, long[] questionId)
        {
            var surveyQuestions = SurveyQuestions(questionId);
            if (length > 0)
            {
                return surveyQuestions.Skip(start).Take(length).List<SurveyQuestion>();
            }
            else
            {
                return surveyQuestions.List<SurveyQuestion>();
            }
        }
        #endregion

        #region Others Function
        public bool HasDuplicateByName(string question, long? id = null)
        {
            ICriteria criteria = Session.CreateCriteria<SurveyQuestion>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", SurveyQuestion.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("Question", question));
            if (id != null && id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            var questionList = criteria.List<SurveyQuestion>();
            if (questionList != null && questionList.Count > 0)
                return true;
            return false;
        }
        public int SurveyQuestionRowCount(string question, string status)
        {
            ICriteria criteria = Session.CreateCriteria<SurveyQuestion>().Add(Restrictions.Not(Restrictions.Eq("Status", SurveyQuestion.EntityStatus.Delete)));
            if (!String.IsNullOrEmpty(question))
                criteria.Add(Restrictions.Like("Question", question, MatchMode.Anywhere));
            if (!String.IsNullOrEmpty(status))
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
        public int GetQuestionWiseSurveyCount(int start, int length, long[] questionId)
        {
            var surveyQuestions = SurveyQuestions(questionId);
            return surveyQuestions.RowCount();
        }
       
        #endregion

        #region Helper Function
        private IQueryOver<SurveyQuestion> SurveyQuestions(long[] questionId)
        {
            var surveyQuestion =
                Session.QueryOver<SurveyQuestion>()
                    .Where(x => x.Status == SurveyQuestion.EntityStatus.Active);
            if (!questionId.Contains(0))
            {
                surveyQuestion = surveyQuestion.Where(x => x.Id.IsIn(questionId));
            }
            return surveyQuestion;
        }
        #endregion
    }
}
