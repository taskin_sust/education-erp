﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class OverTimeFactory : ObjectFactoryBase<Overtime>
    {
        private TeamMemberFactory _teamMemberFactory;
        private readonly IOvertimeService _overtimeService;
        public OverTimeFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _overtimeService = new OvertimeService(session);
        }

        #region Create

        public OverTimeFactory Create()
        {
            Object = new Overtime()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                OverTimeDate = DateTime.Now,
                ApprovedTime = DateTime.Now
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public OverTimeFactory CreateWith(DateTime overTimeDate, DateTime approvedTime)
        {
            Object = new Overtime()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                OverTimeDate = overTimeDate,
                ApprovedTime = approvedTime
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public OverTimeFactory WithTeamMember()
        {
            Object.TeamMember = _teamMemberFactory.Create().Persist().Object;
            return this;
        }

        public OverTimeFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public OverTimeFactory CreateMore(int num)
        {
            _teamMemberFactory.Create().Persist();
            for (int i = 0; i < num; i++)
            {
                var obj = new Overtime()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = Name,
                    CreateBy = 1,
                    ModifyBy = 1,
                    OverTimeDate = DateTime.Now,
                    ApprovedTime = DateTime.Now.AddHours(1)
                };
                obj.TeamMember = _teamMemberFactory.Object;
                ObjectList.Add(obj);
            }
            return this;
        }

        public OverTimeFactory Persist()
        {
            SingleObjectList.ForEach(x =>
            {
                if(x.Id == 0)
                    _overtimeService.Save(new List<Overtime>() { x });
            });

            ObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                    _overtimeService.Save(new List<Overtime>() { x });
            });

            return this;
        }

        #endregion

        #region Others
        #endregion

        #region CleanUp

        public OverTimeFactory CleanUp()
        {
            DeleteAll();
            _teamMemberFactory.Cleanup();
            return this;
        }

        public OverTimeFactory LogCleanUp(OverTimeFactory overTimeFactory)
        {
            if (overTimeFactory.ObjectList.Any())
            {
                DeleteLog(overTimeFactory.ObjectList);
            }
            if (overTimeFactory.SingleObjectList.Any())
            {
                DeleteLog(overTimeFactory.SingleObjectList);
            }
            return this;
        }

        private void DeleteLog(IEnumerable<Overtime> list)
        {
            foreach (var ovrTme in list)
            {
                var tableName = GetEntityTableName(typeof(OvertimeLog));
                string swData = "DELETE FROM " + tableName + " WHERE [OvertimeId] = " + ovrTme.Id + ";";
                _session.CreateSQLQuery(swData).ExecuteUpdate();
            }
        }

        #endregion

    }
}
