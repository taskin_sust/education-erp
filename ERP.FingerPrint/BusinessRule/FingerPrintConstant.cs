﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.FingerPrint.BusinessRule
{
    public class FingerPrintConstant
    {
        public static readonly string GetXmlFileName = "DeviceInfo.xml";
        public static readonly string GetWaitingTime = "FingerPrintIdentiryWaitingTime";


        public static readonly string GetBaseUrl = "BaseUrl";

        public static readonly string GetIndex0 = "Index0";
        public static readonly string GetIndex1 = "Index1";
        public static readonly string GetIndex2 = "Index2";
        public static readonly string GetIndex3 = "Index3";
        public static readonly string GetIndex4 = "Index4";
        public static readonly string GetIndex5 = "Index5";
        public static readonly string GetIndex6 = "Index6";
        public static readonly string GetIndex7 = "Index7";
        public static readonly string GetIndex8 = "Index8";
        public static readonly string GetIndex9 = "Index9";

        public static readonly int FingerIndex0 = 0;
        public static readonly int FingerIndex1 = 1;
        public static readonly int FingerIndex2 = 2;
        public static readonly int FingerIndex3 = 3;
        public static readonly int FingerIndex4 = 4;
        public static readonly int FingerIndex5 = 5;
        public static readonly int FingerIndex6 = 6;
        public static readonly int FingerIndex7 = 7;
        public static readonly int FingerIndex8 = 8;
        public static readonly int FingerIndex9 = 9;

    }
}
