﻿using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DepartmentTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Department")]
    public class DepartmentDeleteTest : DepartmentBaseTest
    {        
        [Fact]
        public void Delete_check_IsTrue() 
        {
            _departmentFactory.Create().WithNameAndRank(_name, 1).WithOrganization().Persist();
            Department hrDepartment = _departmentService.LoadById(_departmentFactory.Object.Id);
            bool isSuccess = _departmentService.Delete(hrDepartment);
            Assert.True(isSuccess);
            hrDepartment = _departmentService.LoadById(_departmentFactory.Object.Id);
            Assert.Null(hrDepartment);
        }
        
        [Fact]
        public void Delete_department_organization_should_not_delete()
        {
            var tm = PersistTeamMember();
            var department =
                _departmentService.LoadById(tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault().Id);
            Assert.Throws<DependencyException>(
                () => _departmentService.Delete(department));
        }

        [Fact]
        public void Should_Return_NullException()
        {
            Assert.Throws<EmptyFieldException>(() => _departmentService.Delete(null));
        }
    }
}
