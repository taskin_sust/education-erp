using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class DailySupportAllowanceSettingFactory : ObjectFactoryBase<DailySupportAllowanceSetting>
    {
        private readonly OrganizationFactory _organizationFactory;
        private readonly DesignationFactory _designationFactory;

        private readonly IDailySupportAllowanceSettingService _prDailySupportAllowanceService;

        public DailySupportAllowanceSettingFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _prDailySupportAllowanceService = new DailySupportAllowanceSettingService(session);
            _organizationFactory = new OrganizationFactory(this, session);
            _designationFactory = new DesignationFactory(this, session);
        }

        #region Create

        public DailySupportAllowanceSettingFactory Create()
        {
            Object = new DailySupportAllowanceSetting()
            {
                Amount = 1000,
                Name = new Guid().ToString() + "_ABC" + DateTime.Now.ToString(),
                EffectiveDate = DateTime.Now
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public DailySupportAllowanceSettingFactory CreateWith(DateTime effectiveDate, DateTime closingDate)
        {
            Object = new DailySupportAllowanceSetting()
            {
                Amount = 1000,
                Name = new Guid().ToString() + "_ABC" + DateTime.Now.ToString(),
                EffectiveDate = effectiveDate,
                ClosingDate = closingDate
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public DailySupportAllowanceSettingFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public DailySupportAllowanceSettingFactory Persist(List<UserMenu> menus)
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _prDailySupportAllowanceService.SaveOrUpdate(menus, x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_prDailySupportAllowanceService.SaveOrUpdate(menus, x); });

            }
            return this;
        }

        public DailySupportAllowanceSettingFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new DailySupportAllowanceSetting() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public DailySupportAllowanceSettingFactory CleanUp()
        {
            DeleteAll();
            return this;
        }

        #endregion

        #region With function

        public DailySupportAllowanceSettingFactory WithOrganization()
        {
            _organizationFactory.Create();
            return this;
        }

        public DailySupportAllowanceSettingFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public DailySupportAllowanceSettingFactory WithDesignation()
        {
            Object.Designation = _designationFactory.Create().WithOrganization(_organizationFactory.Create().Persist().Object).Object;
            return this;
        }

        public DailySupportAllowanceSettingFactory WithOrgAndDesignation(Organization organization,
            Designation designation)
        {
            Object.Organization = organization;
            Object.Designation = designation;
            return this;
        }
        #endregion

    }
}
