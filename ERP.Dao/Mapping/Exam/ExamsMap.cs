﻿using NHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Exam;

namespace UdvashERP.Dao.Mapping.Exam
{
    public partial class ExamsMap : BaseClassMap<Exams,long>
    {
        public ExamsMap()
        {
            Table("Exams");
            LazyLoad();
            Map(x => x.Name).Column("Name");
            Map(x => x.ShortName).Column("ShortName");
            Map(x => x.Code).Column("Code");
            Map(x => x.ExamDate).Column("ExamDate");
            Map(x => x.IsMcq).Column("IsMcq");
            Map(x => x.McqMaxSubjectCount).Column("McqMaxSubjectCount");
            Map(x => x.IsWritten).Column("IsWritten");
            Map(x => x.WrittenMaxSubjectCount).Column("WrittenMaxSubjectCount");
            Map(x => x.FullMarks).Column("FullMarks");
            Map(x => x.TotalQuestion).Column("TotalQuestion");
            Map(x => x.IsAcceptPartialAnswer).Column("IsAcceptPartialAnswer");
            Map(x => x.Rank).Column("Rank");
            Map(x => x.McqFullMarks).Column("McqFullMarks");
            Map(x => x.WrittenFullMark).Column("WrittenFullMarks");
            Map(x => x.IsTakeAnswerSheetWhenUploadMarks).Column("IsTakeAnswerSheetWhenUploadMarks");
            Map(x => x.TotalUniqueSet).Column("TotalUniqueSet");
            Map(x => x.DurationHour).Column("DurationHour");
            Map(x => x.DurationMinute).Column("DurationMinute");
            Map(x => x.IsEnglishversion).Column("IsEnglishversion");
            Map(x => x.IsBanglaVersion).Column("IsBanglaVersion");
            Map(x => x.IsSetGenerationRunning).Column("IsSetGenerationRunning");   

            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.Course).Column("CourseId");
            References(x => x.Branch).Column("BranchId");
            References(x => x.ExamParent).Column("ExamParentId");
           // References(x => x.Campus).Column("CampusId");
            References(x => x.Batch).Column("BatchId");
            HasMany(x => x.ExamDetails).KeyColumn("ExamId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentExamAttendances).KeyColumn("ExamId").Cascade.AllDeleteOrphan().Inverse(); 
            HasMany(x => x.ExamMcqQuestions).KeyColumn("ExamId").Cascade.AllDeleteOrphan().Inverse();
            //HasMany(x => x.ExamMcqQuestions).KeyColumn("ExamId").Cascade.AllDeleteOrphan();
                        HasMany(x => x.ExamMcqQuestionSetPrintList).KeyColumn("ExamId").Cascade.AllDeleteOrphan().Inverse(); 
        }
    }
}
