﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory;
using Xunit;

namespace UdvashERP.Services.Test.Hr.TeamMemberTest
{
    interface ITeamMemberUpdateTest
    {
        #region Update Basic Info

        void Update_Should_Return_BasicInfo_Null_Exception();
        //void Update_Should_Return_BasicInfo_Check_Property();
        void Update_Should_Return_BasicInfo_Successfully();
        
        #endregion

        #region Update Personal Info

        void Update_Should_Return_Null_Exception();
        void Update_Should_Check_Property();
        void Should_Update_Successfully();

        #endregion

        #region Update Parental Info

        void Update_Should_Return_ParentalInfo_Null_Exception();
        void Update_Should_Return_ParentalInfo_Check_Property();
        void Update_Should_Return_ParentalInfo_Successfully();

        #endregion

        #region Update Specialization Info

        void Update_Should_Return_SpecializationInfo_Null_Exception();
        void Update_Should_Return_SpecializationInfo_Check_Property();
        void Update_Should_Return_SpecializationInfo_Successfully(); 

        #endregion

        #region UpdateTaxInformation

        void Should_Return_UpdateTaxInformation_Successfully();
        void Should_Return_InvalidException_For_UserMenu();
        void Should_Return_InvalidException_For_TeamMember();
        void Should_Return_InvalidException_For_UnAuthorized_TeamMember();

        #endregion

        #region TranferTeamMember

        void Should_Return_TranferTeamMember_Successfully();
        void Should_Return_TranferTeamMember_InvalidException_For_TeamMember();
        void Should_Return_TranferTeamMember_InvalidException_For_Usermenu();
        void Should_Return_TranferTeamMember_INvalidException_For_Same_Organization();
        void Should_Return_TranferTeamMember_InvalidException_For_EffectiveDate_Is_GreaterThan_TransferingDate();
        void Should_Return_TranferTeamMember_InvalidException_For_ShiftWeekendEffectiveDate_Is_GreaterThan_TransferingDate();
        void Should_Return_TranferTeamMember_InvalidException_For_PendingLeave();


        #endregion
    }

    [Trait("Area", "Hr")]
    [Trait("Service", "TeamMember")]
    public class TeamMemberUpdateTest : TeamMemberBaseTest, ITeamMemberUpdateTest
    {

        #region Update Basic Info

        [Fact]
        public void Update_Should_Return_BasicInfo_Null_Exception()
        {
            PersistTeamMember();
            Assert.Throws<NullObjectException>(() => teamMemberService.UpdateBasicInformation(null));
        }

        //[Fact]
        //public void Update_Should_Return_BasicInfo_Check_Property()
        //{
        //    const string cardNo = "0004429466"; //dupication check
        //    const string officialEmail = "a@b.com";
        //    const string officialContact = "88018411000";
        //    var tm = PersistTeamMember();
        //    _memberOfficialDetailFactory.CreateWith(cardNo, officialContact, officialEmail);
        //    TeamMember teamMember = _teamMemberService.LoadById(tm.Id);
        //    teamMember.MemberOfficialDetails.Add(_memberOfficialDetailFactory.SingleObjectList.FirstOrDefault());
        //    Assert.Throws<DuplicateEntryException>(() => _teamMemberService.UpdateBasicInformation(teamMember));
        //}

        [Fact]
        public void Update_Should_Return_BasicInfo_Successfully()
        {
            const string cardNo = "0000000000";
            const string officialEmail = "a@b.com";
            const string officialContact = "88018411000";
            var tm = PersistTeamMember();
            memberOfficialDetailFactory.CreateWith(cardNo, officialContact, officialEmail);
            TeamMember teamMember = teamMemberService.LoadById(tm.Id);
            teamMember.MemberOfficialDetails.Add(memberOfficialDetailFactory.SingleObjectList.FirstOrDefault());
            teamMemberService.UpdateBasicInformation(teamMember);
            Assert.NotNull(teamMemberFactory.Object);
            Assert.NotEqual(teamMemberFactory.Object.Id, 0);
        }

        #endregion

        #region Update Personal Info

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            //var teamMember = PersistTeamMember();
            Assert.Throws<NullObjectException>(() => teamMemberService.UpdatePersonalInformation(null, null, null, null, null));
        }
        [Fact]
        public void Update_Should_Check_Property()
        {
            DateTime startDate = DateTime.Now.AddYears(-3);
            DateTime endDate = DateTime.Now.AddYears(-1);

            const long presentThanaId = 21;
            const long permenantThanaId = 22;
            const long presentPostOfficeId = 21;
            const long permenantPostOfficeId = 22;


            //TODO:Initially create team member without mentor for testing. Mentor as a team member  
            var mentorObj = teamMemberService.GetMember(1);

            var mentorEmployment = mentorObj.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            organizationFactory.Create().Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.Object);
            maritalInfoFactory.CreateMore(1);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.Object);

            var dept = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();
            mentorHistoryFactory.CreateMore(1, mentorObj, dept, designation, organizationFactory.Object
                , mentorEmployment.Department, mentorEmployment.Designation, mentorEmployment.Department.Organization);
            teamMemberFactory.CreateWith(
                0
                , teamMemberFactory.GetNewPin() + 1
                , "8801841" + teamMemberFactory.GetRandomNumber()
                , "MIKE TYSON"//required 
                , "টাইসন"//required
                , "https://www.facebook.com/"
                , "tyson_usa"
                , "tyson@onnorokom.com"
                , (int)Gender.Male//required
                , (int)Religion.Islam//required
                , (int)BloodGroup.Bpos
                , (int)Nationality.Bangladeshi//required
                , "11235813213455"
                , "112358"
                , "c1498982"
                , DateTime.Now
                , DateTime.Now
                , "H-22,R-3 Dhaka"
                , "H-22,R-09,Dhaka"
                , "ONNOROKOM GROUP"
                , "EMPLOYEE"
                , "01811419555"
                , "সিনিওর টাইসন"
                , "SR.TYSON"
                , "সুইফট"
                , "SWIFT"
                , "01811111111"
                , "01811111111"
                , "NIL"
                , "NIL").WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                .WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList)
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(mentorHistoryFactory.ObjectList)
                .Persist();
            Assert.Throws<InvalidDataException>(() => teamMemberService.UpdatePersonalInformation(teamMemberFactory.Object, presentThanaId, permenantThanaId, presentPostOfficeId, permenantPostOfficeId));
        }
        [Fact]
        public void Should_Update_Successfully()
        {
            DateTime startDate = DateTime.Now.AddYears(-3);
            DateTime endDate = DateTime.Now.AddYears(-1);

            const long presentThanaId = 21;
            const long permenantThanaId = 22;
            const long presentPostOfficeId = 21;
            const long permenantPostOfficeId = 22;

            //TODO:Initially create team member without mentor for testing. Mentor as a team member  
            var mentorObj = teamMemberService.GetMember(1);

            var mentorEmployment = mentorObj.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            organizationFactory.Create().Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.Object);
            maritalInfoFactory.CreateMore(1);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.Object);

            var dept = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();
            mentorHistoryFactory.CreateMore(1, mentorObj, dept, designation, organizationFactory.Object
                , mentorEmployment.Department, mentorEmployment.Designation, mentorEmployment.Department.Organization);
            teamMemberFactory.CreateWith(
                0
                , teamMemberFactory.GetNewPin() + 1
                , "8801841" + teamMemberFactory.GetRandomNumber()
                , "MIKE TYSON"//required 
                , "টাইসন"//required
                , "https://www.facebook.com/"
                , "tyson_usa"
                , "tyson@onnorokom.com"
                , (int)Gender.Male//required
                , (int)Religion.Islam//required
                , (int)BloodGroup.Bpos
                , (int)Nationality.Bangladeshi//required
                , "11235813213455"
                , "112358"
                , "c1498982"
                , DateTime.Now.AddYears(-15)
                , DateTime.Now.AddYears(-15)
                , "H-22,R-3 Dhaka"
                , "H-22,R-09,Dhaka"
                , "ONNOROKOM GROUP"
                , "EMPLOYEE"
                , "01811419555"
                , "সিনিওর টাইসন"
                , "SR.TYSON"
                , "সুইফট"
                , "SWIFT"
                , "01811111111"
                , "NIL"
                , "NIL"
                , "").WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                .WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList)
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(mentorHistoryFactory.ObjectList)
                .Persist();
            teamMemberService.UpdatePersonalInformation(teamMemberFactory.Object, presentThanaId, permenantThanaId, presentPostOfficeId, permenantPostOfficeId);
            Assert.NotNull(teamMemberFactory.Object);
            Assert.NotEqual(teamMemberFactory.Object.Id, 0);
        }

        #endregion

        #region Update Parental Info

        [Fact]
        public void Update_Should_Return_ParentalInfo_Null_Exception()
        {
            //PersistTeamMember();
            Assert.Throws<NullObjectException>(() => teamMemberService.UpdateParentalInformation(null));
        }
        [Fact]
        public void Update_Should_Return_ParentalInfo_Check_Property()
        {
            const string fatherNameEng = "";
            const string fathernameBangla = "সিনিওর টাইসন";
            const string motherNameEng = "test";
            const string mothernameBangla = "সুইফট";
            const string fatherContactNo = "1188855";
            const string motherContactNo = "41188855";

            var tm = PersistTeamMember();

            var teamMemberFactory2 = teamMemberFactory.CreateWith(
                tm.Id
                , tm.Pin
                , tm.PersonalContact
                , tm.Name
                , tm.FullNameBang
                , tm.Facebook
                , tm.Skype
                , tm.PersonalEmail
                , tm.Gender
                , (int)tm.Religion
                , (int)tm.BloodGroup
                , (int)tm.Nationality
                , tm.NID
                , tm.BCNo
                , tm.PassportNo
                , DateTime.Now
                , DateTime.Now
                , tm.PresentAddress
                , tm.PermanentAddress
                , tm.EmargencyContactName
                , tm.EmargencyContactRelation
                , tm.EmargencyContactCellNo
                , fathernameBangla
                , fatherNameEng
                , mothernameBangla
                , motherNameEng
                , fatherContactNo
                , motherContactNo
                , tm.Specialization
                , tm.ExtracurricularActivities)
                .WithEmploymentHistory(tm.EmploymentHistory.ToList())
                .WithShiftWeekend(tm.ShiftWeekendHistory.ToList())
                .WithMaritalInfo(tm.MaritalInfo.ToList())
                .WithMentorHistory(tm.MentorHistory.ToList());
            Assert.Throws<InvalidDataException>(() => teamMemberService.UpdateParentalInformation(teamMemberFactory2.Object));
        }
        [Fact]
        public void Update_Should_Return_ParentalInfo_Successfully()
        {
            const string fatherNameEng = "test";
            const string fathernameBangla = "সিনিওর টাইসন";
            const string motherNameEng = "test";
            const string mothernameBangla = "সুইফট";
            const string fatherContactNo = "8801811111111";
            const string motherContactNo = "8801811111111";

            var tm = PersistTeamMember();

            var teamMember = teamMemberService.LoadById(tm.Id);
            teamMember.FatherNameEng = fatherNameEng;
            teamMember.FatherNameBang = fathernameBangla;
            teamMember.FatherContactNo = fatherContactNo;
            teamMember.MotherNameEng = motherNameEng;
            teamMember.MotherNameBang = mothernameBangla;
            teamMember.MotherContactNo = motherContactNo;

            teamMemberService.UpdateParentalInformation(teamMember);
            Assert.NotNull(teamMember);
            Assert.NotEqual(teamMember.Id, 0);
        }
        
        #endregion

        #region Update Specialization Info

        [Fact]
        public void Update_Should_Return_SpecializationInfo_Null_Exception()
        {
            //PersistTeamMember();
            Assert.Throws<NullObjectException>(() => teamMemberService.UpdateSpecialization(null));
        }
        [Fact]
        public void Update_Should_Return_SpecializationInfo_Check_Property()
        {
            const string specialization = "test";
            const string extraCurriculamActivities = "";
            var tm = PersistTeamMember();
            var teamMember = teamMemberService.LoadById(tm.Id);
            teamMember.Specialization = specialization;
            teamMember.ExtracurricularActivities = extraCurriculamActivities;
            var updateTeamMember = teamMemberService.UpdateSpecialization(teamMember);
            Assert.True(updateTeamMember);
        }
        [Fact]
        public void Update_Should_Return_SpecializationInfo_Successfully()
        {
            const string specialization = "test";
            const string extraCurriculamActivities = "test";
            var tm = PersistTeamMember();
            var teamMember = teamMemberService.LoadById(tm.Id);
            teamMember.Specialization = specialization;
            teamMember.ExtracurricularActivities = extraCurriculamActivities;
            var updateTeamMember = teamMemberService.UpdateSpecialization(teamMember);
            Assert.True(updateTeamMember);
        }
        
        #endregion

        #region UpdateTaxInformation

        [Fact]
        public void Should_Return_UpdateTaxInformation_Successfully()
        {
            var tm = PersistTeamMember();
            tm.TaxesCircle = "Check TaxesCircle";
            tm.TaxesZone = "Check TaxesZone";
            tm.TinNo = "Check-123456789";
            var org = tm.EmploymentHistory[0].Designation.Organization;
            var br = tm.EmploymentHistory[0].Campus.Branch;
            var userMenu = BuildUserMenu(org, (Program) null, br);
            teamMemberService.UpdateTaxInformation(userMenu,tm);
            Assert.Contains("Check-123456789",teamMemberService.LoadById(tm.Id).TinNo);
        }

        [Fact]
        public void Should_Return_InvalidException_For_UserMenu()
        {
            var tm = PersistTeamMember();
            tm.TaxesCircle = "Check TaxesCircle";
            tm.TaxesZone = "Check TaxesZone";
            tm.TinNo = "Check-123456789";
           
            Assert.Throws<InvalidDataException>(()=>teamMemberService.UpdateTaxInformation(null, tm));
        }

        [Fact]
        public void Should_Return_InvalidException_For_TeamMember()
        {
            var tm = PersistTeamMember();
            tm.TaxesCircle = "Check TaxesCircle";
            tm.TaxesZone = "Check TaxesZone";
            tm.TinNo = "Check-123456789";
            var org = tm.EmploymentHistory[0].Designation.Organization;
            var br = tm.EmploymentHistory[0].Campus.Branch;
            var userMenu = BuildUserMenu(org, (Program)null, br);
            Assert.Throws<InvalidDataException>(() => teamMemberService.UpdateTaxInformation(userMenu, null));
        }

        [Fact]
        public void Should_Return_InvalidException_For_UnAuthorized_TeamMember()
        {
            var tm = PersistTeamMember();
            tm.TaxesCircle = "Check TaxesCircle";
            tm.TaxesZone = "Check TaxesZone";
            tm.TinNo = "Check-123456789";
            tm.Pin = tm.Pin + 1;
            var org = tm.EmploymentHistory[0].Designation.Organization;
            var br = tm.EmploymentHistory[0].Campus.Branch;
            var userMenu = BuildUserMenu(org, (Program)null, br);
            Assert.Throws<InvalidDataException>(() => teamMemberService.UpdateTaxInformation(userMenu, tm));
        }
        
        #endregion

        #region TranferTeamMember

        [Fact]
        public void Should_Return_TranferTeamMember_Successfully()
        {

            #region leave Variable
            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;
            #endregion

            organizationFactory.CreateMore(2).Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            shiftWeekendHistoryFactory.CreateMoreShift(1, organizationFactory.ObjectList);
            maritalInfoFactory.CreateMore(1);
            var mentorObj1 = teamMemberService.GetMember(1);
            var mentorEmployment1 = mentorObj1.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            var mentorObj2 = teamMemberService.GetMember(2);
            var mentorEmployment2 = mentorObj2.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            foreach (var organization in organizationFactory.ObjectList)
            {
                leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit,
                applicationtype, applicationDays, isEncash, minEncashReserveDays,effectiveDate, null, isMale, isFemale, isProbation,
                isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organization).Persist();    
            }

            var dept1 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation1 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept1, designation1, organizationFactory.ObjectList.FirstOrDefault()
                , mentorEmployment1.Department, mentorEmployment1.Designation, mentorEmployment1.Department.Organization);
            
            var dept2 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation2 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept2, designation2, organizationFactory.ObjectList[1]
                , mentorEmployment2.Department, mentorEmployment2.Designation, mentorEmployment2.Department.Organization);

            DateTime startDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;
            DateTime endDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;

            teamMemberFactory.Create()
                .WithEmploymentHistory(new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList.FirstOrDefault() })
                .WithShiftWeekend(new List<ShiftWeekendHistory>(){shiftWeekendHistoryFactory.ObjectList.FirstOrDefault()} )
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(new List<MentorHistory>() { mentorHistoryFactory.ObjectList.FirstOrDefault() })
                .Persist();

            List<Branch> branchList = employmentHistoryFactory.ObjectList.Select(x => x.Campus).Select(y => y.Branch).ToList();
            
            var userMenu = BuildUserMenu(organizationFactory.ObjectList, null, branchList);

            var teamMember = new TeamMember()
            {
                Id = teamMemberFactory.Object.Id,
                PersonalContact = teamMemberFactory.Object.PersonalContact,
                FullNameEng = teamMemberFactory.Object.FullNameEng,
                Name = teamMemberFactory.Object.Name,
                FullNameBang = teamMemberFactory.Object.FullNameBang,
                Facebook = teamMemberFactory.Object.Facebook,
                Skype = teamMemberFactory.Object.Skype,
                PersonalEmail = teamMemberFactory.Object.PersonalEmail,
                Gender = teamMemberFactory.Object.Gender,
                Religion = teamMemberFactory.Object.Religion,
                BloodGroup = teamMemberFactory.Object.BloodGroup,
                Nationality = teamMemberFactory.Object.Nationality,
                NID = teamMemberFactory.Object.NID,
                BCNo = teamMemberFactory.Object.BCNo,
                PassportNo = teamMemberFactory.Object.PassportNo,
                CertificateDOB = teamMemberFactory.Object.CertificateDOB,
                ActualDOB = teamMemberFactory.Object.ActualDOB,
                PresentAddress = teamMemberFactory.Object.PresentAddress,
                PermanentAddress = teamMemberFactory.Object.PermanentAddress,
                EmargencyContactName = teamMemberFactory.Object.EmargencyContactName,
                EmargencyContactRelation = teamMemberFactory.Object.EmargencyContactRelation,
                EmargencyContactCellNo = teamMemberFactory.Object.EmargencyContactCellNo,
                FatherNameBang = teamMemberFactory.Object.FatherNameBang,
                FatherNameEng = teamMemberFactory.Object.FatherNameEng,
                MotherNameBang = teamMemberFactory.Object.MotherNameBang,
                MotherNameEng = teamMemberFactory.Object.MotherNameEng,
                FatherContactNo = teamMemberFactory.Object.FatherContactNo,
                MotherContactNo = teamMemberFactory.Object.MotherContactNo,
                Specialization = teamMemberFactory.Object.Specialization,
                ExtracurricularActivities = teamMemberFactory.Object.ExtracurricularActivities,
                Pin = teamMemberFactory.Object.Pin,
                BusinessId = teamMemberFactory.Object.BusinessId,
                EmploymentHistory = new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList[1] },
                ShiftWeekendHistory = new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList[1] },
                MentorHistory = new List<MentorHistory>() { mentorHistoryFactory.ObjectList[1] }
            };
            
            teamMemberService.TranferTeamMember(userMenu, teamMember, DateTime.Now.AddDays(15));

            Assert.Contains(dept2.Name,teamMemberService.LoadById(teamMemberFactory.Object.Id).EmploymentHistory[0].Department.Name);
        }

        [Fact]
        public void Should_Return_TranferTeamMember_InvalidException_For_TeamMember()
        {
            organizationFactory.CreateMore(2).Persist();
            branchFactory.CreateMore(1, organizationFactory.ObjectList).Persist();
            List<Branch> branchList = employmentHistoryFactory.ObjectList.Select(x => x.Campus).Select(y => y.Branch).ToList();
            var userMenu = BuildUserMenu(organizationFactory.ObjectList, null, branchList);

            Assert.Throws<InvalidDataException>(
                () => teamMemberService.TranferTeamMember(userMenu, null, DateTime.Now.AddYears(15)));
        }

        [Fact]
        public void Should_Return_TranferTeamMember_InvalidException_For_Usermenu()
        {
            Assert.Throws<InvalidDataException>(
                () => teamMemberService.TranferTeamMember(null, null, DateTime.Now.AddYears(15)));
        }

        [Fact]
        public void Should_Return_TransferTeamMember_InvalidException_For_Invalid_TeamMember()
        {
            #region leave Variable
            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;
            #endregion

            organizationFactory.CreateMore(2).Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            maritalInfoFactory.CreateMore(1);
            var mentorObj1 = teamMemberService.GetMember(1);
            var mentorEmployment1 = mentorObj1.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            var mentorObj2 = teamMemberService.GetMember(2);
            var mentorEmployment2 = mentorObj2.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            foreach (var organization in organizationFactory.ObjectList)
            {
                leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit,
                applicationtype, applicationDays, isEncash, minEncashReserveDays, effectiveDate, null, isMale, isFemale, isProbation,
                isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organization).Persist();
            }

            var dept1 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation1 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept1, designation1, organizationFactory.ObjectList.FirstOrDefault()
                , mentorEmployment1.Department, mentorEmployment1.Designation, mentorEmployment1.Department.Organization);

            var dept2 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation2 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept2, designation2, organizationFactory.ObjectList[1]
                , mentorEmployment2.Department, mentorEmployment2.Designation, mentorEmployment2.Department.Organization);

            DateTime startDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;
            DateTime endDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;

            teamMemberFactory.Create()
                .WithEmploymentHistory(new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList.FirstOrDefault() })
                .WithShiftWeekend(new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList.FirstOrDefault() })
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(new List<MentorHistory>() { mentorHistoryFactory.ObjectList.FirstOrDefault() })
                .Persist();

            List<Branch> branchList = employmentHistoryFactory.ObjectList.Select(x => x.Campus).Select(y => y.Branch).ToList();
            var userMenu = BuildUserMenu(organizationFactory.ObjectList, null, branchList);

            var teamMember = new TeamMember()
            {
                Id = (teamMemberFactory.Object.Id+1),
                PersonalContact = teamMemberFactory.Object.PersonalContact,
                FullNameEng = teamMemberFactory.Object.FullNameEng,
                Name = teamMemberFactory.Object.Name,
                FullNameBang = teamMemberFactory.Object.FullNameBang,
                Facebook = teamMemberFactory.Object.Facebook,
                Skype = teamMemberFactory.Object.Skype,
                PersonalEmail = teamMemberFactory.Object.PersonalEmail,
                Gender = teamMemberFactory.Object.Gender,
                Religion = teamMemberFactory.Object.Religion,
                BloodGroup = teamMemberFactory.Object.BloodGroup,
                Nationality = teamMemberFactory.Object.Nationality,
                NID = teamMemberFactory.Object.NID,
                BCNo = teamMemberFactory.Object.BCNo,
                PassportNo = teamMemberFactory.Object.PassportNo,
                CertificateDOB = teamMemberFactory.Object.CertificateDOB,
                ActualDOB = teamMemberFactory.Object.ActualDOB,
                PresentAddress = teamMemberFactory.Object.PresentAddress,
                PermanentAddress = teamMemberFactory.Object.PermanentAddress,
                EmargencyContactName = teamMemberFactory.Object.EmargencyContactName,
                EmargencyContactRelation = teamMemberFactory.Object.EmargencyContactRelation,
                EmargencyContactCellNo = teamMemberFactory.Object.EmargencyContactCellNo,
                FatherNameBang = teamMemberFactory.Object.FatherNameBang,
                FatherNameEng = teamMemberFactory.Object.FatherNameEng,
                MotherNameBang = teamMemberFactory.Object.MotherNameBang,
                MotherNameEng = teamMemberFactory.Object.MotherNameEng,
                FatherContactNo = teamMemberFactory.Object.FatherContactNo,
                MotherContactNo = teamMemberFactory.Object.MotherContactNo,
                Specialization = teamMemberFactory.Object.Specialization,
                ExtracurricularActivities = teamMemberFactory.Object.ExtracurricularActivities,
                Pin = teamMemberFactory.Object.Pin,
                BusinessId = teamMemberFactory.Object.BusinessId,
                EmploymentHistory = new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList[1] },
                ShiftWeekendHistory = new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList[1] },
                MentorHistory = new List<MentorHistory>() { mentorHistoryFactory.ObjectList[1] }
            };
            Assert.Throws<InvalidDataException>(() => teamMemberService.TranferTeamMember(userMenu, teamMember, DateTime.Now.AddDays(15)));
        }

        [Fact]
        public void Should_Return_TranferTeamMember_INvalidException_For_Same_Organization()
        {
            #region leave Variable
            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;
            #endregion

            organizationFactory.CreateMore(2).Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            maritalInfoFactory.CreateMore(1);
            var mentorObj1 = teamMemberService.GetMember(1);
            var mentorEmployment1 = mentorObj1.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            var mentorObj2 = teamMemberService.GetMember(2);
            var mentorEmployment2 = mentorObj2.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            foreach (var organization in organizationFactory.ObjectList)
            {
                leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit,
                applicationtype, applicationDays, isEncash, minEncashReserveDays, effectiveDate, null, isMale, isFemale, isProbation,
                isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organization).Persist();
            }

            var dept1 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation1 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept1, designation1, organizationFactory.ObjectList.FirstOrDefault()
                , mentorEmployment1.Department, mentorEmployment1.Designation, mentorEmployment1.Department.Organization);

            var dept2 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation2 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept2, designation2, organizationFactory.ObjectList[1]
                , mentorEmployment2.Department, mentorEmployment2.Designation, mentorEmployment2.Department.Organization);

            DateTime startDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;
            DateTime endDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;

            teamMemberFactory.Create()
                .WithEmploymentHistory(new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList.FirstOrDefault() })
                .WithShiftWeekend(new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList.FirstOrDefault() })
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(new List<MentorHistory>() { mentorHistoryFactory.ObjectList.FirstOrDefault() })
                .Persist();

            List<Branch> branchList = employmentHistoryFactory.ObjectList.Select(x => x.Campus).Select(y => y.Branch).ToList();
            var userMenu = BuildUserMenu(organizationFactory.ObjectList, null, branchList);

            var teamMember = new TeamMember()
            {
                Id = (teamMemberFactory.Object.Id),
                PersonalContact = teamMemberFactory.Object.PersonalContact,
                FullNameEng = teamMemberFactory.Object.FullNameEng,
                Name = teamMemberFactory.Object.Name,
                FullNameBang = teamMemberFactory.Object.FullNameBang,
                Facebook = teamMemberFactory.Object.Facebook,
                Skype = teamMemberFactory.Object.Skype,
                PersonalEmail = teamMemberFactory.Object.PersonalEmail,
                Gender = teamMemberFactory.Object.Gender,
                Religion = teamMemberFactory.Object.Religion,
                BloodGroup = teamMemberFactory.Object.BloodGroup,
                Nationality = teamMemberFactory.Object.Nationality,
                NID = teamMemberFactory.Object.NID,
                BCNo = teamMemberFactory.Object.BCNo,
                PassportNo = teamMemberFactory.Object.PassportNo,
                CertificateDOB = teamMemberFactory.Object.CertificateDOB,
                ActualDOB = teamMemberFactory.Object.ActualDOB,
                PresentAddress = teamMemberFactory.Object.PresentAddress,
                PermanentAddress = teamMemberFactory.Object.PermanentAddress,
                EmargencyContactName = teamMemberFactory.Object.EmargencyContactName,
                EmargencyContactRelation = teamMemberFactory.Object.EmargencyContactRelation,
                EmargencyContactCellNo = teamMemberFactory.Object.EmargencyContactCellNo,
                FatherNameBang = teamMemberFactory.Object.FatherNameBang,
                FatherNameEng = teamMemberFactory.Object.FatherNameEng,
                MotherNameBang = teamMemberFactory.Object.MotherNameBang,
                MotherNameEng = teamMemberFactory.Object.MotherNameEng,
                FatherContactNo = teamMemberFactory.Object.FatherContactNo,
                MotherContactNo = teamMemberFactory.Object.MotherContactNo,
                Specialization = teamMemberFactory.Object.Specialization,
                ExtracurricularActivities = teamMemberFactory.Object.ExtracurricularActivities,
                Pin = teamMemberFactory.Object.Pin,
                BusinessId = teamMemberFactory.Object.BusinessId,
                EmploymentHistory = new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList[0] },
                ShiftWeekendHistory = new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList[1] },
                MentorHistory = new List<MentorHistory>() { mentorHistoryFactory.ObjectList[1] }
            };
            Assert.Throws<InvalidDataException>(() => teamMemberService.TranferTeamMember(userMenu, teamMember, DateTime.Now.AddDays(15)));
        }

        [Fact]
        public void Should_Return_TranferTeamMember_InvalidException_For_EffectiveDate_Is_GreaterThan_TransferingDate()
        {
            #region leave Variable
            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;
            #endregion

            organizationFactory.CreateMore(2).Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            maritalInfoFactory.CreateMore(1);
            var mentorObj1 = teamMemberService.GetMember(1);
            var mentorEmployment1 = mentorObj1.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            var mentorObj2 = teamMemberService.GetMember(2);
            var mentorEmployment2 = mentorObj2.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            foreach (var organization in organizationFactory.ObjectList)
            {
                leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit,
                applicationtype, applicationDays, isEncash, minEncashReserveDays, effectiveDate, null, isMale, isFemale, isProbation,
                isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organization).Persist();
            }

            var dept1 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation1 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept1, designation1, organizationFactory.ObjectList.FirstOrDefault()
                , mentorEmployment1.Department, mentorEmployment1.Designation, mentorEmployment1.Department.Organization);

            var dept2 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation2 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept2, designation2, organizationFactory.ObjectList[1]
                , mentorEmployment2.Department, mentorEmployment2.Designation, mentorEmployment2.Department.Organization);

            DateTime startDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;
            DateTime endDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;

            teamMemberFactory.Create()
                .WithEmploymentHistory(new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList.FirstOrDefault() })
                .WithShiftWeekend(new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList.FirstOrDefault() })
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(new List<MentorHistory>() { mentorHistoryFactory.ObjectList.FirstOrDefault() })
                .Persist();

            List<Branch> branchList = employmentHistoryFactory.ObjectList.Select(x => x.Campus).Select(y => y.Branch).ToList();
            var userMenu = BuildUserMenu(organizationFactory.ObjectList, null, branchList);

            var teamMember = new TeamMember()
            {
                Id = (teamMemberFactory.Object.Id),
                PersonalContact = teamMemberFactory.Object.PersonalContact,
                FullNameEng = teamMemberFactory.Object.FullNameEng,
                Name = teamMemberFactory.Object.Name,
                FullNameBang = teamMemberFactory.Object.FullNameBang,
                Facebook = teamMemberFactory.Object.Facebook,
                Skype = teamMemberFactory.Object.Skype,
                PersonalEmail = teamMemberFactory.Object.PersonalEmail,
                Gender = teamMemberFactory.Object.Gender,
                Religion = teamMemberFactory.Object.Religion,
                BloodGroup = teamMemberFactory.Object.BloodGroup,
                Nationality = teamMemberFactory.Object.Nationality,
                NID = teamMemberFactory.Object.NID,
                BCNo = teamMemberFactory.Object.BCNo,
                PassportNo = teamMemberFactory.Object.PassportNo,
                CertificateDOB = teamMemberFactory.Object.CertificateDOB,
                ActualDOB = teamMemberFactory.Object.ActualDOB,
                PresentAddress = teamMemberFactory.Object.PresentAddress,
                PermanentAddress = teamMemberFactory.Object.PermanentAddress,
                EmargencyContactName = teamMemberFactory.Object.EmargencyContactName,
                EmargencyContactRelation = teamMemberFactory.Object.EmargencyContactRelation,
                EmargencyContactCellNo = teamMemberFactory.Object.EmargencyContactCellNo,
                FatherNameBang = teamMemberFactory.Object.FatherNameBang,
                FatherNameEng = teamMemberFactory.Object.FatherNameEng,
                MotherNameBang = teamMemberFactory.Object.MotherNameBang,
                MotherNameEng = teamMemberFactory.Object.MotherNameEng,
                FatherContactNo = teamMemberFactory.Object.FatherContactNo,
                MotherContactNo = teamMemberFactory.Object.MotherContactNo,
                Specialization = teamMemberFactory.Object.Specialization,
                ExtracurricularActivities = teamMemberFactory.Object.ExtracurricularActivities,
                Pin = teamMemberFactory.Object.Pin,
                BusinessId = teamMemberFactory.Object.BusinessId,
                EmploymentHistory = new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList[1] },
                ShiftWeekendHistory = new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList[1] },
                MentorHistory = new List<MentorHistory>() { mentorHistoryFactory.ObjectList[1] }
            };
            Assert.Throws<InvalidDataException>(() => teamMemberService.TranferTeamMember(userMenu, teamMember, DateTime.Now.AddDays(-25)));
        }

        [Fact]
        public void Should_Return_TranferTeamMember_InvalidException_For_ShiftWeekendEffectiveDate_Is_GreaterThan_TransferingDate()
        {
            #region leave Variable
            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;
            #endregion

            organizationFactory.CreateMore(2).Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            maritalInfoFactory.CreateMore(1);
            var mentorObj1 = teamMemberService.GetMember(1);
            var mentorEmployment1 = mentorObj1.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            var mentorObj2 = teamMemberService.GetMember(2);
            var mentorEmployment2 = mentorObj2.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            foreach (var organization in organizationFactory.ObjectList)
            {
                leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit,
                applicationtype, applicationDays, isEncash, minEncashReserveDays, effectiveDate, null, isMale, isFemale, isProbation,
                isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organization).Persist();
            }

            var dept1 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation1 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept1, designation1, organizationFactory.ObjectList.FirstOrDefault()
                , mentorEmployment1.Department, mentorEmployment1.Designation, mentorEmployment1.Department.Organization);

            var dept2 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation2 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept2, designation2, organizationFactory.ObjectList[1]
                , mentorEmployment2.Department, mentorEmployment2.Designation, mentorEmployment2.Department.Organization);

            DateTime startDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;
            DateTime endDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;

            teamMemberFactory.Create()
                .WithEmploymentHistory(new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList.FirstOrDefault() })
                .WithShiftWeekend(new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList.FirstOrDefault() })
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(new List<MentorHistory>() { mentorHistoryFactory.ObjectList.FirstOrDefault() })
                .Persist();

            List<Branch> branchList = employmentHistoryFactory.ObjectList.Select(x => x.Campus).Select(y => y.Branch).ToList();
            var userMenu = BuildUserMenu(organizationFactory.ObjectList, null, branchList);

            var teamMember = new TeamMember()
            {
                Id = (teamMemberFactory.Object.Id),
                PersonalContact = teamMemberFactory.Object.PersonalContact,
                FullNameEng = teamMemberFactory.Object.FullNameEng,
                Name = teamMemberFactory.Object.Name,
                FullNameBang = teamMemberFactory.Object.FullNameBang,
                Facebook = teamMemberFactory.Object.Facebook,
                Skype = teamMemberFactory.Object.Skype,
                PersonalEmail = teamMemberFactory.Object.PersonalEmail,
                Gender = teamMemberFactory.Object.Gender,
                Religion = teamMemberFactory.Object.Religion,
                BloodGroup = teamMemberFactory.Object.BloodGroup,
                Nationality = teamMemberFactory.Object.Nationality,
                NID = teamMemberFactory.Object.NID,
                BCNo = teamMemberFactory.Object.BCNo,
                PassportNo = teamMemberFactory.Object.PassportNo,
                CertificateDOB = teamMemberFactory.Object.CertificateDOB,
                ActualDOB = teamMemberFactory.Object.ActualDOB,
                PresentAddress = teamMemberFactory.Object.PresentAddress,
                PermanentAddress = teamMemberFactory.Object.PermanentAddress,
                EmargencyContactName = teamMemberFactory.Object.EmargencyContactName,
                EmargencyContactRelation = teamMemberFactory.Object.EmargencyContactRelation,
                EmargencyContactCellNo = teamMemberFactory.Object.EmargencyContactCellNo,
                FatherNameBang = teamMemberFactory.Object.FatherNameBang,
                FatherNameEng = teamMemberFactory.Object.FatherNameEng,
                MotherNameBang = teamMemberFactory.Object.MotherNameBang,
                MotherNameEng = teamMemberFactory.Object.MotherNameEng,
                FatherContactNo = teamMemberFactory.Object.FatherContactNo,
                MotherContactNo = teamMemberFactory.Object.MotherContactNo,
                Specialization = teamMemberFactory.Object.Specialization,
                ExtracurricularActivities = teamMemberFactory.Object.ExtracurricularActivities,
                Pin = teamMemberFactory.Object.Pin,
                BusinessId = teamMemberFactory.Object.BusinessId,
                EmploymentHistory = new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList[1] },
                ShiftWeekendHistory = new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList[1] },
                MentorHistory = new List<MentorHistory>() { mentorHistoryFactory.ObjectList[1] }
            };
            Assert.Throws<InvalidDataException>(() => teamMemberService.TranferTeamMember(userMenu, teamMember, DateTime.Now.AddDays(-25)));
        }

        [Fact]
        public void Should_Return_TranferTeamMember_InvalidException_For_PendingLeave()
        
        {
            #region leave Variable
            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;
            #endregion

            organizationFactory.CreateMore(2).Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            maritalInfoFactory.CreateMore(1);

            var mentorObj1 = teamMemberService.GetMember(1);
            var mentorEmployment1 = mentorObj1.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            var mentorObj2 = teamMemberService.GetMember(2);
            var mentorEmployment2 = mentorObj2.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            foreach (var organization in organizationFactory.ObjectList)
            {
                leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit,
                applicationtype, applicationDays, isEncash, minEncashReserveDays, effectiveDate, null, isMale, isFemale, isProbation,
                isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organization).Persist();
            }

            var dept1 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation1 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept1, designation1, organizationFactory.ObjectList.FirstOrDefault()
                , mentorEmployment1.Department, mentorEmployment1.Designation, mentorEmployment1.Department.Organization);

            var dept2 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation2 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept2, designation2, organizationFactory.ObjectList[1]
                , mentorEmployment2.Department, mentorEmployment2.Designation, mentorEmployment2.Department.Organization);

            DateTime startDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;
            DateTime endDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;

            teamMemberFactory.Create()
                .WithEmploymentHistory(new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList.FirstOrDefault() })
                .WithShiftWeekend(new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList.FirstOrDefault() })
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(new List<MentorHistory>() { mentorHistoryFactory.ObjectList.FirstOrDefault() })
                .Persist();

            List<Branch> branchList = employmentHistoryFactory.ObjectList.Select(x => x.Campus).Select(y => y.Branch).ToList();
            var userMenu = BuildUserMenu(organizationFactory.ObjectList, null, branchList);

            var leave = leaveFactory.SingleObjectList[0];
            var memberWeekend = teamMemberFactory.Object.ShiftWeekendHistory.OrderByDescending(x => x.EffectiveDate).Select(x => x.Weekend).FirstOrDefault();

            //var dayOfWeek = DateTime.Now.ToString("dddd");
            //int dayOfWeekId = (int)Enum.Parse(typeof(WeekDay), dayOfWeek);

            int days = (int)memberWeekend + 2;
            var expectedDay = (int)Enum.Parse(typeof(WeekDay), DateTime.Now.AddDays(days).DayOfWeek.ToString());
            if (expectedDay == memberWeekend)
                days = days + 1;

            leaveApplicationFactory.CreateWith(0, DateTime.Now.AddDays(days), DateTime.Now.AddDays(days), 
                "Testing Note", (int)LeaveStatus.Pending, "null")
                .WithLeave(leave)
                .WithTeamMember(teamMemberFactory.Object)
                .WithResposibleTeamMember(mentorObj2)
                .Persist();

            Session.Evict(teamMemberFactory.Object);
            //var tm = _teamMemberService.LoadById(_teamMemberFactory.Object.Id).LeaveApplication;
            var teamMember = new TeamMember()
            {
                Id = (teamMemberFactory.Object.Id),
                PersonalContact = teamMemberFactory.Object.PersonalContact,
                FullNameEng = teamMemberFactory.Object.FullNameEng,
                Name = teamMemberFactory.Object.Name,
                FullNameBang = teamMemberFactory.Object.FullNameBang,
                Facebook = teamMemberFactory.Object.Facebook,
                Skype = teamMemberFactory.Object.Skype,
                PersonalEmail = teamMemberFactory.Object.PersonalEmail,
                Gender = teamMemberFactory.Object.Gender,
                Religion = teamMemberFactory.Object.Religion,
                BloodGroup = teamMemberFactory.Object.BloodGroup,
                Nationality = teamMemberFactory.Object.Nationality,
                NID = teamMemberFactory.Object.NID,
                BCNo = teamMemberFactory.Object.BCNo,
                PassportNo = teamMemberFactory.Object.PassportNo,
                CertificateDOB = teamMemberFactory.Object.CertificateDOB,
                ActualDOB = teamMemberFactory.Object.ActualDOB,
                PresentAddress = teamMemberFactory.Object.PresentAddress,
                PermanentAddress = teamMemberFactory.Object.PermanentAddress,
                EmargencyContactName = teamMemberFactory.Object.EmargencyContactName,
                EmargencyContactRelation = teamMemberFactory.Object.EmargencyContactRelation,
                EmargencyContactCellNo = teamMemberFactory.Object.EmargencyContactCellNo,
                FatherNameBang = teamMemberFactory.Object.FatherNameBang,
                FatherNameEng = teamMemberFactory.Object.FatherNameEng,
                MotherNameBang = teamMemberFactory.Object.MotherNameBang,
                MotherNameEng = teamMemberFactory.Object.MotherNameEng,
                FatherContactNo = teamMemberFactory.Object.FatherContactNo,
                MotherContactNo = teamMemberFactory.Object.MotherContactNo,
                Specialization = teamMemberFactory.Object.Specialization,
                ExtracurricularActivities = teamMemberFactory.Object.ExtracurricularActivities,
                Pin = teamMemberFactory.Object.Pin,
                BusinessId = teamMemberFactory.Object.BusinessId,
                EmploymentHistory = new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList[1] },
                ShiftWeekendHistory = new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList[1] },
                MentorHistory = new List<MentorHistory>() { mentorHistoryFactory.ObjectList[1] },
                LeaveApplication = leaveApplicationFactory.ObjectList
            };
            Assert.Throws<InvalidDataException>(() => teamMemberService.TranferTeamMember(userMenu, teamMember, DateTime.Now.AddDays(2)));
        }
        
        [Fact]
        public void Should_Return_TranferTeamMember_InvalidException_For_UnAuthorized_UserMenu()
        {

            #region leave Variable

            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;

            #endregion

            organizationFactory.CreateMore(2).Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            maritalInfoFactory.CreateMore(1);
            var mentorObj1 = teamMemberService.GetMember(1);
            var mentorEmployment1 = mentorObj1.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            var mentorObj2 = teamMemberService.GetMember(2);
            var mentorEmployment2 = mentorObj2.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            foreach (var organization in organizationFactory.ObjectList)
            {
                leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit,
                applicationtype, applicationDays, isEncash, minEncashReserveDays, effectiveDate, null, isMale, isFemale, isProbation,
                isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organization).Persist();
            }

            var dept1 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation1 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept1, designation1, organizationFactory.ObjectList.FirstOrDefault()
                , mentorEmployment1.Department, mentorEmployment1.Designation, mentorEmployment1.Department.Organization);

            var dept2 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation2 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept2, designation2, organizationFactory.ObjectList[1]
                , mentorEmployment2.Department, mentorEmployment2.Designation, mentorEmployment2.Department.Organization);

            DateTime startDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;
            DateTime endDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;

            teamMemberFactory.Create()
                .WithEmploymentHistory(new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList.FirstOrDefault() })
                .WithShiftWeekend(new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList.FirstOrDefault() })
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(new List<MentorHistory>() { mentorHistoryFactory.ObjectList.FirstOrDefault() })
                .Persist();

            List<Branch> branchList = employmentHistoryFactory.ObjectList.Select(x => x.Campus).Select(y => y.Branch).ToList();
            List<Branch> bList = new List<Branch>();
            var i = 15;
            foreach (var branch in branchList)
            {
                var br = new Branch()
                {
                    Id = branch.Id + i,
                    Organization = branch.Organization,
                    Code = branch.Code,
                    Name = branch.Name,
                    Rank = branch.Rank,
                    IsCorporate = branch.IsCorporate
                };
                bList.Add(br);
                i++;
            }
            var userMenu = BuildUserMenu(organizationFactory.ObjectList, null, bList);

            var teamMember = new TeamMember()
            {
                Id = teamMemberFactory.Object.Id,
                PersonalContact = teamMemberFactory.Object.PersonalContact,
                FullNameEng = teamMemberFactory.Object.FullNameEng,
                Name = teamMemberFactory.Object.Name,
                FullNameBang = teamMemberFactory.Object.FullNameBang,
                Facebook = teamMemberFactory.Object.Facebook,
                Skype = teamMemberFactory.Object.Skype,
                PersonalEmail = teamMemberFactory.Object.PersonalEmail,
                Gender = teamMemberFactory.Object.Gender,
                Religion = teamMemberFactory.Object.Religion,
                BloodGroup = teamMemberFactory.Object.BloodGroup,
                Nationality = teamMemberFactory.Object.Nationality,
                NID = teamMemberFactory.Object.NID,
                BCNo = teamMemberFactory.Object.BCNo,
                PassportNo = teamMemberFactory.Object.PassportNo,
                CertificateDOB = teamMemberFactory.Object.CertificateDOB,
                ActualDOB = teamMemberFactory.Object.ActualDOB,
                PresentAddress = teamMemberFactory.Object.PresentAddress,
                PermanentAddress = teamMemberFactory.Object.PermanentAddress,
                EmargencyContactName = teamMemberFactory.Object.EmargencyContactName,
                EmargencyContactRelation = teamMemberFactory.Object.EmargencyContactRelation,
                EmargencyContactCellNo = teamMemberFactory.Object.EmargencyContactCellNo,
                FatherNameBang = teamMemberFactory.Object.FatherNameBang,
                FatherNameEng = teamMemberFactory.Object.FatherNameEng,
                MotherNameBang = teamMemberFactory.Object.MotherNameBang,
                MotherNameEng = teamMemberFactory.Object.MotherNameEng,
                FatherContactNo = teamMemberFactory.Object.FatherContactNo,
                MotherContactNo = teamMemberFactory.Object.MotherContactNo,
                Specialization = teamMemberFactory.Object.Specialization,
                ExtracurricularActivities = teamMemberFactory.Object.ExtracurricularActivities,
                Pin = teamMemberFactory.Object.Pin,
                BusinessId = teamMemberFactory.Object.BusinessId,
                EmploymentHistory = new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList[1] },
                ShiftWeekendHistory = new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList[1] },
                MentorHistory = new List<MentorHistory>() { mentorHistoryFactory.ObjectList[1] }
            };
            
            Assert.Throws<InvalidDataException>(() => teamMemberService.TranferTeamMember(userMenu, teamMember, DateTime.Now.AddDays(15)));
        }

        [Fact]
        public void Should_Return_TranferTeamMember_InvalidException_For_EmploymentHistory()
        {

            #region leave Variable

            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;

            #endregion

            organizationFactory.CreateMore(2).Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            maritalInfoFactory.CreateMore(1);
            var mentorObj1 = teamMemberService.GetMember(1);
            var mentorEmployment1 = mentorObj1.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            var mentorObj2 = teamMemberService.GetMember(2);
            var mentorEmployment2 = mentorObj2.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            foreach (var organization in organizationFactory.ObjectList)
            {
                leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit,
                applicationtype, applicationDays, isEncash, minEncashReserveDays, effectiveDate, null, isMale, isFemale, isProbation,
                isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organization).Persist();
            }

            var dept1 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation1 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept1, designation1, organizationFactory.ObjectList.FirstOrDefault()
                , mentorEmployment1.Department, mentorEmployment1.Designation, mentorEmployment1.Department.Organization);

            var dept2 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation2 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept2, designation2, organizationFactory.ObjectList[1]
                , mentorEmployment2.Department, mentorEmployment2.Designation, mentorEmployment2.Department.Organization);

            DateTime startDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;
            DateTime endDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;

            teamMemberFactory.Create()
                .WithEmploymentHistory(new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList.FirstOrDefault() })
                .WithShiftWeekend(new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList.FirstOrDefault() })
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(new List<MentorHistory>() { mentorHistoryFactory.ObjectList.FirstOrDefault() })
                .Persist();

            List<Branch> branchList = employmentHistoryFactory.ObjectList.Select(x => x.Campus).Select(y => y.Branch).ToList();

            var userMenu = BuildUserMenu(organizationFactory.ObjectList, null, branchList);

            var teamMember = new TeamMember()
            {
                Id = teamMemberFactory.Object.Id,
                PersonalContact = teamMemberFactory.Object.PersonalContact,
                FullNameEng = teamMemberFactory.Object.FullNameEng,
                Name = teamMemberFactory.Object.Name,
                FullNameBang = teamMemberFactory.Object.FullNameBang,
                Facebook = teamMemberFactory.Object.Facebook,
                Skype = teamMemberFactory.Object.Skype,
                PersonalEmail = teamMemberFactory.Object.PersonalEmail,
                Gender = teamMemberFactory.Object.Gender,
                Religion = teamMemberFactory.Object.Religion,
                BloodGroup = teamMemberFactory.Object.BloodGroup,
                Nationality = teamMemberFactory.Object.Nationality,
                NID = teamMemberFactory.Object.NID,
                BCNo = teamMemberFactory.Object.BCNo,
                PassportNo = teamMemberFactory.Object.PassportNo,
                CertificateDOB = teamMemberFactory.Object.CertificateDOB,
                ActualDOB = teamMemberFactory.Object.ActualDOB,
                PresentAddress = teamMemberFactory.Object.PresentAddress,
                PermanentAddress = teamMemberFactory.Object.PermanentAddress,
                EmargencyContactName = teamMemberFactory.Object.EmargencyContactName,
                EmargencyContactRelation = teamMemberFactory.Object.EmargencyContactRelation,
                EmargencyContactCellNo = teamMemberFactory.Object.EmargencyContactCellNo,
                FatherNameBang = teamMemberFactory.Object.FatherNameBang,
                FatherNameEng = teamMemberFactory.Object.FatherNameEng,
                MotherNameBang = teamMemberFactory.Object.MotherNameBang,
                MotherNameEng = teamMemberFactory.Object.MotherNameEng,
                FatherContactNo = teamMemberFactory.Object.FatherContactNo,
                MotherContactNo = teamMemberFactory.Object.MotherContactNo,
                Specialization = teamMemberFactory.Object.Specialization,
                ExtracurricularActivities = teamMemberFactory.Object.ExtracurricularActivities,
                Pin = teamMemberFactory.Object.Pin,
                BusinessId = teamMemberFactory.Object.BusinessId,
                //EmploymentHistory = new List<EmploymentHistory>() { _employmentHistoryFactory.ObjectList[1] },
                ShiftWeekendHistory = new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList[1] },
                MentorHistory = new List<MentorHistory>() { mentorHistoryFactory.ObjectList[1] }
            };
            
            Assert.Throws<InvalidDataException>(() => teamMemberService.TranferTeamMember(userMenu, teamMember, DateTime.Now.AddDays(15)));
        }

        [Fact]
        public void Should_Return_TranferTeamMember_InvalidException_For_ShiftWeekendHistory()
        {

            #region leave Variable

            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 2;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;

            #endregion

            organizationFactory.CreateMore(2).Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.ObjectList);
            maritalInfoFactory.CreateMore(1);
            var mentorObj1 = teamMemberService.GetMember(1);
            var mentorEmployment1 = mentorObj1.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            var mentorObj2 = teamMemberService.GetMember(2);
            var mentorEmployment2 = mentorObj2.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            foreach (var organization in organizationFactory.ObjectList)
            {
                leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit, maxTakingLimit,
                applicationtype, applicationDays, isEncash, minEncashReserveDays, effectiveDate, null, isMale, isFemale, isProbation,
                isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organization).Persist();
            }

            var dept1 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation1 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept1, designation1, organizationFactory.ObjectList.FirstOrDefault()
                , mentorEmployment1.Department, mentorEmployment1.Designation, mentorEmployment1.Department.Organization);

            var dept2 = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation2 = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj1, dept2, designation2, organizationFactory.ObjectList[1]
                , mentorEmployment2.Department, mentorEmployment2.Designation, mentorEmployment2.Department.Organization);

            DateTime startDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;
            DateTime endDate = employmentHistoryFactory.ObjectList.FirstOrDefault().EffectiveDate;

            teamMemberFactory.Create()
                .WithEmploymentHistory(new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList.FirstOrDefault() })
                .WithShiftWeekend(new List<ShiftWeekendHistory>() { shiftWeekendHistoryFactory.ObjectList.FirstOrDefault() })
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(new List<MentorHistory>() { mentorHistoryFactory.ObjectList.FirstOrDefault() })
                .Persist();

            List<Branch> branchList = employmentHistoryFactory.ObjectList.Select(x => x.Campus).Select(y => y.Branch).ToList();

            var userMenu = BuildUserMenu(organizationFactory.ObjectList, null, branchList);

            var teamMember = new TeamMember()
            {
                Id = teamMemberFactory.Object.Id,
                PersonalContact = teamMemberFactory.Object.PersonalContact,
                FullNameEng = teamMemberFactory.Object.FullNameEng,
                Name = teamMemberFactory.Object.Name,
                FullNameBang = teamMemberFactory.Object.FullNameBang,
                Facebook = teamMemberFactory.Object.Facebook,
                Skype = teamMemberFactory.Object.Skype,
                PersonalEmail = teamMemberFactory.Object.PersonalEmail,
                Gender = teamMemberFactory.Object.Gender,
                Religion = teamMemberFactory.Object.Religion,
                BloodGroup = teamMemberFactory.Object.BloodGroup,
                Nationality = teamMemberFactory.Object.Nationality,
                NID = teamMemberFactory.Object.NID,
                BCNo = teamMemberFactory.Object.BCNo,
                PassportNo = teamMemberFactory.Object.PassportNo,
                CertificateDOB = teamMemberFactory.Object.CertificateDOB,
                ActualDOB = teamMemberFactory.Object.ActualDOB,
                PresentAddress = teamMemberFactory.Object.PresentAddress,
                PermanentAddress = teamMemberFactory.Object.PermanentAddress,
                EmargencyContactName = teamMemberFactory.Object.EmargencyContactName,
                EmargencyContactRelation = teamMemberFactory.Object.EmargencyContactRelation,
                EmargencyContactCellNo = teamMemberFactory.Object.EmargencyContactCellNo,
                FatherNameBang = teamMemberFactory.Object.FatherNameBang,
                FatherNameEng = teamMemberFactory.Object.FatherNameEng,
                MotherNameBang = teamMemberFactory.Object.MotherNameBang,
                MotherNameEng = teamMemberFactory.Object.MotherNameEng,
                FatherContactNo = teamMemberFactory.Object.FatherContactNo,
                MotherContactNo = teamMemberFactory.Object.MotherContactNo,
                Specialization = teamMemberFactory.Object.Specialization,
                ExtracurricularActivities = teamMemberFactory.Object.ExtracurricularActivities,
                Pin = teamMemberFactory.Object.Pin,
                BusinessId = teamMemberFactory.Object.BusinessId,
                EmploymentHistory = new List<EmploymentHistory>() { employmentHistoryFactory.ObjectList[1] },
                //ShiftWeekendHistory = new List<ShiftWeekendHistory>() { _shiftWeekendHistoryFactory.ObjectList[1] },
                MentorHistory = new List<MentorHistory>() { mentorHistoryFactory.ObjectList[1] }
            };
            
            Assert.Throws<InvalidDataException>(() => teamMemberService.TranferTeamMember(userMenu, teamMember, DateTime.Now.AddDays(15)));
        }
        
        #endregion

    }
}
